#if !defined(AFX_TVIEW_H__F6AD14DF_4D67_467B_8145_F4379E48F556__INCLUDED_)
#define AFX_TVIEW_H__F6AD14DF_4D67_467B_8145_F4379E48F556__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TView.h : header file
//
#include "MEMDC.H"
#include "ULFormView.h"

/////////////////////////////////////////////////////////////////////////////
// CTView view

class CProject;
class CTView : public CULFormView
{

public:
	CTView(LPVOID lpParam = NULL);
	DECLARE_DYNAMIC(CTView)

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTView)
	protected:
	virtual void OnUpdate();			// overridden to update this view
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CTView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	afx_msg void OnDataChangedVIWWELL1CTRL1();
	DECLARE_MESSAGE_MAP()

public:
	virtual int CalcPrintHeight();
	virtual void LoadTempl(LPCTSTR pszFile);
	virtual void SaveAsTempl(LPCTSTR pszFile);
	virtual void SaveService();
	void GetCompData(BOOL bUpdate = FALSE);
	void SetCompData();
	
// Operations
public:
	CPtrArray  m_arrTools;
// Attributes
public:
	CProject*	m_pWellPI;

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TVIEW_H__F6AD14DF_4D67_467B_8145_F4379E48F556__INCLUDED_)

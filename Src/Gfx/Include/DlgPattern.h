#if !defined(AFX_DLGPATTERN_H__198DBD51_2790_49C3_BA37_48F54F25A68D__INCLUDED_)
#define AFX_DLGPATTERN_H__198DBD51_2790_49C3_BA37_48F54F25A68D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPattern.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgPattern dialog

class CDlgPattern : public CDialog
{
// Construction
public:
	CDlgPattern(DWORD dwPT = 0x11, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgPattern)
	enum { IDD = IDD_PATTERN };
	CComboBox	m_cmbType;
	CListCtrl	m_wndList;
	//}}AFX_DATA
	DWORD m_dwPT;
	CString m_strExt;
	CString m_strFolder;
	CString m_strSelect;
	CImageList m_ImageList;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgPattern)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgPattern)
	virtual BOOL OnInitDialog();
	afx_msg void OnClickList1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkList1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchangeCombo1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPATTERN_H__198DBD51_2790_49C3_BA37_48F54F25A68D__INCLUDED_)

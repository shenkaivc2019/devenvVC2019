// ULCalibrate.h: interface for the CULCalibrate class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ULCALIBRATE_H__4362AFAF_B1BD_4521_AC9A_696C5D0418F2__INCLUDED_)
#define AFX_ULCALIBRATE_H__4362AFAF_B1BD_4521_AC9A_696C5D0418F2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ULCAL.H"

class CXMLSettings;
/* ------------------------------- *
 *  Classes used for calibrating
 * ------------------------------------- */

typedef std::vector<double> vec_dbl;

class CULCalDot : public CObject
{
	DECLARE_SERIAL(CULCalDot)
public:
	CULCalDot();
	CULCalDot(CString Name, CString Alias, int nPri,
						int Destime, BOOL bFixedPoint = FALSE,
						CString strTip = _T("Tip"));
	virtual	~CULCalDot();

	virtual void	Serialize(CArchive& ar);
	virtual void	Serialize(CXMLSettings& xml, BOOL bEx = FALSE);
	virtual void	Serializestr(CXMLSettings& xml, BOOL bEx = FALSE);
	void operator = (const CULCalDot& Dot);

public:
	void			SetDateTm();
	void			SetCalStatus(BOOL bStatus);
	BOOL			GetCalStatus();
	void			SetTip(CString tip);
	void			GetTip(CString& tip);

	inline double UpdateMeas()
	{
		if (m_bFixedPoint)
			return m_fMeas;
		
		m_fMeas = m_Buffer.size() ? m_Buffer.back() : 0.0;
		return m_fMeas;
	}

	inline BOOL IsExceed()
	{
		if (m_fUserMin == m_fUserMax)
			return  FALSE;

		return ((m_fMeas < m_fUserMin) || (m_fMeas > m_fUserMax));
	}

	inline void Exceed()
	{
		int n = m_Buffer.size();
		int i = 0;
		for ( ; i < n; i++)
		{
			double f = m_Buffer[i];

			if (f < m_fUserMin)
				break;
			
			if (f > m_fUserMax)
				break;
		}

		m_bExceed = (i < n);
	}

	inline void CalcMinMax()
	{
		int nData = m_Buffer.size();
		if (nData < 1)
			return ;

		m_fMinMeas = m_Buffer.front();
		m_fMaxMeas = m_Buffer.front();
		for (int i = 0; i < nData; i++)
		{
			double f = m_Buffer[i];
			if (f < m_fMinMeas)
				m_fMinMeas = f;
			if (f > m_fMaxMeas)
				m_fMaxMeas = f;
		}
		
		m_bExceed = (m_fMinMeas < m_fUserMin || m_fMaxMeas > m_fUserMax);
	}

public:
	CString		m_strName;		// 零刻、正刻等
	CString		m_strAlias;		// 别名 
	CString		m_strUnit;
	int			m_iPriority;	// 优先级(0级最高)
	BOOL		m_bSelected;	// 是否选中为当前刻度点
	BOOL		m_bCal;			// TRUE-已刻  FALSE-未刻
	int			m_iDestTm;		// 预置的刻度时间,以s为单位
	time_t		m_time;			// 刻度日期时间
	CString		m_strTip;		// Tip
	double		m_fMeas;		// 测量值
	double		m_fEng;			// 工程值
	double		m_fMinStand;
	double		m_fMaxStand;
	double		m_fFineStand;	// 标称值

	BOOL		m_bFixedPoint;	// 该点是否为定点，gzh,2002/9/16
	CString		m_strTime;		// 记录当时刻度的时间，gzh,2002/9/16
	CString		m_strCalMachineInfo;	// 刻度器信息，保存刻度器信息, added by gzh, 2002/9/10 
	DWORD		m_dwIndex;		// 标示刻度点的索引号0x xx xx xx

	// 如果用户编辑了测量值 ylm 0415 : 解决用户编辑测量值后无法应用的问题
	BOOL		m_bEditMeasure;
	double		m_fUserMin;		// 用户指定采集最小值
	double		m_fUserMax;		// 用户指定采集最大值
	double		m_fUserNormal;	// 用户指定标准值

	BOOL		m_bExceed;	// 采集值是否越界
	double		m_fMaxMeas;	// 采集最大值
	double		m_fMinMeas;	// 采集最小值
	vec_dbl		m_Buffer;	// 刻度时，存放从仪器来的数据
	int			m_nX;		// 刻度点对应CELL表x坐标
	int			m_nY;		// 刻度点对应CELL表y坐标	
};

typedef CArray<ULCalCoef, ULCalCoef&>	CCalCoefArray;

class CULCalCurveSeg : public CObject
{
	DECLARE_SERIAL(CULCalCurveSeg)
public:
	CULCalCurveSeg();
	virtual			~CULCalCurveSeg();
	virtual void	Serialize(CArchive& ar);
	virtual void	Serialize(CXMLSettings& xml);
	virtual void	SerializeEx(CXMLSettings& xml);
	virtual void    Serializestr(CXMLSettings& xml);
	void			operator=(const CULCalCurveSeg& Seg);

public:
	int				GetCalDotCount();		// 得到刻度点的总数
	CULCalDot*		GetCalDot(int nIndex);	// 得到刻度线段上的某一刻度点

	void	AddCalDot(CString strName, CString strAlias, UINT uPri,	UINT uTime);
	void	AddCalDot(CULCalDot* pCalDot);
	int		GetDotIndex(CULCalDot* pDot);
	int		GetCurveHighPriority();
	int		GetDotPriority(LPCTSTR pszName);
	BOOL	IsLowerPriority(LPCTSTR pszItem);
	void	SetSegCalStatus();

	inline CULCalDot* GetCalDot(LPCTSTR pszName)
	{
		for (int i = 0; i < m_CalDotList.GetSize(); i++)
		{
			CULCalDot* pDot = (CULCalDot*)m_CalDotList.GetAt(i);
			if (pDot->m_strName == pszName)
				return pDot;
			if (pDot->m_strAlias == pszName)
				return pDot;
		}

		return NULL;
	}

	// ---------------------------------
	//	得到刻度项目的预置时间:
	// ---------------------------------
	int GetDotDestTM(LPCTSTR pszName)
	{
		CULCalDot* pDot = GetCalDot(pszName);
		return pDot ? pDot->m_iDestTm : -1;
	}
	
	inline void	ClearDots()
	{
		for (int i = m_CalDotList.GetSize() - 1; i > -1 ; i--)
		{
			CULCalDot* pDot = (CULCalDot*)m_CalDotList.GetAt(i);
			delete pDot;
		}
		m_CalDotList.RemoveAll();
	}

	inline int GetCoef(LPCTSTR pszCoef, ULCalCoef& cc)
	{
		int i = m_CalCoefs.GetSize() - 1;
		for ( ; i > -1; i--)
		{
			cc = m_CalCoefs.GetAt(i);
			if (cc.szName == pszCoef)
				break;
			if (cc.szAlias == pszCoef)
				break;
		}

		return i;
	}

	inline BOOL ExceedsLimit()
	{
		int i = m_CalCoefs.GetSize() - 1;
		for ( ; i > -1; i--)
		{
			ULCalCoef cc = m_CalCoefs.GetAt(i);
			if (cc.dRMin == cc.dRMax)
				continue;
			
			if (cc.dValue < cc.dRMin || cc.dRMax < cc.dValue)
				return TRUE;
		}

		for (i = m_CalDotList.GetSize() - 1; i > -1; i--)
		{
			CULCalDot* pDot = (CULCalDot*)m_CalDotList.GetAt(i);
			if (pDot && pDot->IsExceed())
				return TRUE;
		}

		return FALSE;
	}

public:
	CString			m_strName;		// 线段名
	double			m_fValue;		// 线段值
	CString			m_strUnit;  	// 单位
	CPtrArray		m_CalDotList;   // 刻度点数组
	BOOL			m_bSelected;	// 是否选中为当前刻度线段
	BOOL			m_bCal; 		// TRUE-已刻完，FALSE-未刻完
	CCalCoefArray	m_CalCoefs;  	// 增益，偏值
	CString			m_strCalTm[4];	// 刻度时间			
};

// ---------------------------------
//	刻度曲线:
// ---------------------------------

class CULCalCurve : public CObject
{
	DECLARE_SERIAL(CULCalCurve)
public:
	CULCalCurve();
	virtual			~CULCalCurve();
	virtual void	Serialize(CArchive& ar);
	virtual void	Serialize(CXMLSettings& xml);
	virtual void    SerializeEx(CXMLSettings& xml);
	virtual void    Serializestr(CXMLSettings& xml);
	void			operator=(const CULCalCurve& Curve);

public:
	void GetNeighborSeg(double dSegValue, CULCalCurveSeg*& pSeg1, CULCalCurveSeg*& pSeg2);
	void			SetCurveCalStatus();
	int				GetDotCount();
	int				GetCurveSegIndex(CULCalCurveSeg* p);
	CULCalCurveSeg*	GetCurveSeg(int iIndex);
	CULCalCurveSeg*	GetCurveSeg(double dValue);
	CULCalCurveSeg* GetCurveSeg(LPCTSTR pszValue);
	void			AddCurveSeg(double fValue, CString szName = "");
	void			AddCurveSeg(CULCalCurveSeg* pSeg);
	int				GetSegCount();

	inline void ClearCSs()
	{
		for( int i = m_CalCSs.GetSize() - 1; i > -1; i--)
		{
			CULCalCurveSeg* pCS = (CULCalCurveSeg*) m_CalCSs.GetAt(i);
			delete pCS;
		}
		m_CalCSs.RemoveAll();
	}

public:
	CString		m_strVersion;		// 版本号
	CString		m_strName;			// 曲线名
	CString		m_strUnit;			// 单位
	BOOL		m_bSelected;		// 是否选中为当前刻度曲线
	BOOL		m_bCaled;			// 是否刻度完成 
	BOOL		m_bCalMethod;		// 刻度模式 TRUE正常刻度，FALSE表格查询 
	CPtrArray	m_CalCSs;			// 刻度线段链表
	CString		m_strTip;			// 注释
	int			m_nPrecis;			// 精度
	int			m_nCalGroup;		// 刻度曲线的分组
};

#define EXCEEDS_TIME	1
#define NOT_DONE		2
#define NO_ITEM			4

typedef struct tagCalDraw1
{
	CString	strTool;
	CString strCurve;
	CString strUnit;
	CString strCS;
	CString strDraw;
	DWORD	dwPhase;
	int		nPrecis;
	int		nState[4];
	double  fValue[4];
	double  fMaxValue;
	double  fMinValue;
	double  fNorValue;
	tagCalDraw1() {
		nPrecis = -1;
		nState[0] = 0;
		nState[1] = 0;
		nState[2] = 0;
		nState[3] = 0;
		fValue[0] = 0;
		fValue[1] = 0;
		fValue[2] = 0;
		fValue[3] = 0;
		fMaxValue = 0;
		fMinValue = 0;
		fNorValue = 0;
	}
} CALDRAW1;

typedef struct tagCALDRAW
{
	CString	strTool;
	CString strCurve;
	CString strDot;
	CULCalDot*	pCalDot[4];
	double		fMaxValue;
	double		fMinValue;
	double		fNorValue;
	tagCALDRAW() {
		pCalDot[0] = NULL;
		pCalDot[1] = NULL;
		pCalDot[2] = NULL;
		pCalDot[3] = NULL;
		fMaxValue = 0;
		fMinValue = 0;
		fNorValue = 0;
	}
}CALDRAW;

extern DWORD dwCalPhase[4];

#endif // !defined(AFX_ULCALIBRATE_H__4362AFAF_B1BD_4521_AC9A_696C5D0418F2__INCLUDED_)

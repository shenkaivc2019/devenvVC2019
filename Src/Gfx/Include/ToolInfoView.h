#if !defined(AFX_TOOLINFOVIEW_H__1336DDD7_CBDB_4506_803B_A359530CABA3__INCLUDED_)
#define AFX_TOOLINFOVIEW_H__1336DDD7_CBDB_4506_803B_A359530CABA3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ToolInfoView.h : header file
//

#include "Cell2000.h"
#include "ulview.h"
#include "ulInterface.h"

#define		CELL_NULL_HEIGHT		10

class CULTool;

/////////////////////////////////////////////////////////////////////////////
// CToolInfoView view

class CToolInfoView : public CULView
{
	DECLARE_DYNCREATE(CToolInfoView)
public:
	CToolInfoView(LPVOID pTools  = NULL );           // protected constructor used by dynamic creation

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CToolInfoView)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CToolInfoView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CToolInfoView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	virtual int CalcPageHeight();
	virtual int CalcPrintHeight();
	virtual void Print();
	void Clear();
	void InitToolInfo();
	void OpenCellFile();
	void OpenCellFile(CCell2000* pCell);

	virtual void SaveAsBitmap(CDC* pDC, LPCTSTR pszFile);
protected:
	void InitCellData(CCell2000* pCell = NULL);

public:
	CCell2000	m_Cell;
	CPtrArray	m_ToolInfoList;
	CPtrArray*	m_pArrTools;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TOOLINFOVIEW_H__1336DDD7_CBDB_4506_803B_A359530CABA3__INCLUDED_)

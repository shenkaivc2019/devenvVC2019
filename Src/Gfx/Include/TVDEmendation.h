// TVDEmendation.h: interface for the CTVDEmendation class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TVDEMENDATION_H__20C212FF_6BCC_49EF_9DAD_832F930C4F01__INCLUDED_)
#define AFX_TVDEMENDATION_H__20C212FF_6BCC_49EF_9DAD_832F930C4F01__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "IULFile.h"
#include "Units.h"

class CCurve;
class CCurveData;
class AFX_EXT_CLASS CTVDEmendation  
{
public:
	CTVDEmendation();
	virtual ~CTVDEmendation();

	BOOL Process(CUnits* pUnits, IULFile* pFile);
	long TVD(long lMD0, long lOffset0, CCurveData* pTVD, CCurveData* pDEV, CCurveData* pAZIM, int& nCenter); //CCurveData* pAZIM, qyh add 4.16
	long TVD2(long lMD0, long lOffset0, CCurveData* pData, CCurveData* pDEV, CCurveData* pAZIM, int& nCenter);
	void Interpolation(CCurveData* pData, long lStep, int nFrom);
};

#endif // !defined(AFX_TVDEMENDATION_H__20C212FF_6BCC_49EF_9DAD_832F930C4F01__INCLUDED_)

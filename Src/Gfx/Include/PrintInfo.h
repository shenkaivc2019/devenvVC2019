﻿// PrintInfo.h: interface for the CULPrintInfo class.
//
//////////////////////////////////////////////////////////////////////

#pragma warning(disable:4244)

#include "ULCOMMDEF.H"

#if !defined(AFX_PRINTINFO_H__DF1C27ED_A8B9_44BE_ABC7_D7A2F45C3511__INCLUDED_)
#define AFX_PRINTINFO_H__DF1C27ED_A8B9_44BE_ABC7_D7A2F45C3511__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifndef PLOTENTITY_API
	#ifdef PLOTENTITY‌_EXPORT
		#define PLOTENTITY_API __declspec( dllexport )
	#else	
		#define PLOTENTITY_API __declspec( dllimport )
	#endif
#endif

#define		PM_FILE		0x0000
#define		PM_RTIME	0x0001
#define		PM_SHOOT	0x0002
#define		PM_REPLAY	0x0003

class PLOTENTITY_API CULPrintInfo : public CObject
{
public:
	CULPrintInfo();
	virtual ~CULPrintInfo();

// Operations
public:
	void    Init();
	BOOL    PrinterCalibrate(int  nMillimeter);
	//	参数：	nMillimeter : 刻度的长度(毫米)
	//	返回值：TRUE：刻度成功， FALSE ： 刻度失败
	//	说明：	打印机刻度

	BOOL PrinterCalibration(const int & nHLength = 100, const int & nVLength = 100, const int & nGrade = 256,const BOOL bCal = FALSE );
	//	参数：	nHLength : 刻度的水平长度(毫米)
	//	参数：	nVLength : 刻度的垂直长度
	//	参数：	nGrade : 刻度的灰度度
	//	返回值：TRUE：刻度成功， FALSE ： 刻度失败
	//	说明：	打印机刻度		   

	BOOL   SetPrintDC();
	//	参数：	无
	//	返回值：TRUE：设置成功， FALSE ： 设置失败
	//	说明：	设置打印机DC

	void   PrinterSetUp();
	//	说明：	设置打印机DC

	void    ReleasePrintDC();
	//	说明：	释放打印机DC

	CDC* GetPrintDC();
	//	参数：	无
	//	返回值：打印机DC
	//	说明：	获得打印机DC

	CSize  GetPrinterPageSize(); 
	//	参数：	无
	//	返回值：打印纸张
	//	说明：	获得打印机缺省纸张信息
	
	CRect SetPageInfo();
	void SetPageInfo(const CRect& rcPage);
	//	参数： szPage  : 纸张大小
	//	返回值： 无
	//	说明：设置打印机纸张

	void SetPageSize(BOOL bLogging = FALSE);
	
	CRect   GetPageInfo();
	//	参数：无
	//	返回值： 当前设置的打印机纸张信息
	//	说明：获得打印纸张信息
	
	void   SetPrintRatio(float nRatio);
	//	参数：nRatio :比例
	//	返回值： 无
	//	说明：设置打印比例

	float    GetPrintRatio();
	//	参数：无
	//	返回值： 当前打印比例
	//	说明：获得打印比例

	double  SetPrintStartDepth(double  fStartDepth);
	//	参数： fStartDepth : 开始深度
	//	返回值：打印开始深度
	//	说明：设置打印开始深度
	
	double   GetPrintStartDepth();
	//	参数：无
	//	返回值：打印开始深度
	//	说明：获得打印开始深度
	
	void  SetPrintEndDepth(double fEndDepth);
	//	参数； fEndDepth : 打印结束深度
	//	返回值：无
	//	说明：设置打印结束深度
	
	double   GetPrintEndDepth();
	//	参数： 无
	//	返回值： 打印结束深度
	//	说明：获得打印结束深度
	
	void   SetPrintNum(int nPrintNum);
	//	参数：nPrintNum :打印页数
	//	返回值： 无
	//	说明：设置打印页号
	
	int   GetPrintNum();
	//	参数： 无
	//	返回值：当前已打印页数
	//	说明：获得当前打印页数
	
	int   AddPrintNum(); 
	//	参数： 无
	//	返回值：当前打印页数
	//	说明：打印页数增加

	void  SetPrintDirection(int nDirection);
	//	参数： nDirection : 方向
	//	返回值：无
	//	说明：设置打印方向

	int   GetPrintDirection();
	//	参数：无
	//	返回值：当前打印方向
	//	说明：获得打印方向

	// 用于设置测井视图的起始打印坐标
	void SetStartPrintPos(int nStartPos);
	
	// 设置当前打印页的起始深度
	void SetCurPrintDepth(double fCurDepth);

	void  SetPrintReviseX(const double & fReviseX);
	//	参数： fReviseX :X方向打印刻度系数
	//	返回值：无
	//	说明：设置X方向上刻度系数

	void  SetPrintReviseY(const double & fReviseY);
	//	参数： fReviseY :Y方向打印刻度系数
	//	返回值：无
	//	说明：设置Y方向上刻度系数

	double GetPrintReviseX();
	//	参数： 无
	//	返回值：X方向打印刻度系数
	//	说明：获得X方向上刻度系数

	double GetPrintReviseY();
	//	参数： 无
	//	返回值：Y方向打印刻度系数
	//	说明：获得Y方向上刻度系数

	CFont* SetFont(CSize szFont, int nEscapement = 0, BYTE bUnderline = FALSE, long lfWeight = FW_SEMIBOLD);
	CFont* SetFont(CFont* pNewFont, int nEscapement = 0, BYTE bUnderline = FALSE);
	void PrintText(const CString& str, int x, int y);
	void PrintText(const CString& str, int x, int y, int nEscapement, CPen* pPen = new CPen(PS_SOLID, 1, RGB(0, 0, 0)));
	void   PrintText(CSize szfont, int x, int y, CString strText, int nEscapement = 0, 
					int nBkMode = TRANSPARENT, BYTE bUnderline = FALSE, long lfWeight = FW_SEMIBOLD,
					CPen* pPen = new CPen(PS_SOLID, 1, RGB(0, 0, 0)));
	//	参数： szFont : 字体大小. x,y : 打印输出位置  strText :打印内容 . nEscapement : 打印方向
	//	返回值：无
	//	说明：在打印纸某位置输出
	void   PrintText(CString strText, CRect rcText, CSize szFont = CSize(14, 15), int nEscapement = 0, UINT nFormat = DT_SINGLELINE | DT_VCENTER | DT_CENTER);
	//	参数： strText :打印内容 . rcText : 打印输出矩形 ， szFont :字体大小 ,nFormat : 对齐方式 , nEscapement :方向
	//	返回值：无
	//	说明：在打印纸某位置输出

	CRect CoordinateConvert(CRect rect);
	//	参数： rect  矩形
	//	返回值：转换结果
	//	说明：根据方向将矩形座标进行转换

	int  CoordinateConvertX(int  x);
	//	参数： x: 点X座标
	//	返回值：转换结果
	//	说明：根据方向将点X座标进行转换

	int GetPrintDepthCoordinate(double fDepth);
	//	参数：fDepth : 深度
	//	返回值：打印座标值
	//	说明：根据方向将深度转换为座标, 均未经过刻度系数校正的坐标

	void  CoordinateCalibrate(CRect &rect);
	//	参数： rect  矩形
	//	返回值：无
	//	说明：根据打印机刻度系数对座标进行校正

	void  CoordinateCalibrate(CPoint &point);
	//	参数： point
	//	返回值：无
	//	说明：根据打印机刻度系数对座标进行校正

	CRect CoordCalibrate(CRect rect);
	CPoint CoordCalibrate(CPoint point);

	/*int*/ double GetMeterPerPage();
	//	参数： 无
	//	返回值：一页可打多少米
	//	说明：确定一页可打多少米
	
	int   GetMaxPrintPageNum(int& nY); 
	//	参数： 无	
	//	返回值：总共可打多少页
	//	说明：由开始深度和结束深度确定总共可打多少页
	//  使用：在回放打印时用来计算打印页数	  

	void PrintHollowPage(long lHeight); 
	// 打印空白页，2 cm	长度，作为各个打印工作之间的分割

	BOOL StartPrintDoc(UINT nIDDocName);
	BOOL StartPrintDoc(LPCTSTR lpszDocName);
	CString EndPrintDoc();
	BOOL	SetDefPrinter(LPTSTR pPrinterName);
	BOOL	SetPdfPrinter();
	BOOL	RestorePrinter();
	HDC		CreatePrinter(LPTSTR pPrinterName);

	// 时间驱动
	double  SetPrintStartTime(double  fStartTime);	
	double   GetPrintStartTime();	
	void  SetPrintEndTime(double fEndTime);	
	double   GetPrintEndTime();
	void SetCurPrintTime(double fCurTime);
	int GetPrintTimeCoordinate(double fTime); // 时间原点在下方则为False，在上方则为TRUE
	double GetMilliSecondPerPage();	
	int   GetMaxPrintPageNumTime(int& nY); 
	BOOL	SetTimeTop(BOOL bTimeTop);
// Attributes
public:
	CDC     m_printDC;				// 打印机DC
	CString m_strDevice;			// 打印机名称
	BOOL	m_bColor;				// 彩色打印
	HDC		m_hDCDef;				// 默认打印机
	CString m_strDef;				// 默认打印机名
	BOOL	m_bColorDef;			// 默认打印机颜色
	HDC		m_hDCPdf;				// PDF打印机
	CString m_strPdf;				// PDF打印机名
	BOOL	m_bColorPdf;			// PDF打印机颜色
	DWORD	m_dwMode;				// 是否实时打印或射孔
	int		m_nPageRH;				// 实时打印页高
	int		m_nDDC;
	int		m_nPDC;
	BOOL	m_bStartDoc;
	BOOL	m_bShoot;

	CString m_strDoc;
	CString m_strLastDoc;

private:
	CRect   m_rcPage;				// 打印纸张信息
	double	m_fStartPrintDepth;		// 打印开始深度(为整数)
	double	m_fEndPrintDepth;		// 打印结束深度
	int     m_nPageNO;				// 当前页号
	int     m_nDirection; 			// (测井)打印方向
	int     m_nStartPrintPos;       // 打印的起始坐标
	double  m_fCurPrintDepth;       // 当前页的打印起始深度
	double  m_fPrintReviseX;		// 打印机X方向刻度系数
	double  m_fPrintReviseY;		// 打印机Y方向刻度系数
	float	m_nRatio;				// 比例
	double	m_nUnitPerPage;			// 每页能打的米数
	double  m_fLMPerPage;			// 每页能打的0.1mm数
	BOOL	m_bCalibrated;			// 打印机刻度
	
	// 时间
	double	m_fStartPrintTime;		// 打印开始时间(为整数)
	double	m_fEndPrintTime;		// 打印结束时间
	double  m_fCurPrintTime;       // 当前页的打印起始时间
	double	m_nTimePerPage;			// 每页能打的毫秒数
	BOOL	m_bTimeTop;				//	时间驱动时，时间原点的位置，TRUE为在上方
public:
	CDC*    m_pMemDC;
	CString m_strUnit;              //打印单位
	float   m_fUnit;               //公英制单位

};

#endif // !defined(AFX_PRINTINFO_H__DF1C27ED_A8B9_44BE_ABC7_D7A2F45C3511__INCLUDED_)

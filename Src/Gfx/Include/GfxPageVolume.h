#if !defined(AFX_GFXPAGEVOLUME_H__C974F9D4_285C_4E97_853C_AA4A52D571D1__INCLUDED_)
#define AFX_GFXPAGEVOLUME_H__C974F9D4_285C_4E97_853C_AA4A52D571D1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GfxPageVolume.h : header file
//

#include "GfxPage.h"
#include "Curve.h"

class CGraphWnd;

/////////////////////////////////////////////////////////////////////////////
// CGfxPageVolume dialog

class CGfxPageVolume : public CGfxPage
{
// Construction
public:
	CGfxPageVolume(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CGfxPageVolume)
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_GFX_PAGE_VOLUME };
#endif
	CComboBox   m_cbCal;
	CComboBox   m_cbTT;
	int		m_radioShow;
	int     m_nHVMajorPip;
	int     m_nHVMinorPip;
	int     m_nCVMajorPip;
	int     m_nCVMinorPip;
	float  m_fMajorPip;
	float  m_fMinorPip;
	//}}AFX_DATA
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGfxPageVolume)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CGfxPageVolume)
	afx_msg void OnSelchangeCbCal();
	afx_msg void OnSelchangeCbTT();
	afx_msg void OnCheck1();
	afx_msg void OnCheck2();
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	afx_msg void OnRadio3();
	afx_msg void OnKillfocusEdit1();
	afx_msg void OnKillfocusEdit2();
	afx_msg void OnKillfocusEdit3();
	afx_msg void OnKillfocusEdit4();
	afx_msg void OnKillfocusEdit5();
	afx_msg void OnKillfocusEdit6();
	afx_msg void OnChangeEdit7();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void	SelectVolume(int nIndex);
	void	Refresh();
	void    ShowDepthItem(int nShow);
	void    InitCurve();

public:
	int				 m_nTrackType;
	CMFCColorButton m_btnColor;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GFXPAGEVOLUME_H__C974F9D4_285C_4E97_853C_AA4A52D571D1__INCLUDED_)

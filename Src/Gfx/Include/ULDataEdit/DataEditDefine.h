#ifndef __DATAEDITDEFINE_H__ 
#define	__DATAEDITDEFINE_H__

#include <afxtempl.h>
#include "ULCOMMDEF.H"

//操作行为定义
#define  MOVE_HORZ			0		//水平移动
#define  MOVE_VERT			1		//垂直移动
#define  EXPEND_HORZ		2		//水平缩放
#define  EXPEND_VERT		3		//垂直缩放
#define  BASELINE_REVISE	4		//基线校正
#define  FILTER				5		//滤波
#define  DEL				6		//删除
#define  CUT				7		//剪切
#define  PASTE				8		//粘贴
#define  COPY				9		//复制
#define  UNIFORM			10		//切齐
#define  IMAGE				11		//镜象
#define  TURN				12		//反转
#define  INTERPOLATEION		13		//插补
#define  UNDO				14		//恢复
#define  REDO				15		//重做
#define  MOVE_HORZVERT		16		//两个方 向同时移动
#define  RECOVER			17      //恢复删除
#define  ONEPOINTCORRECT	18		//单点修正
#define  COMBINE			19		//曲线拼接
#define  REPLACE			20		//曲线拼接
#define  REVERSE            21      //曲线反向
#define  SPRING_DEPTH		23		//弹性校深
#define  GRAY_LINER			32      //二维曲线灰度线性变换
#define  GRAY_THRESHOLD		33      //二维曲线灰度阈值变换
#define  GRAY_WINDOW		34		//二维曲线灰度窗口变换
#define  GRAY_STRETCH		35      //二维曲线灰度拉伸
#define	 GRAY_EQUALIZE		36		//二维曲线灰度均衡
#define	 ENHA_GRADSHARP		37		//二维曲线图像增强梯度锐化
#define  IAMGE_MANAGE       38      
#define  DEPTH_ADJUSTMENT   40      
#define  USER_DEFINE        50      
#define	 MULTI_BEGIN		51		
#define	 MULTI_END			52		
#define  FLEX_DETPH			53      

#define	 ENHA_SMOOTH		54		
#define	 ENHA_MEDIANFILTER	55		
#define	 ENHA_GAUSSION		56		
#define	 ENHA_LAPLACE		57		

			
// Declare union type
typedef union EDIT_PARAM   //编辑码参数定义
{
   char     ch;
   int      nParam;
   long     lParam;
   float    fParam;
   double   dParam;
   WORD		wParam;
   DWORD	dwParam;
}EDIT_PARAM ;

// 曲线数据删除模式
#define		DELETE_MODE_DATAINVALID	    0
#define		DELETE_MODE_AUTO_FILL	    1
#define		DELETE_MODE_UPTOCOMBINE     2
#define		DELETE_MODE_DOWNTOCOMBINE   3

// 曲线数据粘贴方式
#define		PASTE_MODE_REPLACE			0
#define		PASTE_MODE_UP				1
#define		PASTE_MODE_DOWN				2

//UNDO/REDO方式定义
#define		DATAMANAGE_DO				0
#define		DATAMANAGE_UNDO				1
#define		DATAMANAGE_REDO				2

class CCurve;
class CCurveData;

class DATA_EDIT_OPERATE;

//操作记录数组
typedef CArray  <DATA_EDIT_OPERATE, DATA_EDIT_OPERATE&> OPERATE_LIST;

//操作码定义
class DATA_EDIT_OPERATE 
{
public:
	CCurve*		 pCurve;			//要编辑的曲线指针
	CCurveData*	 pCurveData;		//要编辑的曲线数据指针
	UINT		 nOperateType;		//操作类型：0 整条曲线、1 局部曲线、2 多条曲线
	int          nDriveMode;        //深度驱动或时间驱动
	long		 lTopDepth;			//要编辑的深度范围的开始深度
	long		 lBottomDepth;		//要编辑的深度范围的结束深度
	int 		 nOperateCode;		//编辑操作码
	UINT 		 nModifyCount;		//曲线编辑点个数
	EDIT_PARAM	 unionParam1;		//操作参数1
	EDIT_PARAM	 unionParam2;		//操作参数2
	EDIT_PARAM	 unionParam3;	 	//操作参数3
	EDIT_PARAM   unionParam4;       //操作参数4
	OPERATE_LIST opList;            //操作列表
public:
	DATA_EDIT_OPERATE()
	{
		pCurve = NULL;
		pCurveData = NULL;
		nOperateType = 0;
		nDriveMode = UL_DRIVE_DEPT;
		lTopDepth = 0;
		lBottomDepth = 0;
		nOperateCode = 0;
		nModifyCount = 0;
	}

	DATA_EDIT_OPERATE(const DATA_EDIT_OPERATE& op)
	{
		pCurve = op.pCurve;
		pCurveData = op.pCurveData;
		nOperateType = op.nOperateType;
		nDriveMode = op.nDriveMode;
		lTopDepth = op.lTopDepth;
		lBottomDepth = op.lBottomDepth;
		nOperateCode = op.nOperateCode;
		nModifyCount = op.nModifyCount;
		unionParam1 = op.unionParam1;
		unionParam2 = op.unionParam2;
		unionParam3 = op.unionParam3;
		unionParam4 = op.unionParam4;
		opList.Copy(op.opList);
	}
	
	int operator=(const DATA_EDIT_OPERATE& op)
	{
		pCurve = op.pCurve;
		pCurveData = op.pCurveData;
		nOperateType = op.nOperateType;
		nDriveMode = op.nDriveMode;
		lTopDepth = op.lTopDepth;
		lBottomDepth = op.lBottomDepth;
		nOperateCode = op.nOperateCode;
		nModifyCount = op.nModifyCount;
		unionParam1 = op.unionParam1;
		unionParam2 = op.unionParam2;
		unionParam3 = op.unionParam3;
		unionParam4 = op.unionParam4;
		opList.Copy(op.opList);
		return 0;
	}
};
											   
//以下为临时文件结构定义
typedef struct DATAEDIT_TEMPFILEHEAD
{
	float 	 fileVersion;    //文件版本号
	char 	 szMarker [40];  //文件类型标识
	UINT	 nRecordCount;   //记录个数
}ULDATATEMPFILEHEAD;

//文件记录结构定义：
typedef   struct RECORDHEAD
{
	char   szCurveName[20] ; //曲线名称
	UINT   nPointFrame ;	 //一行数据点个数
	int    ID;			     //曲线ID
	long  lTopDepth ;       //数据深度范围
	long  lBottomDepth;  
	UINT   nDataSize;        //数据点个数
	CPtrArray DataList ;     //数据点列表
}ULDATATEMPFILERECORD;

#endif
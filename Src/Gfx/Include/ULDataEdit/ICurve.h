/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __IULCURVE_H__
#define __IULCURVE_H__

#include "ULInterface.h"
#include "ulcommdef.h"
#define CS_VISIBLE	0x0001
#define CS_PRINT	0x0002
#define CS_CLOSE    0x0004
#define CS_REWIND	0x0100

// Interface
interface ICurve : IUnknown
{
	// 属性存取
	ULMETHOD	SetName(LPCTSTR lpszName) = 0;
	ULMETHOD	GetName(LPTSTR lpszName) = 0;
	ULMTSTR		Name() = 0;

	ULMETHOD	SetSource(LPCTSTR lpszSrc) = 0;
	ULMETHOD	GetSource(LPTSTR lpszSrc) = 0;

	ULMETHOD	SetUnit(LPCTSTR lpszUnit) = 0;
	ULMETHOD	GetUnit(LPTSTR lpszUnit) = 0;
	ULMTSTR		Unit() = 0;	

	ULMETHOD	SetPointFrame(short nPointFrame) = 0; 
	ULMETHOD	GetPointFrame() = 0;

	ULMBOOL		IsName(LPTSTR lpszName) = 0;
	ULMBOOL		IsDelete() = 0;
	ULMBOOL		IsSelect() = 0;
	ULMBOOL		IsVisible()= 0;
	ULMBOOL		IsSave() = 0;
	ULMBOOL		IsPrint() = 0;

	ULMETHOD	Delete(BOOL bDel) = 0;
	ULMETHOD	Select(BOOL bSel) = 0;
	
	ULMINT		Dimension() = 0;		// 维数
	ULMLNG		DepthDistance() = 0;	// 深度间隔
	ULMLNG		DepthDelay() = 0;		// 深度延迟
	ULMBOOL		DrawOffset() = 0;
	ULMETHOD	SetValueType(VARTYPE vt) = 0;
	ULMINT      GetValueType() = 0;
	ULMINT		GetSizePoint() = 0;

	ULMETHOD	SetSamplesCount(int nSamplesCount) = 0; // 垂直方向采样点数
	ULMINT		GetSamplesCount() = 0;

	// 基本操作
	ULMETHOD	SetDepth(int nIndex, long lDepth) = 0;
	ULMLNG		GetDepth(int nIndex) = 0;
	ULMETHOD	SetTime(int nIndex, long lTime) = 0;
	ULMLNG		GetTime(int nIndex) = 0;

	ULMETHOD	SetCurveProp(void* pCurveProp) = 0;
	ULMETHOD	GetCurveProp(void* pCurveProp) = 0;

	// -----------------------------------------------------------------------
	//	数据添加
	// -----------------------------------------------------------------------

	// 一维曲线
	ULMETHOD	AddData(long lDepth, float fValue, long lTime = 0) = 0; 
	ULMETHOD	AddData(long lDepth, double dValue, long lTime = 0) = 0;
	ULMETHOD	AddData(long lDepth, WORD wValue, long lTime = 0) = 0;

	// 一维|多维
	ULMETHOD	AddData(long lDepth, void* pValue, long lTime = 0) = 0;
	
	// 添加状态曲线数据
	ULMETHOD	AddStatusData(long lDepth, float fValue, long lTime = 0) = 0;
	ULMETHOD	AddStatusData(long lDepth, double dValue, long lTime = 0) = 0;
	ULMETHOD	AddStatusData(long lDepth, WORD wValue, long lTime = 0) = 0;
	ULMETHOD	AddStatusData(long lDepth, void* pValue, long lTime = 0) = 0;

	// 数据链表中的数据多少
	ULMINT		GetDataSize() = 0;
	ULMINT		GetDataUpBound() = 0;

	ULMDBL		LeftVal() = 0;	// 左值
	ULMDBL		RightVal()= 0;	// 右值
	ULMDBL		LRdVal() = 0;	// 差值(左 - 右)
	ULMDBL		RLdVal() = 0;	// 差值(右 - 左)      

	// 数据取值
	ULMDBL		GetValue(int nIndex0, int nIndex1 = 0) = 0;
	ULMFLT		GetfltValue(int nIndex0, int nIndex1 = 0) = 0;
	ULMDBL		GetdblValue(int nIndex0, int nIndex1 = 0) = 0;
	ULMWRD		GetwrdValue(int nIndex0, int nIndex1 = 0) = 0;
	ULMETHOD	GetValue(int nIndex0, void* pValue) = 0;
	ULMPTR		GetValueBuffer(int nIndex, BOOL bForceRange = FALSE) = 0;
	ULMINT		GetValueBufferSize() = 0;
	ULMDBL		GetValueOfBuffer(void* pValueBuffer, int nIndex1) = 0;

	ULMDBL		GetValueAtDepth(long lDepth, int nIndex1 = 0) = 0;
	ULMDBL		GetValueAtTime(long lTime, int nIndex1 = 0) = 0;

	ULMDBL		GetValueMax(int nIndex0, int nStart = -1, int nEnd = -1) = 0;
	ULMDBL		GetValueMin(int nIndex0, int nStart = -1, int nEnd = -1) = 0;
	ULMDBL		GetValueMax(void* pValueBuffer, int nStart, int nEnd) = 0;
	ULMDBL		GetValueMin(void* pValueBuffer, int nStart, int nEnd) = 0;

	ULMINT		GetFirstIndexByDepth(long lDepth) = 0;
	ULMINT		GetFirstIndexByTime(long lTime) = 0;
	ULMINT		GetFirstIndexByDepth(long lDepth, int nDirection) = 0;
	ULMINT		GetFirstIndexByTime(long lTime, int nDirection) = 0;

	ULMINT		GetDataMode(int nIndex) = 0;
    ULMETHOD	SetDataMode(int nIndex, DWORD bMode) = 0;

	// 绘图
	ULMINT		LineStyle() = 0;
	ULMINT		LineWidth() = 0;
	ULMDWD		Color() = 0;

	ULMINT		LeftGrid() = 0;
	ULMINT		RightGrid()= 0;

	ULMINT		GetCurveMode() = 0;
	ULMETHOD	SetCurveMode(int nCurveMode) = 0;

	ULMLNG		GetBKBegin() = 0;
	ULMLNG		GetBKEnd() = 0;
	ULMINT		GetCurDisplayPos() = 0;
	ULMETHOD	SetCurDisplayPos(int nPos) = 0;
	ULMINT		IncCurDisplayPos(int nPosAdd) = 0;
	ULMINT		GetCurPrintPos() = 0;
	ULMETHOD	SetCurPrintPos(int nPos) = 0;
	ULMINT		IncCurPrintPos(int nPosAdd) = 0;

	ULMINT		GetCurGapPos() = 0;
	ULMINT		GetPointCountPerDepth() = 0;

	// 图像曲线
	ULMDBL		StartPos() = 0;
	ULMDBL		EndPos() = 0;
	ULMINT		ColorRank() = 0;
	ULMDWD		LightColor()= 0;
	ULMDWD		DarkColor() = 0;

	//  数据赋值
	ULMETHOD	SetValue(int nIndex, float fValue) = 0;
	ULMETHOD	SetValue(int nIndex, double dValue) = 0;
	ULMETHOD	SetValue(int nIndex, WORD wValue) = 0;
	
	/* DepthUnit : [1]m [2]inch */
	ULMFLT		DepthOffset(UINT nDepthUnit) = 0;
	ULMLNG		DepthOffset() = 0;
	ULMLNG		ToolOffset() = 0;	
	ULMINT		AllToolLength() = 0;

	ULMFLT		FeedPoint() = 0;
	ULMINT		FeedPointOfDepth(long lDepth) = 0;

	ULMETHOD	DeleteData(int nStart, int nCount = 1) = 0;
	ULMETHOD	SetValue(int nIndex, double* pValue) = 0;

	ULMETHOD	AddData(long lDepth, char cValue, long lTime = 0) = 0;
	ULMETHOD	AddData(long lDepth, short iValue, long lTime = 0) = 0;
	ULMETHOD	AddData(long lDepth, long lValue, long lTime = 0) = 0; 
	ULMETHOD	AddData(long lDepth, BYTE bValue, long lTime = 0) = 0; 
	ULMETHOD	AddData(long lDepth, DWORD dwValue, long lTime = 0) = 0;
	ULMETHOD	AddStatusData(long lDepth, char cValue, long lTime = 0) = 0;
	ULMETHOD	AddStatusData(long lDepth, short iValue, long lTime = 0) = 0;
	ULMETHOD	AddStatusData(long lDepth, long lValue, long lTime = 0) = 0; 
	ULMETHOD	AddStatusData(long lDepth, BYTE bValue, long lTime = 0) = 0; 
	ULMETHOD	AddStatusData(long lDepth, DWORD dwValue, long lTime = 0) = 0;
	ULMETHOD	SetValue(int nIndex, char cValue) = 0;
	ULMETHOD	SetValue(int nIndex, short iValue) = 0;
	ULMETHOD	SetValue(int nIndex, long lValue) = 0;
	ULMETHOD	SetValue(int nIndex, BYTE bValue) = 0;
	ULMETHOD	SetValue(int nIndex, DWORD dwValue) = 0;
	ULMETHOD	SetDepthOffset(long lDepthOffset) = 0;
	ULMETHOD	SetDepthOffset(float fDepthOffset, UINT nDepthUnit) = 0;
	ULMINT		LeftDotNum() = 0;
	ULMINT		RightDotNum() = 0;
	ULMUNT		TimeInter() = 0;
	ULMETHOD	TimeInter(UINT iTimeInter) = 0;
	ULMETHOD	GetKb(double& dK, double& db) = 0;
	ULMBOOL		IsDepthCurve() = 0;
	ULMLNG		CalcOffset() = 0;
	ULMLNG		TimeDelay() = 0;
	ULMINT		GetLeftVal(LPTSTR lpszValue) = 0;
	ULMINT		GetRightVal(LPTSTR lpszValue) = 0;
	ULMINT		GetStartTime(LPTSTR lpszTime) = 0;
	ULMINT		GetEndTime(LPTSTR lpszTime) = 0;
	ULMINT		LinePWidth() = 0;
	ULMINT		AdjCurDisplayPos(int nCount, int nAdjust = 21) = 0;

	ULMETHOD    GetDepthRange(long* pStart, long* pEnd, int nStart = 0) = 0;
	ULMINT		GetDivision() = 0;
	ULMINT		GetPointDivision() = 0;
	
	ULMINT		GetIndexByDepth(long lDepth, int nStart, int nEnd) = 0;
	ULMPTR		GetData() = 0;
	ULMINT		CalcTitle(DWORD dwStyle = CS_VISIBLE) = 0;
	ULMINT      GetTitle(CStringArray* pTitles) = 0;

	ULMINT		GetPat() = 0;
	ULMETHOD	SetPat(unsigned int nPat) = 0;
	ULMINT		GetStretch() = 0;
	ULMETHOD	SetStretch(unsigned int nStretch) = 0;
	ULMINT		GetMaxRollCount() = 0;
	ULMETHOD	SetMaxRollCount(int nCount) = 0;

	ULMPTR		GetFillProp() = 0;
	ULMETHOD	SetFillProp(void* pFillProp) = 0;

	ULMINT		GetRun() = 0;
	ULMETHOD	SetRun(int nRun) = 0;
	ULMINT      DepthInterval() = 0;
	ULMPTR      GetColorTable() = 0;
	ULMETHOD	TimeDelay(long lTimeDelay) = 0;
	ULMETHOD    ResetData() = 0;
	ULMETHOD    ReplaceData(ICurve* pSource, long lDepth0, long lDepth1) = 0;
	ULMETHOD	SetArrayCount(short nArrayCount) = 0; // Array Curve
	ULMETHOD	GetArrayCount() = 0;

	ULMETHOD    SetPlus(double fPlus) = 0;
	ULMETHOD    SetMulti(double fMulti) = 0;
	ULMDBL      Plus() = 0;
	ULMDBL      Multi() = 0;

	ULMINT      GetPipShow() = 0;
	ULMINT      GetPipType() = 0;
	ULMFLT      GetMajorPip() = 0;
	ULMFLT      GetMinorPip() = 0;

	ULMBOOL		IsLogarithmic() = 0;
	ULMBOOL		IsInpre() = 0;

	ULMINT		GetPointArray() = 0;
	ULMINT		GetIndex12() = 0;

	// Set/Get ReceiverSender, BYTE pBuf[24]
	ULMBOOL		SetReceiver(BYTE* pBuf, float* pRRDistance) = 0;
	ULMBOOL		GetReceiver(BYTE* pBuf, float* pRRDistance) = 0; 
	ULMBOOL		SetSender(BYTE* pBuf, float* pSRDistance) = 0;
	ULMBOOL		GetSender(BYTE* pBuf, float* pSRDistance) = 0;

	ULMBOOL		IsRewind() = 0;
	ULMINT		Direction() = 0;
	ULMINT		LeftMargin(BOOL bPrint) = 0;
	ULMINT		RightMargin(BOOL bPrint) = 0;
	
	ULMETHOD    SetFilterTemplate(int nTemplate) = 0;
	ULMINT      GetFilterTemplate() = 0;

	ULMINT		GetValMode() = 0;
	ULMINT		SetValMode(int nValMode) = 0;
	ULMDBL		GetValueEx(int nIndex) = 0;

	ULMINT      GetWavePlotStyle() = 0;
	ULMINT      GetWaveDistribution() = 0;
	
	// add by bao 
	ULMETHOD	ModifyCurveProperty(LPCTSTR lpszUnit = NULL,		       // 单位
								  double fLeftValue = 0,		       // 最小值
								  double fRightValue = 0,		       // 最大值
								  long  lDepthOffset = 0,		       // 深度偏移
								  int nOffMode = 0,	           // 深度偏移模式
								  int nFilterPiont = 0,	       // 滤波点数
								  int nFilterType = 2,	       // 滤波类型
								  int nDimension = 1,		       // 维数
								  int nPointFrame = 1,	       // 每帧点数
								  int nCurveMode = CURVE_NORMAL, // 曲线模式，用于射孔取芯
								  int nValueType = VT_R4,	       // 曲线数据类型
								  LPCTSTR lpszIDName = NULL	   // 曲线别名
								  ) = 0; // 修改存在曲线的维数信息
	// end
	ULMDBL		IsPrintCurveHead() = 0;
	ULMETHOD    MInit() = 0;
	ULMLNG		TimeDistance() = 0;	// 时间间隔
	ULMBOOL		IsTimeCurve() = 0;//add by xwh  新增加2个接口
	ULMETHOD    GetTimeRange(long* pStart, long* pEnd, int nStart = 0) = 0;
	
	//add by Jasmine 20181204 AddDataMode Override
//	ULMINT		GetDataModeEx(int nIndex, WORD wType) = 0;
//  ULMETHOD	SetDataModeEx(int nIndex, DWORD bMode, WORD wType) = 0;

};

// {E5A9DC70-66B9-46e3-8785-15E1A3D03EF4}
static const IID IID_ICurve = 
{ 0xe5a9dc70, 0x66b9, 0x46e3, { 0x87, 0x85, 0x15, 0xe1, 0xa3, 0xd0, 0x3e, 0xf4 } };


#endif

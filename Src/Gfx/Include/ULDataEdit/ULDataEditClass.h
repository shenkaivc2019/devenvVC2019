#pragma once

#ifndef _ULDATAEDITCLASS_H
#define _ULDATAEDITCLASS_H

#include "DataEditDefine.h"

#ifndef DATAEDIT_API
	#ifdef DATAEDIT_EXPORT
		#define DATAEDIT_API __declspec( dllexport )
	#else
		#define DATAEDIT_API __declspec( dllimport )
	#endif
#endif

class DATAEDIT_API CULDataEdit : public CObject
{
public:
	CULDataEdit(BOOL bDisableUndo = FALSE);
	virtual ~CULDataEdit();

protected:
	DATA_EDIT_OPERATE		m_opEditCode; 		// 当前编辑操作操作码
	DATA_EDIT_OPERATE*      m_pUserEditCode;    // 用户自定义编辑码
	OPERATE_LIST			m_opList; 			// 操作码记录数组
	int 					m_nCurPos; 			// 当前操作码在数组中的位置
	int						m_nDirection;		// 方向
	BOOL					m_bOpenFileFlag;
	BOOL					m_bDisableUndo;
protected:
	int AddToArray(DATA_EDIT_OPERATE  opEditCode);  // 将当前操作码加入数组
	DATA_EDIT_OPERATE  GetOpCode(int nPos) ; 		// 得到指定位置操作码		
	DATA_EDIT_OPERATE  GetOppositeOpration(DATA_EDIT_OPERATE  opEditCode); // 得到操作码的逆操作
	int	  GetRangeIndex(const DATA_EDIT_OPERATE& opEditCode, int& nStartPos, int& nEndPos);// 得到深度范围的索引
	void  DataEdit_MoveHorz(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO); // 曲线水平移动操作
	void  DataEdit_MoveVert(DATA_EDIT_OPERATE &opEditCode ,int nDoMode = DATAMANAGE_DO);	// 曲线垂直移动操作
	void  DataEdit_ExpendHorz(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO); // 曲线水平缩放操作
	void  DataEdit_ExpendVert(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO);// 曲线垂直缩放操作
	void  DataEdit_SpringDepth(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO);// 曲线弹性校深操作
	void  DataEdit_BaselineRevise(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO);// 曲线基线校正操作
	void  DataEdit_Filter(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO);	// 曲线滤波操作
	void  DataEdit_Delete(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO);	// 曲线删除操作
	void  DataEdit_Recover(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO);  // 曲线恢复操作
	
	void  DataEdit_LinerTrans(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO); // 二维曲线灰度线性变换
	void  DataEdit_ThresholdTrans(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO); // 二维曲线灰度阈(yu)值变换
	void  DataEdit_WindowTrans(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO); // 二维曲线灰度窗口变换
	void  DataEdit_GrayStretch(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO); // 二维曲线灰度拉伸
	void  DataEdit_InteEqualize(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO); // 二维曲线灰度均衡
	void  DataEdit_EnhaGradsharp(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO); // 二维曲线梯度锐化
	void  DataEdit_EnhaImage(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO); // 二维曲线梯度锐化
	
	void  DataEdit_Cut(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO);		// 曲线剪切操作
	void  DataEdit_Paste(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO);	// 曲线粘贴操作
	void  DataEdit_Copy(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO);	// 曲线复制操作
	void  DataEdit_Uniform(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO);	// 曲线切齐操作
	void  DataEdit_Image(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO);	// 曲线镜象操作
	void  DataEdit_Turn(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO);	// 曲线反转操作
	void  DataEdit_Reverse(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO); // 曲线反向操作
	void  DataEdit_Interpolation(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO);  // 曲线插补操作
	void  DataEdit_MoveHorzVert(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO);   // 两个方向同时移动
	void  DataEdit_OnePointCorrect(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO);// 单点修正
	void  DataEdit_Combine(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO);	// 曲线合并操作
	void  DataEdit_Replace(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO);	// 曲线拼接操作
	void  DataEdit_IMAGE_MANAGE(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO);	// 
	void  DataEdit_DEPTH_ADJUSTMENT(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO);	// 曲线深度平差
	void  DataEdit_Undo();	// 撤销
	void  DataEdit_Redo();	// 恢复

	///////////////

	void  DataEdit_UndoExpendVert(DATA_EDIT_OPERATE& opEditCode);   //垂直缩放UNDO操作	
	void  DataEdit_UndoDelete(DATA_EDIT_OPERATE& opEditCode);		//删除UNDO操作	
	void  DataEdit_UndoFilter(DATA_EDIT_OPERATE& opEditCode);		//滤波UNDO操作
	void  DataEdit_UndoInterpolation(DATA_EDIT_OPERATE& opEditCode);//插补UNDO操作
	void  DataEdit_UndoUniform(DATA_EDIT_OPERATE& opEditCode);		//切齐UNDO操作
	void  DataEdit_UndoPaste(DATA_EDIT_OPERATE& opEditCode);		//粘贴UNDO操作
	void  DataEdit_UndoCombine(DATA_EDIT_OPERATE& opEditCode);		//拼接UNDO操作
	void  DataEdit_UndoReplace(DATA_EDIT_OPERATE& opEditCode);

	///////////////
	//临时文件操作
	CString m_strFileName;
	CFile   m_DataFile;
	BOOL	PrepareTempFile();											//建立临时文件
	CString	GenerateTempFileName(CString strName);						//生成临时文件名
	DWORD   GetCurFilePointer();										//得到当前文件指针位置
	void	SaveCurveValue(CCurveData* pCurve, int nIndex);	//保存当前值
	void	ReadCurveValue(CCurveData* pCurve, int nIndex);	//读取曲线值
	void	Undo_ResetRawValue(DATA_EDIT_OPERATE &opEditCode);			//从临时文件中读回数据
	void    Redo_ResetRawValue(const DATA_EDIT_OPERATE& opEditCode);     //从临时文件中读回数据
	void	StoreDataForUndo(int nStartPos, int nEndPos, CCurveData* pCurve, DATA_EDIT_OPERATE &opEditCode); //将数据放入临时文件
    void    StoreDataForRedo(DATA_EDIT_OPERATE& opEditCode, int nStartPos, int nEndPos); //将数据放入临时文件

	/////////////////////////////////
	//时间驱动粘贴
	void  DataEdit_Paste_Time(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO);	// 时间驱动曲线粘贴操作
	
	// add by bao 多条曲线同时编辑时的标记存储，方便撤销时一次性撤销完成
	void  MultiOperate(DATA_EDIT_OPERATE& opEditCode);

public:
	BOOL  CanUndo();	//判断是当前是否可以执行UNDO操作
	BOOL  CanRedo();	//判断是当前是否可以执行REDO操作

	void  DataEditManager(DATA_EDIT_OPERATE& opEditCode, int nDoMode = DATAMANAGE_DO); //根据操作码执行对应操作
	void  DataUnDoEditManager(DATA_EDIT_OPERATE& opEditCode);
	void  DataReDoEditManager(DATA_EDIT_OPERATE& opEditCode);
	void  UserOpBegin(DATA_EDIT_OPERATE& opEditCode); //用户自己操作数据前调用，用于恢复功能
	void  UserOpEnd(); //用户操作完数据后调用	
	void  RemoveOpCode(CCurve* pCurve);
	void  RemoveOpCode(CCurveData* pCurveData);
};

DATAEDIT_API CULDataEdit* AfxGetULDataEdit();

#endif
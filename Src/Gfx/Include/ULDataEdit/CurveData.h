// CurveData.h: interface for the CCurveData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CURVEDATA_H__A51AF60A_89E6_4320_BFEB_6F8E952212F8__INCLUDED_)
#define AFX_CURVEDATA_H__A51AF60A_89E6_4320_BFEB_6F8E952212F8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxmt.h>
#include <vector>
#include "ULCOMMDEF.h"

#include "Utility.h"

#define _RUNIT
#ifdef _RUNIT
class CUnits;
#endif

#ifndef DATAEDIT_API
	#ifdef DATAEDIT_EXPORT
		#define DATAEDIT_API __declspec( dllexport )
	#else
		#define DATAEDIT_API __declspec( dllimport )
	#endif
#endif

typedef struct _tagMBlock
{
	DWORD	posA;
	LPBYTE	pAddr;
	DWORD	cbSize;
	DWORD	dwOff;
} MBLOCK;

typedef CList<MBLOCK*, MBLOCK*>	CMPool;

//extern CString GetTempFileName();

//////////////////////////////////////////////////////////////////////////////
//
// Forward class declarations
//
//////////////////////////////////////////////////////////////////////////////

class CCurve;
typedef	std::vector<CCurve*>	vector_curve;

class CULFilter;
class CXMLNode;
struct IPerforation;

//////////////////////////////////////////////////////////////////////////////
//
// supported by class CCurveData
//
//////////////////////////////////////////////////////////////////////////////

/*
* VARTYPE usage key,
*
* * [V] - may appear in a VARIANT
* * [T] - may appear in a TYPEDESC
* * [C] - supported by class _variant_t
*
*
*  VT_EMPTY			[V]        nothing
*  VT_ERROR			[V][T][C]  SCODE

*  VT_I1	  		   [T]     signed char			char
*  VT_I2			[V][T][C]  2 byte signed int	short	
*  VT_I4			[V][T][C]  4 byte signed int	long
*  VT_I8               [T]     signed 64-bit int
*  VT_R4            [V][T][C]  4 byte real			float
*  VT_R8            [V][T][C]  8 byte real			double

*  VT_UI1			[V][T][C]  unsigned char		BYTE
*  VT_UI2              [T]     unsigned short		WORD
*  VT_UI4              [T]     unsigned short		DWORD
*  VT_UI8              [T]     unsigned 64-bit int
*
*/

#define		szcvt		12
#define		szadd		4096
#define		cv_t		CCurveValue<BYTE>

#define		cv_chr		CCurveValue<char>
#define		cv_sht		CCurveValue<short>
#define		cv_lng		CCurveValue<long>

#define		cv_flt		CCurveValue<float>
#define		cv_dbl		CCurveValue<double>

#define		cv_byt		CCurveValue<BYTE>
#define		cv_wrd		CCurveValue<WORD>
#define		cv_dwd		CCurveValue<DWORD>

#define 	cv_i(vt, v, i, x)	((vt*)(&v->m_Value))[i] = (vt)(x)
#define		cv_o(vt, v, i, x)	x = ((vt*)(&v->m_Value))[i]
#define		cv_io(dvt, dv, svt, sv, i) ((dvt*)(&dv->m_Value))[i] = (dvt)(((svt*)(&sv->m_Value))[i])

inline	size_t	sizeofvt(VARTYPE vt)
{
	switch(vt)
	{
	case VT_I1:
	case VT_UI1:
		return 1;
	case VT_I2:
	case VT_UI2:
		return 2;
	case VT_I4:
	case VT_UI4:
	case VT_R4:
		return 4;
	case VT_R8:
		return 8;
	case VT_NULL:
	default:
		return 0;	
	}
}

#define	DS_DTOOL		1 
#define	DS_DCURVE		2
#define DS_DTC			(DS_DTOOL|DS_DCURVE)

#define IsNegative(x,y)		(((x) >> 31) ^ ((y) >> 31))
#define IsBetween(x,y,i)	IsNegative((x)-i,(y)-i)		

//////////////////////////////////////////////////////////////////////////////
//
// class CCurveData declarations
//
//////////////////////////////////////////////////////////////////////////////
class DATAEDIT_API CPtrArrayEx
{
	int m_nSize; 
	void PrepareGroup(int nLen) // 准备内存，nLen是要添加的元素数
	{
		if (nLen + m_nSize > m_nGroupSize * m_arrPointer.GetSize())
		{
			int n = (int)ceil((double)(nLen + m_nSize) / m_nGroupSize) - m_arrPointer.GetSize(); // 添加元素后需要增加的组数
			for (int i = 0; i < n; i++)
			{
				BYTE* pBuf = new BYTE[4 * m_nGroupSize];
				ZeroMemory(pBuf, sizeof(4 * m_nGroupSize));
				m_arrPointer.Add(pBuf);
			}
		}
	}

	void MCopy(int nDestPos, int nSrcPos, int nLen)   // 一组内部分数据的拷贝，nLen不大于nSrcPos所在分组的数据
	{
		if (nLen <= 0)
			return;
		int nStartDestGroupIndex = nDestPos / m_nGroupSize;   // 目标位置的起始处 所在的组索引
		int nStartDestElemenetPos = nDestPos % m_nGroupSize; // 目标位置的起始处的元素索引
	    int nStartSrcGroupIndex = nSrcPos / m_nGroupSize;  // 源位置起始处 所在的组索引
		int nStartSrcElementPos = nSrcPos % m_nGroupSize; // 源位置起始处 所在的组索引
		ASSERT(nLen <= m_nGroupSize - nStartSrcElementPos);
		int nStopDestGroupIndex = (nDestPos + nLen) / m_nGroupSize;
		if (nStartDestGroupIndex == nStopDestGroupIndex) // 数据拷贝到目标位置后在一个组内
		{
			memcpy((int*)m_arrPointer.GetAt(nStartDestGroupIndex) + nStartDestElemenetPos, 
				(int*)m_arrPointer.GetAt(nStartSrcGroupIndex) + nStartSrcElementPos, nLen * 4);
		}
		else if (nStartDestGroupIndex < nStartSrcGroupIndex) // 数据拷贝从源位置往前拷, 要先拷贝前半分组,免得数据被覆盖
		{
			memcpy((int*)m_arrPointer.GetAt(nStartDestGroupIndex) + nStartDestElemenetPos, 
				(int*)m_arrPointer.GetAt(nStartSrcGroupIndex) + nStartSrcElementPos, (m_nGroupSize - nStartDestElemenetPos) * 4);
			if (nLen - m_nGroupSize + nStartDestElemenetPos > 0)
			{
				memcpy((int*)m_arrPointer.GetAt(nStopDestGroupIndex), 
					(int*)m_arrPointer.GetAt(nStartSrcGroupIndex) + nStartSrcElementPos + m_nGroupSize - nStartDestElemenetPos, 
					(nLen - m_nGroupSize + nStartDestElemenetPos) * 4);
			}
		}
		else // 数据拷贝从源位置往后拷, 要先拷贝后半分组,免得数据被覆盖
		{
			if (nLen - m_nGroupSize + nStartDestElemenetPos > 0)
			{
				memcpy((int*)m_arrPointer.GetAt(nStopDestGroupIndex), 
					(int*)m_arrPointer.GetAt(nStartSrcGroupIndex) + nStartSrcElementPos + m_nGroupSize - nStartDestElemenetPos, 
					(nLen - m_nGroupSize + nStartDestElemenetPos) * 4);
			}
			memcpy((int*)m_arrPointer.GetAt(nStartDestGroupIndex) + nStartDestElemenetPos, 
				(int*)m_arrPointer.GetAt(nStartSrcGroupIndex) + nStartSrcElementPos, (m_nGroupSize - nStartDestElemenetPos) * 4);
		}
	}

public:	
	int m_nGroupSize;  // 每组元素个数
	CPtrArray m_arrPointer; // 缓冲区指针
    CPtrArrayEx()
	{
		m_nSize = 0;          // 元素个数
		m_nGroupSize = 1024;  // 每组1024个元素，4096个字节
	}
	~CPtrArrayEx()
	{
		RemoveAll();
	}
	int GetSize() { return m_nSize; }
	int GetUpperBound() { return m_nSize - 1;}

	void RemoveAll() 
	{
		m_nSize = 0; 
		for (int i = m_arrPointer.GetSize() - 1;  i >= 0; i--)
		{
			delete m_arrPointer.GetAt(i);
		}
		m_arrPointer.RemoveAll();
	}
	void Add(void* newElement)
	{ 
		int nGroupIndex = m_nSize / m_nGroupSize;
		int nElementPos = m_nSize % m_nGroupSize;
		if (nGroupIndex >= m_arrPointer.GetSize())
		{
			PrepareGroup(1);
		}
		memcpy((int*)m_arrPointer.GetAt(nGroupIndex) + nElementPos, &newElement, 4);
		m_nSize++;
	}
	void SetSize(int nSize, int nGroupSize = 1024) 
	{
		PrepareGroup(nSize);
		m_nSize = nSize;
	}
	void InsertAt(int nStartIndex, CPtrArray* pNewArray)
	{
		ASSERT(pNewArray != NULL);
		ASSERT(nStartIndex >= 0);
		
		int nLen = pNewArray->GetSize();
		if (nStartIndex > m_nSize)
			PrepareGroup(nLen + nStartIndex + 1 - m_nSize);
		else
		    PrepareGroup(nLen);
		int nEndGroupIndex = (m_nSize - 1) / m_nGroupSize;  // 现有数据最后一组的组序号
		int nEndGroupElementSize = m_nSize - nEndGroupIndex * m_nGroupSize;  // 现有数据最后一组的元素个数
		int nCopyGroupIndex = nStartIndex / m_nGroupSize; // 插入位置的组序号
		int nCopyElementPos = nStartIndex % m_nGroupSize; // 插入位置所在组中的元素索引
		if (nEndGroupIndex != nCopyGroupIndex) // 如果插入位置和现有最后一组不是同一组，则拷贝现有最后一组中的数据到指定位置
		{
			MCopy(nEndGroupIndex * m_nGroupSize + nLen, nEndGroupIndex * m_nGroupSize, nEndGroupElementSize);
		}
		int i;
		for (i = nEndGroupIndex - 1; i > nCopyGroupIndex; i--) // 拷贝中间几组数据
		{
			MCopy(i * m_nGroupSize + nLen, i * m_nGroupSize, m_nGroupSize);
		}
		int nCopySize = min(m_nGroupSize - (nStartIndex % m_nGroupSize), m_nSize - nStartIndex);
		MCopy(nStartIndex + nLen, nStartIndex, nCopySize);
		
		int  n = min(m_nGroupSize - nCopyElementPos, nLen);
		memcpy((int*)m_arrPointer.GetAt(nCopyGroupIndex) + nCopyElementPos, pNewArray->GetData(), n * 4);
		int nOffset = n;
		for (i = 0; i < (nLen - n) / m_nGroupSize; i++)
		{
			memcpy(m_arrPointer.GetAt(nCopyGroupIndex + 1 + i), pNewArray->GetData() + nOffset, m_nGroupSize * 4);
			nOffset += m_nGroupSize;
		}
		if (nLen - nOffset > 0)
		{
			memcpy(m_arrPointer.GetAt(nCopyGroupIndex + 1 + i), pNewArray->GetData() + nOffset, (nLen - nOffset) * 4);
		}
		if (nStartIndex > m_nSize)
			m_nSize = nStartIndex + 1 + nLen;
		else
		    m_nSize += pNewArray->GetSize();
	}
	
	void InsertAt(int nIndex, void* newElement, int nCount = 1)
	{
		ASSERT(nIndex >= 0);    // will expand to meet need
		ASSERT(nCount > 0);     // zero or negative size not allowed

		if (nIndex > m_nSize)
			PrepareGroup(nIndex + 1 - m_nSize);
		else
		    PrepareGroup(1);
		int nEndGroupIndex = m_nSize / m_nGroupSize;
		int nEndElementPos = m_nSize % m_nGroupSize;
		int nCopyGroupIndex = nIndex / m_nGroupSize;
		int nCopyElementPos = nIndex % m_nGroupSize;
		if (nEndGroupIndex != nCopyGroupIndex)
		{
			MCopy(nEndGroupIndex * m_nGroupSize + 1, nEndGroupIndex * m_nGroupSize, nEndElementPos);
		}
		for (int i = nEndGroupIndex - 1; i > nCopyGroupIndex; i--)
		{
			MCopy(i * m_nGroupSize + 1, i * m_nGroupSize, m_nGroupSize);
		}
		int nCopySize = min(m_nGroupSize - (nIndex % m_nGroupSize), m_nSize - nIndex);
		MCopy(nIndex + 1, nIndex, nCopySize);
		INT_PTR p = (INT_PTR)newElement;
		memcpy((int*)m_arrPointer.GetAt(nCopyGroupIndex) + nCopyElementPos, &p, 4);
		if (nIndex > m_nSize)
			m_nSize = nIndex + 1;
		else
			m_nSize++;
	}
	
	void Copy(CPtrArrayEx& ls) 
	{
		RemoveAll();
		for (int i = 0; i < ls.m_arrPointer.GetSize(); i++)
		{
			BYTE* pBuf = new BYTE[m_nGroupSize * 4];
			memcpy(pBuf, ls.m_arrPointer.GetAt(i), m_nGroupSize * 4);
			m_arrPointer.Add(pBuf);
		}
		m_nSize = ls.GetSize();
	}

	void Copy(const CPtrArray& src)
	{
		RemoveAll();
		int nDataSize = src.GetSize();
		int nOffset = 0;
		while (nDataSize > 0)
		{
			BYTE* pBuf = new BYTE[m_nGroupSize * 4];
			if (nDataSize <= m_nGroupSize)
				memcpy(pBuf, src.GetData() + nOffset, nDataSize * 4);
			else
				memcpy(pBuf, src.GetData() + nOffset, m_nGroupSize * 4);
			m_arrPointer.Add(pBuf);
			nDataSize -= m_nGroupSize;
			nOffset += m_nGroupSize;
		}
		m_nSize = src.GetSize();
	}
	
	void RemoveAt(int nIndex, int nCount = 1)
	{
		ASSERT(nIndex >= 0);
		ASSERT(nCount >= 0);
		ASSERT(nIndex + nCount <= m_nSize);
		int nLen = 0; // 删除的长度
		int nGroupStartIndex = nIndex / m_nGroupSize; // 删除开始
		int nElementStartPos = nIndex % m_nGroupSize;
		if (nGroupStartIndex >= 0 && nGroupStartIndex < m_arrPointer.GetSize())
		{
			// 删除的长度不能超出缓冲区的边界
			//nLen = min(nLength, m_nGroupSize - nElementStartPos + m_nGroupSize * (m_arrPointer.GetSize() - nGroupStartIndex - 1));
			nLen = min(nCount, m_nSize - nIndex);
		}
		
		int nGroupStopIndex = (nIndex + nLen) / m_nGroupSize; // 删除结尾
		int nElementStopPos = (nIndex + nLen) % m_nGroupSize;
		int nZeroLen = min(m_nGroupSize - nElementStartPos, nLen);
		if(nZeroLen > 0)
			ZeroMemory((int*)m_arrPointer.GetAt(nGroupStartIndex) + nElementStartPos, nZeroLen * 4);
		int i;
		for (i = nGroupStartIndex + 1; i < nGroupStopIndex; i++)
			ZeroMemory(m_arrPointer.GetAt(i), m_nGroupSize * 4);
		if (nGroupStopIndex != nGroupStartIndex&&nGroupStopIndex<m_arrPointer.GetSize())//XWH 出现过越界崩溃
			ZeroMemory(m_arrPointer.GetAt(nGroupStopIndex), nElementStopPos * 4);
		
		int nDestPos = nIndex;
		int nStartSrcPos = nIndex + nLen;  
		int nStartSrcGroupIndex = nStartSrcPos / m_nGroupSize; // 拷贝起始
		int nStartSrcElementPos = nStartSrcPos % m_nGroupSize;
		
		int nCopySize = min(m_nGroupSize - nStartSrcElementPos, m_nSize - nLen - nIndex);
		MCopy(nDestPos, nStartSrcPos, nCopySize);
		nDestPos += nCopySize;	
		int nStopCopyGroupIndex = m_nSize / m_nGroupSize;
		int nStopCopyElementPos = m_nSize % m_nGroupSize;
		for (i = nStartSrcGroupIndex + 1; i < nStopCopyGroupIndex; i++)
		{
			MCopy(nDestPos, i * m_nGroupSize, m_nGroupSize);
			nDestPos += m_nGroupSize;
		}    
		if (nStopCopyGroupIndex  != nStartSrcGroupIndex)
		{
			MCopy(nDestPos, nStopCopyGroupIndex * m_nGroupSize, nStopCopyElementPos);
		}
		
		m_nSize -= nLen;
	}
	
	void* GetAt(int nPos)
	{
		int nGroupIndex = nPos / m_nGroupSize;
		int nElementPos = nPos % m_nGroupSize;
		INT_PTR p;
		memcpy(&p, (int*)m_arrPointer.GetAt(nGroupIndex) + nElementPos, 4);
		return (void*)p;
	}
	
	void*& operator[](int nPos)
	{
		int nGroupIndex = nPos / m_nGroupSize;
		int nElementPos = nPos % m_nGroupSize;
		return (void*&)*((int*)m_arrPointer.GetAt(nGroupIndex) + nElementPos);
	}
};

class DATAEDIT_API CCurveData : public CObject
{
public:
	CCurveData(VARTYPE nValueType = VT_R4, CCurve* pCurve = NULL);
	CCurveData(CCurveData* pData);
	CCurveData(CCurveData* pData, BOOL bPerforation);
	virtual	~CCurveData();

protected:
	template <class vType>
	class CCurveValue			// 曲线值模版类
	{
	public:
		CCurveValue();
		CCurveValue(unsigned int cElements);
		CCurveValue(CCurveValue<vType>& src, unsigned int cElements = 1);
		CCurveValue(long lDepth, long lTime, vType value);
		CCurveValue(long lDepth, long lTime, vType* value, unsigned int cElements = 1);
		~CCurveValue();

		void	assign(CCurveValue<vType>& src, unsigned int cElements = 1);
		void	assign(long lDepth, long lTime, vType value);
		void	assign(long lDepth, long lTime, vType* value, unsigned int cElements);
		void	swap(CCurveValue<vType>& src, unsigned int cElements = 1);
	public:
		DWORD	m_dwMode;		// 数据模式
		long	m_lDepth;		// 深度
		long	m_lTime;		// 时间
		vType	m_Value;		// 曲线值
	};

public:
	static long GetDepthEDMin(std::vector<CCurveData*>& vecData, long lDepth);
	static long GetDepthEDMax(std::vector<CCurveData*>& vecData, long lDepth);

public:

inline void* FindData(int nIndex)
{
	// cv_t* pcv = NULL;
	if (m_pPFile != NULL)
	{
		try
		{	
			nIndex = (DWORD)m_arrData.GetAt(nIndex);
			POSITION pos = m_mpData.FindIndex(nIndex/m_nGrow);
			if (pos != NULL)
			{
				MBLOCK* pB = m_mpData.GetAt(pos);
				if (pB->pAddr == NULL)
				{
					// TRACE(_T("Read %d\n", pB->cbSize);
					pB->pAddr = new BYTE[pB->cbSize];
					if (pB->posA > INT_MAX)
					{
					}
					m_pPFile->Seek(pB->posA, CFile::begin);
					m_pPFile->Read(pB->pAddr, pB->cbSize);
				}

				return (pB->pAddr + m_cbData*(nIndex%m_nGrow));
			}
		}
		catch (CFileException* e) 
		{
			e->ReportError();
			e->Delete();
			return NULL;
		}
		catch (...)
		{
			return NULL;
		}

		return NULL;
	}

	if (nIndex >= m_arrData.GetSize())
	{
		return NULL;
	}
    return m_arrData.GetAt(nIndex);
// 	if (AfxIsValidAddress(pcv, sizeof(pcv)))
// 	{
// 		return pcv;
// 	}
// 	return NULL;
}

inline BOOL OverBounds(int index)
{
	if(index < 0)
		return TRUE;
	else if(index < m_arrData.GetSize())
		return FALSE;
	return TRUE;
}

inline int GetSize() 
{ 
	return m_arrData.GetSize(); 
}


inline VARTYPE	GetValueType() 
{	
	return m_nValueType; 
}
	
inline void	SetValueType(VARTYPE nValueType)
{
	switch(nValueType)
	{
	case VT_I1:
	case VT_I2:
	case VT_I4:
	case VT_R4:
	case VT_R8:
	case VT_UI1:
	case VT_UI2:
	case VT_UI4:
		break;
	default:
		nValueType = VT_R4;
	}
	
	if (m_nValueType == nValueType)
		return ;

	int nSize = m_arrData.GetSize();
	CCurveData data;
	data.Copy(this);
	Clear();
	m_nValueType = nValueType;
	m_nSizePoint = sizeofvt(nValueType);
	MInit();
	for (int i = 0; i < nSize; i++)
	{
		m_arrData.Add(MNew(data.GetDepth(i), data.GetTime(i), data.GetdblValue(i)));
	}
}

inline DWORD GetDataMode(int nIndex)
{

// #ifdef _BOUNDSCHECK
	if (OverBounds(nIndex))
		return DATA_INVALID;
// #endif
	try
	{
		cv_t* pcv = (cv_t*)FindData(nIndex);
		if(NULL == pcv)
			return DATA_INVALID;
		if((pcv->m_dwMode == DATA_OFFSET) && m_bOffValue)
			return DATA_VALID;
		return pcv->m_dwMode;
	}
	catch (CException* e)
	{
		e->ReportError();
		e->Delete();
	}
	return DATA_INVALID;
}

inline short SetDataMode(int nIndex, DWORD bMode)
{

#ifdef _BOUNDSCHECK
	if (OverBounds(nIndex))
		return UL_ERROR;
#endif

	try
	{
		cv_t* pcv = (cv_t*)FindData(nIndex);
		if(NULL == pcv)
			return UL_ERROR;	
		pcv->m_dwMode = bMode;
		return UL_NO_ERROR;
	}
	catch (CException* e)
	{
		e->ReportError();
		e->Delete();
	}
	return UL_ERROR;
}

//add by Jasmine 20181204
//begin
/*
inline DWORD GetDataBaseMode(int nIndex)
{
	
	// #ifdef _BOUNDSCHECK
	if (OverBounds(nIndex))
		return DATA_INVALID;
	// #endif
	try
	{
		cv_t* pcv = (cv_t*)FindData(nIndex);
		if(NULL == pcv)
			return DATA_INVALID;
		if((pcv->m_dwMode == DATA_OFFSET) && m_bOffValue)
			return DATA_VALID;
		return (pcv->m_dwMode)&0x0f;
	}
	catch (CException* e)
	{
		e->ReportError();
		e->Delete();
	}
	return DATA_INVALID;
}

inline short SetDataBaseMode(int nIndex, DWORD bMode)
{
	
#ifdef _BOUNDSCHECK
	if (OverBounds(nIndex))
		return UL_ERROR;
#endif
	
	try
	{
		cv_t* pcv = (cv_t*)FindData(nIndex);
		if(NULL == pcv)
			return UL_NO_ERROR;		//why not ul_error
		pcv->m_dwMode = (pcv->m_dwMode&0xfff0)|bMode;
		return UL_NO_ERROR;
	}
	catch (CException* e)
	{
		e->ReportError();
		e->Delete();
	}
	return UL_ERROR;
}

inline DWORD GetDataDrawMode(int nIndex)
{
	try
	{
		cv_t* pcv = (cv_t*)FindData(nIndex);
		if(NULL == pcv)
			return DATA_UNDRAW;
		return DATADRAWMODE(pcv->m_dwMode);
	}
	catch (CException* e)
	{
		e->ReportError();
		e->Delete();
	}
	return DATA_UNDRAW;
}

inline short SetDataDrawMode(int nIndex, DWORD bMode)
{
	try
	{
		cv_t* pcv = (cv_t*)FindData(nIndex);
		if(NULL == pcv)
			return UL_ERROR;	
		
		pcv->m_dwMode = DATADRAWMODERESET(pcv->m_dwMode, bMode);
		return UL_NO_ERROR;
	}
	catch (CException* e)
	{
		e->ReportError();
		e->Delete();
	}
	return UL_ERROR;
}

inline short SetDataDrawMode(DWORD bMode)
{
	try
	{
		for (int i = 0; i < m_arrData.GetSize(); i++)
		{
			if (SetDataDrawMode(i, bMode))
			{
				return UL_ERROR;
			}
		}
	}
	catch (CException* e)
	{
		e->ReportError();
		e->Delete();
	}
	
	return UL_NO_ERROR;
}

inline short SetDataBaseMode(DWORD bMode)
{
	try
	{
		for (int i = 0; i < m_arrData.GetSize(); i++)
		{
			if (SetDataBaseMode(i, bMode))
			{
				return UL_ERROR;
			}
		}
	}
	catch (CException* e)
	{
		e->ReportError();
		e->Delete();
	}
	
	return UL_NO_ERROR;
}
*/
//end 

inline short SetDataMode(DWORD bMode)
{
	for (int i = 0; i < m_arrData.GetSize(); i++)
	{
		cv_t* pcv = (cv_t*)FindData(i);
		if(NULL == pcv)
			return UL_ERROR;
		pcv->m_dwMode = bMode;
	}

	return UL_NO_ERROR;
}

#ifdef _RUNIT

inline BOOL RecUnit()
{
	if (m_strTu.GetLength())
		m_bRecUnit = m_strTu.CompareNoCase(m_strUnit);
	else
		m_bRecUnit = FALSE;

	return m_bRecUnit;
}

#endif // _RUNIT

inline size_t GetDataSize()
{
	return sizeof(BYTE) + 2*sizeof(long) + m_nSizePoint*m_nPointArray;
}

inline size_t GetValueSize() 
{ 
	return m_nSizePoint*m_nPointArray;
}

inline double GetMaxValueCmp(double defValue)
{
	int nSize = m_arrData.GetSize();
	if (nSize < 1)
		return defValue;

	double dValue = defValue;
	
	for (int i=0; i<nSize; i++)
	{
		if (dValue < GetdblValue(i))
			dValue = GetdblValue(i);
	}

	return dValue;
}

inline double GetMinValueCmp(double defValue)
{
	int nSize = m_arrData.GetSize();
	if (nSize < 1)
		return defValue;
	
	double dValue = defValue;
	for (int i=0; i<nSize; i++)
	{
		if (GetdblValue(i) < dValue)
			dValue = GetdblValue(i);
	}
	
	return dValue;
}

/* ----- 深度/时间 ----- */
inline long& Depth(int nIndex)
{
	cv_t* pcv = (cv_t*)FindData(nIndex);
	return pcv->m_lDepth;
}

inline long& Time(int nIndex)
{
	cv_t* pcv = (cv_t*)FindData(nIndex);
	return pcv->m_lTime;
}

inline long GetDepth(UINT nIndex)
{
#ifdef _BOUNDSCHECK
	if (OverBounds(nIndex))
		return INVALID_DEPTH;
#endif

	if (nIndex < m_arrData.GetSize())
	{
		cv_t* pcv = (cv_t*)FindData(nIndex);
		if (AfxIsValidAddress(pcv, sizeof(pcv)))
			return pcv->m_lDepth;
	}

	return INVALID_DEPTH;
}

inline long GetDepthED()
{
	int nCount = m_arrData.GetSize();
	while (nCount > 0)
	{
		cv_t* pcv = (cv_t*)FindData(--nCount);
		if (pcv != NULL)
		    return pcv->m_lDepth;
	}

	return INVALID_DEPTH;
}

inline short SetDepth(int nIndex, long lDepth)
{

#ifdef _BOUNDSCHECK
	if (OverBounds(nIndex))
		return UL_ERROR;
#endif

	cv_t* pcv = (cv_t*)FindData(nIndex);
	pcv->m_lDepth = lDepth;
	return UL_NO_ERROR;
}

inline short SetDepthValue(int nIndex, long lDepth)
{
#ifdef _BOUNDSCHECK
	if (OverBounds(nIndex))
		return UL_ERROR;
#endif
	
	cv_t* pcv = (cv_t*)FindData(nIndex);
	pcv->m_lDepth = lDepth;
	SetValueX(nIndex, 0, DTM(lDepth));
	return UL_NO_ERROR;
}

inline short SetTimeValue(int nIndex, long lTime)
{
#ifdef _BOUNDSCHECK
	if (OverBounds(nIndex))
		return UL_ERROR;
#endif
	
	cv_t* pcv = (cv_t*)FindData(nIndex);
	pcv->m_lTime = lTime;
	SetValueX(nIndex, 0, DTM(lTime));
	return UL_NO_ERROR;
}

inline long GetTime(int nIndex)
{

#ifdef _BOUNDSCHECK
	if(OverBounds(nIndex))
		return INVALID_DEPTH;
#endif
	
	cv_t* pcv = (cv_t*)FindData(nIndex);
	if (AfxIsValidAddress(pcv, sizeof(pcv)))
		return pcv->m_lTime;
	else
		return INVALID_DEPTH;
}

inline long GetTimeED()
{
	int nCount = m_arrData.GetSize();
	while (nCount > 0)
	{
		cv_t* pcv = (cv_t*)FindData(--nCount);
		if (pcv != NULL)
		    return pcv->m_lTime;
	}

	return INVALID_DEPTH;
}

inline short SetTime(int nIndex, long lTime)
{

#ifdef _BOUNDSCHECK
	if(OverBounds(nIndex))
		return UL_ERROR;
#endif

	cv_t* pcv = (cv_t*)FindData(nIndex);
	pcv->m_lTime = lTime;
	return UL_NO_ERROR;
}

inline void FRDepth()
{
	m_lFirstDepth = m_lFRDepth;
}

//////////////////////////////////////////////////////////////////////
//
// Add data of 1 dimension curve data
//
//////////////////////////////////////////////////////////////////////
template<typename T>
inline
short AddX(long lDepth, T value, long lTime = 0)
{
	switch(m_nValueType)
	{
	case VT_I1:
		{
			char cValue = (char)value;
			return Add(lDepth, &cValue, lTime);
		}
	case VT_I2:
		{
			short sValue = (short)value;
			return Add(lDepth, &sValue, lTime);
		}
	case VT_I4:
		{
			long lValue = (long)value;
			return Add(lDepth, &lValue, lTime);
		}
	case VT_R4:
		{
			float fValue = (float)value;
			return Add(lDepth, &fValue, lTime);
		}
	case VT_R8:
		{
			double dValue = (double)value;
			return Add(lDepth, &dValue, lTime);
		}
	case VT_UI1:
		{
			BYTE bValue = (BYTE)value;
			return Add(lDepth, &bValue, lTime);
		}
	case VT_UI2:
		{
			WORD wValue = (WORD)value;
			return Add(lDepth, &wValue, lTime);
		}
	case VT_UI4:
		{
			DWORD dwValue = (DWORD)value;
			return Add(lDepth, &dwValue, lTime);
		}
	default:
		return UL_ERROR;
	}
}

inline short Add(long lDepth, char cValue, long lTime = 0)
{
	return AddX(lDepth, cValue, lTime);
}

inline short Add(long lDepth, short sValue, long lTime = 0)
{
	return AddX(lDepth, sValue, lTime);
}

inline short Add(long lDepth, long lValue, long lTime = 0)
{
	return AddX(lDepth, lValue, lTime);
}

inline short Add(long lDepth, float fValue, long lTime = 0)
{
	return AddX(lDepth, fValue, lTime);
}

inline short Add(long lDepth, double dValue, long lTime = 0)
{
	return AddX(lDepth, dValue, lTime);
}

inline short Add(long lDepth, BYTE bValue, long lTime = 0)
{
	return AddX(lDepth, bValue, lTime);
}

inline short Add(long lDepth, WORD wValue, long lTime = 0)
{
	return AddX(lDepth, wValue, lTime);
}

inline short Add(long lDepth, DWORD dwValue, long lTime = 0)
{
	return AddX(lDepth, dwValue, lTime);
}

//////////////////////////////////////////////////////////////////////
//
// Set data of 1 dimension curve data
//
//////////////////////////////////////////////////////////////////////
template<typename T>
inline 
short SetX(int nIndex, long lDepth, T value, long lTime = 0)
{
#ifdef _BOUNDSCHECK
	if(OverBounds(nIndex))
		return UL_ERROR;
#endif

	switch(m_nValueType)
	{
	case VT_I1:
		{
			cv_chr* pcv = (cv_chr*)FindData(nIndex);
			pcv->assign(lDepth, lTime, (char)value);
		}
		break;
	case VT_I2:
		{
			cv_sht* pcv = (cv_sht*)FindData(nIndex);
			pcv->assign(lDepth, lTime, (short)value);
		}
		break;
	case VT_I4:
		{
			cv_lng* pcv = (cv_lng*)FindData(nIndex);
			pcv->assign(lDepth, lTime, (long)value);
		}
		break;
	case VT_R4:
		{
			cv_flt* pcv = (cv_flt*)FindData(nIndex);
			pcv->assign(lDepth, lTime, (float)value);
		}
		break;
	case VT_R8:
		{
			cv_dbl* pcv = (cv_dbl*)FindData(nIndex);
			pcv->assign(lDepth, lTime, (double)value);
		}
		break;
	case VT_UI1:
		{
			cv_byt* pcv = (cv_byt*)FindData(nIndex);
			pcv->assign(lDepth, lTime, (BYTE)value);
		}
	case VT_UI2:
		{
			cv_wrd* pcv = (cv_wrd*)FindData(nIndex);
			pcv->assign(lDepth, lTime, (WORD)value);
		}
		break;
	case VT_UI4:
		{
			cv_dwd* pcv = (cv_dwd*)FindData(nIndex);
			pcv->assign(lDepth, lTime, (DWORD)value);
		}
		break;
	default:
		return UL_ERROR;
	}
	return UL_NO_ERROR;
}

inline short Set(int nIndex, long lDepth, char cValue, long lTime = 0)
{
	return SetX(nIndex, lDepth, cValue, lTime);
}

inline short Set(int nIndex, long lDepth, short sValue, long lTime = 0)
{
	return SetX(nIndex, lDepth, sValue, lTime);
}

inline short Set(int nIndex, long lDepth, long lValue, long lTime = 0)
{
	return SetX(nIndex, lDepth, lValue, lTime);
}

inline short Set(int nIndex, long lDepth, float fValue, long lTime = 0)
{
	return SetX(nIndex, lDepth, fValue, lTime);
}

inline short Set(int nIndex, long lDepth, double dValue, long lTime = 0)
{
	return SetX(nIndex, lDepth, dValue, lTime);
}

inline short Set(int nIndex, long lDepth, BYTE bValue, long lTime = 0)
{
	return SetX(nIndex, lDepth, bValue, lTime);
}

inline short Set(int nIndex, long lDepth, WORD wValue, long lTime = 0)
{
	return SetX(nIndex, lDepth, wValue, lTime);
}

inline short Set(int nIndex, long lDepth, DWORD dwValue, long lTime = 0)
{
	return SetX(nIndex, lDepth, dwValue, lTime);
}

//////////////////////////////////////////////////////////////////////
//
// Set value of different type
//
//////////////////////////////////////////////////////////////////////

template<typename T>
inline 
short SetValueX(int nIndex0, int nIndex1, T value) 
{	
#ifdef _BOUNDSCHECK
	if(OverBounds(nIndex0))
		return UL_ERROR;
#endif

	switch (m_nValueType) 
	{ 
	case VT_I1: 
		{ 
			cv_chr* pcv = (cv_chr*)FindData(nIndex0); 
			cv_i(char, pcv, nIndex1, value); 
		} 
		break; 
	case VT_I2: 
		{ 
			cv_sht* pcv = (cv_sht*)FindData(nIndex0); 
			cv_i(short, pcv, nIndex1, value); 
		} 
		break; 
	case VT_I4: 
		{ 
			cv_lng* pcv = (cv_lng*)FindData(nIndex0); 
			cv_i(long, pcv, nIndex1, value); 
		} 
		break; 
	case VT_R4: 
		{ 
			cv_flt* pcv = (cv_flt*)FindData(nIndex0); 
			cv_i(float, pcv, nIndex1, value); 
		} 
		break; 
	case VT_R8: 
		{ 
			cv_byt* pcv = (cv_byt*)FindData(nIndex0);  // 用cv_dbl有字节对齐问题
			cv_i(double, pcv, nIndex1, value); 
		} 
		break; 
	case VT_UI1: 
		{ 
			cv_byt* pcv = (cv_byt*)FindData(nIndex0); 
			cv_i(BYTE, pcv, nIndex1, value); 
		} 
		break; 
	case VT_UI2: 
		{ 
			cv_wrd* pcv = (cv_wrd*)FindData(nIndex0); 
			cv_i(WORD, pcv, nIndex1, value); 
		} 
		break; 
	case VT_UI4: 
		{ 
			cv_dwd* pcv = (cv_dwd*)FindData(nIndex0); 
			cv_i(DWORD, pcv, nIndex1, value); 
		} 
		break; 
	default: 
		return UL_ERROR; 
	} 
	return UL_NO_ERROR;
}

inline short SetValue(char cValue, int nIndex0, int nIndex1 = 0)
{	
	return SetValueX(nIndex0, nIndex1, cValue);
}

inline short SetValue(short sValue, int nIndex0, int nIndex1 = 0)
{
	return SetValueX(nIndex0, nIndex1, sValue);
}

inline short SetValue(long lValue, int nIndex0, int nIndex1 = 0)
{
	return SetValueX(nIndex0, nIndex1, lValue);
}

inline short SetValue(float fValue, int nIndex0, int nIndex1 = 0)
{
	return SetValueX(nIndex0, nIndex1, fValue);
}

inline short SetValue(double dValue, int nIndex0, int nIndex1 = 0)
{
	return SetValueX(nIndex0, nIndex1, dValue);
}

inline short SetValue(BYTE bValue, int nIndex0, int nIndex1 = 0)
{
	return SetValueX(nIndex0, nIndex1, bValue);
}

inline short SetValue(WORD wValue, int nIndex0, int nIndex1 = 0)
{
	return SetValueX(nIndex0, nIndex1, wValue);
}

inline short SetValue(DWORD dwValue, int nIndex0, int nIndex1 = 0)
{
	return SetValueX(nIndex0, nIndex1, dwValue);
}


//////////////////////////////////////////////////////////////////////
//
// Get value of different type
//
//////////////////////////////////////////////////////////////////////

#define CopyValueX(pcvd, pcvs, vt) \
	switch (m_nValueType)\
	{\
	case VT_I1:\
		{\
			for (int i = 0; i < m_nPointArray; i++)\
				cv_io(vt, pcvd, char, pcvs, i);\
		}\
		break;\
	case VT_I2:\
		{\
			for (int i = 0; i < m_nPointArray; i++)\
				cv_io(vt, pcvd, short, pcvs, i);\
		}\
		break;\
	case VT_I4:\
		{\
			for (int i = 0; i < m_nPointArray; i++)\
				cv_io(vt, pcvd, long, pcvs, i);\
		}\
		break;\
	case VT_R4:\
		{\
			for (int i = 0; i < m_nPointArray; i++)\
				cv_io(vt, pcvd, float, pcvs, i);\
		}\
		break;\
	case VT_R8:\
		{\
			for (int i = 0; i < m_nPointArray; i++)\
				cv_io(vt, pcvd, double, pcvs, i);\
		}\
		break;\
	case VT_UI1:\
		{\
			for (int i = 0; i < m_nPointArray; i++)\
				cv_io(vt, pcvd, BYTE, pcvs, i);\
		}\
		break;\
	case VT_UI2:\
		{\
			for (int i = 0; i < m_nPointArray; i++)\
				cv_io(vt, pcvd, WORD, pcvs, i);\
		}\
		break;\
	case VT_UI4:\
		{\
			for (int i = 0; i < m_nPointArray; i++)\
				cv_io(vt, pcvd, DWORD, pcvs, i);\
		}\
		break;\
	default:\
		return UL_ERROR;\
	}\
	return UL_NO_ERROR\

// Copy char value
//
inline short CopychrValue(cv_t* pcvd, cv_t* pcvs)
{
	CopyValueX(pcvd, pcvs, char);
}

// Copy short value
//
inline short CopyshtValue(cv_t* pcvd, cv_t* pcvs)
{
	CopyValueX(pcvd, pcvs, short);
}

// Copy long value
//
inline short CopylngValue(cv_t* pcvd, cv_t* pcvs)
{
	CopyValueX(pcvd, pcvs, long);
}

// Copy float value
//
inline short CopyfltValue(cv_t* pcvd, cv_t* pcvs)
{
	CopyValueX(pcvd, pcvs, float);
}

// Copy double value
//
inline short CopydblValue(cv_t* pcvd, cv_t* pcvs)
{
	CopyValueX(pcvd, pcvs, double);
}

// Copy BYTE value
//
inline short CopybytValue(cv_t* pcvd, cv_t* pcvs)
{
	CopyValueX(pcvd, pcvs, BYTE);
}

// Copy WORD value
//
inline short CopywrdValue(cv_t* pcvd, cv_t* pcvs)
{
	CopyValueX(pcvd, pcvs, WORD);
}

// Copy DWORD value
//
inline short CopydwdValue(cv_t* pcvd, cv_t* pcvs)
{
	CopyValueX(pcvd, pcvs, DWORD);
}

template<typename T>
inline
T GetValueX(int nIndex0, int nIndex1, T value)
{
#ifdef _BOUNDSCHECK
	if(OverBounds(nIndex0))
	{
		TRACE(_T("Error : Get value out of range!\n"));
		return 0;
	}
#endif

	m_CS.Lock(); 
	switch (m_nValueType) 
	{ 
	case VT_I1: 
		{ 
			cv_chr* pcv = (cv_chr*)FindData(nIndex0);
			if(NULL == pcv)	
			{
				m_CS.Unlock();
				return 0;
			}
			cv_o(char, pcv, nIndex1, value); 
		} 
		break; 
	case VT_I2: 
		{ 
			cv_sht* pcv = (cv_sht*)FindData(nIndex0);
			if(NULL == pcv)	
			{	
				m_CS.Unlock();
				return 0;
			}
			cv_o(short, pcv, nIndex1, value); 
		} 
		break; 
	case VT_I4: 
		{ 
			cv_lng* pcv = (cv_lng*)FindData(nIndex0);
			if(NULL == pcv)	
			{	
				m_CS.Unlock();
				return 0;
			}
			cv_o(long, pcv, nIndex1, value); 
		} 
		break; 
	case VT_R4: 
		{ 
			cv_flt* pcv = (cv_flt*)FindData(nIndex0); 
			if(NULL == pcv)	
			{	
				m_CS.Unlock();
				return 0;
			}
			cv_o(float, pcv, nIndex1, value); 
		} 
		break; 
	case VT_R8: 
		{ 
			cv_byt* pcv = (cv_byt*)FindData(nIndex0);  // 用cv_dbl有字节对齐问题
			if(NULL == pcv)	
			{	
				m_CS.Unlock();
				return 0;
			}
			cv_o(double, pcv, nIndex1, value); 
		} 
		break; 
	case VT_UI1: 
		{ 
			cv_byt* pcv = (cv_byt*)FindData(nIndex0);
			if(NULL == pcv)	
			{	
				m_CS.Unlock();
				return 0;
			}
			cv_o(BYTE, pcv, nIndex1, value); 
		} 
		break; 
	case VT_UI2: 
		{ 
			cv_wrd* pcv = (cv_wrd*)FindData(nIndex0);
			if(NULL == pcv)	
			{	
				m_CS.Unlock();
				return 0;
			}
			cv_o(WORD, pcv, nIndex1, value); 
		} 
		break; 
	case VT_UI4: 
		{ 
			cv_dwd* pcv = (cv_dwd*)FindData(nIndex0); 
			if(NULL == pcv)	
			{	
				m_CS.Unlock();
				return 0;
			}
			cv_o(DWORD, pcv, nIndex1, value); 
		} 
		break; 
	default: 
		TRACE(_T("Error : Get value of unknown type!\n")); 
	} 
	m_CS.Unlock(); 
	return value;
}
// Get char value
//
inline char GetchrValue(int nIndex0, int nIndex1 = 0)
{	
	char cValue = 0;
	return GetValueX(nIndex0, nIndex1, cValue);
}

// Get short value
//
inline short GetshtValue(int nIndex0, int nIndex1 = 0)
{
	short sValue = 0;
	return GetValueX(nIndex0, nIndex1, sValue);
}

// Get long value
//
inline long GetlngValue(int nIndex0, int nIndex1 = 0)
{
	long lValue = 0;
	return GetValueX(nIndex0, nIndex1, lValue);
}

// Get float value
//
inline float GetfltValue(int nIndex0, int nIndex1 = 0)
{
	float fValue = 0;
	return GetValueX(nIndex0, nIndex1, fValue);
}

// Get double value
//
inline double GetdblValue(UINT nIndex0, int nIndex1 = 0)
{
	double dValue = 0;
	return GetValueX(nIndex0, nIndex1, dValue);
}

// Get BYTE value
//
inline BYTE GetbytValue(int nIndex0, int nIndex1 = 0)
{
	BYTE bValue = 0;
	return GetValueX(nIndex0, nIndex1, bValue);
}

// Get WORD value
//
inline WORD GetwrdValue(int nIndex0, int nIndex1 = 0)
{
	WORD wValue = 0;	
	return GetValueX(nIndex0, nIndex1, wValue);
}

// Get DWORD value
//
inline DWORD GetdwdValue(int nIndex0, int nIndex1 = 0)
{
	DWORD dwValue = 0;
	return GetValueX(nIndex0, nIndex1, dwValue);
}

// Get value buffer (pointer)
//
inline void* GetValuePtr(int nIndex, BOOL bForceRange = FALSE)
{
	if (nIndex < 0)
	{
		if (bForceRange)
			nIndex = 0;
		else
			return NULL;
	}
	else if(nIndex >= m_arrData.GetSize())
	{
		if (bForceRange)
			nIndex = m_arrData.GetSize()-1;
		else
			return NULL;
	}
	
	cv_t* pcv = (cv_t*)FindData(nIndex);
	if (pcv != NULL)
	{
		return &pcv->m_Value;
	}
	return NULL;
}

inline void IniCurSavePos(long lStartDepth, int nOff)
{
	int nEndPos = m_arrData.GetSize() - 1;

	//Add by zy 2012 11 09 修改计算回放后保存非绘图模板数据出直线问题
	int nSize = m_arrData.GetSize();
	if(m_nDirection == -1)
	{
		if (nSize > 1)
		{
			long lDepth0 = GetDepth(0);
			long lDepthE = GetDepth(nSize - 1);
			m_nDirection = (BOOL)(lDepth0 < lDepthE);
		}
	}

	if (nOff == 1)
	{
		m_nCurSavePos = 0;
		if (m_nDirection == SCROLL_UP)
		{
			while (m_nCurSavePos < nEndPos)
			{
				if (GetDepth(m_nCurSavePos) > lStartDepth)
					m_nCurSavePos++;
				else
					break;
			}
		}
		else
		{
			while (m_nCurSavePos < nEndPos)
			{
				if (GetDepth(m_nCurSavePos) < lStartDepth)
					m_nCurSavePos++;
				else
					break;
			}
		}
	}
	else
	{
		m_nCurSavePos = nEndPos;
		if (m_nDirection == SCROLL_UP)
		{
			while (m_nCurSavePos > 0)
			{
				if (GetDepth(m_nCurSavePos) < lStartDepth)
					m_nCurSavePos--;
				else
					break;
			}
		}
		else
		{
			while (m_nCurSavePos > 0)
			{
				if (GetDepth(m_nCurSavePos) > lStartDepth)
					m_nCurSavePos--;
				else
					break;
			}
		}
	}
}

void GetCurSaveValues(long lDepth, LPBYTE lpData, size_t sz, long lDLevel, int nOff = 0);


inline short GetValue(int nIndex0, void* pValue)
{
	int nSize = GetValueSize();
	if (AfxIsValidAddress(pValue, nSize))
	{
		m_CS.Lock();
		LPVOID pBuffer = GetValuePtr(nIndex0, TRUE);
		if (pBuffer != NULL)
		{
			memcpy(pValue, pBuffer, nSize);
			m_CS.Unlock();
			return UL_NO_ERROR;
		}
		m_CS.Unlock();
		memset(pValue, 0, nSize);
	}
	
	return UL_ERROR;
}

// Get value of value buffer
// 
inline double GetdblValueOf(void* pValue, int nIndex1, VARTYPE vt = VT_NULL)
{
	if (vt == VT_NULL)
		vt = m_nValueType;
	
	double dValue = 0;
	//m_CS.Lock();
	switch (vt)
	{
	case VT_I1:
		dValue = ((char*)pValue)[nIndex1];
		break;
	case VT_I2:
		dValue = ((short*)pValue)[nIndex1];
		break;
	case VT_I4:
		dValue = ((long*)pValue)[nIndex1];
		break;
	case VT_R4:
		dValue = ((float*)pValue)[nIndex1];
		break;
	case VT_R8:
		dValue = ((double*)pValue)[nIndex1];
		break;
	case VT_UI1:
		dValue = ((BYTE*)pValue)[nIndex1];
		break;
	case VT_UI2:
		dValue = ((WORD*)pValue)[nIndex1];
		break;
	case VT_UI4:
		dValue = ((DWORD*)pValue)[nIndex1];
		break;
	default:
		TRACE(_T("Error : Get value of unsupported type!\n"));
	}
	//m_CS.Unlock();
	return dValue;
}

// Get max value of value buffer
//
inline double GetValueMaxOf(void* pValue, int nStart, int nEnd)
{
	if (m_nPointArray < nEnd)
		nEnd = m_nPointArray;

	switch (m_nValueType)
	{
	case VT_I1:
		{
			char cMax = ((char*)pValue)[nStart];
			for (int i = nStart+1; i < nEnd; i++)
			{
				if (cMax < ((char*)pValue)[i])
					cMax = ((char*)pValue)[i];
			}
			return (double)cMax;
		}
	case VT_I2:
		{
			short sMax = ((short*)pValue)[nStart];
			for (int i = nStart+1; i < nEnd; i++)
			{
				if (sMax < ((short*)pValue)[i])
					sMax = ((short*)pValue)[i];
			}
			return (double)sMax;
		}
	case VT_I4:
		{
			long lMax = ((long*)pValue)[nStart];
			for (int i = nStart+1; i < nEnd; i++)
			{
				if (lMax < ((long*)pValue)[i])
					lMax = ((long*)pValue)[i];
			}
			return (double)lMax;
		}
	case VT_R4:		
		{			
			float fMax = ((float*)pValue)[nStart];
			for (int i = nStart+1; i < nEnd; i++)
			{
				if (fMax < ((float*)pValue)[i])
					fMax = ((float*)pValue)[i];
			}
			return (double)fMax;			
		}
	case VT_R8:
		{
			double dMax = ((double*)pValue)[nStart];
			for (int i = nStart+1; i < nEnd; i++)
			{
				if (dMax < ((double*)pValue)[i])
					dMax = ((double*)pValue)[i];
			}
			return (double)dMax;
		}
	case VT_UI1:
		{
			BYTE bMax = ((BYTE*)pValue)[nStart];
			for (int i = nStart+1; i < nEnd; i++)
			{
				if (bMax < ((BYTE*)pValue)[i])
					bMax = ((BYTE*)pValue)[i];
			}
			return (double)bMax;
		}
	case VT_UI2:
		{
			WORD wMax = ((WORD*)pValue)[nStart];
			for (int i = nStart+1; i < nEnd; i++)
			{
				if (wMax < ((WORD*)pValue)[i])
					wMax = ((WORD*)pValue)[i];
			}
			return (double)wMax;
		}
	case VT_UI4:
		{
			DWORD dwMax = ((DWORD*)pValue)[nStart];
			for (int i = nStart+1; i < nEnd; i++)
			{
				if (dwMax < ((DWORD*)pValue)[i])
					dwMax = ((DWORD*)pValue)[i];
			}
			return (double)dwMax;
		}
	}
	
	return 0.0;
}

// Get min value of value buffer
//
inline double GetValueMinOf(void* pValue, int nStart, int nEnd)
{
	if (m_nPointArray < nEnd)
		nEnd = m_nPointArray;
	
	switch(m_nValueType)
	{
	case VT_I1:
		{
			char cMin = ((char*)pValue)[nStart];
			for (int i = nStart+1; i < nEnd; i++)
			{
				if (cMin > ((char*)pValue)[i])
					cMin = ((char*)pValue)[i];
			}
			return (double)cMin;
		}
	case VT_I2:
		{
			short sMin = ((short*)pValue)[nStart];
			for (int i = nStart+1; i < nEnd; i++)
			{
				if (sMin > ((short*)pValue)[i])
					sMin = ((short*)pValue)[i];
			}
			return (double)sMin;
		}
	case VT_I4:
		{
			long lMin = ((long*)pValue)[nStart];
			for (int i = nStart+1; i < nEnd; i++)
			{
				if (lMin > ((long*)pValue)[i])
					lMin = ((long*)pValue)[i];
			}
			return (double)lMin;
		}
	case VT_R4:		
		{			
			float fMin = ((float*)pValue)[nStart];
			for (int i = nStart+1; i < nEnd; i++)
			{
				if (fMin > ((float*)pValue)[i])
					fMin = ((float*)pValue)[i];
			}
			return (double)fMin;			
		}
	case VT_R8:
		{
			double dMin = ((double*)pValue)[nStart];
			for (int i = nStart+1; i < nEnd; i++)
			{
				if (dMin > ((double*)pValue)[i])
					dMin = ((double*)pValue)[i];
			}
			return (double)dMin;
		}
	case VT_UI1:
		{
			BYTE bMin = ((BYTE*)pValue)[nStart];
			for (int i = nStart+1; i < nEnd; i++)
			{
				if (bMin > ((BYTE*)pValue)[i])
					bMin = ((BYTE*)pValue)[i];
			}
			return (double)bMin;
		}
	case VT_UI2:
		{
			WORD wMin = ((WORD*)pValue)[nStart];
			for (int i = nStart+1; i < nEnd; i++)
			{
				if (wMin > ((WORD*)pValue)[i])
					wMin = ((WORD*)pValue)[i];
			}
			return (double)wMin;
		}
	case VT_UI4:
		{
			DWORD dwMin = ((DWORD*)pValue)[nStart];
			for (int i = nStart+1; i < nEnd; i++)
			{
				if (dwMin > ((DWORD*)pValue)[i])
					dwMin = ((DWORD*)pValue)[i];
			}
			return (double)dwMin;
		}
	}
	
	return 0.0;
}

// Overload get max value
//
inline double GetValueMax(int nIndex0, int nStart, int nEnd)
{
	void* pValue = GetValuePtr(nIndex0);
	if(pValue != NULL)
		return GetValueMaxOf(pValue, nStart, nEnd);
	return 0.0;
}

// Overload get min value
//
inline double GetValueMin(int nIndex0, int nStart, int nEnd)
{
	void* pValue = GetValuePtr(nIndex0);
	if(pValue != NULL)
		return GetValueMinOf(pValue, nStart, nEnd);
	return 0.0;
}

// Copy data from another data
// 
inline short Copy(CCurveData* pData, BOOL bReverse = FALSE)
{
	if (pData == this)
		return UL_NO_ERROR;

	m_arrData.RemoveAll();
	while (m_mpData.GetCount() >= 1)
	{
		MBLOCK* pB = m_mpData.RemoveTail();
		if (pB->pAddr)
			delete pB->pAddr;
		delete pB;
	}

	if (m_nValueType != pData->m_nValueType)
	{
		SetValueType(pData->m_nValueType);
		TRACE(_T("Warning : Copy datas of a different precision!\n"));
	}

	if ((m_nPointFrame != pData->m_nPointFrame) || (m_nArrayCount != pData->m_nArrayCount))
	{
		m_nPointFrame = pData->m_nPointFrame;
		m_nArrayCount = pData->m_nArrayCount;
		m_nPointArray = pData->m_nPointArray;
		m_nDimension  = pData->m_nDimension;
		TRACE(_T("Warning : Copy datas of a different dimension!\n"));
	}

	if (m_nDirection != pData->m_nDirection)
	{
		m_nDirection = pData->m_nDirection;
		TRACE(_T("Warning : Copy datas of a different direction!\n"));
	}

	int nCount = pData->GetSize();
	if (m_mpData.GetCount())
	{
		MBLOCK* pB = m_mpData.GetHead();
		pB->dwOff = 0;
	}
	else
		MInit(nCount);

	pData->m_CS.Lock();
	if (bReverse)
	{
		for (int i = nCount - 1; i > -1; i--)
		{
			cv_t* pcvs = (cv_t*)pData->FindData(i);
			m_arrData.Add(MNew(pcvs));
		}
	}
	else
	{
		for (int i = 0; i < nCount; i++)
		{
			cv_t* pcvs = (cv_t*)pData->FindData(i);
			m_arrData.Add(MNew(pcvs));
		}
	}
	pData->m_CS.Unlock();
	return UL_NO_ERROR;
}

// Copy data from another data
// 
inline short CopyFrom(CCurveData* pData, int nDirect, long lMin, long lMax)
{
	if (pData == this)
		return UL_NO_ERROR;

	//Start:判断是否应该拷贝数据，包括比较方向，是否有数据,要拷贝数据范围是否对等
	if( lMin == lMax )
	{
		return UL_NO_ERROR;
	}
	else if( lMin > lMax )
	{	
		long lTemp = lMin;
		lMin = lMax;
		lMax = lTemp;
	}
	
	if (GetSize() < 1) //该数据为空
	{
		if (m_nValueType != pData->m_nValueType)
		{
			SetValueType(pData->m_nValueType);
			TRACE(_T("Warning : Copy datas of a different precision!\n"));
		}
		
		if ((m_nPointFrame != pData->m_nPointFrame) || (m_nArrayCount != pData->m_nArrayCount))
		{
			m_nPointFrame = pData->m_nPointFrame;
			m_nArrayCount = pData->m_nArrayCount;
			m_nPointArray = pData->m_nPointArray;
			m_nDimension  = pData->m_nDimension;
			TRACE(_T("Warning : Copy datas of a different dimension!\n"));
		}

		if (m_nDirection != nDirect)
		{
			m_nDirection = nDirect;
			TRACE(_T("Warning : Copy datas of a different direction!\n"));
		}	
	}
	else // 该数据不为空
	{
		if (pData->m_nValueType != m_nValueType)
		{
			return UL_NO_ERROR;
		}

		if ((m_nPointFrame != pData->m_nPointFrame) || (m_nArrayCount != pData->m_nArrayCount))
		{
			return UL_NO_ERROR;
		}

		if (m_nDirection != nDirect)
		{
			return UL_NO_ERROR;
		}	
	}

//	AutoDirection();
	
	BOOL bReverse;
	UINT nSize = pData->GetSize();
	if( nSize < 2 )
	{
		return UL_NO_ERROR;
	}
	long lSrcMin = pData->GetDepth(0);
	long lSrcMax = pData->GetDepth(nSize-1);	
	if( lSrcMin == lSrcMax )
	{
		return UL_NO_ERROR;
	}
	else if( lSrcMin > lSrcMax )//源数据方向向上
	{	
		long lTemp = lSrcMin;
		lSrcMin = lSrcMax;
		lSrcMax = lTemp;
		if( SCROLL_UP == nDirect )
		{
			bReverse = FALSE;
		}
		else if( SCROLL_DOWN == nDirect )
		{
			bReverse = TRUE;
		}
		else
		{
			return UL_NO_ERROR;
		}
	}
	else // 源数据方向向下
	{
		if( SCROLL_UP == nDirect )
		{
			bReverse = TRUE;
		}
		else if( SCROLL_DOWN == nDirect )
		{
			bReverse = FALSE;
		}
		else
		{
			return UL_NO_ERROR;
		}
	}

	if (lSrcMin > lMin || lSrcMax < lMax )
	{
		return UL_NO_ERROR;
	}
	//End:判断是否应该拷贝数据，包括比较方向，是否有数据,要拷贝数据范围是否对等

	//Start:拷贝的起始索引
	int indexMin = pData->GetFirstIndexByDepth(lMin);
	int indexMax = pData->GetFirstIndexByDepth(lMax);
	if( indexMin<0 || indexMin>=nSize || indexMax<0 || indexMax>=nSize )
	{
		return UL_NO_ERROR;
	}
	if(indexMin > indexMax)
	{
		int iTemp = indexMin;
		indexMin = indexMax;
		indexMax = iTemp;
	}
	//End:拷贝的起始索引


	int nCount = pData->GetSize();
	if (m_mpData.GetCount())
	{
		MBLOCK* pB = m_mpData.GetHead();
		pB->dwOff = 0;
	}
	else
		MInit(nCount);

	pData->m_CS.Lock();
	if (bReverse)
	{
		for (int i = indexMax; i >= indexMin; i--)
		{
			cv_t* pcvs = (cv_t*)pData->FindData(i);
			m_arrData.Add(MNew(pcvs));
		}
	}
	else
	{
		for (int i = indexMin; i <= indexMax; i++)
		{
			cv_t* pcvs = (cv_t*)pData->FindData(i);
			m_arrData.Add(MNew(pcvs));
		}
	}
	pData->m_CS.Unlock();
	return UL_NO_ERROR;
}

// Move data from another data
//
inline short Move(CCurveData* pData)
{
	if (pData == this)
		return UL_NO_ERROR;
	
	Clear();
	if (m_nValueType != pData->m_nValueType)
	{
		SetValueType(pData->m_nValueType);
		TRACE(_T("Warning : Move datas of a different precision!\n"));
	}
	
	if ((m_nPointFrame != pData->m_nPointFrame) || (m_nArrayCount != pData->m_nArrayCount))
	{
		m_nPointFrame = pData->m_nPointFrame;
		m_nArrayCount = pData->m_nArrayCount;
		m_nPointArray = pData->m_nPointArray;
		m_nDimension = pData->m_nDimension;
		TRACE(_T("Warning : Move datas of a different dimension!\n"));
	}

	MInit();
	m_arrData.Copy(pData->m_arrData);
	m_mpData.AddTail(&pData->m_mpData);
	pData->m_arrData.RemoveAll();
	pData->m_mpData.RemoveAll();
	
	return UL_NO_ERROR;
}

// Get first index of depth
//
inline int GetFirstIndexByDepth(long lDepth, int nDirection = -1)
{
	int nEndPos = m_arrData.GetSize() - 1;
	if (nEndPos < 0)
		return -1;

	if (nEndPos < 2)
		return 0;

	int nStartPos = 0;

	int	nCurPos = (nStartPos + nEndPos)/2;

	if (nDirection < 0)
		nDirection = m_nDirection;
	
	if (nDirection == SCROLL_UP) // 上测时
	{
		// lDepth在整条曲线的下方
		if (GetDepth(0) <= lDepth)
			return 0;
		// lDepth在整条曲线的上方
		if (lDepth <= GetDepth(nEndPos))
			return nEndPos;

		while (TRUE)
		{
			if ((nEndPos - nStartPos) == 1)
			{	
				if ((lDepth - GetDepth(nEndPos)) < (GetDepth(nStartPos) - lDepth))
					nCurPos = nEndPos;
				else
					nCurPos = nStartPos;
				break;
			}
			
			if (lDepth == GetDepth(nCurPos))
				break;

			if (lDepth < GetDepth(nCurPos))
				nStartPos = nCurPos;
			else
				nEndPos = nCurPos;
			nCurPos = (nStartPos+nEndPos)/2;
		}
	}
	else // 下测时
	{
		// lDepth在曲线的上方
		if (lDepth <= GetDepth(0))
			return 0;
		// lDepth在曲线的下方
		if (GetDepth(nEndPos) <= lDepth)
			return nEndPos;

		while (TRUE)
		{
			if ((nEndPos - nStartPos) == 1)
			{	
				if ((lDepth - GetDepth(nStartPos)) < (GetDepth(nEndPos) - lDepth))
					nCurPos = nStartPos;
				else
					nCurPos = nEndPos;
				break;
			}
			
			if (lDepth == GetDepth(nCurPos))
				break;

			if (lDepth > GetDepth(nCurPos))
				nStartPos = nCurPos;
			else
				nEndPos = nCurPos;
			nCurPos = (nStartPos+nEndPos)/2;
		}
	}
	return nCurPos;
}

// Get first index of depth
//
inline int GetIndexByDepth(long lDepth, int nStartPos, int nEndPos)
{	
	int	nCurPos = (nStartPos + nEndPos)/2;	
	if (m_nDirection == SCROLL_UP) // 上测时
	{
		// lDepth在整条曲线的下方
		if (GetDepth(nStartPos) < lDepth)
			return -1;
		
		// lDepth在整条曲线的上方
		if (lDepth < GetDepth(nEndPos))
			return -1;
		
		while (TRUE)
		{
			if ((nEndPos - nStartPos) < 2)
			{	
				nCurPos = nEndPos;
				break;
			}
			
			if (lDepth == GetDepth(nCurPos))
				break;
			
			if (lDepth < GetDepth(nCurPos))
				nStartPos = nCurPos;
			else
				nEndPos = nCurPos;
			nCurPos = (nStartPos+nEndPos)/2;
		}
	}
	else // 下测时
	{
		// lDepth在曲线的上方
		if (lDepth < GetDepth(nStartPos))
			return -1;
		
		// lDepth在曲线的下方
		if (GetDepth(nEndPos) < lDepth)
			return -1;
		
		while (TRUE)
		{
			if ((nEndPos - nStartPos) < 2)
			{	
				nCurPos = nEndPos;
				break;
			}
			
			if (lDepth == GetDepth(nCurPos))
				break;
			
			if (lDepth > GetDepth(nCurPos))
				nStartPos = nCurPos;
			else
				nEndPos = nCurPos;
			nCurPos = (nStartPos+nEndPos)/2;
		}
	}
	return nCurPos;
}

double GetValueAtDepthEx(long lDepth, int nIndex1 = 0);

// Get depth range of drawing
//
inline short GetDepthRange(long* pStart, long* pEnd, int nStart0 = 0)
{
	int nEndPos = m_arrData.GetSize() - 1;
	if (nEndPos < nStart0)
	{
		*pStart = *pEnd = 0;
		return UL_ERROR;
	}
	
	if (nEndPos < 2)
	{
		*pStart = nStart0;
		*pEnd = nEndPos;
		return UL_NO_ERROR;
	}
	
	long l0 = *pStart;
	long lE = *pEnd;
	if (l0 > lE)
	{
		l0 = *pEnd;
		lE = *pStart;
	}

	int nStartPos = nStart0;
	int	nCurPos = (nStartPos + nEndPos)/2;

	// Up index++ depth--
	if (m_nDirection == SCROLL_UP)
	{
		// 最小的深度在整条曲线的下方
		if (GetDepth(nStart0) < l0)
		{
			*pStart = *pEnd = nStart0;
			return UL_NO_ERROR;
		}

		// 最大的深度在整条曲线的上方
		if (lE < GetDepth(nEndPos))
		{
			*pStart = *pEnd = nEndPos;
			return UL_NO_ERROR;
		}

		// 最小的深度在整条曲线的上方
		BOOL bS0 = TRUE, bSE = TRUE;
		if (l0 <= GetDepth(nEndPos))
		{
			*pEnd = nEndPos;
			bSE = FALSE;
		}

		// 最大的深度在整条曲线的下方
		if (GetDepth(nStart0) <= lE)
		{
			*pStart = nStart0;
			bS0 = FALSE;
		}
		
		// 继续查找最大深度的最小索引
		int nEndPosE = nEndPos;
		while (bS0)
		{
			if ((nEndPos - nStartPos) == 1)
			{	
				nCurPos = nStartPos;
				break;
			}
			
			if (lE == GetDepth(nCurPos))
				break;
			
			if (lE < GetDepth(nCurPos))
				nStartPos = nCurPos;
			else
				nEndPos = nCurPos;
			nCurPos = (nStartPos + nEndPos)/2;
		}

		if (bS0)
			*pStart = nCurPos;
		
		// 继续查找最小深度的最大索引
		nCurPos = (nStartPos + nEndPosE)/2;
		while (bSE)
		{
			if ((nEndPosE - nStartPos) == 1)
			{	
				nCurPos = nEndPosE;
				break;
			}
			
			if (l0 == GetDepth(nCurPos))
				break;
			
			if (l0 < GetDepth(nCurPos))
				nStartPos = nCurPos;
			else
				nEndPosE = nCurPos;
			nCurPos = (nStartPos + nEndPosE)/2;
		}

		if (bSE)
			*pEnd = nCurPos;

		return UL_NO_ERROR;
	}
	
	// Down index++ depth++
	// 最小的深度在整条曲线的下方
	if (GetDepth(nEndPos) < l0)
	{
		*pStart = *pEnd = nEndPos;
		return UL_NO_ERROR;
	}
	
	// 最大的深度在整条曲线的上方
	if (lE < GetDepth(nStart0))
	{
		*pStart = *pEnd = nStart0;
		return UL_NO_ERROR;
	}
	
	// 最小的深度在整条曲线的上方
	BOOL bS0 = TRUE, bSE = TRUE;
	if (l0 <= GetDepth(nStart0))
	{
		*pStart = nStart0;
		bS0 = FALSE;
	}
	
	// 最大的深度在整条曲线的下方
	if (GetDepth(nEndPos) <= lE)
	{
		*pEnd = nEndPos;
		bSE = FALSE;
	}
	
	// 继续查找最大深度的最大索引
	int nStartPosE = nStartPos;
	while (bSE)
	{
		if ((nEndPos - nStartPos) == 1)
		{	
			nCurPos = nEndPos;
			break;
		}
		
		if (lE == GetDepth(nCurPos))
			break;
		
		if (lE < GetDepth(nCurPos))
			nEndPos = nCurPos;
		else
			nStartPos = nCurPos;
		nCurPos = (nStartPos + nEndPos)/2;
	}
	
	if (bSE)
		*pEnd = nCurPos;
	
	// 继续查找最小深度的最小索引
	nCurPos = (nStartPosE + nEndPos)/2;
	while (bS0)
	{
		if ((nEndPos - nStartPosE) == 1)
		{	
			nCurPos = nStartPosE;
			break;
		}
		
		if (l0 == GetDepth(nCurPos))
			break;
		
		if (l0 < GetDepth(nCurPos))
			nEndPos = nCurPos;
		else
			nStartPosE = nCurPos;
		nCurPos = (nStartPosE + nEndPos)/2;
	}
	
	if (bS0)
		*pStart = nCurPos;
	
	return UL_NO_ERROR;
}
// Get Time Range of drawing

inline short GetTimeRange(long* pStart, long* pEnd, int nStart0 = 0)
{
	int nEndPos = m_arrData.GetSize() - 1;
	if (nEndPos < nStart0)
	{
		*pStart = *pEnd = 0;
		return UL_ERROR;
	}
	
	if (nEndPos < 2)
	{
		*pStart = nStart0;
		*pEnd = nEndPos;
		return UL_NO_ERROR;
	}
	
	long l0 = *pStart;
	long lE = *pEnd;
	if (l0 > lE)
	{
		l0 = *pEnd;
		lE = *pStart;
	}

	int nStartPos = nStart0;
	int	nCurPos = (nStartPos + nEndPos)/2;
	
	// Down index++ depth++
	// 最小的深度在整条曲线的下方
	if (GetTime(nEndPos) < l0)
	{
		*pStart = *pEnd = nEndPos;
		return UL_NO_ERROR;
	}
	
	// 最大的深度在整条曲线的上方
	if (lE < GetTime(nStart0))
	{
		*pStart = *pEnd = nStart0;
		return UL_NO_ERROR;
	}
	
	// 最小的深度在整条曲线的上方
	BOOL bS0 = TRUE, bSE = TRUE;
	if (l0 <= GetTime(nStart0))
	{
		*pStart = nStart0;
		bS0 = FALSE;
	}
	
	// 最大的深度在整条曲线的下方
	if (GetTime(nEndPos) <= lE)
	{
		*pEnd = nEndPos;
		bSE = FALSE;
	}
	
	// 继续查找最大深度的最大索引
	int nStartPosE = nStartPos;
	while (bSE)
	{
		if ((nEndPos - nStartPos) == 1)
		{	
			nCurPos = nEndPos;
			break;
		}
		
		if (lE == GetTime(nCurPos))
			break;
		
		if (lE < GetTime(nCurPos))
			nEndPos = nCurPos;
		else
			nStartPos = nCurPos;
		nCurPos = (nStartPos + nEndPos)/2;
	}
	
	if (bSE)
		*pEnd = nCurPos;
	
	// 继续查找最小深度的最小索引
	nCurPos = (nStartPosE + nEndPos)/2;
	while (bS0)
	{
		if ((nEndPos - nStartPosE) == 1)
		{	
			nCurPos = nStartPosE;
			break;
		}
		
		if (l0 == GetTime(nCurPos))
			break;
		
		if (l0 < GetTime(nCurPos))
			nEndPos = nCurPos;
		else
			nStartPosE = nCurPos;
		nCurPos = (nStartPosE + nEndPos)/2;
	}
	
	if (bS0)
		*pStart = nCurPos;
	
	return UL_NO_ERROR;
}

// Get first index of time
//
inline int GetFirstIndexByTime(long lTime, int nDirection = -1)
{	
	int nEndPos = m_arrData.GetSize() - 1;
	if(nEndPos < 0)
		return -1;

	if(nEndPos < 2)
		return 0;

	int nStartPos = 0;

	int	nCurPos = (nStartPos + nEndPos)/2;
	
	if(nDirection < 0)
		nDirection = m_nDirection;
	
	if(nDirection == SCROLL_UP) //上
	{
		if (GetTime(0) <= lTime)
			return 0;

		if (lTime <= GetTime(nEndPos))
			return nEndPos;

		while (TRUE)
		{
			if ((nEndPos -nStartPos) == 1)
			{	
				nCurPos = nEndPos;
				break;
			}
			
			if (lTime == GetTime(nCurPos))
				break;
			if (lTime < GetTime(nCurPos))
				nStartPos = nCurPos;
			else
				nEndPos = nCurPos;
			nCurPos = (nStartPos +nEndPos)/2;
		}
	}
	else //下
	{
		if (lTime <= GetTime(0))
			return 0;

		if (GetTime(nEndPos) <= lTime)
			return nEndPos;

		while (TRUE)
		{
			
			if ((nEndPos - nStartPos) == 1)
			{	
				nCurPos = nEndPos;
				break;
			}
			
			if (lTime == GetTime(nCurPos))
				break;
			if (lTime > GetTime(nCurPos))
				nStartPos = nCurPos;
			else
				nEndPos = nCurPos;
			nCurPos = (nStartPos +nEndPos)/2;	
		}
	}
	return nCurPos;
}

public:

	/* ----- 取消关联曲线 ----- */
	void	DetachCurve(CCurve* pCurve);
	void	DetachCurves(BOOL bRemove = FALSE);

	/* ------------------------------------ *
	 *		关联曲线显示打印位置控制
	 * ------------------------------------ */
	void	SetCurDisplayPosSame(int nPos);
	void	IncCurDisplayPosSame(int nAddPos);
	void	SetCurPrintPosSame(int nPos);
	void	IncCurPrintPosSame(int nAddPos);

	/* ------------------------------------ *
	 *		数据操作
	 * ------------------------------------ */
	short	Add(long lDepth, void* pValue, long lTime = 0);
	short	Push(long lDepth, void* pValue, long lTime = 0);
	short	AddZero(int nCount = 1);	
	short	AddZeroValue(long lDepth, void* pValue, long lTime = 0, BYTE bMode = DATA_OFFSET);
	short	AddOffValue(long lDepth, long lTime);
	short	Append(long lDepth, double dValue, long lTime = 0);
	short	Append(CCurveData* pData, int nFrom, BOOL bEnd = FALSE);
	short	Append(CCurveData* pData);
	short   Replace(CCurveData* pData, long lDepth0, long lDepth1);
	void    Assign(CCurveData* pData, int nFrom, int nTo, BOOL bDepth);
	short	Attach(CCurveData* pData);
	void	Clear();
	void	Delete(int nIndex, int nCount = 1);
	short	InsertZero(int nIndex, int nCount = 1);
	short   Insert(int nInsert, long lDepth, double dValue, long lTime = 0);
	short	Insert(CCurveData* pData, int nFrom, int nInsert, BOOL bEnd = FALSE);
	short	Insert(CCurveData* pData);
	void	Reverse();
	int		SetDirection(int nDirection);
	short	Set(int nIndex, long lDepth, void* pValue, long lTime = 0);
		
	/* ----- 滤波 ------ */
	void	SetFilter(LPCTSTR strFilter);
	void	SetFilter(CULFilter* pFilter = NULL);

	/* ----- 取值/赋值 ----- */
	short	SetValue(int nIndex, void* pValue, VARTYPE vt = VT_R8);

	void	SaveValue(CFile& file, int nIndex);
    void	ReadValue(CFile& file, int nIndex);
	void	SaveData(CFile& file, int nIndex);
    void	ReadData(CFile& file, int nIndex);

	void*	GetCurSaveValuePtr() {	return GetValuePtr(m_nCurSavePos);	}
	void*	GetCurSaveValuePtr(int nOff) { return GetValuePtr(m_nCurSavePos + nOff); }
	BOOL	DeleteUseless(int nIndex);
	BOOL    CheckDepth(int nDirection);

	short	SetPointFrame(short nPointFrame)
	{
		if (nPointFrame < 1)
			return UL_ERROR;

		m_nPointFrame = nPointFrame;
		if ((nPointFrame > 1) && (m_nDimension < 2))
			m_nDimension = 2;

		m_nPointArray = m_nPointFrame*m_nArrayCount;
		return UL_NO_ERROR;
	}

	short GetPointFrame()
	{
		return m_nPointFrame;
	}

	short SetArrayCount(short nArrayCount)
	{
		if (nArrayCount < 1)
			return UL_ERROR;

		m_nArrayCount = nArrayCount;
		if ((nArrayCount > 1) && (m_nDimension < 3))
			m_nDimension = 3;
		m_nPointArray = m_nPointFrame*m_nArrayCount;
		return UL_NO_ERROR;
	}

	short GetArrayCount()
	{
		return m_nArrayCount;
	}

	long CalcInterval();
	long DepthInterval();
	
	BOOL SetReceiver(BYTE* pBuf, float* pRRDistance);
	BOOL GetReceiver(BYTE* pBuf, float* pRRDistance);
	BOOL SetSender(BYTE* pBuf, float* pSRDistance);
	BOOL GetSender(BYTE* pBuf, float* pSRDistance);

//#ifdef _RUNIT
//	void    CalcUnit(IUnits* pUnits, LPCTSTR pszPath);
	void    CalcUnit(CUnits* pUnits, LPCTSTR pszUType);
	short   CalcUnitValue(void* nIndex);
//#endif

	int	TestBound();
	/*------节箍识别-------*/
	void	MarkerMatch(long& lDepth, long& lTime, float fValue);

	void    AssignTime(CCurveData* pData, int nFrom, int nTo, BOOL bTime);

public:
	static int		m_nDataID;
	static BOOL		m_bOffValue;
	static float	m_fOffValue;// 偏移补值(浮点)
	static long		m_lOffValue;// 偏移补值(整形)
	static long		m_lFDDepth;	// 第一个深度点数据
	static double   m_fOffDelta;// 偏移插补深度间隔

private:
	short       m_nPointFrame;	// 每帧采样点数
	short		m_nArrayCount;	// 阵列数
	

public:
	int			m_nPointArray;	// 阵列中的点数 = m_nPointFrame*m_nArrayCount
	
	CPtrArrayEx	m_arrData;		// 数据链表
	CPtrArray	m_arrNew;		// 临时数据链表，用于环境校正
	int			m_nDimension;	// 数据维数
	VARTYPE		m_nValueType;	// 曲线值数据类型
	int			m_nMode;		// 曲线模式
	
	// ----- 数据帧 -----
	size_t		m_nSizePoint;	// 每个采样点长度
	int			m_nSamplesCount;// 采样帧数( + faster, - slower than depth)
	
	// ----- 滤波 -----
	BOOL		m_bFilter;		// 滤波
    CULFilter*	m_pFilter;		// 滤波器
	int			m_nFilterTempl;
	
	BOOL		m_bView;
	BOOL        m_bOpen; 
	// ----- 存盘 -----
	BOOL		m_bSave;
	int			m_nSaveSync;	// 异步采样同步
	int			m_nCurSavePos;	// 当前存盘索引

	// ----- 深度偏移 -----
	BOOL		m_bCalcOffset;	// 计算偏移
	BOOL		m_bDrawOffset;	// 显示偏移
	UINT		m_nPointIndex;	// 第一个点
	BOOL		m_nDirection;	// 测井方向

	BOOL		m_bDrawFirst;	// 是否第一时间绘制出曲线

	long		m_lCalcOffset;	// 计算后的最终曲线偏移(相对深度曲线)
	long		m_lDepthOffset;	// 曲线深度偏移
	long		m_lTBOffset;	// 曲线相对所属仪器底部偏移
	long 	    m_lToolOffset;	// 所属仪器深度偏移(相对底部)
	long		m_lToolLength;  // 所属仪器长度
    long		m_lMaxDepthOffset;	// 最大深度偏移
	BOOL		m_bAlignTopOffset;	// 顶部对齐
	long		m_lAllToolLength;	// 仪器串总长
	int			m_nOffsetMode;	// 偏移模式

	float		m_fFeedPoint;	// 每米采样点数
	long		m_lFirstDepth;	// 实时打印
	long		m_lFRDepth;		// FR Depth

	double		m_LeftBound;	// 左超差边界
	double		m_RightBound;	// 右超差边界
	UINT		m_nLeftOutNum;	// 左超差次数
	UINT		m_nRightOutNum;	// 右超差次数
	BOOL		m_bLeftOut;
	BOOL		m_bRightOut;

	// 波列特有数据
	long		m_lTimeDelay;	// 时间偏移
	UINT		m_iTimeInter;	// 时间间隔
	
	float		m_fRRDistance;	// 接收器间距离
	float		m_fSRDistance;  // 发射器与接收器间距离
	LPBYTE		m_pRecver;		// 接收器[24]
	LPBYTE		m_pSender;		// 发射器[24]

public:
	int			m_nID;			
	CCurve*		m_pSrcCurve;	
	vector_curve m_vecCurve;	
	CString		m_strName;		
	IPerforation* m_pITask;		

#ifdef _RUNIT
	CString		m_strTu;		
	BOOL		m_bRecUnit;		
	double		m_fK;			
	double		m_fB;			
	double		m_fAK;			
	double		m_fAB;			
#endif 

	CString		m_strUnit;		
	int			m_nDepthData;	
    CCriticalSection	m_CS;	
	CFile*  m_pPFile;
	CMPool	m_mpData;
	DWORD	m_cbData;
	int		m_nGrow;
	void MInit(int nGrowSize = szadd)
	{
		m_cbData = szcvt + m_nPointArray*m_nSizePoint;
		m_nGrow = min(nGrowSize, szadd);
		if (m_nGrow < 1)
			m_nGrow = szadd;
	}

#define MNexX(d, t, v, m) \
	LPBYTE pAddr = NULL;\
	DWORD dwIndex = m_mpData.GetCount();\
	if (dwIndex)\
	{\
		MBLOCK* pB = m_mpData.GetTail();\
		if ((pB->dwOff + m_cbData) <= pB->cbSize)\
		{\
			cv_t* pData = (cv_t*)(pB->pAddr + pB->dwOff);\
			pData->m_lDepth = d;\
			pData->m_lTime = t;\
			memcpy(&pData->m_Value, &v, sizeof(v));\
			pData->m_dwMode = m;\
			dwIndex--;\
			dwIndex *= m_nGrow;\
			dwIndex += pB->dwOff/m_cbData;\
			pB->dwOff += m_cbData;\
			return (void*)dwIndex;\
		}\
		try\
		{\
			m_pPFile->Seek(pB->posA, CFile::begin);\
			m_pPFile->Write(pB->pAddr, pB->cbSize);\
			pAddr = pB->pAddr;\
			pB->pAddr = NULL;\
		}\
		catch (...)\
		{\
			m_pPFile = NULL;\
		}\
	}\
	MBLOCK* pB = new MBLOCK;\
	if (m_pPFile == NULL)\
	{\
		CString strPF = GetTempFileNameSK();\
		try\
		{\
			m_pPFile = new CFile(strPF, CFile::modeCreate|CFile::modeReadWrite);\
		}\
		catch (CFileException* e)\
		{\
			e->Delete();\
			m_pPFile = NULL;\
		}\
		pB->posA = 0;\
	}\
	else\
		pB->posA = m_pPFile->SeekToEnd();\
	dwIndex *= m_nGrow;\
	m_mpData.AddTail(pB);\
	pB->cbSize = m_cbData*m_nGrow;\
	pB->pAddr = pAddr ? pAddr : new BYTE[pB->cbSize];\
	cv_t* pData = (cv_t*)(pB->pAddr);\
	pData->m_lDepth = d;\
	pData->m_lTime = t;\
	memcpy(&pData->m_Value, &v, sizeof(v));\
	pData->m_dwMode = m;\
	pB->dwOff = m_cbData;\
	return (void*)dwIndex;\

	inline void* MNew(long lDepth, long lTime, char value, DWORD mode = DATA_VALID)
	{
		MNexX(lDepth, lTime, value, mode);
	}

	inline void* MNew(long lDepth, long lTime, short value, DWORD mode = DATA_VALID)
	{
		MNexX(lDepth, lTime, value, mode);
	}

	inline void* MNew(long lDepth, long lTime, long value, DWORD mode = DATA_VALID)
	{
		MNexX(lDepth, lTime, value, mode);
	}

	inline void* MNew(long lDepth, long lTime, float value, DWORD mode = DATA_VALID)
	{
		MNexX(lDepth, lTime, value, mode);
	}

	inline void* MNew(long lDepth, long lTime, double value, DWORD mode = DATA_VALID)
	{
		MNexX(lDepth, lTime, value, mode);
	}

	inline void* MNew(long lDepth, long lTime, BYTE value, DWORD mode = DATA_VALID)
	{
		MNexX(lDepth, lTime, value, mode);
	}

	inline void* MNew(long lDepth, long lTime, WORD value, DWORD mode = DATA_VALID)
	{
		MNexX(lDepth, lTime, value, mode);
	}

	inline void* MNew(long lDepth, long lTime, DWORD value, DWORD mode = DATA_VALID)
	{
		MNexX(lDepth, lTime, value, mode);
	}

	void* MNew(long lDepth, long lTime, void* pValue, DWORD dwMode = DATA_VALID)
	{
		LPBYTE pAddr = NULL;
		DWORD dwIndex = m_mpData.GetCount();
		if (dwIndex)
		{
			MBLOCK* pB = m_mpData.GetTail();
			if ((pB->dwOff + m_cbData) <= pB->cbSize)
			{
				cv_t* pData = (cv_t*)(pB->pAddr + pB->dwOff);
				pData->m_lDepth = lDepth;
				pData->m_lTime = lTime;
				memcpy(&pData->m_Value, pValue, GetValueSize());
				pData->m_dwMode = dwMode;
				dwIndex--;
				dwIndex *= m_nGrow;
				dwIndex += pB->dwOff/m_cbData;
				pB->dwOff += m_cbData;
				return (void*)dwIndex;
			}
			
			try
			{
				m_pPFile->Seek(pB->posA, CFile::begin);
				m_pPFile->Write(pB->pAddr, pB->cbSize);
				pAddr = pB->pAddr;
				pB->pAddr = NULL;
			}
			catch (...)
			{
				m_pPFile = NULL;
			}
		}

		MBLOCK* pB = new MBLOCK;
		if (m_pPFile == NULL)
		{
			CString strPF = GetTempFileNameSK();
			try
			{
				m_pPFile = new CFile(strPF, CFile::modeCreate|CFile::modeReadWrite);
			}
			catch (CFileException* e)
			{
				e->Delete();
				m_pPFile = NULL;
			}
			pB->posA = 0;
		}
		else
			pB->posA = m_pPFile->SeekToEnd();
		
		dwIndex *= m_nGrow;
		m_mpData.AddTail(pB);
		pB->cbSize = m_cbData*m_nGrow;
		pB->pAddr = pAddr ? pAddr : new BYTE[pB->cbSize];
		cv_t* pData = (cv_t*)(pB->pAddr);
		pData->m_lDepth = lDepth;
		pData->m_lTime = lTime;
		memcpy(&pData->m_Value, pValue, GetValueSize());
		pData->m_dwMode = dwMode;
		pB->dwOff = m_cbData;
		return (void*)dwIndex;
	}

	void* MNew(void* pcv)
	{
		LPBYTE pAddr = NULL;
		DWORD dwIndex = m_mpData.GetCount();
		if (dwIndex)
		{
			MBLOCK* pB = m_mpData.GetTail();
			if ((pB->dwOff + m_cbData) <= pB->cbSize)
			{
				cv_t* pData = (cv_t*)(pB->pAddr + pB->dwOff);
				memcpy(pData, pcv, m_cbData);
				pData->m_dwMode = DATA_VALID;
				dwIndex--;
				dwIndex *= m_nGrow;
				dwIndex += pB->dwOff/m_cbData;
				pB->dwOff += m_cbData;
				return (void*)dwIndex;
			}
			
			try
			{
				m_pPFile->Seek(pB->posA, CFile::begin);
				m_pPFile->Write(pB->pAddr, pB->cbSize);
				pAddr = pB->pAddr;
				pB->pAddr = NULL;
			}
			catch (...)
			{
				m_pPFile = NULL;
			}
		}
		
		MBLOCK* pB = new MBLOCK;
		ZeroMemory(pB, sizeof(MBLOCK));
		if (m_pPFile == NULL)
		{
			CString strPF = GetTempFileNameSK();
			try
			{
				m_pPFile = new CFile(strPF, CFile::modeCreate|CFile::modeReadWrite);
			}
			catch (CFileException* e)
			{
				e->Delete();
				m_pPFile = NULL;
			}
			pB->posA = 0;
		}
		else
			pB->posA = m_pPFile->SeekToEnd();
		
		dwIndex *= m_nGrow;
		m_mpData.AddTail(pB);
		pB->cbSize = m_cbData*m_nGrow;
		pB->pAddr = pAddr ? pAddr : new BYTE[pB->cbSize];
		cv_t* pData = (cv_t*)(pB->pAddr);
		memcpy(pData, pcv, m_cbData);
		pData->m_dwMode = DATA_VALID;
		pB->dwOff = m_cbData;
		return (void*)dwIndex;
	}

	void* MNewZ(void* pcv = NULL, int index = -1)
	{
		LPBYTE pAddr = NULL;
		DWORD dwIndex = m_mpData.GetCount();
		if (dwIndex)
		{
			MBLOCK* pB = m_mpData.GetTail();
			if ((pB->dwOff + m_cbData) <= pB->cbSize)
			{
				cv_t* pData = (cv_t*)(pB->pAddr + pB->dwOff);
				if (pcv != NULL)
					memcpy(pData, pcv, szcvt);
				pData->m_dwMode = DATA_VALID;
				dwIndex--;
				dwIndex *= m_nGrow;
				dwIndex += pB->dwOff/m_cbData;
				pB->dwOff += m_cbData;
				if (index < 0)
					m_arrData.Add((void*)dwIndex);
				else
					m_arrData.InsertAt(index, (void*)dwIndex);
				return pData;
			}
			
			try
			{
				m_pPFile->Seek(pB->posA, CFile::begin);
				m_pPFile->Write(pB->pAddr, pB->cbSize);
				pAddr = pB->pAddr;
				pB->pAddr = NULL;
			}
			catch (...)
			{
				m_pPFile = NULL;
			}
		}
		
		MBLOCK* pB = new MBLOCK;
		if (m_pPFile == NULL)
		{
			CString strPF = GetTempFileNameSK();
			try
			{
				m_pPFile = new CFile(strPF, CFile::modeCreate|CFile::modeReadWrite);
			}
			catch (CFileException* e)
			{
				e->Delete();
				m_pPFile = NULL;
			}
			pB->posA = 0;
		}
		else
			pB->posA = m_pPFile->SeekToEnd();
		
		dwIndex *= m_nGrow;
		m_mpData.AddTail(pB);
		pB->cbSize = m_cbData*m_nGrow;
		pB->pAddr = pAddr ? pAddr : new BYTE[pB->cbSize];
		
		//将新分配的内存置0
		if(pAddr == NULL && pB->cbSize)
		{
			ZeroMemory(pB->pAddr , pB->cbSize);
		}

		cv_t* pData = (cv_t*)(pB->pAddr);
		if (pcv != NULL)
			memcpy(pData, pcv, szcvt);
		pData->m_dwMode = DATA_VALID;
		pB->dwOff = m_cbData;
		if (index < 0)
			m_arrData.Add((void*)dwIndex);
		else
			m_arrData.InsertAt(index, (void*)dwIndex);
		return pData;
	}

	void MSave()
	{
		if (m_pPFile == NULL)
			return;

		POSITION pos = m_mpData.GetHeadPosition();
		if (pos == NULL)
			return;
		
		for (;;)
		{
			MBLOCK* pB = m_mpData.GetNext(pos);
			if (pos != NULL)
			{
				if (pB->pAddr)
				{
					try
					{	
						m_pPFile->Seek(pB->posA, CFile::begin);
						m_pPFile->Write(pB->pAddr, pB->cbSize);
						delete pB->pAddr;
						pB->pAddr = NULL;
					}
					catch (CFileException* e)
					{
						e->Delete();
						m_pPFile = NULL;
					}
				
				}
			}
			else
				break;
		}
	}
	
	void MRelease()
	{
		if (m_pPFile == NULL)
			return;

		POSITION pos = m_mpData.GetHeadPosition();
		if (pos == NULL)
			return;
		
		m_CS.Lock();
		for (;;)
		{
			MBLOCK* pB = m_mpData.GetNext(pos);
			if (pos != NULL)
			{
				if (pB->pAddr)
				{
					delete pB->pAddr;
					pB->pAddr = NULL;
				}
			}
			else
				break;
		}
		m_CS.Unlock();
	}

	void MDelete()
	{
		while (m_mpData.GetCount())
		{
			MBLOCK* pB = m_mpData.RemoveHead();
			delete pB->pAddr;
			delete pB;
		}

		if (m_pPFile != NULL)
		{
//			CString strFile = m_pPFile->GetFileName();
			CString strFile = m_pPFile->GetFilePath();
			delete m_pPFile;
			m_pPFile = NULL;
			::DeleteFile(strFile);
		}
	}

	short	AddB(long lDepth, void* pValue, long lTime = 0);

// 2022.09.14 Start
	int m_iPointCount;// 源数据值点数
	long m_lProInterval;	// 处理间隔
	float m_fProIntValue;	// 处理间隔值
// 2022.09.14 End
};

//////////////////////////////////////////////////////////////////////
//
// Curve Value Template Constructors
//
//////////////////////////////////////////////////////////////////////

template<class vType>
CCurveData::CCurveValue<vType>::CCurveValue()
{
    m_lDepth = 0;
	m_lTime = 0;
    m_dwMode = DATA_VALID;
}

template<class vType>
CCurveData::CCurveValue<vType>::CCurveValue(unsigned int cElements)
{
    m_lDepth = 0;
	m_lTime = 0;
	ZeroMemory(&m_Value, sizeof(vType)*cElements);	
    m_dwMode = DATA_VALID;
}

template<class vType>
CCurveData::CCurveValue<vType>::CCurveValue(CCurveValue<vType>& src, unsigned int cElements)
{
	m_dwMode = DATA_VALID;
    m_lDepth = src.m_lDepth;
    m_lTime = src.m_lTime;
    memcpy(&m_Value, &src.m_Value, sizeof(vType)*cElements);	
}

template<class vType>
CCurveData::CCurveValue<vType>::CCurveValue(long lDepth, long lTime, vType value)
{
	m_dwMode = DATA_VALID;
	m_lDepth = lDepth;
	m_lTime = lTime;
	memcpy(&m_Value, &value, sizeof(vType));
}

template<class vType>
CCurveData::CCurveValue<vType>::CCurveValue(long lDepth, long lTime, vType* value, unsigned int cElements)
{
	m_lDepth = lDepth;
	m_lTime = lTime;
	memcpy(&m_Value, value, sizeof(vType)*cElements);
	m_dwMode = DATA_VALID;
}

template<class vType>
CCurveData::CCurveValue<vType>::~CCurveValue()
{
}

template<class vType>
void CCurveData::CCurveValue<vType>::assign(CCurveValue<vType>& src, unsigned int cElements)
{
    m_lDepth = src.m_lDepth;
    m_lTime = src.m_lTime;
    memcpy(&m_Value, &src.m_Value, sizeof(vType)*cElements);
}

template<class vType>
void CCurveData::CCurveValue<vType>::assign(long lDepth, long lTime, vType value)
{
	m_lDepth = lDepth;
	m_lTime = lTime;
	m_Value = value;
}

template<class vType>
void CCurveData::CCurveValue<vType>::assign(long lDepth, long lTime, vType* value, unsigned int cElements)
{
	m_lDepth = lDepth;
	m_lTime = lTime;
	memcpy(&m_Value, value, sizeof(vType)*cElements);	
}

template<class vType>
void CCurveData::CCurveValue<vType>::swap(CCurveValue<vType>& src, unsigned int cElements)
{
    CCurveValue<vType> cv(src, cElements);
	src.assign(*this, cElements);
	assign(cv, cElements);
}

#endif // !defined(AFX_CURVEDATA_H__A51AF60A_89E6_4320_BFEB_6F8E952212F8__INCLUDED_)

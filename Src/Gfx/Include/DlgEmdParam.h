#if !defined(AFX_DLGEMDPARAM_H__DA891D09_C966_4AFF_B11B_3BF76CB5B8D9__INCLUDED_)
#define AFX_DLGEMDPARAM_H__DA891D09_C966_4AFF_B11B_3BF76CB5B8D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "BCGPListCtrl.h"
// DlgEmdParam.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgEmdParam dialog
class CULEmd;
class CGraphWnd;
class CDlgEmdParam : public CDialog
{
// Construction
public:
	CDlgEmdParam(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgEmdParam)
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_EMENDATION_PARAM };
#endif
	CListBox	m_wndDepth;
	CBCGPListCtrl m_wndList;
	//}}AFX_DATA

	enum { pid, pname, ppvalue, pvalue, punit, pcomment, cols};
	CULEmd* m_pULEmd;
	CGraphWnd* m_pGraph;
	int m_iSel;
	long m_lDepthF0;
	long m_lDepthF1;
	void SetDepth(long lDepth, BOOL bStart = TRUE);
	void UpdateDepths(long* pDepths, int nCount);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgEmdParam)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgEmdParam)
	virtual BOOL OnInitDialog();
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	afx_msg void OnOk();
	afx_msg void OnButton4();
	afx_msg void OnButton5();
	afx_msg void OnButton6();
	afx_msg void OnButton7();
	afx_msg void OnButton8();
	afx_msg void OnDblclkList2();
	afx_msg void OnSelchangeList2();
	afx_msg void OnDestroy();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGEMDPARAM_H__DA891D09_C966_4AFF_B11B_3BF76CB5B8D9__INCLUDED_)


#if !defined (_DATADEFINE_H)
#define _DATADEFINE_H

#include "afxtempl.h"



#define		HASH_PARAM		256				//HASH系数
#define     INVALID_VALUE   0XFFFFFFFF

//索引文件头结构
typedef struct  _tagIndexFileHead
{
	char     strFileIdentify[21] ; 		//文件标识"UL2000 INFO HASH FILE"
	char     strFileVersion[4] ;		//文件版本号
	char     strModiTime[8]; 			//最近修改时间
	DWORD	 dwDelNodeCount ;			//删除结点数
}INDEXFILEHEAD;


//HASH地址信息结构：
typedef  struct  _tagAddressStruct
{
	DWORD    dwNodeInfoOffset;; 			//该结点信息偏移

}HASHADDRESSSTRUCT;

//目录信息结构：
typedef   struct _tagNodeStruct
{
	DWORD				dwHashValue ;				//本结点HASHVALUE
	HASHADDRESSSTRUCT   HASHADDRESS[HASH_PARAM];	//本结点HASH表
	WORD 				wStrNodeLen ;			    //位于该地址结点字符串长度（最大32）
	char				strNodeName[32];		    //位于该地址结点名（最长32）
	WORD				wDataItemCount ; 			//数据项个数
	DWORD			    dwFirstDataItemOff ;		//第一个数据项偏移

	_tagNodeStruct()
	{
		//memset(HASHADDRESS , 0XF, sizeof(HASHADDRESSSTRUCT));
		for(int i = 0 ; i  < HASH_PARAM ; i ++)
			HASHADDRESS[i].dwNodeInfoOffset = INVALID_VALUE;
		dwHashValue		= 0;
		wStrNodeLen		= 0 ;
		memset(strNodeName, 0, 32);	
		wDataItemCount		 = 0 ; 		
		dwFirstDataItemOff  = INVALID_VALUE;	
	}

}NODESTRUCT;

//数据文件头
typedef   struct   _tagDataFileHead
{
	char     strFileIdentify[21] ; 		//文件标识"UL2000 INFO DATA FILE"
	char     strFileVersion[4] ;			//文件版本号
	char     strModiTime[8]; 				//最近修改时间
	DWORD	  dwDelBlockCount ;		// 删除块个数

	_tagDataFileHead()
	{
		strncpy(strFileIdentify , "UL2000 INFO DATA FILE", 21);
		strncpy(strFileVersion, "1.00", 4);
		strncpy(strModiTime , "20020808", 8);
		dwDelBlockCount = 0 ;
	}
}DATAFILEHEAD ; 


typedef   struct   _tagDataItemInfo
{
	char     flag[4];			//数据块开始标识
	WORD 	 ItemNameLen;		 	//数据项名称长度
	char     ItemName[32];		//数据项名(不超过32字节)
	DWORD	 dwParentOff ;			//父结点偏移
	DWORD	 dwChildOff ;				//子结点偏移
	DWORD	 dwDataBlockSize ;		//数据块大小

	_tagDataItemInfo()
	{
		strncpy(flag , "DATA", 4);
		ItemNameLen = 0 ;
		memset(ItemName , 0, 32);
		dwParentOff = INVALID_VALUE;
		dwChildOff = INVALID_VALUE;
		dwDataBlockSize = 0 ;
	}
}DATAITEMINFO;

typedef struct   _tagDataItem 
{
	DATAITEMINFO 		dataItemInfo;	
	BYTE 	*  pDataBlock ;		//数据块指针
	
	_tagDataItem ()
	{
		pDataBlock = NULL;
	}
	~_tagDataItem()
	{
		if(pDataBlock != NULL)
		{
			delete pDataBlock ;
			pDataBlock = NULL;
		}
	}

}DATAITEM;

typedef	CArray <DATAITEM , DATAITEM &> DATAITEM_ARRAY ;	

#endif
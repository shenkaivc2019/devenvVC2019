#pragma once

#ifndef GFXHARDWARE_API
	#ifdef GFXHARDWARE_EXPORT
		#define GFXHARDWARE_API __declspec( dllexport )
	#else	
		#define GFXHARDWARE_API __declspec( dllimport )
	#endif
#endif
class GFXHARDWARE_API CHardWareInfo
{
public:
	CHardWareInfo();
	virtual ~CHardWareInfo();
	bool IsRegister(char* pcMachine = nullptr);
	bool GetRegNo(char* pcMachine = nullptr, char* pcRegNo = nullptr);
	bool GenMachine(char* pcid1 = nullptr, char* pcid2 = nullptr, char* pcMachine = nullptr);
private:
	bool GetCpuID(char* pcCpuID/* = nullptr*/);
	bool GetDiskID(char* pcDiskID/* = nullptr*/);
};

GFXHARDWARE_API CHardWareInfo* AfxGetHardWareInfo();
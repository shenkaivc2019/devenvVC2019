﻿#if !defined(AFX_WELLPROPSHEET_H__57958FE4_E163_4640_A7CA_2AD86E87C069__INCLUDED_)
#define AFX_WELLPROPSHEET_H__57958FE4_E163_4640_A7CA_2AD86E87C069__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ULCOMMDEF.h"
// 2020.2.10 Ver1.6.0 TASK【002】 Start
#include "PropertyPageWellsite.h"
#include "PropertyPageWell.h"
#include "PropertyPageHole.h"
#include "PropertyPageRun.h"
#include "WizardStepOne.h"
//#include "WizardWellPage.h"
//#include "WizardDrillPage.h"
//#include "WizardMudPage.h"
//#include "WizardRunPage.h"
//#include "WizardTeamPage.h"

/*
#ifdef _PERFORATION
#include "PerforationPropDataEdit.h"
#endif*/

// WellPropSheet.h : header file
//
#define PAGE_PROJECT		0
#define PAGE_WELLSITE		1
#define PAGE_WELL			2
#define PAGE_HOLE			3
#define	PAGE_RUN			4
// 2020.2.10 Ver1.6.0 TASK【002】 End
#ifndef BASEINFO_API
	#ifdef BASEINFO‌_EXPORT
		#define BASEINFO_API __declspec( dllexport )
	#else	
		#define BASEINFO_API __declspec( dllimport )
	#endif
#endif
/////////////////////////////////////////////////////////////////////////////
// CWellPropSheet

class BASEINFO_API CWellPropSheet : public CPropertySheet
{
	DECLARE_DYNAMIC(CWellPropSheet)

// Construction
public:
	CWellPropSheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CWellPropSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);

// Attributes
public:
// 2020.2.10 Ver1.6.0 TASK【002】 Start
	CWizardStepOne*				m_pPageProjectInfo;
	CPropertyPageWellsite*		m_pPageWellsiteInfo;
	CPropertyPageWell*			m_pPageWellInfo;
	CPropertyPageHole*			m_pPageHoleInfo;
	CPropertyPageRun*			m_pPageRunInfo;
//	CWizardWellPage			m_pageWellField;	// 井场页签
//	CWizardDrillPage		m_pageDrilling;		// 钻井页签
//	CWizardMudPage			m_pageMud;			// 泥浆页签
//	CWizardRunPage			m_pageRun;			// 泥浆页签
//	CWizardTeamPage			m_pageTeam;			// 小队页签
//#ifdef _PERFORATION
//	CPerforationPropHeader	m_PagePerforationInfo;   // 射孔信息
//#endif
	
//	CWizardTimePage			m_pageDrillingTime;	// 时间页签
	
//	CWizardDrillerPage		m_pageDriller;		// 钻头页签
//	CWizardCasedPage		m_pageCasedHole;	// 套管页签
	

//	CWizardCaptainPage		m_pageCaptain;		// 队长页签
//	CWizardOperatorPage		m_pageOperator;		// 操作员页签
//	CWizardEngineerPage		m_pageEngineer;		// 工程师页签
//	CWizardWitnessPage		m_pageWitness;		// 监督员页签
// 2020.2.10 Ver1.6.0 TASK【002】 End
	
// Operations
public:
	void InitSheet();
// 2020.2.10 Ver1.6.0 TASK【002】 Start
	void AddSheetPage(DWORD dwParam , DWORD dwType);
// 2020.2.10 Ver1.6.0 TASK【002】 End
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWellPropSheet)
	public:
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CWellPropSheet();

	// Generated message map functions
public:
	virtual BOOL OnInitDialog();
protected:
	//{{AFX_MSG(CWellPropSheet)
	afx_msg void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WELLPROPSHEET_H__57958FE4_E163_4640_A7CA_2AD86E87C069__INCLUDED_)

//	dibtiff.h
//

#ifndef DIB_TIFF_IMG_NCLUDE_H__
#define DIB_TIFF_IMG_NCLUDE_H__

#include "dibsect.h"
#include <string>
#include <arraycontainer.h>

class TIFFSubImage {
	void InitImg(void)
	{
		m_width = m_height = m_ndx = m_bpp = 0;
		m_name = "";
	}
protected:
	UINT32 m_width;
	UINT32 m_height;
	UINT32 m_ndx;
	UINT32 m_bpp;
	std::string m_name;
public:
	TIFFSubImage(void){ InitImg();}
	TIFFSubImage(const TIFFSubImage& img){ InitImg(); Copy(img);}
	TIFFSubImage(UINT32 w, UINT32 h, UINT32 ndx, UINT32 bpp, const char * name)
	{
		CreateSubImage(w,h,ndx,bpp,name);
	}
	~TIFFSubImage(void){}
	UINT32 GetWidth(void)const{ return m_width;}
	UINT32 GetHeight(void)const{ return m_height;}
	UINT32 GetIndex(void)const{ return m_ndx;}
	UINT32 GetBitsPerSample(void)const{ return m_bpp;}
	const char * GetName(void)const{ return m_name.c_str();}
	void CreateSubImage(UINT32 w, UINT32 h, UINT32 ndx, UINT32 bpp, const char * name)
	{
		if ((w > 0) && (h > 0))
		{
			m_width = w;
			m_height = h;
			m_ndx = ndx;
			m_bpp = bpp;
			m_name = name;
		}
	}
	void Copy(const TIFFSubImage& img)
	{
		InitImg();
		if (img.IsCreated())
		{
			m_width = img.GetWidth();
			m_height = img.GetHeight();
			m_ndx = img.GetIndex();
			m_bpp = img.GetBitsPerSample();
			m_name = img.GetName();
		}
	}
	TIFFSubImage& operator = (const TIFFSubImage& img)
	{
		Copy(img);
		return *this;
	}
	bool IsCreated(void)const{ return (m_width>0);}
};

int OpenTIFF2DIB(const char * filename, DIBSection& dib, int directory = 0);
int SaveDIB2TIFF(const char * output_file, DIBSection& dib);
int OpenBigTIFF2DIB(const char * lpFileName, DIBSection& dib, int directory = 0);
int ReadTIFFDirectories(const char * filename, ArrayContainer<TIFFSubImage>& images);
void ConvertToPDF(const char * tiffName, const char * pdfName);
void GetTIFFProperties(const char * tiffName, UINT32 subImage,
					   ArrayContainer<std::string>& properties);

#endif

// GfxPageCurves.h: interface for the CGfxPageCurves class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GFXPAGECURVES_H__15025B7A_F71D_4A2A_8946_8A18F6A90F48__INCLUDED_)
#define AFX_GFXPAGECURVES_H__15025B7A_F71D_4A2A_8946_8A18F6A90F48__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "GfxProperty.h"
#include "GfxPageTrack.h"
#include "GfxPageGrid.h"
#include "GfxPageCommon.h"
#include "GfxPageImage.h"
#include "GfxPageFill.h"
#include "GfxPageWave.h"
#include "GfxPageVolume.h"
#include "GfxPageMark.h"
#include "GfxPage.h"

class CGfxPageCurves : public CGfxPage  
{
public:
	CGfxPageCurves(CWnd* pParent = NULL);
	virtual ~CGfxPageCurves();
	// Dialog Data
	//{{AFX_DATA(CGfxPageCurves)
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_GFX_PAGE_CURVES };
#endif
	CComboBox	m_cbCurveType;
	CListBox	m_lbCurveList;
	//}}AFX_DATA
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGfxPageCurves)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
protected:
	// Generated message map functions
	//{{AFX_MSG(CGfxPageCurves)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeListCurve();
	afx_msg void OnSelchangeCurveType();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void	InitCurve(CCurve *pCurve);
	void	InitCurveType();
	void	AdjustLayout(CCurve* pCurve);
    
public:
	static 	CGfxPageCommon	m_pageCommon;	// 常规属性控制
	static 	CGfxPageImage	m_pageImage;	// 图象属性控制
    static  CGfxPageFill	m_pageFill;		// 填充属性控制
	static  CGfxPageWave	m_pageWave;		// 波列属性控制
	static  CGfxPageVolume  m_pageVolume;   // 体积属性控制
protected:
	CGfxPage*	m_pActivePage; //
};

#endif // !defined(AFX_GFXPAGECURVES_H__15025B7A_F71D_4A2A_8946_8A18F6A90F48__INCLUDED_)

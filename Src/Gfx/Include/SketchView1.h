#if !defined(AFX_CONSTRUCTVIEW_H__6932A42B_698B_4E30_9320_D2F7928F182F__INCLUDED_)
#define AFX_CONSTRUCTVIEW_H__6932A42B_698B_4E30_9320_D2F7928F182F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ULFormView.h"
#include "ULCOMMDEF.h"

class CCurve;

typedef int ERROR_CODE ;
#define ERROR_INVALID_BIT			1
#define ERROR_INVALID_CASING		2
#define ERROR_INVALID_CASINGDEPTH	3
#define OK							4
#define ERROR_NODATA				8

class CVIWWell;
class CSketchView : public CULFormView
{
	DECLARE_DYNAMIC(CSketchView)
public:
	CSketchView(LPVOID pWellPI = NULL);	// constructor used by dynamic creation
	virtual ~CSketchView();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSketchView)
	protected:
	virtual void OnUpdate();			// overridden to update this view
	//}}AFX_VIRTUAL

// Implementation
protected:

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CSketchView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	afx_msg void OnDataChangedVIWWELL1CTRL1();
	DECLARE_MESSAGE_MAP()

public:
	virtual int CalcPrintHeight();
	virtual void LoadTempl(LPCTSTR pszFile);
	virtual void SaveAsTempl(LPCTSTR pszFile);
	virtual void SaveService();
	
public:
	BOOL	m_bUpdate;
	CProject*	m_pWellPI;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONSTRUCTVIEW_H__6932A42B_698B_4E30_9320_D2F7928F182F__INCLUDED_)

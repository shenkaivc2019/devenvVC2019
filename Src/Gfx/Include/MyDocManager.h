// MyDocManager.h: interface for the CMyDocManager class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MYDOCMANGER_H__51A6C8B5_767A_4241_A839_A7DC94CEECED__INCLUDED_)
#define AFX_MYDOCMANGER_H__51A6C8B5_767A_4241_A839_A7DC94CEECED__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CMyDocManager : public CDocManager  
{
public:
	CMyDocManager();
	virtual ~CMyDocManager();
	
	// override for open/save dialog
	virtual BOOL DoPromptFileName(CString& fileName, UINT nIDSTitle,
			DWORD lFlags, BOOL bOpenFileDialog, CDocTemplate* pTemplate);
};

#endif // !defined(AFX_MYDOCMANGER_H__51A6C8B5_767A_4241_A839_A7DC94CEECED__INCLUDED_)

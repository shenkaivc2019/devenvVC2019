//---------------------------------------------------------------------------//
//     GfxComFunc.h
//     共通函数一些最基本的函数集合
//     2007/07/01
//       
//---------------------------------------------------------------------------//
#if !defined(_GFXCOMFUNC_H)
#define _GFXCOMFUNC_H

#undef AFX_DATA
#define AFX_DATA AFX_EXT_DATA

void APIENTRY GfxLoadString(CString& strBuf, UINT IDPrompt);
// 打开文件内存映射
BOOL APIENTRY OpenFileMapping(LPCTSTR lpszMapName, LPVOID lpvInstance, LPVOID& lpvMem, size_t uiShareMemSize);
// 消除内存映射
BOOL APIENTRY UnmapViewOfFile(LPCTSTR lpszMapName,LPVOID lpvMem);

/*******************
字符处理函数
*********************/
CString APIENTRY GfxDllLoadString(HINSTANCE dllhInstance, UINT uID); // 根据DLL的HINSTANCE和UID,取得DLL中指定资源ID的字符
void APIENTRY ReplaceText(CString& str,LPCTSTR lpszOld, LPCTSTR lpszNew); // 替换文本
CString APIENTRY GetNextCode(LPCSTR lpszStart,LPCSTR lpszFormat,DATE date = NULL); // 根据起始编码,按照指定的格式进行编码(计算)  lpszStart : 起始编码 NULL时返回第一个值 lpszFormat : 编码格式 格式:  #---数字(变长)  9---数字(定长)     A---A~Z   Y---年 M---月 D---日
BOOL APIENTRY StrInArray(CStringArray& ar,LPCSTR text); // 判断字符串是否是字符串数组中的无素
BOOL APIENTRY DWordInArray(CDWordArray& adw,DWORD dw); // 判断DWORD是否是DWORD数组中的无素
void APIENTRY SplitStr(CStringArray& astr,LPCSTR lpszSource, char cSeparator); // 把用cSeparator分割的字符串lpszSource拆开并形成字符串数组astr
void APIENTRY SplitStr(CStringArray& ar,LPCTSTR lpszSource,LPCSTR lpszSeparator); // 把用lpszSeparator分割的字符串lpszSource拆开并形成字符串数组astr
CString APIENTRY ArrayToStr(CStringArray& astr,char cSeparator); // 将字符串数组astr变成用cSeparator分割的字符串
void APIENTRY ArrayToMap(CMapStringToString& map, const CStringArray& astr); // 字符串数组转成字符串映射表
void APIENTRY MapToArray(CStringArray& astr, const CMapStringToString& map); // 字符串映射表转成字符串数组

CString APIENTRY TruncStr(CString& strValue,char cValue); // 截掉字符串的尾部指定字符
CString APIENTRY TruncDouble(CString& strValue); // 截掉数字尾部0(及小数点)

/*******************
对文件进行操作的函数
*********************/
BOOL APIENTRY FindFile(LPCTSTR path,LPCSTR name,CString& full_name); // 在路径lpszPath下搜索名为lpszName的第一个文件,找到返回TRUE,strFull_name为全路径名;否则返回FALSE
void APIENTRY FindFiles(LPCSTR path,LPCSTR ext,CStringArray& ar); // 在指定lpszPath下搜索扩展名为lpszExt的所有文件名
BOOL APIENTRY IsFileExist(LPCTSTR path); // 判断文件是否存在

/*******************
对COM控件进行操作的函数
*********************/
BOOL APIENTRY RegisterControl(char *fname); // 注册指定的控件
BOOL APIENTRY CLSIDExist(CLSID &clsid, LPCTSTR lpszTootPath); // 根据CLSID判断控件是否存在
/*******
路径函数
********/
CString APIENTRY GetRelativePath(LPCTSTR src_path,LPCTSTR dest_path); // 返回相对路径
CString APIENTRY GetAbsolutePath(LPCTSTR rela_path,LPCTSTR dest_path); // 根据相对路径返回绝对路径
/*******
转换函数
********/
CString APIENTRY VariantToStr(const VARIANT& vt); // Variant转换成字符串
void APIENTRY StrToVariant(VARIANT& vt,LPCTSTR lpszText); // 字符串转换成Variant 
CString APIENTRY DecimalToStr(DECIMAL dc); // DECIMAL转换成字符串
DECIMAL APIENTRY StrToDecimal(LPCSTR lpsz); // 字符串转换成DECIMAL
double APIENTRY DecimalToDouble(DECIMAL dc); // DECIMAL转换成Double
CString APIENTRY DoubleToStr(double dVal,LPCTSTR lpszFormat); // Double开变量转换成字符串
CString APIENTRY IntToStr(int iVal,LPCTSTR lpszFormat); // 整型按指定格式转换成字符类型
CString APIENTRY DateToStr(DATE date,LPCTSTR lpszFormat = NULL); // DATE类型的日期转换成指定格式的字符串类型的日期
DATE APIENTRY StrToDate(LPCTSTR lpszDate); // 字符串类型的日期转换成DATE类型的日期
CString APIENTRY UpperCurrency(double dValue, int iSty); // double型的变量转换成大写金额汉字字符串
CString	APIENTRY GetCnUpperStr(CString strInput); // 将输入的数字转换为汉字（不含进位信息）
CString	APIENTRY NumToCnUpper(CString strInput); // 将输入的数字转换为汉字（含进位信息）

double APIENTRY Round(LPCSTR lpszD,int iLen); // 将一浮点行数按指定精度四舍五入
void APIENTRY SortArray(CStringArray& astr); // 字符串数组排序
CString APIENTRY FormatVariant(const VARIANT vt,LPCTSTR lpszFormat); // 
CString APIENTRY AddFixToList(LPCSTR lpszStr,LPCSTR lpszT2,BOOL bPre = TRUE); // 在字符串列表上分别加上前缀或后缀
CString APIENTRY MakeEqualStr(LPCSTR lpszStr1, LPCSTR lpszStr2, LPCSTR lpszOpt); // 根据两个字符串列表形成比较条件字符串
CString APIENTRY ToPyHead(LPCSTR src,int size); // 把汉字变为拼音字头
CString APIENTRY ToPyHead(CString& str); // 把汉字变为拼音字头

BOOL APIENTRY IsTextType(UINT uVt);
CString APIENTRY GetTypeName(UINT uVt);

COLORREF APIENTRY TranslateColor(OLE_COLOR clrColor); // 取得指定颜色的亮色
COLORREF APIENTRY ReversalColor(COLORREF clrValue); // 取得指定颜色的返转色
CString GetSerialNo(LPCSTR name,UINT productID); // 取产品序列号
CString APIENTRY GetCurDate(); // 取得字符串形式的当前日期
/*******
编码解码函数
********/
CString APIENTRY Encode(CString& strSrc); // 编码(BCD)
CString APIENTRY Decode(CString& strSrc); // 解码(BCD)
/*******
系统处理函数
********/
int APIENTRY CompareVariant(const VARIANT& vt1,const VARIANT& vt2); // 比较两个VARIANT变量
void APIENTRY CopyVariant(VARIANT& v1,const VARIANT& v2); // 复制VARIANT类型的变量
VARIANT APIENTRY VariantPlus(const VARIANT& v1,const VARIANT& v2);
CString	APIENTRY ChangeToSqlFormat(VARIANT v,LPCSTR db); // 转换成SQL格式,如null,'a',convert(datetime,'2001/01/01',111)...

//void APIENTRY ThrowGoeasyException(LPCTSTR err);

BOOL IsNT();
void LPtoDP(CDC* pDC,LPPOINT lppt);
void LPtoDP(CDC* pDC,LPRECT lprect);
void LPtoDP(CDC* pDC,LPSIZE lpsize) ;
void DPtoLP(CDC* pDC,LPPOINT lppt);
void DPtoLP(CDC* pDC,LPRECT lprect);
void DPtoLP(CDC* pDC,LPSIZE lpsize) ;

void Win98toNT(LPPOINT lppt,UINT map_mode);
void Win98toNT(LPRECT lprect,UINT map_mode);
void Win98toNT(LPSIZE lpsize,UINT map_mode) ;
void NTtoWin98(LPPOINT lppt,UINT map_mode);
void NTtoWin98(LPRECT lprect,UINT map_mode);
void NTtoWin98(LPSIZE lpsize,UINT map_mode);

//////////////////////////////////////////////////////////////////////
// The following is implemented by MFC
UINT AFXAPI AfxGetFileTitle(LPCTSTR lpszPathName, LPTSTR lpszTitle, UINT nMax);
void AFXAPI AfxGetModuleShortFileName(HINSTANCE hInst, CString& strShortName);

BOOL AFXAPI AfxFullPath(LPTSTR lpszPathOut, LPCTSTR lpszFileIn);
BOOL AFXAPI AfxResolveShortcut(CWnd* pWnd, LPCTSTR pszShortcutFile,
							   LPTSTR pszPath, int cchPath);

// Register Function
BOOL AfxSetRegKey(LPCTSTR lpszKey, LPCTSTR lpszValue, LPCTSTR lpszValueName = NULL);
LONG AfxGetRegKey(HKEY key, LPCTSTR subkey, LPTSTR retdata);
BOOL AfxDeleteRegKey(LPCTSTR lpszKey);
HKEY AfxGetSectionKey(HKEY hKey, LPCTSTR szRegKeyGroup, 
									  LPCTSTR szRegKeyCompany, LPCTSTR szRegKeyApp, LPCTSTR lpszSection );
BOOL AfxRegSetValue( LPCTSTR szRegKeyCompany, LPCTSTR szRegKeyApp, LPCTSTR lpszSection, LPCTSTR lpszEntry, LPCTSTR lpszValue );
CString AfxRegQueryString(HKEY hKey, LPCTSTR szRegKeyGroup, 
										  LPCTSTR szRegKeyCompany, LPCTSTR szRegKeyApp, LPCTSTR lpszSection, LPCTSTR lpszEntry, LPCTSTR lpszDefault );
BOOL AfxRegQueryString(HKEY hKey, LPCTSTR szRegKeyGroup, LPCTSTR szRegKeyCompany, LPCTSTR szRegKeyApp,
									   CStringArray &astrName, CStringArray &astrData );


// Goto URL functions
HINSTANCE AfxGotoURL(LPCTSTR url, int showcmd);

// ActiveX Functions
BOOL AfxRegisterActiveXCtrls( LPCTSTR lpszOcxFileName );

//COMFUNC_BUILD	CString	AfxGetVersionString( );
CString AfxGetFileTitle( LPCTSTR lpszPathName );
CString AfxGetFileTitleNoExt( LPCTSTR lpszPathName );
CString AfxGetFileDirectoryExist( LPCTSTR lpszPathName, LPCTSTR lpszDefault );

CString GetGuidAsString();

BOOL	APIENTRY GfxRestoreWindowPos(CWnd* pWnd, int cmdShow);
void	APIENTRY GfxSaveWindowPos(CWnd* pWnd);

UINT APIENTRY GfxExecute(LPTSTR lpszCommandLine, UINT uCmdShow = SW_SHOWNORMAL);

const char* APIENTRY GetNewGUID();

SYSTEMTIME APIENTRY Time_tToSystemTime(time_t t);
time_t APIENTRY SystemTimeToTime_t(const SYSTEMTIME& st);

#undef AFX_DATA
#define AFX_DATA

#endif // _GFXCOMFUNC_H 
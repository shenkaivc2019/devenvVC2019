// ATool.h: interface for the CATool class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ATOOL_H__8A1F7142_4258_48D6_961A_61CBE9A7F0B1__INCLUDED_)
#define AFX_ATOOL_H__8A1F7142_4258_48D6_961A_61CBE9A7F0B1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "XMLSettings.h"

#ifndef COM_API
	#ifdef COM_EXPORT
		#define COM_API __declspec( dllexport )
	#else
		#define COM_API __declspec( dllimport )
	#endif
#endif

/////////////////////////////////////////////////////////////////////////////
// CSensor

class COM_API CSensor
{
	
public:
	CSensor(LPCTSTR lpszName = NULL, long lPoint = 0, BOOL bVisible = TRUE)
	{
		strName = lpszName;
		if (strName.IsEmpty())
			strName = _T("NONE");
		
		lPoint = lPoint;
		bShow = bVisible;
	}
	
	~CSensor()
	{
		
	}
	
public:
	CString		strName;
	BOOL		bShow;
	long		lPoint;
};

typedef COM_API struct tagTOOL
{
	int	iToolID;	// 仪器ID
	CString		strToolName;	// 仪器名称
	CString		strInfoPath;	// 信息相对路径
	CString		strToolDllDir;	// 动态库相对路径
	CString		strENName;		// 英文名称
	CString		strCNName;		// 中文名称
	CString		strSN;			// 仪器序列号
	CString		strAssetNo;		// 仪器资产号
	CString		strType;		// 类别
	//long		lLength;		// 长度
	float		fLength;		// 长度
	double		fWeight;		// 重量
	double		iDiameter;		// 内径
	double		oDiameter;		// 外径
	double      oDiameterMax;   // 最大外径
	double      oDiameterMin;   // 最小外径
	double		fMaxPressure;	// 最大工作压力
	double		fMaxTemperature;// 最大工作温度
	double		fMaxSpeed;      // 最大工作速度
	int			nBufferSize;
	int         nGroupIndex;    // 分组号, 从0开始
	BOOL		bSensors;
	CString		strToolPic;	// 工具图片路径

	float fCAPI; // 传感器校准系数
	COleDateTime oleCALTime; // 标定时间
	

	tagTOOL()
	{
		strSN = _T("Default");
		fLength = 0.0f;
		fWeight = 0;
		iDiameter = 0;
		oDiameter = 0;
		oDiameterMax = 0;
		oDiameterMin = 0;
		fMaxPressure  = 0;
		fMaxTemperature = 0;
		fMaxSpeed = 0;
		nBufferSize = 0;
		nGroupIndex = 0;
		fCAPI = 1.00f;
		oleCALTime = COleDateTime::GetCurrentTime();
		bSensors = TRUE;
	}
	
	BOOL IsDepthTool()
	{
		strToolName.MakeUpper();
		if (strToolName.Find(_T("DEPT")) > -1)
			return TRUE;
		
		return FALSE;
	}
	
	virtual void Serialize(CXMLSettings& xml); // used for CToolInfoPack
	void GetInfoPath();
	
} TOOL, *PTOOL, NEAR *NPTOOL, FAR *LPTOOL;

class COM_API CATool : public tagTOOL
{
public:
	CATool();
	~CATool();
	virtual void Serialize(CXMLSettings& xml); // used for CToolsView
	
	CString		m_strGroupFrom;
	CPtrArray	m_Sensors;		// Sensor array
	DWORD		m_dwCalibrate;	// Calibrate tool don't allow edit SN
	CString Label();
	BOOL FindImage(CString& strImage, BOOL bBmp = TRUE);
	void ClearSensors()
	{
		for (int i = 0; i < m_Sensors.GetSize(); i++)
		{
			CSensor* pMP = (CSensor*)m_Sensors.GetAt(i);
			if (pMP != NULL)
				delete pMP;
		}
		m_Sensors.RemoveAll();
	}
};

#endif // !defined(AFX_ATOOL_H__8A1F7142_4258_48D6_961A_61CBE9A7F0B1__INCLUDED_)

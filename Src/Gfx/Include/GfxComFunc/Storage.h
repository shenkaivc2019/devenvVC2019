#ifndef __STORAGE_H__
#define __STORAGE_H__

BOOL APIENTRY GetRootStorage(CString& strStgFile, IStoragePtr& stg, BOOL bWrite = TRUE);
BOOL APIENTRY GetChildStorage(LPCTSTR pszName, IStoragePtr stg, IStoragePtr& stgChild, BOOL bWrite = TRUE);
BOOL APIENTRY GetChildStream(LPCTSTR pszName, IStoragePtr stg, IStreamPtr& stream, BOOL bWrite = TRUE);
BOOL APIENTRY DeleteElement(LPCTSTR lpszPath, IStoragePtr& stg);

#endif
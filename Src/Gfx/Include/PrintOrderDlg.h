#if !defined(AFX_PRINTORDERDLG_H__5F5F8DDC_6CA0_4826_AC14_1F4ABCADF24C__INCLUDED_)
#define AFX_PRINTORDERDLG_H__5F5F8DDC_6CA0_4826_AC14_1F4ABCADF24C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrintOrderDlg.h : header file
//
//#include "Project.h"
#include "GraphWnd.h"

/////////////////////////////////////////////////////////////////////////////
// CPrintOrderDlg dialog

#define POF_ADD		1
#define POF_MDF		2

class CPrintOrderDlg : public CDialog
{
// Construction
public:
	CPrintOrderDlg(int nMode = POF_ADD, CWnd* pParent = NULL);   // standard constructor
	virtual ~CPrintOrderDlg();

// Dialog Data
	//{{AFX_DATA(CPrintOrderDlg)
	enum { IDD = IDD_PRINT_ORDER };
		CComboBox	m_wndCombo;
		CTreeCtrl		m_ViewTree;
		CBCGPListCtrl	m_PrintOrderList;
		CString			m_strPOF;
		CMFCButton		m_btnUp;
		CMFCButton		m_btnDown;
	//}}AFX_DATA
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrintOrderDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPrintOrderDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnBtnAdd();
	afx_msg void OnBtnDel();
	afx_msg void OnBtnReplace();
	virtual void OnOK();
	afx_msg void OnDestroy();
	afx_msg void OnDblclkViewTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkPrintorderList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnBtnMoveUp();
	afx_msg void OnBtnMoveDown();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Operations
public:
	void AddView(UINT iItem = (UINT)-1);
	void InitViewTree();
	void InitPrintOrderList(CPof* pPOFile);

// Attributes
public:
	CPofInfo*	m_pPofInfo;
	int			m_nMode;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTORDERDLG_H__5F5F8DDC_6CA0_4826_AC14_1F4ABCADF24C__INCLUDED_)

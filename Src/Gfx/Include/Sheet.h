﻿// Sheet.h: interface for the CSheet class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SHEET_H__E76E493A_1572_459B_9445_75D3FED6F2EE__INCLUDED_)
#define AFX_SHEET_H__E76E493A_1572_459B_9445_75D3FED6F2EE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#define     DEFAULT_DEPTH_TRACK_WIDTH  185

#include "Track.h"
#include "ULInterface.h"
#include "PrintInfo.h"

#ifndef ULVWBASE_API
	#ifdef ULVWBASE‌_EXPORT
		#define ULVWBASE_API __declspec( dllexport )
	#else	
		#define ULVWBASE_API __declspec( dllimport )
	#endif
#endif
typedef std::vector<CCurveData*>	vec_data;

enum
{
		idAutoSave,
		// ------- Sheet -------
		idSheetName,
		idSheetIcon,
		idSheetBackColor,
		idSheetRatioX,
		idSheetRatioY,
		idSheetScrollDepth,
		idSheetDepthOffset,
		idSheetPosition,
		idDriveMode,
		// ------- Track -------
		idTrack,
		idTrackCaption,
		idTrackType,
		idTrackLeft,
		idTrackRight,
		idTrackBorder,
		idTrackLineStyle,
		idTrackLineColor,
		idTrackLineWidth,
		// ------- Grid -------
		idHorizGird,
		idHorizGridLine,
		idHorizGridLine0,
		idHorizGridLine1,
		idHorizGridLine2,
		idHorizGridLine0Interval,
		idHorizGridLine1Interval,
		idHorizGridLine2Interval,
		idHorizGrid0LineStyle,
		idHorizGrid1LineStyle,
		idHorizGrid2LineStyle,
		idHorizGrid0LineColor,
		idHorizGrid1LineColor,
		idHorizGrid2LineColor,
		idHorizGrid0LineWidth,
		idHorizGrid1LineWidth,
		idHorizGrid2LineWidth,
		idVertGird,
		idVertGridLine,
		idVertGridLineMajor,
		idVertGridLineMinor,
		idVertGridLineDivision,
		idVertGridLineDivNumber,
        idVertGridMajorLineStyle,
		idVertGridMinorLineStyle,
		idVertGridMajorLineColor,
		idVertGridMinorLineColor,
		idVertGridMajorLineWidth,
		idVertGridMinorLineWidth,
		// ------- Curve -------
		idCurveName,
		idCurveUnit,
		idCurveLeftValue,
		idCurveRightValue,
		idCurveLineStyle,
		idCurveLineColor,
		idCurveLineWidth,
		idCurveFilter,
		idCurveLeftGrid,
		idCurveRightGrid,
		idCurveOffset,
		idCurveDepthDistance,
		idSheetScrollTime,
		idSheetTimeStart
};

enum { dragNone = 0, dragWnd = 1, dragTop = 2, dragBtm = 4};

class CCurve;
class CGraphView;
class CULKernel;

typedef std::vector<CGraphView*>	vec_gv;
typedef	std::vector<CCurve*>		vec_curve;
typedef CArray<CTrack*, CTrack*>	CTrackArray;

class ULVWBASE_API CSheet
{
public:
	CSheet();
	CSheet(int nMode);
	CSheet(LPCTSTR pszPathName);
	CSheet(CSheet* pSheet);
	virtual ~CSheet();

public:
	void	AddRef() { InterlockedIncrement(&m_nRef); }
	BOOL	Release()
	{ 
		InterlockedDecrement(&m_nRef);
		if (m_nRef == 0)
		{
			delete this;
			return TRUE;
		}

		return FALSE;
	}

public:
	long		m_nRef;
	CString		m_strName;			// 表单名称
	CString		m_strFile;			// 最近文件
	vec_gv		m_vecGraph;			// 视图链表
	vec_curve	m_selCurves;		// 选中曲线
	CTrackArray	m_TrackList;		// 井道链表
	CPtrArray	m_BackCurveList;	// 曲线链表
	COLORREF	m_crBack;

	// Appearance
	float		m_nRatioX;			// 水平比例尺
	float		m_nRatioY;			// 垂直比例尺

	// Depth Drive
	long		m_lStartDepth;		// 起始深度
	long		m_lEndDepth;		// 终止深度	

	// Time Drive
	long		m_lStartTime;		// 起始时间
	long		m_lEndTime;			// 终止时间
	long		m_lJumpDepth;		// 跳跃深度 0
	long		m_lJumpTime;		// 跳跃时间 0
	BOOL		m_bShowTime;
	BOOL		m_bShowDepth;
	
	CSize		m_szPage;			// 页面大小
	int 		m_nGraphMode;		// 竖直水平，默认竖直
	int			m_nDriveMode;		// 驱动方式
	int			m_nIconFileSize;    // 图标大小
	void*		m_pIconBuf;         // 图标缓冲
	CString		m_strIconPath;		// 图标路径
	BOOL        m_bAutoSize;        // 是否需要自动调整井道宽度，用于新建井道时，添加最后一个井道后再自动调整

	long		m_ls;
	double      m_dDelta;

	int			m_nMaxCurve;
	int			m_nHotDivider;
	COLORREF	m_crHotDivider;
	long		m_lDepthOffset;			// 绘图整体偏移量 wle 1515仪器曲线无法显示问题

	BOOL		m_bTimeTop;				// 时间驱动模式下，时间起始点在上方时为TRUE，起始点在下方为FALSE
	float       m_fRatioTime;       // 时间比例
	long        m_lStartTimeLogic;  // 起始时间对应的开始逻辑坐标
	BOOL		m_bBrowseFlag;

private:
	void		InitSheet();

public:
	void		ResetSheet();
	int			CalcPageWidth();
	int			GetScrollMax();

// Ratio Operations
	void		SetRatioX(float nRatio);
	float		GetRatioX() {   return m_nRatioX;	}
	void		SetRatioY(int nRatio);
	void		SetRatioTimeY(float nRatio);
	void		MRatioY(double dK);
	float		GetRatioY()	{	return m_nRatioY;	}
	
// Mode Operations
	void		SetGraphMode(int nMode);
	int			GetGraphMode();
	void		SetModeShow();

// Track Operations
	void		AddTrack(CTrack* pTrack);
	void 		AddNewTrack(CTrack* pTrack);
	void		InsertTrack(int nIndex, int nTrackType);
	void		RemoveTrack(int nIndex);
	BOOL		RemoveTrack(CTrack* pTrack);

	inline void ClearTracks()
	{
		for (int i = m_TrackList.GetSize() - 1; i > -1; i--)
		{
			CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
			if (pTrack != NULL)
				delete pTrack;
		}
		m_TrackList.RemoveAll();  	
	}

	int			GetTrackIndex(CTrack* pTrack, LPRECT lpRect);

	CRect		GetTrackRect(int nIndex);
	void		SetTrackRect(int nIndex, LPRECT lpRect);
	
	CRect		GetHeadRect(int nIndex);
	void		SetHeadRect(int nIndex, CRect rc);
	void		RecalAllTR();
	void		UpdateGrids();

// Curve Operations
	void		RemapTrackCurve(CULKernel* pULKernel);	// 映射：井道 -- 仪器曲线
	void		RemapTrackCurve();
	BOOL		AddCurveToTrack(CCurve* pCurve, int nIndex);
	void		RemoveCurve(CCurve* pCurve);
	void		RemoveAllCurve();
	void		GetAllCurve(CPtrArray& arrCurve);
	int 		GetAllSeledCurve(CPtrArray& arrCurve);
	CString		GetAllSeledCurveName();
	void		SelectAllCurve();
	void	    InvertSelCurve();
	BOOL		IsCurveSelected();
	BOOL		IsSeledCurvesDel();
	BOOL		IsSeledCurvesExt();
	void		EraseAllSeledCurve();
	void		EraseSeledCurve(CCurve* pCurve);
	CCurve*		GetCurve(CString strCurveName);
	CCurve*		GetDepthCurve();
	CCurve*		GetDepthCurve(CPtrArray& CurveList);
	CCurve*     GetDepthCurve(CURVES& vecCurve);
	void		RemapTrackCurve(CURVES& vecCurve);
	void		RemapTrackCurve(vec_data* pDatas);
	void		AutoMap(CURVES& vecCurve);
	void		ReMapPerforationCurve(vec_data* pDatas);
	void		ReMapPerforationSheet(CURVES& vecCurve);
	void		ReMapPerforationCurve(CURVES& vecCurve);
	void		ResetAllCurveData();
	BOOL		GetCurvesTop(long& lTop);
	BOOL		GetCurvesBottom(long& lBottom);
	void		SetProject(IProject* pProject);

	// Layout Operation
	void		Default();
	void		AutoSize();
	void		RecalcLayout();
	void		ExchangeTrackPosition(int nFrom, int nTo);
	void		SetTrackEdge(int nEdge, int x, BOOL bDP = TRUE);

public:
	void DrawBackCurve(CDC* pDC, long lStartDepth, long lEndDepth, BOOL bLogDraw = FALSE);
	BOOL	SaveSheet(LPCTSTR strPath = NULL, BOOL bRP = FALSE, BOOL bDelete = TRUE);
	BOOL	LoadSheet(LPCTSTR pszPath, BOOL bRP = FALSE);				// 加载模板
	UINT    MatchSheet(vec_ic& vecCurve, UINT nCount = -1);				// 匹配并加载模板
	void	Serialize(CXMLSettings& xml);

// Test Operation
	void	Watch(BOOL bTrace = TRUE);

// Draw Operations
	void	SetLogMode(int nWorkMode, int nDriveMode);	// 设置所有井道工作状态
	void	SetDepthRange(long  lStartDepth, long  lEndDepth);			// 设置深度
	void	DrawHead(CDC* pDC, LPRECT lpRect, BOOL bTitle = TRUE);		// 画井道头
	void	DrawHotDivider(CDC* pDC);
	void	DrawHorzGrid(CDC* pDC, long lStartDepth, long lEndDepth);	// 画横格
	void	DrawVertGrid(CDC* pDC, long lStartDepth, long lEndDepth);	// 画竖格
	void	DrawMark(CDC* pDC, long lStartDepth, long lEndDepth);
//	void    DrawCurveFlag(CDC* pDC , long lStartDepth , long lEndDepth);//add by xwh
	void	DrawSheet(CDC* pDC, long lStartDepth, long lEndDepth, int nMode = 0);	// 画曲线 0全屏刷新 1实时刷新
	void	DrawFill(CDC* pDC, long lStartDepth, long lEndDepth, int nMode = 0);
	void	DrawGraph(CDC* pDC, LPRECT lpRect, LPRECT lpDraw, long lTopDepth, long lBottomDepth, BOOL bTwice = TRUE);
	CRect	DrawThumb(CDC* pDC, LPRECT lpRect, long lt, long lb);
	CRect	CalcThumb(LPRECT lpRect, CPoint pt, int nMode, long& lt, long& lb);
	CRect	CalcThumb(LPRECT lpRect, long lt, long lb);

// Print Operations
	void FitPage(int nPageWidth);
	int  GetMaxHeadCurveNum();
	int  PrintHeadInit(int cy = 0, int cx = 0);
	int	 PrintHead(CULPrintInfo* pPrintInfo, int nY);
	void PrintLine(CULPrintInfo* pPrintInfo, double fDepth);
	void PrintVertGrid(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth);
	void PrintHorzGrid(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth);
	void PrintMark(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth);
	void PrintSheet(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth);
	void PrintCurveFlags(CULPrintInfo* pInfo, long lDepth, int nCurve, BOOL bMode);

	BOOL PrintCurveFirst(CULPrintInfo* pInfo, long lMaxDepth, long lMinDepth, BOOL bMode);

	void StaticPrint(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth);
	void PrintFill(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth);

	// add by bao use for Time Driver
	void SetTimeStart(BOOL bTop);
	void SetStartTimeLogic(long lStartTime = 0, BOOL bTrue = TRUE); // 参数1为毫秒, 参数2为打开文件时原点在下方时控制屏幕大小

	CCurve*		GetTimeCurve();
	CCurve*		GetTimeCurve(CPtrArray& CurveList);
	CCurve*     GetTimeCurve(CURVES& vecCurve);
};

#define SHT_FILE(strTempl) (AfxGetComConfig()->m_strAppPath+"\\Template\\Tracks\\"+(strTempl)+".xml")
#define SHT_PATH		   (AfxGetComConfig()->m_strAppPath+"\\Template\\Tracks\\")

#endif // !defined(AFX_SHEET_H__E76E493A_1572_459B_9445_75D3FED6F2EE__INCLUDED_)

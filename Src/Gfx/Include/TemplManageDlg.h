#if !defined(AFX_TEMPLMANAGEDLG_H__79DA46BC_6ADC_4F4A_BFBE_735446F2FED8__INCLUDED_)
#define AFX_TEMPLMANAGEDLG_H__79DA46BC_6ADC_4F4A_BFBE_735446F2FED8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TemplManageDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTemplManageDlg dialog

class CTemplManageDlg : public CDialog
{
// Construction
public:
	CTemplManageDlg(int nType, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTemplManageDlg)
	enum { IDD = IDD_TEMPL_MANAGE_DLG };
	CListCtrl	m_TemplsList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTemplManageDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:
	void OnNewTempl();
	void OnEditTempl();
	void OnSaveTempl();
	void OnDeleteTempl();

	CMenu				m_menu;
	int					m_nType;
	CImageList			m_ImageList;
	CMFCMenuButton		m_btnSave;
	// Generated message map functions
	//{{AFX_MSG(CTemplManageDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnCommand(UINT ID);
	afx_msg void OnItemChange(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkTemplateList(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEMPLMANAGEDLG_H__79DA46BC_6ADC_4F4A_BFBE_735446F2FED8__INCLUDED_)

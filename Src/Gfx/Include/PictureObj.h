// PictureObj.h: interface for the CPictureObj class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PICTUREOBJ_H__858A9AE9_55F0_4080_B17F_1D9E2A3CD5C8__INCLUDED_)
#define AFX_PICTUREOBJ_H__858A9AE9_55F0_4080_B17F_1D9E2A3CD5C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MultiRectTracker.h"


#define TOOL_WIDTH		200
#define TOOL_HEIGHT		8000

class CPictureObj : public CMRTObject, public CPicture
{
public:
	CPictureObj(LPCRECT lpRect, LPCTSTR pszText, LPCSTR pszFile);
	virtual ~CPictureObj();
	virtual void Draw(CDC* pDC, CRect& rect)
	{
		CRect rc = GetRect();
        DrawPicture(pDC->GetSafeHdc(), rc.left, rc.top, rc.Width(), rc.Height());
    }
public:
	CString m_strFile;
};

#endif // !defined(AFX_PICTUREOBJ_H__858A9AE9_55F0_4080_B17F_1D9E2A3CD5C8__INCLUDED_)

#pragma once
#include "GammaDefine.h"
#include "TME_AziGammaTB.h"
#include "TME_GammaTB.h"
#define GAMMAAPICORR __declspec(dllexport)
#define GAMMACAPICORR extern "C" __declspec(dllexport)



GAMMACAPICORR int  GammaSum(int a,int b);

class GAMMAAPICORR GammaCalute
{
public:
	GammaCalute();
	~GammaCalute();
public:
	void  Init(const TS_CaluteParam* pCaluteParam);
	BOOL calculate(TS_ME_AZIGAMMATB& meazigammatb);
	BOOL calculateOne(TS_ME_AZIGAMMATB_ONE& meszigammatb);
	BOOL buildString(CString& strExpress);
	void getDCFAPICF(float& fToolDCF, float& fToolAPICF);
private:
	BOOL calculateSumGamma(TS_ME_AZIGAMMATB& meazigammatb);
	BOOL calculateAPI_D(TS_ME_AZIGAMMATB& meazigammatb);
	BOOL calculateAPI(TS_ME_AZIGAMMATB& meazigammatb);
	BOOL calculateAPIC_G(TS_ME_AZIGAMMATB& meazigammatb);
	BOOL calculateAPIC(TS_ME_AZIGAMMATB& meazigammatb);

	//2020.5.12 dyl start
	BOOL calculateAPI_D(TS_ME_AZIGAMMATB_ONE& meazigammatb);
	BOOL calculateAPI(TS_ME_AZIGAMMATB_ONE& meazigammatb);
	BOOL calculateAPIC_G(TS_ME_AZIGAMMATB_ONE& meazigammatb);
	BOOL calculateAPIC(TS_ME_AZIGAMMATB_ONE& meazigammatb);
	//2020.5.12 dyl start

	TS_CaluteParam m_CaluteParam;
	BOOL m_bIsinit;
	int m_nCount;
	const int m_nConst;
	float m_fToolDCF;
	float m_fAPICF;
public:
	BOOL calculateCBG(TS_ME_AZIGAMMATB& meazigammatb);
public:
	BOOL calculateCBGMWD(TS_ME_AZIGAMMATB& meazigammatb);
	BOOL calculateOneCBGMWD(TS_ME_AZIGAMMATB_ONE& meszigammatb);
	BOOL calculateCBGLWD(TS_ME_AZIGAMMATB& meazigammatb);
	BOOL calculateOneCBGLWD(TS_ME_AZIGAMMATB_ONE& meazigammatb);
	BOOL calculateCBGMWD(TS_ME_GAMMATB& meAziGamma);
	BOOL calculateCBGLWD(TS_ME_GAMMATB& meAziGamma);
};
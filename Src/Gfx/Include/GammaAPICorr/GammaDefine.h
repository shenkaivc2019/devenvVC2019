#pragma once


typedef struct tagTS_CaluteParam
{
	int	iMudType;  // 泥浆类型:light|0.light;hematite|1.hematite;barite|3.barite
	int iDrillingMode;
	double	dToolSize;	// 工具尺寸（可由井眼信息表获取）
	double  dToolACF;
	double	dToolIntDia;	// 内径
	double	dToolExtDia;	// 外径
	double	dPercentageOfKm;// 氯化钾KCl百分比
	double	dDensity;	// 密度	
	double	dHoleSize; // 井眼尺寸（裸眼直经）
	//2020.7.4 sys Start
	float fBitSize;//钻头尺寸
	float fBarrelOD;//探管尺寸
	float fCAPI;//MWD传感器校准系数
	float fLWDCc;//LWD钻铤校正系数
	//float fLWDCAPI;//LWD传感器校准系数
	//2020.7.4 sys End
}TS_CaluteParam;


//2020.5.14 杜永亮 start
//校正单个数据的数据结构体
typedef struct tagTS_ME_AZIGAMMATBONE
{
	int	iAziGaID;							// 自增字段
	char	szRunID[50 + 1];							// 自增字段
	long	lTDateTime;							// 时间（精确到秒）
	int	iMillitime;							// 毫秒
	int	iToolID;							// 工具编号
	long	lAZIGRMDepth;							// 方位伽马测量井深
	long	lAZIGRVDepth;							// 方位伽马测量垂深
    float	fAZIGAM;                    //原始伽马
    float	fAZIGAM_API_D;              //暗电流校正
    float	fAZIGAM_API;                //钻铤壁校正     系数校正
    float	fAZIGAM_APIC_G;             //氯化钾校正
    float	fAZIGAM_APIC;               //泥浆校正       环境校正
	float	fTEMP_Fx10;							// 钻铤温度
	float	fAPM;							// 环空压力
	float	fECDM;							// 循环当量密度=环空压力/方位伽马测量点垂深
	short	nBad;							// 是否坏点:No|0.否;Yes|1.是
	short	nDrillActiv;							// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
	long	lCreateTime;							// 创建时间戳
	short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
	char	szMemo[100 + 1];							// 备注
}TS_ME_AZIGAMMATB_ONE;
//2020.5.14 杜永亮 start
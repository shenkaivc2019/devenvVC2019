// IniFile.h: interface for the CIniFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INIFILE_H__21E58D5A_A7B1_46C9_8D57_A72C9522424D__INCLUDED_)
#define AFX_INIFILE_H__21E58D5A_A7B1_46C9_8D57_A72C9522424D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AFX_EXT_CLASS CIniFile  
{
public:
	CIniFile(LPCTSTR pszFile = NULL);
	virtual ~CIniFile();

	CString GetProp(LPCTSTR pszField, LPCTSTR pszKey, LPCTSTR pszDefault = NULL);
	int GetProp(LPCTSTR pszField, LPCTSTR pszKey, int nDefault);
	CRect GetRect(LPCTSTR pszField, LPCTSTR pszKey, LPRECT pRect);
	int Prompt(LPCTSTR pszKey, LPCTSTR pszDefault, UINT nType = MB_OK);

public:
	CString m_strFile;
};

#endif // !defined(AFX_INIFILE_H__21E58D5A_A7B1_46C9_8D57_A72C9522424D__INCLUDED_)

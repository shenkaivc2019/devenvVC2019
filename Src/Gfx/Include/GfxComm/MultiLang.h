// MultiLang.h: interface for the CMultiLang class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MULTILANG_H__1876CA10_065B_429C_AFAB_6DDE420E6CC4__INCLUDED_)
#define AFX_MULTILANG_H__1876CA10_065B_429C_AFAB_6DDE420E6CC4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef GFXCOMM_API
	#ifdef GFXCOMM_EXPORT
		#define GFXCOMM_API __declspec( dllexport )
	#else	
		#define GFXCOMM_API __declspec( dllimport )
	#endif
#endif

class GFXCOMM_API CMultiLang
{
public:
	CMultiLang();
	virtual ~CMultiLang();

	LANGID SetLang(LANGID lid);

public:
	LANGID m_nLang;
	CString m_strMain;
	CString	m_strLang;
	CStringList m_lstDel;
};

GFXCOMM_API CMultiLang* AfxGetMultiLang();

#endif // !defined(AFX_MULTILANG_H__1876CA10_065B_429C_AFAB_6DDE420E6CC4__INCLUDED_)

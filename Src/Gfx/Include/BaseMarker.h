// BaseMarker.h: interface for the CBaseMarker class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BASEMARKER_H__2CDAD045_2A5E_402C_B3B0_C777FFE02EC8__INCLUDED_)
#define AFX_BASEMARKER_H__2CDAD045_2A5E_402C_B3B0_C777FFE02EC8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "BaseMark.h"
#include "Curve.h"
#include "XMLSettings.h"

class CTrack;
class CBaseMarker  
{
public:
	CBaseMarker();
	virtual ~CBaseMarker();
public:
	void       Serialize(CXMLSettings& xml);
	void       AddMark(CBaseMark* pMark);
	void       DeleteMark(CBaseMark* pMark);
	void       DrawMark(CDC* pDC, long lStartDepth, long lEndDepth);
	void       PrintMark(CULPrintInfo* pInfo, float fMinDepth, float fMaxDepth);
	CBaseMark* HitTest(CPoint pt, CCurve* pCurve);
	CBaseMark* HitTestCurveFlag(CPoint pt, CCurve* pCurve);
	CBaseMark* HitTestCurveFlagText(CPoint pt, CCurve* pCurve);
	CBaseMark* HitTestArrow(CPoint pt, CCurve* pCurve);
	CRect      MarkInRect(CBaseMark * pMark);
	CRect	   MarkInArrow(CBaseMark * pMark);
	void       RemoveAll();
//	void	   DrawCurveFlag(CDC* pDC, long lStartDepth, long lEndDepth);
public:
	CTrack*		m_pTrack;		// ����
	CPtrArray   m_MarkList;     // ��ǩ�б�
};

#endif // !defined(AFX_BASEMARKER_H__2CDAD045_2A5E_402C_B3B0_C777FFE02EC8__INCLUDED_)

#include "Afxtempl.h"

#ifndef _DATA_STRUCT_H
#define _DATA_STRUCT_H

#define   SYM_STRING  0
#define   SYM_DATABLOCK 1

typedef  struct _tagProjectMangeItem
{
	long  m_nNumber ;
	char  szProjectName[32];
	char  szOperator[10];
	char  szJob[20];
	char  szSegment[20] ;
	char  szServiceItem[50];
}PROJECTMANAGEITEM;


typedef  struct _tagProjectInfo
{
	long  m_nProjectNumber;
	char  szProjectName[32];
	char  szCompany[50];
	char  szAddress[50];
	char  szPhone[20] ;
	char  szTime[12];
	char  szProjectFileName[64];
	char  szProjectPath[255];
}PROJECTINFO;

typedef  struct _tagJobInfo
{
	long  m_nJobNumber ;
	char  szJobName[50];
	char  szWellName[50];
	char  szAddress[50];
	char  szTime[12];
	char  szCharacter[255] ;
	char  szJobType[16];
}JOBINFO;


typedef  struct _tagSegmentInfo
{
	long     m_nSegmentNumber ;
	char	 szSegmentName[32];
	double   fMaxDepth;
	double   fMinDepth;
	char	 szOperator[10];
	char	 szServiceItem[50];

}SEGMENTINFO;


//符号类型信息
typedef  struct  _tagSymbolInfo
{
	char        szSymbolName[32] ; //符号项名称
	BYTE      	btType ;  //类型
	char        szSymbolContent[128] ; //字符类型符号内容
	DWORD		dwBlockSize ;  //数据块大小
	char 		szRemark[128];	 //符号说明
}SYMBOLINFO;

//符号
typedef   struct  _tagSymbol
{
	SYMBOLINFO     symbolInfo;		//符号信息
	BYTE   * pDataBlock ;				//数据块
}SYMBOL;


typedef  CArray<SYMBOL , SYMBOL&> SYMBOLARRAY; //符号数组

#endif



#if !defined(AFX_GFXPAGE_H__D3693D4E_E675_4F19_9511_F11E4F188CDB__INCLUDED_)
#define AFX_GFXPAGE_H__D3693D4E_E675_4F19_9511_F11E4F188CDB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CGraphWnd;
class CTrack;
class CCurve;
class CSheet;
class CXComboBox : public CComboBox
{
public:
	int AddString(UINT nID)
	{
		CString string;
		string.LoadString(nID);
		return CComboBox::AddString(string);
	}
};
// GfxPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGfxPage dialog

class CGfxPage : public CDialog
{
	DECLARE_DYNAMIC(CGfxPage)
// Construction
public:
	CGfxPage(UINT nIDTemplate, CWnd* pParentWnd = NULL);
	~CGfxPage();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPropPageImageDlg)
protected:
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGfxPage)
	afx_msg void OnGfxEnter();
	afx_msg void OnGfxEsc();
	afx_msg LRESULT OnGetDefID(WPARAM wp, LPARAM lp);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	static CGraphWnd*	m_pGraph;
	static CTrack*		m_pTrack;
	static CCurve*		m_pCurve;
	static CSheet*		m_pSheet;

	virtual void InitCurve();
protected:
	HACCEL	m_hAccel;
	
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GFXPAGE_H__D3693D4E_E675_4F19_9511_F11E4F188CDB__INCLUDED_)

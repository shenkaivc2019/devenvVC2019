﻿// History.h: interface for the CHistory class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HISTORY_H__B496BA78_D11C_468D_950E_81E35762099F__INCLUDED_)
#define AFX_HISTORY_H__B496BA78_D11C_468D_950E_81E35762099F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "ComConfig.h"
#ifndef BASEINFO_API
	#ifdef BASEINFO‌_EXPORT
		#define BASEINFO_API __declspec( dllexport )
	#else	
		#define BASEINFO_API __declspec( dllimport )
	#endif
#endif
class CXMLSettings;
class BASEINFO_API CHistory
{
public:
	CHistory(CDialog* pDialog = NULL, int nCtrlID = -1);
	virtual ~CHistory();
	void Save(CDialog* pDialog, int nCtrlID = -1, int nCount = 0);
	void Reset(CDialog* pDialog, int nCtrlID);
	CString GetLastest(int nDialog, int nID);
	
	CXMLSettings* m_pXML;
	static int	m_nLimit;
};

#define HISTORY_FILE (AfxGetComConfig()->m_strAppPath + "\\Config\\ProjectWizard.his")

#endif // !defined(AFX_HISTORY_H__B496BA78_D11C_468D_950E_81E35762099F__INCLUDED_)

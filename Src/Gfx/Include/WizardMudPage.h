#if !defined(AFX_WIZARDMUDPAGE_H__3A0918D3_FF22_4F86_B874_AB6B4E81601A__INCLUDED_)
#define AFX_WIZARDMUDPAGE_H__3A0918D3_FF22_4F86_B874_AB6B4E81601A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GridCtrl.h"
#include "MudInformation.h"
#include "WPropertyPage.h"
// WizardMudPage.h : header file
//
#define MudInfoMaxNum 64
/////////////////////////////////////////////////////////////////////////////
// CWizardMudPage dialog

class CWizardMudPage : public CWPropertyPage
{
	DECLARE_DYNCREATE(CWizardMudPage)

// Construction
public:
	CWizardMudPage();
	~CWizardMudPage();

// Dialog Data
	//{{AFX_DATA(CWizardMudPage)
	enum { IDD = IDD_WIZARD_STEP2_MUD1 };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA
	
	CGridCtrl	m_GridCtrl;
	CMudInformation* m_pInfo;	//
	int WriteInfo();
	void ReadInfo();

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CWizardMudPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	//}}AFX_VIRTUAL
	virtual void InitGridCtrl();
	void Clear();
	void UpdateGrid(BOOL bFlag = TRUE);

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CWizardMudPage)
	afx_msg void OnAdd();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:		
	int		m_nID[MudInfoMaxNum];		//1.2.3...
	long	m_dwType[MudInfoMaxNum];
	long	m_lTime[MudInfoMaxNum];
	double	m_dWeight[MudInfoMaxNum];
	double	m_dRm[MudInfoMaxNum];
	double	m_dKm[MudInfoMaxNum];
	double	m_dMudPitTemp[MudInfoMaxNum];
// 2019.07.29 add Temp.C Start
//	double	m_dAdjustedRm[MudInfoMaxNum];
	double	m_dTempC[MudInfoMaxNum];
// 2019.07.29 add Temp.C End

	int m_nSize;
	CStringArray m_Options;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WIZARDMUDPAGE_H__3A0918D3_FF22_4F86_B874_AB6B4E81601A__INCLUDED_)

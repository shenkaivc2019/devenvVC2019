﻿// AllInfoPack.h: interface for the CAllInfoPack class.
//
//////////////////////////////////////////////////////////////////////


#if !defined(AFX_ALLINFOPACK_H__40316E3C_BD61_48F9_BAF9_B47A386276DE__INCLUDED_)
#define AFX_ALLINFOPACK_H__40316E3C_BD61_48F9_BAF9_B47A386276DE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ULCOMMDEF.H"
#include "FilterParam.h"
#include "ULTool.h"
#ifndef ULVWBASE_API
	#ifdef ULVWBASE‌_EXPORT
		#define ULVWBASE_API __declspec( dllexport )
	#else	
		#define ULVWBASE_API __declspec( dllimport )
	#endif
#endif
class CChannel;
class CDataItem;
class CToolInfoPack;
class CULTool;
class CCurve;
class CXMLSettings;

//仪器信息集合数组定义
typedef CArray<CToolInfoPack, CToolInfoPack&> TOOLINFOPACKARRAY;
typedef CArray<CToolInfoPack*, CToolInfoPack*> CToolInfoPacks;
void ReadObjClass(CArchive& ar);

//曲线信息集合
class  ULVWBASE_API CCurveInfoPack : public CObject
{
	DECLARE_SERIAL(CCurveInfoPack)
public:
	CCurveInfoPack();			// Constructor
	~CCurveInfoPack();			// Destructor
	virtual void Serialize(CArchive& ar);
	virtual void Serialize(CXMLSettings& xml);

	virtual	void  operator =  (const CCurveInfoPack& Info);

public:
	void	AssignFromCurve(CCurve* pCurve);	// 从曲线信息中获得相关信息
	void	AssignToCurve(CCurve* pCurve);		// 使用曲线信息	
	void    AssignFilterFromCurve(CCurve* pCurve);

public:
	CString	m_strName;			// 仪器曲线名称
	CString m_strDescrip;		// 仪器曲线描述			
	long	m_lDepthOffset;		// 仪器曲线偏移
	int		m_nOffMode;			// 仪器曲线偏移模式
	long	m_lInterval;		// 曲线深度插补
	// 仪器曲线的绘图属性

	int				m_nLeftGrid;		// 左格
	int				m_nRightGrid;		// 右格
	int				m_nLineWidth;		// 线宽
	int				m_nLineStyle;		// 线型
	double			m_fLeftValue;		// 左值
	double			m_fRightValue;		// 右值
	CString			m_strUnit;			// 单位
	COLORREF		m_crColor;			// 颜色
	long			m_lDepthDistance;	// 深度间隔
	int				m_iTimeDistance;		//time  inter
	long			m_lDepthDelay;		// 深度延迟
	long			m_lTimeDelay;		//时间延迟
	int				m_nFilter;			// 滤波器ID ,未使用
	int             m_nUserFilter;      // 用户自定义滤波器序号
	int             m_nFilterTempl;  // 某滤波类型下的模板序号
	double          m_fMulti;           // 乘因子
	double          m_fPlus;            // 加因子
	BOOL            m_bView;            // 是否显示
	CString			m_strFilter;		// 滤波器名称
	CFilterParams	m_FParams;			// 
};

// 仪器信息
class ULVWBASE_API CToolInfoPack
{

public:
	CToolInfoPack();						// Constructor
	~CToolInfoPack();						// Destructor 

public:
	TOOL		m_Tool;
	CPtrArray	m_CHInfoList;
	CPtrArray	m_CurveInfoList;	// Tool para
	CPtrArray	m_ScopeList;

	CULTool*	m_pULTool;
// 2020.2.28 Ver1.6.0 TASK【002】 Start
	int			m_nToolID;
	vector<TS_MEASPOINTINFOTB> m_vecgMeasPointInfoTB;
// 2020.2.28 Ver1.6.0 TASK【002】 End
public:	
	void	ResetList(BOOL bAll = TRUE);

public:
	virtual void Serialize(CArchive& ar);
	virtual void Serialize(CXMLSettings& xml);

	void	AllocToolInfo(CULTool* pULTool, BOOL bInit = TRUE);	// 获得仪器所有有关信息
	void    ApplyToolInfo(CULTool* pULTool);		// 应用仪器信息
    CCurveInfoPack* GetCurveInfo(CString& strCurve); 
	void    ReplayApplyToolInfo(CULTool* pULTool);

	//Add by zy 2002 9 13
	//Begin
	void	InitScopeInfo(CULTool* pULTool);		//由仪器构造示波器信息结构 
	void	InitCurveInfo(CURVEPROP &CurveInfo,CCurve *pCurve,int nMaxPoint);	//有曲线构造曲线信息结构
	//End

	void	SaveToolProps();
	void	LoadToolProps();
	void    LoadCurvesProps(int nIndex);  //add by gj 20121212

	virtual void operator = (const CToolInfoPack& pToolInfo);
};

#endif // !defined(AFX_ALLINFOPACK_H__40316E3C_BD61_48F9_BAF9_B47A386276DE__INCLUDED_)

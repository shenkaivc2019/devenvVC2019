#ifndef _MEMDC_H_
#define _MEMDC_H_

//////////////////////////////////////////////////
// CMyMemDC - memory DC
//
// Author: Keith Rule
// Email:  keithr@europa.com
// Copyright 1996-1999, Keith Rule
//
// You may freely use or modify this code provided this
// Copyright is included in all derived versions.
//
// History - 10/3/97 Fixed scrolling bug.
//                   Added print support. - KR
//
//           11/3/99 Fixed most common complaint. Added
//                   background color fill. - KR
//
//           11/3/99 Added support for mapping modes other than
//                   MM_TEXT as suggested by Lee Sang Hun. - KR
//
// This class implements a memory Device Context which allows
// flicker free drawing.

#if _MSC_VER < 1300
class CMemDC : public CDC
#else
class CMyMemDC : public CDC
#endif
{
public:	
	CBitmap		m_bitmap;		// Offscreen bitmap
	CBitmap*	m_oldBitmap;	// bitmap originally found in CMyMemDC
	CDC*		m_pDC;			// Saves CDC passed in constructor
	CRect		m_rect;			// Rectangle of drawing area.
	BOOL		m_bMemDC;		// TRUE if CDC really is a Memory DC.
	DWORD		m_dwRop;		// Raster-operation codes while ~CMyMemDC
	// CFile*		m_pFile;		// File
	HANDLE		m_hFile;		// Handle of file mapping
	LPBITMAPINFOHEADER m_pbi;	// 
	LPVOID		m_pBits;		// DIB section bits

public:
	CMyMemDC()
	{
		m_oldBitmap = NULL;
		m_pDC = NULL;
		m_rect.SetRectEmpty();
		m_bMemDC = FALSE;
		m_dwRop = SRCCOPY;
		//m_pFile = NULL;
		m_hFile = NULL;
		m_pbi = NULL;
		m_pBits = NULL;
	}

	void SetBufferDC(CDC* pDC, const CRect* pRect = NULL, BOOL bMask = FALSE)
	{
		ASSERT( pDC != NULL ); 
		
		m_pDC = pDC;
		m_oldBitmap = NULL;
		m_bMemDC = !pDC->IsPrinting();
		
		if ( pRect == NULL ) 
		{
			pDC->GetClipBox( &m_rect );
		}
		else
		{
			m_rect = *pRect;
		}
		
		if ( m_bMemDC ) 
		{
			if ( m_hDC != NULL )
			{
				// if the bitmap is validate, select it . otherwize set the pointer with NULL. Gzh.2004.02.02.
				if (AfxIsValidAddress(m_oldBitmap, sizeof(CBitmap)))  //m_hDC != NULL && m_oldBitmap && m_oldBitmap->m_hObject )
					SelectObject(m_oldBitmap);

				m_oldBitmap = NULL;
				
				m_bitmap.DeleteObject();
				DeleteDC();
			}
			
			CreateCompatibleDC( pDC );
			pDC->LPtoDP( &m_rect );
			
			if (bMask)
				m_bitmap.CreateBitmap(m_rect.Width(), m_rect.Height(), 1, 1, NULL);
			else
				m_bitmap.CreateCompatibleBitmap(pDC, m_rect.Width(), m_rect.Height());

			m_oldBitmap = SelectObject( &m_bitmap );
			
			SetMapMode( pDC->GetMapMode() );		        
			pDC->DPtoLP( &m_rect );
			SetWindowOrg( m_rect.left, m_rect.top );
		} 
		else
		{
			m_bPrinting = pDC->m_bPrinting;
			m_hDC       = pDC->m_hDC;
			m_hAttribDC = pDC->m_hAttribDC;
		}
		
		FillSolidRect(m_rect, pDC->GetBkColor());
	}

	BOOL CopyAs(DWORD dwRop = SRCCOPY)
	{
		if (m_bMemDC) 
		{
			// Copy the offscreen bitmap onto the screen.
			
			// 如果本MemDC无效,	直接返回
			if (GetSafeHdc() == NULL || m_pDC->GetSafeHdc() == NULL)
				return FALSE;
			
			m_pDC->BitBlt(m_rect.left, m_rect.top, m_rect.Width(), m_rect.Height(),
				this, m_rect.left, m_rect.top, dwRop);
			return TRUE;
		}

		return FALSE;
	}

	BOOL CopyFrom(DWORD dwRop = SRCCOPY, CDC* pDC = NULL)
	{
		// if (m_bMemDC) 
		{
			// Copy the offscreen bitmap onto the screen.
			
			// 如果本MemDC无效,	直接返回
			if (GetSafeHdc() == NULL || m_pDC->GetSafeHdc() == NULL)
				return FALSE;
			
			BitBlt(m_rect.left, m_rect.top, m_rect.Width(), m_rect.Height(),
				pDC->GetSafeHdc() ? pDC : m_pDC, m_rect.left, m_rect.top, dwRop);
			return TRUE;
		}
		
		return FALSE;
	}
	
	CMyMemDC(CDC* pDC, const CRect* pRect = NULL, UINT nBitcount = 0) : CDC()
	{
		ASSERT(pDC != NULL); 

		// Some initialization
		m_pDC = pDC;
		m_oldBitmap = NULL;
		m_bMemDC = !pDC->IsPrinting();

		// Get the rectangle to draw
		if (pRect == NULL) {
			pDC->GetClipBox(&m_rect);
		} else {
			m_rect = *pRect;
		}

		m_dwRop = SRCCOPY;
//		m_pFile = NULL;
		m_hFile = NULL;
		m_pBits = NULL;
		
		if (m_bMemDC) {
			// Create a Memory DC
			if (!CreateCompatibleDC(pDC))
			{
				TRACE(_T("Error"));
			}
			pDC->LPtoDP(&m_rect);

			if (nBitcount == 0)
			{
				m_bitmap.CreateCompatibleBitmap(pDC, m_rect.Width(), m_rect.Height());
			}
			else if (nBitcount != ((UINT)-1))
			{
				m_bitmap.CreateBitmap(m_rect.Width(), m_rect.Height(), 1, nBitcount, NULL);
			}

			// 如果无法生成MemDC, 则直接返回
			if (m_bitmap.m_hObject == NULL)
			{  	
				// m_pFile = new CFile(szVmfile, CFile::modeCreate|CFile::modeReadWrite);
				m_pbi = new BITMAPINFOHEADER;
				m_pbi->biSize			= sizeof (BITMAPINFOHEADER);
				m_pbi->biWidth			= abs(m_rect.Width());
				m_pbi->biHeight			= abs(m_rect.Height());
				m_pbi->biPlanes			= 1;
				m_pbi->biBitCount		= 32;
				m_pbi->biCompression	= BI_RGB;
				m_pbi->biSizeImage		= 0;
				m_pbi->biXPelsPerMeter	= 0;
				m_pbi->biYPelsPerMeter	= 0;
				m_pbi->biClrUsed		= 0;
				m_pbi->biClrImportant	= 0;

				m_pbi->biSizeImage = ((((m_pbi->biWidth * m_pbi->biBitCount) + 31) & ~31) >> 3) * m_pbi->biHeight;
				m_hFile = ::CreateFileMapping((HANDLE)0xFFFFFFFF,
					NULL, PAGE_READWRITE, 0, m_pbi->biSizeImage, NULL);
				
				if (m_hFile == NULL)
				{
					TRACE(_T("Error : Failed to create file mapping object!\n"));
				}

				HBITMAP hBitmap = ::CreateDIBSection(pDC->GetSafeHdc(),
					(LPBITMAPINFO)m_pbi, DIB_RGB_COLORS, &m_pBits, m_hFile, 0);
				
//				TRACE(_T("Save BITMAP size %ud, height %d\n"), m_pbi->biSizeImage, m_pbi->biHeight);
				
				if (hBitmap == NULL)
				{
					TRACE(_T("Error : Failed to create bitmap object!\n"));
					CloseHandle(m_hFile);
					m_hFile = NULL;
					m_hDC       = pDC->m_hDC;
					m_hAttribDC = pDC->m_hAttribDC;
					return;
				}
				
				m_bitmap.Attach(hBitmap);
			}

			m_oldBitmap = SelectObject(&m_bitmap);
			
			SetMapMode(pDC->GetMapMode());
			pDC->DPtoLP(&m_rect);
			SetWindowOrg(m_rect.left, m_rect.top);
		} else {
			// Make a copy of the relevent parts of the current DC for printing
			m_bPrinting = pDC->m_bPrinting;
			
			// Create a Memory DC
			if (!CreateCompatibleDC(pDC))
			{
				m_hDC       = pDC->m_hDC;
				m_hAttribDC = pDC->m_hAttribDC;
				return;
			}

			pDC->LPtoDP(&m_rect);
			if (nBitcount == 0)
			{
				m_bitmap.CreateCompatibleBitmap(pDC, m_rect.Width(), m_rect.Height());
			}
			else if (nBitcount != ((UINT)-1))
			{
				m_bitmap.CreateBitmap(abs(m_rect.Width()), abs(m_rect.Height()), 1, nBitcount, NULL);
			}
			
			// 如果无法生成MemDC, 则直接返回
			if (m_bitmap.m_hObject == NULL)
			{  	
				// m_pFile = new CFile(szVmfile, CFile::modeCreate|CFile::modeReadWrite);
				m_pbi = new BITMAPINFOHEADER;
				m_pbi->biSize			= sizeof (BITMAPINFOHEADER);
				m_pbi->biWidth			= abs(m_rect.Width());
				m_pbi->biHeight			= abs(m_rect.Height());
				m_pbi->biPlanes			= 1;
				m_pbi->biBitCount		= 32;
				m_pbi->biCompression	= BI_RGB;
				m_pbi->biSizeImage		= 0;
				m_pbi->biXPelsPerMeter	= 0;
				m_pbi->biYPelsPerMeter	= 0;
				m_pbi->biClrUsed		= 0;
				m_pbi->biClrImportant	= 0;
				
				m_pbi->biSizeImage = ((((m_pbi->biWidth * m_pbi->biBitCount) + 31) & ~31) >> 3) * m_pbi->biHeight;
				m_hFile = ::CreateFileMapping((HANDLE)0xFFFFFFFF,
					NULL, PAGE_READWRITE, 0, m_pbi->biSizeImage, NULL);
				
				if (m_hFile == NULL)
				{
					TRACE(_T("Error : Failed to create file mapping object!\n"));
				}
				
				HBITMAP hBitmap = ::CreateDIBSection(pDC->GetSafeHdc(),
					(LPBITMAPINFO)m_pbi, DIB_RGB_COLORS/*DIB_PAL_COLORS*/, &m_pBits, m_hFile, 0);
				
//				TRACE(_T("Save BITMAP size %ud, height %d\n"), m_pbi->biSizeImage, m_pbi->biHeight);
				
				if (hBitmap == NULL)
				{
					TRACE(_T("Error : Failed to create bitmap object!\n"));
					CloseHandle(m_hFile);
					m_hFile = NULL;
					m_hDC       = pDC->m_hDC;
					m_hAttribDC = pDC->m_hAttribDC;
					return;
				}
				
				m_bitmap.Attach(hBitmap);
			}
			
			m_oldBitmap = SelectObject(&m_bitmap);
			
			SetMapMode(pDC->GetMapMode());
			pDC->DPtoLP(&m_rect);
			SetWindowOrg(m_rect.left, m_rect.top);
		}

		// Fill background
		FillSolidRect(m_rect, pDC->GetBkColor());
	}
	
	~CMyMemDC()	
	{		
		if (m_bMemDC) 
		{
			// Copy the offscreen bitmap onto the screen.

			// 如果本MemDC无效,	直接返回
			if (GetSafeHdc() == NULL)
				return;

			if (m_hFile)
			{
				CloseHandle(m_hFile);
				m_hFile = NULL;
			}
			else if (m_pDC->GetSafeHdc())
			{
				m_pDC->BitBlt(m_rect.left, m_rect.top, m_rect.Width(), m_rect.Height(),
					this, m_rect.left, m_rect.top, m_dwRop);
			}
		} 
		else 
		{
			// All we need to do is replace the DC with an illegal value,
			// this keeps us from accidently deleting the handles associated with
			// the CDC that was passed to the constructor.			
//			m_hDC = m_hAttribDC = NULL;
			if (GetSafeHdc() == NULL)
				return;
			
			if (m_pDC->GetSafeHdc())
			{
				m_pDC->BitBlt(m_rect.left, m_rect.top, m_rect.Width(), m_rect.Height(),
					this, m_rect.left, m_rect.top, m_dwRop);
			}
			
			if (m_hFile)
			{
				CloseHandle(m_hFile);
				m_hFile = NULL;
			}
		}

// 		if (m_pFile != NULL)
// 		{
// 			delete m_pFile;
// 			m_pFile = NULL;
// 			DeleteFile(szVmfile);
// 		}
		
		// Swap back the original bitmap.
		if (AfxIsValidAddress(m_oldBitmap, sizeof(CBitmap)))
				SelectObject(m_oldBitmap);		
	}
	
	// Allow usage as a pointer	
	CMyMemDC* operator->() 
	{
		return this;
	}	

	// Allow usage as a pointer	
	operator CMyMemDC*() 
	{
		return this;
	}
};

#endif
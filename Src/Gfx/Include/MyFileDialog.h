﻿#if !defined(AFX_MYFILEDIALOG_H__51489A4A_3B70_4A6E_86D7_810296B13D31__INCLUDED_)
#define AFX_MYFILEDIALOG_H__51489A4A_3B70_4A6E_86D7_810296B13D31__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DSF.h"
#ifndef ULVWBASE_API
	#ifdef ULVWBASE‌_EXPORT
		#define ULVWBASE_API __declspec( dllexport )
	#else	
		#define ULVWBASE_API __declspec( dllimport )
	#endif
#endif
class ULVWBASE_API CMyFileDialog : public CFileDialog
{
	DECLARE_DYNAMIC(CMyFileDialog)

public:
	
	CMyFileDialog(BOOL bOpenFileDialog, // TRUE for FileOpen, FALSE for FileSaveAs
		LPCTSTR lpszDefExt = NULL,
		LPCTSTR lpszFileName = NULL,
		DWORD dwFlags = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		LPCTSTR lpszFilter = NULL,
		CWnd* pParentWnd = NULL,
		LPCTSTR lpszFile = NULL);

	~CMyFileDialog();

protected:	
	//{{AFX_MSG(CMyFileDialog)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:
	CString		m_strEntry;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYFILEDIALOG_H__51489A4A_3B70_4A6E_86D7_810296B13D31__INCLUDED_)

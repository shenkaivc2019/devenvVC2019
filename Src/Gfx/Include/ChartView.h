// ChartView.h: interface for the CChartView class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHARTVIEW_H__04026AB8_F852_488E_8AE9_B80CAB040C8D__INCLUDED_)
#define AFX_CHARTVIEW_H__04026AB8_F852_488E_8AE9_B80CAB040C8D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CustomizeView.h"

class CChartView : public CCustomizeView  
{
	DECLARE_DYNAMIC(CChartView)
public:
	CChartView(CULFile* pFile = NULL);
	virtual ~CChartView();

// Attributes
public:
	
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChartView)
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void SaveAsTempl(LPCTSTR pszFile);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CChartView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	
};

#endif // !defined(AFX_CHARTVIEW_H__04026AB8_F852_488E_8AE9_B80CAB040C8D__INCLUDED_)

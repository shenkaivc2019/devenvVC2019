#ifndef __ZOOMTRACKER_H
#define __ZOOMTRACKER_H
// Style Flags
enum StyleFlags
{
	solidLine = 1, dottedLine = 2, hatchedBorder = 4,
	resizeInside = 8, resizeOutside = 16, hatchInside = 32,
};

// Hit-Test codes
enum TrackerHit
{
	hitNothing = -1,
	hitTopLeft = 0, hitTopRight = 1, hitBottomRight = 2, hitBottomLeft = 3,
	hitTop = 4, hitRight = 5, hitBottom = 6, hitLeft = 7, hitMiddle = 8
};

/////////////////////////////////////////////////////////////////////////////
// CZoomRectTracker - simple rectangular tracking rectangle w/resize handles
class AFX_EXT_CLASS CZoomRectTracker
{
public:
// Constructors
	CZoomRectTracker();
	CZoomRectTracker(LPCRECT lpSrcRect, UINT nStyle);

// Attributes
	UINT m_nStyle;      // current state
	CRect m_rect;       // current position (always in pixels)
	CSize m_sizeMin;    // minimum X and Y size during track operation
	int m_nHandleSize;  // size of resize handles (default from WIN.INI)
	COLORREF m_clrHandle;	// Color of handle
// Operations
	void Draw(CDC* pDC) const;

	void GetTrueRect(LPRECT lpTrueRect) const;
	void GetRect(LPRECT lpRect) const;
	BOOL SetCursor(CWnd* pWnd, UINT nHitTest) const;
	BOOL Track(CWnd* pWnd, CPoint point, BOOL bAllowInvert = FALSE,	CWnd* pWndClipTo = NULL);
	BOOL TrackRubberBand(CWnd* pWnd, CPoint point, BOOL bAllowInvert = TRUE);
	int HitTest(CPoint point) const;
	int NormalizeHit(int nHandle) const;

	void MoveTracker(CWnd* pWnd, CPoint point, BOOL bAllowInvert = FALSE, CWnd* pWndClipTo = NULL);

// Overridables
	virtual void DrawTrackerRect(LPCRECT lpRect, CWnd* pWndClipTo, CDC* pDC, CWnd* pWnd);
	virtual void AdjustRect(int nHandle, LPRECT lpRect);
	virtual void OnChangedRect(const CRect& rectOld);
	virtual UINT GetHandleMask() const;
	void GetHandleRect(int nHandle, CRect* pHandleRect) const;

// Implementation
public:
	virtual ~CZoomRectTracker();

protected:
	BOOL m_bAllowInvert;    // flag passed to Track or TrackRubberBand
	CRect m_rectLast;
	CSize m_sizeLast;
	BOOL m_bErase;          // TRUE if DrawTrackerRect is called for erasing
	BOOL m_bFinalErase;     // TRUE if DragTrackerRect called for final erase

	// implementation helpers
	int HitTestHandles(CPoint point) const;
	void GetModifyPointers(int nHandle, int**ppx, int**ppy, int* px, int*py);
	virtual int GetHandleSize(LPCRECT lpRect = NULL) const;
	BOOL TrackHandle(int nHandle, CWnd* pWnd, CPoint point, CWnd* pWndClipTo);
	void Construct();
};

#endif

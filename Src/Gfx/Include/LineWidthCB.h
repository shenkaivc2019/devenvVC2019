﻿#if !defined(AFX_LINESTYLECB_H)

#ifndef MFCEXT_API
	#ifdef MFCEXT_EXPORT
		#define MFCEXT_API __declspec( dllexport )
	#else	
		#define MFCEXT_API __declspec( dllimport )
	#endif
#endif

#define AFX_LINESTYLECB_H

#define		WCB_MAX_LINES			4
#define		WCB_MAX_LINE_NAME		16

struct LineWidthAndName
{
	LineWidthAndName()
	{
		ZeroMemory(this, sizeof(LineWidthAndName));
	};

	LineWidthAndName(int nLineStyle, LPCTSTR cpName)
	{
		ZeroMemory(this, sizeof(LineWidthAndName));
		m_nLineStyle = nLineStyle;
		_tcsncpy(m_cLineName, cpName, WCB_MAX_LINE_NAME);
	};

	int		m_nLineStyle;
	TCHAR	m_cLineName[WCB_MAX_LINE_NAME];
};

class MFCEXT_API CLineWidthCB : public CComboBox
{
public:
	CLineWidthCB();

public:
	void Initialize();
	void Reinitialize();
	static void SetLineWidths(BYTE* pWidth, int nCount = WCB_MAX_LINES);
	static int m_nMapMode;
	static LineWidthAndName	m_Lines[WCB_MAX_LINES];

private:
	BOOL m_bInitialized;
	

private:

public:
	//{{AFX_VIRTUAL(CLineWidthCB)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_VIRTUAL

public:
	virtual ~CLineWidthCB();

protected:
	//{{AFX_MSG(CLineWidthCB)
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

#endif 

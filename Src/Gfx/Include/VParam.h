// VParam.h: interface for the CVParam class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VPARAM_H__22870B19_3E02_498E_890F_BB17974B9A8C__INCLUDED_)
#define AFX_VPARAM_H__22870B19_3E02_498E_890F_BB17974B9A8C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CVParam  
{
public:
	CVParam(const VARIANT vParam);
	CVParam(LPCTSTR pszParam, TCHAR s = _T(','));
	void Init(LPCTSTR pszParam, TCHAR s = _T(','));
	
	virtual ~CVParam();

	BOOL SimpleScanf(LPCTSTR lpszText, LPCTSTR lpszFormat, ...);
	BOOL SimpleFloatParse(LPCTSTR lpszText, double& d);
	BOOL GetP(int i, int& iVal);
	BOOL GetP(int i, UINT& uiVal);
	BOOL GetP(int i, long& lVal);
	BOOL GetP(int i, double& dblVal);
	BOOL GetP(int i, CString& strVal);

	CStringArray m_params;
};

#endif // !defined(AFX_VARPARAM_H__22870B19_3E02_498E_890F_BB17974B9A8C__INCLUDED_)

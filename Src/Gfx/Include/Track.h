// Track.h: interface for the CTrack class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TRACK_H__F8E108D6_F46A_419F_A4A1_FD59E81FFAC5__INCLUDED_)
#define AFX_TRACK_H__F8E108D6_F46A_419F_A4A1_FD59E81FFAC5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ITrack.h"
#include "ULCOMMDEF.H"
#include "Line.h"
#include "PrintInfo.h"
#ifndef _VIWCONTROL
#include "BaseMarker.h"
#endif
#include "XMLSettings.h"
#include "Marker.h"
#include "Mark.h"
#include "IProject.h"

#define TI_FIRST	0x0001
#define TI_LAST		0x1000
#define TI_FL		0x1001

// bao add 2011/06/07 first reading 标签
typedef struct tagFIRST
{
	CULPrintInfo*	pInfo;
	int				nX;
	long			lFDepth;
	BOOL			bMode;
	CString			strText;
	BOOL			nBkMode;
	CPen*			pPen;
	int				nDirection;
}FIRST, *PFIRST, *LPFIRST;

// 连接曲线
typedef struct tagLINKCURVE
{
	CRect rectCurve;
	CString CurveType;
}LINKCURVE, *PLINKCURVE, *LPLINKCURVE;

// bao add 2011/6/9 控制输出曲线 连接曲线用
typedef struct tagCONTROLNUM
{
	int nLeft;
	int nRight;
}CONTROLNUM, *PCONTROLNUM, *LPCONTROLNUM;

class CCurve;
class CCurveData;
class CMRTObject;
class CTrack : public ITrack 
{
	DECLARE_ULI(IID_ITrack, ITrack)
		
public:
	CTrack();
	virtual ~CTrack();
public:
	ULMTSTR		GetName() { return (LPTSTR)(LPCTSTR)m_strName; }
	ULMETHOD	SetName(LPCTSTR lpszName) { m_strName = lpszName; return 0; }

	ULMINT		GetType() { return m_nType; }
	ULMETHOD	SetType(int nType);

	ULMDWD		GetColor() { return m_crColor; }
	ULMETHOD	SetColor(COLORREF crColor) { m_crColor = crColor; return 0; }

	ULMINT		GetLineStyle() { return	m_nLineStyle; }
	ULMETHOD	SetLineStyle(int nLineStyle) { m_nLineStyle = nLineStyle; return 0; }

	ULMPTR		GetGridProp() {	return &m_GridInfo; }
	ULMETHOD	SetGridProp(void* pGridProp);
	ULMETHOD	GetDrawSel(void* pDrawSel);

	ULMINT		GetVertGridCount()
	{
		return  m_GridInfo.vLineInfo.nGroupCount * m_GridInfo.vLineInfo.nLineCount;
	}

	void		ChangeGridCount(int nCount0);

	ULMETHOD	GetHeadRect(LPRECT lpRect, BOOL bPrint);
	ULMETHOD	SetHeadRect(LPRECT lpRect)
	{
		m_rcHead.CopyRect(lpRect);
		m_rcHead.NormalizeRect();
		return UL_NO_ERROR;
	}

	ULMETHOD	SetHeadRectTB(LPRECT lpRect)
	{
		m_rcHead.top = lpRect->top;
		m_rcHead.bottom = lpRect->bottom;
		return UL_NO_ERROR;
	}

	ULMETHOD SetHeadRectTB(int nTop, int nBottom)
	{
		m_rcHead.top = nTop;
		m_rcHead.bottom = nBottom;
		return UL_NO_ERROR;
	}

	ULMETHOD	SetHeadRectLR(LPRECT lpRect)
	{
		m_rcHead.left = lpRect->left;
		m_rcHead.right= lpRect->right;
		return UL_NO_ERROR;
	}

	ULMETHOD GetRegionRect(LPRECT lpRect, BOOL bPrint);
	ULMETHOD SetRegionRect(LPRECT lpRect, BOOL bLR = TRUE)
	{
		if(bLR)
		{
			m_rcRegion.left = lpRect->left;
			m_rcRegion.right = lpRect->right;
		}
		else
		{
			m_rcRegion.top = lpRect->top;
			m_rcRegion.bottom = lpRect->bottom;
		}
		m_rcRegion.NormalizeRect();
		return UL_NO_ERROR;
	}

	ULMINT		RegionLeft() { return m_rcRegion.left; }
	ULMINT		RegionRight() { return m_rcRegion.right; }
	ULMINT		RegionWidth() { return m_rcRegion.Width(); }
	ULMINT		RegionTop() { return m_rcRegion.top; }
	ULMINT		RegionBottom() { return m_rcRegion.bottom;	}
	ULMINT		RegionHeight() { return m_rcRegion.Height(); }

	ULMETHOD	GetIUnits(CUnits* pIUnits);
	ULMBOOL		IsColorPrint();
	ULMBOOL		IsTimeShow() { return m_bShowTime;	}
	ULMBOOL		IsDepthShow(){ return m_bShowDepth;	}
	ULMBOOL		IsFirstTrack() { return (m_nTrackIndex & TI_FIRST); }
	ULMBOOL		IsLastTrack() { return (m_nTrackIndex & TI_LAST); }
	ULMBOOL		IsFirstOrLastTrack() { return (m_nTrackIndex & TI_FL); }
	ULMINT		GetGapDepthCount()
	{ 
		if (m_pGapDepthArray)
			return m_pGapDepthArray->GetSize();
		return 0;
	}

	ULMLNG		GetGapDepth(int nIndex)
	{ 
		if (m_pGapDepthArray)
			return m_pGapDepthArray->GetAt(nIndex);
		return INVALID_DEPTH;
	}

	ULMBOOL		IsGapRemain() { return m_bGapRemain; }
	ULMETHOD	IsGapRemain(BOOL bGapR) { m_bGapRemain = bGapR; return 0; }
	ULMINT		GetGapRemain() { return m_nGapRemain; }
	ULMETHOD	SetGapRemain(int nGapR) { m_nGapRemain = nGapR; return 0; }
	ULMFLT		GetRatioY() 
	{
		if (m_nDriveMode == UL_DRIVE_TIME)
		{
			return m_fRatioTime;
		}

		return m_fRatioY; 
	}
	ULMPTR		GetDepthCurve() { return m_pDEPT; }
	ULMETHOD	SetGapPos(int nPos) { m_nGapPos = nPos; return 0; }

	inline ULMINT GetCoordinateFromDepth(long lDepth)
	{   // MM_LOMETRIC，比例尺大小m_nRatio

		return (int)(-1 * (lDepth - m_lStartDepth) / m_fRatioY);
	}

	inline ULMINT GetCoordinateFromTime(long lTime)
	{

		if (m_bTimeTop)
		{
			return (int)(-1 * (lTime - m_lStartTime)  / m_fRatioTime + m_lStartTimeLogic);
		}
		else
		{
			return (int)((lTime - m_lStartTime)  / m_fRatioTime + m_lStartTimeLogic);
		}
			
	}

	inline ULMLNG GetTimeFromCoordinate(int nPos)
	{

		if (m_bTimeTop)
		{
			return  (-1 * (nPos - m_lStartTimeLogic) * m_fRatioTime) + m_lStartTime;
		}
		else
		{
			return  ((nPos - m_lStartTimeLogic) * m_fRatioTime) + m_lStartTime;
		}
	}
	inline ULMLNG GetDepthFromCoordinate(int nPos)		 
	{
		return  (-1 * (nPos * m_fRatioY) + m_lStartDepth);
	}
	
	ULMPTR		GetCurveList() { return &m_vecCurve; }
	ULMETHOD	GetAllCurve(CURVES& vecCurve)
	{
		//vecCurve = m_vecCurve;
	/*	for(int i=0; i<m_vecCurve.size(); i++)
		{
			vecCurve.push_back((ICurve*)m_vecCurve[i]);
		}*/
		return UL_NO_ERROR;
	}
	
	ULMINT		GetSeledCurve(CURVES& vecCurve);	// 得到当前井道内选中曲线

	ULMPTR	FindCurve(ICurve* pCurve, BOOL bLeft);


	// 得到测井状态参数
	ULMINT		GetDriveMode()	{ return m_nDriveMode; }
	ULMINT		GetWorkMode()	{ return m_nWorkMode; }

	ULMETHOD	GetLogMode(int& nDirection, int& nWorkMode, int& nDriveMode)
	{
		// nDirection = m_nLogDirection;
		nWorkMode = m_nWorkMode;
		nDriveMode = m_nDriveMode;
		return UL_NO_ERROR;
	}

	// 系统提供的绘画模式
	ULMETHOD	DrawTitleColor(CDC* pDC, LPRECT lpRect, COLORREF clrStart = RGB(94,146,223), 
					COLORREF clrEnd =  RGB(255,255,255));

	// 系统提供的打印模式
	ULMETHOD	PrintGaps(CULPrintInfo* pInfo, long lMaxDepth, long lMinDepth);
	ULMETHOD	PrintEdge(CULPrintInfo* pInfo, long lMaxDepth, long lMinDepth);
	
	ULMINT		GetPrintDepthAngle();
	ULMLNG		GetTD()
	{
		return m_lTD;
	}

	ULMPTR	    GetAzim()
	{
		return m_pAZIM;
	}

	ULMPTR		GetDev();
	ULMETHOD    SetDev(ICurve* pDEV);
	ULMETHOD    SetAzim(ICurve* pAZIM);
	ULMDBL      GetXDepth(long lDepth);
	ULMETHOD	DrawPat(CDC* pDC, LPRECT lpRect, LPCTSTR pszPat, COLORREF clrFore, COLORREF clrBkg);

	ULMINT GetGridWidth(int nGrid, BOOL bPrint);

public:
	static long m_lTD;
	CPointArray m_ptArray;
public:
	// 属性
	void		SetName(CString strName) { m_strName = strName; }
	void		SetName(_variant_t& var) { m_strName = (LPCTSTR)(_bstr_t)var; }
	void		SetLogMode(int nWorkMode, int nDriveMode);
	void		SetDepthRange(long lStartDepth, long lEndDepth);
	void		SetModeShow(BOOL bTime,BOOL bDepth);
	void		UpdateGrids();

	// 曲线
	void		AddCurve(CCurve* pCurve);
	void		InsertCurve(CCurve* pCurve, int nPos);
	ULMBOOL		RemoveCurve(ICurve* pCurve);
	ULMBOOL		DelCurve(CCurve* pCurve);
	void		ExchangeCurvePosition(int nFrom, int nTo);
	void		ApplyNewCurve(CCurve* pCurve);
	void		ApplyDepthCurve(CCurve* pCurve, CCurveData* pData);
	void		ApplyNewData(CCurveData* pData);
	int			GetSeledCurve(CPtrArray& arrCurve);
	BOOL		IsCurveSelected();
	CCurve*		GetFirstSeledCurve();
	CCurve*		GetCurve(LPCTSTR pszCurve);
	int			GetCurveXPos(int nCurve, long lDepth);		// 得到某曲线指定深度的坐标
	int			GetCurveXPos(CCurve* pCurve, long lDepth);
	ULMINT		GetDepthTitle(CStringArray* pTitles = NULL);
	ULMPTR		GetFont(UINT nIndex = ftCT);

	// 绘图
	void		DrawHead(CDC* pDC, BOOL bTitle = TRUE);
	void		DrawHorzGrid(CDC *pDC, long lStartDepth, long lEndDepth);
    void		DrawVertGrid(CDC *pDC, long lStartDepth, long lEndDepth);
	void		DrawVertGridLinear(CDC* pDC, int nStartPos, int nEndPos);
	void		DrawVertGridLog(CDC* pDC, int nStartPos, int nEndPos);
	void		DrawTrack(CDC* pDC, long lStartDepth, long lEndDepth, int nMode);
	void		DrawFill(CDC* pDC, long lStartDepth, long lEndDepth, int nMode);
	void		DrawMark(CDC *pDC, long lStartDepth, long lEndDepth);
//	void		DrawCurveFlag(CDC *pDC, long lStartDepth, long lEndDepth);//xwh 2013-9-5
	void		DrawThumb(CDC* pDC, LPRECT lpRect, long ls, double dDelta, int nPoint);
	void		DrawBackCurve(CDC *pDC, long lStartDepth, long lEndDepth, BOOL bLogDraw);
	void		DrawResults(CDC* pDC, long nStartDepth, long nEndDepth, CULPrintInfo* pInfo = NULL);
	void		DrawResult(CDC* pDC, LPRECT lpRect, LPCTSTR pszResult, BOOL bSelected);

	// 鼠标
	BOOL		PtInTrack(CPoint pt);			// 逻辑坐标，是否在当前井道
	BOOL		PtInTrackHead(CPoint pt);		// 是否在当前井道头
	int			PtInCurvePos(CPoint pt);		// 所在曲线位置
	BOOL		PtInTrackDragWnd(CPoint pt);	// 是否在井道拖动区

	BOOL		HitHeadTest(CPoint point, int& nIndex, BOOL& bLeftOrRight);
	BOOL		HitTest(CPoint point, int& nCurveIndex, int& nCurveValueIndex, BOOL bSelect = TRUE);
	CCurve*		CurveHitHeadTest(CPoint pt, int & nPos);	// 当前井道头选中曲线
	
	// 打印
	void	PrintHead(CULPrintInfo* pPrintInfo);
	void	PrintVertGrid(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth);
	void	PrintVertGridLinear(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth);
	void	PrintVertGridLog(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth);
	void	PrintHorzGrid(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth);
	void	PrintMark(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth);
	void	PrintTrack(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth);
	void	PrintFill(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth);
	void	StaticPrint(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth);
	void	PrintCurveFlags(CULPrintInfo* pInfo, long lDepth, int nCurve, BOOL bMode);
	BOOL	PrintCurveFirst(CULPrintInfo* pInfo, long lMaxDepth, long lMinDepth, BOOL bMode);
	BOOL	DrawArrow(CULPrintInfo* pInfo, int nX, long lDepth, BOOL bMode, CString strText, int nBkMode = TRANSPARENT,
					  CPen* pPen = new CPen(PS_SOLID, 1, RGB(0, 0, 0)) , int nDY = 0 , int nDX = 0);
	
	void	SetProject(IProject* pProject);
	// bao 
	BOOL	IsDrawFirst(CRect rect, int& arrowDirection, int& iCount, int LoR, int nDirection); // 判断是否已有标签并指明箭头应该指明的方向
	// arrowDirection: 0：左上斜 1：右上斜 2：右下斜 3：左下斜 4：左指 5：右指 6：下指
	// LoR：箭头左指或右指

	BOOL	DrawCurveFirst(CULPrintInfo* pInfo, int nX, long lFDepth, BOOL bMode, CString str,
						   CPen* pPen, int arrowDirection = 4, int iCount = 0); // 画first reading
	BOOL	SortCurveFirst(); // 排序first reading	

	// add by bao 12 12 24
	void		DrawHorzGridTime(CDC *pDC, long lStartTime, long lEndTime);
    void		DrawVertGridTime(CDC *pDC, long lStartTime, long lEndTime);
	void		DrawVertGridLinearTime(CDC* pDC, int nStartPos, int nEndPos);
	void		DrawVertGridLogTime(CDC* pDC, int nStartPos, int nEndPos);
	void		DrawTrackTime(CDC* pDC, long lStartTime, long lEndTime, int nMode);
	void		DrawFillTime(CDC* pDC, long lStartTime, long lEndTime, int nMode);
	void		DrawMarkTime(CDC *pDC, long lStartTime, long lEndTime);
	void		DrawThumbTime(CDC* pDC, LPRECT lpRect, long ls, double dDelta, int nPoint);
	void		DrawBackCurveTime(CDC *pDC, long lStartTime, long lEndTime, BOOL bLogDraw);
	void		DrawResultsTime(CDC* pDC, long nStartTime, long nEndTime, CULPrintInfo* pInfo = NULL);
	void		DrawResultTime(CDC* pDC, LPRECT lpRect, LPCTSTR pszResult, BOOL bSelected);

	void		PrintVertGridTime(CULPrintInfo* pInfo, double fMaxTime, double fMinTime);
	void		PrintVertGridLinearTime(CULPrintInfo* pInfo, double fMaxTime, double fMinTime);
	void		PrintVertGridLogTime(CULPrintInfo* pInfo, double fMaxTime, double fMinTime);
	void		PrintHorzGridTime(CULPrintInfo* pInfo, double fMaxTime, double fMinTime);
	ULMETHOD	PrintGapsTime(CULPrintInfo* pInfo, long lMaxTime, long lMinTime);
	ULMETHOD	PrintEdgeTime(CULPrintInfo* pInfo, long lMaxTime, long lMinTime);
	ULMINT		GetTimeTitle(CStringArray* pTitles = NULL);

public:
	void		AddCurves(CStringList* pList);
	int			RemoveCurve(int index);
	void		ReplaceCurve(int nIndex, CCurve* pCurve);
	void		AssignTrack(CTrack* pTrack);

	virtual void Serialize(CXMLSettings& xml);
	virtual void SerializeMark(CXMLSettings& xml);
public:
	// basic attribute
	CString		m_strName;          // 井道名称
	int			m_nType;			// 井道类型
	
	// drawing set attribute
	COLORREF	m_crColor;			// 井道颜色
	int			m_nLineStyle;		// 井道线型
	int			m_nLineWidth;		// 井道线宽
	GridProp	m_GridInfo;			// 井道格线信息

	float		m_fRatioX;			// 水平比例
	float		m_fRatioY;			// 垂直比例
	float       m_fRatioTime;       // 时间比例   gj 20121217

	CRect		m_rcRegion;			// 该测井道的所占的区域信息
	CRect		m_rcHead;			// 该测井头的所占的区域信息

	__int64	    m_lStartDepth;		// 用户设置的开始深度
	long		m_lEndDepth;		// 用户设置的结束深度
	long		m_lStartTime;
	long		m_lEndTime;
	long        m_lStartTimeLogic;  // 起始时间对应的开始逻辑坐标 gj 20121217
	BOOL		m_bTimeTop;         // 判断时间驱动模式 gj 20121219

	// logging information
	BOOL		m_bShowTime;		// 深度驱动显示时间
	BOOL		m_bShowDepth;		// 时间驱动显示深度
	int			m_nWorkMode;		// 工作模式
	int			m_nDriveMode;		// 驱动模式

	std::vector<CCurve*> m_vecCurve;			// 曲线链表

	BOOL		m_bMouseOverTitle;

	// printing
	int			m_nGapPos;			// 缺口索引
	GapDepthArray* m_pGapDepthArray;// 记录缺口的深度位置，用于打印缺口时
	int         m_nTrackIndex;		// 标记是否是第一或最后一个井道
	BOOL        m_bPrintGap;		// 是否打印缺口

	BOOL        m_bGapRemain;		// 碰到分页时缺口没打完
	int         m_nGapRemain;		// 缺口剩余的长度
	
	BOOL        m_bRTPrint;			// 实时打印标识
	int         m_nGapSize;			// 缺口的大小，单位：0.1mm
	int         m_nGapInterval;		// 缺口时间间隔，单位：s
	int			m_nPrintCount;

	CCurve*		m_pDEPT;			// 深度曲线
	
	CString		m_strDEV;
	CString     m_strAZIM;
	CCurve*		m_pDEV;				// 井斜曲线
	CCurve*		m_pAZIM;			// 方位曲线

	IProject*	m_pProject;
	CMarker     m_Marker;           // 射孔标签

#ifndef _VIWCONTROL
	CBaseMarker m_TestMarker;       // 测试标签
#endif

	// bao
	BOOL		m_bLogFlags;		// 对数格线标签
	BOOL	m_bBracketFlags;		// 是否只取括号内的曲线名

	// bao add 2011/06/07 解决first reading重叠
 	CArray<FIRST, FIRST> m_firstFlags;
 	static CArray<CRect, CRect> m_flagRects;

	// bao add 使曲线相连接 2011/06/08
	CArray<LINKCURVE, LINKCURVE> m_CurveHeadRects; // 输出
	CArray<LINKCURVE, LINKCURVE> m_CurveHeadRectsPrint; // 打印
	// bao add 2011/6/9 控制输出曲线数目
	CArray<CONTROLNUM, CONTROLNUM> m_ControlNum;
	// bao add 2011/6/9
	std::vector<CCurve*> m_DrawCurve;			// 暂时不画曲线链表
	//add by xwh 2013/03/28 区分时间驱动深度驱动参数保存
	double			m_dHorzGridTime[3];
	double			m_dHorzGridDepth[3];
};

#endif // !defined(AFX_TRACK_H__F8E108D6_F46A_419F_A4A1_FD59E81FFAC5__INCLUDED_)

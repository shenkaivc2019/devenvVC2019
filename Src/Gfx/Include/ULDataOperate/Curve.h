#pragma once

#ifndef  __CURVE_H__
#define  __CURVE_H__

#include "ICurve.h"
#include "CurveData.h"
#include "PlotImpl.h"
#include <math.h>
#include "XMLSettings.h"
#include "IComConfig.h"
//#include "DBFilterCurveInfo.h"
#ifndef DATAOPERATE_API
	#ifdef DATAOPERATE_EXPORT
		#define DATAOPERATE_API __declspec( dllexport )
	#else
		#define DATAOPERATE_API __declspec( dllimport )
	#endif
#endif

typedef const unsigned short NEWBY;
#define NB_TRACK	0x0000
#define NB_TOOL		0x0001
#define NB_FILE		0x0002
#define NB_CLIP		0x0004
#define NB_TEMP		0x0008

class CTrack;
class CULFilter;                      
class CCurveFilter;             
class DATAOPERATE_API CCurve : public ICurve
{
	friend class CCurveData;

	DECLARE_ULI(IID_ICurve, ICurve)

public:
    CCurve(NEWBY nbType = NB_TOOL, VARTYPE nValType = VT_R4,
		CXMLNode* pXMLNode = NULL, LPCTSTR lpszCurveName = _T(""));
	CCurve(CCurve* pSrcCurve, BOOL bPerforation);
    virtual ~CCurve();

	void Assign(CCurve& srcCurve);
	void RemoveSelf();
	void SetDirection(int nDirection);
	

	ULMETHOD GetCurveProp(void* pCurveProp);
	ULMETHOD SetCurveProp(void* pCurveProp);

/* ------------------- *
 *  设置曲线数据源
 * ------------------- */
	void		AttachData(CCurveData* pData);	
	CCurveData*	DetachData();
	void		ReleaseData();
	void		CopyData(CCurveData* pData, BOOL bReverse = FALSE);
	void		MoveData(CCurveData* pData);


/* ------------------- *
 * 曲线绘制和打印
 * ------------------- */
	HRESULT		Draw(CDC *pDC , long nStartDepth , long nEndDepth);
	HRESULT	    LogDraw(CDC* pDC, long lStartDepth, long lEndDepth);
	HRESULT	    DrawHeadCurve(CDC*pDC,CRect rc,BOOL bDrawLine=TRUE);
	HRESULT		Print(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth);
	HRESULT		LogPrint(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth);
	HRESULT		PrintHeadCurve(CULPrintInfo* pInfo, LPRECT lpRect);

	HRESULT		GetCurveXPos(long lDepth, BOOL bRoll = TRUE);
	HRESULT		DrawThumb(CDC* pDC, LPRECT lpRect, long lStart, double dDelta, int nPoint);

public:

inline ULMPTR GetData()
{
	return m_pData;
}

inline ULMINT GetCurveMode()
{
	if (m_pData)
		return m_pData->m_nMode;
	
	return m_nMode;
}

inline ULMETHOD SetCurveMode(int nCurveMode)
{ 
	m_nMode = nCurveMode;
	if (m_pData)
	{
		m_pData->m_nMode = nCurveMode; 
		return UL_NO_ERROR;
	}
	
	return UL_ERROR;
}

inline void InvertCurveDirection()
{
	if (m_pData)
	{
		m_pData->Reverse();
		AutoDirection();
		m_pData->SetCurDisplayPosSame(0);
	}
}

//////////////////////////////////////////////////////////////////////
//
//  Curve drawing properties
//
//////////////////////////////////////////////////////////////////////

inline ULMETHOD SetName(LPCTSTR lpszName)
{ 
	m_strName = lpszName;
	if (m_nbType)
	{
		m_strData = lpszName;
		if (m_pData)
			m_pData->m_strName = lpszName;
	}

	return 0; 
}
	
inline ULMETHOD SetSource(LPCTSTR lpszSrc)
{ 
	m_strSource = lpszSrc; 
	return 0; 
}
	
inline ULMTSTR Name()
{ 
	return (LPTSTR)(LPCTSTR)m_strName; 
}

inline ULMBOOL IsDepthCurve()
{
	if (m_bDepth)
		return TRUE;
//	CString strName = m_strName;
//	if (strName.GetLength() < 4)
//		return FALSE;
//	strName = strName.Left(4);
//	strName.MakeUpper ();

//	if (strName.CompareNoCase(_T("DEPT")) && strName.CompareNoCase(_T("DEPTH")))
//		return FALSE;
	CString strName = m_strData;
	strName.MakeUpper ();

	if (strName.CompareNoCase(_T("DEPT")) && strName.CompareNoCase(_T("DEPTH")) && strName.CompareNoCase(_T("WELLDEPTH")))
		return FALSE;		
	return TRUE;
}

inline ULMBOOL IsTimeCurve()
{
	if (m_strName.CompareNoCase(_T("TIME")) && m_strName.CompareNoCase(_T("ETIME")))
		return FALSE;
	else
		return TRUE;
}

inline ULMETHOD GetName(LPTSTR lpszName)
{ 
	_tcscpy(lpszName, m_strName);
	return 0; 
}

inline ULMETHOD GetSource(LPTSTR lpszSrc)
{ 
	//_tcscpy(lpszSrc, m_strSource); 
	_tcscpy(lpszSrc, m_strData); 
	return 0; 
}

inline ULMETHOD SetCurDisplayPos(int nPos)
{ 
	m_nCurDisplayPos = nPos; 
	return 0; 
}
inline ULMETHOD SetCurPrintPos(int nPos)
{ 
	m_nCurPrintPos = nPos; 
	return 0; 
}

inline ULMINT IncCurDisplayPos(int nPosAdd) 
{ 
	m_nCurDisplayPos += nPosAdd;
	return m_nCurDisplayPos; 
}

inline ULMINT AdjCurDisplayPos(int nCount, int nAdjust = 21)
{
	if (nCount > nAdjust)
		m_nCurDisplayPos += (nAdjust - 1);
	if (m_nCurDisplayPos < 0)
		m_nCurDisplayPos = 0;
	return m_nCurDisplayPos;
}
	
inline ULMINT IncCurPrintPos(int nPosAdd)
{ 
	InterlockedExchangeAdd(&m_nCurPrintPos, nPosAdd);
	return m_nCurPrintPos; 
}

ULMINT GetPointCountPerDepth();	// for printing

inline ULMETHOD Delete(BOOL bDel) { m_bDelete = bDel;	return 0;	}
inline ULMETHOD Select(BOOL bSel) { m_bSelected = bSel; return 0;	}
inline ULMETHOD Visible(BOOL bVisible)
{
	if (bVisible)
		m_dwStyle |= CS_VISIBLE;
	else
		m_dwStyle &= ~CS_VISIBLE;
	return 0;
}

inline ULMETHOD Print(BOOL bPrint)
{
	if (bPrint)
		m_dwStyle |= CS_PRINT;
	else
		m_dwStyle &= ~CS_PRINT;
	return 0;
}

inline ULMETHOD Rewind(BOOL bRewind)
{
	if (bRewind)
		m_dwStyle |= CS_REWIND;
	else
		m_dwStyle &= ~CS_REWIND;
	
	return 0;
}

inline ULMBOOL	IsName(LPTSTR lpszName) { return (m_strName == lpszName); }
inline ULMBOOL	IsDelete()  { return m_bDelete;		}
inline ULMBOOL	IsSelect()  { return ((!CCurve::m_bSavingbmp) && m_bSelected);	}
inline ULMBOOL	IsVisible() { return (m_dwStyle & CS_VISIBLE);	}
inline ULMBOOL IsPrint()	
{ 
	if (m_pData)
		return (m_dwStyle & CS_VISIBLE);
	return FALSE;
}

inline ULMBOOL IsRewind()
{
	return (m_dwStyle & CS_REWIND) ? TRUE : FALSE;
}
// 2023.12.26 发现LVwidt总是1 Start
//inline ULMINT	LineWidth()					{ return LVWidth(m_nWidth);	}
//inline ULMINT	LinePWidth()				{ return LPWidth(m_nWidth); }	
ULMINT	LineWidth();
ULMINT	LinePWidth();
// 2023.12.26 发现LVwidt总是1 End
inline ULMINT	LineStyle()					{ return m_nStyle;	}
inline ULMDWD	Color()						{ return m_crColor;	}
inline ULMINT	LeftGrid()					{ return m_nLeftGrid;	}
inline ULMINT	RightGrid()					{ return m_nRightGrid;	}
inline ULMINT	LeftMargin(BOOL bPrint);
inline ULMINT   RightMargin(BOOL bPrint);
inline ULMINT	GetPat()					{ return m_nPat;}
inline ULMETHOD SetPat(unsigned int nPat)	{ m_nPat = nPat; return UL_NO_ERROR;}
inline ULMINT	GetStretch()				{ return m_nStretch;}
inline ULMETHOD SetStretch(unsigned int nStretch){m_nStretch = nStretch; return UL_NO_ERROR;} 
inline ULMINT	GetMaxRollCount()			{ return m_nMaxRollCount;}
inline ULMETHOD SetMaxRollCount(int nCount)	{ m_nMaxRollCount = nCount; return UL_NO_ERROR;}
inline ULMINT	GetCurDisplayPos()	{ return m_nCurDisplayPos;		}
inline ULMINT	GetCurPrintPos(){ 
	if (--m_nCurPrintPos < 0)
	m_nCurPrintPos = 0;
	return m_nCurPrintPos;	
}	
inline ULMINT  GetCurGapPos() { return m_nCurGapPos; }

inline ULMDBL  LeftVal()	{ return m_LeftValue;	}
inline ULMDBL  RightVal()	{ return m_RightValue;	}
inline ULMDBL  LRdVal()		{ return (m_LeftValue - m_RightValue);	}
inline ULMDBL  RLdVal()		{ return (m_RightValue- m_LeftValue);	}
// 2020.11.24 ltg Start
inline ULMDBL  LeftValMapClr()		{ return m_LeftValueMapClr;	}
inline ULMDBL  RightValMapClr()		{ return m_RightValueMapClr;	}
inline ULMDBL  LRdValMapClr()		{ return (m_LeftValueMapClr - m_RightValueMapClr);	}
inline ULMDBL  RLdValMapClr()		{ return (m_RightValueMapClr - m_LeftValueMapClr);	}
// 2020.11.24 ltg End
inline ULMLNG	GetBKBegin(){ return m_lBKBegin;	}
inline ULMLNG	GetBKEnd()	{ return m_lBKEnd;		}

inline ULMINT LeftDotNum()	{ return m_nLeftDotNum;	}
inline ULMINT RightDotNum()	{ return m_nRightDotNum;}
inline ULMINT GetIndex12()
{
	int nIndex = 0;
	if (m_pData)
	{
		nIndex = m_wIndex2*m_pData->GetPointFrame() + m_wIndex1;
		if (nIndex >= m_pData->m_nPointArray)
			nIndex = m_pData->m_nPointArray - 1;
	}

	return nIndex;
}

int TestCurveBound();

ULMINT GetTitle(CStringArray* pTitles);
ULMINT CalcTitle(DWORD dwStyle = CS_VISIBLE);


void removeLastZero(char* numstr)
{
	if (NULL == strchr(numstr, '.'))
		return;
	
	int length = strlen(numstr);
	for (int i = length - 1; i > 0; --i)
	{
		if ('\0' == numstr[i])
		{
			continue;
		}
		else if ('0' == numstr[i])
		{
			numstr[i] = '\0';
		}
		else if ('.' == numstr[i])// 小数点之后全为零
		{
			numstr[i] = '\0';
			break;
		}
		else// 小数点后有非零数字
		{
			break;
		}
	}
}

inline ULMINT GetLeftVal(LPTSTR lpszValue)
{
	int iRet = -1;
	CString strFmt;
	strFmt.Format(_T("%%7.%dlf"), m_nLeftDotNum);
	if (m_pData && m_pData->m_bRecUnit)
	{
		double dK = m_pData->m_fAK;
		double db = m_pData->m_fAB;
		double dTemp = m_LeftValue * dK + db;			
		iRet = _stprintf(lpszValue, strFmt, dTemp);
	}
	else
	{			
		iRet = _stprintf(lpszValue, strFmt, m_LeftValue);
	}
	removeLastZero(lpszValue);

	return iRet;
}

inline ULMINT GetRightVal(LPTSTR lpszValue)
{
	int iRet = -1;
	CString strFmt;
	strFmt.Format(_T("%%7.%dlf"), m_nRightDotNum);
	if (m_pData && m_pData->m_bRecUnit)
	{
		double dK = m_pData->m_fAK;
		double db = m_pData->m_fAB;
		double dTemp = m_RightValue * dK + db;	
		iRet = _stprintf(lpszValue, strFmt, dTemp);
	}
	else
	{
		iRet = _stprintf(lpszValue, strFmt, m_RightValue);
	}
	removeLastZero(lpszValue);
	
	return iRet;
}
	
// 2020.11.24 ltg Start
inline ULMINT GetLeftValMapClr(LPTSTR lpszValue)
{
	int iRet = -1;
	CString strFmt;
	strFmt.Format(_T("%%7.%dlf"), m_nLeftDotNum);
	iRet = _stprintf(lpszValue, strFmt, m_LeftValueMapClr);
	removeLastZero(lpszValue);
	
	return iRet;
}

inline ULMINT GetRightValMapClr(LPTSTR lpszValue)
{
	int iRet = -1;
	CString strFmt;
	strFmt.Format(_T("%%7.%dlf"), m_nRightDotNum);
	iRet = _stprintf(lpszValue, strFmt, m_RightValueMapClr);
	removeLastZero(lpszValue);
	
	return iRet;
}
// 2020.11.24 ltg End

/* ----- 图像曲线特有属性 ----- */
inline ULMDBL  StartPos()
{
	double dblStart = m_dStartPos;
	if (m_pData)
	{
		int nOff = m_wIndex2*m_pData->GetPointFrame();
		dblStart += nOff;
	}

	return dblStart;
}

inline ULMDBL  EndPos()
{
	double dblEnd = m_dEndPos;
	if (m_pData)
	{
		int nOff = m_wIndex2*m_pData->GetPointFrame();
		dblEnd += nOff;
	}

	return dblEnd;
}

inline ULMINT  ColorRank() { return m_nColorRank;	}
inline ULMDWD  LightColor(){ return m_crLightColor;}
inline ULMDWD  DarkColor() { return m_crDarkColor; }

inline ULMINT GetStartTime(LPTSTR lpszTime) 
{
	int nTime = m_dStartPos;
	if (m_pData)
	{
		nTime = m_pData->m_lTimeDelay + m_pData->m_iTimeInter*m_dStartPos;
	}
	return _stprintf(lpszTime, _T("%d"), nTime);
}

inline ULMINT GetEndTime(LPTSTR lpszTime)
{
	int nTime = m_dEndPos + 1;
	if (m_pData)
	{
		nTime = m_pData->m_lTimeDelay + m_pData->m_iTimeInter*(m_dEndPos + 1);
	}
	return _stprintf(lpszTime, _T("%d"), nTime);
}

inline ULMINT GetDivision()
{
	if (m_nDivision < 1)
		m_nDivision = 1;

	return m_nDivision;
}

inline ULMINT GetPointDivision()
{
	return m_nPointDivision;
}

int	GetGrayLevel(int nIndex0, int nIndex1, int nGrayBank = -1, double dMin = -1, double dMax = -1);

/* ----- 波列曲线特有属性 ----- */
inline ULMLNG	DepthDistance()
{
	if (m_lDepthDistance < 512)
		m_lDepthDistance = MTD(1);
	return m_lDepthDistance;
}

inline ULMLNG	DepthDelay()
{
	return m_lDepthDelay;	
}
inline ULMINT   DepthInterval()
{
	if (m_pData)
		return m_pData->DepthInterval();

	return m_lDepthDistance;
}

inline ULMUNT	TimeInter()
{ 
	if (m_pData)
	{
		return m_pData->m_iTimeInter;
	}

	return m_iTimeInter;
}

inline ULMLNG	TimeDelay()
{ 
	if (m_pData)
	{
		return m_pData->m_lTimeDelay;
	}
	return m_lTimeDelay;
}

inline ULMFLT	DepthDistance(UINT nDepthUnit)	{ return DTA(m_lDepthDistance, nDepthUnit);	}
inline ULMFLT	DepthDelay(UINT nDepthUnit)		{ return DTA(m_lDepthDelay, nDepthUnit);	}

inline void DepthDistance(double fDepthDis, UINT nDepthUnit)
{ 
	m_lDepthDistance = ATD(fDepthDis, nDepthUnit);
}

inline void DepthDelay(double fDepthDelay, UINT nDepthUnit)
{ 
	m_lDepthDelay = ATD(fDepthDelay, nDepthUnit);
}

inline ULMETHOD	TimeInter(UINT iTimeInter)
{
	if (m_pData)
	{
		if (iTimeInter < 1)
			iTimeInter = 2;

		m_pData->m_iTimeInter = iTimeInter;
		m_iTimeInter = iTimeInter;
		return UL_NO_ERROR;
	}
	m_iTimeInter = iTimeInter;
	return UL_ERROR;
}

inline ULMETHOD	TimeDelay(long lTimeDelay)
{
	if (m_pData)
	{
		m_pData->m_lTimeDelay = lTimeDelay;
		return UL_NO_ERROR;
	}
	return UL_ERROR;
}

/* ----- 解释曲线特有属性 ----- */

inline ULMDWD BkColor()	{ return m_crBkColor;	}
inline ULMDWD FgColor()	{ return m_crFgColor;	}
inline ULMINT BkMode()	{ return m_nBkMode; }
inline ULMTSTR Pattern()	{ return (LPTSTR)(LPCTSTR)m_strPattern; }
virtual void Serialize(CXMLSettings& xml);

//////////////////////////////////////////////////////////////////////
//
//  Curve data properties
//
//////////////////////////////////////////////////////////////////////

inline ULMTSTR Unit() { return m_pData ? (LPTSTR)(LPCTSTR)m_pData->m_strUnit : NULL; }

inline ULMETHOD SetUnit(LPCTSTR lpszUnit) 
{	
	if (m_pData)
		m_pData->m_strUnit = lpszUnit; 
	return UL_NO_ERROR;
}

#ifdef _RUNIT

inline ULMETHOD SetUnits(LPCTSTR lpszUnit)
{
	if (m_pData)
	{
		m_pData->m_strUnit = lpszUnit;
		m_pData->m_strTu = lpszUnit;
		m_strLRUnit = lpszUnit;
	}

	return UL_NO_ERROR;
}

inline ULMETHOD SetDBUnit(LPCTSTR lpszUnit)
{
	if (m_pData)
	{
		m_pData->m_strUnit = lpszUnit;
	}
	
	return UL_NO_ERROR;
}
inline ULMETHOD SetCurrUnit(LPCTSTR lpszUnit)
{
	if (m_pData)
	{
		m_pData->m_strTu = lpszUnit;
	}
	
	return UL_NO_ERROR;
}
#endif

inline ULMETHOD	GetUnit(LPTSTR lpszUnit) 
{ 
	if (m_pData)
	{
		_tcscpy(lpszUnit, m_pData->m_strUnit);
		return UL_NO_ERROR;
	}
	return UL_ERROR;
}

inline ULMETHOD GetKb(double& dK, double& db)
{
	if (m_pData)
	{
#ifdef _RUNIT

		if (m_pData->m_bRecUnit)
		{
			dK = m_pData->m_fAK;
			db = m_pData->m_fAB;
		}
		else

#endif
		{
			dK = 1.0;
			db = 0.0;
		}

		return UL_NO_ERROR;
	}

	return UL_ERROR;
}

inline ULMBOOL IsView()
{
	if (m_pData)
		return m_pData->m_bView;
	return FALSE;
}

inline ULMETHOD View(BOOL bView)
{
	if (m_pData)
	{
		m_pData->m_bView = bView;
		return UL_NO_ERROR;
	}
	return UL_ERROR;
}

inline ULMBOOL IsOpen()
{
	if (m_pData)
		return m_pData->m_bOpen;
	return FALSE;
}

inline ULMETHOD Open(BOOL bOpen)
{
	if (m_pData)
	{
		m_pData->m_bOpen = bOpen;
		m_pData->m_bView = bOpen;
		m_pData->m_bSave = bOpen;
		return UL_NO_ERROR;
	}
	return UL_ERROR;
}

inline ULMINT Dimension()
{ 
	if (m_pData)
		return m_pData->m_nDimension;
	return 1;
}

inline ULMETHOD	Dimension(int nDimension)
{
	if (m_pData && (nDimension > 0))
		m_pData->m_nDimension = nDimension;
	return UL_NO_ERROR;
}

inline ULMETHOD	SetValueType(VARTYPE nValType)
{
	if (m_pData)
		m_pData->SetValueType(nValType);
	return UL_NO_ERROR;
}

inline ULMINT GetValueType()
{   
	if (m_pData)
		return m_pData->GetValueType();
	return VT_NULL;
}

inline ULMINT GetSizePoint()
{
	if (m_pData)
		return m_pData->m_nSizePoint;
	return 0;
}

inline ULMETHOD	SetPointFrame(short nPointFrame)
{ 
	if (m_pData)
		m_pData->SetPointFrame(nPointFrame);

	return UL_NO_ERROR; 
}

inline ULMETHOD	GetPointFrame()
{	
	if (m_pData)
		return m_pData->GetPointFrame();
	return 0;
}

inline ULMETHOD	SetArrayCount(short nArrayCount)
{ 
	if (m_pData)
		m_pData->SetArrayCount(nArrayCount);

	return UL_ERROR;
}

inline ULMETHOD	GetArrayCount()
{	
	if (m_pData)
		return m_pData->GetArrayCount();
	return 0;
}

inline ULMETHOD	SetSamplesCount(int nSamples)
{ 
	if (m_pData)
	{
		if (nSamples == 0)
			m_pData->m_nSamplesCount = 1;
		else
			m_pData->m_nSamplesCount = nSamples;
	}
	return UL_NO_ERROR; 
}

inline ULMINT	GetSamplesCount()
{ 
	if (m_pData)
		return m_pData->m_nSamplesCount;
	return 0; 
}

inline ULMLNG	CalcOffset()
{
	if (m_pData)
		return m_pData->m_lCalcOffset;
	return 0;
}

inline ULMLNG CalcInterval()
{
	if (m_pData)
		return m_pData->CalcInterval();

	return 0;
}

inline ULMINT	OffsetMode(long* pOffset = NULL)
{
	if (m_pData)
	{
		if (pOffset)
			*pOffset = m_pData->m_lDepthOffset;
		return m_pData->m_nOffsetMode;
	}

	return 0;
}

inline ULMETHOD OffsetMode(int nOffMode, long* pOffset = NULL)
{
	if (m_pData)
	{
		m_pData->m_nOffsetMode = nOffMode;
		if (pOffset)
			m_pData->m_lDepthOffset = *pOffset;
	}

	return UL_NO_ERROR;
}

inline ULMLNG	DepthOffset()
{ 
	if (m_pData)
	{
		m_pData->m_nDepthData = 0;

		if ((m_strName.CompareNoCase(_T("DEPT")) == 0) || (m_strName.CompareNoCase(_T("DEPTH")) == 0))
			m_pData->m_nDepthData |= DS_DCURVE;

		if ((m_strSource.CompareNoCase(_T("TLDEPTH")) == 0) || (m_strSource.CompareNoCase(_T("DEPTH")) == 0))
			m_pData->m_nDepthData |= DS_DTOOL;

		if (m_pData->m_nDepthData)
		{
			m_pData->m_lToolOffset = 0;
			m_pData->m_lDepthOffset = 0;
			m_pData->m_lTBOffset = 0;
			m_pData->m_nOffsetMode = 0;
		}
		return m_pData->m_lTBOffset;
	}
	return 0; 
}

inline ULMFLT DepthOffset(UINT nDepthUnit)
{
	if (m_pData)
		return DTA(m_pData->m_lTBOffset, nDepthUnit);
	return 0;
}

inline ULMLNG	MaxDepthOffset()
{ 
	if (m_pData)
		return m_pData->m_lMaxDepthOffset;
	return 0; 
}

inline ULMFLT MaxDepthOffset(UINT nDepthUnit)
{
	if (m_pData)
		return DTA(m_pData->m_lMaxDepthOffset, nDepthUnit);
	return 0;
}

inline ULMETHOD SetDepthOffset(long lDepthOffset)
{
	if (m_pData)
		m_pData->m_lTBOffset = lDepthOffset;
	return UL_NO_ERROR;
}
	
inline ULMETHOD SetDepthOffset(float fDepthOffset, UINT nDepthUnit)
{
	if (m_pData)
		m_pData->m_lTBOffset = ATD(fDepthOffset, nDepthUnit);
	return UL_NO_ERROR;
}

inline ULMLNG	ToolOffset()
{
	if (m_pData)
		return m_pData->m_lToolOffset;
	return 0;
}

inline ULMFLT ToolLength(UINT nDepthUnit)
{
	if (m_pData)
		return DTM(m_pData->m_lToolLength);
	return 0;
}

inline ULMETHOD	SetToolOffset(float fToolOffset, UINT nDepthUnit)
{
	if (m_pData)
		m_pData->m_lToolOffset = ATD(fToolOffset, nDepthUnit);
	return UL_NO_ERROR;
}

inline ULMETHOD	SetToolLength(float fToolLength, UINT nDepthUnit)
{
	if (m_pData)
		m_pData->m_lToolLength = ATD(fToolLength, nDepthUnit);
	return UL_NO_ERROR;
}

inline ULMBOOL DrawOffset()
{
	if (m_pData)
		return m_pData->m_bDrawOffset;
	return FALSE;
}

inline ULMETHOD DrawOffset(BOOL bDrawOffset)
{
	if (m_pData)
		m_pData->m_bDrawOffset = bDrawOffset;
	return UL_NO_ERROR;
}

inline ULMINT AutoDirection()
{
	if (m_pData)
	{
		int nSize = m_pData->GetSize();
		if (nSize > 1)
		{
			long lDepth0 = m_pData->GetDepth(0);
			long lDepthE = m_pData->GetDepth(nSize - 1);
			m_pData->m_nDirection = (BOOL)(lDepth0 < lDepthE);
		}		

		return m_pData->m_nDirection;
	}

	return 0;
}

inline ULMINT Direction()
{
	if (m_pData)
		return m_pData->m_nDirection;

	return SCROLL_DOWN;
}

inline ULMFLT FeedPoint()
{
	if (m_pData)
		return m_pData->m_fFeedPoint;
	return 0;
}

inline ULMINT FeedPointOfDepth(long lDepth)
{
	if (m_pData)
		return floor(m_pData->m_fFeedPoint*DTM(lDepth)+.5);
	return 0;
}

//////////////////////////////////////////////////////////////////////
//
//  Curve data operations
//
//////////////////////////////////////////////////////////////////////	

inline ULMETHOD	SetDepth(int nIndex, long lDepth)
{
	if (m_pData)
		return m_pData->SetDepth(nIndex, lDepth);
	return UL_ERROR;
}

inline ULMLNG FRDepth()
{
	if (m_pData)
		return m_pData->m_lFirstDepth;
	return LONG_MIN;
}

inline ULMETHOD FRDepth(long lDepth)
{
	if (m_pData)
		m_pData->m_lFirstDepth = lDepth;
	return UL_NO_ERROR;
}

inline ULMLNG GetDepth(int nIndex)
{
	if (m_pData)
		return m_pData->GetDepth(nIndex);
	return UL_ERROR;
}

inline ULMLNG GetDepthED()
{
	if (m_pData)
		return m_pData->GetDepthED();
	
	return INVALID_DEPTH;
}

inline ULMETHOD SetTime(int nIndex, long lTime)
{		
	if (m_pData)
		return m_pData->SetTime(nIndex, lTime);
	return UL_ERROR;
}
inline ULMLNG GetTime(int nIndex)
{		
	if (m_pData)
		return m_pData->GetTime(nIndex);
	return UL_ERROR;
}

inline ULMLNG GetTimeED()
{
	if (m_pData)
		return m_pData->GetTimeED();
	
	return INVALID_DEPTH;
}

inline ULMINT GetDataMode(int nIndex)
{
	if (m_pData)
		return m_pData->GetDataMode(nIndex);
	return DATA_INVALID;
}

inline ULMETHOD	SetDataMode(int nIndex, DWORD bMode)
{
	if (m_pData)
		return m_pData->SetDataMode(nIndex, bMode);
	return UL_NO_ERROR;
}
//add by Jasmine 201181204
//begin
/*
inline ULMINT GetDataModeEx(int nIndex, WORD wType)
{
	if (wType == 0)
	{
		if (m_pData)
			return m_pData->GetDataBaseMode(nIndex);
	}
	else
	{
		if (m_pData)
			return m_pData->GetDataDrawMode(nIndex);
	}
	return DATA_INVALID;
}

inline ULMETHOD	SetDataModeEx(int nIndex, DWORD bMode, WORD wType)
{
	if (wType == 0)
	{
		if (m_pData)
			return m_pData->SetDataBaseMode(nIndex, bMode);
	}
	else
	{
		if (m_pData)
			return m_pData->SetDataDrawMode(nIndex, bMode);
	}
	return UL_NO_ERROR;
}
inline ULMETHOD SetDataModeEx(DWORD bMode, WORD wType)
{
	if (wType == 0)
	{
		if (m_pData)
			return m_pData->SetDataBaseMode(bMode);
	}
	else
	{
		if (m_pData)
			return m_pData->SetDataDrawMode(bMode);
	}
	return UL_NO_ERROR;
}*/
//end
inline ULMETHOD SetDataMode(DWORD bMode)
{
	if (m_pData)
		return m_pData->SetDataMode(bMode);
	return UL_NO_ERROR;
}

/* ----------------------------- *
 *  数据源数据链表操作
 * ----------------------------- */	
inline ULMINT GetDataSize()
{
	if (m_pData)
		return m_pData->m_arrData.GetSize();
	return 0;
}

inline ULMINT AllToolLength()
{
	if (m_pData)
		return m_pData->m_lAllToolLength;
	return 0;
}

inline ULMINT	GetDataUpBound()
{
	if (m_pData)
		return m_pData->m_arrData.GetUpperBound();
	return 0;
}

// Append data from another curve
//
inline short AppendData(CCurve& srcCurve, int nStart, BOOL bArray = FALSE)
{
	if (m_pData && srcCurve.m_pData)
		return m_pData->Append(srcCurve.m_pData, nStart, bArray);
	return UL_ERROR;
}

// Delete data from the data array
//
inline ULMETHOD DeleteData(int nStart, int nCount = 1)
{
	if (m_pData)
		m_pData->Delete(nStart, nCount);
	return UL_ERROR;
}

inline void	RemoveAllData()
{
	if (m_pData)
		m_pData->m_arrData.RemoveAll();
}

inline ULMETHOD ResetData()
{
	if (m_pData)
		m_pData->Clear();

	return UL_NO_ERROR;
}
	
/* ----------------------------- *
 *  添加nSize个数据，并初始化为0
 * ----------------------------- */
inline ULMETHOD AddNewData(int nSize = 1)
{
	if (m_pData)
		return m_pData->AddZero(nSize);
	return UL_ERROR;
}

//////////////////////////////////////////////////////////////////////
//
// Add data of 1 dimension curve data
//
//////////////////////////////////////////////////////////////////////

inline ULMETHOD	AddData(long lDepth, char cValue, long lTime = 0)
{
	if (m_pData)
		m_pData->Add(lDepth, cValue, lTime);
	return UL_ERROR;
}

inline ULMETHOD	AddData(long lDepth, short iValue, long lTime = 0)
{
	if (m_pData)
		m_pData->Add(lDepth, iValue, lTime);
	return UL_ERROR;
}

inline ULMETHOD	AddData(long lDepth, long lValue, long lTime = 0)
{
	if (m_pData)
		m_pData->Add(lDepth, lValue, lTime);
	return UL_ERROR;
}

inline ULMETHOD	AddData(long lDepth, float fValue, long lTime = 0)
{
	if (m_pData)
		m_pData->Add(lDepth, fValue, lTime);
	return UL_ERROR;
}

inline ULMETHOD	AddData(long lDepth, double dValue, long lTime = 0)
{
	if (m_pData)
		m_pData->Add(lDepth, dValue, lTime);
	return UL_ERROR;
}

inline ULMETHOD	AddData(long lDepth, BYTE bValue, long lTime = 0)
{
	if (m_pData)
		m_pData->Add(lDepth, bValue, lTime);
	return UL_ERROR;
}

inline ULMETHOD	AddData(long lDepth, WORD wValue, long lTime = 0)
{
	if (m_pData)
		m_pData->Add(lDepth, wValue, lTime);
	return UL_ERROR;
}

inline ULMETHOD	AddData(long lDepth, DWORD dwValue, long lTime = 0)
{
	if (m_pData)
		m_pData->Add(lDepth, dwValue, lTime);
	return UL_ERROR;
}
	
/* ----- 一维/多维曲线数据添加 ----- */
inline ULMETHOD	AddData(long lDepth, void* pValue, long lTime = 0)
{
	if (m_pData)
	{
		if ((m_fMulti != 1.0) || (m_fPlus != 0.0))
		{
			switch(m_pData->m_nValueType)
			{
			case VT_I1:
				{
					for (int i = 0; i < m_pData->m_nPointArray; i++)
					{
						char vi = ((char*)pValue)[i];
						((char*)pValue)[i] = (char)(vi*m_fMulti + m_fPlus);
					}
				}
				break;
			case VT_I2:
				{
					for (int i = 0; i < m_pData->m_nPointArray; i++)
					{
						short vi = ((short*)pValue)[i];
						((short*)pValue)[i] = (short)(vi*m_fMulti + m_fPlus);
					}
				}
				break;
			case VT_I4:
				{
					for (int i = 0; i < m_pData->m_nPointArray; i++)
					{
						long vi = ((long*)pValue)[i];
						((long*)pValue)[i] = (long)(vi*m_fMulti + m_fPlus);
					}
				}
				break;
			case VT_R4:
				{
					for (int i = 0; i < m_pData->m_nPointArray; i++)
					{
						float vi = ((float*)pValue)[i];
						((float*)pValue)[i] = (float)(vi*m_fMulti + m_fPlus);
					}
				}
				break;
			case VT_R8:
				{
					for (int i = 0; i < m_pData->m_nPointArray; i++)
					{
						double vi = ((double*)pValue)[i];
						((double*)pValue)[i] = (vi*m_fMulti + m_fPlus);
					}
				}
				break;
			case VT_UI1:
				{
					for (int i = 0; i < m_pData->m_nPointArray; i++)
					{
						BYTE vi = ((BYTE*)pValue)[i];
						((BYTE*)pValue)[i] = (BYTE)(vi*m_fMulti + m_fPlus);
					}
				}
				break;
			case VT_UI2:
				{
					for (int i = 0; i < m_pData->m_nPointArray; i++)
					{
						WORD vi = ((WORD*)pValue)[i];
						((WORD*)pValue)[i] = (WORD)(vi*m_fMulti + m_fPlus);
					}
				}
				break;
			case VT_UI4:
				{
					for (int i = 0; i < m_pData->m_nPointArray; i++)
					{
						DWORD vi = ((DWORD*)pValue)[i];
						((DWORD*)pValue)[i] = (DWORD)(vi*m_fMulti + m_fPlus);
					}
				}
				break;
			default:
				return UL_ERROR;
			}
		}

		return m_pData->Add(lDepth, pValue, lTime);
	}
	return UL_ERROR;
}

//////////////////////////////////////////////////////////////////////
//
// Add data of 1 dimension status curve data
//
//////////////////////////////////////////////////////////////////////

inline ULMETHOD	AddStatusData(long lDepth, char cValue, long lTime = 0)
{
	if (m_pData)
		return m_pData->Set(0, lDepth, cValue, lTime);
	return UL_ERROR;
}

inline ULMETHOD	AddStatusData(long lDepth, short iValue, long lTime = 0)
{
	if (m_pData)
		return m_pData->Set(0, lDepth, iValue, lTime);
	return UL_ERROR;
}

inline ULMETHOD	AddStatusData(long lDepth, long lValue, long lTime = 0)
{
	if (m_pData)
		return m_pData->Set(0, lDepth, lValue, lTime);
	return UL_ERROR;
}

inline ULMETHOD	AddStatusData(long lDepth, float fValue, long lTime = 0)
{
	if (m_pData)
		return m_pData->Set(0, lDepth, fValue, lTime);
	return UL_ERROR;
}
	
inline ULMETHOD	AddStatusData(long lDepth, double dValue, long lTime = 0)
{
	if (m_pData)
		return m_pData->Set(0, lDepth, dValue, lTime);
	return UL_ERROR;
}

inline ULMETHOD	AddStatusData(long lDepth, BYTE bValue, long lTime = 0)
{
	if (m_pData)
		return m_pData->Set(0, lDepth, bValue, lTime);
	return UL_ERROR;
}

inline ULMETHOD	AddStatusData(long lDepth, WORD wValue, long lTime = 0)
{
	if (m_pData)
		return m_pData->Set(0, lDepth, wValue, lTime);
	return UL_ERROR;
}

inline ULMETHOD	AddStatusData(long lDepth, DWORD dwValue, long lTime = 0)
{
	if (m_pData)
		return m_pData->Set(0, lDepth, dwValue, lTime);
	return UL_ERROR;
}

// Add data of status data
//
inline ULMETHOD	AddStatusData(long lDepth, void* pValue, long lTime = 0)
{
	if (m_pData)
		return m_pData->Set(0, lDepth, pValue, lTime);
	return UL_ERROR;
}

// Replace part of data
inline ULMETHOD ReplaceData(ICurve* pSource, long lDepth0, long lDepth1)
{
	if (m_pData)
	{
		CCurve* pSrc = (CCurve*)pSource;
		if (pSrc->m_pData)
			return m_pData->Replace(pSrc->m_pData, lDepth0, lDepth1);
	}

	return UL_ERROR;
}

//////////////////////////////////////////////////////////////////////
//
// Get value of different type
//
//////////////////////////////////////////////////////////////////////

inline ULMDBL GetValue(int nIndex0, int nIndex1 = 0)
{
	if (m_pData)
	{
		if (GetDataSize() <= 0)
		{
			return 0;
		}
		return m_pData->GetdblValue(nIndex0, nIndex1);
	}
	return 0;
}

inline ULMETHODCHR  GetchrValue(int nIndex0, int nIndex1 = 0)
{
	if (m_pData)
		return m_pData->GetchrValue(nIndex0, nIndex1);
	return 0;
}

inline ULMFLT	GetfltValue(int nIndex0, int nIndex1 = 0)
{
	if (m_pData)
		return m_pData->GetfltValue(nIndex0, nIndex1);
	return 0;
}

inline ULMDBL	GetdblValue(int nIndex0, int nIndex1 = 0)
{
	if (m_pData)
		return m_pData->GetdblValue(nIndex0, nIndex1);
	return 0;
}

inline ULMETHODWRD	GetwrdValue(int nIndex0, int nIndex1 = 0)
{
	if (m_pData)
		return m_pData->GetwrdValue(nIndex0, nIndex1);
	return 0;
}

// Get value buffer (pointer)
//
inline ULMPTR GetValueBuffer(int nIndex, BOOL bForceRange = FALSE)
{
	if (m_pData)
		return m_pData->GetValuePtr(nIndex, bForceRange);
	return NULL;	
}
	
inline ULMINT GetValueBufferSize()
{
	if (m_pData)
		return m_pData->GetValueSize();
	return 0;
}

// Get value buffer copy
//
inline ULMETHOD	GetValue(int nIndex0, void* pValue)
{
	if (m_pData)
	{
		if (GetDataSize() <= 0)
		{
			return UL_ERROR;
		}
		return m_pData->GetValue(nIndex0, pValue);
	}
	
	return UL_ERROR;
}

// Get value of buffer
//
inline ULMDBL GetValueOfBuffer(void* pValueBuffer, int nIndex1)
{
	if (m_pData)
		return m_pData->GetdblValueOf(pValueBuffer, nIndex1);
	return 0.0;
}

// Get special value
//
inline ULMDBL GetValueAtDepth(long lDepth, int nIndex1 = 0)
{
	if (m_pData)
	{
		UINT nIndex0 = (UINT)m_pData->GetFirstIndexByDepth(lDepth);
		if (nIndex0 < m_pData->GetSize())
			return m_pData->GetdblValue(nIndex0, nIndex1);
	}

	return DBL_MAX;
}

inline ULMDBL GetValueAtDepthEx(long lDepth, int nIndex1 = 0)
{
	if (m_pData)
		return m_pData->GetValueAtDepthEx(lDepth, nIndex1);

	return 0;
}
	
inline ULMDBL GetValueAtTime(long lTime, int nIndex1 = 0)
{
	if (m_pData)
	{
		int nIndex0 = m_pData->GetFirstIndexByTime(lTime);
		if (nIndex0 > -1)
			return m_pData->GetdblValue(nIndex0, nIndex1);
	}

	return DBL_MAX;
}
	
inline double GetCurMaxValue()
{
	if (m_pData)
		return m_pData->GetMaxValueCmp(m_RightValue);
	return m_RightValue;
}
	
inline double GetCurMinValue()
{
	if (m_pData)
		return m_pData->GetMinValueCmp(m_LeftValue);
	return m_LeftValue;
}	

inline ULMDBL GetValueMax(int nIndex0, int nStart = -1, int nEnd = -1)
{
	if (m_pData)
	{
		if (nStart == -1)
			nStart = 0;

		if (nEnd == -1)
			nEnd = m_pData->m_nPointArray;
		else
			nEnd++;

		return m_pData->GetValueMax(nIndex0, nStart, nEnd);	
	}
	
	return 0;	
}

inline ULMDBL GetValueMin(int nIndex0, int nStart = -1, int nEnd = -1)
{
	if (m_pData)
	{
		if (nStart == -1)
			nStart = 0;
		if (nEnd == -1)
			nEnd = m_pData->m_nPointArray;
		else
			nEnd++;
		
		return m_pData->GetValueMin(nIndex0, nStart, nEnd);
	}

	return 0;
}

inline ULMDBL GetValueMax(void* pValueBuffer, int nStart, int nEnd)
{
	if (m_pData)
		return m_pData->GetValueMaxOf(pValueBuffer, nStart, ++nEnd);
	return 0;
}

inline ULMDBL GetValueMin(void* pValueBuffer, int nStart, int nEnd)
{
	if (m_pData)
		return m_pData->GetValueMinOf(pValueBuffer, nStart, ++nEnd);
	return 0;
}

inline ULMBOOL IsDepthValue(int nIndex, double& value)
{
	if (m_pData)
	{
		if (m_pData->m_nDepthData & DS_DCURVE)
		{
			value = m_pData->GetDepth(nIndex);			
			return TRUE;
		}
		
		value = m_pData->GetdblValue(nIndex);
		return FALSE;
	}
	
	return FALSE;
}

//////////////////////////////////////////////////////////////////////
//
// Set value of different type
//
//////////////////////////////////////////////////////////////////////

inline ULMETHOD SetValue(int nIndex, char cValue)
{
	if (m_pData)
		m_pData->SetValue(cValue, nIndex);
	return UL_NO_ERROR;
}

inline ULMETHOD SetValue(int nIndex, short iValue)
{
	if (m_pData)
		m_pData->SetValue(iValue, nIndex);
	return UL_NO_ERROR;
}

inline ULMETHOD SetValue(int nIndex, long lValue)
{
	if (m_pData)
		m_pData->SetValue(lValue, nIndex);
	return UL_NO_ERROR;
}

inline ULMETHOD	  SetValue(int nIndex, float fValue)
{
	if (m_pData)
		m_pData->SetValue(fValue, nIndex);
	return UL_NO_ERROR;
}

inline ULMETHOD SetValue(int nIndex, double dValue)
{
	if (m_pData)
		m_pData->SetValue(dValue, nIndex);
	return UL_NO_ERROR;
}

inline ULMETHOD SetValue(int nIndex, BYTE bValue)
{
	if (m_pData)
		m_pData->SetValue(bValue, nIndex);
	return UL_NO_ERROR;
}

inline ULMETHOD SetValue(int nIndex, WORD wValue)
{
	if (m_pData)
		m_pData->SetValue(wValue, nIndex);
	return UL_NO_ERROR;
}

inline ULMETHOD SetValue(int nIndex, DWORD dwValue)
{
	if (m_pData)
		m_pData->SetValue(dwValue, nIndex);
	return UL_NO_ERROR;
}

/* ------------------------ *
 *  多维数据赋值
 * ------------------------ */

inline ULMETHOD	  SetValue(int nIndex0, int nIndex1, double dValue)
{
	if (m_pData)
		m_pData->SetValue(dValue, nIndex0, nIndex1);
	return UL_NO_ERROR;
}

inline ULMETHOD	SetValue(int nIndex, double* pValue)
{
	if (m_pData)
		return m_pData->SetValue(nIndex, pValue);
	return UL_NO_ERROR;
}

/* ------------ *
 *  数据索引
 * ------------ */
inline ULMINT GetFirstIndexByDepth(long lDepth)
{
	if (m_pData)
		return m_pData->GetFirstIndexByDepth(lDepth/*, m_nDirection*/);
	return 0;
}

inline ULMETHOD GetDepthRange(long* pStart, long* pEnd, int nStart0 = 0)
{
	if (m_pData)
		return m_pData->GetDepthRange(pStart, pEnd, nStart0);
	return UL_ERROR;
}

inline ULMETHOD GetTimeRange(long* pStart, long* pEnd, int nStart0 = 0)
{
	if (m_pData)
		return m_pData->GetTimeRange(pStart, pEnd, nStart0);
	return UL_ERROR;
}

inline ULMINT GetIndexByDepth(long lDepth, int nStart, int nEnd)
{
	if (m_pData)
		return m_pData->GetIndexByDepth(lDepth, nStart, nEnd);
	return -1;
}

inline ULMINT GetFirstIndexByTime(long lTime)
{
	if (m_pData)
		return m_pData->GetFirstIndexByTime(lTime/*, m_nDirection*/);		
	return 0;
}

inline ULMINT GetFirstIndexByDepth(long lDepth, int nDirection)
{
	if (m_pData)
		return m_pData->GetFirstIndexByDepth(lDepth, nDirection);
	return 0;
}

inline ULMINT GetFirstIndexByTime(long lTime, int nDirection)
{
	if (m_pData)
		return m_pData->GetFirstIndexByTime(lTime, nDirection);		
	return 0;
}

/* ---------------------- *
 *  滤波
 * ---------------------- */
inline ULMBOOL	IsFilter()
{
	if (m_pData)
		return m_pData->m_bFilter;
	return FALSE;
}

inline ULMETHOD	Filter(BOOL bFilter)
{
	if (m_pData)
		m_pData->m_bFilter = bFilter;
	return UL_NO_ERROR;
}
	
inline void	SetFilter(LPCTSTR strFilterName)
{
	if (m_pData)
		m_pData->SetFilter(strFilterName);
}
    
inline void	SetFilter(CULFilter* pFilter = NULL)
{
	if (m_pData)
		m_pData->SetFilter(pFilter);
}
	
inline CULFilter* GetFilter()
{
	if (m_pData)
		return m_pData->m_pFilter;
	return NULL;
}

inline ULMETHOD SetFilterTemplate(int nTemplate)
{
	if (m_pData)
		m_pData->m_nFilterTempl = nTemplate;
	return UL_NO_ERROR;
}

inline ULMINT GetFilterTemplate()
{
	if (m_pData)
		return m_pData->m_nFilterTempl;
	return NULL;
}

/* ---------------------- *
 *  数据存盘
 * ---------------------- */
inline ULMBOOL	IsSave()
{	
	if (m_pData)
		return m_pData->m_bSave;
	return FALSE;
}
	
inline ULMETHOD	Save(BOOL bSave)
{
	if (m_pData)
		m_pData->m_bSave = bSave;
	return UL_NO_ERROR;
}
	
inline ULMINT GetCurSavePos()
{
	if (m_pData)
		return m_pData->m_nCurSavePos;
	return 0;
}
	
inline ULMETHOD SetCurSavePos(int nPos)
{
	if (m_pData)
		m_pData->m_nCurSavePos = nPos;
	return UL_NO_ERROR;
}

inline ULMETHOD IniCurSavePos(long lStartDepth, int nOff)
{
	if (m_pData)
		m_pData->IniCurSavePos(lStartDepth, nOff);

	return UL_NO_ERROR;
}

inline ULMETHOD IncCurSavePos(int nPosAdd)
{
	if (m_pData)
		m_pData->m_nCurSavePos += nPosAdd;
	return UL_NO_ERROR;
}

inline ULMPTR GetCurSaveValue()
{
	if (m_pData)
		return m_pData->GetCurSaveValuePtr();
	return NULL;
}

inline ULMPTR GetCurSaveValue(int nOff)
{
	if (m_pData)
		return m_pData->GetCurSaveValuePtr(nOff);
	return NULL;
}

inline ULMVOID GetCurSaveValues(long lDepth, LPBYTE lpData, size_t sz, long lDLevel, int nOff = 0)
{
	if (m_pData)
		m_pData->GetCurSaveValues(lDepth, lpData, sz, lDLevel, nOff);
}

inline ULMINT GetSaveSync()
{
	if (m_pData)
		return m_pData->m_nSaveSync;
	return 0;
}

inline ULMETHOD SetSaveSync(int nSaveSync)
{
	if (m_pData)
		m_pData->m_nSaveSync = nSaveSync;
	return UL_NO_ERROR;
}

// Save data for data edit
//
inline void	SaveValue(CFile& file, int nIndex)
{
	if (m_pData)
		m_pData->SaveValue(file, nIndex);
}

// Read data for data edit
//	
inline void	ReadValue(CFile& file, int nIndex)
{
	if (m_pData)
		m_pData->ReadValue(file, nIndex);
}

// Get curve data size of (mode, depth, time, value) 
// 
inline int GetCurveValueSize()
{
	if (m_pData)
		return m_pData->GetDataSize();
	return 0;
}

// Get curve data size of (value)
//
inline int GetFrameSize()
{
	if (m_pData)
		return m_pData->GetValueSize();
	return 0;
}

/* ---------------------- *
 *  设置同源数据曲线属性
 * ---------------------- */
inline ULMETHOD	SetCurDisplayPosSame(int nPos)
{
	if (m_pData)
		m_pData->SetCurDisplayPosSame(nPos);
	return UL_NO_ERROR;
}

inline ULMETHOD	IncCurDisplayPosSame(int nAddPos)
{
	if (m_pData)
		m_pData->IncCurDisplayPosSame(nAddPos);
	return UL_NO_ERROR;
}

/* --------------------- *
 *  填充曲线属性
 * --------------------- */
inline ULMETHOD SetFillProp(void* pFillProp)
{
	memcpy(&m_FillProp, pFillProp, sizeof(FILLPROP));
	return S_OK;
}

inline ULMPTR GetFillProp()
{
	return &m_FillProp;
}

inline ULMETHOD SetRun(int nRun)			
{ 
	m_nRun = nRun; 
	return UL_NO_ERROR; 
}

inline ULMINT GetRun()					
{ 
	return m_nRun;		
}

inline ULMPTR GetColorTable()
{
	return m_tblColor.GetData();
}

inline ULMPTR GetDBInfo()
{
	return m_pDBCurveInfo;
}

inline ULMETHOD SetDBInfo(void *pDBCurveInfo)
{ 
	m_pDBCurveInfo = (CDBFilterCurveInfo *)pDBCurveInfo;
	
	return UL_NO_ERROR; 
}

inline ULMPTR GetCurveFilter()
{
	return m_pCurveFilter;
}

inline ULMETHOD SetCurveFilter(void *pCurveFilter)
{ 
	m_pCurveFilter = (CCurveFilter*)pCurveFilter;	
	return UL_NO_ERROR; 
}

inline ULMETHOD SetPlus(double fPlus)
{
	m_fPlus = fPlus;
	return UL_NO_ERROR;
}

inline ULMETHOD SetMulti(double fMulti)
{
	m_fMulti = fMulti;
	return UL_NO_ERROR;
}

inline ULMDBL Plus()
{
	return m_fPlus;
}

inline ULMDBL Multi()
{
	return m_fMulti;
}

inline ULMINT GetPipShow()
{
	return m_nPipShow;
}

inline ULMINT GetPipType()
{
	return 0;
}

inline ULMFLT GetMajorPip()
{
	return m_fMajorPip;
}

inline ULMFLT GetMinorPip()
{
	return m_fMinorPip;
}

inline ULMBOOL IsLogarithmic()
{
	return (m_PlotInfo.PlotENName == _T("Log"));
}

inline ULMBOOL IsInpre()
{
	return ((m_PlotInfo.PlotENName == _T("Inpre"))
		||(m_PlotInfo.PlotENName == _T("Impre"))
		||(m_PlotInfo.PlotENName == _T("Filling")));
}

inline ULMINT GetPointArray()
{
	if (m_pData)
		return m_pData->m_nPointArray;

	return 0;
}

inline ULMINT GetWavePlotStyle() { return m_nWavePlotStyle; }
inline ULMINT GetWaveDistribution() { return m_nWaveDistribution; }

inline ULMBOOL SetReceiver(BYTE* pBuf, float* pRRDistance)
{
	if (m_pData)
		return m_pData->SetReceiver(pBuf, pRRDistance);

	return FALSE;
}

inline ULMBOOL GetReceiver(BYTE* pBuf, float* pRRDistance)
{
	if (m_pData)
		return m_pData->GetReceiver(pBuf, pRRDistance);
	
	return 0;
}

inline ULMBOOL SetSender(BYTE* pBuf, float* pSRDistance)
{
	if (m_pData)
		return m_pData->SetSender(pBuf, pSRDistance);

	return FALSE;
}

inline ULMBOOL GetSender(BYTE* pBuf, float* pSRDistance)
{
	if (m_pData)
		return m_pData->GetSender(pBuf, pSRDistance);

	return 0;
}

inline ULMINT GetValMode()
{
	return m_nValMode;
}

inline ULMINT SetValMode(int nValMode)
{
	int nValMode0 = m_nValMode;
	m_nValMode = nValMode;
	return nValMode0;
}

// 2020.08.19 ltg Start
inline ULMLNG GetDrawImgInterval()
{
	return m_lDrawImgInterval;
}

inline ULMETHOD SetDrawImgInterval(long lDrawImgInterval)
{
	m_lDrawImgInterval = lDrawImgInterval;
	
	if (m_pData)
		return m_pData->m_lProInterval = lDrawImgInterval;

	return UL_NO_ERROR;
}

inline ULMLNG GetDrawLineInterval()
{
	return m_lDrawLineInterval;
}

inline ULMETHOD SetDrawLineInterval(long lDrawLineInterval)
{
	m_lDrawLineInterval = lDrawLineInterval;

	if (m_pData)
		return m_pData->m_lProInterval = lDrawLineInterval;

	return UL_NO_ERROR;
}

inline ULMTSTR GetCurveMarking()
{ 
	return (LPTSTR)(LPCTSTR)m_strCurveMarking; 
}
ULMINT GetCurveMarking(CStringArray* pTitles);
inline ULMETHOD SetCurveMarking(LPCTSTR lpszCurveMarking)
{ 
	m_strCurveMarking = lpszCurveMarking;
	return 0;
}
inline ULMTSTR GetLRUnit()
{ 
	return (LPTSTR)(LPCTSTR)m_strLRUnit; 
}
// 取得水平插值点数
inline ULMLNG GetInterH()
{
	return m_lInterHorz;
}
// 设置水平插值点数
inline ULMETHOD SetInterH(long lInterHorz)
{
	m_lInterHorz = lInterHorz;
	return UL_NO_ERROR;
}
// 取得垂直插值点数
inline ULMLNG GetInterV()
{
	return m_lInterVert;
}
// 设置垂直插值点数
inline ULMETHOD SetInterV(long lInterVert)
{
	m_lInterVert = lInterVert;
	return UL_NO_ERROR;
}
// 2020.08.19 ltg End

ULMDBL GetValueEx(int nIndex);

BOOL IsLinear()
{
	return (m_PlotInfo.PlotENName == _T("Linear"));
}

inline CString GetItemName()
{
	CString strItem = m_strName;
	if (m_strSource.GetLength())
	{
		if (m_nTLIndex > 0)
		{
			CString strSource;
			strSource.Format(_T("[%s(%d)]"), m_strSource, m_nTLIndex);
			strItem += strSource;
		}	
		
		strItem += _T("[") + m_strSource + _T("]");
		
	}
	
	if (m_pData)
	{
		if (m_pData->m_strUnit.GetLength())
			strItem += _T("(") + m_pData->m_strUnit + _T(")");
	}

	return strItem;
}

// add by bao 
ULMETHOD	ModifyCurveProperty(LPCTSTR lpszUnit = NULL,		       // 单位
								double fLeftValue = 0,		       // 最小值
								double fRightValue = 0,		       // 最大值
								long  lDepthOffset = 0,		       // 深度偏移
								int nOffMode = 0,	           // 深度偏移模式
								int nFilterPiont = 0,	       // 滤波点数
								int nFilterType = 2,	       // 滤波类型
								int nDimension = 1,		       // 维数
								int nPointFrame = 1,	       // 每帧点数
								int nCurveMode = CURVE_NORMAL, // 曲线模式，用于射孔取芯
								int nValueType = VT_R4,	       // 曲线数据类型
								LPCTSTR lpszIDName = NULL	   // 曲线别名
								); // 修改存在曲线的维数信息
	// end

inline ULMDBL	IsPrintCurveHead() {return m_bPrintCurveHead;}
inline ULMETHOD    MInit(){
	m_pData->MInit();
	return UL_NO_ERROR;
};
//add by xwh 返回时间间距
inline ULMLNG	TimeDistance()
{
	if (m_iTimeDistance > 100)
		m_iTimeDistance = 100;
	return m_iTimeDistance;
}
public:

inline static void SetRTPrint(BOOL bPrint)	{ m_bRTPrint = bPrint;	}
inline static void SetRTSave(BOOL bSave)	{ m_bRTSave = bSave;	}
inline static void SetCurRun(int nRun)		{ m_nRun = nRun;		}
	static IComConfig* m_pComConfig;
	static CList<CCurve*, CCurve*> m_clipCurves;
	static DWORD	m_defWidth;
	

public:
    static BOOL		m_bRTPrint;		// 是否进行实时打印 
    static BOOL		m_bRTSave;		// 是否进行实时数据保存 
	static BOOL		m_bSavingbmp;	// 是否正存位图模式(取消当前选择属性)
	static int		m_nRun;			// 曲线反向次数(主要用在射孔程序中，用来保存曲线是第几次反向)
	static int		m_nMaxRollCount;

public:
	void CopyDataFrom(CCurveData* pData, int nDirect, long lMin, long lMax);
    
	/* ----- 基本属性 ----- */

    CString			m_strName;		// 曲线名称[64]
	CString			m_strIDName;	// 曲线别名[64]
	CString			m_strData;		// 曲线数据源名
	CString			m_strSource;	// 曲线来源[128](仪器名称)
	CString			m_strDescrip;	// 曲线描述
	NEWBY			m_nbType;		// 曲线归属
	DWORD			m_dwStyle;		// 曲线显示，打印，回绕
	CCurveData*		m_pData;		// 数据源
	int				m_nMode;		// 曲线模式 e.g. CURVE_GUN
	int             m_nUserFilter;  // 用户自定义滤波类型
	double          m_fMulti;       // 乘因子
	double          m_fPlus;        // 加因子
	long			m_lInterval;	// 深度插补
	
	/* ----- 显示属性 ----- */
	CTrack*			m_pTrack;		// 所属井道指针

	BOOL            m_bSelected;	// 是否被选中
    BOOL            m_bDelete;		// 是否删除
	
    USHORT			m_nStyle;		// 曲线样式(实线、虚线...)
    DWORD			m_nWidth;		// 曲线宽度
    COLORREF		m_crColor;		// 曲线颜色
	
	CString			m_strLRUnit;	// 左右值单位
    double			m_LeftValue;	// 左值
    double			m_RightValue;	// 右值
// 2020.11.24 ltg Start
	double			m_LeftValueMapClr;	// 左值
    double			m_RightValueMapClr;	// 右值
// 2020.11.24 ltg End
	int				m_nLeftDotNum;	// 记录左值小数点后位数
	int				m_nRightDotNum;	// 记录右值小数点后位数
    int				m_nLeftGrid;	// 左格子
	int				m_nRightGrid;	// 右格子
	int				m_nCurDisplayPos;	// 当前显示点
	unsigned int	m_nPat;
	unsigned int	m_nStretch;

	WORD			m_wIndex1;		// 曲线一维索引
	WORD			m_wIndex2;		// 曲线二维索引
	
	long			m_lBKBegin;		// 显示起始深度
	long			m_lBKEnd;		// 显示终止深度

	int             m_nCorrect;
	
	/* ----- 图像/变密度/电成像属性 ----- */

    double			m_dStartPos;	// 起始位置
    double			m_dEndPos;		// 终止位置
    COLORREF		m_crDarkColor;	// 最深色
    COLORREF		m_crLightColor;	// 最浅色
	CDWordArray		m_tblLocat;     // 位置索引
	CDWordArray     m_tblColor;     // 颜色表
    int				m_nColorRank;	// 色阶
	short			m_nDivision;	// 极板数
	short			m_nPointDivision;	// 空白点数	
	
	/* ----- 波列属性 ----- */

	long			m_lDepthDistance; // 深度间隔
	UINT			m_iTimeDistance; // time distance
    long			m_lDepthDelay;
	UINT			m_iTimeInter;
	long			m_lTimeDelay;
	BOOL			m_bTimeMode;
	int             m_nWavePlotStyle; // 0 线性 1 对数
	int             m_nWaveDistribution; // 0 同一刻度 1 自动等间隔分布

	int				m_nValMode;
	CPtrList		m_lstCurve;

	/* ----- 填充属性 ----- */
	int				m_nBkMode;
	COLORREF		m_crBkColor;
	COLORREF		m_crFgColor;
	CString			m_strPattern;
	
	/* ----- 打印属性 ----- */
	long			m_nCurPrintPos;	// 当前打印点
	int				m_nCurGapPos;	// 当前缺口位置
	int				m_nPF;			// 当前打印标签
	int				m_nPtCountPerDepth;		// 每深度单位内的点的数目
	int             m_nTLIndex;     // 同名仪器索引, 第一相同的仪器为1, 第二个相同的仪器为2
	FILLPROP        m_FillProp;     // 填充属性
	
	/* ----- Volume  ------ */
	int             m_nPipShow; // 0 不显示， 1 左边显示， 2 右边显示
	float           m_fMajorPip;
	float           m_fMinorPip;

	/* -----其它---------- */
	PLOTINFO		 m_PlotInfo;
	CComQIPtr<IPlot> m_Plot;
	BOOL			 m_bDepth;		// 是否是深度曲线
	BOOL			 m_bPrintCurveHead;//是否打印曲线头
	CDBFilterCurveInfo*	 m_pDBCurveInfo;
// 2020.08.19 ltg Start
	long		m_lDrawImgInterval;	// Image 含义是色带高度,间隔米数 单位0.1mm
	long		m_lDrawLineInterval;	// 线性曲线,含义是不画间隔米数 单位0.1mm
	CString		m_strCurveMarking;	// 曲线标识
	long		m_lInterHorz;	// 水平插值点数 2023.08.14 1米多少个点数
	long		m_lInterVert;	// 垂直插值点数
// 2020.08.19 ltg End
// 2021.06.06 ltg Start
	int			m_nDensity;
	inline ULMINT GetDataDensity() { return m_nDensity;	}
// 2021.06.06 ltg End

	CString		m_strXmlPath;
// 2021.11.05 ltg Start
	double m_dLeftDrawValue;
	double m_dRightDrawValue;
	inline ULMDBL GetLeftDrawValue() { return m_dLeftDrawValue; }
	inline ULMDBL GetRightDrawValue() { return m_dRightDrawValue; }
// 2021.11.05 ltg End
// 2022.04.28 Replay Start
	long m_lDepthReplay;
// 2022.04.28 Replay End
// 2022.09.14 Start
	char cFieldName[2000];			// 字段名称
// 2022.09.14 End
// 2023.09.16 Start
 	CCurveFilter* m_pCurveFilter;
	CString m_strReverseName;		// 曲线名称:如果是实时曲线，则是内存名称。如果是内存曲线，则是实时名称。
	short m_nDataFrom;
// 2023.09.16 End
};

#endif
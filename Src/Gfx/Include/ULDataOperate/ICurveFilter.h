/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2023-12-17
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __IULCURVEFILTER_H__
#define __IULCURVEFILTER_H__

#include "ULInterface.h"
#include "ulcommdef.h"
// Interface
interface ICurveFilter : IUnknown
{

};

// {E07834CB-38A7-46DF-B420-21537FBCE76E}
static const IID IID_ICurveFilter =
{ 0xe07834cb, 0x38a7, 0x46df, { 0xb4, 0x20, 0x21, 0x53, 0x7f, 0xbc, 0xe7, 0x6e } };

#endif

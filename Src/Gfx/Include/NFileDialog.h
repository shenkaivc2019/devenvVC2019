#if !defined(AFX_NFILEDIALOG_H__75DBBF4E_0E76_4C55_AC45_CDB40BEC3650__INCLUDED_)
#define AFX_NFILEDIALOG_H__75DBBF4E_0E76_4C55_AC45_CDB40BEC3650__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "FileDlgHelper.h"
#include "ULInterface.h"
#include "ULFileBaseDoc.h"

// NFileDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CNFileDialog dialog
#define IDC_BTN_SETTING 123456
class CNFileDialog : public CFileDialog
{
	DECLARE_DYNAMIC(CNFileDialog)

public:
	CNFileDialog(BOOL bOpenFileDialog, // TRUE for FileOpen, FALSE for FileSaveAs
		LPCTSTR lpszDefExt = NULL,
		LPCTSTR lpszFileName = NULL,
		DWORD dwFlags = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		LPCTSTR lpszFilter = NULL,
		CWnd* pParentWnd = NULL);

public:
	virtual int DoModal();

public:

	
protected:
	void ShowFileInfo();
	void CheckButtons(CString strExt);

	// Handle CDN_ notifications
	virtual void OnFileNameChange();
	virtual void OnFolderChange();
	virtual void OnTypeChange();
	
	//{{AFX_MSG(CNFileDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnCheckCopy();
	afx_msg void OnCheckForm();
	afx_msg void OnCheckScout();
	afx_msg void OnSetting();
	afx_msg void OnBtnAdvanced();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:
	CFileDlgHelper m_dlgHelper;
	CButton		m_btnCopy;
	CButton		m_btnForm;
	CButton		m_btnScout;
    CButton     m_btnSetting;
	BOOL		m_bCopy;
	BOOL		m_bForm;
	BOOL		m_bScout;

public:
	CString		m_strExt;
	vec_ic*		m_pCurves;
	CWnd*       m_pParentWnd;
	SAVEOPT*	m_pSaveOP;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NFILEDIALOG_H__75DBBF4E_0E76_4C55_AC45_CDB40BEC3650__INCLUDED_)

#if !defined(AFX_WIZARDDRILLPAGE_H__2CDC898E_AE6C_4833_AA34_AC7E73CB87CB__INCLUDED_)
#define AFX_WIZARDDRILLPAGE_H__2CDC898E_AE6C_4833_AA34_AC7E73CB87CB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GridCtrl.h"
#include "WPropertyPage.h"
// WizardDrillPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CWizardDrillPage dialog

class CWizardDrillPage : public CWPropertyPage
{
	DECLARE_DYNCREATE(CWizardDrillPage)

// Construction
public:
	CWizardDrillPage();
	~CWizardDrillPage();

// Dialog Data
	//{{AFX_DATA(CWizardDrillPage)
	enum { IDD = IDD_WIZARD_STEP2_DRILL };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA
	double		m_fBitDepth[8];
	double		m_fBitSize[8];
	double      m_fCasingStartDepth[8];
	double		m_fCasingDepth[8];
	double		m_fCasingSize[8];
	double		m_fCasingThickness[8];
	int         m_nBitPrc;
	int         m_nCasingPrc;
	int         m_nDepthPro;
	CComboBox	m_ComboBox;
	CGridCtrl	m_GridCtrl;

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CWizardDrillPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnKillActive();
	//}}AFX_VIRTUAL

	public:
	virtual int WriteInfo(CProject* pProject);
	virtual void ReadInfo(CProject* pProject);
	virtual BOOL CheckInput();
	virtual void InitGridCtrl();

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CWizardDrillPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeCombo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WIZARDDRILLPAGE_H__2CDC898E_AE6C_4833_AA34_AC7E73CB87CB__INCLUDED_)

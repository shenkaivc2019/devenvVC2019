#if !defined(AFX_DLGPARTRANGE_H__8DED657B_78FC_42F3_BD87_CDD91228B6DE__INCLUDED_)
#define AFX_DLGPARTRANGE_H__8DED657B_78FC_42F3_BD87_CDD91228B6DE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPartRange.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgPartRange dialog

class CDlgPartRange : public CDialog
{
// Construction
public:
	CDlgPartRange(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgPartRange)
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PART_RANGE };
#endif
	long m_lDepth0;
	long m_lDepth1;
	//}}AFX_DATA
	

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgPartRange)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgPartRange)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnButton1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	long m_lDepthA0;
	long m_lDepthA1;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPARTRANGE_H__8DED657B_78FC_42F3_BD87_CDD91228B6DE__INCLUDED_)

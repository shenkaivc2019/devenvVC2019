// CalcVolume.h: interface for the CCalcVolume class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CALCVOLUME_H__84244684_8DF7_4DE5_B554_512DE253883F__INCLUDED_)
#define AFX_CALCVOLUME_H__84244684_8DF7_4DE5_B554_512DE253883F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "IULFile.h"

class AFX_EXT_CLASS CCalcVolume  
{
public:
	CCalcVolume();
	virtual ~CCalcVolume();

	BOOL Process(CUnits* pUnits, IULFile* pFile);
};

#endif // !defined(AFX_CALCVOLUME_H__84244684_8DF7_4DE5_B554_512DE253883F__INCLUDED_)

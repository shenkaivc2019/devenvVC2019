﻿#if !defined(AFX_TEMPLACTIVEDLG_H__704F5A71_FB2D_4127_9058_4069CD6530BD__INCLUDED_)
#define AFX_TEMPLACTIVEDLG_H__704F5A71_FB2D_4127_9058_4069CD6530BD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TemplActiveDlg.h : header file
//
#ifndef ULVWBASE_API
	#ifdef ULVWBASE‌_EXPORT
		#define ULVWBASE_API __declspec( dllexport )
	#else	
		#define ULVWBASE_API __declspec( dllimport )
	#endif
#endif

/////////////////////////////////////////////////////////////////////////////
// CTemplActiveDlg dialog
class ULVWBASE_API CTemplActiveDlg : public CDialog
{
// Construction
public:
	CTemplActiveDlg(int nType, CDocument*pDoc, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTemplActiveDlg)
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TEMPL_ACTIVE_DLG };
#endif
	CListBox	m_ListSupported;
	CListBox	m_ListNotSupported;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTemplActiveDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void DoSelect(CString strTempl, UINT id);
	enum { ADDTEMPL, REMOVETEMPL };
	// Generated message map functions
	//{{AFX_MSG(CTemplActiveDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkList(UINT id);
	afx_msg void OnSelect(UINT id);
	afx_msg void OnCommand(UINT id);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:
	int		m_nType;
	CDocument* m_pDoc;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEMPLACTIVEDLG_H__704F5A71_FB2D_4127_9058_4069CD6530BD__INCLUDED_)

// LineTracker.h: interface for the CLineTracker class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LINETRACKER_H__16E5422F_A7C9_40D0_A052_0E865A3F66D4__INCLUDED_)
#define AFX_LINETRACKER_H__16E5422F_A7C9_40D0_A052_0E865A3F66D4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CLineTracker  
{
public:
	CLineTracker();
	virtual ~CLineTracker();

	enum
	{
		// values are compatible with corresponding CRectTracker values
		hitNothing = -1,
        hitBegin   = 0,
        hitEnd     = 2,
        hitMiddle  = 8
	};

	BOOL TrackRubberBand(CWnd* pWnd, CPoint point);
	virtual BOOL TrackHandle(int nHandle, CWnd* pWnd, CPoint point, CWnd* pWndClipTo = NULL);
	virtual BOOL SetCursor(CWnd* pWnd, UINT nHitTest);
	virtual int HitTest(CPoint point);
	virtual void GetHandleRect(int nHandle, CRect* pHandleRect);
	virtual void DrawDragRect(CDC* pDC, LPRECT lpRect);
	
	CRect m_rect;       // current position (always in pixels)
	int m_nHandleSize;  // size of resize handles (default from WIN.INI)
};

class CEllipseTracker : public CLineTracker
{
public:
	CEllipseTracker() {}
	~CEllipseTracker() {}
	virtual void DrawDragRect(CDC* pDC, LPRECT lpRect);
};

class CPolylineTracker : public CLineTracker
{
public:
	CPolylineTracker() { m_bThree = FALSE; }
	~CPolylineTracker() {}
	virtual BOOL TrackHandle(int nHandle, CWnd* pWnd, CPoint point, CWnd* pWndClipTo = NULL);
	
	BOOL m_bThree;
	CArray<CPoint, CPoint> m_points;
};

#endif // !defined(AFX_LINETRACKER_H__16E5422F_A7C9_40D0_A052_0E865A3F66D4__INCLUDED_)

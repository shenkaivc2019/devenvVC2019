// RemarkObject.h: interface for the CRemarkObject class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_REMARKOBJECT_H__AF1C2060_4770_4E3A_B40D_98745A72286B__INCLUDED_)
#define AFX_REMARKOBJECT_H__AF1C2060_4770_4E3A_B40D_98745A72286B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "XmlArchive.h"
#include "Picture.h"

class CBaseRemark; 
    class CRectRemark; 
        class CLineRemark; 
        class CPictureRemark; 
    class CPolygonRemark; 

//////////////////////////////////////////////////////////////////////
// CRemarkObject: 解释符号

class CRemarkObject : public CObject  
{
    DECLARE_SERIAL(CRemarkObject)
public:
	CRemarkObject();
	virtual ~CRemarkObject();
    virtual void Serialize(CArchive& ar); 

	void Draw(CDC* pDC, LPRECT lpRect);
    void Draw(CDC* pDC); 
    CRect GetRect(); 
    void SetRect(CRect rect); 
    void SetOriginalSize(CSize size); 

    void AddRemark(CBaseRemark* pRemark); 
    void AddPoint(CPoint point);
    void SetLastPoint(CPoint point); 
    void SelectObject(CPoint point); 
    void ClearSelection(); 
    CBaseRemark* GetSelectObject(); 
    void RemoveObject(CBaseRemark* pRemark); 
    void Clear(); 

    BOOL CanUndo(); 
    BOOL CanRedo(); 
    void Undo(); 
    void Redo(); 

    BOOL LoadFromFile(CString strFileName, BOOL bReportErr = TRUE); 
    BOOL SaveToFile(CString strFileName); 
    static CRemarkObject* PASCAL LoadSymbol(CString strFileName); 

protected:
    void SerializeObjectList(CXmlArchive& xml, CObList* pList); 

protected:
    CRect m_rcPosition;     // 符号在视图中的位置
    CSize m_size;           // 解释符号的原始大小
    CObList m_listRemark;   // 矢量图形的集合
    CObList m_listUndo;     // Undo 列表
    CObList m_listRedo;     // Redo 列表
};

//////////////////////////////////////////////////////////////////////
// CBaseRemark: 解释符号中矢量图形基类

class CBaseRemark : public CObject
{
    DECLARE_SERIAL(CBaseRemark)
public:
    CBaseRemark(); 
    virtual ~CBaseRemark(); 
    virtual void Serialize(CArchive& ar); 
    virtual void Serialize(CXmlArchive& ar); 
    virtual void Draw(CDC* pDC, CPoint ptTopLeft, double dRatioX, double dRatioY); 
    virtual void SetEndPoint(CPoint point); 
    virtual BOOL HitTest(CPoint point); 
    virtual CBaseRemark* Clone(); 

    HPEN GetPen(); 
    HBRUSH GetBrush(); 
    BOOL IsHitLine(int x1, int y1, int x2, int y2, CPoint point); 

public:
	static CString m_strFPath;	// 符号文件所在路径
    LOGPEN m_logPen;        // 边线样式
    LOGBRUSH m_logBrush;    // 填充样式
    BOOL m_bSelected;       // 是否被选中 
    BOOL m_bAdd;            // 撤消/重做的操作码，表示应添加还是删除。
}; 

//////////////////////////////////////////////////////////////////////
// CRectRemark: 解释符号中矩形图形

class CRectRemark : public CBaseRemark
{
    DECLARE_SERIAL(CRectRemark)
public:
    CRectRemark(); 
    virtual ~CRectRemark(); 
    virtual void Serialize(CArchive& ar); 
    virtual void Serialize(CXmlArchive& xml); 
    virtual void Draw(CDC* pDC, CPoint ptTopLeft, double dRatioX, double dRatioY); 
    virtual void SetEndPoint(CPoint point); 
    virtual BOOL HitTest(CPoint point); 
    virtual CBaseRemark* Clone(); 

public:
    CRect m_rect; // 矩形边界
}; 

//////////////////////////////////////////////////////////////////////
// CLineRemark: 解释符号中直线图形

class CLineRemark : public CRectRemark
{
    DECLARE_SERIAL(CLineRemark)
public:
    CLineRemark(); 
    virtual ~CLineRemark(); 
    virtual void Serialize(CArchive& ar); 
    virtual void Draw(CDC* pDC, CPoint ptTopLeft, double dRatioX, double dRatioY); 
    virtual BOOL HitTest(CPoint point); 
}; 

//////////////////////////////////////////////////////////////////////
// CPictureRemark: 解释符号中位图图形

class CPictureRemark : public CRectRemark, public CPicture
{
    DECLARE_SERIAL(CPictureRemark)
public:
    CPictureRemark(); 
    virtual ~CPictureRemark(); 
    virtual void Serialize(CArchive& ar); 
    virtual void Serialize(CXmlArchive& xml); 
    virtual void Draw(CDC* pDC, CPoint ptTopLeft, double dRatioX, double dRatioY); 
    virtual void SetEndPoint(CPoint point); 
    virtual CBaseRemark* Clone(); 

    BOOL LoadBitmap(CString strFileName); 

public:
    CString m_strFileName; // 位图路径
    BOOL m_bRepeat;        // 表示位图显示方式

protected:
    BOOL m_bNeedLoad; // 是否需要加载位图
    CBitmap m_bitmap; // 位图
}; 

//////////////////////////////////////////////////////////////////////
// CPolygonRemark: 解释符号中多边形图形

class CPolygonRemark : public CBaseRemark
{
    DECLARE_SERIAL(CPolygonRemark)
public:
    CPolygonRemark(); 
    virtual ~CPolygonRemark(); 
    virtual void Serialize(CArchive& ar); 
    virtual void Serialize(CXmlArchive& xml); 
    virtual void Draw(CDC* pDC, CPoint ptTopLeft, double dRatioX, double dRatioY); 
    virtual void SetEndPoint(CPoint point); 
    virtual BOOL HitTest(CPoint point); 
    virtual CBaseRemark* Clone(); 

    void AddPoint(CPoint point); 

protected:
    int m_nCount; // 点数
    LPPOINT m_pPoints; // 点集合
}; 

#endif // !defined(AFX_REMARKOBJECT_H__AF1C2060_4770_4E3A_B40D_98745A72286B__INCLUDED_)

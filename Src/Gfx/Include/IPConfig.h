// IPConfig.h: interface for the CIPConfig class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IPCONFIG_H__21CA1A67_2A6E_40BE_881D_C78585AE5454__INCLUDED_)
#define AFX_IPCONFIG_H__21CA1A67_2A6E_40BE_881D_C78585AE5454__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxsock.h>

class CIPConfig  
{
public:
	CIPConfig();
	virtual ~CIPConfig();
	
	void GetBroadcastIP();
	void RecvReq();
	void GetAllServers();

	BOOL StartServer(LPCTSTR pszServer, u_short uPort = 28001);
	CString GetServerIP(LPCTSTR pszServer);
	UINT GetServerPort(LPCTSTR pszServer, UINT nDefault = 27000);
	CString RegToServer(LPCTSTR pszServer, LPCTSTR pszKey, long lTimeOut = 5);

public:
	CString m_strFile;
	CString m_strServer;
	HANDLE m_hThread;
	HANDLE m_hThreadR;
	CString m_strIP;

	SOCKET&  m_s;
	SOCKET	m_socket;
	SOCKET  m_socketR;

	CStringList m_lstServers;

	CString m_strActive;
	long m_lThread;
	long m_lRegistered;
};

#endif // !defined(AFX_IPCONFIG_H__21CA1A67_2A6E_40BE_881D_C78585AE5454__INCLUDED_)

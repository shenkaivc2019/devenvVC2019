#if !defined(AFX_PATTERNPICKER_H__7F21F6B7_63E2_4C38_9979_BC813B554D8D__INCLUDED_)
#define AFX_PATTERNPICKER_H__7F21F6B7_63E2_4C38_9979_BC813B554D8D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define PPN_SELENDOK (WM_USER + 1100)

// PatternPicker.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPatternPicker window

class CPatternPicker : public CButton
{
// Construction
public:
	CPatternPicker();

// Attributes
public:
	COLORREF m_clrFore;
	COLORREF m_clrBk;
	CString	m_strPattern;
	DWORD	m_dwType;
	
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPatternPicker)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CPatternPicker();

	// Generated message map functions
protected:
	//{{AFX_MSG(CPatternPicker)
	afx_msg void OnClicked();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PATTERNPICKER_H__7F21F6B7_63E2_4C38_9979_BC813B554D8D__INCLUDED_)

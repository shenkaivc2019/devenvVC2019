#if !defined(AFX_STOPCTRL_H__CE874722_5E73_4D70_8A43_D6A14564EE17__INCLUDED_)
#define AFX_STOPCTRL_H__CE874722_5E73_4D70_8A43_D6A14564EE17__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define EM_NONE 0
#define EM_HORZ 1
#define EM_VERT 2
#define EM_BOTH 3
#define EM_RESTORE 4
#define EM_BOUNDS 8
#define EM_AUTOSCROLL 16

#define UM_SELECTCHANGED (WM_USER + 1)
#define UM_POSCHANGED  (WM_USER + 2)

#define STOP_WIDTH 6

// StopCtrl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CStopCtrl window

class AFX_EXT_CLASS CStopCtrl : public CWnd
{
// Construction
public:
	CStopCtrl(DWORD dwMove = EM_NONE, COLORREF clrFace = RGB(0, 0, 0));

// Attributes
public:
	COLORREF m_clrFace;
	DWORD m_dwMove;
	BOOL m_bIsMoving;
	BOOL m_bIsSelected;
	CPoint m_point0; // Old point
	CPoint m_point;  // Current point
	CRect m_rect0; // Old rect
	CRect m_rect;
	CRect m_rectP;
	CRect m_rectB;
	DWORD m_data;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStopCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CStopCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CStopCtrl)
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STOPCTRL_H__CE874722_5E73_4D70_8A43_D6A14564EE17__INCLUDED_)

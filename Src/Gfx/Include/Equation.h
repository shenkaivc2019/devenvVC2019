// Equation.h: interface for the CEquation class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EQUATION_H__D68384AD_9F88_4BE0_906A_2F12A02ACC80__INCLUDED_)
#define AFX_EQUATION_H__D68384AD_9F88_4BE0_906A_2F12A02ACC80__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "XmlArchive.h"


//////////////////////////////////////////////////////////////////////
// CParameter: 方程参数

class CParameter : public CObject  
{
    DECLARE_SERIAL(CParameter)
public:
	CParameter(BOOL bVariable = FALSE);
	virtual ~CParameter();
    virtual void Serialize(CArchive& ar); 
    virtual void Serialize(CXmlArchive& xml, LPCTSTR lpszKeyName = NULL); 
    virtual CParameter* Clone(); 

public:
    BOOL m_bVariable;           // 是否为自变量
    CString m_strName;          // 参数名称
    double m_dValue;            // 参数值
};


// 运算符类型
#define OT_PLUS             0       // 加
#define OT_MINUS            1       // 减
#define OT_MULTIPLY         2       // 乘
#define OT_DIVIDE           3       // 除
#define OT_POWER            4       // 乘方
#define OT_LOGARITHM_10     5       // 对数(10底)
#define OT_LOGARITHM_E      6       // 对数(e底)
#define OT_LEFT_BRACKET     7       // 左括号
#define OT_RIGHT_BRACKET    8       // 右括号
#define OT_BOUNDARY         9       // 起始，结束
#define OT_MAX              10      // 总数


//////////////////////////////////////////////////////////////////////
// COperator: 方程运算符

class COperator : public CObject  
{
    DECLARE_SERIAL(COperator)
public:
    COperator(UINT nType = OT_PLUS); 
	virtual ~COperator();
    virtual double Calculate(double a, double b = 1); 
    int GetParamCount(); 

public:
    UINT m_nOpType;                 // 运算符类型
    CString m_strText;              // 运算符表示
    int m_nLeftPrecede;             // 左优先级
    int m_nRightPrecede;            // 右优先级
};


//////////////////////////////////////////////////////////////////////
// CEquation: 曲线方程

class CEquation : public CObject  
{
    DECLARE_SERIAL(CEquation)
public:
    CEquation(LPCTSTR lpszText = NULL); 
	virtual ~CEquation();
    virtual void Serialize(CArchive& ar); 
    virtual void Serialize(CXmlArchive& xml, LPCTSTR lpszKeyName = NULL); 
    virtual double Calculate(double x);

    CParameter* GetVariable(); 
    void AddParam(CParameter* pParam); 
    BOOL SetText(LPCTSTR lpszText = NULL, BOOL bSilence = TRUE);    // bSilence 表示是否进行错误报告
    CString GetText(); 
    BOOL Parse();

protected:
    int GetNextOperator(CString strText, UINT* nType); 
    CParameter* GetParameter(CString strName); 

public:
    CString m_strText;                  // 表达式
    CObList m_listParam;                // 给定的参数

protected:
    BOOL m_bSuccess;                    // 解析是否成功
    CObList m_listOperator;             // 已定义的运算符
    CObList m_listItem;                 // 解析的运算符与参数
};

#endif // !defined(AFX_EQUATION_H__D68384AD_9F88_4BE0_906A_2F12A02ACC80__INCLUDED_)

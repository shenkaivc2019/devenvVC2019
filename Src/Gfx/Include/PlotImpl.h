/*++

--*/
#ifndef __PLOTIMPL_H
#define __PLOTIMPL_H

#include "IPlot.h"
#include "ULCommdef.h"
#include "Line.h"
#include "PrintInfo.h"
#include <math.h>
#include <atlcom.h>

//extern CFont g_font[ftCount];
//#define _LPTODP

template <class TBase, DWORD dwReserved = 0>
class ATL_NO_VTABLE IPlotImpl : public IPlot
{
public:
	// Constructor
	IPlotImpl()
	{
		m_pTrack = NULL;
		m_pGridProp = NULL;
	}

	// IPlot
	STDMETHOD(Advise)(ITrack* pHolder)
	{
		if (pHolder == NULL)
			return E_FAIL;

		m_pTrack = pHolder;	
		ASSERT(m_pTrack != NULL);
		if (m_pTrack == NULL)
			return E_FAIL;

		m_pGridProp = (GridProp*)m_pTrack->GetGridProp();
		ASSERT(m_pGridProp != NULL);
		if (m_pGridProp == NULL)
			return E_FAIL;

		return S_OK;
	}

	// 绘制井道头曲线
	STDMETHOD(DrawHeadCurve)(VARIANT* dc, ICurve* pCurve, LPRECT lpRect, BOOL bDrawLine = TRUE)
	{
		CDC* pDC = static_cast<CDC*>(dc->byref);
		if (pDC == NULL)
			return S_FALSE;

		CRect rc = lpRect;
		int nResult = 0;
		
		unsigned Type[8];
		CDashLine DashLine(*pDC, Type, CDashLine::GetPattern(Type, 0, pCurve->LineWidth(),CDashLine::DL_SOLID));
		
		COLORREF clrCurve = pCurve->Color();
		CPen* pPen = DashLine.GetRightPen(pCurve->LineStyle(),pCurve->LineWidth(), clrCurve);
		COLORREF cr = pDC->SetTextColor(clrCurve);
		
		CPen* pOldPen = pDC->SelectObject(pPen);
		CFont* pOldFont = pDC->SelectObject((CFont*)m_pTrack->GetFont(ftCT));
		pDC->SetBkMode(TRANSPARENT);
		CStringArray names;
		int i = pCurve->GetTitle(&names);
		int nY = rc.top + 45;
		CRect rcText = rc;
		rcText.bottom -= 45;
		rcText.top = nY;
		int nH = rcText.Height()/((i%2) ? i : (i + 1));
		for (--i; i > -1; i--)
		{
			rcText.bottom = rcText.top + nH;
			pDC->DrawText(names[i], rcText,
				DT_SINGLELINE | DT_VCENTER | DT_CENTER);
			rcText.top = rcText.bottom;
		}
		
		pDC->SelectObject((CFont*)m_pTrack->GetFont(ftCU));
		CString strVal;
		if (bDrawLine)
		{
			DashLine.MoveTo(rc.left, nY);
			DashLine.LineTo(rc.right, nY); // 中间横线		
			
			rcText.left += 4;
			rcText.right -= 4;
			rcText.top = nY;
			rcText.bottom = rcText.top + nH;
			
			TCHAR szVal[32];	
			//if (m_nType == UL_TRACK_WAVE)
			{
				pCurve->GetStartTime(szVal);
			}
			//else
			{
				pCurve->GetLeftVal(szVal);
			}
			
			CString strVal = szVal;
			strVal.TrimLeft(); 
			strVal.TrimRight();
			
			pDC->DrawText(strVal, rcText, DT_SINGLELINE | DT_VCENTER | DT_LEFT);
			
			//if (m_nType == UL_TRACK_WAVE)
			{
				pCurve->GetEndTime(szVal);
			}
			//else
			{
				pCurve->GetRightVal(szVal);
			}
			
			strVal = szVal;
			strVal.TrimLeft(); 
			strVal.TrimRight();
			
			pDC->DrawText(strVal, rcText, DT_SINGLELINE | DT_VCENTER | DT_RIGHT);
		}
		
		strVal = pCurve->Unit();
		
		rc.bottom = nY;
		if (strVal.GetLength())
			pDC->DrawText(strVal, rc, DT_SINGLELINE | DT_VCENTER | DT_CENTER);
		
		pDC->SetTextColor(cr);
		pDC->SelectObject(pOldFont);
		pDC->SelectObject(pOldPen);
		delete pPen;

		return S_OK;
	}

	// 绘制曲线
	STDMETHOD(DrawCurve)(VARIANT* dc, ICurve* pCurve, long lStart, long lEnd)
	{
		DrawCurveDefault(dc, pCurve, lStart, lEnd, 0);
		return S_OK;
	}

	// 绘制实时曲线
	STDMETHOD(LogDrawCurve)(VARIANT* dc, ICurve* pCurve, long lStart, long lEnd)
	{
		DrawCurveDefault(dc, pCurve, lStart, lEnd, 1);
		return S_OK;
	}

	STDMETHOD(ChangeGridCount)()
	{
		return S_OK;
	}

	STDMETHOD(Print)(VARIANT* pInfo, ICurve* pCurve, double fMaxDepth, double fMinDepth)
	{
		return S_OK;
	}
	STDMETHOD(StaticPrint)(VARIANT* pInfo, ICurve* pCurve, double fMaxDepth, double fMinDepth)
	{
		return S_OK;
	}

	STDMETHOD(GetCurveXPos)(ICurve* pCurve, long lDepth, BOOL bRolled = TRUE)
	{
		return S_OK;
	}
	STDMETHOD(PrintHeadCurve)(VARIANT* printinfo,ICurve* pCurve, LPRECT lpRect)
	{
		CULPrintInfo* pPrintInfo = static_cast<CULPrintInfo*>(printinfo->byref);
		if (pPrintInfo == NULL)
			return S_OK;

		CRect rect = lpRect;
		
		unsigned Type[8];
		CDashLine DashLine(*pPrintInfo->GetPrintDC(), Type,
			CDashLine::GetPattern(Type, 0,
			pCurve->LinePWidth(), CDashLine::DL_SOLID));
		
		//COLORREF crCurve = Gbl_ConfigInfo.m_Plot.Colored ? pCurve->m_crColor : 0;
		COLORREF crCurve = pCurve->Color();
		CPen* pPen = DashLine.GetRightPen(pCurve->LineStyle(),
			pCurve->LinePWidth(), crCurve);
		
		COLORREF cr = pPrintInfo->GetPrintDC()->SetTextColor(crCurve);
		
		CPen* pOldPen = pPrintInfo->GetPrintDC()->SelectObject(pPen);
		
		// 名称
		CString strLeft, strRight;
		CString strFmt;
		pCurve->LeftDotNum();
		strFmt.Format(_T("%%7.%dlf"), pCurve->LeftDotNum());
		
		strLeft.Format(strFmt, pCurve->LeftVal());
		strLeft.TrimLeft(); strLeft.TrimRight();
		
		strFmt.Format(_T("%%7.%dlf"), pCurve->RightDotNum());
		strRight.Format(strFmt, pCurve->RightVal());
		strRight.TrimLeft(); strRight.TrimRight();

		CStringArray names;
		int i = pCurve->GetTitle(&names);
		
		CString strUnit = pCurve->Unit();
		int nDirection = pPrintInfo->GetPrintDirection();
		int nBkMode = pPrintInfo->GetPrintDC()->SetBkMode(TRANSPARENT);
		if (nDirection == SCROLL_UP)
		{
			CFont* font = (CFont*)m_pTrack->GetFont(ftCT);
			CFont* pOldFont = pPrintInfo->SetFont(font, 1800);

			TEXTMETRIC tm;
			pPrintInfo->GetPrintDC()->GetTextMetrics(&tm);
			UINT nFlags = pPrintInfo->GetPrintDC()->SetTextAlign(TA_RIGHT);
			
			int y = rect.bottom - sp_y;
			CRect rcText = rect;
			rcText.top += sp_y;
			rcText.bottom = y;
			int nH = rcText.Height()/((i%2) ? i : (i + 1));
			rcText.OffsetRect(0, -abs(tm.tmHeight));
			for (--i; i > -1; i--)
			{
				rcText.top = rcText.bottom - nH;
				pPrintInfo->GetPrintDC()->DrawText(names[i], rcText,
					DT_SINGLELINE | DT_VCENTER | DT_CENTER | DT_NOCLIP);
				rcText.bottom = rcText.top;
			}
			
			rcText.left += sp_x;
			rcText.right -= sp_x;
			rcText.bottom = y;
			rcText.top = rcText.bottom - nH;
			
			font = (CFont*)m_pTrack->GetFont(ftCU);
			pPrintInfo->SetFont(font, 1800);
			pPrintInfo->GetPrintDC()->GetTextMetrics(&tm);
			
			rcText.OffsetRect(0, -abs(tm.tmHeight));
			pPrintInfo->GetPrintDC()->DrawText(strRight, rcText,
				DT_SINGLELINE | DT_VCENTER | DT_LEFT | DT_NOCLIP);
			
			pPrintInfo->GetPrintDC()->DrawText(strLeft, rcText,
				DT_SINGLELINE | DT_VCENTER | DT_RIGHT | DT_NOCLIP);
			
			DashLine.MoveTo(rect.left + 2, y);
			DashLine.LineTo(rect.right - 2, y);
			
			if (strUnit.GetLength())
			{
				rcText.top = y;
				rcText.bottom = rcText.top + nH;
				rcText.OffsetRect(0, -abs(tm.tmHeight));
				pPrintInfo->GetPrintDC()->DrawText(strUnit, rcText,
					DT_SINGLELINE | DT_VCENTER | DT_CENTER | DT_NOCLIP);
			}
			
			pPrintInfo->GetPrintDC()->SetTextAlign(nFlags);
			pPrintInfo->GetPrintDC()->SelectObject(pOldFont);
		}
		else
		{
			CFont* font = (CFont*)m_pTrack->GetFont(ftCT);
			CFont* pOldFont = pPrintInfo->SetFont(font);
			UINT nFlags = pPrintInfo->GetPrintDC()->SetTextAlign(TA_LEFT);
			int y = rect.top + sp_y;
			CRect rcText = rect;
			rcText.bottom -= sp_y;
			rcText.top = y;
			int nH = rcText.Height()/((i%2) ? i : (i + 1));
			for (--i; i > -1; i--)
			{
				rcText.bottom = rcText.top + nH;
				pPrintInfo->GetPrintDC()->DrawText(names[i], rcText,
					DT_SINGLELINE | DT_VCENTER | DT_CENTER | DT_NOCLIP);
				rcText.top = rcText.bottom;
			}
			
			rcText.left += sp_x;
			rcText.right -= sp_x;
			rcText.top = y;
			rcText.bottom = rcText.top + nH;
			
			font = (CFont*)m_pTrack->GetFont(ftCU);
			pPrintInfo->SetFont(font);
			pPrintInfo->GetPrintDC()->DrawText(strLeft, rcText,
				DT_SINGLELINE | DT_VCENTER | DT_LEFT | DT_NOCLIP);
			
			pPrintInfo->GetPrintDC()->DrawText(strRight, rcText,
				DT_SINGLELINE | DT_VCENTER | DT_RIGHT | DT_NOCLIP);
			
			DashLine.MoveTo(rect.left + 2, y);
			DashLine.LineTo(rect.right - 2, y);
			
			if (strUnit.GetLength())
			{
				rcText.bottom = y;
				rcText.top = rcText.bottom - nH;
				pPrintInfo->GetPrintDC()->DrawText(strUnit, rcText,
					DT_SINGLELINE | DT_VCENTER | DT_CENTER | DT_NOCLIP);
			}
			
			pPrintInfo->GetPrintDC()->SetTextAlign(nFlags);
			pPrintInfo->GetPrintDC()->SelectObject(pOldFont);
		}
		pPrintInfo->GetPrintDC()->SetBkMode(nBkMode);
		pPrintInfo->GetPrintDC()->SetTextColor(cr);
		pPrintInfo->GetPrintDC()->SelectObject(pOldPen);
		
		delete pPen;
		

		return S_OK;
	}

protected:
	// 绘制曲线
	HRESULT DrawCurveDefault(VARIANT* dc, ICurve* pCurve, long lStartDepth, long lEndDepth, int nMode)
	{
		CDC* pDC = static_cast<CDC*>(dc->byref);
		if (!pCurve->IsVisible())
		{
			return UL_ERROR;
		}
	
		int nData = pCurve->GetDataSize();
		if (nData < 1)
			return UL_ERROR;
		
		int nLeftMargin = pCurve->LeftMargin(FALSE);	// 左格子宽度
		int nRightMargin = pCurve->RightMargin(FALSE);	// 右格子宽度
		if (nLeftMargin == nRightMargin)
			return UL_ERROR;
	
		// 求出x方向每个像素所代表的值
		double fXValuePerPixel = pCurve->RLdVal()/(float)(nRightMargin - nLeftMargin);
		
		int y, nEnd = 0;
		CArray<CPoint, CPoint> PointArray;
		PointArray.SetSize(0, 8*sizeof(CPoint));
		CPoint point;
		long lDepth, lTime;
		
		if (m_pTrack->GetDriveMode() == UL_DRIVE_DEPT)
		{
			long lStart = lStartDepth;
			long lEnd = lEndDepth;
			int nDisplayPos = pCurve->GetCurDisplayPos();
			
			// To avoid solid line been cut
			if (pCurve->LineStyle() == PS_SOLID)
			{   
				nDisplayPos -= 10;
			}
			
			if (nDisplayPos < 0)
				nDisplayPos = 0;

			//Modify by xx 20161110 不能遍历整条曲线

			//修改神开曲线绘制	
			if (pCurve->GetDepthRange(&lStart, &lEnd, nMode ? nDisplayPos : 0))
				return UL_ERROR;
// 			lStart = 0;
// 			lEnd = pCurve->GetDataSize() - 1;
			
			if (lStart > 0)
			{
				lStart--;
			}
			
			if (nMode == 0)
			{
				lEnd += 2;
				if (lEnd >= nData)
					lEnd = nData-1;
			}
		
#ifdef _LPTODP
			CPoint pt0(LONG_MAX, LONG_MAX);
#endif
			nEnd = lEnd;
			
			for (int i = lStart; i <= lEnd; i++)
			{
				lDepth = pCurve->GetDepth(i);
				DWORD dwMode = pCurve->GetDataMode(i);
			//	if (pCurve->GetDataModeEx(i,1))
				if (dwMode&CURVE_DATAMODE_BAD)
				{
					continue;
				}
				y = m_pTrack->GetCoordinateFromDepth(lDepth);
				
#ifdef _LPTODP
				CPoint pt1(0, y);
				pDC->LPtoDP(&pt1);
				if (pt0.y == pt1.y) 
					continue;
				pt0.y = pt1.y;		
#else
				if (point.y == y)//在深度相同时，曲线无法显示其他值，只能显示第一个值
					continue;
#endif
				point.y = y;
				point.x = nLeftMargin +
					(double)
					((pCurve->GetValueEx(i) - pCurve->LeftVal()) /
					fXValuePerPixel);
				PointArray.Add(point);
			}
		}
		else
		{
			long lStartTime = pCurve->GetTime(0);
			long lEndTime = pCurve->GetTime(nData - 1);

			long lStart = lStartDepth;
			long lEnd = lEndDepth;
			int nDisplayPos = pCurve->GetCurDisplayPos();
			
			// To avoid solid line been cut
			if (pCurve->LineStyle() == PS_SOLID)
			{   
				nDisplayPos -= 10;
			}
			
			if (nDisplayPos < 0)
				nDisplayPos = 0;
			
			if (pCurve->GetTimeRange(&lStart, &lEnd, nMode ? nDisplayPos : 0))
				return UL_ERROR;
			
			if (lStart > 0)
			{
				lStart--;
			}
			
			if (nMode == 0)
			{
				lEnd += 2;
				if (lEnd >= nData)
					lEnd = nData-1;
			}
			
			
			  long nStart = lStart;
			  nEnd = lEnd;			
			

			/*int nStart = pCurve->GetFirstIndexByTime(lStartDepth, 1);
			int nEnd = pCurve->GetFirstIndexByTime(lEndDepth, 1);
			nStart = nStart > 0? nStart : 0;
			nEnd = nEnd > 0? nEnd : 0;
			
			if(nStart > nEnd)
			{
				int nTemp = nStart;
				nStart = nEnd;
				nEnd = nTemp;
			}*/



			for(int i = nStart ; i < nEnd ; i++)
			{
				lTime = pCurve->GetTime (i);
				if(lTime < lStartTime)
					continue;
				if(lTime > lEndTime)
					break;
				
				DWORD dwMode = pCurve->GetDataMode(i);
			//	if (pCurve->GetDataModeEx(i,1))
				if (dwMode & CURVE_DATAMODE_BAD)
				{
					continue;
				}
				y = m_pTrack->GetCoordinateFromTime(lTime);
				point.y = y;
				point.x = nLeftMargin +
					(int)
					((pCurve->GetValueEx(i) - pCurve->LeftVal()) /
					fXValuePerPixel);
				PointArray.Add(point);
			}
		}
		
	/*	if (PointArray.GetSize() < 2)
		{
			return UL_ERROR;
		}*/
		
		return DrawRoll(pCurve, pDC, PointArray.GetData(), PointArray.GetSize(), nMode, nEnd);
	}

	HRESULT DrawRoll(ICurve* pCurve, CDC* pDC, LPPOINT lpPoints, int nCount, 
		BOOL bRuntime, int nEnd /* = 0 */)
	{
		if (nCount < 1)
			return UL_NO_ERROR;

		//Start
		//在随钻测井时当曲线往回运动时，曲线也需要正常绘制，使用数据库后改功能取消
		//将lpPoints中所有数据点排序
		CPoint ptTemp;
		if(nCount >= 3)
		{
			long nSD = lpPoints[0].y; 
			long nED = lpPoints[nCount - 1].y;

	//		if(nSD > nED)
			{
				for(int i = 0 ; i < nCount - 1 ; i++)
				{
					for(int j = 0 ; j< nCount - 1 ; j++)
					{
						if(lpPoints[j].y < lpPoints[j + 1].y)
						{
							ptTemp = lpPoints[j];
							lpPoints[j] = lpPoints[j + 1];
							lpPoints[j + 1] = ptTemp;
						}
					}
				}
			}
	/*		else
			{
				for(int i = 0 ; i < nCount - 1 ; i++)
				{
					for(int j = i ; j< nCount - 1 ; j++)
					{
						if(lpPoints[j].y > lpPoints[j + 1].y)
						{
							ptTemp = lpPoints[j];
							lpPoints[j] = lpPoints[j + 1];
							lpPoints[j + 1] = ptTemp;
						}
					}
				}
			}*/
		}

		//End

		if (!pCurve->IsRewind())
		{
			DrawNoRoll(pCurve, pDC, lpPoints, nCount, bRuntime, nEnd);
			return UL_NO_ERROR;
		}


		CArray<CPoint, CPoint> Points;
		Points.RemoveAll();
		
		// shenkun add, 2003-9-16 - 控制曲线回绕次数
		int nRollCount;

		if (nCount == 1)
		{
			pDC->SetPixel(lpPoints[0], pCurve->Color());
			return UL_NO_ERROR;
		}
		
		unsigned Type[8];
		int w = pCurve->LineWidth();
		COLORREF color = pCurve->Color();
		CDashLine DL(*pDC, Type, CDashLine::GetPattern(Type, 0, w, CDashLine::DL_SOLID));
		if (pCurve->IsSelect())
		{
			w += 6;
			int r = 0, g = 0, b = 0;
			color &= 0x00ffffff;
			b = (color & 0x00ff0000); b = b / 3 + 96; 
			g = (color & 0x0000ff00); g = g / 3 + 96; 
			r = (color & 0x000000ff); r = r / 3 + 96;
			color = RGB(r, g, b);
			if (color == 0)
				color = RGB(96, 96, 96);
		}
		CPen* pPen = DL.GetRightPen(pCurve->LineStyle(), w, color);
		CPen* pOldPen = pDC->SelectObject(pPen);	
		
		long Distance, Width;	
		long OutSegment;
		double oldValue;	// oldValue用来确定出界段长
		
		CPoint oldPoint;
		long oldGradeRight, gradeRight, oldGradeLeft, gradeLeft, oldLeft, left;
		double value;
		
		int nLeftMargin = pCurve->LeftMargin(FALSE);	// 得到左右格子的坐标 
		int nRightMargin = pCurve->RightMargin(FALSE);
		
		// Add by YF 2003-9-5 
		// divided by zero
		if ((nRightMargin - nLeftMargin) == 0)
			return UL_NO_ERROR;
		
		// 取出第一各点并判断是否有越界
		// 第一点的处理
		// oldLeft = lpPoints[nCount-1).x - nLeftMargin;
		// oldPoint.y = lpPoints[nCount-1).y;
		oldLeft = lpPoints[0].x - nLeftMargin;
		oldPoint.y = lpPoints[0].y;
		oldGradeRight = (LONG) floor(oldLeft / (nRightMargin - nLeftMargin));		
		
		if (oldLeft < 0)		// 判断曲线是否越界：left<0则曲线越界
		{
			// 如果越界，求出倍数
			oldGradeLeft = (LONG) floor(abs(oldLeft)/(nRightMargin - nLeftMargin)) + 1;
		}
		
		if ((oldGradeRight > 0) || (oldLeft < 0))  // 曲线越右界，曲线越左界
		{
			if (oldGradeRight > 0)
			{
				oldPoint.x = lpPoints[0].x - oldGradeRight * (nRightMargin - nLeftMargin);
			}
			else
			{
				oldPoint.x = lpPoints[0].x + oldGradeLeft * (nRightMargin - nLeftMargin);
			}
		}
		else // 曲线正常
		{
			oldPoint.x = lpPoints[0].x;
		}
		
		if (bRuntime)
			DL.MoveToEx(oldPoint, pCurve->GetPat(), pCurve->GetStretch());
		else
			DL.MoveTo(oldPoint);
		
		Points.Add(oldPoint);
		oldValue = oldPoint.x;	// tyh1011
		
		// 其他点的处理
		// for(int pointSuffix = nCount- 2; pointSuffix >= 0; pointSuffix--)
		for (int pointSuffix = 1; pointSuffix < nCount ; pointSuffix++)
		{
			//Modify by xx 20150909
			//判断纵坐标，如果和前一个点相同或者相近则不画
			if (lpPoints[pointSuffix].y == oldPoint.y)
			{
				continue;
			}

			gradeRight = (LONG)floor((lpPoints[pointSuffix].x - nLeftMargin)/(nRightMargin - nLeftMargin));
			left = lpPoints[pointSuffix].x - nLeftMargin;
			if (left < 0)
			{
				gradeLeft = (LONG) floor(abs(left)/(nRightMargin - nLeftMargin)) + 1;
			}
			
			if ((gradeRight >= 1) || (left < 0))  // 曲线越右界,曲线越左界
			{
				if (gradeRight >= 1)
				{
					value = lpPoints[pointSuffix].x - gradeRight * (nRightMargin - nLeftMargin);
				}
				else
				{
					value = lpPoints[pointSuffix].x + gradeLeft * (nRightMargin - nLeftMargin);
				}
			}
			else	 // 曲线正常
			{
				value = lpPoints[pointSuffix].x;
			}
			// Distance = abs(lpPoints[pointSuffix].x - lpPoints[pointSuffix+1].x); // tyh 1013
			Distance = abs(lpPoints[pointSuffix].x - lpPoints[pointSuffix - 1].x);
			Width = nRightMargin - nLeftMargin; // tyh 1013
			
			// 曲线由正常(0)变为越右界(1)，则先画与右边界的交点，再画与左边界的交点
			// 曲线由越左界(0)变为正常(1)，则先画右边界的交点，再画与左边界的交点
			if ((gradeRight > oldGradeRight) || (oldLeft < 0 && left >= 0))
			{
				// oldPoint.x = nRightMargin;
				// DashLine.LineTo(nRightMargin, oldPoint.y);
				
				// 画到右边界,后从左边界开始画
				// oldPoint.y = lpPoints[pointSuffix].y;
				// DashLine.MoveTo(nLeftMargin,oldPoint.y);
				
				//////////////////// tyh test  1019 /////////////////////右越界
				int nCount = 0;
				double tempY;
				double VarifyY = 0, IncreamY = 0, tempX = 0;
				
				OutSegment = long(oldValue + Distance - nRightMargin); // 出界段的大小			
				
				// 由三角形等比关系求出越界时当前点距边界点Y的可变增量以及每次越界时距产生Y方向的不变增量,
				// (由于线每次越界折回后与原先的线平行,故增量不变.)
				tempX = nRightMargin - oldValue;// tempX为前一点距越界边界的距离
				if (Distance == 0)
					VarifyY = 0;// 当前点到边界的落差(可变增量)
				else
				{
					//VarifyY = (tempX/Distance)*(lpPoints[pointSuffix].y-lpPoints[pointSuffix+1].y);// 可变增量
					VarifyY = (tempX / Distance) * (lpPoints[pointSuffix].y - lpPoints[pointSuffix - 1].y);
				}
				
				if (tempX == 0)
					IncreamY = 0;
				else
					IncreamY = VarifyY / tempX * Width;// // 两个边界之间的落差(不变增量):与可变增量有关.(三角关系可推)width/temp的比值与m_fPrintReviseX无关
				
				tempY = oldPoint.y + VarifyY;
				// 越界画到越界边界,移到相反的边界.
				nRollCount = 0;
				
				do
				{
					DL.LineTo(nRightMargin, (int) tempY);
					Points.Add(CPoint(nRightMargin, tempY));
					DL.MoveTo(nLeftMargin, (int) tempY);
					Points.Add(CPoint(nLeftMargin, tempY));
					tempY += IncreamY;
					if (nCount > 0)
						OutSegment -= Width;
					nCount++;
					nRollCount++;
				}
				while (OutSegment > Width && nRollCount < pCurve->GetMaxRollCount());
				//////////////////////////////// test end//////////////////// 
			}
			
			// 如果曲线由越右界(1)变为正常(0)，先画与左边界的交点，再画与右边界的交点
			// 如果曲线由正常(1)到越左边界(0)，先画与左边界的焦点，再画与右边界的交点
			else if ((gradeRight < oldGradeRight) || (oldLeft >= 0 && left < 0))//
			{
				// oldPoint.x = nLeftMargin;
				// DashLine.LineTo(nLeftMargin, oldPoint.y);
				
				// 画到左边界后从右边界开始画
				// DashLine.MoveTo(oldPoint.x+nRightMargin-nLeftMargin,oldPoint.y);
				// 与越右界的画线算法不一至，所以修改为下面的语句
				// oldPoint.y = lpPoints[pointSuffix).y;
				// DashLine.MoveTo(nRightMargin,oldPoint.y);
				
				//////////////////////// tyh 1019//////////////////// 
				int nCount = 0;			
				double VarifyY = 0, IncreamY = 0, tempX = 0;
				double tempY;// 移动后的深度
				
				OutSegment = abs(int(nLeftMargin - (oldValue - Distance))); // 出界段的大小			
				
				// 由三角形等比关系求出越界时当前点距边界点Y的可变增量以及每次越界时距产生Y方向的不变增量,
				// (由于线每次越界折回后与原先的线平行,故增量不变.)
				tempX = abs(int(nLeftMargin - oldValue));	// tempX为前一点距越界边界的距离
				if (Distance == 0)
					VarifyY = 0;	// 当前点到边界的落差(可变增量)
				else
				{
					// VarifyY = (tempX/Distance)*(lpPoints[pointSuffix).y-lpPoints[pointSuffix+1).y);// 可变增量
					VarifyY = (tempX / Distance) * (lpPoints[pointSuffix].y - lpPoints[pointSuffix-1].y);// 可变增量
				}
				
				if (tempX == 0)
					IncreamY = 0;
				else
					IncreamY = VarifyY / tempX * Width; // 两个边界之间的落差(不变增量):与可变增量有关.(三角关系可推)width/temp的比值与m_fPrintReviseX无关
				
				tempY = oldPoint.y + VarifyY;
				// 越界画到越界边界,移到相反的边界.
				nRollCount = 0;
				do
				{
					DL.LineTo(nLeftMargin, (int) tempY);
					Points.Add(CPoint(nLeftMargin, tempY));
					DL.MoveTo(nRightMargin, (int) tempY);
					Points.Add(CPoint(nRightMargin, tempY));
					tempY += IncreamY;
					if (nCount > 0)
						OutSegment -= Width;
					nCount++;
					nRollCount++;
				}
				while (OutSegment > Width && nRollCount < pCurve->GetMaxRollCount());
			}
			
			oldGradeRight = gradeRight;    // 如果他们相等则表明他们同为越界或者同为正常			 
			oldPoint.y = lpPoints[pointSuffix].y;
			oldPoint.x = (long) value;
			
			DL.LineTo(oldPoint);
			Points.Add(oldPoint);
			oldValue = oldPoint.x;
			oldLeft = left;
		}
	
		if (nEnd > pCurve->GetCurDisplayPos())
		{
			pCurve->SetPat(DL.m_CurPat);
			pCurve->SetStretch(DL.m_CurStretch);
			pCurve->SetCurDisplayPos(nEnd);
		}
	
		pDC->SelectObject(pOldPen);
		delete pPen;
	
		return UL_NO_ERROR;
	}

	HRESULT DrawNoRoll(ICurve* pCurve, CDC* pDC, LPPOINT lpPoints, int nCount, 
		BOOL bRuntime, int nEnd /* = 0 */)
	{
		int nLeftMargin = pCurve->LeftMargin(FALSE);	// 得到左右格子的坐标 
		int nRightMargin = pCurve->RightMargin(FALSE);
		
		// divided by zero
		int nLRWidth = abs(nRightMargin - nLeftMargin);
		if (nLRWidth == 0)
			return UL_NO_ERROR;

		unsigned Type[8];
		int w = pCurve->LineWidth();
		COLORREF color = pCurve->Color();

		CPoint oldPoint = lpPoints[0];
		oldPoint.x -= nLeftMargin;
		oldPoint.y = lpPoints[0].y;

		int oldGrade = oldPoint.x / nLRWidth;
		if (oldPoint.x < 0)
			oldGrade--;
		
		oldPoint.x = oldPoint.x % nLRWidth;
		if (oldPoint.x < 0)
			oldPoint.x += nLRWidth;
		else if (oldPoint.x == 0) // Critical point
		{
			if (oldGrade < 0)
				oldGrade++;
		}

		oldPoint.x += nLeftMargin;

		int nMaxRoll = 0; // abs(pCurve->GetMaxRollCount());
		int nMinRoll = -nMaxRoll;

		if (nCount == 1)
		{
			if (abs(oldGrade) <= nMaxRoll)
				pDC->SetPixel(oldPoint, color);
			return UL_NO_ERROR;
		}

		CDashLine DL(*pDC, Type, CDashLine::GetPattern(Type, 0, w, CDashLine::DL_SOLID));
		if (pCurve->IsSelect())
		{
			w += 6;
			int r = 0, g = 0, b = 0;
			color &= 0x00ffffff;
			b = (color & 0x00ff0000); b = b / 3 + 96; 
			g = (color & 0x0000ff00); g = g / 3 + 96; 
			r = (color & 0x000000ff); r = r / 3 + 96;
			color = RGB(r, g, b);
			if (color == 0)
				color = RGB(96, 96, 96);
		}

		CPen* pPen = DL.GetRightPen(pCurve->LineStyle(), w, color);
		CPen* pOldPen = pDC->SelectObject(pPen);
		if (bRuntime)
			DL.MoveToEx(oldPoint, pCurve->GetPat(), pCurve->GetStretch());
		else
			DL.MoveTo(oldPoint);

		int newGrade;
		CPoint newPoint;
		double distance;
		for (int i = 1; i < nCount; i++)
		{
			newPoint = lpPoints[i];
			if(newPoint.y == oldPoint.y)
				continue;

			newPoint.x -= nLeftMargin;
			newGrade = newPoint.x / nLRWidth;
			if (newPoint.x < 0)
				newGrade--;
			
			newPoint.x = newPoint.x % nLRWidth;
			if (newPoint.x < 0)
				newPoint.x += nLRWidth;
			else if (newPoint.x == 0) // Critical point
			{
				if (newGrade < 0)
					newGrade++;
			}

			newPoint.x += nLeftMargin;

			if(lpPoints[i].x == lpPoints[i - 1].x)
				oldGrade = newGrade;

			// In same grade, draw directly
			if (newGrade == oldGrade)
			{
				if (abs(newGrade) <= nMaxRoll)
					DL.LineTo(newPoint);
				else
					DL.MoveTo(newPoint);
				oldPoint = newPoint;
				continue;
			}

			distance = abs(lpPoints[i].x - lpPoints[i - 1].x);
			ASSERT(distance != 0);

			double dYX = (double)(newPoint.y - oldPoint.y) / distance;
			double IncreamY = dYX * (double)nLRWidth;
			double nY;
			if (newGrade < oldGrade) // Exceed left region
			{
				nY = (double)oldPoint.y + dYX * (double)(oldPoint.x - nLeftMargin);
				if (oldGrade > nMaxRoll)
				{
					nY += IncreamY*(oldGrade-nMaxRoll-1);
					DL.MoveTo(nRightMargin, (int) nY);
					nY += IncreamY;
					oldGrade = nMaxRoll;
				}

				while (newGrade < oldGrade)
				{
					if (oldGrade < nMinRoll)
						break;

					DL.LineTo(nLeftMargin, (int) nY);
					DL.MoveTo(nRightMargin, (int) nY);
					oldGrade--;
					nY += IncreamY;
				}
			}
			else // Exceed right region
			{
				nY = (double)oldPoint.y + dYX * (double)(nRightMargin - oldPoint.x);
				if (oldGrade < nMinRoll)
				{
					nY += IncreamY*(nMinRoll-oldGrade-1);
					DL.MoveTo(nLeftMargin, (int) nY);
					nY += IncreamY;
					oldGrade = nMinRoll;
				}

				while (newGrade > oldGrade)
				{
					if (oldGrade > nMaxRoll)
						break;

					DL.LineTo(nRightMargin, (int) nY);
					DL.MoveTo(nLeftMargin, (int) nY);
					oldGrade++;
					nY += IncreamY;
				}
			}

			if (newGrade == oldGrade)
			{
				if (abs(newGrade) <= nMaxRoll)
					DL.LineTo(newPoint);
				else
					DL.MoveTo(newPoint);

				oldPoint = newPoint;
				continue;
			}
			
			DL.MoveTo(newPoint);
			oldGrade = newGrade;
			oldPoint = newPoint;
		}
	
		if (nEnd > pCurve->GetCurDisplayPos())
		{
			pCurve->SetPat(DL.m_CurPat);
			pCurve->SetStretch(DL.m_CurStretch);
			pCurve->SetCurDisplayPos(nEnd);
		}
	
		pDC->SelectObject(pOldPen);
		delete pPen;
	
		return UL_NO_ERROR;
	}

	HRESULT DrawRoll(ICurve* pCurve, int nLeft, int nRight, CDC* pDC, LPPOINT lpPoint, int nCount)
	{
		if (nCount < 1)
			return UL_NO_ERROR;
		
		if ((nRight - nLeft) == 0)
			return UL_NO_ERROR;
		
		if (nCount == 1)
		{
			COLORREF clr = RGB(0, 0, 0);
			CPen newPen(PS_SOLID, 1, clr);
			CPen* pPen = pDC->SelectObject(&newPen);
			LOGPEN lp;
			if (pPen->GetLogPen(&lp))
				clr = lp.lopnColor;
			pDC->SetPixel(lpPoint[0], clr);
			pDC->SelectObject(pPen);
			return UL_NO_ERROR;
		}
		
		int nRollCount;
		long Distance, Width;	
		long OutSegment;
		double oldValue;	// oldValue用来确定出界段长
		POINT oldPoint;
		long oldGradeRight, gradeRight, oldGradeLeft, gradeLeft, oldLeft, left;
		double value;	
		
		// 取出第一各点并判断是否有越界
		// 第一点的处理
		oldLeft = lpPoint[nCount - 1].x - nLeft;
		oldPoint.y = lpPoint[nCount - 1].y;
		oldGradeRight = (LONG) floor(oldLeft / nRight - nLeft);		
		
		if (oldLeft < 0)		// 判断曲线是否越界：left<0则曲线越界
		{
			// 如果越界，求出倍数
			oldGradeLeft = (LONG) floor(abs(oldLeft) / (nRight - nLeft)) + 1;
		}
		
		if ((oldGradeRight >= 1) || (oldLeft < 0))  // 曲线越右界，曲线越左界
		{
			if (oldGradeRight >= 1)
				oldPoint.x = lpPoint[nCount - 1].x -
				oldGradeRight * (nRight - nLeft) ;
			else
				oldPoint.x = lpPoint[nCount - 1].x +
				oldGradeLeft * (nRight - nLeft);
		}
		else	 // 曲线正常
		{
			oldPoint.x = lpPoint[nCount - 1].x;
		}
		
		pDC->MoveTo(oldPoint);
		oldValue = oldPoint.x;	// tyh1011
		
		// 其他点的处理
		for (int pointSuffix = nCount - 2; pointSuffix >= 0; pointSuffix--)
		{
			gradeRight = (LONG) floor((lpPoint[pointSuffix].x - nLeft) /
				(nRight - nLeft));
			left = lpPoint[pointSuffix].x - nLeft;
			if (left < 0)
			{
				gradeLeft = (LONG) floor(abs(left) / (nRight - nLeft)) + 1;
			}
			
			if ((gradeRight >= 1) || (left < 0))  // 曲线越右界,曲线越左界
			{
				if (gradeRight >= 1)
					value = lpPoint[pointSuffix].x -
					gradeRight * (nRight - nLeft) ;
				else
					value = lpPoint[pointSuffix].x +
					gradeLeft * (nRight - nLeft)  ;
			}
			else	 // 曲线正常
			{
				value = lpPoint[pointSuffix].x;
			}
			Distance = abs(lpPoint[pointSuffix].x - lpPoint[pointSuffix + 1].x);// tyh 1013
			Width = nRight - nLeft;// tyh 1013
			// 曲线由正常(0)变为越右界(1)，则先画与右边界的交点，再画与左边界的交点
			// 曲线由越左界(0)变为正常(1)，则先画右边界的交点，再画与左边界的交点
			if ((gradeRight > oldGradeRight) || (oldLeft < 0 && left >= 0))
			{
				// oldPoint.x = nRight;
				// DashLine.LineTo(nRight, oldPoint.y);
				
				// 画到右边界,后从左边界开始画
				// oldPoint.y = lpPoint[pointSuffix].y;
				// DashLine.MoveTo(nLeft,oldPoint.y);
				
				//////////////////// tyh test  1019 /////////////////////右越界
				int nCount = 0;
				double tempY;
				double VarifyY = 0, IncreamY = 0, tempX = 0;
				
				OutSegment = long(oldValue + Distance - nRight); // 出界段的大小			
				
				// 由三角形等比关系求出越界时当前点距边界点Y的可变增量以及每次越界时距产生Y方向的不变增量,
				// (由于线每次越界折回后与原先的线平行,故增量不变.)
				tempX = nRight - oldValue;// tempX为前一点距越界边界的距离
				if (Distance == 0)
					VarifyY = 0;// 当前点到边界的落差(可变增量)
				else
					VarifyY = (tempX / Distance) * (lpPoint[pointSuffix].y -
					lpPoint[pointSuffix + 1].y);// 可变增量
				
				if (tempX == 0)
					IncreamY = 0;
				else
					IncreamY = VarifyY / tempX * Width;// // 两个边界之间的落差(不变增量):与可变增量有关.(三角关系可推)width/temp的比值与m_fPrintReviseX无关
				tempY = oldPoint.y + VarifyY;
				// 越界画到越界边界,移到相反的边界.
				nRollCount = 0;
				do
				{
					pDC->LineTo(nRight, (int) tempY);
					pDC->MoveTo(nLeft, (int) tempY);
					tempY += IncreamY;
					if (nCount > 0)
						OutSegment -= Width;
					nCount++;
					nRollCount++;
				}
				while (OutSegment > Width && nRollCount < pCurve->GetMaxRollCount());
				//////////////////////////////// test end//////////////////// 
				
			}
			// 如果曲线由越右界(1)变为正常(0)，先画与左边界的交点，再画与右边界的交点
			// 如果曲线由正常(1)到越左边界(0)，先画与左边界的焦点，再画与右边界的交点
			else if ((gradeRight < oldGradeRight) || (oldLeft >= 0 && left < 0))//
			{
				// oldPoint.x = nLeft;
				// DashLine.LineTo(nLeft, oldPoint.y);
				
				// 画到左边界后从右边界开始画
				// DashLine.MoveTo(oldPoint.x+nRight-nLeft,oldPoint.y);
				// 与越右界的画线算法不一至，所以修改为下面的语句
				// oldPoint.y = lpPoint[pointSuffix].y;
				// DashLine.MoveTo(nRight,oldPoint.y);
				
				//////////////////////// tyh 1019//////////////////// 
				int nCount = 0;			
				double VarifyY = 0, IncreamY = 0, tempX = 0;
				double tempY;// 移动后的深度
				
				OutSegment = abs(int(nLeft - (oldValue - Distance))); // 出界段的大小			
				
				// 由三角形等比关系求出越界时当前点距边界点Y的可变增量以及每次越界时距产生Y方向的不变增量,
				// (由于线每次越界折回后与原先的线平行,故增量不变.)
				tempX = abs(int(nLeft - oldValue));	// tempX为前一点距越界边界的距离
				if (Distance == 0)
					VarifyY = 0;	// 当前点到边界的落差(可变增量)
				else
					VarifyY = (tempX / Distance) * (lpPoint[pointSuffix].y -
					lpPoint[pointSuffix + 1].y);// 可变增量
				
				if (tempX == 0)
					IncreamY = 0;
				else
					IncreamY = VarifyY / tempX * Width; // 两个边界之间的落差(不变增量):与可变增量有关.(三角关系可推)width/temp的比值与m_fPrintReviseX无关
				
				tempY = oldPoint.y + VarifyY;
				// 越界画到越界边界,移到相反的边界.
				nRollCount = 0;
				do
				{
					pDC->LineTo(nLeft, (int) tempY);
					pDC->MoveTo(nRight, (int) tempY);
					tempY += IncreamY;
					if (nCount > 0)
						OutSegment -= Width;
					nCount++;
					nRollCount++;
				}
				while (OutSegment > Width && nRollCount < pCurve->GetMaxRollCount());
			}
			oldGradeRight = gradeRight;    // 如果他们相等则表明他们同为越界或者同为正常			 
			oldPoint.y = lpPoint[pointSuffix].y;
			oldPoint.x = (long) value;
			
			pDC->LineTo(oldPoint);
			oldValue = oldPoint.x;
			oldLeft = left;
	}
	
	return UL_NO_ERROR;
}

	HRESULT PrintRoll(ICurve* pCurve, CULPrintInfo* pInfo, LPPOINT lpPoints, int nCount, int nLeftMargin, int nRightMargin)
	{
		// divided by zero
		if (nRightMargin == nLeftMargin)
			return UL_NO_ERROR;
		
		if (nCount <= 0)
			return UL_NO_ERROR;
		
		//Start
		//在随钻测井时当曲线往回运动时，曲线也需要正常绘制，使用数据库后改功能取消
		//将lpPoints中所有数据点排序
		CPoint ptTemp;
		if(nCount >= 3)
		{
			long nSD = lpPoints[0].y; 
			long nED = lpPoints[nCount - 1].y;

	//		if(nSD > nED)
			{
				for(int i = 0 ; i < nCount - 1 ; i++)
				{
					for(int j = 0 ; j< nCount - 1 ; j++)
					{
						if(lpPoints[j].y < lpPoints[j + 1].y)
						{
							ptTemp = lpPoints[j];
							lpPoints[j] = lpPoints[j + 1];
							lpPoints[j + 1] = ptTemp;
						}
					}
				}
			}
	/*		else
			{
				for(int i = 0 ; i < nCount - 1 ; i++)
				{
					for(int j = i ; j< nCount - 1 ; j++)
					{
						if(lpPoints[j].y > lpPoints[j + 1].y)
						{
							ptTemp = lpPoints[j];
							lpPoints[j] = lpPoints[j + 1];
							lpPoints[j + 1] = ptTemp;
						}
					}
				}
			}*/
		}

		//End

		if (!pCurve->IsRewind())
		{
			PrintNoRoll(pCurve, pInfo, lpPoints, nCount, nLeftMargin, nRightMargin);
			return UL_NO_ERROR;
		}
		
		// 控制曲线回绕次数
		int nRollCount;
		
		//COLORREF crCurve = Gbl_ConfigInfo.m_Plot.Colored ? pCurve->m_crColor : 0;
		COLORREF crCurve = pCurve->Color();
		CDC* pDC = pInfo->GetPrintDC();
		if (nCount == 1)
		{
			pDC->SetPixel(lpPoints[0], crCurve);
			return UL_NO_ERROR;
		}

		unsigned Type[8];
		int w = pCurve->LinePWidth();

		CDashLine DL(*pDC, Type, CDashLine::GetPattern(Type, 0, w, CDashLine::DL_SOLID));
		
		CPen* pPen = DL.GetRightPen(pCurve->LineStyle(), w, crCurve);
		CPen* pOldPen = pDC->SelectObject(pPen);
		
			int nDistance, nWidth;	
			int OutSegment, oldValue;	// oldValue用来确定出界段长						
			int tempLeftMargin = nLeftMargin;
			int tempRightMargin = nRightMargin;
			
			CPoint oldPoint;			// 记录上一点位置
			CPoint tempPoint;
			long oldGradeRight, gradeRight;	
			long oldGradeLeft, gradeLeft;	
			long oldLeft, left;
			int nConvertX;				// 换算后的x值 
			
			// 第一个点设置为最后一个
			oldPoint = lpPoints[nCount - 1];
			
			oldGradeRight = (LONG)
				floor((lpPoints[nCount - 1].x - nLeftMargin) /
				(nRightMargin - nLeftMargin));
			oldLeft = lpPoints[nCount - 1].x - nLeftMargin;
			
			if (oldLeft < 0)	// 判断曲线是否越界：left<0则曲线越界
				// 如果越界，求出倍数
				oldGradeLeft = (LONG) floor(abs(oldLeft) /
				(nRightMargin - nLeftMargin)) + 1;
			
			if ((oldGradeRight >= 1) || (oldLeft < 0))  //曲线越右界，曲线越左界
			{
				// 换算为x值
				if (oldGradeRight >= 1)	// 越右界
					oldPoint.x = lpPoints[nCount - 1].x -
					oldGradeRight * (nRightMargin - nLeftMargin);
				else					// 越左界
					oldPoint.x = lpPoints[nCount - 1].x +
					oldGradeLeft * (nRightMargin - nLeftMargin);
			}
			else	 // 曲线正常
				oldPoint.x = lpPoints[nCount - 1].x;
			
			
			// 根据方向得到X值
			oldPoint.x = pInfo->CoordinateConvertX(oldPoint.x);
			
			oldValue = oldPoint.x;
			DL.MoveTo(oldPoint);		
			
			for (int i = nCount - 1; i >= 0; i--)
			{
				gradeRight = (LONG) floor((lpPoints[i].x - nLeftMargin) /
					(nRightMargin - nLeftMargin));
				
				left = lpPoints[i].x - nLeftMargin;
				
				if (left < 0)
					gradeLeft = (LONG) floor(abs(left) /
					(nRightMargin - nLeftMargin)) + 1;
				
				if ((gradeRight >= 1) || (left < 0))  // 曲线越右界,曲线越左界
				{
					if (gradeRight >= 1)
						nConvertX = lpPoints[i].x -
						gradeRight * (nRightMargin - nLeftMargin);
					else
						nConvertX = lpPoints[i].x +
						gradeLeft * (nRightMargin - nLeftMargin);
				}
				else	 //曲线正常
					nConvertX = lpPoints[i].x;
				
				nDistance = abs(lpPoints[i].x - lpPoints[i + 1].x); 
				nWidth = abs(nRightMargin - nLeftMargin); 
				
				// 曲线由正常(0)变为越右界(1)，则先画与右边界的交点，再画与左边界的交点
				// 曲线由越左界(0)变为正常(1)，则先画右边界的交点，再画与左边界的交点
				if ((gradeRight > oldGradeRight) || (oldLeft < 0 && left >= 0))  //
				{
					int nCount = 0;				
					float VarifyY = 0.0f;
					float IncreamY = 0.0f;
					int tempX = 0;
					int tempY = 0;
					OutSegment = abs(oldValue + nDistance - nRightMargin);//出界段的大小				
					
					// 由三角形等比关系求出越界时当前点距边界点Y的可变增量以及每次越界时距产生Y方向的不变增量,
					// (由于线每次越界折回后与原先的线平行,故增量不变.)
					tempX = nRightMargin - oldValue;// tempX为前一点距越界边界的距离
					
					if (nDistance == 0)
						VarifyY = 0.0f;
					else
						VarifyY = (tempX * 1.0f / nDistance) * (lpPoints[i].y -
						lpPoints[i +1].y);//可变增量
					
					if (tempX == 0)
						IncreamY = 0;
					else
						IncreamY = VarifyY / tempX * nWidth; // 两个边界之间的落差(不变增量):与可变增量有关.(三角关系可推)width/temp的比值
					
					// 左右边界
					tempLeftMargin = pInfo->CoordinateConvertX(nLeftMargin);
					tempRightMargin = pInfo->CoordinateConvertX(nRightMargin);
					
					tempY = oldPoint.y + VarifyY;				
					
					// 越界画到越界边界,移到相反的边界.
					nRollCount = 0;
					do
					{
						CPoint ptStart(tempRightMargin, tempY);
						CPoint ptEnd(tempLeftMargin, tempY);
						pInfo->CoordinateCalibrate(ptStart);
						pInfo->CoordinateCalibrate(ptEnd);
						DL.LineTo(ptStart);
						DL.MoveTo(ptEnd);
						tempY += IncreamY;
						if (nCount > 0)
							OutSegment -= nWidth;
						nCount++;
						nRollCount++;
					}
					while (OutSegment > nWidth && nRollCount < pCurve->GetMaxRollCount());
				}
				// 如果曲线由越右界(1)变为正常(0)，先画与左边界的交点，再画与右边界的交点
				// 如果曲线由正常(1)到越左边界(0)，先画与左边界的交点，再画与右边界的交点
				else if ((gradeRight < oldGradeRight) ||
					(oldLeft >= 0 && left < 0))//
				{
					int nCount = 0;		
					
					float VarifyY = 0.0f;
					float IncreamY = 0.0f;
					int tempX = 0;
					int tempY = 0;	// 移动后的深度
					
					OutSegment = abs(nLeftMargin - (oldValue - nDistance));//出界段的大小			
					
					// 由三角形等比关系求出越界时当前点距边界点Y的可变增量以及每次越界时距产生Y方向的不变增量,
					// (由于线每次越界折回后与原先的线平行,故增量不变.)
					tempX = abs(nLeftMargin - oldValue);//tempX为前一点距越界边界的距离
					if (nDistance == 0)
						VarifyY = 0;
					else
						VarifyY = (tempX * 1.0f / nDistance) * (lpPoints[i].y -
						lpPoints[i +1].y);// 可变增量
					
					if (tempX == 0)
						IncreamY = 0;
					else
						IncreamY = VarifyY / tempX * nWidth; // 两个边界之间的落差(不变增量):与可变增量有关.(三角关系可推)width/temp的比值与m_fPrintReviseX无关
					
					//左右边界
					tempLeftMargin = pInfo->CoordinateConvertX(nLeftMargin);
					tempRightMargin = pInfo->CoordinateConvertX(nRightMargin);
					
					tempY = oldPoint.y + VarifyY;
					
					//越界画到越界边界,移到相反的边界.
					nRollCount = 0;
					do
					{
						CPoint ptStart(tempLeftMargin, tempY);
						CPoint ptEnd(tempRightMargin, tempY);
						pInfo->CoordinateCalibrate(ptStart);
						pInfo->CoordinateCalibrate(ptEnd);
						DL.LineTo(ptStart);
						DL.MoveTo(ptEnd);
						tempY += IncreamY;
						if (nCount > 0)
							OutSegment -= nWidth;
						nCount++;
						nRollCount++;
					}
					while (OutSegment > nWidth && nRollCount < pCurve->GetMaxRollCount());
				}
				
				oldGradeRight = gradeRight;
				oldLeft = left;
				oldPoint.x = nConvertX;
				oldValue = oldPoint.x;
				oldPoint.y = lpPoints[i].y;
				oldPoint.x = pInfo->CoordinateConvertX(oldPoint.x); // 座标转换
				
				tempPoint = oldPoint;
				pInfo->CoordinateCalibrate(tempPoint);	// 座标刻度
				
				// shenkun add, 2003.10.20 - 改正打印机刻度（Y方向缩放）后断线的错误
				if (i == nCount - 1)
				{
					DL.MoveTo(tempPoint);
				}
				else
				{
					DL.LineTo(tempPoint);
				}
				// DL.LineTo(oldPoint);
				
			}
	
		pDC->SelectObject(pOldPen);
		if (pPen)
			delete pPen;
		
		return UL_NO_ERROR;
	}

	HRESULT PrintNoRoll(ICurve* pCurve, CULPrintInfo* pInfo, LPPOINT lpPoints, int nCount, int nLeftMargin, int nRightMargin)
	{
		// divided by zero
		int nLRWidth = abs(nRightMargin - nLeftMargin);
		if (nLRWidth == 0)
			return UL_NO_ERROR;
		
		CDC* pDC = pInfo->GetPrintDC();

		unsigned Type[8];
		int w = pCurve->LinePWidth();
		COLORREF color = pCurve->Color();

		CPoint oldPoint = lpPoints[0];
		oldPoint.x -= nLeftMargin;
		oldPoint.y = lpPoints[0].y;

		int oldGrade = oldPoint.x / nLRWidth;
		if (oldPoint.x < 0)
			oldGrade--;
		
		oldPoint.x = oldPoint.x % nLRWidth;
		if (oldPoint.x < 0)
			oldPoint.x += nLRWidth;
		else if (oldPoint.x == 0) // Critical point
		{
			if (oldGrade < 0)
				oldGrade++;
		}

		oldPoint.x += nLeftMargin;

		int nRoll = 0; 
	
		if (nCount == 1)
		{
			if (abs(oldGrade) <= nRoll)
				pDC->SetPixel(oldPoint, color);
			return UL_NO_ERROR;
		}

		CDashLine DL(*pDC, Type, CDashLine::GetPattern(Type, 0, w, CDashLine::DL_SOLID));

		CPen* pPen = DL.GetRightPen(pCurve->LineStyle(), w, color);
		CPen* pOldPen = pDC->SelectObject(pPen);

		CPoint ptPrint;
		CPoint ptLeft, ptRight;

		ptPrint = oldPoint;
		ptPrint.x = pInfo->CoordinateConvertX(ptPrint.x);
		pInfo->CoordinateCalibrate(ptPrint);
		DL.MoveTo(ptPrint);

		int nLeftPrint = pInfo->CoordinateConvertX(nLeftMargin);
		int nRightPrint = pInfo->CoordinateConvertX(nRightMargin);

		int newGrade;
		CPoint newPoint;
		double distance;
		for (int i = 1; i < nCount; i++)
		{
			newPoint = lpPoints[i];
			newPoint.x -= nLeftMargin;
			newGrade = newPoint.x / nLRWidth;
			if (newPoint.x < 0)
				newGrade--;
			
			newPoint.x = newPoint.x % nLRWidth;
			if (newPoint.x < 0)
				newPoint.x += nLRWidth;
			else if (newPoint.x == 0) // Critical point
			{
				if (newGrade < 0)
					newGrade++;
			}

			newPoint.x += nLeftMargin;

			ptPrint = newPoint;
			ptPrint.x = pInfo->CoordinateConvertX(ptPrint.x);
			pInfo->CoordinateCalibrate(ptPrint);

			// In same grade, draw directly
			if (newGrade == oldGrade)
			{
				if (abs(newGrade) <= nRoll)
					DL.LineTo(ptPrint);
				else
					DL.MoveTo(ptPrint);
				oldPoint = newPoint;
				continue;
			}

			distance = abs(lpPoints[i].x - lpPoints[i - 1].x);
			ASSERT(distance != 0);

			double dYX = (double)(newPoint.y - oldPoint.y) / distance;
			double IncreamY = dYX * (double)nLRWidth;
			double nY;
			if (newGrade < oldGrade) // Exceed left region
			{
				nY = (double)oldPoint.y + dYX * (double)(oldPoint.x - nLeftMargin);
				if (oldGrade > nRoll && newGrade == 0) //进入右边界
				{
					ptRight = CPoint(nRightPrint, (int) nY);
					pInfo->CoordinateCalibrate(ptRight);
					DL.MoveTo(ptRight);
					oldGrade = nRoll;
				}
				else if(oldGrade == 0)				//出左边界
				{
					ptLeft = CPoint(nLeftPrint, (int) nY);
					pInfo->CoordinateCalibrate(ptLeft);
					DL.LineTo(ptLeft);
				}
			}
			else // Exceed right region
			{
				nY = (double)oldPoint.y + dYX * (double)(nRightMargin - oldPoint.x);
				if (oldGrade < nRoll && newGrade == 0)//进入左边界
				{
					ptLeft = CPoint(nLeftPrint, (int) nY);
					pInfo->CoordinateCalibrate(ptLeft);
					DL.MoveTo(ptLeft);
					oldGrade = nRoll;
				}
				else if(oldGrade == 0)				//出右边界
				{
					ptRight = CPoint(nRightPrint, (int) nY);
					pInfo->CoordinateCalibrate(ptRight);
					DL.LineTo(ptRight);
				}
				
			}

			if (newGrade == oldGrade)
			{
				if (abs(newGrade) <= nRoll)
					DL.LineTo(ptPrint);
				else
					DL.MoveTo(ptPrint);

				oldPoint = newPoint;
				continue;
			}
			
			DL.MoveTo(ptPrint);
			oldGrade = newGrade;
			oldPoint = newPoint;
		}
	
		pDC->SelectObject(pOldPen);
		delete pPen;
	
		return UL_NO_ERROR;
	}

public:
	ITrack*		m_pTrack;		// 绘图所在井道的指针，用于获取井道RECT等属性
	GridProp*	m_pGridProp;	// 格线属性
};

#endif
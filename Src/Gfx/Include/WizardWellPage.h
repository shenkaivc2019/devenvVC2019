#if !defined(AFX_WIZARDWELLPAGE_H__E4FF8103_3F8D_4098_BA9C_722BFC391DE2__INCLUDED_)
#define AFX_WIZARDWELLPAGE_H__E4FF8103_3F8D_4098_BA9C_722BFC391DE2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "WPropertyPage.h"

// WizardWellPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CWizardWellPage dialog

class CWizardWellPage : public CWPropertyPage
{
	DECLARE_DYNCREATE(CWizardWellPage)

// Construction
public:
	CWizardWellPage();
	~CWizardWellPage();

// Dialog Data
	//{{AFX_DATA(CWizardWellPage)
	enum { IDD = IDD_WIZARD_STEP2_WELL };
	//}}AFX_DATA

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CWizardWellPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CWizardWellPage)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WIZARDWELLPAGE_H__E4FF8103_3F8D_4098_BA9C_722BFC391DE2__INCLUDED_)

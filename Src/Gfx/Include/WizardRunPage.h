#if !defined(AFX_WIZARDRUNPAGE_H__4770DCCC_42BC_4CA4_BBE0_45CA6818D329__INCLUDED_)
#define AFX_WIZARDRUNPAGE_H__4770DCCC_42BC_4CA4_BBE0_45CA6818D329__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WizardRunPage.h : header file
//
#include "GridCtrl.h"
#include "RunDataInformation.h"
#include "WPropertyPage.h"
/////////////////////////////////////////////////////////////////////////////
// CWizardRunPage dialog

class CWizardRunPage : public CWPropertyPage
{
	DECLARE_DYNCREATE(CWizardRunPage)

// Construction
public:
	CWizardRunPage();
	~CWizardRunPage();

// Dialog Data
	//{{AFX_DATA(CWizardRunPage)
	enum { IDD = IDD_WIZARD_STEP2_RUN };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA
	CGridCtrl	m_GridCtrl;
	CRunDataInformation* m_pInfo;	//
	
	int WriteInfo();
	void ReadInfo();

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CWizardRunPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	

	
	void Clear();
	void UpdateGrid(BOOL bFlag = TRUE);
// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CWizardRunPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnADDRun();
	afx_msg void OnDelRun();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	
	BOOL GetGridItem(int nRow, int nCol);
	BOOL SetGridItem(int iRow, int iCol, CRunDataItem * pItem);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WIZARDRUNPAGE_H__4770DCCC_42BC_4CA4_BBE0_45CA6818D329__INCLUDED_)

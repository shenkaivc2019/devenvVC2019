// Units.h : Declaration of the CUnits

#ifndef __UNITS_H_
#define __UNITS_H_

#define WM_MULUNIT (WM_USER+700)

//#include "resource.h"       // main symbols
#include <afxtempl.h>
#include <math.h>
// 2020.11.05 sys 单位管理 Start
#include "TUnitTB.h"
// 2020.11.05 sys 单位管理 End

using namespace std;

class CUnit;
class CXMLSettings;
class CXMLNode;
class CUnA;

class CGridCtrl;

#ifndef UNITS_API
	#ifdef UNITS_EXPORT
		#define UNITS_API __declspec( dllexport )
	#else	
		#define UNITS_API __declspec( dllimport )
	#endif
#endif

#define DTMF		0.0001			// 1深度单位 = 0.0001米
#define DTM(d)		((d)*DTMF)		// 深度单位换算为米

typedef enum TUnitType
{
	UNIT_METRIC	= 1,
	UNIT_BRITISH	= 2,
	UNIT_INTERNATION	= 4,
	UNIT_USER	= 8
} UnitType;

class CConnectDataBaseImpl;
/////////////////////////////////////////////////////////////////////////////
// CUnits
class UNITS_API CUnits
{
public:
	CString m_strPath;
	CString m_strFormular;
	CXMLSettings*			m_pXmlCu;
	CList<CUnit*, CUnit*>	m_Units;
	UnitType	m_nType;
	CString		m_strCat;	// Unit Category
	CString		m_strCfg;	// Unit Path Name
	CString		m_strCu;
	CString		m_strTypes;
	CString		m_strMenu;
	CStringList m_CatList;
	
	enum LCDKb { speed, temp, tens, volt, lcdvc};
	double		m_fLCDK[lcdvc];
	double		m_fLCDB[lcdvc];

	CString m_strFormatLM;
public:
	CUnits();
	~CUnits();
 
	BOOL LoadCurveUnits(LPCTSTR pszFile = NULL);
	BOOL SaveCurveUnits(LPCTSTR pszFile = NULL);

	CXMLNode* GetCurveUnit(LPCTSTR pszCurve, LPCTSTR pszPath);
	BOOL SaveFormulars();
	CString CatCase(LPCTSTR lpszCat)
	{
		CString strCat = lpszCat;
		int nLen = strCat.GetLength();
		if (nLen < 1)
			return strCat;
		
		if (nLen > 31)
		{
			strCat = strCat.Left(31);
		}
		
		strCat.MakeLower();
		strCat.SetAt(0, *_tcsupr(strCat.Left(1).GetBuffer(1)));
		return strCat;
	}

	int GetUnaCount();
	int GetUnasCount();
	int GetUniCount();


	CUnit* GetUnit(LPCTSTR lpszType, BOOL bNew = FALSE);
	CUnit* FindUnit(LPCTSTR lpszName);
	BOOL SaveConfig(LPCTSTR lpszConfig = NULL, BOOL bDepth = FALSE);
	BOOL LoadConfig(LPCTSTR lpszConfig = NULL, BOOL bDepth = FALSE);
    void CalcLCD(double* pK, double* pB);
	void QueryCatList();
	void InitWndText(CWnd* pWnd);
	void AddUnit(LPCTSTR lpszFile);

	void SetDBConnect(CConnectDataBaseImpl* pConnectDataBaseImpl);
// IUnits
public:
	LPCTSTR GetUCUnit(LPCTSTR lpszName , BOOL bRaw);
	double GetUCValue(LPCTSTR lpszName , double d , BOOL bSet);
	//int FindUnA(BSTR pszType, BSTR pszUnit, /*[out, retval]*/IUnA** ppUnA);
	CUnA* FindUnA(LPCTSTR lpszType, LPCTSTR lpszUnit);
	int EXValue(double dValue, LPCTSTR pszType, LPCTSTR pszCurUnit, LPCTSTR pszRequireUnit, double *pVal);
// 2023.07.07 Start
	//LPCTSTR CalcCurveKb(LPCTSTR lpszPath, LPCTSTR lpszTu, /*[out]*/ BOOL* bRecUnit,  /*[out]*/ double* pK, /*[out]*/ double* pB, /*[out]*/ double* pAK, /*[out]*/ double* pAB);
	LPCTSTR CalcCurveKb(LPCTSTR pszType, LPCTSTR pszCurUnit, LPCTSTR pszDBUnit, BOOL* bRecUnit, double* pK, double* pB, double* pAK, double* pAB);
// 2023.07.07 End
	long get_Language();
	int put_Language(/*[in]*/ long newVal);
	LPCTSTR XValue2(/*[out]*/ double* pVal, LPCTSTR lpszType, LPCTSTR lpszName);
	double SDepth(double d);
	int DDXIVN_Text2(VARIANT *pVDX, int nIDC, int nIDSTC, double *pVal, LPCTSTR lpszType, double minVal, double maxVal, LPCTSTR pszText, LPCTSTR lpszFormat);
	int DDXVN_Text2(VARIANT *pVDX, int nIDC, int nIDSTC, /*[out]*/ double* pVal, LPCTSTR lpszType, double minVal, double maxVal, LPCTSTR lpszName, LPCTSTR lpszFormat);
	int DDXV_Text2(VARIANT *pVDX, int nIDC, int nIDSTC, /*[out]*/ double* pVal, LPCTSTR lpszType, double minVal, double maxVal, LPCTSTR lpszName, int nIDPrompt, LPCTSTR lpszFormat);
	int DDXV_Text(VARIANT *pVDX, int nIDC, /*[out]*/ double* pVal, LPCTSTR lpszType, double minVal, double maxVal, LPCTSTR lpszName, LPCTSTR lpszFormat);
	int DDX_Text2(VARIANT *pVDX, int nIDC, int nIDSTC, /*[out]*/ double* pVal, LPCTSTR lpszType, LPCTSTR lpszName, LPCTSTR lpszFormat);
	LPCTSTR GetCat();
	int SetCat(UnitType lType, LPCTSTR lpszCat);
	double LMValueParse(LPCTSTR lpszText);
	double SValueParse(LPCTSTR lpszText, LPCTSTR lpszType, LPCTSTR lpszName);
	int UpdateMenu(VARIANT* pCmdUI, long lMax);
	int AddCurveUnit(LPCTSTR lpszName, LPCTSTR lpszUnit, BOOL bWrite);
	double SValue(double d, LPCTSTR lpszType, LPCTSTR lpszName);
	LPCTSTR XUnit(LPCTSTR lpszType, LPCTSTR lpszName);
	double TVolt(double d);
	double TTens(double d);
	double TTemp(double d);
	double TSpeed(double d);
	int RebuildCus(LPCTSTR lpszNewVal);
	double XDepth(double d, LPCTSTR lpszViewID = NULL, LPCTSTR lpszItemID = NULL);
	double XValue(double d, LPCTSTR lpszType, LPCTSTR lpszName);
	double GetXValue(double dSrc, LPCTSTR lpszViewID, LPCTSTR lpszItemID);
	//2020.9.19 sys ???? Start
	double GetXValues(double dSrc, LPCTSTR lpszViewID, LPCTSTR lpszItemID);
	//2020.9.19 sys ???? End
	int ShowLCDUnit();
	//int GetUnA(BSTR pszType, BSTR pszName, /*[out, retval]*/IUnA** ppUnA);
	CUnA* GetUnA(LPCTSTR lpszType, LPCTSTR lpszName);
	int DDX_Text(VARIANT *pVDX, int nIDC, /*[out]*/ double* pVal, LPCTSTR lpszType, LPCTSTR lpszName, LPCTSTR lpszFormat);
	LPCTSTR DUnit(LPCTSTR lpszViewID = NULL, LPCTSTR lpszItemID = NULL);
	LPCTSTR Format(LPCTSTR lpszFormat, double value, LPCTSTR lpszType, LPCTSTR lpszName);
	LPTSTR FormatLM(LPCTSTR lpszFormat, long lDepth);
	int ShowFormula(long lStyle);
	int ShowManager(long lStyle);
	int SaveCat(LPCTSTR lpszCat, BOOL bSet);
	int LoadCat(LPCTSTR lpszCat);
	long get_Type();
	int SetType(UnitType lType, long lIndex);
	LPCTSTR get_Formula();
	int put_Formula(/*[in]*/ LPCTSTR lpszNewVal);
	LPCTSTR get_Path();
	int put_Path(/*[in]*/ LPCTSTR lpszNewVal);
	int InitUnits();

	int ViewIDCount(CString strView);
	
// 2020.12.22 ltg Start
//	// Format Extend
//	CString FormatLM(UINT nFormatID, long dvalue)
//	{
//		CString strFormat;
//		VERIFY(strFormat.LoadString(nFormatID) != 0);
//		return Format(strFormat, dvalue, _T("Distance"), _T("Depth"));
//	}
//	// Text Extend
//	void DDX_Text(CDataExchange* pDX, int nIDC, double& value, LPCTSTR pszType, LPCTSTR pszName = NULL, LPCTSTR pszFormat = _T("%.4lf "))
//	{
//		DDX_Text((VARIANT*)pDX, nIDC, &value, pszType, pszName, pszFormat);
//	}
//	
//	void DDXV_Text(CDataExchange* pDX, int nIDC, double& value, LPCTSTR pszType, 
//		double minVal, double maxVal, LPCTSTR pszName = NULL, LPCTSTR pszFormat = _T("%.4lf "))
//	{
//		DDXV_Text((VARIANT*)pDX, nIDC, &value, pszType, minVal, maxVal, pszName, pszFormat);
//	}
//	
//	// Text Extend (Edit + Static)
//	void DDX_Text(CDataExchange* pDX, int nIDC, int nIDSTC, double& value, 
//		LPCTSTR pszType, LPCTSTR pszName = NULL, LPCTSTR pszFormat = _T("%.2lf"))
//	{
//		DDX_Text2((VARIANT*)pDX, nIDC, nIDSTC, &value, pszType, pszName, pszFormat);
//	}
//	
//	void DDXV_Text(CDataExchange* pDX, int nIDC, int nIDSTC, double& value, 
//		LPCTSTR pszType, double minVal, double maxVal, LPCTSTR pszName = NULL, 
//		UINT nIDPrompt = AFX_IDP_PARSE_REAL_RANGE, LPCTSTR pszFormat = _T("%.2lf"))
//	{
//		DDXV_Text2((VARIANT*)pDX, nIDC, nIDSTC, &value, pszType, minVal, maxVal, pszName, nIDPrompt, pszFormat);
//	}
//
//	void DDXVN_Text(CDataExchange* pDX, int nIDC, int nIDSTC, double& value, 
//		LPCTSTR pszType, double minVal, double maxVal, LPCTSTR pszName, LPCTSTR pszFormat = _T("%.4lf"))
//	{
//		DDXVN_Text2((VARIANT*)pDX, nIDC, nIDSTC, &value, pszType, minVal, maxVal, pszName, pszFormat);
//	}
	// Format Extend
	LPTSTR FormatLM(UINT nFormatID, long dvalue);
	// Text Extend
	void DDX_Text(CDataExchange* pDX, int nIDC, double& value, LPCTSTR pszType, LPCTSTR pszName = NULL, LPCTSTR pszFormat = _T("%.4lf "), LPCTSTR lpszViewID = NULL, LPCTSTR lpszItemID = NULL);
	void DDXV_Text(CDataExchange* pDX, int nIDC, double& value, LPCTSTR pszType, double minVal, double maxVal, LPCTSTR pszName = NULL, LPCTSTR pszFormat = _T("%.4lf "), LPCTSTR lpszViewID = NULL, LPCTSTR lpszItemID = NULL);
	// Text Extend (Edit + Static)
	void DDX_Text(CDataExchange* pDX, int nIDC, int nIDSTC, double& value, LPCTSTR pszType, LPCTSTR pszName = NULL, LPCTSTR pszFormat = _T("%.2lf"), LPCTSTR lpszViewID = NULL, LPCTSTR lpszItemID = NULL);
	void DDXV_Text(CDataExchange* pDX, int nIDC, int nIDSTC, double& value, LPCTSTR pszType, double minVal, double maxVal, LPCTSTR pszName = NULL, UINT nIDPrompt = AFX_IDP_PARSE_REAL_RANGE, LPCTSTR pszFormat = _T("%.2lf"), LPCTSTR lpszViewID = NULL, LPCTSTR lpszItemID = NULL);
	void DDXVN_Text(CDataExchange* pDX, int nIDC, int nIDSTC, double& value, LPCTSTR pszType, double minVal, double maxVal, LPCTSTR pszName, LPCTSTR pszFormat = _T("%.4lf"), LPCTSTR lpszViewID = NULL, LPCTSTR lpszItemID = NULL);
// 2020.12.22 ltg End
// 2020.11.05 sys 单位管理 Start
	int DDXVN_Text2_New(double* pVal, TCHAR* pszType, TCHAR* pszBuffer, double minVal, double maxVal, LPCTSTR pszName, LPCTSTR pszFormat, bool bSaveAndValidate);
	BOOL GetUnitInfo(LPCTSTR lpszViewID, LPCTSTR lpszItemID, char* pszUnitType, char* pszCurrUnitName, char* pszDBUnitName = NULL);
// 2020.11.05 sys 单位管理 End

	void DDXIVN_Text(CDataExchange* pDX, int nIDC, int nIDSTC, double& value, 
		LPCTSTR pszType, double minVal, double maxVal, LPCTSTR pszText, LPCTSTR pszFormat = _T("%.4lf"))
	{
		DDXIVN_Text2((VARIANT*)pDX, nIDC, nIDSTC, &value, pszType, minVal, maxVal, pszText, pszFormat);
	}

	// Depth Extend
	void DDX_Depth(CDataExchange* pDX, int nIDC, long& d, LPCTSTR pszFormat = _T("%.4lf "), LPCTSTR lpszViewID = NULL, LPCTSTR lpszItemID = NULL);
	void DDXV_Depth(CDataExchange* pDX, int nIDC, long& d, long dMin, long dMax, LPCTSTR pszFormat = _T("%.4lf "), LPCTSTR lpszViewID = NULL, LPCTSTR lpszItemID = NULL);
	// Depth Extend (Edit + Static)
	void DDX_Depth(CDataExchange* pDX, int nIDC, int nIDSTC, long& dvalue, LPCTSTR pszFormat = _T("%.4lf"), LPCTSTR lpszViewID = NULL, LPCTSTR lpszItemID = NULL);
	void DDXV_Depth(CDataExchange* pDX, int nIDC, int nIDSTC, long& dvalue, long dMin, long dMax, 
		UINT nIDPrompt = AFX_IDP_PARSE_REAL_RANGE, LPCTSTR pszFormat = _T("%.4lf"), LPCTSTR lpszViewID = NULL, LPCTSTR lpszItemID = NULL);

	// GridCtrl Extend
#ifndef _NO_GRIDCTRL
	void DDX_GridText(CDataExchange* pDX, CGridCtrl* pGridCtrl, int nCol, double* pvalue, 
		int nSize, UINT nIDText, LPCTSTR pszType, LPCTSTR pszName = NULL, LPCTSTR pszFormat = _T("%.4lf"));
	
	void DDX_GridText(CDataExchange* pDX, CGridCtrl* pGridCtrl, int nCol, long* pvalue, 
		int nSize, UINT nIDText, LPCTSTR pszType, LPCTSTR pszName = NULL, LPCTSTR pszFormat = _T("%d"));
	
	void DDX_GridText(CDataExchange* pDX, CGridCtrl* pGridCtrl, int nCol, int* pvalue, 
		int nSize, UINT nIDText, LPCTSTR pszType, LPCTSTR pszName = NULL, LPCTSTR pszFormat = _T("%d"));

	//void DDX_DataGridText(CDataExchange* pDX, CUGCtrl* pGridCtrl, int nCol, double* pvalue,
	//	int nSize, UINT nIDText, LPCTSTR pszType, LPCTSTR pszName = NULL, LPCTSTR pszFormat = _T("%.4lf"));

	//void DDX_DataGridText(CDataExchange* pDX, CUGCtrl* pGridCtrl, int nCol, long* pvalue,
	//	int nSize, UINT nIDText, LPCTSTR pszType, LPCTSTR pszName = NULL, LPCTSTR pszFormat = _T("%d"));

	//void DDX_DataGridText(CDataExchange* pDX, CUGCtrl* pGridCtrl, int nCol, int* pvalue,
	//	int nSize, UINT nIDText, LPCTSTR pszType, LPCTSTR pszName = NULL, LPCTSTR pszFormat = _T("%d"));

#endif

// 2020.04.28 np 界面单位项目 Start
	int AddUnitItem(LPCTSTR pszViewID, LPCTSTR pszItemID, int iDBUID, int iSHowUID, LPCTSTR Desc, BOOL bWrite = TRUE);
// 2020.04.28 np 界面单位项目 End
	int ShowManager(long lStyle, LPCTSTR strViewItemID);
	int GetShowId(LPCTSTR lpszViewID, LPCTSTR lpszItemID);//add by yangshuo
// 2020.11.05 sys 单位管理 Start
	BOOL GetShowUnitInfo(LPCTSTR lpszViewID, LPCTSTR lpszItemID, TS_UNITTB* pgUNIT);
// 2020.11.05 sys 单位管理 End
};

UNITS_API CUnits* AfxGetUnits();

#endif //__UNITS_H_

#ifndef _XG_RESOURCE_H_
	#define _XG_RESOURCE_H_

#include  "gfxres.h"

//////////////////////////////////////////////////////
// File commands
#define ID_FILE_PROCSEL						0xD000
#define ID_FILE_REMOVE						0xD001
#define ID_FILE_RECEIVE						0xD002
#define ID_FILE_SEND						0xD003
#define ID_FILE_DETAIL						0xD004
#define ID_FILE_DOTPRINT					0xD005
#define ID_FILE_DOTDETAIL					0xD006
#define ID_FILE_CALCULATE					0xD007
#define ID_FILE_FDIN						0xD008
#define ID_FILE_FDOUT						0xD009
#define ID_FILE_FIRST						0xD00a
#define ID_FILE_PREV						0xD00b
#define ID_FILE_NEXT						0xD00c
#define ID_FILE_LAST						0xD00d
#define ID_FILE_PREVDAY						0xD00e
#define ID_FILE_NEXTDAY						0xD00f
#define ID_FILE_PREVMONTH					0xD010
#define ID_FILE_NEXTMONTH					0xD011
#define ID_FILE_PREVWEEK					0xD012
#define ID_FILE_NEXTWEEK					0xD013
#define ID_FILE_OPEN_DB						0xD014
#define ID_FILE_CLOSE_DB					0xD015

// Edit commands
#define ID_EDIT_TITLECOPY					0xD020
#define ID_EDIT_UNDERCOPY					0xD021
#define ID_EDIT_INSERT						0xD022
#define ID_EDIT_DELETE						0xD023
#define ID_EDIT_SETALL						0xD024
#define ID_EDIT_RELEASE						0xD025
#define ID_EDIT_SHEET						0xD050

// View commands
#define ID_VIEW_LOCATION					0xD030
#define ID_VIEW_RESULT						0xD031
#define ID_VIEW_CHANGE						0xD032
#define ID_VIEW_PAGEPREV					0xD033
#define ID_VIEW_PAGENEXT					0xD034
#define ID_VIEW_BASE						0xD035
#define ID_VIEW_ATT							0xD036
#define ID_VIEW_PAY							0xD037

// Option commands
#define ID_OPTION_DRIVE						0xD040
#define ID_OPTION_OUTSET					0xD041
#define ID_OPTION_SAVEWIDTH					0xD042
#define ID_OPTION_SAVEHEIGHT				0xD043
#define ID_OPTION_CELL_RESET				0xD051


//////////////////////////////////////////////////////
#define	IDB_APP_EXIT                        16384
#define	IDB_FILE_NEW1                       16385
#define	IDB_FILE_NEW2                       16386
#define	IDB_FILE_OPEN1                      16387
#define	IDB_FILE_OPEN2                      16388
#define	IDB_FILE_OPEN3                      16389
#define	IDB_FILE_FIRST                      16390
#define	IDB_FILE_PREV                       16391
#define	IDB_FILE_NEXT                       16392
#define	IDB_FILE_SAVE                       16393
#define	IDB_FILE_REMOVE                     16394
#define	IDB_FILE_LAST                       16395
#define	IDB_FILE_PREVMONTH                  16396
#define	IDB_FILE_NEXTMONTH                  16397
#define	IDB_FILE_PREVDAY                    16398
#define	IDB_FILE_NEXTDAY                    16399
#define	IDB_FILE_PRINT                      16400
#define	IDB_FILE_PRINT_PREVIEW              16401
#define	IDB_FILE_CALCULATE1                 16402
#define	IDB_FILE_CALCULATE2                 16403
#define	IDB_FILE_CALCULATE3                 16404
#define	IDB_FILE_DECIDE                     16405
#define	IDB_FILE_FDIN                       16406
#define	IDB_FILE_FDOUT                      16407
#define	IDB_EDIT_CUT                        16408
#define	IDB_EDIT_COPY                       16409
#define	IDB_EDIT_PASTE                      16410
#define	IDB_EDIT_UNDERCOPY                  16411
#define	IDB_EDIT_INSERT                     16412
#define	IDB_EDIT_DELETE                     16413
#define	IDB_EDIT_EXCEL                      16414
#define	IDB_VIEW_LOCATION                   16415
#define	IDB_VIEW_RESULT                     16416
#define	IDB_VIEW_COMMON                     16417
#define	IDB_VIEW_ATT                        16418
#define	IDB_VIEW_PAY                        16419
#define	IDB_VIEW_REPORT                     16420
#define	IDB_VIEW_PAGEPREV                   16421
#define	IDB_VIEW_PAGENEXT                   16422
#define	IDB_HELP_INDEX                      16423
#define	IDB_OPTION_REFER                    16424
#define	IDB_FILE_EXECSTOP                   16425
#define	IDB_FILE_EXECSTART                  16426
#define	IDB_EDIT_FIGURE                     16427
#define	IDB_EDIT_POSITION                   16428
#define	IDB_FILE_SELITEM                    16429
#define	IDB_OPTION_SENDADD                  16430
#define	IDB_FILE_COMPILE                    16431
#define	IDB_FILE_SET_CELL                   16432
#define	IDB_FILE_SET_TABLE                  16433
#define	IDB_FILE_SET_REPORT                 16434
#define	IDB_OPTION_REPORT                   16435
#define	IDB_REPORT_INPUT                    16436
#define	IDB_REPORT_CHANGE                   16437
#define	IDB_REPORT_OK                       16438
#define	IDB_REPORT_RESET                    16439
#define	IDB_REPORT_CHECK                    16440
#define	IDB_REPORT_MULTIINPUT               16441
#define	IDB_REPORT_ALLOK                    16442
#define	IDB_REPORT_ALLREJECT                16443
#define	IDB_VIEW_REPORT_NEEDOK              16444
#define	IDB_FILE_RESTORE                    16445
#define	IDB_EDIT_RESTORESET                 16446
#define	IDB_EDIT_RESTORERESET               16447
#define	IDB_GROUP_ADD                       16448
#define	IDB_GROUP_REMOVE                    16449
#define	IDB_EDIT_EDITMODE                   16450
#define	IDB_ANALYSIS_MONTHLY                16451
#define	IDB_ANALYSIS_DEPDAILY               16452
#define	IDB_ANALYSIS_WORKTIME               16453
#define	IDB_ANALYSIS_DAY_MANHOUR            16454
#define	IDB_ANALYSIS_HOUR_MANHOUR           16455
#define	IDB_EDIT_ITEM                       16456
#define	IDB_EDIT_WPROTECTON                 16457
#define	IDB_EDIT_WPROTECTOFF                16458
#define	IDB_EDIT_MAKE_TABLE                 16459
#define	IDB_FILE_ORGEN1                     16460
#define	IDB_FILE_CROSS                      16461
#define	IDB_FILE_ANNOUNCE                   16462
#define	IDB_FILE_INPUT                      16463
#define	IDB_FILE_SEARCH                     16464
#define	IDB_FILE_OPEN4                      16465
#define	IDB_VIEW_REAL1                      16466
#define	IDB_VIEW_REAL2                      16467
#define	IDB_FILE_LOGIN                      16468
#define	IDB_FILE_LOGOUT                     16469
#define	IDB_FILE_STORE                      16470
#define	IDB_FILE_RECEIVE                    16471
#define	IDB_FILE_SEND1                      16472
#define	IDB_FILE_SEND2                      16473
#define	IDB_FILE_SEND3                      16474
#define	IDB_FILE_SEND4                      16475
#define	IDB_FILE_ANNOUNCEDAY                16476
#define	IDB_FILE_GRAPH                      16477
#define	IDB_FILE_ERRLIST                    16478
#define	IDB_OPTION_ALARM					16479

//------------------------------------------------------------------------
//  String Table For
//------------------------------------------------------------------------
#define GFX_IDS_MON                         0x4000
#define GFX_IDS_TUE                         0x4001
#define GFX_IDS_WED                         0x4002
#define GFX_IDS_THU                         0x4003
#define GFX_IDS_FRI                         0x4004
#define GFX_IDS_SAT                         0x4005
#define GFX_IDS_SUN                         0x4006
#define GFX_IDS_RECORD_IN_USE               0x4010
#define GFX_IDS_SETDISK                     0x4018
#define GFX_IDS_SPACE                       0x4020
#define GFX_IDS_END                         0x4021
#define GFX_IDS_UNDERLINE_ON                0x4022
#define GFX_IDS_UNDERLINE_OFF               0x4023
#define GFX_IDS_EQUAL                       0x4030
#define GFX_IDS_NOT_EQUAL                   0x4031
#define GFX_IDS_GREATER_EQUAL               0x4032
#define GFX_IDS_LESS_EQUAL                  0x4033
#define GFX_IDS_GREATER                     0x4034
#define GFX_IDS_LESS                        0x4035
#define GFX_IDS_LISTOUT					    0x4040
#define GFX_IDS_EXECUTE					    0x4041
#define GFX_IDS_APPEXECUTE				    0x4050
#define GFX_IDS_NOTAPPOPEN				    0x4051

#define GFX_IDS_REPORTINPUT                 0x4052
#define GFX_IDS_NEEDOK                      0x4053
#define GFX_IDS_REJECT                      0x4054

//------------------------------------------------------------------------
//------------------------------------------------------------------------
#define GFX_IDP_SUCCESS                     0x6000
#define GFX_IDP_NOEMPLOYEE                  0x6001
#define GFX_IDP_ERREMPLOYEE                 0x6002
#define GFX_IDP_NOTERMINAL                  0x6003
#define GFX_IDP_NEEDSAVE                    0x6010
#define GFX_IDP_CHANGEDISK                  0x6012
#define GFX_IDP_DUPLICATEFILE               0x6013
#define GFX_IDP_RECALCULATE                 0x6020
#define GFX_IDP_TOTAL                       0x6021
#define GFX_IDP_CALCULATE                   0x6022
#define GFX_IDP_RENEWAL                     0x6023
#define GFX_IDP_DETAIL                      0x6024
#define GFX_IDP_PARAMLIST                   0x6025
#define GFX_IDP_DEFLIST                     0x6026
#define GFX_IDP_TESTPRN                     0x6027
#define GFX_IDP_REMOVEEMP                   0x6030
#define GFX_IDP_PECLOSE                     0x6040
#define GFX_IDP_PROCSTOP                    0x6041

//------------------------------------------------------------------------
// 
//------------------------------------------------------------------------
#define GFX_IDP_NOTAPPOPEN                  0x6800
#define GFX_IDP_FAILAPPOPEN                 0x6801
#define GFX_IDP_INVALIDDRIVE                0x6802
#define GFX_IDP_NEEDAPPCLOSE                0x6803
#define GFX_IDP_STOPAPPOPEN                 0x6804
#define GFX_IDP_USERRESOURCE                0x6805
#define GFX_IDP_GDIRESOURCE                 0x6806
#define GFX_IDP_FAILAPPMEMERR               0x6807
#define GFX_IDP_SYSDATEERR                  0x6808
#define GFX_IDP_NOTINSTALL                  0x6809
#define GFX_IDP_INVALIDOPCODE               0x6810
#define GFX_IDP_NOPASSWORD                  0x6811
#define GFX_IDP_INVALIDPASSWORD             0x6812
#define GFX_IDP_EXECCHILD                   0x6813
#define GFX_IDP_NOINDRIVE                   0x6820
#define GFX_IDP_NOOUTDRIVE                  0x6821
#define GFX_IDP_DISKFULL                    0x6822
#define GFX_IDP_SETNEWDISK                  0x6823
#define GFX_IDP_ERRCOMPRESS                 0x6824
#define GFX_IDP_ERRDEFROST                  0x6825
#define GFX_IDP_NOFILENAME                  0x6826
#define GFX_IDP_ERROPENALE                  0x6830
#define GFX_IDP_ERRREADALE                  0x6831
#define GFX_IDP_RECORD_IN_USE               0x6832
#define GFX_IDP_NOCALCSERVER                0x6833

#define GFX_IDP_NOTSELECT                   0x6840
#define GFX_IDP_OVERSELECT                  0x6841
#define GFX_IDP_DATAOVER                    0x6850
#define GFX_IDP_DUPLICATECODE               0x6851
#define GFX_IDP_NONAME                      0x6852
#define GFX_IDP_NONICKNAME                  0x6853
#define GFX_IDP_ERRINPUT                    0x6860
#define GFX_IDP_ERRBYTENAME                 0x6861
#define GFX_IDP_ERREMPCODE                  0x6862
#define GFX_IDP_ERRCOMPANYCODE              0x6863
#define GFX_IDP_ERRDEPCODE                  0x6864
#define GFX_IDP_ERRPOSTCODE                 0x6865
#define GFX_IDP_ERRRESERVECODE              0x6866
#define GFX_IDP_ERRCITYCODE                 0x6868
#define GFX_IDP_ERRRECLEN                   0x6869
#define GFX_IDP_ERRRECLENBANK               0x686a
#define GFX_IDP_ERRBANKCODE                 0x686b
#define GFX_IDP_ERRBRCHCODE                 0x686c
#define GFX_IDP_SHIFT_NONE                  0x6880
#define GFX_IDP_MC_NOTUSE                   0x6881
#define GFX_IDP_PUNCH_TURN                  0x6882
#define GFX_IDP_PUNCH_OVER                  0x6883
#define GFX_IDP_PUNCH_INVALID               0x6884
#define GFX_IDP_PUNCH_NOTINS			    0x6885
#define GFX_IDP_ERRPAYPERIOD                0x6890
#define GFX_IDP_ERRPAYDAY                   0x6891
#define GFX_IDP_ERRPAYDAYBANK               0x6892
#define GFX_IDP_DIFFERYEAR                  0x6893
#define GFX_IDP_ERRPEOPEN                   0x68a0
#define GFX_IDP_ERRPEPRINT                  0x68a1
#define GFX_IDP_NONEDEF                     0x68b0
#define GFX_IDP_LIMITLEN                    0x68b1
#define GFX_IDP_NOPOSDIGITS                 0x68b2
#define GFX_IDP_COLLIMITLEN                 0x68b3
#define GFX_IDP_NOROWCOLDIGITS              0x68b4
#define GFX_IDP_ERRCALCULATE                0x68c0
#define GFX_IDP_ERRCALCALREADY              0x68c1
#define GFX_IDP_ERRDOTOPEN                  0x68d0
#define GFX_IDP_ERRDOTALREADY               0x68d1
#define GFX_IDP_ERRDOTWRITE                 0x68d2
#define GFX_IDP_REPORTINPUT                 0x68d3
#define GFX_IDP_NEEDOK                      0x68d4
#define GFX_IDP_REJECT                      0x68d5
#define GFX_IDP_UPDEXCERR1                  0x68d6
#define GFX_IDP_UPDEXCERR2                  0x68d7

//------------------------------------------------------------------------
// 
//------------------------------------------------------------------------

#define GFX_IDP_ERRPEDRV	                23832
#define GFX_IDP_ERRPENODEF		            23833
#define GFX_IDP_ERRPEDEF			        23834

#endif // _XG_RESOURCE_H_

// RawFile.h: interface for the CRawFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RAWFILE_H__C64877E2_8F1C_4992_84E6_2A5A0F1FC7B0__INCLUDED_)
#define AFX_RAWFILE_H__C64877E2_8F1C_4992_84E6_2A5A0F1FC7B0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ULCOMMDEF.H"
#include "XMLSettings.h"
#include "MemPerformFile.h"
#include "ULTool.h"
typedef struct tagCHANNELR
{
    TCHAR   szName[32];					// 通道名称
    int		nBufferSize;				// 通道缓冲长度
    TCHAR   szReserve[32];				// 保留
}CHANNELR;

typedef struct tagTOOLPROPERTY
{
    TCHAR   szName[32];                   // 仪器名称
    int		nChannelCount;                // 仪器申请的通道数量
    int		nBufferSize;                  // 通道缓冲总长度
    TCHAR   szSN[256];                    // 资产号
    int     nCalType;                     // 刻度类型
    TCHAR   szReserve[32];                // 保留
}TOOLPROPERTY;

typedef struct tagTOOLR
{
    TOOLPROPERTY                m_ToolProp;     // 仪器属性
    CArray<CHANNELR, CHANNELR&>	m_ChannelList;  // 所有通道数组
}TOOLR;

typedef struct tagDATAHEAD
{
    long    m_lDepth;                       // 深度
    int     m_nDataSize;                    // 数据长度
    int     m_nToolCount;                   // 调用LogIO的次数
    int     m_nMarkSize;                    // 备注长度
}DATAHEAD;

typedef struct tagDATAHEADEX
{
	COleDateTime	m_oleTime;		//时间
	BYTE			m_bReserve[30];	//保留字节 
}DATAHEADEX;

class   CProject;
class   CToolInfoPack;
class   CULKernel;
class   CULTool;

class CRawFile : public CObject
{
public:
    CRawFile();
    virtual	~CRawFile();

public:

    BOOL		Open(CString strFilePathName);      // 打开文件，存储数据
    BOOL		OpenFile(CString strFilePathName);  // 打开数据文件
    void		Close();                            // 关闭文件
    void		SaveHead();                         // 保存文件头信息
    BOOL		ReadHead();                         // 读取文件头信息
    void		ReadToolCalInfo();
    void		SaveDataHead(DATAHEAD* pDataHead, DATAHEADEX* pDataHeadEx, char* pMark = NULL);  // 保存数据头
	void		SaveDataHeadEx(DATAHEADEX* pDataHeadEx);	//保存扩展数据头	Add by xx 20161108
    void		SaveData(int nIndex, CULTool* pTool, IOBuffer& data);		// 保存数据
	void		LoadParams(int& nIndex);
	CString		GetFirstToolName(BOOL bName = TRUE);	// 获得初始状态
    CString		GetNextToolName();                  // 获得仪器名
	int         GetFirstToolIndex();
	int         GetNextToolIndex(int& nIndex);
    CString		GetData(LPVOID  pBuf);              // 获得数据
	int         GetData(IOBuffer* pData, int& nIndex);
    long		GetDepth();                         // 获得当前深度
    int			GetDataSize();                      // 获得当前帧数据总长度
    int			GetLogIOCount();                    // 获得当前帧中LogIO的个数
    CString		GetMark();                          // 获得当前帧的备注
    
    CToolInfoPack*  GetToolInfoPack(CString strToolName);

    void		SetProject(CProject* pProject);
    void        ResetAllData();                     // 清空数组
    int			GetToolIndex(CString strToolName);  // 获得仪器索引
    BOOL        ReadDataHead();                     // 从文件中读取一个数据头信息

	void	GetDepthInfo();
	BOOL	GetNextDepth(DEPTHINFO* pDI);
	BOOL	SetReadRange(BOOL bLimit = FALSE, long lDepth0 = 0, 
			long lDepth1 = 0);			// 设置读取深度范围

	void		ReadParamInfo();
	void		ReadWinPosInfo();	//获取窗口位置信息
	BOOL		NeedSaveNewFile();
	void		SaveNewFile();
	DWORD		m_dwHeadPos;
	int			m_nFileIndex;
public:
    CPtrArray   m_ToolInfoPackList;		// 仪器信息(回放用)
	CStringArray m_ToolNames;
    CProject*   m_pProject;				// 井段指针
    CULKernel*  m_pKernel;				// 内核指针

    float       m_fVersion;				// 版本号
    CString     m_strFilePathName;		// 文件名及路径
    BOOL        m_bFileIsOpen;			// 文件是否打开
    CFile       m_File;					// 数据文件对象
    int         m_nDirection;			// 文件方向

    DATAHEAD    m_DataHead;				// 临时数据头部信息
    CString     m_strMark;				// 数据头部信息中的备注
    CString     m_strNextToolName;		// 下一只仪器名
    BOOL        m_bOver;				// 文件已结束
    char        m_pBuf[1024];			// 读写缓冲
    int         m_nReadCount;			// 已读取数据的个数
	DWORD		m_dwPosST;				// 起始深度位置
	DWORD		m_dwPosED;				// 终止深度位置
	BOOL		m_bAutoLP;				// 自动加载参数 
	CDWordArray	m_dwParams;
	vec_depth	m_vecDepth;

	// Perforation and coring
	SEGMENT		m_Segment;							//井段信息

	//Add by xx 20161108
	COleDateTime	m_oleNowTime;		//当前回放的时间
	COleDateTime	m_oleTimeST;		//起始时间
	COleDateTime	m_oleTimeED;		//终止时间
	BOOL			m_bCheckTime;		//是否检测时间
	CArray<COleDateTime,COleDateTime> m_arrTimes;

private:
	int	IsInTimeRange();
public:
	BOOL	CanSetTimeRange();
	void	SetTimeRange(COleDateTime& time1, COleDateTime& time2, BOOL bLimit = FALSE);
	DWORD		GetReplayLen();	//获取回放长度
	DWORD		GetReplayHeadPos();	//获取回放起始位置

};

#endif // !defined(AFX_RAWFILE_H__C64877E2_8F1C_4992_84E6_2A5A0F1FC7B0__INCLUDED_)

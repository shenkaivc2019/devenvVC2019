#if !defined(AFX_ULFORMVIEW_H__A6246953_84E1_4E79_91F8_E6C7B66C7A30__INCLUDED_)
#define AFX_ULFORMVIEW_H__A6246953_84E1_4E79_91F8_E6C7B66C7A30__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ULView.h"

// ULFormView.h : header file
//
class CVIWWell;
/////////////////////////////////////////////////////////////////////////////
// CULFormView view

class CULFormView : public CULView
{
	friend class CVIWWell;

public:
	DECLARE_DYNAMIC(CULFormView)

	virtual int CalcPageHeight();
	virtual void Draw(CDC* pDC, LPRECT lpRect);
	virtual void SaveAsBitmap(CDC* pDC, LPCTSTR pszFile);
	

	// Construction
protected:      // must derive your own class
	CULFormView(LPCTSTR lpszTemplateName);
	CULFormView(UINT nIDTemplate);
	~CULFormView();
	
// Implementation
public:
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnActivateView(BOOL, CView*, CView*);
	virtual void OnActivateFrame(UINT, CFrameWnd*);
	BOOL SaveFocusControl();    // updates m_hWndFocus
	virtual void SaveTempl();

public:
	CVIWWell* m_pViwWell;
	LPCTSTR m_lpszTemplateName;
	HWND m_hWndFocus;   // last window to have focus

	virtual void OnDraw(CDC* pDC);      // default does nothing

	//{{AFX_MSG(CFormView)
	afx_msg int OnCreate(LPCREATESTRUCT lpcs);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	afx_msg void OnEdit(UINT nEditCmd);
	afx_msg void OnFormat(UINT nFormat);
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ULFORMVIEW_H__A6246953_84E1_4E79_91F8_E6C7B66C7A30__INCLUDED_)

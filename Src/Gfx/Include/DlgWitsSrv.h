//---------------------------------------------------------------------------//
// 文件名称:	dlgwitssrv.h
// 说明:		服务器端
// 公司名 :		上海神开石油测控技术有限公司
// 作成者:		宁盘
// 作成日:		2019/12/25
// 备注:	无
//---------------------------------------------------------------------------//

#if !defined(AFX_DLGWITSSRV_H__CC0E1630_88CA_4113_BC4C_CA8B78CE9760__INCLUDED_)
#define AFX_DLGWITSSRV_H__CC0E1630_88CA_4113_BC4C_CA8B78CE9760__INCLUDED_

#include <WINSOCK2.H>
#include "HexEditCtrl.h"
#include <afxmt.h>
#include "Resource.h"
#include <vector>
#include <queue>
#include <Afxtempl.h>
#include <afxmt.h>

#define WM_SOCK (WM_USER+29)

//#define WM_TCP_RECV_DATA	(WM_USER+601)		// 网络接受数据
//#define WM_TCP_CONNECT		(WM_USER+602)		// 网络连接
//#define WM_TCP_DISCONNECT	(WM_USER+603)		// 网络断开

#define RECV_BUFFER_SIZE	1024

using namespace std; 

#pragma comment( lib, "WS2_32" )

#define BUFFER_SIZE 4096

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgWitsSrv.h : header file
//
#ifndef WITSSRV_API
#ifdef WITSSRV_EXPORT
#define WITSSRV_API __declspec( dllexport )
#else 
#define WITSSRV_API __declspec( dllimport )
#endif
#endif

//---------------------------------------------------------------------------//
// 结构体名称:	客户端连接信息结点
// 说明:		用来记录客户端的一些连接信息
// 备注:
//---------------------------------------------------------------------------//
struct NodeClientInfo
{
	int iOrder;//编号
	int sClient;//socket值
	CString strIP;//IP地址
	CString strStatus;//连接状态
	BOOL bSendOrRecv;//接收或发送标识符，对于服务器来说，接收为TRUE，发送为FALSE
	CString strComment;//备注
};

/////////////////////////////////////////////////////////////////////////////
// CDlgWitsSrv dialog
class CDlgAttriTools;
class WITSSRV_API CDlgWitsSrv : public CDialog
{
	// Construction
public:
	CDlgWitsSrv(CWnd* pParent = NULL);	// standard constructor
	~CDlgWitsSrv();

	void OnAccept(SOCKET sClient);
	void RecvData ();
	void inItList();//客户端信息列表初始化
	void showList();//客户端信息列表数据填充

	//LRESULT TcpRecvData(WPARAM wParam);
	//LRESULT TcpConnect();
	


public:
	UINT m_nTimer;
	BOOL m_bOpened;
	BOOL m_bDataRun;
	long m_nDataRecv;
	SOCKET m_sServer;//服务器端的socket
	SOCKET m_sClient;//客户端数据socket
	SOCKET m_sClientSend;//客户端发送数据socket的副本
	HANDLE m_hAcceptThread;//accept线程
	HANDLE m_hProbesThread;//探测线程，用来探测服务器与客户端的连接是否断开
	HANDLE m_hSendThread;//send线程
	HANDLE m_hThread;
	BYTE m_Data[BUFFER_SIZE];
	queue<CString> m_queueRecvData;//服务器端接收到的数据队列
	float m_fDepth;
	CFont m_editFont;//字体/大小
	vector<NodeClientInfo> m_vecClientInfo;//存储客户端相关信息列表的数据
	int m_nInfoCount;//计数变量，数据量
	int m_iRow;//计数变量，行数
	vector<SOCKET> vecClientSocket;//客户端
	CListCtrl m_ctrlList;//客户端相关信息列表
	HANDLE m_hMasterChangEvent;
	CList<CRect, CRect> listRect;//对话框放缩
	bool m_bRes;//用作socket流程各函数调用依据

	CDlgAttriTools* m_pDlgAttriTools; //属性对话框
	CString m_strMessage;//服务器端给客户端Send的内容
	CString m_strIP;//服务器端给客户端Send的 客户端IP

	CCriticalSection m_SecServer;//定义全局变量m_Sec



	// Dialog Data
	//{{AFX_DATA(CDlgWitsSrv)
	//enum { IDD = IDD_DLG_WITSSRV };
	CHexEdit	m_ctrlRead;
	UINT	m_nPort;
	UINT	m_nShow;
	BOOL	m_bBitPos;
	BOOL	m_bDepth;
	BOOL	m_bInc;
	BOOL	m_bAzi;
	BOOL	m_bGx;
	BOOL	m_bGy;
	BOOL	m_bGz;
	BOOL	m_bGF;
	BOOL	m_bMF;
	//}}AFX_DATA
	
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgWitsSrv)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL
	
	// Implementation
protected:
	HICON m_hIcon;
	
	// Generated message map functions
	//{{AFX_MSG(CTestNetServerDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	afx_msg void OnBtnOpen();
	afx_msg void OnBtnClose();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//afx_msg void OnItemchangedList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnNMRClickListCard(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnMenuOff();
	afx_msg void OnMenuReConn();
	afx_msg void OnMenuAttri();
	afx_msg LRESULT OnSocket(WPARAM w,LPARAM l);
	afx_msg void OnBnClickedButtonClear();
	afx_msg LRESULT TcpDisConnect();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif

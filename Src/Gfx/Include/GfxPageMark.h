#if !defined(AFX_GFXPAGEMARK_H__9F94D92B_51D4_41EF_BFDD_23FBBB65A1B3__INCLUDED_)
#define AFX_GFXPAGEMARK_H__9F94D92B_51D4_41EF_BFDD_23FBBB65A1B3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GfxPage.h"
#include "ComConfig.h"
// 2022.07.30 Start
#include "LineWidthCB.h"
#include "LineStyleCB.h"
// 2022.07.30 End
// GfxPageMark.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// CGfxPageMark dialog


class CBaseMark;
class CGfxPageMark : public CGfxPage
{
// Construction
public:
	CGfxPageMark(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CGfxPageMark)
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_GFX_PAGE_MARK };
//#endif
	CMFCColorButton  m_MarkColorBtn;
	CXComboBox	m_cbPaintMode;
	CMFCFontComboBox m_cmbFont;
	CComboBox	m_cmbFontSize;
	CComboBox	m_cmbFontStyle;
	CListCtrl	m_markList;
	CString	m_strName;
// 2022.07.30 Start	
	CLineWidthCB m_cbLineWidth;
	CLineStyleCB m_cbLineStyle;
	CComboBox	m_cbLinePos;
	CComboBox	m_cbTextAlign;
	int m_nLineLength;
	int m_nLineLength2;
	float m_fAngle;
	CString	m_strContent;
	int m_nArrowLineLen;
// 2022.07.30 End
	//}}AFX_DATA
	long	m_lDepth;
	
public:
	int			m_nTrackNO;
	CPoint		m_ptClick;
	ULFONT		m_FontInfo;
	CFont		m_Font;
public:
	void  InitMark();
	void  RefreshMark();
	void  AddMarkToList(CBaseMark* pMark);
	void  OnItemSelChanged();
	void  UpdateFont();
	void  UpdateList();
// 2023.07.12 Start
	void InitSheetMark(CSheet* pSheet);
	void OnMarkAdd2Sheet();
	void AddSheetMarkToList(CBaseMark* pMark);
	void OnMarkDeleteSheet();
	void UpdateFont2Win(ULFONT &gFontInfo);
// 2022.07.30 Start	
	int TurnLineWidth(int iWidth, BOOL bValue = TRUE);
// 2022.07.30 End	
// 2023.07.12 End
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGfxPageMark)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	COLORREF  m_markColor;
	// Generated message map functions
	//{{AFX_MSG(CGfxPageMark)
	afx_msg void OnMarkAdd();
	virtual BOOL OnInitDialog();
	afx_msg void OnMarkGoto();
	afx_msg void OnMarkDelete();
	afx_msg void OnGetMarkColor();
	afx_msg void OnAutoMark();
	afx_msg void OnAutoFR();
	afx_msg void OnCurveFlag();
	afx_msg void OnKeydownList1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClickList1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkList1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchangeFont();
	afx_msg void OnSelchangeFontSize();
	afx_msg void OnSelchangeFontStyle();
	afx_msg void OnSelchangePaintMode();
	afx_msg void OnSelchangeLinePos();
	afx_msg void OnPaint();
	afx_msg void OnMarkApply();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GFXPAGEMARK_H__9F94D92B_51D4_41EF_BFDD_23FBBB65A1B3__INCLUDED_)

// BaseMark.h: interface for the CBaseMark class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BASEMARK_H__3925C4A1_2CAB_4E82_9A07_607BBE6673DC__INCLUDED_)
#define AFX_BASEMARK_H__3925C4A1_2CAB_4E82_9A07_607BBE6673DC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MARKS_LINE		0
#define MARKS_TRIANGLE	1
#define MARKS_ARROW		2
#define MARKS_TD		3
#define MARKS_FR		4
#define MARKS_CS		5
#define MARK_Curve		6

#include "XMLSettings.h"
#include "PrintInfo.h"
class CTrack;
class CBaseMark  
{
public:
	CBaseMark();
	virtual ~CBaseMark();
	void        Serialize(CXMLSettings& xml);
	void        Draw(CDC* pDC, long lStartDepth, long lEndDepth);
	void        Print(CULPrintInfo* pInfo, float fMinDepth, float fMaxDepth);
	BOOL        IsPtInShape(CPoint pt);
	void		DrawArrow(CDC *pDC , CRect rcStart , CPoint ptEnd);
//	BOOL		DrawCurveFlag(CDC* pDC, long lStartDepth, long lEndDepth, int nCount);
	long		ComputeDepthOffSet(long lStartDepth, long lEndDepth);
public:
	int			m_nTrackNO;			// 通道号
	int         m_nShape;           // 标记形状
	COLORREF    m_crColor;          // 标记颜色
	COLORREF    m_crText;			// 标记文本颜色
    double      m_fMarkXCoordinate;	// 标记的横坐标
	double		m_fRawXCoordinate;	// 标记原点横坐标，用于FR标签的指针
	CTrack*		m_pTrack;			// 标记井道
	int			m_nPointer;			// 标记指向	0左边界越界，1右边界越界
	BOOL        m_bDepthChange;		// 拖动标签时名称是否改变
	CString		m_strName;			// 标记名称
	//CCurve*  	m_pCurve;			// 标记曲线
	CRect		m_rect;
	CRect       m_rcMark;
	BOOL        m_bHot;
	long		m_lDepth;			// 标记深度
	long		m_lRawDepth;		// 标记原点深度，用于FR标签的指针
	float       m_fXScale;
	BOOL        m_bWatch;           // 监视型标签
	BOOL        m_bRealTime;        // 实时测井时仪器库中添加的标签
	double		m_fLeftMargin;
	double		m_fRightMargin;
	CPoint		m_pointCurveFlag;	//曲线标识点
	CArray<long,long>		m_Arr_lDepthFlag;   //保存曲线表示的深度点
	CArray<CPoint,CPoint>	m_Array_point;
	int			m_nIndex,m_nIndexText;
	int			m_nCurve;
};

#endif // !defined(AFX_BASEMARK_H__3925C4A1_2CAB_4E82_9A07_607BBE6673DC__INCLUDED_)

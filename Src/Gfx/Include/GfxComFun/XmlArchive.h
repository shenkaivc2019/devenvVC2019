// XmlArchive.h: interface for the CXmlArchive class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_XMLARCHIVE_H__7F4CDCFC_CD1E_4B0F_A74B_05814FD8110B__INCLUDED_)
#define AFX_XMLARCHIVE_H__7F4CDCFC_CD1E_4B0F_A74B_05814FD8110B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "stdafx.h"

#ifndef COM_API
	#ifdef COM_EXPORT
		#define COM_API __declspec( dllexport )
	#else
		#define COM_API __declspec( dllimport )
	#endif
#endif
// (XML Exception) XML 文件读取异常
#define XMLE_REACH_FILEEND          1           // 读取过程达到了文件尾
#define XMLE_MISS_BEGIN_KEY         2           // 丢失起始键名
#define XMLE_MISS_END_KEY           3           // 丢失结束键名
#define XMLE_MISMATCH_BEGIN_KEY     4           // 不匹配的起始键名
#define XMLE_MISMATCH_END_KEY       5           // 不匹配的结束键名
#define XMLE_UNKNOWN_CLASS_NAME     6           // 未知类名称

//////////////////////////////////////////////////////////////////////
// CCrossException: 交会图简单异常

class COM_API CCrossException : public CException
{
public:
    CCrossException(LPCTSTR lpszMessage = _T("")); 
    virtual ~CCrossException();     
	virtual BOOL GetErrorMessage(LPTSTR lpszError, UINT nMaxError, PUINT pnHelpContext = NULL);

    CString m_strMessage; 
}; 

//////////////////////////////////////////////////////////////////////
// CXmlArchive: XML序列化文档

class COM_API CXmlArchive
{
public:
	CXmlArchive(CStdioFile* pFile, BOOL bStoring);
	virtual ~CXmlArchive();

    BOOL IsStoring(); 
    void WriteLine(LPCTSTR lpszText); 
    void WriteKey(LPCTSTR lpszKeyName, BOOL bBegin = TRUE); 
    void WriteValue(LPCTSTR lpszKeyName, LPCTSTR lpszValue); 
    void WriteValue(LPCTSTR lpszKeyName, DWORD dwValue); 
    void WriteValue(LPCTSTR lpszKeyName, double fValue); 

    void ReadLine(CString& strText = CString()); 
    void ReadKey(CString strKeyName, BOOL bBegin = TRUE); 
    void ReadValue(CString strKeyName, CString& strValue, BOOL bMultiLine = TRUE); 
    void ReadValue(CString strKeyName, DWORD* pValue); 
    void ReadValue(CString strKeyName, double* pValue); 

    //void AddClass(CRuntimeClass* pClass); 
    CObject* ReadClass(CString& strKeyName = CString()); 
    CRuntimeClass* FindClass(LPCTSTR lpszClassName); 
    void ThrowXmlException(UINT nType, LPCTSTR lpszText1 = NULL, LPCTSTR lpszText2 = NULL); 

    void SerializeObject(LPPOINT pPoint, LPCTSTR lpszKeyName = NULL); 
    void SerializeObject(LPSIZE pSize, LPCTSTR lpszKeyName = NULL); 
    void SerializeObject(LPRECT pRect, LPCTSTR lpszKeyName = NULL); 
    void SerializeObject(LPLOGPEN pPen, LPCTSTR lpszKeyName = NULL); 
    void SerializeObject(LPLOGBRUSH pBrush, LPCTSTR lpszKeyName = NULL); 
    void SerializeObject(LPLOGFONT pFont, LPCTSTR lpszKeyName = NULL); 

protected:
    CStdioFile*     m_pFile;            // 操作的文件
    BOOL            m_bStoring;         // 是否正在保存文件
    UINT            m_nIndent;          // 保存文件时设置的缩进量
    UINT            m_nLineNO;          // 当前读取的行号
    //CPtrList        m_listClass;        // 读取时动态创建对象的列表
};

#endif // !defined(AFX_XMLARCHIVE_H__7F4CDCFC_CD1E_4B0F_A74B_05814FD8110B__INCLUDED_)

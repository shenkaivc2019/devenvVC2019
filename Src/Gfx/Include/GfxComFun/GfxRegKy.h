//---------------------------------------------------------------------------//
// ファイル名：GfxRegKey.h
// 説明　　　：CGfxRegKeyクラスのインターフェイス
// 作成者　　：COSMO 石川
// 作成日　　：2002/09/30
// 備考　　　：TS対応(V4.0)
// 機能		 ：	TerminalService(以下TS)でTimePro-Getを動作させるためにレジストリ
//				構造が変更となっており、この構造に対応するために作成されている。
//				C/SモードとコンソールセッションはC/S時と同じレジストリ構造を使用
//				するが、リモートセッションではクライアント固有の情報は
//				HKEY_LOCAL_MACHINE\SOFTWARE\AMANO\GETに"[TSクライアントのコンピュータ名]"
//				を追加したキー以下を参照する。
//				・クライアント共通キー（HKEY_LOCAL_MACHINE\SOFTWARE\AMANO\GET直下のキー名で判断)
//					Genaral
//					Comm232C
//					Gdbapi
//					GetIni（但し、GetIni\Clientキーのみクライアント個別で[TSクライアントのコンピュータ名]\Clientとして扱う。特例今後はこのようなコードは禁止。）
//					GfxEventLog
//					Xds0000
//					Xds0200
//					Xg8000
//					先頭文字がアルファベットでないキー
// 使用方法	 ：	TimePro-Getで使用するレジストリへのアクセスは全てこのクラスを
//				使用しなければならない。
//				①HKEY_LOCAL_MACHINE\SOFTWARE\AMANO\Get以下にアクセスする場合は
//				HKEY_GETを予約キーに設定すること。
//				②①以外にアクセスする場合は通常のレジストリと同じWIN32の予約キー
//				を指定する。
//				・static指定のメンバ関数は単体で使用する。
//				ex)CGfxRegKey::GetValue(HKEY_GET, "GetIni\\General", "Server", &strServer);
//				・static指定のないメンバ関数はOpen()している必要がる。
// 注意事項	 ：	レジストリ操作に関して本クラスでは機能が足りない場合は本クラスを
//				拡張しなければならない。
//---------------------------------------------------------------------------//
#ifndef _GFXREGKEY_H_
#define _GFXREGKEY_H_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

///////////////////////////////////////////////////////////
// TimePro-Get用の予約キー
#define HKEY_GET			(( HKEY ) 0xF0000000 )
						// HKEY_LOCAL_MACHINE\SOFTWARE\AMANO\GETをルートとする

///////////////////////////////////////////////////////////
// オープンモード
#define GFXREG_OPEN_NORMAL		0	// 詳細はOpen()を参照
#define GFXREG_OPEN_CREATE		1	// 
#define GFXREG_OPEN_READONLY	2	// 

///////////////////////////////////////////////////////////
// エラー値
#define GFXREG_ERR_BASE				0x88880000
#define IsNativeError(x)			((x&0xFFFF0000)==GFXREG_ERR_BASE)
															// CGfxRegKeyクラス独自のエラーかチェック
#define GFXREG_ERR_NOT_OPEN			(GFXREG_ERR_BASE|1)		// オープンされていません。
#define GFXREG_ERR_INVALIDARG		(GFXREG_ERR_BASE|2)		// 必要な引数がNULLポインタです。
#define GFXREG_ERR_NOMATCH_TYPE		(GFXREG_ERR_BASE|3)		// 取得しようとしている値と型が違う（適切な関数を指定すること）
#define GFXREG_ERR_ALLOCATE			(GFXREG_ERR_BASE|4)		// メモリの確保に失敗しました。

///////////////////////////////////////////////////////////
// クラス定義

#undef AFX_DATA
#define AFX_DATA AFX_EXT_DATA

//class CGfxRegKey  
class CGfxRegKey  
{
public:
	CGfxRegKey();
	~CGfxRegKey();

	static long GetValue(HKEY hReserveKey, LPCTSTR szSubKey, LPCTSTR szEntry, DWORD* pdwValue);			// for REG_DWORD
	static long GetValue(HKEY hReserveKey, LPCTSTR szSubKey, LPCTSTR szEntry, CString *pstrValue);		// for REG_SZ or REG_EXPAND_SZ
	static long GetValue(HKEY hReserveKey, LPCTSTR szSubKey, LPCTSTR szEntry, CStringArray* astrValue);	// for REG_MULTI_SZ
	static long GetValue(HKEY hReserveKey, LPCTSTR szSubKey, LPCTSTR szEntry, PBYTE pbyValue, LPDWORD pdwSize, LPDWORD pdwType);	// for REG_BINARY & another type
	long GetValue(LPCTSTR szEntry, DWORD *pdw);					// for REG_DWORD
	long GetValue(LPCTSTR szEntry, CString *pstr);				// for REG_SZ or REG_EXPAND_SZ
	long GetValue(LPCTSTR szEntry, CStringArray *parstr);		// for REG_MULTI_SZ
	long GetValue(LPCTSTR szEntry, PBYTE pbyValue, DWORD *pdwSize, DWORD *pdwType);	// for REG_BINARY & another type
	// 値の取得
	// hReserveKey		: HKEY_GET, HKEY_CURRENT_USER, HKEY_LOCAL_MACHINE, ...
	// szSubKey			: キー（HKEY_GETの場合はHKEY_LOCAL_MACHINE\SOFTWARE\AMANO\GET以下を指定）
	// szEntry			: 値名
	// pdwValue			: REG_DWORD,REG_DWORD_LITTLE_ENDIAN,REG_DWORD_BIG_ENDIAN用
	// pstrValue		: REG_SZ,REG_EXPAND_SZ用
	// parstrValue		: REG_MULTI_SZ用
	// pbyValue			: REG_BINARY,その他用 必要に十分な領域を持った変数のポインタを指定
	//					  取得するサイズはGetValueInfo()で先に取得できます。
	// pdwSize			: 取得したデータのバイト長 (NULL可)
	// pdwType			: 取得したデータの型(REG_～) (NULL可)
	// 戻り値			: 値が取得できた場合は0、取得できなかった場合は0以外が戻る。
	//
	// static型の関数は関数単体で使用が可能。（一時的にオープン－値取得－クローズを行う）
	// static型でない関数はOpen()しておく必要がある。
	// エラーなどで値が取得できない場合は～Valueは変化しない。
	// キーがない場合はキーは作成しません。

	static long SetValue(HKEY hReserveKey, LPCTSTR szSubKey, LPCTSTR szEntry, DWORD dwValue);				// for REG_DWORD
	static long SetValue(HKEY hReserveKey, LPCTSTR szSubKey, LPCTSTR szEntry, LPCTSTR szValue);			// for REG_SZ or REG_EXPAND_SZ
	static long SetValue(HKEY hReserveKey, LPCTSTR szSubKey, LPCTSTR szEntry, CStringArray *pastrValue);	// for REG_MULTI_SZ
	static long SetValue(HKEY hReserveKey, LPCTSTR szSubKey, LPCTSTR szEntry, PBYTE pbyValue, DWORD dwSize, DWORD dwType);	// for REG_BINARY & another type
	long SetValue(LPCTSTR szEntry, DWORD dwValue);				// for REG_DWORD
	long SetValue(LPCTSTR szEntry, LPCTSTR szValue);			// for REG_SZ or REG_EXPAND_SZ
	long SetValue(LPCTSTR szEntry, CStringArray *pastrValue);	// for REG_MULTI_SZ
	long SetValue(LPCTSTR szEntry, PBYTE pbyValue, DWORD dwSize, DWORD dwType);	// for REG_BINARY & another type
	// 値の設定(作成)
	// hReserveKey		: HKEY_GET, HKEY_CURRENT_USER, HKEY_LOCAL_MACHINE, ...
	// szSubKey			: キー（HKEY_GETの場合はHKEY_LOCAL_MACHINE\SOFTWARE\AMANO\GET以下を指定）
	// szEntry			: 値名
	// dwValue			: REG_DWORD,REG_DWORD_LITTLE_ENDIAN,REG_DWORD_BIG_ENDIAN用
	// szValue			: REG_SZ,REG_EXPAND_SZ用
	// parstrValue		: REG_MULTI_SZ用
	// pbyValue			: REG_BINARY,その他用
	// dwSize			: 設定するデータのバイト長
	// dwType			: 設定するデータの型(REG_～)
	// 戻り値			: 値が設定できた場合は0、設定できなかった場合は0以外が戻る。
	//
	// static型の関数は関数単体で使用が可能。（一時的にオープン－値設定－クローズを行う）
	// static型でない関数はOpen()しておく必要がある。
	// キーや値がない場合はキーや値を作成します。

	static long GetValueInfo(HKEY hReserveKey, LPCTSTR szSubKey, LPCTSTR szEntry, LPDWORD pdwSize, LPDWORD pdwType);
	long GetValueInfo(LPCTSTR szEntry, DWORD *pdwSize, DWORD *pdwType);
	// 値のデータ長と型を取得
	// hReserveKey		: HKEY_GET, HKEY_CURRENT_USER, HKEY_LOCAL_MACHINE, ...
	// szSubKey			: キー（HKEY_GETの場合はHKEY_LOCAL_MACHINE\SOFTWARE\AMANO\GET以下を指定）
	// szEntry			: 値名
	// pdwSize			: 取得したデータのバイト長 (NULL可)
	// pdwType			: 取得したデータの型(REG_～) (NULL可)
	// 戻り値			: 値が取得できた場合は0、取得できなかった場合は0以外が戻る。
	//					  値がない場合は2を返す
	//
	// static型の関数は関数単体で使用が可能。（一時的にオープン－Size,Type取得－クローズを行う）
	// static型でない関数はOpen()しておく必要がある。
	// エラーなどでSize、Typeが取得できない場合はSize、Typeは変化しない。

	static long IsExistValue(HKEY hReserveKey, LPCTSTR szSubKey, LPCTSTR szEntry);
	static long IsExistKey(HKEY hReserveKey, LPCTSTR szSubKey);
	long IsExistValue(LPCTSTR szEntry);
	// 値の存在チェック、キーの存在チェック
	// hReserveKey		: HKEY_GET, HKEY_CURRENT_USER, HKEY_LOCAL_MACHINE, ...
	// szSubKey			: キー（HKEY_GETの場合はHKEY_LOCAL_MACHINE\SOFTWARE\AMANO\GET以下を指定）
	// szEntry			: 値名
	// 戻り値			：値(キー)が存在している場合はTRUE、存在しない場合はFALSE
	//
	// static型の関数は関数単体で使用が可能。（一時的にオープン－存在チェック－クローズを行う）
	// static型でない関数はOpen()しておく必要がある。
	// エラーなどでSize、Typeが取得できない場合はSize、Typeは変化しない。

	long Open(HKEY hReserveKey, LPCTSTR szSubKey, long lOpenMode = GFXREG_OPEN_CREATE);
	// キーのオープン
	// hReserveKey		: HKEY_GET, HKEY_CURRENT_USER, HKEY_LOCAL_MACHINE, ...
	// szSubKey			: キー（HKEY_GETの場合はHKEY_LOCAL_MACHINE\SOFTWARE\AMANO\GET以下を指定）
	// lOpenMode		: GFXREG_OPEN_NORMAL		読書き可、キーが存在しない場合はエラー
	//					  GFXREG_OPEN_CREATE		読書き可、キーが存在しない場合は作成する
	//					  GFXREG_OPEN_READONLY		読込み可、キーが存在しない場合はエラー
	// 戻り値			: オープンできた場合は0、できなかった場合は0以外を返す
	//					  サブキーがない場合は2

	long Close();
	// キーのクローズ

	HKEY DetachKey();
	// CGfxRegKeyクラスからキーを切離す
	// オープンしているキーをクラスから切離して取得する。クラスはクローズ状態に戻る。

	void AttatchKey(HKEY hOpenKey);
	// CGfxRegKeyクラスにキーを割り当てる
	// オープンしている場合はクローズされてキーを設定する。クラスはオープン状態になる。

	BOOL IsOpen();
	// オープン中チェック
	// オープンしている場合はTRUE、クローズしている場合はFALSEを返す

	static long DeleteValue(HKEY hReserveKey, LPCTSTR szSubKey, LPCTSTR szEntry);
	long DeleteValue(LPCTSTR szEntry);
	// 値の削除
	// hReserveKey		: HKEY_GET, HKEY_CURRENT_USER, HKEY_LOCAL_MACHINE, ...
	// szSubKey			: キー（HKEY_GETの場合はHKEY_LOCAL_MACHINE\SOFTWARE\AMANO\GET以下を指定）
	// szEntry			: 削除する値名
	// 戻り値			: 削除できた、または値が存在しないは0、削除できなかった場合は0以外を返す
	//
	// static型の関数は関数単体で使用が可能。（一時的にオープン－値削除－クローズを行う）
	// static型でない関数はOpen()しておく必要がある。

	static long DeleteKey(HKEY hReserveKey, LPCTSTR szSubKey, BOOL bAll=TRUE);
	// キーの削除
	// hReserveKey		: HKEY_GET, HKEY_CURRENT_USER, HKEY_LOCAL_MACHINE, ...
	// szSubKey			: キー（HKEY_GETの場合はHKEY_LOCAL_MACHINE\SOFTWARE\AMANO\GET以下を指定）
	// bAll				: 下位のキーと値を全て削除する場合はTRUE、しない場合はFALSE
	// 戻り値			: 削除できた、または値が存在しないは0、削除できなかった場合は0以外を返す
	//
	// static型の関数は関数単体で使用が可能。（一時的にオープン－値削除－クローズを行う）
	// static型でない関数はOpen()しておく必要がある。
	// 現在はbAllはTRUEで固定（理由：Win95では全ての下位データを削除してしまうため)

	long EnumValue(DWORD dwIndex, CString *pstrEntry, LPDWORD pdwReserve=NULL, LPBYTE pbyValue=NULL, DWORD *pdwSize=NULL, DWORD *pdwType=NULL);
	// キーの下位にいる値を取得
	// dwIndex			: 値のインデックス(0～)
	// pstrEntry		: 値名
	// pdwReserve		: 予約、つねにNULL
	// pbyValue			: 値 (NULL可)
	// pdwSize			: 値のデータ長 (NULL可)
	// pdwType			: 値の型 (NULL可)
	// 戻り値			: 取得できた場合は0、できなかった場合は0以外を返す
	//					  値がない場合はERROR_NO_MORE_ITEMS(259)を返す
	//
	// Open()しておく必要がある。

	long EnumKey(DWORD dwIndex, CString *pstrKey);
	// キーの下位にいる値を取得
	// dwIndex			: キーのインデックス(0～)
	// pstrKey			: キー名
	// 戻り値			: 取得できた場合は0、できなかった場合は0以外を返す
	//					  値がない場合はERROR_NO_MORE_ITEMS(259)を返す
	//
	// Open()しておく必要がある。

	static long CGfxRegKey::GetKeyInfo(HKEY hReserveKey, LPCTSTR szSubKey, DWORD *pdwSubKeys, DWORD *pdwMaxSubKeyLen, DWORD *pdwValues, DWORD *pdwMaxValueNameLen, DWORD *pdwMaxValueLen, PFILETIME pftLastWriteTime);
	long  CGfxRegKey::GetKeyInfo(DWORD *pdwSubKeys, DWORD *pdwMaxSubKeyLen, DWORD *pdwValues, DWORD *pdwMaxValueNameLen, DWORD *pdwMaxValueLen, PFILETIME pftLastWriteTime);
	// キーの直下にあるサブキーと値の情報を取得
	// hReserveKey		: HKEY_CURRENT_USER, HKEY_LOCAL_MACHINE, ... ,HKEY_GET
	// szSubKey			: キー（HKEY_GETの場合はHKEY_LOCAL_MACHINE\SOFTWARE\AMANO\GET以下を指定）
	// pdwSubKeys		: サブキーの数 (NULL可)
	// pdwMaxSubKeyLen	: サブキーの最大の文字数 (NULL可)
	// pdwValues		: 値の数 (NULL可)
	// pdwMaxValueNameLen	: 値名の最大の文字数 (NULL可)
	// pdwMaxValueLen	: 値の最大のバイト数 (NULL可)
	// pftLastWriteTime	: 最後に書き込みをした日時 (NULL可)
	// 戻り値			: 取得できた場合は0、できなかった場合は0以外を返す

	long RemoteOpen(LPCTSTR szComputerName, HKEY hReserveKey, LPCTSTR szSubKey, long lOpenMode);
	// リモートＰＣのレジストリにアクセスする
	// szComputerName	: リモートコンピュータ名
	// hReserveKey		: 予約キー
	// szSubKey			: サブキー
	// lOpenMode		: オープンモード
	// 戻り値			: 接続できた場合は0、できなかった場合は0以外を返す
	//
	// リモートＰＣに接続するだけでOpen()と同じ。


private:
	HKEY	m_hOpenKey;			// オープンしたキー
	HKEY	m_hRemoteKey;		// リモートＰＣでオープンしたキー

	static long GetValueX(HKEY hReserveKey, LPCTSTR szSubKey, LPCTSTR szEntry, DWORD dwType, DWORD *pdw, CString *pstr, CStringArray *parstr, PBYTE pby, DWORD *pdwSize, DWORD *pdwType);
	static long GetValueX(HKEY hOpenKey, LPCTSTR szEntry, DWORD dwType, DWORD *pdw, CString *pstr, CStringArray *parstr, PBYTE pby, DWORD *pdwSize, DWORD *pdwType);
	static long SetValueX(HKEY hReserveKey, LPCTSTR szSubKey, LPCTSTR szEntry, DWORD dwType, DWORD dw, LPCTSTR sz, CStringArray *parstr, PBYTE pby, DWORD dwSetSize, DWORD dwSetType);
	static long SetValueX(HKEY hOpenKey, LPCTSTR szEntry, DWORD dwType, DWORD dw, LPCTSTR sz, CStringArray *parstr, PBYTE pby, DWORD dwSetSize, DWORD dwSetType);
	static long GetValueInfoX(HKEY hOpenKey, LPCTSTR szEntry, DWORD *pdwSize, DWORD *pdwType);
	static long OpenX(HKEY hReserveKey, HKEY hRemoteKey, LPCTSTR szSubKey, long lOpenMode, HKEY *phKey);
	static CString CreateSubKey(HKEY hReserveKey, LPCTSTR szSubKey);
	static BOOL IsGlobalKey(CString *pstrSubKey);
	static long CloseX(HKEY hOpenKey);
	static long DeleteValueX(HKEY hOpenKey, LPCTSTR szEntry);
	static long GetKeyInfoX(HKEY hOpenKey, DWORD *pdwSubKeys, DWORD *pdwMaxSubKeyLen, DWORD *pdwValues, DWORD *pdwMaxValueNameLen, DWORD *pdwMaxValueLen, PFILETIME pftLastWriteTime);
};

#undef AFX_DATA
#define AFX_DATA

#endif // _GFXREGKEY_H_

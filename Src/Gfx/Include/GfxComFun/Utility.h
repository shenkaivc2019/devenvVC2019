#ifndef __UTILITY_H
#define __UTILITY_H

#undef AFX_DATA
#define AFX_DATA AFX_EXT_DATA

BOOL   APIENTRY  EnableDlgItem(CWnd* pWnd, int nID, BOOL bEnable);
int    APIENTRY  GetDiskFreeM(CString& strPath);
float  APIENTRY  GetDiskFreeG(CString& strPath);

BOOL APIENTRY  SimpleScanf(LPCTSTR lpszText, LPCTSTR lpszFormat, void* pData);
BOOL APIENTRY  SimpleFloatParse(LPCTSTR lpszText, double& d);

void APIENTRY RecurseFind(CStringList& lstFiles, LPCTSTR pszPath, LPCTSTR pszFind, int nLevel = 0);
CString APIENTRY GetFileName(LPCTSTR pszPathName);

bool FileDelete(LPCTSTR lpszPath);
bool FileCopy(LPCTSTR pFrom, LPCTSTR pTo);
void APIENTRY CopyDir(LPCTSTR src, LPCTSTR dst, BOOL bRecursive = FALSE);

inline int roundleast(int n);
void RoundRect(LPRECT r1);
void MulDivRect(LPRECT r1, LPRECT r2, int num, int div);

void RoundInt(int& n);
void MulDivInt(int& n1, int& n2, int num, int div);

int TwipsToMetrics(int nT, int nLog);
int MetricsToTwips(int nR, int nLog);

BOOL IsDecimal(LPCTSTR strDecimal);
CString	SplitString(CString& str, char ch);
void SplitString(CString str, char ch, CStringArray &strarr);
BOOL SplitPath(LPCTSTR lpszPath, CStringArray& strArray);

template <class T>
void run(T* pData, int left, int right)
{
    int i,j;
    T middle, iTemp;
    i = left;
    j = right;
    // 下面的比较都调用我们重载的操作符函数
    middle = pData[(left+right)/2];  // 求中间值
    do{
        while((pData[i]<middle) && (i<right))// 从左扫描大于中值的数
            i++;           
        while((pData[j]>middle) && (j>left))// 从右扫描大于中值的数
            j--;

        if (i<=j) // 找到了一对值
        {
            // 交换
            iTemp = pData[i];
            pData[i] = pData[j];
            pData[j] = iTemp;
            i++;
            j--;
        }
    }while(i<=j); // 如果两边扫描的下标交错，就停止（完成一次）
	
    // 当左边部分有值(left<j)，递归左半边
    if (left < j)
        run(pData, left, j);
    // 当右边部分有值(right>i)，递归右半边
    if (right > i)
        run(pData, i, right);
}

template <class T>
void QuickSort(T* pData, int Count)
{
    run(pData, 0, Count-1);
}

BOOL APIENTRY PeekAndPump();

int SaveBitmapToFile(HBITMAP hBitmap, LPCTSTR lpFileName);
CString GetTempFileNameSK();
CString GetTempPathSK();
      
template<typename T>
inline
CString toString(LPCTSTR lpszFormat, T t)
{
	CString str;
	str.Format(lpszFormat, t);
	return str;
}

void FormatValue(CString& strValue, double fValue, int nPrecis);
#include <string>
using namespace std;
BOOL time_t2string(time_t t, string& strTime);
time_t  string2time_t(const string& strTime);

#endif
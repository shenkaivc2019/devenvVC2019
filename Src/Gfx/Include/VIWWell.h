#if !defined(AFX_VIWWELL_H__7B530346_B985_40DD_AAF0_69F013F1EFAE__INCLUDED_)
#define AFX_VIWWELL_H__7B530346_B985_40DD_AAF0_69F013F1EFAE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


// VIWWell.h : header file
//

class CXMLSettings;
/////////////////////////////////////////////////////////////////////////////
// CVIWWell window

class CVIWWell : public CWnd
{
// Construction
public:
	DECLARE_DYNCREATE(CVIWWell)
	virtual CString GetProgID()
	{
		CLSID clsid = GetClsid();
		LPOLESTR pstr=NULL;
        ProgIDFromCLSID(clsid, &pstr);
        CString sProgID = pstr;

		return sProgID;
	}

	virtual CString GetFilter()
	{
		CString strFilter;
		return strFilter;
	}

	virtual CLSID const& GetClsid()
	{
		static CLSID const clsid
			= { 0xbb875932, 0x1274, 0x4e95, { 0x8e, 0x40, 0x5b, 0xd3, 0x60, 0x7f, 0x6a, 0xb9 } };
		return clsid;
	}

	virtual BOOL Create(LPCTSTR lpszWindowName, DWORD dwStyle,
		const RECT& rect, CWnd* pParentWnd, UINT nID,
		CFile* pPersist = NULL, BOOL bStorage = FALSE,
		BSTR bstrLicKey = NULL);

// Attributes
public:
	virtual BOOL GetPagein() {return TRUE;}
	virtual void SetPagein(BOOL) {}

// Operations
public:
	virtual BOOL Save(LPCTSTR pszFile) {return TRUE;}
	virtual BOOL Load(LPCTSTR pszFile) {return TRUE;}
	virtual BOOL SetFont(long lFont, long lHeight, long lWeight, long lStyle, LPCTSTR pszFace) {return TRUE;}
	virtual CString GetFont(long lFont, long* lHeight, long* lWeight, long* lStyle) {return _T("");}
	virtual long WriteToBuffer(VARIANT* data) {return 0;}
	virtual long ReadFromBuffer(VARIANT* data) {return 0;}
	virtual long SetAddMode(long lAddMode) {return 0;}
	virtual long ReadFromXML(long pXML) {return 0;}
	virtual long CalcPageHeight() {return 2400;}

	virtual	void Draw(long pDC, long pRect) {}
	virtual long SetFonts(long pCFonts) {return 0;}

	virtual long SetUnits(LPDISPATCH lpUnits) {return 0;}
	virtual void DrawEx(CDC* pDC, LPRECT lpRect) { Draw((long)pDC, (long)lpRect); }
	virtual void SetCFonts(CFont* pFonts) { SetFonts((long)pFonts); }
	virtual BOOL ReadXML(CXMLSettings* pXML) { return ReadFromXML((long)pXML); }
	virtual long SetConfig(const VARIANT& varConfig) {return 0;}
	virtual long SetDoc(const VARIANT& varDoc) {return 0;}
	virtual void GetPageRect(LPRECT lpRect) {}

	virtual void AboutBox() {}
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VIWWELL_H__7B530346_B985_40DD_AAF0_69F013F1EFAE__INCLUDED_)

#if !defined(AFX_MUTITREECTRL_H__1298A0CF_BFBF_414E_A1E1_BA18D9B39FBB__INCLUDED_)
#define AFX_MUTITREECTRL_H__1298A0CF_BFBF_414E_A1E1_BA18D9B39FBB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// MutiTreeCtrl.h : header file
//
#ifndef BASECTRL_API
	#ifdef BASECTRL_EXPORT
		#define BASECTRL_API __declspec( dllexport )
	#else
		#define BASECTRL_API __declspec( dllimport )
	#endif
#endif

/////////////////////////////////////////////////////////////////////////////
// CMutiTreeCtrl window

class BASECTRL_API CMutiTreeCtrl : public CTreeCtrl
{
// Construction
public:
	CMutiTreeCtrl(UINT nBitmap = 0);

// Attributes
public:

// Operations
public:
	CString GetItemPath(HTREEITEM hItem);
	int GetCheckedItems(HTREEITEM hItem, CStringArray& strArray);
	BOOL CheckItem(HTREEITEM hItem, BOOL bCheck = TRUE)
	{
		if (hItem == TVI_ROOT)
		{
			HTREEITEM hChild = GetChildItem(hItem);
			for (;hChild != NULL;)
			{
				CheckItem(hChild, bCheck);
				hChild = GetNextSiblingItem(hChild);
			}
			
			return bCheck;
		}

		return SetItemState(hItem, INDEXTOSTATEIMAGEMASK(bCheck ? 3 : 1),
			TVIS_STATEIMAGEMASK);
	}

	BOOL IsItemChecked(HTREEITEM hItem)
	{
		int nState = GetItemState(hItem, TVIS_STATEIMAGEMASK) >> 12;
		return (nState == 3);
	}

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMutiTreeCtrl)
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL SetItemState( HTREEITEM hItem, UINT nState, UINT nStateMask, BOOL bSearch=TRUE);
	virtual ~CMutiTreeCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CMutiTreeCtrl)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnStateIconClick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKeydown(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

private:
	UINT m_uFlags;
	UINT m_nBitmap;
	CImageList	m_normalList;
	CImageList	m_stateList;
	void TravelSiblingAndParent(HTREEITEM hItem, int nState);
	void TravelChild(HTREEITEM hItem,int nState);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MUTITREECTRL_H__1298A0CF_BFBF_414E_A1E1_BA18D9B39FBB__INCLUDED_)

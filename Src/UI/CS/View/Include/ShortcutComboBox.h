#if !defined(AFX_SHORTCUTCOMBOBOX_H__5E0980DB_C74F_4EB0_B8FF_019724EAE672__INCLUDED_)
#define AFX_SHORTCUTCOMBOBOX_H__5E0980DB_C74F_4EB0_B8FF_019724EAE672__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// ShortcutComboBox.h : header file
#ifndef BASECTRL_API
	#ifdef BASECTRL_EXPORT
		#define BASECTRL_API __declspec( dllexport )
	#else
		#define BASECTRL_API __declspec( dllimport )
	#endif
#endif
/////////////////////////////////////////////////////////////////////////////
// CShortcutComboBox window

class BASECTRL_API CShortcutComboBox : public CComboBox
{
// Construction
public:
	CShortcutComboBox();

// Attributes
public:

// Operations
public:
	void SetWindowText(LPCTSTR lpszString);
	void GetWindowText(CString& rString);
	void GetLBText(int nIndex, CString& rString);
	int AddString(LPCTSTR lpszString);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShortcutComboBox)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CShortcutComboBox();

	// Generated message map functions
protected:
	CString m_strDesktop;

	CString ConvertInput(CString strInput);
	CString ConvertOutput(CString strOutput);

	//{{AFX_MSG(CShortcutComboBox)
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHORTCUTCOMBOBOX_H__5E0980DB_C74F_4EB0_B8FF_019724EAE672__INCLUDED_)

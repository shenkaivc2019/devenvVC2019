#if !defined(AFX_SCOPECURVEFIGUREDLG_H__F5B18181_553A_46DE_BB50_C3403F728352__INCLUDED_)
#define AFX_SCOPECURVEFIGUREDLG_H__F5B18181_553A_46DE_BB50_C3403F728352__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ScopeSectorProp.h"
// ScopeCurveFigureDlg.h : header file
//

#ifndef SCOPE_API
	#ifdef SCOPE_EXPORT
		#define SCOPE_API __declspec( dllexport )
	#else	
		#define SCOPE_API __declspec( dllimport )
	#endif
#endif
/////////////////////////////////////////////////////////////////////////////
// CScopeCurveFigureDlg dialog

//#ifdef _OLD_SCOPES
class SCOPE_API CScopeCurveFigureDlg : public CPropertyPage
{
	DECLARE_DYNCREATE(CScopeCurveFigureDlg)

// Construction
public:
	CScopeCurveFigureDlg();
	~CScopeCurveFigureDlg();

// Dialog Data
	//{{AFX_DATA(CScopeCurveFigureDlg)
	enum { IDD = IDD_SCOPE_CURVE_FIGURE };
	CComboBox	m_cbCurve;
	CListBox	m_lbSector;
	int		m_iFigureType;
	double	m_dStartPos;
	double	m_dEndPos;
	CMFCColorButton m_btnColor;
	CString	m_strName;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CScopeCurveFigureDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CScopeCurveFigureDlg)
	afx_msg void OnBtnColor();
	afx_msg void OnBtnCsadd();
	afx_msg void OnBtnCsdel();
	virtual BOOL OnInitDialog();
	afx_msg void OnRadioFigure();
	afx_msg void OnSelchangeComboCurve();
	afx_msg void OnSelchangeScurveList();
	afx_msg void OnBtnCsmdf();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void		InitSector();
	void		InitSectors();
	void		SelectSector(int nIndex);
	void        FlashScope();
public:
	CWnd*				m_pWnd;
	CPtrArray*			m_pCurvePropList;
	CPtrArray			m_SectorList;
	CScopeSectorProp*	m_pSector;
	int					m_nCurve;

};

//#endif
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCOPECURVEFIGUREDLG_H__F5B18181_553A_46DE_BB50_C3403F728352__INCLUDED_)

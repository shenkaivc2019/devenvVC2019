// ScopeSectorProp.h: interface for the CScopeSectorProp class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCOPESECTORPROP_H__5B647A6F_B5BA_46BB_8144_EFAF91829104__INCLUDED_)
#define AFX_SCOPESECTORPROP_H__5B647A6F_B5BA_46BB_8144_EFAF91829104__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ULCOMMDEF.H"

#ifndef SCOPE_API
	#ifdef SCOPE_EXPORT
		#define SCOPE_API __declspec( dllexport )
	#else	
		#define SCOPE_API __declspec( dllimport )
	#endif
#endif

class SCOPE_API CScopeSectorProp : public tagSCOPESECTOR
{
public:
	CScopeSectorProp();
	CScopeSectorProp(SCOPESECTOR initSector);
	virtual ~CScopeSectorProp();
	void FillInfo(BOOL bSaved = TRUE);
	
public:
	CString		m_strName;		// 标记段名称
//	CString		m_strCurve;		 关联曲线名
	BOOL		m_bDel;
};

#endif // !defined(AFX_SCOPESECTORPROP_H__5B647A6F_B5BA_46BB_8144_EFAF91829104__INCLUDED_)

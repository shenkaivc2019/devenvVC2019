#if !defined(AFX_SCOPECURVEGRIDDLG_H__79995664_215C_46F1_AB64_5FE5442D9C18__INCLUDED_)
#define AFX_SCOPECURVEGRIDDLG_H__79995664_215C_46F1_AB64_5FE5442D9C18__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ScopeCurveGridDlg.h : header file
//

#include "LineStyleCB.h"
#include <afxcolorbutton.h>
/////////////////////////////////////////////////////////////////////////////
// CScopeCurveGridDlg dialog
#ifndef SCOPE_API
	#ifdef SCOPE_EXPORT
		#define SCOPE_API __declspec( dllexport )
	#else	
		#define SCOPE_API __declspec( dllimport )
	#endif
#endif


//#ifdef _OLD_SCOPES
class SCOPE_API CScopeCurveGridDlg : public CPropertyPage
{
// Construction
public:
	CScopeCurveGridDlg();

	CWnd	*m_pWnd;
	UINT	m_nHBigStyle;
	UINT	m_nHBigColor;
	UINT	m_nHSmallStyle;
	UINT	m_nHSmallColor;
	UINT	m_nVBigStyle;
	UINT	m_nVBigColor;
	UINT	m_nVSmallStyle;
	UINT	m_nVSmallColor;
	UINT	m_nMKColor;
	UINT	m_nUnit;

// Dialog Data
	//{{AFX_DATA(CScopeCurveGridDlg)
//	enum { IDD = IDD_SCOPE_CURVE_GRID };
	CLineStyleCB	m_ctrlVSmallStyle;
	CLineStyleCB	m_ctrlVBigStyle;
	CLineStyleCB	m_ctrlHSmallStyle;
	CLineStyleCB	m_ctrlHBigStyle;
	CMFCColorButton	m_ctrlVSmallColor;
	CMFCColorButton	m_ctrlVBigColor;
	CMFCColorButton	m_ctrlMKColor;
	CMFCColorButton	m_ctrlHSmallColor;
	CMFCColorButton	m_ctrlHBigColor;
	BOOL	m_bHSGrid;
	BOOL	m_bHBGrid;
	BOOL	m_bVSGrid;
	BOOL	m_bVBGrid;
	BOOL	m_bMKShow;
	UINT	m_nHBigWidth;
	UINT	m_nHSmallWidth;
	UINT	m_nMKPos;
	UINT    m_nMKInterval;
	float	m_nMKWidth;
	UINT	m_nVBigWidth;
	UINT	m_nVSmallWidth;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CScopeCurveGridDlg)
	protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CScopeCurveGridDlg)
	afx_msg void RefreshData();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	int m_nScopeType;
};

//#endif 
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCOPECURVEGRIDDLG_H__79995664_215C_46F1_AB64_5FE5442D9C18__INCLUDED_)

#if !defined(AFX_SCOPECURVEAXESDLG_H__037BED68_1210_4743_9846_33172DEAB345__INCLUDED_)
#define AFX_SCOPECURVEAXESDLG_H__037BED68_1210_4743_9846_33172DEAB345__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ScopeCurveAxesDlg.h : header file
//
#include "RoundSliderCtrl.h"
//#include "FrequencyCtrl.h"
#include "ScopeCurveProp.h"
/////////////////////////////////////////////////////////////////////////////
// CScopeCurveAxesDlg dialog

#ifndef SCOPE_API
	#ifdef SCOPE_EXPORT
		#define SCOPE_API __declspec( dllexport )
	#else	
		#define SCOPE_API __declspec( dllimport )
	#endif
#endif


//#ifdef _OLD_SCOPES
class SCOPE_API CScopeCurveAxesDlg : public CPropertyPage
{
// Construction
public:
	int m_nYStyle;     // 0: 底部 1：顶部 2：居中
	CScopeCurveAxesDlg();
	CScopeCurveProp* m_pCurve;
	CPtrArray *m_pCurvePropList;
	int m_nStdCurveID;
	int m_nSel;
	int m_nSelCurve;
	CWnd *m_pWnd;
	float m_fYMaxValue;  // Y轴精确的最大值
	float m_fYShowValue; // 显示的Y轴的最大值
	float m_fThreadHold;
	BOOL  m_bCheckTread;
// Dialog Data
	//{{AFX_DATA(CScopeCurveAxesDlg)
//	enum { IDD = IDD_SCOPE_CURVE_AXES };
	CComboBox	m_ctrlYPos;
	CComboBox   m_ctrlXPos;
	CSliderCtrl	m_ctrlYOffset;
	CSliderCtrl	m_ctrlXOffset;
	CRoundSliderCtrl	m_ctrlYSlider;
	CRoundSliderCtrl	m_ctrlXSlider;
	int		m_nXPos;
	int		m_nYPos;
	int		m_nXOffset;
	int		m_nYOffset;
	CString	m_strUserDefine;
	int		m_nChannelCount;
	int		m_nValueStyle; // 0 无  1 自动  2 自定义 
	int		m_nYValueType;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CScopeCurveAxesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CScopeCurveAxesDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSelchangeCombo1();
	afx_msg void OnSelchangeCombo2();
	afx_msg void OnChangeUserDefine();
	afx_msg void OnChangeEditMuti();
	afx_msg void OnRadioAuto();
	afx_msg void OnRadioCust();
	afx_msg void OnRadioNull();
	afx_msg void OnKillfocusUserDefine();
	afx_msg void OnKillfocusThread();
	afx_msg void OnRadioLinear();
	afx_msg void OnRadioLog();
	afx_msg void OnCheckThread();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void Flash();

public:
	int  m_nScopeType;
	int  m_nOldPos;
};

//#endif
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCOPECURVEAXESDLG_H__037BED68_1210_4743_9846_33172DEAB345__INCLUDED_)

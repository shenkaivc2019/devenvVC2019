// ScopeCurveProp.h: interface for the CScopeCurveProp class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCOPECURVEPROP_H__FCC2C6A4_97CF_4398_9893_53FB5EC4ECC0__INCLUDED_)
#define AFX_SCOPECURVEPROP_H__FCC2C6A4_97CF_4398_9893_53FB5EC4ECC0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef SCOPE_API
	#ifdef SCOPE_EXPORT
		#define SCOPE_API __declspec( dllexport )
	#else	
		#define SCOPE_API __declspec( dllimport )
	#endif
#endif

class SCOPE_API CScopeCurveProp : public CObject
{
	DECLARE_SERIAL(CScopeCurveProp)
public:
	CScopeCurveProp();
	virtual	~CScopeCurveProp();
	virtual void	Serialize(CArchive& ar);

public:
	CString		m_strName;				// 曲线名称
	CString		m_strUnit;				// 曲线单位
	COLORREF	m_crColor;				// 曲线颜色
	double		m_fMinValue;			// 左值---下界
	double		m_fMaxValue;			// 右值---上界
	int			m_nStartPos;			// 起始位置，用于声波?
	int			m_nEndPos;				// 终止位置
	int			m_nChannelNum;			// 显示长度, 用于常规曲线
	int			m_nXOffset;				// X轴方向偏移量
	int			m_nYOffset;				// Y轴方向偏移量
	int			m_nRespective;			// 分辨率
	double		m_fXTimes;				// 沿X轴放大倍数
	double		m_fYTimes;				// 沿Y轴放大倍数
	BOOL		m_bShow;				// 是否显示
	BOOL        m_bShowLabel;           // 显示标识
	int			m_nLineStyle;			// 线型
	int			m_nLineWidth;			// 线宽
	int			m_nDimension;			// 曲线维数
	int			m_nFigureType;			// 曲线形状(线型、条型)
};

#endif // !defined(AFX_SCOPECURVEPROP_H__FCC2C6A4_97CF_4398_9893_53FB5EC4ECC0__INCLUDED_)

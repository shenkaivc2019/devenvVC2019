#if !defined(AFX_COMMONSCOPE_H__3C59C88A_508D_42A6_98D7_5A525314861E__INCLUDED_)
#define AFX_COMMONSCOPE_H__3C59C88A_508D_42A6_98D7_5A525314861E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CommonScope.h : header file
//
#include "ULCOMMDEF.H"
#include "ULOscilliscope.h"
/*#include "XPropertyPage.h"*/
/////////////////////////////////////////////////////////////////////////////
// CCommonScope window

typedef struct tagUserDlg
{
	UINT  nID;
	CWnd* pWnd;
}UserDlg;
typedef CArray<UserDlg, UserDlg> CUserDlgArray;

class CULKernel;
class CULTool;

class CCommonScope : public CWnd
{
	DECLARE_DYNCREATE(CCommonScope)
public:
	CCommonScope();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCommonScope)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCommonScope();

	// Generated message map functions
protected:
	//{{AFX_MSG(CCommonScope)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	virtual void SaveScopeConfig(LPCTSTR lpszFile = NULL);
	virtual	void InitHotKey();
	virtual void SetKernel(CULKernel* pKernel, CULTool* pULTool);
	virtual void SetCurve();
	virtual void BeginShowScope();
	virtual void InitScope(const SCOPEINFO& ScopeInfo);
	virtual void ScopeSettingInfo(int nOperate, LPVOID lpParam);
	virtual void ReinitScope();
public:
	CString        m_ToolName;
	CULTool* 	   m_pTool;   //仪器
	UINT		   m_nScopeType;
	CString		   m_strScopeName;
	int			   m_nJ;     // 示波器编号
	BOOL           m_bLoad;
	CPtrArray	   m_CurvePropList;
	CUserDlgArray  m_arUserDlg;  // 用户自定义对话框
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMMONSCOPE_H__3C59C88A_508D_42A6_98D7_5A525314861E__INCLUDED_)

#if !defined(AFX_SCOPECURVEMARKDLG_H__5F77A520_A1E0_40D2_9D0B_EC96BCD58648__INCLUDED_)
#define AFX_SCOPECURVEMARKDLG_H__5F77A520_A1E0_40D2_9D0B_EC96BCD58648__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ScopeCurveMarkDlg.h : header file
//

#include "ScopeMarkProp.h"

#ifndef SCOPE_API
	#ifdef SCOPE_EXPORT
		#define SCOPE_API __declspec( dllexport )
	#else	
		#define SCOPE_API __declspec( dllimport )
	#endif
#endif
/////////////////////////////////////////////////////////////////////////////
// CScopeCurveMarkDlg dialog

//#ifdef _OLD_SCOPES
#define UL_SCOPE_MARK_X      0
#define UL_SCOPE_MARK_HLINE  1
#define UL_SCOPE_MARK_RECT   2
#define UL_SCOPE_MARK_TRIA   3
#define UL_SCOPE_MARK_CROSS  4
#define UL_SCOPE_MARK_VLINE  5
#define UL_SCOPE_MARK_CIRCLE 6

#define UL_SCOPE_MARK_BRUSH  0
#define UL_SCOPE_MARK_NULL   1
#define UL_SCOPE_MARK_GRID   2

class SCOPE_API CScopeCurveMarkDlg : public CPropertyPage
{
public:
	CScopeCurveMarkDlg();
	virtual ~CScopeCurveMarkDlg();
// Construction
public:
	CPtrArray m_MarkList;
	CScopeMarkProp* m_pMark;

	void InitMark();
	void SelectMark(int nIndex);
	void RefreshData();
	void DrawRect();
	CWnd *m_pWnd;
	void Flash();
// Dialog Data
	//{{AFX_DATA(CScopeCurveMarkDlg)
//	enum { IDD = IDD_SCOPE_CURVE_MARK };
	CComboBox	m_ctrlLive;
	CComboBox	m_ctrlCtrl;
	CListBox	m_ctrlMarkList;
	CComboBox	m_ctrlStyle;
	CComboBox	m_ctrlFillStyle;
	CMFCColorButton	m_btnColour;
	BOOL	m_bShow;
	CString	m_strHigh;
	CString	m_strWidth;
	CString	m_strX;
	CString	m_strY;
	int		m_nJoinStyle;
	int		m_nActive;
	CString	m_strChannel;
	//}}AFX_DATA
	CPtrArray *m_pCurvePropList;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CScopeCurveMarkDlg)
	public:
	virtual BOOL OnSetActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CScopeCurveMarkDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnMarkAdd();
	afx_msg void OnMarkDelete();
	afx_msg void OnSelchangeListName();
	afx_msg void OnCheckShow();
	afx_msg void OnSelchangeComboFillstyle();
	afx_msg void OnSelchangeComboStyle();
	afx_msg void OnChangeEditHigh();
	afx_msg void OnChangeEditWidth();
	afx_msg void OnChangeEditX();
	afx_msg void OnChangeEditY();
	afx_msg void OnPaint();
	afx_msg void OnMarkJoin();
	afx_msg void OnSelchangeComboCtrl();
	afx_msg void OnSelchangeComboLive();
	afx_msg void OnRadioNull();
	afx_msg void OnRadioJing();
	afx_msg void OnRadioDong();
	afx_msg void OnChangeEditChannel();
	afx_msg void OnRadioDong2();
	afx_msg void OnBtnMkclr();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

//#endif 
#endif // !defined(AFX_SCOPECURVEMARKDLG_H__5F77A520_A1E0_40D2_9D0B_EC96BCD58648__INCLUDED_)

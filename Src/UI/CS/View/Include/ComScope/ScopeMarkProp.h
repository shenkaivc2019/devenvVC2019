// ScopeMarkProp.h: interface for the CScopeMarkProp class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCOPEMARKPROP_H__BEAE2E3E_64AF_456F_827C_265F0CEC0610__INCLUDED_)
#define AFX_SCOPEMARKPROP_H__BEAE2E3E_64AF_456F_827C_265F0CEC0610__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef SCOPE_API
	#ifdef SCOPE_EXPORT
		#define SCOPE_API __declspec( dllexport )
	#else	
		#define SCOPE_API __declspec( dllimport )
	#endif
#endif

class SCOPE_API CScopeMarkProp:public CObject
{
public:
	CScopeMarkProp();
	virtual ~CScopeMarkProp();
	void Serialize(CArchive& ar);
	CString m_strName;//标记名称
	int		m_nStyle;//标记样式
	COLORREF	m_crLineColor;//标记颜色
	int		m_nFillStyle;//添充方式
	int		m_nXPos;//标记横坐标
	int		m_nYPos;//标记纵坐标
	int		m_nWidth;//标记宽
	int		m_nHigh;//标记高
	BOOL	m_bShow;//标记显示
	
	int		m_nJoinStyle;//连接方式 0: 无; 2:全关联；1：Y轴关联 3 :X轴关联
	int		m_nLiveCurve;//标记Y方向关联的曲线
	int		m_nCtrlCurve;//标记X方向关联的曲线
	BOOL	m_bActive;//标记是否被选中
	int		m_nChannel;//标记所在的通道
	int		m_nXPlus;//标记X方向增益
	int		m_nYPlus;//标记Y方向增益
	int		m_nXCurPos;//标记在曲线上的位置
protected:
	DECLARE_SERIAL(CScopeMarkProp)
};

#endif // !defined(AFX_SCOPEMARKPROP_H__BEAE2E3E_64AF_456F_827C_265F0CEC0610__INCLUDED_)

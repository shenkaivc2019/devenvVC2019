#if !defined(AFX_RADIOSCOPE_H__DD6C5E66_E55C_47F6_BE0D_D563D4DC93A3__INCLUDED_)
#define AFX_RADIOSCOPE_H__DD6C5E66_E55C_47F6_BE0D_D563D4DC93A3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AcousticScope.h"
#include "ScopeCurveFigureDlg.h"

// RadioScope.h : header file
//

#ifdef _OLD_SCOPES
/////////////////////////////////////////////////////////////////////////////
// CRadioScope window
class CRadioScope : public CAcousticScope
{
	DECLARE_DYNCREATE(CRadioScope)
// Construction
public:
	CRadioScope();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRadioScope)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CRadioScope();

	// Generated message map functions
protected:
	//{{AFX_MSG(CRadioScope)
	afx_msg void OnPaint();
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:

	// ��д
	virtual void InitScope(CULTool *pTool);
	virtual void InitScope(const SCOPEINFO& ScopeInfo);
	virtual void SaveScopeConfig(SCOPEINFO& ScopeInfo);
	
	// ��ʾ
	virtual void DrawRadioWave(CDC* pDC);
	void	DrawDotWave(CDC* pDC, int nCurve, int nPoint);
	void	DrawLineWave(CDC* pDC, int nCurve, int nPoint);
	void	DrawBarWave(CDC* pDC, int nCurve, int nPoint, int nXAxePos);	
	
public:
	CScopeCurveFigureDlg	m_ScopeCurveFigureDlg;
	int		m_nBarWidth;
};

/////////////////////////////////////////////////////////////////////////////
#endif 

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RADIOSCOPE_H__DD6C5E66_E55C_47F6_BE0D_D563D4DC93A3__INCLUDED_)

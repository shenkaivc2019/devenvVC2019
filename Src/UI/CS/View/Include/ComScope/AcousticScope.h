#if !defined(AFX_ACOUSTICSCOPE_H__E22E54B5_00FD_11D4_8690_0050BABA4C66__INCLUDED_)
#define AFX_ACOUSTICSCOPE_H__E22E54B5_00FD_11D4_8690_0050BABA4C66__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SinWnd.h : header file
//

#include "ScopeCurveProp.h"
#include "ScopeCurvePropDlg.h"
#include "ScopeCurveAxesDlg.h"
#include "ScopeCurveGridDlg.h"
#include "ScopeCurveMarkDlg.h"
#include "CommonScope.h"
#include <XMLSettings.h>

#ifndef SCOPE_API
	#ifdef SCOPE_EXPORT
		#define SCOPE_API __declspec( dllexport )
	#else	
		#define SCOPE_API __declspec( dllimport )
	#endif
#endif

//#ifdef _OLD_SCOPES
/////////////////////////////////////////////////////////////////////////////
// CAcousticScope window
#define HOT_KEY_COUNT		32
#define MARK_LEFT			0
#define MARK_RIGHT			1
#define MARK_UP				2
#define MARK_DOWN			3 
#define ZOOM_X1				4
#define ZOOM_X10			5
#define ZOOM_X100			6
#define MARK_SELECT			7
#define CURVE_PRE			8
#define CURVE_NEXT			9
#define CURVE_LEFT			10
#define CURVE_RIGHT			11
#define CURVE_UP			12
#define CURVE_DOWN			13
#define CURVE_Y_ZOOM_IN		14
#define CURVE_Y_ZOOM_OUT	15
#define CURVE_X_ZOOM_IN		16
#define CURVE_X_ZOOM_OUT	17


UINT ShowThreadProc(LPVOID pParam);

class CDashLine;
class CULKernel;
class CULTool;
class SCOPE_API CAcousticScope : public CCommonScope
{
	DECLARE_DYNCREATE(CAcousticScope)
// Construction
public:
	CAcousticScope();

// Attributes
public:
	BOOL    Create(CWnd* pParentWnd);
	BOOL AttachScope(CWnd * wnd, UINT ID);
// Operations
public:
	virtual void InitScope(CULTool *pTool);
	void	Flash();
	void	DrawSinWave(CDC* pDC);
	virtual void InitScope(const SCOPEINFO& ScopeInfo);
	virtual void SaveScopeConfig(SCOPEINFO& ScopeInfo);	
	virtual void SetKernel(CULKernel* pULKernel, CULTool* pULTool);
	virtual void ScopeSettingInfo(int nOperate, LPVOID lpParam);
	void	SaveScopeConfig(LPCTSTR lpszFile = NULL);
	
	void	DrawExWave(CDC* pDC);
	void	DrawBack(CDC* pDC, CRect rc, int nY , int nChannel);
	void    DrawThread(CDC* pDC, CRect rc, int nYPos , int nChannel);//绘制门槛线函数
	BOOL	HitThreadCurve(CPoint point);//判断鼠标是否点中曲线
	float	MapValue(int nYpos);
	void	DrawMark(CDC *pDC,CRect rc);
	void    DrawSignatedCurve(CDC* pDC, CRect rc);
	void	BeginShowScope();
	void	Serialize(CArchive& ar);
	void	SetCurve();
	int		GetYPos(CRect rc, int channel, double max, double min ,double value);
	int		GetYPos(CRect rc, double max, double min, double value);
	int		GetPosByValue(CRect rc, CScopeCurveProp *pScopeCurveProp, double fMaxValue, double fValue, CScopeMarkProp *pScopeMarkProp = NULL, BOOL bChannel = TRUE);//TRUE使用pScopeCurveProp中的通道，FALSE使用pScopeMarkProp中的通道
	double	GetMaxValue();
	void	ComputeMark(int nMarkID, CRect rc);
	void	OutPutMaxValue(CDC *pDC, CRect rc);
	
	void	InitHotKey();
	CString	GetAppPath();
	void	SetDefaultHotKey();
	void	InitMap();
	DWORD	LookupKey(DWORD dwKey);
	CPen*	GetRightPen(CDashLine& DashLine, int nStyle, int nPenSize, COLORREF crColor);


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAcousticScope)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CAcousticScope();
	
	// Generated message map functions
protected:
	//{{AFX_MSG(CAcousticScope)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	afx_msg LRESULT OnDataArrived(WPARAM wp, LPARAM lp);
	DECLARE_MESSAGE_MAP()
public:
	int			m_nMode;	// = 1:Sin
							// = 2:e
	UINT        m_nTimerID;
	int         m_nTimerInterval;
	CULKernel*	m_pKernel;

	int			m_nFlash;
	CPtrArray   m_CurveList;
	int			m_nOpCurve;

	CScopeCurvePropDlg	m_ScopeCurvePropDlg;
	CScopeCurveAxesDlg	m_ScopeCurveAxesDlg;
	CScopeCurveGridDlg  m_ScopeCurveGridDlg;
	CScopeCurveMarkDlg  m_ScopeCurveMarkDlg;
	
	int			m_nRadio;
	POINT		Point [2048];
	POINT		ShowPoint[2048];
	BOOL		m_bRunShowThread;
	CEvent*		m_pShowEvent;
	DWORD		m_HotKey[HOT_KEY_COUNT];
	CMapStringToOb	m_KeyMap;
	CMapStringToOb	m_ValueMap;

	CWnd	*	m_pOldWnd;
	int			m_nZoom;
	float		m_fCurValue;
	int			m_nCurPos;
	CString		m_strCurName;
	int			m_nMaxPoint;
	COLORREF	m_crTextColor;
	float		m_fThreadValue;
	BOOL		m_bHitThread;
	float		m_fUnitPerPoint;//每个数据点对应的实际时间单位
};

//#endif
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SINWND_H__E22E54B5_00FD_11D4_8690_0050BABA4C66__INCLUDED_)

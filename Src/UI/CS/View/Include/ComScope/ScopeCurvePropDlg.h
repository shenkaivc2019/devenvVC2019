#if !defined(AFX_SCOPECURVEPROPDLG_H__E6CECFF6_570D_4D08_BE9C_917025856E37__INCLUDED_)
#define AFX_SCOPECURVEPROPDLG_H__E6CECFF6_570D_4D08_BE9C_917025856E37__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ScopeCurvePropDlg.h : header file
//
#include "ScopeCurveProp.h"
#include "LineStyleCB.h"
#include "ScopeCurveAxesDlg.h"
#include "ScopeCurveGridDlg.h"

#ifndef SCOPE_API
	#ifdef SCOPE_EXPORT
		#define SCOPE_API __declspec( dllexport )
	#else	
		#define SCOPE_API __declspec( dllimport )
	#endif
#endif
/////////////////////////////////////////////////////////////////////////////
// CScopeCurvePropDlg dialog

//#ifdef _OLD_SCOPES
class SCOPE_API CScopeCurvePropDlg : public CPropertyPage
{
// Construction
public:
	CScopeCurvePropDlg();
	CPtrArray	*m_pCurvePropList;//曲线属性列表
	CScopeCurveProp *m_pCurve;//当前曲线
	CScopeCurveAxesDlg* m_pScopeCurveAxesDlg;//坐标轴对话框
	CScopeCurveGridDlg* m_pScopeCurveGridDlg;
	CWnd		*m_pWnd;
	void		Flash();
	void		AddCurve(CScopeCurveProp* pCurve);
	void		SelectCurve(int nIndex);
	void		InitCurve();
	void		RefreshData();
	void		InitToolCurve();
	void		ResetCurve();
// Dialog Data
	//{{AFX_DATA(CScopeCurvePropDlg)
//	enum { IDD = IDD_SCOPE_CURVE_PROP };
	CLineStyleCB	m_btnLineStyle;
	CMFCColorButton	m_btnColor;
	CListBox	m_CurveList;
	CString	m_strUnit;
	BOOL	m_bShow;
	BOOL    m_bShow2;
	CString	m_strEndPos;
	CString	m_strLength;
	CString	m_strMaxValue;
	CString	m_strMinValue;
	CString	m_strRespective;
	CString	m_strStartPos;
	CString	m_strXOffset;
	CString	m_strXTimes;
	CString	m_strYOffset;
	CString	m_strYTimes; 
	CString	m_strLineWidth;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CScopeCurvePropDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CScopeCurvePropDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeList1();
	afx_msg void OnBtnColor();
	afx_msg void OnChangeEndPos();
	afx_msg void OnChangeLength();
	afx_msg void OnChangeMaxValue();
	afx_msg void OnChangeMinValue();
	afx_msg void OnChangeRespective();
	afx_msg void OnChangeStartPos();
	afx_msg void OnChangeUnit();
	afx_msg void OnChangeXOffset();
	afx_msg void OnChangeYOffset();
	afx_msg void OnShow();
	afx_msg void OnShow2();
	afx_msg void OnChangeXTimes();
	afx_msg void OnChangeYTimes();
	afx_msg void OnSelchangeLineStyle();
	afx_msg void OnChangeLineWidth();
	afx_msg void OnPaint();
	afx_msg void OnBtnPropertySave();
	afx_msg void OnBtnPropertyRead();
	afx_msg void OnDblclkScurveList();
	//}}AFX_MSG
	afx_msg LONG OnColorSelOk(UINT lParam, LONG wParam);
	DECLARE_MESSAGE_MAP()

public:
	int m_nScopeType;
};

//#endif
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCOPECURVEPROPDLG_H__E6CECFF6_570D_4D08_BE9C_917025856E37__INCLUDED_)

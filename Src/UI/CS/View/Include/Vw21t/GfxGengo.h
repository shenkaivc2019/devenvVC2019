//---------------------------------------------------------------------------//
// ファイル名：Gfxgengo.h
// 説明  ：元号取得関数の定義を行う。
// 会社名：AMANO
// 作成者：岩田
// 作成日：2001/07/03
// 備考　：なし
//---------------------------------------------------------------------------//
#if !defined(_GFXGENGO_H)
#define _GFXGENGO_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#undef AFX_DATA
#define AFX_DATA AFX_EXT_DATA

// 関数ﾌﾟﾛﾄﾀｲﾌﾟ定義
BOOL APIENTRY GfxGetGengo(CString& strGengo, int nGengoNo, CDbConnection* pDbConnection=NULL);
BOOL APIENTRY GfxGetGengo(CString& strGengo, int nGengoNo, CDbRecordset* psetGCMNPRM);
void APIENTRY GfxClearGengo();
BOOL APIENTRY GfxDateToJYear(long YMD, short& nGengoNo, short& nYear, CDbConnection* pDbConnection=NULL);
BOOL APIENTRY GfxDateToJYear(long YMD, short& nGengoNo, short& nYear, CDbRecordset* psetGCMNPRM);

#undef AFX_DATA
#define AFX_DATA

#endif // _GFXGENGO_H 

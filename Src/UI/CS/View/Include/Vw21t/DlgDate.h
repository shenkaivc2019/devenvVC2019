//---------------------------------------------------------------------------//
// ファイル名：DlgDate.h
// 説明  ：日付設定ﾀﾞｲｱﾛｸﾞｸﾗｽ
// 会社名：株式会社 システムプロダクツ
// 作成者：SPC	関口
// 作成日：1998/02/14
// 備考　：なし
//---------------------------------------------------------------------------//
#if !defined _CDLGDATE_H_
#define _CDLGDATE_H_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <Calendar.h>

#undef AFX_DATA
#ifdef _GET21T_BUILD
	#define AFX_DATA AFX_EXT_DATA
#else
	#define AFX_DATA AFX_DATA_IMPORT
#endif

class CDlgDate : public CDialog
{
// コンストラクション
public:
  CDlgDate(CWnd* pParent = NULL);   // 標準のコンストラクタ
  ~CDlgDate();

  CCalendar m_Calendar;

// API
public:
  void GetSelect(CExDate& dateSelect);
  void GetSelect(long& lDateSelect);
  void GetSelect(CExDate& dateFrom, CExDate& dateTo);
  void GetSelect(long& lDateFrom, long& lDateTo);
  void SetRange(long lDateStart, long lDateEnd, short nDays = 1);
  void SetRange(const CExDate& dateStart, const CExDate& dateEnd, short nDays=1);
  BOOL SetSelect(const CExDate& dateFrom, const CExDate& dateTo);
  BOOL SetSelect(const CExDate& dateSelect);
  BOOL SetSelect(long lDateFrom, long lDateTo = 0L); 

// Operations
public:
  void Initialize(int x, int y, const CExDate& dateStart, const CExDate& dateEnd, short nDays);
  void Initialize(int x, int y, long lDateStart, long lDateEnd, short nDays=1);

  POINT m_pointWin;

// オーバーライド
  // ClassWizard は仮想関数のオーバーライドを生成します。

  //{{AFX_VIRTUAL(CDlgDate)
  protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート
  //}}AFX_VIRTUAL

// インプリメンテーション
protected:

  // 生成されたメッセージ マップ関数
  //{{AFX_MSG(CDlgDate)
  virtual BOOL OnInitDialog();
  virtual void OnOK();
  //}}AFX_MSG
  DECLARE_MESSAGE_MAP()
};

#undef AFX_DATA
#define AFX_DATA


//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio は前行の直前に追加の宣言を挿入します。

inline void CDlgDate::GetSelect(CExDate& dateSelect)
{
  m_Calendar.GetSelect(dateSelect);
}

inline void CDlgDate::GetSelect(CExDate& dateFrom, CExDate& dateTo)
{
  m_Calendar.GetSelect(dateFrom, dateTo);
}

inline void CDlgDate::SetRange(const CExDate& dateStart, const CExDate& dateEnd, short nDays)
{
  m_Calendar.SetRange(dateStart, dateEnd, nDays);
}

inline BOOL CDlgDate::SetSelect(const CExDate& dateFrom, const CExDate& dateTo)
{
  return m_Calendar.SetSelect(dateFrom, dateTo);
}

inline BOOL CDlgDate::SetSelect(const CExDate& dateSelect)
{
  return m_Calendar.SetSelect(dateSelect);
}

#endif _CDLGDATE_H_

//---------------------------------------------------------------------------//
// ファイル名：GfxAxGear.h
// 説明  ：AxGear汎用関数ﾍｯﾀﾞｰﾌｧｲﾙ
// 会社名：株式会社 システムプロダクツ
// 作成者：SPC	関口
// 作成日：1998/02/14
// 備考　：なし
//---------------------------------------------------------------------------//
#if !defined(_GFXAXGEAR_H)
#define _GFXAXGEAR_H


#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#undef AFX_DATA
#define AFX_DATA AFX_EXT_DATA

// 2010.07.02 Ver1.00 改善連絡【UG100_5065】Start
// グリッドデータの表示フォーマット最大文字数
#define GFX21T_GRIDFORMAT_MAX (100)
// 2010.07.02 Ver1.00 改善連絡【UG100_5065】End

// 関数ﾌﾟﾛﾄﾀｲﾌﾟ定義
BOOL APIENTRY GfxIsGrid(LPCTSTR szClassName);
BOOL APIENTRY GfxIsGrid(CWnd* pwndGrid);
BOOL APIENTRY GfxIsEdit(LPCTSTR szClassName);
BOOL APIENTRY GfxIsEdit(CWnd* pwndEdit);
BOOL APIENTRY GfxIsComb(LPCTSTR szClassName);
BOOL APIENTRY GfxIsComb(CWnd* pwndComb);
BOOL APIENTRY GfxIsList(LPCTSTR szClassName);
BOOL APIENTRY GfxIsList(CWnd* pwndList);
BOOL APIENTRY GfxGetHGridSelect(CWnd* pwndGrid, int &iColMin, int &iColMax, int &iRowMin, int &iRowMax);
BOOL APIENTRY GfxHGridEditCopy(CWnd* pwndGrid);
BOOL APIENTRY GfxHGridEditUnderCopy(CWnd* pwndGrid);
BOOL APIENTRY GfxHGridEditInsert(CWnd* pwndGrid);
BOOL APIENTRY GfxHGridEditDelete(CWnd* pwndGrid);
BOOL APIENTRY GfxHGridCanInsDel(CWnd* pwndGrid);
void APIENTRY GfxHGridBtnCorrect(CWnd* pwndGrid);
WORD APIENTRY GfxRestoreGridBtnSize(CWnd* pwndGrid, int iOptionNo = -1);
void APIENTRY GfxSaveGridBtnSize(CWnd* pwndGrid, WORD wGridBtnSize, int iOptionNo = -1);
void APIENTRY GfxInitCombTime(CWnd* pwndComb, int iMode = CT_BEFORE_NEXT, CDbConnection* pConnection = NULL);
void APIENTRY GfxInitCombTime(CWnd* pwndComb, CDbRecordset* psetAATTPR1, int iMode = CT_BEFORE_NEXT);
void APIENTRY GfxInitCombTime(CWnd* pwndGrid, short nfld, int iMode = CT_BEFORE_NEXT, CDbConnection* pConnection = NULL);
void APIENTRY GfxInitCombTime(CWnd* pwndGrid, short nfld, CDbRecordset *psetAATTPR1, int iMode = CT_BEFORE_NEXT);
void APIENTRY GfxSetEditTime(CWnd* pwndComb, CWnd* pwndEdit, short nTime);
BOOL APIENTRY GfxGetEditTime(CWnd*  pwndComb, CWnd*  pwndEdit, short& nTime);
void APIENTRY GfxSetEditMinute(CWnd* pwndEdit, short nMinute);
void APIENTRY GfxSetEditMinute(CWnd* pwndEdit, long lMinute);
BOOL APIENTRY GfxGetEditMinute(CWnd*  pwndEdit, short &nMinute);
BOOL APIENTRY GfxGetEditMinute(CWnd* pwndEdit, long &lMinute);
void APIENTRY GfxEditFmtDefItem(CWnd* pwndEdit, int iType, int iTimeUnit = TIME_60UNIT, int iMode = DEFITEM_LONG);
void APIENTRY GfxGetFmtDefItem(CString& strFormat, int iType, int  iTimeUnit = TIME_60UNIT, int iMode = DEFITEM_LONG);
void APIENTRY GfxSetEditDefItem(CWnd* pwndEdit, long lData, int iType, int iTimeUnit);
void APIENTRY GfxGetEditDefItem(CWnd* pwndEdit, long &lData, int iType, int iTimeUnit);
void APIENTRY GfxGetEditDefItem(CWnd*  pwndEdit, short &nData, int iType, int iTimeUnit);
void APIENTRY GfxGetEditDefData(CWnd* pwndEdit, long &lData,  int iType, int iTimeUnit);
void APIENTRY GfxGetEditDefData(CWnd*  pwndEdit, short& nData, int iType, int iTimeUnit);
BOOL APIENTRY GfxCheckEditDefItem(CWnd* pwndEdit, int iType, int iTimeUnit, int iMode);
void APIENTRY GfxInitCombZone(CWnd* pwndComb, int iMode = CZ_ZONE_I);
void APIENTRY GfxInitCombZone(CWnd* pwndGrid, short nFld, int iMode = CZ_ZONE_I);

#undef AFX_DATA
#define AFX_DATA

#endif // _GFXAXGEAR_H

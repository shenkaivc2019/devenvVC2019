//---------------------------------------------------------------------------//
// ファイル名：Calendar.h
// 説明  ：ｶﾚﾝﾀﾞ制御ｸﾗｽ
// 会社名：株式会社 システムプロダクツ
// 作成者：SPC	関口
// 作成日：1998/02/14
// 備考　：なし
//---------------------------------------------------------------------------//
#ifndef _CALENDER_H
#define _CALENDER_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CCalendar : public CWnd
{
// Construction
public:
  CCalendar();

// Attributes
public:
  BOOL CCalendar::CheckSelect();

protected:
  short m_nMonthRowHeight;
  short m_nWeekRowHeight;
  short m_nRowHeight;
  int   m_nColWidth;

  CRect m_rcTitle;
  CRect m_rcDate;
  short m_nStartOfWeek;
  BOOL  m_bFocus;

  short m_nOffset;

  int   m_iMinYM;
  int   m_iMaxYM;

  short m_nDispYear;
  short m_nDispMonth;

  CExDate m_dateSelect;
  CExDate m_dateFocus;
  CExDate m_dateStart;
  CExDate m_dateEnd;
  short m_nDays;

// API
public:
  void SetRange(long dateStart, long dateEnd, short nDays = 1);
  BOOL SetSelect(long dateFrom, long dateTo = 0L);
  void GetSelect(CExDate& dateSelect);
  void GetSelect(CExDate& dateFrom, CExDate& dateTo);

// Operations
protected:
  BOOL DayToRect(int nDay, CRect& rect);
  short PointToDay(CPoint point);
  void OnMonthChange(BOOL bRedraw = TRUE);

protected:
  short GetYM(long ldate);
  short GetYM(short nYear, short nMonth);
  short GetYear(int iYM);
  short GetMonth(int iYM);
  short GetMonth(long lDate);
// Draw
protected:
  void DrawTitle(CDC* pDC, LPCRECT lprcTitle);


// Overrides
public:

// Implementation
public:
  virtual ~CCalendar();

  // Generated message map functions
protected:
  //{{AFX_MSG(CCalendar)
  afx_msg void OnPaint();
  afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
  afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
  afx_msg void OnSetFocus(CWnd* pOldWnd);
  afx_msg void OnKillFocus(CWnd* pNewWnd);
  afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
  afx_msg UINT OnGetDlgCode();
  afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
  //}}AFX_MSG
  DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

inline void CCalendar::GetSelect(CExDate& dateSelect)
{
  dateSelect = m_dateFocus;
}


#endif // _CALENDER_H

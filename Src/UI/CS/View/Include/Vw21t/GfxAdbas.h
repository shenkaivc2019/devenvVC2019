//---------------------------------------------------------------------------//
// ファイル名：GfxAdbas.h
// 説明  ：就業ﾃﾞｰﾀﾍﾞｰｽﾌｧｲﾙ(勤務ｽｹｼﾞｭｰﾙ)関数ﾍｯﾀﾞｰﾌｧｲﾙ
// 会社名：株式会社 システムプロダクツ
// 作成者：SPC	関口
// 作成日：1998/02/14
// 備考　：なし
//---------------------------------------------------------------------------//
#if !defined(_GFXADBAS_H)
#define _GFXADBAS_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#undef AFX_DATA
#define AFX_DATA AFX_EXT_DATA

// 関数ﾌﾟﾛﾄﾀｲﾌﾟ定義
void APIENTRY GfxReadSchPayPeriod(short anPayPeriodTable[12], CDbConnection* pConnection = NULL);
void APIENTRY GfxReadSchPayPeriod(short anPayPeriodTable[12], CDbRecordset *psetSchedulePrm2);
long APIENTRY GfxGetProcSchYM(CDbConnection* pConnection = NULL);
long APIENTRY GfxGetProcSchYM(CDbRecordset *psetSchedulePrm2);
long APIENTRY GfxGetProcSchYM(long lDate, CDbConnection* pConnection = NULL);
long APIENTRY GfxGetProcSchYM(long lDate, CDbRecordset *psetSchedulePrm2);
void APIENTRY GfxGetSchPayPeriod(long lProcYM, long &lStartDay, long &lEndDay, CDbConnection* pConnection = NULL);
void APIENTRY GfxGetSchPayPeriod(long lProcYM, long &lStartDay, long &lEndDay, CDbRecordset *psetSchedulePrm2);
// 2002.12.11 機能改善【A4_018】Start
long APIENTRY GfxGetSchStartDay(CDbConnection* pConnection = NULL);
long APIENTRY GfxGetPreSchStartDay(CDbConnection* pConnection = NULL);
// End

#undef AFX_DATA
#define AFX_DATA

#endif // _GFXADBAS_H 

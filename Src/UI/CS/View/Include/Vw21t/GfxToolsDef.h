//---------------------------------------------------------------------------//
// ファイル名:	GfxToolsDef.h
// 説明:	Gfx関数ヘッダー
// 会社名:	株式会社システムプロダクツ
// 作成者:	関口
// 作成日:	1999/02/03
// 備考:	2000/03/07	DC関連define追加
//---------------------------------------------------------------------------//
#if !defined(_GFXTOOLSDEF_H)
#define _GFXTOOLSDEF_H

// GfxToolsDef.h : 共通定義
//

/////////////////////////////////////////////////////////////////////////////
// 個人／所属コードの桁数

// 構造体
struct US_CMNDIGITS
{
  int nEmpCode;                   // 個人コード桁数
// 2010.04.28 Ver1.00 機能変換【UG1_003】Start
//  int anDepCode[DEPCLASS_MAX];    // 所属コード桁数　　（会大中小順）
//  int alDepCompare[DEPCLASS_MAX]; // 所属コード比較割数（会大中小順）
//  int nDepCodeIn;                 // 所属コード入力階層（最下層、大中小）
	int anDepCode[DEPCLASS_MAX];    // 所属コード桁数　　（会1階層～10階層順）
	int nDepCodeIn;                 // 所属コード入力階層（会1階層～10階層）
// 2010.04.28 Ver1.00 機能変換【UG1_003】End
};

// 2010.04.28 Ver1.00 機能変換【UG1_003】Start
struct CMNPRM_DEPCLASS
{
	int anCodeDigits[NOCMP_DEPCLASS_MAX];     // 階層桁数
	TCHAR aszClassName[NOCMP_DEPCLASS_MAX][DEPNAME_MAX];  // 階層名称
};

struct DEPMENT_DEPCODE
{
	TCHAR szCompanyCode[COMPANYCODE_DIGITS + 1];		// 会社コード
	TCHAR aszClassCode[NOCMP_DEPCLASS_MAX][NOCMP_DEPCODE_DIGITS + 1];	// 階層コード
	short nDepClass;			// 所属階層
};
// 2010.04.28 Ver1.00 機能変換【UG1_003】End

/////////////////////////////////////////////////////////////////////////////
// AxGear 汎用関数

// 日付選択 (CVHComb, CVHList)
/*
#define WDG_SEL_YYMM    "%04d 年 %02d 月"   // 処理年月
#define WDG_SEL_BONUS   "%04d 年 %s"        // 対象賞与
#define WDG_SEL_YY      "%04d 年"           // 処理年
*/

// 日付入力 (CVHEdit)
/*
#define WDG_FMT_YYMMDD  "yyyy/mm/dd"      // 西暦年月日 (Date, long Date)
#define WDG_FMT_YYMM    "yyyy/mm"         // 西暦年月　 (Date, long Date)
#define WDG_FMT_MMDD    "mm/dd"           // 月日　　　 (Date, long Date)
#define WDG_FMT_YY      "####"            // 年　　　　 (Number, int)
#define WDG_FMT_MM      "##"              // 月　　　　 (Number, int)
#define WDG_FMT_DD      "##"              // 月　　　　 (Number, int)
#define WDG_FMT_HHMM    "#0:00"           // 時刻　　　 (Number, int)
*/

// ドロップリストの高さ (CVHComb)
/*
#define CMB_DROP_HEIGHT01   24
#define CMB_DROP_HEIGHT02   44
#define CMB_DROP_HEIGHT03   62
#define CMB_DROP_HEIGHT04   82
#define CMB_DROP_HEIGHT05   100
#define CMB_DROP_HEIGHT06   120
#define CMB_DROP_HEIGHT07   138
#define CMB_DROP_HEIGHT08   158
#define CMB_DROP_HEIGHT09   176
#define CMB_DROP_HEIGHT10   196
*/

// ボタンの幅と高さ (CVHGrid)
#define GRD_BTN_NONE        0
#define GRD_BTN_WIDTH       0x0001
#define GRD_BTN_HEIGHT      0x0002
#define GRD_COL_WIDTH       0x0004
#define GRD_ROW_HEIGHT      0x0008
#define GRD_BTN_ALL         0x000F 

// 時刻
#define CT_BEFORE_TODAY       1
#define CT_BEFORE_NEXT        2
#define CT_BEFORE_TODAY_TNONE 4
#define CT_BEFORE_NEXT_TNONE  5

// 自由定義項目　変数型別桁 (CVHEdit)
#define DEFITEM_SHORT     5         // 共通 short (Number, int or double)
#define DEFITEM_LONG_S    6         // 就業 long  (Number, int or double)
#define DEFITEM_LONG      8         // 給与 long  (Number, int or double)
// 2004.10.14 Ver4.31 機能改善【P5_018】Start
#define DEFITEM_LONG_L    9         // 給与 long  (Number, int or double)
// End

// 時間帯
#define CZ_ZONE_I       9
#define CZ_BREAK        10

/////////////////////////////////////////////////////////////////////////////
// アプリケーション起動／終了時の処理

// プログラム種類

#define PGTYPE_OTHER      0         // その他ＰＧ
#define PGTYPE_MENU       1         // 処理メニュー
#define PGTYPE_PGEXE      2         // 起動状況管理
#define PGTYPE_COMMU      3         // 通信関連
#define PGTYPE_INDMAS     4         // 個人マスター保守関連
#define PGTYPE_OTHMAS     5         // その他マスター保守関連
#define PGTYPE_PARAM      6         // パラメータ保守関連
#define PGTYPE_DC		  7			// DC関連
#define PGTYPE_STOP       999       // 起動禁止

// オプション№

#define EXEI_OPTIONNO_NONE     0        // none code
#define EXEI_OPTIONNO_MIN      1        // min. code
#define EXEI_OPTIONNO_MAX      99       // man. code

#define EXEI_OPTIONNO_CATALOG  10       // catalog start no
#define EXEI_OPTIONNO_CATALOG2 20       // catalog start no
#define EXEI_OPTIONNO_REPORT   90       // report  start no


// 自動起動

#define EXEI_AUTO_OFF     0         // 手動起動
#define EXEI_AUTO_ON      1         // 自動起動


// 構造体

struct US_EXECUTEINFO
{
  int   nOptionNo;           // オプション№
  int   nOperaterLevel;      // オペレータレベル
  TCHAR aszEmpCode[11];      // オペレータコード(Long->String)
  int   nMenuNo;             // メニューNo.    (新規追加)
  int   nSubMenuNo;          // サブメニューNo.(新規追加)
  int   nHandlingLevel;      // 操作レベル     (新規追加)
  BOOL  bAutoFlag;           // 自動起動
  TCHAR aszProgramID[4][9];  // 次起動プログラム(2->1～4)
  int   nNextOption[4];      // 次起動プログラムオプション      (新規追加)
  BOOL  bNextAutoFlag[4];    // 次起動プログラム自動起動フラグ  (新規追加)
};

// 日付指定

#define DEFDATE_TYPE_APP    0         // アプリケーション固有
#define DEFDATE_TYPE_NONE   1         // 初期値なし
#define DEFDATE_TYPE_DAY    2         // 日付初期値指定
#define DEFDATE_TYPE_TERM   3         // 期間初期値指定


// 基準日

#define DEFDATE_POINT_YESTERDAY 0         // 前日
#define DEFDATE_POINT_TODAY     1         // 当日
#define DEFDATE_POINT_WEEK      2         // 前回指定曜日
#define DEFDATE_POINT_TOMORROW  100       // 翌日
#define DEFDATE_POINT_NEXTMONTH 101       // 翌月
#define DEFDATE_POINT_NEXTWEEK  102       // 次回指定曜日


// 期間指定

#define DEFDATE_TERM_DAYS       0         // 日数指定
#define DEFDATE_TERM_PAYPERIOD  1         // 締日指定
#define DEFDATE_TERM_CALENDAR   2         // 暦日指定


// 構造体

struct US_DEFDATESETUP
{
  int   nDateType;
  int   nPoint;
  int   nTerm;
  int   nDays;
  int   nPayPeriod;
};


// 日付入力モード

#define INPUT_DATE_NONE      0         // なし
#define INPUT_DATE_YMD       1         // 処理日
#define INPUT_DATE_SEYMD     2         // 処理期間（処理日）
#define INPUT_DATE_YM        3         // 処理年月
#define INPUT_DATE_SEYM      4         // 処理期間（処理年月）
#define INPUT_DATE_BONUSNO   5         // 対象賞与
#define INPUT_DATE_SEBONUSNO 6         // 対象期間（対象賞与）
#define INPUT_DATE_YEAR      7         // 処理年
#define INPUT_DATE_SEYEAR    8         // 処理期間（処理年）


// システム種別

#define DISP_EMPS_DUMMY     -1        // なし
#define DISP_EMPS_NONE      0         // 就業対象者か給与対象者
#define DISP_EMPS_ATT       1         // 就業対象者
#define DISP_EMPS_PAY       2         // 給与対象者
#define DISP_EMPS_CMN       9         // 全個人

// 構造体
// 現行関数移行の為に残しています。
struct US_EMPSELINFO
{
  int   nOperaterLevel;         // オペレータレベル
  char  szEmpCode[11];          // オペレータコード
  int   nInputDate;             // 日付入力モード
  long  lDefStart;              // 開始初期値
  long  lDefEnd;                // 終了初期値
  long  lFirst;                 // 入力有効範囲
  long  lLast;                  // 入力有効範囲
  int   nPeriod;                // 入力有効期間
  int   nSysFlag;               // システム種別
  BOOL  bRetire;                // 退職処理関連
  BOOL  bTaxRegulation;         // 年末調整関連
  BOOL  bInsurance;             // 社会保険関連
  BOOL  bReportTempTeam;        // 標準出力指定（帳票）テンポラリファイル期間分作成
};

// Get21用処理日選択情報
struct US_DATESELINFO
{
  int   nInputDate;             // 日付入力モード
  long  lDefStart;              // 開始初期値
  long  lDefEnd;                // 終了初期値
  long  lFirst;                 // 入力有効範囲
  long  lLast;                  // 入力有効範囲
  int   nPeriod;                // 入力有効期間
};

// 切換指定

#define SELEMP_FIRST      0         // 先頭個人
#define SELEMP_PREV       1         // 前個人
#define SELEMP_NEXT       2         // 次個人
#define SELEMP_LAST       3         // 末尾個人


// 指定可能範囲

#define EMP_FILE_NONE     0         // 不可
#define EMP_FILE_PREV     0x0001    // 前個人　先頭個人　可能
#define EMP_FILE_NEXT     0x0002    // 次個人　末尾個人　可能

/////////////////////////////////////////////////////////////////////////////
// 共通データベースファイルの処理（就業／給与を含む）

// 処理可能期間算出のモード
#define SAVE_PERIOD_ERR     0         // エラーデータ保存期間
#define SAVE_PERIOD_DAILY   1         // デイリーデータ保存期間
#define SAVE_PERIOD_MONTH   2         // 就業台帳保存期間

// 集計期間期間算出
#define PREVIOUES_YM      0         // 前月
#define CURRENT_YM        1         // 当月
#define NEXT_YM           2         // 翌月

// 構造体
struct US_SUM_PERIOD
{
  long  alProcYM[NEXT_YM + 1];          // 前月処理年月（yyyymm）
  long  alProcStartDay[NEXT_YM + 1];    // 前月開始日（yyyymmdd）
  long  alProcEndDay[NEXT_YM + 1];      // 前月終了日（yyyymmdd）
};

// 構造体
struct US_SUM_PERIOD_ATT
{
  US_SUM_PERIOD agPeriod[ATTTYPE_MAX];
};

// 指定処理年月の集計期間算出

// 構造体
struct ACCUMU_PERIOD
{
  long  lStartDay;              // 集計開始日（yyyymmdd）
  long  lEndDay;                // 集計終了日（yyyymmdd）
};

// 構造体
struct ACCUMU_ATT_PERIOD
{
  ACCUMU_PERIOD agPeriod[ATTTYPE_MAX];
};

/////////////////////////////////////////////////////////////////////////////
// 就業データベースファイルの処理

// テーブルパターン
#define TABLE_PATTERN_A   0
#define TABLE_PATTERN_B   1

/////////////////////////////////////////////////////////////////////////////
// 就業（勤務スケジュール）データベースファイルの処理

// 処理可能期間算出のモード

#define SAVE_PERIOD_SCHDAILY  0         // デイリーデータ保存期間
#define SAVE_PERIOD_MHPLAN    1         // 投入人時（予定）データ保存期間
#define SAVE_PERIOD_MHRSLT    2         // 投入人時（実績）データ保存期間

#endif //_GfXTOOLSDEF_H 

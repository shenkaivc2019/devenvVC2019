#pragma once
// SplitterControl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSplitterControl window
#define SPN_SIZED				(WM_USER + 0x1000)
#define CW_LEFTALIGN			1
#define CW_RIGHTALIGN			2

#define SPLITTER_WIDTH			3
#define SPLITTERCTRL_WIDTH		SPLITTER_WIDTH

typedef struct tagSPC_NMHDR
{
	NMHDR hdr;
	int nCoordinate;
} SPC_NMHDR;

class CSplitterControl : public CStatic
{
// Construction
public:
	CSplitterControl();

// Attributes
public:

protected:
	BOOL		m_bIsPressed;
	int			m_nMin;
	int			m_nMax;

	HCURSOR		m_hCursor;

// Operations
public:

// Overrides

// Implementation
public:
	void ChangeWidth(CWnd* pWnd, int nCoordinate, DWORD dwFlag = CW_LEFTALIGN);

public:
	void		SetRange(int nMin, int nMax);

	void		Create(DWORD dwStyle, const CRect& rect, CWnd* pParent, UINT nID);
	virtual		~CSplitterControl();

	BOOL DoKeyboardSplit();

	// Generated message map functions
protected:
	afx_msg void	OnPaint();
	afx_msg void	OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void	OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void	OnLButtonUp(UINT nFlags, CPoint point);

	DECLARE_MESSAGE_MAP()
};

//---------------------------------------------------------------------------//
// ファイル名：GfxAdba.h
// 説明  ：就業ﾃﾞｰﾀﾍﾞｰｽﾌｧｲﾙ関数ﾍｯﾀﾞｰﾌｧｲﾙ
// 会社名：株式会社 システムプロダクツ
// 作成者：SPC	関口
// 作成日：1998/02/14
// 備考　：なし
//---------------------------------------------------------------------------//
#if !defined(_GFXADBA_H)
#define _GFXADBA_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <rfxdb.h>					// DB共通宣言
#include <Taatlidf.h>				// 就業台帳項目設定
#include <Taindivi.h>				// 個人マスター（就業）
#include <Tgdepmnt.h>				// 所属マスター

#undef AFX_DATA
#define AFX_DATA AFX_EXT_DATA

// 関数ﾌﾟﾛﾄﾀｲﾌﾟ定義
BOOL APIENTRY GfxCheckStore(CDbConnection* pConnection = NULL);
// 2009.05.13 Ver7.10 機能変更【G71_001】Start
void APIENTRY GfxGetItemPercent2(int nNo, long* plDefItem, LPTS_AATLIDF pgAttLdgItemDef, short nPercentCorrect);
// 2009.05.13 Ver7.10 機能変更【G71_001】End
void APIENTRY GfxGetItemPercent(int iNo, long *alDefItem, LPTS_AATLIDF aAttLdgItemDef, short nPercentCorrect);
void APIENTRY GfxGetItemPercent(int iNo, double *adDefItem, LPTS_AATLIDF aAttLdgItemDef, short nPercentCorrect);
long APIENTRY GfxGetItemPercent(short nType, short nNumeratorType, short nDenominatorType, long lNumeratorData, long lDenominatorData, short nPercentCorrect);
double APIENTRY GfxGetItemPercent(short ntype, short nNumeratorType, short nDenominatorType,
								  double dNumeratorData, double dDenominatorData, short nPercentCorrect);
int APIENTRY GfxGetTableNo(long lYMD, long lTableA, long lTableB);
void APIENTRY GfxGetSendingLocationNo(short anLocationNo[], LPTS_AINDIVI pgIndividMasAtt,
									  LPTS_GDEPMNT pgDepartmentMas, int iObjectModel,
									  BOOL bClientCheck = TRUE, CDbConnection* pConnection = NULL);
void APIENTRY GfxGetSendingLocationNo(short anLocationNo[], LPTS_AINDIVI pgIndividMasAtt,
									  LPTS_GDEPMNT pgDepartmentMas, int iObjectModel,
									  BOOL bClientCheck, CDbRecordset* psetCommuPrm2);
BOOL APIENTRY MfxModelCheck(short nModelNo, int iObjectModel);
// 2002.10.21 Ver4.00 FCR【T20261】Start
void APIENTRY GfxGetSendingLocationNoVal(short anLocationNo[], LPTS_AINDIVI pgIndividMasAtt,
									  LPTS_GDEPMNT pgDepartmentMas, int iObjectModel,
									  BOOL bClientCheck = TRUE, CDbConnection* pConnection = NULL);
void APIENTRY GfxGetSendingLocationNoVal(short anLocationNo[], LPTS_AINDIVI pgIndividMasAtt,
									  LPTS_GDEPMNT pgDepartmentMas, int iObjectModel,
									  BOOL bClientCheck, CDbRecordset* psetCommuPrm2);
BOOL APIENTRY MfxModelCheckVal(short nModelNo, int iObjectModel);
// End

#undef AFX_DATA
#define AFX_DATA

#endif // _GFXADBA_H 

//---------------------------------------------------------------------------//
// ファイル名：GfxAdb.h
// 説明  ：共通ﾃﾞｰﾀﾍﾞｰｽﾌｧｲﾙ関数ﾍｯﾀﾞｰﾌｧｲﾙ
// 会社名：株式会社 システムプロダクツ
// 作成者：SPC	関口
// 作成日：1998/02/14
// 備考　：なし
//---------------------------------------------------------------------------//
#if !defined(_GFXADB_H)
#define _GFXADB_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#undef AFX_DATA
#define AFX_DATA AFX_EXT_DATA

// 関数ﾌﾟﾛﾄﾀｲﾌﾟ定義
long APIENTRY GfxGetProcYM(short anPayPeriodTable[], long lDate);
void APIENTRY MfxGetProcYM(short anPayPeriodTable[], long& lPrevProcYM, long& lCurrentProcYM);
void APIENTRY MfxGetPayPeriod(short anPayPeriodTable[12], long lProcYM, long& lStartDay, long& lEndDay);
long APIENTRY GfxGetKeepTerm(int iMode, CDbConnection* pConnection = NULL);
long APIENTRY GfxGetKeepTermJOB(int iMode, CDbConnection* pConnection = NULL);
void APIENTRY GfxGetSumPeriod(US_SUM_PERIOD_ATT* pgSumPeriod, CDbConnection* pConnection = NULL);
void APIENTRY GfxGetSumPeriod(US_SUM_PERIOD_ATT* pgSumPeriod, CDbRecordset* psetAttPrm2);
void APIENTRY GfxGetSumPeriodJOB(US_SUM_PERIOD_ATT* pgSumPeriod, CDbConnection* pConnection = NULL);
void APIENTRY GfxGetSumPeriodJOB(US_SUM_PERIOD_ATT* pgSumPeriod, CDbRecordset* psetJobPr2);
void APIENTRY GfxGetAccPeriod(long lProcYM, ACCUMU_ATT_PERIOD* pgAccPeriod, CDbConnection* pConnection = NULL);
void APIENTRY GfxGetAccPeriod(long lProcYM, ACCUMU_ATT_PERIOD* pgAccPeriod, CDbRecordset* psetAttPrm2);
void APIENTRY GfxGetAccPeriodJOB(long lProcYM, ACCUMU_ATT_PERIOD* pgAccPeriod, CDbConnection* pConnection = NULL);
void APIENTRY GfxGetAccPeriodJOB(long lProcYM, ACCUMU_ATT_PERIOD* pgAccPeriod, CDbRecordset* psetDajobpr2);
void APIENTRY GfxDateToText(CString& strText, long lDate, short nFormat, CDbRecordset* psetGCMNPRM);
void APIENTRY GfxDateToText(CString& strText, long lDate, short nFormat, CDbConnection* pDbConnection=NULL);
void APIENTRY GfxTimeToText(CString& strText, short nTime, short nFormat, CDbRecordset* psetAATTPR1);
void APIENTRY GfxTimeToText(CString& strText, short nTime, short nFormat, CDbConnection* pDbConnection=NULL);
// 2004.05.20 Ver4.30 機能改善【G5_005】対応 Start
long APIENTRY GfxWriteSrvError(LPCTSTR lpszProgramID, short nErrType, LPCTSTR lpszEmpCode, LPCTSTR lpszMessage, CDbConnection* pConnection);
long APIENTRY GfxWriteSrvError(LPCTSTR lpszProgramID, short nErrType, CStringArray* parrEmpCode, CStringArray* parrMessage, CDbConnection* pConnection);
long APIENTRY GfxWriteErrorList(LPCTSTR lpszProgramID, LPCTSTR lpszErrorListName, CStringArray* parrEmpCode, CStringArray* parrMessage, CDbConnection* pConnection);
// End
#undef AFX_DATA
#define AFX_DATA

#endif // _GFXADB_H 

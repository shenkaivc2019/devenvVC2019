﻿#if !defined(AFX_WIZARDSTEPONE_H__9A8873FF_D596_4DA6_8146_5195030D3E64__INCLUDED_)
#define AFX_WIZARDSTEPONE_H__9A8873FF_D596_4DA6_8146_5195030D3E64__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WizardStepOne.h : header file
//
#ifndef BASEINFO_API
	#ifdef BASEINFO‌_EXPORT
		#define BASEINFO_API __declspec( dllexport )
	#else	
		#define BASEINFO_API __declspec( dllimport )
	#endif
#endif

#include "ShortcutComboBox.h"
#include "WPropertyPage.h"
#include "ColorGroupBox.h"
/////////////////////////////////////////////////////////////////////////////
// CWizardStepOne dialog
class CProjectInfo;
class BASEINFO_API CWizardStepOne : public CWPropertyPage
{
// Construction
public:
	CWizardStepOne(CWnd* pParent = NULL);   // standard constructor
	CProjectInfo* m_pInfo;
// Dialog Data
	//{{AFX_DATA(CWizardStepOne)
	// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_WIZARD_STEP1 };
#endif
//	CString	m_strWellName;
//	CBCGPDateTimeCtrl	m_wndDateTimePicker1;
//	CBCGPDateTimeCtrl	m_wndDateTimePicker2;
//	long	m_lDepth1;
//	long	m_lDepth0;
//	CString	m_strTeam;
	CString	m_strProjectClass;
	CString	m_strCreateTime;
	CString	m_strProjectDepart;
	CString	m_strProjectID;
	CString	m_strProjectMemo;
	CString	m_strProjectName;
	CString	m_strSvrCompany;
	CString	m_strOilCompany;
	CString	m_strOilCompanyRep1;
	CString	m_strOilCompanyRep2;
	CString	m_strSvrCompanyRep1;
	CString	m_strSvrCompanyRep2;
	//}}AFX_DATA

	CString	m_strNorthRef;
	CString m_SysDeepref;
	CString m_OlioComPany;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWizardStepOne)
	public:
	virtual BOOL OnSetActive();
	virtual BOOL OnKillActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CWizardStepOne)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnButtonProjectPath();
	afx_msg void OnProjectPathSelChange();
	afx_msg void OnWellNameEditChange();
// 2020.2.18 Ver1.6.0 TASK【002】 Start
//	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButtonServiceItem();
	afx_msg void OnProjectPathEditChange();
	afx_msg void OnWellNameSelChange();
	afx_msg void OnWellNameCloseUp();
	afx_msg void OnProjectPathCloseUp();
	afx_msg void OnKillfocusEditStep1Team();
	afx_msg void OnDatetimepicker1();
	afx_msg void OnDatetimepicker2();
	afx_msg void OnBrowseProjectName();
	afx_msg void OnEditchangeCombo3();
// 2020.2.18 Ver1.6.0 TASK【002】 End
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void	GetData();
	void	SetProjectPath(BOOL bWellNameSelected);
//	void	SetDateTimeState();
//	void	CreateMultiDir(LPCTSTR strFilename);
//	BOOL	CheckInput();	//检查数据的有效性

public:
//	CTime			m_dateCreateProject;
//	CTime			m_timeCreateProject;
	CString			m_strProjectPath;
//	CStringArray	m_arrayServiceItem;		// 服务项目
	CShortcutComboBox m_ShortcutCombo;
	CString			m_strFolderPath;
//	UINT m_nTimer;	//用于选择工程路径历史记录
	CColorGroupBox m_groupBox;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WIZARDSTEPONE_H__9A8873FF_D596_4DA6_8146_5195030D3E64__INCLUDED_)

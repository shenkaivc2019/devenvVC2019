﻿

#pragma once
//---------------------------------------------------------------------------//
// 文件名称:	ProjectInfoShee.h
// 说明:	井场信息管理相关功能文件
// 公司名 :	北京华脉世纪软件科技有限公司
// 作成者:	赵阳
// 作成日:	2020/2/18
// 备注:	无
//---------------------------------------------------------------------------//
#include "IDB.h"
#include "WizardStepOne.h"
#include "PropertyPageWellsite.h"
#include "PropertyPageWell.h"
#include "PropertyPageHole.h"
#include "PropertyPageRun.h"
//---------------------------------------------------------------------------//
// 类名：	井场信息向导功能封装
// 说明：	负责向导属性页功能
// 备注：	无
//---------------------------------------------------------------------------//

#ifndef BASEINFO_API
	#ifdef BASEINFO‌_EXPORT
		#define BASEINFO_API __declspec( dllexport )
	#else	
		#define BASEINFO_API __declspec( dllimport )
	#endif
#endif
/////////////////////////////////////////////////////////////////////////////
// CProjectInfoSheet

class BASEINFO_API CProjectInfoSheet2 : public CPropertySheet
{
	DECLARE_DYNAMIC(CProjectInfoSheet2)

// Construction
public:
	CProjectInfoSheet2(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CProjectInfoSheet2(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CProjectInfoSheet2(LPCTSTR pszCaption, IDB *pIDB , CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
// Attributes
public:
	CWizardStepOne			m_pageProject;
	CPropertyPageWellsite	m_pageWellsite;
	CPropertyPageWell		m_pageWell;
	CPropertyPageHole		m_pageHole;
	CPropertyPageRun		m_pageRun;
	//CProject*				m_pCurrProject;	// 新建工程
	IDB*					m_pIDB;	
// Operations
public:
	void   InitPre(void* pInfo[],int nMaxGrade); 
	void	Init();
	void	UpdateInfoToDB();
	
	
	void	CreateProjectCancel();
	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProjectInfoSheet)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CProjectInfoSheet2();

	// Generated message map functions
protected:
	//{{AFX_MSG(CProjectInfoSheet)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	BOOL  OnCheck(int nPageIndex);
	
	void*  m_ptrInfo[5];
	int m_nMaxGrade;
	CString m_strItemText[5];
};


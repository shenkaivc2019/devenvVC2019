﻿// ProjectWellInfo.h: interface for the CProjectWellInfo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROJECTWELLINFO_H__33F18AD7_F6C4_47D6_8D3C_D8415CE804BD__INCLUDED_)
#define AFX_PROJECTWELLINFO_H__33F18AD7_F6C4_47D6_8D3C_D8415CE804BD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//---------------------------------------------------------------------------//
// 文件名称:	ProjectWellInfo.h
// 说明:	井场信息管理相关功能文件
// 公司名 :	北京华脉世纪软件科技有限公司
// 作成者:	赵阳
// 作成日:	2019/12/20
// 备注:	无
//---------------------------------------------------------------------------//
#include "IDB.h"
//---------------------------------------------------------------------------//
// 类名：	井场信息中井信息功能封装
// 说明：	负责井信息功能
// 备注：	无
//---------------------------------------------------------------------------//
#ifndef BASEINFO_API
	#ifdef BASEINFO‌_EXPORT
		#define BASEINFO_API __declspec( dllexport )
	#else	
		#define BASEINFO_API __declspec( dllimport )
	#endif
#endif
class CProjectWellsiteInfo;
class BASEINFO_API CProjectWellInfo
{
public:
	CProjectWellInfo();
	virtual ~CProjectWellInfo();
	
	CString					m_strWellID;
	CString					m_strWellsiteID;
	IDB*					m_pIDB;
	TS_WELLINFOTB			m_gWellInfo;
	CPtrArray				m_HoleList;	//井眼信息列表
	CProjectWellsiteInfo*	m_pWellsiteInfo;

	BOOL	LoadFromDB();
	BOOL	SaveToDB();
	void	Clear();			//清空
};

#endif // !defined(AFX_PROJECTWELLINFO_H__33F18AD7_F6C4_47D6_8D3C_D8415CE804BD__INCLUDED_)

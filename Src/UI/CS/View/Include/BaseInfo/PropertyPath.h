﻿#if !defined(AFX_PROPERTYPATH_H__C1732DA3_BDCB_4A1E_8AC6_C19CAAD972CE__INCLUDED_)
#define AFX_PROPERTYPATH_H__C1732DA3_BDCB_4A1E_8AC6_C19CAAD972CE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PropertyPath.h : header file
//
#ifndef BASEINFO_API
	#ifdef BASEINFO‌_EXPORT
		#define BASEINFO_API __declspec( dllexport )
	#else	
		#define BASEINFO_API __declspec( dllimport )
	#endif
#endif
/////////////////////////////////////////////////////////////////////////////
// CPropertyPath dialog

class BASEINFO_API CPropertyPath : public CMFCPropertyPage
{
	DECLARE_DYNCREATE(CPropertyPath)

// Construction
public:
	CPropertyPath();
	~CPropertyPath();

// Dialog Data
	//{{AFX_DATA(CPropertyPath)
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PROPERTY_PATH };
#endif
	int		m_nDemoOrUser;
	CString	m_strLogDataPath;
	CString	m_strToolCalPath;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropertyPath)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropertyPath)
	afx_msg void OnBnLogdataPath();
	afx_msg void OnBnToolcalPath();
	afx_msg void OnDemoPath();
	afx_msg void OnUserPath();
	afx_msg void OnRDefinename();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	BOOL m_bUserDefineName;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPERTYPATH_H__C1732DA3_BDCB_4A1E_8AC6_C19CAAD972CE__INCLUDED_)

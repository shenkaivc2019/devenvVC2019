﻿// ProjectRunInfo.h: interface for the CProjectRunInfo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROJECTRUNINFO_H__B136E66F_5CDB_4159_9677_46B4002EEA6B__INCLUDED_)
#define AFX_PROJECTRUNINFO_H__B136E66F_5CDB_4159_9677_46B4002EEA6B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//---------------------------------------------------------------------------//
// 文件名称:	ProjectRunInfo.h
// 说明:	井场信息管理相关功能文件
// 公司名 :	北京华脉世纪软件科技有限公司
// 作成者:	赵阳
// 作成日:	2019/12/20
// 备注:	无
//---------------------------------------------------------------------------//
#include "IDB.h"
//---------------------------------------------------------------------------//
// 类名：	井场信息中趟钻信息功能封装
// 说明：	负责趟钻信息功能
// 备注：	无
//---------------------------------------------------------------------------//
#ifndef BASEINFO_API
	#ifdef BASEINFO‌_EXPORT
		#define BASEINFO_API __declspec( dllexport )
	#else	
		#define BASEINFO_API __declspec( dllimport )
	#endif
#endif
class CProjectHoleInfo;
class BASEINFO_API CProjectRunInfo
{
public:
	CProjectRunInfo();
	virtual ~CProjectRunInfo();

	CString				m_strHoleID;
	CString				m_strRunID;
	IDB*				m_pIDB;
	TS_RUNINFOTB		m_gRunInfo;
	CProjectHoleInfo*	m_pHoleInfo;
	
	void	SetActiveRun();
	BOOL	LoadFromDB();
	BOOL	SaveToDB();
	void	Clear();			//清空
	BOOL	Exists(LPTS_RUNINFOTB pgRUNINFOTB);
};

#endif // !defined(AFX_PROJECTRUNINFO_H__B136E66F_5CDB_4159_9677_46B4002EEA6B__INCLUDED_)

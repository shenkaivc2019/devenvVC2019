﻿// ProjectWellsiteInfo.h: interface for the CProjectWellsiteInfo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROJECTWELLSITEINFO_H__91A65878_818E_4DCF_8484_032B8366755C__INCLUDED_)
#define AFX_PROJECTWELLSITEINFO_H__91A65878_818E_4DCF_8484_032B8366755C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//---------------------------------------------------------------------------//
// 文件名称:	ProjectWellsiteInfo.h
// 说明:	井场信息管理相关功能文件
// 公司名 :	北京华脉世纪软件科技有限公司
// 作成者:	赵阳
// 作成日:	2019/12/20
// 备注:	无
//---------------------------------------------------------------------------//
#include "IDB.h"
//---------------------------------------------------------------------------//
// 类名：	井场信息中井场信息功能封装
// 说明：	负责井场信息功能
// 备注：	无
//---------------------------------------------------------------------------//
#ifndef BASEINFO_API
	#ifdef BASEINFO‌_EXPORT
		#define BASEINFO_API __declspec( dllexport )
	#else	
		#define BASEINFO_API __declspec( dllimport )
	#endif
#endif
class CProjectInfo;
class BASEINFO_API CProjectWellsiteInfo
{
public:;
	CProjectWellsiteInfo();
	virtual ~CProjectWellsiteInfo();

	CString			m_strProjectID;
	CString			m_strWellsiteID;
	IDB*			m_pIDB;
	CProjectInfo*	m_pProjectInfo;
	TS_WELLSITEINFOTB	m_gWellsiteInfo;
	CPtrArray		m_WellList;	//井眼信息列表

	BOOL	LoadFromDB();
	BOOL	SaveToDB();
	void	Clear();			//清空
};

#endif // !defined(AFX_PROJECTWELLSITEINFO_H__91A65878_818E_4DCF_8484_032B8366755C__INCLUDED_)

﻿// ProjectInfo.h: interface for the CProjectInfo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROJECTINFO_H__E7BC304E_D0F7_4232_9E8F_F7C164061A7B__INCLUDED_)
#define AFX_PROJECTINFO_H__E7BC304E_D0F7_4232_9E8F_F7C164061A7B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//---------------------------------------------------------------------------//
// 文件名称:	ProjectInfo .h
// 说明:	工程信息管理相关功能文件
// 公司名 :	北京华脉世纪软件科技有限公司
// 作成者:	赵阳
// 作成日:	2020/2/18
// 备注:	无
//---------------------------------------------------------------------------//
#include "IDB.h"

#ifndef BASEINFO_API
	#ifdef BASEINFO‌_EXPORT
		#define BASEINFO_API __declspec( dllexport )
	#else	
		#define BASEINFO_API __declspec( dllimport )
	#endif
#endif
//---------------------------------------------------------------------------//
// 类名：	工程信息功能封装
// 说明：	负责工程信息功能
// 备注：	无
//---------------------------------------------------------------------------//
class BASEINFO_API CProjectInfo
{
public:
	CProjectInfo();
	virtual ~CProjectInfo();

	CString			m_strProjectID;
	CProject* m_pProject;
	IDB*			m_pIDB;
	TS_PROJECTTB	m_gProjectInfo;
	CPtrArray		m_WellsiteList;	//井场信息列表

	BOOL	LoadFromDB();
	BOOL	SaveToDB();
	void	Clear();			//清空

	// 2020.06.14 ltg 将CWorkspaceBar中的成员移到这里 Start
	// ---------------------------------
	//	Log operations:
	// ---------------------------------
	int GetAllGraphs();
	void LogBegin(int nDirection, CGraphWnd* pActive = NULL);
	//void LogContinue();
	//void LogStop();

	CGraphList			m_Graphs;		// 测井窗口链表
	int					m_nWorkMode;
	int					m_nLog;
	// 2020.06.14 ltg 将CWorkspaceBar中的成员移到这里 End
};

#endif // !defined(AFX_PROJECTINFO_H__E7BC304E_D0F7_4232_9E8F_F7C164061A7B__INCLUDED_)

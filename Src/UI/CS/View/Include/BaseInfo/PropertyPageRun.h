﻿#if !defined(AFX_PROPERTYPAGERUN_H__5C3E4014_AE3F_4B15_B2DE_EAEAAB44B7F7__INCLUDED_)
#define AFX_PROPERTYPAGERUN_H__5C3E4014_AE3F_4B15_B2DE_EAEAAB44B7F7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//---------------------------------------------------------------------------//
// 文件名称:	PropertyPageRun.h
// 说明:	井场信息管理相关功能文件
// 公司名 :	北京华脉世纪软件科技有限公司
// 作成者:	赵阳
// 作成日:	2019/12/20
// 备注:	无
//---------------------------------------------------------------------------//
// PropertyPageRun.h : header file
//
#include "WPropertyPage.h"
#include "NumberEdit.h"
/////////////////////////////////////////////////////////////////////////////
// CPropertyPageRun dialog
//---------------------------------------------------------------------------//
// 类名：	井场信息中趟钻信息功能封装
// 说明：	负责趟钻信息界面功能
// 备注：	无
//---------------------------------------------------------------------------//
#ifndef BASEINFO_API
	#ifdef BASEINFO‌_EXPORT
		#define BASEINFO_API __declspec( dllexport )
	#else	
		#define BASEINFO_API __declspec( dllimport )
	#endif
#endif
class CProjectRunInfo;
class BASEINFO_API CPropertyPageRun : public CWPropertyPage
{
	DECLARE_DYNCREATE(CPropertyPageRun)

// Construction
public:
	CPropertyPageRun();
	~CPropertyPageRun();

	CProjectRunInfo* m_pInfo;
// Dialog Data
	//{{AFX_DATA(CPropertyPageRun)
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_INFO_RUN };
#endif
	CComboBox	m_ctrlBHA;
	CComboBox	m_cbxGAMA;	
	CString	m_strRunID;
	float	m_fToolSize;
	CString m_strToolSize;
	float	m_fRunHours;
	//int		m_nStopDepth;
	//int		m_nStartDepth;

	float		m_fStopDepth;
	float		m_fStartDepth;

	CString m_strStartDepth;
	CString	m_strStopDepth;

	CString	m_strDescription;
	CString	m_strStartTime;
	CString	m_strStopTime;
	CString	m_strHoleID;
	CString	m_strMemo;
	CString	m_strRunName;
	CString	m_strCreateTime;
	CString	m_strBHAID;
	//}}AFX_DATA

	CArray<CString, CString&> m_astrIDS;
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropertyPageRun)
	public:
	virtual void OnOK();
	virtual BOOL OnKillActive();
	virtual BOOL OnSetActive();
	virtual BOOL OnWizardFinish();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropertyPageRun)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeComboBha();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	void GetData();
	virtual int CheckData();
public:
	CDateTimeCtrl m_dtStartTime;
	CDateTimeCtrl m_dtStopTime;
	CNumberEdit m_editToolSize;
	CNumberEdit m_editStartDepth;
	CNumberEdit m_editStopDepth;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPERTYPAGERUN_H__5C3E4014_AE3F_4B15_B2DE_EAEAAB44B7F7__INCLUDED_)

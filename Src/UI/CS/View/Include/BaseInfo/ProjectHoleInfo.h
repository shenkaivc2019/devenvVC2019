﻿// ProjectHoleInfo.h: interface for the CProjectHoleInfo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROJECTHOLEINFO_H__8AC8B6B7_5B96_464F_AE8D_41065DB2BF05__INCLUDED_)
#define AFX_PROJECTHOLEINFO_H__8AC8B6B7_5B96_464F_AE8D_41065DB2BF05__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//---------------------------------------------------------------------------//
// 文件名称:	ProjectHoleInfo.h
// 说明:	井场信息管理相关功能文件
// 公司名 :	北京华脉世纪软件科技有限公司
// 作成者:	赵阳
// 作成日:	2019/12/20
// 备注:	无
//---------------------------------------------------------------------------//
#include "IDB.h"
//---------------------------------------------------------------------------//
// 类名：	井场信息中井眼信息功能封装
// 说明：	负责井眼信息功能
// 备注：	无
//---------------------------------------------------------------------------//
#ifndef BASEINFO_API
	#ifdef BASEINFO‌_EXPORT
		#define BASEINFO_API __declspec( dllexport )
	#else	
		#define BASEINFO_API __declspec( dllimport )
	#endif
#endif
class CProjectWellInfo;
class BASEINFO_API CProjectHoleInfo
{
public:
	CProjectHoleInfo();
	virtual ~CProjectHoleInfo();

	CString				m_strHoleID;
	CString				m_strWellID;
	IDB*				m_pIDB;
	TS_HOLEINFOTB		m_gHoleInfo;
	CPtrArray			m_RunList;	//趟钻信息列表
	CProjectWellInfo*	m_pWellInfo;

	BOOL	LoadFromDB();
	BOOL	SaveToDB();
	void	Clear();			//清空
};

#endif // !defined(AFX_PROJECTHOLEINFO_H__8AC8B6B7_5B96_464F_AE8D_41065DB2BF05__INCLUDED_)

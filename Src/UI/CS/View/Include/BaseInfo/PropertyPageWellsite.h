﻿#if !defined(AFX_PROPERTYPAGEWELLSITE_H__C567D755_9A5C_4AC0_BE9C_3F2A2E64689A__INCLUDED_)
#define AFX_PROPERTYPAGEWELLSITE_H__C567D755_9A5C_4AC0_BE9C_3F2A2E64689A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//---------------------------------------------------------------------------//
// 文件名称:	PropertyPageWellsite.h
// 说明:	井场信息管理相关功能文件
// 公司名 :	北京华脉世纪软件科技有限公司
// 作成者:	赵阳
// 作成日:	2019/12/20
// 备注:	无
//---------------------------------------------------------------------------//
// PropertyPageWellsite.h : header file
//
#include "WPropertyPage.h"
#include "ColorGroupBox.h"
/////////////////////////////////////////////////////////////////////////////
// CPropertyPageWellsite dialog
//---------------------------------------------------------------------------//
// 类名：	井场信息中井场信息功能封装
// 说明：	负责井场信息界面功能
// 备注：	无
//---------------------------------------------------------------------------//
#ifndef BASEINFO_API
	#ifdef BASEINFO‌_EXPORT
		#define BASEINFO_API __declspec( dllexport )
	#else	
		#define BASEINFO_API __declspec( dllimport )
	#endif
#endif
class CProjectWellsiteInfo;
class BASEINFO_API CPropertyPageWellsite : public CWPropertyPage
{
	DECLARE_DYNCREATE(CPropertyPageWellsite)

// Construction
public:
	CPropertyPageWellsite();
	~CPropertyPageWellsite();

	CProjectWellsiteInfo* m_pInfo;
// Dialog Data
	//{{AFX_DATA(CPropertyPageWellsite).36
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_INFO_WELLSITE };
#endif
	CString	m_strWellsiteID;
	CString	m_strState;
	CString	m_strWellsiteName;
	CString	m_strRegion;
	CString	m_strLocation;
	CString	m_strField;
	CString	m_strCounty;
	CString	m_strCountry;
	CString	m_strCreateTime;
	CString	m_strMemo;
	CString	m_strProjectID;
	CString	m_strRig;
	CString	m_strDrillCompany;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropertyPageWellsite)
	public:
	virtual void OnOK();
	virtual BOOL OnKillActive();
	virtual BOOL OnSetActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropertyPageWellsite)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	void GetData();
	CColorGroupBox m_groupBox12;
	CColorGroupBox m_groupBox13;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPERTYPAGEWELLSITE_H__C567D755_9A5C_4AC0_BE9C_3F2A2E64689A__INCLUDED_)

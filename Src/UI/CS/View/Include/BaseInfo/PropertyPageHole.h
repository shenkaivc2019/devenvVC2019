﻿#if !defined(AFX_PROPERTYPAGEHOLE_H__0B28616C_1CA0_4D67_9409_D98E802CB5F0__INCLUDED_)
#define AFX_PROPERTYPAGEHOLE_H__0B28616C_1CA0_4D67_9409_D98E802CB5F0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//---------------------------------------------------------------------------//
// 文件名称:	PropertyPageHole.h
// 说明:	井场信息管理相关功能文件
// 公司名 :	北京华脉世纪软件科技有限公司
// 作成者:	赵阳
// 作成日:	2019/12/20
// 备注:	无
//---------------------------------------------------------------------------//
// PropertyPageHole.h : header file
//
#include "WPropertyPage.h"
#include "ColorGroupBox.h"
#include "NumberEdit.h"
/////////////////////////////////////////////////////////////////////////////
// CPropertyPageHole dialog
//---------------------------------------------------------------------------//
// 类名：	井场信息中井眼信息功能封装
// 说明：	负责井眼信息界面功能
// 备注：	无
//---------------------------------------------------------------------------//
#ifndef BASEINFO_API
	#ifdef BASEINFO‌_EXPORT
		#define BASEINFO_API __declspec( dllexport )
	#else	
		#define BASEINFO_API __declspec( dllimport )
	#endif
#endif
class CProjectHoleInfo;
class BASEINFO_API CPropertyPageHole : public CWPropertyPage
{
	DECLARE_DYNCREATE(CPropertyPageHole)

// Construction
public:
	CPropertyPageHole();
	~CPropertyPageHole();

	CProjectHoleInfo* m_pInfo;
// Dialog Data
	//{{AFX_DATA(CPropertyPageHole)
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_INFO_HOLE };
#endif

	CString	m_strAPISN;
	CString	m_strDirectionDriller1;
	CString	m_strDirectionDriller2;
	CString	m_strDescription;
	CString	m_strGeologist1;
	CString	m_strGeologist2;
	CString	m_strHoleID;
	CString	m_strHoleName;
	CString	m_strJobNumber;
	CString	m_strMWDEngeer1;
	CString	m_strMWDEngeer2;
	CString	m_strMWDKit;
	float	m_fHoleSize;
	double  m_dHoleSize;
	CString	m_strCreateTime;
	CString	m_strWellID;
	CString	m_strMemo;
	//}}AFX_DATA
	CNumberEdit m_dtHoleSize;
	// 2022.11.10 Start	
	CComboBox	m_ctrParentHole;
	std::vector<TS_HOLEINFOTB> m_vecHole;
	float		m_fStopDepth;
	float		m_fStartDepth;
	CString m_strStartDepth;
	CString	m_strStopDepth;
	CNumberEdit m_editStartDepth;
	CNumberEdit m_editStopDepth;
	virtual int CheckData();
	// 2022.11.10 End
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropertyPageHole)
	public:
	virtual void OnOK();
	virtual BOOL OnKillActive();
	virtual BOOL OnSetActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropertyPageHole)
	virtual BOOL OnInitDialog();
	afx_msg void OnEnChangeHoleSize();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	void GetData();
	CColorGroupBox m_groupBox39;
	CColorGroupBox m_groupBox40;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPERTYPAGEHOLE_H__0B28616C_1CA0_4D67_9409_D98E802CB5F0__INCLUDED_)

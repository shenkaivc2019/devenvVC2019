﻿#if !defined(AFX_PROJECTINFOSHEET_H__615557BC_F528_40B5_9975_7FFFF8C9AC42__INCLUDED_)
#define AFX_PROJECTINFOSHEET_H__615557BC_F528_40B5_9975_7FFFF8C9AC42__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//---------------------------------------------------------------------------//
// 文件名称:	ProjectInfoShee.h
// 说明:	井场信息管理相关功能文件
// 公司名 :	北京华脉世纪软件科技有限公司
// 作成者:	赵阳
// 作成日:	2020/2/18
// 备注:	无
//---------------------------------------------------------------------------//
#include "IDB.h"
#include "WizardStepOne.h"
#include "PropertyPageWellsite.h"
#include "PropertyPageWell.h"
#include "PropertyPageHole.h"
#include "PropertyPageRun.h"
//---------------------------------------------------------------------------//
// 类名：	井场信息向导功能封装
// 说明：	负责向导属性页功能
// 备注：	无
//---------------------------------------------------------------------------//

#ifndef BASEINFO_API
	#ifdef BASEINFO‌_EXPORT
		#define BASEINFO_API __declspec( dllexport )
	#else	
		#define BASEINFO_API __declspec( dllimport )
	#endif
#endif
/////////////////////////////////////////////////////////////////////////////
// CProjectInfoSheet

class BASEINFO_API CProjectInfoSheet : public CPropertySheet
{
	DECLARE_DYNAMIC(CProjectInfoSheet)

// Construction
public:
	CProjectInfoSheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CProjectInfoSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CProjectInfoSheet(LPCTSTR pszCaption, IDB *pIDB , CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
// Attributes
public:
	CWizardStepOne			m_pageProject;
	CPropertyPageWellsite	m_pageWellsite;
	CPropertyPageWell		m_pageWell;
	CPropertyPageHole		m_pageHole;
	CPropertyPageRun		m_pageRun;
	CProject*				m_pCurrProject;	// 新建工程
	IDB*					m_pIDB;
	CButton m_btnOK;
// Operations
public:
	void	Init();
	void	AddInfoToDB();
	void	CreateProjectDir();
	BOOL	CreateProjectOk();
	void	CreateProjectCancel();
	CString  CreateProjectID();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProjectInfoSheet)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CProjectInfoSheet();

	// Generated message map functions
protected:
	//{{AFX_MSG(CProjectInfoSheet)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	BOOL  OnCheck(int nPageIndex);
	void OnBtnHelp();
	BOOL  m_bArrayIsInputName[5];
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROJECTINFOSHEET_H__615557BC_F528_40B5_9975_7FFFF8C9AC42__INCLUDED_)

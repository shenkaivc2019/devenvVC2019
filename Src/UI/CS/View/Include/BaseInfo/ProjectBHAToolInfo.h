﻿// ProjectBHAToolInfo.h: interface for the CProjectBHAToolInfo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROJECTBHATOOLINFO_H__075C1A7D_1773_4C05_850F_C228A4CF9FAF__INCLUDED_)
#define AFX_PROJECTBHATOOLINFO_H__075C1A7D_1773_4C05_850F_C228A4CF9FAF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//---------------------------------------------------------------------------//
// 文件名称:	ProjectBHAToolInfo.h
// 说明:	BHA仪器信息信息管理相关功能文件
// 公司名 :	北京华脉世纪软件科技有限公司
// 作成者:	赵阳
// 作成日:	2020/2/18
// 备注:	无
//---------------------------------------------------------------------------//
#include "IDB.h"
//---------------------------------------------------------------------------//
// 类名：	BHA信息中仪器信息功能封装
// 说明：	负责仪器信息功能
// 备注：	无
//---------------------------------------------------------------------------//
#ifndef BASEINFO_API
	#ifdef BASEINFO‌_EXPORT
		#define BASEINFO_API __declspec( dllexport )
	#else	
		#define BASEINFO_API __declspec( dllimport )
	#endif
#endif
class CProjectBHAInfo;
class BASEINFO_API CProjectBHAToolInfo
{
public:
	CProjectBHAToolInfo();
	virtual ~CProjectBHAToolInfo();

	CString				m_strBHAID;
	int					m_nToolID;
	IDB*				m_pIDB;
	CProjectBHAInfo*	m_pBHAInfo;
	TS_BHAINFODTB		m_gBHAToolInfo;
	DB_TOOL_INFO m_gToolInfo;
	vector<TS_MEASPOINTINFOTB> m_vecgMeasPointInfoTB;

	BOOL	LoadFromDB();
	BOOL	SaveToDB();
};

#endif // !defined(AFX_PROJECTBHATOOLINFO_H__075C1A7D_1773_4C05_850F_C228A4CF9FAF__INCLUDED_)

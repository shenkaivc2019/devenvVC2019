﻿#if !defined(AFX_PROPERTYPAGEWELL_H__66CFF622_2C93_4B81_99AE_B39E618174ED__INCLUDED_)
#define AFX_PROPERTYPAGEWELL_H__66CFF622_2C93_4B81_99AE_B39E618174ED__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//---------------------------------------------------------------------------//
// 文件名称:	PropertyPageWell.h
// 说明:	井场信息管理相关功能文件
// 公司名 :	北京华脉世纪软件科技有限公司
// 作成者:	赵阳
// 作成日:	2019/12/20
// 备注:	无
//---------------------------------------------------------------------------//
// PropertyPageWell.h : header file
//
#include "WPropertyPage.h"
#include "ColorGroupBox.h"
#include "NumberEdit.h"
/////////////////////////////////////////////////////////////////////////////
// CPropertyPageWell dialog
//---------------------------------------------------------------------------//
// 类名：	井场信息中井信息功能封装
// 说明：	负责井信息界面功能
// 备注：	无
//---------------------------------------------------------------------------//
#ifndef BASEINFO_API
	#ifdef BASEINFO‌_EXPORT
		#define BASEINFO_API __declspec( dllexport )
	#else	
		#define BASEINFO_API __declspec( dllimport )
	#endif
#endif
class CProjectWellInfo;
class BASEINFO_API CPropertyPageWell : public CWPropertyPage
{
	DECLARE_DYNCREATE(CPropertyPageWell)

// Construction
public:
	CPropertyPageWell();
	~CPropertyPageWell();

	CProjectWellInfo* m_pInfo;
// Dialog Data
	//{{AFX_DATA(CPropertyPageWell)
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_INFO_WELL };
#endif
	CString	m_strWellName;
	float	m_fLocationNorth;
	float	m_fLocationEast;

	CString	m_strLocationNorth;
	CString	m_strLocationEast;

	float	m_fLongitude;
	float	m_fLatitude;
	//2020.7.11 sys [BUG84] Start
	float   m_fLatitudeDegree;
	float   m_fLatitudeBranch;
	float   m_fLatitudeSecond;
	float	m_fLongitudeDegree;
	float	m_fLongitudeBranch;
	float	m_fLongitudeSecond;

	CString   m_strLatitudeDegree;
	CString   m_strLatitudeBranch;
	CString   m_strLatitudeSecond;
	CString	  m_strLongitudeDegree;
	CString	  m_strLongitudeBranch;
	CString	  m_strLongitudeSecond;
	//2020.7.11 sys [BUG84] End
	CString		m_strWellID;

	CComboBox	m_ctrDepthRef;

	CString		m_strWellsiteID;
	CString	m_strMemo;
	CString	m_strCreateTime;
	float	m_fGroundLevel;
	float	m_fKellyBushsea;
	float	m_fKellyBushDrilFlr;
	float	m_fKellyBushGrd;
	float	m_fDrilFlrGrd;
	float	m_fDrilFlrSea;
	int		m_nDepthRef;

	CString	m_strGroundLevel;
	CString	m_strKellyBushDrilFlr;
	CString	m_strKellyBushGrd;
	CString	m_strDrilFlrGrd;
	CString	m_strDrilFlrSea;

	//}}AFX_DATA
	CNumberEdit m_dtLocationNorth;
	CNumberEdit m_dtLocationEast;
	CNumberEdit m_dtDrilFlrGrd;
	CNumberEdit m_dtDrilFlrSea;
	CNumberEdit m_dtKellyBushGrd;
	CNumberEdit m_dtKellyBushDrilFlr;
	CNumberEdit m_dtGroundLevel;


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropertyPageWell)
	public:
	virtual void OnOK();
	virtual BOOL OnKillActive();
	virtual BOOL OnSetActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropertyPageWell)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	void GetData();
	CColorGroupBox m_groupBox14;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPERTYPAGEWELL_H__66CFF622_2C93_4B81_99AE_B39E618174ED__INCLUDED_)

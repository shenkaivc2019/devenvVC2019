﻿// ProjectBHAInfo.h: interface for the CProjectBHAInfo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROJECTBHAINFO_H__A9CCFD13_5D49_4B83_8EBB_507A1C59BC69__INCLUDED_)
#define AFX_PROJECTBHAINFO_H__A9CCFD13_5D49_4B83_8EBB_507A1C59BC69__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//---------------------------------------------------------------------------//
// 文件名称:	ProjectBHAInfo.h
// 说明:	BHA仪器信息信息管理相关功能文件
// 公司名 :	北京华脉世纪软件科技有限公司
// 作成者:	赵阳
// 作成日:	2020/2/18
// 备注:	无
//---------------------------------------------------------------------------//
#include "IDB.h"
#ifndef BASEINFO_API
	#ifdef BASEINFO‌_EXPORT
		#define BASEINFO_API __declspec( dllexport )
	#else	
		#define BASEINFO_API __declspec( dllimport )
	#endif
#endif
//---------------------------------------------------------------------------//
// 类名：	BHA信息功能封装
// 说明：	负责BHA信息管理功能
// 备注：	无
//---------------------------------------------------------------------------//
class BASEINFO_API CProjectBHAInfo
{
public:
	CProjectBHAInfo();
	virtual ~CProjectBHAInfo();

	CString			m_strBHAID;
	IDB*			m_pIDB;
	TS_BHAINFOMTB	m_gBHAINFOMTB;
	CPtrArray		m_ToolList;

	BOOL	LoadFromDB();
	BOOL	SaveToDB();
	void	Clear();			//清空
};

#endif // !defined(AFX_PROJECTBHAINFO_H__A9CCFD13_5D49_4B83_8EBB_507A1C59BC69__INCLUDED_)

/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __ITRACK_H__
#define __ITRACK_H__

#include "ULInterface.h"

#define sp_x 6
#define sp_y 20

// Interface
interface ITrack : IUnknown
{
	//////////////////////////////////////////////////////////////////////
	// 属性
	//////////////////////////////////////////////////////////////////////
	ULMTSTR		GetName() = 0;
	ULMETHOD	SetName(LPCTSTR lpszName) = 0;
	
	ULMINT		GetType() = 0;
	ULMETHOD	SetType(int nType) = 0;
	
	ULMDWD		GetColor() = 0;
	ULMETHOD	SetColor(COLORREF crColor) = 0;
	
	ULMINT		GetLineStyle() = 0;
	ULMETHOD	SetLineStyle(int nLineStyle) = 0;
	
	ULMPTR		GetGridProp() = 0;
	ULMETHOD	SetGridProp(void* pGridProp) = 0;

	ULMINT		GetVertGridCount() = 0;
	
	ULMETHOD	GetHeadRect(LPRECT lpRect, BOOL bPrint) = 0;
	ULMETHOD	SetHeadRect(LPRECT lpRect) = 0;

	ULMETHOD	SetHeadRectLR(LPRECT lpRect) = 0;

	ULMETHOD GetRegionRect(LPRECT lpRect, BOOL bPrint) = 0;
	ULMETHOD SetRegionRect(LPRECT lpRect, BOOL bLR = TRUE) = 0;

	ULMINT	RegionLeft() = 0;
	ULMINT	RegionRight() = 0;
	ULMINT	RegionWidth() = 0;
	ULMINT	RegionTop() = 0;
	ULMINT	RegionBottom() = 0;
	ULMINT	RegionHeight() = 0;

	ULMINT		GetCoordinateFromDepth(long lDepth) = 0;
	ULMLNG		GetDepthFromCoordinate(int nPos) = 0;
	ULMINT		GetCoordinateFromTime(long lTime) = 0;
	ULMLNG		GetTimeFromCoordinate(int nPos) = 0;

	ULMBOOL		IsTimeShow()  = 0;
	ULMBOOL		IsDepthShow() = 0;
	ULMBOOL		IsFirstTrack()= 0;
	ULMBOOL		IsLastTrack() = 0;
	ULMBOOL		IsFirstOrLastTrack() = 0;
	ULMBOOL		IsGapRemain() = 0;
	ULMETHOD	IsGapRemain(BOOL bGapRemain) = 0;
	ULMBOOL		IsColorPrint() = 0;

	ULMINT		GetGapRemain() = 0;
	ULMETHOD	SetGapRemain(int nGapRemain) = 0;
	ULMETHOD	SetGapPos(int nPos) = 0;
	ULMINT		GetGapDepthCount() = 0;
	ULMLNG		GetGapDepth(int nIndex) = 0;
	ULMPTR		GetDepthCurve() = 0;

	ULMETHOD	GetDrawSel(void* pDrawSel) = 0;

	ULMINT		GetDriveMode() = 0;
	ULMINT		GetWorkMode() = 0;

	ULMETHOD	GetLogMode(int& nDirection, int& nWorkMode, int& nDriveMode) = 0;
	ULMFLT		GetRatioY() = 0;
	
	ULMPTR		GetCurveList() = 0;
	ULMETHOD	GetAllCurve(CURVES& vecCurve) = 0;
	ULMINT		GetSeledCurve(CURVES& vecCurve) = 0;	// 得到当前井道内选中曲线

	//////////////////////////////////////////////////////////////////////
	// 绘图
	//////////////////////////////////////////////////////////////////////
	ULMETHOD	DrawTitleColor(CDC* pDC, LPRECT lpRect, COLORREF clrStart = RGB(94,146,223), 
					COLORREF clrEnd =  RGB(255,255,255)) = 0;

	ULMETHOD	PrintGaps(CULPrintInfo* pInfo, long lMaxDepth, long lMinDepth) = 0;
	ULMETHOD	PrintEdge(CULPrintInfo* pInfo, long lMaxDepth, long lMinDepth) = 0;

	ULMBOOL		RemoveCurve(ICurve* pCurve) = 0;
	ULMETHOD	GetIUnits(CUnits* pIUnits) = 0;

	ULMINT		GetPrintDepthAngle() = 0;
	ULMLNG		GetTD() = 0;
	ULMINT		GetDepthTitle(CStringArray* pTitles = NULL) = 0;

	ULMPTR		GetAzim() = 0;
	ULMPTR		GetDev() = 0;
		
	ULMPTR		GetFont(UINT nIndex = ftCT) = 0;
	ULMPTR		FindCurve(ICurve* pCurve, BOOL bLeft) = 0;
	ULMDBL      GetXDepth(long lDepth) = 0;

	ULMETHOD	DrawPat(CDC* pDC, LPRECT lpRect, LPCTSTR pszPat, COLORREF clrFore, COLORREF clrBkg) = 0;
	ULMINT		GetTimeTitle(CStringArray* pTitles = NULL) = 0;
};

// {9A4397BB-7195-461c-A79E-01B4EE3E0DBE}
static const IID IID_ITrack = 
{ 0x9a4397bb, 0x7195, 0x461c, { 0xa7, 0x9e, 0x1, 0xb4, 0xee, 0x3e, 0xd, 0xbe } };

#endif

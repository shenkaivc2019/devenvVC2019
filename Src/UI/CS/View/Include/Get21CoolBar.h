#ifndef _GET21_COOLBAR_H_
	#define _GET21_COOLBAR_H_


/////////////////////////////////
//
typedef struct
{
	int		nCommandID;		// 
	int		nBitmapID;		// 
	LPCSTR	lpszTitle;		// 
	BOOL	bIsDependMenu;	// 
							// いと
							// 

} GET21_COOLBAR_INFO, *LPGET21_COOLBAR_INFO;

	#define		ID_COOLBAR_TERMINATE		-1		// 


	#define		COOLBAR_COMMAND_STR(id, str)		ID_##id, IDB_##id, str
	#define		COOLBAR_COMMAND(id)					ID_##id, IDB_##id, NULL


	#define		COOLBAR_END()		ID_COOLBAR_TERMINATE, 0, NULL
	#define		COOLBAR_SEPARATOR()	ID_SEPARATOR, 0, NULL
	#define		IDB_SEPARATOR		0


////////////////////////////////////////////////////////
class CGet21CoolbarItem
{
protected:
	CArray <GET21_COOLBAR_INFO, GET21_COOLBAR_INFO&> m_array;


public:
	CGet21CoolbarItem(){m_array.RemoveAll();}
	~CGet21CoolbarItem(){m_array.RemoveAll();}

	BOOL Add(int nCommandID, int nBitmapID, LPCSTR lpszTitle = NULL, BOOL bIsDependMenu = TRUE);

	GET21_COOLBAR_INFO& operator[](int index) { return m_array[index]; }
	GET21_COOLBAR_INFO GetAt(int index) { return m_array.GetAt(index); }

	int GetSize() {return m_array.GetSize();}

};

#if 0
typedef struct
{
	LPCSTR 



} GET21_COOLBAR_RESOURCEINFO, *LPGET21_COOLBAR_RESOURCEINFO;

class CGet21CoolbarFromResource
{


};
#endif

class CCoolBar : public CControlBar
{
protected:
	DECLARE_DYNAMIC(CCoolBar)

public:
	CCoolBar();
	virtual ~CCoolBar();

	BOOL Create(CWnd* pParentWnd, DWORD dwStyle,
		DWORD dwAfxBarStyle = CBRS_ALIGN_TOP,
		UINT nID = AFX_IDW_TOOLBAR);

	BOOL GetBarInfo(LPREBARINFO lp)
		{ ASSERT(::IsWindow(m_hWnd));
		  return (BOOL)SendMessage(RB_GETBARINFO, 0, (LPARAM)lp); }
	BOOL SetBarInfo(LPREBARINFO lp)
		{ ASSERT(::IsWindow(m_hWnd));
		  return (BOOL)SendMessage(RB_SETBARINFO, 0, (LPARAM)lp); }
	BOOL GetBandInfo(int iBand, LPREBARBANDINFO lp)
		{ ASSERT(::IsWindow(m_hWnd));
		  return (BOOL)SendMessage(RB_GETBANDINFO, iBand, (LPARAM)lp); }
	BOOL SetBandInfo(int iBand, LPREBARBANDINFO lp)
		{ ASSERT(::IsWindow(m_hWnd));
		  return (BOOL)SendMessage(RB_SETBANDINFO, iBand, (LPARAM)lp); }

	BOOL InsertBand(int iWhere, LPREBARBANDINFO lp)
		{ ASSERT(::IsWindow(m_hWnd));
		  return (BOOL)SendMessage(RB_INSERTBAND, (WPARAM)iWhere, (LPARAM)lp); }
	BOOL DeleteBand(int nWhich)
		{ ASSERT(::IsWindow(m_hWnd));
		  return (BOOL)SendMessage(RB_INSERTBAND, (WPARAM)nWhich); }

	int GetBandCount()
		{ ASSERT(::IsWindow(m_hWnd));
		  return (int)SendMessage(RB_GETBANDCOUNT); }
	int GetRowCount()
		{ ASSERT(::IsWindow(m_hWnd));
	     return (int)SendMessage(RB_GETROWCOUNT); }
	int GetRowHeight(int nWhich)
		{ ASSERT(::IsWindow(m_hWnd));
	     return (int)SendMessage(RB_GETROWHEIGHT, (WPARAM)nWhich); }

protected:
	CGet21CoolbarItem*	m_info;			// 
	UINT 				m_res_id;		// 

	virtual BOOL OnCreateBands() = 0;

	// CControlBar
	virtual CSize CalcFixedLayout(BOOL bStretch, BOOL bHorz);
	virtual CSize CalcDynamicLayout(int nLength, DWORD nMode);
	virtual void OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler);

	DECLARE_MESSAGE_MAP()
	afx_msg int  OnCreate(LPCREATESTRUCT lpcs);
	afx_msg void OnPaint();
	afx_msg void OnHeigtChange(NMHDR* pNMHDR, LRESULT* pRes);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};


class CCToolBar : public CToolBar
{

public:
	CCToolBar();
	virtual ~CCToolBar();

protected:
	DECLARE_DYNAMIC(CCToolBar)
	virtual void OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler);
	void SetButtonStyle(int nIndex, UINT nStyle);

	void _GetButton(int nIndex, TBBUTTON* pButton) const;
	void _SetButton(int nIndex, TBBUTTON* pButton);


	afx_msg BOOL OnNcCreate(LPCREATESTRUCT lpcs);
	afx_msg void OnNcPaint();
	afx_msg void OnPaint();
	afx_msg void OnNcCalcSize(BOOL, NCCALCSIZE_PARAMS*);
//	afx_msg void OnAutoSize();
//	afx_msg void OnSize(UINT nType, int cx, int cy);
	DECLARE_MESSAGE_MAP()
};

class CRebarInfo : public REBARINFO {

public:
	CRebarInfo() {
		memset(this, 0, sizeof(REBARINFO));
		cbSize = sizeof(REBARINFO);
	}
};

class CRebarBandInfo : public REBARBANDINFO {

public:
	CRebarBandInfo() {
		memset(this, 0, sizeof(REBARBANDINFO));
		cbSize = sizeof(REBARBANDINFO);
	}
};

class CGet21CoolBar : public CCoolBar
{

protected:
	DECLARE_DYNAMIC(CGet21CoolBar)
	CCToolBar		m_wndToolBar;		// 
	CBitmap			m_bmBackground;		// 
	CFont			m_font;				// 

public:
	//BOOL Create(CWnd* pParentWnd, LPGET21_COOLBAR_INFO pInfo = NULL);
	BOOL Create(CWnd* pParentWnd, CGet21CoolbarItem* pInfo = NULL);
	BOOL Create(CWnd* pParentWnd, UINT nIDResource);

	CToolBar* GetToolBar() {return &m_wndToolBar;}

protected:

	BOOL CreateToolbar(CCToolBar& tb, UINT nBitmapID = 0);

	BOOL CreateToolbar(CCToolBar& tb, UINT nIDResource, UINT nBitmapID = 0);

	BOOL InsertBandToolbar(CCToolBar& tb, UINT nBitmapID = 0);

	virtual BOOL OnCreateBands();

};

#endif // _GET21_COOLBAR_H_


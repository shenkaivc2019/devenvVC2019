#pragma once

#ifndef BASECTRL_API
	#ifdef BASECTRL_EXPORT
		#define BASECTRL_API __declspec( dllexport )
	#else
		#define BASECTRL_API __declspec( dllimport )
	#endif
#endif

class BASECTRL_API CNumberEdit : public CEdit
{
public:
	CNumberEdit();
	~CNumberEdit();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};


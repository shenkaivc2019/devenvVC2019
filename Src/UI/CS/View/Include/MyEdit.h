#pragma once
#define WM_MY_EDIT_DESTROY WM_USER + 100

#ifndef BASECTRL_API
	#ifdef BASECTRL_EXPORT
		#define BASECTRL_API __declspec( dllexport )
	#else
		#define BASECTRL_API __declspec( dllimport )
	#endif
#endif

// CMyEdit

class BASECTRL_API CMyEdit : public CEdit
{
	DECLARE_DYNAMIC(CMyEdit)

// Overrides
// ClassWizard generated virtual function overrides
//{{AFX_VIRTUAL(CDataViewBar)
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
//}}AFX_VIRTUAL

public:
	CMyEdit();
	virtual ~CMyEdit();


protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
};



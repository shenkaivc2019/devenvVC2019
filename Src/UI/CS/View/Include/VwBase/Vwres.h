#pragma once
//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
//

// 2005.12.22 Ver5.00 機能改善【NG5_002】Start
#define RFX_IDD_DLG_PROC_BNS            794
#define RFX_IDD_DLG_PROC_PERIOD_BNS     795
#define RFX_IDD_DLG_PROC_YM             796
#define RFX_IDD_DLG_PROC_PERIOD_YM      797
// End

// 処理選択クラス
#define RFX_IDD_DLG_PROCSELECT          789
#define RFX_IDC_SETUP                   9999

// 個人／出力指定基本クラス
#define RFX_IDD_SELEMP_VIEW             798
#define RFX_IDD_SELEMP_VIEW_EX          799
#define RFX_IDC_SETDATE                 12287
#define RFX_IDS_COOLBARFONTNAME         18175
#define RFX_ID_OPTION_DEFDATESETUP		0x7bff		// 日付初期設定

#define RFX_IDS_START					(24320)
#define RFX_IDS_CANNOTLOADRESOURCE		(RFX_IDS_START+0)	// "共通リソースライブラリ[%s]の読みこみに失敗しました"
#define RFX_IDS_TERMINATE_NORMAL		(RFX_IDS_START+1)	// "正常終了 L:%d"
#define RFX_IDS_TERMINATE_ERROR			(RFX_IDS_START+2)	// "エラー %d at L:%d"
#define RFX_IDS_ERROR					(RFX_IDS_START+3)	// "エラー %d at L:%d"
#define RFX_IDS_EXIT_NORMAL				(RFX_IDS_START+4)	// "正常終了"
#define RFX_IDS_EXIT_EXCEPTION			(RFX_IDS_START+5)	// "例外終了"
#define RFX_IDS_EXCEPTION_MFC			(RFX_IDS_START+6)	// "例外 %s 発生"
#define RFX_IDS_EXCEPTION_UNKNOWN		(RFX_IDS_START+7)	// "不明な例外発生"
#define RFX_IDS_MESSAGE					(RFX_IDS_START+8)	// "任意のメッセージ %s L:%d"
#define RFX_IDS_ENDPROC					(RFX_IDS_START+9)	// "処理終了"
#define RFX_IDS_EXECCALC				(RFX_IDS_START+10)	// "計算実行"
#define RFX_IDS_PRINTOUT				(RFX_IDS_START+11)	// "印刷出力"
#define RFX_IDS_OLE_INIT_FAILED			(RFX_IDS_START+12)	// "OLE の初期化に失敗しました。OLE ライブラリが正しいバージョンのものか確認してください。"

// 仮
#define		GFX_IDP_SYSTEMERROR  		(RFX_IDS_START+100)	// "システムエラーです。"

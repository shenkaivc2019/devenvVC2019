#if !defined(AFX_RFXDLGSP_H__A1A86344_9AEE_11D3_8D92_00105A14616F__INCLUDED_)
#define AFX_RFXDLGSP_H__A1A86344_9AEE_11D3_8D92_00105A14616F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// RfxDlgSp.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// CVwDlgSetup ダイアログ

#undef AFX_DATA
#ifdef _RFXBASE_BUILD
	#define AFX_DATA AFX_EXT_DATA
#else
	#define AFX_DATA AFX_DATA_IMPORT
#endif

class CVwDlgSetup : public CDialog
{
// コンストラクション
public:
	CVwDlgSetup(UINT nIDTemplate, CWnd* pParent = NULL);

// ダイアログ データ
	//{{AFX_DATA(CVwDlgSetup)
		// メモ: ClassWizard はこの位置にデータ メンバを追加します。
	//}}AFX_DATA

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	virtual void CanCloseDlg();

	virtual BOOL IsDataChanged();
	virtual BOOL UpdateDlgData();
	virtual BOOL SaveDlgData();
	virtual BOOL CheckDlgData();

	//{{AFX_VIRTUAL(CVwDlgSetup)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:
	BOOL m_bNeedSave;

	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CVwDlgSetup)
	afx_msg void OnDestroy();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	virtual afx_msg void OnChangeItems();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#undef AFX_DATA
#define AFX_DATA

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio は前行の直前に追加の宣言を挿入します。



#endif // !defined(AFX_RFXDLGSP_H__A1A86344_9AEE_11D3_8D92_00105A14616F__INCLUDED_)

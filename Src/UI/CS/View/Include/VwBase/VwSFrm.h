// VwFrm.h : CVwBaseSFrame                          
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_RFXFRM_H__CA2BDABA_F703_11D2_8BBB_00105A14616F__INCLUDED_S)
#define AFX_RFXFRM_H__CA2BDABA_F703_11D2_8BBB_00105A14616F__INCLUDED_S

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#undef AFX_DATA
#ifdef _RFXBASE_BUILD
	#define AFX_DATA AFX_EXT_DATA
#else
	#define AFX_DATA AFX_DATA_IMPORT
#endif


class CVwBaseSFrame : public CFrameWnd
{
protected:
	CVwBaseSFrame() noexcept;
	DECLARE_DYNCREATE(CVwBaseSFrame)

	// 2001/07/05 start
	CRuntimeClass**			m_pViewClass;	// 显示的视图的运行时
	CView**					m_pView;		// 显示的视图的指针
	BOOL					m_bIsRegist;	// 使用注册表信息吗？
	CVwCoolbarItem*		m_pCoolBarItem;		// CoolBar按钮信息
	CVwMenuItem*			m_pMenuItem;	// 菜单项目
	int						m_nViewNumber;	// 管理的视数
	UINT					m_nMenuID;			// 菜单资源编号

	CVwMenu				m_menu;				// 菜单

// 查看器
public:
	CPrintPreviewState* m_pModeStuff;

	// 控制栏成员
	CStatusBar m_wndStatusBar;
	CCoolBar* 	m_pCoolBar;

	// 转换视图

	virtual BOOL ChangeView(int nViewNumber, BOOL bIsRemain = FALSE);
	// 初始显示结束后由应用程序类称呼的
	virtual void OnEndofInitialize();

// 操作
public:
	void SwitchToView(int nViewNo, BOOL bIsRemain = FALSE);
	void SetPreviewMode(BOOL bPreview);


	// 酷吧制作。在转换选项编号时重建时使用的1
	BOOL CreateCoolBar();
	BOOL CreateCoolBar(CVwCoolbarItem* pCoolItem);
	BOOL CreateMenu();
	BOOL CreateCoolBar(UINT nIDResource);
	BOOL LoadFrame(UINT nIDResource, DWORD dwDefaultStyle,CWnd* pParentWnd, CCreateContext* pContext);

protected:
	BOOL RestoreDispInfo();
	BOOL SaveDispInfo();

// 覆盖
public:
	virtual void SwitchToTitle(int nViewNo);
	virtual void SwitchToMenu(int nViewNo);
	virtual void SwitchToToolBar(int nViewNo);

	// ClassWizard 生成虚拟函数的覆盖。
	//{{AFX_VIRTUAL(CVwBaseSFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void ActivateFrame(int nCmdShow = -1);
	virtual void OnSetPreviewMode(BOOL bPreview, CPrintPreviewState* pModeStuff);

	virtual void OnEventDBConnected() {};
	virtual void OnEventDBDisConnect() {};
	//}}AFX_VIRTUAL

// 实现
public:
	virtual ~CVwBaseSFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// 生成的消息映射函数
protected:
	//{{AFX_MSG(CVwBaseSFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnDBConnected();
	afx_msg void OnUpdateDBConnected(CCmdUI *pCmdUI);
	afx_msg void OnDBDisconnect();
	afx_msg void OnUpdateDBDisconnect(CCmdUI *pCmdUI);
	afx_msg LRESULT OnDBConnected(WPARAM wp, LPARAM lp);
	afx_msg LRESULT OnDBDisConnected(WPARAM wp, LPARAM lp);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#undef AFX_DATA
#define AFX_DATA

////////////////////////////////////////////
// 关于菜单和CoolBar栏的宏定义

	////////////////////////
	// 菜单定义的开始
	#define		BEGIN_VW_MENU()	\
						{ if (m_pMenuItem) {delete m_pMenuItem; m_pMenuItem = NULL;}	\
							try {m_pMenuItem = new CVwMenuItem;

	// 弹出项目（表示持有子菜单）
	#define		DEF_VW_MENU_POPUP(str)	\
						m_pMenuItem->Add(str,NULL,MF_POPUP);

	// 普通菜单项目
	#define		DEF_VW_MENU_ITEM(str,id) \
						m_pMenuItem->Add(str,id);

	// 设定全部标志的
	#define		DEF_VW_MENU_ITEM_EX(str,id,operation,flag,str_op) \
						m_pMenuItem->Add(str,id,flag,operation,str_op);

	// 分隔符
	#define		DEF_VW_MENU_SEPARATOR()	\
						m_pMenuItem->Add(NULL,NULL,MF_SEPARATOR,VW_MENU_OPERATION_ALWAYS),

	// 菜单定义结束
	#define		END_VW_MENU()	} catch (...) { /* 现在什么都不做 */ } }

	//////////////////////////////////////////////////////////////////
	// 菜单删除项目定义的开始（指定菜单的资源ID）
	#define		BEGIN_VW_MENU_DELETE(id) BEGIN_VW_MENU() m_nMenuID = id;

	// 菜单删除项目定义

	#define		DEF_VW_MENU_DELETE(id,operation)	\
						m_pMenuItem->Add(NULL,id,NULL,operation);

	// 设定全部标志
	#define		DEF_VW_MENU_DELETE_EX(id,operation,str_op)	\
						m_pMenuItem->Add(NULL,id,NULL,operation,str_op);

	// 菜单删除项目定义结束
	#define		END_VW_MENU_DELETE()	END_VW_MENU()


	///////////////////////////
	// CoolBar定义的开始
	#define		BEGIN_VW_COOLBAR()	\
						{ if (m_pCoolBarItem) {delete m_pCoolBarItem; m_pCoolBarItem = NULL;}	\
							try {m_pCoolBarItem = new CVwCoolbarItem;

	// CoolBar按钮（通常）
	#define		DEF_VW_COOLBAR_ITEM(id,bitmap)	\
						m_pCoolBarItem->Add(id,bitmap);

	// CoolBar按钮（扩展）
	#define		DEF_VW_COOLBAR_ITEM_EX(id,bitmap,str,isdepend)	\
						m_pCoolBarItem->Add(id,bitmap,str,isdepend);

	// 分隔符
	#define		DEF_VW_COOLBAR_SEPARATOR()	\
						m_pCoolBarItem->Add(ID_SEPARATOR,NULL);

	// CoolBar定义结束
	#define		END_VW_COOLBAR()		} catch (...) {/* 现在什么都不做 */} }


///////////////////////////////////////////////////////
//多视图支持宏
//ChangeView（）使用所需的手续。

#define		VW_INIT_VIEW(num)	\
		{												\
			m_pViewClass = new CRuntimeClass*[num];		\
			m_pView = new CView*[num];					\
			for (int i = 0; i < num; i++)				\
				m_pView[i] = NULL;						\
			m_nViewNumber = num;						\
		}


#define		VW_INIT_VIEWCLASS(num,class)	\
				m_pViewClass[num] = RUNTIME_CLASS(class);

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio 在上一行之前插入追加宣言。

#endif // !defined(AFX_RFXFRM_H__CA2BDABA_F703_11D2_8BBB_00105A14616F__INCLUDED_)

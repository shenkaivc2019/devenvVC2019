#pragma once

/////////////////////////////////////////////////////////////////////////////
// CVwBaseSApp:

#ifndef __AFXWIN_H__
#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

class CVwBaseSApp : public CWinApp
{
public:
	CVwBaseSApp();
	virtual ~CVwBaseSApp();

	int		m_nClientNo;
	int		m_nShowWnd;
	LPSTR	m_pszWndClassName;
public:
	CConnectDataBase*	m_pdbConnection;
	bool m_bDBAutoConnect;
protected:
	BOOL	m_bSrvCom;
	int		m_nExecFlag;
	BOOL	m_bIsCheckDouble;		// 要检查双重启动吗？
	US_EXECUTEINFO* m_pgExecInfo;	// 命令行信息
	BOOL	m_bIsDirectOK;			// 直接启动许可标志（FALSE：必须从菜单开始）
	DWORD	m_dwOptionNumber;		// 选项编号（当前）
	DWORD	m_dwOptionNumberOrg;	// 选项编号的初始值
	CString	m_strUserCode;			// 个人代码
	DWORD	m_dwOperationLevel;		// 操作级别
	DWORD	m_dwUserLevel;			// 操作员级别
	int		m_nMenuNo;				// 主菜单编号
	int		m_nSubMenuNo;			// 子菜单编号
	BOOL	m_bAutoFlag;			// 自动启动标志
	CString m_strNextPrgID;			// 下一个启动程序ID（在GfxOpen Exec中使用）
	CString m_strAddCmd;			// 追加命令（在GfxOpen Exec中使用）
protected:
	void SetExeName();

	BOOL InitApp();	
	void _xa_MakeHelpFileName(char** lpszHelpFilePath);
	BOOL AnalyzeCommandLine();
public:
	void OperaterLogOut(int nOperationKind);
	CString GetPrivateKey(LPCTSTR lpszSubkey);
	CString GetPrivateKey2(LPCTSTR szSubKey);
	int  GetRegistryInt(LPCTSTR lpszSubkey, LPCTSTR lpszValueName, int nDefault);
	void SetRegistryInt(LPCTSTR lpszSubkey, LPCTSTR lpszValueName, int nValue);
	CString GetRegistryString(LPCTSTR lpszSubkey, LPCTSTR lpszValueName, LPCTSTR lpszDefault);
	void SetRegistryString(LPCTSTR lpszSubkey, LPCTSTR lpszValueName, LPCTSTR lpszValue);
	void ResetGridInfo(int nColMax, int nFixCol, int nOptionNo, int nSeqNo=-1);
	US_EXECUTEINFO* GetExecInfo() {return m_pgExecInfo;}
	DWORD GetOperationLevel() {return m_dwOperationLevel;}
	DWORD GetUserLevel() {return m_dwUserLevel;}
	DWORD GetOptionNumber() {return m_dwOptionNumber;}
	DWORD GetOptionNumberOrg() {return m_dwOptionNumberOrg;}
	const CString GetUserCode() {return m_strUserCode;}
	int GetMenuNo() {return m_nMenuNo;}
	int GetSubMenuNo() {return m_nSubMenuNo;}
	BOOL GetAutoFlag() {return m_bAutoFlag;}
	void SetOptionNumber(DWORD dwNewNumber) {m_dwOptionNumber = dwNewNumber;}
	void SetNextPrgID(LPCSTR lpszNextPrgID) {m_strNextPrgID = lpszNextPrgID;}
	void SetAddCmd(LPCSTR lpszAddCmd) {m_strAddCmd = lpszAddCmd;}
protected:
	virtual BOOL BeginDBConnection();
	virtual void EndDBConnection();
	//{{AFX_VIRTUAL(CVwBaseSApp)
	public:
	virtual BOOL InitInstance(int nExecFlag = -1);
	virtual int ExitInstance();
	virtual int Run();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CVwBaseSApp)
	afx_msg void OnAppAbout();
	afx_msg void OnHelpIndex();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

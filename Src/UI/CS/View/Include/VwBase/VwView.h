// RfxView.h : CVwBaseView 基底クラスの宣言およびインターフェイスの定義をします。
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_RFXVIEW_H__CA2BDABE_F703_11D2_8BBB_00105A14616F__INCLUDED_)
#define AFX_RFXVIEW_H__CA2BDABE_F703_11D2_8BBB_00105A14616F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <CGet21GridInfo.h>

#pragma pack(push, 1)

struct GRIDSIZE_INFO {						// グリッドカラム幅情報
	UINT			uGridID;
	CGet21GridInfo*	pGridInfo;
};
typedef CArray <GRIDSIZE_INFO, GRIDSIZE_INFO&> CGridInfoArray;

#pragma pack(pop)

#undef AFX_DATA
#ifdef _RFXBASE_BUILD
	#define AFX_DATA AFX_EXT_DATA
#else
	#define AFX_DATA AFX_DATA_IMPORT
#endif

class CVwBaseView : public CFormView
{
	DECLARE_DYNAMIC(CVwBaseView)
protected: // シリアライズ機能のみから作成します。
	CVwBaseView(UINT nIDTemplate);           // 動的生成に使用されるプロテクト コンストラクタ。

public:
	//{{AFX_DATA(CVwBaseView)
		// メモ: ClassWizard によってこの位置にデータ メンバが追加されます。
	//}}AFX_DATA

// アトリビュート
public:
	CVwBaseDoc* GetDocument();

	BOOL	m_bModify;
	BOOL	m_bNeedSave;
	int		m_nOptionNo;

	void	AddGridInfo(UINT uGridID);
	void	RestoreGridInfo();
	void	SaveGridInfo();
	void	ResetGridInfo();

	CSize	m_sizeScreen;

protected:
	CGridInfoArray	m_arrGridInfo;

	BOOL IsGridFromCWnd(CWnd* pWnd, CWnd** pWndGrid);

// オペレーション
public:
	void SetScreenSize(const CSize& size) {m_sizeScreen = size;}
	void SetScreenSize(const int x, const int y) {m_sizeScreen = CSize(x, y);}

	void GetScreenRect(LPRECT lpRect);

// オーバーライド
public:
	virtual void TrackPopupMenu(CPoint point);
	virtual BOOL IsDataChanged();
	virtual BOOL UpdateViewData(CVwBaseDoc* pDoc);
	virtual BOOL SaveViewData(CVwBaseDoc* pDoc);
	virtual BOOL CheckViewData(CVwBaseDoc* pDoc);


	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CVwBaseView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV のサポート
	//}}AFX_VIRTUAL

// インプリメンテーション
public:
	virtual ~CVwBaseView();
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);

protected:

// 生成されたメッセージ マップ関数
protected:
	//{{AFX_MSG(CVwBaseView)
	afx_msg void OnDestroy();
	afx_msg void OnFilePrint();
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnEditCut();
	afx_msg void OnUpdateEditCut(CCmdUI* pCmdUI);
	afx_msg void OnEditPaste();
	afx_msg void OnUpdateEditPaste(CCmdUI* pCmdUI);
	afx_msg void OnEditTitleCopy();
	afx_msg void OnUpdateEditTitleCopy(CCmdUI* pCmdUI);
	afx_msg void OnEditUnderCopy();
	afx_msg void OnUpdateEditUnderCopy(CCmdUI* pCmdUI);
	afx_msg void OnEditInsert();
	afx_msg void OnEditDelete();
	afx_msg void OnUpdateEditInsertDelete(CCmdUI* pCmdUI);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnOptionCellReset();
	afx_msg void OnEditSheet();
	afx_msg void OnUpdateEditSheet(CCmdUI* pCmdUI);
	virtual afx_msg void OnChangeItems();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#undef AFX_DATA
#define AFX_DATA

#ifndef _DEBUG  // RfcBaseVw.cpp ファイルがデバッグ環境の時使用されます。
inline CVwBaseDoc* CVwBaseView::GetDocument()
   { return (CVwBaseDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_RFXVIEW_H__CA2BDABE_F703_11D2_8BBB_00105A14616F__INCLUDED_)

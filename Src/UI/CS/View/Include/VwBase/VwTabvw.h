#if !defined(AFX_RFXTABVW_H__2A110665_7F40_11D4_ACBE_00B0D06909A4__INCLUDED_)
#define AFX_RFXTABVW_H__2A110665_7F40_11D4_ACBE_00B0D06909A4__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// RfxTabvw.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// CVwBaseTabView フォーム ビュー

// タブの余白
#define RFX_TABVIEW_TOP							(8)										// 上
#define RFX_TABVIEW_LEFT						(8)										// 左
#define RFX_TABVIEW_RIGHT						(8)										// 右
#define RFX_TABVIEW_BOTTOM						(8)										// 下
#define RFX_TABVIEW_HEIGHT						RFX_TABVIEW_TOP+RFX_TABVIEW_BOTTOM		// 高さ
#define RFX_TABVIEW_WIDTH						RFX_TABVIEW_LEFT+RFX_TABVIEW_RIGHT		// 幅

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include <VwSheet.h>

#undef AFX_DATA
#ifdef _RFXBASE_BUILD
	#define AFX_DATA AFX_EXT_DATA
#else
	#define AFX_DATA AFX_DATA_IMPORT
#endif

class CVwBaseTabView : public CVwBaseView
{
	DECLARE_DYNAMIC(CVwBaseTabView)
protected:
	CVwBaseTabView(UINT nIDTemplate);           // 動的生成に使用されるプロテクト コンストラクタ。
	~CVwBaseTabView();

// フォーム データ
public:
	//{{AFX_DATA(CVwBaseTabView)
		// メモ: ClassWizard はこの位置にデータメンバを追加します。
	//}}AFX_DATA

// アトリビュート
public:

protected:
	CVwBaseSheet*		m_pSheet;

// オペレーション
public:
	void AddPage(CVwBasePage* pPage);

	CVwBaseSheet* GetSheetCtrl();

	CVwBasePage* GetActivePage();
	BOOL SetActivePage(CVwBasePage* pPage);

	void SetScreenSize(const CSize& size);
	void SetScreenSize(const int x, const int y) { SetScreenSize(CSize(x, y)); }

// オーバーライド
	virtual void CreatePropertyPage();

	virtual BOOL IsDataChanged();
	virtual BOOL UpdateViewData(CVwBaseDoc* pDoc);
	virtual BOOL SaveViewData(CVwBaseDoc* pDoc);
	virtual BOOL CheckViewData(CVwBaseDoc* pDoc);

	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CVwBaseTabView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート
	//}}AFX_VIRTUAL

// インプリメンテーション

	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CVwBaseTabView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnOptionCellReset();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CVwBaseTabViewEx フォーム ビュー

class CVwBaseTabViewEx : public CVwBaseTabView
{
	DECLARE_DYNAMIC(CVwBaseTabViewEx)
protected:
	CVwBaseTabViewEx(UINT nIDTemplate);           // 動的生成に使用されるプロテクト コンストラクタ。
	~CVwBaseTabViewEx();

// アトリビュート
protected:
	CVwBaseSheetEx*	m_pSheetEx;

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CVwBaseTabViewEx)
	//}}AFX_VIRTUAL

// インプリメンテーション

	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CVwBaseTabViewEx)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#undef AFX_DATA
#define AFX_DATA

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_RFXTABVW_H__2A110665_7F40_11D4_ACBE_00B0D06909A4__INCLUDED_)

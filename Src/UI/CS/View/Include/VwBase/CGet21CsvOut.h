#ifndef _GET21_GET21CSVOUT_H_
	#define _GET21_GET21CSVOUT_H_


/////////////////////////////////////////////////////////////////////////////
// IMPORT/EXPORT
//

#define REG_GETINI			"\\getini\\GENERAL\\"
#define	MAX_TEMPPATH		(256)
#define	CSVFILE_NAME		"Get.csv"
#define	EXECUTE_PROGRAM		"excel"
#define	EXECUTE_OPTION		"open"
#define SHEETSETUP_TITLE				(short)0					// 
#define SHEETSETUP_DQUOTATION 			(short)1					// 
#define SHEETSETUP_LSPCCUT				(short)2					// 
#define SHEETSETUP_YEAR4FIGURE			(short)3					// 
#define SHEETSETUP_TIME100UNIT			(short)4					// 
#define SHEETSETUP_TIMESIGN				(short)5					// 
#define SHEETSETUP_MONEYSIGN			(short)6					// 
#define SHEETSETUP_DISPTYPE				(short)7					// 
#define SHEETSETUP_ITEM_MAX				(short)8					// 

#define SHEETSETUP_CHECK_OFF			(short)0					// 
#define SHEETSETUP_CHECK_ON				(short)1					// 

#define SHEETSETUP_DISPTYPE_NONE		(short)0					// 
#define SHEETSETUP_DISPTYPE_TIME		(short)1					// 

#define SHEETSETUP_STRING_DISPTYPE_TIME	_T("__:__")					// 
#define SUBKEY_SHEETSETUP				_T("SheetSetup")
#define SUBKEY_SHEETSETUP_ALL			_T("Xg0000\\") SUBKEY_SHEETSETUP
#define VALNAME_TITLE					_T("GridTitle")
#define VALNAME_DQUOTATION				_T("DQuotation")
#define VALNAME_LSPCCUT					_T("LSpaceCut")
#define VALNAME_YEAR4FIGURE				_T("Year4Figure")
#define VALNAME_TIME100UNIT				_T("Time100Unit")
#define VALNAME_TIMESIGN				_T("TimeSign")
#define VALNAME_MONEYSIGN				_T("MoneySign")
#define VALNAME_DISPTYPE				_T("DispType")

#undef AFX_DATA
#define AFX_DATA AFX_EXT_DATA

class CGet21CsvOut
{

public:
	CGet21CsvOut();
	~CGet21CsvOut();

	int ExcelExecute(void* pVhGrid);
	int GetGridData(VARIANT varData, CString& strCsvBuff);
};

#undef AFX_DATA
#define AFX_DATA

#endif // _GET21_GET21CSVOUT_H_

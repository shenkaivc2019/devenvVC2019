﻿#if !defined(AFX_MYRECENTFILELIST_H__B28E63E0_03BF_11D4_8690_0050BABA4C66__INCLUDED_)
#define AFX_MYRECENTFILELIST_H__B28E63E0_03BF_11D4_8690_0050BABA4C66__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MyRecentFileList.h : header file
//
#include <afxadv.h>
/////////////////////////////////////////////////////////////////////////////
// CMyRecentFileList dialog
class CMyRecentFileList : public CRecentFileList
{
public:
	CMyRecentFileList(UINT nStart, LPCTSTR lpszSection,
				   LPCTSTR lpszEntryFormat, int nSize,
				   int nMaxDispLen = AFX_ABBREV_FILENAME_LEN)
				   : CRecentFileList(nStart, lpszSection, lpszEntryFormat, nSize,
				   nMaxDispLen) {};
   virtual void UpdateMenu(CCmdUI* pCmdUI);
   virtual void Remove( int nIndex );
   //virtual void WriteList();
   //virtual void ReadList();
};  

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYRECENTFILELIST_H__B28E63E0_03BF_11D4_8690_0050BABA4C66__INCLUDED_)

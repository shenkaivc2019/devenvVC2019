//---------------------------------------------------------------------------//
// ファイル名:	RfxMenu.h
// 説明：	メニュー作成クラス定義
// 会社名：	株式会社マックインターフェイス
// 作成者：	山本裕太
// 作成日：	1998/8/26
// 備考:	なし
//---------------------------------------------------------------------------//

#ifndef _VW_MENU_H_
	#define	_VW_MENU_H_

////////////////////////
// 変更履歴
//   1999/6/2   ・クラスヘッダを規約にあわせました。
//   1999/1/23  ・「一時的な定義」というコメントを削除しました。
//              ・その他コメント化されている定義を削除しました。
//   1999/1/21  ・IsViewItem() を追加しました。
//   1999/1/14  ・ファイルヘッダを規約にあわせました。


///////////////////////////////////
// 操作レベルの定義
#define		VW_OPERATION_DISABLE			(0x00)		// 選択不可
#define		VW_OPERATION_SHOW			(0x01)		// 表示のみ
#define		VW_OPERATION_MODIFY			(0x02)		// 修正可能
#define		VW_OPERATION_SET				(0x03)		// 設定可能
#define		VW_OPERATION_ALWAYS			(0x04)		// いつでも表示

// 1999/1/7 定義を追加しました。
#define		VW_MENU_OPERATION_DISABLE	(0x04)		// いつでも無効
#define		VW_MENU_OPERATION_ALWAYS		(0x00)		// いつでも表示


//////////////////////////////////////////////////////////////////////
// 特殊なメニューフラグ
//   Windows標準のMF_xxシリーズと重ならないように注意してください。

// リソースIDを指定するフラグ。
//   VW_MENUITEMのdwMenuFlagにこれを指定すると，
//   LoadStringが行なわれます。
#define		VW_MF_LOADSTRING		(0x10000000)

// ポップアップで使用する項目フラグ
//   VW_MENUITEMのdwMenuFlagにこれを指定すると，
//   m_pPopupMenuにも追加されます
#define		VW_MF_USEPOPUP		(0x20000000)


#define		VW_OPTIONNUMBER_ALL	(-1)


//---------------------------------------------------------------------------//
// 日本語構造体名：	メニュー項目
// 説明：	メニュー項目クラスで使用されます。
// 備考：	なし
//---------------------------------------------------------------------------//
typedef struct
{
	DWORD	dwMenuFlag;			// メニューのフラグ
	UINT	nCommandID;			// メニューのコマンドID
	LPCSTR	lpszString;			// 表示文字列

	DWORD	dwOperationLevel;	// 操作レベル

	LPCSTR lpszOptionNumber;	// オプションナンバーについての記述
								// ex: "1-31,50,60" など。

} VW_MENUITEM, *LPVW_MENUITEM;


#undef AFX_DATA
#ifdef _RFXBASE_BUILD
	#define AFX_DATA AFX_EXT_DATA
#else
	#define AFX_DATA AFX_DATA_IMPORT
#endif

//---------------------------------------------------------------------------//
// 日本語クラス名:	メニュー項目クラス
// 説明:	メニュー項目クラス
// 備考:	なし
//---------------------------------------------------------------------------//
class CVwMenuItem
{
protected:

	CArray <VW_MENUITEM, VW_MENUITEM&> m_array;

public:

	CVwMenuItem() {m_array.RemoveAll();}
	~CVwMenuItem() { m_array.RemoveAll();}

	BOOL Add(LPCSTR lpszString, UINT nCommandID, DWORD dwMenuFlag = MF_STRING,
						DWORD dwOperationLevel = 0, LPCSTR lpszOptionNumber = NULL);

	VW_MENUITEM& operator[](int index) { return m_array[index]; }
	VW_MENUITEM GetAt(int index) { return m_array.GetAt(index); }
	int GetSize() {return m_array.GetSize();}

	void RemoveAll() {m_array.RemoveAll();}
};


//
//---------------------------------------------------------------------------//
// 日本語構造体名：	メニュー削除用構造体
// 説明：	メニュー削除クラスで使用されます。
// 備考：	なし
//---------------------------------------------------------------------------//
typedef struct
{
	UINT	nCommandID;			// 消す項目のコマンドID
	DWORD	dwOperationLevel;	// 操作レベル
	LPCSTR	lpszOptionNumber;

} VW_DELETE_MENUITEM, *LPVW_DELETE_MENUITEM;



//---------------------------------------------------------------------------//
// 日本語クラス名:	メニュー削除クラス
// 説明:	メニュー削除クラス
// 備考:	なし
//---------------------------------------------------------------------------//
class CVwDeleteMenuItem
{
protected:
	CArray <VW_DELETE_MENUITEM, VW_DELETE_MENUITEM&> m_array;

public:
	CVwDeleteMenuItem() {m_array.RemoveAll();}
	~CVwDeleteMenuItem(){m_array.RemoveAll();}

	BOOL Add(UINT nCommandID, DWORD dwOperationLevel = VW_OPERATION_ALWAYS, LPCSTR lpszOptionNumber = NULL);

	VW_DELETE_MENUITEM& operator[](int index) { return m_array[index]; }
	VW_DELETE_MENUITEM GetAt(int index) { return m_array.GetAt(index); }
	int GetSize() {return m_array.GetSize();}

	void RemoveAll() {m_array.RemoveAll();}
};


//---------------------------------------------------------------------------//
// 日本語クラス名:	メニュークラス
// 説明:	メニュークラス
// 備考:	なし
//---------------------------------------------------------------------------//
class  CVwMenu
{
public:
	CVwMenu();
	~CVwMenu();

	/////////////////
	// メンバ関数

	// メニュー作成（リソースを使用しない場合）
	BOOL CreateMenu(CWnd* pWnd, CVwMenuItem* pMenuItem,
				DWORD dwOptionNumber = VW_OPTIONNUMBER_ALL, DWORD dwOperationLevel = VW_OPERATION_ALWAYS);

	// メニュー作成（リソースを使用する場合）
	BOOL CreateMenu(CWnd* pWnd, UINT nResourceID, CVwMenuItem* pMenuItem,
				DWORD dwOptionNumber = VW_OPTIONNUMBER_ALL, DWORD dwOperationLevel = VW_OPERATION_ALWAYS);

	BOOL CreatePopupMenu(CWnd* pWned, DWORD dwOptionNumber = VW_OPTIONNUMBER_ALL, DWORD dwOperationLevel = VW_OPERATION_ALWAYS);

	// サブメニューの取得(CMenuとおなじ)
	CMenu* GetSubMenu(int nPos);

	// メインメニューの取得
	CMenu* GetMenu() {return m_pMenu;}

protected:

	BOOL IsViewItem(VW_MENUITEM& menu, DWORD dwOperationLevel, DWORD lpszOptionNumber);

	CMenu*	m_pMenu;			// 内部で持ってるメニュー
	CWnd*	m_pWnd;				// m_pMenuを結び付けるウィンドウ
	CMenu*	m_pSubMenu;			// m_pMenuにはいってるサブメニュー

	int		m_nSubmenuMax;		// サブメニューの数

};

#undef AFX_DATA
#define AFX_DATA

#endif // _VW_MENU_H_

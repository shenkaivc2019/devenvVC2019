//// 共通定義ファイル
//
//#ifndef _RFX_DB_H_
//#define	_RFX_DB_H_
//
//// インポート／エクスポートの定義
//#ifdef RFX_TBLREC_BUILD
//	#define		RFX_TBLREC_CLASS		AFX_CLASS_EXPORT
//#else
//	#define		RFX_TBLREC_CLASS		AFX_CLASS_IMPORT
//#endif
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// 桁数
//
//#define	TITLE_DIGITS							32								// タイトル桁数
//#define	NAME_DIGITS								10								// 名称桁数
//#define	TIMECARDCODE_DIGITS						15								// タイムカードコード桁数
//#define	WORDNAME_DISITS							30								// 氏名(漢字)桁数
//#define	BYTENAME_DIGITS							30								// 氏名（ｶﾅ）桁数
//#define	COMPANY_DIGITS							2								// 会社コード桁数
//#define	DEPCODEHIGH_DIGITS						8								// 大階層所属コード桁数
//#define	DEPCODEMID_DIGITS						7								// 中階層所属コード桁数
//#define	DEPCODELOW_DIGITS						6								// 小階層所属コード桁数
//#define	DEPCODEALL_DIGITS						10								// 総所属コード桁数
//// 2010.04.27 Ver1.00 機能変更【UG1_003】Start
//#define DEPCODEALL_DIGITS_UG					27								// UG総所属コード桁数
//// 2010.04.27 Ver1.00 機能変更【UG1_003】End
//#define	WARDEANEST_DIGITS						1								// 区切記号桁数
//#define	ENCLOSURE_DIGITS						1								// 囲み記号桁数
//#define	DEFID_DIGITS							5								// 定義体ＩＤ桁数
//#define PUNCH_DATA_SIZE							64								// 打刻データ桁数
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// 範囲
//
//#define	ITEM_ATX30MSG							80								// ATX-30 個人メッセージ演算項目
//#define	TATX30MSG_NONE							0								// none code
//#define	TATX30MSG_MIN							1								// min. code
//#define	TATX30MSG_MAX							(ITEM_ATX30MSG)					// max. code
//
//#define	MAINMANU_NO_MIN							0								// メインメニューNo.最小値
//#define	MAINMANU_NO_MAX							99								// メインメニューNo.最大値
//#define	SUBMANU_NO_MIN							0								// サブメニューNo.最小値
//#define	SUBMANU_NO_MAX							99								// サブメニューNo.最大値
//
//// 事由コード
//#define	REZNCODE_NONE							0								// なし
//#define	REZNCODE_MIN							1								// 打刻事由コード最小値
//#define	REZNCODE_MAX							999								// 打刻事由コード最大値
//
//// スケジュール項目
//#define	SCHEDULE_ITEM_NONE						0L								// なし
//#define	SCHEDULE_ITEM_MIN						9001L							// スケジュール項目No.最小値
//#define	SCHEDULE_ITEM_MAX						9999L							// スケジュール項目No.最大値
//
//#define	FIO_RECORD_MIN							1								// レコード長最大値
//
//#define	SFTPRM_PARAMNO_MAX						64								// 勤務区分パラメータ種別最大値
//
//																				// 会社マスター
//#define RFX_COMPANY_MAX							100								// max. record
//#define RFX_COMPANYCODE_MIN						0								// min. code
//#define RFX_COMPANYCODE_MAX						(RFX_COMPANY_MAX - 1)			// max. code
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// カタログNo.範囲
//
//// 勤務スケジュール
//#define	ITEM_SCHEDULE_PERIOD					10								// 個人単位の期間
//#define	SCHEDULE_PERIOD_MIN						10								// 個人単位の期間最小値
//#define	SCHEDULE_PERIOD_MAX						19								// 個人単位の期間最大値
//
//#define	ITEM_SCHEDULE_PLURAL					10								// 複数個人の１日
//#define	SCHEDULE_PLURAL_MIN						20								// 複数個人の１日最小値
//#define	SCHEDULE_PLURAL_MAX						29								// 複数個人の１日最大値
//
//// ジョブ
//#define	ITEM_JOBCATALOG_THROW					10								// 投入工数表
//#define	JOB_CATALOGNO_THROW_MIN					10								// 投入工数表最小値
//#define	JOB_CATALOGNO_THROW_MAX					19								// 投入工数表最大値
//
//#define	ITEM_JOBCATALOG_WORK					10								// ワーク別投入工数表
//#define	JOB_CATALOGNO_WORK_MIN					20								// ワーク別投入工数表最小値
//#define	JOB_CATALOGNO_WORK_MAX					29								// ワーク別投入工数表最大値
//
//#define	ITEM_JOBCATALOG_SUPPOR					10								// 応援工数表
//#define	JOB_CATALOGNO_SUPPOR_MIN				30								// 応援工数表最小値
//#define	JOB_CATALOGNO_SUPPOR_MAX				39								// 応援工数表最大値
//
//#define	ITEM_JOBCATALOG_RECEIVE					10								// 授援工数表
//#define	JOB_CATALOGNO_RECEIVE_MIN				40								// 授援工数表最小値
//#define	JOB_CATALOGNO_RECEIVE_MAX				49								// 授援工数表最大値
//
//#define	ITEM_JOBCATALOG_DIRECT					10								// 授援工数表
//#define	JOB_CATALOGNO_DIRECT_MIN				50								// 授援工数表最小値
//#define	JOB_CATALOGNO_DIRECT_MAX				59								// 授援工数表最大値
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// その他項目
//
//#define	ITEM_WORKDEF							2								// ワークテーブル自由定義項目
//
//
//// 罫線
//#define	LINE_NONE								0								// なし
//#define	LINE_SLIM_SOLID							1								// 細・実線
//#define	LINE_THICK_SOLID						2								// 太・実線
//#define	LINE_SLIM_BROKEN						3								// 細・破線
//
//// 日毎データ有無
//#define DAILY_OFF								0								// 出力しない
//#define DAILY_ON								1								// 出力する
//
//// 処理日指定
//#define	INPUTDATE_OFF							0								// 処理期間
//#define	INPUTDATE_ON							1								// 処理月
//
//// 承認欄有無
//#define	SIGNARIA_OFF							0								// 出力しない
//#define	SIGNARIA_ON								1								// 出力する
//
//// 入出力データ種別
//#define	DATATYPE_INDIVI							0								// 個人マスター
//#define	DATATYPE_DEPMNT							10								// 所属マスター
//#define	DATATYPE_POSTTB							11								// 役職テーブル
//#define	DATATYPE_WORKTB							12								// ワークテーブル
//#define	DATATYPE_DAILY							20								// デイリーデータ
//#define	DATATYPE_DAYJOB							21								// ジョブデイリーデータ
//#define	DATATYPE_ATTLDG							30								// 就業台帳
//#define	DATATYPE_JOBLDG							31								// ジョブ台帳
//#define	DATATYPE_MONTHLG						32								// 月次台帳
//#define	DATATYPE_PAYLDG							33								// 給与台帳
//#define	DATATYPE_BONUSLG						50								// 賞与台帳
//#define	DATATYPE_TAXLDG							60								// 年調台帳
//#define	DATATYPE_INSLDG							70								// 社会保険台帳
//
////外部ﾃﾞｰﾀ入力未処理者ﾘｽﾄｴﾗｰｺｰﾄﾞ
//#define GIEXERR_ERR_01							1								// データにエラーがあります
//#define GIEXERR_ERR_02							2								// 処理期間が一致しません
//#define GIEXERR_ERR_03							3								// 個人マスターに登録されていません
//#define GIEXERR_ERR_04							4								// 氏名が登録されていません
//#define GIEXERR_ERR_05							5								// 所属が登録されていません
//#define GIEXERR_ERR_06							6								// まだ入社していません
//#define GIEXERR_ERR_07							7								// 既に退職しています
//#define GIEXERR_ERR_08							8								// 既に退職処理済みです
//#define GIEXERR_ERR_09							9								// データは既に使用されています
//#define GIEXERR_ERR_10							10								// 最大数まで登録されています
//
//// 入力項目
//#define SELITEM_MST								1								// 個人マスター項目
//#define SELITEM_DAY								2001							// デイリーデータ項目
//#define SELITEM_ATT								3001							// 就業台帳項目
//#define SELITEM_PAY								4001							// 給与台帳項目
//#define SELITEM_BNS								5001							// 賞与台帳項目
//#define SELITEM_TAX								6001							// 年調台帳項目
//#define SELITEM_INS								7001							// 社会保険台帳項
//
//// 強調
//#define BOLD_OFF								0								// なし
//#define BOLD_ON									1								// あり
//
//// 色設定値
//#define	COLOR_DEFAULT							-1								// カラー初期値（システムカラー）
//#define	COLOR_MINCHECK							0								// カラー最小値
//#define	COLOR_MAXCHECK							255								// カラー最大値
//
//// タグ種別
//#define	TAGKIND_DB								0								// ＤＢ項目データ
//#define	TAGKIND_NOTDB							1								// ＤＢ以外データ
//#define	TAGKIND_STRING							2								// 固定文字
//
//// フォント名称
//#define	FONT_NAME_MSMIN							0								// ＭＳ明朝
//#define	FONT_NAME_NONE							1								// なし
//
//// フォントサイズ
//#define	FONT_SIZE0								0								// 7.5
//#define	FONT_SIZE1								1								// 9
//#define	FONT_SIZE2								2								// 14
//#define	FONT_SIZE3								3								// なし
//
//// 網掛けフラグ
//#define	NETFLAG_NOMAL							0								// 通常
//#define	NETFLAG_ON								1								// 網掛け
//
//// 出力形式(種別)
//#define OUTPUT_CHAR								0								// 文字列
//#define OUTPUT_FIG_A							1								// 数値(変換なし)
//#define OUTPUT_FIG_B							2								// 数値(小数点１桁)
//#define OUTPUT_FIG_C							3								// 数値(小数点２桁)
//#define OUTPUT_MONEY							4								// 金額
//#define OUTPUT_DATE_A							5								// 年月日(ZZZ9/Z9/Z9)
//#define OUTPUT_DATE_B							6								// 年月日(ZZZ9年Z9月Z9日)
//#define OUTPUT_DATE_C							7								// 年月日(和暦)
//#define OUTPUT_DATE_D							8								// 年月(ZZZ9/Z9)
//#define OUTPUT_DATE_E							9								// 年月(ZZZ9年Z9月)
//#define OUTPUT_DATE_F							10								// 年月日(和暦)
//#define OUTPUT_DATE_G							11								// 月日(Z9/Z9日)
//#define OUTPUT_DATE_H							12								// 月日(Z9月Z9日)
//#define OUTPUT_TIME_60							13								// 時間(60進)
//#define OUTPUT_TIME_100							14								// 時間(100進)
//#define OUTPUT_STR_FULL							15								// 半固定文字(全角)
//#define OUTPUT_STR_HALF							16								// 半固定文字(半角)
//
//// 出力形式(０埋)
//#define	OUTPUT_ZERO_OFF							0								// ゼロ埋めなし
//#define	OUTPUT_ZERO_ON							1								// ゼロ埋めあり
//
//// 出力形式(出力位置)
//#define LOCATE_LEFT								0								// 左詰
//#define LOCATE_CENTER							1								// 中央
//#define LOCATE_RIGHT							2								// 右詰
//
//// 出力形式(ｸﾘｱ指定)
//#define	CLEAR_ZERO								0								// データが0の場合、"0"を印字
//#define	CLEAR_NONE								1								// データが0の場合、印字なし
//
//// 略称フラグ
//#define	TV_NICKNAME_OFF							0								// なし
//#define	TV_NICKNAME_ON							1								// あり
//
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// 構造体
//
//// 座標軸
//typedef	struct tagAXIS
//{
//	short	nX;																	// X軸
//	short	nY;																	// Y軸
//} AXIS;
//
//// 色設定
//typedef struct tagCOLOR
//{
//	short	nRed;																// 赤
//	short	nGreen;																// 緑
//	short	nBlue;																// 青
//} COLOR;
//
//// 開始、終了時刻
//typedef struct tagHM
//{
//	short	nHMStart;															// 開始時刻
//	short	nHMEnd;																// 終了時刻
//} HM;
//
//
//// 2003.02.18 Ver4.10 【G4_014】
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// 新届出ワークフロー共通定義
//
//// 承認段階数
//#define NEWRPT_LEVEL_NONE                       0                              // none code
//#define NEWRPT_LEVEL_MIN                        1                              // min. code
//#define NEWRPT_LEVEL_MAX                        5                              // max. code
//
//// 承認待ち段階数
//#define NEWRPT_NEEDAPPRO_LEVEL_MIN              (NEWRPT_LEVEL_MIN - 1)         // min. code
//#define NEWRPT_NEEDAPPRO_LEVEL_MAX              (NEWRPT_LEVEL_MAX - 1)         // max. code
//
//// 届出種別区分
//#define NEWRPT_REPORTKIND_MAX                   100                            // max. record
//#define NEWRPT_REPORTKINDCODE_MIN               0                              // min. code
//#define NEWRPT_REPORTKINDCODE_MAX               (NEWRPT_REPORTKIND_MAX - 1)    // max. code
//
//// 承認ルートNo.
//#define NEWRPT_ROUTE_MAX                        10                             // max. record
//#define NEWRPT_ROUTENO_MIN                      0                              // min. code
//#define NEWRPT_ROUTENO_MAX                      (NEWRPT_ROUTE_MAX - 1)         // max. code
//#define NEWRPT_ROUTENO_MANAG                    100                            // 管理範囲
//
//// ルート選択種別
//#define NEWRPT_ROUTETYPE_EMP                    0                              // 個人別承認ルート
//#define NEWRPT_ROUTETYPE_DEP                    1                              // 所属別承認ルート
//
//// 段階承認者数
//#define NEWRPT_INLEVEL_NONE                     0                              // none code
//#define NEWRPT_INLEVEL_MIN                      1                              // min. code
//#define NEWRPT_INLEVEL_MAX                      9                              // max. code
//
//// 届出入力種類
//#define NEWRPT_REPORTTYPE_COM_SLIP              0                               // 共通（伝票届出期間設定）
//#define NEWRPT_REPORTTYPE_COM_DAILY             1                               // 共通（デイリー届出期間設定）
//#define NEWRPT_REPORTTYPE_COM_MONTH             2                               // 共通（月次台帳届出期間設定）
//#define NEWRPT_REPORTTYPE_COM_BONUS             3                               // 共通（賞与台帳届出期間設定）
//#define NEWRPT_REPORTTYPE_COM_TAX               4                               // 共通（年調台帳届出期間設定）
//#define NEWRPT_REPORTTYPE_COM_INDIVI            5                               // 共通（個人マスター届出期間設定）
//#define NEWRPT_REPORTTYPE_COM_HUMAN             6                               // 共通（人事情報届出期間設定）
//#define NEWRPT_REPORTTYPE_MC_MIN                10                              // MC（伝票型） 最小値
//#define NEWRPT_REPORTTYPE_MC_MAX                19                              // MC（伝票型） 最大値
//#define NEWRPT_REPORTTYPE_PUNCH_MIN             20                              // 打刻修正（伝票型） 最小値
//#define NEWRPT_REPORTTYPE_PUNCH_MAX             29                              // 打刻修正（伝票型） 最大値
//#define NEWRPT_REPORTTYPE_SCH_MIN               30                              // スケジュール（伝票型） 最小値
//#define NEWRPT_REPORTTYPE_SCH_MAX               39                              // スケジュール（伝票型） 最大値
//#define NEWRPT_REPORTTYPE_ADD_MIN               40                              // 追加データ・時間数（伝票型） 最小値
//#define NEWRPT_REPORTTYPE_ADD_MAX               49                              // 追加データ・時間数（伝票型） 最大値
//#define NEWRPT_REPORTTYPE_COUNT_MIN             50                              // 回数データ（伝票型） 最小値
//#define NEWRPT_REPORTTYPE_COUNT_MAX             58                              // 回数データ（伝票型） 最大値
//#define NEWRPT_REPORTTYPE_CLN_MIN               60                              // カレンダ（伝票型） 最小値
//#define NEWRPT_REPORTTYPE_CLN_MAX               68                              // カレンダ（伝票型） 最大値
//// 2005.11.30 Ver5.00 【NA5_001】Start
////#define NEWRPT_REPORTTYPE_OVERWK_MIN            80                              // 残業時間（伝票型） 最小値
////#define NEWRPT_REPORTTYPE_OVERWK_MAX            88                              // 残業時間（伝票型） 最大値
//
//#define NEWRPT_REPORTTYPE_OVERPCH_MIN            70                              // 時間外時刻（伝票型） 最小値
//#define NEWRPT_REPORTTYPE_OVERPCH_MAX            78                              // 時間外時刻（伝票型） 最大値
//#define NEWRPT_REPORTTYPE_OVERTIME_MIN           80                              // 時間外時間帯（伝票型） 最小値
//#define NEWRPT_REPORTTYPE_OVERTIME_MAX           88                              // 時間外時間帯（伝票型） 最大値
//// End
//
//#define NEWRPT_REPORTTYPE_MONTH_MIN             110                             // 月次台帳 最小値
//#define NEWRPT_REPORTTYPE_MONTH_MAX             119                             // 月次台帳 最大値
//#define NEWRPT_REPORTTYPE_BONUS_MIN             120                             // 賞与台帳 最小値
//#define NEWRPT_REPORTTYPE_BONUS_MAX             129                             // 賞与台帳 最大値
//#define NEWRPT_REPORTTYPE_TAX_MIN               130                             // 年調台帳 最小値
//#define NEWRPT_REPORTTYPE_TAX_MAX               139                             // 年調台帳 最大値
//#define NEWRPT_REPORTTYPE_DAILY_MIN             140                             // デイリーデータ 最小値
//#define NEWRPT_REPORTTYPE_DAILY_MAX             149                             // デイリーデータ 最大値
//#define NEWRPT_REPORTTYPE_COMMON_MIN            150                             // 個人共通マスター 最小値
//#define NEWRPT_REPORTTYPE_COMMON_MAX            159                             // 個人共通マスター 最大値
//#define NEWRPT_REPORTTYPE_HUMAN_MIN             160                             // 人事履歴情報 最小値
//#define NEWRPT_REPORTTYPE_HUMAN_MAX             179                             // 人事履歴情報 最大値
//
//// 届出入力種類判定
//// MC（伝票型）
//#define IsReportTypeMC(ReportType)              inside(ReportType, NEWRPT_REPORTTYPE_MC_MIN, NEWRPT_REPORTTYPE_MC_MAX)
//// 打刻修正（伝票型）
//#define IsReportTypePch(ReportType)             inside(ReportType, NEWRPT_REPORTTYPE_PUNCH_MIN, NEWRPT_REPORTTYPE_PUNCH_MAX)
//// スケジュール（伝票型）
//#define IsReportTypeSch(ReportType)             inside(ReportType, NEWRPT_REPORTTYPE_SCH_MIN, NEWRPT_REPORTTYPE_SCH_MAX)
//// 追加データ・時間数（伝票型）
//#define IsReportTypeAdd(ReportType)             inside(ReportType, NEWRPT_REPORTTYPE_ADD_MIN, NEWRPT_REPORTTYPE_ADD_MAX)
//// 回数データ（伝票型）
//#define IsReportTypeCnt(ReportType)             inside(ReportType, NEWRPT_REPORTTYPE_COUNT_MIN, NEWRPT_REPORTTYPE_COUNT_MAX)
//// カレンダ（伝票型）
//#define IsReportTypeCln(ReportType)             inside(ReportType, NEWRPT_REPORTTYPE_CLN_MIN, NEWRPT_REPORTTYPE_CLN_MAX)
//// 2005.11.30 Ver5.00機能改善【NA5_001】Start
////// 残業時間（伝票型）
////#define IsReportTypeOverWk(ReportType)          inside(ReportType, NEWRPT_REPORTTYPE_OVERWK_MIN, NEWRPT_REPORTTYPE_OVERWK_MAX)
//// 時間外時刻（伝票型）
//#define IsReportTypeOverPch(ReportType)             inside(ReportType, NEWRPT_REPORTTYPE_OVERPCH_MIN, NEWRPT_REPORTTYPE_OVERPCH_MAX)
//// 時間外時間帯（伝票型）
//#define IsReportTypeOverTime(ReportType)             inside(ReportType, NEWRPT_REPORTTYPE_OVERTIME_MIN, NEWRPT_REPORTTYPE_OVERTIME_MAX)
//// End
//// 伝票型
//#define IsReportTypeSlip(ReportType)            (IsReportTypeMC(ReportType)      \
//                                              || IsReportTypePch(ReportType)     \
//                                              || IsReportTypeSch(ReportType)     \
//                                              || IsReportTypeAdd(ReportType)     \
//                                              || IsReportTypeCnt(ReportType)     \
//                                              || IsReportTypeCln(ReportType)     \
//                                              || IsReportTypeOverPch(ReportType) \
//                                              || IsReportTypeOverTime(ReportType)) \
//// End
//// 残業時間（伝票型）：Ver4.10未対応のため除外
////                                              || IsReportTypeOverWk(ReportType)) \
//// 月次台帳
//#define IsReportTypeMonth(ReportType)           inside(ReportType, NEWRPT_REPORTTYPE_MONTH_MIN, NEWRPT_REPORTTYPE_MONTH_MAX)
//// 賞与台帳
//#define IsReportTypeBonus(ReportType)           inside(ReportType, NEWRPT_REPORTTYPE_BONUS_MIN, NEWRPT_REPORTTYPE_BONUS_MAX)
//// 年調台帳
//#define IsReportTypeTax(ReportType)             inside(ReportType, NEWRPT_REPORTTYPE_TAX_MIN, NEWRPT_REPORTTYPE_TAX_MAX)
//// デイリーデータ
//#define IsReportTypeDaily(ReportType)           inside(ReportType, NEWRPT_REPORTTYPE_DAILY_MIN, NEWRPT_REPORTTYPE_DAILY_MAX)
//// 個人共通マスター
//#define IsReportTypeGindivi(ReportType)         inside(ReportType, NEWRPT_REPORTTYPE_COMMON_MIN, NEWRPT_REPORTTYPE_COMMON_MAX)
//// 人事履歴情報
//#define IsReportTypeHuman(ReportType)           inside(ReportType, NEWRPT_REPORTTYPE_HUMAN_MIN, NEWRPT_REPORTTYPE_HUMAN_MAX)
//
//// 入力項目
//#define NEWRPT_ITEM_EMP_BASE_MIN                1001                            // 個人マスター項目（基本項目） 最小値
//#define NEWRPT_ITEM_EMP_BASE_MAX                1999                            // 個人マスター項目（基本項目） 最大値
//#define NEWRPT_ITEM_EMP_SERVICE_MIN             2001                            // 個人マスター項目（勤務携帯） 最小値
//#define NEWRPT_ITEM_EMP_SERVICE_MAX             2999                            // 個人マスター項目（勤務携帯） 最大値
//#define NEWRPT_ITEM_EMP_WORK_MIN                3001                            // 個人マスター項目（就業項目） 最小値
//#define NEWRPT_ITEM_EMP_WORK_MAX                3999                            // 個人マスター項目（就業項目） 最大値
//#define NEWRPT_ITEM_EMP_SUPPLY_MIN              4001                            // 個人マスター項目（支給控除） 最小値
//#define NEWRPT_ITEM_EMP_SUPPLY_MAX              4999                            // 個人マスター項目（支給控除） 最大値
//#define NEWRPT_ITEM_EMP_PAY_MIN                 5001                            // 個人マスター項目（給与項目） 最小値
//#define NEWRPT_ITEM_EMP_PAY_MAX                 5999                            // 個人マスター項目（給与項目） 最大値
//#define NEWRPT_ITEM_EMP_TAX_MIN                 6001                            // 個人マスター項目（税区分） 最小値
//#define NEWRPT_ITEM_EMP_TAX_MAX                 6999                            // 個人マスター項目（税区分） 最大値
//#define NEWRPT_ITEM_EMP_TRANSFER_MIN            7001                            // 個人マスター項目（振込項目） 最小値
//#define NEWRPT_ITEM_EMP_TRANSFER_MAX            7999                            // 個人マスター項目（振込項目） 最大値
//#define NEWRPT_ITEM_EMP_INSURANCE_MIN           8001                            // 個人マスター項目（社会保険） 最小値
//#define NEWRPT_ITEM_EMP_INSURANCE_MAX           8999                            // 個人マスター項目（社会保険） 最大値
//#define NEWRPT_ITEM_DAILY_MIN                   9001                            // デイリーデータ項目 最小値
//#define NEWRPT_ITEM_DAILY_MAX                   9999                            // デイリーデータ項目 最大値
//#define NEWRPT_ITEM_WORK_MIN                    10001                           // 就業台帳項目（年休含む） 最小値
//#define NEWRPT_ITEM_WORK_MAX                    10999                           // 就業台帳項目（年休含む） 最大値
//#define NEWRPT_ITEM_PAY_MIN                     1001                            // 給与台帳項目 最小値
//#define NEWRPT_ITEM_PAY_MAX                     1999                            // 給与台帳項目 最大値
//#define NEWRPT_ITEM_BONUS_MIN                   12001                           // 賞与台帳項目 最小値
//#define NEWRPT_ITEM_BONUS_MAX                   12999                           // 賞与台帳項目 最大値
//#define NEWRPT_ITEM_TAX_MIN                     13001                           // 年調台帳項目 最小値
//#define NEWRPT_ITEM_TAX_MAX                     13999                           // 年調台帳項目 最大値
//#define NEWRPT_ITEM_INSURANCE_MIN               14001                           // 社会保険台帳項目 最小値
//#define NEWRPT_ITEM_INSURANCE_MAX               14999                           // 社会保険台帳項目 最大値
//#define NEWRPT_ITEM_HUMAN_BASE_MIN              37001                           // 人事基本項目 最小値
//#define NEWRPT_ITEM_HUMAN_BASE_MAX              37999                           // 人事基本項目 最大値
//#define NEWRPT_ITEM_SCH_MIN                     38001                           // 個人基本スケジュール情報 最小値
//#define NEWRPT_ITEM_SCH_MAX                     38999                           // 個人基本スケジュール情報 最大値
//#define NEWRPT_ITEM_HUMAN_INFO_MIN              51001                           // 人事情報項目 最小値
//#define NEWRPT_ITEM_HUMAN_INFO_MAX              70999                           // 人事情報項目 最大値
//#define NEWRPT_ITEM_SLIP_DIVISION				1								// 伝票型・出退区分
//#define NEWRPT_ITEM_SLIP_CALENDAR1				2								// 伝票型・カレンダ
//#define NEWRPT_ITEM_SLIP_CALENDAR2				3								// 伝票型・不在理由
//#define NEWRPT_ITEM_SLIP_SHIFT					4								// 伝票型・勤務区分
//#define NEWRPT_ITEM_SLIP_PUNCHMC				5								// 伝票型・打刻ＭＣ
//#define NEWRPT_ITEM_SLIP_PUNCH					6								// 伝票型・打刻
//#define NEWRPT_ITEM_SLIP_REGHMSTARTEND			7								// 伝票型・定時開始終了
//#define NEWRPT_ITEM_SLIP_REZNCODE				9								// 伝票型・事由コード
//#define NEWRPT_ITEM_SLIP_CNTMC1					12								// 伝票型・回数ＭＣ１
//#define NEWRPT_ITEM_SLIP_CNTMC2					13								// 伝票型・回数ＭＣ２
//#define NEWRPT_ITEM_SLIP_CNTMC3					14								// 伝票型・回数ＭＣ３
//#define NEWRPT_ITEM_SLIP_CNTMC4					15								// 伝票型・回数ＭＣ４
//#define NEWRPT_ITEM_SLIP_CNTMC5					16								// 伝票型・回数ＭＣ５
//#define NEWRPT_ITEM_SLIP_CNTMC6					17								// 伝票型・回数ＭＣ６
//#define NEWRPT_ITEM_SLIP_PLANOT0				9016							// 伝票型・予定時間外１
//#define NEWRPT_ITEM_SLIP_PLANOT1				9017							// 伝票型・予定時間外２
//#define NEWRPT_ITEM_SLIP_DEL_MIN				9126							// 伝票型・追加データ 最小値
//#define NEWRPT_ITEM_SLIP_DEL_MAX				9131							// 伝票型・追加データ 最大値
//#define NEWRPT_ITEM_SLIP_DEFITEM_MIN			9100							// 伝票型・時間数項目 最小値
//#define NEWRPT_ITEM_SLIP_DEFITEM_MAX			9119							// 伝票型・時間数項目 最大値
//#define NEWRPT_ITEM_SLIP_BRK0					9019							// 伝票型・休憩１
//#define NEWRPT_ITEM_SLIP_BRK1					9025							// 伝票型・休憩２
//#define NEWRPT_ITEM_SLIP_BRK2					9031							// 伝票型・休憩３
//#define NEWRPT_ITEM_SLIP_BRK3					9037							// 伝票型・休憩４
//#define NEWRPT_ITEM_SLIP_BRK4					9043							// 伝票型・休憩５
//#define NEWRPT_ITEM_SLIP_BRK5					9049							// 伝票型・休憩６
//// 2005.11.30 Ver5.00 【NA5_001】Start
//#define NEWRPT_ITEM_SLIP_OVERPCH1				19001							// 伝票型・時間外時刻区分
//#define NEWRPT_ITEM_SLIP_OVERPCH2				19002							// 伝票型・時間外時刻
//#define NEWRPT_ITEM_SLIP_OVERPCH3				19003							// 伝票型・時間外事由コード
//#define NEWRPT_ITEM_SLIP_OVERTIM1				19004							// 伝票型・時間外時間帯１
//#define NEWRPT_ITEM_SLIP_OVERTIM2				19005							// 伝票型・時間外時間帯２
//#define NEWRPT_ITEM_SLIP_OVERTIM3				19006							// 伝票型・時間外時間帯３
//#define NEWRPT_ITEM_SLIP_OVERTIM4				19007							// 伝票型・時間外時間帯４
//#define NEWRPT_ITEM_SLIP_OVERTIM5				19008							// 伝票型・時間外時間帯５
//// End
//
//// 人事情報データタイプ
//#define NEWRPT_HUMANDATATYPE_MIN                11                              // 最小値（家族）
//#define NEWRPT_HUMANDATATYPE_MAX                16                              // 最大値（婚姻届出）
//
//#define NEWRPT_HUMANDATATYPE_FAMILY             11                              // 家族
//#define NEWRPT_HUMANDATATYPE_ADDRESS            12                              // 住所
//#define NEWRPT_HUMANDATATYPE_COMMUTATION        13                              // 通勤手段
//#define NEWRPT_HUMANDATATYPE_QUALIFICATION      14                              // 資格
//#define NEWRPT_HUMANDATATYPE_INSURANCE          15                              // 保険
//#define NEWRPT_HUMANDATATYPE_MARRIAGE           16                              // 婚姻届出
//
//// 共通届出構造体
//#pragma	pack(push, 1)
//
//typedef struct tagNEWRPT_RECORD_INFO                                            // 届出レコード情報
//{
//    long  lRecordTime;                                                          // 作成日時
//    long  lRecordID;                                                            // ＩＤ
//    short nRecordType;                                                          // 種別
//    long  lSerialNo;                                                            // シリアルNo.
//} NEWRPT_RECORD_INFO;
//
//typedef NEWRPT_RECORD_INFO FAR*                 LPNEWRPT_RECORD_INFO;
//
//#pragma	pack(pop)
//// End
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//#endif // _RFX_BD_H_

#ifndef _GET21_GET21GRIDINFO_H_
	#define _GET21_GET21GRIDINFO_H_

#include "Get21GridReg.h"

/////////////////////////////////////////////////////////////////////////////
// IMPORT/EXPORT ��`
//

#undef AFX_DATA
#define AFX_DATA AFX_EXT_DATA

class CGet21GridInfo
{
	LPGET21REG_GRIDINFO		m_pInitGridInfo;

public:
	CGet21GridInfo();			// 
	~CGet21GridInfo();			// 

	int InitGrid( void* pVhGrid );
	int ResetGrid( void* pVhGrid );
	int SaveGrid( void* pVhGrid,
				 int nOptionNo = -1, int nSeqNo = -1 );
	int RestoreGrid( void* pVhGrid,
				 int nOptionNo = -1, int nSeqNo = -1 );
};

#undef AFX_DATA
#define AFX_DATA

#endif // _GET21_GET21GRIDINFO_H_

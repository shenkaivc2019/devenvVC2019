#pragma once
#include "IDB.h"
#include "DBDefine.h"
#include <vector>
//文件名称 :ConnectDataBase.h
//说明:连数据库的相关功能文件
//公司名: 上海神开石油测控技术有限公司
//作者: 杨硕
//日期: 2020/01/22
//备注: 无

#define WM_DATABASE_CONNECT (WM_USER + 1050)
#define WM_DATABASE_DISCONNECT (WM_USER + 1051)

class CConnectDataBase
{
public:
	static CConnectDataBase* GetInstance();
	CConnectDataBase();
	static CConnectDataBase* m_pInstance;
public:
	~CConnectDataBase();

//------------------------------------//
//函数名 : 打开指定的数据库
//参数 : dbname 要打开的数据库名称
//说明: 连接数据库服务器并打开指定的数据库
//返回值: 
//备注:
//-------------------------------------//
	BOOL ConnectDBSource(LPCTSTR strUsername, LPCTSTR strServer, LPCTSTR strPasswd);
	BOOL OpenDatabase(LPCTSTR dbname = NULL);

	BOOL QueryUnitsPre(std::vector<TS_UNITTB>& vecUnit);

	BOOL CloseDBServerPre();

public:
	IDB* m_pIDB;
	HINSTANCE	m_hDllDB;
	BOOL		m_bDBOpen;
	BOOL		m_bConnect;

	CString		m_strServer;   //服务器相关变量
	CString		m_strUsername;
	CString		m_strPasswd;
	CString		m_strDbName;
	BOOL		m_bAutoConnect;

	void ReadDBSourceParam();
};


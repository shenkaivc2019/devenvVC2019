#if !defined(AFX_RFXDLGSL_H__03DC2095_628C_11D4_8E95_00105A14616F__INCLUDED_)
#define AFX_RFXDLGSL_H__03DC2095_628C_11D4_8E95_00105A14616F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// RfxDlgSl.h : 処理選択ダイアログヘッダー ファイル

//---------------------------------------------------------------------------//
// 日本語構造体名：	カタログ情報構造体
// 説明：	処理選択画面のリストボックスに出す内容です。
// 備考：	なし
//---------------------------------------------------------------------------//

#pragma pack( push, 1 )

typedef struct tagRFX_PROCSEL_INFO
{
	int			nCatalogNumber;				// カタログ番号(== オプションナンバー)
	CString		strCatalogName;				// カタログ名
	BOOL		bIsSetting;					// 設定ボタンを有効にする?
	void operator = (const tagRFX_PROCSEL_INFO& gInfo)
	{ nCatalogNumber=gInfo.nCatalogNumber; strCatalogName=gInfo.strCatalogName; bIsSetting=gInfo.bIsSetting; };
	tagRFX_PROCSEL_INFO()
	{ nCatalogNumber=0; strCatalogName.Empty(); bIsSetting=FALSE; };
} RFX_PROCSEL_INFO, FAR * LPRFX_PROCSEL_INFO;

#pragma pack(pop)

typedef CArray <RFX_PROCSEL_INFO, RFX_PROCSEL_INFO> CVwProcSelArray;

//---------------------------------------------------------------------------//
// 日本語クラス名：	処理選択ダイアログクラス
// 説明：	処理選択ダイアログクラス
// 備考：	なし
//---------------------------------------------------------------------------//

#undef AFX_DATA
#ifdef _RFXBASE_BUILD
	#define AFX_DATA AFX_EXT_DATA
#else
	#define AFX_DATA AFX_DATA_IMPORT
#endif

class CVwDlgProcSelect : public CDialog
{
// コンストラクション
public:
	CVwDlgProcSelect(CVwProcSelArray* pArray, BOOL bSetup=TRUE, LPCTSTR lpszCaption=NULL, CWnd* pParent=NULL);
	~CVwDlgProcSelect();


	//{{AFX_DATA(CVwDlgProcSelect)
	enum { IDD = RFX_IDD_DLG_PROCSELECT };
		// メモ: ClassWizard はこの位置にデータ メンバを追加します。
	//}}AFX_DATA

// オペレーション
public:
	// 選択されたオプション(カタログ)ナンバー取得
	int GetCatalogNo() { return m_nCatalog; }
	void SetCatalogNo(int nCatalogNo) { m_nCatalog = nCatalogNo; }

	void SetCatalogName(LPCTSTR lpszCatalogName) { SetCatalogName(m_nCatalog, lpszCatalogName); }
	void SetCatalogName(int nCatalogNo, LPCTSTR lpszCatalogName);

	void DispProcList();	// 処理リスト表示

protected:
	BOOL		m_bIsSetup;					// 設定ボタンありますか?
	int			m_nCatalog;					// カタログ(オプション)番号
	CString 	m_strCaption;				// キャプション

	// カタログ情報構造体配列へのポインタを格納する
	CVwProcSelArray*	m_parrProcList;

protected:
	// 設定ボタンが押されたとき
	virtual int DoSetting(int nCatalogNumber) {return RFX_IDC_SETUP;}

// オーバーライド
	//{{AFX_VIRTUAL(CXaABaseDlgProcSelect)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:
	//{{AFX_MSG(CXaABaseDlgProcSelect)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnSetup();
	//}}AFX_MSG
	afx_msg void OnVLSelChangePslProclist();
	afx_msg void OnVLDblClkPslProclist();
	DECLARE_EVENTSINK_MAP()
	DECLARE_MESSAGE_MAP()
};

#undef AFX_DATA
#define AFX_DATA

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_RFXDLGSL_H__03DC2095_628C_11D4_8E95_00105A14616F__INCLUDED_)

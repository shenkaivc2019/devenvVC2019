#if !defined(AFX_COMBSPG_H__D2B7B682_1807_11D3_8BE6_00105A14616F__INCLUDED_)
#define AFX_COMBSPG_H__D2B7B682_1807_11D3_8BE6_00105A14616F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// RfxPage.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// CVwBasePage ダイアログ

#undef AFX_DATA
#ifdef _RFXBASE_BUILD
	#define AFX_DATA AFX_EXT_DATA
#else
	#define AFX_DATA AFX_DATA_IMPORT
#endif

class CVwBasePage : public CPropertyPage
{
	DECLARE_DYNAMIC(CVwBasePage)

// コンストラクション
public:
	CVwBasePage(CWnd* pParentWnd, UINT nIDTemplate);
	~CVwBasePage();

	CFont*	m_pFont;
	CSize	m_sizeScreen;

	void SetScreenSize(const CSize& size) {m_sizeScreen = size;}
	void SetScreenSize(const int x, const int y) {m_sizeScreen = CSize(x, y);}

	void GetScreenRect(LPRECT lpRect);

	CVwBaseDoc* GetDocument()
	{ return (CVwBaseDoc*)((CFrameWnd*)AfxGetApp()->m_pMainWnd)->GetActiveDocument(); }

protected:
	CWnd*	m_pParentWnd;

	CGridInfoArray	m_arrGridInfo;
	int		m_nPageNo;


// ダイアログ データ
	//{{AFX_DATA(CVwBasePage)
		// メモ - ClassWizard はこの位置にデータ メンバを追加します。
		//    この位置に生成されるコードを編集しないでください。
	//}}AFX_DATA

public:
	void AddGridInfo(UINT uGridID);
	void RestoreGridInfo();
	void SetActivePage();
	void SaveGridInfo();
	void ResetGridInfo();

	int ActiveMessageBox(UINT IDPrompt, UINT type = MB_OK);
	int ActiveMessageBox(LPCTSTR pszText, UINT type = MB_OK);

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	virtual BOOL OnSetActive();

protected:
	virtual BOOL IsDataChanged();
	virtual BOOL UpdateViewData(CVwBaseDoc* pDoc);
	virtual BOOL SaveViewData(CVwBaseDoc* pDoc);
	virtual BOOL CheckViewData(CVwBaseDoc* pDoc);

	//{{AFX_VIRTUAL(CVwBasePage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート
	//}}AFX_VIRTUAL

// インプリメンテーション
public:
	virtual BOOL OnUpdate(LPARAM lHint, CVwBaseDoc* pDoc);

protected:
	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CVwBasePage)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnOptionCellReset();
	virtual afx_msg void OnChangeItems();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	#undef AFX_DATA
	#define AFX_DATA

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_COMBSPG_H__D2B7B682_1807_11D3_8BE6_00105A14616F__INCLUDED_)

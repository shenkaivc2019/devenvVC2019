#pragma once


typedef struct
{
	int		nCommandID;		// 
	int		nBitmapID;		// 
	LPCSTR	lpszTitle;		// 
	BOOL	bIsDependMenu;	// 
							// いと
							// 

} VW_TOOLBAR_INFO, *LPVW_TOOLBAR_INFO;

#define		ID_TOOLBAR_TERMINATE		-1		// 


#define		TOOLBAR_COMMAND_STR(id, str)		ID_##id, IDB_##id, str
#define		TOOLBAR_COMMAND(id)					ID_##id, IDB_##id, NULL


#define		TOOLBAR_END()		ID_TOOLBAR_TERMINATE, 0, NULL
#define		TOOLBAR_SEPARATOR()	ID_SEPARATOR, 0, NULL
#define		IDB_SEPARATOR		0
////////////////////////////////////////////////////////
class CVwMFCToolbarItem
{
protected:
	CArray <VW_TOOLBAR_INFO, VW_TOOLBAR_INFO&> m_array;


public:
	CVwMFCToolbarItem() { m_array.RemoveAll(); }
	~CVwMFCToolbarItem() { m_array.RemoveAll(); }

	BOOL Add(int nCommandID, int nBitmapID, LPCSTR lpszTitle = NULL, BOOL bIsDependMenu = TRUE);

	VW_TOOLBAR_INFO& operator[](int index) { return m_array[index]; }
	VW_TOOLBAR_INFO GetAt(int index) { return m_array.GetAt(index); }

	int GetSize() { return m_array.GetSize(); }

};
////////////////////////////////////////////////////////
// CVwMFCToolBar

class CVwMFCToolBar : public CMFCToolBar
{
	DECLARE_DYNAMIC(CVwMFCToolBar)

public:
	CVwMFCToolBar();
	virtual ~CVwMFCToolBar();

	BOOL OnCreateBands();
	BOOL Create(CWnd* pParentWnd, UINT nIDResource, CVwMFCToolbarItem* pItem);
	BOOL Create(CWnd* pParentWnd, UINT nIDResource);
protected:
	DECLARE_MESSAGE_MAP()
protected:
	CVwMFCToolbarItem*		m_pItem;			// Item成员
	UINT 				m_res_id;		// 资源ID

	CMFCToolBarImages m_MFCToolBarImage;

	CFont			m_font;				//

};
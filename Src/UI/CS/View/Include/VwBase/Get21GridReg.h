#ifndef _GET21_GET21GRIDREG_H_
	#define _GET21_GET21GRIDREG_H_


/////////////////////////////////////////////////////////////////////////////
// IMPORT/EXPORT 
//
enum GET21_REG_OPENMODE
{
	GET21_REG_OPENMODE_APP,			// 
									// 

	GET21_REG_OPENMODE_GETINI,		// 
									// 
};

// 
typedef struct
{
	int			nColumnNumber;		// 
	int*		pColumnWidths;		// 
	int			nRowHeight;			// 

} GET21REG_GRIDINFO, *LPGET21REG_GRIDINFO;


class CGet21GridReg
{
public:

	CGet21GridReg();			// 
	~CGet21GridReg();			// 

	BOOL Open(GET21_REG_OPENMODE nMode = GET21_REG_OPENMODE_APP);

	BOOL Close();

	BOOL SetGridInfo(const LPGET21REG_GRIDINFO pInfo,
			 int nOptionNo = -1, int nSeqNo = -1 );

	BOOL GetGridInfo(LPGET21REG_GRIDINFO pInfo,
			 int nOptionNo = -1, int nSeqNo = -1 );
};

#endif // _GET21_GET21GRIDREG_H_

#if !defined(AFX_COMBSST_H__D2B7B681_1807_11D3_8BE6_00105A14616F__INCLUDED_)
#define AFX_COMBSST_H__D2B7B681_1807_11D3_8BE6_00105A14616F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// RfxSheet.h : ヘッダー ファイル
//

#include "VwPage.h"

/////////////////////////////////////////////////////////////////////////////
// CVwBaseSheet

#undef AFX_DATA
#ifdef _RFXBASE_BUILD
	#define AFX_DATA AFX_EXT_DATA
#else
	#define AFX_DATA AFX_DATA_IMPORT
#endif

class CVwBaseSheet : public CPropertySheet
{
	DECLARE_DYNAMIC(CVwBaseSheet)

// コンストラクション
public:
	CVwBaseSheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CVwBaseSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CVwBaseSheet(CWnd* pParentWnd);

protected:
	CFont	m_font;
	CWnd*	m_pParentWnd;

// アトリビュート
public:

// オペレーション
public:
	void AddPage(CVwBasePage *pPage);

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CVwBaseSheet)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL Create(CWnd* pParentWnd = NULL, DWORD dwStyle = (DWORD)-1, DWORD dwExStyle = 0);
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL

// インプリメンテーション
public:
	virtual ~CVwBaseSheet();

	// 生成されたメッセージ マップ関数
protected:
	//{{AFX_MSG(CVwBaseSheet)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CVwBaseSheetEx

class CVwBaseSheetEx : public CVwBaseSheet
{
	DECLARE_DYNAMIC(CVwBaseSheetEx)

// コンストラクション
public:
	CVwBaseSheetEx(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CVwBaseSheetEx(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CVwBaseSheetEx(CWnd* pParentWnd);


// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CVwBaseSheetEx)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// インプリメンテーション
public:
	virtual ~CVwBaseSheetEx();

	// 生成されたメッセージ マップ関数
protected:
	//{{AFX_MSG(CVwBaseSheetEx)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#undef AFX_DATA
#define AFX_DATA

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_COMBSST_H__D2B7B681_1807_11D3_8BE6_00105A14616F__INCLUDED_)

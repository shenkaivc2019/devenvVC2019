#ifndef _VW_BASE_H
#define	_VW_BASE_H

#pragma pack(push,1)

#pragma once

#define	UPDATE_FILE_OPEN						1
#define	UPDATE_FILE_SAVE						2
#define	UPDATE_NEED_SAVE						3
#define	UPDATE_INPUT_CHECK						4
#define	UPDATE_AUTO_FLAG						5
#define	UPDATE_FILE_CALCULATE					6
#define	UPDATE_FILE_CLOSE						7

//程序种类
#define PGTYPE_OTHER      0         // 其他PG
#define PGTYPE_MENU       1         // 处理菜单
#define PGTYPE_PGEXE      2         // 启动状况管理
#define PGTYPE_COMMU      3         // 通信关联
#define PGTYPE_INDMAS     4         // 个人主保守相关
#define PGTYPE_OTHMAS     5         // 其他主保守相关
#define PGTYPE_PARAM      6         // 参数维护相关
#define PGTYPE_STOP       999       // 禁止启动

#define		RFX_RESOURCEDLL_NAME		("XgResource.DLL")

struct US_EXECUTEINFO
{
	int   nOptionNo;           // 选项No.
	int   nOperaterLevel;      // 操作员级别
	TCHAR aszEmpCode[11];      // 操作员代码（Long->String）
	int   nMenuNo;             // 菜单No.（新增）
	int   nSubMenuNo;          // 子菜单No.（新增）
	int   nHandlingLevel;      // 操作级别（新增）
	BOOL  bAutoFlag;           // 自动启动
	TCHAR aszProgramID[4][9];  // 下一个启动程序（2->1～4）
	int   nNextOption[4];      // 下一个启动程序选项（新增）
	BOOL  bNextAutoFlag[4];    // 下一个启动程序自动启动标志（新增）
};
#define	HANDLINGLEVEL_NONE		0		// 不可操作
#define	HANDLINGLEVEL_DISP		1		// 仅显示
#define	HANDLINGLEVEL_INPUT		2		// 可输入
#define	HANDLINGLEVEL_SETUP		3		// 可设置

#include <scbarg.h>
#include <VwMFCToolBar.h>
#include <VwMenu.h>
#include "ConnectDataBase.h"
#include <MyRecentFileList.h>
#include <XgResource.h>
#include <VwRes.h>
//#include <Vwdb.h>
#include <VwSApp.h>
#include <VwSFrm.h>
#include <VwSAppEx.h>
#include <VwSFrmEx.h>
#include <VwMAppEx.h>
#include <VwMFrmEx.h>
#include <VwDoc.h>
#include <VwView.h>
#include <VwTabvw.h>
#include <VwPage.h>

#pragma pack(pop)

#endif // _VW_BASE_H

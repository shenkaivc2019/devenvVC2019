// RfxDoc.h : CVwBaseDoc クラスの宣言およびインターフェイスの定義をします。
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_RFXDOC_H__CA2BDABC_F703_11D2_8BBB_00105A14616F__INCLUDED_)
#define AFX_RFXDOC_H__CA2BDABC_F703_11D2_8BBB_00105A14616F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#undef AFX_DATA
#ifdef _RFXBASE_BUILD
	#define AFX_DATA AFX_EXT_DATA
#else
	#define AFX_DATA AFX_DATA_IMPORT
#endif
class CVwBaseDoc : public CDocument
{
protected: // シリアライズ機能のみから作成します。
	CVwBaseDoc() noexcept;
	DECLARE_DYNCREATE(CVwBaseDoc)

// アトリビュート
public:

// オペレーション
public:
	void DocHardClose();
	void DocResetFlags();

//オーバーライド
	virtual BOOL DocFileOpen(BOOL bNeedDoc = FALSE, BOOL bUpdateView = FALSE, LPARAM lParam1 = 0L, LPARAM lParam2 = 0L);
	virtual BOOL DocFileSave();
	virtual BOOL DocFileClose();
	virtual BOOL DocRecordChange();
	virtual BOOL DocFileRead(LPARAM lParam1, LPARAM lParam2);
	virtual BOOL DocFileWrite();

	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CVwBaseDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL CanCloseFrame(CFrameWnd* pFrame);
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:
	BOOL	m_bNeedDoc;			// set app or mainframe => DocFileOpen
	BOOL	m_bClose;

public:
	BOOL	m_bRead;			// set form view
	BOOL	m_bWrite;			// set form view
	BOOL	m_bInput;			// set form view
	BOOL	m_bNeedSave;		// set form view

	virtual ~CVwBaseDoc();

protected:

// 生成されたメッセージ マップ関数
protected:
	//{{AFX_MSG(CVwBaseDoc)
	afx_msg void OnFileSave();
	afx_msg void OnUpdateFileSave(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#undef AFX_DATA
#define AFX_DATA

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_RFXDOC_H__CA2BDABC_F703_11D2_8BBB_00105A14616F__INCLUDED_)

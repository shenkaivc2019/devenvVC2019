// CurveFilter.h: interface for the CCurveFilter class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BASEMARKER_H__2CDAD045_2A5E_402C_B3B0_C777FFE02EC8__INCLUDED_CurveFilter)
#define AFX_BASEMARKER_H__2CDAD045_2A5E_402C_B3B0_C777FFE02EC8__INCLUDED_CurveFilter

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "XMLSettings.h"

class CDBFilterCurveInfo;

#pragma	pack(push, 1)
typedef struct tag_CURVE_FILTER
{	
	CDBFilterCurveInfo*	 m_pDBCurveInfo;
	TCHAR szDataSourceName[64];
} CURVE_FILTER;

typedef	CURVE_FILTER FAR*	LPTS_CURVE_FILTER;

#pragma	pack(pop)

class CCurve;
class CSheet;

class CCurveFilter
{
public:
	CCurveFilter(CCurve* pCurve);
	CCurveFilter(CSheet* pSheet);
	virtual ~CCurveFilter();
public:
	void Serialize(CXMLSettings& xml);
public:
	CCurve* m_pParentCurve;
	CSheet* m_pParentSheet;
	CPtrArray m_arrFilterList; // �б�
};

#endif // !defined(AFX_BASEMARKER_H__2CDAD045_2A5E_402C_B3B0_C777FFE02EC8__INCLUDED_CurveFilter)

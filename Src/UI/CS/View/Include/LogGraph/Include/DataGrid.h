#if !defined(AFX_DATAGRID_H__A7C51A76_9C68_4621_B948_0940158B6747__INCLUDED_)
#define AFX_DATAGRID_H__A7C51A76_9C68_4621_B948_0940158B6747__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GridCtrl.h"
#include "Curve.h"

// DataGrid.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDataGrid window

class CDataGrid : public CGridCtrl
{
	// Construction
public:
	CDataGrid();

	// Attributes
public:

	// Operations
public:

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataGrid)
	//}}AFX_VIRTUAL

	// Implementation
public:
	virtual ~CDataGrid();

	// Generated message map functions
protected:
	//{{AFX_MSG(CDataGrid)
	// NOTE - the ClassWizard will add and remove member functions here.
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	virtual COleDataSource* CopyTextFromGrid();
	virtual DROPEFFECT OnDragOver(COleDataObject* pDataObject,
		DWORD dwKeyState, CPoint point);
	virtual BOOL OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect,
		CPoint point);
	void OnCheckAll(BOOL bCheck = TRUE);
	void GetSelectGridState(BOOL &bHave, BOOL &bCheckAll);
public:
	BOOL m_bDragCurve;
	CCurve* m_pCurve;
	CArray <CCurve*, CCurve*&> m_CurveArray;

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATAGRID_H__A7C51A76_9C68_4621_B948_0940158B6747__INCLUDED_)

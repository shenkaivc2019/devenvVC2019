#if !defined(AFX_DLGGRADIENT_H__167286E5_0369_4F17_A401_5FE373D8B0D5__INCLUDED_)
#define AFX_DLGGRADIENT_H__167286E5_0369_4F17_A401_5FE373D8B0D5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GradientStatic.h"
#include "GradientList.h"

// DlgGradient.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgGradient dialog

class CDlgGradient : public CDialog
{
// Construction
public:
	CDlgGradient(CWnd* pParent = NULL);   // standard constructor

public:
	BOOL LoadFrom(LPCTSTR pszFile);
	BOOL SaveTo(LPCTSTR pszFile);
	DWORD AnsiHex2DWORD(LPTSTR s);
// Dialog Data
	//{{AFX_DATA(CDlgGradient)
	enum { IDD = IDD_GRADIENT_EDITOR };
	CStatic	m_stc1;
	int	m_nLocat;
	int m_nColorR,m_nColorG,m_nColorB;
	//}}AFX_DATA
	CMFCColorButton m_btnColor;
	CGradientStatic	m_stcGradient;
	CGradientList	m_lstGradient;
	CString m_strTables;
	BOOL m_bSet;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgGradient)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgGradient)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnBtnDelete();
	afx_msg void OnBtnColor();
	afx_msg void OnSelchangeList1();
	afx_msg void OnBtnNew();
	afx_msg void OnChangeEdit2();
	afx_msg void OnBtnDel();
	afx_msg void OnBtnLoad();
	afx_msg void OnBtnSaveas();
	afx_msg void OnAdd();
	//}}AFX_MSG
	//afx_msg void OnSelectedChanged(WPARAM wp);
	afx_msg LRESULT OnSelectedChanged(WPARAM wp, LPARAM lp);
	//afx_msg void OnPosChanged(WPARAM wp, LPARAM lp);
	afx_msg LRESULT OnPosChanged(WPARAM wp, LPARAM lp);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGGRADIENT_H__167286E5_0369_4F17_A401_5FE373D8B0D5__INCLUDED_)

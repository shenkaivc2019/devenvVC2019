#ifndef __IEILOG_H__
#define __IEILOG_H__

#include "ULInterface.h"
#include "ULCOMMDEF.H"

// Interfaces
interface IEILog : IUnknown
{
	// 基本操作
	ULMETHOD	Clear() = 0;
	ULMETHOD	SetFileHeadInfo(void *pInfo) = 0;
	ULMETHOD	GetFileHeadInfo(void *pInfo) = 0;
	ULMETHOD	SetGenHeadInfo(void *pInfo) = 0;
	ULMETHOD	GetGenHeadInfo(void *pInfo) = 0;
	ULMETHOD	SetOperateInfo(void *pInfo) = 0;
	ULMETHOD	GetOperateInfo(void *pInfo) = 0;
	ULMETHOD	SetWellInfo(void *pInfo) = 0;
	ULMETHOD	GetWellInfo(void *pInfo) = 0;
	ULMETHOD	SetBoreholeInfo(void *pInfo) = 0;
	ULMETHOD	GetBoreholeInfo(void *pInfo) = 0;
	ULMETHODUNT	AddToolInfo(void *pInfo) = 0;
	ULMETHOD	AddToolCurveInfo(UINT nIndex , void *pInfo) = 0;
	ULMETHODUNT	GetToolCount() = 0;
	ULMETHOD	GetToolInfo(UINT nIndex , void *pInfo) = 0;
	ULMETHODUNT	GetToolCurveCount(UINT nIndex) = 0;
	ULMETHOD	GetToolCurveInfo(UINT nToolIndex , UINT nCurveIndex , void *pInfo) = 0;
	ULMETHOD    AddBitAndCaseInfo(CString strBitInfo, CString strCaseInfo) = 0;
	ULMETHOD	SetLogData(void *pInfo) = 0;
	ULMETHOD	ApplyProject() = 0;
	ULMETHOD    ApplyCalibration(CPtrArray& arrTools) = 0;
	ULMETHOD    ApplyJobs() = 0;
	ULMETHOD    ApplyParameter(void* pTPTree, CString strTools) = 0;
	ULMETHOD    SetProjectInfo() = 0;
	ULMETHOD    GetProjectInfo() = 0;
};

// 接口ID

// {FC20971E-11C1-4d9e-B96B-CD3C9009BAC6}
static const IID IID_IEILog = 
{ 0xfc20971e, 0x11c1, 0x4d9e, { 0xb9, 0x6b, 0xcd, 0x3c, 0x90, 0x9, 0xba, 0xc6 } };

//extern "C" DWORD ULGetVersion();
//extern "C" IUnknown* CreateXFile();

#endif
// Mark.h: interface for the CMark class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MARK_H__37934DD6_5CC8_4564_BAB8_8A33846DB946__INCLUDED_)
#define AFX_MARK_H__37934DD6_5CC8_4564_BAB8_8A33846DB946__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CCurve;
class CTrack;
class CULPrintInfo;
class CXMLSettings;
//------------------------
// 标记
//------------------------
#define MARKS_HORIZONTAL_LINE		0
#define MARKS_TRIANGLE				1
#define MARKS_ARROW					2
#define MARKS_VERTICAL_LINE			3
#define MARKS_FIRE_LINE				4
#define MARKS_MARK_LINE				5
#define MARKS_FIRE_LINE1			6
#define MARKS_MARK_LINE1			7
#define MARKS_MAX_NULL				8
#define MARKS_MIN_NULL				9
#define MARKS_BLOCK_CURVE           10
#define MARKS_PERFORATION_COLLOR    11
#define MARKS_SPLICE				12
//
// 节箍
// SC_ERR：standard collar index error
// CI_ERR：collar index error
//
#define SC_ERR			-1
#define CI_ERR			-1

class CMark  
{
public:
	CMark();
	CMark(CMark* pMark,CTrack* pTrack = NULL);
	virtual ~CMark();

public:
	void		Draw(CDC* pDC, long lStartDepth, long lEndDepth);
	BOOL		IsPtInShape(CPoint pt);
	void        Serialize(CXMLSettings& xml);
public:
	long m_lDepthAdjust;
	CString		m_strName;			// 标记名称

	CTrack*		m_pTrack;			// 标记井道
	CCurve*  	m_pCurve;			// 标记曲线
	long		m_lDepth;			// 标记深度
	long		m_lArrowPos;
//	int			m_nCurveIndex;		// 标记曲线索引
	CRect		m_rect;
		
	int			m_nPointer;			// 标记指向	0左，1右
	int 	 	m_nShape; 	 		// 标记形状
	CRect		m_rcMark;			// 标记范围
	COLORREF    m_crColor;          // 标记颜色
	COLORREF    m_crText;			// 标记文本颜色
	
	CString		m_strText;			// 标记说明
	int			m_nTrackNO;			// 通道号

	BOOL		m_bHot;				// 标记选中
	BOOL		m_bLock;

	BOOL        m_bDepthChange;		// 拖动标签时名称是否改变
	double      m_fMarkXCoordinate;	// 标记的横坐标
	float       m_fXScale;

	int			m_nCollarIndex;		// 节箍索引
	int			m_nStdCollarIndex;	// 第n枪的标箍号
	float       m_fLimit;           // 垂直线的位置
	float       m_fLValue;          // CCL曲线的左值
	float       m_fRValue;          // CCL曲线的右值

	BOOL        m_bWatch;           // 监视型标签
	DWORD       m_dwTime;           // 标记出现的时间

	//
	// 打印标签
	//
	void		Print(CULPrintInfo* pInfo);
};

#endif // !defined(AFX_MARK_H__37934DD6_5CC8_4564_BAB8_8A33846DB946__INCLUDED_)

#if !defined(AFX_TEMPLACTIVEDLG_H__704F5A71_FB2D_4127_9058_4069CD6530BD__INCLUDED_)
#define AFX_TEMPLACTIVEDLG_H__704F5A71_FB2D_4127_9058_4069CD6530BD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TemplActiveDlg.h : header file
//
// 2023.08.18 Start
#include <map>
using namespace std;

typedef struct tagPROSELECT
{
	int OPTEMPL; // 处理模板操作类型 ADDTEMPL, REMOVETEMPL 
	int nType; // 所选节点类型 ID_PRESENTATION_ACTIVE,ID_HEADER_ACTIVE,ID_CVS_ACTIVE,ID_PLOTTEMPLATE_ACTIVE
	
} PROSELECT;
// 2023.08.18 End

/////////////////////////////////////////////////////////////////////////////
// CTemplActiveDlg dialog
class CULDoc;
class CTemplActiveDlg : public CDialog
{
// Construction
public:
	CTemplActiveDlg(int nType, CULDoc*pDoc, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTemplActiveDlg)
	enum { IDD = IDD_TEMPL_ACTIVE_DLG };
	CListBox	m_ListSupported;
	CListBox	m_ListNotSupported;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTemplActiveDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
// 2023.08.18 Start
//	void DoSelect(CString strTempl, UINT id);
//	enum { ADDTEMPL, REMOVETEMPL };
	void DoSelect(CString strTempl, int nType, UINT id);
	enum { NONETEMPL, ADDTEMPL, REMOVETEMPL };
	BOOL AddItem2LeftList(LPCTSTR lpszFind);
// 2023.08.18 End
	// Generated message map functions
	//{{AFX_MSG(CTemplActiveDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkList(UINT id);
	afx_msg void OnSelect(UINT id);
	afx_msg void OnCommand(UINT id);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:
	int		m_nType;
	CULDoc* m_pDoc;
// 2023.08.18 Start
	std::map<CString, PROSELECT> m_mapProSelect;
// 2023.08.18 End
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEMPLACTIVEDLG_H__704F5A71_FB2D_4127_9058_4069CD6530BD__INCLUDED_)

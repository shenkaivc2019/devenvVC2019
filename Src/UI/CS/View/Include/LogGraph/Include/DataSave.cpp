#include "stdafx.h"
#include <stdio.h>
#include <DataSave.h>



__int64 TimeDiff(SYSTEMTIME left, SYSTEMTIME right)
{
	CTime tmLeft(left.wYear, left.wMonth, left.wDay, 0, 0, 0);
	CTime tmRight(right.wYear, right.wMonth, right.wDay, 0, 0, 0);

	CTimeSpan sp = tmLeft - tmRight;
	long MillisecondsL = (left.wHour * 3600 + left.wMinute * 60 + left.wSecond) * 1000 + left.wMilliseconds;
	long MillisecondsR = (right.wHour * 3600 + right.wMinute * 60 + right.wSecond) * 1000 + right.wMilliseconds;

	return  (__int64)sp.GetDays() * 86400000 + (MillisecondsL - MillisecondsR);//此处返回毫秒  
}

//构造函数
CDataSave::CDataSave()
{
	memset(m_szPath,0,sizeof(m_szPath));
	m_nCount=0;
	m_nCountBin = 0;
 	m_hFile=INVALID_HANDLE_VALUE;
	m_hFileBin = INVALID_HANDLE_VALUE;
}
CDataSave::~CDataSave()
{
    CloseFile();
	CloseFileBin();
}

void CDataSave::SetFolder(const char *pFilePath)
{
	strcpy(m_szPath,pFilePath);
}

BOOL  CDataSave::CreateFolder(LPCTSTR lpFolder)
{
	WIN32_FIND_DATA wfd;
	BOOL rValue = FALSE;
	HANDLE hFind = FindFirstFile(lpFolder, &wfd);
	if ((hFind!=INVALID_HANDLE_VALUE) && (wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY))
	{
		rValue = TRUE;
	}
	FindClose(hFind);
	if(!rValue)
	{
		return CreateDirectory(lpFolder,NULL);
	}
	return TRUE;
}

//创建文件
bool CDataSave::CreateFile()
{
	if(!CreateDirectoryIfNone(this->m_szPath))
	{
		return false;
	}

	::GetLocalTime(&m_lastSysTime);
	char szFileName[256]={0};
	//sprintf(szFileName,"\\%04d%02d%02d%02d%02d%02d.txt",st.wYear,st.wMonth,st.wDay,st.wHour,st.wMinute,st.wSecond);

	m_FileName = "%04d%02d%02d%02d%02d%02d.txt";
	sprintf(szFileName, "\\" + m_FileName, m_lastSysTime.wYear, m_lastSysTime.wMonth, m_lastSysTime.wDay, m_lastSysTime.wHour, m_lastSysTime.wMinute, m_lastSysTime.wSecond);

	char szFilePath[512]={0};
	strcpy(szFilePath,this->m_szPath);
	strcat(szFilePath,szFileName);
	m_hFile=::CreateFileA(szFilePath,GENERIC_WRITE,FILE_SHARE_READ,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
	m_FilePath = szFilePath;
	if(INVALID_HANDLE_VALUE==m_hFile)
	{
		return false;
	}
	return true;
}

//创建文件
bool CDataSave::CreateFileBin()
{
	if (!CreateDirectoryIfNone(this->m_szPath))
	{
		return false;
	}

	::GetLocalTime(&m_lastSysTime);

	char szFileNameBin[256] = { 0 };
	//m_FileNameBin = "%04d%02d%02d%02d%02d%02dBin.txt";
	//sprintf(szFileNameBin, "\\" + m_FileNameBin, m_lastSysTime.wYear, m_lastSysTime.wMonth, m_lastSysTime.wDay, m_lastSysTime.wHour, m_lastSysTime.wMinute, m_lastSysTime.wSecond);
	m_FileNameBin = "%04d%02d%02d%02d%02dBin.txt";
	sprintf(szFileNameBin, "\\" + m_FileNameBin, m_lastSysTime.wYear, m_lastSysTime.wMonth, m_lastSysTime.wDay, m_lastSysTime.wHour, m_lastSysTime.wMinute);


	char szFilePathBin[512] = { 0 };
	strcpy(szFilePathBin, this->m_szPath);
	strcat(szFilePathBin, szFileNameBin);
	m_hFileBin = ::CreateFileA(szFilePathBin, GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	m_FilePathBin = szFilePathBin;
	if (INVALID_HANDLE_VALUE == m_hFileBin)
	{
		return false;
	}
	return true;
}

//关闭文件
bool CDataSave::CloseFile()
{
	BOOL bRet=FALSE;
	if(INVALID_HANDLE_VALUE !=m_hFile)
	{
		bRet=::CloseHandle(m_hFile);
		m_hFile=INVALID_HANDLE_VALUE;
	}
	return bRet==1;
}


//关闭文件
bool CDataSave::CloseFileBin()
{
	BOOL bRet = FALSE;
	if (INVALID_HANDLE_VALUE != m_hFileBin)
	{
		bRet = ::CloseHandle(m_hFileBin);
		m_hFileBin = INVALID_HANDLE_VALUE;
	}
	return bRet == 1;
}

//保存原始点
bool CDataSave::SavePoint(float fRaw)
{
	if(INVALID_HANDLE_VALUE ==m_hFile)
	{
		if(!CreateFile())
		{
			return false;
		}
	}

	//写原始点到文件
	char szPoint[32]={0};
	sprintf(szPoint,"%.3f\r\n",fRaw);
	DWORD dwWritten=0;
	if(!WriteFile(m_hFile,szPoint,strlen(szPoint),&dwWritten,NULL) || 0==dwWritten)
	{
		return false;
	}

	//当原始点数达到 FILE_POINTS_COUNT 后重新创建新文件
	m_nCount++;
	if (m_nCount>FILE_POINTS_COUNT)
	{
		m_nCount=0;
		if(!CloseFile())
		{
			return false;
		}
		if(!CreateFile())
		{
			return false;
		}
	}
	return true;
}

//保存解码结果
bool CDataSave::SaveDecode(const char *pszDecode)
{
	if(INVALID_HANDLE_VALUE ==m_hFile)
	{
		if(!CreateFile())
		{
			return false;
		}
	}
	//写文件

	DWORD dwWritten=0;
	if(!WriteFile(m_hFile,pszDecode,strlen(pszDecode),&dwWritten,NULL) || 0==dwWritten)
	{
		return false;
	}
	return true;   
}

//保存错误日志
bool CDataSave::SaveErrorLog(const char *pszErrorLog)
{
	if (INVALID_HANDLE_VALUE == m_hFile)
	{
		if (!CreateFile())
		{
			return false;
		}
	}
	//写文件
	DWORD dwWritten = 0;
	if (!WriteFile(m_hFile, pszErrorLog, strlen(pszErrorLog), &dwWritten, NULL) || 0 == dwWritten)
	{
		return false;
	}

	//当原始点数达到 FILE_POINTS_COUNT 后重新创建新文件
	m_nCount++;
	if (m_nCount > 1000000)
	{
		m_nCount = 0;
		if (!CloseFile())
		{
			return false;
		}
		if (!CreateFile())
		{
			return false;
		}
	}

	return true;
}

//保存错误日志
bool CDataSave::SaveDataBin(const char* pszErrorLog, int iFileLen)
{
	if (INVALID_HANDLE_VALUE == m_hFileBin)
	{
		if (!CreateFileBin())
		{
			return false;
		}
	}
	//写文件
	DWORD dwWritten = 0;
	if (!WriteFile(m_hFileBin, pszErrorLog, iFileLen, &dwWritten, NULL) || 0 == dwWritten)
	{
		return false;
	}

	//当原始点数达到 FILE_POINTS_COUNT 后重新创建新文件
	//m_nCountBin++;
	//if (m_nCountBin > 10000)
	//{
	//	m_nCountBin = 0;
	//	if (!CloseFileBin())
	//	{
	//		return false;
	//	}
	//	if (!CreateFileBin())
	//	{
	//		return false;
	//	}
	//}
	SYSTEMTIME st;
	::GetLocalTime(&st);
	__int64 lTimeDiff = TimeDiff(st, m_lastSysTime);
	if (lTimeDiff > 1000 * 60 * 5)
	{
		m_lastSysTime = st;
		if (!CloseFileBin())
		{
			return false;
		}
		if (!CreateFileBin())
		{
			return false;
		}
	}

	return true;
}

BOOL CDataSave::DirectoryExists(const CString& Path)
{
#ifdef WIN32
	DWORD dwRetVal = GetFileAttributes(Path);
	if (dwRetVal == 0xFFFFFFFF)
		return FALSE;

	// SB:  check for directory attribute
	else if (dwRetVal & FILE_ATTRIBUTE_DIRECTORY)
		return TRUE;

	return FALSE;
#else
	CFileStatus FileStatus;

	if (CFile::GetStatus(Path, FileStatus) == TRUE &&
		FileStatus.m_attribute & directory)
	{
		return TRUE;
	} // if
	else if (FileSystemExists(GetFullPathName(Path)))
	{
		return TRUE;
	} // else if

	return FALSE;
#endif
} // DirectoryExists
BOOL CDataSave::MakePath(const CString& NewDirectory)
{
	CString NewDir = NewDirectory;  // Copy string for manipulation
	CString  DirName;
	BOOL    bRetVal = TRUE;

	// Error if no directory specified.
	if (NewDir.GetLength() == 0)
	{
		return FALSE;
	} // if

	// Make sure the directory name ends in a slash
	if (NewDir[NewDir.GetLength() - 1] != '\\' && NewDir[NewDir.GetLength() - 1] != '/')
	{
		NewDir = NewDir + '\\';
	} // if

	// Create each directory in the path
	int	nIndex = 0;
	int	nFSIndex = 0;
	BOOL  bDone = FALSE;
	while (!bDone)
	{
		// Extract one directory
		nIndex = NewDir.Find(_T('\\'));
		nFSIndex = NewDir.Find(_T('/'));
		if ((nFSIndex >= 0) && (nFSIndex < nIndex))	nIndex = nFSIndex;
		if (nIndex != -1)
		{
			DirName = DirName + NewDir.Left(nIndex);
			NewDir = NewDir.Right(NewDir.GetLength() - nIndex - 1);

			// The first time through, we might have a drive name
			if (DirName.GetLength() >= 1 && DirName[DirName.GetLength() - 1] != ':')
			{
				bRetVal = MakeDirectory(DirName);
			} // if
			DirName = DirName + '\\';
		} // if
		else
		{
			// We're finished
			bDone = TRUE;
		} // else
	} // while

	// Return the last MakeDirectory() return value.
	return bRetVal;
} // MakePath
	//***************************************************************************
		//
		// Name:    MakeDirectory
		//
		// Purpose: To make a directory.
		//
		// Example: BOOL bRetVal = fs.MakeDirectory("c:\\foo\\bar");
		//
		// Notes:   None
		//
		//***************************************************************************
		//@doc FileSys
		//@mfunc Creates a subdirectory.
		//@rdesc Nonzero if successful; 0 if an error occurred.
		//@parm const CString& | NewDirectory | The name of the new directory.
		//@xref <c FileSys> <mf FileSys::MakePath>
		// <mf FileSys::DeleteDirectory>
		//@ex | 
		// BOOL bRetVal = fs.MakeDirectory("c:\\foo\\bar");
BOOL CDataSave::MakeDirectory(const CString& NewDirectory)
{
#ifdef WIN32

	SECURITY_ATTRIBUTES security_attrib;
	security_attrib.nLength = sizeof(SECURITY_ATTRIBUTES);
	security_attrib.lpSecurityDescriptor = NULL;
	security_attrib.bInheritHandle = TRUE;

	BOOL bRetVal = CreateDirectory((const TCHAR*)NewDirectory, &security_attrib);
	return bRetVal;

#else

	int nRetVal = _tmkdir((const char*)NewDirectory);
	if (nRetVal == -1)
	{
		return FALSE;
	} // if

	return TRUE;

#endif
} // MakeDirectory

BOOL CDataSave::CreateDirectoryIfNone(LPCTSTR lpszPath)
{
	BOOL bRet = TRUE;
	CString strPath = lpszPath;

	if (!strPath.IsEmpty())
	{
		if (!DirectoryExists(strPath))
		{
			bRet = MakePath(strPath);
		}
	}
	return bRet;
}
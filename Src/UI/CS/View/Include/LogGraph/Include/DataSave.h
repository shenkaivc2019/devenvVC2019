#ifndef _gyc_sk_data_save_h_
#define _gyc_sk_data_save_h_
#define FILE_POINTS_COUNT 180000

class CDataSave
{
private:
	char m_szPath[512];//文件夹路径
	int m_nCount;//累积点数 
	int m_nCountBin;
	HANDLE m_hFile;
	HANDLE m_hFileBin;

	BOOL CreateFolder(LPCTSTR lpFolder);//创建文件夹
	bool CreateFile();//创建文件
	bool CreateFileBin();//创建文件
public:
	CString m_FileName;
	CString m_FilePath;
	CString m_FileNameBin;
	CString m_FilePathBin;
	SYSTEMTIME m_lastSysTime;
	CDataSave();//构造函数
	~CDataSave();
	void SetFolder(const char *pFilePath);
	
	bool CloseFile();//关闭文件
	bool CloseFileBin();//关闭文件
	bool SavePoint(float fRaw);//保存原始点
	bool SaveDecode(const char *pszDecode);
	bool SaveErrorLog(const char *pszErrorLog);
	bool SaveDataBin(const char* pszErrorLog, int iFileLen);
	BOOL DirectoryExists(const CString& Path);
	BOOL CreateDirectoryIfNone(LPCTSTR pFilename);
	BOOL MakePath(const CString& NewDirectory);
	BOOL MakeDirectory(const CString& NewDirectory);
};
#endif
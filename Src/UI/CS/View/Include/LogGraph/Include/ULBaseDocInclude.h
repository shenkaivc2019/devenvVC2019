// ULDoc.h: interface for the CULBaseDoc class.
//
//////////////////////////////////////////////////////////////////////
#pragma once
#ifndef _UL_DOC_BASE_INCLUDE_H
#define	_UL_DOC_BASE_INCLUDE_H

#include "ULInterface.h"
#include "ULCOMMDEF.H"

class CULBaseDoc;
typedef CList<CULBaseDoc*, CULBaseDoc*> CULBaseDocList;

#define  NSTRUCT 8  //宏定义结构体个数

class CCurve;
class CMDIChildWndEx;
typedef CList<CMDIChildWndEx*, CMDIChildWndEx*> CFrameUlChildList;
class CGraphHeaderView;
class CGraphWnd;
class CProject;
class CULFileBaseDoc;
class CULView;
class CPrintParam;
class CPof;
class CToolParams;
class CCustomizeView;

//定义一个结构体
typedef struct  
{
	bool IsShow;
	CString StrUp;
	CString StrDown;
	double MinUnit;
	double MaxUnit;
} FIELD;

class CSheet;
class CSheetInfo : public CTemplInfo
{
public:
	CSheet* m_pSheet;
	CMDIChildWndEx* m_pFrame;
	CSheetInfo()
	{
		m_pSheet = NULL;
		m_pFrame = NULL;
	}

	~CSheetInfo();
};

class CCell2000;
class CCellInfo : public CTemplInfo
{	
public:
	CCell2000* pCell;
	CMDIChildWndEx* m_pFrame;
	CCellInfo()
	{
		pCell = NULL;
		m_pFrame = NULL;
	}
};

class CCstInfo : public CTemplInfo
{
public:
	LPVOID	lpReserved;
	CMDIChildWndEx* m_pFrame;
	CCstInfo()
	{
		lpReserved = NULL;
		m_pFrame = NULL;
	}
};

typedef struct _tagCLOSEALLFILE
{
	BOOL bCancel;
	BOOL bYesAll;
	BOOL bNoAll;
}CLOSE_ALL_FILE;

#ifdef PEG_CHARTVIEW

typedef struct tagPegDataSource
{
	float   fdepth;        // 深度
	float	fDev;		   // 井斜
	float	fAzim;		   // 方位
}PegDataSource;

typedef struct tagPegDataULTARGET
{
	float	fVertDepth;	        // 垂深
	float   fSNPos;             //南北坐标
	float   fEWPos;             //东西坐标
	float   fAlpha;             //狗腿度
	float   fProOffset;         //投影位移
	float   fClosedAzim;        //闭合方位
	float   fHOffset;           //水平位移   
} PegDataULTARGET;

typedef struct tagDATAVALUE
{
	float	lDepth;				// 深度
	float	lVertDepth;			// 垂深
	float	fDev;				// 斜度、井斜
	float	fAzim;				// 方位
	float	fTrueAzim;			// 真方位、闭合方位
	float	fTotalAzim;			// 总方位、投影位移
	float	lTotalOffset;		// 总位移、水平位移
	float	lEPos;				// E坐标、
	float	lNPos;				// N坐标
	float	fAlpha;				// 狗腿度
	float   fTemperature;       //温度
	float   fGSide;             //重力高边
	float   fPSide;             //陀螺高边
}DATAVALUE;

#else

typedef struct tagDATAVALUE
{
	double	fDepth;				// 深度
	double	fVertDepth;			// 垂深
	double	fDev;				// 斜度
	double	fAzim;				// 方位
	double	fTrueAzim;			// 真方位
	double	fTotalAzim;			// 总方位
	double	fTotalOffset;		// 总位移
	double	fEPos;				// E坐标
	double	fNPos;				// N坐标
	double	fAlpha;				// 狗腿度
}DATAVALUE;

#endif

typedef std::vector<DATAVALUE>	VEC_DataValue;
typedef CArray<DATAVALUE, DATAVALUE> CARRAY_DataValue;

typedef struct tagULTARGET
{
	double	fVertDepth;			// 垂深
	double	fRadius;			// 靶半径
	double	fAzim;				// 靶方位
	double	fOffset;			// 靶位移
}ULTARGET;

typedef std::vector<ULTARGET>	VECTOR_Target;

typedef struct
{
	long	lDepth;				// 深度
	long	lVertDepth;			// 垂深
	long	lOffset;			// 距井口位移
	double	fAzim;				// 距井口方位
	long	lOffsetFromULTARGET;	// 距指定靶心位移
	double	fAzimFromULTARGET;	// 距指定靶心方位
}ValueInfo;

#define	AT_NEW	0x0001
#define AT_FRM	0x0002
#define AT_CRT	0x0004
#define _FileThread

#define FRAME_SIZE	250
#define DISPLAY_MAP 2000

typedef struct _tagULVERTEX_INFO
{
	float x;
	float z;
	BYTE bTime;
	BYTE bColorRank;
}ULVERTEX_INFO;

typedef struct _tagDRAW_INFO
{
	long nDepth;
	float nCoordinate;
	ULVERTEX_INFO VertexArray[FRAME_SIZE];
}DRAW_INFO;
/////////////////////////////////////////////////////////////////////////////////////

#define PI (double)3.14159265358979323846
#define AngleToRadian(Angle) (float)(PI * Angle / 180.0f)
#define cosa(Angle) cos(AngleToRadian(Angle))
#define sina(Angle) sin(AngleToRadian(Angle))
#define	ULRFT_PVT_CURVE_COUNT 64

class CULPrintInfo;
class CXMLSettings;
class CServiceTableItem;
class CULKernel;
class CULTool;
class CGroupView;
class CCurve;
class CEILog;

typedef CList<CCell2000*, CCell2000*> CCellList;


class CULFileBaseDoc;
typedef CList<CULFileBaseDoc*, CULFileBaseDoc*> CULFileBaseDocList;

#define SO_DEPT		0x0001
#define SO_DIR		0x0002
#define SO_METRIC	0x0100
#define SO_STEP		0x0200
#define SO_WRAP		0x0400
#define SO_MIN		0x0800
#define SO_INP		(SO_STEP|SO_MIN)
#define SO_DEPTH_ONCE_PER_RECORD 0x1000

typedef struct tagSAVEOPT
{
	DWORD dwOption;
	long lMinDepth;		// 文件另存时控制存盘的深度范围
	long lMaxDepth;		// 文件另存时控制存盘的深度范围
	BOOL bRecordDown;	// 文件的方向
	BOOL bMetric;		// 单位制
	long nStep;         // 采样间距
	float fVersion;		// LAS版本
    BOOL bDepthOncePerRecord;  // 另存为 LIS 格式时, 
                              // TRUE - 深度每记录保存一个; 
                              // FALSE - 深度每帧保存一个。
	BOOL bCurveRename;
	DWORD dwNameSpaceIndex;
}SAVEOPT;

#ifndef _LOGIC

typedef struct _tagRFT_PVT_MARK
{
	BOOL bShow;
	int nTime;
	double fValue;
	int nType;
	COLORREF cr;
}RFT_PVT_MARK;

#define RFT_TEST_TYPE_NORMAL	1
#define RFT_TEST_TYPE_LIMITED	2
#define RFT_TEST_TYPE_LOSTSEAL	3
#define RFT_TEST_TYPE_DRY		4
#define RFT_TEST_TYPE_UNRECOGNIZABLE 5

#define RFT_PROFILE_TYPE_SUMMARY	0
#define RFT_PROFILE_TYPE_MVD		1
#define RFT_PROFILE_TYPE_FVD		2
#define RFT_PROFILE_TYPE_BVD		3

#define RFT_PROFILE_DEPTH			0
#define RFT_PROFILE_TRUE			1

#define RFT_MARK_TYPE_RECT			1
#define RFT_MARK_TYPE_DTRI			2

#define RFT_MUD_FLAG				0x00000001
#define RFT_FLAG_DEFAULT			0
#define RFT_FLAG_MUD_BEFORE			0
#define RFT_FLAG_MUD_AFTER			1

typedef struct _tagRFT_PVT_CURVE
{
	BYTE cName[32];
	BOOL bShow;
	double fMinValue;
	double fMaxValue;
	COLORREF crColor;
}RFT_PVT_CURVE;

typedef struct _tagRFTPROPERTY
{
	BOOL bValidate;
	long lDepth;
	long lTrueDepth;
	UINT nTestType;

	double fMudBefore;
	double fMudAfter;
	double fLastBuild;
	double fFormation;
	double fFirstTime;
	double fSecondTime;
	double fBuildUpTime;
	double fMobility;
	double fVolumes;
	double fFactor;
	int nProbeType;

	int nSetStart;
	int nSetEnd;
	int nRetractStart;
	int nRetractEnd;

	double fHorzMin;
	double fHorzMax;
	double fGaugeMin;
	double fGaugeMax;
	UINT nHorzGrid;
	UINT nVertGrid;
	BYTE bHorzName[64];
	BYTE bVertName[64];
	BYTE bDepthUnit[16];
	BYTE bPressureUnit[16];
	BYTE bFileName[64];
	BYTE bTime[64];

	double fTimeStep;
	BYTE bGauge[64];

	RFT_PVT_CURVE Curve[ULRFT_PVT_CURVE_COUNT];
	//	CArray<RFT_PVT_CURVE, RFT_PVT_CURVE> Curve;

	RFT_PVT_MARK mrMudBefore;
	RFT_PVT_MARK mrMudAfter;
	RFT_PVT_MARK mrLastBuild;
	RFT_PVT_MARK mrFirstBegin;
	RFT_PVT_MARK mrSecondBegin;
	RFT_PVT_MARK mrBeginBuild;
}RFT_PROPERTY;

class CRftProfileLine : public CObject
{
public:
	CRftProfileLine();
	~CRftProfileLine();

	BOOL Compute();
	BOOL Clear();

public:
	double k;
	double b;
	double fStartValue;
	double fEndValue;
	double fStartDepth;
	double fEndDepth;
	COLORREF crColor;
	int nSize;
	CArray<double, double> Depth;
	CArray<double, double> Value;
};

class CRftProfileInfo : public CObject
{
public:

	CRftProfileInfo();
	~CRftProfileInfo();

public:
	double fStartDepth;
	double fEndDepth;
	int nProfileType;
	int nMarkType;
	COLORREF crMarkColor;

	CWnd *pWnd;
	CWnd *pParent;
	UINT nHorzGrid;
	UINT nVertGrid;
	double fHorzMin;
	double fHorzMax;
	double fVertMin;
	double fVertMax;
	CString strHorzName;
	CString strVertName;
	CString strDepthUnit;
	CString strPressureUnit;
	CString strTitle;
	UINT nFlag;

	BOOL bShowSelectLine;
	double fSelectLineValue;
	BOOL bSelectPos;
	int nSelectIndex;
	BOOL bOmit;
	int nOmitIndex;

	CArray<double, double> Depth;
	CArray<double, double> Value;
	CPtrArray ProfileLine;
};

#endif

//extern CString AfxGetComConfig()->m_strAppPath;

class CCurveData;
typedef struct tagSDATA
{
	CCurveData* pData;
	size_t sz;
	LPBYTE pValue;
	long lDepth0;
	long lDepthE;
	int  nRSample;
} SDATA;
//////////////////////////////////////////////////////////////////
typedef struct tagParam
{
	int nSize;
	double* pParam;
}Param;

#endif // _UL_DOC_BASE_INCLUDE_H

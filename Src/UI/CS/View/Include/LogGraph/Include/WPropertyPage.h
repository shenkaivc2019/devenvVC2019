#if !defined(AFX_NEWPROPERTYPAGE_H__B2E4CD12_8FC4_48B5_A826_528E34B3D089__INCLUDED_)
#define AFX_NEWPROPERTYPAGE_H__B2E4CD12_8FC4_48B5_A826_528E34B3D089__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// WPropertyPage.h : header file
class CProject;
/////////////////////////////////////////////////////////////////////////////
// CWPropertyPage dialog

class CWPropertyPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CWPropertyPage)

// Construction
public:
	CWPropertyPage();
	CWPropertyPage(UINT nIDTemplate);
	~CWPropertyPage();

// Dialog Data
	//{{AFX_DATA(CWPropertyPage)
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CWPropertyPage)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	BOOL OnCommand(WPARAM wParam, LPARAM lParam);

public:
	CStringArray m_string;
	virtual int WriteInfo(CProject* pProject);
	virtual void ReadInfo(CProject* pProject);

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CWPropertyPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NEWPROPERTYPAGE_H__B2E4CD12_8FC4_48B5_A826_528E34B3D089__INCLUDED_)

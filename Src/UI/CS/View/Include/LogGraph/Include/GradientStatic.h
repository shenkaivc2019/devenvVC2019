#if !defined(AFX_GRADIENTSTATIC_H__1931CD3D_F922_4484_883E_FDE7BE967062__INCLUDED_)
#define AFX_GRADIENTSTATIC_H__1931CD3D_F922_4484_883E_FDE7BE967062__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "StopCtrl.h"

// GradientStatic.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGradientStatic window

class AFX_EXT_CLASS CGradientStatic : public CWnd
{
	DECLARE_DYNAMIC(CGradientStatic)
// Construction
public:
	CGradientStatic();

// Attributes
public:
	CDWordArray m_tblLocat;
	CDWordArray m_tblColor;

	CList<CStopCtrl*, CStopCtrl*> m_lstStops;
	CStopCtrl* m_pSelected;
	int m_nStopID;
	CRect m_rectClick;

// Operations
public:
	void Draw(CDC* pDC, LPRECT lpRect);
	CStopCtrl* GetSelected();
	void DeleteSelected();
	void ResetColors(CDWordArray& tblLocat, CDWordArray& tblColor);
	void SetStopPos(int nPos);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGradientStatic)
	public:
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CGradientStatic();

	// Generated message map functions
protected:
	//{{AFX_MSG(CGradientStatic)
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	//afx_msg void OnSelectChanged(WPARAM wp);
	LRESULT OnSelectChanged(WPARAM wp, LPARAM lp);
	//afx_msg void OnPosChanged(WPARAM wp);
	LRESULT OnPosChanged(WPARAM wp, LPARAM lp);
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRADIENTSTATIC_H__1931CD3D_F922_4484_883E_FDE7BE967062__INCLUDED_)

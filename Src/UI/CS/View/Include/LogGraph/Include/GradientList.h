#if !defined(AFX_GRADIENTLIST_H__B9CE7E04_91FC_430E_B6A2_CC91CDD2B474__INCLUDED_)
#define AFX_GRADIENTLIST_H__B9CE7E04_91FC_430E_B6A2_CC91CDD2B474__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AFX_EXT_CLASS CGradientItem
{
public:
	CDWordArray tblLocat;
	CDWordArray tblColor;
};

// GradientList.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGradientList window

class AFX_EXT_CLASS CGradientList : public CListBox
{
// Construction
public:
	CGradientList();

// Attributes
public:

// Operations
public:
	void DrawGradient(LPDRAWITEMSTRUCT lpDIS);
	int AddGradient(LPCTSTR lpszItem, CDWordArray& tblLocat, CDWordArray& tblColor);
	void SetGradient(int nItem, CDWordArray& tblLocat, CDWordArray& tblColor);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGradientList)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	virtual void DeleteItem(LPDELETEITEMSTRUCT lpDeleteItemStruct);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CGradientList();

	// Generated message map functions
protected:
	//{{AFX_MSG(CGradientList)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRADIENTLIST_H__B9CE7E04_91FC_430E_B6A2_CC91CDD2B474__INCLUDED_)

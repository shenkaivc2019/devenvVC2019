// SCurve.h: interface for the CCurve class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCURVE_H__A4B6093B_E3BD_42DB_B8CF_9C2A81615095__INCLUDED_)
#define AFX_SCURVE_H__A4B6093B_E3BD_42DB_B8CF_9C2A81615095__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ICurve.h" 
class CSCurve
{
public:
	CSCurve(LPCTSTR pszName, LPCTSTR pszUnit);
	virtual ~CSCurve();

	int Dimension(); 
	double GetValueOfBuffer(int nIndex); 	
	int GetValueBufferSize(); 	
	void SetValType(VARTYPE lValType); 
	
public:
	VARTYPE m_nValueType;
	LPBYTE m_pValue;
    ICurve* m_pICurve; 

	int m_nMode;
	CString m_strName;
	CString m_strUnit;
	CString m_strUType;
	CString m_strUName;
	size_t m_nSizePoint;
	int m_nPointFrame;
};

#endif // !defined(AFX_SCURVE_H__A4B6093B_E3BD_42DB_B8CF_9C2A81615095__INCLUDED_)

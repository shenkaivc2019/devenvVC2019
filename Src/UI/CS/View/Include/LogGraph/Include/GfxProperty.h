#if !defined(AFX_GFXPROPERTY_H__10E221D2_0F42_11D2_842B_0000B43382FE__INCLUDED_)
#define AFX_GFXPROPERTY_H__10E221D2_0F42_11D2_842B_0000B43382FE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// GfxProperty.h : header file
//

#include "PinButton.h"
#include "F:\gitee\devenvVC2019\Src\Gfx\Src\MFCExt\resource.h"

/////////////////////////////////////////////////////////////////////////////
// CGfxProperty dialog
#ifndef MFCEXT_API
	#ifdef MFCEXT_EXPORT
		#define MFCEXT_API __declspec( dllexport )
	#else	
		#define MFCEXT_API __declspec( dllimport )
	#endif
#endif

class MFCEXT_API CGfxProperty : public CDialog
{
// Construction
public:
	void DoIt();
	void AddPage(CDialog* pPage, LPCTSTR pText);
	void AddPage(CDialog* pPage, UINT nIDText);
	void RemovePage(CDialog* pPage);
	CGfxProperty(CWnd* pParent = NULL);   // standard constructor

	virtual ~CGfxProperty();

	void MaximizeDlgRect(CDialog * pDlg, int & cx, int & cy);
	void ClearTabArray();

	CMFCTabCtrl	m_wndTab;
	enum { CG_TAB_HEIGHT = 20 };
	void InstallHook(BOOL bInstall, LPCTSTR pszText = NULL);

	bool LockingProperty(BOOL bLocking);
	bool GetLockingProperty();
	CDocument * pDocument;
	CWnd * pWnd;

// Dialog Data
	//{{AFX_DATA(CGfxProperty)
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_GFX_PROPERTY };
//#endif
	CPinButton	wndOk;
	CPinButton	wndPin;
	CPinButton	wndHelp;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGfxProperty)
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGfxProperty)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnBtHelp();
	afx_msg void OnBtPin();
	afx_msg void OnPaint();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

MFCEXT_API CGfxProperty* AfxGetGfxProperty();	// 属性对话框：用于全局

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GFXPROPERTY_H__10E221D2_0F42_11D2_842B_0000B43382FE__INCLUDED_)

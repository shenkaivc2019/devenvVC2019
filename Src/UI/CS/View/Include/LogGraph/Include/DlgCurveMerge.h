#if !defined(AFX_DLGCURVEMERGE_H__C936D2E5_8290_4B0E_AA42_367F38C7827C__INCLUDED_)
#define AFX_DLGCURVEMERGE_H__C936D2E5_8290_4B0E_AA42_367F38C7827C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// DlgCurveMerge.h : header file
//

class CULFile;
/////////////////////////////////////////////////////////////////////////////
// CDlgCurveMerge dialog

class CDlgCurveMerge : public CDialog
{
// Construction
public:
	CDlgCurveMerge(CULFile* pFile, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgCurveMerge)
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CURVE_MERGE };
#endif

	CCheckListBox	m_lbXCurves;
	CCheckListBox	m_lbACurves;
	CString	m_strASuffix;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgCurveMerge)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgCurveMerge)
	virtual BOOL OnInitDialog();
	afx_msg void OnButton9();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	afx_msg void OnButton3();
	afx_msg void OnButton4();
	afx_msg void OnButton5();
	afx_msg void OnButton6();
	afx_msg void OnButton7();
	afx_msg void OnButton8();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void InitCurves(CULFile* pFile, CCheckListBox* pList);
	void CheckAll(CCheckListBox* pList, BOOL bCheck = TRUE);
	void CheckInverse(CCheckListBox* pList);
	void CheckDif(CCheckListBox* pList, CCheckListBox* pDList);

public:
	CULFile*	m_pAFile;
	CULFile*	m_pFile;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGCURVEMERGE_H__C936D2E5_8290_4B0E_AA42_367F38C7827C__INCLUDED_)

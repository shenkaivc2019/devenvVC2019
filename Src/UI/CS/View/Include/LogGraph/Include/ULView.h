#if !defined(AFX_ULVIEW_H__BACE6398_490F_4830_8522_C817994E091E__INCLUDED_)
#define AFX_ULVIEW_H__BACE6398_490F_4830_8522_C817994E091E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MyMEMDC.H"

#define		DEFAULT_PAGE_HEIGHT		2400
#define		DEFAULT_PAGE_WIDTH		2160
#define		DEFAULT_TITLE_HEIGHT	200
#define		ITEM_HEIGHT				50
#define		DEFAULT_LOGINFO_HEIGHT	300
#define		DEFAULT_HEADER_HEIGHT	200
#define		DEFAULT_PERFORATIONCTRL 200

#define		MAX_CHAR				32

#define		CELL_HEIGHT		2400

// ��ͼ����
#define		ULV_GRAPH				0x00000001
#define		ULV_GRAPHCURVE			0x00000002 
#define		ULV_GRAPHHEAD			0x00000004
#define		ULV_LOGINFO				0x00000008
// 2023.08.18 Start
// #define		ULV_PROJECTINFO		0x00000010
// 2023.08.18 End
#define     ULV_TOOLINFO			0x00000020
#define		ULV_HOLLOW				0x00000040
// 2023.08.18 Start
// #define		ULV_CALREPORT			0x00000080
// 2023.08.18 End
#define		ULV_TOOLS				0x00000100
#define     ULV_RAWDATA				0x00000200
#define		ULV_PARAM				0x00000400
#define		ULV_CONSTRUCT			0x00000800
#define     ULV_GROUP				0x00001000
#define     ULV_CALSUMMARY			0x00002000
#define		ULV_CALCOEF				0x00004000
#define		ULV_CHARTS				0x00008000
//#define	ULV_ELUCIDATE			0x00010000
#define		ULV_UHISOLID			0x00010000
#define		ULV_CALCHART			0x00020000
#define		ULV_SCHEDULE			0X00040000
#define		ULV_RUNDATA				0x00080000
// 2020.3.9 Ver1.6.0 TASK��002�� Start	
//#define	ULV_RFTPVT				0x00080000
//#define	ULV_RFT_SUMMARY			0x00100000
//#define	ULV_RFT_DATAGROUP		0x00200000
//#define		ULV_HOLE			0x00100000
// 2020.3.9 Ver1.6.0 TASK��002�� End00
// 2023.08.18 Start
//#define		ULV_RFT_PROFILEGROUP	0x00400000
// 2023.08.18 End
#define     ULV_TABLEINFOVIEW		0x00800000
#define		ULV_TOOLINFOVIEW		0x01000000
#define		ULV_OUTPUTSTABLE		0x02000000
// 2023.08.18 Start
// #define     ULV_MARKREPORT          0x04000000
// 2023.08.18 End
#define     ULV_FACTOR              0x08000000
#define		ULV_CUSTOMIZEVIEW		0x10000000
// 2023.08.18 Start
// #define     ULV_CROSS_PLOT          0x20000000 
// 2023.08.18 End
#define		ULV_OGRESULT			0x40000000
#define		ULV_CALUSER				0x80000000
// 2023.08.18 Start
#define		ULV_EQUIPMENT			0x00100000
#define		ULV_CALIBRATION			0x00000080
#define		ULV_ENVIRONMENTAL		0x00400000
// #define		ULV_MNEMONICS			0x04000000
// #define		ULV_COMMENTS			0x20000000
// #define		ULV_REMARKS				0x00000010
// #define		ULV_DISCLAIMER			0x00200000
#define		ULV_SURVEY				0x04000000
// 2023.08.18 End

#define		ULV_GRAPHS			(ULV_GRAPHCURVE|ULV_GRAPH)
#define		ULV_CELL			(ULV_GRAPHHEAD|ULV_GROUP|ULV_CALCHART)
#define		ULV_CAL_REPORTS		(ULV_CALSUMMARY|ULV_CALCHART|ULV_CALCOEF|ULV_CALUSER)
#define		ULV_CVS				(ULV_CONSTRUCT|ULV_TOOLS|ULV_CUSTOMIZEVIEW|ULV_CHARTS|ULV_OGRESULT)

#define		EC_UNDO				0x00000001
#define		EC_REDO				0x00000002
#define		EC_SALL				0x00000004
#define		EC_HORZ				0x00000100
#define		EC_VERT				0x00000200
#define		EC_HZVT				0x00000300
#define		EC_AUTO				0x00001000
#define		EC_LOCAL			0x00002000
#define		EC_DEFINE			0x00010000

#define		Not(x, p)	if (x & (p))\
						{\
							x &= ~(p);\
						}\
						else\
						{\
							x |= (p);\
						}

#define		TM_FORMAT			_T("%b %d %H:%M %Y")

// ULView.h : header file
//
class CMainFrame;
class CMemDC;
class CCell2000;
class CGraphWnd;
class CPof;
class CProject;
class CULFile;
class CULPrintInfo;
class CULTool;
class CGraphHeaderView;
class CSymbolManagerDlg;
class CBCGPListCtrl;

/////////////////////////////////////////////////////////////////////////////
// CULView view

typedef struct _tagPDFPRINTINFO
{
	TCHAR strFileName[256];
	UINT nHeight;
}PDFPRINTINFO;

class CULView : public CScrollView
{
	DECLARE_DYNCREATE(CULView)

	// friend classes that call protected CULView overridables
	friend class CULDoc;
	friend class CWorkspaceBar;

public:
	CULView();           // constructor used by dynamic creation
	virtual ~CULView();

// Attributes
public:
	static  CMainFrame*		m_pMainWnd;
	static	CULPrintInfo*	m_pPrintInfo;
	static 	HCURSOR			m_hcurLine;
	static  HCURSOR			m_hcurRect;
	static CArray<PDFPRINTINFO, PDFPRINTINFO> m_pdfPrintInfo;
	static BOOL	m_bAutoFitPaper;
	static BOOL	m_bPrinter820;
	static BOOL m_bDefineSymbol;
	static CSymbolManagerDlg*	m_pSymDlg;
	static long	m_nLineStyle;
	static int	m_nFileID;
	static void InitFonts();
	static int	m_nTA;

public:
	CULDoc*		m_pDoc;
	CRect		m_rcPage;
	CString		m_strTempPath;
	DWORD		m_dwEdit;
	int			m_nRefresh;
	CCell2000*	m_pEdit;


// Operations

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CULView)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnUpdate();			// overridden to update this view
	virtual void OnUpdateCell();
	//}}AFX_VIRTUAL

public:
	virtual void OnChangeVisualStyle();
	virtual int CalcPageWidth();
	virtual int CalcPageHeight()
	{
		return DEFAULT_PAGE_HEIGHT;
	}

	virtual int CalcPrintHeight()
	{
		return DEFAULT_PAGE_HEIGHT;
	}


	virtual void SetScrollInformation();
	virtual	void Draw(CDC* pDC, LPRECT lpRect);
	virtual void Print();
	virtual void Print(CDC* pDC, LPRECT lpRect);
	virtual void SaveAsBitmap(CDC* pDC, LPCTSTR pszFile);
	virtual void LoadTempl(LPCTSTR pszFile) {}
	virtual void SaveTempl() {}
	virtual void SaveAsTempl(LPCTSTR pszFile) {}
	virtual void SaveService() {}
	virtual void OnColsSize(CSize szBorder = CSize(20, 20));
	virtual void OnRowsSize();
	
// Implementation
public:
	static void PrintBlank(long lHeight);
	static void DrawBlank(CDC* pDC, long lHeight);
	void PrintCell(CCell2000* pCell);
	void PrintCell(CString strCell);
	static void PrintList(CDC* pDC, LPRECT lpRect, CBCGPListCtrl* pList, 
						UINT nID, int nTH = DEFAULT_TITLE_HEIGHT);
	static void PrintLogInfo(CULDoc* pDoc, CGraphWnd* pGraph, DWORD dwStyle = 0);
	static void DrawTitle(CDC* pDC, LPRECT lpRect, LPCTSTR lpszTitle, int nTH = DEFAULT_TITLE_HEIGHT);
	static void DrawTextInRect(CDC* pDC, CString& strText, CRect& rect, UINT nFormat);
	void PrintCellImage(CCell2000* pCell, CDC* pDC, long col, long row, long nSheet, LPRECT lpRect);
	void PrintCell(CCell2000* pCell, CDC* pDC, CRect& rect);
	void PrintCalReport(CProject* pProject);
	void PrintCalReport(CULTool* pTool, UINT uPhase);

public:
	DWORD Locked();
	static long GetCellWidth(CCell2000* pCell, long type = 0, long border = 0);
	static long GetCellHeight(CCell2000* pCell, long type = 0);
	static CPoint GetCellPos(CCell2000* pCell, long col, long row, long type = 0, CPoint ptTopLeft = CPoint(0, 0));
	static CSize GetCellSize(CCell2000* pCell, long type = 0, CSize szBorder = CSize(2, 2));
	CSize SetCellFitSize(CCell2000* pCell, BOOL bPrint = FALSE);
	void ResizeCell();

	static void ResetCellSize(CCell2000* pCell);
	void SetCellFitColWidth(CCell2000* pCell, CSize szBorder = CSize(20, 20));
	static void AddPdfPrintInfo(UINT nHeight, CString strFileName);

	static void DefineCell(CCell2000* pCell, BOOL bNote = FALSE);
	static void DefineCell(CCell2000* pCell, long col, long row, long sheet);
	static void InvalidateCell(CCell2000* pCell, BOOL bNote = FALSE, CProject* pPrj = NULL);
	static void CloseSymDlg();
	BOOL SaveCell(CCell2000* pCell, long col, long row, long sheet);
	
	BOOL SetScrollPos32(int nBar, int nPos, BOOL bRedraw = TRUE);
    int	 GetScrollPos32(int nBar, BOOL bGetTrackPos);
	void OnDrawList(CDC* pDC, CBCGPListCtrl* pList);
	void DrawPage(CDC* pDC, CRect rect, COLORREF crPage = 0xFFFFFF, COLORREF crBorder = 0);
	BOOL BeginCapture();
	void CaptureWnd(CMyMemDC* pDC, CWnd* pWnd, CRect& rect, int nPos = 0);
	void EndCapture(BOOL bFullScreen = FALSE);
	void SaveAsBitmap(CString strFile);
	void SaveCellAsBitmap(CDC* pDC, LPCTSTR pszFile, CCell2000* pCell);
	void SaveListAsBitmap(CDC* pDC, LPCTSTR pszFile, CBCGPListCtrl* pList);
	void CreatePdfLinkFile(CString strFileName);
	BOOL CreateCell(CCell2000* pCell, CWnd* pParent, BOOL bAllowSize = TRUE, UINT nID = 123/*AFX_IDC_CELL*/);
	void PrepareCell(CCell2000* pCell, CDC* pDC, LPRECT lpRect);
	POINT ScrollToPositionEx(POINT pt);
	void ScrollToCellCurPos();

protected:
	
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CULView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnEditClear();
	afx_msg void OnEditClearAll();
	afx_msg void OnEditCopy();
	afx_msg void OnEditCut();
	afx_msg void OnEditFind();
	afx_msg void OnEditPaste();
	afx_msg void OnEditPasteSpecial();
	afx_msg void OnEditReplace();
	afx_msg void OnEditSelectAll();
	afx_msg void OnEditUndo();
	afx_msg void OnEditRedo();
	afx_msg void OnEditColInsertLeft();
	afx_msg void OnEditColInsertRight();
	afx_msg void OnEditRowInsertTop();
	afx_msg void OnEditRowInsertBottom();
	afx_msg void OnEditColRemove();
	afx_msg void OnEditRowRemove();
	afx_msg void OnEditColWidthBest();
	afx_msg void OnEditRowHeightBest();
	afx_msg void OnEditJoinCellRange();
	afx_msg void OnEditUnjoinCellRange();
	afx_msg void OnEditJoinRangeCol();
	afx_msg void OnEditJoinRangeRow();
	afx_msg void OnEditAlignTop();
	afx_msg void OnEditAlignMiddle();
	afx_msg void OnEditAlignBottom();
	afx_msg void OnEditAlignLeft();
	afx_msg void OnEditAlignCenter();
	afx_msg void OnEditAlignRight();
	afx_msg void OnEditFillColor();
	afx_msg void OnEditLineCombo();
	afx_msg void OnSymbolInsert();
	afx_msg void OnSymbolDefine();
	afx_msg void OnSymbolRefresh();
	afx_msg void OnUpdateSymbolInsert(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSymbolDefine(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSymbolRefresh(CCmdUI* pCmdUI);
	//}}AFX_MSG
	afx_msg	void OnUpdateEdit(CCmdUI* pCmdUI);
	afx_msg void OnBorderType(UINT nID);
	afx_msg void OnUpdateBorderType(CCmdUI* pCmdUI);
	afx_msg void OnSymbolManager();
	afx_msg void OnCustomizeSymbol();
	afx_msg void OnUpdateCustomizeSymbol(CCmdUI* pCmdUI);

	DECLARE_MESSAGE_MAP()
};

#define TMP_FILE(strTempl) (Gbl_AppPath+"\\Temp\\"+(strTempl)+".ulx")

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ULVIEW_H__BACE6398_490F_4830_8522_C817994E091E__INCLUDED_)

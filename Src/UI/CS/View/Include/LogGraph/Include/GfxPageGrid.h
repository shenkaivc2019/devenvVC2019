#if !defined(AFX_GFXPAGEGRID_H__B85BE816_26AD_44B4_B1BA_983058A0E54A__INCLUDED_)
#define AFX_GFXPAGEGRID_H__B85BE816_26AD_44B4_B1BA_983058A0E54A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "LineWidthCB.h"
#include "LineStyleCb.h"
#include "GfxPage.h"
#include "Track.h"
class CTrack;

/////////////////////////////////////////////////////////////////////////////
// CGfxPageGrid dialog

class CGfxPageGrid : public CGfxPage
{
// Construction
public:
	CGfxPageGrid(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CGfxPageGrid)
	enum { IDD = IDD_GFX_PAGE_GRID };
	CComboBox	m_cbVerticalGridType;
	CLineWidthCB m_cbVerticalSmallLinewidth;
	CLineWidthCB m_cbVerticalMiddleLinewidth;
	CLineStyleCB m_cbVerticalMiddleLinetype;
	CLineStyleCB m_cbVerticalSmallLinetype;
	CMFCColorButton m_btnVerticalSmallLineColor;
	CMFCColorButton m_btnVerticalMiddleLineColor;
	BOOL	m_bHLine;
	BOOL	m_bVLine;
	int		m_nGroup;
	int		m_nLineCount;
	BOOL	m_bHasVerticalSmallGrid;
	BOOL	m_bHasVerticalMiddleGrid;
	//}}AFX_DATA
	BOOL			m_bHorzGrid[3];
	double			m_nHorzGrid[3];
	CLineWidthCB	m_cbHorzLineWidth[3];
	CLineStyleCB	m_cbHorzLinetype[3];
	CMFCColorButton m_btnHorzLineColor[3];
	int             m_nDriveMode;   //add by gj 20121220
	double			m_nHorzGridTime[3];  
	double			m_nHorzGridDepth[3];
	long m_lStep1;
	long m_lStep2;
	long m_lStep3;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGfxPageGrid)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation

protected:
	// Generated message map functions
	//{{AFX_MSG(CGfxPageGrid)
	afx_msg void OnChangeEditGroup();
	afx_msg void OnChangeEditLineCount();
	virtual BOOL OnInitDialog();
	afx_msg void OnKillfocusEditLineCount();
	afx_msg void OnKillfocusEditGroup();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void	InitTrack();
	void	RefreshData();
	void    EnableDlgItem(UINT nID, BOOL bEnable);
	void    EnableHorzGridItem(BOOL bHLine);
	void    EnableVertGirdItem(BOOL bVLine);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GFXPAGEGRID_H__B85BE816_26AD_44B4_B1BA_983058A0E54A__INCLUDED_)

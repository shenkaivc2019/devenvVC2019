#if !defined(AFX_LINESTYLECB_H__550FF5B1_090C_4FF3_9D5E_9482AD682821__INCLUDED_)
#define AFX_LINESTYLECB_H__550FF5B1_090C_4FF3_9D5E_9482AD682821__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LineStyleCB.h : header file
//

//#define		SCB_MAX_LINES			8
#define		SCB_MAX_LINES			9
#define		SCB_MAX_LINE_NAME		16

struct	LineStyleAndName
{
	LineStyleAndName()
	{
		ZeroMemory( this, sizeof(LineStyleAndName));
	};
	LineStyleAndName(int nLineStyle, LPCTSTR cpName)
	{
		ZeroMemory( this, sizeof(LineStyleAndName));
		m_nLineStyle = nLineStyle;
		_tcsncpy( m_cLineName, cpName, SCB_MAX_LINE_NAME);
	};
	int		m_nLineStyle;
	TCHAR	m_cLineName[ SCB_MAX_LINE_NAME ];
};

/////////////////////////////////////////////////////////////////////////////
// CLineStyleCB window

class AFX_EXT_CLASS CLineStyleCB : public CComboBox
{
// Construction
public:
	CLineStyleCB();

// Attributes
public:
	void			Initialize( void );

private:
	BOOL			m_bInitialized;
	CString			m_strName;

	static LineStyleAndName	m_Lines[SCB_MAX_LINES];

private:

public:
	int				GetSelectedLineStyle( void );
	CString			GetSelectedLineName( void );

	void			SetSelectedLineStyle(int nLineStyle);
	void			SetSelectedLineName(CString& strName);

	bool			RemoveLine(int nLineStyle);
	bool			RemoveLine(CString& strName);
	
	int				AddLine(int nLineStyle, CString& strName);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLineStyleCB)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLineStyleCB();

	// Generated message map functions
protected:
	//{{AFX_MSG(CLineStyleCB)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LINESTYLECB_H__550FF5B1_090C_4FF3_9D5E_9482AD682821__INCLUDED_)

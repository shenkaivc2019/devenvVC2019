////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 1999 by Thierry Maurel
// All rights reserved
//
// Distribute freely, except: don't remove my name from the source or
// documentation (don't take credit for my work), mark your changes (don't
// get me blamed for your possible bugs), don't alter or remove this
// notice.
// No warrantee of any kind, express or implied, is included with this
// software; use at your own risk, responsibility for damages (if any) to
// anyone resulting from the use of this software rests entirely with the
// user.
//
// Send bug reports, bug fixes, enhancements, requests, flames, etc., and
// I'll try to keep a version up to date.  I can be reached as follows:
//    tmaurel@caramail.com
//
////////////////////////////////////////////////////////////////////////////////
// Version : 1.0					   * Author : T.Maurel
// Date    : 30.08.99
//
// MultiRectTracker.h: interface for the CMultiRectTracker class.
//
//////////////////////////////////////////////////////////////////////
//
// This class is useful for vector editors, or similar applications
// where the user can select or move some objects on the screen.
// Anywhere you use CRectTracker, you can use this extended version,
// with the same options for drawing (handle size, style...).
// The principle is contains in a loop : for each object, update the 
// m_rect and call the CRectTracker corresponding function.
// The m_rect is not used in other cases, so there is no potential pb.
//
//////////////////////////////////////////////////////////////////////
#if !defined(AFX_MULTIRECTTRACKER_H__62278817_5EF6_11D3_9F79_AE25E9FEAB06__INCLUDED_)
#define AFX_MULTIRECTTRACKER_H__62278817_5EF6_11D3_9F79_AE25E9FEAB06__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Just because of the templates
#include <afxtempl.h>
#include "MRTObject.h"
#include "IULFile.h"

#define _AUTOSCROLLING

class CMRTOprt;
class CULDoc;

////////////////////////////////////////////////////////////////////////////////
// This class use some of CRectTracker functions, (hit, draw, cursor...)
// to be compatible with the Microsoft tracker drawing.
//
class CMultiRectTracker : public CRectTracker
{
	friend class CMRTObject;

	// Constructors/Destructor
public:
	CMultiRectTracker();
	virtual ~CMultiRectTracker();

	// API
public:
	// Very similar to CRectTracker functions
	void Draw(CDC* pDC);
	int HitTest(CPoint point, CDC* pDC);
	int HitTestHandles(CPoint point, CDC* pDC);
	BOOL SetCursor(CWnd* pWnd, UINT nHitTest, CDC* pDC);
	BOOL Track(CWnd* pWnd, CPoint point, CDC* pDC, BOOL bAllowInvert = FALSE,
		CWnd* pWndClipTo = NULL);

	// Specific to the CMultiRectTracker
	void Add(CMRTObject* pObject);
	void Remove(CMRTObject* pObject);
	int Find(CMRTObject* pObject);
	void RemoveAll();
	void ClearObjects();

	void Reorder(long lOrder);
	void Group();
	void Ungroup();
	void Select(CMRTOprt* pOprt);

	void SetRectColor(COLORREF clrRect);
	CRect GetGroupRect();
	CRect GetTotalRect();
	
	BOOL CreateInPlaceEdit(CRect rectEdit, CWnd* pWnd);
	BOOL DestroyEdit();
	
	// General operations
	int OnLButtonDown(UINT nFlags, CPoint point, CWnd* pWnd, CDC* pDC);
	int OnLButtonDblClk(CPoint point, CWnd* pWnd, CDC* pDC);
	int OnChar(UINT nChar, UINT nRepCnt, UINT nFlags, CWnd* pWnd, CDC* pDC);
	BOOL PreTranslateMessage(MSG* pMsg, CWnd* pWnd);

	// Edit operations
	void Align(DWORD alignment, CMRTObject* pObj0 = NULL);
	void AlignMMText(CArray<CMRTObject*, CMRTObject*>* objects, DWORD alignment, CMRTObject* pObj0 = NULL);

	void Undo(CWnd* pWnd);
	void Redo(CWnd* pWnd);

	void ObjLock();
	void BackLayer();

	void OnEditDelete();
	void OnEditProp(CWnd* pWnd);
	void OnEditCopy(CWnd* pWnd);
	void OnEditCut(CWnd* pWnd);
	void OnEditPaste(CWnd* pWnd);
	void OnUpdateEditPaste(CCmdUI* pCmdUI);

	void AddOprt(CMRTOprt* pOprt);

	BOOL CanUndo()
	{
		if (m_lstStack.GetCount())
			return (m_posCurr != NULL);

		return FALSE;
	}

	BOOL CanRedo()
	{
		return (m_posCurr != m_lstStack.GetTailPosition());
	}

	CString GetNewPicName();
	void SetDoc(IULFile* pDoc);
	
	// Internal functions
protected:
	bool IsEmpty()
	{
		return (m_Objects.GetSize() == 0);
	}
	
	void CreatePen();
	void GetDrawHandleRect(int nHandle, CRect* pHandleRect) const;
	void OwnerDraw(CDC* pDC, CMRTObject* pObject, BOOL bHighLight = TRUE);
	void EraseTrackerRect(LPCRECT lpRect, CWnd* pWndClipTo, CDC* pDC, CWnd* pWnd);
	virtual void DrawTrackerRect(LPCRECT lpRect, CWnd* pWndClipTo,
		CDC* pDC, CWnd* pWnd);

	// The most important function !
	BOOL MultiTrackHandle(CWnd* pWnd, CPoint point, CDC* pDC, CWnd* pWndClipTo);
	BOOL TrackHandleEx(int nHandle, CWnd* pWnd, CPoint point, CWnd* pWndClipTo, CDC* pDC);

	void CopyPositions(CDC* pDC); // Get a copy of the rectangles
	void UpdateObjects(CDC* pDC); // Update the objects positions, from copied rectangles
	void ClearPositions(); // erase the rectangle's copy

	void OnPrepareDC(CDC* pDC);
	void Obj2Clipboard(COleDataSource* pSource);
	void Txt2Clipboard(COleDataSource* pSource, CList<CMRTObject*,CMRTObject*>* pObjects);
	void Rtf2Clipboard(COleDataSource* pSource);
	void Wmf2Clipboard(COleDataSource* pSource, CList<CMRTObject*,CMRTObject*>* pObjects);
	void Bmp2Clipboard(COleDataSource* pSource, CList<CMRTObject*,CMRTObject*>* pObjects);
	void AddSort(CMRTObject* pObject, CList<CMRTObject*, CMRTObject*>* pList);
	

public:
	CList<CMRTOprt*, CMRTOprt*> m_lstStack;
	POSITION m_posCurr;

	CList<CMRTObject*, CMRTObject*> m_lstObjs;
	CMRTObjList	m_lstAdds;
	CMRTObjList m_lstDels;
	CMRTObjList m_lstGrps;

	CArray<CMRTObject*, CMRTObject*> m_Objects; // Array of objects pointers
	BOOL m_dwAdd;
	CString m_strAdd;

	BOOL m_bFirstErase;
	CEdit* m_pEdit;
	CMRTObject* m_pObject; // temporary pointer used if the hit is on an object.
	CMRTObject* m_pOldObj;
	CWnd* m_pWnd;
	IULFile* m_pDoc;

	// Internal datas
protected:
	UINT m_nRCF;
	UINT m_nBIFF;
	UINT m_nXMLSF;
	UINT m_nHTMLF;
	UINT m_nRTFF;
	UINT m_nCSVF;
	
	COLORREF m_clrRect;
	CArray <CRect*, CRect*> m_CopyPosition; // array of rectangles, objects position copy.
#ifdef _AUTOSCROLLING
	CRect m_rectGroup;
#endif
};

#endif // !defined(AFX_MULTIRECTTRACKER_H__62278817_5EF6_11D3_9F79_AE25E9FEAB06__INCLUDED_)

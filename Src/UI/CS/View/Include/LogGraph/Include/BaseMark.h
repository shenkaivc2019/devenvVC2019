// BaseMark.h: interface for the CBaseMark class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BASEMARK_H__3925C4A1_2CAB_4E82_9A07_607BBE6673DC__INCLUDED_)
#define AFX_BASEMARK_H__3925C4A1_2CAB_4E82_9A07_607BBE6673DC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MARKS_LINE		0
#define MARKS_TRIANGLE	1
#define MARKS_ARROW		2
#define MARKS_TD		3
#define MARKS_FR		4
#define MARKS_CS		5
#define MARK_Curve		6

#define DEHT_NONE				0
#define DEHT_BODY				1
#define DEHT_TOPLEFT			2
#define DEHT_TOPMIDDLE			3
#define DEHT_TOPRIGHT			4
#define DEHT_BOTTOMLEFT			5
#define DEHT_BOTTOMMIDDLE		6
#define DEHT_BOTTOMRIGHT		7
#define DEHT_LEFTMIDDLE			8
#define DEHT_RIGHTMIDDLE		9

#include "XMLSettings.h"
#include "PrintInfo.h"
#include "ComConfig.h"
class CTrack;
class CSheet;
class CBaseMark : public CObject
{
	DECLARE_DYNAMIC(CBaseMark)
	//DECLARE_DYNCREATE(CBaseMark)
protected:
	USHORT		m_nArrowLineStyle;		// 曲线样式(实线、虚线...)
    DWORD		m_nArrowLineWidth;		// 曲线宽度
    COLORREF	m_crArrowColor;			// 曲线颜色
	int			m_nLineLength;

	// Sizes
	CSize m_szMarkerSize;
	bool m_bLock;
	bool m_bTextLine;
	bool m_bAttDepth;
public:
	CSheet* m_pSheet;
public:
	CBaseMark();
	virtual ~CBaseMark();
	void        Serialize(CXMLSettings& xml);
	virtual void Draw(CDC* pDC, long lStartDepth, long lEndDepth);
	virtual void Print(CULPrintInfo* pInfo, float fMinDepth, float fMaxDepth);
	virtual BOOL IsPtInShape(CPoint pt);
	void		DrawArrow(CDC *pDC , CRect rcStart , CPoint ptEnd);
//	BOOL		DrawCurveFlag(CDC* pDC, long lStartDepth, long lEndDepth, int nCount);
	virtual long ComputeDepthOffSet(long lStartDepth, long lEndDepth);
	void		DrawTD(CDC *pDC , long nXPos , int nYPos);
	void		UpdateFont(CDC *pDC , LOGFONT& lf);
	void Clear();
	void SetMarkerSize(CSize szMarkerSize);
	CSize GetMarkerSize() const;	
	double GetLeft() const;
	double GetRight() const;
	double GetTop() const;
	double GetBottom() const;
	void SetLeft(double left);
	void SetRight(double right);
	void SetTop(double top);
	void SetBottom(double bottom);
	CRect GetRect() const;
	//	int GetHitCode(CPoint point);
	int GetHitCode(CPoint point) const;
	int GetHitCode(const CPoint& point, const CRect& rect) const;
	// Selection
	virtual void DrawSelectionMarkers(CDC* dc, CRect rect) const;
	virtual CRect GetSelectionMarkerRect(UINT marker, CRect rect) const;
public:
	int			m_nTrackNO;			// 通道号
	int         m_nShape;           // 标记形状
	COLORREF    m_crColor;          // 标记颜色
	COLORREF    m_crText;			// 标记文本颜色
    double      m_fMarkXCoordinate;	// 标记的横坐标
	double		m_fRawXCoordinate;	// 标记原点横坐标，用于FR标签的指针
	CTrack*		m_pTrack;			// 标记井道
	int			m_nPointer;			// 标记指向	0左边界越界，1右边界越界
	BOOL        m_bDepthChange;		// 拖动标签时名称是否改变
	CString		m_strName;			// 标记名称
	//CCurve*  	m_pCurve;			// 标记曲线
	CRect		m_rect;
	CRect       m_rtText;
	BOOL        m_bHot;
	long		m_lDepth;			// 标记深度
	long		m_lRawDepth;		// 标记原点深度，用于FR标签的指针
	float       m_fXScale;
	BOOL        m_bWatch;           // 监视型标签
	BOOL        m_bRealTime;        // 实时测井时仪器库中添加的标签
	double		m_fLeftMargin;
	double		m_fRightMargin;
	CPoint		m_pointCurveFlag;	//曲线标识点
	CArray<long,long>		m_Arr_lDepthFlag;   //保存曲线表示的深度点
	CArray<CPoint,CPoint>	m_Array_point;
	int			m_nIndex,m_nIndexText;
	int			m_nCurve;
	ULFONT		m_FontInfo;
	CFont		m_Font;

	float		m_fAngle;
	DWORD		m_defWidth;
	BOOL		m_bSelected;
	ULMDWD		GetArrowColor() { return m_crArrowColor; }
	ULMETHOD	SetArrowColor(COLORREF crColor) { m_crArrowColor = crColor; return 0; }
	ULMINT		GetArrowLineStyle() { return	m_nArrowLineStyle; }
	ULMETHOD	SetArrowLineStyle(int nLineStyle) { m_nArrowLineStyle = nLineStyle; return 0; }
	ULMINT		GetArrowLineWidth() { return	m_nArrowLineWidth; }
	ULMETHOD	SetArrowLineWidth(int nLineWidth) { m_nArrowLineWidth = nLineWidth; return 0; }
	ULMINT		GetArrowLineLength() { return m_nLineLength; }
	ULMETHOD	SetArrowLineLength(int nLineLength) { m_nLineLength = nLineLength; return 0; }
	BOOL IsSelected() const;
	void Select(BOOL selected);
	void SetRect(double left, double top, double right, double bottom);
	void setLock(bool bLock) { m_bLock = bLock; };
	bool getLock() { return m_bLock; };
	void setTextLine(bool bTextLine) { m_bTextLine = bTextLine; };
	bool getTextLine() { return m_bTextLine; };
	void setAttDepth(bool bAttDepth) { m_bAttDepth = bAttDepth; };
	bool getAttDepth() { return m_bAttDepth; };

	CString		m_strContent;			// 标记内容
	int m_iArrowLinePos;
	int			m_nFirstLineLength;
	int m_iTextAlign;
	int m_nArrowLineLen;
};

#endif // !defined(AFX_BASEMARK_H__3925C4A1_2CAB_4E82_9A07_607BBE6673DC__INCLUDED_)

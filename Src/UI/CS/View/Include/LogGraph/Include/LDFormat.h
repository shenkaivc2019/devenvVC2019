/*******************************************************************************
  Copyright (C) 2004-2007 CPL Logging company.
  
  Source File Name	    :	LDFormat.h
  Project Name		    :	EILOG Software system
  File Generated Time	:	2004-5-28
  Description			:	LDF file format head
    
  Modify History  :
--------------------------------------------------------------------------------  
    Date	   	      Author	                 Modify Description
---------------	    -------------------	     -------------------------------
2006-5-16            chen jiang hao            add curve type define
2005-11-28           chen jiang hao            modify curvename length 8 to 16
2005-11-18           chen jiang hao            modify _log_operate_info & liquid info & well info
2005-7-5             chen jiang hao            modify tool info
2005-6-21            chen jiang hao            modify liquid info
2005-6-20            chen jiang hao            modify well info 
2005-4-22			 chen jiang hao            add tool work mode
2004-11-10	         chen jiang hao            add curve calcdelay 
2004-5-28            chen jiang hao		       define format struct 

*******************************************************************************/

#ifndef _EILOG_LDF_H 
#define _EILOG_LDF_H 

#if _MSC_VER > 1000
#pragma once
#endif 

#pragma pack( push, 1)

//曲线类型
#define CurveType_CURVE		0
#define CurveType_IMAGE		1
#define CurveType_WAVE		2
#define CurveType_VDL		3
#define CurveType_IMAGE_BHTV 5
#define CurveType_CONST		 6

//数据类型
#define DataType_BYTE		0
#define DataType_SHORT		1
#define DataType_USHORT		2
#define DataType_LONG		3
#define DataType_ULONG		4
#define DataType_FLOAT		5
#define DataType_DOUBLE		6

/*测井采样方式*/
/*模拟*/
#define Simulate_Mode    0x00
/*脉冲*/
#define Pluse_Mode       0x01
/*3506*/
#define PCM3506_Mode     0x02
/*3508*/
#define PCM3508_Mode     0x03
/*生产测井*/
#define WTC_Mode         0x04
/*TCC100K*/
#define BPSK_Mode    0x05
/*DTB*/
#define DTB_Mode     0x06
/*CAN*/
#define CAN_Mode     0x07

/*测井记录方式*/
/*深度中断上传数据*/
#define DEPTH_INT_TRANSFER_MODE          0
/*定时上传数据*/
#define TIMER_TRANSFER_MODE              1
/*等帧上传数据*/
#define FRAME_TRANSFER_MODE              2

//测井方向
//向上
#define ACME_LOG_UP				1
//向下
#define ACME_LOG_DOWN			-1
//固定
#define LOG_STATIONARY		0

#define SYSTEM_NOTIFY     255
#define BROADCAST_NOTIFY  254
#define ERROR_NOTIFY      253 

typedef struct _standard_file_head          //标准文件头
{
    char szToken[8];					// 文件标示号  RAW = 原始数据  LDFDAT = 工程量数据
    unsigned short  wVersion;				// 版本号  高位 0X01 主版本号 低位 0x00  副版本号
    unsigned int uWellInfoPt;				// 井场信息块起始位置
    unsigned int uToolInfoPt;				// 仪器信息块起始位置
    unsigned int uDataPt;				// 数据体起始位置
    unsigned int uFrameSize;				// 一帧数据大小

} STD_FILE_HEAD;//26字节

typedef struct _general_info_head            // 通用文件头
{
    char  szFileName[80];					 // 文件名
    float fStartIndex;						 // 起始索引值
    float fEndIndex;						 // 终止索引值
    char  szIndexUnit[10];					 // 索引值单位
    float fMaxInterval;						 // 最大采样间隔
    short sLogDirection;					 // 测井方向
    short sLogRecordMode;					 // 测井记录方式
    short sLogSampleMode;					 // 测井采样方式
    float fStartDepth;						 // 测井开始深度
    float fEndDepth;						 // 测井结束深度
    short sToolNum;						     // 仪器数 
    char  Reserve[20];						 // 保留

} GEN_INFO_HEAD; //138字节


typedef struct _tool_info_struct			 //仪器信息结构
{
	char  szName[16];			// 仪器名	 
	char  szDescription[60];	// 仪器描述
	char  szSerialNo[16];		// 仪器系列号				 
	char  szAsset[40];			// 仪器资产号
	char  szImageFile[255];		// 仪器系列标识
	BOOL  bIsShareChannel;      // 是否共享信道
	float fDiameter;			// 仪器直径 厘米 		 
	float fLength;				// 仪器长度 
	float fWeight;				// 仪器重量
	float fMaxSpeed;			// 仪器最大测速 
	ULONG uOffset;				// 仪器数据偏移
	ULONG uInDataSize;			// 仪器输出数据大小
	ULONG uOutDataSize;			// 仪器数据块大小	 
	ULONG uCurveNum;			// 仪器曲线数	
	ULONG uWorkMode;			// 仪器工作模式
} TOOL_INFO_STRUCT;


typedef struct _curve_info_struct          // 曲线信息结构
{
    char szName[16];						// 曲线名
    char szAlias[32];						// 曲线别名     
    char szDescription[60];               // 描述曲线
    char szUnit[16];       				  // 曲线数据单位
    unsigned long bComputed; 			  // 是否计算曲线
    unsigned long uCurveType;			  // 曲线类型
    float fLValue;         				  // 左工程值
    float fRValue;         				  // 右工程值
    float fMutiple;        				  // 乘因子
    float fAddition;       				   // 加因子
    float fDelay;          				  // 物理延迟
    float fCalcDelay;					   // 计算延迟
	LONG  uFilter;         				   // 滤波       
	LONG  uDataType;					   // 数据类型
	LONG  uDataLength;						// 数据长度	
	LONG  uVertPtNum;						// 纵向采样数	
	LONG  uHorzPtNum;						// 径向采样数	
	char  szHorzUnit[16];					// 径向采样数据单位
	float fHorzValue;						// 径向采样起始值	
	float fHorzPtInterval;  				// 径向采样间隔	       
	short sOffset;     			            // 曲线数据偏移
} CURVE_INFO_STRUCT;

//操作信息
typedef struct _log_operate_info		//测井操作信息
{	
  char servicecompany[80];				//测井公司名
  char oilcompany[80];					//油公司
  char well[80];						//井名
  char nation[60];						//国家
  char state[60];						//洲/省
  char county[60];						//县/区
  char field[80];						//油田
  char location[200];					//位置描述
  char team[80];						//测井队
  char recordedBy[20];					//操作员
  char witnessedBy[20];					//现场测井监督
  char serviceOrderNo[40];				//服务订单号

} LOG_OPERATE_INFO;

//井场信息
typedef struct _well_info
{
  float KB;							    //方钻杆补心高度
  float EDF;							//钻台高度
  float GL;							    //地面/海平面
  float ZeroLength;						//对零长度
  char  BitInfo[1024];                  //钻头程序
  char  CaseInfo[1024];                 //套管程序
  float DepthDrill;                     //井时井深
  float Bottom_Logged_Depth;            //测量段底部深度
  float Top_Logged_Depth;               //测量段顶部深度
  char  szLogTime[12];					 //测井时间
  char  szDrillTime[12];				 //完钻时间
  char  reserve[36];					//保留
} WELL_INFO;
//
typedef struct _borehole_liquid_info
{
  char  typeFluid[60];					    //流体类型
  char  sourceSample[60];				    //样品来源
  char fluidDensity[60];					//密度（1b/gal）
  char fluidViscosity[60];					//弹性粘度
  char noWaterAPI[60];                      //失水API度(cm3/30min)
  char fluid_pH[60];						//pH值或泥浆碱度
  char RmMeasTemp[60];						//测量温度下泥浆电阻率
  char RmfMeasTemp[60];						//测量温度下泥浆滤液电阻率                                
  char RmcMeasTemp[60];						//测量温度下泥饼电阻率
  char  timeCircBegin[60];				    //循环开始日期/时间
  char  timeCircStopped[60];			    //循环结束日期/时间
  char reserve[20];						    //保留
} BOREHOLE_LIQUID_INFO;


#pragma pack(pop , _EILOG_LDF_H)

#endif












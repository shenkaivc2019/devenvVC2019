// UnA.h : Declaration of the CUnA

#ifndef __UNA_H_
#define __UNA_H_

//#include "resource.h"       // main symbols

#ifndef UNA_API
	#ifdef UNA_EXPORT
		#define UNA_API __declspec( dllexport )
	#else	
		#define UNA_API __declspec( dllimport )
	#endif
#endif
/////////////////////////////////////////////////////////////////////////////
// CUnA
class UNA_API CUnA
{
public:
	CString	m_strText;	 // STANDARD_FORM
	CStringList texts;	// TEXTS_FORM
	CString description;
	double	k0;	// To a standard unit
	double	b0;
	double  n0;
	double  m0;
	double  k1; // To a general unit
	double  b1;
	double  n1;
	double  m1;

	CUnA()
	{
	}

// IUnA
public:
	LPCTSTR get_Text();
	double TX(double s);
//2020.9.19 sys 单位管理 Start
	double TSX(double s);
//2020.9.19 sys 单位管理 End
	double TS(double d);
};

UNA_API CUnA* AfxGetUnA();

#endif //__UNA_H_

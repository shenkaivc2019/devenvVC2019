#if !defined(AFX_GFXPAGETRACK_H__1A397A47_7DB6_4EAC_B8AA_888CE4C751C6__INCLUDED_1)
#define AFX_GFXPAGETRACK_H__1A397A47_7DB6_4EAC_B8AA_888CE4C751C6__INCLUDED_1

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GfxPageTrack.h : header file
//
#include "LineWidthCB.h"
#include "LineStyleCb.h"
#include "GfxPage.h"

class CSheet;
/////////////////////////////////////////////////////////////////////////////
// CGfxPageGraph dialog

class CGfxPageGraph : public CGfxPage
{
// Construction
public:
	CGfxPageGraph(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CGfxPageGraph)
	enum { IDD = IDD_GFX_PAGE_GRAPH };
	CComboBox m_cbHRatio;
	CComboBox m_cbVRatio;
	CComboBox m_cbDepthMode;
	CString	m_strGraphName;
	long	m_lDepth0;
	long	m_lDepth1;
	CMFCColorButton m_btnDarkColor;
	//}}AFX_DATA
protected:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGfxPageGraph)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGfxPageGraph)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeDev();
	afx_msg void OnSelchangeAzim();
	afx_msg void OnChangeGraphName();
	afx_msg void OnKillfocusStartDepthPos();
	afx_msg void OnKillfocusEndDepthPos();
	afx_msg void OnKillfocusHratio();
	afx_msg void OnKillfocusVratio();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void  InitGraph();
	void  RefreshData();
	CString m_strFormat;
	long	m_lDepthA0;
	long	m_lDepthA1;

	BOOL IsInch(float fRatioTime); // 判断设置的时间比例是否为英寸单位，是则返回TRUE，不是返回FALSE
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GFXPAGETRACK_H__1A397A47_7DB6_4EAC_B8AA_888CE4C751C6__INCLUDED_1)

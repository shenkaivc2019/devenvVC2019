// FilterParam.h: interface for the CFilterParam class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FILTERPARAM_H__DD284226_6FDD_4DB9_9C45_17790606F734__INCLUDED_)
#define AFX_FILTERPARAM_H__DD284226_6FDD_4DB9_9C45_17790606F734__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CFilterParam  
{
public:
	CFilterParam()
	{
		m_dValue = 0.0;
	}

	CFilterParam(CFilterParam* pFP)
	{
		m_strName = pFP->m_strName;
		m_dValue = pFP->m_dValue;
		m_strDesc = pFP->m_strDesc;
	}
	
	virtual ~CFilterParam()
	{

	}

public:
	CString	m_strName;		// 参数名称
	double	m_dValue;		// 参数值
	CString m_strDesc;		// 参数描述

};

typedef CList<CFilterParam*, CFilterParam*>	CFPList;

class CXMLSettings;
class CFilterParams
{
public:
	CFilterParams() { }
	~CFilterParams()
	{
		while (m_fpList.GetCount())
			delete m_fpList.RemoveHead();
	}

	inline BOOL Add(LPCTSTR pszName, double dValue, LPCTSTR pszDesc)
	{
		CFilterParam* pFP = new CFilterParam;
		pFP->m_strName = pszName;
		pFP->m_dValue = dValue;
		pFP->m_strDesc = pszDesc;
		m_fpList.AddTail(pFP);
		return TRUE;
	}

	inline void ClearParams()
	{
		while (m_fpList.GetCount())
			delete m_fpList.RemoveHead();
	}

	CString GetName(int nIndex)
	{
		POSITION pos = m_fpList.FindIndex(nIndex);
		if (pos != NULL)
		{
			CFilterParam* pFP = m_fpList.GetAt(pos);
			return pFP->m_strName;
		}
		
		return _T("");
	}

	double GetValue(int nIndex)
	{
		POSITION pos = m_fpList.FindIndex(nIndex);
		if (pos != NULL)
		{
			CFilterParam* pFP = m_fpList.GetAt(pos);
			return pFP->m_dValue;
		}

		return 0.0;
	}

	CString GetDescription(int nIndex)
	{
		POSITION pos = m_fpList.FindIndex(nIndex);
		if (pos != NULL)
		{
			CFilterParam* pFP = m_fpList.GetAt(pos);
			return pFP->m_strDesc;
		}
		
		return _T("");
	}

	inline void Copy(const CFilterParams* pFPs)
	{
		ClearParams();
		for (POSITION pos = pFPs->m_fpList.GetHeadPosition(); pos != NULL; )
		{
			CFilterParam* pFP = pFPs->m_fpList.GetNext(pos);
			m_fpList.AddTail(new CFilterParam(pFP));
		}
	}

	inline void Attach(CFilterParams* pFPs)
	{
		ClearParams();
		m_fpList.AddTail(&pFPs->m_fpList);
		pFPs->m_fpList.RemoveAll();
	}

	BOOL SetName(int nIndex, LPCTSTR pszName)
	{
		POSITION pos = m_fpList.FindIndex(nIndex);
		if (pos != NULL)
		{
			CFilterParam* pFP = m_fpList.GetAt(pos);
			pFP->m_strName = pszName;
			return TRUE;
		}
		
		return FALSE;
	}
	
	BOOL SetValue(int nIndex, double dValue)
	{
		POSITION pos = m_fpList.FindIndex(nIndex);
		if (pos != NULL)
		{
			CFilterParam* pFP = m_fpList.GetAt(pos);
			pFP->m_dValue = dValue;
			return TRUE;
		}
		
		return FALSE;
	}
	
	BOOL SetDescription(int nIndex, LPCTSTR pszDesc)
	{
		POSITION pos = m_fpList.FindIndex(nIndex);
		if (pos != NULL)
		{
			CFilterParam* pFP = m_fpList.GetAt(pos);
			pFP->m_strDesc = pszDesc;
			return TRUE;
		}
		
		return FALSE;
	}

	virtual void Serialize(CXMLSettings& xml);

public:
	CString		m_strFilter;
	CFPList		m_fpList;
};

#endif // !defined(AFX_FILTERPARAM_H__DD284226_6FDD_4DB9_9C45_17790606F734__INCLUDED_)

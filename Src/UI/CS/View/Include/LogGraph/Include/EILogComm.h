#ifndef  _EILOG_COMM_DISPLAY_H
#define _EILOG_COMM_DISPLAY_H
#if _MSC_VER > 1000
#pragma once
#endif 

#pragma pack( push, 1)

//当前工作驱动模式
#define DRIVEDEPTH 0
#define DRIVETIME 1

//记录时的工作模式
//上提有比例
#define WORKSCALEUP 1
//下放有比例
#define WORKSCALEDOWN 2
//上提无比例
#define WORKNOSCALEUP 4
//下放无比例
#define WORKNOSCALEDOWN 8

//工作或监视
#define STATEWATCH false
#define STATERECORD true

//打印否
#define PLOTNO false
#define PLOTYES true

//数据类型，数据格式还是数据等
#define EILOGFORMAT 0
#define EILOGDATA 1

typedef struct TAG_EILogComm
{
	char flag[8]; //标志，"EILogDI"
	unsigned long count;
	unsigned long drivemode; //当前工作驱动模式
	unsigned long workmode; //记录时的工作模式
	bool workstate; //工作或监视
	bool plotstate; //打印否
	unsigned long sysstate; //系统其它信息
	unsigned long type; //数据类型，数据格式还是数据等
	unsigned long flowlength; //后跟数据长度
} EILogComm;

#pragma pack(pop )

#endif
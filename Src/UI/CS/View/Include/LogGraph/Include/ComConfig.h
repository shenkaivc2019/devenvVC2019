// ComConfig.h: interface for the CComConfig class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COMCONFIG_H__C319D928_48AF_40AB_AB07_384902E68F13__INCLUDED_)
#define AFX_COMCONFIG_H__C319D928_48AF_40AB_AB07_384902E68F13__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ULInterface.h"
#include "IComConfig.h"

#define  COM_TOOL   1
#define  COM_BOARD  2
#define  COM_FILEFT 3
#define  COM_FILTER 4
#define  COM_TRACK  5
#define  LINE_LEVEL 4

#define  LAYER_FILL		  0
#define  LAYER_HORZ_LINE  1
#define  LAYER_VERT_LINE  2
#define  LAYER_CURVE      3
#define  LAYER_MARK	      4
#define	 LAYER_LABLE	  5

#ifndef CONFIG_API
    #ifdef CONFIG_EXPORT
		#define CONFIG_API __declspec( dllexport )
	#else	
		#define CONFIG_API __declspec( dllimport )
	#endif
#endif

#define     DATASAVEPATH_DEMO   0     //存盘路径定义设置，默认的系统路径，为测井数据与仪器刻度数据的存盘处理
#define     DATASAVEPATH_USER   1     //用户自定义存盘路径，Gzh,2002.11.4

typedef struct _tagWatchedBoardState
{
	short boardIndex;
	BYTE  boardName[MAX_PATH]; 
} WATCHED_BOARD_STATE;

typedef struct tagULFONT
{
    LONG	  cy;
    LONG      lfWeight;
    BYTE      lfItalic;
    BYTE      lfUnderline;
	BYTE      lfStrikeOut;
    BYTE      lfCharSet;
    TCHAR     lfFaceName[LF_FACESIZE];
} ULFONT, *PULFONT;

//[Watch]
typedef struct tagWATCH
{
	int  Select;//用户选择
	int	 Command;//用户命令*/
	int  DataEdit;//数据修改
	int  Warning;//测井警告
	int  Error;	//侧井错误
	int  Scope;//是否显示示波器
	int  RawData;//是否显示原始数据
	int  BoardIndex;//当前选中板卡
	int  Radio;//示波器的刷新率
	int	 Flash;//状态窗口刷新频率
	int  AddWidth;
	int  Color;
	int  UserSelWay;
	int  test;
	WATCHED_BOARD_STATE  WatchedBaordStat;
	BOOL Param;
	BOOL Saving;
	BOOL Printing;	
	int  FCount;
	ULFONT FFont[ftCount];
	int  Refresh;//绘图刷新频率
	BOOL ShowDepth;
	WORD nLayerCount;
	WORD Layer[10];
/*	结构体对齐导致大小变化修改为使用WORD
	BOOL bDataSequence;	// 数据视图是否按照实际仪器顺序显示
	BYTE reserve[710];
*/
	WORD bDataSequence;	// 数据视图是否按照实际仪器顺序显示
	BYTE reserve[712];
}SYS_WATCH;
//[Net]
typedef struct tagNet
{
	DWORD  IPAddress;
	WORD   PortNumber;
	DWORD  ReConnectTime;
	DWORD  MaxConnectNum;
	int    ConnectMode;
	BYTE reserve[1024];
}SYS_NET;
//[Plot]
typedef struct tagPLOT
{
	int  LogData;//测井数据
	int  Header;//图头
	int  Shape;//仪器外形图
	int  LogHeader;//测井头
	int  CaliGraph;//刻度图
	int  CaliGene;//刻度因子
	int  VeriGraph;//验证图
	int  HumanInfo;//人员汇总表
	int  ToolInfo;//仪器汇总表
	int  WellInfo;//井场汇总表
	BOOL PrintGap;
	int  PrintGapSize;
	int	 PrintGapInterVal;
	BOOL CurveFlag;
	int  CurveFlagInterval;
	int	 DepthAngle;
	BOOL Colored;
	BOOL CurveFirst;
	BOOL ModifyParam;
	TCHAR PlotVersion[128]; //测井信息小图头的软件版本
	TCHAR PlotProcessing[128]; //测井信息小图头的数据处理类型
	BOOL bMemDC;
	BOOL bPrintMark1000;
	BOOL bPrintMark500;
	BOOL bCurveLableWithColor;
	BOOL bGapUseTimeCurve;
	BYTE reserve[728];
}SYS_PLOT;

//[System]
typedef struct tagSYSTEM
{
	int  DriveMode;//驱动模式
	BOOL  DriveShowTime;
	BOOL  DriveShowDepth;
	int  Language;//语言
	int  Metrics;//单位
	int  ToolMode;//仪器组合显示方式
	COLORREF DrawThumb;
	BOOL RelogMode;
	BOOL RelogFilter;
	float AutoShootSpeedLimit;
	float FireError;
	int   CurveRollCount;
	BOOL  ShowMPs;
	BOOL  RelogDepth;	// 是否使用原始文件中保存的深度
	BYTE  LineWidth[4];
	BYTE  LinePWidth[4];
	char  UnitCat[32];		// 单位配置
	BOOL  PictureScaled;
	BOOL  RelogDepthRangeMode;
	BOOL  Completed;
	BOOL  RelogCalData;		// 使用原始文件中的刻度数据
	UINT  nPicDelayTime;	// 系统启动画面停留时间
	UINT  nReplayDrawCurve;	// 回放时是否显示曲线
	BYTE  reserve[952];
}SYS_SYSTEM;

//[Record]
typedef struct tagRECORD
{
	int  Period;			// 磁盘缓冲刷新间隔
	int  Buffer;			// 文件数据缓冲大小
	int  RawData;			// 原始采样值
	int  Engineer;			// 工程值
	int  Format;			// 数据格式
	BOOL bInterpolation;	// 是否进行插补，new
	long lInterVal;         // 插补间隔，new
	BOOL bNumName;			// 数字命名
	TCHAR chWorkPath[MAX_PATH];// PDF打印工作目录
	BOOL bDeleteTemp;		// 是否删除临时文件
	BOOL bAutoFit;
	BOOL bSaveTail;			// 是否保存实测后丢失的尾部数据
	BYTE reserve[752];
}SYS_RECORD;

//打印设置
typedef struct tagPRINTINFO {
	int		PrintGrade;				// 打印等级
	float	PrintVLen;				// 垂直长度
	float	PrintHLen;				// 水平长度
	
	int     nPrintGrade;            // 打印的灰度
	int     nPrintHLength;			// 水平打印长度
	int		nPrintVLength;			// 垂直打印长度
	double  fPrintHCalLength;       // Horizonal length calibrated 
	double  fPrintVCalLength;		// Vertical length calibrated
	double  fHRate;					// Horizonal rate, calibrated
	double  fVRate;					// Vertical rate, calibrated

	BOOL	bPrintLogInfoHead;		// 打印头部测井信息
	BOOL	bPrintLogInfoTail;		// 打印尾部测井信息
	BOOL	bPrintTrackHeadTwice;	// 双边打印道头
	BOOL	bPrintDiff;				// 打印刻度测量差
	BYTE    reserve[1016];
} SYS_PRINTINFO;

//[Other]
typedef struct tagCOLUMNSTATE
{
	UINT nIndex;
	char strName[32];
	UINT bShow;
}COLUMN_STATE;

typedef struct tagCHARTVIEWCOLUMNS
{
	COLUMN_STATE columnState[15];
}CHART_COLUMNS;

typedef struct tagOTHER
{
	int		DepthType;//深度类型
	int		PrintMode;//打印模式
	char	EditPassword[16];//允许编辑的密码
	int		DepthOut;//深度独立显示
	int		LogModifyEnable;//测井过程中允许修改曲线和数据
	int		PageCount;//打印页数
	int		Time;//间隔时间
	BOOL	bCanModifyDraw;
	BOOL	bCanSaveScopeInfo;
	BOOL	bModifyWellInfoSame;
	BOOL	bCanModifyEngFile;
	BOOL    bCalcDepthOffset;
	int 	AlignTopOffset;
	BOOL    bDrawDepthOffset;
	BOOL	bOffsetValue;
	BOOL	bOffsetValueSet;
	float	fOffsetValue;
	BYTE    ToolProperties[20];//仪器组合视图中仪器属性是否check
	BYTE    ToolsProperties[12];//仪器组合视图中仪器串属性是否check
	BOOL	bAllowEdit;
	double	lOffsetDelta;
	BOOL	bBitType;
	int		nCalSummaryType;
	BOOL	bCanModifyHeader; // 允许图头图尾的编辑
	short   nAXPVersion;
	BYTE    reserve[950];
}SYS_OTHER;

typedef struct tagCALIBRATE
{
	int Before;//测前刻度
	int After;//测后刻度
	int Master;//主刻度
	int DataTable;//刻度数据表
	int CoefTable;//刻度系数表
	int CurveGraph;//刻度曲线图
	int MeasTable;//刻度点数据明细
	int VerBefore;//测前刻度校验
	int VerAfter;//测后刻度校验
	int VerMaster;//主刻度校验
	int BKGrid;//背景网格
	int LinkDot;//刻度点连线
	BYTE reserve[1024];
}SYS_CALIBRATE;

typedef struct tagDRAWMARK
{
	int nMode;
	int nWidth;
	LONG crColor;
	int nMarkType;
	BYTE reserve[1024];
}SYS_DRAWMARK;


////////////////////

typedef struct _tagBoardInfo
{
	TCHAR  szName[CHAR_SIZE];			//接口板名
	TCHAR  szManufactory[CHAR_SIZE];		//生产板卡的厂家名
	TCHAR  szSeries[CHAR_SIZE];			//板卡所属系列
	TCHAR  szDLLName[CHAR_SIZE];			//板卡动态库名称
	BOOL  bActive;
	BOOL  bRestore;
	BYTE  reserved[504];
}BOARD_INFO;

typedef struct _tagFileTypeInfo
{
	TCHAR  ChineseName[CHAR_SIZE];
	TCHAR  EnglishName[CHAR_SIZE];
	TCHAR  ManuFactory[CHAR_SIZE];
	TCHAR  Type[CHAR_SIZE];
	TCHAR  DLLName[CHAR_SIZE];
	TCHAR  Date[CHAR_SIZE]; 
	BYTE  reserved[RESEVERED_SIZE];
}FILEFORMAT_INFO;

typedef struct _tagTrackTypeInfo
{
	TCHAR  ChineseName[CHAR_SIZE];
	TCHAR  EnglishName[CHAR_SIZE];
	TCHAR  ManuFactory[CHAR_SIZE];
	TCHAR  Type[CHAR_SIZE];
	TCHAR  DLLName[CHAR_SIZE];
	TCHAR  Date[CHAR_SIZE]; 
	BYTE  reserved[RESEVERED_SIZE];
}TRACK_INFO;

typedef struct _tagChannelInfo
{
	TCHAR szName[CHAR_SIZE];			// 通道名称
	UINT nCount;					// 通道个数
	BYTE reserved[RESEVERED_SIZE];
}CHANNEL_INFO;

typedef struct _tagFileState
{
	BYTE  aryState[4096];
}FILESTATE;

typedef struct _tagDataSavePath		        
{
   double fVersion;                         //The version of the system
   UINT   nSaveStyle;                       //The Data Save style, Demo or user define,or others style  
   TCHAR szLogDataSavePath[MAX_PATH];        //The path of the log data saved
   TCHAR szToolCalSavePath[MAX_PATH];        //The path of the tool cal coefficient saved 
   TCHAR szReserve1[MAX_PATH];               //Reserved path name   
   TCHAR szReserve2[MAX_PATH];
   BOOL	 bUserDefinename;	// 是否自定义数据文件名
   BYTE reserve[1020];
}DATASAVEPATH;

typedef struct _tagLoggingOperation			//测井时，同时进行的操作处理
{											
	BOOL bPrinting;							//At the same time,printting the data curves in real time
	BOOL bSaving;							//As Save logging data  as logging.
}OPERATIONLOG;

typedef struct tagFILTERINFO
{
	TCHAR  FilterName[CHAR_SIZE];                   //滤波器的类型名称
	TCHAR  ManuFactory[CHAR_SIZE];                  //公司名称
	TCHAR  dllName[CHAR_SIZE];
	TBYTE  reserved[RESEVERED_SIZE];                //滤波器的dll相对路径路径
}FILTER_INFO;

typedef struct _tagProjectWizardSetting
{
	TCHAR strDefaultWellName[CHAR_SIZE];	//默认井名
	double fStartDepth;				//开始深度
	double fEndDepth;				//终止深度
	int nDrillerProcess;			//钻头程序
	int nCasedholeProcess;			//套管程序
	UINT nHistoryMaxCount;			//最大历史记录数
	BOOL bHistoryAutoFill;			//自动填充历史记录
	BYTE reserve[1024];
} PROJECTWIZARDSETTING, SYS_PWS;

typedef struct _tagMixToolInfo
{
	TOOL_INFO  toolInfo;
	TCHAR       szPath[MAX_PATH];
	BYTE       reserved[1024];
} MIX_TOOL_INFO;

typedef struct _tagMixBoardInfo
{
	BOARD_INFO  boardInfo;
    TCHAR       szPath[MAX_PATH];
	BYTE       reserved[1024];
} MIX_BOARD_INFO;

typedef struct _tagMixFileTypeInfo
{
	TCHAR             szPath[MAX_PATH];
	FILEFORMAT_INFO  fileFormatInfo;
	BYTE       reserved[1024];
}MIX_FILEFORMAT_INFO;

typedef struct _tagMixTrackTypeInfo
{
	TCHAR        szPath[MAX_PATH];
	TRACK_INFO  trackInfo;
	BYTE        reserved[1024]; 
}MIX_TRACK_INFO;

typedef struct _tagMixFilterInfo
{
	FILTER_INFO  filterInfo;
	TCHAR         szPath[MAX_PATH];
	BYTE       reserved[1024];
} MIX_FILTER_INFO;

typedef struct _tagSystemInfo
{
    SYS_WATCH		Watch;				// [Watch]
	SYS_PLOT		Plot;				// [Plot]
	SYS_SYSTEM		System;			    // [System]
	SYS_RECORD		Record;			    // [Record]
	SYS_PRINTINFO	PrintInfo;		    // [PRINTINFO]
	SYS_OTHER		Other;				// [Other]
	SYS_CALIBRATE	Cal;				// [Cal]
	SYS_DRAWMARK	DrawMark;			// [DRAWMARK]
	DATASAVEPATH	DataSavePath;	    // 数据保存路径设置
	SYS_NET			Net;				// [Net]
    SYS_PWS			ProjectWizardSetting;
	FILESTATE		fileState;
    CHART_COLUMNS    chartColumnState;  //三图一表中数据表列的状态600字节
	BYTE			reserve[1448];
} SYSTEMINFO;

typedef  CArray <BOARD_INFO, BOARD_INFO&> BOARDARRAY;
typedef  CArray <FILTER_INFO, FILTER_INFO&> FILTERARRAY;
typedef  CArray <TRACK_INFO,  TRACK_INFO&>	TRACKARRAY;
typedef  CArray <FILEFORMAT_INFO, FILEFORMAT_INFO&> FILEFORMATARRAY;
typedef  CArray <CHANNEL_INFO, CHANNEL_INFO&>CHANNELARRAY;

typedef  CArray <MIX_TOOL_INFO, MIX_TOOL_INFO&> MIXTOOLARRAY;
typedef  CArray <MIX_BOARD_INFO, MIX_BOARD_INFO&> MIXBOARDARRAY;
typedef  CArray <MIX_TRACK_INFO, MIX_TRACK_INFO&>  MIXTRACKARRAY; 
typedef  CArray <MIX_FILTER_INFO , MIX_FILTER_INFO&> MIXFILTERARRAY;
typedef  CArray <MIX_FILEFORMAT_INFO, MIX_FILEFORMAT_INFO&> MIXFILEFORMATARRAY;

class  CONFIG_API CComConfig : public IComConfig
{
	DECLARE_ULI(IID_IComConfig, IComConfig)

public:
	SYS_WATCH     m_Watch;			// [Watch]
	SYS_PLOT      m_Plot;			// [Plot]
	SYS_SYSTEM    m_System;			// [System]
	SYS_RECORD    m_Record;			// [Record]
	SYS_PRINTINFO m_PrintInfo;		// [PRINTINFO]
	SYS_OTHER     m_Other;			// [Other]
	SYS_CALIBRATE m_Cal;			// [Cal]
	SYS_DRAWMARK  m_DrawMark;       // [DrawMark]
	SYS_NET       m_Net;            // [Net]
	CStringArray  m_fileState;
	DATASAVEPATH         m_DataSavePath; //数据保存路径设置
    PROJECTWIZARDSETTING m_ProjectWizardSetting;

	SYSTEMINFO  m_systemInfo;
	
    CString  m_strAppPath;
	CString  m_strInfoName;

	FILTERARRAY     m_filterArray;
	TOOLARRAY	    m_toolArray;
	BOARDARRAY	    m_boardArray;
	TRACKARRAY		m_trackArray;
	CPtrArray	    m_channelArray;
    FILEFORMATARRAY m_fileFormatArray;

	MIXFILTERARRAY      m_mixFilterArray;
	MIXTOOLARRAY	    m_mixToolArray;
	MIXBOARDARRAY	    m_mixBoardArray;
	MIXTRACKARRAY       m_mixTrackArray;
    MIXFILEFORMATARRAY  m_mixFileFormatArray;

//	MIX_TOOL_INFO   m_mixToolInfo;
//	MIX_BOARD_INFO  m_mixBoardInfo;
//	MIX_FILTER_INFO m_mixFilterInfo;
	int m_nServiceWndSelIndex;
	// 回绕次数
	int m_nRollCount;				
	BOOL m_bUpdate;

	CFont m_font[ftCount];
public:	
	CComConfig(LPCSTR lpzWordPath = NULL, BOOL bDataManager = FALSE);
	virtual ~CComConfig();
	void  Init();                                      //初始系统配置文件 
	void  GetAppPath();                               //获得应用程序路
		
	BOOL CheckDll(CString& strPath, CString& strDllFile);
	BOOL InstallTool(CString strFilePath);            //安装仪器
	BOOL InstallBoard(CString strFilePath);           //安装板卡
	BOOL InstallFilter(CString strFilePath);          //安装滤波器
	BOOL InstallTrack(CString strFilePath);			  //安装记录道
	BOOL InstallFileFormat(CString strFilePath);      //安装文件格式

	void  DefaultSystemSet();                //初始化系统配置 
private:                                     //获得板卡通道信息
	int  GetBoardCHInfo(CString strPath, CStringArray &chNameArray);

// -----------------------------------------------------------------------
public:
	ULMDWD SetLWLevel(int nLWLevel);
// 2023.12.26 Start
	ULMDWD TurnLineWidth(int iWidth, BOOL bValue = TRUE);
// 2023.12.26 End
	ULMPTR GetToolArray();

	ULMLNG GetProp(LPCTSTR pszPage, LPCTSTR pszProp);
	ULMLNG SetProp(LPCTSTR pszPage, LPCTSTR pszProp , BOOL bValue);

	//-----------------------Tool Interface-------------------------------
	int   EnumToolKeys(CStringArray &toolArray);              //枚举所有安装的仪器
	BOOL  ReadToolInfo(CString strPath, CString strBuffer[]); //读取仪器信息
	int   GetAllToolInfo();           //获得所有仪器信息
	void  DeleteTool(int nIndex);                             //删除仪器
	int   GetToolInstalledCount();                            //获得已安装仪器数量  
	CString	GetToolNO(CString strToolName);                   //获得仪器序列号
	CString GetToolDllPath(TOOL_INFO  toolInfo);              //获得仪器DLL路径
	TOOL_INFO*	GetToolInfo(CString strToolName);             //获得仪器信息
	TOOL_INFO*  GetToolInfo(int nIndex );                     //获得仪器信息
    TOOL_INFO   LoadToolInfo(CString strToolPath,BOOL bFullName = FALSE);//获得仪器信息 
    // -----------------------Board Interface-----------------------------
	int  EnumBoardKeys(CStringArray& boardArray);             //枚举所有已安装板卡
	int  EnumBoardKeys(CStringList& boardList, BOOL bNoCase = TRUE);
	int  GetAllBoardInfo();         //获得所有板卡信息  
    BOOL ReadBoardInfo(CString strPath, CString strBuffer[]); //读取板卡信息
	int  GetBoardNum(CString strBoardName);                   //获得板卡的索引号
	void DeleteBoard(int nIndex);                             //删除板卡
	int  GetBoardInstalledCount();                            //获得板卡安装数量
	CString GetBoardName(int Num);                            //获得板卡名称
	CString GetBoardDllPath(BOARD_INFO boardInfo);            //获得板卡DLL路径
	BOARD_INFO*	 GetBoardInfo(CString strBoardName);          //获得板卡信息
	BOARD_INFO   LoadBoardInfo(CString strBoardPath,BOOL bFullName = FALSE);//获得板卡信息
	int	GetBoardCH(CString strBoardName , CStringArray &chNameArray);
	//--------------------------File Format-------------------------------
	int  EnumFileFormatKeys(CStringArray &fileFormatAry);
	int  GetAllFileFormatInfo();
	BOOL ReadFileFormatInfo(CString strPath, CString strBuffer[]);
    int  GetFileFormatNum(CString strFileFormat);
	void DeleteFileFormat(int nIndex);
	int  GetFileFormatInstalledCount();
	CString GetFileFormatDllPath(FILEFORMAT_INFO  fileFormatInfo); 
	FILEFORMAT_INFO LoadFileFormatInfo(CString strFilePath, BOOL bFullName = FALSE);
	int GetAllFileFormatName(CStringArray& strFileAry);
	BOOL GetSelectedFileState(CString strFile);
	//-----------------------Track Interface------------------------------
	int  EnumTrackKeys(CStringArray &fileFormatAry);
	int  GetAllTrackInfo();
	BOOL ReadTrackInfo(CString strPath, CString strBuffer[]);
	int  GetTrackNum(CString strTrack);
	void DeleteTrack(int nIndex);
	int  GetTrackInstalledCount();
	CString GetTrackDllPath(TRACK_INFO  trackInfo);
	TRACK_INFO LoadTrackInfo(CString strTrackPath, BOOL bFullName = FALSE);
	int GetAllTrackName(CStringArray& strTrackAry);
	// ----------------------Filter Interface-----------------------------
	int  EnumFilterKeys(CStringArray &filterArray);           //枚举所有安装滤波器
	int  GetAllFilterInfo();      //获得所有安装滤波器信息
	int  GetFilterInstalledCount();                           //获得已安装滤波器数量
	BOOL ReadFilterInfo(CString strPath, CString strBuffer[]);//获得滤波器信息
	void DeleteFilter(int nIndex);                            //删除滤波器
	FILTER_INFO* GetFilterInfo(CString strFilterName);                          //获得滤波器信息
	FILTER_INFO  LoadFilterInfo(CString strFilterPath,BOOL bFullName = FALSE);  //获得滤波器信息
   //---------------------------other-------------------------------------
	void DeleteComponent(LPCTSTR pszPath);
	void RecursiveDelete(CString szPath);                     //删除已安装设备文件
	BOOL CheckExist(UINT ComID, CString strFile);             //检查设备是否安装
	BOOL WriteSystemSet();                                   //存储系统配置文件
	BOOL ReadSystemSet();                                    //读取系统配置文件
	BOOL GetSystemSet();				//获得预置系统配置文件
	int CreateDir(LPCTSTR lpszPath);
	void SaveBoards(BOOL bRestore = FALSE);
	void SetBoards(CStringList* pBoards);
	void CopyDirectory(CString strTargetPath, CString strSourcePath);
	CString FindInfoPath(CString strDllPath);
	BOOL FindSubKey(CRegKey& key, LPCTSTR lpszKey, LPCTSTR lpszSubKey);

private:	
	void ReadStorage(LPSTORAGE pStg, UINT nComType);
	void RewriteAllComponent();
};

//extern CComConfig Gbl_ConfigInfo;
CONFIG_API CComConfig* AfxGetComConfig();
//extern CFont g_font[ftCount];

#endif // !defined(AFX_COMCONFIG_H__C319D928_48AF_40AB_AB07_384902E68F13__INCLUDED_)

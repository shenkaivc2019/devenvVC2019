#if !defined(AFX_GFXPAGEFILL_H__EDE24F80_F559_4DBE_BE2C_FDF0BF0C5331__INCLUDED_)
#define AFX_GFXPAGEFILL_H__EDE24F80_F559_4DBE_BE2C_FDF0BF0C5331__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GfxPage.h"
#include "PatternPicker.h"

// GfxPageFill.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGfxPageFill dialog

class CTrack;
class CCurve;
class CGfxPageFill : public CGfxPage
{
	// Construction
public:
	CGfxPageFill(CWnd* pParent = NULL);   // standard constructor	
	// Dialog Data
	//{{AFX_DATA(CGfxPageFill)
	enum { IDD = IDD_GFX_PAGE_FILL };
	CPatternPicker m_btnPattern;
	CComboBox	m_cmbLeft;
	CComboBox	m_cmbRight;
	//}}AFX_DATA
	
	CMFCColorButton	m_btnColor;
	CMFCColorButton    m_btnBKColor;

	float	m_fLeftValue;
	float	m_fRightValue;
	int		m_nLeftFillType;        // 左边界模式
	int		m_nRightFillType;       // 右边界模式
	int		m_nCurSelect;
	int		m_nFillNumber;
	int		m_nFillType;			// 填充类型

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGfxPageFill)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	
public:	
	CString	m_strName;
	void FlushData();
	void InitCurve();
	LONG OnColorSelOk(UINT lParam, LONG wParam);
	// Implementation
protected:
	
	// Generated message map functions
	//{{AFX_MSG(CGfxPageFill)
	afx_msg void OnRadioLeftValue();
	afx_msg void OnRadioLeftCurve();
	afx_msg void OnRadioRightValue();
	afx_msg void OnRadioRightCurve();
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeCurveName();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
		
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPPAGEFILLDLG_H__EDE24F80_F559_4DBE_BE2C_FDF0BF0C5331__INCLUDED_)

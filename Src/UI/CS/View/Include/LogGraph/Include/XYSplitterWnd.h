#if !defined(AFX_XYSPLITTERWND_H__8E3E5264_02A9_11D2_BF99_000021000B7C__INCLUDED_)
#define AFX_XYSPLITTERWND_H__8E3E5264_02A9_11D2_BF99_000021000B7C__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// xySplitterWnd.h : header file
//

#define WM_SPLITTER_MOVED			(WM_USER+1000)
#define WM_SPLITTER_DBLCLICKED		(WM_USER+1001)

/////////////////////////////////////////////////////////////////////////////
// CxSplitterWnd window

class AFX_EXT_CLASS CxSplitterWnd : public CStatic
{
// Construction
public:
	CxSplitterWnd();

// Attributes
public:

// Operations
public:
    void SetMinWidth(int left, int right);

    BOOL AttachAsLeftPane(DWORD ctrlId);
    BOOL AttachAsRightPane(DWORD ctrlId);
    BOOL DetachAllPanes(void);
    void RecalcLayout(BOOL bRepaint = TRUE, BOOL bErase = TRUE);
	void SetParentWnd(CWnd* pWnd)
	{
		m_Parent = pWnd;
		m_Parent->ModifyStyleEx(WS_EX_CLIENTEDGE, 0, SWP_DRAWFRAME);
	}

protected:
    BOOL GetMouseClipRect(LPRECT rectClip, CPoint point);

    CWnd        *m_Parent;
    CDWordArray m_leftIds, m_rightIds;
    int         m_minLeft, m_minRight;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CxSplitterWnd)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CxSplitterWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CxSplitterWnd)
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// CySplitterWnd window

class AFX_EXT_CLASS CySplitterWnd : public CStatic
{
// Construction
public:
	CySplitterWnd();

// Attributes
public:

// Operations
public:
    void SetMinHeight(int above, int below);

    BOOL AttachAsAbovePane(DWORD ctrlId);
    BOOL AttachAsBelowPane(DWORD ctrlId);
    BOOL DetachAllPanes(void);
    void RecalcLayout(BOOL bRepaint = TRUE, BOOL bErase = TRUE);
	void SetParentWnd(CWnd* pWnd)
	{
		m_Parent = pWnd;
		m_Parent->ModifyStyleEx(WS_EX_CLIENTEDGE, 0, SWP_DRAWFRAME);
	}

protected:
    BOOL GetMouseClipRect(LPRECT rectClip, CPoint point);

    CWnd        *m_Parent;
    CDWordArray m_aboveIds, m_belowIds;
    int         m_minAbove, m_minBelow;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CySplitterWnd)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CySplitterWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CySplitterWnd)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnPaint();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XYSPLITTERWND_H__8E3E5264_02A9_11D2_BF99_000021000B7C__INCLUDED_)

// Channel.h: interface for the CChannel class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHANNEL_H__C75143E6_8B76_4D2B_A7BD_608A6D27829A__INCLUDED_)
#define AFX_CHANNEL_H__C75143E6_8B76_4D2B_A7BD_608A6D27829A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxmt.h>
#include "XMLSettings.h"
#include "ULInterface.h"
#include "ULCHANNELDEF.h"

typedef struct tagCHANNEL 
{
	CString	strName;
	CString strType;
	double	dblRate;
	long	lPrecision;
	BOOL    bManual;
	CString	strBoardName;
	CString	strChannelName;
	DWORD	dwID;
	DWORD   dwBufferSize;
    UINT    nOffset;
	tagCHANNEL()
	{
		strName.Empty();	
		strType.Empty();
		dblRate = 0.0;
		lPrecision = 0;	
		bManual = FALSE;
		strBoardName.Empty();
		strChannelName.Empty();	
		dwID = 0;
		dwBufferSize = 10240;
		nOffset=0;
	}
	
	void Serialize(CXMLSettings& xml)
	{
		if (xml.IsStoring())
		{
			xml.Write("Version", (DWORD)MAKEWORD(_UL2000_VERSION_MINOR, _UL2000_VERSION_MAJOR));
			xml.Write("Name", strName);
			xml.Write("Type", strType);
			xml.Write("DataRate", dblRate);
			xml.Write("Precision", lPrecision);
			xml.Write("Manual", bManual);
			xml.Write("BoardName", strBoardName);
			xml.Write("ChannelName", strChannelName);
			xml.Write("ID", dwID);	
			xml.Write("BufferSize", dwBufferSize);
			xml.Write("Offset",nOffset);
		}
		else
		{
			DWORD dwVersion;
			xml.Read("Version", dwVersion);
			xml.Read("Name", strName);
			xml.Read("Type", strType);
			xml.Read("DataRate", dblRate);
			xml.Read("Precision", lPrecision);
			xml.Read("Manual", bManual);
			xml.Read("BoardName", strBoardName);
			xml.Read("ChannelName", strChannelName);
			xml.Read("ID", dwID);	
			xml.Read("BufferSize", dwBufferSize);
			xml.Read("Offset",nOffset);
		}
	}
	
	void Serialize(CArchive& ar)
	{	
		if (ar.IsLoading())
		{
			float fVersion;
			ar >> fVersion;
			ar >> strName;
			ar >> strType;
			ar >> dblRate;
			ar >> lPrecision;
			ar >> bManual;
			ar >> strBoardName;
			ar >> strChannelName;		
			ar >> dwID;	
			ar >> dwBufferSize;
			ar >> nOffset;
			BYTE bReserved[1020];
			ar.Read(bReserved, 1020);
		}
		else
		{
			float fVersion = 2.001f;
			ar << fVersion;
			ar << strName;
			ar << strType;
			ar << dblRate;
			ar << lPrecision;
			ar << bManual;
			ar << strBoardName;
			ar << strChannelName;		
			ar << dwID;	
			ar << dwBufferSize;
			ar << nOffset; 
			BYTE bReserved[1020];
			// ZeroMemory(bReserved, 1024);
			ar.Write(bReserved, 1020);
		}
	}
	
} CHANNEL, *PCHANNEL, NEAR *NPCHANNEL, FAR *LPCHANNEL;

class CDataItem;

class CChannel : public tagCHANNEL
{

public:
	CChannel();
	CChannel(CHANNEL initChn);
	~CChannel();
	
// Operations
public:	
	virtual short	OpenChannel();		// Initialize the Channel
	virtual short	CloseChannel();		// Close the Channel
	
	CString			GetEventName();		// 读写互斥事件名称
	CString			GetMapFileName();	// 映射文件名称

	void			SetBufferSize(DWORD dwSize);
	DWORD			GetBufferSize();	
	void			ZeroBuffer();
	LPVOID			GetBuffer();

	int				GetData(LPVOID pData, int nNum);
	int				SetData(LPVOID pData, int nNum);

	BOOL            Unlock();
	void            Start();
	void            Stop();

// Attributes
public:
	CString			m_strEventName;		// 读写互斥事件名称
	CString			m_strMapFileName;	// 映射文件名称
	
	CDataItem*		m_pDataItem;		// 数据项指针
	
	// Data Exchange Attribute
	CEvent*			m_pEvent;			// 读写互斥事件
	HANDLE			m_hExchangeFile;	// 交换文件句柄
	LPVOID			m_lpDataBuffer;		// 缓冲数据指针
	
	ChannelHeader*  m_pDataHeader;
};

#endif // !defined(AFX_CHANNEL_H__C75143E6_8B76_4D2B_A7BD_608A6D27829A__INCLUDED_)

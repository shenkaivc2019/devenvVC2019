//*******************************************************************************
// COPYRIGHT NOTES
// ---------------
// This is a part of the UL2000 (Professional Version)
// Copyright (C) 1998-2001 ArenaSoft Ltd.
// All rights reserved.
//
// This source code can be used, distributed or modified
// only under terms and conditions 
// of the accompanying license agreement.
//*******************************************************************************
//#define _AXP

#ifndef AFX_DSF__H___
#define AFX_DSF__H___
/*
#ifdef _AXP
	#define strAppTitle	_T("AXP")
	#define strCorp		_T("ArenaPetro")
	#define strWeb		_T("http://www.arenapetro.com")
	#define strSoft		_T("Arena Soft On-Line")
	#define strEmail	_T("mailto:help@arenapetro.com")
#endif*/

#ifdef _AXP
//	#define strAppTitle	_T("SK-LWD")
	#define strAppTitle	_T("LogGraph")
	#define strCorp		_T("ArenaPetro")
//	#define strWeb		_T("http://www.arenapetro.com")
	#define strWeb		_T("http://www.shenkai.com")
	#define strSoft		_T("Arena Soft On-Line")
//	#define strEmail	_T("mailto:help@arenapetro.com")
	#define strEmail	_T("mailto:11082012@qq.com")
#endif


// 数据文件
const TCHAR LULFilePath[] = _T ("Last File Path");
const TCHAR ULFileFilter[] = _T ("");

const TCHAR LCBackPath[] = _T("Last Back Curve Path");		// 背景曲线
const TCHAR LCSplicePath[] = _T("Last Splice Curve Path");	// 文件拼接

// 工程文件
const TCHAR LProjectPath[] = _T ("Last Project Path");
const TCHAR ProjectFilter[] = _T ("Project File (*.ulp)|*.ulp|All Files (*.*)|*.*||");

// 脚本文件
const TCHAR LScriptPath[] = _T ("Last Script Path");
const TCHAR ScriptFilter[] = _T ("Script File (*.spt)|*.spt|All Files (*.*)|*.*||"); 

// 原始文件
const TCHAR LRawFilePath[] = _T ("Last Raw File Path");
const TCHAR RawFilter[] = _T ("Log Raw Data (*.raw)|*.raw||");

// 工程信息
const TCHAR LPrjInfoPath[] = _T ("Last PrjInfo Path");
const TCHAR PrjInfoFilter[] = _T ("XML File (*.xml)|*.xml|All Files (*.*)|*.*|");

// 刻度文件
const TCHAR ULCFilter[] = _T("Calibreate File (.ulc)|*.ulc||");

// 图表文件另存
const TCHAR LHeaderPath[] = _T("Last Header Path");
const TCHAR HeaderFilter[] = _T("Header File (*.ulh)|*.ulh|Cell File (*.cll)|*.cll|All Files (*.*)|*.*||");

const TCHAR LChartPath[] = _T("Last Chart Path");
const TCHAR ChartFilter[] = _T("");

// 图形文件
const TCHAR LImagePath[] = _T("Last Image Path");
const TCHAR ImageFilter[] = _T("Bmp File (*.bmp)|*.bmp|Jpeg File (*.jpg)|*.jpg|Gif File (*.gif)|*.gif|All Files (*.*)|*.*||");
const TCHAR ImagesFilter[] = _T("Images (*.bmp;*.jpg;*.gif;*.png)|*.bmp;*.jpg;*.gif;*.png|All Files (*.*)|*.*||");

// 示波器文件另存
const TCHAR ScopeFilter[] = _T("Scope Config (*.scg)|*.scg|All Files (*.*)|*.*||");

// 节箍数据文件
const TCHAR LHFilePath[] = _T("Last Hoop File Path");
const TCHAR HFileFilter[] = _T("节箍数据文件(*.lst)|*.lst|");

// 射孔取芯文件
const TCHAR LPCFilePath[] = _T("Last Perforation coring File Path");
const TCHAR PCFileFilter[] = _T("射孔取心文件(*.sqt)|*.sqt|");

// 绘图设置
#define DSF_ROOT				_T ("DrawSettings")
#define DSF_TRACK_IDFMT			_T ("Track%02d")
#define DSF_TRACK_NAME			_T ("TrackName")
#define DSF_TRACK_TYPE			_T ("TrackTypex")
#define DSF_TRACK_COLOR			_T ("TrackColor")
#define DSF_TRACK_LINESTYLE		_T ("TrackLineStyle")
#define DSF_TRACK_GRIDINFO		_T ("TrackGridInfo")
#define DSF_TRACK_HEADRC		_T ("TrackHeadRect")
#define DSF_TRACK_REGIONRC		_T ("TrackRegionRect")
#define DSF_TRACK_BORDER_LINEWIDTH		_T ("TrackBorderWidth")
#define DSF_TRACK_BORDER_COLOR			_T ("TrackBorderColor")
#define DSF_TRACK_BORDER_LINESTYLE		_T ("TrackBorderLineStyle")
#define DSF_GRID_SMALL			_T ("SmallGrid")
#define DSF_GRID_MIDDLE			_T ("MiddleGrid")
#define DSF_GRID_BIG			_T ("BigGrid")
#define DSF_GRID_STEP			_T ("Step")
#define DSF_GRID_LINEWIDTH		_T ("LineWidth")
#define DSF_GRID_COLOR			_T ("Color")
#define DSF_GRID_LINETYPE		_T ("LineType")
#define DSF_GRID_GROUP			_T ("Group")
#define DSF_GRID_LINE			_T ("Line")
#define DSF_GRID_VGGROUP		_T ("VGridGroup")
#define DSF_GRID_VGLINE			_T ("VGridLine")
#define DSF_GRID_VGTYPE			_T ("VGridType")
#define DSF_CURVE_COUNT			_T ("CurveCount")
#define DSF_CURVE_IDFMT			_T ("Curve%02d")
#define DSF_HGRID_SHOW          _T("TrackHasHorzGrid")
#define DSF_VGRID_SHOW          _T("TrackHasVertGrid")

#endif

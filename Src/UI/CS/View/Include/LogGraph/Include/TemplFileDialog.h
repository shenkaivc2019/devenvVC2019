#if !defined(AFX_NFILEDIALOG_H__75DBBF4E_0E76_4C55_AC45_CDB40BEC3650__INCLUDED_CTemplFileDialog)
#define AFX_NFILEDIALOG_H__75DBBF4E_0E76_4C55_AC45_CDB40BEC3650__INCLUDED_CTemplFileDialog

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "FileDlgHelper.h"
#include "ULInterface.h"
#include "ULFile.h"

// NFileDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTemplFileDialog dialog
#define IDC_BTN_SETTING 123456
class CTemplFileDialog : public CFileDialog
{
	DECLARE_DYNAMIC(CTemplFileDialog)

public:
	CTemplFileDialog(BOOL bOpenFileDialog, // TRUE for FileOpen, FALSE for FileSaveAs
		LPCTSTR lpszDefExt = NULL,
		LPCTSTR lpszFileName = NULL,
		DWORD dwFlags = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		LPCTSTR lpszFilter = NULL,
		CWnd* pParentWnd = NULL);

public:
	virtual int DoModal();

public:

	
protected:
	void ShowFileInfo();
	void CheckButtons(CString strExt);

	// Handle CDN_ notifications
	virtual void OnFileNameChange();
	virtual void OnFolderChange();
	virtual void OnTypeChange();
	
	//{{AFX_MSG(CTemplFileDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnCheckSaveAsTempl();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:
	CFileDlgHelper m_dlgHelper;
	CButton		m_btnSaveAsTempl;

public:
	CString		m_strExt;
	CWnd*       m_pParentWnd;
	BOOL		m_bSaveAsTempl;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NFILEDIALOG_H__75DBBF4E_0E76_4C55_AC45_CDB40BEC3650__INCLUDED_CTemplFileDialog)

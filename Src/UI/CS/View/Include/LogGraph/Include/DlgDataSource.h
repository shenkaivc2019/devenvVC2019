#if !defined(AFX_DlgDataSource_H__981C6524_7ABF_4B04_B40C_AC276A9976CF__INCLUDED_)
#define AFX_DlgDataSource_H__981C6524_7ABF_4B04_B40C_AC276A9976CF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ULInterface.h"
#include "ULDoc.h"
#include "DataEditDefine.h"
#include <map>
using namespace std;
class CCurve;
// DlgDataSource.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgDataSource dialog

class CDlgDataSource : public CDialog
{
// Construction
public:
	CDlgDataSource(vec_ic* pDatas, CCurve* pCurve, CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgDataSource();

// Dialog Data
	//{{AFX_DATA(CDlgDataSource)
	enum { IDD = IDD_DATA_SOURCE };
	CComboBox	m_cbDataSrc;
	short	m_nIndex1;
	short	m_nIndex2;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgDataSource)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgDataSource)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSelchangeCombo1();
	afx_msg void OnEnChangeEdtQuery();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	map<CString, CCurve*> m_mapCurves;
public:
	vec_ic* m_pDatas;
	CCurve* m_pCurve;

	CEdit m_editMatchCondition;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DlgDataSource_H__981C6524_7ABF_4B04_B40C_AC276A9976CF__INCLUDED_)

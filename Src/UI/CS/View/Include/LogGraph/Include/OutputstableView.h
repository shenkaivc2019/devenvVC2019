#if !defined(AFX_OUTPUTSTABLEVIEW_H__3A8F5DB1_E553_4715_8987_0D838F591717__INCLUDED_)
#define AFX_OUTPUTSTABLEVIEW_H__3A8F5DB1_E553_4715_8987_0D838F591717__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OutputstableView.h : header file
//

#include "Cell2000.h"
#include "ulview.h"
#include "ulInterface.h"

#define		CELL_NULL_HEIGHT		10

class CULTool;

/////////////////////////////////////////////////////////////////////////////
// COutputstableView view

class COutputstableView : public CULView
{
	DECLARE_DYNCREATE(COutputstableView)
public:
	COutputstableView(LPVOID pvecCurve  = NULL );           // protected constructor used by dynamic creation
	

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COutputstableView)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~COutputstableView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(COutputstableView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	virtual int CalcPageHeight();
	virtual int CalcPrintHeight();
	virtual void Print();
	void Clear();
	void InitCurveInfo();
	void OpenCellFile();
	void OpenCellFile(CCell2000* pCell);

	virtual void SaveAsBitmap(CDC* pDC, LPCTSTR pszFile);
protected:
	void InitCellData(CCell2000* pCell = NULL);
private:
	CString GetToolDescription(CString strName, CString strPath);

public:
	CCell2000	m_Cell;
    CPtrArray	m_CurveInfoList;
	vec_ic*	    m_pvecCurve;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OUTPUTSTABLEVIEW_H__3A8F5DB1_E553_4715_8987_0D838F591717__INCLUDED_)

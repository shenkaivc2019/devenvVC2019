﻿/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __SCOURT_FUNCTION_H__
#define __SCOURT_FUNCTION_H__

#include "IScoutControl.h"
#ifdef _DEBUG
	#define  ULSCOUTCONNTROL		_T("ULScoutControld.dll")
#else
	#define  ULSCOUTCONNTROL		_T("ULScoutControl.dll")
#endif // _DEBUG
#ifndef DATAEDIT_API
	#ifdef DATAEDIT_EXPORT
		#define DATAEDIT_API __declspec( dllexport )
	#else
		#define DATAEDIT_API __declspec( dllimport )
	#endif
#endif
BOOL DATAEDIT_API LoadScout();
void DATAEDIT_API UnloadScout();

// Create the scoutcontrol object according the project
int DATAEDIT_API CreateScout(CString strFileName,	// Scout file name
				CString strProjName);	// Project name

// Destroy the file
void DATAEDIT_API DestroyScout();

// Add Event Item
int DATAEDIT_API AEI(
		UINT	nID,			// Event id
		LPCTSTR pszSrc,			// Event source
		DWORD	dwType,			// Event type
		LPCTSTR pszEvent,		// Event content
		LPCTSTR pszResult,		// Event result
		LPCTSTR pszRemark = NULL,	// Event remark
		long    lDepth = 0,			// Event depth
		LPCTSTR pszTool = NULL,		// Event tool
		LPCTSTR pszPara = NULL,		// Event parameter
		double	lfValue = 0			// Event value
);

// Add Event Item
int DATAEDIT_API AEI(
		UINT	nID,			// Event id
		LPCTSTR pszSrc,			// Event source
		DWORD	dwType,			// Event type
		UINT	nEvent,			// Event content
		LPCTSTR pszResult,		// Event result
		LPCTSTR pszRemark = NULL,	// Event remark
		long    lDepth = 0,			// Event depth
		LPCTSTR pszTool = NULL,		// Event tool
		LPCTSTR pszPara = NULL,		// Event parameter
		double	lfValue = 0			// Event value
		);

// Add Event Item
int DATAEDIT_API AEI(
		UINT	nID,			// Event id
		LPCTSTR pszSrc,			// Event source
		DWORD	dwType,			// Event type
		UINT	nEvent,		// Event content
		UINT	nResult,		// Event result
		LPCTSTR pszRemark = NULL,	// Event remark
		long    lDepth = 0,			// Event depth
		LPCTSTR pszTool = NULL,		// Event tool
		LPCTSTR pszPara = NULL,		// Event parameter
		double	lfValue = 0			// Event value
		);

// Add Event Item
int DATAEDIT_API AEI(
		UINT	nID,			// Event id
		LPCTSTR pszSrc,			// Event source
		DWORD	dwType,			// Event type
		UINT	nEvent,			// Event content
		UINT	nResult,		// Event result
		UINT    nRemark,		// Event remark
		long    lDepth = 0,			// Event depth
		LPCTSTR pszTool = NULL,		// Event tool
		LPCTSTR pszPara = NULL,		// Event parameter
		double	lfValue = 0			// Event value
		);

// Whether the object validate, Validate:TRUE, Invalidate:FALSE; Only used to open and close Scout files
BOOL DATAEDIT_API IsValidateScout();

DATAEDIT_API extern IScout* g_pScout;

#endif

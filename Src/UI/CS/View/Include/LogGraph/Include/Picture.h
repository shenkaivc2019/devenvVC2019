#if !defined(AFX_PICTURE_H__081D008A_5029_4C56_B80C_37793E323DF6__INCLUDED_)
#define AFX_PICTURE_H__081D008A_5029_4C56_B80C_37793E323DF6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif

#define HIMETRIC_INCH 2540
#define ERROR_TITLE _T("Error")

#ifdef ERROR_TITLE

#include <afxctl.h>

class CPicture : public CPictureHolder
{
public:
	CPicture();
	virtual ~CPicture();

protected:
	HBITMAP Load(DWORD dwLen);
	HGLOBAL  hGlobal;

public:
	HBITMAP LoadPicture(HBITMAP hBitmap);
	BOOL LoadPicture(CString sFilePathName);//从文件读取图像
	BOOL Load(HINSTANCE hInstance,LPCTSTR lpszResourceName, LPCSTR ResourceType);//从资源读取图像
	BOOL LoadPictureData(BYTE* pBuffer, int nSize);//从内存读取图像
	BOOL SaveAsBitmap(CString sFilePathName);//写入到BMP文件
	void Render(CDC* pDC, LPRECT pDrawRect/*目标矩形，单位是逻辑坐标单位*/, LPRECT
		pSrcRect=NULL/*来源矩形，单位是0.01毫米,如果为空，则拉伸整个图像到目标矩形*/,LPCRECT prcWBounds=NULL/*图元文件专用，绑定矩形*/);//在给定的DC上画图，
	
	void  FreePicture();//释放图像，作用同CPictureHolder::~CPictureHolder()
	LONG  get_Height(); // 以0.01毫米为单位的图像高度
	LONG  get_Width();  // 以0.01毫米为单位的图像宽度

public:
	DWORD   _GetWidth  (int nMapMode = MM_TEXT);
	DWORD   _GetHeight (int nMapMode = MM_TEXT);
	BOOL    DrawPicture(HDC hdc, long x, long y, long cx, long cy);
};

#else

#include <stdio.h>
typedef enum
{
	OFM_WRITE        = 0,
	OFM_READ         = 1
}FILE_OPENMODE;

//=-----------------------------
class CFileProcess
{
private :
	FILE  *m_File;

public:
	CFileProcess();
   ~CFileProcess();
    BOOL	Open        (LPCTSTR FileName, FILE_OPENMODE Open_Mode);
	BOOL	Write       (void * zBuffer, DWORD cSize);
    BOOL	Read        (void * zBuffer, DWORD cSize);
	LONG    GetSize     ();
	void    Close       ();
};

class CPicture  
{
public:
	CPicture();
	virtual ~CPicture();
	
protected:
	HBITMAP Load(DWORD dwLen);
	IPicture* m_pPict;
	HGLOBAL  hGlobal;

public:
	HBITMAP LoadPicture(HBITMAP hBitmap);
	HBITMAP LoadPicture(LPCTSTR FileName);
	HBITMAP LoadPicture(LPCTSTR FileName, HDC hdc);
	void    FreePicture();
    HDC     _GetDC     ( void );
	HBITMAP _GetHandle ( void );
	DWORD   _GetWidth  (int nMapMode = MM_TEXT);
	DWORD   _GetHeight (int nMapMode = MM_TEXT);
	BOOL    DrawPicture(HDC hdc, long x, long y, long cx, long cy);
};

#endif

#endif // !defined(AFX_PICTURE_H__081D008A_5029_4C56_B80C_37793E323DF6__INCLUDED_)

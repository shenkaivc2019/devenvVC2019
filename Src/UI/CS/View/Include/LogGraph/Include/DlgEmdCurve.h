#if !defined(AFX_DLGEMDCURVE_H__6C651F6C_3E6A_4DF8_89D4_006CE11D9948__INCLUDED_)
#define AFX_DLGEMDCURVE_H__6C651F6C_3E6A_4DF8_89D4_006CE11D9948__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// DlgEmdCurve.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgEmdCurve dialog
class CULEmd;
class CDlgEmdCurve : public CDialog
{
// Construction
public:
	CDlgEmdCurve(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgEmdCurve)
	enum { IDD = IDD_EMENDATION_CURVE };
	CBCGPListCtrl	m_lstOutput;
	CBCGPListCtrl	m_lstInput;
	//}}AFX_DATA
	
	enum { cname, creset, ccomment, cols};
	CULEmd* m_pULEmd;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgEmdCurve)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgEmdCurve)
	afx_msg void OnButton1();
	virtual BOOL OnInitDialog();
	afx_msg void OnButton3();
	afx_msg void OnButton2();
	afx_msg void OnButton4();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGEMDCURVE_H__6C651F6C_3E6A_4DF8_89D4_006CE11D9948__INCLUDED_)

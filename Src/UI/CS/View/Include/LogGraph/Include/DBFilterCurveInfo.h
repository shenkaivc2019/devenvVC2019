// DBCurveInfo.h: interface for the CDBFilterCurveInfo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DBCURVEINFO_H__B0DB3686_9F5F_46A2_BABE_7E1832D5B603__INCLUDED_)
#define AFX_DBCURVEINFO_H__B0DB3686_9F5F_46A2_BABE_7E1832D5B603__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef DATAFILTERCOND_API
	#ifdef DATAFILTERCOND_EXPORT
		#define DATAFILTERCOND_API __declspec( dllexport )
	#else	
		#define DATAFILTERCOND_API __declspec( dllimport )
	#endif
#endif

enum OrderType
{
	OrderType_MDepth = 1,// 按测深
	OrderType_TVD = 2,// 垂深
	OrderType_Time = 3// 时间
};
enum DepthType
{
	DepthType_MDepth = 0,// 按测深
	DepthType_TVD = 1// 垂深
};
class CDataFilterCond;
class DATAFILTERCOND_API CDBFilterCurveInfo
{
public:
	CDBFilterCurveInfo();
	virtual ~CDBFilterCurveInfo();

	void Assign(CDataFilterCond* pDataFilterCond);
	void Clone(CDBFilterCurveInfo* pDBFilterCurveInfo);
public:
	long m_lStartDepth;
	long m_lEndDepth;
	long m_lInterHorz;
	long m_lInterVert;
	COleDateTime m_StartTime;
	COleDateTime m_EndTime;
	CString m_strRunNo;
	short m_nBad;
	int m_iOnButton;
//2021.06.16 sys Start
	int m_iLas;
//2021.06.16 sys End
	OrderType m_iOrderType;
	int m_nOrder;		//排序方式，默认0 测深降序；
	int m_lReadPos;		//当前读取位置
	long m_lReadCount;	// 读取的数量，如果为-1则无限制
	DepthType m_iDepthType;
	int m_iRelog;
	CString m_strRelogDataID;
	int m_iMemInter;
	CString m_strHoleID;
};

#endif // !defined(AFX_DBCURVEINFO_H__B0DB3686_9F5F_46A2_BABE_7E1832D5B603__INCLUDED_)

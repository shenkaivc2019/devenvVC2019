#ifndef __ULKERNEL_H__
#define __ULKERNEL_H__

#include "stdafx.h"
#include "ULCOMMDEF.h"
#include "ULEXFormat.h"
#include "ULInterface.h"

#ifdef _LOGIC
class CCurveData;
class CGraphWnd;
typedef std::vector<CCurveData*>	vec_data;
#else
#include "MemPerformFile.h"
#endif

class	CCurve;
class	CULTool;
class	CChannel;
class	CULFile;
class	CRawFile;
class	CProject;

typedef std::vector<CULFile*>	vector_ulfile;

#define  UL_ALLREADY		0 
#define  UL_NOTALLREADY		1
#define	 MAX_TOOL_LENGTH	MTD(100)


//////////////////////////////////////////////////////////////////////////////////////////////////
// UL核心类定义及使用

class CULKernel : public CObject
{
	DECLARE_SERIAL(CULKernel)
public:
	CULKernel();								// Constructor
	~CULKernel();								// Destructor 
	virtual void Serialize(CArchive& ar);

public:
	/* ----------------- *
	 *  Tool Operations
	 * ----------------- */
	short	OpenTool();
	short	CloseTool();
	short	ReplayCloseTool();
	short	AddTool(CULTool* pULTool);
	short	AddTool(CString& strToolName);
	short	DeleteTool(CString& strToolName);
	short	OpenTool(CString& strToolName);
	CULTool* GetTool(CString& strToolName);
	void	ClearTools();

	/* ------------------ *
	 *  Curve Operations
	 * ------------------ */
	CCurve*	GetDepthCurve(CURVES& vecCurve);
	CCurveData* GetDepthCurve(vec_data& vecData, vec_data& vecStatus);
	CCurve* GetDepthCurve();
	CCurve* GetTimeCurve();
	short	GetAllCurve(CURVES& vecCurve);
	short	GetAllCurveProps(CURVEPROPS& vecCurve, BOOL bDelAdd = FALSE);
	void	ApplyCurve(CCurve* pCurve);
	long	GetFDepth();

	/* ---------------------- *
	 *  RawFile Operations
	 * ---------------------- */
#ifndef _LOGIC
	BOOL	SaveRawData();			// 向原始文件中保存数据
	BOOL	ReplayRawFile();		// 原始文件数据回放(一帧数据)
	BOOL	ReplayRawFileReadFile();		// add by gj20130603 读取数据
	BOOL	ReplayRawFileCompute(DWORD m_uiReplayRate);		// add by gj20130603 计算数据
	BOOL	ReplayRawFileOnce();	// 一次完成所有回放
	BOOL	ReplayRawCheckBuff();

	void	SaveParamFirst();
	void	SaveParam();
	void	SaveParams(long lDepth, CFile* pFile = NULL, CFile* pRaw = NULL); // 保存仪器参数
#endif
	void	CleanRawBuffer();		// 清除原始缓冲

	/* ------------------------- *
	 *  UL File Store Operations
	 * ------------------------- */
	
#ifndef _LOGIC
	void    SaveData();
	void	LoggingSave();
	void	LoggingSaveBegin();
	void	LoggingSaveEnd();
	void    SaveFileHeaderToDisk(CProject* pProject, CString strFileName);
#endif
	BOOL	SaveCurveValue();
	void	ClearULFiles(LPCTSTR pszMarks = NULL);		// 释放工程文件对象
	long	GetCurSaveDepth();
	BOOL	InterpolateFile(LPCTSTR pszFile);
	void	SaveCurveMarks(LPCTSTR pszFile);

	/* ------------------ *
	 *  Log Operations
	 * ------------------ */
#ifndef _LOGIC
	void	DispatchData();
	short	BeforeLog();
	short	AfterLog();
	short	AfterReplay();
	void    ReplayOpenTool();
	void	RemoveGraph(CGraphWnd* pGraph);
#endif
	
	void	SetWorkMode(int nWorkMode);
	void	CalcCalGene();
	void	SetBoardMode(int nWorkMode);
	void	SetAllUseBoard(int nCmd);
	int		SetCurDisplayMode(int nDispMode); 
	BOOL	SetCommand(AskCommand* pCmd);
	
	short	BeforeCalibrate();
	short	BeforeVerify();
	short	BeforeReplay();
	
	short	AfterCalibrate();
	short	AfterVerify();

	short	SendCommand(char* pCmd, int nNum)	{	return UL_NO_ERROR;	}	// Send the command to physics device
	short	GetData(char* pData, int* nNum)	{	return UL_NO_ERROR;	}	// Get the status from physics device
	short	GetStatus()	{	return UL_NO_ERROR;	}  // Get the status from physics device

	
	void    ReleaseCurvesData();
	void    PrepareToolBuffer(CULTool* pTool);
	void    PrepareReadBuffer(CULTool* pTool);  //add by gj 20130627回放

	BOOL    CanReplayDispatchData();

	BOOL	SaveCurveValueTime();	// add by bao 2013 01 08 时间驱动 

public:
	// 预检查
	BOOL	PreCheckFile(CString& strToolDllName);
	void	PrePareLogInfo();	// 设置测井曲线参数
	void	ApplyUnitSetting();	// 应用已经设置的曲线单位
	void	PrePareGroupInfo();
	void	SetEvent()
	{
		if (m_hEvent)
		{
			::PulseEvent(m_hEvent);
		}
	}

private:
	BOOL	MapExchangeFile();
	BOOL	UnMapExchangeFile();
	BOOL    CanRefresh();

public:
	BOOL SetChannelBufSize(CChannel *pChannel);
	BOOL SetChannelOffset();
	void SetToolInterface();

	HANDLE				m_hEvent;
	BOOL				m_bBoardsSet;
	ULONG				m_nWorkMode;
	
	CPtrArray			m_arrTools;		// 仪器串指针
	CArray<HWND, HWND>	m_hSendArray;
	UINT				m_uMessageID;
	
	long				m_lDepth;
	long				m_lDepthOff;
	long				m_lTime;

	long				m_bRunThread;
	CEvent*				m_pChannelEvent;
	HANDLE				m_hExchangeFile;
	LPVOID				m_lpCodeBuffer;
	ULLogInfo			m_logInfo;

#ifndef _LOGIC
	CRawFile*			m_pRawFile;			// 原始文件指针	
	CMemPerformFile		m_MemPerformFile;	// 内存改造模式
#endif

	CCriticalSection	m_Cs;
	CCurveData*			m_pDepthData;		// 深度时间曲线
	vec_data			m_vecData;
	vec_data			m_vecStatus;
	long				m_nArrivePoint;
	
	BOOL				m_bLoggingSave;		// 测井存盘
	vector_ulfile		m_vecULFile;		// 工程文件链表
	CStringArray		m_filePathArray;
	ULSaveDataInfo*		m_pSaveInfo;
	
	int					m_nCurDisplayMode;  // 当前主界面工作模式
	int					m_nAllToolState;

	CMapStringToPtr		m_mapTool;
	CString				m_strToolName;
	int                 m_nRefreshCal;
	DWORD               m_dwStartTick;
	CStringArray        m_strSetChannels;
	int                 m_nToolIndex;
	
	int					m_nDebug;
	int					m_nReplayDireciton;

	//add by gj 20121228 回放进度条显示
	DWORD				m_dwReplayLength;
	DWORD				m_dwHeadLength;
	DWORD				m_dwCurrLength;
	BOOL				m_bFirstReplay;
	BOOL				m_bLockCheck;//xwh 2013-5-21 the check the thread be locked or not
	BOOL				m_bReadFinish;  //读取完成
	BOOL				m_bWriteFinish; //计算完成
	HANDLE*				ToolEvent;    //深度仪器等待的事件
	HANDLE*				DepthEvent;   //其余仪器等待的事件
	DWORD				m_RefreshCount; //刷新次数

#ifdef _LWD
	CULFile*			m_pRTFile;
#endif

};

//////////////////////////////////////////////////////////////////////////////////////////////////
#endif

// ULDoc.h: interface for the CULDoc class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ULDOC_H__4CABD210_C649_4858_BC7E_12946022515D__INCLUDED_)
#define AFX_ULDOC_H__4CABD210_C649_4858_BC7E_12946022515D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ULInterface.h"
#include "ULCOMMDEF.H"

class CULDoc;
typedef CList<CULDoc*, CULDoc*> CULDocList;

#define  NSTRUCT 8  //宏定义结构体个数

class CCurve;
class CChildFrame;
typedef CList<CChildFrame*, CChildFrame*> CFrameList;
class CGraphHeaderView;
class CGraphWnd;
class CProject;
class CULFile;
class CULView;
class CPrintParam;
class CPof;
class CToolParams;
class CCustomizeView;

//定义一个结构体
typedef struct  
{
	bool IsShow;
	CString StrUp;
	CString StrDown;
	double MinUnit;
	double MaxUnit;
} FIELD;

class CSheet;
class CSheetInfo : public CTemplInfo
{
public:
	CSheet* pSheet;
	CChildFrame* pFrame;
	CSheetInfo()
	{
		pSheet = NULL;
		pFrame = NULL;
	}

	~CSheetInfo();
};

class CCell2000;
class CCellInfo : public CTemplInfo
{	
public:
	CCell2000* pCell;
	CChildFrame* pFrame;
	CCellInfo()
	{
		pCell = NULL;
		pFrame = NULL;
	}
// 2023.08.20 Start
	~CCellInfo();
// 2023.08.20 End
};

class CCstInfo : public CTemplInfo
{
public:
	LPVOID	lpReserved;
	CChildFrame* pFrame;
	CCstInfo()
	{
		lpReserved = NULL;
		pFrame = NULL;
	}
};

typedef struct _tagCLOSEALLFILE
{
	BOOL bCancel;
	BOOL bYesAll;
	BOOL bNoAll;
}CLOSE_ALL_FILE;

#ifdef PEG_CHARTVIEW

typedef struct tagPegDataSource
{
	float   fdepth;        // 深度
	float	fDev;		   // 井斜
	float	fAzim;		   // 方位
}PegDataSource;

typedef struct tagPegDataTarget
{
	float	fVertDepth;	        // 垂深
	float   fSNPos;             //南北坐标
	float   fEWPos;             //东西坐标
	float   fAlpha;             //狗腿度
	float   fProOffset;         //投影位移
	float   fClosedAzim;        //闭合方位
	float   fHOffset;           //水平位移   
} PegDataTarget;

typedef struct tagDATAVALUE
{
	float	lDepth;				// 深度
	float	lVertDepth;			// 垂深
	float	fDev;				// 斜度、井斜
	float	fAzim;				// 方位
	float	fTrueAzim;			// 真方位、闭合方位
	float	fTotalAzim;			// 总方位、投影位移
	float	lTotalOffset;		// 总位移、水平位移
	float	lEPos;				// E坐标、
	float	lNPos;				// N坐标
	float	fAlpha;				// 狗腿度
	float   fTemperature;       //温度
	float   fGSide;             //重力高边
	float   fPSide;             //陀螺高边
}DATAVALUE;

#else

typedef struct tagDATAVALUE
{
	double	fDepth;				// 深度
	double	fVertDepth;			// 垂深
	double	fDev;				// 斜度
	double	fAzim;				// 方位
	double	fTrueAzim;			// 真方位
	double	fTotalAzim;			// 总方位
	double	fTotalOffset;		// 总位移
	double	fEPos;				// E坐标
	double	fNPos;				// N坐标
	double	fAlpha;				// 狗腿度
}DATAVALUE;

#endif

typedef std::vector<DATAVALUE>	vec_dv;
typedef CArray<DATAVALUE, DATAVALUE> arr_dv;

typedef struct tagTARGET
{
	double	fVertDepth;			// 垂深
	double	fRadius;			// 靶半径
	double	fAzim;				// 靶方位
	double	fOffset;			// 靶位移
}TARGET;

typedef std::vector<TARGET>	vec_tg;

typedef struct
{
	long	lDepth;				// 深度
	long	lVertDepth;			// 垂深
	long	lOffset;			// 距井口位移
	double	fAzim;				// 距井口方位
	long	lOffsetFromTarget;	// 距指定靶心位移
	double	fAzimFromTarget;	// 距指定靶心方位
}ValueInfo;

#define	AT_NEW	0x0001
#define AT_FRM	0x0002
#define AT_CRT	0x0004
//#define _FileThread

#define FRAME_SIZE	250
#define DISPLAY_MAP 2000

typedef struct _tagVERTEX_INFO
{
	float x;
	float z;
	BYTE bTime;
	BYTE bColorRank;
}VERTEX_INFO;

typedef struct _tagDRAW_INFO
{
	long nDepth;
	float nCoordinate;
	VERTEX_INFO VertexArray[FRAME_SIZE];
}DRAW_INFO;

class CULDoc : public CDocument
{
	DECLARE_DYNCREATE(CULDoc)

public:
	CULDoc();
	virtual ~CULDoc();

	static CString ComboTemplsName(void* pTemplList)
	{
		CTemplInfoList* pTempls = (CTemplInfoList*)pTemplList;
		CString strRes = _T("");
		POSITION pos = pTempls->GetHeadPosition();
		for ( ; pos != NULL; )
		{
			CTemplInfo* pInfo = pTempls->GetNext(pos);
			if (pInfo && pInfo->strTempl.GetLength())
			{
				strRes += pInfo->strTempl;
				strRes += _T("_");
			}
		}
		
		int nLen = strRes.GetLength();
		if (nLen < 1)
			return strRes;
		
		return strRes.Left( --nLen);
	}

public:
	// Presentation operations
	CSheetInfo* GetSheetInfo(LPCTSTR pszTempl)
	{
		for (POSITION pos = m_sheetiList.GetHeadPosition(); pos != NULL; )
		{
			CSheetInfo* pSheetInfo = m_sheetiList.GetNext(pos);
			if (pSheetInfo->strTempl == pszTempl)
				return pSheetInfo;
		}
		
		return NULL;
	}

	CSheetInfo* AddSheetTempl(LPCTSTR pszTempl, DWORD dwAdd = 0);
	int AddSheetTempls(CStringArray& strTempls)
	{
		int nCount = strTempls.GetSize();
		int nSheets = nCount;
		for (int i = 0; i < nCount; i++)
		{
			if (AddSheetTempl(strTempls[i]))
				continue ;
			nSheets --;
		}
		
		return nSheets;
	}

	void GetSheetFiles(CStringArray* pFiles)
	{
		for (POSITION pos = m_sheetiList.GetHeadPosition(); pos != NULL; )
		{
			CSheetInfo* pSheetInfo = m_sheetiList.GetNext(pos);
			if (pSheetInfo != NULL)
				pFiles->Add(pSheetInfo->strFile);
		}
	}

	void GetSheetTempls(CStringArray* pTempls)
	{
		for (POSITION pos = m_sheetiList.GetHeadPosition(); pos != NULL; )
		{
			CSheetInfo* pSheetInfo = m_sheetiList.GetNext(pos);
			if (pSheetInfo != NULL)
				pTempls->Add(pSheetInfo->strTempl);
		}
	}

	CSheetInfo* RemoveSheetInfo(LPCTSTR pszTempl)
	{
		for (POSITION pos = m_sheetiList.GetHeadPosition(); pos != NULL; )
		{
			POSITION pos1 = pos;
			CSheetInfo* pSheetInfo = m_sheetiList.GetNext(pos);
			if (pSheetInfo->strTempl == pszTempl)
			{
				m_sheetiList.RemoveAt(pos1);
				return pSheetInfo;
			}
		}
		
		return NULL;
	}

	void ClearSheetInfo()
	{
		while (m_sheetiList.GetCount())
		{
			delete m_sheetiList.RemoveHead();
		}
	}
	
	// Header operations

	CCellInfo* GetCellInfo(LPCTSTR pszTempl)
	{
		for (POSITION pos = m_celliList.GetHeadPosition(); pos != NULL; )
		{
			CCellInfo* pCellInfo = m_celliList.GetNext(pos);
			if (pCellInfo->strTempl == pszTempl)
				return pCellInfo;
		}

		return NULL;
	}

	CCellInfo* AddCellTempl(LPCTSTR pszTempl, DWORD dwAdd = 0);
// 2023.08.04 Start
	CCellInfo* AddCellsTempl(LPCTSTR pszTempl, LPCTSTR pszTemplPath, DWORD dwULV = 4, DWORD dwAdd = 0);
	CGraphWnd* GetGraphWnd(LPCTSTR lpszName);
// 2023.08.04 End
	int AddCellTempls(CStringArray& strTempls)
	{
		int nCount = strTempls.GetSize();
		int nCells = nCount;
		for (int i = 0; i < nCount; i++)
		{
			if (AddCellTempl(strTempls[i]))
				continue ;
			nCells --;
		}
		
		return nCells;
	}

	void GetCellTempls(CStringArray* pTempls)
	{
		for (POSITION pos = m_celliList.GetHeadPosition(); pos != NULL; )
		{
			CCellInfo* pCellInfo = m_celliList.GetNext(pos);
			if (pCellInfo != NULL)
				pTempls->Add(pCellInfo->strTempl);
		}
	}

	CCellInfo* RemoveCellInfo(LPCTSTR pszTempl)
	{
		for (POSITION pos = m_celliList.GetHeadPosition(); pos != NULL; )
		{
			POSITION pos1 = pos;
			CCellInfo* pCellInfo = m_celliList.GetNext(pos);
			int nPos = pCellInfo->strTempl.Find ('.');
			CString strName = pCellInfo->strTempl;
			if(nPos >= 0)
				strName = pCellInfo->strTempl.Left (nPos);
			if (strName == pszTempl || pCellInfo->strTempl == pszTempl)
			{
				m_celliList.RemoveAt(pos1);
				return pCellInfo;
			}
		}

		return NULL;
	}

	void ClearCellInfo()
	{
		while (m_celliList.GetCount())
		{
			delete m_celliList.RemoveHead();
		}
	}

	// Get all cells of this document using
	POSITION FindCell(CCell2000* pCell, CGraphHeaderView** ppFView);
	void GetCelliList(CCellInfoList* pCells);
	void CellfromPof();


	CCstInfo* AddCstTempl(LPCTSTR pszTempl, DWORD dwAdd = 0);
	CCstInfo* GetCstInfo(LPCTSTR pszTempl)
	{
		for (POSITION pos = m_cstiList.GetHeadPosition(); pos != NULL; )
		{
			CCstInfo* pCstInfo = m_cstiList.GetNext(pos);
			if (pCstInfo->strTempl == pszTempl)
				return pCstInfo;
		}

		return NULL;
	}

	CCstInfo* RemoveCstInfo(LPCTSTR pszTempl)
	{
		for (POSITION pos = m_cstiList.GetHeadPosition(); pos != NULL; )
		{
			POSITION pos1 = pos;
			CCstInfo* pCstInfo = m_cstiList.GetNext(pos);
			int nPos = pCstInfo->strTempl.Find ('.');
			CString strName = pCstInfo->strTempl;
			if (nPos >= 0)
				strName = pCstInfo->strTempl.Left (nPos);
			if (strName == pszTempl || pCstInfo->strTempl == pszTempl)
			{
				m_cstiList.RemoveAt(pos1);
				return pCstInfo;
			}
		}

		return NULL;
	}

	void ClearCstInfo()
	{
		while (m_cstiList.GetCount())
		{
			delete m_cstiList.RemoveHead();
		}
	}

	// Plot operations

	CPofInfo* GetPofInfo(LPCTSTR pszTempl);

	BOOL AddPofTempl(LPCTSTR pszTempl);
	int AddPofTempls(CStringArray& strTempls)
	{
		int nCount = strTempls.GetSize();
		for (int i = 0; i < nCount; i++)
		{
			if (AddPofTempl(strTempls[i]))
				continue ;
			nCount --;
		}

		return nCount;
	}

	void GetPofTempls(CStringArray* pTempls);
	CPofInfo* RemovePofInfo(LPCTSTR pszTempl);
	void ClearPofInfo();

	// Frame operations
	virtual void OpenFrames(DWORD dwTypes);
	virtual void CloseFrames();
	CChildFrame* GetFirstFrame(DWORD dwType, LPCTSTR pszTempl = NULL);
	

	// View operations
	CView* GetFirstView(DWORD dwType, LPCTSTR pszTempl = NULL);
	CGraphWnd* GetGraphWnd(UINT nIndex = 0);
	CULView* GetCustomizeView(UINT nIndex = 0);
	CGraphWnd* GetPrintGraph();
	void GetDepthRange(int nDirection, long& lDepth0, long& lDepth1, int nDriveMode = UL_DRIVE_DEPT);
	void AdjustAllGraphs(BOOL bRemap = FALSE, BOOL bInit = FALSE);
	CULView* GetPPView(CPrintParam* pParam, BOOL bInit = FALSE);
	void UpdateViews(DWORD dwType);
	void UpdateCells(DWORD dwType);
	void InvalidateCell(CCell2000* pCell, long lCol, long lRow, long lSheet);
	virtual void ShowView(DWORD dwType)
	{

	}

	// Infor operations
	virtual void GetRecord(CString& strDoc, CString& strTime, CString& strRecord,
		CString& strDepthST, CString& strDepthED, CSheet* pSheet)
	{

	}

	virtual void GetRecord2(CString& strComp, CString& strTeam, CString& strWell, CString& strField)
	{

	}

	CPtrArray* GetTools();
	CString	GetSimpleName(LPCTSTR pszPathName = NULL)
	{
		CString strPathName = pszPathName ? pszPathName : m_strPathName;
		int nLen = strPathName.GetLength();
		if (nLen > 0)
		{
			int nF = strPathName.ReverseFind('\\');
			if (nF < 0)
				return strPathName;
			
			return strPathName.Right(nLen - nF - 1);
		}
		
		return _T("");
	}

	static BOOL  OnAdjustView(CGraphWnd* pGraph);
	CPof*        GetPOF(int nIndex);
	void         PreparePrint(CSheet* pSheet, BOOL bRecalcGaps = FALSE);
	void         TraceFile(LPCTSTR pszFile);
	int          Trace(UINT nID, DWORD dwType, UINT nEvent, UINT nResult, LPCTSTR pszRemark = NULL);
	int          Trace(UINT nID, DWORD dwType, UINT nEvent, LPCTSTR pszResult, LPCTSTR pszRemark = NULL);
	void         TraceClose();
	BOOL         IsNoData();
	void         ReleaseCurvesData();
	void         SaveDatas();
	CString      GetToolSNByCurve(CString strCurve);//得到仪器系列号
	CString      GetToolANByCurve(CString strCurve);//得到仪器资产号
	CString		 GetUniqueName(CString strName);
	
    BOOL		 IsCurveNameExist(CString strName);
	void		 SolidCompute();
	CCurve*		 GetCurve(CString strName);			//获取指定的曲线	
	void		 SetDisplayPosition();				//根据当前深度设置显示位置
	void		 SetDisplayDepth(long nDepth);		//设置当前显示深度
	void		 ClearSolidInfo();
// 2024.01.06 Start
	bool	SheetIsUsing(LPCSTR strSheetName, LPSTR lpszPofName);
	bool	CellIsUsing(LPCSTR strCellName, LPSTR lpszPofName);
	bool	IsPofNameExist(CPofInfo* pPofInfo, LPCSTR lpszName);
	// 取得打印对象列表 Pof info list
	void GetPofInfoList(CPofInfoList& pofInfoList, LPCTSTR pszTempl = NULL);
// 2024.01.06 End
public:
	static CULDoc* GetDoc(LPCTSTR pszPathName);
	static CULDoc* GetDoc(int nIndex);
	static CULFile* CloseFiles();
	static BOOL CloseFile(CULFile* pULFile, CLOSE_ALL_FILE *pInfo = NULL);
	static void EarseDoc(CULDoc* pDoc);
	inline static int	GetCount()	{	return m_DocList.GetCount(); }
	static CULFile* GetFile(int nIndex = 0);
	static CULDocList	m_DocList;	// Share opened documents

public:
	CFrameList		m_frmList;		// Frame list
	CSheetInfoList	m_sheetiList;	// Sheet info list
	CCellInfoList	m_celliList;	// Cell info list
	CCstInfoList	m_cstiList;		// Customize info list
	CPofInfoList	m_pofiList;		// Pof info list
	CProject*		m_pProject;		// The project info
	long			m_nRead;
	DWORD			m_dwModified;	// Modify flag of templs info list
	CPtrArray		m_arrTools;		// Tools' array
	vec_ic			m_vecCurve;
	CCurve*			m_pDCurve;
	CCurve*			m_pTCurve;
	CArray<long, long> m_GapDepthArray;
	CList<CToolParams*, CToolParams*> 	    m_tpTree;		// Tool parameters tree
	arr_dv          m_arData;		// 三图一表井斜数据
	vec_tg		    m_vecTarget;	// 三图一表靶心数据
	
	//立体图所需数据
	CPtrArray		m_VertexList;	// 立体图各顶点数据
	UINT			m_nStartPos;	// 起始点
	UINT			m_nEndPos;		// 终止点
	long			m_nDisplayDepth;// 当前显示深度
	long			m_nDisplayRange;// 显示深度范围
	double			m_fRatio;		//径向放大比例

	int				m_nDriveMode;	// 打开文件时文件存储的驱动模式
protected:
	DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_ULDOC_H__4CABD210_C649_4858_BC7E_12946022515D__INCLUDED_)

// PrintOrderFile.h: interface for the CPof class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PRINTORDERFILE_H__6A10774A_33BC_4743_9605_A4ED0766FD6F__INCLUDED_)
#define AFX_PRINTORDERFILE_H__6A10774A_33BC_4743_9605_A4ED0766FD6F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CXMLSettings;

typedef struct ToolCal
{
	BOOL m_IsGraph;
	BOOL m_IsData;
	BOOL m_IsCurve;
	char ToolName[64];
	char CalType[32];
}CALPARAM;

typedef CArray<CALPARAM, CALPARAM> CCALPARAMArray;

class CPrintParam
{
public:
	CPrintParam()
	{
		m_nFx = -1;
		m_dwType = 0;
		m_nParam = 0;
		m_strParam.Empty();
		
		m_bParam = FALSE;
		m_lParam = 0;
		m_lParam2= 0;
	}
	
	~CPrintParam() { }
	virtual void Serialize(CXMLSettings& xml);
	
public:
	int		m_nFx;			// 文件标识
	DWORD	m_dwType;		// 打印类别
	
	int		m_nParam;		// 索引参数				
	CString m_strParam;		// 名称参数
	BOOL	m_bParam;
	long	m_lParam;
	long	m_lParam2;
	
	CCALPARAMArray m_cpArray;
};

#include "ULInterface.h"

class CPof  
{
public:
	CPof();
	virtual ~CPof();

public:
	static void PreIndex(CPtrArray* pParamArray);
	int  GetOrderCount();
	BOOL SaveOrder(LPCTSTR lpszFile);
	BOOL LoadOrder(LPCTSTR lpszFile);
	void AddHead(CStringList* pList, CCellInfoList* pCellList);
	void ClearParams();
	void InitList(CListCtrl* pList);
// 2023.07.08 Start
	void GetGraphSheetName(int iIndex, LPSTR lpszName);
// 2023.07.08 End	

public:
	CString		m_strFile;		// Load/Save file path
	CPtrArray	m_ParamArray;	// Print param array
};

class CChildFrame;
class CPofInfo : public CTemplInfo
{
public:
	CPof* pPof;
	CChildFrame* pFrame;
	CPofInfo()
	{
		pPof = NULL;
		pFrame = NULL;
	}
	
	~CPofInfo();
};

#define PRN_FILE(strTempl) (Gbl_AppPath+"\\Template\\Prints\\"+(strTempl)+".slo")

#endif // !defined(AFX_PRINTORDERFILE_H__6A10774A_33BC_4743_9605_A4ED0766FD6F__INCLUDED_)

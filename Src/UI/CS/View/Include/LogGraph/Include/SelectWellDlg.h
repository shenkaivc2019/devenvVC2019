#if !defined(AFX_SELECTWELLDLG_H__EEC9DCD4_38F4_47A9_B42E_6AA232AC11AE__INCLUDED_)
#define AFX_SELECTWELLDLG_H__EEC9DCD4_38F4_47A9_B42E_6AA232AC11AE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectWellDlg.h : header file
//

#include "ULDoc.h"

/////////////////////////////////////////////////////////////////////////////
// CSelectWellDlg dialog

class CSelectWellDlg : public CDialog
{
// Construction
public:
	CSelectWellDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSelectWellDlg)
	enum { IDD = IDD_CROSSPLOT_SELECT_WELL };
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectWellDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:
    CULDoc* GetSelectedWell(); 

protected:
    CULDoc* m_pDocSel; 
    BOOL IsInvalidValue(double value); 

	// Generated message map functions
	//{{AFX_MSG(CSelectWellDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnTreeSelectChange(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnBtnSelectWell(); 
    afx_msg void OnBtnExit(); 
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTWELLDLG_H__EEC9DCD4_38F4_47A9_B42E_6AA232AC11AE__INCLUDED_)

#if !defined(AFX_CUSTOMIZEVIEW_H__6932A42B_698B_4E30_9320_D2F7928F182F__INCLUDED_)
#define AFX_CUSTOMIZEVIEW_H__6932A42B_698B_4E30_9320_D2F7928F182F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ULFormView.h"
#include "ULCOMMDEF.h"

class CCurve;

class CVIWWell;
class CCstInfo;
class CCustomizeView : public CULFormView
{
	DECLARE_DYNAMIC(CCustomizeView)
public:
	CCustomizeView(LPVOID pCst = NULL);	// constructor used by dynamic creation
	virtual ~CCustomizeView();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCustomizeView)
	protected:
	virtual void OnUpdate();			// overridden to update this view
	//}}AFX_VIRTUAL
	virtual void Draw(CDC* pDC, LPRECT lpRect);
// Implementation
protected:

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CCustomizeView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	afx_msg void OnDataChangedVIWWELL1CTRL1();
	DECLARE_MESSAGE_MAP()

public:
	virtual int CalcPrintHeight();
	virtual void LoadTempl(LPCTSTR pszFile);
	virtual void SaveAsTempl(LPCTSTR pszFile);
	virtual void SaveService();
	
public:
	BOOL	m_bUpdate;
	CCstInfo* m_pCst;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CUSTOMIZEVIEW_H__6932A42B_698B_4E30_9320_D2F7928F182F__INCLUDED_)

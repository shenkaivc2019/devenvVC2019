#if !defined(AFX_GROUPVIEW_H__315322B7_F329_40D8_8EBE_4DA8E68D4D1E__INCLUDED_)
#define AFX_GROUPVIEW_H__315322B7_F329_40D8_8EBE_4DA8E68D4D1E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ULView.h"
#include "ULInterface.h"

// GroupView.h : header file
//
class CCell2000;
class CULFile;
class CPrintOrderFile;

typedef CList<CMFCListCtrl*, CMFCListCtrl*>	CListList;
typedef CList<CCell2000*, CCell2000*>	CCellList;

/////////////////////////////////////////////////////////////////////////////
// CGroupView view
class CRftDataGroupView;
class CRftProfileGroupView;
class CGroupView : public CULView
{
protected:
	DECLARE_DYNCREATE(CGroupView)

// Attributes
public:
	CGroupView(LPVOID lpParam = NULL);
	virtual ~CGroupView();

// Operations
public:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGroupView)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnUpdate();			// overridden to update this view
	virtual void OnUpdateCell();
	//}}AFX_VIRTUAL

// Implementation
protected:
	
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CGroupView)
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
    afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	afx_msg LRESULT OnCellEdit(WPARAM wp, LPARAM lp);
	DECLARE_EVENTSINK_MAP()
	afx_msg void OnEditFinishCell50(UINT nID, LPCTSTR text, long FAR* approve);
	DECLARE_MESSAGE_MAP()
public:
	static POSITION FindCell(CCell2000* pCell, CGraphHeaderView** ppFView);
	virtual int CalcPageHeight();
	virtual int CalcPrintHeight();
	virtual void Draw(CDC* pDC, LPRECT lpRect);
	virtual void Print();
	virtual void SaveAsBitmap(CDC* pDC, LPCTSTR lpszFile);
	void GetViewRange(LPRECT lpRect, int& nStart, int& nEnd);
	void PrepareDraw(CDC* pDC, LPRECT lpRect);
	CCell2000* GetCell(int iIndex, int nOff = 0);
	CBCGPListCtrl* GetList(int iIndex, int nOff = 0);
	void* GetParam(int iIndex, int& nOff);
	void ClearCells();

	virtual void SaveAsTempl(LPCTSTR pszFile);
	virtual void SaveTempl();
public:
	
	CPofInfo*			m_pPofInfo;	// Pointer to the print order infofile
	CPof*				m_pPOFile;	// Pointer to the print order file
	BOOL				m_bRefresh;	// Recalc all view page pos

	CListList			m_listList;	// List list
	CMapWordToPtr		m_listPos;	// Look for list

	CCellList			m_cellList;	// Cell list
	CMapWordToPtr		m_cellPos;	// Look for cell
	int                 m_nCellID;  // The ID of the cell to create

	CArray<int, int>	m_pagePos;	// Each view page start pos y
	CArray<int, int>	m_paramPos;	// To get the param start pos y
	std::vector<int>	m_ctrlsofp;	// Ctrls of param have
	HTREEITEM			m_hItem;	// Tree view item

	CRftDataGroupView*	m_pRftDataGroupView;
	CRftProfileGroupView* m_pRftProfileGroupView;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GROUPVIEW_H__315322B7_F329_40D8_8EBE_4DA8E68D4D1E__INCLUDED_)

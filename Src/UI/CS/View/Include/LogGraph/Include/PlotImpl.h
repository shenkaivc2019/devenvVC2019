/*++

--*/
#ifndef __PLOTIMPL_H
#define __PLOTIMPL_H

#include "IPlot.h"
#include "ULCommdef.h"
#include "Line.h"
#include "PrintInfo.h"
#include <math.h>
#include <atlcom.h>
// 2023.12.09 Start
#define DEBUG_FILE_LOG

#ifdef DEBUG_FILE_LOG
#include <DataSave.h>
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End

extern CFont g_font[ftCount];
//#define _LPTODP

template <class TBase, DWORD dwReserved = 0>
class ATL_NO_VTABLE IPlotImpl : public IPlot
{
public:
// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
		CDataSave m_DebugLogSave; // Debug日志保存
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End

	// Constructor
	IPlotImpl()
	{
		m_pTrack = NULL;
		m_pGridProp = NULL;
	}

	// IPlot
	STDMETHOD(Advise)(ITrack* pHolder)
	{
		if (pHolder == NULL)
			return E_FAIL;

		m_pTrack = pHolder;	
		ASSERT(m_pTrack != NULL);
		if (m_pTrack == NULL)
			return E_FAIL;

		m_pGridProp = (GridProp*)m_pTrack->GetGridProp();
		ASSERT(m_pGridProp != NULL);
		if (m_pGridProp == NULL)
			return E_FAIL;

		return S_OK;
	}

	// 绘制井道头曲线
	STDMETHOD(DrawHeadCurve)(VARIANT* dc, ICurve* pCurve, LPRECT lpRect, BOOL bDrawLine = TRUE)
	{
		CDC* pDC = static_cast<CDC*>(dc->byref);
		if (pDC == NULL)
			return S_FALSE;

		CRect rc = lpRect;
		int nResult = 0;
		
		unsigned Type[8];
		int iLineWidth = pCurve->LineWidth();
		CDashLine DashLine(*pDC, Type, CDashLine::GetPattern(Type, 0, iLineWidth,CDashLine::DL_SOLID));
		
		COLORREF clrCurve = pCurve->Color();
		CPen* pPen = DashLine.GetRightPen(pCurve->LineStyle(), iLineWidth, clrCurve);
		COLORREF cr = pDC->SetTextColor(clrCurve);
		
		CPen* pOldPen = pDC->SelectObject(pPen);
		CFont* pOldFont = pDC->SelectObject((CFont*)m_pTrack->GetFont(ftCT));
		pDC->SetBkMode(TRANSPARENT);
		CStringArray names;
		int i = pCurve->GetTitle(&names);
// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
		if (names.GetSize() > 0)
		{
			CString strPath = "c:\\SKData\\Linear" + names[0];
			m_DebugLogSave.SetFolder(strPath);
		}
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
		int nY = rc.top + 45;
		CRect rcText = rc;
		rcText.bottom -= 45;
		rcText.top = nY;
		int nH = rcText.Height()/((i%2) ? i : (i + 1));
		for (--i; i > -1; i--)
		{
			rcText.bottom = rcText.top + nH;
			pDC->DrawText(names[i], rcText,
				DT_SINGLELINE | DT_VCENTER | DT_CENTER);
			rcText.top = rcText.bottom;
		}
		
		pDC->SelectObject((CFont*)m_pTrack->GetFont(ftCU));
		CString strVal;
		if (bDrawLine)
		{
			DashLine.MoveTo(rc.left, nY);
			DashLine.LineTo(rc.right, nY); // 中间横线		
			
			rcText.left += 4;
			rcText.right -= 4;
			rcText.top = nY;
			rcText.bottom = rcText.top + nH;
			
			TCHAR szVal[32];	
			//if (m_nType == UL_TRACK_WAVE)
			{
				pCurve->GetStartTime(szVal);
			}
			//else
			{
				pCurve->GetLeftVal(szVal);
			}
			
			CString strVal = szVal;
			strVal.TrimLeft(); 
			strVal.TrimRight();
			
			pDC->DrawText(strVal, rcText, DT_SINGLELINE | DT_VCENTER | DT_LEFT);
			
			//if (m_nType == UL_TRACK_WAVE)
			{
				pCurve->GetEndTime(szVal);
			}
			//else
			{
				pCurve->GetRightVal(szVal);
			}
			
			strVal = szVal;
			strVal.TrimLeft(); 
			strVal.TrimRight();
			
			pDC->DrawText(strVal, rcText, DT_SINGLELINE | DT_VCENTER | DT_RIGHT);
		}
		
// 2020.09.05 ltg Start
//		strVal = pCurve->Unit();
		strVal = pCurve->GetLRUnit();
// 2020.09.05 ltg End
		rc.bottom = nY;
		if (strVal.GetLength())
			pDC->DrawText(strVal, rc, DT_SINGLELINE | DT_VCENTER | DT_CENTER);
		
		pDC->SetTextColor(cr);
		pDC->SelectObject(pOldFont);
		pDC->SelectObject(pOldPen);
		delete pPen;

		return S_OK;
	}

	// 绘制曲线
	STDMETHOD(DrawCurve)(VARIANT* dc, ICurve* pCurve, long lStart, long lEnd)
	{
		DrawCurveDefault(dc, pCurve, lStart, lEnd, 0);
		return S_OK;
	}

	// 绘制实时曲线
	STDMETHOD(LogDrawCurve)(VARIANT* dc, ICurve* pCurve, long lStart, long lEnd)
	{
		DrawCurveDefault(dc, pCurve, lStart, lEnd, 1);
		return S_OK;
	}

	STDMETHOD(ChangeGridCount)()
	{
		return S_OK;
	}

	STDMETHOD(Print)(VARIANT* pInfo, ICurve* pCurve, double fMaxDepth, double fMinDepth)
	{
		return S_OK;
	}
	STDMETHOD(StaticPrint)(VARIANT* pInfo, ICurve* pCurve, double fMaxDepth, double fMinDepth)
	{
		return S_OK;
	}

	STDMETHOD(GetCurveXPos)(ICurve* pCurve, long lDepth, BOOL bRolled = TRUE)
	{
		return S_OK;
	}
	STDMETHOD(PrintHeadCurve)(VARIANT* printinfo,ICurve* pCurve, LPRECT lpRect)
	{
		CULPrintInfo* pPrintInfo = static_cast<CULPrintInfo*>(printinfo->byref);
		if (pPrintInfo == NULL)
			return S_OK;

		CRect rect = lpRect;
		
		unsigned Type[8];
		CDashLine DashLine(*pPrintInfo->GetPrintDC(), Type,
			CDashLine::GetPattern(Type, 0,
			pCurve->LinePWidth(), CDashLine::DL_SOLID));
		
		//COLORREF crCurve = Gbl_ConfigInfo.m_Plot.Colored ? pCurve->m_crColor : 0;
		COLORREF crCurve = pCurve->Color();
		CPen* pPen = DashLine.GetRightPen(pCurve->LineStyle(),
			pCurve->LinePWidth(), crCurve);
		
		COLORREF cr = pPrintInfo->GetPrintDC()->SetTextColor(crCurve);
		
		CPen* pOldPen = pPrintInfo->GetPrintDC()->SelectObject(pPen);
		
		// 名称
		CString strLeft, strRight;
		CString strFmt;
		pCurve->LeftDotNum();
		strFmt.Format(_T("%%7.%dlf"), pCurve->LeftDotNum());
		
		//strLeft.Format(strFmt, strVal);
		TCHAR szVal[32];	
		//if (m_nType == UL_TRACK_WAVE)
		{
			pCurve->GetStartTime(szVal);
		}
		//else
		{
			pCurve->GetLeftVal(szVal);
		}		
		strLeft = szVal;

		strLeft.TrimLeft(); strLeft.TrimRight();
		
		strFmt.Format(_T("%%7.%dlf"), pCurve->RightDotNum());
		//strRight.Format(strFmt, pCurve->RightVal());
		//if (m_nType == UL_TRACK_WAVE)
		{
			pCurve->GetEndTime(szVal);
		}
		//else
		{
			pCurve->GetRightVal(szVal);
		}		
		strRight = szVal;

		strRight.TrimLeft(); strRight.TrimRight();

		CStringArray names;
		int i = pCurve->GetTitle(&names);
// 2020.09.05 ltg Start
//		CString strUnit = pCurve->Unit();
		CString strUnit = pCurve->GetLRUnit();
// 2020.09.05 ltg End	
		int nDirection = pPrintInfo->GetPrintDirection();
		int nBkMode = pPrintInfo->GetPrintDC()->SetBkMode(TRANSPARENT);
		if (nDirection == SCROLL_UP)
		{
			CFont* font = (CFont*)m_pTrack->GetFont(ftCT);
			CFont* pOldFont = pPrintInfo->SetFont(font, 1800);

			TEXTMETRIC tm;
			pPrintInfo->GetPrintDC()->GetTextMetrics(&tm);
			UINT nFlags = pPrintInfo->GetPrintDC()->SetTextAlign(TA_RIGHT);
			
			int y = rect.bottom - sp_y;
			CRect rcText = rect;
			rcText.top += sp_y;
			rcText.bottom = y;
			int nH = rcText.Height()/((i%2) ? i : (i + 1));
			rcText.OffsetRect(0, -abs(tm.tmHeight));
			for (--i; i > -1; i--)
			{
				rcText.top = rcText.bottom - nH;
				pPrintInfo->GetPrintDC()->DrawText(names[i], rcText,
					DT_SINGLELINE | DT_VCENTER | DT_CENTER | DT_NOCLIP);
				rcText.bottom = rcText.top;
			}
			
			rcText.left += sp_x;
			rcText.right -= sp_x;
			rcText.bottom = y;
			rcText.top = rcText.bottom - nH;
			
			font = (CFont*)m_pTrack->GetFont(ftCU);
			pPrintInfo->SetFont(font, 1800);
			pPrintInfo->GetPrintDC()->GetTextMetrics(&tm);
			
			rcText.OffsetRect(0, -abs(tm.tmHeight));
			pPrintInfo->GetPrintDC()->DrawText(strRight, rcText,
				DT_SINGLELINE | DT_VCENTER | DT_LEFT | DT_NOCLIP);
			
			pPrintInfo->GetPrintDC()->DrawText(strLeft, rcText,
				DT_SINGLELINE | DT_VCENTER | DT_RIGHT | DT_NOCLIP);
			
			DashLine.MoveTo(rect.left + 2, y);
			DashLine.LineTo(rect.right - 2, y);
			
			if (strUnit.GetLength())
			{
				rcText.top = y;
				rcText.bottom = rcText.top + nH;
				rcText.OffsetRect(0, -abs(tm.tmHeight));
				pPrintInfo->GetPrintDC()->DrawText(strUnit, rcText,
					DT_SINGLELINE | DT_VCENTER | DT_CENTER | DT_NOCLIP);
			}
			
			pPrintInfo->GetPrintDC()->SetTextAlign(nFlags);
			pPrintInfo->GetPrintDC()->SelectObject(pOldFont);
		}
		else
		{
			CFont* font = (CFont*)m_pTrack->GetFont(ftCT);
			CFont* pOldFont = pPrintInfo->SetFont(font);
			UINT nFlags = pPrintInfo->GetPrintDC()->SetTextAlign(TA_LEFT);
			int y = rect.top + sp_y;
			CRect rcText = rect;
			rcText.bottom -= sp_y;
			rcText.top = y;
			int nH = rcText.Height()/((i%2) ? i : (i + 1));
			for (--i; i > -1; i--)
			{
				rcText.bottom = rcText.top + nH;
				pPrintInfo->GetPrintDC()->DrawText(names[i], rcText,
					DT_SINGLELINE | DT_VCENTER | DT_CENTER | DT_NOCLIP);
				rcText.top = rcText.bottom;
			}
			
			rcText.left += sp_x;
			rcText.right -= sp_x;
			rcText.top = y;
			rcText.bottom = rcText.top + nH;
			
			font = (CFont*)m_pTrack->GetFont(ftCU);
			pPrintInfo->SetFont(font);
			pPrintInfo->GetPrintDC()->DrawText(strLeft, rcText,
				DT_SINGLELINE | DT_VCENTER | DT_LEFT | DT_NOCLIP);
			
			pPrintInfo->GetPrintDC()->DrawText(strRight, rcText,
				DT_SINGLELINE | DT_VCENTER | DT_RIGHT | DT_NOCLIP);
			
			DashLine.MoveTo(rect.left + 2, y);
			DashLine.LineTo(rect.right - 2, y);
			
			if (strUnit.GetLength())
			{
				rcText.bottom = y;
				rcText.top = rcText.bottom - nH;
				pPrintInfo->GetPrintDC()->DrawText(strUnit, rcText,
					DT_SINGLELINE | DT_VCENTER | DT_CENTER | DT_NOCLIP);
			}
			
			pPrintInfo->GetPrintDC()->SetTextAlign(nFlags);
			pPrintInfo->GetPrintDC()->SelectObject(pOldFont);
		}
		pPrintInfo->GetPrintDC()->SetBkMode(nBkMode);
		pPrintInfo->GetPrintDC()->SetTextColor(cr);
		pPrintInfo->GetPrintDC()->SelectObject(pOldPen);
		
		delete pPen;
		

		return S_OK;
	}

protected:
	// 绘制曲线
	HRESULT DrawCurveDefault(VARIANT* dc, ICurve* pCurve, long lStartDepth, long lEndDepth, int nMode)
	{
		CDC* pDC = static_cast<CDC*>(dc->byref);
		if (!pCurve->IsVisible())
		{
			return UL_ERROR;
		}
	
		int nData = pCurve->GetDataSize();
		if (nData < 1)
			return UL_ERROR;
		
		int nLeftMargin = pCurve->LeftMargin(FALSE);	// 左格子宽度
		int nRightMargin = pCurve->RightMargin(FALSE);	// 右格子宽度
		if (nLeftMargin == nRightMargin)
			return UL_ERROR;
	
		// 求出x方向每个像素所代表的值
		double fXValuePerPixel = pCurve->RLdVal()/(float)(nRightMargin - nLeftMargin);
		
		int y, nEnd = 0;
// 2020.09.03 ltg Start
//		CArray<CPoint, CPoint> PointArray;
//		PointArray.SetSize(0, 8*sizeof(CPoint));
//		CPoint point;
		CArray<MS_DRAWPOINT, MS_DRAWPOINT> PointArray;
		PointArray.SetSize(0, 8*sizeof(MS_DRAWPOINT));
		MS_DRAWPOINT gDRAWPOINT;
		gDRAWPOINT.bNoDraw = false;
//		::ZeroMemory(&gDRAWPOINT, sizeof(MS_DRAWPOINT));
// 2020.09.03 ltg End
// 2020.09.03 ltg Start
//		long lDepth, lTime;
		long lTime;
		long lPreDepth = 0L, lCurDepth = 0L, lNextDepth = 0L;
// 2020.09.03 ltg End
		
		if (m_pTrack->GetDriveMode() == UL_DRIVE_DEPT)
		{
			long lStart = lStartDepth;
			long lEnd = lEndDepth;
			int nDisplayPos = pCurve->GetCurDisplayPos();
			
			// To avoid solid line been cut
			if (pCurve->LineStyle() == PS_SOLID)
			{   
				nDisplayPos -= 10;
			}
			
			if (nDisplayPos < 0)
				nDisplayPos = 0;

			//Modify by xx 20161110 不能遍历整条曲线

// 2023.09.15 Start
			//修改神开曲线绘制	
			if (pCurve->GetDepthRange(&lStart, &lEnd, nMode ? nDisplayPos : 0))
				return UL_ERROR;
// 			lStart = pCurve->GetFirstIndexByDepth(lStartDepth, nMode);
// 			lEnd = pCurve->GetFirstIndexByDepth(lEndDepth, nMode);
// 2023.09.15 End
// 			lStart = 0;
// 			lEnd = pCurve->GetDataSize() - 1;
			

			if (lStart > 0)
			{
				lStart--;
			}
			
			if (nMode == 0)
			{
				lEnd += 2;
				if (lEnd >= nData)
					lEnd = nData-1;
			}
		
#ifdef _LPTODP
			CPoint pt0(LONG_MAX, LONG_MAX);
#endif
			nEnd = lEnd;
// 2020.09.03 ltg Start
			long lDrawUpInterval = 0;
			lPreDepth = pCurve->GetDepth(lStart);
// 2020.09.03 ltg End

			for (int i = lStart; i <= lEnd; i++)
			{
// 2020.09.03 ltg Start
//				lDepth = pCurve->GetDepth(i);
				lCurDepth = pCurve->GetDepth(i);
// 2020.09.03 ltg End
				DWORD dwMode = pCurve->GetDataMode(i);
			//	if (pCurve->GetDataModeEx(i,1))
				if (dwMode&CURVE_DATAMODE_BAD)
				{
					continue;
				}
// 2020.09.02 ltg Start
				if (dwMode&CURVE_DATAMODE_INVALID)
				{
					continue;
				}
// 2020.09.02 ltg End
// 2020.09.12 ltg Start
//				y = m_pTrack->GetCoordinateFromDepth(lDepth);
				y = m_pTrack->GetCoordinateFromDepth(lCurDepth);
// 2020.09.12 ltg End
				
#ifdef _LPTODP
				CPoint pt1(0, y);
				pDC->LPtoDP(&pt1);
				if (pt0.y == pt1.y) 
					continue;
				pt0.y = pt1.y;		
#else
// 2020.09.03 ltg Start
// 				if (point.y == y)//在深度相同时，曲线无法显示其他值，只能显示第一个值
// 					continue;
// #endif
// 				point.y = y;
// 				point.x = nLeftMargin +
// 					(double)
// 					((pCurve->GetValueEx(i) - pCurve->LeftVal()) /
// 					fXValuePerPixel);
// 				PointArray.Add(point);
				if (gDRAWPOINT.point.y == y)//在深度相同时，曲线无法显示其他值，只能显示第一个值
					continue;
#endif
				double dValue = pCurve->GetValueEx(i);
//				long lDrawDownInterval2 = pCurve->GetDrawInterval();
// 				TRACE(_T("DrawCurveDefault: %s%d %s%d %s%lf\n"), "lDrawDownInterval=", lDrawDownInterval,
// 					"pCurve->GetDrawInterval()=", lDrawDownInterval2,"dValue=", dValue);
// 
// 				TRACE(_T("DrawCurveDefault: %s%d %s%d %s%d\n"), "lCurDepth=", lCurDepth,
// 					"lPreDepth=", lPreDepth,"lNextDepth=", lNextDepth);
				if (dValue == -999.25)
				{
					continue;
				}
				lDrawUpInterval = abs(lCurDepth - lPreDepth);
				if (lDrawUpInterval >= pCurve->GetDrawLineInterval())
				{
					gDRAWPOINT.bNoDraw = true;
					if (PointArray.GetSize() == 0)
					{
						lPreDepth = lCurDepth;
						continue;
					}
				}
				else
				{
					gDRAWPOINT.bNoDraw = false;
				}

				gDRAWPOINT.point.y = y;
// 2021.11.05 ltg Start
// 				gDRAWPOINT.point.x = nLeftMargin +
// 					(double)
// 					((pCurve->GetValueEx(i) - pCurve->LeftVal()) /
// 					fXValuePerPixel);

// 				double dLeftDrawValue = pCurve->GetLeftDrawValue();
// 				if (dLeftDrawValue != -999.25 && dValue < dLeftDrawValue)
// 				{
// 					dValue = dLeftDrawValue;
// 				}
// 				double dRightDrawValue = pCurve->GetRightDrawValue();
// 				if (dRightDrawValue != -999.25 && dValue > dRightDrawValue)
// 				{
// 					dValue = dRightDrawValue;
// 				}
				gDRAWPOINT.point.x = nLeftMargin + (double)((dValue - pCurve->LeftVal()) / fXValuePerPixel);
// 2021.11.05 ltg End
				PointArray.Add(gDRAWPOINT);
				
				lPreDepth = lCurDepth;
// 2020.09.03 ltg End
			}
		}
		else
		{
			long lStartTime = pCurve->GetTime(0);
			long lEndTime = pCurve->GetTime(nData - 1);

			long lStart = lStartDepth;
			long lEnd = lEndDepth;
			int nDisplayPos = pCurve->GetCurDisplayPos();
			
			// To avoid solid line been cut
			if (pCurve->LineStyle() == PS_SOLID)
			{   
				nDisplayPos -= 10;
			}
			
			if (nDisplayPos < 0)
				nDisplayPos = 0;
			
			if (pCurve->GetTimeRange(&lStart, &lEnd, nMode ? nDisplayPos : 0))
				return UL_ERROR;
			
			if (lStart > 0)
			{
				lStart--;
			}
			
			if (nMode == 0)
			{
				lEnd += 2;
				if (lEnd >= nData)
					lEnd = nData-1;
			}
			
			
			  long nStart = lStart;
			  nEnd = lEnd;			
			

			/*int nStart = pCurve->GetFirstIndexByTime(lStartDepth, 1);
			int nEnd = pCurve->GetFirstIndexByTime(lEndDepth, 1);
			nStart = nStart > 0? nStart : 0;
			nEnd = nEnd > 0? nEnd : 0;
			
			if(nStart > nEnd)
			{
				int nTemp = nStart;
				nStart = nEnd;
				nEnd = nTemp;
			}*/



			for(int i = nStart ; i < nEnd ; i++)
			{
				lTime = pCurve->GetTime (i);
				if(lTime < lStartTime)
					continue;
				if(lTime > lEndTime)
					break;
				
				DWORD dwMode = pCurve->GetDataMode(i);
			//	if (pCurve->GetDataModeEx(i,1))
				if (dwMode & CURVE_DATAMODE_BAD)
				{
					continue;
				}
				y = m_pTrack->GetCoordinateFromTime(lTime);
// 2020.09.03 ltg Start
// 				point.y = y;
// 				point.x = nLeftMargin +
// 					(int)
// 					((pCurve->GetValueEx(i) - pCurve->LeftVal()) /
// 					fXValuePerPixel);
// 				PointArray.Add(point);
				gDRAWPOINT.point.y = y;
				gDRAWPOINT.point.x = nLeftMargin +
					(double)
					((pCurve->GetValueEx(i) - pCurve->LeftVal()) /
					fXValuePerPixel);
				PointArray.Add(gDRAWPOINT);
// 2020.09.03 ltg End
			}
		}
		
		if (PointArray.GetSize() < 2)
		{
			return UL_ERROR;
		}
// 2023.12.09 Start
//		return DrawRoll(pCurve, pDC, PointArray.GetData(), PointArray.GetSize(), nMode, nEnd);
		return DrawRoll1(pCurve, pDC, PointArray.GetData(), PointArray.GetSize(), nMode, nEnd);
// 2023.12.09 End
	}

HRESULT DrawRoll(ICurve* pCurve, CDC* pDC, LPMS_DRAWPOINT lpDrawPoints, int nCount, 
		BOOL bRuntime, int nEnd /* = 0 */)
	{
		if (nCount < 1)
			return UL_NO_ERROR;
// 2020.09.03 ltg Start
 		long lStartDraw = 0;
		for(lStartDraw = 0 ; lStartDraw < nCount - 1; lStartDraw++)
		{
			if (!lpDrawPoints[lStartDraw].bNoDraw)
			{
				break;
			}
		}
		lStartDraw--;
		if (lStartDraw >= nCount - 1)
		{
			return UL_NO_ERROR;
		}
// 2020.09.03 ltg End
		//Start
		//在随钻测井时当曲线往回运动时，曲线也需要正常绘制，使用数据库后改功能取消
		//将lpDrawPoints中所有数据点排序
		MS_DRAWPOINT ptTemp;
		ptTemp.bNoDraw = false;
		if(nCount >= 3)
		{
			long nSD = lpDrawPoints[lStartDraw].point.y; 
			long nED = lpDrawPoints[nCount - 1].point.y;

	//		if(nSD > nED)
			{
				for(int i = 0 ; i < nCount - 1 ; i++)
				{
					for(int j = 0 ; j< nCount - 1 ; j++)
					{
						if(lpDrawPoints[j].point.y < lpDrawPoints[j + 1].point.y)
						{
							ptTemp = lpDrawPoints[j];
							lpDrawPoints[j] = lpDrawPoints[j + 1];
							lpDrawPoints[j + 1] = ptTemp;
						}
					}
				}
			}
	/*		else
			{
				for(int i = 0 ; i < nCount - 1 ; i++)
				{
					for(int j = i ; j< nCount - 1 ; j++)
					{
						if(lpDrawPoints[j].y > lpDrawPoints[j + 1].y)
						{
							ptTemp = lpDrawPoints[j];
							lpDrawPoints[j] = lpDrawPoints[j + 1];
							lpDrawPoints[j + 1] = ptTemp;
						}
					}
				}
			}*/
		}

		//End

		if (!pCurve->IsRewind())
		{
 			DrawNoRoll(pCurve, pDC, lpDrawPoints, nCount, bRuntime, nEnd);
			return UL_NO_ERROR;
		}


		CArray<MS_DRAWPOINT, MS_DRAWPOINT> Points;
		Points.RemoveAll();
		
		// shenkun add, 2003-9-16 - 控制曲线回绕次数
		int nRollCount;

		if (nCount == 1)
		{
			pDC->SetPixel(lpDrawPoints[lStartDraw].point, pCurve->Color());
			return UL_NO_ERROR;
		}
		
		unsigned Type[8];
		int w = pCurve->LineWidth();
		COLORREF color = pCurve->Color();
		CDashLine DL(*pDC, Type, CDashLine::GetPattern(Type, 0, w, CDashLine::DL_SOLID));
		// 2021.06.04 ltg Start
		CRect rcTrack;
		m_pTrack->GetRegionRect(&rcTrack, FALSE);
		short nDensity = pCurve->GetDataDensity();
		// 2021.06.04 ltg End
		if (pCurve->IsSelect())
		{
			w += 6;
			int r = 0, g = 0, b = 0;
			color &= 0x00ffffff;
			b = (color & 0x00ff0000); b = b / 3 + 96; 
			g = (color & 0x0000ff00); g = g / 3 + 96; 
			r = (color & 0x000000ff); r = r / 3 + 96;
			color = RGB(r, g, b);
			if (color == 0)
				color = RGB(96, 96, 96);
		}
		CPen* pPen = DL.GetRightPen(pCurve->LineStyle(), w, color);
		CPen* pOldPen = pDC->SelectObject(pPen);	
		
		long Distance, Width;	
		long OutSegment;
		double oldValue;	// oldValue用来确定出界段长
		
		MS_DRAWPOINT oldPoint;
		oldPoint.bNoDraw = false;
		long oldGradeRight, gradeRight, oldGradeLeft, gradeLeft, oldLeft, left;
		double value;
		
		int nLeftMargin = pCurve->LeftMargin(FALSE);	// 得到左右格子的坐标 
		int nRightMargin = pCurve->RightMargin(FALSE);
		
		// Add by YF 2003-9-5 
		// divided by zero
		if ((nRightMargin - nLeftMargin) == 0)
			return UL_NO_ERROR;
		
		// 取出第一各点并判断是否有越界
		// 第一点的处理
		// oldLeft = lpDrawPoints[nCount-1).x - nLeftMargin;
		// oldPoint.y = lpDrawPoints[nCount-1).y;
		oldPoint.bNoDraw = lpDrawPoints[lStartDraw].bNoDraw;
		oldLeft = lpDrawPoints[lStartDraw].point.x - nLeftMargin;
		oldPoint.point.y = lpDrawPoints[lStartDraw].point.y;
		oldGradeRight = (LONG) floor(oldLeft / (nRightMargin - nLeftMargin));		
		
		if (oldLeft < 0)		// 判断曲线是否越界：left<0则曲线越界
		{
			// 如果越界，求出倍数
			oldGradeLeft = (LONG) floor(abs(oldLeft)/(nRightMargin - nLeftMargin)) + 1;
		}
		
		if ((oldGradeRight > 0) || (oldLeft < 0))  // 曲线越右界，曲线越左界
		{
			if (oldGradeRight > 0)
			{
				oldPoint.point.x = lpDrawPoints[lStartDraw].point.x - oldGradeRight * (nRightMargin - nLeftMargin);
			}
			else
			{
				oldPoint.point.x = lpDrawPoints[lStartDraw].point.x + oldGradeLeft * (nRightMargin - nLeftMargin);
			}
		}
		else // 曲线正常
		{
			oldPoint.point.x = lpDrawPoints[lStartDraw].point.x;
		}

		// 2021.06.04 ltg Start
		int iDensityWidth = 25;
		switch (nDensity)
		{
		case 0:
			break;
		case 1:
			DL.MoveTo(rcTrack.left, oldPoint.point.y);
			DL.LineTo(rcTrack.left - iDensityWidth, oldPoint.point.y);
			break;
		case 2:
			DL.MoveTo(rcTrack.left, oldPoint.point.y);
			DL.LineTo(rcTrack.left + iDensityWidth, oldPoint.point.y);
			break;
		case 3:
			DL.MoveTo(rcTrack.right, oldPoint.point.y);
			DL.LineTo(rcTrack.right - iDensityWidth, oldPoint.point.y);
			break;
		case 4:
			DL.MoveTo(rcTrack.right, oldPoint.point.y);
			DL.LineTo(rcTrack.right + iDensityWidth, oldPoint.point.y);
			break;
		}
		// 2021.06.04 ltg End
		
		if (bRuntime)
		{
			DL.MoveToEx(oldPoint.point, pCurve->GetPat(), pCurve->GetStretch());
		}
		else
		{
			DL.MoveTo(oldPoint.point);
		}
		
		Points.Add(oldPoint);
		oldValue = oldPoint.point.x;	// tyh1011
// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
 		//CString strErrorLog;
 		//strErrorLog.Format("\r\n 数据点数%d", nCount);
 		//m_DebugLogSave.SaveErrorLog(strErrorLog);
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
		// 其他点的处理
		// for(int pointSuffix = nCount- 2; pointSuffix >= 0; pointSuffix--)
		for (int pointSuffix = lStartDraw; pointSuffix < nCount ; pointSuffix++)
		{
			//Modify by xx 20150909
			//判断纵坐标，如果和前一个点相同或者相近则不画
			if (lpDrawPoints[pointSuffix].point.y == oldPoint.point.y)
			{
				continue;
			}

			gradeRight = (LONG)floor((lpDrawPoints[pointSuffix].point.x - nLeftMargin)/(nRightMargin - nLeftMargin));
			left = lpDrawPoints[pointSuffix].point.x - nLeftMargin;
			if (left < 0)
			{
				gradeLeft = (LONG) floor(abs(left)/(nRightMargin - nLeftMargin)) + 1;
			}
			
			if ((gradeRight >= 1) || (left < 0))  // 曲线越右界,曲线越左界
			{
				if (gradeRight >= 1)
				{
					value = lpDrawPoints[pointSuffix].point.x - gradeRight * (nRightMargin - nLeftMargin);
				}
				else
				{
					value = lpDrawPoints[pointSuffix].point.x + gradeLeft * (nRightMargin - nLeftMargin);
				}
			}
			else	 // 曲线正常
			{
				value = lpDrawPoints[pointSuffix].point.x;
			}
			// Distance = abs(lpDrawPoints[pointSuffix].x - lpDrawPoints[pointSuffix+1].x); // tyh 1013
			Distance = abs(lpDrawPoints[pointSuffix].point.x - lpDrawPoints[pointSuffix - 1].point.x);
			Width = nRightMargin - nLeftMargin; // tyh 1013
			
			// 曲线由正常(0)变为越右界(1)，则先画与右边界的交点，再画与左边界的交点
			// 曲线由越左界(0)变为正常(1)，则先画右边界的交点，再画与左边界的交点
			if ((gradeRight > oldGradeRight) || (oldLeft < 0 && left >= 0))
			{
				// oldPoint.x = nRightMargin;
				// DashLine.LineTo(nRightMargin, oldPoint.y);
				
				// 画到右边界,后从左边界开始画
				// oldPoint.y = lpDrawPoints[pointSuffix].y;
				// DashLine.MoveTo(nLeftMargin,oldPoint.y);
				
				//////////////////// tyh test  1019 /////////////////////右越界
				int nCount = 0;
				double tempY;
				double VarifyY = 0, IncreamY = 0, tempX = 0;
				
				OutSegment = long(oldValue + Distance - nRightMargin); // 出界段的大小			
				
				// 由三角形等比关系求出越界时当前点距边界点Y的可变增量以及每次越界时距产生Y方向的不变增量,
				// (由于线每次越界折回后与原先的线平行,故增量不变.)
				tempX = nRightMargin - oldValue;// tempX为前一点距越界边界的距离
				if (Distance == 0)
					VarifyY = 0;// 当前点到边界的落差(可变增量)
				else
				{
					//VarifyY = (tempX/Distance)*(lpDrawPoints[pointSuffix].y-lpDrawPoints[pointSuffix+1].y);// 可变增量
					VarifyY = (tempX / Distance) * (lpDrawPoints[pointSuffix].point.y - lpDrawPoints[pointSuffix - 1].point.y);
				}
				
				if (tempX == 0)
					IncreamY = 0;
				else
					IncreamY = VarifyY / tempX * Width;// // 两个边界之间的落差(不变增量):与可变增量有关.(三角关系可推)width/temp的比值与m_fPrintReviseX无关
				
				tempY = oldPoint.point.y + VarifyY;
				// 越界画到越界边界,移到相反的边界.
				nRollCount = 0;
				
				do
				{
// 2020.09.02 ltg Start
// 					DL.LineTo(nRightMargin, (int) tempY);
// 					MS_DRAWPOINT gDrawPoint;
// 					gDrawPoint.point = CPoint(nRightMargin, tempY);
// 					Points.Add(gDrawPoint);
// 					DL.MoveTo(nLeftMargin, (int) tempY);
// 					MS_DRAWPOINT gDrawPoint2;
// 					gDrawPoint2.point = CPoint(nLeftMargin, tempY);
// 					Points.Add(gDrawPoint2);
					if (!oldPoint.bNoDraw)
					{
						DL.LineTo(nRightMargin, (int) tempY);
						// 2021.06.04 ltg Start
						int iDensityWidth = 25;
						switch (nDensity)
						{
						case 0:
							break;
						case 1:
							DL.MoveTo(rcTrack.left, (int)tempY);
							DL.LineTo(rcTrack.left - iDensityWidth, (int)tempY);
							DL.MoveTo(nRightMargin, (int) tempY);
							break;
						case 2:
							DL.MoveTo(rcTrack.left, (int)tempY);
							DL.LineTo(rcTrack.left + iDensityWidth, (int)tempY);
							DL.MoveTo(nRightMargin, (int) tempY);
							break;
						case 3:
							DL.MoveTo(rcTrack.right, (int)tempY);
							DL.LineTo(rcTrack.right - iDensityWidth, (int)tempY);
							DL.MoveTo(nRightMargin, (int) tempY);
							break;
						case 4:
							DL.MoveTo(rcTrack.right, (int)tempY);
							DL.LineTo(rcTrack.right + iDensityWidth, (int)tempY);
							DL.MoveTo(nRightMargin, (int) tempY);
							break;
						}						
						// 2021.06.04 ltg End
					//	TRACE(_T("DrawRoll:  %s \n"), "DL.LineTo(nRightMargin, (int) tempY);");
					}
					else
					{
						DL.MoveTo(nRightMargin, (int) tempY);
					//	TRACE(_T("DrawRoll:  %s \n"), "DL.LineTo(nRightMargin, (int) tempY);");
					}
					MS_DRAWPOINT gDrawPoint;
					gDrawPoint.bNoDraw = oldPoint.bNoDraw;
					gDrawPoint.point = CPoint(nRightMargin, tempY);
					Points.Add(gDrawPoint);
					DL.MoveTo(nLeftMargin, (int) tempY);
					MS_DRAWPOINT gDrawPoint2;
					gDrawPoint2.bNoDraw = oldPoint.bNoDraw;
					gDrawPoint2.point = CPoint(nLeftMargin, tempY);
					Points.Add(gDrawPoint2);
// 2020.09.02 ltg End
					tempY += IncreamY;
					if (nCount > 0)
						OutSegment -= Width;
					nCount++;
					nRollCount++;
				}
				while (OutSegment > Width && nRollCount < pCurve->GetMaxRollCount());
				//////////////////////////////// test end//////////////////// 
			}
			
			// 如果曲线由越右界(1)变为正常(0)，先画与左边界的交点，再画与右边界的交点
			// 如果曲线由正常(1)到越左边界(0)，先画与左边界的焦点，再画与右边界的交点
			else if ((gradeRight < oldGradeRight) || (oldLeft >= 0 && left < 0))//
			{
				// oldPoint.x = nLeftMargin;
				// DashLine.LineTo(nLeftMargin, oldPoint.y);
				
				// 画到左边界后从右边界开始画
				// DashLine.MoveTo(oldPoint.x+nRightMargin-nLeftMargin,oldPoint.y);
				// 与越右界的画线算法不一至，所以修改为下面的语句
				// oldPoint.y = lpDrawPoints[pointSuffix).y;
				// DashLine.MoveTo(nRightMargin,oldPoint.y);
				
				//////////////////////// tyh 1019//////////////////// 
				int nCount = 0;			
				double VarifyY = 0, IncreamY = 0, tempX = 0;
				double tempY;// 移动后的深度
				
				OutSegment = abs(int(nLeftMargin - (oldValue - Distance))); // 出界段的大小			
				
				// 由三角形等比关系求出越界时当前点距边界点Y的可变增量以及每次越界时距产生Y方向的不变增量,
				// (由于线每次越界折回后与原先的线平行,故增量不变.)
				tempX = abs(int(nLeftMargin - oldValue));	// tempX为前一点距越界边界的距离
				if (Distance == 0)
					VarifyY = 0;	// 当前点到边界的落差(可变增量)
				else
				{
					// VarifyY = (tempX/Distance)*(lpDrawPoints[pointSuffix).y-lpDrawPoints[pointSuffix+1).y);// 可变增量
					VarifyY = (tempX / Distance) * (lpDrawPoints[pointSuffix].point.y - lpDrawPoints[pointSuffix-1].point.y);// 可变增量
				}
				
				if (tempX == 0)
					IncreamY = 0;
				else
					IncreamY = VarifyY / tempX * Width; // 两个边界之间的落差(不变增量):与可变增量有关.(三角关系可推)width/temp的比值与m_fPrintReviseX无关
				
				tempY = oldPoint.point.y + VarifyY;
				// 越界画到越界边界,移到相反的边界.
				nRollCount = 0;
				do
				{
// 2020.09.02 ltg Start
// 					DL.LineTo(nLeftMargin, (int) tempY);
// 					MS_DRAWPOINT gDrawPoint;
// 					gDrawPoint.bNoDraw = false;
// 					gDrawPoint.point = CPoint(nLeftMargin, tempY);
// 					Points.Add(gDrawPoint);
// 					DL.MoveTo(nRightMargin, (int) tempY);
// 					MS_DRAWPOINT gDrawPoint2;
// 					gDrawPoint2.bNoDraw = false;
// 					gDrawPoint2.point = CPoint(nRightMargin, tempY);
// 					Points.Add(gDrawPoint2);
					if (!oldPoint.bNoDraw)
					{
						DL.LineTo(nLeftMargin, (int) tempY);
					//	TRACE(_T("DrawRoll:  %s \n"), "DL.LineTo(nLeftMargin, (int) tempY);");
						// 2021.06.04 ltg Start
						int iDensityWidth = 25;
						switch (nDensity)
						{
						case 0:
							break;
						case 1:
							DL.MoveTo(rcTrack.left, (int)tempY);
							DL.LineTo(rcTrack.left - iDensityWidth, (int)tempY);
							DL.MoveTo(nLeftMargin, (int)tempY);
							break;
						case 2:
							DL.MoveTo(rcTrack.left, (int)tempY);
							DL.LineTo(rcTrack.left + iDensityWidth, (int)tempY);
							DL.MoveTo(nLeftMargin, (int)tempY);
							break;
						case 3:
							DL.MoveTo(rcTrack.right, (int)tempY);
							DL.LineTo(rcTrack.right - iDensityWidth, (int)tempY);
							DL.MoveTo(nLeftMargin, (int)tempY);
							break;
						case 4:
							DL.MoveTo(rcTrack.right, (int)tempY);
							DL.LineTo(rcTrack.right + iDensityWidth, (int)tempY);
							DL.MoveTo(nLeftMargin, (int)tempY);
							break;
						}						
						// 2021.06.04 ltg End
					}
					else
					{
						DL.MoveTo(nLeftMargin, (int) tempY);
				//		TRACE(_T("DrawRoll:  %s \n"), "DL.MoveTo(nLeftMargin, (int) tempY);");
					}
					MS_DRAWPOINT gDrawPoint;
					gDrawPoint.bNoDraw = oldPoint.bNoDraw;
					gDrawPoint.point = CPoint(nLeftMargin, tempY);
					Points.Add(gDrawPoint);
					DL.MoveTo(nRightMargin, (int) tempY);
					MS_DRAWPOINT gDrawPoint2;
					gDrawPoint2.bNoDraw = oldPoint.bNoDraw;
					gDrawPoint2.point = CPoint(nRightMargin, tempY);
					Points.Add(gDrawPoint2);
// 2020.09.02 ltg End
					tempY += IncreamY;
					if (nCount > 0)
						OutSegment -= Width;
					nCount++;
					nRollCount++;
				}
				while (OutSegment > Width && nRollCount < pCurve->GetMaxRollCount());
			}
			
			oldGradeRight = gradeRight;    // 如果他们相等则表明他们同为越界或者同为正常	
			oldPoint.bNoDraw = lpDrawPoints[pointSuffix].bNoDraw;
			oldPoint.point.y = lpDrawPoints[pointSuffix].point.y;
			oldPoint.point.x = (long) value;
// 2020.09.02 ltg Start
//			DL.LineTo(oldPoint);
// 			long lD0 = abs(m_pTrack->GetDepthFromCoordinate(lpDrawPoints[pointSuffix].point.y) -
// 				m_pTrack->GetDepthFromCoordinate(lpDrawPoints[pointSuffix - 1].point.y));
// 			long lNoDrawInterval =  pCurve->GetDrawInterval();
// 			if (lD0  < lNoDrawInterval)
//			TRACE(_T("DrawRoll:  %s%d \n"), "oldPoint.bNoDraw=", oldPoint.bNoDraw);
			if (oldPoint.bNoDraw)
			{

				if (bRuntime)
				{
					DL.MoveToEx(oldPoint.point, pCurve->GetPat(), pCurve->GetStretch());
				}
				else
				{
					DL.MoveTo(oldPoint.point);
				}
			}
			else
			{
 				DL.LineTo(oldPoint.point);
				// 2021.06.04 ltg Start
				int iDensityWidth = 25;
				switch (nDensity)
				{
				case 0:
					break;
				case 1:
					DL.MoveTo(rcTrack.left, oldPoint.point.y);
					DL.LineTo(rcTrack.left - iDensityWidth, oldPoint.point.y);
					DL.MoveTo(oldPoint.point);
					break;
				case 2:
					DL.MoveTo(rcTrack.left, oldPoint.point.y);
					DL.LineTo(rcTrack.left + iDensityWidth, oldPoint.point.y);
					DL.MoveTo(oldPoint.point);
					break;
				case 3:
					DL.MoveTo(rcTrack.right, oldPoint.point.y);
					DL.LineTo(rcTrack.right - iDensityWidth, oldPoint.point.y);
					DL.MoveTo(oldPoint.point);
					break;
				case 4:
					DL.MoveTo(rcTrack.right, oldPoint.point.y);
					DL.LineTo(rcTrack.right + iDensityWidth, oldPoint.point.y);
					DL.MoveTo(oldPoint.point);
					break;
				}				
				// 2021.06.04 ltg End
			}
// 2020.09.02 ltg End
			Points.Add(oldPoint);
			oldValue = oldPoint.point.x;
			oldLeft = left;
		}
	
		if (nEnd > pCurve->GetCurDisplayPos())
		{
			pCurve->SetPat(DL.m_CurPat);
			pCurve->SetStretch(DL.m_CurStretch);
			pCurve->SetCurDisplayPos(nEnd);
		}
	
		pDC->SelectObject(pOldPen);
		delete pPen;
		return UL_NO_ERROR;
	}
// 2023.12.09 Start
HRESULT DrawRoll1(ICurve* pCurve, CDC* pDC, LPMS_DRAWPOINT lpDrawPoints, int nCount, BOOL bRuntime, int nEnd /* = 0 */)
{
	if (nCount < 1)
		return UL_NO_ERROR;
	long lStartDraw = 0;
	for (lStartDraw = 0; lStartDraw < nCount - 1; lStartDraw++)
	{
		if (!lpDrawPoints[lStartDraw].bNoDraw)
		{
			break;
		}
	}
	if (lStartDraw >= nCount - 1)
	{
		return UL_NO_ERROR;
	}
	// 2020.09.03 ltg End
			//Start
			//在随钻测井时当曲线往回运动时，曲线也需要正常绘制，使用数据库后改功能取消
			//将lpDrawPoints中所有数据点排序
	MS_DRAWPOINT ptTemp;
	ptTemp.bNoDraw = false;
// 2023.12.11 Start
// 	if (nCount >= 3)
// 	{
// 		long nSD = lpDrawPoints[lStartDraw].point.y;
// 		long nED = lpDrawPoints[nCount - 1].point.y;
// 		// 排序
// 		if(nSD > nED)
// 		{
// 			for (int i = 0; i < nCount - 1; i++)
// 			{
// 				for (int j = 0; j < nCount - 1; j++)
// 				{
// 					if (lpDrawPoints[j].point.y < lpDrawPoints[j + 1].point.y)
// 					{
// 						ptTemp = lpDrawPoints[j];
// 						lpDrawPoints[j] = lpDrawPoints[j + 1];
// 						lpDrawPoints[j + 1] = ptTemp;
// 					}
// 				}
// 			}
// 		}
// 		else
// 		{
// 			for (int i = 0; i < nCount - 1; i++)
// 			{
// 				for (int j = i; j < nCount - 1; j++)
// 				{
// 					if (lpDrawPoints[j].point.y > lpDrawPoints[j + 1].point.y)
// 					{
// 						ptTemp = lpDrawPoints[j];
// 						lpDrawPoints[j] = lpDrawPoints[j + 1];
// 						lpDrawPoints[j + 1] = ptTemp;
// 					}
// 				}
// 			}
// 		}
// 	}
// 2023.12.11 End
	if (!pCurve->IsRewind())
	{
		DrawNoRoll(pCurve, pDC, lpDrawPoints, nCount, bRuntime, nEnd);
		return UL_NO_ERROR;
	}

	CArray<MS_DRAWPOINT, MS_DRAWPOINT> Points;
	Points.RemoveAll();

	// 控制曲线回绕次数
	int nRollCount;

	if (nCount == 1)
	{
		pDC->SetPixel(lpDrawPoints[lStartDraw].point, pCurve->Color());
		return UL_NO_ERROR;
	}

	unsigned Type[8];
	int w = pCurve->LineWidth();
	COLORREF color = pCurve->Color();
	CDashLine DL(*pDC, Type, CDashLine::GetPattern(Type, 0, w, CDashLine::DL_SOLID));
	CRect rcTrack;
	m_pTrack->GetRegionRect(&rcTrack, FALSE);
	short nDensity = pCurve->GetDataDensity();
	if (pCurve->IsSelect())
	{
		w += 6;
		int r = 0, g = 0, b = 0;
		color &= 0x00ffffff;
		b = (color & 0x00ff0000); b = b / 3 + 96;
		g = (color & 0x0000ff00); g = g / 3 + 96;
		r = (color & 0x000000ff); r = r / 3 + 96;
		color = RGB(r, g, b);
		if (color == 0)
			color = RGB(96, 96, 96);
	}
	CPen* pPen = DL.GetRightPen(pCurve->LineStyle(), w, color);
	CPen* pOldPen = pDC->SelectObject(pPen);

	long Distance, Width;
	long OutSegment;
	double oldValue;	// oldValue用来确定出界段长

	MS_DRAWPOINT oldPoint;
	oldPoint.bNoDraw = false;
	long oldGradeRight, gradeRight, oldGradeLeft, gradeLeft, oldLeft, left;
	double value;

	int nLeftMargin = pCurve->LeftMargin(FALSE);	// 得到左右格子的坐标 
	int nRightMargin = pCurve->RightMargin(FALSE);

	// divided by zero
	if ((nRightMargin - nLeftMargin) == 0)
		return UL_NO_ERROR;

	// 取出第一各点并判断是否有越界
	// 第一点的处理
	oldPoint.bNoDraw = lpDrawPoints[lStartDraw].bNoDraw;
	oldLeft = lpDrawPoints[lStartDraw].point.x - nLeftMargin;
	oldPoint.point.y = lpDrawPoints[lStartDraw].point.y;
	oldGradeRight = (LONG)floor(oldLeft / (nRightMargin - nLeftMargin));

	if (oldLeft < 0)		// 判断曲线是否越界：left<0则曲线越界
	{
		// 如果越界，求出倍数
		oldGradeLeft = (LONG)floor(abs(oldLeft) / (nRightMargin - nLeftMargin)) + 1;
	}

	if ((oldGradeRight > 0) || (oldLeft < 0))  // 曲线越右界，曲线越左界
	{
		if (oldGradeRight > 0)
		{
			oldPoint.point.x = lpDrawPoints[lStartDraw].point.x - oldGradeRight * (nRightMargin - nLeftMargin);
		}
		else
		{
			oldPoint.point.x = lpDrawPoints[lStartDraw].point.x + oldGradeLeft * (nRightMargin - nLeftMargin);
		}
	}
	else // 曲线正常
	{
		oldPoint.point.x = lpDrawPoints[lStartDraw].point.x;
	}

	// 2021.06.04 ltg Start
	int iDensityWidth = 25;
	switch (nDensity)
	{
	case 0:
		break;
	case 1:
		DL.MoveTo(rcTrack.left, oldPoint.point.y);
		DL.LineTo(rcTrack.left - iDensityWidth, oldPoint.point.y);
		break;
	case 2:
		DL.MoveTo(rcTrack.left, oldPoint.point.y);
		DL.LineTo(rcTrack.left + iDensityWidth, oldPoint.point.y);
		break;
	case 3:
		DL.MoveTo(rcTrack.right, oldPoint.point.y);
		DL.LineTo(rcTrack.right - iDensityWidth, oldPoint.point.y);
		break;
	case 4:
		DL.MoveTo(rcTrack.right, oldPoint.point.y);
		DL.LineTo(rcTrack.right + iDensityWidth, oldPoint.point.y);
		break;
	}
	// 2021.06.04 ltg End

	if (bRuntime)
	{
		DL.MoveToEx(oldPoint.point, pCurve->GetPat(), pCurve->GetStretch());
	}
	else
	{
		DL.MoveTo(oldPoint.point);
	}

	Points.Add(oldPoint);
	oldValue = oldPoint.point.x;	// tyh1011
// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
// 	CString strErrorLog;
// 	strErrorLog.Format("\r\n 数据点数%d", nCount);
// 	m_DebugLogSave.SaveErrorLog(strErrorLog);
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
			// 其他点的处理
			// for(int pointSuffix = nCount- 2; pointSuffix >= 0; pointSuffix--)
	for (int pointSuffix = lStartDraw; pointSuffix < nCount; pointSuffix++)
	{
		//Modify by xx 20150909
		//判断纵坐标，如果和前一个点相同或者相近则不画
		if (lpDrawPoints[pointSuffix].point.y == oldPoint.point.y)
		{
			continue;
		}

		gradeRight = (LONG)floor((lpDrawPoints[pointSuffix].point.x - nLeftMargin) / (nRightMargin - nLeftMargin));
		left = lpDrawPoints[pointSuffix].point.x - nLeftMargin;
		if (left < 0)
		{
			gradeLeft = (LONG)floor(abs(left) / (nRightMargin - nLeftMargin)) + 1;
		}

		if ((gradeRight >= 1) || (left < 0))  // 曲线越右界,曲线越左界
		{
			if (gradeRight >= 1)
			{
				value = lpDrawPoints[pointSuffix].point.x - gradeRight * (nRightMargin - nLeftMargin);
			}
			else
			{
				value = lpDrawPoints[pointSuffix].point.x + gradeLeft * (nRightMargin - nLeftMargin);
			}
		}
		else	 // 曲线正常
		{
			value = lpDrawPoints[pointSuffix].point.x;
		}
		// Distance = abs(lpDrawPoints[pointSuffix].x - lpDrawPoints[pointSuffix+1].x); // tyh 1013
		Distance = abs(lpDrawPoints[pointSuffix].point.x - lpDrawPoints[pointSuffix - 1].point.x);
		Width = nRightMargin - nLeftMargin; // tyh 1013

		// 曲线由正常(0)变为越右界(1)，则先画与右边界的交点，再画与左边界的交点
		// 曲线由越左界(0)变为正常(1)，则先画右边界的交点，再画与左边界的交点
		if ((gradeRight > oldGradeRight) || (oldLeft < 0 && left >= 0))
		{
			// oldPoint.x = nRightMargin;
			// DashLine.LineTo(nRightMargin, oldPoint.y);

			// 画到右边界,后从左边界开始画
			// oldPoint.y = lpDrawPoints[pointSuffix].y;
			// DashLine.MoveTo(nLeftMargin,oldPoint.y);

			//////////////////// tyh test  1019 /////////////////////右越界
			int nCount = 0;
			double tempY;
			double VarifyY = 0, IncreamY = 0, tempX = 0;

			OutSegment = long(oldValue + Distance - nRightMargin); // 出界段的大小			

			// 由三角形等比关系求出越界时当前点距边界点Y的可变增量以及每次越界时距产生Y方向的不变增量,
			// (由于线每次越界折回后与原先的线平行,故增量不变.)
			tempX = nRightMargin - oldValue;// tempX为前一点距越界边界的距离
			if (Distance == 0)
				VarifyY = 0;// 当前点到边界的落差(可变增量)
			else
			{
				//VarifyY = (tempX/Distance)*(lpDrawPoints[pointSuffix].y-lpDrawPoints[pointSuffix+1].y);// 可变增量
				VarifyY = (tempX / Distance) * (lpDrawPoints[pointSuffix].point.y - lpDrawPoints[pointSuffix - 1].point.y);
			}

			if (tempX == 0)
				IncreamY = 0;
			else
				IncreamY = VarifyY / tempX * Width;// // 两个边界之间的落差(不变增量):与可变增量有关.(三角关系可推)width/temp的比值与m_fPrintReviseX无关

			tempY = oldPoint.point.y + VarifyY;
			// 越界画到越界边界,移到相反的边界.
			nRollCount = 0;

			do
			{
				// 2020.09.02 ltg Start
				// 					DL.LineTo(nRightMargin, (int) tempY);
				// 					MS_DRAWPOINT gDrawPoint;
				// 					gDrawPoint.point = CPoint(nRightMargin, tempY);
				// 					Points.Add(gDrawPoint);
				// 					DL.MoveTo(nLeftMargin, (int) tempY);
				// 					MS_DRAWPOINT gDrawPoint2;
				// 					gDrawPoint2.point = CPoint(nLeftMargin, tempY);
				// 					Points.Add(gDrawPoint2);
				if (!oldPoint.bNoDraw)
				{
					DL.LineTo(nRightMargin, (int)tempY);
					// 2021.06.04 ltg Start
					int iDensityWidth = 25;
					switch (nDensity)
					{
					case 0:
						break;
					case 1:
						DL.MoveTo(rcTrack.left, (int)tempY);
						DL.LineTo(rcTrack.left - iDensityWidth, (int)tempY);
						DL.MoveTo(nRightMargin, (int)tempY);
						break;
					case 2:
						DL.MoveTo(rcTrack.left, (int)tempY);
						DL.LineTo(rcTrack.left + iDensityWidth, (int)tempY);
						DL.MoveTo(nRightMargin, (int)tempY);
						break;
					case 3:
						DL.MoveTo(rcTrack.right, (int)tempY);
						DL.LineTo(rcTrack.right - iDensityWidth, (int)tempY);
						DL.MoveTo(nRightMargin, (int)tempY);
						break;
					case 4:
						DL.MoveTo(rcTrack.right, (int)tempY);
						DL.LineTo(rcTrack.right + iDensityWidth, (int)tempY);
						DL.MoveTo(nRightMargin, (int)tempY);
						break;
					}
					// 2021.06.04 ltg End
				//	TRACE(_T("DrawRoll:  %s \n"), "DL.LineTo(nRightMargin, (int) tempY);");
				}
				else
				{
					DL.MoveTo(nRightMargin, (int)tempY);
					//	TRACE(_T("DrawRoll:  %s \n"), "DL.LineTo(nRightMargin, (int) tempY);");
				}
				MS_DRAWPOINT gDrawPoint;
				gDrawPoint.bNoDraw = oldPoint.bNoDraw;
				gDrawPoint.point = CPoint(nRightMargin, tempY);
				Points.Add(gDrawPoint);
				DL.MoveTo(nLeftMargin, (int)tempY);
				MS_DRAWPOINT gDrawPoint2;
				gDrawPoint2.bNoDraw = oldPoint.bNoDraw;
				gDrawPoint2.point = CPoint(nLeftMargin, tempY);
				Points.Add(gDrawPoint2);
				// 2020.09.02 ltg End
				tempY += IncreamY;
				if (nCount > 0)
					OutSegment -= Width;
				nCount++;
				nRollCount++;
			} while (OutSegment > Width && nRollCount < pCurve->GetMaxRollCount());
			//////////////////////////////// test end//////////////////// 
		}

		// 如果曲线由越右界(1)变为正常(0)，先画与左边界的交点，再画与右边界的交点
		// 如果曲线由正常(1)到越左边界(0)，先画与左边界的焦点，再画与右边界的交点
		else if ((gradeRight < oldGradeRight) || (oldLeft >= 0 && left < 0))//
		{
			// oldPoint.x = nLeftMargin;
			// DashLine.LineTo(nLeftMargin, oldPoint.y);

			// 画到左边界后从右边界开始画
			// DashLine.MoveTo(oldPoint.x+nRightMargin-nLeftMargin,oldPoint.y);
			// 与越右界的画线算法不一至，所以修改为下面的语句
			// oldPoint.y = lpDrawPoints[pointSuffix).y;
			// DashLine.MoveTo(nRightMargin,oldPoint.y);

			//////////////////////// tyh 1019//////////////////// 
			int nCount = 0;
			double VarifyY = 0, IncreamY = 0, tempX = 0;
			double tempY;// 移动后的深度

			OutSegment = abs(int(nLeftMargin - (oldValue - Distance))); // 出界段的大小			

			// 由三角形等比关系求出越界时当前点距边界点Y的可变增量以及每次越界时距产生Y方向的不变增量,
			// (由于线每次越界折回后与原先的线平行,故增量不变.)
			tempX = abs(int(nLeftMargin - oldValue));	// tempX为前一点距越界边界的距离
			if (Distance == 0)
				VarifyY = 0;	// 当前点到边界的落差(可变增量)
			else
			{
				// VarifyY = (tempX/Distance)*(lpDrawPoints[pointSuffix).y-lpDrawPoints[pointSuffix+1).y);// 可变增量
				VarifyY = (tempX / Distance) * (lpDrawPoints[pointSuffix].point.y - lpDrawPoints[pointSuffix - 1].point.y);// 可变增量
			}

			if (tempX == 0)
				IncreamY = 0;
			else
				IncreamY = VarifyY / tempX * Width; // 两个边界之间的落差(不变增量):与可变增量有关.(三角关系可推)width/temp的比值与m_fPrintReviseX无关

			tempY = oldPoint.point.y + VarifyY;
			// 越界画到越界边界,移到相反的边界.
			nRollCount = 0;
			do
			{
				// 2020.09.02 ltg Start
				// 					DL.LineTo(nLeftMargin, (int) tempY);
				// 					MS_DRAWPOINT gDrawPoint;
				// 					gDrawPoint.bNoDraw = false;
				// 					gDrawPoint.point = CPoint(nLeftMargin, tempY);
				// 					Points.Add(gDrawPoint);
				// 					DL.MoveTo(nRightMargin, (int) tempY);
				// 					MS_DRAWPOINT gDrawPoint2;
				// 					gDrawPoint2.bNoDraw = false;
				// 					gDrawPoint2.point = CPoint(nRightMargin, tempY);
				// 					Points.Add(gDrawPoint2);
				if (!oldPoint.bNoDraw)
				{
					DL.LineTo(nLeftMargin, (int)tempY);
					//	TRACE(_T("DrawRoll:  %s \n"), "DL.LineTo(nLeftMargin, (int) tempY);");
						// 2021.06.04 ltg Start
					int iDensityWidth = 25;
					switch (nDensity)
					{
					case 0:
						break;
					case 1:
						DL.MoveTo(rcTrack.left, (int)tempY);
						DL.LineTo(rcTrack.left - iDensityWidth, (int)tempY);
						DL.MoveTo(nLeftMargin, (int)tempY);
						break;
					case 2:
						DL.MoveTo(rcTrack.left, (int)tempY);
						DL.LineTo(rcTrack.left + iDensityWidth, (int)tempY);
						DL.MoveTo(nLeftMargin, (int)tempY);
						break;
					case 3:
						DL.MoveTo(rcTrack.right, (int)tempY);
						DL.LineTo(rcTrack.right - iDensityWidth, (int)tempY);
						DL.MoveTo(nLeftMargin, (int)tempY);
						break;
					case 4:
						DL.MoveTo(rcTrack.right, (int)tempY);
						DL.LineTo(rcTrack.right + iDensityWidth, (int)tempY);
						DL.MoveTo(nLeftMargin, (int)tempY);
						break;
					}
					// 2021.06.04 ltg End
				}
				else
				{
					DL.MoveTo(nLeftMargin, (int)tempY);
					//		TRACE(_T("DrawRoll:  %s \n"), "DL.MoveTo(nLeftMargin, (int) tempY);");
				}
				MS_DRAWPOINT gDrawPoint;
				gDrawPoint.bNoDraw = oldPoint.bNoDraw;
				gDrawPoint.point = CPoint(nLeftMargin, tempY);
				Points.Add(gDrawPoint);
				DL.MoveTo(nRightMargin, (int)tempY);
				MS_DRAWPOINT gDrawPoint2;
				gDrawPoint2.bNoDraw = oldPoint.bNoDraw;
				gDrawPoint2.point = CPoint(nRightMargin, tempY);
				Points.Add(gDrawPoint2);
				// 2020.09.02 ltg End
				tempY += IncreamY;
				if (nCount > 0)
					OutSegment -= Width;
				nCount++;
				nRollCount++;
			} while (OutSegment > Width && nRollCount < pCurve->GetMaxRollCount());
		}

		oldGradeRight = gradeRight;    // 如果他们相等则表明他们同为越界或者同为正常	
		oldPoint.bNoDraw = lpDrawPoints[pointSuffix].bNoDraw;
		oldPoint.point.y = lpDrawPoints[pointSuffix].point.y;
		oldPoint.point.x = (long)value;
		// 2020.09.02 ltg Start
		//			DL.LineTo(oldPoint);
		// 			long lD0 = abs(m_pTrack->GetDepthFromCoordinate(lpDrawPoints[pointSuffix].point.y) -
		// 				m_pTrack->GetDepthFromCoordinate(lpDrawPoints[pointSuffix - 1].point.y));
		// 			long lNoDrawInterval =  pCurve->GetDrawInterval();
		// 			if (lD0  < lNoDrawInterval)
		//			TRACE(_T("DrawRoll:  %s%d \n"), "oldPoint.bNoDraw=", oldPoint.bNoDraw);
		if (oldPoint.bNoDraw)
		{

			if (bRuntime)
			{
				DL.MoveToEx(oldPoint.point, pCurve->GetPat(), pCurve->GetStretch());
			}
			else
			{
				DL.MoveTo(oldPoint.point);
			}
		}
		else
		{
			DL.LineTo(oldPoint.point);
			// 2021.06.04 ltg Start
			int iDensityWidth = 25;
			switch (nDensity)
			{
			case 0:
				break;
			case 1:
				DL.MoveTo(rcTrack.left, oldPoint.point.y);
				DL.LineTo(rcTrack.left - iDensityWidth, oldPoint.point.y);
				DL.MoveTo(oldPoint.point);
				break;
			case 2:
				DL.MoveTo(rcTrack.left, oldPoint.point.y);
				DL.LineTo(rcTrack.left + iDensityWidth, oldPoint.point.y);
				DL.MoveTo(oldPoint.point);
				break;
			case 3:
				DL.MoveTo(rcTrack.right, oldPoint.point.y);
				DL.LineTo(rcTrack.right - iDensityWidth, oldPoint.point.y);
				DL.MoveTo(oldPoint.point);
				break;
			case 4:
				DL.MoveTo(rcTrack.right, oldPoint.point.y);
				DL.LineTo(rcTrack.right + iDensityWidth, oldPoint.point.y);
				DL.MoveTo(oldPoint.point);
				break;
			}
			// 2021.06.04 ltg End
		}
		// 2020.09.02 ltg End
		Points.Add(oldPoint);
		oldValue = oldPoint.point.x;
		oldLeft = left;
	}

	if (nEnd > pCurve->GetCurDisplayPos())
	{
		pCurve->SetPat(DL.m_CurPat);
		pCurve->SetStretch(DL.m_CurStretch);
		pCurve->SetCurDisplayPos(nEnd);
	}

	pDC->SelectObject(pOldPen);
	delete pPen;
	return UL_NO_ERROR;
}
// 2023.12.09 End
	HRESULT DrawNoRoll(ICurve* pCurve, CDC* pDC, LPMS_DRAWPOINT lpDrawPoints, int nCount, 
		BOOL bRuntime, int nEnd /* = 0 */)
	{
		int nLeftMargin = pCurve->LeftMargin(FALSE);	// 得到左右格子的坐标 
		int nRightMargin = pCurve->RightMargin(FALSE);
		
		// divided by zero
		int nLRWidth = abs(nRightMargin - nLeftMargin);
		if (nLRWidth == 0)
			return UL_NO_ERROR;
// 2020.09.03 ltg Start
		long lStartDraw = 0;
		for(lStartDraw = 0 ; lStartDraw < nCount - 1; lStartDraw++)
		{
			if (!lpDrawPoints[lStartDraw].bNoDraw)
			{
				break;
			}
		}
		if (lStartDraw >= nCount - 1)
		{
			return UL_NO_ERROR;
		}
// 2020.09.03 ltg End
		unsigned Type[8];
		int w = pCurve->LineWidth();
		COLORREF color = pCurve->Color();

		MS_DRAWPOINT oldPoint;
		oldPoint.bNoDraw = lpDrawPoints[lStartDraw].bNoDraw;
		oldPoint.point = lpDrawPoints[lStartDraw].point;
		oldPoint.point.x -= nLeftMargin;
		oldPoint.point.y = lpDrawPoints[lStartDraw].point.y;

		int oldGrade = oldPoint.point.x / nLRWidth;
		if (oldPoint.point.x < 0)
			oldGrade--;
		
		oldPoint.point.x = oldPoint.point.x % nLRWidth;
		if (oldPoint.point.x < 0)
			oldPoint.point.x += nLRWidth;
		else if (oldPoint.point.x == 0) // Critical point
		{
			if (oldGrade < 0)
				oldGrade++;
		}

		oldPoint.point.x += nLeftMargin;

		int nMaxRoll = 0; // abs(pCurve->GetMaxRollCount());
		int nMinRoll = -nMaxRoll;

		if (nCount == 1)
		{
			if (abs(oldGrade) <= nMaxRoll)
				pDC->SetPixel(oldPoint.point, color);
			return UL_NO_ERROR;
		}

		CDashLine DL(*pDC, Type, CDashLine::GetPattern(Type, 0, w, CDashLine::DL_SOLID));
		// 2021.06.04 ltg Start
		CRect rcTrack;
		m_pTrack->GetRegionRect(&rcTrack, FALSE);
		short nDensity = pCurve->GetDataDensity();
		// 2021.06.04 ltg End
		if (pCurve->IsSelect())
		{
			w += 6;
			int r = 0, g = 0, b = 0;
			color &= 0x00ffffff;
			b = (color & 0x00ff0000); b = b / 3 + 96; 
			g = (color & 0x0000ff00); g = g / 3 + 96; 
			r = (color & 0x000000ff); r = r / 3 + 96;
			color = RGB(r, g, b);
			if (color == 0)
				color = RGB(96, 96, 96);
		}

		CPen* pPen = DL.GetRightPen(pCurve->LineStyle(), w, color);
		CPen* pOldPen = pDC->SelectObject(pPen);
		// 2021.06.04 ltg Start
		int iDensityWidth = 25;
		switch (nDensity)
		{
		case 0:
			break;
		case 1:
			DL.MoveTo(rcTrack.left, oldPoint.point.y);
			DL.LineTo(rcTrack.left - iDensityWidth, oldPoint.point.y);
			break;
		case 2:
			DL.MoveTo(rcTrack.left, oldPoint.point.y);
			DL.LineTo(rcTrack.left + iDensityWidth, oldPoint.point.y);
			break;
		case 3:
			DL.MoveTo(rcTrack.right, oldPoint.point.y);
			DL.LineTo(rcTrack.right - iDensityWidth, oldPoint.point.y);
			break;
		case 4:
			DL.MoveTo(rcTrack.right, oldPoint.point.y);
			DL.LineTo(rcTrack.right + iDensityWidth, oldPoint.point.y);
			break;
		}
		// 2021.06.04 ltg End
		if (bRuntime)
			DL.MoveToEx(oldPoint.point, pCurve->GetPat(), pCurve->GetStretch());
		else
			DL.MoveTo(oldPoint.point);

		int newGrade;
		MS_DRAWPOINT newPoint;
		double distance;
		if (lStartDraw < 0) lStartDraw = 0;
		for (int i = lStartDraw; i < nCount; i++)
		{
			newPoint = lpDrawPoints[i];
			newPoint.bNoDraw = lpDrawPoints[i].bNoDraw;
			newPoint.point = lpDrawPoints[i].point;
			if(newPoint.point.y == oldPoint.point.y)
				continue;

			newPoint.point.x -= nLeftMargin;
			newGrade = newPoint.point.x / nLRWidth;
			if (newPoint.point.x < 0)
				newGrade--;
			
			newPoint.point.x = newPoint.point.x % nLRWidth;
			if (newPoint.point.x < 0)
				newPoint.point.x += nLRWidth;
			else if (newPoint.point.x == 0) // Critical point
			{
				if (newGrade < 0)
					newGrade++;
			}

			newPoint.point.x += nLeftMargin;

			if(lpDrawPoints[i].point.x == lpDrawPoints[i - 1].point.x)
				oldGrade = newGrade;

// 2020.12.20 ltg Start
			// In same grade, draw directly
			if (newGrade == oldGrade)
			{
				if (abs(newGrade) <= nMaxRoll && !newPoint.bNoDraw)
				{
					DL.LineTo(newPoint.point);
					// 2021.06.04 ltg Start
					int iDensityWidth = 25;
					switch (nDensity)
					{
					case 0:
						break;
					case 1:
						DL.MoveTo(rcTrack.left, oldPoint.point.y);
						DL.LineTo(rcTrack.left - iDensityWidth, oldPoint.point.y);
						DL.MoveTo(newPoint.point);
						break;
					case 2:
						DL.MoveTo(rcTrack.left, oldPoint.point.y);
						DL.LineTo(rcTrack.left + iDensityWidth, oldPoint.point.y);
						DL.MoveTo(newPoint.point);
						break;
					case 3:
						DL.MoveTo(rcTrack.right, oldPoint.point.y);
						DL.LineTo(rcTrack.right - iDensityWidth, oldPoint.point.y);
						DL.MoveTo(newPoint.point);
						break;
					case 4:
						DL.MoveTo(rcTrack.right, oldPoint.point.y);
						DL.LineTo(rcTrack.right + iDensityWidth, oldPoint.point.y);
						DL.MoveTo(newPoint.point);
						break;
					}
					// 2021.06.04 ltg End
				}
				else
					DL.MoveTo(newPoint.point);
				oldPoint = newPoint;
				oldPoint.point = newPoint.point;
				continue;
			}

			distance = abs(lpDrawPoints[i].point.x - lpDrawPoints[i - 1].point.x);
			ASSERT(distance != 0);

			double dYX = (double)(newPoint.point.y - oldPoint.point.y) / distance;
			double IncreamY = dYX * (double)nLRWidth;
			double nY;
			if (newGrade < oldGrade) // Exceed left region
			{
				nY = (double)oldPoint.point.y + dYX * (double)(oldPoint.point.x - nLeftMargin);
				if (oldGrade > nMaxRoll) // 上个点在右出边界
				{
					nY += IncreamY*(oldGrade-nMaxRoll-1);
					DL.MoveTo(nRightMargin, (int) nY);
					nY += IncreamY;
					oldGrade = nMaxRoll;
				}

				while (newGrade < oldGrade)
				{
					if (oldGrade < nMinRoll)
						break;

// 2020.09.13 ltg Start
//					DL.LineTo(nLeftMargin, (int) nY);
					if (oldPoint.bNoDraw)
					{
						DL.MoveTo(nLeftMargin, (int)nY);
					}
					else
					{
						DL.LineTo(nLeftMargin, (int)nY);
						// 2021.06.04 ltg Start
						int iDensityWidth = 25;
						switch (nDensity)
						{
						case 0:
							break;
						case 1:
							DL.MoveTo(rcTrack.left, (int)nY);
							DL.LineTo(rcTrack.left - iDensityWidth, (int)nY);
							DL.MoveTo(nLeftMargin, (int)nY);
							break;
						case 2:
							DL.MoveTo(rcTrack.left, (int)nY);
							DL.LineTo(rcTrack.left + iDensityWidth, (int)nY);
							DL.MoveTo(nLeftMargin, (int)nY);
							break;
						case 3:
							DL.MoveTo(rcTrack.right, (int)nY);
							DL.LineTo(rcTrack.right - iDensityWidth, (int)nY);
							DL.MoveTo(nLeftMargin, (int)nY);
							break;
						case 4:
							DL.MoveTo(rcTrack.right, (int)nY);
							DL.LineTo(rcTrack.right + iDensityWidth, (int)nY);
							DL.MoveTo(nLeftMargin, (int)nY);
							break;
						}					
						// 2021.06.04 ltg End
					}
// 2020.09.13 ltg End
// 					DL.MoveTo(nRightMargin, (int) nY);
					oldGrade--;
					nY += IncreamY;
				}
			}
			else // Exceed right region
			{
				nY = (double)oldPoint.point.y + dYX * (double)(nRightMargin - oldPoint.point.x);
				if (oldGrade < nMinRoll) // 上个点在左出边界
				{
					nY += IncreamY*(nMinRoll-oldGrade-1);
					DL.MoveTo(nLeftMargin, (int) nY);
					nY += IncreamY;
					oldGrade = nMinRoll;
				}

				while (newGrade > oldGrade)
				{
					if (oldGrade > nMaxRoll)
						break;

// 2020.09.13 ltg Start
//					DL.LineTo(nRightMargin, (int) nY);
					if (oldPoint.bNoDraw)
					{
						DL.MoveTo(nRightMargin, (int)nY);
					}
					else
					{
						DL.LineTo(nRightMargin, (int)nY);
						// 2021.06.04 ltg Start
						int iDensityWidth = 25;
						switch (nDensity)
						{
						case 0:
							break;
						case 1:
							DL.MoveTo(rcTrack.left, (int)nY);
							DL.LineTo(rcTrack.left - iDensityWidth, (int)nY);
							DL.MoveTo(nRightMargin, (int)nY);
							break;
						case 2:
							DL.MoveTo(rcTrack.left, (int)nY);
							DL.LineTo(rcTrack.left + iDensityWidth, (int)nY);
							DL.MoveTo(nRightMargin, (int)nY);
							break;
						case 3:
							DL.MoveTo(rcTrack.right, (int)nY);
							DL.LineTo(rcTrack.right - iDensityWidth, (int)nY);
							DL.MoveTo(nRightMargin, (int)nY);
							break;
						case 4:
							DL.MoveTo(rcTrack.right, (int)nY);
							DL.LineTo(rcTrack.right + iDensityWidth, (int)nY);
							DL.MoveTo(nRightMargin, (int)nY);
							break;
						}						
						// 2021.06.04 ltg End
					}
// 2020.09.13 ltg End
// 					DL.MoveTo(nLeftMargin, (int) nY);
					oldGrade++;
					nY += IncreamY;
				}
			}

			if (newGrade == oldGrade)
			{
				if (abs(newGrade) <= nMaxRoll && !newPoint.bNoDraw)
				{
					DL.LineTo(newPoint.point);
					// 2021.06.04 ltg Start
					switch (nDensity)
					{
					case 0:
						break;
					case 1:
						DL.MoveTo(rcTrack.left, newPoint.point.y);
						DL.LineTo(rcTrack.left - iDensityWidth, newPoint.point.y);
						DL.MoveTo(newPoint.point);
						break;
					case 2:
						DL.MoveTo(rcTrack.left, newPoint.point.y);
						DL.LineTo(rcTrack.left + iDensityWidth, newPoint.point.y);
						DL.MoveTo(newPoint.point);
						break;
					case 3:
						DL.MoveTo(rcTrack.right, newPoint.point.y);
						DL.LineTo(rcTrack.right - iDensityWidth, newPoint.point.y);
						DL.MoveTo(newPoint.point);
						break;
					case 4:
						DL.MoveTo(rcTrack.right, newPoint.point.y);
						DL.LineTo(rcTrack.right + iDensityWidth, newPoint.point.y);
						DL.MoveTo(newPoint.point);
						break;
						}					
					// 2021.06.04 ltg End
				}
				else
					DL.MoveTo(newPoint.point);

				oldPoint = newPoint;
				oldPoint.point = newPoint.point;
				continue;
			}

// 2020.12.20 ltg End
			
			DL.MoveTo(newPoint.point);
			oldGrade = newGrade;
			oldPoint.point = newPoint.point;
		}
	
		if (nEnd > pCurve->GetCurDisplayPos())
		{
			pCurve->SetPat(DL.m_CurPat);
			pCurve->SetStretch(DL.m_CurStretch);
			pCurve->SetCurDisplayPos(nEnd);
		}
	
		pDC->SelectObject(pOldPen);
		delete pPen;
	
		return UL_NO_ERROR;
	}

	HRESULT DrawRoll(ICurve* pCurve, int nLeft, int nRight, CDC* pDC, LPMS_DRAWPOINT lpDrawPoints, int nCount)
	{
		if (nCount < 1)
			return UL_NO_ERROR;
		
		if ((nRight - nLeft) == 0)
			return UL_NO_ERROR;
		
		if (nCount == 1)
		{
			COLORREF clr = RGB(0, 0, 0);
			CPen newPen(PS_SOLID, 1, clr);
			CPen* pPen = pDC->SelectObject(&newPen);
			LOGPEN lp;
			if (pPen->GetLogPen(&lp))
				clr = lp.lopnColor;
			pDC->SetPixel(lpDrawPoints[0].point, clr);
			pDC->SelectObject(pPen);
			return UL_NO_ERROR;
		}
		
		int nRollCount;
		long Distance, Width;	
		long OutSegment;
		double oldValue;	// oldValue用来确定出界段长
		POINT oldPoint;
		long oldGradeRight, gradeRight, oldGradeLeft, gradeLeft, oldLeft, left;
		double value;	

		// 2021.06.04 ltg Start
		CRect rcTrack;
		m_pTrack->GetRegionRect(&rcTrack, FALSE);
		short nDensity = pCurve->GetDataDensity();
		// 2021.06.04 ltg End
		
		// 取出第一各点并判断是否有越界
		// 第一点的处理
		oldLeft = lpDrawPoints[nCount - 1].point.x - nLeft;
		oldPoint.y = lpDrawPoints[nCount - 1].point.y;
		oldGradeRight = (LONG) floor(oldLeft / nRight - nLeft);		
		
		if (oldLeft < 0)		// 判断曲线是否越界：left<0则曲线越界
		{
			// 如果越界，求出倍数
			oldGradeLeft = (LONG) floor(abs(oldLeft) / (nRight - nLeft)) + 1;
		}
		
		if ((oldGradeRight >= 1) || (oldLeft < 0))  // 曲线越右界，曲线越左界
		{
			if (oldGradeRight >= 1)
				oldPoint.x = lpDrawPoints[nCount - 1].point.x -
				oldGradeRight * (nRight - nLeft) ;
			else
				oldPoint.x = lpDrawPoints[nCount - 1].point.x +
				oldGradeLeft * (nRight - nLeft);
		}
		else	 // 曲线正常
		{
			oldPoint.x = lpDrawPoints[nCount - 1].point.x;
		}
		
		pDC->MoveTo(oldPoint);
		oldValue = oldPoint.x;	// tyh1011
		
		// 其他点的处理
		for (int pointSuffix = nCount - 2; pointSuffix >= 0; pointSuffix--)
		{
			gradeRight = (LONG) floor((lpDrawPoints[pointSuffix].point.x - nLeft) /
				(nRight - nLeft));
			left = lpDrawPoints[pointSuffix].point.x - nLeft;
			if (left < 0)
			{
				gradeLeft = (LONG) floor(abs(left) / (nRight - nLeft)) + 1;
			}
			
			if ((gradeRight >= 1) || (left < 0))  // 曲线越右界,曲线越左界
			{
				if (gradeRight >= 1)
					value = lpDrawPoints[pointSuffix].point.x -
					gradeRight * (nRight - nLeft) ;
				else
					value = lpDrawPoints[pointSuffix].point.x +
					gradeLeft * (nRight - nLeft)  ;
			}
			else	 // 曲线正常
			{
				value = lpDrawPoints[pointSuffix].point.x;
			}
			Distance = abs(lpDrawPoints[pointSuffix].point.x - lpDrawPoints[pointSuffix + 1].point.x);// tyh 1013
			Width = nRight - nLeft;// tyh 1013
			// 曲线由正常(0)变为越右界(1)，则先画与右边界的交点，再画与左边界的交点
			// 曲线由越左界(0)变为正常(1)，则先画右边界的交点，再画与左边界的交点
			if ((gradeRight > oldGradeRight) || (oldLeft < 0 && left >= 0))
			{
				// oldPoint.x = nRight;
				// DashLine.LineTo(nRight, oldPoint.y);
				
				// 画到右边界,后从左边界开始画
				// oldPoint.y = lpPoint[pointSuffix].y;
				// DashLine.MoveTo(nLeft,oldPoint.y);
				
				//////////////////// tyh test  1019 /////////////////////右越界
				int nCount = 0;
				double tempY;
				double VarifyY = 0, IncreamY = 0, tempX = 0;
				
				OutSegment = long(oldValue + Distance - nRight); // 出界段的大小			
				
				// 由三角形等比关系求出越界时当前点距边界点Y的可变增量以及每次越界时距产生Y方向的不变增量,
				// (由于线每次越界折回后与原先的线平行,故增量不变.)
				tempX = nRight - oldValue;// tempX为前一点距越界边界的距离
				if (Distance == 0)
					VarifyY = 0;// 当前点到边界的落差(可变增量)
				else
					VarifyY = (tempX / Distance) * (lpDrawPoints[pointSuffix].point.y -
					lpDrawPoints[pointSuffix + 1].point.y);// 可变增量
				
				if (tempX == 0)
					IncreamY = 0;
				else
					IncreamY = VarifyY / tempX * Width;// // 两个边界之间的落差(不变增量):与可变增量有关.(三角关系可推)width/temp的比值与m_fPrintReviseX无关
				tempY = oldPoint.y + VarifyY;
				// 越界画到越界边界,移到相反的边界.
				nRollCount = 0;
				do
				{
					pDC->LineTo(nRight, (int) tempY);
					// 2021.06.04 ltg Start
					int iDensityWidth = 25;
					switch (nDensity)
					{
					case 0:
						break;
					case 1:
						pDC->MoveTo(rcTrack.left, (int)tempY);
						pDC->LineTo(rcTrack.left - iDensityWidth, (int)tempY);
						break;
					case 2:
						pDC->MoveTo(rcTrack.left, (int)tempY);
						pDC->LineTo(rcTrack.left + iDensityWidth, (int)tempY);
						break;
					case 3:
						pDC->MoveTo(rcTrack.right, (int)tempY);
						pDC->LineTo(rcTrack.right - iDensityWidth, (int)tempY);
						break;
					case 4:
						pDC->MoveTo(rcTrack.right, (int)tempY);
						pDC->LineTo(rcTrack.right + iDensityWidth, (int)tempY);
						break;
					}
					// 2021.06.04 ltg End
					pDC->MoveTo(nLeft, (int) tempY);
					tempY += IncreamY;
					if (nCount > 0)
						OutSegment -= Width;
					nCount++;
					nRollCount++;
				}
				while (OutSegment > Width && nRollCount < pCurve->GetMaxRollCount());
				//////////////////////////////// test end//////////////////// 
				
			}
			// 如果曲线由越右界(1)变为正常(0)，先画与左边界的交点，再画与右边界的交点
			// 如果曲线由正常(1)到越左边界(0)，先画与左边界的焦点，再画与右边界的交点
			else if ((gradeRight < oldGradeRight) || (oldLeft >= 0 && left < 0))//
			{
				// oldPoint.x = nLeft;
				// DashLine.LineTo(nLeft, oldPoint.y);
				
				// 画到左边界后从右边界开始画
				// DashLine.MoveTo(oldPoint.x+nRight-nLeft,oldPoint.y);
				// 与越右界的画线算法不一至，所以修改为下面的语句
				// oldPoint.y = lpPoint[pointSuffix].y;
				// DashLine.MoveTo(nRight,oldPoint.y);
				
				//////////////////////// tyh 1019//////////////////// 
				int nCount = 0;			
				double VarifyY = 0, IncreamY = 0, tempX = 0;
				double tempY;// 移动后的深度
				
				OutSegment = abs(int(nLeft - (oldValue - Distance))); // 出界段的大小			
				
				// 由三角形等比关系求出越界时当前点距边界点Y的可变增量以及每次越界时距产生Y方向的不变增量,
				// (由于线每次越界折回后与原先的线平行,故增量不变.)
				tempX = abs(int(nLeft - oldValue));	// tempX为前一点距越界边界的距离
				if (Distance == 0)
					VarifyY = 0;	// 当前点到边界的落差(可变增量)
				else
					VarifyY = (tempX / Distance) * (lpDrawPoints[pointSuffix].point.y -
					lpDrawPoints[pointSuffix + 1].point.y);// 可变增量
				
				if (tempX == 0)
					IncreamY = 0;
				else
					IncreamY = VarifyY / tempX * Width; // 两个边界之间的落差(不变增量):与可变增量有关.(三角关系可推)width/temp的比值与m_fPrintReviseX无关
				
				tempY = oldPoint.y + VarifyY;
				// 越界画到越界边界,移到相反的边界.
				nRollCount = 0;
				do
				{
					pDC->LineTo(nLeft, (int) tempY);
					// 2021.06.04 ltg Start
					int iDensityWidth = 25;
					switch (nDensity)
					{
					case 0:
						break;
					case 1:
						pDC->MoveTo(rcTrack.left, (int)tempY);
						pDC->LineTo(rcTrack.left - iDensityWidth, (int)tempY);
						break;
					case 2:
						pDC->MoveTo(rcTrack.left, (int)tempY);
						pDC->LineTo(rcTrack.left + iDensityWidth, (int)tempY);
						break;
					case 3:
						pDC->MoveTo(rcTrack.right, (int)tempY);
						pDC->LineTo(rcTrack.right - iDensityWidth, (int)tempY);
						break;
					case 4:
						pDC->MoveTo(rcTrack.right, (int)tempY);
						pDC->LineTo(rcTrack.right + iDensityWidth, (int)tempY);
						break;
					}
					// 2021.06.04 ltg End
					pDC->MoveTo(nRight, (int) tempY);
					tempY += IncreamY;
					if (nCount > 0)
						OutSegment -= Width;
					nCount++;
					nRollCount++;
				}
				while (OutSegment > Width && nRollCount < pCurve->GetMaxRollCount());
			}
			oldGradeRight = gradeRight;    // 如果他们相等则表明他们同为越界或者同为正常			 
			oldPoint.y = lpDrawPoints[pointSuffix].point.y;
			oldPoint.x = (long) value;
			
			pDC->LineTo(oldPoint);
			// 2021.06.04 ltg Start
			int iDensityWidth = 25;
			switch (nDensity)
			{
			case 0:
				break;
			case 1:
				pDC->MoveTo(rcTrack.left, oldPoint.y);
				pDC->LineTo(rcTrack.left - iDensityWidth, oldPoint.y);
				pDC->MoveTo(oldPoint);
				break;
			case 2:
				pDC->MoveTo(rcTrack.left, oldPoint.y);
				pDC->LineTo(rcTrack.left + iDensityWidth, oldPoint.y);
				pDC->MoveTo(oldPoint);
				break;
			case 3:
				pDC->MoveTo(rcTrack.right, oldPoint.y);
				pDC->LineTo(rcTrack.right - iDensityWidth, oldPoint.y);
				pDC->MoveTo(oldPoint);
				break;
			case 4:
				pDC->MoveTo(rcTrack.right, oldPoint.y);
				pDC->LineTo(rcTrack.right + iDensityWidth, oldPoint.y);
				pDC->MoveTo(oldPoint);
				break;
					}
			
			// 2021.06.04 ltg End
			oldValue = oldPoint.x;
			oldLeft = left;
	}
	
	return UL_NO_ERROR;
}

	HRESULT PrintRoll(ICurve* pCurve, CULPrintInfo* pInfo, LPMS_DRAWPOINT lpDrawPoints, int nCount, int nLeftMargin, int nRightMargin)
	{
// 2023.12.09 Start
		return PrintRoll1(pCurve, pInfo, lpDrawPoints,  nCount, nLeftMargin, nRightMargin);
// 2023.12.09 End
		// divided by zero
		if (nRightMargin == nLeftMargin)
			return UL_NO_ERROR;
		
		if (nCount <= 0)
			return UL_NO_ERROR;
// 2020.09.19 ltg Start
		long lStartDraw = 0;
		for (lStartDraw = 0 ; lStartDraw < nCount - 1; lStartDraw++)
		{
			if (!lpDrawPoints[lStartDraw].bNoDraw)
			{
				break;
			}
		}
		lStartDraw--;
		if (lStartDraw >= nCount - 1)
		{
			return UL_NO_ERROR;
		}
// 2020.09.19 ltg End	
		
		// 2021.06.04 ltg Start
		CRect rcTrack;
		m_pTrack->GetRegionRect(&rcTrack, TRUE);
		rcTrack = pInfo->CoordinateConvert(rcTrack);
		short nDensity = pCurve->GetDataDensity();
		// 2021.06.04 ltg End
		//Start
		//在随钻测井时当曲线往回运动时，曲线也需要正常绘制，使用数据库后改功能取消
		//将lpDrawPoints中所有数据点排序
		MS_DRAWPOINT ptTemp;
		ptTemp.bNoDraw =false;
		if(nCount >= 3)
		{
			long nSD = lpDrawPoints[0].point.y; 
			long nED = lpDrawPoints[nCount - 1].point.y;

	//		if(nSD > nED)
			{
				for(int i = 0 ; i < nCount - 1 ; i++)
				{
					for(int j = 0 ; j< nCount - 1 ; j++)
					{
						if(lpDrawPoints[j].point.y < lpDrawPoints[j + 1].point.y)
						{
							ptTemp = lpDrawPoints[j];
							lpDrawPoints[j] = lpDrawPoints[j + 1];
							lpDrawPoints[j + 1] = ptTemp;
						}
					}
				}
			}
	/*		else
			{
				for(int i = 0 ; i < nCount - 1 ; i++)
				{
					for(int j = i ; j< nCount - 1 ; j++)
					{
						if(lpDrawPoints[j].y > lpDrawPoints[j + 1].y)
						{
							ptTemp = lpDrawPoints[j];
							lpDrawPoints[j] = lpDrawPoints[j + 1];
							lpDrawPoints[j + 1] = ptTemp;
						}
					}
				}
			}*/
		}

		//End

		if (!pCurve->IsRewind())
		{
			PrintNoRoll(pCurve, pInfo, lpDrawPoints, nCount, nLeftMargin, nRightMargin);
			return UL_NO_ERROR;
		}
		
		// 控制曲线回绕次数
		int nRollCount;
		
		//COLORREF crCurve = Gbl_ConfigInfo.m_Plot.Colored ? pCurve->m_crColor : 0;
		COLORREF crCurve = pCurve->Color();
		CDC* pDC = pInfo->GetPrintDC();
		if (nCount == 1)
		{
			pDC->SetPixel(lpDrawPoints[0].point, crCurve);
			return UL_NO_ERROR;
		}

		unsigned Type[8];
		int w = pCurve->LinePWidth();

		CDashLine DL(*pDC, Type, CDashLine::GetPattern(Type, 0, w, CDashLine::DL_SOLID));
		
		CPen* pPen = DL.GetRightPen(pCurve->LineStyle(), w, crCurve);
		CPen* pOldPen = pDC->SelectObject(pPen);
		
			int nDistance, nWidth;	
			int OutSegment, oldValue;	// oldValue用来确定出界段长						
			int tempLeftMargin = nLeftMargin;
			int tempRightMargin = nRightMargin;
			
			MS_DRAWPOINT oldPoint;			// 记录上一点位置
			oldPoint.bNoDraw =false;
			bool bNoDraw =false;
			MS_DRAWPOINT tempPoint;
			tempPoint.bNoDraw =false;
			long oldGradeRight, gradeRight;	
			long oldGradeLeft, gradeLeft;	
			long oldLeft, left;
			int nConvertX;				// 换算后的x值 
			

// 2020.09.19 ltg Start
//			// 第一个点设置为最后一个
//			oldPoint = lpDrawPoints[nCount - 1];
// 			oldPoint.bNoDraw = lpDrawPoints[nCount - 1].bNoDraw;
// 			
// 			oldGradeRight = (LONG)
// 				floor((lpDrawPoints[nCount - 1].point.x - nLeftMargin) /
// 				(nRightMargin - nLeftMargin));
// 			oldLeft = lpDrawPoints[nCount - 1].point.x - nLeftMargin;
// 			
// 			if (oldLeft < 0)	// 判断曲线是否越界：left<0则曲线越界
// 				// 如果越界，求出倍数
// 				oldGradeLeft = (LONG) floor(abs(oldLeft) /
// 				(nRightMargin - nLeftMargin)) + 1;
// 			
// 			if ((oldGradeRight >= 1) || (oldLeft < 0))  //曲线越右界，曲线越左界
// 			{
// 				// 换算为x值
// 				if (oldGradeRight >= 1)	// 越右界
// 					oldPoint.point.x = lpDrawPoints[nCount - 1].point.x -
// 					oldGradeRight * (nRightMargin - nLeftMargin);
// 				else					// 越左界
// 					oldPoint.point.x = lpDrawPoints[nCount - 1].point.x +
// 					oldGradeLeft * (nRightMargin - nLeftMargin);
// 			}
// 			else	 // 曲线正常
// 				oldPoint.point.x = lpDrawPoints[nCount - 1].point.x;
// 			
// 			
// 			// 根据方向得到X值
// 			oldPoint.point.x = pInfo->CoordinateConvertX(oldPoint.point.x);

			// 第一个点设置为第一个
			oldPoint = lpDrawPoints[lStartDraw];
			oldPoint.bNoDraw = lpDrawPoints[lStartDraw].bNoDraw;
			
			oldGradeRight = (LONG)
				floor((lpDrawPoints[lStartDraw].point.x - nLeftMargin) /
				(nRightMargin - nLeftMargin));
			oldLeft = lpDrawPoints[lStartDraw].point.x - nLeftMargin;
			
			if (oldLeft < 0)	// 判断曲线是否越界：left<0则曲线越界
				// 如果越界，求出倍数
				oldGradeLeft = (LONG) floor(abs(oldLeft) /
				(nRightMargin - nLeftMargin)) + 1;
			
			if ((oldGradeRight >= 1) || (oldLeft < 0))  //曲线越右界，曲线越左界
			{
				// 换算为x值
				if (oldGradeRight >= 1)	// 越右界
					oldPoint.point.x = lpDrawPoints[lStartDraw].point.x -
					oldGradeRight * (nRightMargin - nLeftMargin);
				else					// 越左界
					oldPoint.point.x = lpDrawPoints[lStartDraw].point.x +
					oldGradeLeft * (nRightMargin - nLeftMargin);
			}
			else	 // 曲线正常
				oldPoint.point.x = lpDrawPoints[lStartDraw].point.x;
			
			
			// 根据方向得到X值
			oldPoint.point.x = pInfo->CoordinateConvertX(oldPoint.point.x);
// 2020.09.19 ltg End
			// 2021.06.04 ltg Start
			int iDensityWidth = 13;
			switch (nDensity)
			{
			case 0:
				break;
			case 1:
				DL.MoveTo(rcTrack.left, oldPoint.point.y);
				DL.LineTo(rcTrack.left - iDensityWidth, oldPoint.point.y);
				break;
			case 2:
				DL.MoveTo(rcTrack.left, oldPoint.point.y);
				DL.LineTo(rcTrack.left + iDensityWidth, oldPoint.point.y);
				break;
			case 3:
				DL.MoveTo(rcTrack.right, oldPoint.point.y);
				DL.LineTo(rcTrack.right - iDensityWidth, oldPoint.point.y);
				break;
			case 4:
				DL.MoveTo(rcTrack.right, oldPoint.point.y);
				DL.LineTo(rcTrack.right + iDensityWidth, oldPoint.point.y);
				break;
			}
			// 2021.06.04 ltg End
			oldValue = oldPoint.point.x;
			DL.MoveTo(oldPoint.point);		
			
// 2020.09.19 ltg Start
//			for (int i = nCount - 1; i >= 0; i--)
			// 2020.12.20 ltg Start
			//for (int i = lStartDraw ; i < nCount - 1; i++)
			if (lStartDraw < 0) lStartDraw = 0;
			for (int i = lStartDraw ; i < nCount; i++)
			// 2020.12.20 ltg End
// 2020.09.19 ltg End
			{
				gradeRight = (LONG) floor((lpDrawPoints[i].point.x - nLeftMargin) /
					(nRightMargin - nLeftMargin));
				
				left = lpDrawPoints[i].point.x - nLeftMargin;
				bNoDraw = lpDrawPoints[i].bNoDraw;
				
				if (left < 0)
					gradeLeft = (LONG) floor(abs(left) /
					(nRightMargin - nLeftMargin)) + 1;
				
				if ((gradeRight >= 1) || (left < 0))  // 曲线越右界,曲线越左界
				{
					if (gradeRight >= 1)
						nConvertX = lpDrawPoints[i].point.x -
						gradeRight * (nRightMargin - nLeftMargin);
					else
						nConvertX = lpDrawPoints[i].point.x +
						gradeLeft * (nRightMargin - nLeftMargin);
				}
				else	 //曲线正常
					nConvertX = lpDrawPoints[i].point.x;
				
				nDistance = abs(lpDrawPoints[i].point.x - lpDrawPoints[i + 1].point.x); 
				nWidth = abs(nRightMargin - nLeftMargin); 
				
				// 曲线由正常(0)变为越右界(1)，则先画与右边界的交点，再画与左边界的交点
				// 曲线由越左界(0)变为正常(1)，则先画右边界的交点，再画与左边界的交点
				if ((gradeRight > oldGradeRight) || (oldLeft < 0 && left >= 0))  //
				{
					int nCount = 0;				
					float VarifyY = 0.0f;
					float IncreamY = 0.0f;
					int tempX = 0;
					int tempY = 0;
					OutSegment = abs(oldValue + nDistance - nRightMargin);//出界段的大小				
					
					// 由三角形等比关系求出越界时当前点距边界点Y的可变增量以及每次越界时距产生Y方向的不变增量,
					// (由于线每次越界折回后与原先的线平行,故增量不变.)
					tempX = nRightMargin - oldValue;// tempX为前一点距越界边界的距离
					
					if (nDistance == 0)
						VarifyY = 0.0f;
					else
						VarifyY = (tempX * 1.0f / nDistance) * (lpDrawPoints[i].point.y -
						lpDrawPoints[i +1].point.y);//可变增量
					
					if (tempX == 0)
						IncreamY = 0;
					else
						IncreamY = VarifyY / tempX * nWidth; // 两个边界之间的落差(不变增量):与可变增量有关.(三角关系可推)width/temp的比值
					
					// 左右边界
					tempLeftMargin = pInfo->CoordinateConvertX(nLeftMargin);
					tempRightMargin = pInfo->CoordinateConvertX(nRightMargin);
					
					tempY = oldPoint.point.y + VarifyY;				
					
					// 越界画到越界边界,移到相反的边界.
					nRollCount = 0;
					do
					{
						CPoint ptStart(tempRightMargin, tempY);
						CPoint ptEnd(tempLeftMargin, tempY);
						pInfo->CoordinateCalibrate(ptStart);
						pInfo->CoordinateCalibrate(ptEnd);
						DL.LineTo(ptStart);
						// 2021.06.04 ltg Start
						int iDensityWidth = 13;
						switch (nDensity)
						{
						case 0:
							break;
						case 1:
							DL.MoveTo(rcTrack.left, (int)ptStart.y);
							DL.LineTo(rcTrack.left - iDensityWidth, (int)ptStart.y);
							DL.MoveTo(ptStart);
							break;
						case 2:
							DL.MoveTo(rcTrack.left, (int)tempY);
							DL.LineTo(rcTrack.left + iDensityWidth, (int)ptStart.y);
							DL.MoveTo(ptStart);
							break;
						case 3:
							DL.MoveTo(rcTrack.right, (int)tempY);
							DL.LineTo(rcTrack.right - iDensityWidth, (int)ptStart.y);
							DL.MoveTo(ptStart);
							break;
						case 4:
							DL.MoveTo(rcTrack.right, (int)tempY);
							DL.LineTo(rcTrack.right + iDensityWidth, (int)ptStart.y);
							DL.MoveTo(ptStart);
							break;
						}						
						// 2021.06.04 ltg End
						DL.MoveTo(ptEnd);
						tempY += IncreamY;
						if (nCount > 0)
							OutSegment -= nWidth;
						nCount++;
						nRollCount++;
					}
					while (OutSegment > nWidth && nRollCount < pCurve->GetMaxRollCount());
				}
				// 如果曲线由越右界(1)变为正常(0)，先画与左边界的交点，再画与右边界的交点
				// 如果曲线由正常(1)到越左边界(0)，先画与左边界的交点，再画与右边界的交点
				else if ((gradeRight < oldGradeRight) ||
					(oldLeft >= 0 && left < 0))//
				{
					int nCount = 0;		
					
					float VarifyY = 0.0f;
					float IncreamY = 0.0f;
					int tempX = 0;
					int tempY = 0;	// 移动后的深度
					
					OutSegment = abs(nLeftMargin - (oldValue - nDistance));//出界段的大小			
					
					// 由三角形等比关系求出越界时当前点距边界点Y的可变增量以及每次越界时距产生Y方向的不变增量,
					// (由于线每次越界折回后与原先的线平行,故增量不变.)
					tempX = abs(nLeftMargin - oldValue);//tempX为前一点距越界边界的距离
					if (nDistance == 0)
						VarifyY = 0;
					else
						VarifyY = (tempX * 1.0f / nDistance) * (lpDrawPoints[i].point.y -
						lpDrawPoints[i +1].point.y);// 可变增量
					
					if (tempX == 0)
						IncreamY = 0;
					else
						IncreamY = VarifyY / tempX * nWidth; // 两个边界之间的落差(不变增量):与可变增量有关.(三角关系可推)width/temp的比值与m_fPrintReviseX无关
					
					//左右边界
					tempLeftMargin = pInfo->CoordinateConvertX(nLeftMargin);
					tempRightMargin = pInfo->CoordinateConvertX(nRightMargin);
					
					tempY = oldPoint.point.y + VarifyY;
					
					//越界画到越界边界,移到相反的边界.
					nRollCount = 0;
					do
					{
						CPoint ptStart(tempLeftMargin, tempY);
						CPoint ptEnd(tempRightMargin, tempY);
						pInfo->CoordinateCalibrate(ptStart);
						pInfo->CoordinateCalibrate(ptEnd);
						DL.LineTo(ptStart);
						// 2021.06.04 ltg Start
						int iDensityWidth = 13;
						switch (nDensity)
						{
						case 0:
							break;
						case 1:
							DL.MoveTo(rcTrack.left, (int)ptStart.y);
							DL.LineTo(rcTrack.left - iDensityWidth, (int)ptStart.y);
							DL.MoveTo(ptStart);
							break;
						case 2:
							DL.MoveTo(rcTrack.left, (int)tempY);
							DL.LineTo(rcTrack.left + iDensityWidth, (int)ptStart.y);
							DL.MoveTo(ptStart);
							break;
						case 3:
							DL.MoveTo(rcTrack.right, (int)tempY);
							DL.LineTo(rcTrack.right - iDensityWidth, (int)ptStart.y);
							DL.MoveTo(ptStart);
							break;
						case 4:
							DL.MoveTo(rcTrack.right, (int)tempY);
							DL.LineTo(rcTrack.right + iDensityWidth, (int)ptStart.y);
							DL.MoveTo(ptStart);
							break;
						}						
						// 2021.06.04 ltg End
						DL.MoveTo(ptEnd);
						tempY += IncreamY;
						if (nCount > 0)
							OutSegment -= nWidth;
						nCount++;
						nRollCount++;
					}
					while (OutSegment > nWidth && nRollCount < pCurve->GetMaxRollCount());
				}
				
				oldGradeRight = gradeRight;
				oldLeft = left;
				oldPoint.point.x = nConvertX;
				oldValue = oldPoint.point.x;
				oldPoint.point.y = lpDrawPoints[i].point.y;
				oldPoint.point.x = pInfo->CoordinateConvertX(oldPoint.point.x); // 座标转换
				
				tempPoint = oldPoint;
				tempPoint.bNoDraw = oldPoint.bNoDraw = bNoDraw;
				pInfo->CoordinateCalibrate(tempPoint.point);	// 座标刻度
				
				// shenkun add, 2003.10.20 - 改正打印机刻度（Y方向缩放）后断线的错误
// 2020.09.19 ltg Start
//				if (i == nCount - 1)
//				{
//					DL.MoveTo(tempPoint.point);
//				}
//				else
//				{
//					DL.LineTo(tempPoint.point);
//				}
				if (tempPoint.bNoDraw)
				{
					DL.MoveTo(tempPoint.point);
				}
				else
				{
					DL.LineTo(tempPoint.point);
					// 2021.06.04 ltg Start
					int iDensityWidth = 13;
					switch (nDensity)
					{
					case 0:
						break;
					case 1:
						DL.MoveTo(rcTrack.left, (int)tempPoint.point.y);
						DL.LineTo(rcTrack.left - iDensityWidth, (int)tempPoint.point.y);
						DL.MoveTo(tempPoint.point);
						break;
					case 2:
						DL.MoveTo(rcTrack.left, (int)tempPoint.point.y);
						DL.LineTo(rcTrack.left + iDensityWidth, (int)tempPoint.point.y);
						DL.MoveTo(tempPoint.point);
						break;
					case 3:
						DL.MoveTo(rcTrack.right, (int)tempPoint.point.y);
						DL.LineTo(rcTrack.right - iDensityWidth, (int)tempPoint.point.y);
						DL.MoveTo(tempPoint.point);
						break;
					case 4:
						DL.MoveTo(rcTrack.right, (int)tempPoint.point.y);
						DL.LineTo(rcTrack.right + iDensityWidth, (int)tempPoint.point.y);
						DL.MoveTo(tempPoint.point);
						break;
					}					
					// 2021.06.04 ltg End
				}

// 2020.09.19 ltg End
				// DL.LineTo(oldPoint);
				
			}
	
		pDC->SelectObject(pOldPen);
		if (pPen)
			delete pPen;
		
		return UL_NO_ERROR;
	}
// 2023.12.09 Start
HRESULT PrintRoll1(ICurve* pCurve, CULPrintInfo* pInfo, LPMS_DRAWPOINT lpDrawPoints, int nCount, int nLeftMargin, int nRightMargin)
{
	// divided by zero
	if (nRightMargin == nLeftMargin)
		return UL_NO_ERROR;
	if (nCount <= 0)
		return UL_NO_ERROR;
	long lStartDraw = 0;
	for (lStartDraw = 0; lStartDraw < nCount - 1; lStartDraw++)
	{
		if (!lpDrawPoints[lStartDraw].bNoDraw)
		{
			break;
		}
	}
	lStartDraw--;
	if (lStartDraw >= nCount - 1)
	{
		return UL_NO_ERROR;
	}

	CRect rcTrack;
	m_pTrack->GetRegionRect(&rcTrack, TRUE);
	rcTrack = pInfo->CoordinateConvert(rcTrack);
	short nDensity = pCurve->GetDataDensity();

	//在随钻测井时当曲线往回运动时，曲线也需要正常绘制，使用数据库后改功能取消
	//将lpDrawPoints中所有数据点排序
	MS_DRAWPOINT ptTemp;
	ptTemp.bNoDraw = false;
// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
	CString strErrorLog;
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
	if (!pCurve->IsRewind())
	{
// 2023.12.09 Start
//		PrintNoRoll(pCurve, pInfo, lpDrawPoints, nCount, nLeftMargin, nRightMargin);
		PrintNoRoll1(pCurve, pInfo, lpDrawPoints, nCount, nLeftMargin, nRightMargin);
		// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
		strErrorLog.Format("\r\nStaticPrint End\r\n");
		m_DebugLogSave.SaveErrorLog(strErrorLog);
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
		return UL_NO_ERROR;
	}

	// 控制曲线回绕次数
	int nRollCount;
	COLORREF crCurve = pCurve->Color();
	CDC* pDC = pInfo->GetPrintDC();
	if (nCount == 1)
	{
		pDC->SetPixel(lpDrawPoints[0].point, crCurve);
		return UL_NO_ERROR;
	}

	unsigned Type[8];
	int w = pCurve->LinePWidth();

	CDashLine DL(*pDC, Type, CDashLine::GetPattern(Type, 0, w, CDashLine::DL_SOLID));

	CPen* pPen = DL.GetRightPen(pCurve->LineStyle(), w, crCurve);
	CPen* pOldPen = pDC->SelectObject(pPen);

	int nDistance, nWidth;
	int OutSegment, oldValue;	// oldValue用来确定出界段长						
	int tempLeftMargin = nLeftMargin;
	int tempRightMargin = nRightMargin;

	MS_DRAWPOINT oldPoint;			// 记录上一点位置
	oldPoint.bNoDraw = false;
	bool bNoDraw = false;
	MS_DRAWPOINT tempPoint;
	tempPoint.bNoDraw = false;
	long oldGradeRight, gradeRight;
	long oldGradeLeft, gradeLeft;
	long oldLeft, left;
	int nConvertX;				// 换算后的x值 

	// 第一个点设置为第一个
	oldPoint = lpDrawPoints[lStartDraw];
	oldPoint.bNoDraw = lpDrawPoints[lStartDraw].bNoDraw;

	oldGradeRight = (LONG)floor((lpDrawPoints[lStartDraw].point.x - nLeftMargin) / (nRightMargin - nLeftMargin));
	oldLeft = lpDrawPoints[lStartDraw].point.x - nLeftMargin;

	if (oldLeft < 0)	// 判断曲线是否越界：left<0则曲线越界
		// 如果越界，求出倍数
		oldGradeLeft = (LONG)floor(abs(oldLeft) / (nRightMargin - nLeftMargin)) + 1;

	if ((oldGradeRight >= 1) || (oldLeft < 0))  //曲线越右界，曲线越左界
	{
		// 换算为x值
		if (oldGradeRight >= 1)	// 越右界
			oldPoint.point.x = lpDrawPoints[lStartDraw].point.x -
			oldGradeRight * (nRightMargin - nLeftMargin);
		else					// 越左界
			oldPoint.point.x = lpDrawPoints[lStartDraw].point.x +
			oldGradeLeft * (nRightMargin - nLeftMargin);
	}
	else	 // 曲线正常
		oldPoint.point.x = lpDrawPoints[lStartDraw].point.x;

	// 根据方向得到X值
	oldPoint.point.x = pInfo->CoordinateConvertX(oldPoint.point.x);
	int iDensityWidth = 13;
	switch (nDensity)
	{
	case 0:
		break;
	case 1:
		DL.MoveTo(rcTrack.left, oldPoint.point.y);
		DL.LineTo(rcTrack.left - iDensityWidth, oldPoint.point.y);
		break;
	case 2:
		DL.MoveTo(rcTrack.left, oldPoint.point.y);
		DL.LineTo(rcTrack.left + iDensityWidth, oldPoint.point.y);
		break;
	case 3:
		DL.MoveTo(rcTrack.right, oldPoint.point.y);
		DL.LineTo(rcTrack.right - iDensityWidth, oldPoint.point.y);
		break;
	case 4:
		DL.MoveTo(rcTrack.right, oldPoint.point.y);
		DL.LineTo(rcTrack.right + iDensityWidth, oldPoint.point.y);
		break;
	}
	oldValue = oldPoint.point.x;
	DL.MoveTo(oldPoint.point);

	if (lStartDraw < 0) lStartDraw = 0;
	for (int i = lStartDraw; i < nCount; i++)
	{
		gradeRight = (LONG)floor((lpDrawPoints[i].point.x - nLeftMargin) /
			(nRightMargin - nLeftMargin));

		left = lpDrawPoints[i].point.x - nLeftMargin;
		bNoDraw = lpDrawPoints[i].bNoDraw;

		if (left < 0)
			gradeLeft = (LONG)floor(abs(left) /
				(nRightMargin - nLeftMargin)) + 1;

		if ((gradeRight >= 1) || (left < 0))  // 曲线越右界,曲线越左界
		{
			if (gradeRight >= 1)
				nConvertX = lpDrawPoints[i].point.x -
				gradeRight * (nRightMargin - nLeftMargin);
			else
				nConvertX = lpDrawPoints[i].point.x +
				gradeLeft * (nRightMargin - nLeftMargin);
		}
		else	 //曲线正常
			nConvertX = lpDrawPoints[i].point.x;

		nDistance = abs(lpDrawPoints[i].point.x - lpDrawPoints[i + 1].point.x);
		nWidth = abs(nRightMargin - nLeftMargin);

		// 曲线由正常(0)变为越右界(1)，则先画与右边界的交点，再画与左边界的交点
		// 曲线由越左界(0)变为正常(1)，则先画右边界的交点，再画与左边界的交点
		if ((gradeRight > oldGradeRight) || (oldLeft < 0 && left >= 0))  //
		{
			int nCount = 0;
			float VarifyY = 0.0f;
			float IncreamY = 0.0f;
			int tempX = 0;
			int tempY = 0;
			OutSegment = abs(oldValue + nDistance - nRightMargin);//出界段的大小				

			// 由三角形等比关系求出越界时当前点距边界点Y的可变增量以及每次越界时距产生Y方向的不变增量,
			// (由于线每次越界折回后与原先的线平行,故增量不变.)
			tempX = nRightMargin - oldValue;// tempX为前一点距越界边界的距离

			if (nDistance == 0)
				VarifyY = 0.0f;
			else
				VarifyY = (tempX * 1.0f / nDistance) * (lpDrawPoints[i].point.y -
					lpDrawPoints[i + 1].point.y);//可变增量

			if (tempX == 0)
				IncreamY = 0;
			else
				IncreamY = VarifyY / tempX * nWidth; // 两个边界之间的落差(不变增量):与可变增量有关.(三角关系可推)width/temp的比值

			// 左右边界
			tempLeftMargin = pInfo->CoordinateConvertX(nLeftMargin);
			tempRightMargin = pInfo->CoordinateConvertX(nRightMargin);

			tempY = oldPoint.point.y + VarifyY;

			// 越界画到越界边界,移到相反的边界.
			nRollCount = 0;
			do
			{
				CPoint ptStart(tempRightMargin, tempY);
				CPoint ptEnd(tempLeftMargin, tempY);
				pInfo->CoordinateCalibrate(ptStart);
				pInfo->CoordinateCalibrate(ptEnd);
				DL.LineTo(ptStart);
				int iDensityWidth = 13;
				switch (nDensity)
				{
				case 0:
					break;
				case 1:
					DL.MoveTo(rcTrack.left, (int)ptStart.y);
					DL.LineTo(rcTrack.left - iDensityWidth, (int)ptStart.y);
					DL.MoveTo(ptStart);
					break;
				case 2:
					DL.MoveTo(rcTrack.left, (int)tempY);
					DL.LineTo(rcTrack.left + iDensityWidth, (int)ptStart.y);
					DL.MoveTo(ptStart);
					break;
				case 3:
					DL.MoveTo(rcTrack.right, (int)tempY);
					DL.LineTo(rcTrack.right - iDensityWidth, (int)ptStart.y);
					DL.MoveTo(ptStart);
					break;
				case 4:
					DL.MoveTo(rcTrack.right, (int)tempY);
					DL.LineTo(rcTrack.right + iDensityWidth, (int)ptStart.y);
					DL.MoveTo(ptStart);
					break;
				}
				DL.MoveTo(ptEnd);
				tempY += IncreamY;
				if (nCount > 0)
					OutSegment -= nWidth;
				nCount++;
				nRollCount++;
			} while (OutSegment > nWidth && nRollCount < pCurve->GetMaxRollCount());
		}
		// 如果曲线由越右界(1)变为正常(0)，先画与左边界的交点，再画与右边界的交点
		// 如果曲线由正常(1)到越左边界(0)，先画与左边界的交点，再画与右边界的交点
		else if ((gradeRight < oldGradeRight) ||
			(oldLeft >= 0 && left < 0))//
		{
			int nCount = 0;

			float VarifyY = 0.0f;
			float IncreamY = 0.0f;
			int tempX = 0;
			int tempY = 0;	// 移动后的深度

			OutSegment = abs(nLeftMargin - (oldValue - nDistance));//出界段的大小			

			// 由三角形等比关系求出越界时当前点距边界点Y的可变增量以及每次越界时距产生Y方向的不变增量,
			// (由于线每次越界折回后与原先的线平行,故增量不变.)
			tempX = abs(nLeftMargin - oldValue);//tempX为前一点距越界边界的距离
			if (nDistance == 0)
				VarifyY = 0;
			else
				VarifyY = (tempX * 1.0f / nDistance) * (lpDrawPoints[i].point.y -
					lpDrawPoints[i + 1].point.y);// 可变增量

			if (tempX == 0)
				IncreamY = 0;
			else
				IncreamY = VarifyY / tempX * nWidth; // 两个边界之间的落差(不变增量):与可变增量有关.(三角关系可推)width/temp的比值与m_fPrintReviseX无关

			//左右边界
			tempLeftMargin = pInfo->CoordinateConvertX(nLeftMargin);
			tempRightMargin = pInfo->CoordinateConvertX(nRightMargin);

			tempY = oldPoint.point.y + VarifyY;

			//越界画到越界边界,移到相反的边界.
			nRollCount = 0;
			do
			{
				CPoint ptStart(tempLeftMargin, tempY);
				CPoint ptEnd(tempRightMargin, tempY);
				pInfo->CoordinateCalibrate(ptStart);
				pInfo->CoordinateCalibrate(ptEnd);
				DL.LineTo(ptStart);
				int iDensityWidth = 13;
				switch (nDensity)
				{
				case 0:
					break;
				case 1:
					DL.MoveTo(rcTrack.left, (int)ptStart.y);
					DL.LineTo(rcTrack.left - iDensityWidth, (int)ptStart.y);
					DL.MoveTo(ptStart);
					break;
				case 2:
					DL.MoveTo(rcTrack.left, (int)tempY);
					DL.LineTo(rcTrack.left + iDensityWidth, (int)ptStart.y);
					DL.MoveTo(ptStart);
					break;
				case 3:
					DL.MoveTo(rcTrack.right, (int)tempY);
					DL.LineTo(rcTrack.right - iDensityWidth, (int)ptStart.y);
					DL.MoveTo(ptStart);
					break;
				case 4:
					DL.MoveTo(rcTrack.right, (int)tempY);
					DL.LineTo(rcTrack.right + iDensityWidth, (int)ptStart.y);
					DL.MoveTo(ptStart);
					break;
				}
				// 2021.06.04 ltg End
				DL.MoveTo(ptEnd);
				tempY += IncreamY;
				if (nCount > 0)
					OutSegment -= nWidth;
				nCount++;
				nRollCount++;
			} while (OutSegment > nWidth && nRollCount < pCurve->GetMaxRollCount());
		}
		oldGradeRight = gradeRight;
		oldLeft = left;
		oldPoint.point.x = nConvertX;
		oldValue = oldPoint.point.x;
		oldPoint.point.y = lpDrawPoints[i].point.y;
		oldPoint.point.x = pInfo->CoordinateConvertX(oldPoint.point.x); // 座标转换

		tempPoint = oldPoint;
		tempPoint.bNoDraw = oldPoint.bNoDraw = bNoDraw;
		pInfo->CoordinateCalibrate(tempPoint.point);	// 座标刻度

		if (tempPoint.bNoDraw)
		{
			DL.MoveTo(tempPoint.point);
		}
		else
		{
			DL.LineTo(tempPoint.point);
			// 2021.06.04 ltg Start
			int iDensityWidth = 13;
			switch (nDensity)
			{
			case 0:
				break;
			case 1:
				DL.MoveTo(rcTrack.left, (int)tempPoint.point.y);
				DL.LineTo(rcTrack.left - iDensityWidth, (int)tempPoint.point.y);
				DL.MoveTo(tempPoint.point);
				break;
			case 2:
				DL.MoveTo(rcTrack.left, (int)tempPoint.point.y);
				DL.LineTo(rcTrack.left + iDensityWidth, (int)tempPoint.point.y);
				DL.MoveTo(tempPoint.point);
				break;
			case 3:
				DL.MoveTo(rcTrack.right, (int)tempPoint.point.y);
				DL.LineTo(rcTrack.right - iDensityWidth, (int)tempPoint.point.y);
				DL.MoveTo(tempPoint.point);
				break;
			case 4:
				DL.MoveTo(rcTrack.right, (int)tempPoint.point.y);
				DL.LineTo(rcTrack.right + iDensityWidth, (int)tempPoint.point.y);
				DL.MoveTo(tempPoint.point);
				break;
			}
		}
	}

	pDC->SelectObject(pOldPen);
	if (pPen)
		delete pPen;

	return UL_NO_ERROR;
}
// 2023.12.09 End
	HRESULT PrintNoRoll(ICurve* pCurve, CULPrintInfo* pInfo, LPMS_DRAWPOINT lpDrawPoints, int nCount, int nLeftMargin, int nRightMargin)
	{
		// divided by zero
		int nLRWidth = abs(nRightMargin - nLeftMargin);
		if (nLRWidth == 0)
			return UL_NO_ERROR;
		
		CDC* pDC = pInfo->GetPrintDC();

		unsigned Type[8];
		int w = pCurve->LinePWidth();
		COLORREF color = pCurve->Color();

		MS_DRAWPOINT oldPoint = lpDrawPoints[0];
		oldPoint.bNoDraw = lpDrawPoints[0].bNoDraw;
		oldPoint.point.x -= nLeftMargin;
		oldPoint.point.y = lpDrawPoints[0].point.y;

		int oldGrade = oldPoint.point.x / nLRWidth;
		if (oldPoint.point.x < 0)
			oldGrade--;
		
		oldPoint.point.x = oldPoint.point.x % nLRWidth;
		if (oldPoint.point.x < 0)
			oldPoint.point.x += nLRWidth;
		else if (oldPoint.point.x == 0) // Critical point
		{
			if (oldGrade < 0)
				oldGrade++;
		}

		oldPoint.point.x += nLeftMargin;

		int nRoll = 0; 
	
		if (nCount == 1)
		{
			if (abs(oldGrade) <= nRoll)
				pDC->SetPixel(oldPoint.point, color);
			return UL_NO_ERROR;
		}

		CDashLine DL(*pDC, Type, CDashLine::GetPattern(Type, 0, w, CDashLine::DL_SOLID));
		// 2021.06.04 ltg Start
		CRect rcTrack;
		m_pTrack->GetRegionRect(&rcTrack, TRUE);
		rcTrack = pInfo->CoordinateConvert(rcTrack);
		short nDensity = pCurve->GetDataDensity();
		// 2021.06.04 ltg End
		CPen* pPen = DL.GetRightPen(pCurve->LineStyle(), w, color);
		CPen* pOldPen = pDC->SelectObject(pPen);

		MS_DRAWPOINT ptPrint;
// 2024.01.15 打印有横线（乱线）问题 Start
		::ZeroMemory(&ptPrint, sizeof(MS_DRAWPOINT));
// 2024.01.15 打印有横线（乱线）问题 End
		ptPrint.bNoDraw = false;
		MS_DRAWPOINT ptLeft, ptRight;
// 2024.01.15 打印有横线（乱线）问题 Start
		::ZeroMemory(&ptLeft, sizeof(MS_DRAWPOINT));
		::ZeroMemory(&ptRight, sizeof(MS_DRAWPOINT));
// 2024.01.15 打印有横线（乱线）问题 End
		ptLeft.bNoDraw = false;
		ptRight.bNoDraw = false;

		ptPrint = oldPoint;
		ptPrint.point.x = pInfo->CoordinateConvertX(ptPrint.point.x);
		pInfo->CoordinateCalibrate(ptPrint.point);
		DL.MoveTo(ptPrint.point);
		// 2021.06.04 ltg Start
		int iDensityWidth = 13;
		switch (nDensity)
		{
		case 0:
			break;
		case 1:
			DL.MoveTo(rcTrack.left, (int)ptPrint.point.y);
			DL.LineTo(rcTrack.left - iDensityWidth, (int)ptPrint.point.y);
			DL.MoveTo(ptPrint.point);
			break;
		case 2:
			DL.MoveTo(rcTrack.left, (int)ptPrint.point.y);
			DL.LineTo(rcTrack.left + iDensityWidth, (int)ptPrint.point.y);
			DL.MoveTo(ptPrint.point);
			break;
		case 3:
			DL.MoveTo(rcTrack.right, (int)ptPrint.point.y);
			DL.LineTo(rcTrack.right - iDensityWidth, (int)ptPrint.point.y);
			DL.MoveTo(ptPrint.point);
			break;
		case 4:
			DL.MoveTo(rcTrack.right, (int)ptPrint.point.y);
			DL.LineTo(rcTrack.right + iDensityWidth, (int)ptPrint.point.y);
			DL.MoveTo(ptPrint.point);
			break;
		}		
		// 2021.06.04 ltg End

		int nLeftPrint = pInfo->CoordinateConvertX(nLeftMargin);
		int nRightPrint = pInfo->CoordinateConvertX(nRightMargin);

		int newGrade;
		MS_DRAWPOINT newPoint;
		newPoint.bNoDraw = false;
		double distance;
		// 2020.12.20 ltg Start
		//for (int i = 1; i < nCount; i++)
		for (int i = 1; i <= nCount; i++)
		// 2020.12.20 ltg End
		{
			newPoint = lpDrawPoints[i];
			newPoint.bNoDraw = lpDrawPoints[i].bNoDraw;
			newPoint.point = lpDrawPoints[i].point;
			if(newPoint.point.y == oldPoint.point.y)
				continue;

			newPoint.point.x -= nLeftMargin;
			newGrade = newPoint.point.x / nLRWidth;
			if (newPoint.point.x < 0)
				newGrade--;
			
			newPoint.point.x = newPoint.point.x % nLRWidth;
			if (newPoint.point.x < 0)
				newPoint.point.x += nLRWidth;
			else if (newPoint.point.x == 0) // Critical point
			{
				if (newGrade < 0)
					newGrade++;
			}

			newPoint.point.x += nLeftMargin;
// 2024.01.15 Start
			if (lpDrawPoints[i].point.x == lpDrawPoints[i - 1].point.x)
				oldGrade = newGrade;
// 2024.01.15 End

			ptPrint = newPoint;
			ptPrint.bNoDraw = newPoint.bNoDraw;
			ptLeft.bNoDraw = newPoint.bNoDraw;
// 2024.01.15 打印有横线（乱线）问题 Start
//			ptPrint.point.x = pInfo->CoordinateConvertX(ptPrint.point.x);
// 2024.01.15 打印有横线（乱线）问题 End
			pInfo->CoordinateCalibrate(ptPrint.point);

			// In same grade, draw directly
			if (newGrade == oldGrade)
			{
// 2020.09.19 ltg Start
// 				if (abs(newGrade) <= nRoll)
// 					DL.LineTo(ptPrint.point);
// 				else
// 					DL.MoveTo(ptPrint.point);
				if (abs(newGrade) <= nRoll && !ptPrint.bNoDraw)
				{
					DL.LineTo(ptPrint.point);
					// 2021.06.04 ltg Start
					int iDensityWidth = 13;
					switch (nDensity)
					{
					case 0:
						break;
					case 1:
						DL.MoveTo(rcTrack.left, (int)ptPrint.point.y);
						DL.LineTo(rcTrack.left - iDensityWidth, (int)ptPrint.point.y);
						DL.MoveTo(ptPrint.point);
						break;
					case 2:
						DL.MoveTo(rcTrack.left, (int)ptPrint.point.y);
						DL.LineTo(rcTrack.left + iDensityWidth, (int)ptPrint.point.y);
						DL.MoveTo(ptPrint.point);
						break;
					case 3:
						DL.MoveTo(rcTrack.right, (int)ptPrint.point.y);
						DL.LineTo(rcTrack.right - iDensityWidth, (int)ptPrint.point.y);
						DL.MoveTo(ptPrint.point);
						break;
					case 4:
						DL.MoveTo(rcTrack.right, (int)ptPrint.point.y);
						DL.LineTo(rcTrack.right + iDensityWidth, (int)ptPrint.point.y);
						DL.MoveTo(ptPrint.point);
						break;
					}					
					// 2021.06.04 ltg End
				}
				else
					DL.MoveTo(ptPrint.point);
// 2020.09.19 ltg End
				oldPoint = newPoint;
// 2024.01.15 Start
 				oldPoint.point = newPoint.point;
// 2024.01.15 End
				continue;
			}

			distance = abs(lpDrawPoints[i].point.x - lpDrawPoints[i - 1].point.x);
// 			ASSERT(distance != 0);

			double dYX = (double)(newPoint.point.y - oldPoint.point.y) / distance;
			double IncreamY = dYX * (double)nLRWidth;
			double nY;
			if (newGrade < oldGrade) // Exceed left region
			{
				nY = (double)oldPoint.point.y + dYX * (double)(oldPoint.point.x - nLeftMargin);
				if (oldGrade > nRoll && newGrade == 0) //进入右边界
				{
					ptRight.point = CPoint(nRightPrint, (int) nY);
					pInfo->CoordinateCalibrate(ptRight.point);
					DL.MoveTo(ptRight.point);
					oldGrade = nRoll;
				}
				else if(oldGrade == 0)				//出左边界
				{
					ptLeft.point = CPoint(nLeftPrint, (int) nY);
					pInfo->CoordinateCalibrate(ptLeft.point);
// 2020.09.19 ltg Start
//					DL.LineTo(ptLeft.point);
					if (ptLeft.bNoDraw)
					{
						DL.MoveTo(ptLeft.point);
					}
					else
					{
						DL.LineTo(ptLeft.point);
						// 2021.06.04 ltg Start
						int iDensityWidth = 13;
						switch (nDensity)
						{
						case 0:
							break;
						case 1:
							DL.MoveTo(rcTrack.left, (int)ptLeft.point.y);
							DL.LineTo(rcTrack.left - iDensityWidth, (int)ptLeft.point.y);
							DL.MoveTo(ptLeft.point);
							break;
						case 2:
							DL.MoveTo(rcTrack.left, (int)ptLeft.point.y);
							DL.LineTo(rcTrack.left + iDensityWidth, (int)ptLeft.point.y);
							DL.MoveTo(ptLeft.point);
							break;
						case 3:
							DL.MoveTo(rcTrack.right, (int)ptLeft.point.y);
							DL.LineTo(rcTrack.right - iDensityWidth, (int)ptLeft.point.y);
							DL.MoveTo(ptLeft.point);
							break;
						case 4:
							DL.MoveTo(rcTrack.right, (int)ptPrint.point.y);
							DL.LineTo(rcTrack.right + iDensityWidth, (int)ptLeft.point.y);
							DL.MoveTo(ptLeft.point);
							break;
						}						
					// 2021.06.04 ltg End
					}
// 2020.09.19 ltg End
				}
			}
			else // Exceed right region
			{
				nY = (double)oldPoint.point.y + dYX * (double)(nRightMargin - oldPoint.point.x);
				if (oldGrade < nRoll && newGrade == 0)//进入左边界
				{
					ptLeft.point = CPoint(nLeftPrint, (int) nY);
					pInfo->CoordinateCalibrate(ptLeft.point);
					DL.MoveTo(ptLeft.point);
					oldGrade = nRoll;
				}
				else if(oldGrade == 0)				//出右边界
				{
					ptRight.point = CPoint(nRightPrint, (int) nY);
					pInfo->CoordinateCalibrate(ptRight.point);
// 2020.09.19 ltg Start
//					DL.LineTo(ptRight.point);
					if (ptRight.bNoDraw)
					{
						DL.MoveTo(ptRight.point);
					}
					else
					{
						DL.LineTo(ptRight.point);
						// 2021.06.04 ltg Start
						int iDensityWidth = 13;
						switch (nDensity)
						{
						case 0:
							break;
						case 1:
							DL.MoveTo(rcTrack.left, (int)ptRight.point.y);
							DL.LineTo(rcTrack.left - iDensityWidth, (int)ptRight.point.y);
							DL.MoveTo(ptRight.point);
							break;
						case 2:
							DL.MoveTo(rcTrack.left, (int)ptRight.point.y);
							DL.LineTo(rcTrack.left + iDensityWidth, (int)ptRight.point.y);
							DL.MoveTo(ptRight.point);
							break;
						case 3:
							DL.MoveTo(rcTrack.right, (int)ptRight.point.y);
							DL.LineTo(rcTrack.right - iDensityWidth, (int)ptRight.point.y);
							DL.MoveTo(ptRight.point);
							break;
						case 4:
							DL.MoveTo(rcTrack.right, (int)ptRight.point.y);
							DL.LineTo(rcTrack.right + iDensityWidth, (int)ptRight.point.y);
							DL.MoveTo(ptRight.point);
							break;
						}						
					// 2021.06.04 ltg End
					}
// 2020.09.19 ltg End
				}
				
			}

			if (newGrade == oldGrade)
			{
// 2020.09.19 ltg Start
// 				if (abs(newGrade) <= nRoll)
// 					DL.LineTo(ptPrint.point);
// 				else
// 					DL.MoveTo(ptPrint.point);
				if (abs(newGrade) <= nRoll && !ptPrint.bNoDraw)
				{
					DL.LineTo(ptPrint.point);
					// 2021.06.04 ltg Start
					int iDensityWidth = 13;
					switch (nDensity)
					{
					case 0:
						break;
					case 1:
						DL.MoveTo(rcTrack.left, (int)ptPrint.point.y);
						DL.LineTo(rcTrack.left - iDensityWidth, (int)ptPrint.point.y);
						DL.MoveTo(ptPrint.point);
						break;
					case 2:
						DL.MoveTo(rcTrack.left, (int)ptPrint.point.y);
						DL.LineTo(rcTrack.left + iDensityWidth, (int)ptPrint.point.y);
						DL.MoveTo(ptPrint.point);
						break;
					case 3:
						DL.MoveTo(rcTrack.right, (int)ptRight.point.y);
						DL.LineTo(rcTrack.right - iDensityWidth, (int)ptPrint.point.y);
						DL.MoveTo(ptPrint.point);
						break;
					case 4:
						DL.MoveTo(rcTrack.right, (int)ptPrint.point.y);
						DL.LineTo(rcTrack.right + iDensityWidth, (int)ptPrint.point.y);
						DL.MoveTo(ptPrint.point);
						break;
					}					
					// 2021.06.04 ltg End
				}
				else
 					DL.MoveTo(ptPrint.point);
// 2020.09.19 ltg End

				oldPoint = newPoint;
				continue;
			}
			
			DL.MoveTo(ptPrint.point);
			oldGrade = newGrade;
			oldPoint = newPoint;
		}
	
		pDC->SelectObject(pOldPen);
		delete pPen;
	
		return UL_NO_ERROR;
	}
	HRESULT PrintNoRoll1(ICurve* pCurve, CULPrintInfo* pInfo, LPMS_DRAWPOINT lpDrawPoints, int nCount, int nLeftMargin, int nRightMargin)
	{
		// divided by zero
		int nLRWidth = abs(nRightMargin - nLeftMargin);
		if (nLRWidth == 0)
			return UL_NO_ERROR;
		long lStartDraw = 0;
		for (lStartDraw = 0; lStartDraw < nCount - 1; lStartDraw++)
		{
			if (!lpDrawPoints[lStartDraw].bNoDraw)
			{
				break;
			}
		}
		if (lStartDraw >= nCount - 1)
		{
			return UL_NO_ERROR;
		}
		CDC* pDC = pInfo->GetPrintDC();

		unsigned Type[8];
		int w = pCurve->LinePWidth();
		COLORREF color = pCurve->Color();
		MS_DRAWPOINT oldPoint = lpDrawPoints[lStartDraw];
		oldPoint.bNoDraw = lpDrawPoints[lStartDraw].bNoDraw;
		oldPoint.point = lpDrawPoints[lStartDraw].point;
		oldPoint.point.x -= nLeftMargin;
		oldPoint.point.y = lpDrawPoints[lStartDraw].point.y;

		int oldGrade = oldPoint.point.x / nLRWidth;
		if (oldPoint.point.x < 0)
			oldGrade--;

		oldPoint.point.x = oldPoint.point.x % nLRWidth;
		if (oldPoint.point.x < 0)
			oldPoint.point.x += nLRWidth;
		else if (oldPoint.point.x == 0) // Critical point
		{
			if (oldGrade < 0)
				oldGrade++;
		}

		oldPoint.point.x += nLeftMargin;

		int nMaxRoll = 0; // abs(pCurve->GetMaxRollCount());
		int nMinRoll = -nMaxRoll;

		if (nCount == 1)
		{
			if (abs(oldGrade) <= nMaxRoll)
				pDC->SetPixel(oldPoint.point, color);
			return UL_NO_ERROR;
		}

		CDashLine DL(*pDC, Type, CDashLine::GetPattern(Type, 0, w, CDashLine::DL_SOLID));
		CRect rcTrack;
// 2024.01.15 打印有横线（乱线）问题 Start
//		m_pTrack->GetRegionRect(&rcTrack, TRUE);
		m_pTrack->GetRegionRect(&rcTrack, FALSE);
// 2024.01.15 打印有横线（乱线）问题 End
		rcTrack = pInfo->CoordinateConvert(rcTrack);
		short nDensity = pCurve->GetDataDensity();
		CPen* pPen = DL.GetRightPen(pCurve->LineStyle(), w, color);
		CPen* pOldPen = pDC->SelectObject(pPen);

		MS_DRAWPOINT ptPrint;
// 2024.01.15 打印有横线（乱线）问题 Start
		::ZeroMemory(&ptPrint, sizeof(MS_DRAWPOINT));
// 2024.01.15 打印有横线（乱线）问题 End
		ptPrint.bNoDraw = false;
		MS_DRAWPOINT ptLeft, ptRight;
// 2024.01.15 打印有横线（乱线）问题 Start
		::ZeroMemory(&ptLeft, sizeof(MS_DRAWPOINT));
		::ZeroMemory(&ptRight, sizeof(MS_DRAWPOINT));
// 2024.01.15 打印有横线（乱线）问题 End
		ptLeft.bNoDraw = false;
		ptRight.bNoDraw = false;

		ptPrint = oldPoint;
// 2024.01.15 打印有横线（乱线）问题 Start
//		ptPrint.point.x = pInfo->CoordinateConvertX(ptPrint.point.x);
// 2024.01.15 打印有横线（乱线）问题 End
		pInfo->CoordinateCalibrate(ptPrint.point);

		int iDensityWidth = 13;
		switch (nDensity)
		{
		case 0:
			break;
		case 1:
			DL.MoveTo(rcTrack.left, (int)ptPrint.point.y);
			DL.LineTo(rcTrack.left - iDensityWidth, (int)ptPrint.point.y);
			//DL.MoveTo(ptPrint.point);
			break;
		case 2:
			DL.MoveTo(rcTrack.left, (int)ptPrint.point.y);
			DL.LineTo(rcTrack.left + iDensityWidth, (int)ptPrint.point.y);
			//DL.MoveTo(ptPrint.point);
			break;
		case 3:
			DL.MoveTo(rcTrack.right, (int)ptPrint.point.y);
			DL.LineTo(rcTrack.right - iDensityWidth, (int)ptPrint.point.y);
			//DL.MoveTo(ptPrint.point);
			break;
		case 4:
			DL.MoveTo(rcTrack.right, (int)ptPrint.point.y);
			DL.LineTo(rcTrack.right + iDensityWidth, (int)ptPrint.point.y);
			//DL.MoveTo(ptPrint.point);
			break;
		}
		DL.MoveTo(ptPrint.point);

		int nLeftPrint = pInfo->CoordinateConvertX(nLeftMargin);
		int nRightPrint = pInfo->CoordinateConvertX(nRightMargin);
// 2024.01.15 打印有横线（乱线）问题 Start
		ptLeft.point.x = nLeftPrint;
		ptRight.point.x = nRightPrint;
// 2024.01.15 打印有横线（乱线）问题 End
// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
		CString strTemp;
		CString strErrorLog;
		strErrorLog.Format("\r\n PrintNoRoll1 Start\r\n回绕:%s;1nCount=%d;lStartDraw=%d;nLeftMargin=%d;nRightMargin=%d\r\n", pCurve->IsRewind() ? "是" : "否", nCount, lStartDraw, nLeftMargin, nRightMargin);
		strErrorLog += "depth,value,y1,x1,bNoDraw,oldGrade,newGrade,nMaxRoll,status\r\n";
		m_DebugLogSave.SaveErrorLog(strErrorLog);
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
		int newGrade = 0;
		MS_DRAWPOINT newPoint;
// 2024.01.15 打印有横线（乱线）问题 Start
		::ZeroMemory(&newPoint, sizeof(MS_DRAWPOINT));
// 2024.01.15 打印有横线（乱线）问题 End
		double distance = 0.0f;
		if (lStartDraw < 0) lStartDraw = 0;
		for (int i = lStartDraw + 1; i < nCount; i++)
		{
// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
			strErrorLog.Format("%d,%f,%d,%d,%d", lpDrawPoints[i].lDepth, lpDrawPoints[i].dValue, lpDrawPoints[i].point.y, lpDrawPoints[i].point.x, lpDrawPoints[i].bNoDraw);
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
			newPoint = lpDrawPoints[i];
			newPoint.bNoDraw = lpDrawPoints[i].bNoDraw;
			newPoint.point = lpDrawPoints[i].point;
			if (newPoint.point.y == oldPoint.point.y)
			{
// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
				strTemp.Format(",%d,%d,continue,if (newPoint.point.y == oldPoint.point.y)\r\n", oldGrade, newGrade);
				strErrorLog += strTemp;
				m_DebugLogSave.SaveErrorLog(strErrorLog); 
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
				continue;
			}

			newPoint.point.x -= nLeftMargin;
			newGrade = newPoint.point.x / nLRWidth;
			if (newPoint.point.x < 0)
				newGrade--;

			newPoint.point.x = newPoint.point.x % nLRWidth;
			if (newPoint.point.x < 0)
				newPoint.point.x += nLRWidth;
			else if (newPoint.point.x == 0) // Critical point
			{
				if (newGrade < 0)
					newGrade++;
			}

			newPoint.point.x += nLeftMargin;
 
		if (lpDrawPoints[i].point.x == lpDrawPoints[i - 1].point.x)
 				oldGrade = newGrade;

			ptPrint = newPoint;
			ptPrint.point = newPoint.point;
			ptPrint.bNoDraw = newPoint.bNoDraw;
			ptLeft.bNoDraw = newPoint.bNoDraw;
			ptRight.bNoDraw = newPoint.bNoDraw;

			ptPrint.point.x = pInfo->CoordinateConvertX(ptPrint.point.x);
			pInfo->CoordinateCalibrate(ptPrint.point);
			// In same grade, draw directly
			if (newGrade == oldGrade)
			{
				if (abs(newGrade) <= nMaxRoll && !ptPrint.bNoDraw)
				{
					DL.LineTo(ptPrint.point);
// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
					strTemp.Format(",%d,%d,LineTo,continue,First if (newGrade == oldGrade) if (abs(newGrade) <= nMaxRoll && !ptPrint.bNoDraw) ptPrint.point.y=%d;ptPrint.point.x=%d;lpDrawPoints[i - 1].point.y=%d;lpDrawPoints[i - 1].point.x=%d;nLeftPrint=%d;nRightPrint=%d\r\n",
						oldGrade, newGrade, ptPrint.point.y, ptPrint.point.x, lpDrawPoints[i - 1].point.y, lpDrawPoints[i - 1].point.x, nLeftPrint, nRightPrint);
					strErrorLog += strTemp;
					m_DebugLogSave.SaveErrorLog(strErrorLog);
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
					int iDensityWidth = 13;
					switch (nDensity)
					{
					case 0:
						break;
					case 1:
						DL.MoveTo(rcTrack.left, (int)ptPrint.point.y);
						DL.LineTo(rcTrack.left - iDensityWidth, (int)ptPrint.point.y);
						DL.MoveTo(ptPrint.point);
						break;
					case 2:
						DL.MoveTo(rcTrack.left, (int)ptPrint.point.y);
						DL.LineTo(rcTrack.left + iDensityWidth, (int)ptPrint.point.y);
						DL.MoveTo(ptPrint.point);
						break;
					case 3:
						DL.MoveTo(rcTrack.right, (int)ptPrint.point.y);
						DL.LineTo(rcTrack.right - iDensityWidth, (int)ptPrint.point.y);
						DL.MoveTo(ptPrint.point);
						break;
					case 4:
						DL.MoveTo(rcTrack.right, (int)ptPrint.point.y);
						DL.LineTo(rcTrack.right + iDensityWidth, (int)ptPrint.point.y);
						DL.MoveTo(ptPrint.point);
						break;
					}
				}
				else
				{
					DL.MoveTo(ptPrint.point);
// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
					strTemp.Format(",%d,%d,MoveTo,continue,First newGrade == oldGrade else continue ptPrint.point.y=%d;ptPrint.point.x=%d;lpDrawPoints[i - 1].point.y=%d;lpDrawPoints[i - 1].point.x=%d MoveTo;nLeftPrint=%d;nRightPrint=%d\r\n",
						oldGrade, newGrade, ptPrint.point.y, ptPrint.point.x, lpDrawPoints[i - 1].point.y, lpDrawPoints[i - 1].point.x, nLeftPrint, nRightPrint);
					strErrorLog += strTemp;
					m_DebugLogSave.SaveErrorLog(strErrorLog);
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
				}
				oldPoint = newPoint;
				oldPoint.point = newPoint.point;
				continue;
			}

			distance = abs(lpDrawPoints[i].point.x - lpDrawPoints[i - 1].point.x);
			// 			ASSERT(distance != 0);

			double dYX = (double)(newPoint.point.y - oldPoint.point.y) / distance;
			double IncreamY = dYX * (double)nLRWidth;
			double nY;
			if (newGrade < oldGrade) // Exceed left region
			{
// 2024.01.15 打印有横线（乱线）问题 Start
//				nY = (double)oldPoint.point.y + dYX * (double)(nLeftMargin - oldPoint.point.x);
				nY = (double)oldPoint.point.y + dYX * (double)(oldPoint.point.x - nLeftMargin);
// 2024.01.15 打印有横线（乱线）问题 End
				if (oldGrade > nMaxRoll) // 上个点在右出右边界
				{
					nY += IncreamY * (oldGrade - nMaxRoll - 1);
					ptRight.point.y = (int)nY;
// 2024.01.15 打印有横线（乱线）问题 Start
					ptLeft.point.y = nY;// CPoint(nLeftPrint, (int)nY);
//					ptLeft.point = CPoint(nLeftPrint, (int)nY);
// 2024.01.15 打印有横线（乱线）问题 End
					pInfo->CoordinateCalibrate(ptRight.point);
					DL.MoveTo(ptRight.point);
// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
					strTemp.Format(",%d,%d,MoveTo,if (newGrade < oldGrade) if (oldGrade > nMaxRoll) ptPrint.point.y=%d;ptPrint.point.x=%d;lpDrawPoints[i - 1].point.y=%d;lpDrawPoints[i - 1].point.x=%d;nLeftPrint=%d;nRightPrint=%d\r\n",
						oldGrade, newGrade, ptPrint.point.y, ptPrint.point.x, lpDrawPoints[i - 1].point.y, lpDrawPoints[i - 1].point.x, nLeftPrint, nRightPrint);
					strErrorLog += strTemp;
					m_DebugLogSave.SaveErrorLog(strErrorLog);
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
					nY += IncreamY;
					oldGrade = nMaxRoll;
				}
				while (newGrade < oldGrade)
				{
					if (oldGrade < nMinRoll)
						break;
					ptLeft.point.y = nY;
					pInfo->CoordinateCalibrate(ptLeft.point);
					if (oldPoint.bNoDraw)
					{
						DL.MoveTo(ptLeft.point);
// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
						strTemp.Format(",%d,%d while (newGrade < oldGrade) if (oldPoint.bNoDraw) ptPrint.point.y=%d;ptPrint.point.x=%d;lpDrawPoints[i - 1].point.y=%d;lpDrawPoints[i - 1].point.x=%d MoveTo;nLeftPrint=%d;nRightPrint=%d\r\n",
							oldGrade, newGrade, ptPrint.point.y, ptPrint.point.x, lpDrawPoints[i - 1].point.y, lpDrawPoints[i - 1].point.x, nLeftPrint, nRightPrint);
						strErrorLog += strTemp;
						m_DebugLogSave.SaveErrorLog(strErrorLog);
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
					}
					else
					{
						DL.LineTo(ptLeft.point);
// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
						strTemp.Format(",%d,%d,LineTo,while (newGrade < oldGrade) else ptPrint.point.y=%d;ptPrint.point.x=%d;lpDrawPoints[i - 1].point.y=%d;lpDrawPoints[i - 1].point.x=%d;nLeftPrint=%d;nRightPrint=%d\r\n",
							oldGrade, newGrade, ptPrint.point.y, ptPrint.point.x, lpDrawPoints[i - 1].point.y, lpDrawPoints[i - 1].point.x, nLeftPrint, nRightPrint);
						strErrorLog += strTemp;
						m_DebugLogSave.SaveErrorLog(strErrorLog);
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
						int iDensityWidth = 25;
						switch (nDensity)
						{
						case 0:
							break;
						case 1:
							DL.MoveTo(rcTrack.left, (int)nY);
							DL.LineTo(rcTrack.left - iDensityWidth, (int)nY);
							DL.MoveTo(nLeftMargin, (int)nY);
							break;
						case 2:
							DL.MoveTo(rcTrack.left, (int)nY);
							DL.LineTo(rcTrack.left + iDensityWidth, (int)nY);
							DL.MoveTo(nLeftMargin, (int)nY);
							break;
						case 3:
							DL.MoveTo(rcTrack.right, (int)nY);
							DL.LineTo(rcTrack.right - iDensityWidth, (int)nY);
							DL.MoveTo(nLeftMargin, (int)nY);
							break;
						case 4:
							DL.MoveTo(rcTrack.right, (int)nY);
							DL.LineTo(rcTrack.right + iDensityWidth, (int)nY);
							DL.MoveTo(nLeftMargin, (int)nY);
							break;
						}
					}
					oldGrade--;
					nY += IncreamY;
				}
			}
			else // Exceed right region
			{
				nY = (double)oldPoint.point.y + dYX * (double)(nRightMargin - oldPoint.point.x);
				if (oldGrade < nMinRoll)//进入左边界
				{
					nY += IncreamY * (nMinRoll - oldGrade - 1);
					ptLeft.point.y = nY;// CPoint(nLeftPrint, (int)nY);
					pInfo->CoordinateCalibrate(ptLeft.point);
					DL.MoveTo(ptLeft.point);
// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
					strTemp.Format(",%d,%d,MoveTo,else // Exceed right region ptPrint.point.y=%d;ptPrint.point.x=%d;lpDrawPoints[i - 1].point.y=%d;lpDrawPoints[i - 1].point.x=%d;nLeftPrint=%d;nRightPrint=%d\r\n",
						oldGrade, newGrade, ptPrint.point.y, ptPrint.point.x, lpDrawPoints[i - 1].point.y, lpDrawPoints[i - 1].point.x, nLeftPrint, nRightPrint);
					strErrorLog += strTemp;
					m_DebugLogSave.SaveErrorLog(strErrorLog);
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
					nY += IncreamY;
					oldGrade = nMinRoll;
				}
				while (newGrade > oldGrade)
				{
					if (oldGrade > nMaxRoll)
						break;
					ptRight.point.y = nY;
					pInfo->CoordinateCalibrate(ptRight.point);
					if (oldPoint.bNoDraw)
					{
						DL.MoveTo(ptRight.point);
// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
						strTemp.Format(",%d,%d,MoveTo,while (newGrade > oldGrade) if (oldPoint.bNoDraw) ptPrint.point.y=%d;ptPrint.point.x=%d;lpDrawPoints[i - 1].point.y=%d;lpDrawPoints[i - 1].point.x=%d;nLeftPrint=%d;nRightPrint=%d\r\n",
							oldGrade, newGrade, ptPrint.point.y, ptPrint.point.x, lpDrawPoints[i - 1].point.y, lpDrawPoints[i - 1].point.x, nLeftPrint, nRightPrint);
						strErrorLog += strTemp;
						m_DebugLogSave.SaveErrorLog(strErrorLog);
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
					}
					else
					{
						DL.LineTo(ptRight.point);
// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
						strTemp.Format(",%d,%d,LineTo,while (newGrade > oldGrade) else ptPrint.point.y=%d;ptPrint.point.x=%d;lpDrawPoints[i - 1].point.y=%d;lpDrawPoints[i - 1].point.x=%d;nLeftPrint=%d;nRightPrint=%d\r\n",
							oldGrade, newGrade, ptPrint.point.y, ptPrint.point.x, lpDrawPoints[i - 1].point.y, lpDrawPoints[i - 1].point.x, nLeftPrint, nRightPrint);
						strErrorLog += strTemp;
						m_DebugLogSave.SaveErrorLog(strErrorLog);
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
						int iDensityWidth = 25;
						switch (nDensity)
						{
						case 0:
							break;
						case 1:
							DL.MoveTo(rcTrack.left, (int)nY);
							DL.LineTo(rcTrack.left - iDensityWidth, (int)nY);
							DL.MoveTo(nRightMargin, (int)nY);
							break;
						case 2:
							DL.MoveTo(rcTrack.left, (int)nY);
							DL.LineTo(rcTrack.left + iDensityWidth, (int)nY);
							DL.MoveTo(nRightMargin, (int)nY);
							break;
						case 3:
							DL.MoveTo(rcTrack.right, (int)nY);
							DL.LineTo(rcTrack.right - iDensityWidth, (int)nY);
							DL.MoveTo(nRightMargin, (int)nY);
							break;
						case 4:
							DL.MoveTo(rcTrack.right, (int)nY);
							DL.LineTo(rcTrack.right + iDensityWidth, (int)nY);
							DL.MoveTo(nRightMargin, (int)nY);
							break;
						}
					}
					oldGrade++;
					nY += IncreamY;
				}
			}
			if (newGrade == oldGrade)
			{
				if (abs(newGrade) <= nMaxRoll && !ptPrint.bNoDraw)
				{
					DL.LineTo(ptPrint.point);
// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
					strTemp.Format(",%d,%d,LineTo,if (newGrade == oldGrade) if (abs(newGrade) <= nMaxRoll && !ptPrint.bNoDraw) ptPrint.point.y=%d;ptPrint.point.x=%d;lpDrawPoints[i - 1].point.y=%d;lpDrawPoints[i - 1].point.x=%d;nLeftPrint=%d;nRightPrint=%d\r\n",
						oldGrade, newGrade, ptPrint.point.y, ptPrint.point.x, lpDrawPoints[i - 1].point.y, lpDrawPoints[i - 1].point.x, nLeftPrint, nRightPrint);
					strErrorLog += strTemp;
					m_DebugLogSave.SaveErrorLog(strErrorLog);
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
					int iDensityWidth = 13;
					switch (nDensity)
					{
					case 0:
						break;
					case 1:
						DL.MoveTo(rcTrack.left, (int)ptPrint.point.y);
						DL.LineTo(rcTrack.left - iDensityWidth, (int)ptPrint.point.y);
						DL.MoveTo(ptPrint.point);
						break;
					case 2:
						DL.MoveTo(rcTrack.left, (int)ptPrint.point.y);
						DL.LineTo(rcTrack.left + iDensityWidth, (int)ptPrint.point.y);
						DL.MoveTo(ptPrint.point);
						break;
					case 3:
						DL.MoveTo(rcTrack.right, (int)ptRight.point.y);
						DL.LineTo(rcTrack.right - iDensityWidth, (int)ptPrint.point.y);
						DL.MoveTo(ptPrint.point);
						break;
					case 4:
						DL.MoveTo(rcTrack.right, (int)ptPrint.point.y);
						DL.LineTo(rcTrack.right + iDensityWidth, (int)ptPrint.point.y);
						DL.MoveTo(ptPrint.point);
						break;
					}
				}
				else
				{
					DL.MoveTo(ptPrint.point);
// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
					strTemp.Format(",%d,%d,MoveTo,if (newGrade == oldGrade) else && !ptPrint.bNoDraw) ptPrint.point.y=%d;ptPrint.point.x=%d;lpDrawPoints[i - 1].point.y=%d;lpDrawPoints[i - 1].point.x=%d;nLeftPrint=%d;nRightPrint=%d\r\n",
						oldGrade, newGrade, ptPrint.point.y, ptPrint.point.x, lpDrawPoints[i - 1].point.y, lpDrawPoints[i - 1].point.x, nLeftPrint, nRightPrint);
					strErrorLog += strTemp;
					m_DebugLogSave.SaveErrorLog(strErrorLog);
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
				}

				oldPoint = newPoint;
				oldPoint.point = newPoint.point;
				continue;
			}
// 2024.01.15 打印有横线（乱线）问题 Start
			if (ptPrint.point.x < nLeftPrint)
			{
				DL.MoveTo(nLeftPrint, ptPrint.point.y);
			}
			else if (ptPrint.point.x > nRightPrint)
			{
				DL.MoveTo(nRightPrint, ptPrint.point.y);
			}
			else
				DL.MoveTo(ptPrint.point);
// 2024.01.15 打印有横线（乱线）问题 End

// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
			strTemp.Format(",%d,%d,MoveTo,ptPrint.point.y=%d;ptPrint.point.x=%d;lpDrawPoints[i - 1].point.y=%d;lpDrawPoints[i - 1].point.x=%d;nLeftPrint=%d;nRightPrint=%d\r\n",
				oldGrade, newGrade, ptPrint.point.y, ptPrint.point.x, lpDrawPoints[i - 1].point.y, lpDrawPoints[i - 1].point.x, nLeftPrint, nRightPrint);
			strErrorLog += strTemp;
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
			oldGrade = newGrade;
			oldPoint = newPoint;
			oldPoint.point = newPoint.point;
// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
			m_DebugLogSave.SaveErrorLog(strErrorLog);
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
		}

		pDC->SelectObject(pOldPen);
		delete pPen;
// 2023.12.09 Start
#ifdef DEBUG_FILE_LOG
		strErrorLog.Format("\r\nPrintNoRoll1 End");
		m_DebugLogSave.SaveErrorLog(strErrorLog);
#else
#endif // DEBUG_FILE_LOG
// 2023.12.09 End
		return UL_NO_ERROR;
	}

public:
	ITrack*		m_pTrack;		// 绘图所在井道的指针，用于获取井道RECT等属性
	GridProp*	m_pGridProp;	// 格线属性
};

#endif
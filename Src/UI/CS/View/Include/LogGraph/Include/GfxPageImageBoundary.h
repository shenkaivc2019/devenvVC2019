#if !defined(AFX_GFXPAGEIMAGE_H__C436A496_9C32_4CA1_B651_156F0C3646C3__INCLUDED_1)
#define AFX_GFXPAGEIMAGE_H__C436A496_9C32_4CA1_B651_156F0C3646C3__INCLUDED_1

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "LineWidthCB.h"
#include "LineStyleCB.h"
#include "GfxPage.h"

// GfxPageImage.h : header file
//

class CCurve;
class CULTool;
/////////////////////////////////////////////////////////////////////////////
// CProoPageImageDlg dialog

class CGfxPageImageBoundary : public CGfxPage
{

// Construction
public:
	CGfxPageImageBoundary(CWnd* pParent = NULL);   // standard constructor
// Dialog Data
	//{{AFX_DATA(CGfxPageImageBoundary)
	enum { IDD = IDD_GFX_PAGE_IMAGEBOUNDARY };
	CComboBox	m_cbColorRank;
	BOOL	m_bDisplay;
	BOOL	m_bPrint;
	BOOL	m_bSave;
	double	m_fLeftValue;
	double	m_fRightValue;
	double	m_fLeftValueMapClr;
	double	m_fRightValueMapClr;
	double	m_fEndPosition;
	double	m_fStartPosition;
	CString	m_strName;
	CString m_strName2;
	CString	m_strUnit;
	CMFCColorButton m_btnLightColor;
	CMFCColorButton m_btnDarkColor;
	UINT	m_lTimeDelay;
	UINT	m_iTimeInter;
	long	m_lDepthOffset;
// 2020.08.19 ltg Start
	long	m_lDrawImgInterval;		// Image 含义是色带高度,含义是不画间隔米数 单位0.1mm
	long	m_lDrawLineInterval;	// 线性曲线,含义是不画间隔米数 单位0.1mm
	CString m_strCurveMarking;
// 2020.08.19 ltg End

	CLineWidthCB		m_cbLineWidth;
	CLineStyleCB		m_cbLineStyle;
	CMFCColorButton	m_btnColor;
	BOOL				m_bRewind;
	BOOL				m_bLineNone;

	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGfxPageImageBoundary)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
// Implementation
protected:
	CString DoubleToString(double f);

	// Generated message map functions
	//{{AFX_MSG(CGfxPageImageBoundary)
	afx_msg void OnChangeEditEndpos();
	afx_msg void OnChangeEditStartpos();
	afx_msg void OnChangeDepthoffset();
	afx_msg void OnKillFocusDepthOffset();
	afx_msg void OnKillFocusLeftValue();
	afx_msg void OnKillFocusLeftValueMapClr();
	afx_msg void OnChangeEditName();
	afx_msg void OnKillFocusRightValue();
	afx_msg void OnKillFocusRightValueMapClr();
	afx_msg void OnChangeEditUnit();
	afx_msg void OnKillfocusEditEndpos();
	afx_msg void OnKillfocusEditStartpos();
	virtual BOOL OnInitDialog();
	afx_msg void OnDarkcolor();
	afx_msg void OnLightcolor();
	afx_msg void OnDestroy();
	afx_msg void OnChangeEditTimeinter();
	afx_msg void OnChangeEditTimedelay();
	afx_msg void OnKillfocusEditTimeinter();
	afx_msg void OnKillfocusEditTimedelay();
	afx_msg void OnColor();
	afx_msg void OnColorr();
	afx_msg void OnChangeDrawImgInterVal();
	afx_msg void OnChangeDrawLineInterVal();
	afx_msg void OnChangeEditAliasName();
	afx_msg void OnChangeMarking();
	afx_msg void OnChangeMinBoundary();
	afx_msg void OnChangeMaxBoundary();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void RefreshData();
	void InitCurve();
	int ChangeColorToNumber(COLORREF crColor);
	COLORREF ChangeNumberToColor(int nValue);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GFXPAGEIMAGE_H__C436A496_9C32_4CA1_B651_156F0C3646C3__INCLUDED_1)






















#if !defined(AFX_GFXPAGECOMMON_H__C974F9D4_285C_4E97_853C_AA4A52D571D1__INCLUDED_)
#define AFX_GFXPAGECOMMON_H__C974F9D4_285C_4E97_853C_AA4A52D571D1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GfxPageCurve.h : header file
//
#include "LineWidthCB.h"
#include "LineStyleCB.h"
#include "GfxPage.h"

class CCurve;
class CGraphWnd;
class CULTool;

/////////////////////////////////////////////////////////////////////////////
// CGfxPageCommon dialog

class CGfxPageCommon : public CGfxPage
{
// Construction
public:
	CGfxPageCommon(CWnd* pParent = NULL);   // standard constructor
	~CGfxPageCommon()
	{
		m_pCurve = NULL;
		m_pGraph = NULL;
		m_pTool = NULL;
	}

// Dialog Data
	//{{AFX_DATA(CGfxPageCommon)
	enum { IDD = IDD_GFX_PAGE_COMMON };
	CLineWidthCB		m_cbLineWidth;
	CLineStyleCB		m_cbLineStyle;
	CComboBox			m_cbValMode;
	CMFCColorButton	m_btnColor;
	CString				m_strName;
	CString             m_strName2;  /// 曲线别名
	CString				m_strUnit;
	BOOL				m_bDisplay;
	BOOL				m_bPrint;
	UINT				m_nLeftGrid;
	UINT				m_nRightGrid;
	double				m_fLeftValue;
	double				m_fRightValue;
	BOOL				m_bRewind;
	long				m_lDepthOffset;
	long				m_lTimeOffset;
	long				m_lDepthInterval;
	BOOL				m_bPrintCurveHead;
	long				m_lTimeInterval;
// 2020.09.11 ltg Start
	long	m_lDrawInterval;	// 如果是Image 含义是色带高度,如果是线性曲线,含义是不画间隔米数 单位0.1mm
// 2020.09.11 ltg End
// 2021.06.06 ltg Start
	CComboBox m_cbDensity;
// 2021.06.06 ltg End
// 2021.11.05 ltg Start
	double				m_dLeftDrawValue;
	double				m_dRightDrawValue;
// 2021.11.05 ltg End
	//}}AFX_DATA
#ifndef _LOGIC
	CComboBox           m_cbFilter;
	CMFCButton			m_btnProperty;
#endif
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGfxPageCommon)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CGfxPageCommon)
	virtual BOOL OnInitDialog();
	afx_msg void OnKillFocusLeftValue();
	afx_msg void OnKillFocusRightValue();
	afx_msg void OnChangeEditLeftgrid();
	afx_msg void OnChangeEditRightgrid();
	afx_msg void OnKillFocusLeftGrid();
	afx_msg void OnKillFocusRightGrid();
	afx_msg void OnSelChangeFilter();
	afx_msg void OnFilterSetting();
// 2020.09.11 ltg Start
	afx_msg void OnChangeDrawInterVal();
// 2020.09.11 ltg End
// 2021.11.05 ltg Start
	afx_msg void OnChangeLeftDrawValue();
	afx_msg void OnKillFocusLeftDrawValue();
	afx_msg void OnChangeRightDrawValue();
	afx_msg void OnKillFocusRightDrawValue();
// 2021.11.05 ltg End
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void	InitCurve();
	void	RefreshData();
	CString DoubleToString(double f);

public:
	int				m_nTrackType;
	CULTool*		m_pTool;
	int             m_nFilterSel;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GFXPAGECOMMON_H__C974F9D4_285C_4E97_853C_AA4A52D571D1__INCLUDED_)

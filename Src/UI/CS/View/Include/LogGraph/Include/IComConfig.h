#ifndef __IComConfig_INTERFACE_DEFINED__
#define __IComConfig_INTERFACE_DEFINED__

#include "ULInterface.h"

#define  CHAR_SIZE       128
#define  RESEVERED_SIZE  512

typedef struct tagTOOLINFO
{
	TCHAR  ChineseName[CHAR_SIZE];
	TCHAR  EnglishName[CHAR_SIZE];
	TCHAR  ManuFactory[CHAR_SIZE];
	TCHAR  Series[CHAR_SIZE] ;
	TCHAR  Type[CHAR_SIZE];
	TCHAR  S_NO[CHAR_SIZE];
	TCHAR  DLLName[CHAR_SIZE];
	TCHAR  CalName[CHAR_SIZE];
	TCHAR  Date[CHAR_SIZE];
	TCHAR  Dependence[_MAX_PATH];
	BYTE   reserved[_MAX_PATH];
}TOOL_INFO;

typedef  CArray <TOOL_INFO, TOOL_INFO&> TOOLARRAY;

// Interface
interface IComConfig : public IUnknown
{
public:
	ULMDWD SetLWLevel(int nLWLevel) = 0;
// 202312.26 Start
	ULMDWD TurnLineWidth(int iWidth, BOOL bValue = TRUE) = 0;
// 202312.26 End
	ULMPTR GetToolArray() = 0;
	ULMLNG GetProp(LPCTSTR pszPage, LPCTSTR pszProp) = 0;
	ULMLNG SetProp(LPCTSTR pszPage, LPCTSTR pszProp , BOOL bValue) = 0;
};

// {78E83048-4403-4e77-932E-69AE0CDC8989}
static const GUID IID_IComConfig = 
{ 0x78e83048, 0x4403, 0x4e77, { 0x93, 0x2e, 0x69, 0xae, 0xc, 0xdc, 0x89, 0x89 } };

#endif
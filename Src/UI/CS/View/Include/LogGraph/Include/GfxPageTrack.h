#if !defined(AFX_GFXPAGETRACK_H__1A397A47_7DB6_4EAC_B8AA_888CE4C751C6__INCLUDED_)
#define AFX_GFXPAGETRACK_H__1A397A47_7DB6_4EAC_B8AA_888CE4C751C6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GfxPageTrack.h : header file
//
#include "LineWidthCB.h"
#include "LineStyleCb.h"
#include "GfxPage.h"

class CSheet;
/////////////////////////////////////////////////////////////////////////////
// CGfxPageTrack dialog

class CGfxPageTrack : public CGfxPage
{
// Construction
public:
	CGfxPageTrack(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CGfxPageTrack)
	enum { IDD = IDD_GFX_PAGE_TRACK };
	CComboBox m_cbDev;
	CComboBox m_cbAzim;
	CString	m_strTrackName;
	double	m_lTrackLeft;
	double	m_lTrackRight;
	CButton m_chkResult;
	CLineWidthCB m_cbBorderLinewidth;
	CLineStyleCB m_cbBorderLinetype;
	CMFCColorButton m_btnBorderLineColor;
	//}}AFX_DATA
protected:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGfxPageTrack)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGfxPageTrack)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeDev();
	afx_msg void OnSelchangeAzim();
	afx_msg void OnChangeTrackName();
	afx_msg void OnKillfocusTrackLeftpos();
	afx_msg void OnKillfocusTrackRightpos();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	afx_msg void OnCheck1();
	DECLARE_MESSAGE_MAP()

public:
	void  SetTrackEdge(int iRight);
	void  InitTrack();
	void  RefreshData();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GFXPAGETRACK_H__1A397A47_7DB6_4EAC_B8AA_888CE4C751C6__INCLUDED_)

// MRTOprt.h: interface for the CMRTOprt class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MRTOPRT_H__0BEDDDEF_A4C1_4409_845D_68A131B06E93__INCLUDED_)
#define AFX_MRTOPRT_H__0BEDDDEF_A4C1_4409_845D_68A131B06E93__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "XMLSettings.h"

class CMRTObject;
class CMRTOprt
{
public:
	CMRTOprt() { }
	CMRTOprt(CMRTObject* pMRTObj) { m_pOprtObjs.Add(pMRTObj); }
	CMRTOprt(CMRTObjArray* pMRTObjs) { m_pOprtObjs.Copy(*pMRTObjs); }
	virtual ~CMRTOprt() {}
	virtual BOOL Do() = 0;
	virtual BOOL Undo() = 0;
	CMRTObjArray m_pOprtObjs;
};

class CMRTAdd : public CMRTOprt
{
public:
	CMRTAdd(CMRTObject* pMRTObj, CMRTObjList* pList,
		CMRTObjList* pAdd) : CMRTOprt(pMRTObj)
	{
		m_pList = pList;
		m_pAdd = pAdd;
	}
	
	CMRTAdd(CMRTObjArray* pMRTObjs, CMRTObjList* pList,
		CMRTObjList* pAdd) : CMRTOprt(pMRTObjs)
	{
		m_pList = pList;
		m_pAdd = pAdd;
	}
	
	virtual BOOL Do();
	virtual BOOL Undo();
	CMRTObjList* m_pList;
	CMRTObjList* m_pAdd;
};

class CMRTDel : public CMRTOprt
{
public:
	CMRTDel(CMRTObjArray* pMRTObjs, CMRTObjList* pList,
		CMRTObjList* pRemove) : CMRTOprt(pMRTObjs)
	{
		m_pList = pList;
		m_pRemove = pRemove;
		m_lstCopy.AddTail(m_pList);
	}

	virtual BOOL Do();
	virtual BOOL Undo();
	CMRTObjList* m_pList;
	CMRTObjList* m_pRemove;
	CMRTObjList m_lstCopy;
};

class CMRTMove : public CMRTOprt
{
public:
	CMRTMove() {}
	CMRTMove(CMRTObject* pMRTObj, LPRECT lpRect);
	CMRTMove(CMRTObjArray* pMRTObjs, CArray<CRect*, CRect*>* pRects);
	virtual ~CMRTMove();
	virtual BOOL Do();
	virtual BOOL Undo();
	CArray<CRect*, CRect*> m_pRects;
};

class CMRTGroup : public CMRTOprt
{
public:
	CMRTGroup(CMRTObjArray* pMRTObjs, CMRTObjList* pList, CMRTObjList* pAdd) : CMRTOprt(pMRTObjs)
	{ 
		m_pList = pList;
		m_pAdd = pAdd;
		m_pGroup = NULL;
		m_lstCopy.AddTail(m_pList);
	}
	virtual ~CMRTGroup();

	virtual BOOL Do();
	virtual BOOL Undo();
	
	CMRTObjList* m_pList;
	CMRTObjList* m_pAdd;
	CMRTObjList m_lstCopy;
	CMRTObject* m_pGroup;
};

class CMRTUngroup : public CMRTOprt
{
public:
	CMRTUngroup(CMRTObjArray* pMRTObjs, CMRTObjList* pList, CMRTObjList* pGroup) : CMRTOprt(pMRTObjs)
	{ 
		m_pList = pList;
		m_pGroup = pGroup;
		m_lstCopy.AddTail(m_pList);
	}
	virtual BOOL Do();
	virtual BOOL Undo();
	CMRTObjList* m_pList;
	CMRTObjList* m_pGroup;
	CMRTObjList m_lstChildren;
	CMRTObjList m_lstCopy;
};

class CMRTProp : public CMRTOprt
{
public:
	CMRTProp(CMRTObject* pMRTObj, DWORD dwProp);
	virtual BOOL Do();
	virtual BOOL Undo();
	DWORD m_dwProp;
	CXMLSettings m_xml;
};

class CMRTReorder : public CMRTOprt
{
public:
	CMRTReorder(CMRTObjArray* pMRTObjs, CMRTObjList* pList, long lOrder) : CMRTOprt(pMRTObjs)
	{
		m_pList = pList;
		m_lOrder = lOrder;
		m_lstCopy.AddTail(m_pList);
	}

	~CMRTReorder() {}
	virtual BOOL Do();
	virtual BOOL Undo();
	CMRTObjList* m_pList;
	CMRTObjList m_lstCopy;
	long m_lOrder;
};

#endif // !defined(AFX_MRTOPRT_H__0BEDDDEF_A4C1_4409_845D_68A131B06E93__INCLUDED_)

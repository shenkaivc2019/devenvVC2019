// Marker.h: interface for the CMarker class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MARKER_H__3B8D06EF_5249_4CB6_A114_8EAA8D6E9B5F__INCLUDED_)
#define AFX_MARKER_H__3B8D06EF_5249_4CB6_A114_8EAA8D6E9B5F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "mark.h"

class CMark;
class CSheet;
class CCurve;
class CTrack;
class CULPrintInfo;
class CXMLSettings;
class CGraphWnd;
//------------------------
// 标记器
//------------------------

class CMarker  
{
public:
	CMarker();
	CMarker(CSheet* pSheet);
	virtual ~CMarker();

public:
	int			AddMark(CMark* pMark);
	int			AddMark(CString strName, CCurve* pCurve, long lDepth, int nShape = 0, int nPointer = 0);
	void		AddMark(CTrack* pTrack, long lDepth, int nCollarIndex = -1, int nStdCollarIndex = -1, 
		                  int nLineStyle = MARKS_PERFORATION_COLLOR, long lDepthAdjust = 0);
	BOOL		ModifyMark(long lDepth,int nCollarIndex, int nStdCollarIndex);
	int         AddNullMark(long lDepth);
	int			GetMark(CString strName);
	CRect       GetMarkRect(int nMark);
	//void		DeleteMark(CString strName , long fDepth , int nTrackNO);
	void        DeleteMark(CMark* pMark);
	void		ResetMark();
	void        Serialize(CXMLSettings& xml);
	void		CopyTo(CTrack* pTrack);

	int			GetMarkShape(int nMark);
	void		SetMarkShape(int nMark, int nShape);

	COLORREF	GetMarkColor(int nMark);
	void		SetMarkColor(int nMark, COLORREF clr);

	void		DrawMark(CDC* pDC, long lStartDepth, long lEndDepth);
	void        InitMarkerDepth();
	void        AdjustMarksDepth(long lAdjustDepth);


	//
	// 打印标签
	//
	void		PrintMark(CULPrintInfo* pInfo);
	BOOL DeleteMark(CString& strName);

public:
	CMark*      HitTest(CPoint pt, CCurve* pCurve);
	CRect		MarkInRect(CMark * pMark);
	int			GetMarkSize();

public:
	CGraphWnd*	m_pGraph;
	CSheet*		m_pSheet;		// 当前相关页面
	CTrack*		m_pTrack;		// 井道
	CPtrArray	m_MarkList;		// 标记链表
	long        m_lMinDepth;    // NullMark的最小值
	long        m_lMaxDepth;    // NullMark的最大值
};

#endif // !defined(AFX_MARKER_H__3B8D06EF_5249_4CB6_A114_8EAA8D6E9B5F__INCLUDED_)

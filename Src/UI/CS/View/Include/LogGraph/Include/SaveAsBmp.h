#ifndef _SAVE_AS_BMP_H_
#define _SAVE_AS_BMP_H_

class CMemDC;
enum ImageFileTypes {
	JPEGFileType,
		BMPFileType,
		TIFFFileType,
		GIFFileType,
		OtherFileType,
		UnknownFileType
};

ImageFileTypes DetermineFileType(LPCTSTR filename);
BOOL WriteDCToDIB(LPCTSTR szFile, CDC* pDC);
BOOL WriteWindowToDIB(LPCTSTR szFile, CWnd* pWnd);
BOOL WriteWindowToDC(CDC* pDC, LPRECT lpRect, CWnd* pWnd);
BOOL WriteWindowToDC(CDC* pDC, LPRECT lpRect, CWnd* pWnd, CRect rcWnd);
BOOL WriteClientToDC(CDC* pDC, LPRECT lpRect, CWnd* pWnd);
HANDLE BitmapToDIB(HBITMAP hBitmap, HPALETTE hPal);
HANDLE DDBToDIB(CBitmap& bitmap, DWORD dwCompression, CPalette* pPal, CDC* pDC = NULL);
BOOL WriteDIB(LPCTSTR szFile, HANDLE hDIB);

BOOL WriteBitmap(LPCTSTR pszFile, CBitmap& bitmap);
BOOL DrawBitmap(CDC* pDC, CBitmap& bitmap, CRect rect);
BOOL DrawTransparent(CDC* pDC, CBitmap& bitmap, CRect rect, COLORREF clrTransparent);
BOOL PrintBitmap(CDC* pDC, CBitmap& bitmap, CRect& rect, LONG rop = SRCCOPY);

#ifdef _TIFFSUPPORT
int WriteTIF(LPCTSTR szFile, LPBITMAPINFOHEADER lpbi, LPBYTE pBits);
#endif

#endif
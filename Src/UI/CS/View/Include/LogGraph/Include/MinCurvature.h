#ifndef __MINCURVATURE_H__
#define __MINCURVATURE_H__


class AFX_EXT_CLASS CMinCurvature  
{
public:
	CMinCurvature();
	virtual ~CMinCurvature();
	
	double CalcDepthDiff(double fDepth1, double fDepth2, double fAngleDEV1, double fAngleDEV2, double fAngleAZIM1, double fAngleAZIM2);

};


#endif

#if !defined(AFX_WIZARDTEAMPAGE_H__29ADED6D_0E4D_4A0F_90BB_2C6CF0B6B50D__INCLUDED_)
#define AFX_WIZARDTEAMPAGE_H__29ADED6D_0E4D_4A0F_90BB_2C6CF0B6B50D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "WPropertyPage.h"
// WizardTeamPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CWizardTeamPage dialog

class CWizardTeamPage : public CWPropertyPage
{
	DECLARE_DYNCREATE(CWizardTeamPage)

// Construction
public:
	CWizardTeamPage();
	~CWizardTeamPage();

// Dialog Data
	//{{AFX_DATA(CWizardTeamPage)
	enum { IDD = IDD_WIZARD_STEP3_TEAM };
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CWizardTeamPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CWizardTeamPage)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WIZARDTEAMPAGE_H__29ADED6D_0E4D_4A0F_90BB_2C6CF0B6B50D__INCLUDED_)

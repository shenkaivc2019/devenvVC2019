#if !defined(AFX_WELLPROPSHEET_H__57958FE4_E163_4640_A7CA_2AD86E87C069__INCLUDED_)
#define AFX_WELLPROPSHEET_H__57958FE4_E163_4640_A7CA_2AD86E87C069__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ULCOMMDEF.h"
// 2020.2.10 Ver1.6.0 TASK��002�� Start
//#include "PropertyPageWellsite.h"
//#include "PropertyPageWell.h"
//#include "PropertyPageHole.h"
//#include "PropertyPageRun.h"
//#include "WizardStepOne.h"
//#include "WizardWellPage.h"
//#include "WizardDrillPage.h"
//#include "WizardMudPage.h"
//#include "WizardRunPage.h"
//#include "WizardTeamPage.h"

/*
#ifdef _PERFORATION
#include "PerforationPropDataEdit.h"
#endif*/

// WellPropSheet.h : header file
//
#define PAGE_WELLSITE		1
#define PAGE_WELL			2
#define PAGE_HOLE			3
#define	PAGE_RUN			4
// 2020.2.10 Ver1.6.0 TASK��002�� End

/////////////////////////////////////////////////////////////////////////////
// CWellPropSheet

class CWellPropSheet : public CPropertySheet
{
	DECLARE_DYNAMIC(CWellPropSheet)

// Construction
public:
	CWellPropSheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CWellPropSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);

// Attributes
public:
// 2020.2.10 Ver1.6.0 TASK��002�� Start
//	CPropertyPageWellsite		m_pageWellsiteInfo;
//	CPropertyPageWell			m_pageWellInfo;
//	CPropertyPageHole			m_pageHoleInfo;
//	CPropertyPageRun			m_pageRunInfo;
//	CWizardWellPage			m_pageWellField;	// ����ҳǩ
//	CWizardDrillPage		m_pageDrilling;		// �꾮ҳǩ
//	CWizardMudPage			m_pageMud;			// �ཬҳǩ
//	CWizardRunPage			m_pageRun;			// �ཬҳǩ
//	CWizardTeamPage			m_pageTeam;			// С��ҳǩ
//#ifdef _PERFORATION
//	CPerforationPropHeader	m_PagePerforationInfo;   // �����Ϣ
//#endif
	
//	CWizardTimePage			m_pageDrillingTime;	// ʱ��ҳǩ
	
//	CWizardDrillerPage		m_pageDriller;		// ��ͷҳǩ
//	CWizardCasedPage		m_pageCasedHole;	// �׹�ҳǩ
	

//	CWizardCaptainPage		m_pageCaptain;		// �ӳ�ҳǩ
//	CWizardOperatorPage		m_pageOperator;		// ����Աҳǩ
//	CWizardEngineerPage		m_pageEngineer;		// ����ʦҳǩ
//	CWizardWitnessPage		m_pageWitness;		// �ලԱҳǩ
// 2020.2.10 Ver1.6.0 TASK��002�� End
	
// Operations
public:
	void InitSheet();
// 2020.2.10 Ver1.6.0 TASK��002�� Start
	void AddSheetPage(DWORD dwParam , DWORD dwType);
// 2020.2.10 Ver1.6.0 TASK��002�� End
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWellPropSheet)
	public:
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CWellPropSheet();

	// Generated message map functions
protected:
	//{{AFX_MSG(CWellPropSheet)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WELLPROPSHEET_H__57958FE4_E163_4640_A7CA_2AD86E87C069__INCLUDED_)

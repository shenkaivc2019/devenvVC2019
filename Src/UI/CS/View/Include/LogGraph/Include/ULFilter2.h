// ULFilter.h: interface for the CULFilter class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ULFILTER_H__D1F440FA_EE38_4C2D_9436_477A87A5FA68__INCLUDED_)
#define AFX_ULFILTER_H__D1F440FA_EE38_4C2D_9436_477A87A5FA68__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "IFilter.h"
#include "IULFilter.h"
#include "FilterParam.h"
#include "ICurve.h"
//#include "ULFile.h"
#include "ULBaseDocInclude.h"
#ifndef DATAEDIT_API
	#ifdef DATAEDIT_EXPORT
		#define DATAEDIT_API __declspec( dllexport )
	#else
		#define DATAEDIT_API __declspec( dllimport )
	#endif
#endif
//typedef struct tagParam 
//{
//	int nSize;
//	double* pParam;
//}Param;

class CCurveInfoPack;//modify by gj 20121212 为修改服务项目中滤波
class CComConfig;
class DATAEDIT_API CULFilter : public IULFilter
{
	DECLARE_ULI(IID_IULFilter, IULFilter)

public:
	CULFilter();
	virtual ~CULFilter();

public:
	//static CComConfig* m_pConfigInfo;
	//static CPtrArray m_arFilters;
	//static int GetFilters();
	//static CULFilter* GetFilter(int nIndex);
	//static CULFilter* GetFilter(LPCTSTR lpszFilterType);
	//static void ClearFilters();
	//inline static int GetCount() { return m_arFilters.GetSize(); }

	ULMBOOL	AddParam(LPCTSTR lpszName, double dValue = 0.0, LPCTSTR lpszDesc = NULL)
	{
		return m_Params.Add(lpszName, dValue, lpszDesc);
	}

	ULMINT	GetParamsCount()
	{
		return m_Params.m_fpList.GetCount();
	}

	ULMTSTR	GetParamName(int nIndex)
	{
		return (LPTSTR)(LPCTSTR)m_Params.GetName(nIndex);
	}
	
	ULMDBL	GetParamValue(int nIndex)
	{
		return m_Params.GetValue(nIndex);
	}
	
	ULMTSTR	GetParamDescription(int nIndex)
	{
		return (LPTSTR)(LPCTSTR)m_Params.GetDescription(nIndex);
	}

	ULMETHOD RemoveAllParams()
	{
		m_Params.ClearParams();
		return UL_NO_ERROR;
	}

	ULMDBL	GetCurveValue(ICurve* pCurve, int nIndex)
	{
		return pCurve->GetValue(nIndex);
	}
	ULMETHOD SetCurveValue(ICurve* pCurve, int nIndex, double fltVal)
	{
		pCurve->SetValue(nIndex, fltVal);
		return UL_NO_ERROR;
	}

	ULMINT	GetCurveValueUB(ICurve* pCurve)
	{
		return pCurve->GetDataUpBound();
	}

	void	Assign(CULFilter& srcFilter);
	
	virtual void Serialize(CXMLSettings& xml);
	
public:
	ULMBOOL	LoadFilter(LPCTSTR strTypeName);
	short	ParamSetting();
	void	ReleaseFilter();
	ULMTSTR GetFilterName() { return (LPTSTR)(LPCTSTR)m_strTypeName; }

	ULMETHOD   Filter(ICurve* pCurve, int nStartPos, int nEndPos);
	ULMETHOD   Filter(double& dValue, BOOL bClear)
	{
		if (m_pIFilter)
			return m_pIFilter->Filter(dValue, bClear);
		return UL_ERROR;
	}
	ULMETHOD	Filter(FILTER_VALUE *pInValue , FILTER_VALUES &vecOut , BOOL bClear)
	{
		if (m_pIFilter)
			return m_pIFilter->Filter(pInValue , vecOut , bClear);
		return UL_ERROR;
	}
	
	ULMETHOD SetTemplate(int nTemplate);//modify by gj 20121212 实现移至.cpp中

	ULMINT GetTemplate();//modify by gj 20121212 实现移至.cpp中

public:
	inline BOOL	SetParamName(int nIndex, LPCTSTR strName)
	{
		return m_Params.SetName(nIndex, strName);
	}

	inline BOOL	SetParamValue(int nIndex, double dValue)
	{
		return m_Params.SetValue(nIndex, dValue);
	}

	inline BOOL	SetParamDescription(int nIndex, LPCTSTR strDes)
	{
		return m_Params.SetDescription(nIndex, strDes);
	}

	void	SetParams();
	short	SetParamTemplate(int nTempl)
	{
		if (m_pIFilter)
			return m_pIFilter->SetParamTemplate(nTempl);
		return UL_NO_ERROR;
	}

	ULMTSTR GetFilterPath();

	// 获取指定曲线指针 2011/9/13 add by bao
	//ULMPTR		GetCurvePtr(LPCTSTR pszName);
	//void		SetULFilePtr(CULFile *pULFile) { m_pULFile = pULFile; }
	void		SetULFilePtr(CDocument *pULFile);

	BOOL	SaveFilter(CFile* pFile, BOOL bRP = FALSE, BOOL bDelete = TRUE);
	BOOL	ReadFilter(CFile* pFile, BOOL bRP = FALSE, BOOL bDelete = TRUE);
	BOOL	LoadFileFilter(LPCTSTR strTypeName);
	BOOL	SaveCurParam(); // 每次打开设置滤波窗口前，保存当前曲线的滤波点数

public:
	HINSTANCE		m_hDllInst;
	CString			m_strTypeName;
	CFilterParams	m_Params;
	CString         m_strFilePath;

public:
	IFilterBase*		m_pIFilter;		// 客户二次开发滤波器指针
	ICurve*         m_pCurve;
	CCurveInfoPack*	m_CurveInfoPack;
	// 获取指定曲线指针
public:
	CDocument* m_pULFile;

	CArray<Param, Param> m_ParamArray;
};

class DATAEDIT_API CULFilterManager
{
public:
	CULFilterManager();
	virtual ~CULFilterManager();

public:
	CComConfig* m_pConfigInfo;
	CPtrArray m_arFilters;
	int GetFilters();
	CULFilter* GetFilter(int nIndex);
	CULFilter* GetFilter(LPCTSTR lpszFilterType);
	void ClearFilters();
	inline int GetCount() { return m_arFilters.GetSize(); }
};

DATAEDIT_API CULFilterManager* AfxGetULFilterManager();

#endif // !defined(AFX_ULFILTER_H__D1F440FA_EE38_4C2D_9436_477A87A5FA68__INCLUDED_)

#if !defined(AFX_GFXPAGEWAVE_H__402D4571_E1F3_4FE7_83E1_E401BAA9D185__INCLUDED_)
#define AFX_GFXPAGEWAVE_H__402D4571_E1F3_4FE7_83E1_E401BAA9D185__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CCurve;
class CULTool;

#include "GfxPage.h"

/////////////////////////////////////////////////////////////////////////////
// CGfxPageWave dialog

class CGfxPageWave : public CGfxPage
{
// Construction
public:
	CGfxPageWave(CWnd* pParent = NULL);   // standard constructor

	void		RefreshData();
	void		InitCurve();
	int			ChangeColorToNumber(COLORREF crColor);
	COLORREF	ChangeNumberToColor(int nValue);

// Dialog Data
	//{{AFX_DATA(CGfxPageWave)
	enum { IDD = IDD_GFX_PAGE_WAVE };
	CListBox m_lbCurveList;
	CComboBox m_cbColorRank;
	CMFCColorButton m_btnCurveColor;
	BOOL	m_bDisplay;
	BOOL	m_bPrint;
	BOOL	m_bSave;
	long	m_lDepthDelay;
	long	m_lDepthDistance;
	long    m_lTimeDistance;
	long	m_lDepthOffset;
	int		m_iEndPosition;
	int		m_iStartPosition;
	double	m_fLowerValue;
	CString	m_strName;
	CString m_strName2;
	CString	m_strUnit;
	double	m_fUpperValue;
	UINT	m_lTimeDelay;
	UINT	m_iTimeInter;
	int     m_nPlotStyle;
	int     m_nDistribution;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGfxPageWave)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL
// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGfxPageWave)
	afx_msg void OnChangeEditEndpos();
	afx_msg void OnChangeEditStartpos();
	afx_msg void OnChangeEditUppervalue();
	afx_msg void OnChangeEditName();
	afx_msg void OnChangeEditLowervalue();
	afx_msg void OnChangeEditUnit();
	afx_msg void OnKillfocusEditUppervalue();
	afx_msg void OnKillfocusEditLowervalue();
	afx_msg void OnKillfocusEditStartpos();
	afx_msg void OnKillfocusEditEndpos();
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	BOOL		IsNumber(CString strNum);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GFXPAGEWAVE_H__402D4571_E1F3_4FE7_83E1_E401BAA9D185__INCLUDED_)

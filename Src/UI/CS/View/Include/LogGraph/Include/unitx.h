// Machine generated IDispatch wrapper class(es) created with ClassWizard
/////////////////////////////////////////////////////////////////////////////
// IUnA wrapper class

class IUnA : public COleDispatchDriver
{
public:
	IUnA() {}		// Calls COleDispatchDriver default constructor
	IUnA(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IUnA(const IUnA& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	double TS(double d);
	double TX(double s);
	CString GetText();
};

class CGridCtrl;
#define UT_METRIC		1
#define	UT_BRITISH		2
#define UT_INTERNATION	4
#define UT_USER			8

/////////////////////////////////////////////////////////////////////////////
// IUnits wrapper class

class IUnits : public COleDispatchDriver
{
public:
	IUnits() {}		// Calls COleDispatchDriver default constructor
	IUnits(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IUnits(const IUnits& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	void InitUnits();
	CString GetPath();
	void SetPath(LPCTSTR lpszNewValue);
	CString GetFormula();
	void SetFormula(LPCTSTR lpszNewValue);
	void SetType(long lType, long lIndex);
	long GetType();
	void LoadCat(LPCTSTR bstrCat);
	void SaveCat(LPCTSTR bstrCat, long bSet);
	void ShowManager(long lStyle);
	void ShowFormula(long lStyle);
	CString FormatLM(LPCTSTR strFormat, long lDepth);
	CString Format(LPCTSTR pszFormat, double value, LPCTSTR pszType, LPCTSTR pszName);
	void DDX_Text(VARIANT* pVDX, long nIDC, double* pVal, LPCTSTR pszType, LPCTSTR pszName, LPCTSTR pszFormat);
	LPDISPATCH GetUnA(LPCTSTR pszType, LPCTSTR pszName);
	CString DUnit();
	void ShowLCDUnit();
	double XValue(double d, LPCTSTR pszType, LPCTSTR pszName);
	double XDepth(double d);
	void RebuildCus(LPCTSTR newVal);
	double TSpeed(double d);
	double TTemp(double d);
	double TTens(double d);
	double TVolt(double d);
	CString XUnit(LPCTSTR pszType, LPCTSTR pszName);
	double SValue(double d, LPCTSTR pszType, LPCTSTR pszName);
	void AddCurveUnit(LPCTSTR pszName, LPCTSTR pszUnit, long bWrite);
	void UpdateMenu(VARIANT* pCmdUI, long lMax);
	double SValueParse(LPCTSTR lpszText, LPCTSTR pszType, LPCTSTR pszName);
	double LMValueParse(LPCTSTR lpszText);
	void SetCat(long lType, LPCTSTR pszCat);
	CString GetCat();
	void DDX_Text2(VARIANT* pVDX, long nIDC, long nIDSTC, double* pVal, LPCTSTR pszType, LPCTSTR pszName, LPCTSTR pszFormat);
	void DDXV_Text(VARIANT* pVDX, long nIDC, double* pVal, LPCTSTR pszType, double minVal, double maxVal, LPCTSTR pszName, LPCTSTR pszFormat);
	void DDXV_Text2(VARIANT* pVDX, long nIDC, long nIDSTC, double* pVal, LPCTSTR pszType, double minVal, double maxVal, LPCTSTR pszName, long nIDPrompt, LPCTSTR pszFormat);
	void DDXVN_Text2(VARIANT* pVDX, long nIDC, long nIDSTC, double* pVal, LPCTSTR pszType, double minVal, double maxVal, LPCTSTR pszName, LPCTSTR pszFormat);
	void DDXIVN_Text2(VARIANT* pVDX, long nIDC, long nIDSTC, double* pVal, LPCTSTR pszType, double minVal, double maxVal, LPCTSTR pszText, LPCTSTR pszFormat);
	double SDepth(double d);
	CString XValue2(double* pVal, LPCTSTR pszType, LPCTSTR pszName);
	long GetLanguage();
	void SetLanguage(long nNewValue);
	CString CalcCurveKb(LPCTSTR pszPath, LPCTSTR pszTu, long* bRecUnit, double* pK, double* pB, double* pAK, double* pAB);
	double EXValue(double dValue, LPCTSTR lpszType, LPCTSTR lpszCurUnit, LPCTSTR lpszRequireUnit);
	LPDISPATCH FindUnA(LPCTSTR pszType, LPCTSTR pszUnit);
	double GetUCValue(LPCTSTR pszName, double d, long bSet);
	CString GetUCUnit(LPCTSTR pszName, long bRaw);

	// Format Extend
	CString FormatLM(UINT nFormatID, long dvalue)
	{
		CString strFormat;
		VERIFY(strFormat.LoadString(nFormatID) != 0);
		return Format(strFormat, dvalue, _T("LMetric"), _T("Depth"));
	}
	
	// Text Extend
	void DDX_Text(CDataExchange* pDX, int nIDC, double& value, LPCTSTR pszType, 
		LPCTSTR pszName = NULL, LPCTSTR pszFormat = _T("%.4lf "))
	{
		DDX_Text((VARIANT*)pDX, nIDC, &value, pszType, pszName, pszFormat);
	}

	void DDXV_Text(CDataExchange* pDX, int nIDC, double& value, LPCTSTR pszType, 
		double minVal, double maxVal, LPCTSTR pszName = NULL, LPCTSTR pszFormat = _T("%.4lf "))
	{
		DDXV_Text((VARIANT*)pDX, nIDC, &value, pszType, minVal, maxVal, pszName, pszFormat);
	}

	// Text Extend (Edit + Static)
	void DDX_Text(CDataExchange* pDX, int nIDC, int nIDSTC, double& value, 
		LPCTSTR pszType, LPCTSTR pszName = NULL, LPCTSTR pszFormat = _T("%.4lf"))
	{
		DDX_Text2((VARIANT*)pDX, nIDC, nIDSTC, &value, pszType, pszName, pszFormat);
	}

	void DDXV_Text(CDataExchange* pDX, int nIDC, int nIDSTC, double& value, 
		LPCTSTR pszType, double minVal, double maxVal, LPCTSTR pszName = NULL, 
		UINT nIDPrompt = AFX_IDP_PARSE_REAL_RANGE, LPCTSTR pszFormat = _T("%.4lf"))
	{
		DDXV_Text2((VARIANT*)pDX, nIDC, nIDSTC, &value, pszType, minVal, maxVal, pszName, nIDPrompt, pszFormat);
	}

	void DDXVN_Text(CDataExchange* pDX, int nIDC, int nIDSTC, double& value, 
		LPCTSTR pszType, double minVal, double maxVal, LPCTSTR pszName, LPCTSTR pszFormat = _T("%.4lf"))
	{
		DDXVN_Text2((VARIANT*)pDX, nIDC, nIDSTC, &value, pszType, minVal, maxVal, pszName, pszFormat);
	}

	void DDXIVN_Text(CDataExchange* pDX, int nIDC, int nIDSTC, double& value, 
		LPCTSTR pszType, double minVal, double maxVal, LPCTSTR pszText, LPCTSTR pszFormat = _T("%.4lf"))
	{
		DDXIVN_Text2((VARIANT*)pDX, nIDC, nIDSTC, &value, pszType, minVal, maxVal, pszText, pszFormat);
	}

	// Depth Extend
	void DDX_Depth(CDataExchange* pDX, int nIDC, long& d, LPCTSTR pszFormat = _T("%.4lf "))
	{
		double dLM = d;
		DDX_Text(pDX, nIDC, dLM, _T("LMetric"), _T("Depth"), pszFormat);
		d = dLM;
	}
	
	void DDXV_Depth(CDataExchange* pDX, int nIDC, long& d, long dMin, long dMax, LPCTSTR pszFormat = _T("%.4lf "))
	{
		double dLM = d;
		DDXV_Text(pDX, nIDC, dLM, _T("LMetric"), dMin, dMax, _T("Depth"), pszFormat);
		d = dLM;
	}	

	// Depth Extend (Edit + Static)
	void DDX_Depth(CDataExchange* pDX, int nIDC, int nIDSTC, long& dvalue, LPCTSTR pszFormat = _T("%.4lf"))
	{
		double dLM = dvalue;
		DDX_Text(pDX, nIDC, nIDSTC, dLM, _T("LMetric"), _T("Depth"), pszFormat);
		dvalue = floor(dLM + .5);
	}
	
	void DDXV_Depth(CDataExchange* pDX, int nIDC, int nIDSTC, long& dvalue, long dMin, long dMax, 
		UINT nIDPrompt = AFX_IDP_PARSE_REAL_RANGE, LPCTSTR pszFormat = _T("%.4lf"))
	{
		double dLM = dvalue;
		DDXV_Text(pDX, nIDC, nIDSTC, dLM, _T("LMetric"), dMin, dMax, _T("Depth"), nIDPrompt, pszFormat);
		dvalue = floor(dLM + .5);
	}

	// GridCtrl Extend
#ifndef _NO_GRIDCTRL
	void DDX_GridText(CDataExchange* pDX, CGridCtrl* pGridCtrl, int nCol, double* pvalue, 
		int nSize, UINT nIDText, LPCTSTR pszType, LPCTSTR pszName = NULL, LPCTSTR pszFormat = _T("%.4lf"));

	void DDX_GridText(CDataExchange* pDX, CGridCtrl* pGridCtrl, int nCol, long* pvalue, 
		int nSize, UINT nIDText, LPCTSTR pszType, LPCTSTR pszName = NULL, LPCTSTR pszFormat = _T("%d"));

	void DDX_GridText(CDataExchange* pDX, CGridCtrl* pGridCtrl, int nCol, int* pvalue, 
		int nSize, UINT nIDText, LPCTSTR pszType, LPCTSTR pszName = NULL, LPCTSTR pszFormat = _T("%d"));
#endif
};

extern IUnits* g_units;
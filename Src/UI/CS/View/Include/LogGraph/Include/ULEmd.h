// ULEmd.h: interface for the CULEmd class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ULEMD_H__BD96014F_FE90_452F_906D_7A263FDD4D22__INCLUDED_)
#define AFX_ULEMD_H__BD96014F_FE90_452F_906D_7A263FDD4D22__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "IEmd.h"
#include "IULEmd.h"
#include "IULFile.h"
#include "XMLSettings.h"
#include "BCGPListCtrl.h"

class CULEmd : public IULEmd  
{
	DECLARE_ULI(IID_IULEmd, IULEmd)

public:
	CULEmd(LPCTSTR pszEmd);
	virtual ~CULEmd();

public:
	ULMPTR	GetICurve(LPCTSTR pszCName);
	ULMPTR  GetOCurve(LPCTSTR pszCName, int nValueType = VT_R4 , int nDimension = 1);
	// 获得输出曲线 (曲线不存在时，自动创建数据类型为nValueType的曲线，存在时返回已有曲线)
	
	// ---------------------------------
	//	获取参数:
	// ---------------------------------
	
	ULMINT	GetParamsCount()
	{
		return m_params.GetSize();
	}

	ULMTSTR	GetParamName(int nIndex)
	{
		return _T("");
	}

	ULMINT  GetParamIndex(LPCTSTR pszPName);

	ULMDBL	GetParamVal(int nIndex);
	ULMTSTR GetParamStr(int nIndex); 		// 获得参数字符值(缺省),不存在时返回空串
	ULMINT  GetParamInt(int nIndex);		// 获得参数整形值(布尔值/枚举值)(缺省),不存在时返回-1
	ULMDBL	GetParamVald(int nIndex, long lDepth);	// 获得指定深度参数双精度浮点值
	ULMTSTR GetParamStrd(int nIndex, long lDepth); 	// 获得指定深度参数字符值
	ULMINT  GetParamIntd(int nIndex, long lDepth);	// 获得指定深度参数整形值(布尔值/枚举值)
	ULMTSTR	GetParamUnit(int nIndex); // 获得参数单位
	ULMTSTR GetParamDesc(int nIndex); // 获得参数描述信息
	ULMDBL	GetParamVal(LPCTSTR pszPName);		// 获得参数双精度浮点值(缺省),不存在时返回0
	ULMTSTR GetParamStr(LPCTSTR pszPName); 		// 获得参数字符值(缺省),不存在时返回空串
	ULMINT  GetParamInt(LPCTSTR pszPName);		// 获得参数整形值(布尔值/枚举值)(缺省),不存在时返回-1
	ULMDBL	GetParamVald(LPCTSTR pszPName, long lDepth);	// 获得指定深度参数双精度浮点值
	ULMTSTR GetParamStrd(LPCTSTR pszPName, long lDepth); 	// 获得指定深度参数字符值
	ULMINT  GetParamIntd(LPCTSTR pszPName, long lDepth);	// 获得指定深度参数整形值(布尔值/枚举值)
	ULMTSTR	GetParamUnit(LPCTSTR pszPName); // 获得参数单位
	ULMTSTR GetParamDesc(LPCTSTR pszPName); // 获得参数描述信息
	
	ULMINT  GetDepthsCount();
	ULMINT  GetDepths(long* pDepths);
	ULMINT  GetParamsVal(double* pValues, int iStart = 0, int nCount = -1);
	ULMINT  GetParamsVald(double* pValues, long lDepth, int iStart = 0, int nCount = -1);

	ULMINT DefICurves(int nCount, CString* pICurves, CString* pRemarks);
	ULMINT DefOCurves(int nCount, CString* pOCurves, CString* pRemarks);
	ULMINT DefParams(int nCount, CString* pParams, double* pValues, CString* pUnits, CString* pDescs);
	ULMETHOD UserProcess()
	{
		m_bUserProcess = TRUE;
		return UL_NO_ERROR;
	}
	
public:
	BOOL LoadEmd(LPCTSTR pszPath, UINT nIndex = (UINT)-1);
	void ReleaseEmd();

	void SaveParamValues();
	void InitParamValues();
	void ClearParamValues();

	BOOL ProcessEmd();
	void UpdateMenu(CCmdUI* pCmdUIx, long lMax);
	void GetICurvesName(CStringList* pList);
	BOOL InitList(CBCGPListCtrl* pList, LPCTSTR pszKey);
	void InitDepths(CListBox* pList);
	int InsertDepth(CListBox* pList, int iDepth);
	int SetDepth(CListBox* pList, int iDepth, long lDepth0, long lDepth1);
	int SetDepth(CListBox* pList, int iDepth, long lDepth0, long lDepth1, DWORD dwSet);
	int DeleteDepth(CListBox* pList, int iDepth);
	void GetDepthPart(int iDepth, long& lDepth0, long& lDepth1, long& lDepthA0, long& lDepthA1);
	void SaveParam(CBCGPListCtrl* pList, int iDepth);
	void InitParam(CBCGPListCtrl* pList, int iDepth);
	void SaveList(CBCGPListCtrl* pList, LPCTSTR pszKey);
	BOOL SaveParams(LPCTSTR pszFile);
	BOOL LoadParams(LPCTSTR pszFile);
	BOOL SaveDefault();
	int GetDepthIndex(long lDepth);
	void GetDepthRange(long& lDepth0, long& lDepth1);
	BOOL UserDefinedProcess();
	ICurve* GetICurve(UINT nIndex);

public:
	BOOL m_bUserProcess;
	BOOL m_bLock;
	IULFile* m_pULFile;

	HMODULE m_hEmd;
	IEmd* m_pEmd;
	CList<IEmd*,IEmd*> m_lstEmd;

	CStringList m_lstFiles;

	CStringList m_lstNames;
	CStringList	m_lstTexts;
	CStringList m_lstEmdFiles;
	CUIntArray  m_arrEmdIndex;

	CString m_strMenu;
	CString m_strPath;
	CString m_strEmd;
	CString m_strDef;

	CArray<long,long> m_depths;
	
	CPtrArray m_params; // param note
	CPtrArray m_values;	// param values list

	CXMLNode* m_pPNode;
	CMapStringToPtr	m_mapInput;
	CMapStringToPtr m_mapOutput;
	CXMLSettings m_xml;
	CXMLSettings m_def;
	int m_iDepth;
	CString m_strSuffix;
};

#endif // !defined(AFX_ULEMD_H__BD96014F_FE90_452F_906D_7A263FDD4D22__INCLUDED_)

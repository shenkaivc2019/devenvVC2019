// BaseMarker.h: interface for the CMarkerSheet class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BASEMARKER_H__2CDAD045_2A5E_402C_B3B0_C777FFE02EC8__INCLUDED_CMarkerSheet)
#define AFX_BASEMARKER_H__2CDAD045_2A5E_402C_B3B0_C777FFE02EC8__INCLUDED_CMarkerSheet

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "BaseMarker.h"
#include "Curve.h"
#include "XMLSettings.h"

class CSheet;
class CMarkerSheet : public CBaseMarker
{

public:
	CMarkerSheet();
	virtual ~CMarkerSheet();
public:
	void       Serialize(CXMLSettings& xml);
	void       AddMark(CBaseMark* pMark);
	void       DeleteMark(CBaseMark* pMark);
	virtual void DrawMark(CDC* pDC, long lStartDepth, long lEndDepth);
	virtual void PrintMark(CULPrintInfo* pInfo, float fMinDepth, float fMaxDepth);
	CBaseMark* HitTest(CPoint pt, CCurve* pCurve);
	CBaseMark* HitTestCurveFlag(CPoint pt, CCurve* pCurve);
	CBaseMark* HitTestCurveFlagText(CPoint pt, CCurve* pCurve);
	CBaseMark* HitTestArrow(CPoint pt, CCurve* pCurve);
	CRect      MarkInRect(CBaseMark * pMark);
	CRect	   MarkInArrow(CBaseMark * pMark);
	void       RemoveAll();
//	void	   DrawCurveFlag(CDC* pDC, long lStartDepth, long lEndDepth);
	int	GetSelectCount() const;
	int GetSize() const;
	CBaseMark* GetAt(int index) const;
	void Select(CBaseMark* obj, BOOL select);
	void SetVirtualSize(CSize size);
	CSize GetVirtualSize() const;
public:
	CSize m_sizeVirtualSize;
	CSheet* m_pSheet;
};

#endif // !defined(AFX_BASEMARKER_H__2CDAD045_2A5E_402C_B3B0_C777FFE02EC8__INCLUDED_CMarkerSheet)

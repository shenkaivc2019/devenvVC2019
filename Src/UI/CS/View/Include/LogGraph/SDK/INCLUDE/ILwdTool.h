/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2014-1-20
 * 平台版本号：     AXP 1.75
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.75
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/

#ifndef __ILWDTOOL__
#define __ILWDTOOL__

interface ILwdTool : IUnknown
{
	virtual short _stdcall SetULLwdTool(void* pULLwdTool) = 0; // 设置ULLwdTool接口
	virtual short _stdcall InitLwdTool() = 0; // 初始化Lwd仪器
	virtual short _stdcall OpenLwdTool() = 0; // 打开Lwd仪器
	virtual short _stdcall CloseLwdTool() = 0; // 关闭Lwd仪器

	
	virtual short _stdcall AnalyzeData() = 0;// 收到仪器返回的数据后调用仪器对应的此函数

	virtual short _stdcall Check() = 0;
};

// {BFE44F95-2272-432b-9BB5-D8AFE050824D}
static const GUID IID_ILwdTool = 
{ 0xbfe44f95, 0x2272, 0x432b, { 0x9b, 0xb5, 0xd8, 0xaf, 0xe0, 0x50, 0x82, 0x4d } };

extern "C" DWORD ULGetVersion();
extern "C" IUnknown* CreateTool();

#endif
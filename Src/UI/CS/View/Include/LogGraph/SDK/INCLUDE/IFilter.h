/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __IFILTER_H__
#define __IFILTER_H__

#include "ULInterface.h"

typedef struct _tagFILTER_VALUE
{
	double fValue;
	LPVOID pData;
	long lDepth;
	long lTime;
}FILTER_VALUE;

#define FILTER_VALUES std::vector<FILTER_VALUE>

// Interfaces
interface IFilterBase : IUnknown
{
	// 基本操作
	ULMETHOD	AdviseFilter(void* pv) = 0;
	ULMETHOD	InitFilter() = 0;
	ULMETHOD	Filter(ICurve* pCurve, int nStartPos, int nEndPos) = 0;
	ULMETHOD	Filter(double& dValue, BOOL bClear) = 0;
	ULMETHOD	Filter(FILTER_VALUE *pInValue , FILTER_VALUES &vecOut , BOOL bClear)=0;
	ULMETHOD	ParamSetting() = 0;
	ULMETHOD	SetParamTemplate(int nTemplate) = 0;
	
};

//// {AD2F26CD-F5FF-4654-BB52-165F02DFC5DE}
//static const IID IID_IFilter = 
//{ 0xad2f26cd, 0xf5ff, 0x4654, { 0xbb, 0x52, 0x16, 0x5f, 0x2, 0xdf, 0xc5, 0xde } };

// {5D88B4FE-6E79-48BF-9FEC-074374CBA4D5}
static const IID IID_IFilterBase =
 { 0x5d88b4fe, 0x6e79, 0x48bf, { 0x9f, 0xec, 0x7, 0x43, 0x74, 0xcb, 0xa4, 0xd5 } };

extern "C" DWORD		ULGetVersion();
extern "C" IUnknown*	CreateFilter();

#endif
﻿// Line.h : main header file for the CDashLine
//
//	The Bresenham function in this file is derived from code from 
//			Jean-Claude Lanz mailto:Jclanz@bluewin.ch
//		and he presumably shares copyright to it
//	Otherwise the copyright belongs to Llew S. Goodstadt 
//		http://www.lg.ndirect.co.uk    mailto:lg@ndirect.co.uk
//		who hereby grants you fair use and distribution rights of the code 
//		in both commercial and non-commercial applications.
//
/////////////////////////////////////////////////////////////////////////////
// CDashLine

#pragma once
#ifndef PLOTENTITY_API
	#ifdef PLOTENTITY‌_EXPORT
		#define PLOTENTITY_API __declspec( dllexport )
	#else	
		#define PLOTENTITY_API __declspec( dllimport )
	#endif
#endif

#define PS_DASHDASHDOT		9
#define PS_DASHDASHDOTDOT	10
#define PS_LONGDASH			11

class PLOTENTITY_API CDashLine
{
public:
	CDashLine(CDC& dc, unsigned* pattern, unsigned count);
	CDashLine(CDC& dc, unsigned style, unsigned m_PenSize, BOOL m_Round );
	~CDashLine();
	void SetPattern(unsigned* pattern, unsigned count);
	void SetPattern( unsigned style, unsigned m_PenSize, BOOL m_Round );
	enum {	DL_SOLID, DL_DASH, DL_DOT, DL_DASHDOT, DL_DASHDOTDOT, DL_DASHDOTDOTDOT, 
			DL_DASH_GAP, DL_DOT_GAP, DL_DASHDOT_GAP, DL_DASHDOTDOT_GAP, DL_DASHDOTDOTDOT_GAP,
			DL_LONGDASH, DL_COUNT}; 
// Returns count of elements (dash/dot and gaps)
// You must be careful to pass in enough memory for pattern
// It is probably safest to always have an array of [8]
	static unsigned GetPattern(unsigned* pattern, bool round, unsigned pensize, unsigned style);
	CPen* GetRightPen(int nStyle, int nPenWidth, COLORREF crColor);

protected:
	CDC& m_DC;
	unsigned int	m_Count;
	unsigned int*	m_Pattern;
	CPoint			m_CurPos;

	void Reset();
	void Bresenham(LONG x, LONG y);

public:
	unsigned int	m_CurPat;
	unsigned int	m_CurStretch;
	void BezierTo(POINT* dest);
	void MoveTo(const POINT& p) {MoveTo(p.x, p.y);}
	void MoveToEx(const POINT& p, unsigned int i, unsigned int s) {MoveToEx(p.x, p.y, i, s);}
	void MoveTo(int x, int y);
	void MoveToEx(int x, int y, unsigned int i, unsigned int s);
	void LineTo(const POINT& p) {LineTo(p.x, p.y);}
	void LineTo(int x, int y);
	//ylm
	void LineTo(int x,int y,int width,COLORREF color);
	//
};

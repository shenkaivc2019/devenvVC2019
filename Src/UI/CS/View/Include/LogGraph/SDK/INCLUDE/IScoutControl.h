/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __ISCOUT_H__
#define __ISCOUT_H__

#include "ULInterface.h"
#include "ULCOMMDEF.H"

//-------------------------------------------------------------
// 监控文件头定义，监控文件中的各种信息定义
//-------------------------------------------------2003.1.11---
typedef struct tagScoutFileHead{
	char chIndicator[24];			// 监视文件标识
	char chProjectName[32];			// 工程名称
	char chWorkSegName[32];			// 工作段名称
	//	char chReserve1[32];		// 预留1
	//	char chReserve2[32];		// 预留2
	//	char chReserve3[32];		// 预留3
}SCOUTFILEHEAD;


//-------------------------------------------------------------
// 仪器状态及状态参数mod by zjp 06.01.10
//----------------------------------------------------------------

typedef struct tagTOOLPARAEVENT{
	char strTool[20];
	char strPara[20];
	double fParaValue;
}TOOLPARAEVENT;

//-------------------------------------------------------------
// 监控事件结构的定义，该事件主要用于UL2000软件各种操作的日志纪录
//-------------------------------------------------2003.1.10---

#define ET_INFO		0
#define ET_WARNING	1
#define ET_ERROR	2

#define ES_INFO		(ET_INFO<<16)
#define ES_WARNING	(ET_WARNING<<16)
#define ES_ERROR	(ET_ERROR<<16)
#define ET_TOTAL	0
#define	ET_LOG		1
#define ET_CAL		2
#define ET_DATAEDIT	3
#define ET_TOOLOP	4
#define ET_SYSERROR	5
#define ET_OTHER	6

typedef struct tagSCOUTEVENT{
	WORD wdSize;				//监控事件结构的大小
	char chName[64];			//产生的监控事件名称
	char chDate[11];			//日期
	char chTime[9];				//时间
	char chOrigination[64];		//事件来源
	long lDepth;				//监控事件产生的深度位置
	TOOLPARAEVENT ToolPara;		//对应仪器的参数信息  mod by zjp 06.01.10	
	WORD wdEventType;			//事件类型
	BYTE btEventType;			//事件发生的状态：正确，错误或警告
	char chEventContent[64];	//事件内容
	char chEventResult[64];		//事件结果
	char chMark[64];			//该监控事件备注
	BYTE nReserved[20];
}SCOUTEVENT; 


// Interfaces
interface IScout : IUnknown
{
	ULMETHOD		OpenFile(LPCSTR lpFileName) = 0;
	ULMETHOD		CloseFile() = 0;
	ULMPTR			GetScoutFileHead() = 0;
	ULMINT			GetScoutSize(int nScoutType = ET_TOTAL) = 0;
	ULMPTR			GetScoutEventItem(int nItem, int nScoutType = ET_TOTAL) = 0;
	ULMETHOD		Create(LPCTSTR lpFileName, LPCTSTR lpProjName) = 0;
	ULMETHOD		AddEventItem(													
						LPCTSTR	lpstrName,			//产生的监控事件名称
						LPCTSTR lpstrOrigination,	//事件来源
						long lDepth,				//监控事件产生的深度位置
						LPCTSTR lpToolName,
						LPCTSTR lpParaName,
						double fParaValue,
						WORD wdEventTpe,			//事件类型	  
						BYTE byEventType,			//事件发生的状态：正确，警告或错误
						LPCTSTR lpstrEventContent,	//事件内容
						LPCTSTR lpstrEventResult,	//事件结果
						LPCTSTR lpstrMark) = 0;		//事件背注
	ULMETHOD		Destroy() = 0;
};

// 接口ID

// {B21A8BC0-BA9A-4ecc-8BA6-BFFC16EA6030}
static const IID IID_IScout = 
{ 0xb21a8bc0, 0xba9a, 0x4ecc, { 0x8b, 0xa6, 0xbf, 0xfc, 0x16, 0xea, 0x60, 0x30 } };

// {254FA599-14EB-46d9-99A0-9796A19C817E}
static const IID IID_IULScout = 
{ 0x254fa599, 0x14eb, 0x46d9, { 0x99, 0xa0, 0x97, 0x96, 0xa1, 0x9c, 0x81, 0x7e } };

interface IULScout : IUnknown
{
	ULMETHOD		AddEventItem(													
		LPCTSTR	lpstrName,				//产生的监控事件名称
		LPCTSTR lpstrOrigination,		//事件来源	  
		BYTE byEventType,				//事件发生的状态：正确 = 0，警告 = 1或错误 = 2
		LPCTSTR lpstrEventContent,		//事件内容
		LPCTSTR lpstrEventResult,		//事件结果
		long lDepth = 0,				//监控事件产生的深度位置
		LPCTSTR lpToolName = NULL,		//	事件产生的仪器名
		LPCTSTR lpParaName = NULL,		//	参数名
		double fParaValue = 0,			//	参数值
		LPCTSTR lpstrMark = NULL) = 0;		//事件备注
};

extern "C" DWORD	ULGetVersion();
extern "C" IUnknown* CreateScout();

#endif
/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2018-12-26
 * 平台版本号：     AXP 1.90
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.95
 *                  创建
 *
 * 版本号：			1.95 正式版
 *                  GetCurTVDDepth()	//获取当前深度点对应的True Vertical Depth
 *
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __IULALGORITHM_H__
#define __IULALGORITHM_H__

#include "ULInterface.h"
#include "ULCOMMDEF.H"

// Interface
interface IULAlgorithm : IUnknown
{
	ULMLNG GetTVDValue(long lDepth) = 0;
};

// {E4F35AA0-98A2-4FCA-8315-DFA888A97EEE}
static const IID IID_IULAlgorithm = 
{ 0xE4F35AA0, 0x98A2, 0x4FCA, { 0x83, 0x15, 0xDF, 0xA8, 0x88, 0xA9, 0x7E, 0xEE } };

#endif
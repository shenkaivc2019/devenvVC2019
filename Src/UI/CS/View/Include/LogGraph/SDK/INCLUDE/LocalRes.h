/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __LOCALRES_H__
#define __LOCALRES_H__

/* Eliminate the resource id conflict

e.g. My.cpp in MyDLL

HINSTANCE g_hInstance = NULL;

static AFX_EXTENSION_MODULE MyDLL = { NULL, NULL };

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("My.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(MyDLL, hInstance))
			return 0;

		g_hInstance = hInstance;

		// ...
*/

extern HINSTANCE g_hInstance;

class CLocalRes
{
public:
	CLocalRes()
	{
		m_hInstOld = AfxGetResourceHandle ();
		ASSERT (m_hInstOld != NULL);

		AfxSetResourceHandle (g_hInstance);
	}

	CLocalRes(HINSTANCE hInstance)
	{
		m_hInstOld = AfxGetResourceHandle ();
		ASSERT (m_hInstOld != NULL);

		AfxSetResourceHandle (hInstance);
	}
	
	virtual ~CLocalRes()
	{
		AfxSetResourceHandle (m_hInstOld);
	}
	
protected:
	HINSTANCE m_hInstOld;
};

#endif
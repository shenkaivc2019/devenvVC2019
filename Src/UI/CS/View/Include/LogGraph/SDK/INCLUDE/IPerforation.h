/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#pragma once
#include "ULInterface.h"

interface IPerforation : public IUnknown
{
	ULMETHOD MarkerMatch(long lDepth, long lTime, float fValue) = 0;
};

//{1A34B636-DDD2-4b8f-BE3A-DCD2097AC830}
static const IID IID_PERFORATION = 
{ 0x1a34b636, 0xddd2, 0x4b8f, {0xbe, 0x3a, 0xdc, 0xd2, 0x9, 0x7a, 0xc8, 0x30 } };


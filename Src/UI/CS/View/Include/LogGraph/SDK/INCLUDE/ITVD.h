// ITVD.h: interface for the IPulse class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IPULSE_H__2203C1D5_30B7_49C8_9C2D_3417B8CC8A1A__INCLUDED_)
#define AFX_IPULSE_H__2203C1D5_30B7_49C8_9C2D_3417B8CC8A1A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ULInterface.h"
#include "DBDefine.h"

// {77C03466-7005-440f-B4C9-B842058DDD3F}
static const GUID IID_ITVD = 
{ 0x77c03466, 0x7005, 0x440f, { 0xb4, 0xc9, 0xb8, 0x42, 0x5, 0x8d, 0xdd, 0x3f } };

interface ITVD : public IUnknown  
{
public:
	virtual short _stdcall Init() = 0;
	virtual short _stdcall SetStart(SURVEY_INFO *pInfo) = 0;
	virtual short _stdcall SetEnd(SURVEY_INFO *pInfo) = 0;
	virtual int _stdcall GetTVD(int nDepth) = 0;
	virtual short _stdcall GetSurvey(SURVEY_INFO *pInfo) = 0;
};

#endif // !defined(AFX_IPULSE_H__2203C1D5_30B7_49C8_9C2D_3417B8CC8A1A__INCLUDED_)

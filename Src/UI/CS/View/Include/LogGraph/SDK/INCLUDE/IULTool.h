/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __IULTOOL_H__
#define __IULTOOL_H__

#include "ULInterface.h"
#include "ULCOMMDEF.H"

#define MUD_MASK_RM			0
#define MUD_MASK_KM			1
#define MUD_MASK_WEIGHT		2
#define MUD_MASK_TYPE		3
// 2019.07.29 add Temp.C Start
// #define MUD_MASK_ARM		4
#define MUD_MASK_TEMP_C		4
// 2019.07.29 add Temp.C End
#define MUD_MASK_TEMP		5
#define RUN_MASK_HOLESIZE	6
#define RUN_MASK_ENDTIME	7
#define RUN_MASK_ENDDEPTH	8

// Interface
interface IULTool : IUnknown
{
	// ---------------------------------
	//	仪器间通信:
	// ---------------------------------

	ULMINT   GetToolCount() = 0;					// 仪器所在仪器串的仪器数
	ULMETHOD GetTools(ULTOOLS& vecTools) = 0; 	    // 所有仪器接口指针链表	
	ULMPTR   GetTool(int nIndex) = 0;				// 据索引返回ULTOOL结构指针
	ULMPTR   GetTool(LPCTSTR lpszToolName) = 0;	    // 据名称返回ULTOOL结构指针
	ULMETHOD ReloadTool(LPCTSTR lpszToolFile) = 0;	// 重新加载指定仪器动态库	

	
	// ---------------------------------
	//	设置仪器属性:
	// ---------------------------------
	ULMETHOD SetToolProperty(int nCount,			 // 仪器个数
							 LPCTSTR lpszName,		 // 仪器名称
							 LPCTSTR lpszENName,	 // 英文名称
							 LPCTSTR lpszCNName,	 // 中文名称
							 LPCTSTR lpszType,		 // 仪器类型
							 float fToolLen,		 // 仪器长度(m)
							 float fBasePoint) = 0;	 // 基本测量点值

	ULMETHOD SetToolInfo(LPCTSTR pszSN, LPCTSTR pszAssetNo, 
		                double Diameter, double fWeight, double fMaxSpeed) = 0;
    ULMETHOD GetToolInfo(LPTSTR strSN, LPTSTR strAssetNo, 
		                double& Diameter, double& fWeight, double& fMaxSpeed) = 0;
	// ---------------------------------
	//	申请通道:
	// ---------------------------------
	ULMPTR   AllocNewChannel(DWORD dwBufferSize,	// 缓冲区大小
							 LPCTSTR lpszType,		// 通道类型		
							 double fDataRate,		// 最小传速率
							 long  lPrecision,		// 精度
							 void* pDataItem ) = 0;	// 数据项指针(NULL)

	// ---------------------------------
	//	添加曲线:
	// ---------------------------------
	ULMPTR	 AddCurve(	LPCTSTR lpszName,		       // 曲线名称
						LPCTSTR lpszUnit,		       // 单位
						double fLeftValue,		       // 最小值
						double fRightValue,		       // 最大值
						long  lDepthOffset,		       // 深度偏移
						int nOffMode = 0,	           // 深度偏移模式
						int nFilterPiont = 0,	       // 滤波点数
						int nFilterType = 2,	       // 滤波类型
						int nDimension = 1,		       // 维数
						int nPointFrame = 1,	       // 每帧点数
						int nCurveMode = CURVE_NORMAL, // 曲线模式，用于射孔取芯
						int nValueType = VT_R4,	       // 曲线数据类型
						LPCTSTR lpszIDName = NULL	   // 曲线别名 
					) = 0;

	ULMPTR	 AddStatusCurve( LPCTSTR lpszName,	            // 曲线名称
						 	 LPCTSTR lpszUnit,	            // 单位
							 double fLeftValue,	            // 左值
							 double fRightValue,	        // 右值
							 int nDimension = 1,	        // 维数	
							 int nPointFrame = 1,           // 每帧点数
							 int nCurveMode = CURVE_STATUS, // 状态曲线
							 int nValueType = VT_R4,        // 数据类型
							 LPCTSTR lpszIDName = NULL      // 别名
					 ) = 0; 

	
	// 设置DataView窗口的曲线显示，pStringArray为CStringArray的指针
	// CStringArray放的是当前仪器所有要显示的曲线
	ULMETHOD CurvesView(void* pStringArray) = 0;    
   
	// 打开曲线，默认是全打开的, 如果调用此函数，未传入的曲线不打开
	// 所谓关闭状态就是界面完全看不到，适用于多功能仪器
	ULMETHOD CurvesOpen(void* pStringArray) = 0;   
	
	// 得到\设置服务项目制作时用户选择的自定义滤波器序号
	ULMINT   GetUserDefineFilterType(int nCurve) = 0;   
	// ---------------------------------
	//	获取相应通道数据缓冲:
	// ---------------------------------

	ULMPTR	 GetChannelBuffer(int nIndex) = 0;	
	ULMPTR	 GetChannelBuffer(LPCTSTR lpszName) = 0;	

	// 仪器申请所有通道的总大小
	ULMINT   GetAllChannelSize() = 0;        
	
	// 自身申请的通道大小      
	ULMINT   GetAllocChannelSize() = 0;  

	// 据名称返回IULTool接口指针
	ULMPTR   GetIULTool(LPCTSTR pszToolName) = 0;
	
	// 据名称返回ITool接口指针
	ULMPTR   GetITool(LPCTSTR pszToolName) = 0;	
            
	// 仪器分组号
	ULMINT   GetGroupIndex() = 0;    
	
	//设置仪器图片
	ULMETHOD SetImagePath(LPCTSTR strPath) = 0;                     
	ULMTSTR  GetSavePath() = 0;

	ULMETHOD AddMeasurePoint(LPCTSTR lpszPoint, long lPoint = 0, BOOL bShow = TRUE) = 0;
	ULMETHOD GroupWithTool(LPCTSTR strToolName) = 0;
	// ---------------------------------------------------
	//	添加曲线数据: 曲线索引/名称，深度，曲线值，时间
	// ---------------------------------------------------
	ULMETHOD AddCurveData(int nCurve, long lDepth, void* pValue, long lTime =0) = 0;
	ULMETHOD AddCurveData(LPCTSTR lpszName, long lDepth, void* pValue, long lTime =0, LPCTSTR lpszIDName = NULL) = 0;

	// -----------------------------------------------------------------------
	//	添加状态曲线数据：曲线索引/名称，深度，曲线值，时间
	// -----------------------------------------------------------------------
	ULMETHOD AddStatusCurveData(int nCurve, void* pValue, int nShowPos = 0) = 0;
	ULMETHOD AddStatusCurveData(LPCTSTR lpszName, void* pValue, int nShowPos = 0, LPCTSTR lpszIDName = NULL) = 0;
	// ---------------------------------
	//	属性操作函数:
	// ---------------------------------

	ULMETHOD GetToolName(LPTSTR lpszToolName) = 0;
	ULMETHOD GetToolDllName(LPTSTR lpszDllName) = 0;
	ULMTSTR  ToolName() = 0;
	ULMTSTR  ToolDllName() = 0;
	ULMETHOD SetCurDepth(long lDepth) = 0;
	ULMLNG	 GetCurDepth() = 0;
	ULMETHOD SetCurTime(long lTime) = 0;
	ULMLNG	 GetCurTime() = 0;

	ULMETHOD SetCurSpeed(double fSpeed) = 0;
	ULMETHOD SetCurVol(double fVol) = 0;
	ULMETHOD SetCurTemp(double fTemp) = 0;
	ULMETHOD SetCurTension(double fTension) = 0;
	
	ULMDBL	 GetCurSpeed() = 0;
	ULMDBL	 GetCurVol() = 0;
	ULMDBL	 GetCurTemp() = 0;
	ULMDBL	 GetCurTension() = 0;	

	// -------------------------------
	//	添加示波器: 类型，名称
	// -------------------------------
	ULMETHOD AddToolScope(int nType, LPCTSTR lpszTitle) = 0;	
	// nOperate的值，参考 ULOscilliscope.h
	ULMBOOL  ScopeSetting(int nOperate, int nParam, LPVOID lpParam = NULL, int nParam2 = 0) = 0; 
	// -------------------------------
	//	添加控制或刻度窗口(对话框):
	// -------------------------------

	ULMETHOD AddUserDlg(void* pDlg, UINT nID, LPCTSTR lpszTitle = NULL) = 0;
	ULMETHOD AddUserCalDlg(void* pDlg, UINT nID) = 0;

	// --------------------------------
	//   添加边界测试函数
	// ---------------------------------
	ULMETHOD SetCurveBounds(int nCurve, double fLeftBound, double fRightBound) = 0;	
	ULMINT   TestCurveBound(int nCurve) = 0;
	ULMETHOD AddMark(int nCurve, LPCTSTR lpszName, long lDepth, DWORD dwColor, int nType) = 0; // nType 0 线 1 三角 2 箭头

	// -----------------------------------------------------------------------
	//	系统刻度操作函数:
	// -----------------------------------------------------------------------
	ULMBOOL	 NeedCalibrate() = 0;
	
	ULMETHOD SetCalType(int nCalibrateType) = 0;
	ULMINT	 GetCalType() = 0;
	ULMETHOD SetCalPhase(UINT uPhase) = 0;
	ULMINT	 GetCalPhase() = 0;

	ULMETHOD AddCalParam(void* pCurveData, void* pSegData, void* pDotData) = 0;
	ULMETHOD AddCalParam(UINT uPhase, void* pCurveData, void* pSegData, void* pDotData) = 0;
	
	ULMETHOD AddCalDotValue(DWORD nIndexHeadCalDot, double dCalDotValue) = 0;
	ULMDWD	 GetCalDotIndexHead(int nIndex) = 0;

	ULMINT	 GetCalCurveNum() = 0;					// 得到当前刻度类型待刻曲线数量
	ULMINT	 GetCalSegNum(int nCurve) = 0;			// 得到曲线刻度段数量	
	ULMINT	 GetCalDotNum(int nCurve, int nSeg) = 0; // 得到曲线段刻度点数量

	ULMINT	 GetCalDotNum() = 0;
	ULMPTR	 GetCalDot(int nIndex) = 0;

	// 刻度系数存取
	ULMETHOD AddDefaultCoef(int nCalPhase, int nCurve, int nSeg, LPCTSTR lpszName, double fCoef) = 0; 
	ULMDBL	 GetCalCoef(int nCurveId, int nSegmentId, int nCoefId) = 0;			
	ULMDBL	 GetCalCoef(int nCalType, int nCurveId, int nSegmentId, int nCoefId) = 0;
	ULMETHOD GetCalValue(int nCurve, int nSeg, int nDot, double& dMeasure, double& dEngineer) = 0;
	ULMETHOD SetCoef(int IndexCurve, int IndexSeg, int nIndexCalCoef, LPCTSTR lpszName, double fCoef) = 0;
	ULMETHOD SetCoef(int nCalType, int IndexCurve, int IndexSeg, int nIndexCalCoef, LPCTSTR lpszName, double fCoef) = 0; 

	//  刻度文件存取
	ULMTSTR	 GetCalFilePath(UINT uPhase) = 0;
	ULMETHOD SetCalFilePath(UINT uPhase, LPCTSTR lpszPath) = 0;
	ULMETHOD SaveCalInfo(CFile* pFile, UINT uCalPhase) = 0;
	ULMETHOD ReadCalInfo(CFile* pFile, UINT uCalPhase) = 0;
		
	// -----------------------------------------------------------------------
	//	自定义刻度操作函数
	// -----------------------------------------------------------------------
	ULMETHOD SetCalUserCurveNum(int nNumb) = 0;

	// 刻度系数存取
	ULMINT	 AddCoefficient(double fCoef, int nCurve) = 0;
	ULMINT	 AddUserDefualtCoef(double fCoef, int nCurve) = 0;			 // 添加一个缺省的刻度系数到曲线
	ULMINT	 AddUserDefualtCoef(double fCoef, int nNum, int nCurve) = 0; // 添加nNum个缺省的刻度系数到曲线
	ULMETHOD SetCoefficient(double fCoef, int nIndex, int nCurve) = 0;	 // 设置nCurve的第nIndex个的系数值
	ULMDBL	 GetCoefficient(int nIndex, int nCurve) = 0;		         // 获取刻度系数数据链表中的系数
	ULMINT	 GetCoefNum(int nCurve) = 0;						         // 据曲线索引获取刻度系数数目

	ULMETHOD EmptyCoefficient() = 0;				// 清空刻度系数数据链表
	ULMETHOD EmptyCoefficient(int nCurve) = 0;	    // 清空nCurve的刻度系数

	ULMETHOD SaveCalFile(CFile* pFile) = 0;
	ULMETHOD ReadCalFile(CFile* pFile) = 0;

	//  自定义刻度文件存取
	ULMETHOD CalUserSave(CFile* pFile, UINT uPhase) = 0;
	ULMETHOD CalUserLoad(CFile* pFile, UINT uPhase) = 0;
	ULMETHOD CalUserSave(CFile* pFile, LPCTSTR strCalPhase) = 0;
	ULMETHOD CalUserLoad(CFile* pFile, LPCTSTR strCalPhase) = 0;	
 
	// 设置某刻度阶段自定义打印
    ULMETHOD EnableCalUserPrint(UINT uPhase) = 0;       

	//  获取仪器属性
	ULMETHOD  GetToolProp(void* pToolProp) = 0;
	
	// ---------------------------------
	//	设置刻度点范围(等同 SetCalDotVarRange):
	// ---------------------------------
	ULMETHOD SetCalDotVarMin(LPCTSTR lpszCurve, LPCTSTR lpszDot, float fMinValue) = 0;
	ULMETHOD SetCalDotVarMax(LPCTSTR lpszCurve, LPCTSTR lpszDot, float fMaxValue) = 0;
	ULMETHOD SetCalDotVarNormal(LPCTSTR lpszCurve, LPCTSTR lpszDot, float fNorValue) = 0;

	// -----------------------------------------------------------------------
	//	获取仪器曲线
	// -----------------------------------------------------------------------
	ULMETHOD GetCurves(CURVES& vecCurve) = 0;
	ULMINT	 GetCurveCount() = 0;
	ULMPTR	 GetCurve(int nIndex) = 0;
	ULMPTR   GetCurve(LPCTSTR lpszCurveName, LPCTSTR lpszIDName = NULL) = 0;
	ULMPTR	 GetStatusCurve(int nIndex) = 0;
	ULMPTR	 GetStatusCurve(LPCTSTR lpszCurveName, LPCTSTR lpszIDName = NULL) = 0;
		
	ULMETHOD GetToolCurves(LPCTSTR lpszToolName, CURVES& vecCurve) = 0;
	ULMINT	 GetToolCurveCount(LPCTSTR lpszToolName) = 0;
	ULMPTR	 GetToolCurve(LPCTSTR lpszToolName, LPCTSTR lpszCurveName, LPCTSTR lpszIDName = NULL) = 0;

	// 系统工程或文件曲线
	ULMETHOD GetULCurves(CURVES& vecCurve, UINT nFile = 0) = 0;
	ULMPTR	 GetULCurve(LPCTSTR lpszCurveName, UINT nFile = 0, LPCTSTR lpszIDName = NULL) = 0;

	// 设置时间驱动
	ULMLNG	 SetTimeStartInfo(float fStartTime, int nDirection) = 0;
	ULMLNG	 SetRelativeTime() = 0;

	// 向板卡下发命令
	ULMDWD	 SendCommand(int nChannelNum, PTBYTE pCmdBuf, UINT nSize) = 0;

	ULMETHOD SetCalDelayTime(int nDelay) = 0;
	ULMINT   GetCalDelayTime() = 0;

	ULMVOID  SetUserCalTime(UINT nTime) = 0;
	ULMUNT   GetUserCalTime() = 0;

	ULMETHOD SetCaledTime(int nPhase, DWORD dwTime) = 0; // 自主刻度方式来设置刻度时间
	ULMDWD   GetCaledTime(UINT uPhase) = 0;

	ULMETHOD CalUserCancel() = 0;                        // 自主刻度方式，用户调用来退出刻度界面
	ULMETHOD BeforeCalibrate(LPCTSTR lpszItem) = 0;      // 自主刻度方式，用户调用来启动CalibrateIO
	ULMETHOD AfterCalibrate() = 0;                       // 自主刻度方式，用户调用来结束CalibrateIO

	// ---------------------------------
	//	表格刻度:
	// ---------------------------------
	ULMETHOD AddCell(LPCTSTR lpszCell) = 0;
	ULMPTR	 GetCell(LPCTSTR lpszCell) = 0;

	ULMETHOD DDX(LPCTSTR lpszID, int* nValue) = 0;
	ULMETHOD DDX(LPCTSTR lpszID, float* fValue) = 0;
	ULMETHOD DDX(LPCTSTR lpszID, double* dValue) = 0;
	ULMETHOD DDX(LPCTSTR lpszID, CString* strValue) = 0;
	ULMETHOD DDX(LPCTSTR lpszID, void* lpPaintFunc) = 0;

	ULMPTR	 GetValue(LPCTSTR lpszID) = 0; // 由ID返回变量地址
	
	ULMETHOD UpdateData(LPCTSTR lpszCell, BOOL bSaveAndValidate = TRUE) = 0;
	ULMETHOD UpdateData(BOOL bSaveAndValidate /* = TRUE */) = 0;

	ULMETHOD SetCalDotEng(int nCurve, int nSeg, int nDot, double dEngineer) = 0;
	ULMETHOD SetCalDotEng(int nPhase, int nCurve, int nSeg, int nDot, double dEngineer) = 0;
	ULMETHOD DDXF(LPCTSTR strID, void* lpFunc) = 0;		
	ULMETHOD GetCalDotMaxValue(int nCur, int nSeg, int nDot, double& fMax) = 0;  // 得到最大刻度测量值			
	ULMETHOD GetCalDotMinValue(int nCur, int nSeg, int nDot, double& fMin) = 0;  // 得到最小刻度测量值		
	ULMETHOD GetCalDotLastValue(int nCur, int nSeg, int nDot, double& fLast) = 0; // 得到最后一个刻度测量值
	ULMETHOD SetCalDotCoefRange(int nPhase, int nCur, int nSeg, LPCTSTR lpszCoef, double fMinValue, double fMaxValue) = 0;
	
	// 设置刻度点当前刻度阶段的采集时间,单位ms
	ULMETHOD SetCalDotTime(LPCTSTR lpszDotName, DWORD dwTime) = 0;    

	// ---------------------------------
	//	仪器刻度方式声明:
	// ---------------------------------
	ULMETHOD SetCalType(UINT uPhase, int nType, void* pWnd = NULL, UINT nID = 0) = 0;
	ULMETHOD SetCalValues(int nValue = VM_AVE) = 0;		//设置取值模式
	ULMDBL	 GetCalDotValue(int nCur, int nSeg, int nDot, double dErr = DBL_MAX, int nValue = VM_DEF) = 0;

	ULMETHOD DefineOffsetMode(LPCTSTR lpszName, UINT nMode) = 0;
	ULMETHOD SetCalDotInitEng(int nCurve, int nSeg, int nDot, double dEngineer) = 0;
	ULMETHOD SetCalDotInitEng(int nCalType, int nCurve, int nSeg, int nDot, double dEngineer) = 0;
	ULMETHOD SetCalDotStandard(int nCurve, int nSeg, int nDot, double dStand) = 0;
	ULMETHOD SetCalDotStandard(int nCalType, int nCurve, int nSeg, int nDot, double dStand) = 0;
	ULMETHOD SetCalDotInitStandard(int nCurve, int nSeg, int nDot, double dStand) = 0;
	ULMETHOD SetCalDotInitStandard(int nCalType, int nCurve, int nSeg, int nDot, double dStand) = 0;
	ULMETHOD ClearCalDotBuffer(int nCurve, int nSeg, int nDot) = 0;

	ULMETHOD CalInvalidate(UINT nDays = 30) = 0;	// 设置刻度有效期
	ULMETHOD AddDefCoef(UINT nPhase, UINT nCurve, UINT nSeg, LPCTSTR lpszName, 
		double fCoef, double fRMin, double fRMax, double fRNom) = 0;

	// 设置刻度点范围
	ULMETHOD SetCalDotVarRange(LPCTSTR lpszCurve, LPCTSTR lpszDot, double fMinVal, double fMaxVal, double fNomVal) = 0;

	
	ULMETHOD SetCalDotMea(int nCur, int nSeg, int nDot, double dMea) = 0;
	ULMETHOD SetCalDotMea(int nPhase, int nCur, int nSeg, int nDot, double dMea) = 0;
	ULMETHOD SetCoefUnit(LPCTSTR lpszCurve , LPCTSTR lpszName,	LPCTSTR lpszUnit) = 0;
	ULMETHOD SetDotUnit(LPCTSTR lpszCurve , LPCTSTR lpszName,	LPCTSTR lpszUnit) = 0;

	// ---------------------------------
	//	UNUSED:
	// ---------------------------------
	ULMPTR	 GetParamBuffer(LPCTSTR lpszParam, BOOL bVerifyTime = TRUE) = 0;
	ULMETHOD SetParamSize(LPCTSTR lpszParam, ULONG cbSize) = 0;
	ULMLNG	 GetParamSize(LPCTSTR lpszParam) = 0;
	// ------- -------------------------

	// ---------------------------------
	//	仪器参数存取:
	// ---------------------------------
	ULMBOOL	 AddParam(LPCTSTR pszKey, int iVal, LPCTSTR pszType = _T(""), LPCTSTR pszDesc = _T(""), 
		LPCTSTR pszUnit = _T(""), CStringArray* pArray = NULL, LPCTSTR pszComment = _T(""), DWORD dwStyle = 3) = 0;

	ULMBOOL	 AddParam(LPCTSTR pszKey, UINT uiVal, LPCTSTR pszType = _T(""), LPCTSTR pszDesc = _T(""),
		LPCTSTR pszUnit = _T(""), LPCTSTR pszComment = _T(""), DWORD dwStyle = 3) = 0;

	ULMBOOL	 AddParam(LPCTSTR pszKey, float& fVal, LPCTSTR pszType = _T(""), LPCTSTR pszDesc = _T(""),
		LPCTSTR pszUnit = _T(""), LPCTSTR pszComment = _T(""), DWORD dwStyle = 3) = 0;

	ULMBOOL	 AddParam(LPCTSTR pszKey, double& dVal, LPCTSTR pszType = _T(""), LPCTSTR pszDesc = _T(""),
		LPCTSTR pszUnit = _T(""), LPCTSTR pszComment = _T(""), DWORD dwStyle = 3) = 0;

	ULMBOOL	 AddParam(LPCTSTR pszKey, LPCTSTR pszVal, LPCTSTR pszType = _T(""), LPCTSTR pszDesc = _T(""),
		LPCTSTR pszUnit = _T(""), CStringArray* pArray = NULL, LPCTSTR pszComment = _T(""), DWORD dwStyle = 3) = 0;

	ULMBOOL	 AddParam(LPCTSTR pszKey, LPBYTE pData, DWORD dwBytes, LPCTSTR pszType = _T(""), LPCTSTR pszDesc = _T(""),
		LPCTSTR pszUnit = _T(""), LPCTSTR pszComment = _T(""), DWORD dwStyle = 3) = 0;

	ULMBOOL	 GetParam(LPCTSTR pszKey, int& iVal) = 0;
	ULMBOOL	 GetParam(LPCTSTR pszKey, UINT& uiVal) = 0;
	ULMBOOL	 GetParam(LPCTSTR pszKey, float&	fVal) = 0;
	ULMBOOL	 GetParam(LPCTSTR pszKey, double& dVal) = 0;
	ULMBOOL  GetParam(LPCTSTR pszKey, CString& sVal) = 0;
	ULMDWD	 GetParam(LPCTSTR pszKey, LPBYTE pData) = 0;

	ULMBOOL  AddParam(LPCTSTR pszKey, short sVal, LPCTSTR pszType = _T(""), LPCTSTR pszDesc = _T(""),
		LPCTSTR pszUnit = _T(""), LPCTSTR pszComment = _T(""), DWORD dwStyle = 3) = 0;
	ULMBOOL	 AddParam(LPCTSTR pszKey, long lVal, LPCTSTR pszType = _T(""), LPCTSTR pszDesc = _T(""),
		LPCTSTR pszUnit = _T(""), LPCTSTR pszComment = _T(""), DWORD dwStyle = 3) = 0;
	ULMBOOL  AddParam(LPCTSTR pszKey, WORD wVal, LPCTSTR pszType = _T(""), LPCTSTR pszDesc = _T(""),
		LPCTSTR pszUnit = _T(""), LPCTSTR pszComment = _T(""), DWORD dwStyle = 3) = 0;
	ULMBOOL  AddParam(LPCTSTR pszKey, DWORD dwVal, LPCTSTR pszType = _T(""), LPCTSTR pszDesc = _T(""), 
		LPCTSTR pszUnit = _T(""), LPCTSTR pszComment = _T(""), DWORD dwStyle = 3) = 0;
	
	// 使用\n分隔符加入枚举参数
	ULMBOOL  AddParam2(LPCTSTR pszKey, int iVal, LPCTSTR pszList, LPCTSTR pszType = NULL, 
		LPCTSTR pszDesc = NULL, LPCTSTR pszUnit = NULL, LPCTSTR pszComment = NULL, DWORD dwStyle = 3) = 0;
	ULMBOOL  AddParam2(LPCTSTR pszKey, LPCTSTR pszVal, LPCTSTR pszList, LPCTSTR pszType = NULL, 
		LPCTSTR pszDesc = NULL, LPCTSTR pszUnit = NULL, LPCTSTR pszComment = NULL, DWORD dwStyle = 3) = 0;

	ULMBOOL  GetParam(LPCTSTR pszKey, short& sVal) = 0;
	ULMBOOL	 GetParam(LPCTSTR pszKey, long& lVal) = 0;
	ULMBOOL	 GetParam(LPCTSTR pszKey, WORD& wVal) = 0;
	ULMBOOL	 GetParam(LPCTSTR pszKey, DWORD&	dwVal) = 0;

	//ULMETHOD GetIUnits(IUnits** pIUnits) = 0;
	ULMETHOD GetIUnits(CUnits* pIUnits) = 0;
	ULMETHOD SaveParam(LPCTSTR pszKey) = 0;
	ULMBOOL  GetULParam(UINT nID, void* pValue) = 0;
	ULMBOOL	 SetULParam(UINT nID, void* pValue) = 0;

	ULMETHOD SaveParams() = 0; // 存参调用

	// 表操作
	ULMPTR	 OpenTable(LPCTSTR lpszTableName, LPCTSTR lpszTableCHName = NULL) = 0;
	ULMPTR	 CreateTable(LPCTSTR lpszTableName, LPCTSTR lpszTableCHName, int nField) = 0;
	ULMETHOD CloseTable(LPCTSTR lpszTableName, LPCTSTR lpszTableCHName = NULL) = 0;

	// 文件存取
	ULMDWD	 WriteToFile(CFile* pFile, DWORD dwType, IULTool* pToolUpdate = NULL) = 0;
	ULMDWD   ReadFromFile(CFile* pFile, DWORD dwType) = 0;
		
	ULMETHOD SetUserDefineFilterType(int nCurve, int nType) = 0;
	ULMETHOD AddDefCoef(UINT nPhase, UINT nCurve, UINT nSeg, LPCTSTR lpszName, LPCTSTR lpszAlias, 
		double fCoef, double fRMin, double fRMax, double fRNor) = 0;
    
    ULMDWD   NeedUpdateCalibrate() = 0; 
	ULMETHOD SetCalDotStandardRange(LPCTSTR lpszCurve, LPCTSTR lpszDot, double fMinVal, double fMaxVal, double fNorVal) = 0;
		
	// pszInfo canbe any of string "ID,OD,ODMax,ODMin,PressMax,TempMax,SpeedMax"
	// each unit is "m, m, m, m, Pa, K, m/s"
	// 内径,外径,最大外径,最小外径,最大工作压力,最大工作温度,最大测速
	ULMETHOD SetToolInfoEx(LPCTSTR pszInfo, double* pValue) = 0;
	ULMETHOD GetToolInfoEx(LPCTSTR pszInfo, double* pValue) = 0;

	ULMETHOD GetToolPath(LPTSTR pszPath) = 0;
	ULMETHOD AddCalDesc(LPCTSTR pszName, LPCTSTR pszValue) = 0; // 添加当前刻度阶段的描述
	ULMETHOD AddCalDesc(UINT uPhase, LPCTSTR pszName, LPCTSTR pszValue) = 0; // 添加特定刻度阶段的描述
	ULMETHOD UpdateCalDesc(LPCTSTR pszName, LPCTSTR pszValue) = 0; //更新当前刻度阶段的描述
	ULMETHOD UpdateCalDesc(UINT uPhase, LPCTSTR pszName, LPCTSTR pszValue) = 0; //更新特定刻度阶段的描述
	ULMETHOD ClearCalDescs() = 0; // 清理当前阶段的描述
	ULMETHOD ClearCalDescs(UINT uPhase) = 0; // 清理特定阶段的描述
	ULMETHOD DeleteCalDescs(LPCTSTR pszName) = 0; // 删除当前阶段的某个描述
	ULMETHOD DeleteCalDescs(UINT uPhase, LPCTSTR pszName) = 0; // 删除特定阶段的某个描述
	ULMTSTR	 GetCalDesc(LPCTSTR pszName) = 0;// 获取当前刻度阶段的描述
	ULMTSTR	 GetCalDesc(UINT uPhase, LPCTSTR pszName) = 0;// 获取特定刻度阶段的描述
	ULMPTR GetProjectObj() =0;
	ULMETHOD GetMudInfo(DWORD nMask , double &fValue) = 0;
	ULMLNG	 GetCurRun() = 0;
	ULMETHOD SetRunInfo(DWORD nMask , DWORD *pParam) = 0;
	ULMETHOD SaveRunInfo() = 0;
};

// {203B4589-BD37-4ea7-8BB2-471C6917F24A}
static const IID IID_IULTool = 
{ 0x203b4589, 0xbd37, 0x4ea7, { 0x8b, 0xb2, 0x47, 0x1c, 0x69, 0x17, 0xf2, 0x4a } };

#endif

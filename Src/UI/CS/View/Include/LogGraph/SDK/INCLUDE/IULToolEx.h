/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 *
 * 版本号：			1.70 正式版
 *                  StartChannels()		本仪器库通道启动，开始收发数据
 *
 * 版本号：			1.70.31
 *                  GetCurCalGroup()
 *					返回当前刻度界面组号
 *
 * 版本号			1.71
 *					SetCurveNoOffset(LPCTSTR lpszName, BOOL bCalcOffset)
 *					SetCurveNoOffset(int nCurve, BOOL bCalcOffset)
 *					设置曲线不进行深度偏移，开始测量即开始绘制曲线
 *					参数1为曲线名称或者曲线索引，参数2为是否进行偏移FALSE进行偏移（原本默认）
 *
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __IULTOOLEX_H__
#define __IULTOOLEX_H__

#include "ULInterface.h"
#include "ULCOMMDEF.H"
#include "IDB.h"

// Interface
interface IULToolEx : IUnknown
{
	ULMETHOD SaveCalFile() = 0; 							//保存当前刻度文件	
	ULMETHOD ReadCalFile(CString strCalFileName) = 0; 	    //读取当前刻度文件
	ULMETHOD ShowCalReport(CWnd *pParent) = 0;				//显示刻度报告
	ULMETHOD ShowCalSummary(CWnd *pParent) = 0;				//显示刻度列表
	ULMETHOD SetCalDotStandardRange(UINT nPhase , LPCTSTR lpszCurve, LPCTSTR lpszDot, double fMinVal, double fMaxVal, double fNorVal) = 0;
	ULMETHOD SetCalDotVarRange(UINT nPhase , LPCTSTR lpszCurve, LPCTSTR lpszDot, double fMinVal, double fMaxVal, double fNomVal) = 0;

	// 刻度分组函数
	virtual	short _stdcall AddCalParamEx(void* pCurve, void* pSeg, void* pDot, LPCTSTR pszGroupName = _T(" ")) = 0;
	virtual	short _stdcall AddCalParamEx(UINT uPhase, void* pCurveData, void* pSegData, void* pDotData, LPCTSTR pszGroupName = _T(" ")) = 0;
	ULMETHOD StartChannels() = 0;
	ULMETHOD GetCurCalGroupInt() = 0;

	virtual BOOL _stdcall SetCurveNoOffset(LPCTSTR lpszName, BOOL bCalcOffset) = 0; // 设置曲线是否进行深度偏移
	virtual BOOL _stdcall SetCurveNoOffset(int nCurve, BOOL bCalcOffset) = 0;
	virtual float _stdcall GetThreadHoldValue()=0;
	virtual BOOL _stdcall SetThreadHoldValue(float fValue)=0;
	virtual IDB* _stdcall GetIDB()=0;
	virtual BOOL _stdcall LogFlash()=0;
	virtual double _stdcall GetUCValue(CString pszName, double d , BOOL bSet) = 0;
	virtual CString _stdcall GetUCUnit(CString pszName , BOOL bRaw) = 0;
};

// {C7E5E573-4B90-41b2-AC7C-B0A3B06041E2}
static const IID IID_IULToolEx = 
{ 0xc7e5e573, 0x4b90, 0x41b2, { 0xac, 0x7c, 0xb0, 0xa3, 0xb0, 0x60, 0x41, 0xe2 } };

#endif

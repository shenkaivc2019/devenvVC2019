/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2014-2-24
 * 平台版本号：     AXP 1.75
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.75
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/

#ifndef __IULLWDFILE__
#define __IULLWDFILE__


interface IULLwdFile : IUnknown
{
	// 当前要保存数据的存储空间，要保存数据的长度（字节），预留备用接口（可约定代表的含义）
	virtual DWORD _stdcall SaveToFile(PTBYTE pDataBuf, DWORD nDataLen, LPARAM lpParam = 0) = 0;

};

// {781DD8E0-3597-4b69-8D60-5943F924BA66}
static const GUID IID_IULLwdFile = 
{ 0x781dd8e0, 0x3597, 0x4b69, { 0x8d, 0x60, 0x59, 0x43, 0xf9, 0x24, 0xba, 0x66 } };


#endif
/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __IULBOARD_H__
#define __IULBOARD_H__

#include "ULInterface.h"

// Interface
interface IULBoard : IUnknown
{
	// -----------------------------------------------------------------------
	//	设置板卡属性：板卡名称，缓冲大小
	// -----------------------------------------------------------------------
	ULMETHOD	ulBDSetProperty(char strName[], ULONG nLen) = 0;
	ULMETHOD	AddUserDlg(void* pDlg, UINT nID) = 0;

	// -----------------------------------------------------------------------
	//	添加通道
	// -----------------------------------------------------------------------
	ULMETHOD	AddFixChannel(LPCTSTR lpszType, // 通道类型	
							USHORT nNum,		// 通道索引
							double dDataRate,	// 数传速率
							long  lPrecision	// 传输精度
							) = 0;
	
	// -----------------------------------------------------------------------
	//	板卡重载函数
	// -----------------------------------------------------------------------
	ULMETHOD	ulBDSingleIO(LPVOID pDataBuf) = 0;	
	ULMETHOD	ulBDSetMode(BYTE* pParam,int nNum) = 0;
	ULMETHOD	ulBDGetMode(BYTE* pParam,int nNum) = 0;
	ULMETHOD	ulBDSendCommand(int nChannelNo, BYTE* pCmd, int nNum) = 0;
	
	// -----------------------------------------------------------------------
	//	通道操作函数
	// -----------------------------------------------------------------------
	ULMINT		GetTotalChannelNum() = 0;
	ULMINT		GetAllocedChannelNum() = 0;
	ULMDWD		GetChannelID(short nTypeNum, short nNum = 0) = 0;
	ULMETHOD	SetChannelData(short nTypeNum, LPVOID pData, int nNum) = 0;

	ULMETHOD	GetBoardPath(LPTSTR pszPath) = 0;
	ULMETHOD    GetServiceName(LPTSTR pszService) = 0;
};

extern const IID IID_IULBoard;

#endif

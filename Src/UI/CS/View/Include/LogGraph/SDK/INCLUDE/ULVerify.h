/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
// ULVerify.h: interface for the CULVerify class.
//
////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_ULVERIFY_H__6C4492CC_1BCF_4CA8_BF15_0FAA45B5B5AE__INCLUDED_)
#define AFX_ULVERIFY_H__6C4492CC_1BCF_4CA8_BF15_0FAA45B5B5AE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxtempl.h>

class CULVerify : public CObject  
{
public:
	CULVerify();
	virtual ~CULVerify();

};

class __declspec(dllexport) CULVerifyDot : public CObject
{
public:
	CULVerifyDot(CString Name,CString Alias,int uPri,int Destime,CString tip="Test" );
	CULVerifyDot();
	~CULVerifyDot();
	virtual void Serialize(CArchive& ar);
	void operator=(const CULVerifyDot &Dot);

public:
	CString     m_strName;		     //校验点名称
	CString		m_strAlias;			 //校验点别名
	int	        m_iPriority;         //优先级(0级最高)
	BOOL        m_bSelected;		 //是否选中为当前校验点
	BOOL        m_bVerify;           //TRUE-已校  FALSE-未校
	int	        m_iDesTime;          //预置的刻度时间,以毫秒为单位
	CTime		m_DateTm;            //校验日期时间
	CString     m_strTip;            //tip
	double		m_fMeas;             //测量值
	double      m_fEng;              //工程值
	double      m_fMinStand,m_fMaxStand,m_fFineStand;//标称值

	CString     m_strTime;           //记录当时校验的时间
	DWORD		m_dwIndex;           //标示刻度点的索引号0x xx xx xx ,By Gzh,2003.1.6

	BOOL			m_bEditMeasure;		
	CArray<double, double>	m_Buffer;	 //刻度时，存放从仪器来的数据

public:
	void   SetDateTm();
	void   SetCalStatus(BOOL bStatus);
	BOOL   GetCalStatus();
	void   SetDestime(int uDesTime);
	int    GetDestime();
	int    GetPriority();
	void   SetTip(CString tip);
	void   GetTip(CString &tip);

protected:
	DECLARE_SERIAL(CULVerifyDot)
};


class __declspec(dllexport) CULVerifyCurve : public CObject
{
public:
	CULVerifyCurve();
	~CULVerifyCurve();
	virtual void Serialize(CArchive& ar);
	void operator=(const CULVerifyCurve &Curve);
public:
	CString		m_strVersion;	// 版本号
	CString		m_strName;		// 曲线名
	CString		m_strUnit;		// 单位
	BOOL        m_bSelected;    // 是否选中为当前刻度曲线
	BOOL        m_bVerify;		// 是否校验完成 
	int			m_nVerifyMethod;// 校验模式 
	CString		m_strTip;		// tip
	CPtrArray	m_VerifyDotList;//校验点列表

public:
	void SetCurveVerifyStatus();
	int	 GetDotCount();
	void ResetVerifyDotList();	

	DECLARE_SERIAL(CULVerifyCurve)
};

#endif // !defined(AFX_ULVERIFY_H__6C4492CC_1BCF_4CA8_BF15_0FAA45B5B5AE__INCLUDED_)

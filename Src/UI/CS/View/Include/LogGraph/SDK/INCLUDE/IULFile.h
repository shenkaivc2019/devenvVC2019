/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 
 * 版本号			1.72
 *					GetCurve(LPCTSTR lpszName, LPCTSTR lpszSrc)
 *					根据曲线名称以及曲线所属仪器库获取文件中曲线指针
 *					参数1为曲线名，参数2为所属仪器库名称
 *
 *                  GetAXPVersion()
 *					获得AXP软件的版本号
 *
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __IULFILE_H__
#define __IULFILE_H__

#include "ULCOMMDEF.H"
#include "ULInterface.h"

#define FT_CREATE	0x0001
#define FT_MODIFY	0x0002
#define FT_CRTMDF	(FT_CREATE|FT_MODIFY)

// Interface
interface IULFile : IUnknown
{
	// ---------------------------------
	//	Get information:
	// ---------------------------------

	ULMTSTR		GetPath() = 0;			// return IDE path

	ULMTSTR		GetScout() = 0;			// return scout file path
	ULMETHOD	SetScout(LPCTSTR lpszScout) = 0;
	ULMTSTR		GetParam() = 0;			// return tool param path
	ULMETHOD	SetParam(LPCTSTR lpszParam) = 0;
	
	ULMPTR		GetProjectInfo() = 0;	// return project info
	ULMETHOD	SetProjectInfo(void* pPi) = 0;

	ULMETHOD    GetTools(vec_ts& vecTool) = 0;
	ULMETHOD	CreateTools(vec_ts& vecTools, UINT nTools = 1) = 0;

	// ---------------------------------
	//	Sheet file save:
	// ---------------------------------

	ULMINT		GetSheetCount() = 0;
	ULMETHOD    GetSheets(CStringArray* pArrSheet) = 0;
	ULMETHOD	SaveSheet(int i, LPCTSTR lpszSheet) = 0;
	ULMETHOD	SetSheets(CStringArray* pArrSheet) = 0;
	ULMETHOD	SetSheet(int i, LPCTSTR lpszSheet) = 0;

	// ---------------------------------
	//	Print file save:
	// ---------------------------------
	ULMETHOD    GetPrints(CStringArray* pArrPrint, CStringArray* pArrTempl) = 0;
	ULMETHOD	SetPrints(CStringArray* pArrPrint, CStringArray* pArrTempl) = 0;

	// ---------------------------------
	//	Cell file save:
	// ---------------------------------

	ULMINT		GetCellCount() = 0;
	ULMETHOD    GetCells(CStringArray* pArrCell, CStringArray* pArrTempl) = 0;
	ULMETHOD	SaveCell(int i, LPCTSTR lpszCell) = 0;
	ULMETHOD	SetCells(CStringArray* pArrCell, CStringArray* pArrTempl) = 0;
	ULMETHOD	SetCell(int i, LPCTSTR lpszCell) = 0;
	
	ULMETHOD    Close() = 0;

	// ---------------------------------
	//	Add curve data:
	// ---------------------------------

	ULMETHOD	AddCurveData(int nCurve, long lDepth, void* pValue, long lTime = 0) = 0;
	ULMETHOD	AddCurveDatas(int nCurve, long lDepth, long lNextDepth, void* pValue, long lTime = 0) = 0;
	ULMETHOD	AutoMapSheet(BOOL bAutoMap = TRUE) = 0;

	// ---------------------------------
	//	Get file curve:
	// ---------------------------------

	ULMINT	GetCurves(vec_ic** pCurves) = 0;
	ULMINT	GetCurveCount() = 0;
	ULMPTR	GetCurve(int nIndex) = 0;
	ULMPTR	GetCurve(LPCTSTR lpszName) = 0;

	ULMTSTR GetMarks() = 0;
	ULMETHOD SetMarks(LPCTSTR lpszMarks) = 0;

	ULMETHOD SetStatusPrompt(LPCTSTR pszText) = 0;

	ULMETHOD GetInterface(const IID &ID , void **p) = 0;

	ULMLNG GetTime(DWORD dwTime = FT_CREATE) = 0;
	ULMETHOD SetTime(time_t time, DWORD dwTime = FT_CREATE) = 0;

	ULMBOOL  IsProjectFile() = 0;    // 是否是工程内部文件
	ULMETHOD    GetTools(CPtrArray* arrTools) = 0;
	ULMETHOD    ApplyParam(CString strTools) = 0;
	ULMETHOD    AutoMapSheet(CString strTools) = 0;     //根据仪器串映射绘图模板

	ULMPTR	CreateCurve(LPCTSTR pszName, VARTYPE vt = VT_R4) = 0;
	
	ULMPTR  GetDEPT() = 0;
	ULMLNG	GetUniqueNameSuffix(CStringArray* pArrNames) = 0;
	ULMPTR	GetTVDData() = 0;
	ULMPTR	GetTARData() = 0;
	ULMETHOD RecalcTVDData(double fDegree, double fInterval, double fStart, double fEnd) = 0;
	ULMSTR	GetFilePath() = 0;
	ULMPTR  GetCurveNoCase(LPCTSTR pszCurve) = 0;
	ULMETHOD SetStatusProcess(int nPrecent) = 0;

	// Filter file save
	ULMETHOD	SaveFileter(int i, LPCTSTR lpszFilter) = 0;
	ULMETHOD	ReadFileter(int i, LPCTSTR lpszFilter) = 0;

	ULMPTR	GetCurve(LPCTSTR lpszName, LPCTSTR lpszSrc) = 0;
	ULMTSTR GetAXPVersion() = 0; 
	ULMPTR	CreateCurve(LPCTSTR pszName, VARTYPE vt = VT_R4 , int nDimension = 1) = 0;
	ULMPTR	GetDataBase(DWORD dwParam) = 0;
	ULMINT	GetTVDDepth(long lMDDepth) = 0;
};

// {86390A97-2AB2-47fc-8A82-CB0E2C97E9A4}
static const IID IID_IULFile = 
{ 0x86390a97, 0x2ab2, 0x47fc, { 0x8a, 0x82, 0xcb, 0xe, 0x2c, 0x97, 0xe9, 0xa4 } };

#endif

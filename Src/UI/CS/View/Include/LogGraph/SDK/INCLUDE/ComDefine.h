#if !defined(_GETDEF_H)
#define _GETDEF_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// GetDef.h : 共通定義ファイル
//

/////////////////////////////////////////////////////////////////////////////
// RGB color
#define RGB_COLOR_WHITE (RGB(0xff, 0xff, 0xff))
#define RGB_COLOR_LGRAY (RGB(0xc0, 0xc0, 0xc0))
#define RGB_COLOR_GRAY  (RGB(0x80, 0x80, 0x80))
#define RGB_COLOR_BLACK (RGB(0x00, 0x00, 0x00))
#define RGB_COLOR_RED   (RGB(0xff, 0x00, 0x00))
#define RGB_COLOR_BLUE  (RGB(0x00, 0x00, 0xff))

/////////////////////////////////////////////////////////////////////////////
// user message

#define TUM_FILE_OPEN   (WM_USER + 0x100) // app -> doc
#define TUM_FILE_SAVE   (WM_USER + 0x101) // app -> doc
#define TUM_FILE_CLOSE  (WM_USER + 0x102) // app -> doc
#define TUM_VIEW_CHANGE (WM_USER + 0x200) // app or view -> main frame


/////////////////////////////////////////////////////////////////////////////
// update view hint : doc -> view
/*
#define UPDATE_FILE_OPEN      1
#define UPDATE_FILE_SAVE      2
#define UPDATE_NEED_SAVE      3
#define UPDATE_INPUT_CHECK    4
#define UPDATE_AUTO_FLAG      5
#define UPDATE_FILE_CALCULATE 6
#define UPDATE_FILE_CLOSE     7
*/

/////////////////////////////////////////////////////////////////////////////
// data-base file
// record max. / item max. / code
                          // 個人マスター
#define EMPCODE_DIGITS  10        // max. digits
#define EMPCODE_NONE    0L        // none code
#define EMPCODE_MIN     1L        // min. code
#define EMPCODE_MAX     99999998L // max. code

                          // 所属マスター
#define DEPCODE_DIGITS  9           // max. digits
#define DEPCODE_NONE    0L          // none code
#define DEPCODE_MIN     1L          // mix. code
#define DEPCODE_MAX     999999999L  // max. code

                          // 所属階層
#define DEPCLASS_MAX  4 // max. record
#define DEPCLASS_IN   0 // （最下層）
#define DEPCLASS_CMP  0 // （会社）
#define DEPCLASS_DEPL 1 // 大階層所属
#define DEPCLASS_DEPM 2 // 中階層所属
#define DEPCLASS_DEPS 3 // 小階層所属


                          // 会社マスター
#define COMPANY_MAX     10                // max. record
#define COMPANYCODE_MIN 0                 // min. code
#define COMPANYCODE_MAX (COMPANY_MAX - 1) // max. code

#define COMPANYIES_OFF  0 // 複数会社なし min. digits
#define COMPANYIES_ON   1 // 複数会社あり max. digits

                          // 役職テーブル
#define POST_MAX      99              // max. record
#define POSTCODE_NONE POST_MAX        // none code
#define POSTCODE_MIN  1               // max. code
#define POSTCODE_MAX  (POST_MAX - 1)  // max. code


                          // 予備テーブル
#define RESERVE_MAX     100               // max. record
#define RESERVECODE_MIN 0                 // max. code
#define RESERVECODE_MAX (RESERVE_MAX - 1) // max. code


                          // 銀行マスター
#define BANKCODE_NONE 0L        // none code
#define BANKCODE_MIN  1L        // min. code
#define BANKCODE_MAX  9999999L  // max. code


                          // 銀行コード
#define BANKSCODE_NONE  0L    // none code
#define BANKSCODE_MIN   1L    // min. code
#define BANKSCODE_MAX   9999L // max. code


                          // 支店コード
#define BRCHSCODE_NONE  0L                // none code
#define BRCHSCODE_MIN   (BRCHSCODE_NONE)  // min. code
#define BRCHSCODE_MAX   999L              // max. code

                          // 新規／継続
#define BANK_DEPOSIT_CONTINUE 0 // 継続
#define BANK_DEPOSIT_NEW      1 // 新規
#define BANK_DEPOSIT_CHANGE   2 // 変更


                          // 預金種目
#define BANK_DEPOSIT_GENERAL  1 // 普通預金
#define BANK_DEPOSIT_CURRENT  2 // 当座預金
#define BANK_DEPOSIT_OTHER    9 // その他


                          // 市町村マスター
#define CTYCODE_NONE  0L      // none code
#define CTYCODE_MIN   1L      // min. code
#define CTYCODE_MAX   999999L // max. code

                          // 集計区分
#define ATTTYPE_MAX   10                // max. record
#define ATTTYPENO_MIN 0                 // min. code
#define ATTTYPENO_MAX (ATTTYPE_MAX - 1) // max. code

                          // 計算区分
#define PAYTYPE_MAX   10                // max. record
#define PAYTYPENO_MIN 0                 // min. code
#define PAYTYPENO_MAX (PAYTYPE_MAX - 1) // max. code


                          // 部門区分
#define CLASSTYPE_MAX   6                   // max. record
#define CLASSTYPENO_MIN 0                   // min. code
#define CLASSTYPENO_MAX (CLASSTYPE_MAX - 1) // max. code

                          // クライアント
#define CLIENT_MAX    10000             // max. record
#define CLIENTNO_MIN  0                 // min. code
#define CLIENTNO_MAX  (CLIENT_MAX - 1)  // max. code

                          // 勤務時間帯
#define SHIFTDEF_MAX	99					// max. record
#define SHIFTNO_NONE	0					// none code
#define SHIFTNO_MIN		1					// min. code
#define SHIFTNO_MAX		(SHIFTDEF_MAX)		// max. code
#define SHIFTNO_MAX_EX	400					// max. code


                          // 打刻ＭＣ、回数ＭＣ
#define MCDEF_MAX   99          // max. record
#define MCCODE_NONE 0           // none code
#define MCCODE_MIN  1           // min. code
#define MCCODE_MAX  (MCDEF_MAX) // max. code


                          // カレンダ（不在理由含む）
#define CALENDEF_MAX  99  // max. record
#define CALENNO_NONE  0   // none code


#define CALENDEF1_MAX 10              // max. record
#define CALEN1NO_MIN  1               // min. code
#define CALEN1NO_MAX  (CALENDEF1_MAX) // max. code


#define CALENDEF2_MAX (CALENDEF_MAX - CALENDEF1_MAX)  // max. record
#define CALEN2NO_MIN  (CALEN1NO_MAX + 1)              // min. code
#define CALEN2NO_MAX  (CALENDEF_MAX)                  // max. code


#define CALEN_MOVE_OFF 0  // 振替なし
#define CALEN_MOVE_ON  1  // 振替あり
// 2006.02.15 Ver5.00 機能改善【NA5_006】Start
#define CALEN_MOVE_ON_HOLDAY		2  // あり（国民の祝日）
// End

#define WEEK_MAX  7 // max. record
#define WEEK_MON  0 // 月曜日
#define WEEK_TUE  1 // 火曜日
#define WEEK_WED  2 // 水曜日
#define WEEK_THU  3 // 木曜日
#define WEEK_FRI  4 // 金曜日
#define WEEK_SAT  5 // 土曜日
#define WEEK_SUN  6 // 日曜日


#define MONTH_MAX 12  // max. record
#define MONTH_JAN 1   // １月
#define MONTH_FEB 2   // ２月
#define MONTH_MAR 3   // ３月
#define MONTH_APR 4   // ４月
#define MONTH_MAY 5   // ５月
#define MONTH_JUN 6   // ６月
#define MONTH_JUL 7   // ７月
#define MONTH_AUG 8   // ８月
#define MONTH_SEP 9   // ９月
#define MONTH_OCT 10  // １０月
#define MONTH_NOV 11  // １１月
#define MONTH_DEC 12  // １２月

                          // 通信パラメータ（通信先）
#define COM_LOCATION_MAX    254                 // max. record
#define COM_LOCATIONNO_NONE 0                   // none code
#define COM_LOCATIONNO_MIN  1                   // min. code
#define COM_LOCATIONNO_MAX  (COM_LOCATION_MAX)  // max. code

#define COM_SEND_ALL    998 // 全通信先
#define COM_SEND_USEDEP 999 // 所属マスター準拠

                          // ATM端末パラメータ
#define ATM_PARAM_MAX   8                   // max. record
#define ATM_PARAMNO_MIN 0                   // min. code
#define ATM_PARAMNO_MAX (ATM_PARAM_MAX - 1) // max. code


                          // 年間カレンダ種別
#define CALENTYPE_MAX   10                  // max. record
#define CALENTYPENO_MIN 0                   // min. code
#define CALENTYPENO_MAX (CALENTYPE_MAX - 1) // max. code


                          // 年休更新テーブル種別
#define PVTABLE_MAX     10                // max. record
#define PVTABLETYPE_MIN 0                 // min. code
#define PVTABLETYPE_MAX (PVTABLE_MAX - 1) // max. code


                          // 自動カウンタ
#define ACCCOUNTER_MAX    30                // max. record
#define ACCCOUNTERNO_NONE 0                 // none code
#define ACCCOUNTERNO_MIN  1                 // min. code
#define ACCCOUNTERNO_MAX  (ACCCOUNTER_MAX)  // max. code


                          // パラメータ　使用区分
#define PARAMSETUP_OFF  0 // 未使用
#define PARAMSETUP_ON   1 // 使用


// 共通

                          // 自由定義項目　等
#define ITEM_COMPR2DEF  1   // 集計区分パラメータ（処理月毎）
#define ITEM_COMPR3DEF  10  // 計算区分パラメータ

// 就業
                          // 自由定義項目　等
#define ITEM_INDATTDEF    20  // 個人マスター（就業）

#define ITEM_ATTPR1DEF    20  // 就業パラメータ
#define ITEM_ATTPR2DEF    10  // 就業パラメータ（集計区分）
#define ITEM_SFTPRMDEF    5   // 勤務時間帯
#define ITEM_DAILYPUNCH   8   // デイリーデータ（打刻データ）
#define ITEM_DAILYDATA    20  // 　　　〃　　　（時間数項目）
#define ITEM_DAILYCOUNT   6   // 　　　〃　　　（回数データ）
#define ITEM_DAILYDEL     6   // 　　　〃　　　（追加データ）
#define ITEM_DAILYASSIST  20  // 　　　〃　　　（応援データ）
#define ITEM_DAILYPLANOT  2   // 　　　〃　　　（予定時間外）
#define ITEM_DAILYBREAK   6   // 　　　〃　　　（休憩時刻)
// 2005.11.15 Ver5.00 機能改善【NA5_001】Start
#define ITEM_DAILYOTTIME  4   // 　　　〃　　　（時間外時刻）
#define ITEM_DAILYOTZONE  5   // 　　　〃　　　（時間外時間帯)
// End

                          // 打刻データ
#define PUNCH_IN      0 // 出勤
#define PUNCH_OUT     1 // 退勤
#define PUNCH_GOOUT   2 // 外出
#define PUNCH_GOBACK  3 // 戻り
#define PUNCH_GOOUT2  4 // 外出２
#define PUNCH_GOBACK2 5 // 戻り２
#define PUNCH_GOOUT3  6 // 外出３
#define PUNCH_GOBACK3 7 // 戻り３
#define PUNCH_IN2     6 // 出勤２
#define PUNCH_OUT2    7 // 退勤２


#define DAILY_VALID 42  // デイリーデータ有効日数
#define DAILY_MONTH 31  // デイリーデータ１ヶ月最大日数

#define ITEM_ATTLEDGER  80  // 就業台帳（集計項目）

#define ATTLEDGERNO_NONE  0                 // none code
#define ATTLEDGERNO_MIN   1                 // min. code
#define ATTLEDGERNO_MAX   (ITEM_ATTLEDGER)  // max. code

// 2009.05.14 Ver7.00 改善連絡【GetN700_4170】Start
#define ITEM_ATTLEDGER2	80  // 就業台帳２（集計項目）

#define ATTLEDGER2NO_NONE  0                 // none code
#define ATTLEDGER2NO_MIN   1                 // min. code
#define ATTLEDGER2NO_MAX   (ITEM_ATTLEDGER2) // max. code
// 2009.05.14 Ver7.00 改善連絡【GetN700_4170】End

#define ITEM_ALLREP   80  // 指定項目演算項目
#define ITEM_ATX20MSG 80  // ATX-20メッセージ演算項目
#define ITEM_AMX30MSG 80  // AMX-30メッセージ演算項目


// 給与

                          // 自由定義項目　等
#define ITEM_INDPAYDEF  50  // 個人マスター（給与）
#define ITEM_PAYMENTDEF 30  // 個人マスター（支給控除）
#define ITEM_PAYROLLDEF 20  // 個人マスター（給与項目）
#define ITEM_PAYPR1DEF  20  // 給与パラメータ
#define ITEM_PAYPR2DEF  20  // 給与パラメータ（集計区分）


#define ITEM_PAYLEDGER    80                // 給与台帳
#define PAYLEDGERNO_NONE  0                 // none code
#define PAYLEDGERNO_MIN   1                 // min. code
#define PAYLEDGERNO_MAX   (ITEM_PAYLEDGER)  // max. code


#define ITEM_BNSLEDGER    40                // 賞与台帳
#define BNSLEDGERNO_NONE  0                 // none code
#define BNSLEDGERNO_MIN   1                 // min. code
#define BNSLEDGERNO_MAX   (ITEM_BNSLEDGER)  // max. code


#define ITEM_TAXLEDGER    40                // 年調台帳
#define TAXLEDGERNO_NONE  0                 // none code
#define TAXLEDGERNO_MIN   1                 // min. code
#define TAXLEDGERNO_MAX   (ITEM_TAXLEDGER)  // max. code



#define ITEM_TAXBEFORE  4 // 前職分


#define ITEM_TAXSUMMER  4 // 夏期特別減税


                          // 賞与№
#define BONUS_MAX   4               // max. record
#define BONUSNO_MIN 0               // min. code
#define BONUSNO_MAX (BONUS_MAX - 1) // max. code


                          // 台帳項目モード
#define LDG_MODE_NONE         0   // なし
#define LDG_MODE_ATTENDANCE   1   // 出勤日数
#define LDG_MODE_ABSENCE      2   // 欠勤日数
#define LDG_MODE_PAY_TAX      3   // 課税支給
#define LDG_MODE_PAY_TAXFREE  4   // 非課税支給
#define LDG_MODE_PAY_ALL      5   // 総支給額
#define LDG_MODE_INS_SCL      6   // 社会保険料
#define LDG_MODE_TAX_INCOME   7   // 所得税
#define LDG_MODE_TAX_RSDNC    8   // 住民税
#define LDG_MODE_INS_LIFE_P   9   // 生命保険料
#define LDG_MODE_INS_ID_PNS_P 10  // 個人年金保険料
#define LDG_MODE_INS_NL_SRT_P 11  // 短期損害保険料
#define LDG_MODE_INS_NL_LNG_P 12  // 長期損害保険料
#define LDG_MODE_DEDUCT_ETC   13  // その他控除額
#define LDG_MODE_PAY_DEDUCT   14  // 差引支給額
#define LDG_MODE_BANK_A       15  // 銀行振込Ａ
#define LDG_MODE_BANK_B       16  // 銀行振込Ｂ
#define LDG_MODE_BANK_C       17  // 銀行振込Ｃ
#define LDG_MODE_PAY_CASH     18  // 現金支給額
#define LDG_MODE_PRINT_FLAG   19  // 帳票出力条件
#define LDG_MODE_PAYMENT      21  // 支給金額
#define LDG_MODE_TAX_RECEIPT  22  // 源泉徴収税額
#define LDG_MODE_DEDUCT_PAY   24  // 給与所得控除後の額
#define LDG_MODE_DCT_INS_SC   25  // 社会保険料控除額
#define LDG_MODE_DCT_INS_SC_R 26  // 申告社会保険料控除額
#define LDG_MODE_DCT_PREMIUM  27  // 小規模企業掛金控除額
#define LDG_MODE_INS_LIFE_T   28  // 生命保険料
#define LDG_MODE_INS_LIFE_R   29  // 申告生命保険料
#define LDG_MODE_INS_ID_PNS_T 30  // 個人年金保険料
#define LDG_MODE_INS_ID_PNS_R 31  // 申告個人年金保険料
#define LDG_MODE_DCT_INS_LIFE 32  // 生命保険料の控除額
#define LDG_MODE_INS_NL_SRT_T 33  // 短期損害保険料
#define LDG_MODE_INS_NL_LNG_T 34  // 長期損害保険料
#define LDG_MODE_DCT_INS_NL   35  // 損害保険料の控除額
#define LDG_MODE_SPOUSE_INCM  36  // 配偶者給与所得額
#define LDG_MODE_SPOUSE_DCT   37  // 配偶者特別控除額
#define LDG_MODE_INS_NL_SRT_R 38  // 申告短期損害保険料
#define LDG_MODE_INS_NL_LNG_R 39  // 申告長期損害保険料
#define LDG_MODE_TTL_DCT_INCM 42  // 所得控除額の合計
#define LDG_MODE_INCOME_TAX   43  // 課税給与所得金額
#define LDG_MODE_TAX_YEAR     44  // 年税額
#define LDG_MODE_DEDUCT_HOUSE 45  // 住宅取得等特別控除額
#define LDG_MODE_TAX_YEAR_DCT 47  // 差引年税額
#define LDG_MODE_OVER_DCT     48  // 差引超過額
#define LDG_MODE_LACK_DCT     49  // 差引不足額
#define LDG_MODE_TAX_YEAR_RED 51  // 特減控除前差引年税額
#define LDG_MODE_REDUCTION    52  // 特別減税額

// general value
#define NOR_TIME_NONE   -1441 // 打刻なし
#define NOR_TIME_MIN    -1440 // 前日の開始
#define NOR_TIME_DAYMIN 0     // 当日の開始
#define NOR_TIME_DAYMAX 1439  // 当日の終了
#define NOR_TIME_MAX    2879  // 翌日の終了

#define NOR_TIME_ONEDAY 1440  // １日の分数

#define NOR_PRMTIME_MIN 0     // 0～
#define NOR_PRMTIME_MAX 29999 //  ～499:59

#define WDG_TIME_NONE     (-1)  // 入力での時刻なし

#define WDG_DAY_NONE      (-1)  // 入力での日付なし
#define WDG_DAY_BEFORE    0     // 入力での前日
#define WDG_DAY_TODAY     1     // 入力での当日
#define WDG_DAY_TOMORROW  2     // 入力での翌日

#define FREE_LONG_S_MIN -999999L    // 自由項目（long型 就業６桁）の最小値
#define FREE_LONG_S_MAX 999999L     // 自由項目（long型 就業６桁）の最大値
#define FREE_LONG_MIN   -99999999L  // 自由項目（long型 給与８桁）の最小値
#define FREE_LONG_MAX   99999999L   // 自由項目（long型 給与８桁）の最大値
// 2009.02.27 Ver7.00 FCR【F0936】Start
#define FREE_LONG_9D_MIN  -999999999L // 自由項目（long型 給与９桁）の最小値
#define FREE_LONG_9D_MAX  999999999L  // 自由項目（long型 給与９桁）の最大値
// 2009.02.27 Ver7.00 FCR【F0936】End

// general style
                          // オペレータレベル
#define OP_LEVEL_OFF        0   // 自動起動待ち状態
#define OP_LEVEL_EMPLOYEE   1   // 従業員
#define OP_LEVEL_MANAGER    5   // 所属長
#define OP_LEVEL_HIGH_RANK  12  // 一般最上位
#define OP_LEVEL_SUPERVISOR 13  // システム管理者、
#define OP_LEVEL_SALES      14  // アマノ営業
#define OP_LEVEL_ENGINEER   15  // アマノＳＥ
#define OP_LEVEL_DEVELOP    16  // アマノ開発

                          // 自由定義項目　型
#define TYPE_NULL     0 // なし
#define TYPE_NUMERIC  1 // 回数
#define TYPE_DECIMAL  2 // 小数点付き回数
#define TYPE_HOUR     3 // 時間数
#define TYPE_MONEY    4 // 金額


                          // 自由定義項目　社保項目モード
#define INSURANCEMODE_NONE  0 // なし


                          // 入力条件
#define ITEM_INPUTITEM  30  // 入力項目数


                          // 対象条件　演算子
#define COMPARE_EQL 0 // 等しい
#define COMPARE_NEQ 1 // 以外
#define COMPARE_GEQ 2 // 以上
#define COMPARE_LEQ 3 // 以下
#define COMPARE_GTR 4 // 大きい
#define COMPARE_LES 5 // 小さい


                          // 時間数表示単位
#define TIME_60UNIT   0 // ６０進
#define TIME_100UNIT  1 // １００進
                          // 出勤率小数点以下扱い
#define PER_DECI3_CUTDOWN 0 // 小数点以下３桁を切り捨て
#define PER_DECI3_ROUND   1 // 　　　　〃　　　四捨五入
#define PER_DECI3_RAISE   2 // 　　　　〃　　　切り上げ
#define PER_DECI2_CUTDOWN 3 // 小数点以下２桁を切り捨て
#define PER_DECI2_ROUND   4 // 　　　　〃　　　四捨五入
#define PER_DECI2_RAISE   5 // 　　　　〃　　　切り上げ
#define PER_DECI1_CUTDOWN 6 // 小数点以下１桁を切り捨て
#define PER_DECI1_ROUND   7 // 　　　　〃　　　四捨五入
#define PER_DECI1_RAISE   8 // 　　　　〃　　　切り上げ

// raw data file

                          // 出退区分
#define IOIDENTITY_ALL      0   // 全打刻        // 2003.02.18 Ver4.10 【G4_014】
#define IOIDENTITY_IN       1   // 出勤
#define IOIDENTITY_OUT      2   // 退勤
#define IOIDENTITY_GOOUT    3   // 外出
#define IOIDENTITY_GOBACK   4   // 戻り
#define IOIDENTITY_GOOUT2   5   // 外出２
#define IOIDENTITY_GOBACK2  6   // 戻り２
#define IOIDENTITY_GOOUT3   7   // 外出３
#define IOIDENTITY_GOBACK3  8   // 戻り３
#define IOIDENTITY_IN2      9   // 出勤２
#define IOIDENTITY_OUT2     10  // 退勤２


// report

#define RPT_ROW_MAX     99  // 行数
#define RPT_COLUMN_MAX  130 // 桁数


                          // 縦横指定
#define RPT_FORMAT_VERTICAL 0 // 縦型
#define RPT_FORMAT_HORIZON  1 // 横型


                          // アンダーライン
#define RPT_UNDERLINE_OFF 0 // なし
#define RPT_UNDERLINE_ON  1 // あり


// file input / output
#define FIO_RECORD_MAX  1024  // レコード長

                          // CR.LF有無
#define FIO_CRLF_OFF  0 // なし
#define FIO_CRLF_ON   1 // あり

                          // EOF有無
#define FIO_EOF_OFF 0 // なし
#define FIO_EOF_ON  1 // あり

                          // ＣＳＶ
#define FIO_CSV_OFF 0 // 未対応
#define FIO_CSV_ON  1 // 対応

                          // ファイル入出力　形態
#define FIO_FORM_DATA     0 // データ
#define FIO_FORM_NAME     1 // データ対応名称
#define FIO_FORM_BYTENAME 2 // （データ対応カナ名称）

                          // ファイル入出力　型
#define FIO_TYPE_NUMERIC      0   // 変換なし
#define FIO_TYPE_DECIMAL      1   // 変換なし　小数点２桁
#define FIO_TYPE_DECI_PER10   2   // 1/10変換　小数点１桁
#define FIO_TYPE_HOUR_60UNIT  3   // 時分変換　６０進(:)
#define FIO_TYPE_HOUR_100UNIT 4   // 時分変換　１００進
#define FIO_TYPE_HOUR_10UNIT  5   // 時分変換　１０進
#define FIO_TYPE_MONEY        6   // 変換なし　金額
#define FIO_TYPE_TIME         7   // 時分変換　時刻
#define FIO_TYPE_DATE         8   // 変換なし　日付
#define FIO_TYPE_ZERO_AST     9   // 0 -> *
#define FIO_TYPE_ONE_AST      10  // 1 -> *
// 2003.10.28 Ver4.20 機能改善【G42_006】
#define FIO_TYPE_HOUR_60UNIT_PERIOD  103   // 時分変換　６０進(.)

                          // ファイル入出力　数値項目変換方法
#define FIO_MODE_NONE       0       // なし
#define FIO_MODE_SEPARATE   0x0001  // 区切り記号
#define FIO_MODE_ZEROSUPPLY 0x0002  // ゼロ埋め
#define FIO_MODE_PLUSSIGN   0x0004  // プラス符号
#define FIO_MODE_SPACE      0x0008  // 数値間スペース
#define FIO_MODE_ZERO_CTL   0x0010  // 数値０出力制御
#define FIO_MODE_BIGFONT    0x0020  // 拡大印刷


// ATM
													// ATM端末　モデル
#define	MODEL_NONE				-1
#define	MODEL_AMX_10			0
#define	MODEL_ACX_10			20
#define	MODEL_AFX_10			30
#define	MODEL_ATX_20			11
#define	MODEL_AMX_20			4
#define	MODEL_AMX_30			2
#define	MODEL_CSX				71
#define	MODEL_CEX				80
#define	MODEL_ATX_10			10					// 00.11.27 ATX-10対応
#define	MODEL_AGX_50			82					// 01.07.18 Ver2.10 X0246
#define	MODEL_AGX_30C			22
#define	MODEL_AGX_30Y			27
#define	MODEL_AGX_30N			3
// 2002.10.21 Ver4.00 FCR【T20261】Start
#define	MODEL_AGX_REJI			23
// End
#define MODEL_AGX_300			6					// 2007.11.13 Ver6.10 機能改善【G61_006】
#define MODEL_AGX_300AV			7					// 2009.01.07 Ver6.30 機能改善【C63_004】
// 2009.04.01 Ver6.22 機能変更【C63_006】Start
#define MODEL_SX_100A			8
// 2009.04.01 Ver6.22 機能変更【C63_006】End
// 2009.05.13 Ver7.00 機能変更【C63_005】Start
#define MODEL_AGX_300C			28
// 2009.05.13 Ver7.00 機能変更【C63_005】End

													// ATM端末　対象モデル
#define	OBJECT_AMX_10			0x0001
#define	OBJECT_ACX_10			0x0002
#define	OBJECT_AFX_10			0x0004
#define	OBJECT_ATX_20			0x0008
#define	OBJECT_AMX_20			0x0010
#define	OBJECT_AMX_30			0x0020
#define	OBJECT_CSX				0x0040
#define	OBJECT_CEX				0x0100
#define	OBJECT_RECEIVE_MODEL	0x00ff
#define	OBJECT_CSX_DOWN_NO		0x0200

#define	OBJECT_ATX_10			0x1000				// 00.11.27 ATX-10対応
#define	OBJECT_AGX_50			0x1100				// 01.07.18 Ver2.10 X0246
#define	OBJECT_AGX_30C			0x2000
#define	OBJECT_AGX_30N			0x4000
// 2002.10.21 Ver4.00 FCR【T20261】Start
#define	OBJECT_AGX_REJI			0x8000
// End
#define OBJECT_AGX_300			0x0080				// 2007.11.13 Ver6.10 機能改善【G61_006】
#define OBJECT_AGX_300AV		0x0400				// 2009.01.07 Ver6.30 機能改善【C63_004】
// 2009.04.01 Ver6.22 機能変更【C63_006】Start
#define OBJECT_SX_100A			0x10000
// 2009.04.01 Ver6.22 機能変更【C63_006】End
// 2009.05.13 Ver7.00 機能変更【C63_005】Start
#define OBJECT_AGX_300C			0x0800
// 2009.05.13 Ver7.00 機能変更【C63_005】End
/////////////////////////////////////////////////////////////////////////////
// character code

#define CHAR_CR   0x0D  // CR
#define CHAR_LF   0x0A  // LF
#define CHAR_EOF  0x1A  // EOF


/////////////////////////////////////////////////////////////////////////////
// getini Key (クライアント管理) by H.S (trinity)
// 2002/09/30 TS対応 Start
//	注意)自前でSOFTWARE\\AMANO\\GETを宣言しない。
//		 レジストリ操作を行う場合はCGfxRegKeyを使用すること
//
//#define KEY_GETROOT		_T("SOFTWARE\\AMANO\\GET")
//#define HIVE_GETINI		HKEY_LOCAL_MACHINE
//#define KEY_GETINI		KEY_GETROOT _T("\\getini")
#define SUBKEY_GETINI		_T("getini")
// End

#define SUBKEY_GENERAL       _T("General")
#define VALNAME_PGPATH       _T("ProgramPath")		// プログラムファイルのパス名
#define VALNAME_VUPATH       _T("VerUpPath")		// バージョンアップのパス名
#define VALNAME_TMPATH       _T("TempPath")			// テンポラリディレクトリもパス名
#define VALNAME_DTPATH       _T("DataPath")			// データ
#define VALNAME_SVNAME       _T("Server")			// サーバ名
#define VALNAME_TXTSVNAME    _T("TxtServer")		// テキストサーバ名
#define VALNAME_ALFSVNAME    _T("AlfarServer")		// アルファサーバ名
#define VALNAME_PICSVNAME    _T("PicturServer")		// 画像登録サーバ名
#define VALNAME_COMPATH      _T("ComPath")			// 格納先パス

#define SUBKEY_CLIENT        _T("Client")
#define VALNAME_SERIALNO     _T("SerialNo") // クライアント№　0～999
#define VALNAME_REMOTE       _T("Remote")   // リモートクライアント

#define SUBKEY_SETUP         _T("Setup")
#define VALNAME_OS           _T("OS")             // MS-Windowsの種類(0-1)
#define VALNAME_MACHINETYPE  _T("MachineType")    // ＰＣの機種(0-1)
#define VALNAME_PRINTWAIT    _T("PrintWaitTimer") // ドットプリンタの行間ウェイト(X10ms)

#define SUBKEY_STRESS        _T("Stress")
#define VALNAME_USERRESOURCE _T("UserResource") // ユーザリソースの割合
#define VALNAME_GDIRESOURCE  _T("GDIResource")  // ＧＤＩリソースの割合

/////////////////////////////////////////////////////////////////////////////
// X???????? Key by H.S (trinity)
// 2002/09/30 TS対応 Start
//#define HIVE_XINI            HKEY_LOCAL_MACHINE
//#define KEY_XINI             KEY_GETROOT _T("\\%s")
#define SUBKEY_XINI			_T("%s")
// End

#define SUBKEY_MAINFRAME     _T("MainFrame")
#define VALNAME_WINDOWPOS    _T("WindowPos%d")  // 親ウィンドウ表示位置

#define SUBKEY_VIEWBAR       _T("View Bar")
#define VALNAME_TOOLBAR      _T("ToolBar")    // ツールバー　　　0:非表示 1:表示
#define VALNAME_STATUSBAR    _T("StatusBar")  // ステータスバー　0:非表示 1:表示

#define SUBKEY_GRIDSIZE      _T("GridSize")
#define SUBKEY_GRIDSIZENO    _T("GridSize No%02d")
#define VALNAME_COLWIDTHS    _T("ColumnWidths") // 列の幅（intの配列 [REG_BINARY]）
#define VALNAME_ROWHEIGHT    _T("RowHeight")    // 行の高さ
#define VALNAME_BTNWIDTH     _T("ButtonWidth")  // ボタンの幅
#define VALNAME_BTNHEIGHT    _T("ButtonHeight") // ボタンの高さ

#define SUBKEY_XGENERAL      _T("General")
#define SUBKEY_XGENERALNO    _T("General No%02d") 
#define VALNAME_FDPATH       _T("FdPath")       // 外部入出力
#define VALNAME_FDPATHNAMECHOOSE       _T("FdPathNameChoose") // 2004.07.02 Ver4.11 機能改善【G5_006】、【G5_007】

/////////////////////////////////////////////////////////////////////////////
//  f?????apini Key by H.S (trinity) 暫定 (f?????ap.ini (data path))

// 2002/09/30 TS対応 Start
//#define HIVE_FAPINI          HKEY_LOCAL_MACHINE
//#define KEY_FAPINI           KEY_GETROOT _T("\\f%sapini")
#define SUBKEY_FAPINI           _T("f%sapini")
// End

////////////////////////////////////////////////////////////////////////////
// menu bar
#define MENU_POS_FILE     0 // [????]
#define SUBMENU_POS_OPEN  2 // [開く]

#define MENU_STR_OPENEMP  "個人指定(\036O\037?)...\tCtrl+O"
#define MENU_STR_OPENEMPS "出力指定(\036O\037?)...\tCtrl+O"
#define MENU_STR_OPENDEP  "所属指定(\036O\037?)...\tCtrl+O"


// 操作レベル
#define	HANDLINGLEVEL_NONE		0		// 操作不可
#define	HANDLINGLEVEL_DISP		1		// 表示のみ
#define	HANDLINGLEVEL_INPUT		2		// 入力可能
#define	HANDLINGLEVEL_SETUP		3		// 設定可能

#define	FREE_FLAG_MAX			5       // 予備

#endif //_GETDEF_H

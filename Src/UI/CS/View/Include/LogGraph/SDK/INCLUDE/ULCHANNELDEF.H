/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __ULCHANNELDEF_H
#define __ULCHANNELDEF_H

// 通道信息
#define DEFAULT_BUFFER_SIZE		4096

// -----------------------------------------------------------------------
//	板卡通道信息
// -----------------------------------------------------------------------

// 结构定义：

typedef struct  tagULBDCHDLLINFO
{
	CString		strChName;
	HINSTANCE   hCHDll;
	USHORT	    nCHNum;	
	USHORT	    nCHState;	
	
	CString	  	 strType;
	double		 dDataRate;
	LONG		 lPrecision;
	
}ULBDCHDllInfo;

typedef struct tagChannelHeader
{
	int nLength;
	long lLock;
	BOOL bStart;
}ChannelHeader;

// 通道状态字定义
#define		CH_UNUSE    	0		//没有使用
#define		CH_USE_ACTIVE	1		//有一只仪器使用
#define		CH_USE_IDLE	    2		//空闲
#define		CH_BAD	        3		//坏
#define		CH_UINSTALL   	4		//没有安装
#define     CH_USE_SHARE	5		//共享(多个仪器使用)

#endif
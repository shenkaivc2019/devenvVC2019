/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __ITOOLPARAM_H__
#define __ITOOLPARAM_H__

#include "ITool.h"

// Interfaces
interface IToolParam : IUnknown
{
	ULMETHOD	OnSaveParam() = 0;
	ULMETHOD	OnLoadParam() = 0;
	ULMETHOD	OnPowerOn() = 0;
	ULMETHOD	OnPowerOff() = 0;
};

// 接口ID
// {E4FE4845-DAAF-4997-AEBD-61EDB00EE40E}
static const IID IID_IToolParam = 
{ 0xe4fe4845, 0xdaaf, 0x4997, { 0xae, 0xbd, 0x61, 0xed, 0xb0, 0xe, 0xe4, 0xe } };

#endif
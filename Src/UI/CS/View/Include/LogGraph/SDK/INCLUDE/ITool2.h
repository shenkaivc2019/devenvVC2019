/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __ITOOL2_H__
#define __ITOOL2_H__

#include "ITool.h"

// Interfaces
interface ITool2 : IUnknown
{
	// 基本操作
	ULMETHOD	  SetCurCountDown(float fCurCountDown, bool bFalg = TRUE) = 0; //设置到计数
	ULMETHODBOOL  SetPerforationControl(bool bAllowFire, bool bAllowShift) = 0; // 射孔取芯控制，
	                                                                    // bAllowFire-允许换档
	                                                                    // bAllowShift-允许点火
	ULMETHODBOOL  GetPerforationControl(bool& bAllowFire, bool& bAllowShift, bool& bAuto) = 0; // 射孔取芯控制，
	                                                                    // bAllowFire-换档有效
	                                                                    // bAllowShift-点火有效
	                                                                    // bAuto-自动取芯有效
	                                                    
	//ULMETHODBOOL  IsFireAllow() = 0;  //点火是否被允许 
};

// 接口ID

// {B6EED056-F0BA-46fe-952B-93E44BF72D86}
static const IID IID_ITool2 = 
{ 0xb6eed056, 0xf0ba, 0x46fe, { 0x95, 0x2b, 0x93, 0xe4, 0x4b, 0xf7, 0x2d, 0x86 } };

#endif
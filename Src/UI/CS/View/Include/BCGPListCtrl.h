#if !defined(AFX_BCGLISTCTRL_H__50D57A86_4742_41F6_BE36_37DCEBDCDA13__INCLUDED_)
#define AFX_BCGLISTCTRL_H__50D57A86_4742_41F6_BE36_37DCEBDCDA13__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//*******************************************************************************
// COPYRIGHT NOTES
// ---------------
// This is a part of the BCGControlBar Library
// Copyright (C) 1998-2000 BCGSoft Ltd.
// All rights reserved.
//
// This source code can be used, distributed or modified
// only under terms and conditions 
// of the accompanying license agreement.
//*******************************************************************************
// BCGListCtrl.h : header file
//

//#include "BCGCBPro.h"
#include "BCGpHeaderCtrl.h"
#ifndef BASECTRL_API
	#ifdef BASECTRL_EXPORT
		#define BASECTRL_API __declspec( dllexport )
	#else
		#define BASECTRL_API __declspec( dllimport )
	#endif
#endif
/////////////////////////////////////////////////////////////////////////////
// CBCGPListCtrl window

class BASECTRL_API CBCGPListCtrl : public CMFCListCtrl
{
	DECLARE_DYNAMIC(CBCGPListCtrl)

// Construction
public:
	CBCGPListCtrl();

// Attributes
public:
	virtual CBCGPHeaderCtrl& GetHeaderCtrl ()
	{
		return m_wndHeader;
	}

	// Mark sorted column by background color
	void EnableMarkSortedColumn (BOOL bMark = TRUE, BOOL bRedraw = TRUE);

public:
	CStringList		m_lstOptions;

protected:
	CBCGPHeaderCtrl	m_wndHeader;
	int				m_iSortedColumn;
	BOOL			m_bAscending;
	BOOL			m_bMarkSortedColumn;
	COLORREF		m_clrSortedColumn;
	HFONT			m_hOldFont;
	int				m_iFocusColumn;
	CWnd*			m_pWndInPlace;
	CComboBox*		m_pWndCombo;
	int				m_iEditRow;
	int				m_iEditColumn;
	int				m_nEdit[256];
	CList<int,int>	m_lstReadonly;
	CList<int,int>	m_lstComboRow;
	CRect			m_rectButton;
	BOOL			m_bButtonIsDown;
	BOOL			m_bDrawGridLine;
	CMap<DWORD,DWORD,COLORREF,COLORREF>	m_mapTxColors;
	CMap<DWORD,DWORD,COLORREF,COLORREF>	m_mapBkColors;

	DWORD			m_dwDragStyle;
	CImageList*		m_pDragImage;
	BOOL			m_bDragging;
	int				m_nDragIndex, 
					m_nDropIndex;
	int				m_iDropRow;
	CWnd*			m_pDropWnd;
	CPoint			m_ptDropPoint;

	// print
	HFONT		m_hPrinterFont;	// if NULL, mirror display font
	HFONT		m_hMirrorFont;	// font object used when mirroring
	int			m_nCharWidth;
	int			m_nRowHeight;
	int			m_nRowsPerPage;
	int			m_nHighlight;
	CStringList m_lstHeader;

// Operations
public:
	static int GetSelectedItems(CListCtrl* pList, CUIntArray& nSelItems);
	static int GetSelectedItemText(CListCtrl* pList, CString& strText);

	// Sorting operations:
	virtual void Sort (int iColumn, BOOL bAscending = TRUE, BOOL bAdd = FALSE);
	void SetSortColumn (int iColumn, BOOL bAscending = TRUE, BOOL bAdd = FALSE);
	void RemoveSortColumn (int iColumn);
	void EnableMultipleSort (BOOL bEnable = TRUE);
	BOOL IsMultipleSort () const;
	
	virtual CWnd* CreateInPlaceEdit (CRect rectEdit, int iRow, int iColumn);
	CComboBox* CreateCombo(CRect rect, int iRow, int iColumn);
	BOOL PrepareControl(DWORD dwStyleEx = LVS_EX_FULLROWSELECT|LVS_EX_HEADERDRAGDROP);
	void MakeColumnVisible(int iColumn);
	BOOL GetCellRect(int iRow, int iColumn, LPRECT lpRect);
	void EnableEditColumn(int iColumn, BOOL bEdit = TRUE, BOOL bReadonly = FALSE);
	void EnableComboRow(int iRow, BOOL bCombo = TRUE);
	void EnableComboColumn(int iColumn, BOOL bCombo = TRUE);
	void EnableBrowseColumn(int iColumn, BOOL bBrowse = TRUE);
	void EditColumn(int iColumn, int nEnable = 16);

	void EnableDrag(DWORD dwDragStyle = 1)
	{
		m_dwDragStyle = dwDragStyle;
	}

	void DropItemTo(int& nDragIndex, int& nDropIndex);
	
	void DeleteSelectedItems();

	void DrawGridLine(BOOL bDraw = TRUE)
	{
		m_bDrawGridLine = bDraw;
	}
	
	void SetTextColor(int nRow, int nColum, COLORREF clr)
	{
		DWORD dwKey = MAKELONG(nRow, nColum);
		m_mapTxColors[dwKey] = clr;
	}

	void SetBkColor(int nRow, int nColum, COLORREF clr)
	{
		DWORD dwKey = MAKELONG(nRow, nColum);
		m_mapBkColors[dwKey] = clr;
	}

	// -----
	// Print
	// -----
	void SetPrinterFont(CFont* pFont);
	void Print ();
	void SetHeaderString(CStringList* pListHeader);

// Overrides
	virtual int OnCompareItems (LPARAM lParam1, LPARAM lParam2, int iColumn);

	// Support for individual cells text/background colors:
	virtual COLORREF OnGetCellTextColor (int nRow, int nColum)
	{
		DWORD dwKey = MAKELONG(nRow, nColum);
		COLORREF clr;
		if (m_mapTxColors.Lookup(dwKey, clr))
			return clr;

		return GetTextColor ();
	}

	virtual COLORREF OnGetCellBkColor (int nRow, int nColum)
	{
		DWORD dwKey = MAKELONG(nRow, nColum);
		COLORREF clr;
		if (m_mapBkColors.Lookup(dwKey, clr))
			return clr;

		return GetBkColor ();
	}

	virtual HFONT OnGetCellFont (int /*nRow*/, int /*nColum*/, DWORD /*dwData*/ = 0)
	{
		return NULL;
	}

	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/) { }
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void PrintHeader(CDC* pDC, CPrintInfo* pInfo);
	virtual void PrintFooter(CDC* pDC, CPrintInfo* pInfo);
	virtual int DrawRow(CDC* pDC, int nItem);

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBCGPListCtrl)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CBCGPListCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CBCGPListCtrl)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnColumnClick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSysColorChange();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg BOOL OnEndLabelEdit(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBeginDrag(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	//}}AFX_MSG
	afx_msg void OnCustomDraw(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg LRESULT OnStyleChanged(WPARAM wp, LPARAM lp);
	afx_msg void OnEditKillFocus();
	afx_msg void OnSelectCombo();
	afx_msg void OnComboKillFocus();
	afx_msg void OnCloseCombo();
	DECLARE_MESSAGE_MAP()

	static int CALLBACK CompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
	BOOL InitList ();
	void InitColors ();
	int IndexToOrder (int iIndex);
	virtual void InitHeader ();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BCGLISTCTRL_H__50D57A86_4742_41F6_BE36_37DCEBDCDA13__INCLUDED_)

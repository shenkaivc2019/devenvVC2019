// sizecbar.cpp : implementation file
//

#include "stdafx.h"
#include "scbargEx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//// ボタンサイズ
//#define		BUTTONSIZE_CX	(56)
//#define		BUTTONSIZE_CY	(40)
//
//// イメ�`ジサイズ
//#define		IMAGESIZE_CX	(22)
//#define		IMAGESIZE_CY	(20)
//
//#define		VW_REBAR_NOSTR	("")
//
//#define		STRING_WITDH		16

// ボタンサイズ
#define		BUTTONSIZE_CX	(16)
#define		BUTTONSIZE_CY	(16)

// イメ�`ジサイズ
#define		IMAGESIZE_CX	(16)
#define		IMAGESIZE_CY	(16)

#define		VW_REBAR_NOSTR	("")

#define		STRING_WITDH		0


int CALLBACK EnumFontFamProcEx(ENUMLOGFONT FAR *lpelf,
                             NEWTEXTMETRIC FAR *lpntm,
                             int FontType,
                             LPARAM lParam)
{
    UNUSED_ALWAYS(lpelf);
    UNUSED_ALWAYS(lpntm);
    UNUSED_ALWAYS(FontType);
    UNUSED_ALWAYS(lParam);

    return 0;
}

/////////////////////////////////////////////////////////////////////////
// CCoolBarEx

IMPLEMENT_DYNAMIC(CCoolBarEx, baseCCoolBarEx);

CCoolBarEx::CCoolBarEx()
{
	m_hNotifyWnd = NULL;
    m_cyGripper = 12;
	SetSCBStyle(GetSCBStyle() |	SCBS_SIZECHILD);
	    m_bActive = FALSE;
    CDC dc;
    dc.CreateCompatibleDC(NULL);
    m_sFontFace = (::EnumFontFamilies(dc.m_hDC,
        _T("卜悶"), (FONTENUMPROC) EnumFontFamProcEx, 0) == 0) ?
        _T("卜悶") : _T("Tahoma");
    dc.DeleteDC();

	m_info = NULL;
	m_res_id = NULL;
}

CCoolBarEx::~CCoolBarEx()
{
}

BEGIN_MESSAGE_MAP(CCoolBarEx, baseCCoolBarEx)
    //{{AFX_MSG_MAP(CCoolBarEx)
    ON_WM_NCLBUTTONUP()
    ON_WM_NCHITTEST()
    //}}AFX_MSG_MAP
    ON_MESSAGE(WM_SETTEXT, OnSetText)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////
// CCoolBarEx message handlers

BOOL CCoolBarEx::Create(LPCTSTR lpszWindowName, CWnd* pParentWnd,
                               CSize sizeDefault, BOOL bHasGripper,
                               UINT nID, DWORD dwStyle, CVwCoolbarItemEx* pInfo)
{
	ASSERT_VALID(pParentWnd);
	m_hNotifyWnd = pParentWnd->m_hWnd;

	m_info = pInfo;

    return baseCCoolBarEx::Create(lpszWindowName, pParentWnd, sizeDefault, bHasGripper, nID, dwStyle);
}

BOOL CCoolBarEx::Create(LPCTSTR lpszWindowName,
                               CWnd* pParentWnd, UINT nID,
                               DWORD dwStyle)
{
	ASSERT_VALID(pParentWnd);
	m_hNotifyWnd = pParentWnd->m_hWnd;

	return baseCCoolBarEx::Create(lpszWindowName, pParentWnd, nID, dwStyle);
}

/////////////////////////////////////////////////////////////////////////
// Mouse Handling
//

void CCoolBarEx::OnNcLButtonUp(UINT nHitTest, CPoint point)
{
    if (nHitTest == HTCLOSE)
        m_pDockSite->ShowControlBar(this, FALSE, FALSE); // hide

    baseCCoolBarEx::OnNcLButtonUp(nHitTest, point);
}

#ifndef COLOR_GRADIENTACTIVECAPTION
#define COLOR_GRADIENTACTIVECAPTION     27
#define COLOR_GRADIENTINACTIVECAPTION   28
#define SPI_GETGRADIENTCAPTIONS         0x1008
#endif

void CCoolBarEx::NcCalcClient(LPRECT pRc, UINT nDockBarID)
{
    CRect rcBar(pRc); // save the bar rect

    // subtract edges
    baseCCoolBarEx::NcCalcClient(pRc, nDockBarID);

    if (!HasGripper())
        return;

    CRect rc(pRc); // the client rect as calculated by the base class

    BOOL bHorz = (nDockBarID == AFX_IDW_DOCKBAR_TOP) ||
                 (nDockBarID == AFX_IDW_DOCKBAR_BOTTOM);

    if (bHorz)
        rc.DeflateRect(m_cyGripper, 0, 0, 0);
    else
        rc.DeflateRect(0, m_cyGripper, 0, 0);

    // set position for the "x" (hide bar) button
    CPoint ptOrgBtn;
    if (bHorz)
        ptOrgBtn = CPoint(rc.left - 14, rc.top);
    else
        ptOrgBtn = CPoint(rc.right - 12, rc.top - 14);

    m_biHide.Move(ptOrgBtn - rcBar.TopLeft());

    *pRc = rc;
}

void CCoolBarEx::NcPaintGripper(CDC* pDC, CRect rcClient)
{
    if (!HasGripper())
        return;
#ifndef _SCB_STYLE_FLAT
    CRect gripper = rcClient;
    CRect rcbtn = m_biHide.GetRect();
    BOOL bHorz = IsHorzDocked();

    gripper.DeflateRect(1, 1);
    if (bHorz)
    {   // gripper at left
        gripper.left -= m_cyGripper;
        gripper.right = gripper.left + 3;
        gripper.top = rcbtn.bottom + 3;
    }
    else
    {   // gripper at top
        gripper.top -= m_cyGripper;
        gripper.bottom = gripper.top + 3;
        gripper.right = rcbtn.left - 3;
    }

    pDC->Draw3dRect(gripper, ::GetSysColor(COLOR_BTNHIGHLIGHT),
        ::GetSysColor(COLOR_BTNSHADOW));

    gripper.OffsetRect(bHorz ? 3 : 0, bHorz ? 0 : 3);

    pDC->Draw3dRect(gripper, ::GetSysColor(COLOR_BTNHIGHLIGHT),
        ::GetSysColor(COLOR_BTNSHADOW));

    m_biHide.Paint(pDC);

#else

    // compute the caption rectangle
    BOOL bHorz = IsHorzDocked();
    CRect rcGrip = rcClient;
    CRect rcBtn = m_biHide.GetRect();
    if (bHorz)
    {   // right side gripper
        rcGrip.left -= m_cyGripper + 1;
        rcGrip.right = rcGrip.left + 11;
        rcGrip.top = rcBtn.bottom + 3;
    }
    else
    {   // gripper at top
        rcGrip.top -= m_cyGripper + 1;
        rcGrip.bottom = rcGrip.top + 11;
        rcGrip.right = rcBtn.left - 3;
    }
    rcGrip.InflateRect(bHorz ? 1 : 0, bHorz ? 0 : 1);

    // draw the caption background
    //CBrush br;
    COLORREF clrCptn = m_bActive ?
        ::GetSysColor(COLOR_ACTIVECAPTION) :
        ::GetSysColor(COLOR_INACTIVECAPTION);

    // query gradient info (usually TRUE for Win98/Win2k)
    BOOL bGradient = FALSE;
    ::SystemParametersInfo(SPI_GETGRADIENTCAPTIONS, 0, &bGradient, 0);
    
    if (!bGradient)
        pDC->FillSolidRect(&rcGrip, clrCptn); // solid color
    else
    {
        // gradient from left to right or from bottom to top
        // get second gradient color (the right end)
        COLORREF clrCptnRight = m_bActive ?
            ::GetSysColor(COLOR_GRADIENTACTIVECAPTION) :
            ::GetSysColor(COLOR_GRADIENTINACTIVECAPTION);

        // this will make 2^6 = 64 fountain steps
        int nShift = 6;
        int nSteps = 1 << nShift;

        for (int i = 0; i < nSteps; i++)
        {
            // do a little alpha blending
            int nR = (GetRValue(clrCptn) * (nSteps - i) +
                      GetRValue(clrCptnRight) * i) >> nShift;
            int nG = (GetGValue(clrCptn) * (nSteps - i) +
                      GetGValue(clrCptnRight) * i) >> nShift;
            int nB = (GetBValue(clrCptn) * (nSteps - i) +
                      GetBValue(clrCptnRight) * i) >> nShift;

            COLORREF cr = RGB(nR, nG, nB);

            // then paint with the resulting color
            CRect r2 = rcGrip;
            if (bHorz)
            {
                r2.bottom = rcGrip.bottom - 
                    ((i * rcGrip.Height()) >> nShift);
                r2.top = rcGrip.bottom - 
                    (((i + 1) * rcGrip.Height()) >> nShift);
                if (r2.Height() > 0)
                    pDC->FillSolidRect(r2, cr);
            }
            else
            {
                r2.left = rcGrip.left + 
                    ((i * rcGrip.Width()) >> nShift);
                r2.right = rcGrip.left + 
                    (((i + 1) * rcGrip.Width()) >> nShift);
                if (r2.Width() > 0)
                    pDC->FillSolidRect(r2, cr);
            }
        }
    }

    // draw the caption text - first select a font
    CFont font;
    int ppi = pDC->GetDeviceCaps(LOGPIXELSX);
    int pointsize = MulDiv(96, 96, ppi); // 8.5 points at 96 ppi

    LOGFONT lf;
    BOOL bFont = font.CreatePointFont(pointsize, m_sFontFace);
    if (bFont)
    {
        // get the text color
        COLORREF clrCptnText = m_bActive ?
            ::GetSysColor(COLOR_CAPTIONTEXT) :
            ::GetSysColor(COLOR_INACTIVECAPTIONTEXT);

        int nOldBkMode = pDC->SetBkMode(TRANSPARENT);
        COLORREF clrOldText = pDC->SetTextColor(clrCptnText);

        if (bHorz)
        {
            // rotate text 90 degrees CCW if horizontally docked
            font.GetLogFont(&lf);
            font.DeleteObject();
            lf.lfEscapement = 900;
            font.CreateFontIndirect(&lf);
        }
        
        CFont* pOldFont = pDC->SelectObject(&font);
        CString sTitle;
        GetWindowText(sTitle);

        CPoint ptOrg = bHorz ?
            CPoint(rcGrip.left - 1, rcGrip.bottom - 3) :
            CPoint(rcGrip.left + 3, rcGrip.top - 1);

        pDC->ExtTextOut(ptOrg.x, ptOrg.y,
            ETO_CLIPPED, rcGrip, sTitle, NULL);

        pDC->SelectObject(pOldFont);
        pDC->SetBkMode(nOldBkMode);
        pDC->SetTextColor(clrOldText);
    }

    // draw the button
    m_biHide.Paint(pDC);
#endif
}

LRESULT CCoolBarEx::OnNcHitTest(CPoint point)
{
    CRect rcBar;
    GetWindowRect(rcBar);

    UINT nRet = baseCCoolBarEx::OnNcHitTest(point);
    if (nRet != HTCLIENT)
        return nRet;

    CRect rc = m_biHide.GetRect();
    rc.OffsetRect(rcBar.TopLeft());
    if (rc.PtInRect(point))
        return HTCLOSE;

    return HTCLIENT;
}

/////////////////////////////////////////////////////////////////////////
// CCoolBarEx implementation helpers

void CCoolBarEx::OnUpdateCmdUI(CFrameWnd* pTarget,
                                      BOOL bDisableIfNoHndler)
{
    UNUSED_ALWAYS(bDisableIfNoHndler);
    UNUSED_ALWAYS(pTarget);

    if (!HasGripper())
        return;

    BOOL bNeedPaint = FALSE;

    CPoint pt;
    ::GetCursorPos(&pt);
    BOOL bHit = (OnNcHitTest(pt) == HTCLOSE);
    BOOL bLButtonDown = (::GetKeyState(VK_LBUTTON) < 0);

    BOOL bWasPushed = m_biHide.bPushed;
    m_biHide.bPushed = bHit && bLButtonDown;

    BOOL bWasRaised = m_biHide.bRaised;
    m_biHide.bRaised = bHit && !bLButtonDown;

	CWnd* pFocus = GetFocus();
    BOOL bActiveOld = m_bActive;
    m_bActive = (pFocus->GetSafeHwnd() && IsChild(pFocus));
    if (m_bActive != bActiveOld)
        bNeedPaint = TRUE;

    bNeedPaint |= (m_biHide.bPushed ^ bWasPushed) ||
                  (m_biHide.bRaised ^ bWasRaised);

    if (bNeedPaint)
        SendMessage(WM_NCPAINT);
}

/////////////////////////////////////////////////////////////////////////
// CSCBButtonEx

CSCBButtonEx::CSCBButtonEx()
{
    bRaised = FALSE;
    bPushed = FALSE;
}

void CSCBButtonEx::Paint(CDC* pDC)
{
    CRect rc = GetRect();

    if (bPushed)
        pDC->Draw3dRect(rc, ::GetSysColor(COLOR_BTNSHADOW),
            ::GetSysColor(COLOR_BTNHIGHLIGHT));
    else
        if (bRaised)
            pDC->Draw3dRect(rc, ::GetSysColor(COLOR_BTNHIGHLIGHT),
                ::GetSysColor(COLOR_BTNSHADOW));

    COLORREF clrOldTextColor = pDC->GetTextColor();
    pDC->SetTextColor(::GetSysColor(COLOR_BTNTEXT));
    int nPrevBkMode = pDC->SetBkMode(TRANSPARENT);
    CFont font;
    int ppi = pDC->GetDeviceCaps(LOGPIXELSX);
    int pointsize = MulDiv(60, 96, ppi); // 6 points at 96 ppi
    font.CreatePointFont(pointsize, _T("Marlett"));
    CFont* oldfont = pDC->SelectObject(&font);

    pDC->TextOut(ptOrg.x + 3, ptOrg.y + 3, CString(_T("r"))); // x-like

    pDC->SelectObject(oldfont);
    pDC->SetBkMode(nPrevBkMode);
    pDC->SetTextColor(clrOldTextColor);
}

BOOL CCoolBarEx::HasGripper() const
{
#if defined(_SCB_MINIFRAME_CAPTION) || !defined(_SCB_REPLACE_MINIFRAME)
    // if the miniframe has a caption, don't display the gripper
    if (IsFloating())
        return FALSE;
#endif //_SCB_MINIFRAME_CAPTION

    return TRUE;
}

LRESULT CCoolBarEx::OnSetText(WPARAM wParam, LPARAM lParam)
{
    LRESULT lResult = baseCCoolBarEx::OnSetText(wParam, lParam);

    SendMessage(WM_NCPAINT);

    return lResult;
}

BOOL CCoolBarEx::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
	// 繍WM_NOTIFY��連廬住公��連宥岑完笥侃尖

	::SendMessage(m_hNotifyWnd, WM_NOTIFY, wParam, lParam);
	*pResult = 0;
	
	return TRUE;
}

void CCoolBarEx::SetNotifyWindow(HWND hNotifyWnd)
{
	m_hNotifyWnd = hNotifyWnd;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL CCoolBarEx::OnCreateBands()
{
	// フォントの恬撹は、コンストラクタでやったほうが措いです。
	LOGFONT lf;

	CString strFontName;

	// フォント兆函誼 1999/5/25
	//if (strFontName.LoadString("RFX_IDS_COOLBARFONTNAME") == FALSE)
	{
		strFontName = "卜悶";
	}

	// フォントを恬撹する
	memset(&lf, 0, sizeof(lf));
	lf.lfHeight = -12;
	lf.lfWeight = 400;
	lf.lfCharSet = SHIFTJIS_CHARSET;
	lf.lfOutPrecision = CLIP_CHARACTER_PRECIS;
	lf.lfClipPrecision = CLIP_STROKE_PRECIS;
	lf.lfQuality = DRAFT_QUALITY;
	lf.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
	lstrcpy(lf.lfFaceName, strFontName);
	m_font.CreateFontIndirect(&lf);

	// �啜弔妨遒蟲韻討�
	if (m_info)
		return CreateToolbar(m_wndToolBar);

	// ツ�`ルバ�`リソ�`スを聞う
	else if (m_res_id)
		return CreateToolbar(m_wndToolBar, m_res_id, NULL);

	return FALSE;
}

///////////////////////////////////
// ク�`ルバ�`坪へツ�`ルバ�`を伏撹
///////////////////////////////////
BOOL CCoolBarEx::CreateToolbar(CMFCCToolBar& tb, UINT nBitmapID)
{
	// ツ�`ルバ�`伏撹
	if (!tb.Create(this, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_TOOLTIPS | CBRS_SIZE_DYNAMIC | CBRS_FLYBY))
	{
		return FALSE;
	}
	// m_pParentWnd
	//if (!tb.Create(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC)
	//	|| !tb.LoadToolBar(IDR_MAINFRAME))
	//{
	//	return FALSE;
	//}

	//if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
//	| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
//	!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))

	// フォントセット
	tb.SetFont(&m_font, FALSE);

	// フラットボタンへ�筝�
	tb.ModifyStyle(0, TBSTYLE_FLAT);

	// m_infoをもとにボタンを弖紗。
	int i;

	//CMFCCToolBar& ctrl = &tb;
	////LPVW_COOLBAR_INFO p_info;
	//VW_COOLBAR_INFO info;

	//TBBUTTON button;

	//// メニュ�`を函誼しておく。 9/18
	//CMenu* pMenu = NULL;
	//if (GetParentOwner())
	//{
	//	pMenu = GetParentOwner()->GetMenu();
	//}
	//int max, nButtonCnt;

	//max = m_info->GetSize();
	//nButtonCnt = -1;

	//BOOL bIsSep = TRUE;			// 1��まえのボタンがセパレ�`タですか?
	//CDC* pDC = ctrl.GetDC();	// デバイスコンテキスト

	//pDC->SelectObject(m_font);	// フォントをセットする

	//int nSpacePixel;

	//// スペ�`ス１��蛍のピクセル方を��麻する
	//if (pDC)
	//{
	//	//ABC abc;
	//	CSize size;
	//	//if (pDC->GetCharABCWidths(0x20, 0x20, &abc))
	//	size = pDC->GetTextExtent(" ", 1);
	//	nSpacePixel = size.cx;
	//}
	//for (i = 0; i < max; i++)
	//{
	//	int image;
	//	CString strHint;

	//	info = (*m_info)[i];

	//	// メニュ�`に卆贋するボタンは��揖コマンドIDがメニュ�`に
	//	// 贋壓するかチェックする。なかったら弖紗しない。
	//	if (info.nBitmapID && info.bIsDependMenu && pMenu)
	//	{
	//		if (pMenu->GetMenuState(info.nCommandID, MF_BYCOMMAND) == (UINT)-1)
	//		{
	//			continue;
	//		}
	//	}

	//	// イメ�`ジを隔ったボタン
	//	if (info.nBitmapID)
	//		image = ctrl.AddBitmap(1, info.nBitmapID);
	//	// セパレ�`タ
	//	else
	//		image = NULL;

	//	if (image > -1)
	//	{
	//		button.iBitmap = image;
	//		button.idCommand = info.nCommandID;
	//		button.fsState = TBSTATE_ENABLED;
	//		button.fsStyle = (info.nBitmapID) ? TBSTYLE_BUTTON : TBSTYLE_SEP;
	//		button.iString = NULL;

	//		// セパレ�`タが�B�Aしている��栽はカットする 9/18
	//		if (!info.nBitmapID && bIsSep)
	//			continue;

	//		if (ctrl.AddButtons(1, &button) == FALSE)
	//		{	// 弖紗払��
	//			continue;
	//		}
	//		nButtonCnt++;
	//		bIsSep = !(info.nBitmapID);

	//		if (info.lpszTitle == NULL && info.nBitmapID)
	//		{
	//			CString str;

	//			// リソ�`スから猟忖双を�iみ�zむ。
	//			if (STRING_WITDH > 0 && str.LoadString(info.nCommandID))
	//			{
	//				int idx;

	//				// 匯桑恷瘁のセクションをラベルにします。
	//				if ((idx = str.ReverseFind('\n')) != -1)
	//				{
	//					strHint = str.Mid(idx + 1);
	//					info.lpszTitle = strHint.GetBuffer(STRING_WITDH + 1);
	//				}

	//				// ��つからなかったら燕幣しません。
	//				else
	//				{
	//					info.lpszTitle = VW_REBAR_NOSTR;
	//				}
	//			}
	//			else
	//			{
	//				info.lpszTitle = VW_REBAR_NOSTR;
	//			}
	//		}

	//		// 猟忖双�O協
	//		if (info.lpszTitle)
	//		{
	//			CSize size(BUTTONSIZE_CX + 1, 0);
	//			char szFmt[14];
	//			char szBuf[64];

	//			size = pDC->GetTextExtent(info.lpszTitle, lstrlen(info.lpszTitle));

	//			int nMaxCharPxcel = BUTTONSIZE_CX;

	//			if (size.cx < BUTTONSIZE_CX)
	//			{
	//				int space = (BUTTONSIZE_CX - size.cx) / 2;

	//				space = space / nSpacePixel;

	//				// "%6c%s%6cみたいなフォ�`マット恬撹
	//				if (space > 0)
	//				{
	//					wsprintf(szFmt, "%%%dc%%s%%%dc", space, space);
	//					wsprintf(szBuf, szFmt, 0x20, info.lpszTitle, 0x20);
	//				}

	//				// �{屁せずに猟忖双をそのまま�駻� (1999/6/30)
	//				else
	//					lstrcpy(szBuf, info.lpszTitle);

	//				size = pDC->GetTextExtent(szBuf, lstrlen(szBuf));

	//			}

	//			// BUTTONSIZE_CXになるように�{屁
	//			else
	//			{
	//				lstrcpy(szBuf, info.lpszTitle);
	//				do
	//				{
	//					int nLen = lstrlen(szBuf);

	//					szBuf[nLen - 1] = '\0';

	//					size = pDC->GetTextExtent(szBuf, nLen);
	//				} while (size.cx > BUTTONSIZE_CX);
	//			}
	//			if (STRING_WITDH > 0)
	//				tb.SetButtonText(nButtonCnt, szBuf);
	//		}
	//	}
	//}
	//if (pDC)
	//	ReleaseDC(pDC);

	//// ク�`ルバ�`へツ�`ルバ�`を弖紗
	//InsertBandToolbar(tb, nBitmapID);

	//// ツ�`ル チップスサポ�`ト
	//tb.SetBarStyle(tb.GetBarStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);

	//// ツ�`ルバ�`サイズ�O協
	//tb.SendMessage(TB_SETBITMAPSIZE, 0, MAKELONG(IMAGESIZE_CX, IMAGESIZE_CY));

	return TRUE;
}

//////////////////////////////////////////
// リソ�`スを聞ってク�`ルバ�`を恬撹する。
//////////////////////////////////////////
BOOL CCoolBarEx::CreateToolbar(CMFCCToolBar& tb, UINT nIDResource, UINT nBitmapID)
{
	//// ツ�`ルバ�`伏撹
	//if (!tb.Create(this,
	//	WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN |
	//	CBRS_TOOLTIPS | CBRS_SIZE_DYNAMIC | CBRS_FLYBY) ||
	//	!tb.LoadToolBar(nIDResource))
	//{
	//	return FALSE;
	//}

	//// フォントセット
	//tb.SetFont(&m_font, FALSE);

	//// フラットボタンへ�筝�
	//tb.ModifyStyle(0, TBSTYLE_FLAT);

	//// 猟忖双の峺協
	//CToolBarCtrl& ctrl = tb.GetToolBarCtrl();
	//int i, max;
	//TBBUTTON button;

	//max = ctrl.GetButtonCount();

	//CDC* pDC = GetDC();

	//if (pDC == NULL)
	//{
	//	return FALSE;
	//}

	//for (i = 0; i < max; i++)
	//{
	//	ctrl.GetButton(i, &button);

	//	if (button.idCommand)
	//	{
	//		CString strHint;
	//		CString str;
	//		char* szTitle;

	//		// リソ�`スから猟忖双を�iみ�zむ。
	//		if (str.LoadString(button.idCommand))
	//		{
	//			int idx;
	//			// 匯桑恷瘁のセクションをラベルにします。
	//			if ((idx = str.ReverseFind('\n')) != -1)
	//			{
	//				strHint = str.Mid(idx + 1);
	//				szTitle = strHint.GetBuffer(STRING_WITDH + 1);
	//			}
	//			// ��つからなかったら燕幣しません。
	//			else
	//			{
	//				szTitle = VW_REBAR_NOSTR;
	//			}
	//			// 猟忖双�O協
	//			if (szTitle)
	//			{
	//				CSize size;
	//				char szFmt[12];
	//				char szBuf[STRING_WITDH + 2];
	//				// 2002.01.10 Ver2.11 VC6���� Start
	//				size = pDC->GetTextExtent(szTitle, lstrlen(szTitle));
	//				int space = (BUTTONSIZE_CX - size.cx) / 2;
	//				//					size = pDC->GetTextExtent(szTitle, lstrlen(szTitle));
	//									// End
	//				space = (space + 7) / 8;
	//				// "%6c%s%6cみたいなフォ�`マット恬撹
	//				wsprintf(szFmt, "%%%dc%%s%%%dc", space, space);
	//				wsprintf(szBuf, szFmt, 0x20, szTitle, 0x20);
	//				tb.SetButtonText(i, szBuf);
	//			}
	//		}
	//	}
	//	else
	//	{
	//		tb.SetButtonText(i, VW_REBAR_NOSTR);
	//	}
	//}

	//if (pDC)
	//	ReleaseDC(pDC);

	//// ク�`ルバ�`へツ�`ルバ�`を弖紗
	//InsertBandToolbar(tb, nBitmapID);

	//// ツ�`ル チップスサポ�`ト
	//tb.SetBarStyle(tb.GetBarStyle() |
	//	CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);

	return TRUE;
}

//-------------------------------------------------------------------
// ク�`ルバ�`へツ�`ルバ�`を弖紗
//-------------------------------------------------------------------
BOOL CCoolBarEx::InsertBandToolbar(CMFCCToolBar& tb, UINT nBitmapID)
{
	// 嫌恷弌サイズ函誼
	CSize szHorz = tb.CalcFixedLayout(-1, LM_HORZ);

	// バックグラウンドのビットマップ�iみ�zみ
	if (nBitmapID > 0)
	{
		VERIFY(m_bmBackground.LoadBitmap(nBitmapID));
	}

	// ツ�`ルバ�`弖紗
	CRebarBandInfo rbbi;
	rbbi.fMask = RBBIM_STYLE | RBBIM_CHILD | RBBIM_CHILDSIZE |
		RBBIM_BACKGROUND | RBBIM_COLORS;

	rbbi.fStyle = RBBS_FIXEDBMP | RBBS_GRIPPERALWAYS; 	// グリッパ�`を竃す。1998/8/7
	rbbi.hwndChild = tb;
	rbbi.cxMinChild = szHorz.cx;

	rbbi.cyMinChild = BUTTONSIZE_CY - 1;

	rbbi.hbmBack = m_bmBackground;
	rbbi.clrFore = GetSysColor(COLOR_BTNTEXT);
	rbbi.clrBack = GetSysColor(COLOR_BTNFACE);

	if (!InsertBand(-1, &rbbi))
	{
		return FALSE;
	}

	return TRUE;
}

//-------------------------------------------------------------------
// ツ�`ルバ�`クラス
//-------------------------------------------------------------------
/////////////////////////////////////////////////////////////////////
//  CMFCCToolBar
IMPLEMENT_DYNAMIC(CMFCCToolBar, CMFCToolBar)
BEGIN_MESSAGE_MAP(CMFCCToolBar, CMFCToolBar)
	ON_WM_NCCREATE()
	ON_WM_NCPAINT()
	ON_WM_PAINT()
	ON_WM_NCCALCSIZE()
END_MESSAGE_MAP()
//ON_MESSAGE(TB_AUTOSIZE, OnAutoSize)
//ON_WM_SIZE()

CMFCCToolBar::CMFCCToolBar()
{
	//m_bDelayedButtonLayout = FALSE;
}
CMFCCToolBar::~CMFCCToolBar()
{
}

/////////////////////////////////////////////////////////////////////
//  メッセ�`ジハンドラ
BOOL CMFCCToolBar::OnNcCreate(LPCREATESTRUCT lpcs)
{
	CFrameWnd* pFrame = GetParentFrame();
	ASSERT_VALID(pFrame);
	SetOwner(pFrame);
	return CMFCToolBar::OnNcCreate(lpcs);
}

void CMFCCToolBar::OnNcPaint()
{
	Default();
}

void CMFCCToolBar::OnPaint()
{
	Default();
}

//-------------------------------------------------------------------
//
//-------------------------------------------------------------------
void CMFCCToolBar::OnNcCalcSize(BOOL, NCCALCSIZE_PARAMS*)
{
	Default();
}
///////////////////////////////////////////////////////////////////////////////////////////////////
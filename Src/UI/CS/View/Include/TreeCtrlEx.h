#if !defined(AFX_TREECTRLEX_H__2604D499_7702_4A9E_8DC7_E120A85B383D__INCLUDED_)
#define AFX_TREECTRLEX_H__2604D499_7702_4A9E_8DC7_E120A85B383D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifndef BASECTRL_API
	#ifdef BASECTRL_EXPORT
		#define BASECTRL_API __declspec( dllexport )
	#else
		#define BASECTRL_API __declspec( dllimport )
	#endif
#endif
// Image List Index
#define		IDI_IPROJECT			1
#define		IDI_SPROJECT			1
#define		IDI_IPROINFO			2
#define		IDI_SPROINFO			2
#define		IDI_IWELLINF			3
#define     IDI_SWELLINF			3
#define		IDI_ISCOUT				4
#define		IDI_SSCOUT				4
#define		IDI_ISERVICE			7
#define		IDI_SSERVICE			7
#define		IDI_IPRINTTEMPL			8
#define		IDI_SPRINTTEMPL			8
#define		IDI_ITOOLCMB			9
#define		IDI_STOOLCMB			9
#define		IDI_ITOOL				11
#define		IDI_STOOL				11
#define		IDI_IPRESENTATION		12
#define		IDI_SPRESENTATION		12
#define		IDI_IGRAPH				14
#define		IDI_SGRAPH				14
#define		IDI_IHEADER				13
#define		IDI_SHEADER				13
#define		IDI_IULFILE				17
#define		IDI_SULFILE				17
#define		IDI_ICELL				18
#define		IDI_SCELL				18
#define		IDI_IOBJECT				19
#define		IDI_SOBJECT				19
#define		IDI_IGLOBAL				20
#define		IDI_SGLOBAL				20
#define		IDI_ITRACK				21
#define		IDI_STRACK				21
#define		IDI_IDATA				23
#define		IDI_SDATA				23
#define		IDI_IPRIFILE			25
#define		IDI_SPRIFILE			25
#define     IDI_ITYPE				26
#define     IDI_STYPE				26
#define		IDI_IINRFILE			27
#define     IDI_SINRFILE			27
#define		IDI_ICVS				28
#define		IDI_SCVS				28
#define     IDI_ICUSTOME			29
#define     IDI_SCUSTOME			29
#define     IDI_IDATA2				30
#define     IDI_SDATA2				30
#define     IDI_FILE_NODE			31 
// 2020.2.8 Ver1.6.0 TASK??002?? Start
#define     IDI_WELLSITE_INFO		32
#define     IDI_WELL_INFO			33
#define     IDI_HOLE_INFO			34
#define     IDI_RUN_INFO			35
#define     IDI_WELLSITE_ITEM		36
#define     IDI_WELL_ITEM			37
#define     IDI_HOLE_ITEM			38
#define     IDI_RUN_ITEM			39
// 2020.2.7 Ver1.6.0 TASK??002?? End
// TreeCtrlEx.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTreeCtrlEx window

class BASECTRL_API CTreeCtrlEx : public CTreeCtrl
{
	// Construction
public:
	CTreeCtrlEx();

	// Attributes
public:
	BOOL SetItemFont(HTREEITEM hItem, LOGFONT& logfont);
	BOOL SetItemBold(HTREEITEM hItem, BOOL bBold);
	BOOL SetItemColor(HTREEITEM hItem, COLORREF color);
	BOOL GetItemFont(HTREEITEM hItem, LOGFONT* plogfont);
	BOOL GetItemBold(HTREEITEM hItem);
	COLORREF GetItemColor(HTREEITEM hItem);
	int GetItemImg(HTREEITEM hItem, BOOL bSel = FALSE);

#ifdef _MULTI_SEL
	HTREEITEM FindNextItem(HTREEITEM hItem);
	BOOL SelectItems(HTREEITEM hItemFrom, HTREEITEM hItemTo);
	int ClearSelection();
	int GetSelectedItems(CPtrList* pItems);
#endif
	
protected:
	struct Color_Font
	{
		COLORREF color;
		LOGFONT logfont;
	};
	CMap<void*, void*, Color_Font, Color_Font&> m_mapColorFont;

#ifdef _MULTI_SEL
	HTREEITEM m_hItemFSel;
#endif

	// Operations
public:
	BOOL ExpandAll(HTREEITEM hItem, UINT nCode = TVE_EXPAND);
	
	HTREEITEM InsItem(UINT nID, int nImage, int nSelectedImage,
		HTREEITEM hParent = TVI_ROOT, HTREEITEM hInsertAfter = TVI_LAST);

	void DeleteChildItems(HTREEITEM hItem);

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTreeCtrlEx)
	protected:
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	//}}AFX_VIRTUAL

	// Implementation
public:
	virtual ~CTreeCtrlEx();
	BOOL MoveItem(HTREEITEM hItem, BOOL bDown = TRUE);
	HTREEITEM CopyItem(HTREEITEM hItem, HTREEITEM hNewParent, HTREEITEM hAfter = TVI_LAST);

	// Generated message map functions
protected:

	//{{AFX_MSG(CTreeCtrlEx)
	afx_msg void OnPaint();
	afx_msg void OnDeleteItem(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemExpanded(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
#ifdef	_MULTI_SEL
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
#endif
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TREECTRLEX_H__2604D499_7702_4A9E_8DC7_E120A85B383D__INCLUDED_)

﻿// ServiceTableItem.h: interface for the CServiceTableItem class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERVICETABLEITEM_H__19185813_B1B1_4D2E_BA67_B8D61705D3B9__INCLUDED_)
#define AFX_SERVICETABLEITEM_H__19185813_B1B1_4D2E_BA67_B8D61705D3B9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AllInfoPack.h"
#include "ULInterface.h"
#include "ULBaseDoc.h"
#include "PrintOrderFile.h"
#ifndef ULVWBASE_API
	#ifdef ULVWBASE‌_EXPORT
		#define ULVWBASE_API __declspec( dllexport )
	#else	
		#define ULVWBASE_API __declspec( dllimport )
	#endif
#endif
typedef struct tagToolsCommand
{
	CString strCommandName;
	BYTE    cmd[64];
	UINT    nCmdRLen;
	BOOL    bRead;
	tagToolsCommand()
	{
		nCmdRLen=0;
		ZeroMemory(cmd,sizeof(cmd));
	}
    void    Serialize(CXMLSettings& xml)
	{
		if(xml.IsStoring())
		{
			xml.Write(_T("CommandName"),strCommandName);
			xml.Write(_T("CommandData"),cmd,sizeof(cmd));
			if(nCmdRLen<0||nCmdRLen>64)
			{
				nCmdRLen=0;
			}
			xml.Write(_T("CommandRLen"),nCmdRLen);
			xml.Back();
		}
		else
		{
			
			xml.Read(_T("CommandName"),strCommandName);
			UINT  nLen = sizeof(cmd);
			xml.Read(_T("CommandData"),cmd,nLen);
			xml.Read(_T("CommandRLen"),nCmdRLen);
			if(nCmdRLen<0||nCmdRLen>64)
			{
				nCmdRLen=0;
			}
			xml.Back();	
		}
	}
}ToolsCommand;

typedef CList<ToolsCommand*,ToolsCommand*> ToolsCmdLst;

class CULTool;
class CSheet;
class CProject;
class CULKernel;
class CProjectBHAInfo;
// #define __TLIST

// 服务表项
class ULVWBASE_API CServiceTableItem : public CULBaseDoc
{

public:
	DECLARE_DYNAMIC(CServiceTableItem)
	CServiceTableItem();
	virtual ~CServiceTableItem();
	virtual void Serialize(CArchive& ar);

public:
	int			AddToolInfo(CToolInfoPack* pTIP);
	int         InsertAt(int nIndex, CToolInfoPack & ToolInfo); //Insert one item into the aray
	int         SetAt(int nIndex, CToolInfoPack &ToolInfo);     //Set the element with ...
	int			DelToolInfo(int nIndex);
	int         GetToolInfoCount();
	//CToolInfoPack* GetToolInfoPack(CString strToolName);
	CToolInfoPack* GetToolInfoPack(int nIndex);
	CCurveInfoPack* GetCurveInfoPack(int nTool, int nCurve);
	BOOL	WriteToStream(IStreamPtr& stream);
	BOOL	ReadFromStream(IStreamPtr& stream);
	BOOL	SaveContent(LPCTSTR lpStgName = "");
	BOOL	ReadContent(LPCTSTR lpStgName = "");
	int		ReadComboInfo(LPCTSTR lpStgName = "", UINT nType = 1);
	BOOL	SaveSetting(LPCTSTR lpSetting = "", LPCTSTR lpStgName = "", LPCTSTR lpStreamName = "Setting");
	BOOL	ReadSetting(LPCTSTR lpSetting = "", LPCTSTR lpStgName = "", LPCTSTR lpStreamName = "Setting");

	BOOL	RenameSheet(CString strSheet, CString strNewSheet);

	CString ComboSheetName()
	{
		return ComboTemplsName(&m_sheetiList);
	}

	CString ComboCellName()
	{
		return ComboTemplsName(&m_celliList);
	}

	CString ComboPrintOrderName()
	{
		POSITION pos = m_pofiList.GetHeadPosition();
		CString strRes = _T("");
		if (pos == NULL)
			return strRes;
		
		for ( ; pos != NULL; )
		{
			CPofInfo* pPof = m_pofiList.GetNext(pos);
			if (pPof != NULL)
			{
				if (strRes.GetLength())
					strRes += ",";
				strRes += pPof->strTempl;	
			}			
		}

		return strRes;
	}

	void		ComboToolName();	
	void		GetPrints(CStringArray* pArrPrint, CStringArray* pArrTempl);
	void		DeleteCurve(CCurve* pCurve);

	void		ResetTools();
	BOOL		ResetParams();

	virtual void OpenFrames(DWORD dwTypes);
	virtual void ShowView(DWORD dwType);
	virtual void GetRecord(CString& strDoc, CString& strTime, CString& strRecord,
		CString& strDepthST, CString& strDepthED, CSheet* pSheet);
	virtual void GetRecord2(CString& strComp, CString& strTeam, CString& strWell, CString& strField);
	
	BOOL		SaveToolsParam(CULKernel* pKernel);
	BOOL		LoadToolParam(CULTool* pTool);
	void		Load200(CArchive& ar);
	void Serialize(CXMLSettings& xml);

	void MoveTool(UINT iSrc, UINT iDest);
	void RefreshTool(UINT iTool);
	void SetTools(CPtrArray* pTools);
	void ClearTools();

	virtual BOOL LoadDataFromDB(LPCTSTR lpszServiceName = NULL);

public:
	static DWORD m_dwReset;

#ifdef __TLIST
	CPtrArray	m_Tools;
#else
	TOOLINFOPACKARRAY	m_ComboToolInfo;
#endif

public:
	void    ClearToolsCommand();
	void    SendToolsCommand();
	void    SendCommand(LPVOID pBuffer, UINT nNum, DWORD dwID, UINT nIndex = 0);
	VARIANT				m_toolstrings;
	CString				m_strItem;					// 表项名称
	CString				m_strTools;					// 仪器组合
	ULLogInfo			m_logInfo;                  // 测井信息
	int					m_nLogType;                 // log type : open hole...
	int                 m_nDriveMode;
	long				m_lDepthStart;				// the start depth / time
	long				m_lDepthEnd;				// the end   depth / time
	BOOL				m_bEnabled;					// 该服务项目是否能够被使用，不能用：FALSE、能够被使用：TRUE
	BOOL				m_bSel;                     // 选中标识
	BOOL				m_bActivated;				// 激活标识
	CTime				m_tmLogging;				// 测井时间
    ToolsCmdLst         m_lstToolsCmd;              // 仪器串命令列表
	CStringArray        m_arrTargetBoards;          // 命令发送到的板卡
	CString				m_strToolName,m_strCurveName;//保存选中的仪器名称，曲线名称
	long				m_lAliCurDepOffSet;
	BOOL				m_bTop,m_bAlignCurve;		//记录服务项深度偏移状态
	// 2020.5.12 ltg Start
	CProjectBHAInfo* m_pProjectBHAInfo;
	IDB* m_pIDB;
	// 2020.5.12 ltg End
};

#define SRV_FILE(strTempl) (AfxGetComConfig()->m_strAppPath+"\\Template\\Services\\"+(strTempl)+".uls")


#endif // !defined(AFX_SERVICETABLEITEM_H__19185813_B1B1_4D2E_BA67_B8D61705D3B9__INCLUDED_)

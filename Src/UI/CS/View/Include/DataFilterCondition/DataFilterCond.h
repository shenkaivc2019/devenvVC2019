﻿// DataFilterCond.h: interface for the CDataFilterCond class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DATAFILTERCOND_H__CE7834E7_DA37_492F_B803_1C2A9E5DACB6__INCLUDED_)
#define AFX_DATAFILTERCOND_H__CE7834E7_DA37_492F_B803_1C2A9E5DACB6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "IDB.h"
#include <string>
#include <map>
using namespace std;
#include <THoleInfoTB.h>
#include <TCurveStore.h>
#include <TWellInfoTB.h>

#ifndef DATAFILTERCOND_API
	#ifdef DATAFILTERCOND_EXPORT
		#define DATAFILTERCOND_API __declspec( dllexport )
	#else	
		#define DATAFILTERCOND_API __declspec( dllimport )
	#endif
#endif


class DATAFILTERCOND_API CDataFilterCond  
{
public:
	CDataFilterCond();
	CDataFilterCond(IDB* pIDB);
	virtual ~CDataFilterCond();

	int ShowDlg(int iNo = 1, LPTSTR lpszViewItemID = NULL);
	void InitData();
	void Clear();

	IDB* m_pIDB;
	long m_lStartDepth;
	long m_lEndDepth;
	long m_lInterHorz;
	long m_lInterVert;
	COleDateTime m_StartTime;
	COleDateTime m_EndTime;
	CString m_strRunNo;
	short m_nBad;
	int m_iOnButton;
//2021.06.16 sys Start
	int m_iLas;
//2021.06.16 sys End
	TS_WELLINFOTB m_gWELLINFOTB ;
	TS_HOLEINFOTB m_gHOLEINFOTB; // 当前选择洞信息
	vector<TS_CURVEINFOTB> m_vecCurveInfo; // 当前选择选择曲线
	CString m_strDBName;
	CString m_strServer;
// 	CString m_strProjectName;
// 	CString m_strWellsiteName;
// 	CString m_strWellName;
	BOOL m_bBreakReadCurve;
	//2020.7.22 ver1.6.0 bug[117] start
	TS_WELLSITEINFOTB m_gWELLSITETB;
	TS_PROJECTTB m_gPROJECTTB;
	//2020.7.22 ver1.6.0 bug[117] end
	CString m_strViewItemID;
	int m_iRelog;
	CString m_strRelogDataID;
	int m_iMemInter;
	CString m_strAfxGetAppName;
};

DATAFILTERCOND_API CDataFilterCond* AfxGetDataFilterCond();

#endif // !defined(AFX_DATAFILTERCOND_H__CE7834E7_DA37_492F_B803_1C2A9E5DACB6__INCLUDED_)

#if !defined(AFX_SERVICESELDLG_H__006E4CA8_92BA_42AF_9A02_13B641439FD2__INCLUDED_)
#define AFX_SERVICESELDLG_H__006E4CA8_92BA_42AF_9A02_13B641439FD2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "BCGPListCtrl.h"
// ServiceSelDlg.h : header file
//
#ifndef TOOLGROP_API
	#ifdef TOOLGROP_EXPORT
		#define TOOLGROP_API __declspec( dllexport )
	#else	
		#define TOOLGROP_API __declspec( dllimport )
	#endif
#endif
/////////////////////////////////////////////////////////////////////////////
// CServiceSelDlg dialog

class CWorkspaceBar;
class CServiceTableItem;

class TOOLGROP_API CServiceSelDlg : public CDialogEx

{
// Construction
public:

	// 编辑所有服务项目 0
	// 新建工程选择服务 1 
	// 当前工程添加服务 2
	enum Mode { def = 0, sel = 1, add = 2}; 

	CServiceSelDlg(UINT nMode = 0, CWnd* pParent = NULL);
	~CServiceSelDlg();
// Dialog Data
	//{{AFX_DATA(CServiceSelDlg)
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SERVICE_TABLE };
#endif
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

public:
	void ChangeTabProperty();

    CMFCTabCtrl		m_wndTab;
	CBCGPListCtrl   m_wndServeItemSelNakedLog;
	CBCGPListCtrl	m_wndServeItemSelCasedLog;
	CBCGPListCtrl	m_wndServeItemSelTestLog;
	CBCGPListCtrl	m_wndServeItemSelRecordLog;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CServiceSelDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CServiceSelDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnBnAdd();
	afx_msg void OnBnDelete();
	afx_msg void OnBnLoad();
	afx_msg void OnBnModify();
	virtual void OnOK();
	afx_msg void OnBtnCopy();
	//}}AFX_MSG
	afx_msg LRESULT OnChangeActiveTab(WPARAM wp, LPARAM lp);
	DECLARE_MESSAGE_MAP()

public:	
	void AddServiceItem(CServiceTableItem *pService);
	BOOL RemoveServiceItem(CString strItemName);
	CServiceTableItem* FindServiceItem(CString strServiceItemName);
	CString SetServiceName(CString strServiceItemName , CStringArray *pNameList);
public:
	CBCGPListCtrl*		m_pWnd[4];
	int					m_nServices[4];
	
	CPtrArray			m_arrayServiceItem;		// 所有的服务项目 
	CStringArray        m_arrayServiceItemName;	// 所有的服务项目名字
	int                 m_nMode;				// 1选择服务项目至工程  
												// 2添加服务项目至工程
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERVICESELDLG_H__006E4CA8_92BA_42AF_9A02_13B641439FD2__INCLUDED_)

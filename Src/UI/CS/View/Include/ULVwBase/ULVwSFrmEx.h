// ULVwMFrmEx.h : CULVwBaseSFrameEx                          
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
// #define _TIME_STOP
#include "GfxProperty.h"
#include "WorkspaceBar.h"
//#include "ClassViewBar.h"
#include "DataView.h"
//#include "ThumbView.h"
#include "PropertiesViewBar.h"
#include "DataEditDefine.h"
#include "PrintInfo.h"
//#include "WatchBar.h"
//#include "ScriptMap.h"
//#include "EILog.h"	// Added by ClassView
//#include "PerforationDockBar.h"
#include "Kernel.h"
//#include "RftPVTFrame.h"
#include <winsock2.h>
//#include "ULEmd.h"
//#include "IDB.h"
//#include "MudInformation.h"
//#include "RunDataInformation.h"
//#include "SurveyTable.h"

#include "ULTool.h"

class	CULKernel;
class	CULTool;
class   CULChildFrame;
class   CCalChildFrame;
class	CRawDataView;
class	CCommandViewBar;
class	CULBaseDoc;
class	CRftManage;
class	CReplayControlBar;
class  CToolInfoPack;
class CDlgEmdParam;
class CULEmd;


//-----------------
// Statusbar panes:
//-----------------
#define	nStatusInfo		0
#define nStatusProgress 1
#define nStatusSave		2
#define nStatusPrint	3
#define nStatusDate		4
#define nStatusTime		5
#define nProgressWidth	300

// ---------------------------------
//	Log stat:
// ---------------------------------
#define	LOG_STOP		0x0000
#define	LOG_UP			0x0001
#define	LOG_DOWN		0x0002
//#define	LOG_LEFT		0x0004
#define	LOG_TIME		0x0004
#define	LOG_RIGHT		0x0008
#define	LOG_ALL			0x000F
#define	LOG_WATCH		0x0010
#define LOG_ALLWATCH	(LOG_ALL|LOG_WATCH)
#define LOG_CAL			0x0100


#define TA_0			0x0000
#define TA_1			0x0001
#define TA_2			0x0002

#ifndef ULVWBASE_API
	#ifdef ULVWBASE‌_EXPORT
		#define ULVWBASE_API __declspec( dllexport )
	#else	
		#define ULVWBASE_API __declspec( dllimport )
	#endif
#endif
class ULVWBASE_API CULVwBaseSFrameEx : public CVwBaseSFrameEx
{
	//friend class CULChildFrame;
	friend class CScript;
protected:
	CULVwBaseSFrameEx() noexcept;
	DECLARE_DYNCREATE(CULVwBaseSFrameEx)
	//DECLARE_DYNAMIC(CULVwBaseSFrameEx)
// 查看器
public:

// 操作
public:

// 覆盖
public:

	// ClassWizard 生成虚拟函数的覆盖。
	//{{AFX_VIRTUAL(CULVwBaseSFrameEx)
	//virtual BOOL MDITabActivate(CWnd* pWndActivate);
	virtual BOOL DestroyWindow();
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// 实现
public:
	virtual ~CULVwBaseSFrameEx();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// 生成的消息映射函数
protected:
	//{{AFX_MSG(CULVwBaseSFrameEx)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnWindowManager();
	afx_msg void OnHelpKeyboardmap();
	afx_msg void OnViewProject();
	afx_msg void OnUpdateViewProject(CCmdUI* pCmdUI);
	afx_msg void OnViewData();
	afx_msg void OnUpdateViewData(CCmdUI* pCmdUI);
	afx_msg void OnViewThumbnail();
	afx_msg void OnUpdateViewThumbnail(CCmdUI* pCmdUI);
	afx_msg void OnViewProperties();
	afx_msg void OnUpdateViewProperties(CCmdUI* pCmdUI);
	afx_msg void OnViewGraphToolbar();
	afx_msg void OnUpdateViewGraphToolbar(CCmdUI* pCmdUI);
	afx_msg void OnViewFullScreen();
	afx_msg void OnHelpWeb();
	afx_msg void OnMdiMoveToNextGroup();
	afx_msg void OnMdiMoveToPrevGroup();
	afx_msg void OnMdiNewHorzTabGroup();
	afx_msg void OnMdiNewVertGroup();
	afx_msg void OnMdiCancel();
	afx_msg void OnClose();
	afx_msg void OnProjectNew();
	afx_msg void OnProjectOpen();
	afx_msg void OnProjectSave();
	afx_msg void OnUpdateProjectSave(CCmdUI* pCmdUI);
	afx_msg void OnProjectSaveAs();
	afx_msg void OnUpdateProjectSaveAs(CCmdUI* pCmdUI);
	afx_msg void OnProjectClose();
	afx_msg void OnUpdateProjectClose(CCmdUI* pCmdUI);
	afx_msg void OnFileNew();
	afx_msg void OnUpdateFileNew(CCmdUI* pCmdUI);
	afx_msg void OnFileOpen();
	afx_msg void OnFileSave();
	afx_msg void OnUpdateFileSave(CCmdUI* pCmdUI);
	afx_msg void OnFileSaveAs();
	afx_msg void OnUpdateFileSaveAs(CCmdUI* pCmdUI);
	afx_msg void OnFileClose();
	afx_msg void OnUpdateFileClose(CCmdUI* pCmdUI);
	afx_msg void OnFilePrintSetup();
	afx_msg void OnFilePrinterCalibration();
	afx_msg void OnPrintCalCoef();
	afx_msg void OnFilePrint();
	afx_msg void OnUpdateFilePrint(CCmdUI* pCmdUI);
	afx_msg void OnFilePrintPdf();
	afx_msg void OnUpdateFilePrintPdf(CCmdUI* pCmdUI);
	afx_msg void OnLoggingUp();
	afx_msg void OnUpdateLoggingUp(CCmdUI* pCmdUI);
	afx_msg void OnLoggingTime();
	afx_msg void OnUpdateLoggingTime(CCmdUI* pCmdUI);
	afx_msg void OnLoggingDown();
	afx_msg void OnUpdateLoggingDown(CCmdUI* pCmdUI);
	afx_msg void OnLoggingDot();
	afx_msg void OnUpdateLoggingDot(CCmdUI* pCmdUI);
	afx_msg void OnLoggingWatch();
	afx_msg void OnUpdateLoggingWatch(CCmdUI* pCmdUI);
	afx_msg void OnMarkToolbar();
	afx_msg void OnUpdateMarkToolbar(CCmdUI* pCmdUI);
	afx_msg void OnCalibrateBegin();
	afx_msg void OnUpdateCalibrateBegin(CCmdUI* pCmdUI);
	afx_msg void OnOptionSetuptool();
	afx_msg void OnUninsatllTool();
	afx_msg void OnServiceTable();
	afx_msg void OnUpdateServiceTable(CCmdUI* pCmdUI);
	afx_msg void OnOptionCustom();
	afx_msg void OnViewTableToolbar();
	afx_msg void OnViewReplayToolbar();
	afx_msg void OnUpdateViewReplayToolbar(CCmdUI* pCmdUI);
	afx_msg void OnViewDataEditToolbar();
	afx_msg void OnUpdateViewTableToolbar(CCmdUI* pCmdUI);
	afx_msg void OnReplayBegin();
	afx_msg void OnReplayPause();
	afx_msg void OnReplayFinish();
	afx_msg void OnPlaybackCompute();
	afx_msg void OnCalibrateEnd();
	afx_msg void OnUpdateCalibrateEnd(CCmdUI* pCmdUI);
	afx_msg void OnInsertBackcurve();
	afx_msg void OnUpdateInsertBackcurve(CCmdUI* pCmdUI);
	afx_msg void OnRawDataView();
	afx_msg void OnViewWorkspace();
	afx_msg void OnUpdateViewWorkspace(CCmdUI* pCmdUI);
	afx_msg void OnViewScope();
	afx_msg void OnUpdateViewScope(CCmdUI* pCmdUI);
	afx_msg void OnViewCommandBar();
	afx_msg void OnUpdateViewCommandBar(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewDataeditToolbar(CCmdUI* pCmdUI);
	afx_msg void OnUpdateComputeReplay(CCmdUI* pCmdUI);
	//afx_msg void OnViewCustomize();
	afx_msg void OnApplayScript();
	afx_msg void OnUpdateUserButtons(CCmdUI* pCmdUI);
	afx_msg void OnLanguageSP();
	afx_msg void OnUpdateLanguageSP(CCmdUI* pCmdUI);
	afx_msg void OnLanguageCh();
	afx_msg void OnUpdateLanguageCh(CCmdUI* pCmdUI);
	afx_msg void OnLanguageEn();
	afx_msg void OnUpdateLanguageEn(CCmdUI* pCmdUI);
	afx_msg void OnUnitMetrics();
	afx_msg void OnUpdateUnitMetrics(CCmdUI* pCmdUI);
	afx_msg void OnUnitBritish();
	afx_msg void OnUpdateUnitBritish(CCmdUI* pCmdUI);
	afx_msg void OnUnitInternational();
	afx_msg void OnUpdateUnitInternational(CCmdUI* pCmdUI);
	afx_msg void OnUnitManager();
	afx_msg void OnUpdatePlaybackSetspeed(CCmdUI* pCmdUI);
	afx_msg void OnPlaybackSlowdown();
	afx_msg void OnPlaybackFastfoward();
	afx_msg void OnUpdatePlaybackPause(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePlaybackFinish(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePlaybackSlowdown(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePlaybackFastfoward(CCmdUI* pCmdUI);
	afx_msg void OnConstructionView();
	afx_msg void OnParamsView();
	afx_msg void OnSinAdd();
	afx_msg void OnSinSave();
	afx_msg void OnProjectInfoSave();
	afx_msg void OnProjectInfoLoad();
	afx_msg void OnLoggingBrowse();
	afx_msg void OnUpdateLoggingBrowse(CCmdUI* pCmdUI);
	afx_msg void OnChartView();
	afx_msg void OnUpdateChartView(CCmdUI* pCmdUI);
	afx_msg void OnChartViewSet();
	afx_msg void OnUpdateChartViewSet(CCmdUI* pCmdUI);
	afx_msg void OnUpdateTemplActive(CCmdUI* pCmdUI);
	afx_msg void OnTemplActive(UINT ID);
	afx_msg void OnTemplManage(UINT ID);
	afx_msg void OnLoggingStop();
	afx_msg void OnUpdateLoggingStop(CCmdUI* pCmdUI);
	afx_msg void OnViewClass();
	afx_msg void OnUpdateViewClass(CCmdUI* pCmdUI);
	afx_msg void OnToolsView();
	afx_msg void OnUpdateToolsView(CCmdUI* pCmdUI);
	afx_msg void OnMarkReportView();
	afx_msg void OnUpdateMarkReportView(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewTasstToolbar(CCmdUI* pCmdUI);
	afx_msg void OnViewTasstToolbar();
	afx_msg void OnJobParameter();
	afx_msg void OnUpdateRawDataView(CCmdUI* pCmdUI);
	afx_msg void OnUpdateActOnProject(CCmdUI* pCmdUI);
	afx_msg void OnUpdateProject(CCmdUI* pCmdUI);
	afx_msg void OnUpdateConstructionView(CCmdUI* pCmdUI);
	afx_msg void OnUpdateParamsView(CCmdUI* pCmdUI);
	afx_msg void OnImportCalFile();
	afx_msg void OnUpdateImportCalFile(CCmdUI* pCmdUI);
	afx_msg void OnImportSysinfo();
	afx_msg void OnExportSysinfo();
	afx_msg void OnUpdateToolControl1(CCmdUI* pCmdUI);
	afx_msg void OnUpdateUnitCustomize1(CCmdUI* pCmdUI);
	afx_msg void OnUnitLcd();
	afx_msg void OnUnitFormular();
	afx_msg void OnMdiSave();
	afx_msg void OnMdiClose();
	afx_msg void OnMdiCloseAllButThis();
	afx_msg void OnMdiCopyFullPath();
	afx_msg void OnMdiOpenFolder();
	afx_msg void OnUpdateMdiMenu(CCmdUI* pCmdUI);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnUpdatePlaybackCompute(CCmdUI* pCmdUI);
	afx_msg void OnDestroy();
	afx_msg void OnHelpEmail();
	afx_msg void OnUpdateHelp1(CCmdUI* pCmdUI);
	afx_msg void OnUpdateOptionCustom(CCmdUI* pCmdUI);
	afx_msg void OnLoggingShot();
	afx_msg void OnUpdateLoggingShot(CCmdUI* pCmdUI);
	afx_msg void OnPerforationSchedule();
	afx_msg void OnUpdatePerforationSchedule(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDataRft(CCmdUI* pCmdUI);
	afx_msg void OnDataRft();	
	afx_msg void OnUpdateToolTest(CCmdUI* pCmdUI);
	afx_msg void OnToolTest();
	afx_msg HRESULT OnHotkeyMsg(WPARAM wp, LPARAM lp);
	afx_msg void OnTableinfoView();
	afx_msg void OnUpdateTableinfoView(CCmdUI* pCmdUI);
	afx_msg void OnToolinfoView();
	afx_msg void OnOutputstable();
	afx_msg void OnUpdateToolinfoView(CCmdUI* pCmdUI);
	afx_msg void OnUpdateOutputstable(CCmdUI* pCmdUI);
	afx_msg void OnFileAllClose();
	afx_msg void OnUpdateFileAllClose(CCmdUI* pCmdUI);	
	afx_msg void OnFactorView();
	afx_msg void OnUpdateFactorView(CCmdUI* pCmdUI);
	afx_msg void OnAlarmSettings();
	afx_msg void OnUpdateAlarmSettings(CCmdUI* pCmdUI);
	afx_msg void OnViewToolasstToolbar();
	afx_msg void OnUpdateViewToolasstToolbar(CCmdUI* pCmdUI);
	afx_msg void OnAlignLeft();
	afx_msg void OnLwdChecktool();
	afx_msg void OnLwdReadData();
	afx_msg void OnLwdParamdld();
	afx_msg void OnLwdTooltest();
	afx_msg void OnUpdateLwdChecktool(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLwdParamdld(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLwdReaddata(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLwdTooltest(CCmdUI* pCmdUI);
	afx_msg void OnRunDataInfoView();
	afx_msg void OnUpdateRunDataInfoView(CCmdUI* pCmdUI);
	afx_msg void OnFileDatabaseOpen();
	afx_msg void OnUpdateFileDatabaseOpen(CCmdUI* pCmdUI);
	afx_msg void OnFileDatabaseClose();
	afx_msg void OnUpdateFileDatabaseClose(CCmdUI* pCmdUI);
	afx_msg void OnSurveyInfo();
	afx_msg void OnHoleView();
	//}}AFX_MSG
	afx_msg void OnTaFirst(UINT nID);
	afx_msg void OnUpdateTaFirst(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEmendation1(CCmdUI* pCmdUI);
	afx_msg void OnEmendation1(UINT nID);
	afx_msg void OnHelp1(UINT nID);
	afx_msg BOOL OnMDIWindowCmdEx(UINT nID);
	afx_msg void OnPlaybackSetspeed();
	afx_msg void OnCalibrationReport(UINT nID);
	afx_msg void OnCalibrationUserReport();
	afx_msg void OnUpdateCalibrationReport(CCmdUI* pCmdUI);
	//afx_msg void OnToolsViewUserToolbar(UINT id);
	//afx_msg void OnUpdateToolsViewUserToolbar(CCmdUI* pCmdUI);
	afx_msg void OnUnitCustomize(UINT nID);
	afx_msg void OnToolControl(UINT nID);
	afx_msg void OnSinActive(UINT nID);
	afx_msg void OnUpdateSinActive(CCmdUI* pCmdUI);
	afx_msg void OnSinModify(UINT nID);
	afx_msg void OnSinDelete(UINT nID);
	afx_msg void OnUpdateLogStop(CCmdUI* pCmdUI);
	//afx_msg LRESULT OnToolbarReset(WPARAM, LPARAM);	
	//afx_msg LRESULT OnToolbarContextMenu(WPARAM, LPARAM);
	afx_msg LRESULT OnGetTabToolTip(WPARAM, LPARAM);
	//afx_msg LRESULT OnHelpCustomizeToolbars(WPARAM, LPARAM);
	afx_msg LRESULT OnStartCustomize(WPARAM, LPARAM);
	//afx_msg LRESULT OnToolbarCreateNew(WPARAM, LPARAM);
	afx_msg LRESULT OnDataArrived(WPARAM, LPARAM);
	afx_msg LRESULT OnDataSave(WPARAM, LPARAM);
	afx_msg LRESULT OnReplay(WPARAM, LPARAM);
	afx_msg LRESULT OnChildClose(WPARAM, LPARAM);
	afx_msg LRESULT OnSocket(WPARAM, LPARAM);
	afx_msg LRESULT OnPropUpdate(WPARAM, LPARAM);
	afx_msg void OnFormat(UINT nFormat);
	//用于测试定义消息函数
	afx_msg LRESULT OnCalBegin(WPARAM, LPARAM);
	afx_msg LRESULT OnCalEnd(WPARAM, LPARAM);
	DECLARE_MESSAGE_MAP()
protected:
	void DisplayMsgOnTitleBar(long lDepth);

public:
	virtual BOOL CreateDockingWindows();
	void SetDockingWindowIcons(BOOL bHiColorIcons);
	//BOOL CreateDockingBars();
	//void SetDockingBarsIcons(BOOL bHiColorIcons);
	void OnViewArea(CPtrArray* pArray);
	void OnUpdateViewArea(CPtrArray* pArray, CCmdUI* pCmdUI);
	void HideBars();
	void ActiveWorkspaceBar();
	BOOL DeleteKey (LPCTSTR pszPath, BOOL bAdmin);
	// Operations
public:
	CWnd* AddScopeWnd(CULTool* pULTool, CToolInfoPack* pTIP, int j);
	void SaveScopeSetup();
	void ReloadState(LPCTSTR pszSetting);
	void ClearScopeWnd();
	void AddToolCtrlWnd(CULTool* pULTool);
	void ClearToolWnd();
	void RefreshDataView();
	void RefreshAfterCurveOpened(CULTool* pULTool);
	BOOL IsActiveProject();
	BOOL OpenFile(CString& strFile, BOOL bSW = TRUE);
	virtual BOOL OpenProject(CString& strProject);
	
	BOOL ShowParamsView(BOOL bShow = TRUE, BOOL bDestory = FALSE);
	void RedrawAll();
	CULChildFrame* CreateChild(DWORD dwType, LPVOID pParam = NULL, CULBaseDoc* pULDoc = NULL);
	CProject* GetCurProject();
	CString GetActiveDocPath();
	void ClearActWnd();
	void SetMainMenu();
	void ResetMenu();
	void OnServiceActive();
	void LoadCtrlWnd();
	void LoadScopeWnd();
	void ShowScopeWnd(CULTool* pTool, int nScope, BOOL bShow);
	void Logging(int nDirection);
	BOOL IsPerforation(){return m_bPerforation;}
	
	void SetMainFont(BOOL bRefresh = TRUE); //添加已设置的字体
	BOOL ShowClassBar(BOOL bShow = TRUE);
	CULChildFrame* FindChild(DWORD dwType, LPCTSTR pszTempl);
	
	// Perforation and coring operation:
	void AddShootTab(BOOL bShow  = TRUE );
	void RemoveShootTab();

	BOOL IsToolTestMode() {return m_bToolTest;}
	void ChartViewSet(CULChildFrame* pChild, CString strPathName);

	LRESULT	OnProjectAdd(WPARAM wp, LPARAM);
	int     GetCtrlWndIndex(CBInfo* pInfo);
	void    AddMark(CCurve* pToolCurve, LPCTSTR lpszName, long lDepth, DWORD dwColor, int nType);

	void    ResChangeEx();
    virtual HWND GetHwnd(); 
    virtual ICurve* CreateCorrectedCurve(CString strName, CString strUnit, UINT nPointFrame); 
    virtual void AddCorrectedCurve(CULBaseDoc* pWell, ICurve* pCurve, BOOL bRefresh = TRUE); 
	
	void ArrangeWindow(UINT nID); // add by bao 修改水平平铺关闭后，再次打开选择水平平铺出现界面混乱
	void AddSubVersion();//add by gj 增加子版本号
	void CheckDllPath(BOOL bReplay); // 检查回放动态库的路径正确与否。
	void SetReplaybackSetspeed(); // 设置回放速度
	void DisableUnits(CCmdUI* pCmdUI);//add by gj130904 测井时单位制设置不可用
	void RefreshDataViewUnit();
// 2020.2.18 Ver1.6.0 TASK【002】 Start	
	void OpenDatabase();
// 2020.2.18 Ver1.6.0 TASK【002】 End	
	//void ReadConfigFromINI();
	// Control bar embedded members
public:  
	//CMFCMenuBar			m_wndMenuBar;		
	//CMFCStatusBar			m_wndStatusBar;	
	CMFCToolBar			m_wndToolBar;			// 主工具栏
	CMFCToolBar			m_wndToolbarGraph;		// 测井绘图栏
// 2019.09.25 改善对应[SK10001] Start
	CMFCToolBar			m_wndToolbarGSelect;	// 测井浏览栏
// 2019.09.25 改善对应[SK10001] End
	CMFCToolBar			m_wndToolbarTable;		// 表格工具栏
	CMFCToolBar			m_wndToolbarDataEdit;	// 数据编辑栏
	//CMFCToolBar			m_wndToolbarAsst;	// 仪器辅助栏
	//CMFCToolBar			m_wndToolbarReplay;		// 回放控制栏
    //CMFCToolBar            m_wndToolbarCross;      // 交会图工具条

	CWorkspaceBar*			m_pWndWorkspace;		// 工作框
	//CClassViewBar			m_wndClassView;		// 对象框
	//CDataViewBar			m_wndDataView;		// 数据框
	//CThumbViewBar			m_wndThumbView;		// 缩略图
//	CPerforationControlBar	m_wndPerforationControlBar;	// 射孔控制窗口
//	CPerforationMarkerBar   m_wndPerforationMarkBar;    //节箍调整窗口
//	CWatchBar				m_wndWatchBar;		// 查看框
	//CPropertiesViewBar		m_wndPropertiesBar;	// 属性框	

	// Dockingbar list
	CPtrArray				m_WorkBars;			// 工作浮动框 
	CPtrArray				m_ScopeBars;		// 示波器浮动框
	CPtrArray				m_CmdBars;			// 仪器控制浮动框
	CPtrArray				m_Scopes;			// 声波示波器
	CPtrList				m_preList;			// 绘图模板
	CPtrList				m_hdrList;			// 图头模板
    CPtrList                m_chtList;          // 交会图版列表
	CStringArray			m_helps;

//	CInfoBar				m_wndInfoBar;
	//DATA_EDIT_OPERATE		m_CopyOperate;
	CMFCToolBarImages		m_UserImages;
	//CSurveyTable			m_SurveyTable;
public:
	//CCalChildFrame*			m_pCalFrm;			// Calibrate childframe
	//CULChildFrame*            m_pChildRawData;	// Rawdata childframe
	//CULChildFrame*			m_pChildParam;		// Tool param childframe
	//CRftPVTFrame*			m_pRFTPVTFrame;
	//CRftManage*				m_pRFTManage;
	CProject*  m_pProject;
	// Attributes
public:
	CULEmd*	m_pEmd;
	CDlgEmdParam* m_pDlgEmdP;
	//CEILog					m_EILog;
	long					m_nLog;
	BOOL					m_bSave;			// 实时保存				
	BOOL					m_bPrint;			// 实时打印
	long					m_nSaving;			// 线程存盘计数
	BOOL					m_bDrawGridLine;	// 是否画格线
	CULPrintInfo*			m_pULPrintInfo;
	//CScriptMap				m_Scripts;
	//CCommandViewBar*		m_pCmdBar;			// 仪器控制命令栏
	//CReplayControlBar*		m_pReplayControlBar;		//	回放控制对话框	add by bao 2013/7/18
	BOOL					m_bNoProjectCal; 	// 标识是否是在不打开工程的情况下进行刻度
	//CULChildFrame*			m_pActiveChild;
	BOOL					m_bBrowseFlag;
	BOOL					m_bTVDFlag;			//当前TVD校正标志
#ifdef _TIME_STOP
	int						m_nToStop;
#endif

#ifdef _DATA_BASE
	IDB*					m_pIDB;
	HINSTANCE				m_hDllDB;
	BOOL					m_bDBOpen;
#endif

#ifdef _LWD
	//CMudInformation m_MudInformation;
	//CRunDataInformation m_RunInformation;

	DWORD					m_dwLoggingStart;
	BOOL					m_bLogFlash;
	BOOL					LoggingFlash();
#endif

protected:
	BOOL					m_bPerforation;
	BOOL					m_bToolTest;		// 仪器测试
	SOCKET					m_Socket;			// 深度数据显示
	BOOL					m_bStopNow;			// 立即停止测井，对话框上的取消失效
	
public:
	BOOL					DBReadRunInfo();
	BOOL					DBWriteRunInfo();
	BOOL					DBReadMudInfo();
	BOOL					DBWriteMudInfo();

 //   UINT					m_iReplayRate[7];     //modify by gj 20121226
	//UINT					m_iReplayRateIndex;
	BOOL					m_bIsCascade; // 当窗口为层叠显示时为TRUE
	BOOL                    m_bFileOpen;
	BOOL					m_bRawOpen;
	BOOL                    m_bLogging;//是否在测井状态add by gj20130924 用于禁用单位制
	bool					m_bMyLogging;
};

_inline void CULVwBaseSFrameEx::RedrawAll()
{
	CRect rectOld;
	GetWindowRect (rectOld);
	RedrawWindow (NULL, NULL, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE | RDW_ALLCHILDREN);
	GetDesktopWindow ()->RedrawWindow (rectOld, NULL, RDW_INVALIDATE | RDW_UPDATENOW | 
		RDW_ERASE | RDW_ALLCHILDREN);
	RecalcLayout();
	RefreshDataViewUnit();
}

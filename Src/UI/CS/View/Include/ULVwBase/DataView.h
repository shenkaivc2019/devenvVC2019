﻿// DataView.h: interface for the CDataViewBar class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DataView_H__F9C28949_2C41_47AD_9BAB_E2516CEEC6E0__INCLUDED_)
#define AFX_DataView_H__F9C28949_2C41_47AD_9BAB_E2516CEEC6E0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ULTool.h"
#include "MyEdit.h"

#ifndef ULVWBASE_API
	#ifdef ULVWBASE‌_EXPORT
		#define ULVWBASE_API __declspec( dllexport )
	#else	
		#define ULVWBASE_API __declspec( dllimport )
	#endif
#endif

class CDataGrid;
class CULKernel;

class ULVWBASE_API CDataViewBar : public CDockablePane
{
// Construction
public:
	CDataViewBar();

	enum {cname, cvalue, cunit, cols };

protected:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataViewBar)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CDataViewBar();

	// Generated message map functions
protected:
	//{{AFX_MSG(CDataViewBar)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnDataviewDelete();
	afx_msg void OnDataviewDeleteall();
	afx_msg void OnCopyToExcel();
	afx_msg void OnDataViewEdit();
	afx_msg void OnPointDelete();
	afx_msg void OnUpdateCopyToExcel(CCmdUI* pCmdUI);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnGridCheck();
	afx_msg void OnUpdateGridCheck(CCmdUI *pCmdUI);
	afx_msg void OnDataviewInsertPoint();
	//}}AFX_MSG
	afx_msg LRESULT OnLCDDataArrived(WPARAM wp, LPARAM lp);
	afx_msg LRESULT OnMyEditDestroy(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

public:
	void		InitGrid(CULKernel* pKernel, BOOL bInvalidate = TRUE);
	void		ReinitGrid();
	void		ResetGrid();
	void		UpdateGrid();
	void		InitUnits();
	void		DisplayToolRow(CULTool* pULTool);
	void		RefreshTools(CULKernel* pKernel);

// Control Members
public:
	CDataGrid*	m_pGridCtrl;
	CULKernel*  m_pKernel;
	int			m_nCount;
	int			m_nGridPos;
	BOOL        m_bRefresh;
	CCurveArray m_Curves;
	CPtrArray	m_arrTools;		// 仪器串指针
	bool m_bUseGrid;
#ifdef _LWD
	BOOL		m_bShowCurve;
#endif
	CRITICAL_SECTION m_cs;	//创建访问表格的临界区
private:
	CString m_strText;  //editText for curve val change add by zjy 2014 12 18
	CMyEdit* m_pEdit;
	CRect m_EditRect;
	int m_icol;
	HANDLE m_hMutex; //互斥对象
	int m_irow;
};

#endif // !defined(AFX_DataView_H__F9C28949_2C41_47AD_9BAB_E2516CEEC6E0__INCLUDED_)

﻿#if !defined(AFX_GRAPHHEADERWND_H__FA561176_4910_4FCB_B9B7_CFC8F57905E1__INCLUDED_)
#define AFX_GRAPHHEADERWND_H__FA561176_4910_4FCB_B9B7_CFC8F57905E1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ULBaseView.h"

#ifndef ULVWBASE_API
	#ifdef ULVWBASE‌_EXPORT
		#define ULVWBASE_API __declspec( dllexport )
	#else	
		#define ULVWBASE_API __declspec( dllimport )
	#endif
#endif
// GraphHeaderWnd.h : header file
//

class CDragWnd;
class CGraphView;
class CGraphWnd;
class CSheet;
class CTrack;
class CCurve;
class CXMLNode;

typedef struct tagTCDRAGINFO
{
	int		nTrackType;
	int		nTrackFrom;		// The track, in which the curve is drag out
	int		nCurveIndex;	// The curve, being dragged
	CSheet* pSrcSheet;		// The sheet, in which the curve is drag out
	CTrack* pSrcTrack;		// The track, in which the curve is drag out
	CCurve* pSrcCurve;		// The curve, being dragged
	CXMLNode* pXMLNode;		// The new curve initial
} TCDRAGINFO;

/////////////////////////////////////////////////////////////////////////////
// CGraphHeaderWnd view

class ULVWBASE_API CGraphHeaderWnd : public CULBaseView
{
	DECLARE_DYNCREATE(CGraphHeaderWnd)

public:
	CGraphHeaderWnd(LPVOID pSheet = NULL);	

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGraphHeaderWnd)
	public:
	virtual BOOL Create(CWnd* pParentWnd, UINT nID);
	virtual DROPEFFECT OnDragEnter(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	virtual void OnDragLeave();
	virtual DROPEFFECT OnDragOver(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	virtual BOOL OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point);
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CGraphHeaderWnd();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CGraphHeaderWnd)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	virtual int CalcPageWidth();
	virtual int CalcPageHeight();
	virtual void SetScrollInformation();
	virtual void Draw(CDC* pDC, LPRECT lpRect);
	virtual void DrawPage(CDC* pDC, CRect rect, COLORREF crPage);
	// Track/Curve Dragging Operation
	int 		TrackEdgeHitTest(CPoint point);
	CTrack*		TrackHitTest(CPoint point);
	CCurve*		CurveHitTest(CPoint point, int& nSelTrack, int &nSelCurve); 
	CCurve*     CurveHitHeadTest(CTrack* pTrack, CPoint pt, int& nPos);
	int			TrackOrderByPoint(CPoint point);
	void		InvalidateAll(BOOL bResetScroll = FALSE);	// 刷新井道头，绘图区域和缩略图
public:
	void SetFilterInfo(CCurve* pCurve,CXMLNode* pXMLNode);//add by gj 20121212
	CGraphView*	m_pParentView;		// 父类指针
	CGraphWnd*	m_pGraphWnd;		// 井道视图指针
	CSheet*		m_pSheet;			// 绘图设置指针

	// Track Edge Dragging
	BOOL		m_bEdgeDragging;
	int			m_nDragEdge;

	// Track Dragging
	BOOL		m_bTrackDragging;
	CTrack*		m_pDragTrack; 
	int			m_nDragTrackIndex;
	CDragWnd*	m_pDragWnd;
	int			m_nTrackType;

	// Curve Dragging
	COleDropTarget	m_oleDropTarget;
	
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRAPHHEADERWND_H__FA561176_4910_4FCB_B9B7_CFC8F57905E1__INCLUDED_)

﻿#if !defined(AFX_GRAPHVIEW_H__A202A85B_3CDD_40B1_A9B4_2C3B07061B84__INCLUDED_)
#define AFX_GRAPHVIEW_H__A202A85B_3CDD_40B1_A9B4_2C3B07061B84__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


//#include "GraphHeaderWnd.h"
#include "XYSplitterWnd.h"
#include "ULDataEditClass.h"

// GraphView.h : header file
//
#ifndef ULVWBASE_API
	#ifdef ULVWBASE‌_EXPORT
		#define ULVWBASE_API __declspec( dllexport )
	#else	
		#define ULVWBASE_API __declspec( dllimport )
	#endif
#endif
/////////////////////////////////////////////////////////////////////////////
// CGraphView view
class CSheet;
class CGraphHeaderWnd;
class CGraphWnd;
class CULChildFrame;
class ULVWBASE_API CGraphView : public CView
{
public:
	CGraphView(LPVOID lpParam = NULL);
	DECLARE_DYNCREATE(CGraphView)

// Attributes
public:
	
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGraphView)
	public:
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CGraphView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CGraphView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	afx_msg LRESULT OnDataArrived(WPARAM wp, LPARAM lp);
	DECLARE_MESSAGE_MAP()

public:
	void	SetSheet(CSheet* pSheet);

public:
	CGraphHeaderWnd*	m_pGraphHeader;
	CGraphWnd*			m_pGraphWnd;
	CySplitterWnd		m_ySplitter;
	CSheet*				m_pSheet;
	CULChildFrame*		m_pParentFrame;
	CULDataEdit			m_DataEditPerform;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRAPHVIEW_H__A202A85B_3CDD_40B1_A9B4_2C3B07061B84__INCLUDED_)

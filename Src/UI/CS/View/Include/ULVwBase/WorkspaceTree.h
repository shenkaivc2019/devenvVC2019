﻿#if !defined(AFX_WORKSPACETREE_H__0517A828_D18F_11D3_A727_009027900694__INCLUDED_)
#define AFX_WORKSPACETREE_H__0517A828_D18F_11D3_A727_009027900694__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TreeCtrlEx.h"

// WorkspaceTree.h : header file
//
#ifndef ULVWBASE_API
	#ifdef ULVWBASE‌_EXPORT
		#define ULVWBASE_API __declspec( dllexport )
	#else	
		#define ULVWBASE_API __declspec( dllimport )
	#endif
#endif
/////////////////////////////////////////////////////////////////////////////
// CWorkspaceTree window
class CProject;
class CServiceTableItem;
class CWorkspaceBar;
class CPrintParam;
class CGraphView;
class CULFileBaseDoc;
class CULChildFrame;
class CPofInfo;
class CULBaseDoc;

class ULVWBASE_API CWorkspaceTree : public CTreeCtrlEx
{
	friend class CWorkspaceBar;
// Construction
public:
	CWorkspaceTree();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWorkspaceTree)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CWorkspaceTree();

	// Generated message map functions
protected:
	//{{AFX_MSG(CWorkspaceTree)
	afx_msg void OnDblclk(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBeginDrag(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelChangeWorkspaceTree(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL		AddProject(CProject* pProject, BOOL bActive = TRUE);
	virtual BOOL		AddFile(CULFileBaseDoc* pFile, HTREEITEM hFileItem = NULL);
	virtual BOOL		AddPrintFile(CString strName);
	virtual BOOL		DelFileItem(CULFileBaseDoc* pFile);
	virtual BOOL		AddService(CServiceTableItem* pService, CProject* pProject = NULL);
	virtual BOOL		AddNewService(CString strService, CProject* pProject = NULL);
	virtual BOOL		AddGraphView(CGraphView* pGraphView, CServiceTableItem* pService = NULL);
	virtual BOOL		SetActProject(CProject* pProject);
	virtual BOOL		SetActProject(HTREEITEM hProject);
	virtual BOOL		SetActService(CServiceTableItem* pService);
	virtual BOOL		SetActService(HTREEITEM hService);
	virtual BOOL		SetActService(int nIndex = 0);
	virtual CServiceTableItem* GetService(int nIndex);
	virtual CULBaseDoc*		GetDoc(HTREEITEM hItem);
	virtual DWORD       GetDocType(HTREEITEM hItem); // add by gj 20140311

	virtual void		UpdateDocItems(CULBaseDoc* pDoc, DWORD dwType = (DWORD)-1);
	virtual void		UpdateSelItem(CULBaseDoc* pDoc);
	virtual HTREEITEM	GetPrjItem(CProject* pProject);
	virtual HTREEITEM	GetFileItem(CULFileBaseDoc* pULFile);
	virtual HTREEITEM	GetPriItem(HTREEITEM hFileItem);
	virtual HTREEITEM	GetSvrItem(CServiceTableItem* pService);//
	virtual HTREEITEM	GetSvrItem(int nIndex);
	virtual void		RefreshSvrItem(HTREEITEM hSvrItem);	//刷新服务项目节点
	virtual HTREEITEM	GetSvrChildItem(CServiceTableItem* pService, UINT nImage);
	virtual HTREEITEM	GetToolItemOfService(int nTool, CServiceTableItem* pService);
	virtual void		RefreshOrder(HTREEITEM& hItem, CULBaseDoc* pFile, CPofInfo* pPofInfo);
	virtual BOOL		ChangePrintParam(CPrintParam* pParam, CULBaseDoc* pDoc);
	virtual int			GetIndexOfOrder(HTREEITEM &hItem);
	virtual void		RefreshFileItem(CULFileBaseDoc* pULFile);		//刷新文件节点

	// 在工程树中添加、删除一个sheet对象
	virtual BOOL		AddOneSheetItem(CString strToAddSheetName,CULChildFrame *pChildFrame);
	virtual BOOL		RemoveOneSheetItem(CULChildFrame *pChildFrame);
	virtual void		RefreshTools();							//刷新仪器组合节点
	virtual HTREEITEM	GetChildItemImage(HTREEITEM hItem, int iImage);
	virtual void		RefreshDocHeaders(CULBaseDoc* pDoc);		//刷新图表设置节点
	virtual void		ActiveDoc(CULBaseDoc* pDoc);

	virtual void		AddDataFileToTree();
	virtual BOOL		AddDataFile(CString strProjectPath);

// 2020.2.18 Ver1.6.0 TASK【002】 Start
	virtual BOOL		SetActRun(HTREEITEM hRun);
// 2020.2.18 Ver1.6.0 TASK【002】 End
public:
	CProject*			m_pActPrj;		// 当前打开工程
	CStringArray		m_strsDataFile;	// 文件列表
	CWorkspaceBar*		m_pParent;		// 父窗口
	
	// 当前活动叶项
	HTREEITEM			m_hRoot;
	HTREEITEM			m_hProject;
// 2020.2.10 Ver1.6.0 TASK【002】 Start
	HTREEITEM			m_hRun;
// 2020.2.10 Ver1.6.0 TASK【002】 End
	HTREEITEM			m_hService;
	HTREEITEM			m_hTools;
	HTREEITEM           m_hSelItem;
	HTREEITEM           m_hCurFile;		//打开文件的根节点
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WORKSPACETREE_H__0517A828_D18F_11D3_A727_009027900694__INCLUDED_)

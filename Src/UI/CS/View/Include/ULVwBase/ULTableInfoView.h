﻿#if !defined(AFX_TABLEINFOVIEW_H__3B42446F_181E_47C0_83B1_AC93C5EB2E66__INCLUDED_)
#define AFX_TABLEINFOVIEW_H__3B42446F_181E_47C0_83B1_AC93C5EB2E66__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TableInfoView.h : header file
//

#include "Cell2000.h"
#include "ULBaseView.h"

/////////////////////////////////////////////////////////////////////////////
// CULTableInfoView view

#ifndef ULVWBASE_API
	#ifdef ULVWBASE‌_EXPORT
		#define ULVWBASE_API __declspec( dllexport )
	#else	
		#define ULVWBASE_API __declspec( dllimport )
	#endif
#endif

class CULTool;
#define CELL_NULL_HEIGHT 10
class ULVWBASE_API CULTableInfoView : public CULBaseView
{
	DECLARE_DYNCREATE(CULTableInfoView)
public:
	CULTableInfoView(LPVOID pTools=NULL);           // protected constructor used by dynamic creation
	

// Attributes
protected:
     virtual ~CULTableInfoView();
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CULTableInfoView)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnUpdate();     // first time after construct
	//}}AFX_VIRTUAL

// Implementation
protected:
	
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CULTableInfoView)
		// NOTE - the ClassWizard will add and remove member functions here.
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	virtual int CalcPageHeight();
	virtual int CalcPrintHeight();
	virtual void SaveAsBitmap(CDC* pDC,LPCTSTR pszFile);
	void Clear();
	void InitToolInfo();
	void OpenCellFile();
	void OpenCellFile(CCell2000* pCell);

protected:
	void InitCallData(CCell2000* pCell=NULL);
	void WriteVolumeBmp();
	
public:
	CCell2000   m_Cell;
	CPtrArray   m_ToolInfoList;
	CPtrArray*  m_pArrTools;
	long		m_lImages[4];
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TABLEINFOVIEW_H__3B42446F_181E_47C0_83B1_AC93C5EB2E66__INCLUDED_)

﻿// PropertiesViewBar.h: interface for the CPropertiesViewBar class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROPERTIESVIEWBAR_H__91E4A4A8_1AD0_450E_841C_409767044A55__INCLUDED_)
#define AFX_PROPERTIESVIEWBAR_H__91E4A4A8_1AD0_450E_841C_409767044A55__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef ULVWBASE_API
	#ifdef ULVWBASE‌_EXPORT
		#define ULVWBASE_API __declspec( dllexport )
	#else	
		#define ULVWBASE_API __declspec( dllimport )
	#endif
#endif

// 属性编辑类型
#define PT_VIEW				0
#define PT_SHEET			1
#define PT_TRACK			2
#define PT_CURVE			3
#define PT_ITEM				4

class CTrack;
class CGraphView;
class CSheet;
class CServiceTableItem;
class CMyBCGPProp;
class ULVWBASE_API CBasePropertiesToolBar : public CMFCToolBar
{
public:	
	virtual void OnUpdateCmdUI(CFrameWnd* /*pTarget*/, BOOL bDisableIfNoHndler)	
	{		
		CMFCToolBar::OnUpdateCmdUI ((CFrameWnd*) GetOwner (), bDisableIfNoHndler);
	}

	virtual BOOL AllowShowOnList () const		{	return FALSE;	}
};

class ULVWBASE_API CPropertiesViewBar : public CDockablePane
{
// Construction
public:
	CPropertiesViewBar();

	void AdjustLayout ();

// Attributes
public:
	void SetVSDotNetLook (BOOL bSet)
	{
		m_wndPropList.SetVSDotNetLook (bSet);
		m_wndPropList.SetGroupNameFullWidth (bSet);
	}

	void OnChangeVisualStyle ();
	void EraseTrack(CTrack* pTrack)
	{
		if (pTrack == m_pTrack)
			m_pTrack = NULL;
	}

	void EraseCurve(CCurve* pCurve)
	{
		if (pCurve == m_pCurve)
			m_pCurve = NULL;
	}

public:
//	CComboBox			m_wndObjectCombo;	// 对象
	CBasePropertiesToolBar	m_wndToolBar;		// 排序
	CFont				m_fntPropList;		// 字体
	CMFCPropertyGridCtrl		m_wndPropList;		// 属性
	CImageList			m_imageList;		// 线型
	CStringList			m_lstIconNames;
	BOOL				m_bAutoSave;		// 自动保存

	// ------- 属性组 -------
//	CMFCPropertyGridProperty*			m_pSheetProp;		// 页面
	CMFCPropertyGridProperty*		m_pSheetProp;		// 页面
	CMFCPropertyGridProperty*			m_pTrackProp;		// 井道
	CMFCPropertyGridProperty*			m_pGridProp;		// 格线
	CMFCPropertyGridProperty*			m_pCurveProp;		// 曲线

public:
	CGraphView*			m_pGraphView;	// 当前视图
	CSheet*				m_pSheet;		// 当前页面
	CTrack*				m_pTrack;		// 当前井道
	CCurve*				m_pCurve;		// 当前曲线
	CServiceTableItem  *m_pService;		// 当前服务项目

	
// Attributes
protected:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPropertiesViewBar)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CPropertiesViewBar();

	// Generated message map functions
protected:
	//{{AFX_MSG(CPropertiesViewBar)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSortingprop();
	afx_msg void OnUpdateSortingprop(CCmdUI* pCmdUI);
	afx_msg void OnProperies1();
	afx_msg void OnUpdateProperies1(CCmdUI* pCmdUI);
	afx_msg void OnExpand();
	afx_msg void OnUpdateExpand(CCmdUI* pCmdUI);
	afx_msg void OnPaint();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnSettingChange(UINT uFlags, LPCTSTR lpszSection);
	//}}AFX_MSG
	afx_msg LRESULT OnPropertyChanged(WPARAM wp, LPARAM lp);
	afx_msg LRESULT PropertyUpdate(WPARAM wp, LPARAM lp);
	DECLARE_MESSAGE_MAP()

public:
	void InitPropList ();
	void InitSheetProp(CSheet* pSheet);
	void InitTrackProp();
	void InitCurveProp();
	void ResetPropList();
	void EnableItem(int nItem, BOOL bEnable);
	void SetPropListFont();

	void ModifySheetProp(CSheet* pSheet, int nOldDriveMode);
	BOOL IsInch(float fRatioTime); // 判断设置的时间比例是否为英寸单位，是则返回TRUE，不是返回FALSE
};

#endif // !defined(AFX_PROPERTIESVIEWBAR_H__91E4A4A8_1AD0_450E_841C_409767044A55__INCLUDED_)

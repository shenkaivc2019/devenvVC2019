#pragma once

/////////////////////////////////////////////////////////////////////////////
// CULVwBaseMAppEx:

#ifndef __AFXWIN_H__
#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"
#define AFX_IDC_CELL                    123
#define ID_CURVE_DATASOURCE1            0x7EF5
#define IDM_CURVEEDIT_RESUME            1743
#define ID_EDIT_CUTUNIFORM_DEPTHHEAD    33014
#define ID_CURVE_DATASOURCE1            0x7EF5
#define ID_CURVE_DEPTH_CORRECT          40009
#define ID_INSERT_BACKCURVE             33168
#define ID_APPLAY_SCRIPT                33160
#define IDC_USER_BUTTONS                1780
#define IDM_CONSTRUCTION_VIEW           1797
#define IDM_PROJECT_INFO_SAVE           33245
#define IDM_PROJECT_INFO_LOAD           33246
#define IDM_CHART_VIEW_SET              33375
#define IDD_TEST_REPORT                 33371
#define ID_JOB_PARAMETER                33278
#define AFX_IDC_GRIDCTRL                107
#define IDC_MY_EDIT_TEXT                60057
#define ID_CVS_ACTIVE                   40022
#define IDS_RELOGGING                   37993
#define ID_EMENDATION1                  0x8980
#define IDM_OPENED_FILE12               0x8A0B
#define IDM_SI_ACTIVE_0                 32800
#define IDM_SI_MODIFY_0                 32832
#define IDM_SI_DELETE_0                 32864
#define ID_TOOL_CONTROLE                32799
#define ID_CALC_VOLUME                  33302
#define ID_CURVE_DATASOURCE1            0x7EF5
#define ID_CURVE_DATASOURCEE            0x7FF3
#define IDS_DATA_CUT_HEAD_REMARK        37954
#define IDD_CALC_VOLUME                 13016
#define IDC_COMBO_CASEDHOLE_PROCESS     1493
#define ID_CVS_MANAGE                   40032


#define isCtrlPressed	((0x8000 & GetKeyState(VK_CONTROL)) != 0)
#define isShiftPressed	((0x8000 & GetKeyState(VK_SHIFT)) != 0)

#ifdef _DEBUG
	#define isActived 1
#else
	#define isActived (((CULVwBaseMAppEx*)::AfxGetApp())->m_strReg.GetLength() && (((CULVwBaseMAppEx*)::AfxGetApp())->m_strReg == MD5String(((CULVwBaseMAppEx*)::AfxGetApp())->m_strActive)))
#endif
#define _DOG_NEW
#include "IDB.h"

#ifndef ULVWBASE_API
	#ifdef ULVWBASE‌_EXPORT
		#define ULVWBASE_API __declspec( dllexport )
	#else	
		#define ULVWBASE_API __declspec( dllimport )
	#endif
#endif

class CGfxProperty;
class CMyRecentFileList;
class CULTool;
class CXMLSettings;
class CULDataEdit;
class CUnits;
class CULVwBaseMFrameEx;
class ULVWBASE_API CULVwBaseMAppEx : public CVwBaseMAppEx
{
	DECLARE_DYNCREATE(CULVwBaseMAppEx)
public:
	CULVwBaseMAppEx();
	virtual ~CULVwBaseMAppEx();
	//BOOL	m_bHiColorIcons;
	// Override from CBCGPWorkspace
	virtual void PreLoadState();
	virtual void LoadCustomState ();
	virtual void SaveCustomState ();
	BOOL DamDogQuery();
	BOOL CheckDongle();
	BOOL CheckHasp();
	BOOL CanbeActive();
	BOOL RealTimeCheckDog();
public:
	STARTUPINFO				m_StartupInfo;          
	PROCESS_INFORMATION		m_ProcessInfo;		// 用于启动ULDataManager
	//{{AFX_VIRTUAL(CULVwBaseMAppEx)
	public:
	virtual BOOL InitInstance(int nExecFlag = -1, CULVwBaseMFrameEx* pULVwBaseMFrameEx = NULL);
	virtual int ExitInstance();
	virtual CDocument* OpenDocumentFile(LPCTSTR lpszFileName);
	virtual LRESULT ProcessWndProcException(CException* e, const MSG* pMsg);	
	virtual BOOL OnDDECommand(LPTSTR lpszCommand);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CULVwBaseMAppEx)
	afx_msg void OnAppAbout();
	afx_msg void OnAppSubAbout();
	afx_msg void OnViewAppLook();
	afx_msg void OnUpdateFileMruFile1(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileMruProject1(CCmdUI* pCmdUI);
	//}}AFX_MSG
	afx_msg void OnOpenRecentFile(UINT nID);
	afx_msg void OnOpenRecentWorkspace(UINT nID);
	DECLARE_MESSAGE_MAP()
public:	
	UINT	GetClipboadFormat()	{ return m_nFormat;	}	// 井道间曲线拖动CF
	virtual void	LoadStdProfileSettings(UINT nMaxMRU);
	void	SaveStdProfileSettings();		// 保存最近工程、文件信息
	void	RestartDataManager();
	BOOL	RegShellFileType(LPCTSTR lpszFileType, int nIcon,
				LPCTSTR lpszFileTypeName, LPCTSTR pszFileIcon = NULL);
	void    UnRegShellFileType(LPCTSTR lpszFileType, LPCTSTR lpszFileTypeName);
	
	CWnd*	GetDMWnd();
	void	SetStatusPrompt(UINT nID);		// 设置状态栏提示信息
	void	SetStatusPrompt(LPCTSTR pszText);
	BOOL	InitTBManager(LPCTSTR pszFile);
	void	ReadConfig(CULTool* pULTool, LPCTSTR pszPath);
	void    TestDiskSpace();
	void	InitUnits();

	void FormatValue(CString& strValue, double fValue, int nPrecis);

	void BCGCBProCleanUp();
public:
	CMyRecentFileList*	m_pRecentWorkspace; // 最近工程链表

public:
	HANDLE		m_hSem;
	CString		m_strReg;
	CString		m_strActive;
	UINT		m_nFormat;		// Clipboard Format
	CImageList	m_bList;
	CImageList	m_wkspcImages;
	CWnd*		m_pDMWnd;
	BOOL		m_bResetBoards;
	CXMLSettings*	m_pXML;
	CString		m_strVersion;
	//CIPConfig	m_ipc;

	CString m_strMessage;
private:
	void ShowTipAtStartup(void);
	void ShowTipOfTheDay(void);
// 2019.10.14 演示版 Start
	int m_iAvailDays;
	BOOL m_bDemo;
// 2019.10.14 演示版 End
};

// ClipBoardData.h: interface for the CClipBoardData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLIPBOARDDATA_H__F9DDE985_8866_4D43_976E_85F6D36DCCB6__INCLUDED_)
#define AFX_CLIPBOARDDATA_H__F9DDE985_8866_4D43_976E_85F6D36DCCB6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CCurve;
class CClipBoardData : public CObject
{
public:
	CClipBoardData();
	~CClipBoardData();
	virtual void Serialize(CArchive& ar);

protected:
	DECLARE_SERIAL(CClipBoardData)

public:
	void ResetClipBoard();
	void AddCurve(CCurve* pCurve);
	BOOL IsEmpty();
public:
	CPtrArray	m_nIndexList;
	CPtrArray	m_ClipCurveList;
};

CClipBoardData* AfxGetClipBoardData();

#endif // !defined(AFX_CLIPBOARDDATA_H__F9DDE985_8866_4D43_976E_85F6D36DCCB6__INCLUDED_)

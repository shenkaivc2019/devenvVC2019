
// ChildFrm.h: CULChildFrame 类的接口
//

#pragma once

#include "ULDataEditClass.h"
#include "ClipBoardData.h"
#include "ULTableInfoView.h"
#include "FlexCorrect.h"

#define LT_UNLOCK	0x00000000
#define LT_LOCKED	0x00000001
#define LT_SAVING	0x00000002
#define LT_LOGING	0x00000004
#define LT_PRINT	0x00000008
#define LT_CLOSE	0x00000010
#define LT_EDIT		0x00000020

#define ST_SHOW		0x00000001
#define ST_CREATE	0x00000002

#ifndef ULVWBASE_API
	#ifdef ULVWBASE‌_EXPORT
		#define ULVWBASE_API __declspec( dllexport )
	#else	
		#define ULVWBASE_API __declspec( dllimport )
	#endif
#endif

class CULVwBaseMFrameEx;
class CSheet;
class CGraphView; // 测井
class CGraphWnd;
class CRawDataView;
class CGraphHeaderWnd; // 井道头
class CGraphHeaderView;
class CElucidateView;
class CPropertyFrame;
class CULFilter;
class CULBaseDoc;
class CULFileBaseDoc;
class CULBaseView;
class CSheet;
class ULVWBASE_API CULChildFrame : public CMDIChildWndEx
{
	DECLARE_DYNCREATE(CULChildFrame)

	friend class CGraphWnd; // 井道

public:
	//CULChildFrame() noexcept;
	CULChildFrame(DWORD dwType = 0, LPVOID lpParam = NULL, CULBaseDoc* lpDoc = NULL) noexcept;
// 特性
protected:
	CSplitterWndEx m_wndSplitter;
public:

// 操作
public:

// 重写
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void ActivateFrame(int nCmdShow = -1);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	//}}AFX_VIRTUAL

	virtual BOOL IsReadOnly();
	virtual BOOL CanShowOnMDITabs() { return (BOOL)(m_dwState & ST_SHOW); }
	void SetTitle(UINT nID)
	{
		if (GetSafeHwnd())
		{
			CString strText;
			strText.LoadString(nID);
			SetWindowText(strText);
		}
	}

	// Implementation
public:
	virtual ~CULChildFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// 生成的消息映射函数
protected:
	//{{AFX_MSG(CULChildFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClose();
	afx_msg void OnDataedit();
	afx_msg void OnUpdateDataedit(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSelectCurve(CCmdUI* pCmdUI);
	afx_msg void OnCurveeditMoveHorz();
	afx_msg void OnUpdateCurveeditMoveHorz(CCmdUI* pCmdUI);
	afx_msg void OnCurveeditMoveVert();
	afx_msg void OnUpdateCurveeditMoveVert(CCmdUI* pCmdUI);
	afx_msg void OnCurveeditFlexHorz();
	afx_msg void OnUpdateCurveeditFlexHorz(CCmdUI* pCmdUI);
	afx_msg void OnCurveeditFlexVert();
	afx_msg void OnCurveeditRound();
	afx_msg void OnCurveeditMirror();
	afx_msg void OnDataCopy();
	afx_msg void OnUpdateDataCopy(CCmdUI* pCmdUI);
	afx_msg void OnCurveeditSelinvert();
	afx_msg void OnCurveeditDel();
	afx_msg void OnUpdateCurveeditDel(CCmdUI* pCmdUI);
	afx_msg void OnCurveeditResume();
	afx_msg void OnUpdateCurveeditResume(CCmdUI* pCmdUI);
	afx_msg void OnCurveeditRealdel();
	afx_msg void OnUniformHighup();
	afx_msg void OnUniformHighdown();
	afx_msg void OnUniformLowup();
	afx_msg void OnUniformLowdown();
	afx_msg void OnUniformDepth();
	afx_msg void OnEditCutuniformHead();
	afx_msg void OnEditCutuniformShortest();
	afx_msg void OnEditCutuniformTrail();
	afx_msg void OnEditCutuniformDepthhead();
	afx_msg void OnEditCutuniformDepthtrail();
	afx_msg void OnCurveeditInterpolation();
	afx_msg void OnUpdateCurveeditInterpolation(CCmdUI* pCmdUI);
	afx_msg void OnCurveeditFilteragain();
	afx_msg void OnUpdateCurveeditFilteragain(CCmdUI* pCmdUI);
	afx_msg void OnCurveeditMoveHorzDetail();
	afx_msg void OnCurveeditMoveVertDetail();
	afx_msg void OnCurveeditAdjustBevelDown();
	afx_msg void OnCurveeditAdjustBevelUp();
	afx_msg void OnDatamoveHorz();
	afx_msg void OnUpdateDatamoveHorz(CCmdUI* pCmdUI);
	afx_msg void OnDataflexHorz();
	afx_msg void OnUpdateDataflexHorz(CCmdUI* pCmdUI);
	afx_msg void OnDataflexVert();
	afx_msg void OnUpdateDataflexVert(CCmdUI* pCmdUI);
	afx_msg void OnDataAdjustBevelDown();
	afx_msg void OnUpdateDataAdjustBevelDown(CCmdUI* pCmdUI);
	afx_msg void OnDataAdjustBevelUp();
	afx_msg void OnUpdateDataAdjustBevelUp(CCmdUI* pCmdUI);
	afx_msg void OnDataFilter();
	afx_msg void OnUpdateDataFilter(CCmdUI* pCmdUI);
	afx_msg void OnDataPropertybox();
	afx_msg void OnDataDelete1();
	afx_msg void OnDataDelete2();
	afx_msg void OnDataDelete3();
	afx_msg void OnDataDelete4();
	afx_msg void OnDataCut1();
	afx_msg void OnDataCut2();
	afx_msg void OnDataCut3();
	afx_msg void OnDataCut4();
	afx_msg void OnDatapasteReplace();
	afx_msg void OnDatapasteMove();
	afx_msg void OnDatapasteUp();
	afx_msg void OnDatapasteDown();
	afx_msg void OnDataeditDrag();
	afx_msg void OnUpdateDataeditDrag(CCmdUI* pCmdUI);
	afx_msg void OnDataeditUndo();
	afx_msg void OnUpdateDataeditUndo(CCmdUI* pCmdUI);
	afx_msg void OnDataeditRedo();
	afx_msg void OnUpdateDataeditRedo(CCmdUI* pCmdUI);
	afx_msg void OnCurveeditIntensity();
	afx_msg void OnCurveeditEnhasmooth();
	afx_msg void OnCurveeditEnhagaussion();
	afx_msg void OnCurveeditMedianfilter();
	afx_msg void OnCurveeditLinertrans();
	afx_msg void OnCurveeditEnhagradsharp();
	afx_msg void OnCurveeditEnhalaplace();
	afx_msg void OnCurveeditEqualize();
	afx_msg void OnCurveeditThreshold();
	afx_msg void OnCurveeditWindow();
	afx_msg void OnCurveeditStretch();
	afx_msg void OnCurveeditSplice();
	afx_msg void OnCurveeditPaste();
	afx_msg void OnUpdateCurveeditPaste(CCmdUI* pCmdUI);
	afx_msg void OnCurveeditAutoexpend();
	afx_msg void OnUpdateCurveeditAutoexpend(CCmdUI* pCmdUI);
	afx_msg void OnCurveeditMerge();
	afx_msg void OnCurveeditFilter();   //add by gj 20121224
	afx_msg void OnCurveCopy();
	afx_msg void OnUpdateCurveCopy(CCmdUI* pCmdUI);
	afx_msg void OnCurvePaste();
	afx_msg void OnUpdateCurvePaste(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCurveeditSplice(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCurveeditMerge(CCmdUI* pCmdUI);
	afx_msg void OnCurveeditSelDataSrc();
	afx_msg void OnUpdateCurveeditSelDataSrc(CCmdUI* pCmdUI);
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnCurvePreviewUp();
	afx_msg void OnUpdateCurvePreviewUp(CCmdUI* pCmdUI);
	afx_msg void OnCurvePreviewDown();
	afx_msg void OnUpdateCurvePreviewDown(CCmdUI* pCmdUI);
	afx_msg void OnCurveDepthCorrect();;
	afx_msg void OnUpdateDatapasteReplace(CCmdUI* pCmdUI);
	afx_msg void OnImageManage();
	afx_msg void OnUpdateImageManage(CCmdUI* pCmdUI);
	afx_msg void OnBkcurveAdjust();
	afx_msg void OnCurveMoving();
	afx_msg void OnUpdateCurveMoving(CCmdUI* pCmdUI);
	afx_msg void OnCurveTVDEmendation();
	afx_msg void OnUpdateCurveTVD(CCmdUI* pCmdUI);
	afx_msg void OnCalcVolume();
	afx_msg void OnUpdateCalcVolume(CCmdUI* pCmdUI);
	afx_msg void OnCalcTT();
	afx_msg void OnUpdateCalcTT(CCmdUI* pCmdUI);
	afx_msg void OnFlexBaseCurve();
	afx_msg void OnUpdateFlexBaseCurve(CCmdUI* pCmdUI);
	afx_msg void OnFlexAddRange();
	afx_msg void OnUpdateFlexAddRange(CCmdUI* pCmdUI);
	afx_msg void OnFlexAllCurves();
	afx_msg void OnUpdateFlexAllCurves(CCmdUI* pCmdUI);
	afx_msg void OnFlexCorrectCurve();
	afx_msg void OnUpdateFlexCorrectCurve(CCmdUI* pCmdUI);
	afx_msg void OnFlexDeleteRange();
	afx_msg void OnUpdateFlexDeleteRange(CCmdUI* pCmdUI);
	afx_msg void OnFlexDo();
	afx_msg void OnUpdateFlexDo(CCmdUI* pCmdUI);
	afx_msg void OnFlexStartPos();
	afx_msg void OnUpdateFlexStartPos(CCmdUI* pCmdUI);
	afx_msg void OnFlexEndPos();
	afx_msg void OnUpdateFlexEndPos(CCmdUI* pCmdUI);
	afx_msg void OnDataDepthMd();
	afx_msg void OnUpdateDataDepthMd(CCmdUI* pCmdUI);
	afx_msg void OnDataDepthTvd();
	afx_msg void OnUpdateDataDepthTvd(CCmdUI* pCmdUI);
	afx_msg void OnCurveArrayUp();
	afx_msg void OnCurveArrayDown();
	afx_msg void OnDataRealtimeTvd();
	afx_msg void OnUpdateDataRealtimeTvd(CCmdUI* pCmdUI);
	//}}AFX_MSG
	afx_msg void OnUpdateUniform(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditCutuniform(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDataDelete(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDataCut(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDataPaste(CCmdUI* pCmdUI);
	afx_msg void OnCurveDataSource(UINT nID);
	afx_msg void OnUpdateCurveDataSource(CCmdUI* pCmdUI);
	DECLARE_MESSAGE_MAP()

public:
	// 曲线编辑
	void DoFilter(CULFilter* pFilter, CCurve* pCurve);
	void DataEditPerformAll(DATA_EDIT_OPERATE opCode);
	void DataEditPerform(DATA_EDIT_OPERATE opCode);
	void DataEditPerform(DATA_EDIT_OPERATE opCode, CCurve* pCurve);
	long Uniform(int nType);
	long CutUniform(int nType);
	BOOL DataCanPaste() {	return !AfxGetClipBoardData()->IsEmpty();	}
	void AutoAdjustment(long lDepth, int index);
	
	int IsMatch(CString strName, CURVES arrCurve);	
	int GetToolsNum();
	void HideWindow();
	void CurveDepthEquidistance(CCurve *pCurve, long disGrow);
	UINT CurveDepthEmendation(long centerDept, long correct, 
									 CCurve *pCurve, CCurve *pCurveDev, BOOL bFromFile);
	CMark* AddFlexMark(int nIndex, DWORD dwColor);
	BOOL DeleteFlexMark(int nIndex);
	void ResetFlexMarks();
	CMark* AddShaderMark(DWORD dwColor, long lDepth);
	void DeleteShaderMark();
	
	static BOOL AddSpliceMark(int nIndex, DWORD dwColor);
	static BOOL DeleteSpliceMark(int nIndex);
	static void UpdateSpliceMarkDepth(int nIndex, float fDepth);
	static void ResetSpliceMarks();
public:
	static CPropertyFrame*	m_pDEPropFrame;

// Attributes
public:
	CView*				m_pView;		// 当前视指针
	DWORD				m_nViewType;	// 当前视类型
	
	CGraphView*			m_pGraphView;		// 测井
	CGraphHeaderWnd*	m_pGraphHeader;		// 井道头
	CGraphWnd*			m_pGraphWnd;		// 井道

	//CRawDataView*       m_pRDView;		// 原始数据
	CGraphHeaderView*	m_pGHView;		// 图表视图
	//CElucidateView*		m_pELView;		// 解释视图
	CULBaseView*			m_pULView;		// 仪器组合|仪器参数|井身结构

	CSheet*				m_pSheet;		// 绘图设置
	CULBaseDoc*				m_pDoc;			// 所属文档
	CULFileBaseDoc*			m_pULFile;		// 所属文件
	CFlexCorrect        m_FlexCorrect;
	BOOL				m_bAutoDes;
	BOOL				m_bDestroy;		// 关闭
	DWORD				m_dwLocked;		// 锁定
	DWORD				m_dwState;
	long                m_nAutoScrollStartDepth;
	long                m_nAutoScrollEndDepth;
	int                 m_nAutoScrollDirection;
	BOOL                m_bAutoScrollUp;
	BOOL                m_bAutoScrollDown;
	vec_ic				m_vecTVDCurve;
};

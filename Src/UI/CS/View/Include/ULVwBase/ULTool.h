﻿// ULTool.h: interface for the CULTool class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ULTOOL_H__0C7F0018_6776_4C96_B474_E4D8569454DB__INCLUDED_)
#define AFX_ULTOOL_H__0C7F0018_6776_4C96_B474_E4D8569454DB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Channel.h"
#include "Curve.h"
#include "IToolParam.h"
#include "IToolBoard.h"
#include "IULTool.h"
#include "IULToolEx.h"
#include "ITool2.h"

#include "ULCAL.h"
#include "ULEXFORMAT.h"
#include "ULFilter.h"
#include "ULCalibrate.h"
#ifdef _PERFORATION
#ifdef _OLD_SCOPES
#include "ULOscilliscope.h"
#endif
#include "DataExchangeHelp.h"
#endif
#include "ToolParam.h"
#include "ComConfig.h"
#include "ATool.h"
#include "IProject.h"
#include "IScoutControl.h"
#include "IULAlgorithm.h"

#ifndef ULVWBASE_API
	#ifdef ULVWBASE‌_EXPORT
		#define ULVWBASE_API __declspec( dllexport )
	#else	
		#define ULVWBASE_API __declspec( dllimport )
	#endif
#endif

#define SetCalDotStd		SetCalDotStandard
#define SetCalDotInitStd	SetCalDotInitStandard

//////////////////////////////////////////////////////////////////////////////
//
// Forward class declarations
//
//////////////////////////////////////////////////////////////////////////////

// Tool states
//
#define TS_IDLE		0		// 闲置
#define	TS_LOG		1		// 测井
#define	TS_CAL		2		// 刻度
#define	TS_VERIFY	3		// 校验

// Cal report
//
#define CR_GRAPH	0x0001	// 打印刻度图
#define CR_CURVE	0x0002	// 打印刻度曲线
#define CR_DATA		0x0004	// 打印刻度因子

// Cal file load
#define LS_SAVE		0x0001
#define LS_TIME		0x0002


#define	DL_CHANNEL		0x0001
#define DL_CURVE		0x0002
#define DL_STATE		0x0004
#define DL_CALIBRATE	0x0008
#define DL_ERROR		0x0010
#define EL_UNIT			0x0100

#define	DP_VIEWDATA		0x0001
#define DP_SAVEDATA		0x0002
#define DP_CALIBRATE	0x0004
#define DP_PARAMETER	0x0008

#ifdef _OLD_SCOPES

typedef struct tagScopeSettings
{
	int nXMax;       // X轴最大值
	int nYMax;       // Y轴最大值
	int nDataPerSec; // 每秒的数据量
	int nCycle;      // 采数周期
	int nCollTimes;  // 采集次数
	int nShowCmd;    // 显示命令 0 停止 1 监视 2 测井
	int nYMaxL;      // y 轴最大值的取值范围的左边界
	int nYMaxR;      // y 轴最大值的取值范围的右边界
	int nVGridCount; // 垂直格子数
	int nHGridCount; // 水平格子数
	int nDepthScopeCustomized; // 定制深度示波器 0:不定制 1：定制
	int nCurveCustomized;      // 定制示波器曲线
	int nScopeIndex;		   // 示波器索引 -1为所有示波器
	int nNumberType;			//工具面示波器外围数据类型
	int nParamID;				//要获取的参数ID
	BYTE bReserve[504];		   // 保留
}ScopeSettings;

#endif

// For the calibrate data to store or load
//
typedef struct tagCalFileHead	
{
	TCHAR szFile[32];			// 文件名
	TCHAR szTool[16];			// 仪器名
	TCHAR szToolSN[16];			// 仪器序列号
	TCHAR szTime[16];			// 刻度时间
	TCHAR szPhase[8];			// 刻度类型
	long  lTime;				// 刻度时间
	TCHAR szReserve[32];		// 刻度保留字节
}ULCALFILEHEAD;

typedef struct tagScopeUserDlg
{
	int    nScope;     // Scope ID
	CWnd*  pWnd;       // 示波器用户自定义对话框
	UINT   nID;      // ID 
}ScopeUserDlg;
typedef CArray<ScopeUserDlg, ScopeUserDlg> CScopeUserDlgArray;

// Tool control bar
//
#define ID_TOOLCTRL	32100
typedef struct tagCtrlBar
{
	CWnd*	pWnd;
	UINT	nID;
	CString strTitle;
	SIZE	size;
}CBInfo;
typedef CArray<CBInfo*, CBInfo*>	CBInfoArray;

typedef struct tagScopeCurvesInfo
{
	int nIndex;  // 示波器序列号
	CString strCurve;  // 要添加到示波器的曲线名
}SCInfo;
typedef CArray<SCInfo, SCInfo> SCInfoArray;

class CCalInfo;

class CTable;
typedef CArray<CTable*, CTable*>	CTableArray;

typedef CMap<DWORD, DWORD, void*, void*>	CMapDWORDToPtr;
typedef CMap<DWORD, DWORD, double, double>	CMapDWORDToDbl;
typedef CArray<CCurve*, CCurve*>	CCurveArray;
typedef CMap<int, int, CString, CString>	CMapIntToString;

typedef struct tagCALDESC 
{
	CString strName;
	CString strValue;
}CALDESC;

typedef CList<CALDESC*, CALDESC*> CCalDescs;

class CToolInfoPack;

struct IOBuffer
{
	BYTE*  pBuffer;
	int* pLength;   // 总长度
	int nChannelCount; // 通道数量
	int* pChannelLength; // 各通道长度
};

typedef CArray<IOBuffer, IOBuffer> CIOBufferArray;

const time_t tmNew = 946656000;

/////////////////////////////////////////////////////////////////////////////
// CULTool

class CCell2000;
class ULVWBASE_API CULTool : public IULTool, public CATool ,public IULToolEx, public IULScout, public IULAlgorithm
{

	DECLARE_ULI_MAPX()

// Construction
public:
	CULTool();
	virtual ~CULTool();

// Operations
public:
	//inline ULMETHOD GetIUnits(CUniI** pIUnits);
	inline ULMETHOD GetIUnits(CUnits* pIUnits);
	inline ULMDWD WriteToFile(CFile* pFile, DWORD dwType, IULTool* pToolUpdate = NULL);
	inline ULMDWD ReadFromFile(CFile* pFile, DWORD dwType);

	//------------------------------
	// Tools communicate methods:
	//------------------------------

	inline ULMINT GetToolCount()
	{	
		return m_vecTools.size();	
	}

	inline ULMINT GetUserDefineFilterType(int nCurve)
	{
		int nType = 0;
		if (nCurve >= 0 && nCurve < m_Curves.GetSize())
		{
			CCurve* pCurve = (CCurve*)m_Curves.GetAt(nCurve);
			nType = pCurve->m_nUserFilter;
		}
		return nType;
	}

	inline ULMETHOD SetUserDefineFilterType(int nCurve, int nType)
	{
		if (nCurve >= 0 && nCurve < m_Curves.GetSize())
		{
			CCurve* pCurve = (CCurve*)m_Curves.GetAt(nCurve);
			pCurve->m_nUserFilter = nType;
			return UL_NO_ERROR;
		}
		return UL_ERROR;
	}

	inline ULMETHOD GetTools(ULTOOLS& vecTools)
	{
		vecTools.assign(m_vecTools.begin(), m_vecTools.end());
		return UL_NO_ERROR;
	}

	inline ULMPTR GetTool(int nIndex)
	{	
		if (nIndex < 0)
			return NULL;
		else if (nIndex < m_vecTools.size())
			return &m_vecTools[nIndex];
		return NULL;	
	}

	inline ULMPTR GetTool(LPCTSTR lpszToolName)
	{
		CString strTool = lpszToolName;
		for (int i = 0; i < m_vecTools.size(); i++)
		{
			CULTool* pULTool = (CULTool*)m_vecTools[i].pIULTool;
			if (pULTool->strToolName == strTool)
				return &m_vecTools[i];
		}
		return NULL;
	}

	inline ULMPTR GetIULTool(LPCTSTR lpszToolName)
	{
		CString strTool = lpszToolName;
		for (int i = 0; i < m_vecTools.size(); i++)
		{
			CULTool* pULTool = (CULTool*)m_vecTools[i].pIULTool;
			if (pULTool->strToolName == strTool)
				return m_vecTools[i].pIULTool;
		}
		return NULL;
	}

	inline ULMPTR GetITool(LPCTSTR lpszToolName)
	{
		CString strTool = lpszToolName;
		for (int i = 0; i < m_vecTools.size(); i++)
		{
			CULTool* pULTool = (CULTool*)m_vecTools[i].pIULTool;
			if (pULTool->strToolName == strTool)
				return m_vecTools[i].pITool;
		}
		return NULL;
	}

	//----------------------------------
	// Tool-board communicate methods:
	//----------------------------------

	BOOL MapExchangeFile();		// Create file mapping
	void UnMapExchangeFile();	// Close file mapping
	ULMDWD SendCommand(int nChannelNum, PTBYTE pCmdBuf, UINT nSize);
	DWORD SendAskCmd(AskCommand * pCmd);

	//--------------------------------
	// Load / Unload user tool:
	//--------------------------------
	
	BOOL LoadTool(CString strToolName, BOOL bQuiet = FALSE);
	void ReleaseTool(BOOL bEmptyPath = TRUE);
	ULMETHOD ReloadTool(LPCTSTR lpszToolFile);
	
	//------------------------------
	// Tool initialize:
	//------------------------------

	ULMETHOD SetToolProperty(	int nCount, 
								LPCTSTR lpszName, 
								LPCTSTR lpszENName,
								LPCTSTR lpszCNName,	
								LPCTSTR lpszType, 
								float fToolLen, 
								float fBasePoint );

	ULMPTR AllocNewChannel(	DWORD nBufferSize, 
							LPCTSTR lpszType, 
							double dblRate,
							long lPrecision, 
							void* pDataItem /* =NULL */);

	ULMPTR AddCurve(LPCTSTR lpszName,	
					LPCTSTR lpszUnit,
					double fLeftValue, 
					double fRightValue,
					long lDepthOffset,
					int nOffMode = OM_TB, 
					int nFilterPiont = 0,	
					int nFilterType = 2,
					int nDimension = 1, 
					int nPointFrame = 1,
					int nCurveMode = CURVE_NORMAL,
					int nValueType = VT_R4,
					LPCTSTR lpszIDName = NULL);

	ULMPTR AddStatusCurve(	LPCTSTR lpszName, 
							LPCTSTR lpszUnit,
							double fLeftValue,
							double fRightValue,
							int nDimension = 1,
							int nPointFrame = 1,
							int nCurveMode = CURVE_STATUS,
							int nValueType = VT_R4,
							LPCTSTR lpszIDName = NULL);


	//------------------------------------
	// Add user window / Declare scope:
	//------------------------------------

	ULMETHOD AddUserDlg(void* pDlg, UINT nID, LPCTSTR lpszTitle = NULL);
	void ClearCtrlWnds();
	ULMETHOD AddUserCalDlg(void* pDlg, UINT nID); 
	ULMETHOD AddToolScope(int nType, LPCTSTR lpszName);
	//------------------------------
	// Channel operations:
	//------------------------------

	ULMPTR GetChannelBuffer(int nIndex);	// Get channel buffer	
	ULMPTR GetChannelBuffer(LPCTSTR lpszChannelName);

	BOOL AllocChannel(CChannel* pChannel, int nIndex);	// Alloc channel
	void AddChannel(CChannel* pChannel);	// Add channel
	void ClearChannels();				// Clear all channels
	
	ULMINT GetAllChannelSize();				// Get all channel buffer size
	ULMINT GetAllocChannelSize();           // 自身申请的通道大小
	ULMINT GetGroupIndex() { return nGroupIndex; }      // 仪器分组号
	int GetChannelOffset(int nChannelNum);	// Get the channel buffer offset 
											// 指定通道在总长度中的偏移量

	int GetChannelLength(int nChannel);
	//------------------------------
	// Add curve data:
	//------------------------------

	ULMETHOD AddCurveData(int nCurve, long lDepth, void* pValue, long lTime = 0);
	ULMETHOD AddCurveData(LPCTSTR lpszName, long lDepth, void* pValue, long lTime = 0, LPCTSTR lpszIDName = NULL);

	ULMETHOD AddStatusCurveData(int nCurve, void* pValue, int nShowPos = 0);
	ULMETHOD AddStatusCurveData(LPCTSTR lpszName, void* pValue, int nShowPos =0, LPCTSTR lpszIDName = NULL);

	//------------------------------
	// Log operations:
	//------------------------------

	ULMETHOD OpenTool();	// Alloc channels
	ULMETHOD CloseTool();	// Release channels

	ULMETHOD BeforeLog(ULLogInfo* pLogInfo);
	ULMETHOD LogIO();
	ULMETHOD AfterLog();

	//------------------------------
	// Replay operations:
	//------------------------------

	ULMETHOD ReplayOpenTool();	// Add tool to vector
	ULMETHOD ReplayCloseTool(); // Remove tool from vector

	inline ULMETHOD BeforeReplay()
	{
		if(m_pITool)
			m_pITool->BeforeReplay();
		return UL_NO_ERROR;
	}

	inline ULMETHOD ReplayIO()
	{
		if (m_pITool)
			m_pITool->LogIO();
		return UL_NO_ERROR;
	}

	inline ULMETHOD AfterReplay()
	{
		if(m_pITool)
			m_pITool->AfterReplay();
		return UL_NO_ERROR;
	}

	//--------------------------------
	// Drive by time:
	//--------------------------------

	ULMLNG SetTimeStartInfo(float fStartTime, int nDirection);
	ULMLNG SetRelativeTime();

	//------------------------------
	// LCD panel properties:
	//------------------------------

	inline ULMETHOD	SetCurDepth(long lDepth) 
	{
		InterlockedExchange(&m_lCurDepth, lDepth);
		m_lRDSDepth = lDepth;
		if (m_nWorkMode != WORKMODE_COMPUTEPLAYBACK)
		{
			m_lCurDepthIndex++;
		}
		return UL_NO_ERROR; 
	}

	ULMLNG GetCurDepth();

	ULMPTR GetProjectObj()
	{
		return m_pIProject;
	}
	
	inline ULMETHOD	SetCurTime(long lTime)
	{ 
		InterlockedExchange(&m_lCurTime, lTime);
		return UL_NO_ERROR;
	}

	inline ULMETHOD SetCurTime(SYSTEMTIME sysTime)
	{
		m_sysCurTime = sysTime;
		return UL_NO_ERROR;
	}
	inline ULMLNG GetCurTime()
	{
		return m_lCurTime;
	}

	inline ULMETHOD	SetCurSpeed(double fSpeed)
	{
		CULTool::m_fCurSpeed = fSpeed;
		return UL_NO_ERROR;
	}

	inline ULMDBL GetCurSpeed()	
	{	
		return CULTool::m_fCurSpeed;
	}

	inline ULMETHOD	SetCurVol(double fVol)
	{ 
		CULTool::m_fCurVol = fVol;
		return UL_NO_ERROR;
	}

	inline ULMDBL GetCurVol()
	{	
		return CULTool::m_fCurVol;
	}

	inline ULMETHOD	SetCurTemp(double fTemp)
	{ 
		CULTool::m_fCurTemp = fTemp;
		return UL_NO_ERROR; 
	}

	inline ULMDBL GetCurTemp()
	{	
		return CULTool::m_fCurTemp;
	}

	inline ULMETHOD	SetCurTension(double fTension)
	{ 
		m_fCurTension = fTension;
		return UL_NO_ERROR;
	}

	inline ULMDBL GetCurTension()
	{	
		return CULTool::m_fCurTension;
	}

	//------------------------------
	// Tool properties:
	//------------------------------
	
	inline ULMETHOD	GetToolName(LPTSTR lpszToolName)
	{
		_tcscpy(lpszToolName, strToolName);
		return UL_NO_ERROR;
	}

	inline ULMTSTR ToolName()
	{
		return (LPTSTR)(LPCTSTR)strToolName;
	}

	inline ULMETHOD	GetToolDllName(LPTSTR lpszDllName)
	{
		_tcscpy(lpszDllName, m_strToolDllName);
		return UL_NO_ERROR;
	}

	ULMETHOD GetToolPath(LPTSTR pszPath);

	inline CString GetToolDllPath()
	{
		int iFind = m_strToolDllName.ReverseFind('\\');
		if (iFind > 0)
			return m_strToolDllName.Left(iFind + 1);

		return _T("");
	}

	inline ULMTSTR ToolDllName()
	{
		return (LPTSTR)(LPCTSTR)m_strToolDllName;
	}

	CString	GetEventName();	// Tool event name

	inline ULMETHOD	GetToolProp(void* pToolProp)
	{
		if (AfxIsValidAddress(pToolProp, sizeof(TOOL)))
		{
			*(LPTOOL)pToolProp = *this;
		}
		return UL_ERROR;
	}

	inline void	SetToolInfoPack(DWORD pToolInfoPack)
	{
		m_pToolInfoPack = (CToolInfoPack*)pToolInfoPack;
	}

	inline void SetWorkMode(int nWorkMode)	
	{ 
		m_nWorkMode = nWorkMode; 
	}

	inline int SetToolState(int nToolState)
	{
		int nOldState = m_nToolState;
		m_nToolState = nToolState;
		return nOldState;
	}

	inline int GetToolState()
	{
		return m_nToolState;
	}

	// 显示模式：测井/监视/射孔取芯
	inline int SetCurDisplayMode(int nDispMode)
	{
		int nOldMode = m_nCurDisplayMode;
		m_nCurDisplayMode = nDispMode; 
		return nOldMode;
	}

	inline int GetCurDisplayMode()
	{
		return m_nCurDisplayMode;
	}

	//------------------------------
	// Curve operations:
	//------------------------------

	void AddCurve(CCurve* pCurve);
	void ClearCurves();
	void ClearStatusCurves();
	inline void ClearCurvesData()
	{
		for (int i = m_Curves.GetSize() - 1; i > -1; i--)
		{
			CCurve* pCurve = m_Curves.GetAt(i);
			if (pCurve != NULL)
			{
				pCurve->ResetData();
				pCurve->m_nCurDisplayPos = 0;
			}
		}
	}

	inline void MoveCurvesData(CPtrArray* pCurves)
	{
		pCurves->RemoveAll();
		int nCurves = m_Curves.GetSize();
		for (int i = 0; i < nCurves; i++)
		{
			CCurve* pCurve = m_Curves.GetAt(i);
			CCurveData* pData = new CCurveData(pCurve->m_pData);
			pData->Copy(pCurve->m_pData);
			pCurves->Add(pData);
			pCurve->m_pData->m_arrData.RemoveAll();
		}
	}

	inline void CopyCurvesData(CPtrArray* pCurves)
	{
		int nCurves = m_Curves.GetSize();
		for (int i = 0; i < nCurves; i++)
		{
			CCurve* pCurve = m_Curves.GetAt(i);
			CCurveData* pData = (CCurveData*)pCurves->GetAt(i);
			pCurve->m_pData->Copy(pData);
			delete pData;
		}
	}

	ULMINT GetCurveCount() 
	{ 
		return m_Curves.GetSize(); 
	}

	ULMINT GetDisplayCurveCount()  //得到仪器在DataView里要显示的曲线个数
	{
		int count = 0;
		int i = 0;
		for( ; i < m_Curves.GetSize(); i++)
		{
			CCurve *pCurve = m_Curves.GetAt(i);
            if (pCurve->IsView())
				count++;
		}

		for (i = 0; i < m_StatusCurves.GetSize(); i++)
		{
			CCurve *pCurve=m_StatusCurves.GetAt(i);
            if (pCurve->IsView())
				count++;
		}

		return count;
	}

	ULMPTR GetCurve(int nIndex)
	{
		if(nIndex < 0)
			return NULL;
		else if (nIndex < m_Curves.GetSize())
			return m_Curves.GetAt(nIndex);
		return NULL;
	}

	ULMETHOD GetCurves(CURVES& vecCurve);
	ULMPTR GetCurve(LPCTSTR lpszName, LPCTSTR lpszIDName = NULL);
	ULMPTR GetStatusCurve(LPCTSTR lpszName, LPCTSTR lpszIDName = NULL);

	ULMPTR GetStatusCurve(int nIndex)
	{
		if (nIndex < 0)
			return NULL;
		else if (nIndex < m_StatusCurves.GetSize())
			return m_StatusCurves.GetAt(nIndex);
		return NULL;
	}
	
	inline ULMETHOD GetToolCurves(LPCTSTR lpszToolName, CURVES& vecCurve)
	{
		for (int i = 0; i < m_vecTools.size(); i++)
		{
			CULTool* pULTool = (CULTool*)m_vecTools.at(i).pIULTool;
			if (_tcscmp(pULTool->strToolName, lpszToolName) == 0)
				return pULTool->GetCurves(vecCurve);			
		}
		return UL_ERROR;
	}

	inline ULMINT GetToolCurveCount(LPCTSTR lpszToolName)
	{
		for (int i = 0; i < m_vecTools.size(); i++)
		{
			CULTool* pULTool = (CULTool*)m_vecTools.at(i).pIULTool;
			if (_tcscmp(pULTool->strToolName, lpszToolName) == 0)
				return pULTool->GetCurveCount();	
		}
		return -1;
	}

	inline ULMPTR GetToolCurve(LPCTSTR lpszToolName, LPCTSTR lpszCurveName, LPCTSTR lpszIDName = NULL)
	{
		for (int i = 0; i < m_vecTools.size(); i++)
		{
			CULTool* pULTool = (CULTool*)m_vecTools.at(i).pIULTool;
			if(_tcscmp(pULTool->strToolName, lpszToolName)==0)
				return pULTool->GetCurve(lpszCurveName, lpszIDName);	
		}
		return NULL;
	}
	
	ULMETHOD GetULCurves(CURVES& vecCurve, UINT nFile = 0);
	ULMPTR GetULCurve(LPCTSTR lpszCurveName, UINT nFile = 0, LPCTSTR lpszIDName = NULL);

	//------------------------------
	// Table operations:
	//------------------------------

	ULMPTR OpenTable(LPCTSTR lpszTableName, LPCTSTR lpszTableCHName = NULL);
	ULMPTR CreateTable(LPCTSTR lpszTableName, LPCTSTR lpszTableCHName, int nField);
	ULMETHOD CloseTable(LPCTSTR lpszTableName, LPCTSTR lpszTableCHName = NULL);

	//------------------------------
	// Cell declaration:
	//------------------------------

	ULMETHOD AddCell(LPCTSTR lpszCell)
	{
		CString str = lpszCell;
		str += "(" + strToolName + ")";
		m_arCellName.Add(str);
		return UL_NO_ERROR;
	}
	
	ULMPTR GetCell(LPCTSTR lpszCell)
	{
		return NULL;
	}
	
	ULMETHOD UpdateData(LPCTSTR lpszCell, BOOL bSaveAndValidate = TRUE);
	ULMETHOD UpdateData(BOOL bSaveAndValidate = TRUE);

	//------------------------------
	// Data exchange helper:
	//------------------------------

	ULMETHOD DDX(LPCTSTR lpszID, float* fValue)
	{
#ifdef _PERFORATION
		CString str = strToolName + "::" + lpszID;
		Gbl_DataEx.AddDataExchange(str, fValue);
#endif
		return UL_NO_ERROR;
	}
	
	ULMETHOD DDX(LPCTSTR lpszID, double* dValue)
	{
#ifdef _PERFORATION
		CString str = strToolName + "::" + lpszID;
		Gbl_DataEx.AddDataExchange(str, dValue);
#endif
		return UL_NO_ERROR;
	}
	
	ULMETHOD DDX(LPCTSTR lpszID, int* nValue)
	{
#ifdef _PERFORATION
		CString str = strToolName + "::" + lpszID;
		Gbl_DataEx.AddDataExchange(str, nValue);
#endif
		return UL_NO_ERROR;
	}
	
	ULMETHOD DDX(LPCTSTR lpszID, CString* strValue)
	{
#ifdef _PERFORATION
		CString str = strToolName + "::" + lpszID;
		Gbl_DataEx.AddDataExchange(str, strValue);
#endif
		return UL_NO_ERROR;
	}
	
	ULMETHOD DDX(LPCTSTR lpszID, void* lpPaintFunc)
	{
#ifdef _PERFORATION
		CString str = strToolName + "::" + lpszID;
		Gbl_DataEx.AddDataExchange(str, lpPaintFunc);
#endif
		return UL_NO_ERROR;
	}
	
	ULMETHOD DDXF(LPCTSTR strID, void* lpFunc)
	{
#ifdef _PERFORATION
		CString str = strToolName + "::" + strID;
		PPAINTFUNC* p1 = new PPAINTFUNC;
		memcpy(p1, lpFunc, sizeof(p1));
		Gbl_DataEx.AddDataExchange(str, p1);
#endif
		return UL_NO_ERROR;
	}

	ULMPTR GetValue(LPCTSTR lpszID)
	{
		void* pRet = NULL;
#ifdef _PERFORATION
		Gbl_DataEx.FindDataFromID(lpszID, (void*)pRet);
#endif
		return pRet;
	}

	//------------------------------
	// Tool parameter operations:
	//------------------------------

	ULMETHOD SaveParams();	// Save param to rawfile
	ULMETHOD SaveParam(LPCTSTR pszKey);
	

	ULMBOOL	AddParam(LPCTSTR pszKey, int iVal, LPCTSTR pszType = _T(""), LPCTSTR pszDesc = _T(""), 
		LPCTSTR pszUnit = _T(""), CStringArray* pArray = NULL, LPCTSTR pszComment = _T(""), DWORD dwPrint = PS_DEFAULT)
	{
		CToolParam* pTP = m_Params.GetParam(pszKey);
		if (pTP)
		{
			if (pArray != NULL)
				pTP->m_strArray.Copy(*pArray);

			pTP->SetVal(iVal);
			pTP->m_strType = pszType;
			pTP->m_strDesc = pszDesc;
			pTP->m_strUnit = pszUnit;
			pTP->m_strComment = pszComment;
            pTP->m_bDisplayPrint = dwPrint;
			return TRUE;
		}

		return FALSE;
	}

	ULMBOOL	AddParam2(LPCTSTR pszKey, int iVal, LPCTSTR pszList, LPCTSTR pszType = NULL, 
		LPCTSTR pszDesc = NULL, LPCTSTR pszUnit = NULL, LPCTSTR pszComment = NULL, DWORD dwPrint = PS_DEFAULT);
	ULMBOOL	AddParam2(LPCTSTR pszKey, LPCTSTR pszVal, LPCTSTR pszList, LPCTSTR pszType = NULL, 
		LPCTSTR pszDesc = NULL, LPCTSTR pszUnit = NULL, LPCTSTR pszComment = NULL, DWORD dwPrint = PS_DEFAULT);
	
	
	ULMBOOL	AddParam(LPCTSTR pszKey, UINT uiVal, LPCTSTR pszType = NULL,
		LPCTSTR pszDesc = NULL, LPCTSTR pszUnit = NULL, LPCTSTR pszComment = NULL, DWORD dwPrint = PS_DEFAULT)
	{
		CToolParam* pTP = m_Params.GetParam(pszKey);
		if (pTP)
		{
			pTP->SetVal(uiVal);
			pTP->m_strType = pszType;
			pTP->m_strDesc = pszDesc;
			pTP->m_strUnit = pszUnit;
			pTP->m_strComment = pszComment;
            pTP->m_bDisplayPrint = dwPrint;
			return TRUE;
		}
		
		return FALSE;
	}
	
	ULMBOOL	AddParam(LPCTSTR pszKey, float& fVal, LPCTSTR pszType = _T(""),
		LPCTSTR pszDesc = _T(""), LPCTSTR pszUnit = _T(""), LPCTSTR pszComment = _T(""), DWORD dwPrint = PS_DEFAULT)
	{
		CToolParam* pTP = m_Params.GetParam(pszKey);
		if (pTP)
		{
			pTP->SetVal(fVal);
			pTP->m_strType = pszType;
			pTP->m_strDesc = pszDesc;
			pTP->m_strUnit = pszUnit;
			pTP->m_strComment = pszComment;
            pTP->m_bDisplayPrint = dwPrint;
			return TRUE;
		}
		
		return FALSE;
	}
	
	ULMBOOL	AddParam(LPCTSTR pszKey, double& dVal, LPCTSTR pszType = _T(""),
		LPCTSTR pszDesc = _T(""), LPCTSTR pszUnit = _T(""), LPCTSTR pszComment = _T(""), DWORD dwPrint = PS_DEFAULT)
	{
		CToolParam* pTP = m_Params.GetParam(pszKey);
		if (pTP)
		{
			pTP->SetVal(dVal);
			pTP->m_strType = pszType;
			pTP->m_strDesc = pszDesc;
			pTP->m_strUnit = pszUnit;
			pTP->m_strComment = pszComment;
            pTP->m_bDisplayPrint = dwPrint;
			return TRUE;
		}
		
		return FALSE;
	}
	
	ULMBOOL	AddParam(LPCTSTR pszKey, LPCTSTR pszVal, LPCTSTR pszType = _T(""), LPCTSTR pszDesc = _T(""),
		LPCTSTR pszUnit = _T(""), CStringArray* pArray = NULL, LPCTSTR pszComment = _T(""), DWORD dwPrint = PS_DEFAULT)
	{
		CToolParam* pTP = m_Params.GetParam(pszKey);
		if (pTP)
		{
			if (pArray != NULL && (pArray->GetSize() > 0))
			{
				pTP->m_strArray.Copy(*pArray);
				pTP->m_nValType = VT_INT|VT_ARRAY;
			}

			pTP->SetVal(pszVal);
			pTP->m_strType = pszType;
			pTP->m_strDesc = pszDesc;
			pTP->m_strUnit = pszUnit;
			pTP->m_strComment = pszComment;            
			pTP->m_bDisplayPrint = dwPrint;
			return TRUE;
		}
		
		return FALSE;
	}
	
	ULMBOOL	AddParam(LPCTSTR pszKey, LPBYTE pData, DWORD dwBytes, LPCTSTR pszType = _T(""), 
		LPCTSTR pszDesc = _T(""), LPCTSTR pszUnit = _T(""), LPCTSTR pszComment = _T(""), DWORD dwPrint = PS_DEFAULT)
	{
		CToolParam* pTP = m_Params.GetParam(pszKey);
		if (pTP)
		{
			pTP->SetVal(pData, dwBytes);
			pTP->m_strType = pszType;
			pTP->m_strDesc = pszDesc;
			pTP->m_strUnit = pszUnit;
			pTP->m_strComment = pszComment;
            pTP->m_bDisplayPrint = dwPrint;
			return TRUE;
		}
		
		return FALSE;
	}

	ULMBOOL AddParam(LPCTSTR pszKey, short sVal, LPCTSTR pszType = _T(""), 
		LPCTSTR pszDesc = _T(""), LPCTSTR pszUnit = _T(""), LPCTSTR pszComment = _T(""), DWORD dwPrint = PS_DEFAULT)
	{
		CToolParam* pTP = m_Params.GetParam(pszKey);
		if (pTP)
		{
			pTP->SetVal(sVal);
			pTP->m_strType = pszType;
			pTP->m_strDesc = pszDesc;
			pTP->m_strUnit = pszUnit;
			pTP->m_strComment = pszComment;
			pTP->m_bDisplayPrint = dwPrint;
			return TRUE;
		}
		
		return FALSE;
	}

	ULMBOOL	AddParam(LPCTSTR pszKey, long lVal, LPCTSTR pszType = _T(""), 
		LPCTSTR pszDesc = _T(""), LPCTSTR pszUnit = _T(""), LPCTSTR pszComment = _T(""), DWORD dwPrint = PS_DEFAULT)
	{
		CToolParam* pTP = m_Params.GetParam(pszKey);
		if (pTP)
		{
			pTP->SetVal(lVal);
			pTP->m_strType = pszType;
			pTP->m_strDesc = pszDesc;
			pTP->m_strUnit = pszUnit;
			pTP->m_strComment = pszComment;
            pTP->m_bDisplayPrint = dwPrint;
			return TRUE;
		}
		
		return FALSE;
	}

	ULMBOOL AddParam(LPCTSTR pszKey, WORD wVal, LPCTSTR pszType = _T(""), 
		LPCTSTR pszDesc = _T(""), LPCTSTR pszUnit = _T(""), LPCTSTR pszComment = _T(""), DWORD dwPrint = PS_DEFAULT)
	{
		CToolParam* pTP = m_Params.GetParam(pszKey);
		if (pTP)
		{
			pTP->SetVal(wVal);
			pTP->m_strType = pszType;
			pTP->m_strDesc = pszDesc;
			pTP->m_strUnit = pszUnit;
			pTP->m_strComment = pszComment;
            pTP->m_bDisplayPrint = dwPrint;
			return TRUE;
		}

		return FALSE;
	}

	ULMBOOL AddParam(LPCTSTR pszKey, DWORD dwVal, LPCTSTR pszType = _T(""), 
		LPCTSTR pszDesc = _T(""), LPCTSTR pszUnit = _T(""), LPCTSTR pszComment = _T(""), DWORD dwPrint = PS_DEFAULT)
	{
		CToolParam* pTP = m_Params.GetParam(pszKey);
		if (pTP)
		{
			pTP->SetVal(dwVal);
			pTP->m_strType = pszType;
			pTP->m_strDesc = pszDesc;
			pTP->m_strUnit = pszUnit;
			pTP->m_strComment = pszComment;
            pTP->m_bDisplayPrint = dwPrint;
			return TRUE;
		}

		return FALSE;
	}
	
	ULMBOOL	GetParam(LPCTSTR pszKey, int& iVal)
	{
		return m_Params.GetParam(pszKey, iVal);
	}
	
	ULMBOOL	GetParam(LPCTSTR pszKey, UINT& uiVal)
	{
		return m_Params.GetParam(pszKey, uiVal);
	}
	
	ULMBOOL	GetParam(LPCTSTR pszKey, float&	fVal)
	{
		return m_Params.GetParam(pszKey, fVal);
	}
	
	ULMBOOL	GetParam(LPCTSTR pszKey, double& dVal)
	{
		return m_Params.GetParam(pszKey, dVal);
	}
	
	ULMBOOL GetParam(LPCTSTR pszKey, CString& sVal)
	{
		return m_Params.GetParam(pszKey, sVal);
	}
	
	ULMDWD GetParam(LPCTSTR pszKey, LPBYTE pData)
	{
		return m_Params.GetParam(pszKey, pData);
	}

	ULMBOOL GetParam(LPCTSTR pszKey, short& sVal)
	{
		return m_Params.GetParam(pszKey, sVal);
	}

	ULMBOOL	GetParam(LPCTSTR pszKey, long& lVal)
	{
		return m_Params.GetParam(pszKey, lVal);
	}

	ULMBOOL	GetParam(LPCTSTR pszKey, WORD& wVal)
	{
		return m_Params.GetParam(pszKey, wVal);
	}

	ULMBOOL	GetParam(LPCTSTR pszKey, DWORD&	dwVal)
	{
		return m_Params.GetParam(pszKey, dwVal);
	}

	ULMBOOL GetULParam(UINT nID, void* pValue);
	ULMBOOL SetULParam(UINT nID, void* pValue);

	// ------- UNUSED -------
	ULMPTR GetParamBuffer(LPCTSTR lpszParam, BOOL bVerifyTime = TRUE) { return NULL; }
	ULMETHOD SetParamSize(LPCTSTR lpszParam, ULONG cbSize) { return UL_ERROR; }
	ULMLNG GetParamSize(LPCTSTR lpszParam)	{ return 0;	}
	// ------- UNUSED -------

	//------------------------------
	// Depth offset mode:
	//------------------------------

	ULMETHOD GroupWithTool(LPCTSTR strToolName);
	ULMETHOD DefineOffsetMode(LPCTSTR lpszName, UINT nMode)
	{
		return UL_NO_ERROR;
	}

	//------------------------------
	// Sensors operations:
	//------------------------------

	ULMETHOD AddMeasurePoint(LPCTSTR lpszPoint, long lPoint, BOOL bShow = TRUE)
	{
		CSensor* pMP = new CSensor(lpszPoint, lPoint, bShow);
		m_Sensors.Add(pMP);
		return UL_NO_ERROR;
	}
	
	void ClearSensors();
	void SerializeProps(CXMLSettings& xml, DWORD dwSN = 0x03);
	void SerializeCurves(CXMLSettings& xml);
	ULMETHOD SerializeCalInfo(CXMLSettings& xml, UINT uPhase, BOOL bTime = TRUE);
	ULMETHOD SerializeCalInfo(CXMLSettings& xml, BOOL bTime = TRUE);


	ULMETHOD AddCalDesc(LPCTSTR pszName, LPCTSTR pszValue);
	ULMETHOD UpdateCalDesc(LPCTSTR pszName, LPCTSTR pszValue);
	ULMETHOD ClearCalDescs();
	ULMETHOD DeleteCalDescs(LPCTSTR pszName);
	ULMTSTR	 GetCalDesc(LPCTSTR pszName);


	ULMETHOD AddCalDesc(UINT uPhase, LPCTSTR pszName, LPCTSTR pszValue);
	ULMETHOD UpdateCalDesc(UINT uPhase, LPCTSTR pszName, LPCTSTR pszValue);
	ULMETHOD ClearCalDescs(UINT uPhase);
	ULMETHOD DeleteCalDescs(UINT uPhase, LPCTSTR pszName);
	ULMTSTR	 GetCalDesc(UINT uPhase, LPCTSTR pszName);
	//--------------------------
	// Calibrate operations:
	//--------------------------
	BOOL LoadCalInfo();
	void LoadCalInfo(CString str, DWORD dwLS = 0, BOOL* pCheck = NULL);

	inline ULMBOOL	NeedCalibrate() 
	{ 
		return m_dwCalibrate; 
	}

    inline ULMDWD NeedUpdateCalibrate()
    {
        return m_dwCalUpdate; 
    }
	
	inline ULMETHOD	SetCalType(int nCalType)
	{
		for (int i=0; i<4; i++)
		{
			m_nCalType[i] = nCalType;
			m_bCalDefined[i] = TRUE;	
		}

		if (nCalType == UL_CAL_USER)
			m_dwCalibrate = CP_MASTER|CP_BEFROR|CP_AFTER;
		
		return UL_NO_ERROR;
	}
	
	inline ULMETHOD SetCalType(UINT uPhase, int nCalType, void* pWnd = NULL, UINT nID = 0)
	{
		if (uPhase > 3)
			return UL_ERROR;

		m_nCalType[uPhase] = nCalType;
		m_CalWnds[uPhase] = (CWnd*)pWnd;
		m_CalWndsID[uPhase] = nID;
		m_bCalDefined[uPhase] = TRUE;
		m_dwCalibrate = m_dwCalibrate | dwCalPhase[uPhase];
		return UL_NO_ERROR;
	}
	
	inline ULMINT GetCalType() 
	{	
		if (m_uCalPhase < 0)
			m_uCalPhase = 0;
		else if (m_uCalPhase > 3)
			m_uCalPhase = 3;
		
		return m_nCalType[m_uCalPhase];	
	}

	inline ULMBOOL IsCalUserPrint()
	{
		if (m_uCalPhase < 0)
			m_uCalPhase = 0;
		else if (m_uCalPhase > 3)
			m_uCalPhase = 3;
		return m_bCalUserPrint[m_uCalPhase];
	}
	
	inline ULMETHOD EnableCalUserPrint(UINT uPhase)
	{
	    if (m_uCalPhase < 0 || m_uCalPhase > 3)
			return UL_ERROR;
		m_bCalUserPrint[uPhase] = TRUE;
		return UL_NO_ERROR;
	}

	inline ULMETHOD SetCalPhase(UINT uPhase)
	{
		if (uPhase > 3)
			return UL_ERROR;
		m_uCalPhase = uPhase;
		return UL_NO_ERROR;
	}
	
	inline ULMINT GetCalPhase() 
	{ 
		return m_uCalPhase; 
	}

	ULMETHOD GetCalTime(CTime& tm);
	ULMDWD   GetCaledTime(UINT uPhase)
	{
		if (uPhase > 4)
			return 0;
		return m_tmCaled[uPhase].GetTime();
	}
	COLORREF GetCalTimeColor(CTime& tmCaled);
	inline COLORREF GetCalTimeColor(UINT uPhase)
	{
		if (uPhase > 4)
			return RGB(192, 192, 192);

		if (((m_nCalType[uPhase] & (UL_CAL_USER|UL_CAL_INDEPENDENT)) == 0) && (m_CalCurves[uPhase].GetSize() < 1))
			return RGB(192, 192, 192);

		return GetCalTimeColor(m_tmCaled[uPhase]);
	}

	BOOL ExceedsLimit(UINT nPhase);

	int	GetToolSNs(CStringList* pList);
	void InitCalCell(CCell2000* pCell, UINT uPhase, UINT nCalGroup = 0); // add by bao 2013/0603 刻度分组
	void EnableCalCell(CCell2000* pCell, UINT uPhase, long para, UINT nCalGroup = 0); // add by bao 2013/0603 刻度分组
	void FillCalResult(CCell2000* pCell, UINT uPhase, long col, long row, UINT nCalGroup = 0); // add by bao 2013/0603 刻度分组
	void SuckCalResult(CCell2000* pCell, UINT uPhase, long col, long row, UINT nCalGroup = 0); // add by bao 2013/0603 刻度分组
	void UpdateCalCell(CCell2000* pCell, UINT uPhase, BOOL bSaveAndValidate = TRUE, UINT nCalGroup = 0); // add by bao 2013/0603 刻度分组
	void InitCalMsg(CWnd * pCalWnd, UINT uCalMsgID);
	void AddCalChart(CCell2000* pCell, DWORD dwPhases = 15);
	void AddCalChart(CPtrList* pList, DWORD dwPhases = 15);
	int GetCharts(LPCTSTR pszText, CStringList* pList, CStringList* pT);
	BOOL GetChart(CString& strChart, LPCTSTR pszText, CStringList* pList, POSITION& pos);

	void FillChart(CCell2000* pCell, LPCTSTR pszText, CStringList* pMap, long& col, long& row,
		long colE, LPCTSTR pszFormat, int nIndex, long para, DWORD dwPhases, int nPhase);
	void FillChart(CCell2000* pCell, CStringList* pList, CStringList* pMap, long& col, long& row,
		long colE, LPCTSTR pszFormat, int nIndex, long para, DWORD dwPhases);
	
	//--------------------------
	// Calibrate called:
	//--------------------------

	ULMETHOD BeforeCalibrate(LPCTSTR lpszItem);	
	ULMETHOD CalibrateIO();
	ULMETHOD AfterCalibrate();
	ULMETHOD ComputeCalCoefficient();

	BOOL	OnCalNew();
	BOOL	OnCalLoad();
	
	//------------------------------------
	// System calibrate operations:
	//------------------------------------

	inline CULCalCurveSeg* GetCalCS(UINT uPhase, UINT iCur, UINT iSeg)
	{
		int nCurCount = m_CalCurves[uPhase].GetSize();
		if (iCur >= nCurCount) return NULL;
		CULCalCurve* pCur = (CULCalCurve*)m_CalCurves[uPhase].GetAt(iCur);
		ASSERT(pCur != NULL);
		int nSegCount = pCur->m_CalCSs.GetSize();
		if (iSeg >= nSegCount) return NULL;  
		return (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(iSeg);
	}

	inline CULCalDot* GetCalDot(UINT uPhase, UINT iCur, UINT iSeg, UINT iDot)
	{
		CULCalCurveSeg* pCS = GetCalCS(uPhase, iCur, iSeg);
		if (pCS != NULL)
		{
			int nDotCount = pCS->m_CalDotList.GetSize();
			if  (iDot >= nDotCount) return NULL;
			return (CULCalDot*)pCS->m_CalDotList.GetAt(iDot);
		}

		return NULL;
	}

	inline CULCalDot* GetCalDot(UINT uPhase, DWORD dwDot)
	{
		return GetCalDot(uPhase, CCIndex(dwDot), CSIndex(dwDot), CDIndex(dwDot));	
	}

	inline void ClearDotsMap(CMapStringToPtr* pMap)
	{
		for (POSITION pos = pMap->GetStartPosition(); pos != NULL; )
		{
			CString strDot;
			CPtrList* pDotList = NULL;
			pMap->GetNextAssoc(pos, strDot, (void*&)pDotList);
			if (pDotList)
				delete pDotList;
		}
		pMap->RemoveAll();
	}

	inline void CopyDotsMap(CMapStringToPtr* pMap)
	{
		ClearDotsMap(&m_mapDots);

		if (pMap != NULL)
		{
			POSITION pos = pMap->GetStartPosition();
			while ( pos != NULL)
			{
				CString strDot;
				CPtrList* pDotList = NULL;
				pMap->GetNextAssoc(pos, strDot, (void*&)pDotList);
				m_mapDots[strDot] = pDotList;
			}
		}
	}

	UINT GetCalDotsTime(LPCTSTR pszItem);

	inline ULMETHOD ClearCalDotsBuffer()
	{
		for (int i = 0; i < m_CalDotList.GetSize(); i++)
		{
			CULCalDot* pDot = (CULCalDot*)m_CalDotList.GetAt(i);
			pDot->m_Buffer.clear();
		}

		return UL_NO_ERROR;
	}

	inline ULMETHOD UpdateDot(CULCalDot* pDot)
	{
		int nData = pDot->m_Buffer.size();
		if (nData  < 1)
			return UL_ERROR;
			
		double fMin = pDot->m_Buffer.at(0);
		double fMax = pDot->m_Buffer.at(0);
		double fSum = fMin;

		for (int i = 1; i < nData; i++)
		{
			double fVal = pDot->m_Buffer.at(i);

			if (fVal < fMin)
				fMin = fVal;

			if (fMax < fVal)
				fMax = fVal;
			
			fSum += fVal;
		}
			
		pDot->m_fMaxMeas = fMax;
		pDot->m_fMinMeas = fMin;

		if (pDot->m_fUserMin == pDot->m_fUserMax)
			pDot->m_bExceed = FALSE;
		else
			pDot->m_bExceed = (fMax > pDot->m_fUserMax || fMin < pDot->m_fUserMin);
		
		switch (m_nCalVM)
		{
		case VM_MIN:
			pDot->m_fMeas = fMin;
			break;
		case VM_MAX:
			pDot->m_fMeas = fMax;
			break;
		case VM_AVE:
			pDot->m_fMeas = fSum/nData;
			break;
		case VM_END:
		default:
			pDot->m_fMeas = pDot->m_Buffer.back();
			break;
		}

		return UL_NO_ERROR;
	}

	ULMETHOD AddCalParam(void* pCurveData, void* pSegData, void* pDotData);
	ULMETHOD AddCalParam(UINT uPhase, void* pCurveData, void* pSegData, void* pDotData);
	
	// ------- 向当前刻度类型增加刻度曲线 -------

	void AddCalCurve(ULCalCurveData* pCurve, UINT nCalGroup = 0); // add by bao 2013/0603 刻度分组
	void AddCalCurve(CULCalCurve* pCalCurve);	
	void AddCalCurve(CString strName, CString strUnit, CString strTip, int nPrecis, UINT nCalGroup = 0); // add by bao 2013/0603 刻度分组
	
	// ------- 在线段链表上添加一条刻度线段 -------

	void AddCalCurveSeg(ULCalSegData* pSeg);
	void AddCalCurveSeg(CULCalCurve* pCurve, CULCalCurveSeg *pSeg);
	void AddCalCurveSeg(CString strCurveName, CULCalCurveSeg* pSeg);
	
	// ------- 向指定曲线增加刻度项目 -------

	void AddCalDot(ULCalDotData* pDot);
	void AddCalDot(CString strCurveName, CString strItemName, CString strAlias, 
					UINT uPri, UINT uDes, BOOL, CString szTip = _T("Tip"));
	ULMETHOD AddCalDotValue(DWORD nIndexHeadCalDot, double dCalDotValue);

	// ---------------------------------
	//	取得刻度曲线:
	// ---------------------------------
	
	inline void GetCalCurves(UINT uPhase, CPtrArray* pCalCurves)
	{
		ASSERT(uPhase < 3);
		pCalCurves->Copy(m_CalCurves[uPhase]);
	}

	inline CULCalCurve* GetCalCurve(UINT uPhase, LPCTSTR pszCur)
	{
		if (uPhase > 3)
			return NULL;

		for (int i = 0; i < m_CalCurves[uPhase].GetSize(); i++)
		{
			CULCalCurve* pCur = (CULCalCurve*)m_CalCurves[uPhase].GetAt(i);
			if (pCur->m_strName.CompareNoCase(pszCur) == 0)
				return pCur;
		}

		return NULL;
	}

	inline CULCalCurveSeg* GetCalCS(UINT uPhase, LPCTSTR pszCur, LPCTSTR pszCS)
	{
		CULCalCurve* pCur = GetCalCurve(uPhase, pszCur);
		if (pCur != NULL)
			return pCur->GetCurveSeg(pszCS);
		return NULL;
	}

	inline CULCalCurve* GetCalCurve(LPCTSTR pszCur)
	{
		return GetCalCurve(m_uCalPhase, pszCur);
	}

	inline CULCalCurve* GetCalCurve(int nCur)
	{
		ASSERT(nCur >= 0);
		ASSERT(nCur < m_CalCurves[m_uCalPhase].GetSize());

		return (CULCalCurve*)m_CalCurves[m_uCalPhase].GetAt(nCur);
	}
	
	// Get current calibrate curve count
	//
	inline ULMINT GetCalCurveNum()	
	{
		return m_CalCurves[m_uCalPhase].GetSize();
	}

	ULMINT GetCalSegNum(int nCurve);
	ULMINT GetCalDotNum(int nCurve, int nSeg);
	ULMINT GetCoefNum(CULCalCurveSeg *pSeg);

	ULMETHOD GetCalValue(int nCurve, int nSeg, int nDot, double& dMeasure, double& dEngineer);
	
	inline ULMINT GetCalDotNum()	{	return m_CalDotList.GetSize();	}
	inline ULMPTR GetCalDot(int nIndex)
	{	
		if (nIndex < 0)
			return NULL;

		if (nIndex < m_CalDotList.GetSize())
			return m_CalDotList.GetAt(nIndex);

		return NULL;
	}

	ULMDWD GetCalDotIndexHead(int nIndex)
	{
		CULCalDot* pDot = (CULCalDot*)GetCalDot(nIndex);
		if (pDot != NULL)
			return pDot->m_dwIndex;

		return (DWORD) -1;
	}

	void SetSysCalCurveState(BOOL bCaled = FALSE);
	void SetToolCalStatus();
	BOOL GetToolCalStatus();

	ULMETHOD SetCalDelayTime(int nDelay);
	ULMINT	GetCalDelayTime();
	ULMETHOD SetCaledTime(int nPhase, DWORD dwTime)
	{
		if (nPhase > 3)
			return UL_ERROR;
		m_tmCaled[nPhase] = dwTime;
		return UL_NO_ERROR;
	}
	//------------------------------------------------
	// System calibrate - Coefficient operations:
	//-------------------------------------------------

	ULMETHOD	AddCoef(int nCurve, int nSeg, CString strName, double fCoef);               
	ULMETHOD	AddDefaultCoef(int nPhase, int nCurve, int nSeg, LPCTSTR lpszName, double fCoef);
	ULMETHOD	AddDefCoef(UINT nPhase, UINT nCurve, UINT nSeg, LPCTSTR lpszName, double fCoef,
					double fRMin, double fRMax, double fRNor);
	ULMETHOD    AddDefCoef(UINT nPhase, UINT nCurve, UINT nSeg, LPCTSTR lpszName, LPCTSTR lpszAlias, 
					double fCoef, double fRMin, double fRMax, double fRNor);
	ULMETHOD	SetCoef(int iCur, int iSeg, int iCoef, LPCTSTR lpszName, double fCoef);
	ULMETHOD	SetCoef(int nPhase, int iCur, int iSeg, int iCoef, LPCTSTR lpszName, double fCoef);
	ULMDBL		GetCalCoef(int nCurveId, int nSegmentId, int nCoefId);			
	ULMDBL		GetCalCoef(int nCalType, int nCurveId, int nSegmentId, int nCoefId);
	ULMETHOD	RemoveCoef(int nCurve,int nSeg);
	ULMETHOD	SetCalDotEng(int nCurve, int nSeg, int nDot, double dEng);
	ULMETHOD	SetCalDotEng(int nPhase, int nCurve, int nSeg, int nDot, double dEng);
	ULMETHOD	SetCalDotMea(int nCurve, int nSeg, int nDot, double dMea);
	ULMETHOD	SetCalDotMea(int nPhase, int nCurve, int nSeg, int nDot, double dMea);
	ULMETHOD	SetCalDotInitEng(int nCurve, int nSeg, int nDot, double dEngineer);
	ULMETHOD	SetCalDotInitEng(int nCalType, int nCurve, int nSeg, int nDot, double dEngineer);
	ULMETHOD	SetCalDotStd(int nCurve, int nSeg, int nDot, double dStand);
	ULMETHOD	SetCalDotStd(int nCalType,int nCurve, int nSeg, int nDot, double dStand);
	ULMETHOD	SetCalDotInitStd(int nCurve, int nSeg, int nDot, double dStand);
	ULMETHOD	SetCalDotInitStd(int nCalType, int nCurve, int nSeg, int nDot, double dStand);

	ULMETHOD	GetCalDotMaxValue(int nCurve, int nSeg, int nDot, double& fMax);  // 得到最大刻度测量值			
	ULMETHOD	GetCalDotMinValue(int nCurve, int nSeg, int nDot, double& fMin);  // 得到最小刻度测量值		
	ULMETHOD	GetCalDotLastValue(int nCurve, int nSeg, int nDot, double& fLast);// 得到最后一个刻度测量值
	ULMETHOD	SetCalValues(int nValue /* = VM_AVE */);
	ULMDBL		GetCalDotValue(int nCur, int nSeg, int nDot, double dErr = DBL_MAX, int nValue = VM_DEF);
	ULMETHOD	SetCalDotCoefRange(int nPhase, int nCur, int nSeg, LPCTSTR lpszCoef, double fMinValue, double fMaxValue); // 设置刻度点系数范围
	ULMETHOD	ClearCalDotBuffer(int nCurve, int nSeg, int nDot);

	inline void ClearCalCurves(UINT uPhase)
	{
		for (int i = m_CalCurves[uPhase].GetSize() - 1; i > -1; i--)
        {
            CULCalCurve* pCur = (CULCalCurve*)m_CalCurves[uPhase].GetAt(i);
            delete pCur;
        }
        m_CalCurves[uPhase].RemoveAll();
	}

	inline void ClearCalCurves()
	{
		for (int i = 0; i < 4; i++)
			ClearCalCurves(i);
	}
	
	//---------------------------------------------
	// System calibrate - Save/Print operations:
	//---------------------------------------------	
	BOOL		WriteCalTextFile(CString strFile);	// Wirte calibrate text file

	ULMETHOD	ReadCalInfo(LPCTSTR pszFile, UINT uPhase = (UINT)-1);	// Read calibrate file
	ULMETHOD	WriteCalInfo(LPCTSTR pszFile, UINT uPhase = (UINT)-1);	// Write calibrate file
	ULMETHOD	ReadCalInfo(CFile* pFile, UINT uPhase);
	ULMETHOD    ReadCalInfoEx(CFile* pFile, UINT uPhase);
	ULMETHOD	SaveCalInfo(CFile* pFile, UINT uPhase);
	inline void	ReloadCalInfo()
	{
		ReadCalInfo(m_CalFilePathArray[m_uCalPhase]);
	}

	//---------------------------------------------
	// Calibrate reports:
	//---------------------------------------------	


	ULMETHOD SetCalDotVarMin(LPCTSTR lpszCurve, LPCTSTR lpszDot, float fMinValue)
	{
		if (m_dwLoad & DL_CALIBRATE)
			return UL_ERROR;

		return SetCalDotVarMin(lpszCurve, lpszDot, (double)fMinValue);
	}
	
	ULMETHOD SetCalDotVarMax(LPCTSTR lpszCurve, LPCTSTR lpszDot, float fMaxValue)
	{
		if (m_dwLoad & DL_CALIBRATE)
			return UL_ERROR;

		return SetCalDotVarMax(lpszCurve, lpszDot, (double)fMaxValue);
	}
	
	ULMETHOD SetCalDotVarNormal(LPCTSTR lpszCurve, LPCTSTR lpszDot, float fNormalValue)
	{
		if (m_dwLoad & DL_CALIBRATE)
			return UL_ERROR;
		return SetCalDotVarNormal(lpszCurve, lpszDot, (double)fNormalValue);
	}
    ULMETHOD SetCalDotTime(LPCTSTR lpszDotName, DWORD dwTime);
	ULMETHOD SetCalDotVarMin(LPCTSTR lpszCurve, LPCTSTR lpszDot, double fMinValue);
	ULMETHOD SetCalDotVarMax(LPCTSTR lpszCurve, LPCTSTR lpszDot, double fMaxValue);
	ULMETHOD SetCalDotVarNormal(LPCTSTR lpszCurve, LPCTSTR lpszDot, double fNormalValue);
	ULMETHOD SetCalDotVarRange(LPCTSTR lpszCurve, LPCTSTR lpszDot, double fMinVal, double fMaxVal, double fNorVal);
	ULMETHOD SetCalDotVarRange(UINT nPhase , LPCTSTR lpszCurve, LPCTSTR lpszDot, double fMinVal, double fMaxVal, double fNorVal);
	ULMETHOD SetCalDotStandardRange(LPCTSTR lpszCurve, LPCTSTR lpszDot, double fMinVal, double fMaxVal, double fNorVal);
	ULMETHOD SetCalDotStandardRange(UINT nPhase , LPCTSTR lpszCurve, LPCTSTR lpszDot, double fMinVal, double fMaxVal, double fNorVal);
	
	void SetUserDotVar();
	void CalMeasMaxMin();
	
	//---------------------------------------
	// User calibrate operations:
	//---------------------------------------

	ULMETHOD	CalUserCancel();			// 取消本次刻度
	short    	CalUserCalculate();			// 计算刻度曲线的刻度系数
	ULMETHOD	CalUserLoad(CFile* pFile, LPCTSTR strCalPhaseName);	// 载入刻度系数
    ULMETHOD	CalUserSave(CFile* pFile, LPCTSTR strCalPhaseName);	// 保存刻度系数
	short		CalUserPrint(CDC* pDC, CSize szPage, CString strCalPhase);
	short		CalSysPrint(CDC* pDC, CSize szPage, CString strCalPhase);
	ULMETHOD	CalUserLoad(CFile* pFile, UINT uCalPhase);	// 载入刻度系数
    ULMETHOD	CalUserSave(CFile* pFile, UINT uCalPhase);	// 保存刻度系数
	short		CalUserPrint(CDC* pDC, CSize szPage, UINT uCalPhase);	// 打印刻度系数数据 	
	short		CalSysPrint(CDC* pDC, CSize szPage, UINT uCalPhase);	// 系统默认打印方式

	//-------------------------------------------
	// User calibrate - Coefficient operations:
	//-------------------------------------------

	ULMETHOD	SetCalUserCurveNum(int nNumb); 
	ULMINT		GetCalUserCurvesNum()	{	return m_nUserCalCurves;	}

	ULMINT		AddCoefficient(double fCoef, int nCurve);
	ULMINT		AddUserDefualtCoef(double fCoef, int nCurve);
	ULMINT		AddUserDefualtCoef(double fCoef, int nNum, int nCurve);
	ULMETHOD	SetCoefficient(double fCoef, int nIndex, int nCurve);
	ULMDBL		GetCoefficient(int nIndex, int nCurve);
	ULMINT		GetCoefNum(int nCurve);
	ULMETHOD	EmptyCoefficient();
	ULMETHOD	EmptyCoefficient(int nCurve);
	void		ClearCoefficient();

	ULMVOIDIMP	SetUserCalTime(UINT nTime) { m_nCalUserTime = nTime; }
	ULMUNTIMP	GetUserCalTime() { return m_nCalUserTime; } 

	//-------------------------------------------
	// User calibrate - Save/Print operations:
	//-------------------------------------------
	
	ULMTSTR GetCalFilePath(UINT uPhase)
	{
		if (uPhase > 3)	return NULL;
		if (m_CalFilePathArray[uPhase].IsEmpty())
			return NULL;
		
		return (LPSTR)(LPCTSTR)m_CalFilePathArray[uPhase];
	}
	LPCTSTR GetCalFilePath()
	{
		return (LPCTSTR)m_CalFilePathArray[m_uCalPhase%4];
	}
	ULMETHOD SetCalFilePath(UINT uPhase, LPCTSTR lpszPath)
	{
		if (uPhase > 3)	return UL_ERROR;
		m_CalFilePathArray[uPhase] = lpszPath;
		return UL_NO_ERROR;
	}

	ULMETHOD	SaveCalFile(CFile* pFile);	// Save the calibrate data by user
	ULMETHOD	ReadCalFile(CFile* pFile);	// Read the calibrate data by user 
	void		WriteCalUserTextFile(CString);
	BOOL		ReadUserCalFile(CString strFileName, CString strCalType);
	ULMETHOD SetCoefUnit(LPCTSTR lpszCurve , LPCTSTR lpszName,	LPCTSTR lpszUnit);
	ULMETHOD SetDotUnit(LPCTSTR lpszCurve , LPCTSTR lpszName,	LPCTSTR lpszUnit);

	ULMETHOD	CalInvalidate(UINT nDays = 30)
	{
		m_nCalDays = nDays;
		return UL_NO_ERROR;
	}

	//------------------------
	// Verify operations:
	//------------------------

	ULMETHOD BeforeVerify();
	ULMETHOD VerifyIO();
	ULMETHOD AfterVerify();

	//------------------------
	// Attributes:
	//------------------------
	LPTSTR	SN()
	{
		return (LPTSTR)(LPCTSTR)strSN;
	}

	void SN(LPCTSTR pszSN)
	{
		strSN = pszSN;
	}

	void SNChanged(LPCTSTR pszSN, BOOL bReplay = FALSE, int iOption = 0, 
		BOOL* pCheck = NULL);

	CString GetCalDescFrom(LPCTSTR pszFile, int nCount = 256);
	CString GetCalTimeFrom(LPCTSTR pszFile, LPCTSTR pFormat = _T("%Y-%m-%d %H:%M"));
	
	void UpdateDlgData(BOOL bSaveAndValidate = TRUE);

	ULMETHOD    SetImagePath(LPCTSTR strPath);                     //设置仪器图片
	ULMETHOD	SetToolInfo(LPCTSTR strSN, LPCTSTR strAssetNo, double Diameter, double fWeight, double fMaxSpeed);
	ULMETHOD    SetToolInfoEx(LPCTSTR pszInfo, double* pValue);
	ULMETHOD    GetToolInfoEx(LPCTSTR pszInfo, double* pValue);
	ULMETHOD	GetToolInfo(LPTSTR strSN, LPTSTR strAssetNo, double& Diameter, double& fWeight, double& fMaxSpeed);
	//----------------------------------
	// Perforation and coring operations:
	//----------------------------------
	short SetCurCountDown(float fCurCountDown, BOOL bFlag = TRUE);

	ULMTSTR GetSavePath() { return (LPTSTR)(LPCTSTR)m_strSavePath; }
	ULMBOOL ScopeSetting(int nOperate, int nParam, LPVOID lpParam = NULL, int nParam2 = 0);
	ULMETHOD CurvesView(void* pStringArray);
	ULMETHOD CurvesOpen(void* pStringArray);  // 未打开的曲线不显示、不打印、不存盘
	ULMETHOD SetCurveBounds(int nCurve, double fLeftBound, double fRightBound);	
	ULMETHOD AddMark(int nCurve, LPCTSTR lpszName, long lDepth, DWORD dwColor, int nType);
	ULMETHOD SetCurveOutOfBoundNum(int nCurve, int nLeftNum, int nRightNum);
	void     InitBoundInfo();  // 将越界次数清零
	ULMINT   TestCurveBound(int nCurve);
	ULMETHOD SaveCalFile();//由仪器库调用，保存系统方式的刻度数据
	ULMETHOD ReadCalFile(CString strCalFileName);
	ULMETHOD ShowCalReport(CWnd *pParent);
	ULMETHOD ShowCalSummary(CWnd *pParent);
	ULMETHOD StartChannels();
	ULMETHOD UnlockChannel(int iIndex = -1/*ALL*/);

	//void UnlockChannel();
	void StartChannel();
	void StopChannel();

	short CalcAllMetaReport();
	short CalcMetaReport(UINT uCalPhase);
	long  GetMetaReportHeight();
	long  GetMetaReportHeight(UINT uCalPhase);

	void  ReadCalFileEx(CFile* pFile, UINT uPhase);	//读取数据文件中的自定义刻度
	void  SaveCalFileEx(CFile* pFile, UINT uPhase);	//保存数据文件中的自定义刻度
	void  ReloadCalFile();							//重新加载自定义刻度报告

	int	  GetMaxNoF(LPCTSTR pszFind, TCHAR szL = '-');

	// add by bao 12/12/13 仪器库能够记录时间
	ULMETHOD		AddEventItem(
		LPCTSTR	lpstrName,				//产生的监控事件名称
		LPCTSTR lpstrOrigination,		//事件来源	  
		BYTE byEventType,				//事件发生的状态：正确，警告或错误
		LPCTSTR lpstrEventContent,		//事件内容
		LPCTSTR lpstrEventResult,		//事件结果
		long lDepth = 0,				//监控事件产生的深度位置
		LPCTSTR lpToolName = NULL,		//	事件产生的仪器名
		LPCTSTR lpParaName = NULL,		//	参数名
		double fParaValue = 0,			//	参数值
		LPCTSTR lpstrMark = NULL);		//事件备注

	short CalUserSaveEX(CFile* pFile, UINT uPhase);
	short CalUserLoadEX(CFile* pFile, UINT uPhase);

	virtual	short _stdcall AddCalParamEx(void* pCurve, void* pSeg, void* pDot, LPCTSTR pszGroupName = _T(" "));
	virtual	short _stdcall AddCalParamEx(UINT uPhase, void* pCurveData, void* pSegData, void* pDotData, LPCTSTR pszGroupName = _T(" "));
	int GetCalGroupCount(UINT nPhase);

	// add by bao 2013-6-7
	virtual short _stdcall SaveCalToPrj(LPCTSTR lpszPath, int nPhase = 0); // 保存刻度文件到工程目录下
	// add by gj 2013-10-23
	virtual short _stdcall CopyCalToPrj(LPCTSTR lpszPath, int nPhase = 0); // 拷贝刻度文件到工程目录下
	virtual void _stdcall FileCopyTo(CString source, CString destination, CString searchStr, BOOL cover = TRUE);//拷贝文件
	virtual short _stdcall CopyCalFile(CFile* pFile, UINT uPhase);  //copy user Cal file  add by gj 20131025

	//xwh 2013 -7-29
	ULMETHOD GetCurCalGroupInt();
	virtual float _stdcall GetThreadHoldValue();
	virtual BOOL _stdcall SetThreadHoldValue(float fValue);
	virtual BOOL _stdcall SetCurveNoOffset(LPCTSTR lpszName, BOOL bCalcOffset); // 设置曲线是否进行深度偏移
	virtual BOOL _stdcall SetCurveNoOffset(int nCurve, BOOL bCalcOffset);
	virtual IDB* _stdcall GetIDB();
	virtual BOOL _stdcall LogFlash();
	virtual double _stdcall GetUCValue(CString pszName, double d , BOOL bSet);
	virtual CString _stdcall GetUCUnit(CString pszName , BOOL bRaw);
//	virtual BOOL _stdcall SetSaveRawData(BOOL bFlag = TRUE);
	//add by jasmine 2018-12-26
	ULMPTR GetCurveFromAllTools(LPCTSTR lpszName, LPCTSTR lpszIDName = NULL);

	ULMLNG GetTVDValue(long lDepth);
	ULMETHOD GetMudInfo(DWORD nMask , double &fValue);
	ULMLNG	 GetCurRun();
	ULMETHOD SetRunInfo(DWORD nMask , DWORD *pParam);
	ULMETHOD SaveRunInfo();
public:

	static BOOL IsSaveRawData()
	{ 
		return m_bSaveRawData; 
	}
	
	static void SetSavRawData(BOOL bSaveRawData)
	{ 
		m_bSaveRawData = bSaveRawData; 
	}

	static CString ConfigPath(TOOL* pTool)
	{
		int nLen = pTool->strToolDllDir.GetLength();
		if (nLen < 2)
			return _T("");

		CString strPath = pTool->strToolDllDir.Right(nLen - 1);
		int nFind = strPath.ReverseFind('\\');
		if (nFind <  0)
			return _T("");

		strPath = strPath.Left(nFind);
		return strPath;
	}
// Attributes
public:
	static DWORD	m_dwLoad;		// Load tools style
	static vec_ts	m_vecTools;		// All tools opened

	static BOOL		m_bSaveRawData;	// Save rawfile flag

	static long		m_lCurDepth;	// Current depth
	static long		m_lCurTime;		// Current time
	static SYSTEMTIME	m_sysCurTime;	//Current time of systemtime's type
	
	static double	m_fCurSpeed;	// Current speed
	static double	m_fCurVol;		// Current voltage
	static double	m_fCurTension;	// Current tension
	static double	m_fCurTemp;		// Current temperature
	static int		m_nFileID;
	
	static double	m_dCurCountDown;// For perforation

	static long		m_lCurDepthIndex; // Current depth index

public:
	DWORD				m_dwDisabled;	// Disabled
	CString				m_strToolDllName;	// Tool dll path name
	HMODULE				m_hTool;			// Tool dll module
	CToolInfoPack*		m_pToolInfoPack;

	int					m_nToolCount;		// Count of tool with same name
	int                 m_nSameToolIndex;   // the index of the same Tool 第一根相同的仪器为1， 第二根相同的仪器为2
	BOOL				m_bOpened;			// Is tool opened
	CEvent*				m_pEvent;			// Tool event
	CToolParams			m_Params;			// Tool parameters
	ULONG				m_nWorkMode;		// Work mode
	int					m_nToolState;		// Tool stat
	int					m_nCurDisplayMode;	// Disp stat
	
	long 				m_lOffset;			// Tool depth offset

	ULLogInfo*			m_pLogInfo;			// Log information

	//unsigned long		m_nStartmSCount;
	DWORD				m_dwStartTickCount;	// the tickcount(milliseconds) when the log begin
	//float				m_fTimeStartTime;
	
	CPtrArray			m_Scopes;			// 示波器
	
	// Curve Related Attribute
	CCurveArray			m_Curves;			// 本仪器所包含的曲线
	CCurveArray			m_StatusCurves;		// 状态曲线链表

	// Channel Related Attribute
	CPtrArray			m_Channels;			// 本仪器所引用的通道信息：所需的数据在此进行交换

	CCriticalSection	m_CS;				// 缓冲区临界区
	long				m_lRDSDepth;		// Used for the raw buffer one log io, current depth

	CIOBufferArray		m_BufReadArray;     // 读取用的缓冲区数组  //add by gj20130627
	CIOBufferArray		m_BufferArray;		// 计算用的缓冲区数组
	int					m_nUseCount;		// 缓冲区使用数量
	int                 m_nReadUseCount;    // 读取的缓冲区数量  //add by gj20130627

	CEvent*				m_pChannelEvent;	// 通道事件 
	CEvent*				m_pWaitEvent;		// 等待事件
	CEvent              m_DepthToolEvent;   
	HANDLE				m_hExchangeFile;	// 映射文件句柄	
	LPVOID				m_lpCodeBuffer;		// 映射文件地址
	long				m_bRunThread1;		// 线程标识

	// ---------------------------------
	//	Current calibrate infor:
	// ---------------------------------

	UINT			m_nCalDays;			// 有效期(初始化)
	UINT			m_uCalPhase;		// 当前刻度阶段
	DWORD			m_dwReport;			// 刻度报告参数
	DWORD			m_dwCalReports;		// 刻度报告打印参数
    DWORD           m_dwCalUpdate;      // 更新文件的刻度类型, 临时变量,不存储在文件。
	UINT			m_nCalDelayTime;	// 刻度延迟时间(ms)

	CString			m_strCSName;		// 当前刻度线段的标记
	CPtrArray		m_CalDotList;		// 当前进行刻度的刻度点链表

	CStringList		m_DotsList;			// 当前刻度点顺序
	CStringList     m_DotsAliasList;    // 刻度点别名
	CMapStringToPtr	m_mapDots;			// 当前刻度点集合
	
	CString			m_strCalFilePath;	// 当前刻度文件路径

	CWnd*			m_pCalWnd;			// 客户仪器刻度窗口
	UINT			m_nCalWndID;
	UINT            m_uCalMsgID;
	
	// ---------------------------------
	//	Calibrate informations:
	// ---------------------------------

	CPtrArray	m_CalCurves[4];
	BOOL		m_bCaled[4];
	CTime		m_tmCaled[4];
	int 		m_nCalType[4];
	BOOL        m_bCalDefined[4];
	BOOL        m_bCalUserPrint[4];
	CWnd*		m_CalWnds[4];
	UINT		m_CalWndsID[4];
	CCalDescs	m_CalDescs[4];

	CStringArray	m_CalFilePathArray;		// 刻度文件路径
	CStringArray	m_CalReportArray;		// 刻度报告打印表格
	
	int 		m_nUserCalCurves;
	CDblArray*	m_pArrCalCoefs;
	
	int			m_nCalVM;			// 取值(显示)模式	
	UINT		m_nCalUserTime;
	
	CMapDWORDToDbl		m_mapEngInit;
	CMapDWORDToDbl		m_mapStdInit;

	ITool*				m_pITool;			// 客户二次开发仪器指针
	IToolParam*			m_pIParam;			
	IToolBoard*			m_pIBoard;
	ITool2*				m_pITool2;
	CBInfoArray			m_arrWnd;			// 客户仪器控制窗口链表

	CStringArray		m_arCellName;		// Cell页链表
	CMapStringToPtr		m_mapCell;
	CMapStringToPtr     m_mapULParam;       // 用户设置共同变量
 
	CString				m_strGroupTo;		// 组合仪器名称
	CString             m_strImagePath;
	CString             m_strSavePath;      // 实时测井保存文件的路径
	CUIntArray          m_arAllocChannelSize;  // 申请的通道数

#ifdef _OLD_SCOPES
	ScopeSettings       m_ScopeSettings;    // 示波器的一些设置
	SCInfoArray         m_arrScopeCurves;   // 为示波器添加的曲线列表
	CPtrArray           m_arDepthScopeItems;// 深度示波器自定义的项目
	CScopeUserDlgArray  m_arScopeUserDlg;   // 用户为示波器添加的自定义对话框
#endif

	HMETAFILE			m_hMetaReport[4];	// 自定义刻度报告句柄
	short				m_szMetaReport[4];	// 自定义刻度报告高度

	DWORD				m_dwCalMetaReports;	// 自定义刻度报告输出控制
	static IProject*	m_pIProject;

public:
	CMapIntToString		m_mapCalGroup[4];	// 刻度分组名称对应
	int					m_nCurGroup;		// 当前分组
	int					m_nGroup;		// 当前刻度界面组号

	long				m_lNoDataCount; // 回放时没有数据的帧的个数，>10时认为仪器已坏
	float				m_fThreadHold;
	BOOL				m_bSaveToolRawData;//保存该仪器原始数据
	CString				m_strCalPhase[4];
};

#endif // !defined(AFX_ULTOOL_H__0C7F0018_6776_4C96_B474_E4D8569454DB__INCLUDED_)

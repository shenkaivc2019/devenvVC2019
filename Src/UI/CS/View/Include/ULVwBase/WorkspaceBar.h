﻿// WorkspaceBar.h: interface for the CWorkspaceBar class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WorkspaceBAR1_H__FFAF44CB_B481_4420_8FDF_AF44ABE73D6C__INCLUDED_)
#define AFX_WorkspaceBAR1_H__FFAF44CB_B481_4420_8FDF_AF44ABE73D6C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ULCOMMDEF.H"

#ifndef ULVWBASE_API
	#ifdef ULVWBASE‌_EXPORT
		#define ULVWBASE_API __declspec( dllexport )
	#else	
		#define ULVWBASE_API __declspec( dllimport )
	#endif
#endif

class CCurve;
class CReplaySheet;
class CGraphWnd;
class CWorkspaceTree;
class CProject;
class CServiceTableItem;
class CULBaseDoc;
class CULFileBaseDoc;
typedef CList<CGraphWnd*, CGraphWnd*> CGraphList;

class ULVWBASE_API CWorkspaceBar : public CDockablePane
{
	friend class CULVwBaseSFrameEx;

public:
	CWorkspaceBar(CWorkspaceTree* pWndTree = NULL);
	virtual ~CWorkspaceBar();

	void AdjustLayout();
	void OnChangeVisualStyle();

public:
	virtual void FillWorkspace();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWorkspaceBar)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

	// Generated message map functions
public:
	//{{AFX_MSG(CWorkspaceBar)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnPaint();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnUlpSave();
	afx_msg void OnSiAdd();
	afx_msg void OnSiActive();
	afx_msg void OnSiSave();
	afx_msg void OnSiModify();
	afx_msg void OnSiDelete();
	afx_msg void OnToolSNSet();
	afx_msg void OnPlotRefresh();
	afx_msg void OnPlotRemove();
	afx_msg void OnAdd();
	afx_msg void OnDelete();
	afx_msg void OnParamReset();
	afx_msg void OnMultiSendto();
	afx_msg void OnUpdateMultiSend(CCmdUI* pCmdUI);
	afx_msg void OnMultiDelete();
	afx_msg void OnCmdProjectClose();
	afx_msg void OnCurveOption(UINT id);
    afx_msg void OnUpdateCalibration(); 
	afx_msg void OnRename();
	afx_msg LRESULT OnThreadSegmentMsg(WPARAM wParam, LPARAM lParam);
	afx_msg void OnCopyWellInformation();
	afx_msg void OnPasteWellInformation();
	afx_msg void OnUpdatePasteWell(CCmdUI* pCmdUI);
	afx_msg void OnWellsiteAddWell();
	afx_msg void OnWellAddHole();
	afx_msg void OnWellDel();
	afx_msg void OnHoleAddRun();
	afx_msg void OnHoleDel();
	afx_msg void OnRunDel();
	afx_msg void OnWellsiteDel();
	afx_msg void OnProjectAddWellsite();
	afx_msg void OnRunActive();
	afx_msg void OnUpdateRunActive(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRunDel(CCmdUI* pCmdUI);
	//}}AFX_MSG
	afx_msg void OnUpdateCloseFile(CCmdUI* pCmdUI);
	afx_msg void OnSendToFile(UINT nID);
	afx_msg LRESULT OnChangeActiveTab(WPARAM, LPARAM);
	afx_msg void OnUpdateSort(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSiAMD(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSiDEL(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSiPR(CCmdUI* pCmdUI);
	DECLARE_MESSAGE_MAP()

public:

	// ---------------------------------
	//	Project operations:
	// ---------------------------------

	virtual BOOL AddProject(CProject* pProject);
	virtual BOOL AddNewService(CString strService, CProject* pProject = NULL);
	virtual BOOL AddService(CServiceTableItem* pService, CProject* pProject = NULL);
	virtual BOOL OnProjectClose();
	virtual BOOL OnCloseAllProject();
	void    ClearFile();
	void	ClearLeaf(CProject* pProject);

	BOOL	OpenFile(CString& strFile, BOOL bSA = FALSE);

	// ---------------------------------
	//	Service operations:
	// ---------------------------------
	virtual BOOL ApplyServiceItem(CServiceTableItem* pService);
	BOOL CloseActService();
	void ApplyComputeReplay(CProject* pProject, CReplaySheet* pSheetReplay);
	void SiActive(int nIndex);
	void SiModify(int nIndex);
	void SiDelete(int nIndex);

	// ---------------------------------
	//	Log operations:
	// ---------------------------------
	int GetAllGraphs();
	void LogBegin(int nDirection, CGraphWnd* pActive = NULL);
	void LogContinue();
	void LogStop();

	void SaveParamPosition();
	// ---------------------------------
	//	Replay operations:
	// ---------------------------------
	void ReplayBegin();
	UINT ReplayPause();
	void ReplayStart();
	void SetFrameRate(UINT uiFrameRate);
	void ReplayFinish();  //add by gj 20121226
	void OnReplayStart();

	// ---------------------------------
	//	Graph header operations:
	// ---------------------------------
	void AddDraw(CString strTemplateList , CULBaseDoc *pDoc);
	void AddHeader(CString strTemplateList , CULBaseDoc *pDoc);
	void AddPrints(CString strTemplateList , CULBaseDoc *pDoc);
	//add by gj20130627
	void CopyBufferArray();  //拷贝读取数组到计算数组
	void CloseAllEvent();	//关闭事件句柄

	BOOL ReplayToolOK(); // 根据序号判断是否进行回放开始
public:
	BOOL CloseFile(CULFileBaseDoc* pULFile, BOOL bNotRemove= FALSE);
	void OnClose();
	BOOL DeleteCurve(CCurve* pCurve, CULBaseDoc* pDoc, CULFileBaseDoc * pFile, BOOL bMessageBox= TRUE);
	CWorkspaceTree*	m_pWndTree;
	UINT			m_nCurrSort;

	//回放时的状态：
	//0：没有打开回放工程
	//1：打开回放工程没开始回放
	//2：打开回放工程正在进行回放
	//3：打开回放工程开始了回放过程，但中途暂时停止
	//4：完成了回放过程。
	UINT				m_iReplayStatus;	
	BOOL                m_bPrjMsgOut;				//关闭工程时弹出对话框的判断
	CStringArray        m_arrOpenedFileName;		//打开文件的名字列表
	CPtrArray           m_arrOpenedProjectFile;		//打开的工程内部文件
	CPtrArray           m_arrOpenedFile;			//所有打开的文件的指针

	int					m_nWorkMode;
	CServiceTableItem*	m_pActService;
	int					m_nService;
	UINT				m_uiFrameRate;
	ULSaveDataInfo		m_saveInfo;

	CProject*			m_pClickULP;	// 点击的工程指针
	CServiceTableItem*	m_pClickSvr;	// 点击的服务项目指针
	HTREEITEM			m_hItemClick;	// 点击的树叶

	CGraphList			m_Graphs;		// 测井窗口链表
	DWORD               m_dwReplaySpan;
	CWinThread*         m_pReplayReadFile;   //add by gj 20130604 回放读数据线程
	CWinThread*         m_pReplayCompute;   //add by gj 20130604  回放计算数据线程
	HANDLE				m_hReadEvent;  //读文件事件 add by gj
	HANDLE				m_hComputeEvent; //计算事件
	HANDLE				m_hPauseEvent;//暂停事件
	bool                m_bExitReplay; //停止线程
};

UINT  ReplayReadFileThread(LPVOID lpParam);  //modify by gj 20130604  读数据线程
UINT  ReplayComputeThread(LPVOID lpParam);  //modify by gj 20130604   计算数据线程
#endif // !defined(AFX_WorkspaceBAR1_H__FFAF44CB_B481_4420_8FDF_AF44ABE73D6C__INCLUDED_)

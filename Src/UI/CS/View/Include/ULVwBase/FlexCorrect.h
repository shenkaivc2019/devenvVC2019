// FlexCorrect.h: interface for the CFlexCorrect class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FLEXCORRECT_H__113F2D60_7AD4_48E4_A77C_113B19106526__INCLUDED_)
#define AFX_FLEXCORRECT_H__113F2D60_7AD4_48E4_A77C_113B19106526__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ULInterface.h"

#define PR_BASECURVE       0x0001
#define PR_CORRECTCURVE    0x0002
#define PR_ADDRANGE        0x0004

class CCurve;
class CULChildFrame;
class CGraphWnd;
class CMark;

class CFlexRange
{
public:
	long m_lBDepthTop;
	long m_lCDepthTop;
	long m_lBDepthBottom;
	long m_lCDepthBottom;
};

class CFlexCorrect  
{
public:
	CFlexCorrect();
	virtual ~CFlexCorrect();
public:
	void OnUpdateBaseCurve(CCmdUI* pCmdUI);
	void OnUpdateCorrectCurve(CCmdUI* pCmdUI);
	void OnUpdateAllCurves(CCmdUI* pCmdUI);
	void OnUpdateAddRange(CCmdUI* pCmdUI);
	void OnUpdateDeleteRange(CCmdUI* pCmdUI);
	void OnUpdateDo(CCmdUI* pCmdUI);

	void OnBaseCurve();
	void OnCorrectCurve();
	void OnAllCurves();
	void OnAddRange();
	void OnDeleteRange();
	void OnDo();
	void Teminater();

	void OnStartPos();
	void OnUpdateStartPos(CCmdUI* pCmdUI);
	void OnEndPos();
	void OnUpdateEndPos(CCmdUI* pCmdUI);
	
	void ClearRange();
	long* HitTest(CCurve* pCurve, long lDepth);
	void DrawRange(CDC* pDC, long lDepthTop, long lDepthBottom);
	BOOL TrackHandle(CGraphWnd* pWnd, CCurve* pCurve, long lDepth);
	void TrackPos(CGraphWnd* pWnd, CMark* pMark, CPoint pt);
protected:	
	CCurve* m_pBaseCurve;
	CCurve* m_pCorrectCurve;
	DWORD m_dwProgress;
	CPtrArray m_RangeArray;
	vec_ic m_CorrectingCurves;
	BOOL m_bSetStartPos;
	BOOL m_bSetEndPos;
	CMark* m_pStartMark;
	CMark* m_pEndMark;
public:
	CULChildFrame* m_pFrame;
	long   m_lDepthOld[4];
	int		m_nFirst;
};	

#endif // !defined(AFX_FLEXCORRECT_H__113F2D60_7AD4_48E4_A77C_113B19106526__INCLUDED_)

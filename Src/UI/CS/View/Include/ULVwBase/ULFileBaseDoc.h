﻿// ULFile.h: interface for the CULFileBaseDoc class.
//
//////////////////////////////////////////////////////////////////////
#pragma once

#include "IFile.h"
#include "IULFile.h"

#include "ULFileFormat.h"
#include "ULBaseDoc.h"
#include "ULBaseView.h"
#include "PrintOrderFile.h"
#include "Project.h"

//
//
//#define PI (double)3.14159265358979323846
//#define AngleToRadian(Angle) (float)(PI * Angle / 180.0f)
//#define cosa(Angle) cos(AngleToRadian(Angle))
//#define sina(Angle) sin(AngleToRadian(Angle))
//#define	ULRFT_PVT_CURVE_COUNT 64
//
//class CULPrintInfo;
//class CXMLSettings;
//class CServiceTableItem;
//class CULKernel;
//class CULTool;
//class CGroupView;
//class CCurve;
//class CEILog;
//
//typedef CList<CCell2000*, CCell2000*> CCellList;
//
//
//class CULFileBaseDoc;
//typedef CList<CULFileBaseDoc*, CULFileBaseDoc*> CULFileBaseDocList;
//
//#define SO_DEPT		0x0001
//#define SO_DIR		0x0002
//#define SO_METRIC	0x0100
//#define SO_STEP		0x0200
//#define SO_WRAP		0x0400
//#define SO_MIN		0x0800
//#define SO_INP		(SO_STEP|SO_MIN)
//#define SO_DEPTH_ONCE_PER_RECORD 0x1000
////
////typedef struct tagSAVEOPT
////{
////	DWORD dwOption;
////	long lMinDepth;		// 文件另存时控制存盘的深度范围
////	long lMaxDepth;		// 文件另存时控制存盘的深度范围
////	BOOL bRecordDown;	// 文件的方向
////	BOOL bMetric;		// 单位制
////	long nStep;         // 采样间距
////	float fVersion;		// LAS版本
////    BOOL bDepthOncePerRecord;  // 另存为 LIS 格式时, 
////                              // TRUE - 深度每记录保存一个; 
////                              // FALSE - 深度每帧保存一个。
////	BOOL bCurveRename;
////	DWORD dwNameSpaceIndex;
////}SAVEOPT;
//
//#ifndef _LOGIC
//
////typedef struct _tagRFT_PVT_MARK
////{
////	BOOL bShow;
////	int nTime;
////	double fValue;
////	int nType;
////	COLORREF cr;
////}RFT_PVT_MARK;
//
//#define RFT_TEST_TYPE_NORMAL	1
//#define RFT_TEST_TYPE_LIMITED	2
//#define RFT_TEST_TYPE_LOSTSEAL	3
//#define RFT_TEST_TYPE_DRY		4
//#define RFT_TEST_TYPE_UNRECOGNIZABLE 5
//
//#define RFT_PROFILE_TYPE_SUMMARY	0
//#define RFT_PROFILE_TYPE_MVD		1
//#define RFT_PROFILE_TYPE_FVD		2
//#define RFT_PROFILE_TYPE_BVD		3
//
//#define RFT_PROFILE_DEPTH			0
//#define RFT_PROFILE_TRUE			1
//
//#define RFT_MARK_TYPE_RECT			1
//#define RFT_MARK_TYPE_DTRI			2
//
//#define RFT_MUD_FLAG				0x00000001
//#define RFT_FLAG_DEFAULT			0
//#define RFT_FLAG_MUD_BEFORE			0
//#define RFT_FLAG_MUD_AFTER			1
//
//typedef struct _tagRFT_PVT_CURVE
//{
//	BYTE cName[32];
//	BOOL bShow;
//	double fMinValue;
//	double fMaxValue;
//	COLORREF crColor;
//}RFT_PVT_CURVE;
//
//typedef struct _tagRFTPROPERTY
//{
//	BOOL bValidate;
//	long lDepth;
//	long lTrueDepth;
//	UINT nTestType;
//
//	double fMudBefore;
//	double fMudAfter;
//	double fLastBuild;
//	double fFormation;
//	double fFirstTime;
//	double fSecondTime;
//	double fBuildUpTime;
//	double fMobility;
//	double fVolumes;
//	double fFactor;
//	int nProbeType;
//
//	int nSetStart;
//	int nSetEnd;
//	int nRetractStart;
//	int nRetractEnd;
//
//	double fHorzMin;
//	double fHorzMax;
//	double fGaugeMin;
//	double fGaugeMax;
//	UINT nHorzGrid;
//	UINT nVertGrid;
//	BYTE bHorzName[64];
//	BYTE bVertName[64];
//	BYTE bDepthUnit[16];
//	BYTE bPressureUnit[16];
//	BYTE bFileName[64];
//	BYTE bTime[64];
//
//	double fTimeStep;
//	BYTE bGauge[64];
//
//	RFT_PVT_CURVE Curve[ULRFT_PVT_CURVE_COUNT];
////	CArray<RFT_PVT_CURVE, RFT_PVT_CURVE> Curve;
//
//	RFT_PVT_MARK mrMudBefore;
//	RFT_PVT_MARK mrMudAfter;
//	RFT_PVT_MARK mrLastBuild;
//	RFT_PVT_MARK mrFirstBegin;
//	RFT_PVT_MARK mrSecondBegin;
//	RFT_PVT_MARK mrBeginBuild;
//}RFT_PROPERTY;
//
//class CRftProfileLine : public CObject
//{
//public:
//	CRftProfileLine();
//	~CRftProfileLine();
//
//	BOOL Compute();
//	BOOL Clear();
//
//public:
//	double k;
//	double b;
//	double fStartValue;
//	double fEndValue;
//	double fStartDepth;
//	double fEndDepth;
//	COLORREF crColor;
//	int nSize;
//	CArray<double , double> Depth;
//	CArray<double , double> Value;
//};
//
//class CRftProfileInfo : public CObject
//{
//public:
//
//	CRftProfileInfo();
//	~CRftProfileInfo();
//
//public:
//	double fStartDepth;
//	double fEndDepth;
//	int nProfileType;
//	int nMarkType;
//	COLORREF crMarkColor;
//
//	CWnd *pWnd;
//	CWnd *pParent;
//	UINT nHorzGrid;
//	UINT nVertGrid;
//	double fHorzMin;
//	double fHorzMax;
//	double fVertMin;
//	double fVertMax;
//	CString strHorzName;
//	CString strVertName;
//	CString strDepthUnit;
//	CString strPressureUnit;
//	CString strTitle;
//	UINT nFlag;
//	
//	BOOL bShowSelectLine;
//	double fSelectLineValue;
//	BOOL bSelectPos;
//	int nSelectIndex;
//	BOOL bOmit;
//	int nOmitIndex;
//
//	CArray<double , double> Depth;
//	CArray<double , double> Value;
//	CPtrArray ProfileLine;
//};
//
//#endif
//
//extern CString AfxGetComConfig()->m_strAppPath;
//
//class CCurveData;
//typedef struct tagSDATA
//{
//	CCurveData* pData;
//	size_t sz;
//	LPBYTE pValue;
//	long lDepth0;
//	long lDepthE;
//	int  nRSample;
//} SDATA;

#ifndef ULVWBASE_API
	#ifdef ULVWBASE‌_EXPORT
		#define ULVWBASE_API __declspec( dllexport )
	#else	
		#define ULVWBASE_API __declspec( dllimport )
	#endif
#endif

class ULVWBASE_API CULFileBaseDoc : public IULFile, public CULBaseDoc
{
	DECLARE_ULI(IID_IULFile, IULFile)
	DECLARE_DYNCREATE(CULFileBaseDoc)
	
public:
	CULFileBaseDoc();
	virtual ~CULFileBaseDoc();

// Interface implement
public:
	ULMTSTR GetPath() { return (LPTSTR)(LPCTSTR)AfxGetComConfig()->m_strAppPath; }
	ULMTSTR GetScout() { return (LPTSTR)(LPCTSTR)m_strScout; }
	ULMTSTR GetFilePath() { return (LPTSTR)(LPCTSTR)GetPathName(); }
	ULMETHOD SetScout(LPCTSTR lpszScout)
	{
		m_strScout = lpszScout;
		return UL_NO_ERROR;
	}

	ULMETHOD GetTools(ULTOOLS& vecTools);			// 

	ULMTSTR  GetParam() { return (LPTSTR)(LPCTSTR)m_strParam; }
	ULMTSTR  GetMarks() ;
	ULMTSTR  GetWellName();

	ULMETHOD SetParam(LPCTSTR lpszParam);
	ULMETHOD SetMarks(LPCTSTR lpszMarks);

	ULMETHOD CreateTools(ULTOOLS& vecTools, UINT nTools = 1);

	ULMINT   GetSheetCount();
	ULMETHOD GetSheets(CStringArray* pArrSheet);	// 获取绘图设置
	ULMETHOD SetSheets(CStringArray* pArrSheet);	// 设置绘图设置
	ULMETHOD SaveSheet(int i, LPCTSTR lpszSheet);	// 保存绘图设置
	ULMETHOD SetSheet(int i, LPCTSTR lpszSheet);	// 应用绘图设置

	ULMETHOD AutoMapSheet(BOOL bAutoMap = TRUE)

	{
		m_bAutoMap = bAutoMap;
		return UL_NO_ERROR;
	}

	inline ULMINT GetCellCount()
	{
		return m_celliList.GetCount();
	}

	ULMETHOD GetCells(CStringArray* pArrCell, CStringArray* pArrTempl);	// 获取图表文件
	ULMETHOD SetCells(CStringArray* pArrCell, CStringArray* pArrTempl);	// 设置图表文件
	ULMETHOD SaveCell(int i, LPCTSTR lpszCell);	// 保存图表文件
	ULMETHOD SetCell(int i, LPCTSTR lpszCell) {	return UL_NO_ERROR;	}	// 应用图表文件

	ULMETHOD GetPrints(CStringArray* pArrPrint, CStringArray* pArrTempl);
	ULMETHOD SetPrints(CStringArray* pArrPrint, CStringArray* pArrTempl);
	
	ULMETHOD AddCurveData(int nCurve, long lDepth, void* pValue, long lTime = 0);
	ULMETHOD AddCurveDatas(int nCurve, long lDepth, long lNextDepth, void* pValue, long lTime = 0);

	ULMPTR   GetProjectInfo();
	ULMETHOD SetProjectInfo(void* pPi) { return UL_NO_ERROR; }

	ULMINT GetCurves(CURVES** ppCurves)
	{
		*ppCurves = &m_vecCurve;
		return m_vecCurve.size();
	}
	
	ULMINT GetCurveCount()
	{
		if (m_pActPrj != NULL)
		{
			return m_arrToolCurves.GetSize();
		}
		else
		{
			return m_vecCurve.size();
		}
	}
	
	ULMPTR GetCurve(int nIndex)
	{
		if (nIndex < 0)
			return NULL;
		if (nIndex < m_vecCurve.size())
			return m_vecCurve.at(nIndex);
		return NULL;
	}
	
	ULMPTR GetCurve(LPCTSTR pszName);
	ULMPTR GetCurveNoCase(LPCTSTR pszName);

	ULMETHOD Close()
	{
		if (m_bOpened)
		{
			if (m_pIFile)
				m_pIFile->Close();
			m_bOpened = FALSE;
		}
		
		return UL_NO_ERROR;
	}

	ULMETHOD	SetStatusPrompt(LPCTSTR pszText);
	ULMETHOD    SetStatusProcess(int nPrecent);
	ULMETHOD    GetInterface(const IID &ID , void **p);
	ULMLNG		GetTime(DWORD dwTime = FT_CREATE);
	ULMETHOD    SetTime(time_t time, DWORD dwTime = FT_CREATE);
	ULMPTR		CreateCurve(LPCTSTR pszName, VARTYPE vt = VT_R4);
	ULMPTR		GetDEPT() { return m_pDCurve; }

	ULMLNG	GetUniqueNameSuffix(CStringArray* pArrNames);
	ULMPTR	CreateCurve(LPCTSTR pszName, VARTYPE vt = VT_R4 , int nDimension = 1);
	ULMPTR	GetDataBase(DWORD dwParam);
public:
	void SetTools();	// 设置仪器串
	void AssignSameTools();
	// ---------------------------------
	//	加载/卸载 文件格式:
	// ---------------------------------

	BOOL    LoadFileFormat(CString strFileFormat);
	long	GetDepthInterval();
	void	GetAllCurveProps(CURVEPROPS& vecCurve, BOOL bDelAdd = FALSE);
	BOOL	DeleteCurve(CCurve* pCurve);
	void	AddCurve(CCurve* pCurve);
	void	AdjustDirection();

	//------------------------------
	// Save operations:
	//------------------------------
	BOOL DoFileSave();
	BOOL Save(BOOL bAsny = TRUE); // Save
	BOOL SaveAs(CString strFileFormat, CString strFileName, BOOL bAsny = TRUE); // Save as
	BOOL SaveProject(CString strFileFormat, CString strFileName);	// Save project as
	BOOL SaveAs();	// Save thread called procedure
	
	BOOL Create(LPCTSTR pszFileFormat, CString& strFileName);		// Create file
	void SaveFileHead(CProject* pProject, CServiceTableItem* pSi);
	ULMETHOD SaveDataBegin(CULKernel* pKernel, BOOL bSaveAs = FALSE, long lStartDepth = 0, long lEndDepth = 0);				
	ULMETHOD PreSaveData(long lStartDepth, long lEndDepth);
	void SaveDataFrame();
	void SaveDataFrame(int nFrame, int nOff, long lDLevel);
	void SaveDataEnd(LPCTSTR pszMarks = NULL);
	
	//------------------------------
	// Load operations:
	//------------------------------

	BOOL  Read(LPCTSTR lpszFile, BOOL bShowMsg = FALSE);
	BOOL  ReadAllCurves(LPCTSTR pszFile);
	BOOL  Open(CString strFile);
	short ReadFileHead();
	//void  ReadCurSegOption();
	short ReadDataBegin();
	short ReadDataFrame(int nPos);
	void  ReadDataEnd();

	// ---------------------------------
	//	Childframe operations:
	// ---------------------------------
	virtual void OpenFrames(DWORD dwTypes);
	virtual void ShowView(DWORD dwType);
	virtual void CloseFrames();
	BOOL         CanCloseFrame(CLOSE_ALL_FILE *pInfo = NULL);
	virtual void GetRecord(CString& strDoc, CString& strTime, CString& strRecord, CString& strDepthST, CString& strDepthED, CSheet* pSheet);
	virtual void GetRecord2(CString& strComp, CString& strTeam, CString& strWell, CString& strField);
	void         GetValueInfo(UINT nIndex, UINT nTargetIndex, ValueInfo& vi);
	void		 ApplyCurve(CCurve* pCurve);
	ULMETHOD     Compute(double fDegree);
	ULMETHOD     RecalcTVDData(double fDegree, double fInterval, double fStart, double fEnd);
	ULMBOOL      IsProjectFile() { return m_bProjectFile; }   // 是否是工程内部文件
	ULMETHOD     GetTools(CPtrArray* arrTools);
	ULMETHOD     ApplyParam(CString strTools);
	ULMETHOD     AutoMapSheet(CString strTools);     //根据仪器串映射绘图模板
	ULMPTR	 GetTVDData() { return &m_arData; }
	ULMPTR	 GetTARData() { return &m_vecTarget; }
	void		UpdateInfors();
	void		RemapSheets();

	ULMETHOD	SaveFileter(int i, LPCTSTR lpszFilter);
	ULMETHOD	ReadFileter(int i, LPCTSTR lpszFilter);

	void SaveDataFrameTime(); // add by bao 2013 01 08 
	BOOL SaveAsALD();
	BOOL SaveAsByTime();	// add by bao 2013 7 30 按照时间保存
	void SaveDataFrameByTime(int nFrame, int nOff, long lDLevel);	// add by bao 2013 7 30 按照时间保存

	ULMPTR	GetCurve(LPCTSTR lpszName, LPCTSTR lpszSrc);// 根据曲线名称以及曲线所属仪器库获取文件中曲线指针
	ULMTSTR GetAXPVersion(); //获取AXP软件的版本号 add by gj 130910

	BOOL m_bSaveFlag;	//保存标志位,当处于监视测井时,把数据更新到m_vecRTCurve中
#ifdef _LWD
	BOOL	m_bReSave;	//重新保存标志，当实时测井时修改曲线数据后刷新当前已保存的文件
	vec_ic	m_vecRTCurve;//实时测井时保存用于数据修改的曲线，如果不选择保存功能则曲线不存在
	void InitRTCurves(CURVEPROPS *vecCurve);//初始化用于数据处理的曲线
#endif

public:
	HMODULE		m_hFFmt;		// File format driver
	IFile*		m_pIFile;		// File interface

	BOOL		m_bOpened;		// Is open
	BOOL		m_bReadCurves;	// Read curve only
	BOOL		m_bAutoDelete;	// When end saving
	BOOL		m_bAutoMap;		// Auto search presentation
	BOOL		m_bCurveFlag;

	long		m_lFrameCount;	// Read frame count
	short		m_nDriveMode;	// Driver mode
	short		m_nDirection;	// Depth curve direction
	SAVEOPT*	m_pSaveOP;		// Save operation

	CTime		m_tmCreate;		// Created time
	CTime		m_tmModify;		// Modifyed time

	CString		m_strExtent;	// File extent name
	CString		m_strExtOpened;
	CString		m_strScout;		// Scout file path
	CString		m_strParam;		// Param file path
	CString	    m_strMarks;		// Marks file path

	CGraphWnd*  m_pGraph;		// First view
	CPtrArray   m_arrTools;		// Tool list

	BOOL		m_bSTarget;
	BOOL        m_bProjectFile;  //是否是工程打开的文件

	CCurve*     m_pCAL;
	CCurve*     m_pDEV;
	
	CPtrList	m_lstSave;

	CPtrArray   m_arrToolCurves; // 用于保存滤波指针
	float       m_fVersion;//用于保存LAS版本号 add by gj130903
	CString       m_strVersion;//AXP版本号

    // m_pEILog is used by FFLDF.dll to read the file format "*.ldf", 
    // logic need to support this file format too.
	CEILog*    m_pEILog;
#ifndef _LOGIC
	RFT_PROPERTY m_RftProperty;
#endif

	CProject* m_pActPrj;
};

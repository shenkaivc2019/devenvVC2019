// MemPerformFile.h: interface for the CMemPerformFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MEMPERFORMFILE_H__C1F34C97_EC60_46AA_ADA4_23CFA7B980A9__INCLUDED_)
#define AFX_MEMPERFORMFILE_H__C1F34C97_EC60_46AA_ADA4_23CFA7B980A9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <vector>

class CCurveData;
class CTrack;
class CGraphWnd;
class CULKernel;

// 曲线信息结构
typedef struct _tagFILEHEADCURVEINFO
{
	char CurveName[10];			// curve Name 
	BYTE CurveID[4];			// curve data ID
	char CurveDataSource[10];	// curve data source
	WORD CurvePagePoint;		// a number of page of data 
	WORD CuveDataByteNum;		// byte num
}CURVEINFO;

// 深度信息
typedef struct _tagDepthInfo
{
	long	lDepth;
	DWORD   dwPosition;
}DEPTHINFO;

// 数据帧信息
typedef struct _tagDataFrameInfo
{
	long	lDepth;				// 当前帧深度(long)
	BYTE	btCurveNO;			// 曲线数据标识(BYTE)标识曲线索引
	BYTE	btCount;			// 数据点个数(BYTE)标识该深度上曲线点的个数
}DATAFRAMEINFO;

typedef std::vector<CCurveData*>	vec_data;
typedef std::vector<CGraphWnd*>		vec_graph;
typedef std::vector<CURVEINFO>		vec_info;
typedef std::vector<DEPTHINFO>		vec_depth;

// 临时文件处理，用来优化内存
class CMemPerformFile : public CObject  
{
public:
	CMemPerformFile();
	virtual ~CMemPerformFile();

public:
	BOOL	PrePareAllWork(CULKernel* pULKernel); // pre work
	BOOL	AfterAllWork();  
	
	BOOL	GetCurveInfo(CString strCurveName, CURVEINFO&	CurveInfo) ; // get curve info by curvename 
	ULONG	SaveCurPage();  // save a page data to file and return file position
	ULONG	SaveEndPage();
	
	long	GetCurPrintDepth(); 
	long	SetCurDispDepth();
	long	SetCurPrintDepth(long lDepth);
	
	long	GetArrayDepth(int nIndex);
	long	GetArrayDetphCount();
	
	int		GetCurveInfo(vec_info& vecCrvInfo); // load curve info from temp file and return curve count
	int	    SetCurDirection(int nDirection);	// set current log direction
	int	    SetCurDriveMode(int nDriveMode);	// set current log direction

	int		DataPerformAll();
	// when screen scroll

	int		PrepareCurPrintPage(long lStartDepth, long lEndDepth); //when playback print a page , prepare all data of this page 
	void	RePrepareTempFile();	// 用于监视转向时，将临时文件状态转化为初始状态
	short   GetCurSaveDepth();		// 获得当前存盘深度
	BOOL    CreateTempFile(int nCreate = 3);		// 创建临时文件
	CFile*	GetParamFile();


protected:
	void	Init();
	void    FromCStringToChar(CString strSource, char* ObjChar);
	void	FillCurveInfoArray();	// 填充曲线数据信息 
	
	int		SaveFileHead();			// save temp file head info 
	int     GetDataPos(long lDepth, int& nFindedMode);
	int	    FindIndex(CCurveData* pCurve, long lDepth);
	
	short   GetCurSaveTime();		// 获取当前存盘时间
	

public:
	vec_graph	m_vecGraph;			// 窗口链表
	vec_data	m_vecCurve;			// 曲线数据向量表
	vec_info	m_vecCurveInfo;		// 曲线数据信息
	vec_depth	m_vecDepth;			// 临时文件中深度索引信息

	long		m_lSaveEndDepth;    // 临时文件的结束深度或者时间
	BOOL		m_bSave;			// 是否存盘
	BOOL		m_bPrint;			// 是否打印
	int 		m_nTempFileOpen;	// 打开标识

protected:										  
	CString		m_strFileName;		// 文件名称 
	CString     m_strFilePath;		// 文件路径
	CFile		m_File;				// 临时文件
	CFile		m_PF;				// 仪器参数文件
	

	ULONG	    m_nPageSize;		// 一帧数据大小

	ULONG		m_nCurPagePos;		// 当前帧在链表中的位置
	ULONG		m_nDataStartPos;	// 临时文件中数据开始点

	long		m_lCurPrintDepth;	// 当前打印深度
	long		m_lCurDispDepth;	// 当前显示深度

	int			m_nDriveMode;		// drive mode 
	int			m_nDirection;		// direction 
		
	CCurveData* m_pDepthCurve;
	CULKernel*	m_pULKernel;

	long		m_lLostDepth;		// 数据保存(读取*10)缓冲深度
	int			m_nLostPoint;		// 缓冲点数
};

#endif // !defined(AFX_MEMPERFORMFILE_H__C1F34C97_EC60_46AA_ADA4_23CFA7B980A9__INCLUDED_)

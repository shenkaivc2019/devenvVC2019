﻿#ifndef __PROJECT_H
#define __PROJECT_H

#include "RawFile.h"
#include "Kernel.h"
#include "ToolParam.h"
#include "XMLSettings.h"
#include "IProject.h"
//#include "PerforationDef.h"
#include "WPropertyPage.h"
//#include "Perforation.h"
//#include "SaveFileName.h"
//#include "PropertyPath.h"
// 2020.1.19 Ver1.6.0 TASK【002】 Start
//#include "ProjectInfo.h"
//#include "ProjectWellsiteInfo.h"
//#include "ProjectWellInfo.h"
//#include "ProjectHoleInfo.h"
//#include "ProjectRunInfo.h"
// 2020.1.19 Ver1.6.0 TASK【002】 End	
//////////////////////////////////////////////////////////////////////
// Declaration
#ifndef ULVWBASE_API
	#ifdef ULVWBASE‌_EXPORT
		#define ULVWBASE_API __declspec( dllexport )
	#else	
		#define ULVWBASE_API __declspec( dllimport )
	#endif
#endif
#ifdef _DEBUG
#  define _CRTDBG_MAP_ALLOC
#  define _CRTDBG_MAP_ALLOC_NEW
#  include <crtdbg.h>
#  include <assert.h>
#endif

class CCell2000;
class CTrack;
class CSheet;
class CServiceTableItem;
class CProjectInfo;
class CProjectRunInfo;
class CProjectBHAInfo;

typedef std::vector<CServiceTableItem*> vector_service;

#define	csc	6

//////////////////////////////////////////////////////////////////////
// 工程

class ULVWBASE_API CProject : public IProject
{
	DECLARE_ULI(IID_IPROJECT, IProject)

public:
	CProject(BOOL bReplay = FALSE, BOOL bNew = FALSE);
	~CProject();
	
	ULMCTSTR GetName() { return m_strPName; }
	ULMDWD WriteToFile(CFile* pFile, DWORD dwType = 1);
	ULMDWD ReadFromFile(CFile* pFile, DWORD dwType = 1);
	ULMBOOL GetInformation(LPCTSTR pszName, CString* pValue, DWORD dwIndex = -1);
	ULMBOOL SetInformation(LPCTSTR pszName, LPCTSTR pszValue, DWORD dwIndex = 0);
	ULMBOOL SetInforBuffer(LPCTSTR pszName, LPBYTE pByte, DWORD dwSize, DWORD dwIndex = 1);
	ULMDWD GetInforBuffer(LPCTSTR pszName, LPBYTE pByte, DWORD dwSize, DWORD dwIndex = 1);
	ULMINT	GetOGResults(CArray<long, long>* pDepths, CStringArray* pResults, int* pSelect = NULL);
	ULMINT	SetOGResults(CArray<long, long>* pDepths, CStringArray* pResults);
	ULMINT	GetCasingPro(double* pCasingSize, double* pCasingDepth , double* pCasingThickness);
	ULMINT	GetBitPro(double* pBitSize, double* pBitDepth);
	ULMINT	SetCasingPro(double* pCasingSize, double* pCasingDepth,double*pCasingThickness , int nProCount);
	ULMDBL	GetCasingSizeByDepth(double fCasingDepth);

	virtual void Serialize(CXMLSettings& xml);
	virtual void SerializeWK(CXMLSettings& xml);
	virtual void SerializeRaw(CArchive& ar);

public:
	//static CStringList m_strInfos;
	CStringList m_strInfos;
	//static BOOL InitInformation(CXMLSettings* pXML, BOOL bNew = FALSE);
	BOOL InitInformation(CXMLSettings* pXML, BOOL bNew = FALSE);
	CString		Save();						// 保存工程文件
	BOOL		SaveAs(LPCTSTR pszFile);
	BOOL		Load(CString strFileName, BOOL bInfor = FALSE);	// 加载工程文件
	BOOL		Load2(CString strFileName);
	
	// 服务项目
	BOOL		RenameService(CString& strService, CString& strServiceNew);
	
	CServiceTableItem* GetService(CString strItem);
	CServiceTableItem* GetService(UINT nItem);
	void		CloseServices();
	void		ClearService(CServiceTableItem* pService, BOOL bDelete = TRUE);
	void		ClearServices();

	int		SetActive(CServiceTableItem* pService);
	void	ReleaseActive(CServiceTableItem* pService = NULL);

	// 仪器加载
	void		LoadAllTool();      // 加载所有仪器
	void		LoadAllTool(CServiceTableItem * pServiceTableItem);
	void		UnloadAllTool();    // 卸载所有仪器

	/* ------------------------- *
	 *  RAW DATA FILE OPERATION
	 * ------------------------- */
    void		SetRawFileToolInfoList();
	void		GetToolInfo();		// 获得仪器信息
	CString		GetScout();
	void LoadToolsParam();
	void LoadToolCalInfo();

	BOOL ImportCalFile(LPCTSTR pszFile);
	void NewService();
	CString GetSavePath();
	int	GetMaxNoF(LPCTSTR pszFind, TCHAR szL = '-');
	void InvalidateCell(CCell2000* pCell, long col, long row, long sheet);
	BOOL SaveInformation(CString note, CString string2);
	CXMLNode* FindSymbol(CString strName, DWORD dwIndex = 1);
	BOOL SetSymbolValue(CXMLNode* pNode, CString strValue);
	void InitProjectInfo(CPropertySheet* pPS);
	BOOL SaveProjectInfo(CPropertySheet* pPS);
	void InitProjectInfo(CWPropertyPage* pPage);
	BOOL SaveProjectInfo(CWPropertyPage* pPage);

	void GetInformations(WELLPROJECTINFO* pWPI);
	void UpdateTool(void* pTool);
	CULTool* CheckParam(CString strop, CStringArray& strarr);

	//BOOL GetPerforationInfo(TAILOR_INFO& ti);
	BOOL SetPerforationPName(CString strName);
	CString GetPerforationPName();
	//BOOL InitPerforationInfo(TAILOR_INFO& ti);

	// Results
	int InsertRange(long lDepth);
	int SelectRange(long lDepth);
	void SplitRange(int iRange);
	void DeleteRange(int iRange);
	void GetDepths(int iRange, long& lDepth0, long& lDepth1, long& lDepthA0, long& lDepthA1);
	void SetDepths(int iRange, long lDepth0, long lDepth1);
	void ReadConfigFromINI();

	void NotifyWndRcvData(WPARAM wParam, LPARAM lParam);
	void AddRcvDataWnd(HWND hWnd);
	BOOL RemoveRcvDataWnd(HWND hWnd);
public:
	CArray<HWND, HWND>	m_awndRcvData;

// 2020.1.19 Ver1.6.0 TASK【002】 Start
	CProjectInfo*	m_pProjectInfo;
// 2020.1.19 Ver1.6.0 TASK【002】 End

	CString			m_strPName;		// 工程名称
	CString			m_strPPath;		// 工程路径
	CString			m_strPDiagram;	// 井缩略图

	POSITION		m_posDoc;
	BOOL			m_bLoad;		// 是否装载
	DWORD			m_bComputReplay;// 回放标识
	int				m_nActive;		// 激活的服务项目
	int				m_nWellType;
	int				m_nBitPrc;
	int				m_nCasingPrc;
	double			m_fBitDepth[8];
	double			m_fBitSize[8];
	double          m_fCasingStartDepth[8];
	double			m_fCasingDepth[8];
	double			m_fCasingSize[8];
	double			m_fCasingThickness[8];
	float			m_fRadio;
	
	CArray<long, long>	m_ogDepths;
	CStringArray	m_ogResults;
	int				m_iSelResult;

	CStringArray		m_strServices;  // 服务项目名称
	vector_service		m_Services;		// 运行时服务项目链表
	CServiceTableItem*	m_pService;		// 当前激活的服务项目

	//CULKernel		m_ULKernel;			// 仪器组合链表：工程当前包含的一系列仪器信息
	CULKernel*		m_pULKernel;			// 仪器组合链表：工程当前包含的一系列仪器信息
	CRawFile*		m_pRawFile;			// 原始文件
	CXMLSettings	m_xmlInfo;

	CString			m_strComboName;
	int				m_nDriveMode;
	//WELLPROJECTINFO	m_NewProjectInfo;	// 全部工程信息，1.60版开始使用
	BOOL            m_bRealTime;		// 实时作业工程
	
	//CPropertyPath m_dlgPath;

	//static CList<CProject*, CProject*> m_clipProject; // 用于复制工程信息
	CList<CProject*, CProject*> m_clipProject; // 用于复制工程信息

	int m_nRed;
	int m_nYel;

public:
	//static SYSTEMTIME m_sysCurTime;	//当前测井时间
	SYSTEMTIME m_sysCurTime;	//当前测井时间

	inline ULMETHOD	SetCurTime(SYSTEMTIME sysTime)
	{
		//CProject::m_sysCurTime = sysTime;
		m_sysCurTime = sysTime;
		return UL_NO_ERROR;
	}
	
	inline ULMETHOD GetCurTime(SYSTEMTIME& sysTime)
	{
		//sysTime = CProject::m_sysCurTime;
		sysTime = m_sysCurTime;
		return UL_NO_ERROR;
	}

	//序列配置文件相关
public:
	CStringArray m_arrSerialConfigSrcPath;		//序列配置文件源路径集合
	CStringArray m_arrSerialConfigDesPath;		//序列配置文件目标路径集合
	CStringArray m_arrRawSerialPath;			//Raw文件中读取到的配置路径集合
	
	void ClearSerialConfig();
	ULMETHOD SetSerialConfig(CStringArray* pArrSerialPath);
	ULMETHOD GetSerialPath(LPCTSTR pszSrcPath, CString* pDesPath);
};

//extern CXMLSettings	g_xmlInfo;

_inline CServiceTableItem* CProject::GetService(UINT nItem)
{	
	if (nItem < m_Services.size())
		return m_Services[nItem];
	
	return NULL;
}

class ULVWBASE_API CProjectManager
{
public:
	CProjectManager();
	virtual ~CProjectManager();
	IDB* m_pIDB;
protected:
	CPtrArray m_arProjects;
	long m_lCurrProjectIndex;
	CProjectRunInfo* m_ActiveProjectRunInfo;
	HTREEITEM m_hActiveProjectRunInfo;
public:
	BOOL LoadFromDB();
	int AddProject(CProject* pProject);
	CProject* GetProject(int nIndex);
	CProject* GetProject(LPCTSTR lpszFilterType);
	void CloseProject(CProject* pProject);
	void ClearProjects();
	inline int GetCount() { return m_arProjects.GetSize(); }
	CProject* GetActiveProject();
	CProjectRunInfo* GetActiveRun();
	void SetActiveRun(CProjectRunInfo* hActiveProjectRunInfo);
	HTREEITEM GetActiveRunTreeItem();
	void SetActiveRunTreeItem(HTREEITEM m_hActiveProjectRunInfo);
	inline void SetActiveProject(long lIndex);
	void SetActiveProject(CProject* pProject);
};

ULVWBASE_API CProjectManager* AfxGetProjectManager();

//ULVWBASE_API CProject* AfxGetProject();
//ULVWBASE_API void AfxSetProject(CProject* pProject);

#define SYM_FILE(strTempl) (AfxGetComConfig()->m_strAppPath + "\\Template\\Symbols\\" + (strTempl) + ".xls")
#define DGR_FILE(strTempl) (AfxGetComConfig()->m_strAppPath + "\\Template\\Diagrams\\" + (strTempl))
#define SI_SYSTEM	0x00000001
#define SI_USER		0xFFFFFFFE

#endif

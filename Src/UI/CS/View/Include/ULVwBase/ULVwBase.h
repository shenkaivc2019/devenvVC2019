#pragma once
#ifndef _UL_VW_BASE_H
#define	_UL_VW_BASE_H

#pragma pack(push,1)
#include <ULVwSApp.h>
#include <ULVwSFrm.h>
#include <ULVwSAppEx.h>
#include <ULVwSFrmEx.h>
#include <ULVwMAppEx.h>
#include <ULVwMFrmEx.h>
#include <ServiceTableItem.h>
#include <ConnectDataBase.h>
#include <ULVwBase.h>
#include <ULTableInfoView.h>
#include <ULChildFrm.h>
#include <ULBaseView.h>
#include <ULBaseDoc.h>
#include <Project.h>
#include <GraphView.h>

#pragma pack(pop)

#endif // _UL_VW_BASE_H

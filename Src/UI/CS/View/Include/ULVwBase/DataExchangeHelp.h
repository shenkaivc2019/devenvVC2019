// DataExchangeHelp.h: interface for the CDataExchangeHelp class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DATAEXCHANGEHELP_H__5FD4A8E3_99F8_4A43_ADEE_240D080EFB6B__INCLUDED_)
#define AFX_DATAEXCHANGEHELP_H__5FD4A8E3_99F8_4A43_ADEE_240D080EFB6B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ULInterface.h"
#include "Cell2000.h"
//#include "CalDrawView.h"
#include "ULCalibrate.h"
#include "MyMEMDC.h"

#define DDX_TYPE_FLT		0
#define DDX_TYPE_DBL		1
#define DDX_TYPE_INT		2
#define DDX_TYPE_STR		3
#define DDX_TYPE_FUN		4
#define DDX_TYPE_PFC		5

class CMyMemDC;
class CULTool;
class CProject;
class CDataExchangeHelp
{
public:
	CDataExchangeHelp();
	virtual	~CDataExchangeHelp();

public:
	void	AddMap(CMapStringToPtr* pMap);
	void	DestroyMap();
	short	FindData(CString strContext, void*& va);
	BOOL    HaveData();
	void    GetSysID(CStringArray& ptrSys);
	void	GetUserID(CString strtool , CStringArray& ptrUser);

	int CalCellERFOperate(CULTool* pULTool, CStringArray& strArr, CALDRAW1& caldraw1, BOOL bDot, BOOL bEng);
	int CalCellOperate(CULTool* pULTool, CStringArray& strArr, CALDRAW& caldraw);
	CULCalCurveSeg* CalCellChartOperate(CULTool* pULTool, CStringArray& strArr);
	CULCalDot* CalCellRCOperate(CULTool* pULTool, CStringArray& strArr);
	int CalCellXYOperate(CULTool* pULTool, CStringArray& strArr);

	CMyMemDC* PrepareDraw(CCell2000* pCell, CRect& rc, int nRow, int nCol, CString strID, CString& strPath);
	void EndDraw(CCell2000 *pCell, CMyMemDC* pMemDC, int nRow, int nCol, CString strPath);
	CString	GetSaveBMPPath(CString strID);

private:
	CMapStringToPtr	m_fDataMap;
	CMapStringToPtr	m_dDataMap;
	CMapStringToPtr	m_strDataMap;
	CMapStringToPtr	m_iDataMap;
	CMapStringToPtr	m_funDataMap;
	CMapStringToPtr m_pFuncMap;
	CPtrArray		m_MapArray;

	void InitManager();
	BOOL SelCalPaintType(CString strID, int row, int col, CCell2000* pCell, CProject* pPrj);

public:
	void AddDataExchange(CString strID, float* value) {	m_fDataMap[strID] = value;	}
	void AddDataExchange(CString strID, double* value){	m_dDataMap[strID] = value;	}
	void AddDataExchange(CString strID, CString* value)	{ m_strDataMap[strID] = value;	}
	void AddDataExchange(CString strID, int* value) { m_iDataMap[strID] = value; }
	void AddDataExchange(CString strID, void* value) {	m_funDataMap[strID] = value; }
	void AddDataExchange(CString strID, PPAINTFUNC* value) { m_pFuncMap[strID] = value; }

	void UpdateCell(CCell2000* pCell, CProject* pPrj);	//刷新CELL
	void UpdateCell(CCell2000* pCell, long col, long row, CProject* pPrj);
	void UpdateVar(CCell2000* pCell);							//刷新值
	void UpdateVarByPos(int nRow, int nCol, CCell2000* pCell);
	void ClearMap();
	short FindDataFromID(CString strContext, void* va);
	BOOL IsHaveData();
	void GetAllSysID(CStringArray& ptrSys);
	void GetAllUserID(CString strtool,CStringArray& ptrUser);
};

extern CDataExchangeHelp Gbl_DataEx;

class CULCalCurve;
class CULCalCurveSeg;

////////////////////////系统刻度图/////////////////////////////////
void SysCalPaintX(CCell2000* pCell, long col, long row, long colE, long rowE, CALDRAW1* pCalDraw);
void SysCalPaint1(CDC* pDC, CRect rect, CALDRAW1* pCalDraw);
void SysCalPaint2(CDC* pDC, CRect rect, CALDRAW* pCalDraw);
void SysCalChartPaint(CDC *pDC, CRect rcCell, CULCalCurveSeg* pSeg);
void SysCalRCPaint(CDC *pDC, CRect rcCell, CULCalDot *pDot);
///////////////////////////////////////////////////////////////////
void SysCalXYPaint(CDC *pDC, CRect rcCell, CPtrArray& CurveList);
void PrintBKChart(CDC *pDC, CRect rc);
void LineToDash(CDC* pDC, CPoint start, CPoint end);
void PrintCalSeg(CDC* pDC, CULCalCurve* pCurve, CRect rc);
void GetBalanceValue(double* pMin, double* pMax);
double GetEquational(CULCalDot* pDot);
void ValueToString(double value, CString& str);
void DrawXInfo(CDC* pDC, double min, double max, CRect rc);
void DrawYInfo(CDC* pDC, double min, double max, CRect rc);
void LineToDot(CDC* pDC, CPoint p1, CPoint p2);
#endif // !defined(AFX_DATAEXCHANGEHELP_H__5FD4A8E3_99F8_4A43_ADEE_240D080EFB6B__INCLUDED_)

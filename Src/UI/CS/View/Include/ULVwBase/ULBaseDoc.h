﻿// ULDoc.h: interface for the CULBaseDoc class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "ULInterface.h"
#include "ULCOMMDEF.H"
#include "ULBaseDocInclude.h"
//
//class CULBaseDoc;
////typedef CList<CULBaseDoc*, CULBaseDoc*> CULBaseDocList;
//
//#define  NSTRUCT 8  //宏定义结构体个数
//
//class CCurve;
class CULChildFrame;
//typedef CList<CULChildFrame*, CULChildFrame*> CFrameUlChildList;
//class CGraphHeaderView;
//class CGraphWnd;
//class CProject;
class CULFileBaseDoc;
class CULBaseView;
//class CPrintParam;
//class CPof;
//class CToolParams;
//class CCustomizeView;
//
////定义一个结构体
//typedef struct  
//{
//	bool IsShow;
//	CString StrUp;
//	CString StrDown;
//	double MinUnit;
//	double MaxUnit;
//} FIELD;
//
//class CSheet;
//class CSheetInfo : public CTemplInfo
//{
//public:
//	CSheet* m_pSheet;
//	CULChildFrame* m_pFrame;
//	CSheetInfo()
//	{
//		m_pSheet = NULL;
//		m_pFrame = NULL;
//	}
//
//	~CSheetInfo();
//};
//
//class CCell2000;
//class CCellInfo : public CTemplInfo
//{	
//public:
//	CCell2000* pCell;
//	CULChildFrame* pFrame;
//	CCellInfo()
//	{
//		pCell = NULL;
//		pFrame = NULL;
//	}
//};
//
//class CCstInfo : public CTemplInfo
//{
//public:
//	LPVOID	lpReserved;
//	CULChildFrame* pFrame;
//	CCstInfo()
//	{
//		lpReserved = NULL;
//		pFrame = NULL;
//	}
//};
//
//typedef struct _tagCLOSEALLFILE
//{
//	BOOL bCancel;
//	BOOL bYesAll;
//	BOOL bNoAll;
//}CLOSE_ALL_FILE;
//
//#ifdef PEG_CHARTVIEW
//
//typedef struct tagPegDataSource
//{
//	float   fdepth;        // 深度
//	float	fDev;		   // 井斜
//	float	fAzim;		   // 方位
//}PegDataSource;
//
//typedef struct tagPegDataULTARGET
//{
//	float	fVertDepth;	        // 垂深
//	float   fSNPos;             //南北坐标
//	float   fEWPos;             //东西坐标
//	float   fAlpha;             //狗腿度
//	float   fProOffset;         //投影位移
//	float   fClosedAzim;        //闭合方位
//	float   fHOffset;           //水平位移   
//} PegDataULTARGET;
//
//typedef struct tagULDATAVALUE
//{
//	float	lDepth;				// 深度
//	float	lVertDepth;			// 垂深
//	float	fDev;				// 斜度、井斜
//	float	fAzim;				// 方位
//	float	fTrueAzim;			// 真方位、闭合方位
//	float	fTotalAzim;			// 总方位、投影位移
//	float	lTotalOffset;		// 总位移、水平位移
//	float	lEPos;				// E坐标、
//	float	lNPos;				// N坐标
//	float	fAlpha;				// 狗腿度
//	float   fTemperature;       //温度
//	float   fGSide;             //重力高边
//	float   fPSide;             //陀螺高边
//}DATAVALUE;
//
//#else
//
//typedef struct tagULDATAVALUE
//{
//	double	fDepth;				// 深度
//	double	fVertDepth;			// 垂深
//	double	fDev;				// 斜度
//	double	fAzim;				// 方位
//	double	fTrueAzim;			// 真方位
//	double	fTotalAzim;			// 总方位
//	double	fTotalOffset;		// 总位移
//	double	fEPos;				// E坐标
//	double	fNPos;				// N坐标
//	double	fAlpha;				// 狗腿度
//}DATAVALUE;
//
//#endif
//
//typedef std::vector<DATAVALUE>	VEC_DataValue;
//typedef CArray<DATAVALUE, DATAVALUE> CARRAY_DataValue;
//
//typedef struct tagULTARGET
//{
//	double	fVertDepth;			// 垂深
//	double	fRadius;			// 靶半径
//	double	fAzim;				// 靶方位
//	double	fOffset;			// 靶位移
//}ULTARGET;
//
//typedef std::vector<ULTARGET>	VECTOR_Target;
//
//typedef struct
//{
//	long	lDepth;				// 深度
//	long	lVertDepth;			// 垂深
//	long	lOffset;			// 距井口位移
//	double	fAzim;				// 距井口方位
//	long	lOffsetFromULTARGET;	// 距指定靶心位移
//	double	fAzimFromULTARGET;	// 距指定靶心方位
//}ULValueInfo;
//
//#define	AT_NEW	0x0001
//#define AT_FRM	0x0002
//#define AT_CRT	0x0004
//#define _FileThread
//
//#define FRAME_SIZE	250
//#define DISPLAY_MAP 2000
//
//typedef struct _tagULVERTEX_INFO
//{
//	float x;
//	float z;
//	BYTE bTime;
//	BYTE bColorRank;
//}ULVERTEX_INFO;
//
//typedef struct _tagULDRAW_INFO
//{
//	long nDepth;
//	float nCoordinate;
//	ULVERTEX_INFO VertexArray[FRAME_SIZE];
//}ULDRAW_INFO;
#ifndef ULVWBASE_API
	#ifdef ULVWBASE‌_EXPORT
		#define ULVWBASE_API __declspec( dllexport )
	#else	
		#define ULVWBASE_API __declspec( dllimport )
	#endif
#endif

class ULVWBASE_API CULBaseDoc : public CDocument
{
	DECLARE_DYNCREATE(CULBaseDoc)

public:
	CULBaseDoc();
	virtual ~CULBaseDoc();

	static CString ComboTemplsName(void* pTemplList)
	{
		CTemplInfoList* pTempls = (CTemplInfoList*)pTemplList;
		CString strRes = _T("");
		POSITION pos = pTempls->GetHeadPosition();
		for ( ; pos != NULL; )
		{
			CTemplInfo* pInfo = pTempls->GetNext(pos);
			if (pInfo && pInfo->strTempl.GetLength())
			{
				strRes += pInfo->strTempl;
				strRes += _T("_");
			}
		}
		
		int nLen = strRes.GetLength();
		if (nLen < 1)
			return strRes;
		
		return strRes.Left( --nLen);
	}

public:
	// Presentation operations
	CSheetInfo* GetSheetInfo(LPCTSTR pszTempl)
	{
		for (POSITION pos = m_sheetiList.GetHeadPosition(); pos != NULL; )
		{
			CSheetInfo* pSheetInfo = (CSheetInfo*)m_sheetiList.GetNext(pos);
			if (pSheetInfo->strTempl == pszTempl)
				return pSheetInfo;
		}
		
		return NULL;
	}

	BOOL AddSheetTempl(LPCTSTR pszTempl, DWORD dwAdd = 0);
	int AddSheetTempls(CStringArray& strTempls)
	{
		int nCount = strTempls.GetSize();
		int nSheets = nCount;
		for (int i = 0; i < nCount; i++)
		{
			if (AddSheetTempl(strTempls[i]))
				continue ;
			nSheets --;
		}
		
		return nSheets;
	}

	void GetSheetFiles(CStringArray* pFiles)
	{
		for (POSITION pos = m_sheetiList.GetHeadPosition(); pos != NULL; )
		{
			CSheetInfo* pSheetInfo = (CSheetInfo*)m_sheetiList.GetNext(pos);
			if (pSheetInfo != NULL)
				pFiles->Add(pSheetInfo->strFile);
		}
	}

	void GetSheetTempls(CStringArray* pTempls)
	{
		for (POSITION pos = m_sheetiList.GetHeadPosition(); pos != NULL; )
		{
			CSheetInfo* pSheetInfo = (CSheetInfo*)m_sheetiList.GetNext(pos);
			if (pSheetInfo != NULL)
				pTempls->Add(pSheetInfo->strTempl);
		}
	}

	CSheetInfo* RemoveSheetInfo(LPCTSTR pszTempl)
	{
		for (POSITION pos = m_sheetiList.GetHeadPosition(); pos != NULL; )
		{
			POSITION pos1 = pos;
			CSheetInfo* pSheetInfo = (CSheetInfo*)m_sheetiList.GetNext(pos);
			if (pSheetInfo->strTempl == pszTempl)
			{
				m_sheetiList.RemoveAt(pos1);
				return pSheetInfo;
			}
		}
		
		return NULL;
	}

	void ClearSheetInfo()
	{
		while (m_sheetiList.GetCount())
		{
			delete m_sheetiList.RemoveHead();
		}
	}
	
	// Header operations

	CCellInfo* GetCellInfo(LPCTSTR pszTempl)
	{
		for (POSITION pos = m_celliList.GetHeadPosition(); pos != NULL; )
		{
			CCellInfo* pCellInfo = (CCellInfo*)m_celliList.GetNext(pos);
			if (pCellInfo->strTempl == pszTempl)
				return pCellInfo;
		}

		return NULL;
	}

	CCellInfo* AddCellTempl(LPCTSTR pszTempl, DWORD dwAdd = 0);
	int AddCellTempls(CStringArray& strTempls)
	{
		int nCount = strTempls.GetSize();
		int nCells = nCount;
		for (int i = 0; i < nCount; i++)
		{
			if (AddCellTempl(strTempls[i]))
				continue ;
			nCells --;
		}
		
		return nCells;
	}

	void GetCellTempls(CStringArray* pTempls)
	{
		for (POSITION pos = m_celliList.GetHeadPosition(); pos != NULL; )
		{
			CCellInfo* pCellInfo = (CCellInfo*)m_celliList.GetNext(pos);
			if (pCellInfo != NULL)
				pTempls->Add(pCellInfo->strTempl);
		}
	}

	CCellInfo* RemoveCellInfo(LPCTSTR pszTempl)
	{
		for (POSITION pos = m_celliList.GetHeadPosition(); pos != NULL; )
		{
			POSITION pos1 = pos;
			CCellInfo* pCellInfo = (CCellInfo*)m_celliList.GetNext(pos);
			int nPos = pCellInfo->strTempl.Find ('.');
			CString strName = pCellInfo->strTempl;
			if(nPos >= 0)
				strName = pCellInfo->strTempl.Left (nPos);
			if (strName == pszTempl || pCellInfo->strTempl == pszTempl)
			{
				m_celliList.RemoveAt(pos1);
				return pCellInfo;
			}
		}

		return NULL;
	}

	void ClearCellInfo()
	{
		while (m_celliList.GetCount())
		{
			delete m_celliList.RemoveHead();
		}
	}

	// Get all cells of this document using
	POSITION FindCell(CCell2000* pCell, CGraphHeaderView** ppFView);
	void GetCelliList(CCellInfoList* pCells);
	void CellfromPof();


	CCstInfo* AddCstTempl(LPCTSTR pszTempl, DWORD dwAdd = 0);
	CCstInfo* GetCstInfo(LPCTSTR pszTempl)
	{
		for (POSITION pos = m_cstiList.GetHeadPosition(); pos != NULL; )
		{
			CCstInfo* pCstInfo = (CCstInfo*)m_cstiList.GetNext(pos);
			if (pCstInfo->strTempl == pszTempl)
				return pCstInfo;
		}

		return NULL;
	}

	CCstInfo* RemoveCstInfo(LPCTSTR pszTempl)
	{
		for (POSITION pos = m_cstiList.GetHeadPosition(); pos != NULL; )
		{
			POSITION pos1 = pos;
			CCstInfo* pCstInfo = (CCstInfo*)m_cstiList.GetNext(pos);
			int nPos = pCstInfo->strTempl.Find ('.');
			CString strName = pCstInfo->strTempl;
			if (nPos >= 0)
				strName = pCstInfo->strTempl.Left (nPos);
			if (strName == pszTempl || pCstInfo->strTempl == pszTempl)
			{
				m_cstiList.RemoveAt(pos1);
				return pCstInfo;
			}
		}

		return NULL;
	}

	void ClearCstInfo()
	{
		while (m_cstiList.GetCount())
		{
			delete m_cstiList.RemoveHead();
		}
	}

	// Plot operations

	CPofInfo* GetPofInfo(LPCTSTR pszTempl);

	BOOL AddPofTempl(LPCTSTR pszTempl);
	int AddPofTempls(CStringArray& strTempls)
	{
		int nCount = strTempls.GetSize();
		for (int i = 0; i < nCount; i++)
		{
			if (AddPofTempl(strTempls[i]))
				continue ;
			nCount --;
		}

		return nCount;
	}

	void GetPofTempls(CStringArray* pTempls);
	CPofInfo* RemovePofInfo(LPCTSTR pszTempl);
	void ClearPofInfo();

	// Frame operations
	virtual void OpenFrames(DWORD dwTypes);
	virtual void CloseFrames();
	CULChildFrame* GetFirstFrame(DWORD dwType, LPCTSTR pszTempl = NULL);
	

	// View operations
	CView* GetFirstView(DWORD dwType, LPCTSTR pszTempl = NULL);
	CGraphWnd* GetGraphWnd(UINT nIndex = 0);
	CULBaseView* GetCustomizeView(UINT nIndex = 0);
	CGraphWnd* GetPrintGraph();
	void GetDepthRange(int nDirection, long& lDepth0, long& lDepth1, int nDriveMode = UL_DRIVE_DEPT);
	void AdjustAllGraphs(BOOL bRemap = FALSE, BOOL bInit = FALSE);
	CULBaseView* GetPPView(CPrintParam* pParam, BOOL bInit = FALSE);
	void UpdateViews(DWORD dwType);
	void UpdateCells(DWORD dwType);
	void InvalidateCell(CCell2000* pCell, long lCol, long lRow, long lSheet);
	virtual void ShowView(DWORD dwType)
	{

	}

	// Infor operations
	virtual void GetRecord(CString& strDoc, CString& strTime, CString& strRecord,
		CString& strDepthST, CString& strDepthED, CSheet* pSheet)
	{

	}

	virtual void GetRecord2(CString& strComp, CString& strTeam, CString& strWell, CString& strULFIELD)
	{

	}

	CPtrArray* GetTools();
	CString	GetSimpleName(LPCTSTR pszPathName = NULL)
	{
		CString strPathName = pszPathName ? pszPathName : m_strPathName;
		int nLen = strPathName.GetLength();
		if (nLen > 0)
		{
			int nF = strPathName.ReverseFind('\\');
			if (nF < 0)
				return strPathName;
			
			return strPathName.Right(nLen - nF - 1);
		}
		
		return _T("");
	}

	static BOOL  OnAdjustView(CGraphWnd* pGraph);
	CPof*        GetPOF(int nIndex);
	void         PreparePrint(CSheet* pSheet, BOOL bRecalcGaps = FALSE);
	void         TraceFile(LPCTSTR pszFile);
	int          Trace(UINT nID, DWORD dwType, UINT nEvent, UINT nResult, LPCTSTR pszRemark = NULL);
	int          Trace(UINT nID, DWORD dwType, UINT nEvent, LPCTSTR pszResult, LPCTSTR pszRemark = NULL);
	void         TraceClose();
	BOOL         IsNoData();
	void         ReleaseCurvesData();
	void         SaveDatas();
	CString      GetToolSNByCurve(CString strCurve);//得到仪器系列号
	CString      GetToolANByCurve(CString strCurve);//得到仪器资产号
	CString		 GetUniqueName(CString strName);
	
    BOOL		 IsCurveNameExist(CString strName);
	void		 SolidCompute();
	CCurve*		 GetCurve(CString strName);			//获取指定的曲线	
	void		 SetDisplayPosition();				//根据当前深度设置显示位置
	void		 SetDisplayDepth(long nDepth);		//设置当前显示深度
	void		 ClearSolidInfo();
public:
	static CULBaseDoc* GetDoc(LPCTSTR pszPathName);
	static CULBaseDoc* GetDoc(int nIndex);
	static CULFileBaseDoc* CloseFiles();
	static BOOL CloseFile(CULFileBaseDoc* pULFile, CLOSE_ALL_FILE *pInfo = NULL);
	static void EarseDoc(CULBaseDoc* pDoc);
	inline static int	GetCount()	{	return m_DocList.GetCount(); }
	static CULFileBaseDoc* GetFile(int nIndex = 0);
	static CULBaseDocList	m_DocList;	// Share opened documents

public:
	CFrameUlChildList		m_frmList;		// Frame list
	CSheetInfoList	m_sheetiList;	// Sheet info list
	CCellInfoList	m_celliList;	// Cell info list
	CCstInfoList	m_cstiList;		// Customize info list
	CPofInfoList	m_pofiList;		// Pof info list
	CProject*		m_pProject;		// The project info
	long			m_nRead;
	DWORD			m_dwModified;	// Modify flag of templs info list
	CPtrArray		m_arrTools;		// Tools' array
	vec_ic			m_vecCurve;
	CCurve*			m_pDCurve;
	CCurve*			m_pTCurve;
	CArray<long, long> m_GapDepthArray;
	CList<CToolParams*, CToolParams*> 	    m_tpTree;		// Tool parameters tree
	CARRAY_DataValue          m_arData;		// 三图一表井斜数据
	VECTOR_Target		    m_vecTarget;	// 三图一表靶心数据
	
	//立体图所需数据
	CPtrArray		m_VertexList;	// 立体图各顶点数据
	UINT			m_nStartPos;	// 起始点
	UINT			m_nEndPos;		// 终止点
	long			m_nDisplayDepth;// 当前显示深度
	long			m_nDisplayRange;// 显示深度范围
	double			m_fRatio;		//径向放大比例

	int				m_nDriveMode;	// 打开文件时文件存储的驱动模式
protected:
	DECLARE_MESSAGE_MAP()
};

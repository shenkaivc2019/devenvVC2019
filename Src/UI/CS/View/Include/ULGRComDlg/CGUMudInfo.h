#pragma once
// MyCUG.h : interface of the CUGMudInfo class
//
/////////////////////////////////////////////////////////////////////////////
#include "ugctrl.h"
#include "UGCTRado.h"
#include "UGCTelps.h"
#include "UGCTLabeled.h"
#include "UGCTMarquee.h"

#include "IDB.h"
#include <vector>

using namespace std;

#ifndef ULGRCOMDLG_API
	#ifdef ULGRCOMDLG_EXPORT
		#define ULGRCOMDLG_API __declspec(dllexport)
	#else	
		#define ULGRCOMDLG_API __declspec(dllimport)
	#endif
#endif

#define WM_GRIDCOLDEL_2 WM_USER+105

//2020.5.30 dyl start 加入毫秒，加两列
//#define COLS 10//列数
//#define COLS 13//列数
//2020.5.30 dyl end
#define COLS 9 //列数,added by david 2020/07/10

#define	LABEL_BACK		RGB(100,150,200)
#define SUBLABEL_BACK	RGB(220,200,180)

#define MY_SEPARATOR		10
#define USE_UPPER			1001
#define USE_LOWER			1002
#define USE_PASSWORD		1003
#define USE_CHARLIMIT		1004
#define USE_NUMLIMIT		1005
#define USE_FINISHTEST		1006
#define USE_SETMASK			1007
#define USE_SETCOXMASK		1008
#define USE_SETCOXNUMBER	1009
#define USE_SETCOXCURRENCY	1010

#define FLEXEDIT_ID			320001
#define COXMASK_ID			320002
#define COXNUMBER_ID		320003
#define COXCURRENCY_ID		320004


class ULGRCOMDLG_API CUGMudInfo :public CUGCtrl
{
public:
	CUGMudInfo();
	~CUGMudInfo();

	// celltypes
	CUGRadioType m_radioType;
	CUGEllipsisType m_ellipType;
	CUGLabeledType m_labeledType;
	CUGMarqueeType m_marqueeType;
	
protected:

	// font functions
	struct _tagFonts
	{
		int fontIndex;
		CFont font;
		_tagFonts* next;

		_tagFonts()
		{
			fontIndex = -1;
			next = NULL;
		}
	} *m_fontsList;

	CFont* m_defFont;
	CFont* m_smallFont;
	CFont* m_largeFont;

	BOOL ShowFontDlg(CUGCell& cell, LOGFONT& selectedLogFont, COLORREF& textColor);
	CFont* CreateFont(int nHeight, int nWidth, int nEscapement, int nOrientation, int nWeight, BYTE bItalic, BYTE bUnderline, BYTE cStrikeOut, BYTE nCharSet, BYTE nOutPrecision, BYTE nClipPrecision, BYTE nQuality, BYTE nPitchAndFamily, LPCTSTR lpszFacename);
	CFont* CreateFontIndirect(LOGFONT newLogFont);

	// additional edit controls
	CUGEdit m_myCUGEdit;

private:
	DECLARE_MESSAGE_MAP()

public:
	//***** Over-ridable Notify Functions *****
	virtual void OnSetup();
	//mouse and key strokes
	virtual void OnDClicked(int col, long row, RECT* rect, POINT* point, BOOL processed);

	virtual void OnKeyDown(UINT* vcKey, BOOL processed);
	virtual void OnCharDown(UINT* vcKey, BOOL processed);

	//cell type notifications
	virtual int OnCellTypeNotify(long ID, int col, long row, long msg, long param);

	//editing
	virtual int OnEditStart(int col, long row, CWnd** edit);
	virtual int OnEditVerify(int col, long row, CWnd* edit, UINT* vcKey);
	virtual int OnEditFinish(int col, long row, CWnd* edit, LPCTSTR string, BOOL cancelFlag);
	virtual int OnEditContinue(int oldcol, long oldrow, int* newcol, long* newrow);

	virtual COLORREF OnGetDefBackColor(int section);

	//mouse and key strokes
	virtual void OnLClicked(int col, long row, int updn, RECT* rect, POINT* point, int processed);
	virtual void OnSH_LClicked(int col, long row, int updn, RECT* rect, POINT* point, BOOL processed = 0);

	//movement and sizing
	virtual void OnSelectionChanged(int startCol, long startRow, int endCol, long endRow, int blockNum);

	//menu notifications
	virtual int  OnMenuStart(int col, long row, int section);

	//探测单元格的值前后是否有变化
	/*
	virtual int OnEditStart(int col, long row, CWnd** edit);
	virtual int OnEditFinish(int col, long row, CWnd* edit, LPCTSTR string, BOOL cancelFlag);
	*/

public:
	//字体，宋体（9）
	CFont m_bigHdgFont;
	CFont m_smallHdgFont;
	CFont m_gridDefFont;

	vector<int> m_vecMudInfoID;//存放MudInfoTB表中的MudID字段的向量
};

﻿#pragma once

#include <vector>
using namespace std;

#ifndef ULGRCOMDLG_API
	#ifdef ULGRCOMDLG_EXPORT
		#define ULGRCOMDLG_API __declspec(dllexport)
	#else	
		#define ULGRCOMDLG_API __declspec(dllimport)
	#endif
#endif
// DlgTimeDrillingMode 对话框

class MyCug;
class ULGRCOMDLG_API DlgTimeDrillingMode : public CDialogEx
{
	DECLARE_DYNAMIC(DlgTimeDrillingMode)

public:
	DlgTimeDrillingMode(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~DlgTimeDrillingMode();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_TIMEDRILLINGMODE };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();

	vector<TS_TIMEDRILLINGMODE> m_vecTimeDrillingMode;
	MyCug* m_pCtrl;

	void ShowGrid(vector<TS_TIMEDRILLINGMODE> vecTimeDrillingMode);

	void OnAdd();
	void OnDel();
	void OnAlter();
	void OnCheck();

	BOOL UpdateData(BOOL bSaveAndValidate = TRUE);

	void OnOutputExcel();//导出至Excel

	time_t string2time_t(const string& strTime);
	BOOL time_t2string(time_t t, string& strTime);

	CString ModeIntToStr(int nMode);
	int ModeStrToInt(CString strMode);

	afx_msg LRESULT DelGridCol(WPARAM wParam, LPARAM lParam);

	//对应数据库表TimeDrillingMode中的数据
	UINT m_iRunID;
	CString m_strStartTime;
	CString m_strMillitime;
	//CComboBox m_cbDrillingMode;
	UINT m_iDrillingMode;
	CString m_strDrillingMode;
	CString m_strEndTime;
	CString m_strEndMillitime;

	vector<TS_RUNINFOTB> m_vecRunInfo;
	IDB* m_pIDB;

	//2020.5.29 dyl start
	void onDraw();
	//2020.5.29 dyl end
};

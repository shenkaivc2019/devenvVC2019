﻿#pragma once
#include "CGUMudInfo.h"

#ifndef ULGRCOMDLG_API
	#ifdef ULGRCOMDLG_EXPORT
		#define ULGRCOMDLG_API __declspec(dllexport)
	#else	
		#define ULGRCOMDLG_API __declspec(dllimport)
	#endif
#endif

// DlgMudInfo 对话框

class ULGRCOMDLG_API DlgMudInfo : public CDialogEx
{
	DECLARE_DYNAMIC(DlgMudInfo)

public:
	DlgMudInfo(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~DlgMudInfo();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_MUDINFO };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:

	virtual BOOL OnInitDialog();

	void OnAdd();
	void OnDel();
	void OnAlter();
	void OnCheck();

	void OnOutputExcel();
	BOOL DoVerify();

	CString MudTypeIntToStr(int iMudType);
	int MudTypeStrToInt(CString strType);
	void  ShowGrid(const vector<TS_MUDINFOTB>& vecMudInfo);
	BOOL time_t2string(time_t t, string& strTime);
	time_t string2time_t(const string& strTime);
	
	afx_msg LRESULT DelGridCol(WPARAM wParam, LPARAM lParam);

	BOOL OnInitUpdateData(BOOL bSaveAndValidate = TRUE);

	CUGMudInfo m_ctrl;

	vector<TS_MUDINFOTB> m_vecMudInfo;

	IDB* m_pIDB;

	//double PostPoint(double number, UINT bits);
	afx_msg void OnBnClickedMubAdd();
	afx_msg void OnBnClickedMudSave();
	afx_msg void OnBnClickedMudDel();
//2020.9.23 sys Start 单位
	std::vector<TS_VIEWITEMUNITTB> m_vecInitViewItem;
//2020.9.23 sys End 单位
};

﻿#pragma once

#ifndef ULGRCOMDLG_API
	#ifdef ULGRCOMDLG_EXPORT
		#define ULGRCOMDLG_API __declspec(dllexport)
	#else	
		#define ULGRCOMDLG_API __declspec(dllimport)
	#endif
#endif
// DlgMudInfoAdd 对话框

class ULGRCOMDLG_API DlgMudInfoAdd : public CDialogEx
{
	DECLARE_DYNAMIC(DlgMudInfoAdd)

public:
	DlgMudInfoAdd(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~DlgMudInfoAdd();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_MUDINFO_ADD };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	CString m_strName;
	CString m_strStartTime;
	CString m_strEndTime;
	CComboBox m_cmbType;
	CString m_strType;
	int m_iMudType;
	CString m_strDensity;
	CString m_strResistivity;
	CString m_strTemperature;
	CString m_strPContent;
	//CString m_strCOPPercentage;
	CString m_strDownHoleTemperature;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL OnInitDialog();
	void InitMudType();

	BOOL FloatTypeChecking(CString strFloat);//浮点类型数据输入校验
	BOOL TimeFormatCheck(CString str);//时间格式校验

	int MudTypeStrToInt(CString strMudType);
	CString MudTypeIntToStr(int iMudType);
	afx_msg void OnEnChangeEditMudinfoResistivity();
	afx_msg void OnEnChangeEditMudinfoTemperature();
	afx_msg void OnEnChangeEditMudinfoDensity();
	afx_msg void OnEnChangeEditMudinfoPcontent();
	afx_msg void OnEnChangeEditMudinfoCppercentage();
	afx_msg void OnEnChangeEditMudinfoUndertemperature();
	afx_msg void OnEnChangeEditMudinfoStarttime();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);

	//2020.6.3 dyl start
protected:
	// 开始毫秒
	int m_startMilli;
	// 结束毫秒
	int m_endMilli;
	CTime m_lastTime;//上次结束时间
	int m_lastMilli;//上次结束毫秒
public:
	void SetLastTime(const time_t lastTime);
	//2020.6.3 dyl end
	void SetDefaultParams(TS_MUDINFOTB *pRecord);
};

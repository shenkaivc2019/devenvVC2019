﻿#pragma once

#include "TRunInfoTB.h"
#include <vector>

using namespace std;
#ifndef ULGRCOMDLG_API
	#ifdef ULGRCOMDLG_EXPORT
		#define ULGRCOMDLG_API __declspec(dllexport)
	#else	
		#define ULGRCOMDLG_API __declspec(dllimport)
	#endif
#endif

// DlgTimeDrillingModeAdd 对话框

class ULGRCOMDLG_API DlgTimeDrillingModeAdd : public CDialogEx
{
	DECLARE_DYNAMIC(DlgTimeDrillingModeAdd)

public:
	DlgTimeDrillingModeAdd(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~DlgTimeDrillingModeAdd();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_TIMEDRILLINGMODE_ADD };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	//afx_msg void OnBnClickedButtonDrillingmode();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnEnChangeEditStarttime();
	afx_msg void OnEnChangeEditEndtime();

	CString m_strRunId;
	CComboBox m_comRunId;
	CString m_strStartTime;
	CString m_strMillitime;
//	CComboBox m_cbDrillingMode;
	UINT m_iDrillingMode;
	CString m_strDrillingMode;
	CString m_strEndTime;
	CString m_strEndMillitime;

	CString m_strRunID;
	
	vector<TS_RUNINFOTB> m_vecRunInfo;

	BOOL TimeFormatCheck(CString str);
	virtual BOOL OnInitDialog();
	void InitModeComb();
	void InitDrillingNameComb();
	int ModeStrToInt(CString strMode);

	void SetLastTime(const CTime lastTime, const int lastMillitime);

	//2020.5.27 dyl start 时间控件绑定的变量
public:
	CDateTimeCtrl m_dateTimeStart;
	CDateTimeCtrl m_dateTimeEnd;
protected:
	CTime m_lastTime;
	int m_lastMillitime;
	//2020.5.27 dyl end

	IDB* m_pIDB;
};

﻿#pragma once

#include <vector>
#include <string>

using namespace std;

#ifndef ULGRCOMDLG_API
	#ifdef ULGRCOMDLG_EXPORT
		#define ULGRCOMDLG_API __declspec(dllexport)
	#else	
		#define ULGRCOMDLG_API __declspec(dllimport)
	#endif
#endif
// CDlgGamaCorrParameter 对话框

class ULGRCOMDLG_API CDlgGamaCorrParameter : public CDialogEx
{
	DECLARE_DYNAMIC(CDlgGamaCorrParameter)

public:
	CDlgGamaCorrParameter(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~CDlgGamaCorrParameter();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_GAMA_CORR_PARA };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedSave();
	
	void InitCmbToolType();

	CListBox m_list;
	int m_nCurSel;
	CString m_strMName;
	CComboBox m_cmbMPos;
	UINT m_iMPos;
	CString m_strMPos;
	CString m_strCalibrationTime;
	CString m_strCAPI; // 传感器校准系数
	CString m_strOD;
	CString m_strID;

	CString m_strHoleSize;
	CString m_strToolSize;
	CString m_strACF; // 钻铤校正系数

	IDB* m_pIDB;
	
	virtual BOOL OnInitDialog();
	afx_msg void OnCbnSelchangeComboMPos();
	void InitList();
	void time_t2string(time_t t, string& strTime);
	void EmptyMemberVariable();
	void NameToId(CString strName);
	
	vector<MS_GAMACORR_PARA> m_vecGamaCorrPara;
	UINT m_iMID;
	BOOL TimeFormatCheck(CString str);//时间格式校验
	BOOL FloatTypeChecking(CString strFloat);//浮点型数据输入校验

	BOOL OnInitUpdateData(BOOL bSaveAndValidate = TRUE);

	afx_msg void OnEnChangeEditOd();
	afx_msg void OnEnChangeEditAcf();
	afx_msg void OnEnChangeEditId();
	afx_msg void OnEnChangeEditCalibrationTime();
	afx_msg void OnLbnSelchangeListGammaSubSerial();
	CDateTimeCtrl m_dateTimeGmaCheck;
	CString m_strS2B;
//2020.9.23 sys Start 单位
	CString strUName;
	CString m_strHoleSizeUnit;
	CString m_strS2BUnit;
	CString m_strODUnit;
	CString m_strIDUnit;
	std::vector<TS_VIEWITEMUNITTB> m_vecInitViewItem;
	std::map<int, CString> mapUidName;
	CString strItemID;
//2020.9.23 sys End 单位
};

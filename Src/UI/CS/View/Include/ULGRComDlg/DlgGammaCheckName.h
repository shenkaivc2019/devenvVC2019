﻿#pragma once

#ifndef ULGRCOMDLG_API
	#ifdef ULGRCOMDLG_EXPORT
		#define ULGRCOMDLG_API __declspec(dllexport)
	#else	
		#define ULGRCOMDLG_API __declspec(dllimport)
	#endif
#endif
// DlgGammaCheckName 对话框

class ULGRCOMDLG_API DlgGammaCheckName : public CDialogEx
{
	DECLARE_DYNAMIC(DlgGammaCheckName)

public:
	DlgGammaCheckName(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~DlgGammaCheckName();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_GAMMACHECKNAME };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	CString m_strGSName;
};

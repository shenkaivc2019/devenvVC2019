﻿#pragma once

#include <vector>
#include <string>

using namespace std;

#ifndef ULGRCOMDLG_API
	#ifdef ULGRCOMDLG_EXPORT
		#define ULGRCOMDLG_API __declspec(dllexport)
	#else	
		#define ULGRCOMDLG_API __declspec(dllimport)
	#endif
#endif
// DlgGamaCheck 对话框

class ULGRCOMDLG_API DlgGamaCheck : public CDialogEx
{
	DECLARE_DYNAMIC(DlgGamaCheck)

public:
	DlgGamaCheck(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~DlgGamaCheck();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_GAMACHECK };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	
	void InitCmbToolType();

	CListBox m_list;
	int m_nCurSel;
	CString m_strGSName;
	CComboBox m_cmbToolType;
	UINT m_iToolType;
	CString m_strToolType;
	CString m_strCalibrationTime;
	CString m_strDescription;
	CString m_strACF;
	CString m_strOD;
	CString m_strID;

	//2020.7.5 sys Start
	CString m_BitSize;
	CString m_BarrelOD;
	CString m_MWDCAPI;
	CString m_LWDCc;
	CString m_LWDCAPI;
	//2020.7.5 sys End

	IDB* m_pIDB;
	
	virtual BOOL OnInitDialog();
	afx_msg void OnCbnSelchangeComboToolType();
	void InitList();
	void time_t2string(time_t t, string& strTime);
	CString TypeToolNumToStr(UINT nToolType);
	int TypeToolStrToNum(CString strToolType);
	void EmptyMemberVariable();
	void NameToId(CString strName);
	
	vector<TS_GAMMASUB> m_vecGammaSub;
	UINT m_iGSID;
	void OnOutputExcel();
	BOOL TimeFormatCheck(CString str);//时间格式校验
	BOOL FloatTypeChecking(CString strFloat);//浮点型数据输入校验

	BOOL OnInitUpdateData(BOOL bSaveAndValidate = TRUE);

	afx_msg void OnEnChangeEditOd();
	afx_msg void OnEnChangeEditAcf();
	afx_msg void OnEnChangeEditId();
	afx_msg void OnEnChangeEditCalibrationTime();
	afx_msg void OnLbnSelchangeListGammaSubSerial();
	LRESULT OnDelGamaCheck(WPARAM wParam, LPARAM lParam);
	CDateTimeCtrl m_dateTimeGmaCheck;
	afx_msg void OnBnClickedGamaDel();
};

﻿#pragma once
#include "CGUCurveStore.h"

#ifndef ULGRCOMDLG_API
	#ifdef ULGRCOMDLG_EXPORT
		#define ULGRCOMDLG_API __declspec(dllexport)
	#else	
		#define ULGRCOMDLG_API __declspec(dllimport)
	#endif
#endif
// DlgCurveStore 对话框

class ULGRCOMDLG_API DlgCurveStore : public CDialogEx
{
	DECLARE_DYNAMIC(DlgCurveStore)

public:
	DlgCurveStore(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~DlgCurveStore();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_CURVESTORE };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	
	int TypeStrToInt(CString strType);
	CString TypeIntToStr(int iType);

	void ShowGrid(const vector<TS_CURVESTORE>& vecCurveStore);

	afx_msg LRESULT DelGridCol(WPARAM wParam, LPARAM lParam);
	
	vector<TS_CURVESTORE> m_vecCurveStore;

	CUGCurveStore m_ctrl;
	IDB* m_pIDB;

	BOOL UpdateData(BOOL bSaveAndValidate = TRUE);

	void OnAdd();
	void OnDel();
	void OnAlter();
	void OnCheck();

	void OnOutputExcel();
};

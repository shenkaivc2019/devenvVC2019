﻿#pragma once

#include <vector>

using namespace std;

#ifndef ULGRCOMDLG_API
	#ifdef ULGRCOMDLG_EXPORT
		#define ULGRCOMDLG_API __declspec(dllexport)
	#else	
		#define ULGRCOMDLG_API __declspec(dllimport)
	#endif
#endif
// DlgCurveStoreAdd 对话框

class ULGRCOMDLG_API DlgCurveStoreAdd : public CDialogEx
{
	DECLARE_DYNAMIC(DlgCurveStoreAdd)

public:
	DlgCurveStoreAdd(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~DlgCurveStoreAdd();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_CURVESTORE_ADD };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnCbnSelchangeComboTablename();

	virtual BOOL OnInitDialog();
	void InitType();
	void InitData();
	void InitTableName();
	int TypeStrToInt(CString strType);
	CString TypeIntToStr(int iType);

	CString m_strID;
	CString m_strNameCN;
	CString m_strAlias;
	CString m_strDescCN;
	CString m_strDescEN;
	CComboBox m_cmbType;
	CString m_strType;
	int m_iType;
	vector<CString> m_vecAllTableName;
	CComboBox m_cbTableName;
	IDB* m_pIDB;
};

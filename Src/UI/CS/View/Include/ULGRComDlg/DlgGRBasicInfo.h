
// BasicInfoMaintainView.h: CDlgGRBasicInfo 类的接口
//

#pragma once
#include "DlgTimeDrillingMode.h"
#include "DlgGamaCheck.h"
#include "DlgMudInfo.h"
#include "DlgCurveStore.h"

#ifndef ULGRCOMDLG_API
	#ifdef ULGRCOMDLG_EXPORT
		#define ULGRCOMDLG_API __declspec(dllexport)
	#else	
		#define ULGRCOMDLG_API __declspec(dllimport)
	#endif
#endif

class ULGRCOMDLG_API CDlgGRBasicInfo : public CDialogEx
{
public: // 仅从序列化创建
	CDlgGRBasicInfo(CWnd* pParent = nullptr);
	virtual ~CDlgGRBasicInfo();

	DECLARE_DYNAMIC(CDlgGRBasicInfo)

public:
#ifdef AFX_DESIGN_TIME
	enum{ IDD = IDD_BASICINFOMAINTAIN_FORM };
#endif
public:
	virtual BOOL OnInitDialog(); // 构造后第一次调用
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持


protected:

// 生成的消息映射函数
protected:
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTcnSelchangeTabCtrl(NMHDR* pNMHDR, LRESULT* pResult);
	
	CTabCtrl m_TabCtrl;
	DlgTimeDrillingMode m_dlgDrillingMode;
	DlgGamaCheck m_dlgGamaCheck;
	DlgMudInfo m_dlgMudInfo;
	DlgCurveStore m_dlgCurveStore;

	BOOL m_iStatus;
	
	afx_msg void OnMenuMainAdd();
	afx_msg void OnMenuMainDel();
	afx_msg void OnMenuMainAlter();
	afx_msg void OnMenuMainCheck();

	int	m_view_min_x;
	int	m_view_min_y;

	int m_printPage;
	BOOL m_isPrinting;
	CRect m_drawRect;

	IDB* m_pIDB;

	void DrawTitle(CDC* pDC, int nPageNum);

	void BeginPrintContentTab0(MyCug* ctrl);
	void BeginPrintContentTab2(CUGMudInfo* ctrl);
	void BeginPrintContentTab3(CUGCurveStore* ctrl);
	void EndPrintContentTab0(MyCug* ctrl);
	void EndPrintContentTab2(CUGMudInfo* ctrl);
	void EndPrintContentTab3(CUGCurveStore* ctrl);

	afx_msg void OnOutpotExcel();

	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
	afx_msg void OnSaveData();
};

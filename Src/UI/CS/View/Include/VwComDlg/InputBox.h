#if !defined(AFX_INPUTBOX_H__53D2E67F_3B4E_4576_8637_2BDF52312C45__INCLUDED_)
#define AFX_INPUTBOX_H__53D2E67F_3B4E_4576_8637_2BDF52312C45__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifndef VWCOMDLG_API
	#ifdef VWCOMDLG_EXPORT
		#define VWCOMDLG_API __declspec( dllexport )
	#else	
		#define VWCOMDLG_API __declspec( dllimport )
	#endif
#endif
#define IM_DIGIT	0
#define IM_STRING	1
#define IM_DEPTH	2
#define IM_PAGESIZE	3
#define IM_CHECK	4
#define IM_DEPTHHEAD 8

// InputBox.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CInputBox dialog

class VWCOMDLG_API CInputBox : public CDialog
{
// Construction
public:
	CInputBox(CWnd* pParent = NULL, int nMode = 0 /*数值*/);   // standard constructor
	CInputBox(UINT nIDPrompt, CWnd* pParent = NULL, int nMode = 0 /*数值*/);
	CInputBox(UINT nIDTitle, UINT nIDPrompt, CWnd* pParent = NULL, int nMode = 0 /*数值*/);

// Dialog Data
	//{{AFX_DATA(CInputBox)
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_INPUTBOX };
#endif
	CString	m_strPrompt;
	CString	m_strValue;
	int		m_bAll;
	long	m_lDepth;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInputBox)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CInputBox)
	virtual void OnOK();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnKillfocusEditInput();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void	SetTitle(LPCTSTR lpszTitle);
	void	SetPrompt(LPCTSTR lpszPrompt);
	void	SetPrompt(UINT nID);

public:
	int		m_nMode;		// 0:数据值	1：字符串
	int		m_nChars;
	CString m_strTitle;
	double	m_fValue;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INPUTBOX_H__53D2E67F_3B4E_4576_8637_2BDF52312C45__INCLUDED_)

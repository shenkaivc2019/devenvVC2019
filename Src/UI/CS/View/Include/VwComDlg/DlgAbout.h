//---------------------------------------------------------------------------//
//文件名称:DlgAbout.h
//文件说明：程序版本共通信息对话框,为其他程序提供版本信息。
//创建时间：2007/07/01
//修改时间：2008/12/30
//---------------------------------------------------------------------------//

#if !defined _CABOUTDLG_H
#define  _CABOUTDLG_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#undef AFX_DATA
#ifdef _GFXCOMDLG_BUILD
#define AFX_DATA AFX_EXT_DATA
#else
#define AFX_DATA AFX_DATA_IMPORT
#endif

class CAboutDlg : public CDialog
{
public:
	CAboutDlg(CWnd* pParent, UINT uIDIcon = 0);

	// Operations
public:

	//{{AFX_VIRTUAL(CAboutDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV     
	//}}AFX_VIRTUAL

	//            
protected:
	UINT  m_uIDIcon;

	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#undef AFX_DATA
#define AFX_DATA

#endif //  _CABOUTDLG_H

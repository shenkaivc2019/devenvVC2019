//----------------------------------------------------------------------//
// ファイル名:	Get21CoolBar.cpp
//
// 説明:	クールバー＆ツールバー クラス
// 会社名:	株式会社マックインターフェイス
// 作成者:	松本・山本
// 作成日:	1998/7/22
// 備考:	なし
//----------------------------------------------------------------------//

///////////////////////////
// 変更履歴
//   1999/7/8   ・ボタンが有効状態から無効状態になる際にボタン状態が不正に
//                なる不具合を修正しました。
//   1999/6/30  ・文字列をセンタリングする際にスペースが最低左右に1つずつ
//                入ってしまう不具合を修正しました。
//   1999/5/25  ・クールバー上のボタンが無効状態から有効状態になるときに、
//                テキストの表示が重なってしまう不具合を修正しました。
//              ・すべてのボタンテキストがBUTTONSIZE_CXに満たないときに、
//                ボタンサイズがずれる不具合を修正しました。
//              ・フォント名をストリングテーブルに移しました。
//              ・初期表示時にボタンサイズが遅れて変更される不具合を修正し
//                ました。
//   1999/3/16  ・コメント上で「ポイント」となっている箇所を「ピクセル」に
//                しました（語弊が有るため）。
//   1999/3/8   ・フォントを「ＭＳ Ｐゴシック 9ポイント」にしました。
//              ・フォントの変更に伴い、BUTTONSIZE_CXを56にしました。
//   1999/2/23  ・SetFontの第２引数をFALSE指定するようにしました。
//              ・一部のコメント化された部分を削除しました。
//   1999/1/21  ・ポイント数を調整する際に最初のボタンしか調整していなかっ
//                た不具合を修正しました。
//   1999/1/20  ・クールバーの下の文字列で、最後のセクションが見つからなかっ
//                たときに以前の文字列が表示されてしまう不具合を修正しまし
//                た。
//   1999/1/14  ・ソースヘッダを規約にあわせました。
//   1998/12/25 ・クールバーの下の文字列を文字数ではなく、ポイント数で
//                調整するようにしました。

#include "StdAfx.h"
#include "resource.h"

#include <Get21CoolBar.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// ボタンサイズ
#define		BUTTONSIZE_CX	(56)
#define		BUTTONSIZE_CY	(40)

// イメージサイズ
#define		IMAGESIZE_CX	(22)
#define		IMAGESIZE_CY	(20)

#define		GET21_REBAR_NOSTR	("")

#define		STRING_WITDH		16

// フォント名
#define		GET21_REBAR_FONTNAME	("ＭＳ Ｐゴシック")

BOOL CGet21CoolbarItem::Add(int nCommandID, int nBitmapID, LPCSTR lpszTitle, BOOL bIsDependMenu)
{
	try 
	{
		GET21_COOLBAR_INFO rec;

		rec.nCommandID			= nCommandID;
		rec.nBitmapID			= nBitmapID;
		rec.lpszTitle			= lpszTitle;
		rec.bIsDependMenu		= bIsDependMenu;

		m_array.Add(rec);

	}
	catch (CMemoryException* pEX)
	{
		delete pEX;

		return FALSE;
	}

	return TRUE;
}


//-------------------------------------------------------------------
// クールバークラス
//-------------------------------------------------------------------
/////////////////////////////////////////////////////////////////////
//  CCoolBar
IMPLEMENT_DYNAMIC(CCoolBar, CControlBar)
BEGIN_MESSAGE_MAP(CCoolBar, CControlBar)
	//{{AFX_MSG_MAP(CCoolBar)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_NOTIFY_REFLECT(RBN_HEIGHTCHANGE, OnHeigtChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//-------------------------------------------------------------------
// コンストラクタ
//-------------------------------------------------------------------
CCoolBar::CCoolBar()
{
	// クールバー情報構造体の初期化
	m_info = NULL;

	m_res_id = NULL;

}

//-------------------------------------------------------------------
// デストラクタ
//-------------------------------------------------------------------
CCoolBar::~CCoolBar()
{
}

//-------------------------------------------------------------------
// クールバー生成
//-------------------------------------------------------------------
BOOL CCoolBar::Create(CWnd* pParentWnd, DWORD dwStyle, DWORD dwAfxBarStyle, UINT nID)
{
	// 親ウインドウオブジェクトチェック
	ASSERT_VALID(pParentWnd);

	// 動的コントロールバー非対応
	dwStyle &= ~CBRS_SIZE_DYNAMIC;

	// スタイルの保存 ( m_dwStyle : CControlBar データメンバ )
	m_dwStyle = dwAfxBarStyle;
	if (nID == AFX_IDW_TOOLBAR)
	{
		m_dwStyle |= CBRS_HIDE_INPLACE;
	}
	dwStyle |= CCS_NODIVIDER | CCS_NOPARENTALIGN;

	// クールバー初期化
	static BOOL bInit = FALSE;
	if (!bInit)
	{
		INITCOMMONCONTROLSEX iccex;
		iccex.dwSize = sizeof(INITCOMMONCONTROLSEX);
		iccex.dwICC = ICC_COOL_CLASSES;
		InitCommonControlsEx(&iccex);
		bInit = TRUE;
	}

	// クールバーウインドウの生成
	CRect rc;
	rc.SetRectEmpty();
	return CWnd::CreateEx(WS_EX_TOOLWINDOW, REBARCLASSNAME, NULL, dwStyle, rc, pParentWnd, nID);

	//m_dwStyle = dwStyle & CBRS_ALL; // save the control bar styles

	//// register and create the window - skip CControlBar::Create()
	//CString wndclass = ::AfxRegisterWndClass(CS_DBLCLKS,
	//	::LoadCursor(NULL, IDC_ARROW),
	//	::GetSysColorBrush(COLOR_BTNFACE), 0);

	////dwStyle &= ~CBRS_ALL; // keep only the generic window styles
	//dwStyle |= WS_CLIPCHILDREN; // prevents flashing
	//if (!CWnd::Create(wndclass, "REBARCLASSNAME", dwStyle,
	//	CRect(0, 0, 0, 0), pParentWnd, nID))
	//	return FALSE;
	//return TRUE;
}


/////////////////////////////////////////////////////////////////////
//  メッセージハンドラ
int CCoolBar::OnCreate(LPCREATESTRUCT lpcs)
{
	ASSERT(m_info || m_res_id);

	if (CControlBar::OnCreate(lpcs) != -1)
	{
		return OnCreateBands();
	}
	return -1;
}

void CCoolBar::OnPaint()
{
	Default();
}

void CCoolBar::OnHeigtChange(NMHDR* pNMHDR, LRESULT* pRes)
{
	CWnd* pParent = GetParent();
	CRect rcParent;
	pParent->GetWindowRect(&rcParent);
	pParent->PostMessage(WM_SIZE, 0, MAKELONG(rcParent.Width(), rcParent.Height()));
	*pRes = 0;
}

BOOL CCoolBar::OnEraseBkgnd(CDC* pDC)
{
	return (BOOL)Default();
}

/////////////////////////////////////////////////////////////////////
//  CControlBar オーバーライド関数
CSize CCoolBar::CalcFixedLayout(BOOL bStretch, BOOL bHorz)
{
	CRect rc;
	GetWindowRect(&rc);
	CSize sz(bHorz && bStretch ? 0x7FFF : rc.Width(), !bHorz && bStretch ? 0x7FFF : rc.Height());

	return sz;
}

CSize CCoolBar::CalcDynamicLayout(int nLength, DWORD dwMode)
{
	return CalcFixedLayout(dwMode & LM_STRETCH, dwMode & LM_HORZ);
}

void CCoolBar::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler)
{
	UpdateDialogControls(pTarget, bDisableIfNoHndler);
}



//-------------------------------------------------------------------
// ツールバークラス
//-------------------------------------------------------------------
/////////////////////////////////////////////////////////////////////
//  CCToolBar
IMPLEMENT_DYNAMIC(CCToolBar, CToolBar)
BEGIN_MESSAGE_MAP(CCToolBar, CToolBar)
	ON_WM_NCCREATE()
	ON_WM_NCPAINT()
	ON_WM_PAINT()
	ON_WM_NCCALCSIZE()
END_MESSAGE_MAP()
	//ON_MESSAGE(TB_AUTOSIZE, OnAutoSize)
	//ON_WM_SIZE()

CCToolBar::CCToolBar()
{
	//m_bDelayedButtonLayout = FALSE;
}
CCToolBar::~CCToolBar()
{
}

/////////////////////////////////////////////////////////////////////
//  メッセージハンドラ
BOOL CCToolBar::OnNcCreate(LPCREATESTRUCT lpcs)
{
	CFrameWnd* pFrame = GetParentFrame();
	ASSERT_VALID(pFrame);
	SetOwner(pFrame);
	return CToolBar::OnNcCreate(lpcs);
}

void CCToolBar::OnNcPaint()
{
	Default();
}

void CCToolBar::OnPaint()
{
	Default();
}

//-------------------------------------------------------------------
//
//-------------------------------------------------------------------
void CCToolBar::OnNcCalcSize(BOOL, NCCALCSIZE_PARAMS*)
{
	Default();
}



/////////////////////////////////////////////////////////////////////
//  CCoolBarCmdUI
/////////////////////////////////////////////////////////////////////
class CCoolBarCmdUI : public CCmdUI
{
public:
	virtual void Enable(BOOL bOn);
	virtual void SetCheck(int nCheck);
};


///////////////
// 有効／無効
///////////////
void CCoolBarCmdUI::Enable(BOOL bOn)
{
	m_bEnableChanged = TRUE;
	CToolBar* pToolBar = (CToolBar*)m_pOther;
	ASSERT(pToolBar != NULL);
	ASSERT_KINDOF(CToolBar, pToolBar);
	ASSERT(m_nIndex < m_nIndexMax);

	UINT nOldStyle = pToolBar->GetButtonStyle(m_nIndex);
	UINT nNewStyle = nOldStyle;

	// 無効時
	if (!bOn)
	{
		nNewStyle |= TBBS_DISABLED;
	}

	// 有効時
	else
	{
		nNewStyle &= ~TBBS_DISABLED;
	}

	ASSERT(!(nNewStyle & TBBS_SEPARATOR));

	// スタイルが変わったとき
	if (nNewStyle != nOldStyle)
	{
		pToolBar->SetButtonStyle(m_nIndex, nNewStyle);

		// 無効 → 有効になったときに再描画する。
		// いつでも再描画するようにしました。 1999/7/8
		//if (!(nNewStyle & TBBS_DISABLED))
			pToolBar->RedrawWindow();
	}
}

///////////////
// コマンドUI
///////////////
void CCToolBar::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler)
{
	CCoolBarCmdUI state;
	state.m_pOther = this;

	state.m_nIndexMax = (UINT)DefWindowProc(TB_BUTTONCOUNT, 0, 0);
	for (state.m_nIndex = 0; state.m_nIndex < state.m_nIndexMax; state.m_nIndex++)
	{
		// ボタン状態取得
		TBBUTTON button;
		VERIFY(DefWindowProc(TB_GETBUTTON, state.m_nIndex, (LPARAM)&button));

		// TBSTATE_ENABLED == TBBS_DISABLED so invert it
		button.fsState ^= TBSTATE_ENABLED;
		//button.fsState |= TBSTATE_ENABLED;

		state.m_nID = button.idCommand;

		// セパレータを作成の属性チェック
		if (!(button.fsStyle & TBSTYLE_SEP))
		{
			if (CWnd::OnCmdMsg(state.m_nID, CN_UPDATE_COMMAND_UI, &state, NULL))
			{
				continue;
			}
			state.DoUpdate(pTarget, bDisableIfNoHndler);
		}
	}
	// コントロール更新
	UpdateDialogControls(pTarget, bDisableIfNoHndler);
}

// Take your pick:
//#define MYTBBS_CHECKED TBBS_CHECKED			// use "checked" state
//#define MYTBBS_CHECKED TBBS_PRESSED			// use pressed state

//-------------------------------------------------------------------
//
//-------------------------------------------------------------------
void CCoolBarCmdUI::SetCheck(int nCheck)
{
	ASSERT(nCheck >= 0 && nCheck <= 2); // 0=>off, 1=>on, 2=>indeterminate
	CToolBar* pToolBar = (CToolBar*)m_pOther;
	ASSERT(pToolBar != NULL);
	ASSERT_KINDOF(CToolBar, pToolBar);
	ASSERT(m_nIndex < m_nIndexMax);

	UINT nOldStyle = pToolBar->GetButtonStyle(m_nIndex);
	UINT nNewStyle = nOldStyle & ~(TBBS_CHECKED | TBBS_INDETERMINATE);

	if (nCheck == 1)
	{
		nNewStyle |= TBBS_CHECKED;
	}
	else if (nCheck == 2)
	{
		nNewStyle |= TBBS_INDETERMINATE;
	}

	if (nNewStyle != nOldStyle)
	{
		ASSERT(!(nNewStyle & TBBS_SEPARATOR));
		pToolBar->SetButtonStyle(m_nIndex, nNewStyle);
		pToolBar->Invalidate();
	}
}

//-------------------------------------------------------------------
// Ｇｅｔ２１クールバー汎用クラス
//-------------------------------------------------------------------
////////////////////////////////////////////////////////////////
// CGet21CoolBar
IMPLEMENT_DYNAMIC(CGet21CoolBar, CCoolBar)

/////////////////////////////
// Ｇｅｔ２１クールバー生成
/////////////////////////////
BOOL CGet21CoolBar::Create(CWnd* pParentWnd, CGet21CoolbarItem* pInfo)
{
	m_info = pInfo;

	// Ｇｅｔ２１クールバー生成
	if (!CCoolBar::Create(pParentWnd, WS_CHILD|WS_VISIBLE | WS_BORDER | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | RBS_TOOLTIPS | RBS_BANDBORDERS | RBS_VARHEIGHT | CBRS_TOOLTIPS))
	{
		return FALSE;
	}

	return TRUE;
}


//////////
// 生成
//////////
BOOL CGet21CoolBar::Create(CWnd* pParentWnd, UINT nIDResource)
{
	m_res_id = nIDResource;

	// Ｇｅｔ２１クールバー生成
	if (!CCoolBar::Create(pParentWnd, WS_CHILD | WS_VISIBLE | WS_BORDER | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | RBS_TOOLTIPS | RBS_BANDBORDERS | RBS_VARHEIGHT | CBRS_TOOLTIPS))
	{
		return FALSE;
	}

	return TRUE;
}



///////////////////////////////////
// クールバー内へツールバーを生成
///////////////////////////////////
BOOL CGet21CoolBar::CreateToolbar(CCToolBar& tb, UINT nBitmapID)
{
	// ツールバー生成
	if (!tb.Create(this, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_TOOLTIPS | CBRS_SIZE_DYNAMIC | CBRS_FLYBY))
	{
		return FALSE;
	}

	// フォントセット
	tb.SetFont(&m_font, FALSE);

	// フラットボタンへ変更
	tb.ModifyStyle(0, TBSTYLE_FLAT);

	// m_infoをもとにボタンを追加。
	int i;

	CToolBarCtrl& ctrl = tb.GetToolBarCtrl();
	//LPGET21_COOLBAR_INFO p_info;
	GET21_COOLBAR_INFO info;

	TBBUTTON button;

	// メニューを取得しておく。 9/18
	CMenu* pMenu = NULL;
	if (GetParentOwner())
	{
		pMenu = GetParentOwner()->GetMenu();
	}
	int max, nButtonCnt;

	max = m_info->GetSize();
	nButtonCnt = -1;

	BOOL bIsSep = TRUE;			// 1個まえのボタンがセパレータですか?
	CDC* pDC = ctrl.GetDC();	// デバイスコンテキスト

	pDC->SelectObject(m_font);	// フォントをセットする

	int nSpacePixel;

	// スペース１個分のピクセル数を計算する
	if (pDC)
	{
		//ABC abc;
		CSize size;
		//if (pDC->GetCharABCWidths(0x20, 0x20, &abc))
		size = pDC->GetTextExtent(" ", 1);
		nSpacePixel = size.cx;
	}
	for (i = 0; i < max; i++)
	{
		int image;
		CString strHint;

		info = (*m_info)[i];

		// メニューに依存するボタンは，同コマンドIDがメニューに
		// 存在するかチェックする。なかったら追加しない。
		if (info.nBitmapID && info.bIsDependMenu && pMenu)
		{
			if (pMenu->GetMenuState(info.nCommandID, MF_BYCOMMAND) == (UINT)-1)
			{
				continue;
			}
		}

		// イメージを持ったボタン
		if (info.nBitmapID)
			image = ctrl.AddBitmap(1, info.nBitmapID);
		// セパレータ
		else
			image = NULL;

		if (image > -1)
		{
			button.iBitmap		= image;
			button.idCommand	= info.nCommandID;
			button.fsState		= TBSTATE_ENABLED;
			button.fsStyle		= (info.nBitmapID) ? TBSTYLE_BUTTON : TBSTYLE_SEP;
			button.iString		= NULL;

			// セパレータが連続している場合はカットする 9/18
			if (!info.nBitmapID && bIsSep)
				continue;

			if (ctrl.AddButtons(1, &button) == FALSE)
			{	// 追加失敗
				continue;
			}
			nButtonCnt++;
			bIsSep = !(info.nBitmapID);

			if (info.lpszTitle == NULL && info.nBitmapID)
			{
				CString str;

				// リソースから文字列を読み込む。
				if (str.LoadString(info.nCommandID))
				{
					int idx;

					// 一番最後のセクションをラベルにします。
					if ((idx = str.ReverseFind('\n')) != -1)
					{
						strHint = str.Mid(idx+1);
						info.lpszTitle = strHint.GetBuffer(STRING_WITDH + 1);
					}

					// 見つからなかったら表示しません。
					else
					{
						info.lpszTitle = GET21_REBAR_NOSTR;
					}
				}
				else
				{
					info.lpszTitle = GET21_REBAR_NOSTR;
				}
			}

			// 文字列設定
			if (info.lpszTitle)
			{
				CSize size(BUTTONSIZE_CX + 1,0);
				char szFmt[14];
				char szBuf[64];

				size = pDC->GetTextExtent(info.lpszTitle, lstrlen(info.lpszTitle));

				int nMaxCharPxcel = BUTTONSIZE_CX;

				if (size.cx < BUTTONSIZE_CX)
				{
					int space = (BUTTONSIZE_CX - size.cx) / 2;

					space = space / nSpacePixel;

					// "%6c%s%6cみたいなフォーマット作成
					if (space > 0)
					{
						wsprintf(szFmt, "%%%dc%%s%%%dc", space, space);
						wsprintf(szBuf, szFmt, 0x20, info.lpszTitle, 0x20);
					}

					// 調整せずに文字列をそのまま採用 (1999/6/30)
					else
						lstrcpy(szBuf, info.lpszTitle);

					size = pDC->GetTextExtent(szBuf, lstrlen(szBuf));

				}

				// BUTTONSIZE_CXになるように調整
				else
				{
					lstrcpy(szBuf, info.lpszTitle);
					do
					{
						int nLen = lstrlen(szBuf);

						szBuf[nLen-1] = '\0';

						size = pDC->GetTextExtent(szBuf, nLen);
					}
					while (size.cx > BUTTONSIZE_CX);
				}
				tb.SetButtonText(nButtonCnt, szBuf);
			}
		}
	}
	if (pDC)
		ReleaseDC(pDC);

	// クールバーへツールバーを追加
	InsertBandToolbar(tb, nBitmapID);

	// ツール チップスサポート
	tb.SetBarStyle(tb.GetBarStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);

	// ツールバーサイズ設定
	tb.SendMessage(TB_SETBITMAPSIZE, 0, MAKELONG(IMAGESIZE_CX, IMAGESIZE_CY));

	return TRUE;
}

//////////////////////////////////////////
// リソースを使ってクールバーを作成する。
//////////////////////////////////////////
BOOL CGet21CoolBar::CreateToolbar(CCToolBar& tb, UINT nIDResource, UINT nBitmapID)
{
	// ツールバー生成
	if (!tb.Create(this,
		WS_CHILD|WS_VISIBLE|WS_CLIPSIBLINGS|WS_CLIPCHILDREN|
			CBRS_TOOLTIPS|CBRS_SIZE_DYNAMIC|CBRS_FLYBY) ||
		 !tb.LoadToolBar(nIDResource))
	{
		return FALSE;
	}

	// フォントセット
	tb.SetFont(&m_font, FALSE);

	// フラットボタンへ変更
	tb.ModifyStyle(0, TBSTYLE_FLAT);

	// 文字列の指定
	CToolBarCtrl& ctrl = tb.GetToolBarCtrl();
	int i, max;
	TBBUTTON button;

	max = ctrl.GetButtonCount();

	CDC* pDC = GetDC();

	if (pDC == NULL)
	{
		return FALSE;
	}

	for (i = 0; i < max; i++)
	{
		ctrl.GetButton(i, &button);

		if (button.idCommand)
		{
			CString strHint;
			CString str;
			char* szTitle;

			// リソースから文字列を読み込む。
			if (str.LoadString(button.idCommand))
			{
				int idx;
				// 一番最後のセクションをラベルにします。
				if ((idx = str.ReverseFind('\n')) != -1)
				{
					strHint = str.Mid(idx+1);
					szTitle = strHint.GetBuffer(STRING_WITDH+1);
				}
				// 見つからなかったら表示しません。
				else
				{
					szTitle = GET21_REBAR_NOSTR;
				}
				// 文字列設定
				if (szTitle)
				{
					CSize size;
					char szFmt[12];
					char szBuf[STRING_WITDH+2];
					// 2002.01.10 Ver2.11 VC6対応 Start
					size = pDC->GetTextExtent(szTitle, lstrlen(szTitle));
					int space = (BUTTONSIZE_CX - size.cx) / 2;
//					size = pDC->GetTextExtent(szTitle, lstrlen(szTitle));
					// End
					space = (space + 7) / 8;
					// "%6c%s%6cみたいなフォーマット作成
					wsprintf(szFmt, "%%%dc%%s%%%dc", space, space);
					wsprintf(szBuf, szFmt, 0x20, szTitle, 0x20);
					tb.SetButtonText(i, szBuf);
				}
			}
		}
		else
		{
			tb.SetButtonText(i, GET21_REBAR_NOSTR);
		}
	}

	if (pDC)
		ReleaseDC(pDC);

	// クールバーへツールバーを追加
	InsertBandToolbar(tb, nBitmapID);

	// ツール チップスサポート
	tb.SetBarStyle(tb.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);

	return TRUE;
}

#ifndef RBBS_GRIPPERALWAYS
	#define		RBBS_GRIPPERALWAYS		0x00000080		// グリッパーを出す
#endif

//-------------------------------------------------------------------
// クールバーへツールバーを追加
//-------------------------------------------------------------------
BOOL CGet21CoolBar::InsertBandToolbar(CCToolBar& tb, UINT nBitmapID)
{
	// 幅最小サイズ取得
	CSize szHorz = tb.CalcDynamicLayout(-1, LM_HORZ);

	// バックグラウンドのビットマップ読み込み
	if (nBitmapID > 0)
	{
		VERIFY(m_bmBackground.LoadBitmap(nBitmapID));
	}

	// ツールバー追加
	CRebarBandInfo rbbi;
	rbbi.fMask = RBBIM_STYLE|RBBIM_CHILD|RBBIM_CHILDSIZE|
				 RBBIM_BACKGROUND|RBBIM_COLORS;

	rbbi.fStyle = RBBS_FIXEDBMP | RBBS_GRIPPERALWAYS; 	// グリッパーを出す。1998/8/7
	rbbi.hwndChild  = tb;
	rbbi.cxMinChild = szHorz.cx;

	rbbi.cyMinChild = BUTTONSIZE_CY - 1;

	rbbi.hbmBack = m_bmBackground;
	rbbi.clrFore = GetSysColor(COLOR_BTNTEXT);
	rbbi.clrBack = GetSysColor(COLOR_BTNFACE);

	if (!InsertBand(-1, &rbbi))
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CGet21CoolBar::OnCreateBands()
{
	// フォントの作成は、コンストラクタでやったほうが良いです。
	LOGFONT lf;

	CString strFontName;

	// フォント名取得 1999/5/25
	if (strFontName.LoadString(RFX_IDS_COOLBARFONTNAME) == FALSE)
	{
		strFontName = GET21_REBAR_FONTNAME;
	}

	// フォントを作成する
	memset(&lf, 0, sizeof(lf));
	lf.lfHeight			= -12;
	lf.lfWeight			= 400;
	lf.lfCharSet		= SHIFTJIS_CHARSET;
	lf.lfOutPrecision	= CLIP_CHARACTER_PRECIS;
	lf.lfClipPrecision	= CLIP_STROKE_PRECIS;
	lf.lfQuality		= DRAFT_QUALITY;
	lf.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
	lstrcpy(lf.lfFaceName, strFontName);
	m_font.CreateFontIndirect(&lf);

	// 動的に割り当てる
	if (m_info)
		return CreateToolbar(m_wndToolBar);

	// ツールバーリソースを使う
	else if (m_res_id)
		return CreateToolbar(m_wndToolBar, m_res_id, NULL);

	return FALSE;
}


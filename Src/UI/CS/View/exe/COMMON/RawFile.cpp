// RawFile.cpp: implementation of the CRawFile class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AllInfoPack.h"
#include "RawFile.h"
#include "..\UL2000\Project.h"
#include "..\UL2000\resource.h"
#include "..\UL2000\Mainfrm.h"
#include "..\UL2000\ParamsView.h"
#include "..\UL2000\ChildFrm.h"
#include "..\UL2000\UL2000.h"
#include "..\UL2000\\ServiceTableItem.h"
#include "IToolParam.h"
#include "BCGPXMLSettings.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRawFile::CRawFile()
{
	m_pKernel = NULL;
	m_fVersion = 0.0f;
	m_bFileIsOpen = FALSE;
	m_nDirection = SCROLL_UP;
	m_bOver = FALSE;
	m_nReadCount = 0;
	ZeroMemory(&m_DataHead, sizeof(DATAHEAD));
	m_pProject = NULL;
	m_bAutoLP = FALSE;
	m_dwHeadPos = 0;
	m_nFileIndex = 1;

	//
	ZeroMemory(&m_oleNowTime, sizeof(COleDateTime));
	ZeroMemory(&m_oleTimeST, sizeof(COleDateTime));
	ZeroMemory(&m_oleTimeED, sizeof(COleDateTime));
	m_bCheckTime = FALSE;
}

CRawFile::~CRawFile()
{
	ResetAllData();
	if (m_bFileIsOpen)
		m_File.Close();

	m_pProject = NULL;
}

void CRawFile::SetProject(CProject* pProject)
{
	m_pProject = pProject;
	m_pKernel = &m_pProject->m_ULKernel;
}

BOOL CRawFile::Open(CString strFilePathName)
{
	if (strFilePathName.IsEmpty())
		return FALSE;

	if (!m_File.Open(strFilePathName, CFile::modeWrite | CFile::modeCreate))
		return FALSE;

	m_bFileIsOpen = TRUE;
	m_strFilePathName = strFilePathName;
	return TRUE;
}

BOOL CRawFile::OpenFile(CString strFilePathName)
{
	if (strFilePathName.IsEmpty())
		return FALSE;

	if (!m_File.Open(strFilePathName, CFile::modeRead))
		return FALSE;

	m_bFileIsOpen = TRUE;
	m_strFilePathName = strFilePathName;
	
	return ReadHead();
}

void CRawFile::Close()
{
	if (!m_bFileIsOpen)
		return;

	m_File.Close();

	//更改文件名
	CString strTime;
	SYSTEMTIME time;
	GetLocalTime(&time);
	strTime.Format(_T("%02d-%02d-%02d-%02d-%02d-%02d"),time.wYear%100, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond);
	int nPos = m_strFilePathName.ReverseFind('.');
	CString strFileName = m_strFilePathName.Left(nPos);
	//查找文件名中是否包含创建时间
	CString strStarTime;
	nPos = strFileName.ReverseFind('(');
	if (nPos != -1)
	{
		strStarTime = strFileName.Mid(nPos+1, strFileName.GetLength()-nPos-2);
		strFileName = strFileName.Left(nPos);
		
	}
	CString strNewFileName;
	strNewFileName.Format("%s(%s~%s).raw",strFileName, strStarTime, strTime);
	CopyFile(m_strFilePathName , strNewFileName , FALSE);
	DeleteFile(m_strFilePathName);
	//完成文件名称更改

	m_bFileIsOpen = FALSE;
	m_strFilePathName.Empty();
	ResetAllData();
}

void CRawFile::ResetAllData()
{
/*	for (int i = 0; i < m_ToolList.GetSize(); i++)
	{
		TOOLR* pTool = (TOOLR*) m_ToolList.GetAt(i);
		pTool->m_ChannelList.RemoveAll();
		delete pTool;
	}
	m_ToolList.RemoveAll();
*/
	for (int i = 0; i < m_ToolInfoPackList.GetSize(); i++)
	{
		CToolInfoPack* pToolInfoPack = (CToolInfoPack*)
			m_ToolInfoPackList.GetAt(i);
		delete pToolInfoPack;
	}
	m_ToolInfoPackList.RemoveAll();
}

int CRawFile::GetToolIndex(CString strToolName)
{
	for (int i = 0; i < m_ToolInfoPackList.GetSize(); i++)
	{
		CToolInfoPack* pTIPack = (CToolInfoPack*)m_ToolInfoPackList.GetAt(i);
		TOOL* pTool = &pTIPack->m_Tool;
		if (strToolName == pTool->strToolName)
			return i;
	}

	return -1;
}

void CRawFile::SaveHead()
{
//	m_fVersion = 2.101f;
//	m_fVersion = 2.200f; // 添加保存仪器参数部分
//	m_fVersion = 2.301f; // 原始数据通道后保存深度、时间、深度索引
//	m_fVersion = 2.400f;	//添加保存示波器窗口位置信息
//	m_fVersion = 2.500f;	//添加保存序列配置信息
	m_fVersion = 2.510f;	//添加扩展数据头

	m_File.Write(&m_fVersion, sizeof(float));

	////////////////////Version>=2.4时   Add by xx/////////////////////////////////////
	//保存窗口位置信息
	//CBCGPRegistrySP::SetRuntimeClass(RUNTIME_CLASS(CBCGPXMLSettings));
	theApp.SetRegistryBase(_T("Settings"));
	CMainFrame* pMainFrame = (CMainFrame*) AfxGetMainWnd();
	theApp.SaveState(pMainFrame);
	// save settings:
	CString strSetting = _T(Gbl_AppPath + "\\Temp\\winpos.xml");
	CBCGPXMLSettings::WriteXMLToFile(FALSE, strSetting);
	
	DWORD dwSetLen = 0;
	CFile wpFile;
	if(wpFile.Open(strSetting, CFile::modeRead))
	{
		dwSetLen = wpFile.GetLength();
		BYTE* pBuf = new BYTE[dwSetLen];
		m_File.Write(&dwSetLen, sizeof(DWORD));
		wpFile.Read(pBuf, dwSetLen);
		m_File.Write(pBuf, dwSetLen);
		wpFile.Close();
		delete[]pBuf;
		DeleteFile(strSetting);
	}
	else
	{
		m_File.Write(&dwSetLen, sizeof(DWORD));
	}
	///////////////////////////////////End////////////////////////////////////////////

	//////////////////////////Version>=2.5时   Add by xx//////////////////////////////
	//保存序列配置信息
	CStringArray* pArrPath = &m_pProject->m_arrSerialConfigSrcPath;
	
	CArray<DWORD, DWORD> arrDataLen;	//数据长度集合
	CArray<BYTE*, BYTE*> arrData;	//数据集合
	CStringArray arrFileName;		//文件名集合
	//读取配置
	int si = 0;
	for(si = 0; si < pArrPath->GetSize(); si++)
	{
		CString strPath = pArrPath->GetAt(si);
		
		CString strName;
		int nPos = strPath.ReverseFind('\\');
		if (nPos > 0)
		{
			strName = strPath.Mid(nPos+1);
		}
		else
		{
			continue;
		}

		CFile sFile;
		DWORD dwDataLen = 0;
		if (sFile.Open(strPath, CFile::modeRead))
		{
			dwDataLen = sFile.GetLength();
			BYTE* pData = new BYTE[dwDataLen];
			sFile.Read(pData,dwDataLen);
			sFile.Close();
			
			arrFileName.Add(strName);
			arrDataLen.Add(dwDataLen);
			arrData.Add(pData);
		}
	}

	//写入Raw文件
	DWORD dwSerialLen = 0;
	for(si = 0; si < arrFileName.GetSize(); si++)
	{
		dwSerialLen += sizeof(DWORD)*2 +  arrFileName.GetAt(si).GetLength() + arrDataLen.GetAt(si);
	}
	m_File.Write(&dwSerialLen, sizeof(DWORD));	//配置总长度
	for(si = 0; si < arrFileName.GetSize(); si++)
	{
		CString strName = arrFileName.GetAt(si);
		BYTE* pData = arrData.GetAt(si);

		DWORD dwNameLen = strName.GetLength();
		DWORD dwOneLen = sizeof(DWORD) + dwNameLen + arrDataLen.GetAt(si);
		
		m_File.Write(&dwOneLen, sizeof(DWORD));		//一个配置长度
		
		m_File.Write(&dwNameLen, sizeof(DWORD));	//配置文件名长度
		m_File.Write(strName,strName.GetLength());	//配置文件名

		m_File.Write(pData, arrDataLen.GetAt(si));	//配置
	}

	//释放内存
	for (si = 0; si < arrData.GetSize(); si++)
	{
		BYTE* pData = arrData.GetAt(si);
		delete[] pData;
	}
	arrFileName.RemoveAll();
	arrDataLen.RemoveAll();
	arrData.RemoveAll();

	/////////////////////////////////////////////////////////////////////////////////
	m_pProject->SetRawFileToolInfoList();
	CXMLSettings xml(FALSE, "ToolInfoPacks");
	int nSize = m_ToolInfoPackList.GetSize();
	xml.Write("ToolInfoCount", nSize);
	int i = 0;
	for (i = 0; i < nSize; i++)
	{
		if (xml.CreateKey("ToolInfo%02d", i))
		{
			CToolInfoPack* pTIP = (CToolInfoPack*)m_ToolInfoPackList.GetAt(i);
			pTIP->Serialize(xml);
			xml.Back();
		}
	}
	xml.WriteXMLToFile(&m_File);
	m_pProject->GetToolInfo();
	m_File.Write(&m_pKernel->m_logInfo, sizeof(ULLogInfo));
	m_pProject->WriteToFile(&m_File, -1);
	
	/////////// >= 2.2 ///////add by bao 12/12/11///////////////
	CString strPath = Gbl_AppPath + "\\Temp";
	if (_tchdir(strPath))
	{
		_tmkdir(strPath);
	}
	CString strPath1 = strPath + "\\~ReplayParam.tmp";
	CString strFile = strPath + "\\~ReplayParamS.tmp";
	CFile files, file;
	if (files.Open(strFile, CFile::modeCreate | CFile::modeWrite))
	{
		int nTools = m_pKernel->m_arrTools.GetSize();
		files.Write(&nTools, sizeof(int));
		
		for (int i = 0; i < nTools; i++)
		{
			DWORD dwBytesRemaining = 0;
			UINT nBytesRead = 0;
			
			CULTool* pULTool = (CULTool*) m_pKernel->m_arrTools.GetAt(i);
			if (file.Open(strPath1, CFile::modeCreate | CFile::modeWrite))
			{
				if (pULTool->m_Params.WriteToFile(&file))
				{
					file.Close();
					if (file.Open(strPath1, CFile::modeRead))
					{
						dwBytesRemaining = file.GetLength();
						BYTE* pBuf = new BYTE[dwBytesRemaining];
						files.Write(&dwBytesRemaining, sizeof(DWORD));
						nBytesRead = file.Read(pBuf, dwBytesRemaining);
						files.Write(pBuf, nBytesRead);
						file.Close();
						delete[]pBuf;
						DeleteFile(strPath1);
					}
					else
					{
						files.Write(&dwBytesRemaining, sizeof(DWORD));
					}
					
				}
				else
				{
					files.Write(&dwBytesRemaining, sizeof(DWORD));
				}
			}
			else
			{
				files.Write(&dwBytesRemaining, sizeof(DWORD));
			}
		}
		files.Close();
	}
	
	DWORD dwLen = 0;
	if (files.Open(strFile, CFile::modeRead))
	{
		dwLen = files.GetLength();
		BYTE* pBYTE = new BYTE[dwLen];
		m_File.Write(&dwLen, sizeof(DWORD));
		files.Read(pBYTE, dwLen);
		m_File.Write(pBYTE, dwLen);
		files.Close();
		DeleteFile(strFile);
		delete[]pBYTE;
	}
	else
		m_File.Write(&dwLen, sizeof(DWORD));
	
	/////////////////////////////////////////

	for (i = 0; i < m_pKernel->m_arrTools.GetSize() ; i++)
	{
		CULTool* pTool = (CULTool*) m_pKernel->m_arrTools.GetAt(i);
		pTool->m_Params.SaveFirst();
		if (pTool->m_dwCalibrate)
			pTool->WriteToFile(&m_File, 4);
		else
		{
			DWORD dwCal = 0;
			m_File.Write(&dwCal, sizeof(DWORD));
		}
	}

	m_dwHeadPos = m_File.GetPosition();
}

BOOL CRawFile::ReadHead()
{
	m_fVersion = 2.101f;
	m_File.Read(&m_fVersion, sizeof(float));
	if (m_fVersion < 2.0f)
	{
		return FALSE;
	}

	///////////////////////////////////////////////////////////////
	//读取窗口位置信息	Add by xx 2016-07-19
	if(m_fVersion >= 2.4000f)
	{
		DWORD dwLen = 0;
		m_File.Read(&dwLen, sizeof(DWORD));
		if (dwLen > 0)
		{
			CString strSetting;
			strSetting = Gbl_AppPath + "\\Temp\\winpos.xml";
			CFile wpFile;
			if(wpFile.Open(strSetting, CFile::modeCreate | CFile::modeWrite))
			{
				BYTE* pBuf = new BYTE[dwLen];
				m_File.Read(pBuf, dwLen);
				wpFile.Write(pBuf, dwLen);
				wpFile.Close();
				delete pBuf;
			}
		}
	}
	//////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////
	//读取序列配置 Add by xx 2016-10-19
	if (m_fVersion >= 2.5000f)
	{
		m_pProject->m_arrRawSerialPath.RemoveAll();
		DWORD dwTotalLen = 0;
		m_File.Read(&dwTotalLen, sizeof(DWORD));
		while(dwTotalLen > sizeof(DWORD))
		{
			DWORD dwOneLen = 0;
			m_File.Read(&dwOneLen, sizeof(DWORD));
			dwTotalLen -= sizeof(DWORD);
			if (dwTotalLen >= dwOneLen)
			{
				BYTE* pOneSerial = new BYTE[dwOneLen];
				m_File.Read(pOneSerial, dwOneLen);
				dwTotalLen -= dwOneLen;
				
				DWORD dwNameLen = *(DWORD*)pOneSerial;
				CString strName((char*)&pOneSerial[sizeof(DWORD)], dwNameLen);
				BYTE* pData = &pOneSerial[sizeof(DWORD) + dwNameLen];
				DWORD dwDataLen = dwOneLen - sizeof(DWORD) - dwNameLen;

				CString strPath = Gbl_AppPath + "\\Temp\\" + strName;
				CFile fSerial;
				if (fSerial.Open(strPath, CFile::modeCreate | CFile::modeWrite))
				{
					fSerial.Write(pData, dwDataLen);
					fSerial.Close();
					//存配置路径至工程中
					m_pProject->m_arrRawSerialPath.Add(strPath);
				}
				delete[] pOneSerial;
				//memcpy(strName, pOneSerial[sizeof(DWORD)], dwNameLen);
				//strncpy((LPCTSTR)strName, pOneSerial[sizeof(DWORD)], dwNameLen);
			}
			else
			{
				break;
			}
		}
		if (dwTotalLen > 0)
		{
			m_File.Seek(dwTotalLen, CFile::current);
		}

	}

	//////////////////////////////////////////////////////////////////

	if (m_fVersion < 2.002f)
	{
		CArchive ar(&m_File, CArchive::load);
		int nSize = 0;
		ar >> nSize;
		int i = 0;
		for (i = 0; i < nSize; i++)
		{
			CToolInfoPack* pToolInfoPack = new CToolInfoPack;
			ReadObjClass(ar);
			pToolInfoPack->Serialize(ar);
			m_ToolInfoPackList.Add(pToolInfoPack);
		}	
		ar.Close();

		m_File.Read(&nSize, sizeof(int));
		for (i = 0; i < nSize; i++)
		{
			TOOLR* pTool = new TOOLR;
			m_File.Read(&pTool->m_ToolProp, sizeof(TOOLPROPERTY));
			CToolInfoPack* pTIPack = (CToolInfoPack*)m_ToolInfoPackList.GetAt(i);
			pTIPack->m_Tool.nBufferSize = pTool->m_ToolProp.nBufferSize;
			int nCount = 0;
			m_File.Read(&nCount, sizeof(nCount));
			for (int j = 0 ; j < nCount ; j++)
			{
				CHANNELR Channel;
				m_File.Read(&Channel, sizeof(CHANNELR));
				pTool->m_ChannelList.Add(Channel);
			}
			// m_ToolList.Add(pTool);
		}
	}
	else
	{
		CXMLSettings xml;
		if (xml.ReadXMLFromFile(&m_File))
		{
			int nSize = 0;
			xml.Read("ToolInfoCount", nSize);
			for (int i = 0; i < nSize; i++)
			{
				if (xml.Open("ToolInfo%02d", i))
				{
					CToolInfoPack* pTIP = new CToolInfoPack;
					pTIP->Serialize(xml);
					m_ToolInfoPackList.Add(pTIP);
					xml.Back();
				}
				
			}
		}
		else
			return FALSE;
	}
	
	m_File.Read(&m_pKernel->m_logInfo, sizeof(ULLogInfo));
	m_pProject->NewService();
	if (m_fVersion < 2.003f)
	{
		TRY
		{
			CArchive arProject(&m_File, CArchive::load);
			m_pProject->SerializeRaw(arProject);
			arProject.Flush();
			int nSize = 0;
			m_File.Read(&nSize, sizeof(int));
			WELLPROJECTINFO wpi;
			if (nSize <= sizeof(wpi))
			{
				m_File.Read(&wpi, nSize);
				m_pProject->GetInformations(&wpi);
			}
			else
				m_File.Seek(nSize, CFile::current);
		}
		CATCH(CFileException, e)
		{
			e->ReportError();
			e->Delete();
		}
		END_CATCH
	}
	else
		m_pProject->ReadFromFile(&m_File, -1);

	m_nDirection = m_pKernel->m_logInfo.nDirection;
	
	/////////// >= 2.2 ///////add by bao 12/12/11///////////////
	if (m_fVersion >= 2.2000) // 读取仪器参数数据
	{
		DWORD dwLen = 0;
		m_File.Read(&dwLen, sizeof(DWORD));
		if (dwLen > 0)
		{
			CString strPath = Gbl_AppPath + "\\Temp";
			if (_tchdir(strPath))
			{
				_tmkdir(strPath);
			}
			strPath = strPath + "\\~ReplayParams.tmp";
			CFile files;
			if (files.Open(strPath, CFile::modeCreate | CFile::modeWrite))
			{
				BYTE* pBuf = new BYTE[dwLen];
				m_File.Read(pBuf, dwLen);
				files.Write(pBuf, dwLen);
				files.Close();
				//DeleteFile(strPath); 由ReadParamInfo来删除
				delete [] pBuf;
			}
		}
		
	}
	///////////////////////////////////////////////////////////////

	
	// ReadDataHead();
	return TRUE;
}

void CRawFile::ReadWinPosInfo()
{
	CString strSetting = Gbl_AppPath + "\\Temp\\winpos.xml";
	if (m_fVersion >= 2.4000f)
	{
		CFile file;
		if(file.Open(strSetting, CFile::modeRead))
		{
			file.Close();
			CMainFrame* pMainFrame = (CMainFrame*) AfxGetMainWnd();
			pMainFrame->ReloadState(strSetting);
			DeleteFile(strSetting);
		}
	}
}

void CRawFile::ReadParamInfo()
{
	if (m_fVersion >= 2.2000) // 读取仪器参数数据
	{
		CString strPath = Gbl_AppPath + "\\Temp";
		if (_tchdir(strPath))
		{
			_tmkdir(strPath);
		}
		CString strPath1 = strPath + "\\~ReplayParam.tmp";
		CString strFile = strPath + "\\~ReplayParamS.tmp";
		CFile files, file;
		if (files.Open(strFile, CFile::modeRead))
		{
			int nTools = 0;
			files.Read(&nTools, sizeof(int));

			for (int i = 0; i < nTools; i++)
			{
				DWORD dwBytesRemaining = 0;
				files.Read(&dwBytesRemaining, sizeof(DWORD));
				BYTE* pBuf = new BYTE[dwBytesRemaining];
				files.Read(pBuf, dwBytesRemaining);
				if (file.Open(strPath1, CFile::modeCreate | CFile::modeWrite))
				{
					file.Write(pBuf, dwBytesRemaining);
					file.Close();
				}
				delete [] pBuf;

				if (i >= m_pKernel->m_arrTools.GetSize())
				{
					continue;
				}
				CULTool* pULTool = (CULTool*) m_pKernel->m_arrTools.GetAt(i);
				if (file.Open(strPath1, CFile::modeRead))
				{
					pULTool->m_Params.ReadFromFile(&file);
					file.Close();
				}
				DeleteFile(strPath1);
			}

			files.Close();
		}
		
		DeleteFile(strFile);
	}
}

void CRawFile::ReadToolCalInfo()
{
	int nSize = 0;
	if (m_fVersion >= 1.60f)
	{
		CString strCalFile = "Temp";
		for (int i = 0 ; i < m_pKernel->m_arrTools.GetSize() ; i++)
		{
			CULTool* pTool = (CULTool*) m_pKernel->m_arrTools.GetAt(i);
			//Add by zy 2012 4 12当仪器库未能成功加载时，不读取仪器库
			if(pTool->m_pITool != NULL)
				pTool->ReadFromFile(&m_File, 4);
		}
	}

	GetFirstToolName(FALSE);
}

void CRawFile::SaveDataHead(DATAHEAD* pDataHead, DATAHEADEX* pDataHeadEx, char* pMark)
{
	m_File.Write(pDataHead, sizeof(DATAHEAD));
	if (pDataHead->m_nMarkSize > 0 && pMark != NULL)
		m_File.Write(pMark, pDataHead->m_nMarkSize);
	SaveDataHeadEx(pDataHeadEx);	//调用保存扩展数据头
}

void CRawFile::SaveDataHeadEx(DATAHEADEX* pDataHeadEx)
{
	m_File.Write(pDataHeadEx, sizeof(DATAHEADEX));
}

void CRawFile::SaveData(int nIndex, CULTool* pTool, IOBuffer& data)
{
	if (pTool->strToolName.IsEmpty())
		return ;

	// int nIndex = GetToolIndex(strToolName);
	// TOOLR* pTool = (TOOLR*) m_ToolList.GetAt(nIndex);
	nIndex ++;
	m_File.Write(&nIndex, sizeof(nIndex));
	short sChannel = data.nChannelCount;
	m_File.Write(&sChannel, sizeof(short)); // 通道数
	m_File.Write(data.pChannelLength, sizeof(int) * sChannel);

	m_File.Write(data.pBuffer, *data.pLength);
	// m_File.Flush ();
}

void CRawFile::LoadParams(int& nIndex)
{
	if (m_bAutoLP)
	{
		int nTools = m_pKernel->m_arrTools.GetSize();
		while (nIndex < 1)
		{
			UINT nTool = (UINT)((-nIndex) - 1);
			if (nTool < nTools)
			{
				CULTool* pULTool = (CULTool*) m_pKernel->m_arrTools.GetAt(nTool);
				pULTool->m_Params.ReadFromFile(&m_File);
				if (pULTool->m_pIParam)
				{
					pULTool->m_pIParam->OnLoadParam();
				}
			}
			else
			{
				DWORD cbLen = 0;
				m_File.Read(&cbLen, sizeof(DWORD));
				m_File.Seek(cbLen, CFile::current);
			}
			
			if (m_File.Read(&nIndex, sizeof(int)) != sizeof(int))
				break;
		}	
	}
	else
	{
		while (nIndex < 1)
		{
			DWORD cbLen = 0;
			m_File.Read(&cbLen, sizeof(DWORD));
			m_File.Seek(cbLen, CFile::current);
			if (m_File.Read(&nIndex, sizeof(int)) != sizeof(int))
				break;
		}
	}
	
}

CString CRawFile::GetFirstToolName(BOOL bName /* = TRUE */)
{
	int nIndex = 0;

	if (m_dwParams.GetSize())
	{
		// Load initial params
		m_File.Seek(m_dwParams[0], CFile::begin);
		m_File.Read(&nIndex, sizeof(int));
		LoadParams(nIndex);
	}

	// Load first tool data
	m_File.Seek(m_dwPosST, CFile::begin);
	m_bOver = FALSE;
	ReadDataHead();
	return GetNextToolName();
}

int CRawFile::GetFirstToolIndex()
{
	int nIndex = 0;
	if (m_dwParams.GetSize())
	{
		// Load initial params
		m_File.Seek(m_dwParams[0], CFile::begin);
		m_File.Read(&nIndex, sizeof(int));
		LoadParams(nIndex);
	}

	// Load first tool data
	m_File.Seek(m_dwPosST, CFile::begin);
	m_bOver = FALSE;
	ReadDataHead();
	GetNextToolIndex(nIndex);
	return nIndex;
}

int CRawFile::GetNextToolIndex(int& nIndex)
{
	//TRACE("Raw Data Tool Count:%d Depth: %d\n", m_DataHead.m_nToolCount, m_DataHead.m_lDepth);
	if (m_nReadCount < m_DataHead.m_nToolCount)
	{
		m_File.Read(&nIndex, sizeof(int));
		LoadParams(nIndex);	
		nIndex--;       //  仪器索引

		if (nIndex < m_ToolInfoPackList.GetSize() && nIndex >= 0)
		{
			CToolInfoPack* pTIPack = (CToolInfoPack*) m_ToolInfoPackList.GetAt(nIndex);
			m_strNextToolName = pTIPack->m_Tool.strToolName;
			m_nReadCount++;
			return nIndex;
		}
		else
		{
			DWORD dw = m_File.GetPosition();
			//TRACE("%d", nIndex);
		}
	}

	DWORD dwPos = m_File.GetPosition();
	if ((m_File.GetPosition() > m_dwPosED) || ReadDataHead())
	{
		m_strNextToolName.Empty();
		return -1;
	}
	
	m_File.Read(&nIndex, sizeof(int));
	LoadParams(nIndex);
	nIndex--;
	if (nIndex < m_ToolInfoPackList.GetSize() && nIndex >= 0)
	{
		CToolInfoPack* pTIPack = (CToolInfoPack*) m_ToolInfoPackList.GetAt(nIndex);
		m_strNextToolName = pTIPack->m_Tool.strToolName;
		m_nReadCount++;
	}
	else
		nIndex = -1;
	return -2;
}

CString CRawFile::GetNextToolName()
{
	int nIndex = 0;
	if (m_nReadCount < m_DataHead.m_nToolCount)
	{
		m_File.Read(&nIndex, sizeof(int));
		LoadParams(nIndex);	
		nIndex--;

		if (nIndex < m_ToolInfoPackList.GetSize() && nIndex >= 0)
		{
			CToolInfoPack* pTIPack = (CToolInfoPack*) m_ToolInfoPackList.GetAt(nIndex);
			m_strNextToolName = pTIPack->m_Tool.strToolName;
			m_nReadCount++;
			return m_strNextToolName;
		}
		else
		{
			DWORD dw = m_File.GetPosition();
			//TRACE("%d", nIndex);
		}
	}

	if ((m_File.GetPosition() > m_dwPosED) || ReadDataHead())
	{
		m_strNextToolName.Empty();
		return m_strNextToolName;
	}
	
	m_File.Read(&nIndex, sizeof(int));
	LoadParams(nIndex);
	nIndex--;
	if (nIndex < m_ToolInfoPackList.GetSize() && nIndex >= 0)
	{
		CToolInfoPack* pTIPack = (CToolInfoPack*) m_ToolInfoPackList.GetAt(nIndex);
		m_strNextToolName = pTIPack->m_Tool.strToolName;
		m_nReadCount++;
	}
	else
		m_strNextToolName.Empty();

	return m_strNextToolName;
}

CString CRawFile::GetData(LPVOID pBuf)
{
	if (pBuf == NULL)
		return m_strNextToolName;

	int nIndex = GetToolIndex(m_strNextToolName);
	CToolInfoPack* pTIPack = (CToolInfoPack*)m_ToolInfoPackList.GetAt(nIndex);

	if (m_fVersion > 2.300f)
	{
		m_File.Read(pBuf, pTIPack->m_Tool.nBufferSize + sizeof(long) + sizeof(long) + sizeof(long));
	}
	else
		m_File.Read(pBuf, pTIPack->m_Tool.nBufferSize + sizeof(long));	
	return GetNextToolName();
}

int CRawFile::GetData(IOBuffer* pData, int& nIndex)
{
	CToolInfoPack* pTIPack = (CToolInfoPack*)m_ToolInfoPackList.GetAt(nIndex);
	BOOL bZeroLen = FALSE;
	//Add by xx 2016-11-08
	//检测时间
	BOOL bJump = FALSE;
	BOOL bFinsh = FALSE;
	if (m_fVersion >= 2.510f)
	{
		int nState = IsInTimeRange();
		if (nState == -1 || nState == 1)	//小于最小时间,跳过数据
		{	
			bJump = TRUE;
			short sCount = 0;
			m_File.Read(&sCount, sizeof(short));  // 通道数目

			int nLen = 0;
			for (int i = 0; i < sCount; i++)
			{
				int nCount = 0;
				m_File.Read(&nCount, sizeof(int));
				nLen += nCount;
			}
			
			nLen += sizeof(long) + sizeof(long) + sizeof(long);
			m_File.Seek(nLen, CFile::current);
		}
		if (nState == 1)
		{
			bFinsh = TRUE;
		}
	}

	if (!bJump)	//不检测
	{
		if (m_fVersion < 2.100)
		{
			int nLen = pTIPack->m_Tool.nBufferSize + sizeof(long);
			if (*pData->pLength != nLen)
			{
				delete pData->pBuffer;
				pData->pBuffer = new BYTE[nLen];
				*pData->pLength = nLen;
			}
			m_File.Read(pData->pBuffer, nLen);
		}
		else
		{
			short sCount = 0;
			m_File.Read(&sCount, sizeof(short));  // 通道数目
			BOOL bUpdate = FALSE;
			if (pData->nChannelCount != sCount)
			{
				pData->nChannelCount = sCount;
				
				if (pData->pChannelLength != NULL)
					delete pData->pChannelLength;
				
				pData->pChannelLength = NULL;
				if (sCount > 0)
					pData->pChannelLength = new int[sCount];
				bUpdate = TRUE;
			}
			
			int nLen = 0;
			for (int i = 0; i < sCount; i++)
			{
				m_File.Read(&pData->pChannelLength[i], sizeof(int));
				nLen += pData->pChannelLength[i];
			}
			
			
			if (nLen ==  0)
				bZeroLen = TRUE;
			nLen += sizeof(long);
			if (m_fVersion > 2.300f)
			{
				nLen += sizeof(long);
				nLen += sizeof(long);
			}
			if (bUpdate || *pData->pLength < nLen)
			{
				delete pData->pBuffer;
				pData->pBuffer = new BYTE[nLen];
			}
			*pData->pLength = nLen;
			m_File.Read(pData->pBuffer, nLen);
		}

	}

	int nResult = GetNextToolIndex(nIndex);

	if (bFinsh)	//直接结束回放
	{
		nResult = -4;
	}
	else if (bJump)	//未向仪器通道中存入数据
	{
		nResult = -100;	
	}

	if (bZeroLen)
	{
		if (nResult < 0)
			return (-3 + nResult);
		else
			return -3;
	}
	else
		return nResult;
}

long CRawFile::GetDepth()
{
	return m_DataHead.m_lDepth;
}

int CRawFile::GetDataSize()
{
	return m_DataHead.m_nDataSize;
}

int CRawFile::GetLogIOCount()
{
	return m_DataHead.m_nToolCount;
}

CString CRawFile::GetMark()
{
	return m_strMark;
}

BOOL CRawFile::ReadDataHead()
{
	ZeroMemory(&m_DataHead, sizeof(DATAHEAD));
	int nSize = m_File.Read(&m_DataHead, sizeof(DATAHEAD));
	if (nSize < sizeof(DATAHEAD))
	{
		m_strNextToolName.Empty();
		ZeroMemory(&m_DataHead, sizeof(DATAHEAD));
		m_bOver = TRUE;
	}
	else
	{
		if (m_DataHead.m_nMarkSize > 0 && m_DataHead.m_nMarkSize < 1024)
		{
			ZeroMemory(m_pBuf, 1024);
			m_File.Read(m_pBuf, m_DataHead.m_nMarkSize);
			m_strMark = m_pBuf;
		}
		else if (m_DataHead.m_nMarkSize > 1024)
			m_bOver = TRUE;
	}
	m_nReadCount = 0;

	//读取扩展数据头
	if (m_fVersion >= 2.510f)
	{
		DATAHEADEX DataHeadEx;
		ZeroMemory(&DataHeadEx, sizeof(DATAHEADEX));
		nSize = m_File.Read(&DataHeadEx, sizeof(DATAHEADEX));
		if (nSize != sizeof(DATAHEADEX))
		{
			m_strNextToolName.Empty();
			
			m_bOver = TRUE;
		}
		else
		{
			m_oleNowTime = DataHeadEx.m_oleTime;	//设置时间
		}
	}
	return m_bOver;
}

CToolInfoPack* CRawFile::GetToolInfoPack(CString strToolName)
{
	strToolName.MakeUpper();
	for (int i = 0; i < m_ToolInfoPackList.GetSize(); i++)
	{
		CToolInfoPack* pToolInfoPack = (CToolInfoPack*)	m_ToolInfoPackList.GetAt(i);
		CString strName = pToolInfoPack->m_Tool.strToolName;
		strName.MakeUpper();
		if (strName == strToolName || strName == "TL" + strToolName)
			return pToolInfoPack;
	}
	return NULL;
}

BOOL CRawFile::SetReadRange(BOOL bLimit /* = FALSE */, long lDepth0 /* = 0 */, 
	long lDepth1 /* = 0 */)
{
	GetDepthInfo();
	m_dwPosST = m_vecDepth.front().dwPosition;
	m_dwPosED = m_vecDepth.back().dwPosition;

	if (bLimit)
	{
		long lDepthST = min(lDepth0, lDepth1);  // 被限定的回放起始深度
		long lDepthED = max(lDepth0, lDepth1);  // 被限定的回放结束深度
		
		m_nDirection = (m_vecDepth.front().lDepth > m_vecDepth.back().lDepth) ? SCROLL_UP : SCROLL_DOWN;
		if (m_nDirection == SCROLL_UP)
		{
			lDepthST = lDepthED;
			lDepthED = min(lDepth0, lDepth1);
		}

		long lDelta = m_vecDepth.front().lDepth - lDepthST;
		int n0 = (lDelta < 0) ? -1 : ((lDelta == 0) ? 0 : 1);
		int nSize = m_vecDepth.size();
		int i = 0;
		for (i = 1; i < nSize; i++)
		{
			lDelta = m_vecDepth.at(i).lDepth - lDepthST;
			int n = (lDelta < 0) ? -1 : ((lDelta == 0) ? 0 : 1);
			if (n != n0)
			{
				m_dwPosST = m_vecDepth.at(i-1).dwPosition;
				break;
			}
		}
		
		lDelta = m_vecDepth.back().lDepth - lDepthED;
		n0 = (lDelta < 0) ? -1 : ((lDelta == 0) ? 0 : 1);
		for (i = nSize - 2; i > -1; i--)
		{
			lDelta = m_vecDepth.at(i).lDepth - lDepthED;
			int n = (lDelta < 0) ? -1 : ((lDelta == 0) ? 0 : 1);
			if (n != n0)
			{
				m_dwPosED = m_vecDepth.at(i+1).dwPosition;
				break;
			}
		}
	}

	//m_vecDepth.clear();
	if (m_dwParams.GetSize() < 1)
		return TRUE;

	DWORD dwPos = m_dwParams[0];
	for (int i = 1; i < m_dwParams.GetSize(); i++)
	{
		if (m_dwParams[i] > m_dwPosST)
		{
			dwPos = m_dwParams[i-1];
			break;
		}
	}
	
	m_dwParams.RemoveAll();
	m_dwParams.Add(dwPos);
	return TRUE;
}

void CRawFile::GetDepthInfo()
{
	DWORD dwPosition = m_File.GetPosition();
	for (int i = 0; i < m_ToolInfoPackList.GetSize(); i++)
	{
		DWORD dwCal = 0;
		m_File.Read(&dwCal, sizeof(DWORD));
		if (dwCal & 4)
		{
			DWORD cbLen = 0;
			m_File.Read(&cbLen, sizeof(DWORD));
			if (cbLen > 0)
				m_File.Seek(cbLen, CFile::current);
		}
	}

	int nTotalLen = m_File.GetLength();
	m_vecDepth.clear();
	m_dwParams.RemoveAll();
	m_arrTimes.RemoveAll();

	DEPTHINFO di;
	di.dwPosition = m_File.GetPosition();
	ReadDataHead();
	di.lDepth = m_DataHead.m_lDepth;
	m_vecDepth.push_back(di);
	while (GetNextDepth(&di))
	{
		m_vecDepth.push_back(di);
		if (m_oleNowTime.GetStatus() == COleDateTime::valid)
		{
			m_arrTimes.Add(m_oleNowTime);
		}
		
	}
	//设置起始终止时间
	if (m_arrTimes.GetSize())
	{
		m_oleTimeST = m_arrTimes.GetAt(0);
		m_oleTimeED = m_arrTimes.GetAt(m_arrTimes.GetSize() - 1);
	}

	m_File.Seek(dwPosition, CFile::begin);
	m_bOver = FALSE;
}

BOOL CRawFile::GetNextDepth(DEPTHINFO* pDI)
{
	int nIndex = 0;

	while (m_nReadCount < m_DataHead.m_nToolCount)
	{
		DWORD dwPos = m_File.GetPosition();
		m_File.Read(&nIndex, sizeof(int));

		if (nIndex < 0)
			m_dwParams.Add(dwPos);

		while (nIndex < 0)
		{
			DWORD cbLen = 0;
			m_File.Read(&cbLen, sizeof(DWORD));
			m_File.Seek(cbLen, CFile::current);
			if (m_File.Read(&nIndex, sizeof(int)) != sizeof(int))
				break;
		}

		nIndex--;
		if (nIndex < 0)
			return FALSE;

		if (nIndex < m_ToolInfoPackList.GetSize())
		{
			CToolInfoPack* pTIPack = (CToolInfoPack*)m_ToolInfoPackList.GetAt(nIndex);
			if (m_fVersion < 2.1f)
				m_File.Seek(pTIPack->m_Tool.nBufferSize + sizeof(long),	CFile::current);
			else
			{
				short sChannelCount = 0;
				m_File.Read(&sChannelCount, sizeof(short));
				int nOff = 0;
				for (int i = 0; i < sChannelCount; i++)
				{
					int nLen = 0;
					m_File.Read(&nLen, sizeof(int));
					nOff += nLen;
				}
				if (m_fVersion > 2.300f)
				{
					m_File.Seek(nOff + sizeof(long) + sizeof(long) + sizeof(long), CFile::current);
				}
				else
					m_File.Seek(nOff + sizeof(long), CFile::current);
			}
			m_nReadCount++;
		}
		else
			return FALSE;
	}

	pDI->dwPosition = m_File.GetPosition();
	
	if (ReadDataHead())
		return FALSE;

	pDI->lDepth = m_DataHead.m_lDepth;
	return TRUE;
}

BOOL CRawFile::NeedSaveNewFile()
{
	if(m_File.GetPosition() > 1024 * 1024 * 30/*100*1024*/)
	//if(m_File.GetPosition() > 1024 * 400/*100*1024*/)	//Test
		return TRUE;

	return FALSE;
}

void CRawFile::SaveNewFile()
{
	//当前时间
	CString strTime;
	SYSTEMTIME time;
	GetLocalTime(&time);
	strTime.Format(_T("%02d-%02d-%02d-%02d-%02d-%02d"),time.wYear%100,time.wMonth,time.wDay,time.wHour,time.wMinute, time.wSecond);
	
	int nPos = m_strFilePathName.ReverseFind('.');
	CString strFileName = m_strFilePathName.Left(nPos);

	//查找文件名中是否包含创建时间
	CString strStarTime;
	nPos = strFileName.ReverseFind('(');
	if (nPos != -1)
	{
		strStarTime = strFileName.Mid(nPos+1, strFileName.GetLength()-nPos-2);
		strFileName = strFileName.Left(nPos);
		
	}

	CString strNewFileName;
	//strNewFileName.Format("%s[%d].raw",strFileName , m_nFileIndex);
	strNewFileName.Format("%s(%s~%s).raw",strFileName, strStarTime, strTime);
	m_nFileIndex++;
	m_File.Close();
	CopyFile(m_strFilePathName , strNewFileName , FALSE);
	
	//Modify by xx 2016-01-11
	//原始文件文件名添加创建时间信息
	CString strNewPath;
	strNewPath.Format(_T("%s(%s).raw"), strFileName, strTime);
	m_File.Rename(m_strFilePathName, strNewPath);
	m_strFilePathName = strNewPath;
	m_File.Open(m_strFilePathName , CFile::modeWrite);
	m_File.SetLength(m_dwHeadPos);
}

//在范围内，返回0；大于最大时间，返回1；小于最小时间，返回-1；
int CRawFile::IsInTimeRange()
{
	if (!m_bCheckTime)	//不检测时间，返回0
	{
		return 0;
	}

	if (m_oleNowTime < m_oleTimeST)
	{
		return -1;
	}
	if (m_oleNowTime > m_oleTimeED)
	{
		return 1;
	}
	return 0;
}

void CRawFile::SetTimeRange(COleDateTime& time1, COleDateTime& time2, BOOL bLimit /* = FALSE */)
{
	if (m_arrTimes.GetSize() == 0)
	{
		time1 = m_oleTimeST;
		time2 = m_oleTimeED;
		return;
	}

	m_bCheckTime = bLimit;
	if (m_bCheckTime)
	{
		m_oleTimeST = m_arrTimes.GetAt(0);
		m_oleTimeED = m_arrTimes.GetAt(m_arrTimes.GetSize() - 1);
		if(time1 > time2)
		{
			time1 = time2;
		}
		if (time1 > m_oleTimeST && time1 < m_oleTimeED)
		{
			m_oleTimeST = time1;
		}
		else if (time1 >= m_oleTimeED)
		{
			m_oleTimeST = m_oleTimeED;
		}
		time1 = m_oleTimeST;

		if(time2 < m_oleTimeED && time2 > m_oleTimeST)
		{
			m_oleTimeED = time2;
		}
		else if (time2 <= m_oleTimeST)
		{
			m_oleTimeED = m_oleTimeST;
		}
		time2 = m_oleTimeED;
	}
	else
	{
			time1 = m_oleTimeST = m_arrTimes.GetAt(0);
			time2 = m_oleTimeED = m_arrTimes.GetAt(m_arrTimes.GetSize() - 1);
	}
}

BOOL CRawFile::CanSetTimeRange()
{
	return (m_fVersion >= 2.510f && m_arrTimes.GetSize() > 0)? TRUE : FALSE;
}

DWORD CRawFile::GetReplayLen()
{
	int nEndPos = 0;
	if (CanSetTimeRange())
	{	
		for (int i = m_arrTimes.GetSize() - 1; i >= 0; i--)
		{
			COleDateTime timeTemp = m_arrTimes.GetAt(i);
			if (timeTemp <= m_oleTimeED)
			{
				nEndPos = i;
				break;
			}
		}
		return (m_vecDepth.at(nEndPos).dwPosition);
	}
	return m_File.GetLength();
}

DWORD CRawFile::GetReplayHeadPos()
{
	int nStartPos = 0;
	for (int i = 0; i < m_arrTimes.GetSize(); i++)
	{
		COleDateTime timeTemp = m_arrTimes.GetAt(i);
		if (timeTemp >= m_oleTimeST)
		{
			nStartPos = i;
			break;
		}
	}
	return m_vecDepth.at(nStartPos).dwPosition;
}
// DataGrid.cpp : implementation file
// 从CGridCtrl类继承,重载了几个虚函数,以实现DataView的Drag&Drop功能

#include "stdafx.h"
#include "resource.h"
#include "DataGrid.h"
#include "GridCellCheck.h"
#include "GraphHeaderWnd.h"
#include "Curve.h"
#include "DataView.h"
#include "Units.h"
#include "ChildFrm.h"
#include "MainFrm.h"
#include "Sheet.h"
#include "DSF.h"
#include "Project.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


extern CUnits* g_units;
/////////////////////////////////////////////////////////////////////////////
// CDataGrid

CDataGrid::CDataGrid()  //主程序运行时创建
{
	m_bDragCurve = FALSE;
	m_pCurve = NULL;
}

CDataGrid::~CDataGrid() //主程序退出时析构
{
}


BEGIN_MESSAGE_MAP(CDataGrid, CGridCtrl)
	//{{AFX_MSG_MAP(CDataGrid)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		ON_WM_CONTEXTMENU()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CDataGrid message handlers

COleDataSource* CDataGrid::CopyTextFromGrid()
{
	USES_CONVERSION;

	CCellRange Selection = GetSelectedCellRange();
	if (!IsValid(Selection))
		return NULL;

	// Write to shared file (REMEBER: CF_TEXT is ANSI, not UNICODE, so we need to convert)
	CSharedFile sf(GMEM_MOVEABLE | GMEM_DDESHARE | GMEM_ZEROINIT);

	// Get a tab delimited string to copy to cache
	CString str;
	CGridCellBase* pCell;
	int row = 0;

	do // for (; row <= Selection.GetMaxRow(); row++)
	{
		// don't copy hidden cells
		if (m_arRowHeights[row] <= 0)
			continue;

		str.Empty();
		pCell = GetCell(row, 0);
		if (pCell)
		{
			str += pCell->GetText();
			if (Selection.GetMaxCol() > 0)
				str += _T("\t");
		}

		for (int col = Selection.GetMinCol();
			col <= Selection.GetMaxCol();
			col++)
		{
			// don't copy hidden cells
			if (m_arColWidths[col] <= 0)
				continue;

			pCell = GetCell(row, col);
			if (pCell)
			{
				if ((pCell->GetState() & GVIS_SELECTED) || (row == 0))
					str += pCell->GetText();
			}
			if (col != Selection.GetMaxCol())
				str += _T("\t");
		}
		if (row != Selection.GetMaxRow())
			str += _T("\r\n");

		sf.Write(T2A(str.GetBuffer(1)), str.GetLength());
		str.ReleaseBuffer();

		if (row == 0)
			row = Selection.GetMinRow();
		else
			row++;
	}
	while (row <= Selection.GetMaxRow());

	char c = '\0';
	sf.Write(&c, 1);

	DWORD dwLen = sf.GetLength();
	HGLOBAL hMem = sf.Detach();
	if (!hMem)
		return NULL;

	hMem = ::GlobalReAlloc(hMem, dwLen,
			GMEM_MOVEABLE | GMEM_DDESHARE | GMEM_ZEROINIT);
	if (!hMem)
		return NULL;

	// Cache data
	COleDataSource* pSource = new COleDataSource();
	pSource->CacheGlobalData(CF_TEXT, hMem);

	return pSource;
}

DROPEFFECT CDataGrid::OnDragOver(COleDataObject* pDataObject,
	DWORD dwKeyState, CPoint point)
{
	// 测井的时候不让拉	
#ifndef _LWD
	if (CDockablePane::m_nSlideSteps == 1)
		return DROPEFFECT_NONE;
#endif

	UINT nFormat = ::RegisterClipboardFormat(strAppTitle);
	HGLOBAL hData = pDataObject->GetGlobalData(nFormat);
	TCDRAGINFO* pTCDragInfo = (TCDRAGINFO*) ::GlobalLock(hData);	
	CCurve* pSrcCurve = pTCDragInfo->pSrcCurve;	// 数据来源曲线的指针
	BOOL bEnable = TRUE;
	if (pSrcCurve->m_PlotInfo.PlotENName.CompareNoCase("ImageUpDown") == 0 || pSrcCurve->m_PlotInfo.PlotENName.CompareNoCase("ImageBoundary") == 0
		|| pSrcCurve->m_PlotInfo.PlotENName.CompareNoCase("Image") == 0)
	{
		bEnable = FALSE;
	}

	::GlobalUnlock(hData);
	::GlobalFree(hData);
	if (!bEnable || pSrcCurve ==
		NULL ||
		pSrcCurve->m_pData ==
		NULL ||
		pSrcCurve->m_pData->m_pSrcCurve ==
		NULL/*||pSrcCurve->m_pData->m_pSrcCurve->m_nbType == NB_TOOL*/)
		return DROPEFFECT_NONE;

	if (hData != NULL)
	{
		m_bDragCurve = TRUE;
		return DROPEFFECT_COPY;
	}

	m_bDragCurve = FALSE;	
	return CGridCtrl::OnDragOver(pDataObject, dwKeyState, point);
}
/*
BOOL CDataGrid::OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect,
	CPoint point)
{
	if (m_bDragCurve)
	{
		CDataViewBar* pBar = (CDataViewBar*) GetParent();

		if (pBar != NULL)
		{
			if (!pBar->m_bRefresh)
				DeleteAllItems();
			pBar->m_bRefresh = TRUE;
			
#ifdef _LWD
			pBar->m_bShowCurve = TRUE;
#endif
		}
		UINT nFormat = ::RegisterClipboardFormat(strAppTitle);
		HGLOBAL hData = pDataObject->GetGlobalData(nFormat);

		if (hData == NULL)
			return FALSE;

		TCDRAGINFO* pTCDragInfo = (TCDRAGINFO*) ::GlobalLock(hData);
		if (pTCDragInfo->nTrackType > -1 || pTCDragInfo->pXMLNode)
			return FALSE;

		CCurve* pSrcCurve = pTCDragInfo->pSrcCurve;	// 数据来源曲线的指针
		CSheet* pSrcSheet = pTCDragInfo->pSrcSheet;
		//add by zjy 2014 12 15
		m_CurveArray.Add(pSrcCurve);
		::GlobalUnlock(hData);
		::GlobalFree(hData);

		int k = pSrcCurve->GetIndex12();
		m_pCurve = pSrcCurve;

		GV_ITEM item;		
		item.mask = GVIF_TEXT | GVIF_FORMAT | GVIF_PARAM;
		item.nFormat = DT_CENTER |
			DT_VCENTER |
			DT_SINGLELINE |
			DT_END_ELLIPSIS;
		CCurveData* pData = pSrcCurve->m_pData; // 曲线数据的指针
		int nRows = pData->GetSize() + 1;   	// 行数

		long iFrom = 0;
		long iEnd = nRows;

#define sz_most	20480
#define sz_more	4096

		CChildFrame* pChild = DYNAMIC_DOWNCAST(CChildFrame, ((CMainFrame*)AfxGetMainWnd())->GetActiveFrame());
		if (::IsWindow(pChild->GetSafeHwnd()) && pChild->m_pSheet && ((iEnd - iFrom) > sz_more))
		{
			iFrom = pChild->m_pSheet->m_lStartDepth;
			iEnd = pChild->m_pSheet->m_lEndDepth;
			if (pData->GetDepthRange(&iFrom, &iEnd))
			{
				iFrom = 0;
				iEnd = nRows;
			}
			else
				iEnd++;
		}

		// 先前没有拖放进曲线数据		
		if (GetColumnCount() == 0)
		{
			if ((iEnd - iFrom) > sz_most)
			{
				int nResult = AfxMessageBox(IDS_DATA_TOO_MANY, MB_YESNOCANCEL);
				if (nResult == IDCANCEL)
					return FALSE;
				
				if (nResult == IDYES)
				{
					iEnd = iFrom + sz_most;
				}
			}

			BOOL bDirection = pData->m_nDirection;
			DeleteAllItems();
			SetFixedColumnCount(1);
			SetFixedRowCount(1);
			SetColumnWidth(0, 80);	// 设置0列的宽度

			int lCols = GetColumnCount();	// 先前的列数

			
			nRows = iEnd - iFrom;

			int nCols = lCols + 1; // pData->m_nPointArray;

			if (nRows > sz_more)
			{
				nCols = lCols + 1;
			}
			
			SetColumnCount(nCols);

			int j;
			for (j = lCols; j < nCols; j++)
				SetColumnWidth(j, 100);	// 设置当前的列宽


			item.row = 0;
			item.col = 0;

			// add by bao 2013/7/9 时间模版拖拽的曲线以时间方式显示
			if (pSrcSheet != NULL && pSrcSheet->m_nDriveMode == UL_DRIVE_TIME)
			{
				item.strText.LoadString(IDS_TIMESYMBOL);
				item.strText += "(";
				item.strText += "s";
				item.strText += ")"; 
				SetItem(&item);	// 重新用SetColumnCount或SetRowCount先前设置好的单元文字不变

				CString strText = pSrcCurve->m_strName;
				if (pSrcCurve->m_pData)
					strText += "(" + pSrcCurve->m_pData->m_strUnit + ")";
				// AfxFormatString1(strText, IDS_CURVE_VALUE, pSrcCurve->m_strName);
				for (j = lCols; j < nCols; j++)
				{
					item.row = 0;
					item.col = j;
					if (nCols - lCols > 1)
						item.strText.Format("%s[%d]", strText, j);
					else
						item.strText.Format("%s", strText);
					SetItem(&item);	// 设置第一行的变量名
				}
				
				SetRowCount(nRows);
				nRows = iEnd + iFrom - 2;
				
				try
				{
					for (int i = iFrom; i < iEnd; i++)
					{
						item.row = i - iFrom + 1;
						item.col = 0;
						long lTime = 0;
						int iTime = i;
						lTime = pData->GetTime(iTime);
						if (lTime < 0)
						{
							continue;
						}
						
						double d = lTime;
						d = d / 1000.0; // 毫秒转变为秒
						
						// item.strText.Format("%d", lDepth);
						item.strText.Format("%10.3lf", d);
						item.lParam = lTime;
						SetItem(&item);	// 设置深度值
						
						
						for (j = lCols; j < nCols; j++)
						{
							// item.row = i;
							item.col = j;
							item.strText.Format("%13.3lf", pData->GetdblValue(iTime, k));	// 2维的情况,将0换成2维索引
							SetItem(&item);
							SetCellType(item.row, item.col, RUNTIME_CLASS(CGridCellCheck));
							CGridCellCheck *pCell = (CGridCellCheck *)GetCell(item.row, item.col);
							if (pCell)
							{
								pCell->SetCheck(!pData->GetDataDrawMode(iTime)) ;
							}
						}
					}
				}
				catch (CException* e)
				{
					e->ReportError();
					e->Delete();
					return FALSE;
				}
				
				return TRUE;
			}
			else
			{
				item.strText.LoadString(IDS_DEPTH);
				item.strText += "(";
				item.strText += g_units->DUnit();
				item.strText += ")"; 
				SetItem(&item);	// 重新用SetColumnCount或SetRowCount先前设置好的单元文字不变

				CString strText = pSrcCurve->m_strName;
				if (pSrcCurve->m_pData)
					strText += "(" + pSrcCurve->m_pData->m_strUnit + ")";
				// AfxFormatString1(strText, IDS_CURVE_VALUE, pSrcCurve->m_strName);
				for (j = lCols; j < nCols; j++)
				{
					item.row = 0;
					item.col = j;
					if (nCols - lCols > 1)
						item.strText.Format("%s[%d]", strText, j);
					else
						item.strText.Format("%s", strText);
					SetItem(&item);	// 设置第一行的变量名
				}
				
				SetRowCount(nRows);
				nRows = iEnd + iFrom - 2;
				
				//排序操作
				CArray<int , int> ValueIndex;
				CArray<long , long> ValueDepth;
				for(int i = iFrom ; i < iEnd ; i++)
				{
					long lDepth = pData->GetDepth(i);
					if(i == iFrom)//第一点
					{
						ValueIndex.Add(i);
						ValueDepth.Add(lDepth);
					}
					else
					{
						BOOL bAdd = FALSE;
						for(int j = 0 ; j < ValueIndex.GetSize() ; j++)
						{
							if(lDepth <= ValueDepth[j])
							{
								ValueIndex.InsertAt(j , i);
								ValueDepth.InsertAt(j , lDepth);
								
								bAdd = TRUE;
								break;
							}
						}

						if(!bAdd)
						{
							ValueIndex.Add(i);
							ValueDepth.Add(lDepth);
						}
					}
				}
				//
				try
				{
					IUnA* pUnA = new IUnA(g_units->GetUnA(_T("LMetric"), _T("Depth")));
					for (i = iFrom; i < iEnd; i++)
					{
						item.row = i - iFrom + 1;
						item.col = 0;
						long lDepth = 0;
						int iDepth = bDirection ? i : (nRows - i);
				//		lDepth = pData->GetDepth(iDepth);
						lDepth = ValueDepth[i - iFrom];
						if (lDepth == INVALID_DEPTH)
						{
							continue;
						}
						
						double d = lDepth;
						if (pUnA != NULL)
							d = pUnA->TX(lDepth);
						
						// item.strText.Format("%d", lDepth);
						item.strText.Format("%10.4lf", d);
					//	item.lParam = lDepth;
						item.lParam = ValueIndex[i - iFrom];
						SetItem(&item);	// 设置深度值
						
						
						for (j = lCols; j < nCols; j++)
						{
							// item.row = i;
							item.col = j;
						//	item.strText.Format("%13.4lf", pData->GetdblValue(iDepth, k));	// 2维的情况,将0换成2维索引
							item.strText.Format("%13.4lf", pData->GetdblValue(ValueIndex[i - iFrom], k));	// 2维的情况,将0换成2维索引
							SetItem(&item);
							SetCellType(item.row, item.col, RUNTIME_CLASS(CGridCellCheck));
							CGridCellCheck *pCell = (CGridCellCheck *)GetCell(item.row, item.col);
							if (pCell)
							{
								pCell->SetCheck(!pData->GetDataDrawMode(iDepth)) ;
							}
						}
					}
					delete pUnA;
				}
				catch (CException* e)
				{
					e->ReportError();
					e->Delete();
					return FALSE;
				}
				
				return TRUE;
			}
		}

		int lCols = GetColumnCount();	// 先前的列数
		int nCols = lCols + 1;	// 当前的列数 (改成多维的话,只要把1换成pData->m_nPointFrame)
		SetColumnCount(nCols);	// 只有与SetRowCount一起设置完的区域，SetItem 才有效

		int j;
		for (j = lCols; j < nCols; j++)
			SetColumnWidth(j, 100);	// 设置当前的列宽


		CString strText = pSrcCurve->m_strName;
		if (pSrcCurve->m_pData)
			strText += "(" + pSrcCurve->m_pData->m_strUnit + ")";
		// AfxFormatString1(strText, IDS_CURVE_VALUE, pSrcCurve->m_strName);
		for (j = lCols; j < nCols; j++)
		{
			item.row = 0;
			item.col = j;
			// if (nCols - lCols > 1)
			if (k > 0)
				item.strText.Format("%s[%d]", strText, k);
			else
				item.strText.Format("%s", strText);
			SetItem(&item);	// 设置第一行的变量名
		}

		// add by bao 2013/7/9 时间模版拖拽的曲线以时间方式显示
		CString strTemp;
		strTemp.LoadString(IDS_TIMESYMBOL);
		strTemp += "(";
		strTemp += "s";
		strTemp += ")"; 
		GV_ITEM item2;
		item2.row = 0;
		item2.col = 0;
		item2.mask = GVIF_TEXT; 
		GetItem(&item2);
		if (strTemp == item2.strText)
		{
			nRows = GetRowCount();
			for (int i = 1; i < nRows; i++)
			{
				long lTime = GetItemData(i, 0);
				
				int nIndex = pData->GetFirstIndexByTime(lTime, SCROLL_DOWN);
				if (nIndex < -1)
					break;
				if (nIndex < 0)
					continue;
				
				for (j = lCols; j < nCols; j++)
				{
					item.row = i;
					item.col = j;
					item.strText.Format("%13.4lf", pData->GetdblValue(nIndex));	// 2维的情况,将0换成2维索引
					SetItem(&item);
				}
			}
		}
		else
		{
			//排序操作
			CArray<int , int> ValueIndex;
			CArray<long , long> ValueDepth;
			for(int i = iFrom ; i < iEnd ; i++)
			{
				long lDepth = pData->GetDepth(i);
				if(i == iFrom)//第一点
				{
					ValueIndex.Add(i);
					ValueDepth.Add(lDepth);
				}
				else
				{
					BOOL bAdd = FALSE;
					for(int j = 0 ; j < ValueIndex.GetSize() ; j++)
					{
						if(lDepth <= ValueDepth[j])
						{
							ValueIndex.InsertAt(j , i);
							ValueDepth.InsertAt(j , lDepth);
							
							bAdd = TRUE;
							break;
						}
					}

					if(!bAdd)
					{
						ValueIndex.Add(i);
						ValueDepth.Add(lDepth);
					}
				}
			}
			
			//
			nRows = GetRowCount();
			CCurve* pDCurve = m_CurveArray[0];
			CCurveData* pDData = pDCurve->m_pData;
			long lDepthIndex = 0;
			long lDepth = 0;
			long nIndex = 0;

			for (i = 1; i < nRows; i++)
			{
				lDepthIndex = GetItemData(i, 0);
				lDepth = pDData->GetDepth(lDepthIndex);
				
			//	int nIndex = pData->GetFirstIndexByDepth(lDepth, -1);
			//	if (nIndex < 0)
			//		continue;
				while(ValueDepth[nIndex] < lDepth)	//找到第一个大于等于给定深度的数据点位置
				{
					nIndex++;
				}

				for (j = lCols; j < nCols; j++)
				{
					item.row = i;
					item.col = j;
				//	item.strText.Format("%13.4lf", pData->GetdblValue(nIndex));	// 2维的情况,将0换成2维索引
					item.strText.Format("%13.4lf", pData->GetdblValue(ValueIndex[nIndex]));	
					SetItem(&item);
				}
			}
		}

		return TRUE;
	}

	return CGridCtrl::OnDrop(pDataObject, dropEffect, point);
}*/

BOOL CDataGrid::OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect,
	CPoint point)
{
	if (m_bDragCurve)
	{
		if(m_CurveArray.GetSize() > 0)	//已有显示曲线
			return TRUE;

		CChildFrame* pChild = DYNAMIC_DOWNCAST(CChildFrame, ((CMainFrame*)AfxGetMainWnd())->GetActiveFrame());
		
		if(pChild->m_pDoc != NULL && pChild->m_pDoc->m_pProject->m_pService != NULL)	//实时测井绘图窗口不接受拖放曲线
			return TRUE;
		
		CDataViewBar* pBar = (CDataViewBar*) GetParent();

		if (pBar != NULL)
		{
			if (!pBar->m_bRefresh)
				DeleteAllItems();
			pBar->m_bRefresh = TRUE;
			
#ifdef _LWD
			pBar->m_bShowCurve = TRUE;
#endif
		}
		UINT nFormat = ::RegisterClipboardFormat(strAppTitle);
		HGLOBAL hData = pDataObject->GetGlobalData(nFormat);

		if (hData == NULL)
			return FALSE;

		TCDRAGINFO* pTCDragInfo = (TCDRAGINFO*) ::GlobalLock(hData);
		if (pTCDragInfo->nTrackType > -1 || pTCDragInfo->pXMLNode)
			return FALSE;

		CCurve* pSrcCurve = pTCDragInfo->pSrcCurve;	// 数据来源曲线的指针
		CSheet* pSrcSheet = pTCDragInfo->pSrcSheet;
		//add by zjy 2014 12 15
		m_CurveArray.Add(pSrcCurve);
		::GlobalUnlock(hData);
		::GlobalFree(hData);

		int k = pSrcCurve->GetIndex12();
		m_pCurve = pSrcCurve;

		GV_ITEM item;		
		item.mask = GVIF_TEXT | GVIF_FORMAT | GVIF_PARAM;
		item.nFormat = DT_CENTER |
			DT_VCENTER |
			DT_SINGLELINE |
			DT_END_ELLIPSIS;
		CCurveData* pData = pSrcCurve->m_pData; // 曲线数据的指针
		int nRows = pData->GetSize() + 1;   	// 行数

		long iFrom = 0;
		long iEnd = nRows;
/*
#define sz_most	409600
#define sz_more	4096
*/
#define sz_most	409600
#define sz_more	409600
		
		if (::IsWindow(pChild->GetSafeHwnd()) && pChild->m_pSheet && ((iEnd - iFrom) > sz_more))
		{
			iFrom = pChild->m_pSheet->m_lStartDepth;
			iEnd = pChild->m_pSheet->m_lEndDepth;
			if (pData->GetDepthRange(&iFrom, &iEnd))
			{
				iFrom = 0;
				iEnd = nRows;
			}
			else
				iEnd++;
		}

		// 先前没有拖放进曲线数据		
		if ((iEnd - iFrom) > sz_most)
		{
			int nResult = AfxMessageBox(IDS_DATA_TOO_MANY, MB_YESNOCANCEL);
			if (nResult == IDCANCEL)
				return FALSE;
			
			if (nResult == IDYES)
			{
				iEnd = iFrom + sz_most;
			}
		}

		BOOL bDirection = pData->m_nDirection;
		DeleteAllItems();
		SetFixedColumnCount(1);
		SetFixedRowCount(1);
		SetColumnWidth(0, 80);	// 设置0列的宽度

		int lCols = GetColumnCount();	// 先前的列数

		
		nRows = iEnd - iFrom;

		int nCols = lCols + 3; // pData->m_nPointArray;

		if (nRows > sz_more)
		{
		//	nCols = lCols + 1;
			nRows = nRows + 1;
		}
		
		SetColumnCount(nCols);

		int j;
		for (j = lCols; j < nCols; j++)
			SetColumnWidth(j, 100);	// 设置当前的列宽


		item.row = 0;
		item.col = 0;

		// add by bao 2013/7/9 时间模版拖拽的曲线以时间方式显示
		if (pSrcSheet != NULL && pSrcSheet->m_nDriveMode == UL_DRIVE_TIME)
		{
			item.strText.LoadString(IDS_TIMESYMBOL);
			item.strText += "(";
			item.strText += "s";
			item.strText += ")"; 
			SetItem(&item);	// 重新用SetColumnCount或SetRowCount先前设置好的单元文字不变

			CString strText = pSrcCurve->m_strName;
			if (pSrcCurve->m_pData)
				strText += "(" + pSrcCurve->m_pData->m_strUnit + ")";
			// AfxFormatString1(strText, IDS_CURVE_VALUE, pSrcCurve->m_strName);
			for (j = lCols; j < nCols; j++)
			{
				item.row = 0;
				item.col = j;
				if (nCols - lCols > 1)
					item.strText.Format("%s[%d]", strText, j);
				else
					item.strText.Format("%s", strText);
				SetItem(&item);	// 设置第一行的变量名
			}
			
			SetRowCount(nRows);
			nRows = iEnd + iFrom - 2;
			
			try
			{
				for (int i = iFrom; i < iEnd; i++)
				{
					item.row = i - iFrom + 1;
					item.col = 0;
					long lTime = 0;
					int iTime = i;
					lTime = pData->GetTime(iTime);
					if (lTime < 0)
					{
						continue;
					}
					
					double d = lTime;
					d = d / 1000.0; // 毫秒转变为秒
					
					// item.strText.Format("%d", lDepth);
					item.strText.Format("%10.3lf", d);
					item.lParam = lTime;
					SetItem(&item);	// 设置深度值
					
					
					for (j = lCols; j < nCols; j++)
					{
						// item.row = i;
						item.col = j;
						item.strText.Format("%13.3lf", pData->GetdblValue(iTime, k));	// 2维的情况,将0换成2维索引
						SetItem(&item);
						if(item.row >= 0 && item.col >= 0)
							SetCellType(item.row, item.col, RUNTIME_CLASS(CGridCellCheck));
						CGridCellCheck *pCell = (CGridCellCheck *)GetCell(item.row, item.col);
						if (pCell)
						{
//							pCell->SetCheck(!pData->GetDataDrawMode(iTime)) ;

							DWORD dwMode = pData->GetDataMode(iTime);
							pCell->SetCheck(!(dwMode & CURVE_DATAMODE_BAD));
						}
					}
				}
			}
			catch (CException* e)
			{
				e->ReportError();
				e->Delete();
				return FALSE;
			}
			
			return TRUE;
		}
		else
		{
			item.strText.LoadString(IDS_DEPTH);
			item.strText += "(";
			//item.strText += g_units->DUnit();
			item.strText += AfxGetUnits()->DUnit();
			item.strText += ")"; 
			SetItem(&item);	// 重新用SetColumnCount或SetRowCount先前设置好的单元文字不变

			CString strText = pSrcCurve->m_strName;
			if (pSrcCurve->m_pData)
				strText += "(" + pSrcCurve->m_strLRUnit + ")";
			
			item.row = 0;
			item.col = 1;
			item.strText.Format("%s", strText);
			SetItem(&item);	// 设置第一行的变量名
			
			item.row = 0;
			item.col = 2;
			item.strText = "Source";
			SetItem(&item);

			item.row = 0;
			item.col = 3;
			item.strText = "Run";
			SetItem(&item);

			SetRowCount(nRows);
			nRows = iEnd + iFrom - 2;
			
			//排序操作
			CArray<int , int> ValueIndex;
			CArray<long , long> ValueDepth;
			int i = 0;
			for(i = iFrom ; i < iEnd ; i++)
			{
				long lDepth = pData->GetDepth(i);
				lDepth = AfxGetUnits()->GetXValue(lDepth, _T("LogGraph"), _T("Depth"));
				if(i == iFrom)//第一点
				{
					ValueIndex.Add(i);
					ValueDepth.Add(lDepth);
				}
				else
				{
					BOOL bAdd = FALSE;
					for(int j = 0 ; j < ValueIndex.GetSize() ; j++)
					{
						if(lDepth <= ValueDepth[j])
						{
							ValueIndex.InsertAt(j , i);
							ValueDepth.InsertAt(j , lDepth);
							
							bAdd = TRUE;
							break;
						}
					}

					if(!bAdd)
					{
						ValueIndex.Add(i);
						ValueDepth.Add(lDepth);
					}
				}
			}
			//
			try
			{
//				IUnA* pUnA = new IUnA(g_units->GetUnA(_T("LMetric"), _T("Depth")));
				for (int a = 0;a<ValueDepth.GetSize();a++)
				{
				 	if (ValueDepth[a] < 0)
				 	{
						ValueDepth.RemoveAt(a);
						ValueIndex.RemoveAt(a);
				 		iEnd = iEnd - 1;
				 	}
 				}
				for (i = iFrom; i < iEnd; i++)
				{
					item.row = i - iFrom + 1;
					item.col = 0;
					long lDepth = 0;
					int iDepth = bDirection ? i : (nRows - i);
			//		lDepth = pData->GetDepth(iDepth);
					lDepth = ValueDepth[i - iFrom];
					if (lDepth == INVALID_DEPTH)
					{
						continue;
					}
					
//					double d = lDepth;
//					if (pUnA != NULL)
//						d = pUnA->TX(lDepth);
					
					// item.strText.Format("%d", lDepth);
					item.strText.Format("%10.4lf", DTM(lDepth));
				//	item.lParam = lDepth;
					item.lParam = ValueIndex[i - iFrom];
					SetItem(&item);	// 设置深度值
					DWORD dwMode = pData->GetDataMode(item.lParam);

					item.col = 1;
					item.strText.Format("%13.4lf", pData->GetdblValue(ValueIndex[i - iFrom], k));	// 2维的情况,将0换成2维索引
					SetItem(&item);
					SetCellType(item.row, item.col, RUNTIME_CLASS(CGridCellCheck));
					CGridCellCheck *pCell = (CGridCellCheck *)GetCell(item.row, item.col);
					
					
					if (pCell)
					{
//						pCell->SetCheck(!pData->GetDataDrawMode(iDepth)) ;
						pCell->SetCheck(!(dwMode & CURVE_DATAMODE_BAD));
					}

					DWORD dwSource = dwMode & 0x00FF0000;
					item.col = 2;
					if(dwSource == CURVE_DATAMODE_SOURCE_MEM)
						item.strText = "Memory";
					else if(dwSource == CURVE_DATAMODE_SOURCE_MUD)
						item.strText = "Decode";
					SetItem(&item);

					
					DWORD dwNum = dwMode & 0x0000FF00;
					dwNum = dwNum / 256;
					item.col = 3;
					item.strText.Format("%d" , dwNum);
					if (dwNum >= 0)
					{
						char RunNo[50];
						int nNum = dwMode & 0x0000FF00;
						nNum = nNum >> 8;
						if (g_pMainWnd->m_pIDB != NULL)
						{
							g_pMainWnd->m_pIDB->GetGuid(nNum, RunNo);
							CString strRunName = "";
							for (int i = 0; i < g_pMainWnd->m_vecAllRun.size(); i++)
							{
								if (_tcscmp(g_pMainWnd->m_vecAllRun[i].szRunID, RunNo) == 0)
								{
									strRunName = g_pMainWnd->m_vecAllRun[i].szRunName;
								}
							}
							item.strText.Format("%s" , strRunName);
						}
						SetItemData(i, 2, dwNum);
					}
					SetItem(&item);

				}
			//	delete pUnA;
			}
			catch (CException* e)
			{
				e->ReportError();
				e->Delete();
				return FALSE;
			}
			
			return TRUE;
		}
		
	}

	return CGridCtrl::OnDrop(pDataObject, dropEffect, point);
}

BOOL CDataGrid::OnCommand(WPARAM wParam, LPARAM lParam)
{
	return TRUE;
	TRACE("CDataGrid::OnCommand\n");//add by Jasmine 20181130
	if ((wParam >> 16) == BN_CLICKED)
	{
		if (!m_pCurve)
		{
			return FALSE;
		}
		
		CChildFrame* pChild = DYNAMIC_DOWNCAST(CChildFrame, ((CMainFrame*)AfxGetMainWnd())->GetActiveFrame());
		if (!pChild)
		{
			return FALSE;
		}
		CCellID cellID = GetFocusCell();
		
		CGridCellBase *pCell = GetCell(cellID.row, cellID.col);
		if (!pCell)
		{
			return FALSE;
		}
		if (pCell->IsKindOf(RUNTIME_CLASS(CGridCellCheck)))
		{
			CGridCellCheck *pCellCheck = (CGridCellCheck *)pCell;
			if (pCellCheck)
			{
				BOOL bCheck = pCellCheck->GetCheck();
				int nIndex = pCellCheck->GetData();
			//	m_pCurve->SetDataModeEx(nIndex, !bCheck, 1);
				DWORD dwMode = m_pCurve->GetDataMode(nIndex);
				if(!bCheck) //不显示置1
				{
					dwMode = dwMode | CURVE_DATAMODE_BAD;
					m_pCurve->SetDataMode(nIndex , dwMode);
				}
				else //正常显示置0
				{
					DWORD dwMask = CURVE_DATAMODE_BAD;
					dwMask = ~dwMask;
					dwMode = dwMode & dwMask;
					m_pCurve->SetDataMode(nIndex , dwMode);
				}
#ifdef _DATA_BASE
				if(g_pMainWnd->m_pIDB != NULL)
				{
					RTDataInfo RTData;
					ZeroMemory(&RTData , sizeof(RTDataInfo));
					CString strName = m_pCurve->m_strData;
					memcpy(RTData.CurveName , strName , strName.GetLength());
					long lDepth = m_pCurve->GetDepth(nIndex);
					long lTime = m_pCurve->GetTime(nIndex);

					RTData.Depth = lDepth;
					RTData.Bad = !bCheck;
				
					int nNum = dwMode & 0x0000FF00;
					nNum = nNum >> 8;
					g_pMainWnd->m_pIDB->GetGuid(nNum , RTData.RunNo);
					
					CTime ct((time_t)lTime);
					ct.GetAsSystemTime(RTData.Time);

//					g_pMainWnd->m_pIDB->SetMTData(DB_DATA_MASK_BAD , &RTData);
					g_pMainWnd->m_pIDB->SetCurveValue(DB_DATA_MASK_BAD , &RTData);
				}
#endif
			}
		}
		pChild->Invalidate();
	}
	return CGridCtrl::OnCommand(wParam, lParam);
}

void CDataGrid::OnCheckAll(BOOL bCheck)
{
	CCellRange cellRange = GetSelectedCellRange();
	int nMinRow = cellRange.GetMinRow();
	int nMaxRow = cellRange.GetMaxRow();
	int nMinCol = cellRange.GetMinCol();
	int nMaxCol = cellRange.GetMaxCol();
	
	CChildFrame* pChild = DYNAMIC_DOWNCAST(CChildFrame, ((CMainFrame*)AfxGetMainWnd())->GetActiveFrame());
	if (!pChild)
	{
		return;
	}
	for (int iRow = nMinRow; iRow <= nMaxRow; iRow++)
	{
		CGridCellBase *pCell = GetCell(iRow, 1);
		if (!pCell)
		{
			continue;
		}
		if (pCell->IsKindOf(RUNTIME_CLASS(CGridCellCheck)))
		{
			CGridCellCheck *pCellCheck = (CGridCellCheck *)pCell;
			if (pCellCheck)
			{
				pCellCheck->SetCheck(bCheck);
				int nIndex = 0;
				if (!pChild->m_pSheet)
				{
					m_pCurve->GetFirstIndexByDepth(pCellCheck->GetData());
				}
				else 
				{
					if(pChild->m_pSheet->m_nDriveMode == UL_DRIVE_TIME)
					{
						nIndex = m_pCurve->GetFirstIndexByTime(pCellCheck->GetData());
					}
					else
					{
						nIndex = m_pCurve->GetFirstIndexByDepth(pCellCheck->GetData());
					}
				}
//				m_pCurve->SetDataModeEx(nIndex, !bCheck, 1);
				DWORD dwMode = m_pCurve->GetDataMode(nIndex);
				if(!bCheck)
				{
					dwMode = dwMode | CURVE_DATAMODE_BAD;
					m_pCurve->SetDataMode(nIndex , dwMode);
				}
				else
				{
					DWORD dwMask = CURVE_DATAMODE_BAD;
					dwMask = !dwMask;
					dwMode = dwMode | dwMask;
					m_pCurve->SetDataMode(nIndex , dwMode);
				}
			}
		}
	}
	pChild->Invalidate();
}

void CDataGrid::GetSelectGridState(BOOL &bHave, BOOL &bCheckAll)
{
	CCellRange cellRange = GetSelectedCellRange();
	int nMinRow = cellRange.GetMinRow();
	int nMaxRow = cellRange.GetMaxRow();
	int nMinCol = cellRange.GetMinCol();
	int nMaxCol = cellRange.GetMaxCol();
	
	
	bCheckAll = TRUE;
	bHave = FALSE;
	if (!m_pCurve)
	{
		return;
	}
	if (nMinCol <= 1 && nMaxCol >= 1)
	{
		nMinRow = nMaxRow? nMaxRow:1;
		for (int iRow = nMinRow; iRow <= nMaxRow && bCheckAll; iRow++)
		{
			CGridCellBase *pCell = GetCell(iRow, 1);
			if (!pCell)
			{
				continue;
			}
			if (pCell->IsKindOf(RUNTIME_CLASS(CGridCellCheck)))
			{
				bHave = TRUE;
				if (!((CGridCellCheck *)pCell)->GetCheck())
				{
					bCheckAll = FALSE;
				}
			}
		}
	}
	else
	{
		bCheckAll = FALSE;
	}
}

void CDataGrid::OnContextMenu(CWnd* pWnd, CPoint point)
{
// 	CWnd *pOwner = GetOwner();
//     if (pOwner && IsWindow(pOwner->m_hWnd))
// 	{
// 		pOwner->SendMessage(WM_CONTEXTMENU, point.x, point.y);
// 	}

 	CWnd::OnContextMenu(pWnd, point);
}
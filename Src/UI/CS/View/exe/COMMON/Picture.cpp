// CPicture.cpp: implementation of the CPicture class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "PreviewDialog.h"
#include "Picture.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

#ifdef ERROR_TITLE

#include <shlwapi.h>
#pragma comment(lib, "Shlwapi.lib")

CPicture::CPicture()
{
	hGlobal = NULL;
}

//-------------注意这里的释放---------------
CPicture::~CPicture()
{
}

//-----------------------------------------------------------------------------
// Does:   Free The Allocated Memory That Holdes The IPicture Interface Data
// ~~~~    And Clear Picture Information
//
// Note:   This Might Also Be Useful If U Only Need To Show The Picture Once
// ~~~~~   Or If U Copy The Picture To The Device Context, So It Can Still
//  	   Remain On Screen - But IPicture Data Is Not Needed No More
//
//-----------------------------------------------------------------------------
void CPicture::FreePicture()
	//=============================================================================
{
	if (m_pPict != NULL)
	{
		m_pPict->Release();
		m_pPict = NULL;
	}
}
//-----------------------------------------------------------------------------
// Does:   Open a Resource And Load It Into IPicture (Interface)
// ~~~~    (.BMP .DIB .EMF .GIF .ICO .JPG .WMF)
//
// Note:   When Adding a Bitmap Resource It Would Automatically Show On "Bitmap"
// ~~~~    This NOT Good Coz We Need To Load It From a Custom Resource "BMP"
//  	   To Add a Custom Rresource: Import Resource -> Open As -> Custom
//  	   (Both .BMP And .DIB Should Be Found Under "BMP")
//
// InPut:  ResourceName - As a UINT Defined (Example: IDR_PICTURE_RESOURCE)
// ~~~~~   ResourceType - Type Name (Example: "JPG")
//
// OutPut: TRUE If Succeeded...
// ~~~~~~
//-----------------------------------------------------------------------------
BOOL CPicture::Load(HINSTANCE hInstance, LPCTSTR lpszResourceName,
	LPCSTR ResourceType)
	//=============================================================================
{
	HGLOBAL hGlobal = NULL;
	HRSRC hSource = NULL;
	LPVOID lpVoid = NULL;
	int nSize = 0;
	BOOL bResult = FALSE;
	if (m_pPict != NULL)
		FreePicture(); // Important - Avoid Leaks...

	hSource = FindResource(hInstance, lpszResourceName, ResourceType);

	if (hSource == NULL)
	{
		HWND hWnd = AfxGetApp()->GetMainWnd()->m_hWnd;
		MessageBoxEx(hWnd, "FindResource() Failed\t", ERROR_TITLE,
			MB_OK | MB_ICONSTOP, LANG_ENGLISH);
		return(FALSE);
	}

	hGlobal = LoadResource(hInstance, hSource);
	if (hGlobal == NULL)
	{
		HWND hWnd = AfxGetApp()->GetMainWnd()->m_hWnd;
		MessageBoxEx(hWnd, "LoadResource() Failed\t", ERROR_TITLE,
			MB_OK | MB_ICONSTOP, LANG_ENGLISH);
		return(FALSE);
	}

	lpVoid = LockResource(hGlobal);
	if (lpVoid == NULL)
	{
		HWND hWnd = AfxGetApp()->GetMainWnd()->m_hWnd;
		MessageBoxEx(hWnd, "LockResource() Failed\t", ERROR_TITLE,
			MB_OK | MB_ICONSTOP, LANG_ENGLISH);
		return(FALSE);
	}

	nSize = (UINT) SizeofResource(hInstance, hSource);
	if (LoadPictureData((BYTE *) hGlobal, nSize))
		bResult = TRUE;

	UnlockResource(hGlobal); // 16Bit Windows Needs This
	FreeResource(hGlobal); // 16Bit Windows Needs This (32Bit - Automatic Release)
	return(bResult);
}

//-----------------------------------------------------------------------------
// Does:   Open a File And Load It Into IPicture (Interface)
// ~~~~    (.BMP .DIB .EMF .GIF .ICO .JPG .WMF)
//
// InPut:  sFilePathName - Path And FileName Target To Save
// ~~~~~  
//
// OutPut: TRUE If Succeeded...
// ~~~~~~
//-----------------------------------------------------------------------------
BOOL CPicture::LoadPicture(CString sFilePathName)
	//=============================================================================
{
	if (!PathFileExists(sFilePathName))
		return FALSE;
	BOOL bResult = FALSE;
	CFile PictureFile;
	CFileException e;
	int nSize = 0;

	if (m_pPict != NULL)
		FreePicture(); // Important - Avoid Leaks...

	if (PictureFile.Open(sFilePathName, CFile::modeRead | CFile::typeBinary,
						&e))
	{
		nSize = PictureFile.GetLength();
		BYTE* pBuffer = new BYTE[nSize];

		if (PictureFile.Read(pBuffer, nSize) > 0)
		{
			if (LoadPictureData(pBuffer, nSize))
				bResult = TRUE;
		}

		PictureFile.Close();
		delete[] pBuffer;
	}
	else // Open Failed...
	{
		TCHAR szCause[255];
		e.GetErrorMessage(szCause, 255, NULL);
		HWND hWnd = AfxGetApp()->GetMainWnd()->m_hWnd;
		MessageBoxEx(hWnd, szCause, ERROR_TITLE, MB_OK | MB_ICONSTOP,
			LANG_ENGLISH);
		bResult = FALSE;
	}
	return(bResult);
}

//-----------------------------------------------------------------------------
// Does:   Read The Picture Data From a Source (File / Resource)
// ~~~~    And Load It Into The Current IPicture Object In Use
//
// InPut:  Buffer Of Data Source (File / Resource) And Its Size
// ~~~~~  
//
// OutPut: Feed The IPicture Object With The Picture Data
// ~~~~~~  (Use Draw Functions To Show It On a Device Context)
//  	   TRUE If Succeeded...
//-----------------------------------------------------------------------------
BOOL CPicture::LoadPictureData(BYTE* pBuffer, int nSize)
	//=============================================================================
{
	BOOL bResult = FALSE;
	HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE, nSize);

	if (hGlobal == NULL)
	{
		HWND hWnd = AfxGetApp()->GetMainWnd()->m_hWnd;
		MessageBoxEx(hWnd, "Can not allocate enough memory\t", ERROR_TITLE,
			MB_OK | MB_ICONSTOP, LANG_ENGLISH);
		return(FALSE);
	}

	void* pData = GlobalLock(hGlobal);
	memcpy(pData, pBuffer, nSize);
	GlobalUnlock(hGlobal);

	IStream* pStream = NULL;

	if (CreateStreamOnHGlobal(hGlobal, TRUE, &pStream) == S_OK)
	{
		HRESULT hr;
		if ((hr = OleLoadPicture(pStream, nSize, FALSE, IID_IPicture,
					(LPVOID *) &m_pPict)) ==
			E_NOINTERFACE)
		{
			HWND hWnd = AfxGetApp()->GetMainWnd()->m_hWnd;
			MessageBoxEx(hWnd, "IPicture interface is not supported\t",
				ERROR_TITLE, MB_OK | MB_ICONSTOP, LANG_ENGLISH);
			return(FALSE);
		}
		else // S_OK
		{
			pStream->Release();
			pStream = NULL;
			bResult = TRUE;
		}
	}

	FreeResource(hGlobal); // 16Bit Windows Needs This (32Bit - Automatic Release)

	return(bResult);
}

//-----------------------------------------------------------------------------
// Does:   Draw The Loaded Picture Direct To The Client DC
// ~~~~
//
// Note:   Bigger OR Smaller Dimentions Than The Original Picture Size
// ~~~~    Will Draw The Picture Streached To Its New Given NEW Dimentions...
//
// InPut:  pDC - Given DC To Draw On
// ~~~~~   pSrcRect- Dimentions Of The Picture To Draw From(As a Rectangle)
//   DrawRect - Dimentions Of The Picture To Draw To(As a Rectangle)
// OutPut: TRUE If Succeeded...
// ~~~~~~
//-----------------------------------------------------------------------------
//=============================================================================

void CPicture::Render(CDC* pDC, LPRECT pDrawRect, LPRECT pSrcRect/*=NULL*/,
	LPCRECT prcWBounds/*=NULL*/)
{
	if (pDC == NULL || m_pPict == NULL)
		return ;

	CRect recrDest(pDrawRect);

	long Width = 0;
	long Height = 0;
	m_pPict->get_Width(&Width);
	m_pPict->get_Height(&Height);

	CRect SrcRect(0, 0, Width, Height);

	if (pSrcRect)
	{
		SrcRect = *pSrcRect;
	}
	CRect DrawRect(pDrawRect);
	HRESULT hrP = NULL;

	hrP = m_pPict->Render(pDC->m_hDC, DrawRect.left,				  // Left
					  DrawRect.top, 				  // Top
					  DrawRect.Width(), // Right
					  DrawRect.Height(), // Bottom
					  SrcRect.left, SrcRect.top, SrcRect.Width(),
					SrcRect.Height(), prcWBounds);

	if (SUCCEEDED(hrP))
		return;
	AfxThrowMemoryException();
	return;
}

//-----------------------------------------------------------------------------
// Does:   Saves The Picture That Is Stored In The IPicture Object As a Bitmap
// ~~~~    (Converts From Any Known Picture Type To a Bitmap / Icon File)
//
// InPut:  sFilePathName - Path And FileName Target To Save
// ~~~~~
//
// OutPut: TRUE If Succeeded...
// ~~~~~~
//-----------------------------------------------------------------------------
BOOL CPicture::SaveAsBitmap(CString sFilePathName)
	//=============================================================================
{
	BOOL bResult = FALSE;
	ILockBytes* Buffer = 0;
	IStorage* pStorage = 0;
	IStream* FileStream = 0;
	BYTE* BufferBytes;
	STATSTG BytesStatistics;
	DWORD OutData;
	long OutStream;
	CFile BitmapFile; CFileException e;
	double SkipFloat = 0;
	DWORD ByteSkip = 0;
	_ULARGE_INTEGER RealData;

	CreateILockBytesOnHGlobal(NULL, TRUE, &Buffer); // Create ILockBytes Buffer

	HRESULT hr = ::StgCreateDocfileOnILockBytes(Buffer,
					STGM_SHARE_EXCLUSIVE | STGM_CREATE | STGM_READWRITE, 0,
					&pStorage);

	hr = pStorage->CreateStream(L"PICTURE",
					STGM_SHARE_EXCLUSIVE | STGM_CREATE | STGM_READWRITE, 0, 0,
					&FileStream);

	m_pPict->SaveAsFile(FileStream, TRUE, &OutStream); // Copy Data Stream
	FileStream->Release();
	pStorage->Release();
	Buffer->Flush();

	// Get Statistics For Final Size Of Byte Array
	Buffer->Stat(&BytesStatistics, STATFLAG_NONAME);

	// Cut UnNeeded Data Coming From SaveAsFile() (Leave Only "Pure" Picture Data)
	SkipFloat = (double(OutStream) / 512); // Must Be In a 512 Blocks...
	if (SkipFloat > DWORD(SkipFloat))
		ByteSkip = (DWORD) SkipFloat + 1;
	else
		ByteSkip = (DWORD) SkipFloat;
	ByteSkip = ByteSkip * 512; // Must Be In a 512 Blocks...

	// Find Difference Between The Two Values
	ByteSkip = (DWORD) (BytesStatistics.cbSize.QuadPart - ByteSkip);

	// Allocate Only The "Pure" Picture Data
	RealData.LowPart = 0;
	RealData.HighPart = 0;
	RealData.QuadPart = ByteSkip;
	BufferBytes = (BYTE *) malloc(OutStream);
	if (BufferBytes == NULL)
	{
		Buffer->Release();
		HWND hWnd = AfxGetApp()->GetMainWnd()->m_hWnd;
		MessageBoxEx(hWnd, "Can not allocate enough memory\t", ERROR_TITLE,
			MB_OK | MB_ICONSTOP, LANG_ENGLISH);
	}

	Buffer->ReadAt(RealData, BufferBytes, OutStream, &OutData);

	if (BitmapFile.Open(sFilePathName,
					CFile::typeBinary | CFile::modeCreate | CFile::modeWrite,
					&e))
	{
		BitmapFile.Write(BufferBytes, OutData);
		BitmapFile.Close();
		bResult = TRUE;
	}
	else // Write File Failed...
	{
		TCHAR szCause[255];
		e.GetErrorMessage(szCause, 255, NULL);
		HWND hWnd = AfxGetApp()->GetMainWnd()->m_hWnd;
		MessageBoxEx(hWnd, szCause, ERROR_TITLE, MB_OK | MB_ICONSTOP,
			LANG_ENGLISH);
		bResult = FALSE;
	}

	Buffer->Release();
	free(BufferBytes);

	return(bResult);
}

LONG CPicture::get_Height()
{
	LONG nHeight = 0;

	if (m_pPict != NULL)
	{
		m_pPict->get_Height(&nHeight);
	}

	return nHeight;
}
LONG CPicture::get_Width()
{
	LONG nWidth = 0;

	if (m_pPict != NULL)
	{
		m_pPict->get_Width(&nWidth);
	}

	return nWidth;
}

#else

//----------------CFileProcess class------------------------------------
//constructor
CFileProcess::CFileProcess()
{
	m_File = NULL;  	   //File *m_File;
}

//destructor !
CFileProcess::~CFileProcess()
{
	//	this->Close();
}

//opens a file either for reading or writing
//打开一个文件用于读写
BOOL CFileProcess::Open(LPCTSTR FileName, FILE_OPENMODE Open_Mode)
{
	if (_tcslen(FileName) == 0)
		return FALSE;

	switch (Open_Mode)
	{
	case OFM_READ:
		m_File = _tfopen(FileName, _T("rb"));
		break;
	case OFM_WRITE:
		m_File = _tfopen(FileName, _T("wb"));
		break;
	}

	//ASSERT(m_File != NULL);
	if (!m_File)
		return FALSE;
	return TRUE;
}


//Reads data from the file stream.
//从文件流中读取数据
BOOL CFileProcess::Read(VOID* zBuffer, DWORD cSize)
{
	//从m_File中读取cSize 个BYTE类型的数据存储到zBuffer中。
	DWORD dwFreadReslut = fread(zBuffer, sizeof(BYTE), cSize, m_File);
	if (dwFreadReslut < cSize)
		return FALSE;
	return TRUE;
}

//Writes data to the file stream.
//写数据到文件流
BOOL CFileProcess::Write(VOID* zBuffer, DWORD cSize)
{
	//fwrite :write size*count byte to buffer.
	if (fwrite(zBuffer, sizeof(BYTE), cSize, m_File) < cSize)
		return FALSE;
	return TRUE;
}

//Gets the File Size
//得到文件大小
LONG CFileProcess::GetSize(VOID)
{
	//ftell:一般用于读入数据后，文件的指针位置
	long t_location = ftell(m_File);
	//moves the file pointer to  a specified location 
	fseek(this->m_File, 0, SEEK_END);

	long f_size = ftell(this->m_File);
	fseek(this->m_File, t_location, SEEK_SET);
	return f_size;
}

//Closes the File
VOID CFileProcess::Close(VOID)
{
	if (m_File)
	{
		fclose(this->m_File);
		this->m_File = NULL;
	}
	return;
}

//-----------------------
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPicture::CPicture()
{
	m_pPict = NULL;
	hGlobal = NULL;
}

//-------------注意这里的释放---------------
CPicture::~CPicture()
{
	this->FreePicture();
}

/*
OUR MAIN FUNCTION
*/
HBITMAP CPicture::LoadPicture(LPCTSTR FileName)
{
	CFileProcess cFile;  //the image file

	//if we couldn't open the image file then we should get out of here
	if (!cFile.Open(FileName, OFM_READ)) //打开要读的文件
		return NULL;

	FreePicture();

	//we must know exactly the buffer size to allocate
	//in order to read the image in it
	//so get the image file size
	long nSize = cFile.GetSize(); //得到我们要分配内存的大小，即文件的大小
	//allocate enough memory to hold the image
	//it must be allocated using GlobalAlloc
	//otherwise it will fail...try using new or malloc and you'll see!!!
	hGlobal = GlobalAlloc(GMEM_MOVEABLE, nSize);

	//get pointer to first byte
	void* pData = GlobalLock(hGlobal);

	//read the file in the prev allocated memory
	if (!cFile.Read(pData, nSize)) //读nSize个字节到已经分配的内存区域
		return NULL;;
	//we don't need the file any more so close it
	cFile.Close();
	GlobalUnlock(hGlobal);  //释放内存区域

	IStream* pStream = NULL;
	//ole routines....
	//creates a stream from our handle
	//don't know why and how?
	if (CreateStreamOnHGlobal(hGlobal, TRUE, &pStream) != S_OK)
		return NULL;

	HRESULT hr;
	//aha..now let ole reads the image and load it for us
	//param1:指向包含图片数据流的指针
	//param2:从流中读取nSize字节的大小，
	//param3:Reference to the identifier of the interface 
	// describing the type of interface pointer to return
	//param4: Address of output variable that receives interface  pointer requested in riid
	hr = OleLoadPicture(pStream, nSize, FALSE, IID_IPicture,
			(LPVOID *) &this->m_pPict);
	if (hr != S_OK)
		return NULL;

	//	if ((hr = OleLoadPicture(pStream, nSize, FALSE, IID_IPicture, (LPVOID *)&this->m_pPict)) != S_OK)
	//		return NULL;
	HBITMAP hbmp = NULL;
	//return an HBITMAP to our image like the LoadImage function does
	//we might need the handle
	this->m_pPict->get_Handle((OLE_HANDLE *) &hbmp);
	return hbmp;
}


//an overloaded version of the above to draw the picture
//as well in a given DC
HBITMAP CPicture::LoadPicture(LPCTSTR FileName, HDC hdc)
{
	HBITMAP hbmp = this->LoadPicture(FileName);
	this->DrawPicture(hdc, 0, 0, this->_GetWidth(), _GetHeight());
	return hbmp;
}

//returns the current DC of the loaded image
HDC CPicture::_GetDC()
{
	if (!m_pPict)
		return NULL;
	HDC pDC ;
	m_pPict->get_CurDC(&pDC);
	return  pDC;
}

//returns the Handle of the loaded image(HBITMAP)
HBITMAP CPicture::_GetHandle()
{
	if (!m_pPict)
		return NULL; 

	HBITMAP hbmp;
	m_pPict->get_Handle((OLE_HANDLE *) &hbmp);
	return  hbmp;
}

//Cleans up
void CPicture::FreePicture()
{
	if (m_pPict)
	{
		m_pPict->Release();
		m_pPict = NULL;
	}

	if (hGlobal)
	{
		GlobalFree(hGlobal);
		hGlobal = NULL;
	}
}

#endif

HBITMAP CPicture::Load(DWORD dwLen)
{
	IStream* pStream = NULL;
	//ole routines....
	//creates a stream from our handle
	//don't know why and how?
	if (CreateStreamOnHGlobal(hGlobal, TRUE, &pStream) != S_OK)
		return NULL;

	HRESULT hr;
	//aha..now let ole reads the image and load it for us
	//param1:指向包含图片数据流的指针
	//param2:从流中读取nSize字节的大小，
	//param3:Reference to the identifier of the interface 
	// describing the type of interface pointer to return
	//param4: Address of output variable that receives interface  pointer requested in riid
	hr = OleLoadPicture(pStream, dwLen, FALSE, IID_IPicture,
			(LPVOID *) &this->m_pPict);

	if (hr != S_OK)
		return NULL;

	//	if ((hr = OleLoadPicture(pStream, nSize, FALSE, IID_IPicture, (LPVOID *)&this->m_pPict)) != S_OK)
	//		return NULL;
	HBITMAP hbmp = NULL;
	//return an HBITMAP to our image like the LoadImage function does
	//we might need the handle
	this->m_pPict->get_Handle((OLE_HANDLE *) &hbmp);
	return hbmp;
}

HBITMAP CPicture::LoadPicture(HBITMAP hBitmap)
{
	CBitmap bitmap;
	bitmap.Attach(hBitmap);
	ASSERT(bitmap.GetSafeHandle());

	// Convert the bitmap to a DIB
	BITMAP bm;
	BITMAPINFOHEADER bi;
	LPBITMAPINFOHEADER lpbi;
	DWORD dwLen;
	HANDLE hDIB;
	HANDLE handle;

	LPBITMAPFILEHEADER lphdr;

	HDC hDC;
	HPALETTE hPal = (HPALETTE) GetStockObject(DEFAULT_PALETTE);

	// Get bitmap information
	bitmap.GetObject(sizeof(bm), (LPSTR) & bm);

	// Initialize the bitmapinfoheader
	bi.biSize = sizeof(BITMAPINFOHEADER);
	bi.biWidth = bm.bmWidth;
	bi.biHeight = bm.bmHeight;
	bi.biPlanes = 1;
	bi.biBitCount = bm.bmPlanes * bm.bmBitsPixel;
	bi.biCompression = BI_RGB;
	bi.biSizeImage = 0;
	bi.biXPelsPerMeter = 0;
	bi.biYPelsPerMeter = 0;
	bi.biClrUsed = 0;
	bi.biClrImportant = 0;

	// Compute the size of the  infoheader and the color table
	int nColors = (1 << bi.biBitCount);
	if (nColors > 256)
		nColors = 0;

	dwLen = sizeof(BITMAPFILEHEADER) + bi.biSize + nColors * sizeof(RGBQUAD);

	// We need a device context to get the DIB from
	hDC = ::CreateCompatibleDC(NULL);

	hPal = SelectPalette(hDC, hPal, FALSE);
	RealizePalette(hDC);

	// Allocate enough memory to hold bitmapinfoheader and color table
	hDIB = GlobalAlloc(GMEM_FIXED, dwLen);

	if (!hDIB)
	{
		TRACE(_T("Error : Out of memory!\n"));
		SelectPalette(hDC, hPal, FALSE);
		DeleteDC(hDC);
		return NULL;
	}

	lpbi = (LPBITMAPINFOHEADER) ((LPBYTE) hDIB + sizeof(BITMAPFILEHEADER));

	*lpbi = bi;

	// Call GetDIBits with a NULL lpBits param, so the device driver 
	// will calculate the biSizeImage field 
	GetDIBits(hDC, (HBITMAP) bitmap.GetSafeHandle(), 0L, (DWORD) bi.biHeight,
		(LPBYTE) NULL, (LPBITMAPINFO) lpbi, (DWORD) DIB_RGB_COLORS);

	bi = *lpbi;

	// If the driver did not fill in the biSizeImage field, then compute it
	// Each scan line of the image is aligned on a DWORD (32bit) boundary
	if (bi.biSizeImage == 0)
	{
		bi.biSizeImage = ((((bi.biWidth * bi.biBitCount) + 31) & ~31) / 8) * bi.biHeight;
	}

	// Realloc the buffer so that it can hold all the bits
	dwLen += bi.biSizeImage;

	if (handle = GlobalReAlloc(hDIB, dwLen, GMEM_MOVEABLE))
		hDIB = handle;
	else
	{
		GlobalFree(hDIB);

		// Reselect the original palette
		SelectPalette(hDC, hPal, FALSE);
		DeleteDC(hDC);
		return NULL;
	}

	// Get the bitmap bits
	lpbi = (LPBITMAPINFOHEADER) ((LPBYTE) hDIB + sizeof(BITMAPFILEHEADER));

	// FINALLY get the DIB
	BOOL bGotBits = GetDIBits(hDC, (HBITMAP)
						bitmap.GetSafeHandle(), 0L,				// Start scan line
						(DWORD)
						bi.biHeight,		// # of scan lines
		(LPBYTE)
						lpbi 			// address for bitmap bits
 +
						(bi.biSize + nColors * sizeof(RGBQUAD)),
						(LPBITMAPINFO)
						lpbi,		// address of bitmapinfo
						(DWORD)
						DIB_RGB_COLORS);		// Use RGB for color table

	if (!bGotBits)
	{
		TRACE(_T("Error : Faile to get DIBits!\n"));
		GlobalFree(hDIB);

		SelectPalette(hDC, hPal, FALSE);
		DeleteDC(hDC);
		return NULL;
	}

	SelectPalette(hDC, hPal, FALSE);
	DeleteDC(hDC);

	lphdr = (LPBITMAPFILEHEADER) hDIB;

	nColors = 1 << lpbi->biBitCount;

	// Fill in the fields of the file header 
	lphdr->bfType = ((WORD) ('M' << 8) | 'B');	// is always "BM"
	lphdr->bfSize = GlobalSize(hDIB);
	lphdr->bfReserved1 = 0;
	lphdr->bfReserved2 = 0;
	lphdr->bfOffBits = (DWORD)
		(sizeof(BITMAPFILEHEADER) + lpbi->biSize + nColors * sizeof(RGBQUAD));

	// GlobalUnlock(hDIB);  //释放内存区域

	hGlobal = hDIB;

	return Load(dwLen);
}

//returns the width of the loaded image  
DWORD CPicture::_GetWidth(int nMapMode /* = MM_TEXT */)
{
	if (!m_pPict)
		return 0;

	OLE_XSIZE_HIMETRIC pWidth;
	m_pPict->get_Width(&pWidth);

	if (nMapMode == MM_LOMETRIC)
		return (pWidth * 10);

	HDC tDC = ::CreateCompatibleDC(0);
	int nWidth = MulDiv(pWidth, ::GetDeviceCaps(tDC, LOGPIXELSX),
					HIMETRIC_INCH);
	DeleteDC(tDC);
	return (DWORD) nWidth;
}


//returns the height of the loaded image
DWORD CPicture::_GetHeight(int nMapMode /* = MM_TEXT */)
{
	if (!m_pPict)
		return 0;

	OLE_YSIZE_HIMETRIC pHeight;
	m_pPict->get_Height(&pHeight);
	if (nMapMode == MM_LOMETRIC)
		return (pHeight * 10);

	HDC tDC = ::CreateCompatibleDC(0);
	int nHeight = MulDiv(pHeight, ::GetDeviceCaps(tDC, LOGPIXELSY),
					HIMETRIC_INCH);
	DeleteDC(tDC);
	return (DWORD) nHeight;
}

//Draws the image in a specified DC..with given dimensions
//specify -1 for width and height if you like to draw
//with original dimensions
BOOL CPicture::DrawPicture(HDC hdc, long x, long y, long cx, long cy)
{
	if (!m_pPict)
		return FALSE;

	LONG pHeight, pWidth;
	if (cx == -1)
		cx = this->_GetWidth();
	if (cy == -1)
		cy = this->_GetHeight();
	m_pPict->get_Width(&pWidth);
	m_pPict->get_Height(&pHeight);
	if (m_pPict->Render(hdc, x, y, cx, cy, 0, pHeight, pWidth, -pHeight, NULL) !=
		S_OK)
		return FALSE;

	return TRUE;
}

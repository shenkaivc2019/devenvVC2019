// GfxPageCurve.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "GfxPageCommon.h"
#include "ULTool.h"
#include "GraphWnd.h"
#include "MainFrm.h"
#include "ULFilter.h"
#include "Utility.h"
#include "ComConfig.h"
#include "Units.h"
#include "Sheet.h"
#include "Handler.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


extern CUnits* g_units;

/////////////////////////////////////////////////////////////////////////////
// CGfxPageCommon dialog

//Add to support FilterList

CGfxPageCommon::CGfxPageCommon(CWnd* pParent /*=NULL*/)
	: CGfxPage(IDD_GFX_PAGE_COMMON, pParent)
{
	//{{AFX_DATA_INIT(CGfxPageCommon)
	m_strName = _T("");
	m_strUnit = _T("");
	m_bDisplay = FALSE;
	m_bPrint = FALSE;
	m_nLeftGrid = 0;
	m_nRightGrid = 0;
	m_fLeftValue = 0.0f;
	m_fRightValue = 0.0f;
	m_bRewind = FALSE;
	m_lDepthOffset = 0;
	m_lTimeOffset = 0;
	m_lDepthInterval = 0;
	m_lTimeInterval = 20;
	m_bPrintCurveHead = FALSE;
	//}}AFX_DATA_INIT
	m_pCurve = NULL;
	m_pTool = NULL;
	m_nFilterSel = -1;
}

void CGfxPageCommon::DoDataExchange(CDataExchange* pDX)
{
	CGfxPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGfxPageCommon)
	DDX_Control(pDX, IDC_LINEWIDTH, m_cbLineWidth);
	DDX_Control(pDX, IDC_CB_LINETYPE, m_cbLineStyle);
	DDX_Control(pDX, IDC_CURVE_TYPE, m_cbValMode);
	DDX_Control(pDX, IDC_BTN_LINECOLOR, m_btnColor);
	DDX_Text(pDX, IDC_TIME_INTERVAL, m_lTimeInterval);
	DDX_Text(pDX, IDC_TIMEOFFSET, m_lTimeOffset);
	DDX_Text(pDX, IDC_STATIC_NAME, m_strName);
	DDX_Text(pDX, IDC_STATIC_NAME2, m_strName2);
	DDX_Text(pDX, IDC_STATIC_UNIT, m_strUnit);
	DDX_Check(pDX, IDC_CHECK_DISPLAY, m_bDisplay);
	DDX_Check(pDX, IDC_CHECK_PRINT, m_bPrint);
	DDX_Text(pDX, IDC_EDIT_LEFTGRID, m_nLeftGrid);
	DDX_Text(pDX, IDC_EDIT_RIGHTGRID, m_nRightGrid);
	DDX_Text(pDX, IDC_EDIT_LEFTVALUE, m_fLeftValue);
	DDX_Text(pDX, IDC_EDIT_RIGHTVALUE, m_fRightValue);
	DDX_Check(pDX, IDC_CHECK3, m_bRewind);
	DDX_Check(pDX, IDC_CHECK_PRINTHEAD, m_bPrintCurveHead);
	//}}AFX_DATA_MAP
#ifndef _LOGIC 
	DDX_Control(pDX, IDC_COMBO_FILTER, m_cbFilter);
	DDX_Control(pDX, IDC_FILTER_SETTING, m_btnProperty);
#endif
	g_units->DDX_Depth(pDX, IDC_DEPTHOFFSET, m_lDepthOffset);
//	g_units->DDX_Depth(pDX, IDC_DEPTHOFFSET, m_lTimeOffset);
	g_units->DDX_Depth(pDX, IDC_DEPTH_INTERVAL, m_lDepthInterval);
//	g_units->DDX_TIME(pDX, IDC_TIME_INTERVAL, m_lTimeInterval);
}

BEGIN_MESSAGE_MAP(CGfxPageCommon, CGfxPage)
	//{{AFX_MSG_MAP(CGfxPageCommon)
	ON_BN_CLICKED(IDC_BTN_LINECOLOR, RefreshData)
	ON_BN_CLICKED(IDC_CHECK_DISPLAY, RefreshData)
	ON_BN_CLICKED(IDC_CHECK_PRINT, RefreshData)
	ON_BN_CLICKED(IDC_CHECK3, RefreshData)
	ON_BN_CLICKED(IDC_CHECK_PRINTHEAD, RefreshData)
	ON_EN_KILLFOCUS(IDC_EDIT_LEFTVALUE, OnKillFocusLeftValue)
	ON_EN_KILLFOCUS(IDC_EDIT_RIGHTVALUE, OnKillFocusRightValue)
	ON_EN_CHANGE(IDC_EDIT_LEFTGRID, OnChangeEditLeftgrid)
	ON_EN_CHANGE(IDC_EDIT_RIGHTGRID, OnChangeEditRightgrid)
	ON_CBN_SELCHANGE(IDC_CB_LINETYPE, RefreshData)
	ON_CBN_SELCHANGE(IDC_LINEWIDTH, RefreshData)
	ON_EN_KILLFOCUS(IDC_DEPTHOFFSET, RefreshData)
	ON_EN_CHANGE(IDC_STATIC_NAME,RefreshData)
	ON_EN_CHANGE(IDC_STATIC_NAME2, RefreshData)
	ON_EN_CHANGE(IDC_STATIC_UNIT,RefreshData)
	ON_EN_KILLFOCUS(IDC_EDIT_LEFTGRID, OnKillFocusLeftGrid)
	ON_EN_KILLFOCUS(IDC_EDIT_RIGHTGRID, OnKillFocusRightGrid)
	ON_WM_DESTROY()
	ON_CBN_SELCHANGE(IDC_CURVE_TYPE, RefreshData)
	ON_EN_KILLFOCUS(IDC_DEPTH_INTERVAL, RefreshData)
	ON_EN_KILLFOCUS(IDC_TIME_INTERVAL,RefreshData)
	ON_CBN_SELENDOK(IDC_COMBO_FILTER, OnSelChangeFilter)
	ON_BN_CLICKED(IDC_FILTER_SETTING, OnFilterSetting)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGfxPageCommon message handlers

// 初始化对话框
//
//
BOOL CGfxPageCommon::OnInitDialog() 
{
	CGfxPage::OnInitDialog();

#ifndef _LOGIC 
	//m_btnProperty.SetImage (IDB_BCGBARRES_PROP32);
	m_btnProperty.SetImage(IDB_AFXBARRES_PRINT32);
	m_btnProperty.m_nFlatStyle = CMFCButton::BUTTONSTYLE_FLAT;
	CString strTemp;
	strTemp.LoadString(IDS_PARAM_SETTING);
	m_btnProperty.SetTooltip (strTemp);
#endif

	m_btnColor.EnableOtherButton ("");
	m_btnColor.SetColor ((COLORREF)-1);
	m_btnColor.SetColumnsNumber (10);
	
	if (m_pCurve != NULL)
	{
		InitCurve();
	}

	m_cbLineWidth.Initialize();
	m_cbLineStyle.Initialize();

#ifndef _LOGIC
	CString strText;
	strText.LoadString(IDS_NULL);
	m_cbFilter.SetWindowText(strText);
	m_cbFilter.ResetContent();
	int nSize = AfxGetComConfig()->m_filterArray.GetSize();
	for (int i=0; i< nSize; i++)			
	m_cbFilter.AddString(AfxGetComConfig()->m_filterArray[i].FilterName);			
	m_cbFilter.AddString(strText);

	// 在测井时不允许修改滤波器属性
	// 倘若以后的属性对话框是隐藏的，而不是实时创建的，则下面的代码应写在OnShow中
	// 2013 02 22 允许测井时修改这些属性
/*	BOOL bLog = (BOOL)(m_pGraph && m_pGraph->m_bIsLogging);
	
	if (bLog)
	{
		GetDlgItem(IDC_CB_LINETYPE)->EnableWindow(FALSE);
		GetDlgItem(IDC_LINEWIDTH)->EnableWindow(FALSE);
		GetDlgItem(IDC_BTN_LINECOLOR)->EnableWindow(FALSE);
	}
*/
#endif

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

// 初始化曲线属性
//
//
void CGfxPageCommon::InitCurve()
{
	if (m_pCurve != NULL && AfxIsValidString(m_pCurve->m_strName))
	{
		m_cbLineStyle.SetSelectedLineStyle(m_pCurve->m_nStyle);
		m_cbLineWidth.SetCurSel(LWLevel(m_pCurve->m_nWidth));
		m_strName		= m_pCurve->m_strName;
		m_strUnit		= m_pCurve->Unit();
		m_bDisplay		= m_pCurve->IsVisible();
		m_bPrint		= m_pCurve->IsPrint();
		m_bRewind		= m_pCurve->IsRewind();
		m_nLeftGrid		= m_pCurve->m_nLeftGrid;
		m_fLeftValue	= m_pCurve->m_LeftValue;
		m_nRightGrid	= m_pCurve->m_nRightGrid;
		m_fRightValue	= m_pCurve->m_RightValue;
		m_strName		= m_pCurve->m_strName;
		m_strName2      = m_pCurve->m_strIDName;
		m_strUnit		= m_pCurve->Unit();
		m_lDepthOffset	= m_pCurve->DepthOffset();
		m_cbValMode.SetCurSel(m_pCurve->m_nValMode);
		m_btnColor.SetColor(m_pCurve->m_crColor);
		m_bPrintCurveHead = m_pCurve->m_bPrintCurveHead;
		
#ifndef _LOGIC
		//在选择曲线时，显示相应的滤波方法 add by yf 2003-12-25
		if (m_pCurve->GetFilter() != NULL)
		{
			CString strFilterName = m_pCurve->GetFilter()->GetFilterName();
			if (strFilterName.IsEmpty())
				strFilterName.LoadString(IDS_NULL);
			m_nFilterSel = m_cbFilter.FindStringExact(-1, strFilterName);
			m_cbFilter.SetCurSel(m_nFilterSel);
		}
		else //没有滤波器
		{
			m_nFilterSel = m_cbFilter.GetCount()-1;
			m_cbFilter.SetCurSel(m_nFilterSel);
		}
#endif
		CString strPlotStyle = m_pCurve->m_PlotInfo.PlotENName;
		/*		if ((strPlotStyle.CompareNoCase(_T("Depth")) == 0)
		|| (strPlotStyle.CompareNoCase(_T("OBL")) == 0)
		|| (strPlotStyle.CompareNoCase(_T("Date")) == 0))
		{
		m_pCurve->m_bTimeMode = FALSE;
		GetDlgItem(IDC_DEPTH_INTERVAL)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_DEPTH_INTERVAL)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_DEPTHOFFSET)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_DEPTHDELAY)->ShowWindow(SW_SHOW);
		m_lDepthInterval = m_pCurve->m_lDepthDistance;
		#ifndef _LOGIC
		GetDlgItem(IDC_STATIC_FILTER)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_FILTER_SETTING)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_FILTER)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_TIME_INTERVAL)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_TIME_INTERVAL)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_TimeDelay)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_TIMEOFFSET)->ShowWindow(SW_HIDE);
		#endif	
		}
		else*/ 
		if ((strPlotStyle.CompareNoCase(_T("Depth")) == 0)
		|| (strPlotStyle.CompareNoCase(_T("OBL")) == 0)
			|| (strPlotStyle.CompareNoCase(_T("Date")) == 0)
			|| (strPlotStyle.CompareNoCase(_T("Text")) == 0)
			|| (strPlotStyle.CompareNoCase(_T("Time")) == 0))
		{
#ifndef _LOGIC
			GetDlgItem(IDC_STATIC_FILTER)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_FILTER_SETTING)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_COMBO_FILTER)->ShowWindow(SW_HIDE);
#endif
			if (m_pGraph!=NULL)
			{
				if (m_pGraph->m_pSheet->m_nDriveMode)
				{
					GetDlgItem(IDC_DEPTH_INTERVAL)->ShowWindow(SW_SHOW);
					GetDlgItem(IDC_STATIC_DEPTH_INTERVAL)->ShowWindow(SW_SHOW);
					GetDlgItem(IDC_DEPTHOFFSET)->ShowWindow(SW_SHOW);
					GetDlgItem(IDC_STATIC_DEPTHDELAY)->ShowWindow(SW_SHOW);
#ifndef _LOGIC
					GetDlgItem(IDC_STATIC_TIME_INTERVAL)->ShowWindow(SW_HIDE);
					GetDlgItem(IDC_TIME_INTERVAL)->ShowWindow(SW_HIDE);
					GetDlgItem(IDC_STATIC_TimeDelay)->ShowWindow(SW_HIDE);
					GetDlgItem(IDC_TIMEOFFSET)->ShowWindow(SW_HIDE);
#endif
					m_lDepthInterval = m_pCurve->m_lDepthDistance;
				}
				else
				{
#ifndef _LOGIC
					GetDlgItem(IDC_STATIC_TIME_INTERVAL)->ShowWindow(SW_SHOW);
					GetDlgItem(IDC_TIME_INTERVAL)->ShowWindow(SW_SHOW);
					GetDlgItem(IDC_STATIC_TimeDelay)->ShowWindow(SW_SHOW);
					GetDlgItem(IDC_TIMEOFFSET)->ShowWindow(SW_SHOW);
#endif
					GetDlgItem(IDC_DEPTH_INTERVAL)->ShowWindow(SW_HIDE);
					GetDlgItem(IDC_STATIC_DEPTH_INTERVAL)->ShowWindow(SW_HIDE);
					GetDlgItem(IDC_DEPTHOFFSET)->ShowWindow(SW_HIDE);
					GetDlgItem(IDC_STATIC_DEPTHDELAY)->ShowWindow(SW_HIDE);
					m_lTimeInterval = m_pCurve->m_iTimeDistance;
					m_pCurve->m_bTimeMode = TRUE;
				}
			}
		}
		else
		{
			GetDlgItem(IDC_DEPTH_INTERVAL)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_DEPTH_INTERVAL)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_TIME_INTERVAL)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_TIME_INTERVAL)->ShowWindow(SW_HIDE);
#ifndef _LOGIC
			if (m_pSheet)
			{
				if (m_pSheet->m_nDriveMode)
				{
					GetDlgItem(IDC_DEPTHOFFSET)->ShowWindow(SW_SHOW);
					GetDlgItem(IDC_STATIC_DEPTHDELAY)->ShowWindow(SW_SHOW);
					GetDlgItem(IDC_STATIC_TimeDelay)->ShowWindow(SW_HIDE);
					GetDlgItem(IDC_TIMEOFFSET)->ShowWindow(SW_HIDE);
				}
				else
				{
					GetDlgItem(IDC_DEPTHOFFSET)->ShowWindow(SW_HIDE);
					GetDlgItem(IDC_STATIC_DEPTHDELAY)->ShowWindow(SW_HIDE);
					GetDlgItem(IDC_STATIC_TimeDelay)->ShowWindow(SW_SHOW);
					GetDlgItem(IDC_TIMEOFFSET)->ShowWindow(SW_SHOW);
				}
			}
			
			GetDlgItem(IDC_STATIC_FILTER)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_FILTER_SETTING)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_COMBO_FILTER)->ShowWindow(SW_SHOW);
#else
			// GetDlgItem(IDC_STATIC_FILTER)->ShowWindow(SW_HIDE);
			// GetDlgItem(IDC_FILTER_SETTING)->ShowWindow(SW_HIDE);
			// GetDlgItem(IDC_COMBO_FILTER)->ShowWindow(SW_HIDE);
#endif
		}
		
		UpdateData(FALSE);
	}
}


// 修改曲线左值
//
//
void CGfxPageCommon::OnKillFocusLeftValue()
{
	CString strInput;
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_LEFTVALUE);
	pEdit->GetWindowText(strInput);
	
	double d;
	if (SimpleFloatParse(strInput, d))
	{
        int pos = 0, nPosition;
		pos = strInput.Find('.');
		if(pos == -1)
			nPosition = 0;
		else
			nPosition = strInput.GetLength() - pos -1;
		if(m_pCurve != NULL)
		{
			m_pCurve->m_nLeftDotNum = nPosition;
		}
		
		RefreshData();
	}
	else
	{
		pEdit->SetWindowText(DoubleToString(m_fLeftValue));
	}
}

// 修改曲线右值
//
//
void CGfxPageCommon::OnKillFocusRightValue()
{
	CString strInput;
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_RIGHTVALUE);
	pEdit->GetWindowText(strInput);
	double d;
	if (SimpleFloatParse(strInput, d))
	{
		int pos = 0,nPosition;
		pos = strInput.Find('.');
		if(pos == -1)
			nPosition = 0;
		else
			nPosition = strInput.GetLength() - pos -1;
		if(m_pCurve != NULL)
		{
			m_pCurve->m_nRightDotNum = nPosition;
		}
		
		RefreshData();
	}
	else
	{
		pEdit->SetWindowText(DoubleToString(m_fRightValue));
	}
}

// 修改曲线左格子
//
//
void CGfxPageCommon::OnChangeEditLeftgrid() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CGfxPage::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	CString strText;
	GetDlgItem(IDC_EDIT_LEFTGRID)->GetWindowText(strText);

	if (!strText.IsEmpty())
		RefreshData();
}

// 修改曲线右格子
//
//
void CGfxPageCommon::OnChangeEditRightgrid() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CGfxPage::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	CString strText;
	GetDlgItem(IDC_EDIT_RIGHTGRID)->GetWindowText(strText);

	if (!strText.IsEmpty())
		RefreshData();
}

//
//
//
void CGfxPageCommon::OnKillFocusRightGrid()
{
	CString strInput;
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_RIGHTGRID);
	pEdit->GetWindowText(strInput);
	if (!IsDecimal(strInput))
	{
		pEdit->SetWindowText(DoubleToString(m_nRightGrid));
	}
}

void CGfxPageCommon::RefreshData()
{
	UpdateData(TRUE);
	if (m_pCurve != NULL)
	{
		m_pCurve->m_strName = m_strName;
		m_pCurve->m_strIDName = m_strName2;
		m_pCurve->SetUnit(m_strUnit);
		CMainFrame* pMainFrame = (CMainFrame*)AfxGetMainWnd();
		pMainFrame->m_wndPropertiesBar.PostMessage(UM_PROP_UPDATE, PT_ITEM, idCurveUnit);
		pMainFrame->m_wndDataView.UpdateGrid();
		//是否显示
		m_pCurve->Visible(m_bDisplay);
		m_pCurve->Print(m_bPrint);

		m_pCurve->Rewind(m_bRewind);
		m_pCurve->m_nLeftGrid = m_nLeftGrid;
		m_pCurve->m_LeftValue = m_fLeftValue;
		m_pCurve->m_nValMode = m_cbValMode.GetCurSel();

		//add by yh 2009.2.3 
		//解决了bug:当右格子大于最大格子时曲线头显示在井道外侧
		if (m_pTrack == NULL)
			m_pTrack = m_pCurve->m_pTrack;
		
		m_pCurve->m_nRightGrid = m_nRightGrid;
		m_pCurve->m_RightValue = m_fRightValue;
		m_pCurve->m_crColor = m_btnColor.GetColor();
		m_pCurve->m_nStyle = m_cbLineStyle.GetSelectedLineStyle();
	
		int nLWLevel = m_cbLineWidth.GetCurSel();
		if (nLWLevel != CB_ERR)
			m_pCurve->m_nWidth = AfxGetComConfig()->SetLWLevel(nLWLevel);
		m_pCurve->SetDepthOffset(m_lDepthOffset);
		m_pCurve->m_lDepthDistance = m_lDepthInterval;
		m_pCurve->m_iTimeDistance  = m_lTimeInterval;
		m_pCurve->m_bPrintCurveHead = m_bPrintCurveHead;
	}
	
	if (m_pGraph != NULL)
		m_pGraph->InvalidateAll();

}

void CGfxPageCommon::OnKillFocusLeftGrid()
{
	CString strInput;
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_LEFTGRID);
	pEdit->GetWindowText(strInput);
	if (!IsDecimal(strInput))
	{
		pEdit->SetWindowText(DoubleToString(m_nLeftGrid));
	}
}

CString CGfxPageCommon::DoubleToString(double f)
{
	CString strInput;
	strInput.Format("%f", f);
	int iDotPos = strInput.Find('.');
	if (iDotPos == -1)
		return strInput;
	int iZeroCount = 0;
	for (int i = strInput.GetLength() - 1; i > iDotPos; i--)
	{
		if (strInput.GetAt(i) == '0')
			iZeroCount++;
		else
			break;
	}
	CString strOutput = strInput.Left(strInput.GetLength() - iZeroCount);
	if (strOutput.Right(1) == ".")
		strOutput = strOutput.Left(strOutput.GetLength() - 1);
	return strOutput;
}

void CGfxPageCommon::OnSelChangeFilter()
{
#ifndef _LOGIC

	// 根据选择来设定滤波方法
	CString strFilter = "";
	int n = m_cbFilter.GetCurSel();
	if ( (CB_ERR<n) && (n<m_cbFilter.GetCount()-1) )		
		m_cbFilter.GetLBText(n, strFilter);
	if (m_nFilterSel != n)
	{
		m_pCurve->SetFilter(strFilter);
		m_nFilterSel = n;
	}

#endif
}

void CGfxPageCommon::OnFilterSetting()
{
#ifndef _LOGIC
	RefreshData();

	if (m_pCurve != NULL)
	{
		CULFilter* pFilter = m_pCurve->GetFilter();
		
		if (pFilter != NULL)
			pFilter->SetParams();
		else
		{
			AfxMessageBox(IDS_TEXT_NOFILTER);
		}
	}
	else
	{
		AfxMessageBox(IDS_TEXT_NOCURSEL);
	}

#endif

}

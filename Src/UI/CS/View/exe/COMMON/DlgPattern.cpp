// DlgPattern.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "DlgPattern.h"
#include "MyMEMDC.h"
#include "SaveAsBmp.h"
#include "Utility.h"
#include "RemarkObject.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CString g_strPatPath;

/////////////////////////////////////////////////////////////////////////////
// CDlgPattern dialog


CDlgPattern::CDlgPattern(DWORD dwPT /* = 0x03 */, CWnd* pParent /* = NULL */) : CDialog(CDlgPattern::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPattern)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_dwPT = dwPT;
}


void CDlgPattern::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPattern)
	DDX_Control(pDX, IDC_COMBO1, m_cmbType);
	DDX_Control(pDX, IDC_LIST1, m_wndList);
	//}}AFX_DATA_MAP

}


BEGIN_MESSAGE_MAP(CDlgPattern, CDialog)
	//{{AFX_MSG_MAP(CDlgPattern)
	ON_NOTIFY(NM_CLICK, IDC_LIST1, OnClickList1)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST1, OnDblclkList1)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnSelchangeCombo1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgPattern message handlers

BOOL CDlgPattern::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_ImageList.Create(32, 32, ILC_COLORDDB, 0, 1);
	m_wndList.SetImageList(&m_ImageList, LVSIL_NORMAL);
	
	m_cmbType.ResetContent();
	if (m_dwPT & 0x01)
	{
		m_cmbType.AddString(_T("Graphic"));
		m_cmbType.AddString(_T("FillSymbol"));
	}
	
	if (m_dwPT & 0x10)
	{
		m_cmbType.AddString(_T("RemarkSymbol"));
	}

	if (m_cmbType.GetCount())
	{
		m_cmbType.SetCurSel(0);
		OnSelchangeCombo1();
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgPattern::OnClickList1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW *pListView = (NM_LISTVIEW *)pNMHDR;
	int nSelect = m_wndList.HitTest(pListView->ptAction);
	m_strSelect = m_strFolder + _T("\\") + m_wndList.GetItemText(nSelect, 0) + m_strExt;

	*pResult = 0;
}

void CDlgPattern::OnDblclkList1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	EndDialog(IDOK);
}

void CDlgPattern::OnSelchangeCombo1() 
{
	int nSel = m_cmbType.GetCurSel();
	if (nSel == CB_ERR)
		return;
	
	m_wndList.DeleteAllItems();
	while(m_ImageList.GetImageCount())
		m_ImageList.Remove(0);

	int nItem = 0;
	m_cmbType.GetLBText(nSel, m_strFolder);
	
	CRect rect(0, 0, 32, 32);

	CStringList lstFiles;
// 	CString strFind = g_strPatPath.Left(g_strPatPath.ReverseFind(_T('\\')));
// 	strFind = strFind.Left(strFind.ReverseFind(_T('\\'))) + _T("\\") + strText;
	CString strFind = g_strPatPath + m_strFolder;
	RecurseFind(lstFiles, strFind, _T("*.rmk"), 1);

	CDC* pDC = GetDC();
	POSITION pos = lstFiles.GetHeadPosition();
	int i = 0;
	
	for (; pos != NULL; i++)
	{
		CString strFile = lstFiles.GetNext(pos);
		CRemarkObject obj;
		if (!obj.LoadFromFile(strFile))
			continue;

		CMyMemDC dc(pDC, &rect);
		dc.FillSolidRect(&rect, RGB(255,255,255));
		obj.Draw(&dc, &rect);
		CBitmap* pBitmap = dc.SelectObject(dc.m_oldBitmap);
		dc.m_pDC = NULL;
		m_ImageList.Add(pBitmap, pBitmap);
		m_wndList.InsertItem(nItem, GetFileName(strFile), nItem);
		nItem++;
	}
	ReleaseDC(pDC);

	if (nItem > 0)
	{
		m_strExt = _T(".rmk");
		return;
	}

	m_strExt = _T(".bmp");
	lstFiles.RemoveAll();

	if (m_strFolder.CompareNoCase(_T("FillSymbol")) == 0)
	{
		CString strPN[] = { _T("None"), _T("Solid"), _T("Horizontal"), _T("Vertical"),
			_T("Forward Slash"), _T("Backward Slash"), _T("Crosshatch"), _T("Diagonal Cross") };
		
		CString strSave = g_strPatPath + m_strFolder + _T("\\");
		pDC = GetDC();
		for (/*int */i = 0; i < 8; i++)
		{
			CMyMemDC dc(pDC, &rect);
			dc.FillSolidRect(&rect, RGB(255,255,255));
			if (i == 1)
			{
				dc.FillSolidRect(rect, RGB(0, 0, 0));
			}
			else
			{
				CBrush br;
				br.CreateHatchBrush(i - 2, RGB(0, 0, 0));
				dc.FillRect(rect, &br);
			}
			WriteDCToDIB(strSave + strPN[i] + _T(".bmp"), &dc);
			CBitmap* pBitmap = dc.SelectObject(dc.m_oldBitmap);
			dc.m_pDC = NULL;
			m_ImageList.Add(pBitmap, pBitmap);
			m_wndList.InsertItem(nItem, strPN[i], nItem);
			nItem++;
			lstFiles.AddTail(strPN[i]);
		}
		ReleaseDC(pDC);
	}

	CFileFind ff;
	CString strPathName = strFind + _T("\\*.bmp");
	BOOL bFind = ff.FindFile(strPathName);
	while (bFind)
	{
		bFind = ff.FindNextFile();
		CString strBitmap = ff.GetFileName();
		strBitmap = strBitmap.Left(strBitmap.Find('.'));
		if (lstFiles.Find(strBitmap))
			continue;

		CString strName = ff.GetFilePath();
		HBITMAP hBmp = (HBITMAP) LoadImage(NULL, strName, IMAGE_BITMAP, 32,
									32, LR_LOADFROMFILE);
		if (hBmp != NULL)
		{
			CBitmap bmp;
			bmp.Attach(hBmp);
			m_ImageList.Add(&bmp, &bmp);	
			m_wndList.InsertItem(nItem, strBitmap, nItem);
			nItem++;
		}
	}
	ff.Close();
}

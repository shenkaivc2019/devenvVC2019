// Track.cpp: implementation of the CTrack class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ComConfig.h"
#include "resource.h"
#include "Track.h"
#include "Curve.h"
#include "DSF.h"
#include "ITrackThumb.h"
//#include "BCGPDrawManager.h"
#include "MainFrm.h"
#include "Units.h"

#ifdef _PERFORATION
#include "Perforation.h"
extern std::auto_ptr<CPerforation> const g_pTask;
#endif

extern CUnits* g_units;
extern void DrawPattern(CDC* pDC, LPRECT lpRect, LPCTSTR pszPattern, COLORREF clrFore, COLORREF clrBkg);

CArray<CRect, CRect> CTrack::m_flagRects;

ULMIMP CTrack::GetIUnits(CUnits* pIUnits)
{
	pIUnits = g_units;
	return UL_NO_ERROR;
}

ULMPTRIMP CTrack::GetFont(UINT nIndex /* = ftCT */) 
{
	return &g_font[nIndex%ftCount];
}

long CTrack::m_lTD = INVALID_DEPTH;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTrack::CTrack()
{
	m_strName = "";
	m_nType = 0;
	m_crColor = RGB(0, 0, 0);
	m_nLineStyle = PS_SOLID;										
	m_nLineWidth = 3;

	m_bMouseOverTitle = FALSE;
	m_pDEPT = NULL;
	m_pAZIM	= NULL;
	m_pDEV = NULL;

	m_fRatioX = 1.0f;
	m_fRatioY = 200;

	m_nDriveMode = UL_DRIVE_DEPT;
	m_nWorkMode = WORKMODE_IDLE;

	m_bShowTime = FALSE;
	m_bShowDepth = FALSE;

	m_lStartDepth = _DEF_SDEPTH;	//	用户设置的开始深度								
	m_lEndDepth = _DEF_EDEPTH;	//	用户设置的结束深度								

	m_nTrackIndex = 0;
	m_bPrintGap = FALSE;			//	是否打印缺口。									
	m_bGapRemain = FALSE;			//	碰到分页时缺口没打完；							
	m_nGapRemain = 0;				//	缺口剩余的长度。								
	m_bRTPrint = FALSE;
	m_nGapPos = 0;
	m_nGapSize = 0;				//	缺口的大小，单位：0.1mm							
	m_nGapInterval = 0;
	m_nPrintCount = 0;

	m_strAZIM = _T("AZIM");
	m_strDEV = _T("DEV");

	m_rcRegion.SetRectEmpty();
	m_rcHead.SetRectEmpty();
	m_rcRegion.left = 2*PAGE_MARGIN;
	m_rcRegion.right = m_rcRegion.left + 524;
	SetHeadRectLR(m_rcRegion);

	// 初始化格线设置
	m_GridInfo.bHasHLine = TRUE;
	m_GridInfo.bHasVLine = TRUE;
	m_GridInfo.hLine[0].bHasGrid = TRUE;
	m_GridInfo.hLine[0].nGridStep = 10000;
	m_GridInfo.hLine[0].nGridLinewidth = AfxGetComConfig()->SetLWLevel(0);
	m_GridInfo.hLine[0].clrGrid = RGB(0, 0, 0);
	m_GridInfo.hLine[0].nGridLinetype = 0;
	m_GridInfo.hLine[1].bHasGrid = TRUE;
	m_GridInfo.hLine[1].nGridStep = 50000;
	m_GridInfo.hLine[1].nGridLinewidth = AfxGetComConfig()->SetLWLevel(1);
	m_GridInfo.hLine[1].clrGrid = RGB(0, 0, 0);
	m_GridInfo.hLine[1].nGridLinetype = 0;
	m_GridInfo.hLine[2].bHasGrid = TRUE;
	m_GridInfo.hLine[2].nGridStep = 250000;
	m_GridInfo.hLine[2].nGridLinewidth = AfxGetComConfig()->SetLWLevel(2);
	m_GridInfo.hLine[2].clrGrid = RGB(0, 0, 0);
	m_GridInfo.hLine[2].nGridLinetype = 0;
	m_GridInfo.vLineInfo.nGridType = 0;	// 0表示线性格线，1表示对数格线
	m_GridInfo.vLineInfo.nGroupCount = 2;
	m_GridInfo.vLineInfo.nLineCount = 5;
	m_GridInfo.vLineInfo.bHasMiddleGrid = TRUE;
	m_GridInfo.vLineInfo.nMiddleGridLinewidth = AfxGetComConfig()->SetLWLevel(1);
	m_GridInfo.vLineInfo.colorMiddleGrid = RGB(0, 0, 0);
	m_GridInfo.vLineInfo.nMiddleGridLinetype = 0;
	m_GridInfo.vLineInfo.bHasSmallGrid = TRUE;
	m_GridInfo.vLineInfo.nSmallGridLinewidth = AfxGetComConfig()->SetLWLevel(0);
	m_GridInfo.vLineInfo.colorSmallGrid = RGB(0, 0, 0);
	m_GridInfo.vLineInfo.nSmallGridLinetype = 0;
	m_pGapDepthArray = NULL;


	m_Marker.m_pTrack = this;
	m_TestMarker.m_pTrack = this;
	m_pProject = NULL;

	// bao
	m_bLogFlags = FALSE;
	m_bBracketFlags = FALSE;

	//gj 20121217
	m_fRatioTime = 100.0f;
	m_lStartTimeLogic = 0;
	m_bTimeTop = TRUE;
	m_lStartTime = _DEF_STIME;
	m_lEndTime = _DEF_ETIME;
	//add by xwh 2013-03-28
// 	for(int i=0;i<3;i++)
// 	{
// 		m_dHorzGridTime[i] = 0;
// 		m_dHorzGridDepth[i] = 0;
// 	}
	m_dHorzGridTime[0] = 6000;
	m_dHorzGridTime[1] = 30000;
	m_dHorzGridTime[2] = 60000;
	m_dHorzGridDepth[0] = 10000;
	m_dHorzGridDepth[1] = 50000;
	m_dHorzGridDepth[2] = 250000;
	
}

CTrack::~CTrack()
{
	if (m_pAZIM)
	{
		m_pAZIM->Release();
		m_pAZIM = NULL;
	}

	if (m_pDEV)
	{
		m_pDEV->Release();
		m_pDEV = NULL;
	}

	for (int i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
		if (pCurve != NULL)
		{
			if (pCurve->m_nbType == NB_TRACK)
				pCurve->Release();
		}
	}
	m_vecCurve.clear();
}

//////////////////////////////////////////////////////////////////////
// 属性
//////////////////////////////////////////////////////////////////////

ULMIMP CTrack::SetType(int nType)
{
	m_nType = nType;
	return UL_NO_ERROR;
}

ULMIMP CTrack::SetGridProp(void* pGridProp)
{
	memcpy(&m_GridInfo, pGridProp, sizeof(GridProp));
	return UL_NO_ERROR;
}

ULMIMP CTrack::GetDrawSel(void* pDrawSel)
{
	memcpy(pDrawSel, &AfxGetComConfig()->m_DrawMark, sizeof(DRAWSEL));
	return UL_NO_ERROR;
}

void CTrack::ChangeGridCount(int nCount0)
{
	int nTotalCount = m_GridInfo.vLineInfo.nGroupCount* m_GridInfo.vLineInfo.nLineCount;
	if (nTotalCount <= nCount0)
		return;
	
	for (int i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
		if (pCurve->m_nRightGrid == nCount0)
			pCurve->m_nRightGrid = nTotalCount;
	}
}

//////////////////////////////////////////////////////////////////////
// 曲线
//////////////////////////////////////////////////////////////////////

void CTrack::AddCurve(CCurve* pCurve)
{
	m_vecCurve.push_back(pCurve);
	pCurve->m_pTrack = this;
	if (pCurve->m_Plot != NULL)
	{
		pCurve->m_Plot->Advise(this);
	}
	else
	{
		CLSID clsid;
		LPOLESTR szCLSID  = L"Linear.PlotLinear";
		CLSIDFromProgID(szCLSID, &clsid); 
		pCurve->m_Plot.CoCreateInstance(clsid);
		if (pCurve->m_Plot != NULL)
		{
			pCurve->m_PlotInfo.PlotID = clsid;
			pCurve->m_Plot->Advise(this);
		}
	}
}

void CTrack::InsertCurve(CCurve* pCurve, int nPos)
{
	if (nPos < 0)
		nPos = 0;
	if (nPos > m_vecCurve.size())
		nPos = m_vecCurve.size();

	m_vecCurve.insert(m_vecCurve.begin() + nPos, pCurve);
	pCurve->m_pTrack = this;
	if(pCurve->m_Plot != NULL)
	{
		pCurve->m_Plot->Advise(this);
	}
} 

ULMBOOLIMP CTrack::RemoveCurve(ICurve* pCurve)
{
	for (int i = 0; i < m_vecCurve.size(); i++)
	{
		if (pCurve == m_vecCurve.at(i))
		{
			m_vecCurve.erase(m_vecCurve.begin() + i);
			((CCurve *) pCurve)->m_pTrack = NULL;
			return TRUE;
		}
	}	
	return FALSE;
}

ULMBOOLIMP CTrack::DelCurve(CCurve* pCurve)
{
	for (int i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve1 = (CCurve*) m_vecCurve.at(i);
		if (pCurve == pCurve1)
		{
			m_vecCurve.erase(m_vecCurve.begin() + i);
			((CCurve *) pCurve)->m_pTrack = NULL;
			if (pCurve->m_nbType == NB_TRACK)
			{
				if (g_GfxProp.GetSafeHwnd())
					g_GfxProp.ClearTabArray();
				pCurve->Release();
			}
			return TRUE;
		}
	}	
	return FALSE;
}

void CTrack::ExchangeCurvePosition(int nFrom, int nTo)
{
	if (nTo < 0 || nFrom < 0 || nFrom == nTo)
		return ;

	CCurve* pCurveFrom = m_vecCurve.at(nFrom);
	if (nTo < nFrom)
	{
		m_vecCurve.erase(m_vecCurve.begin() + nFrom);
		m_vecCurve.insert(m_vecCurve.begin() + nTo, pCurveFrom);
	}
	else
	{
		m_vecCurve.insert(m_vecCurve.begin() + nTo, pCurveFrom);
		m_vecCurve.erase(m_vecCurve.begin() + nFrom);
	}
}   													   

void CTrack::ApplyNewCurve(CCurve* pCurve)
{
	if (pCurve == NULL)
		return ;

	if (pCurve->m_strName == m_strAZIM)
	{
		if (pCurve != m_pAZIM)
		{
			if (m_pAZIM != NULL)
				m_pAZIM->Release();
			pCurve->AddRef();
			m_pAZIM = pCurve;
		}
	}

	if (pCurve->m_strName == m_strDEV)
	{
		if (pCurve != m_pDEV)
		{
			if (m_pDEV != NULL)
				m_pDEV->Release();
			pCurve->AddRef();
			m_pDEV = pCurve;
		}
	}
	
	int nCurve = m_vecCurve.size();
	for (int i = 0; i < nCurve; i++)
	{
		CCurve* pCurve1 = (CCurve*) m_vecCurve.at(i);
		if (pCurve1->m_pData)
			continue;
		
		CString strName = pCurve1->m_strData.GetLength() ? pCurve1->m_strData : pCurve1->m_strName;
		if (pCurve->m_strName.CompareNoCase(strName) == 0)
		{
			if (pCurve->m_strSource.IsEmpty() || pCurve1->m_strSource.IsEmpty())
			{
				pCurve1->AttachData(pCurve->m_pData);
				pCurve1->m_pTrack = this;
				if (pCurve1->m_Plot != NULL)
				{
					pCurve1->m_Plot->Advise(this);
				}
				//break;
			}

			if ((pCurve->m_strSource == pCurve1->m_strSource) &&
						 (pCurve->m_nTLIndex == pCurve1->m_nTLIndex))
			{
				pCurve1->AttachData(pCurve->m_pData);
				pCurve1->m_pTrack = this;
				if (pCurve1->m_Plot != NULL)
				{
					pCurve1->m_Plot->Advise(this);
				}

				//break;
			}
		}
	}

// 	if ((m_nType == UL_TRACK_DEPTH) && (i == nCurve) && pCurve->IsDepthCurve())
// 		ApplyDepthCurve(pCurve, pCurve->m_pData);
}

void CTrack::ApplyNewData(CCurveData* pData)
{
	if (pData->m_pSrcCurve == NULL)
		return ;

	// 镜像时，pSrcCurve的数据并不是镜像的数据
	CString strName = pData->m_pSrcCurve->m_strName;
	CString strSrc = pData->m_pSrcCurve->m_strSource;

	int nCurve = m_vecCurve.size();
	int i = 0;
	for (i = 0; i < nCurve; i++)
	{
		CCurve* pCurve1 = (CCurve*) m_vecCurve.at(i);
		if (strName == pCurve1->m_strName)
		{
			if (strSrc.IsEmpty() /*|| pCurve1->m_strSource.IsEmpty()*/)
			{
				pCurve1->AttachData(pData);
				pCurve1->m_pTrack = this;
				break;
			}

			if (strSrc == pCurve1->m_strSource)
			{
				pCurve1->AttachData(pData);
				pCurve1->m_pTrack = this;
				break;
			}
		}
	}

// 	if ((m_nType == UL_TRACK_DEPTH) &&
// 		(i == nCurve) &&
// 		pData->m_pSrcCurve->IsDepthCurve())
// 		ApplyDepthCurve(pData->m_pSrcCurve, pData);
}

void CTrack::ApplyDepthCurve(CCurve* pCurve, CCurveData* pData /* = NULL */)
{
	BOOL bNoDepth = TRUE;
	int nCurve = m_vecCurve.size();
	int j = 0;
	for (j = 0; j < nCurve; j++)
	{
		CCurve* pCurve1 = (CCurve*) m_vecCurve.at(j);
		if (pCurve1->IsDepthCurve())
		{
			pCurve1->AttachData(pCurve->m_pData);
			pCurve1->m_pTrack = this;
			bNoDepth = FALSE;
			// 关联井道中的所有深度曲线
		}
	}

	if (bNoDepth)
	{
		CCurve* pCurve1 = new CCurve(NB_TRACK);
		pCurve1->SetName("DEPT");
		AddCurve(pCurve1);
		pCurve1->AttachData(pCurve->m_pData);
		pCurve1->m_pTrack = this;
	}
}	

ULMINTIMP CTrack::GetSeledCurve(CURVES& vecCurve)
{
	int nCount = 0;
	int i = 0;
	for (i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
		if (pCurve->m_bSelected)
		{
			m_vecCurve.push_back(pCurve);
			nCount++;
		}
	}
	return nCount;
}

int CTrack::GetSeledCurve(CPtrArray& arrCurve)
{
	int nCount = 0;
	int i = 0;
	for (i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
		if (pCurve->m_bSelected)
		{
			arrCurve.Add(pCurve);
			nCount++;
		}
	}
	return nCount;
}

BOOL CTrack::IsCurveSelected()
{
	int i = 0;
	for (i = 0; i < m_vecCurve.size(); i++)
	{
		if (m_vecCurve.at(i)->IsSelect())
			return TRUE;
	}
	return FALSE;
}

CCurve* CTrack::GetFirstSeledCurve()
{
	for (int i = 0; i < m_vecCurve.size(); i++)
	{
		ICurve* pCurve = m_vecCurve.at(i);
		if (pCurve->IsSelect() && pCurve->IsVisible())
			return (CCurve*)pCurve;
	}
	
	return NULL;
}

CCurve* CTrack::GetCurve(LPCTSTR pszCurve)
{
	for (int i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
		if (pCurve->m_strName == pszCurve)
			return pCurve;
	}

	return NULL;
}

int CTrack::GetCurveXPos(int nCurve, long lDepth)
{
	CCurve* pCurve = (CCurve*) m_vecCurve.at(nCurve);	
	return GetCurveXPos(pCurve, lDepth);
}

int CTrack::GetCurveXPos(CCurve* pCurve, long lDepth)
{
	//if (m_pTrackBase != NULL)
	//	return m_pTrackBase->GetCurveXPos(pCurve, lDepth);
	return -1;
}

//////////////////////////////////////////////////////////////////////
// 测井设置
//////////////////////////////////////////////////////////////////////
void CTrack::SetLogMode(int nWorkMode, int nDriveMode)
{
//	m_nLogDirection = nDirection;
	m_nWorkMode = nWorkMode;
//	m_nDriveMode = nDriveMode;

	// Curve's direction is decided by data file not sheet

// 	for (int i = 0; i < m_vecCurve.size(); i++)
// 	{
// 		CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
// 		if (pCurve->AutoDirection() != nDirection)
// 			pCurve->InvertCurveDirection();
// 		if (NULL != pCurve->m_pData)
// 			pCurve->m_pData->m_nDirection = nDirection;
// 	}
}

void CTrack::SetDepthRange(long lStart, long lEnd)
{
	switch(m_nDriveMode)
	{
	case 0:
		m_lStartTime = lStart;
		m_lEndTime = lEnd;
		break;
	case 1:
		m_lStartDepth = lStart;
		m_lEndDepth = lEnd;
		break;
	default:
		break;
	}
}

void CTrack::SetModeShow(BOOL bTime, BOOL bDepth)
{
	m_bShowTime = bTime;
	m_bShowDepth = bDepth;
}

void CTrack::UpdateGrids()
{
	int nLWLevel = LWLevel(m_GridInfo.vLineInfo.nSmallGridLinewidth);
	m_GridInfo.vLineInfo.nSmallGridLinewidth = AfxGetComConfig()->SetLWLevel(nLWLevel);
	nLWLevel = LWLevel(m_GridInfo.vLineInfo.nMiddleGridLinewidth);
	m_GridInfo.vLineInfo.nMiddleGridLinewidth = AfxGetComConfig()->SetLWLevel(nLWLevel);
	int i = 0;
	for (i = 0; i < 3; i++)
	{
		nLWLevel = LWLevel(m_GridInfo.hLine[i].nGridLinewidth);
		m_GridInfo.hLine[i].nGridLinewidth = AfxGetComConfig()->SetLWLevel(nLWLevel);
	}

	for (i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
		if (pCurve != NULL)
		{
			nLWLevel = LWLevel(pCurve->m_nWidth);
			pCurve->m_nWidth = AfxGetComConfig()->SetLWLevel(nLWLevel);
		}
	}
}

//////////////////////////////////////////////////////////////////////
// 绘图：
//		绘图接口不为空，则调用ITRACKBASE的接口函数
//		返回 UL_NO_ERROR 没有实现，调用系统默认实现函数
//		返回 UL_ERROR 空实现|实现，不调用系统默认实现函数
//  	绘图接口指针为空，do nothing
//////////////////////////////////////////////////////////////////////
void CTrack::DrawHead(CDC* pDC, BOOL bTitle /* = TRUE */)
{
	CPen* pOldPen = pDC->GetCurrentPen();
	CBrush* pOldBrush = pDC->SelectObject(CBrush::FromHandle((HBRUSH)
											GetStockObject(NULL_BRUSH)));	
	//////////////////////////////////////////////////////////////////////////	
	CRect rc;
	GetHeadRect(&rc, FALSE);

	rc.bottom -= TRACK_MARGIN;

	CRect rc1 = rc, rc2;
	int nLeftMargin = 0;
	int nRightMargin = 0;

	CPtrList lstCurves;
	// bao
	m_CurveHeadRects.RemoveAll();
	for (int i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
		int nHeight = pCurve->CalcTitle();
		if (nHeight == 0)
			continue;

		pCurve->m_lstCurve.RemoveAll();
		if (pCurve->GetValMode())
			pCurve->m_lstCurve.AddTail(&lstCurves);

		lstCurves.AddTail(pCurve);

		nHeight *= 100;
		rc1.top = rc1.bottom - nHeight - TRACK_MARGIN;

		nLeftMargin = pCurve->LeftMargin(FALSE);
		nRightMargin = pCurve->RightMargin(FALSE);

		rc2 = rc1;

		rc2.left = nLeftMargin;
		rc2.right = nRightMargin;

	/*	// bao add 2011/06/08
		BOOL bLink = FALSE;
		for (int z = 0; z < m_CurveHeadRects.GetSize(); z++)
		{
			LINKCURVE oldLinkCurve = m_CurveHeadRects.GetAt(z);
			CRect tempRect = oldLinkCurve.rectCurve;
			if(rc2.left != 0)
			{
				if (tempRect.right == rc2.left)
				{
					rc2.top = tempRect.top;
					rc2.bottom = tempRect.bottom;
					tempRect.right = rc2.right;
					oldLinkCurve.rectCurve = tempRect;
					m_CurveHeadRects.RemoveAt(z);
					m_CurveHeadRects.InsertAt(z, oldLinkCurve, 1);
					bLink = TRUE;
				}
			}
			if (rc2.right != 10)
			{
				if (tempRect.left == rc2.right)
				{
					rc2.top = tempRect.top;
					rc2.bottom = tempRect.bottom;
					tempRect.left = rc2.left;
					oldLinkCurve.rectCurve = tempRect;
					m_CurveHeadRects.RemoveAt(z);
					m_CurveHeadRects.InsertAt(z, oldLinkCurve, 1);
					bLink = TRUE;
				}
			}

		}*/
		if (pCurve->IsSelect())
		{
			rc = rc2;
			rc.DeflateRect(0, -15, 0, 45);
			pDC->FillSolidRect(rc, RGB(33, 142, 255));
			
			for (int i = rc.left + 1; i < rc.right - 1; i += 5)
			{
				for (int j = rc.bottom - 1; j > rc.top + 1; j -= 5)
				{
					pDC->SetPixel(i, j, RGB(255, 250, 255));
				}
			}
			
			CPen pen(m_nLineStyle, 2, RGB(0, 16, 33));
			pDC->SelectObject(&pen);
			
			pDC->MoveTo(rc.left, rc.top);
			pDC->LineTo(rc.left, rc.bottom);
			pDC->LineTo(rc.right, rc.bottom);
			pDC->LineTo(rc.right, rc.top);
			
			rc.DeflateRect(20, 20);
			pDC->FillSolidRect(rc, RGB(250, 250, 250));
			
			pDC->MoveTo(rc.left, rc.top);
			pDC->LineTo(rc.left, rc.bottom);
			pDC->LineTo(rc.right, rc.bottom);
			pDC->LineTo(rc.right, rc.top);
		}

		if (!pCurve->m_bDelete && pCurve->IsVisible())
		{
			pCurve->DrawHeadCurve(pDC,rc2);
		}

		
		/*if (!bLink)
		{
			// bao add 2011/06/08
			LINKCURVE linkCurve;
			linkCurve.rectCurve = rc2;
			m_CurveHeadRects.Add(linkCurve);*/

			rc1.bottom = rc1.top + TRACK_MARGIN;
		//}
	}

	//////////////////////////////////////////////////////////////////////////	
	GetHeadRect(&rc, FALSE);

	rc.top = rc.bottom - TRACK_MARGIN;
	
	CPen penBox(m_nLineStyle, PEN_BORDER_SIZE, TRACK_EDGE_COLOR);
	pDC->SelectObject(&penBox);
	if (bTitle)
	{
		DrawTitleColor(pDC, rc, RGB(94, 146, 223), RGB(255, 255, 255));

		rc.top -= 2;
		pDC->MoveTo(rc.left, rc.top);
		pDC->LineTo(rc.right, rc.top);
	}

	GetHeadRect(&rc, FALSE);

	pDC->MoveTo(rc.left, rc.top);
	pDC->LineTo(rc.right, rc.top);
	pDC->LineTo(rc.right, rc.bottom);
	pDC->LineTo(rc.left, rc.bottom);
	pDC->LineTo(rc.left, rc.top);

	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
}

ULMIMP CTrack::DrawTitleColor(CDC* pDC, LPRECT lpRect, COLORREF clrStart,
	COLORREF clrEnd)
{
	int r, g, b;
	if (m_bMouseOverTitle)
	{
		r = 255 - GetRValue(clrStart);
		g = 255 - GetGValue(clrStart);
		b = 255 - GetBValue(clrStart);
		clrStart = RGB(249, 201, 107); // RGB(r, g, b); 
	}   	  


	r = (GetRValue(clrEnd) - GetRValue(clrStart));
	g = (GetGValue(clrEnd) - GetGValue(clrStart));
	b = (GetBValue(clrEnd) - GetBValue(clrStart));

	int nSteps = max(abs(r), max(abs(g), abs(b)));

	CRect rc = lpRect;
	float fStep = (float) (abs(rc.Height())) / (float) nSteps;

	float rStep = r / (float) nSteps;
	float gStep = g / (float) nSteps;
	float bStep = b / (float) nSteps;

	r = GetRValue(clrStart);
	g = GetGValue(clrStart);
	b = GetBValue(clrStart);

	CBrush brush;
	RECT rectFill;		
	for (int iOnBand = 0; iOnBand < nSteps; iOnBand++)
	{
		::SetRect(&rectFill, rc.left, rc.bottom - (int) (iOnBand * fStep),
			rc.right + 1, rc.bottom - (int) ((iOnBand + 1) * fStep));	

		if (iOnBand <= nSteps / 2)
		{
			brush.CreateSolidBrush(RGB(r + rStep * iOnBand,
									g + gStep * iOnBand, b + bStep * iOnBand));
		}
		else
		{
			brush.CreateSolidBrush(RGB(r + rStep * (nSteps - 1 - iOnBand),
									g + gStep * (nSteps - 1 - iOnBand),
									b + bStep * (nSteps - 1 - iOnBand)));
		}
		pDC->FillRect(&rectFill, &brush);
		brush.DeleteObject();

		if ((int) (iOnBand * fStep) > abs(rc.Height()))
		{
			break;
		}
	}

	pDC->SetBkMode(TRANSPARENT);
	CFont* pOldFont = pDC->SelectObject(&g_font[ftCT]);
	pDC->DrawText(m_strName, lpRect, DT_SINGLELINE | DT_VCENTER | DT_CENTER);
	pDC->SelectObject(pOldFont);

	return UL_NO_ERROR;
}

void CTrack::DrawTrack(CDC* pDC, long lStartDepth, long lEndDepth, int nMode)
{
	int i = 0;
	if (nMode == 0) // 静态绘制
	{
/*		for (i = 0; i < m_vecCurve.size(); i++)
		{
			CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
			if (pCurve->GetCurveMode() & CURVE_BACK || pCurve->IsDepthCurve())
				continue;

			if (pCurve->IsInpre())
				pCurve->Draw(pDC, lStartDepth , lEndDepth);
		}*/

		for (i = 0; i < m_vecCurve.size(); i++)
		{
			CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
			if (pCurve->GetCurveMode() & CURVE_BACK || (pCurve->IsDepthCurve() && GetDriveMode() == UL_DRIVE_DEPT))
				continue;

			if (!pCurve->IsInpre())
				pCurve->Draw(pDC, lStartDepth , lEndDepth);
		}
	}
	else			// 动态绘制
	{
		for (i = 0; i < m_vecCurve.size(); i++)
		{
			CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
			if (pCurve->GetCurveMode() & CURVE_BACK || (pCurve->IsDepthCurve() && GetDriveMode() == UL_DRIVE_DEPT))
				continue;

		/*	if ( (pCurve->GetCurveMode() & CURVE_GUN) && -1 != pCurve->m_strName.Find(_T("CCL")) )
			{
				COLORREF clr = RGB(255,0,0);
				clr = RGB(0,255,0);
				clr = RGB(0,0,255); 

				TRACE("CTrack::DrawTrack pCurve = %p\n",pCurve);
				//g_pTask->DrawCurves(pDC , lStartDepth , lEndDepth, pCurve);
				//continue;
			}*/

			if (!pCurve->IsInpre())
				pCurve->LogDraw(pDC , lStartDepth , lEndDepth);
		}
	}
}

void CTrack::DrawFill(CDC* pDC, long lStartDepth, long lEndDepth, int nMode)
{
	int i = 0;
	if (nMode == 0) // 静态绘制
	{
		for (i = 0; i < m_vecCurve.size(); i++)
		{
			CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
			if (pCurve->GetCurveMode() & CURVE_BACK || pCurve->IsDepthCurve())
				continue;

			if (pCurve->IsInpre())
				pCurve->Draw(pDC, lStartDepth , lEndDepth);
		}
	}
	else			// 动态绘制
	{
		for (i = 0; i < m_vecCurve.size(); i++)
		{
			CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
			if (pCurve->GetCurveMode() & CURVE_BACK || pCurve->IsDepthCurve())
				continue;
			
			if (pCurve->IsInpre())
				pCurve->LogDraw(pDC , lStartDepth , lEndDepth);
		}
	}
}

void CTrack::DrawHorzGrid(CDC* pDC, long lStartDepth, long lEndDepth)
{
	int nStartDepth = min(lStartDepth, lEndDepth);
	int nEndDepth = max(lStartDepth, lEndDepth);
	
	if (m_pProject)
	{
		DrawResults(pDC, nStartDepth, nEndDepth);
		return;
	}

/*
	if (GetDriveMode() == 0)
	{
		m_GridInfo.hLine[0].nGridStep = 1;   //modify by gj 
		m_GridInfo.hLine[1].nGridStep = 5;
		m_GridInfo.hLine[2].nGridStep = 25;
	}*/

	if (m_GridInfo.bHasHLine)
	{
		unsigned pattern[8];
		int nLinewidth, nLinetype;
		COLORREF colorLine;
		long lDepth;
		
		int nLwMin[3] = {0, 5, 8};

		int nDelta = m_GridInfo.hLine[2].nGridLinewidth;
		int nLStart = nStartDepth - nDelta;
		int nLEnd = nEndDepth + nDelta;

		if (GetDriveMode() == UL_DRIVE_DEPT)
		{
			if (nLStart < m_lStartDepth)
				nLStart = m_lStartDepth;
			if (nLEnd > m_lEndDepth)
				nLEnd = m_lEndDepth;
		}
		
		CRect rcTrack;
		GetRegionRect(&rcTrack, FALSE);

		for (int i = 0; i < 3; i++)
		{
			if (m_GridInfo.hLine[i].bHasGrid)
			{
				// 线宽
				//nLinewidth = nLwMin[i];
				//if (LVWidth(m_GridInfo.hLine[i].nGridLinewidth) > nLwMin[i])
				//	nLinewidth = LVWidth(m_GridInfo.hLine[i].nGridLinewidth);
				nLinewidth = LVWidth(m_GridInfo.hLine[i].nGridLinewidth);
				// 线型
				nLinetype = m_GridInfo.hLine[i].nGridLinetype;

				// 线色
				colorLine = m_GridInfo.hLine[i].clrGrid;
				
				CDashLine gridLine(*pDC, pattern, 
					CDashLine::GetPattern(pattern,0, 2 * nLinewidth, nLinetype));
				CPen penGrid(PS_SOLID, nLinewidth, colorLine);
				//add  by  xwh  此处最好加上保护措施，防止步长设置为0 时 出错
				if (m_nDriveMode)
				{
					for(int K1=0;K1<3;K1++)
					{
						m_GridInfo.hLine[K1].nGridStep = m_dHorzGridDepth[K1];
					}
					if (m_GridInfo.hLine[0].nGridStep==0) m_GridInfo.hLine[0].nGridStep= 10000;
					if (m_GridInfo.hLine[1].nGridStep==0) m_GridInfo.hLine[1].nGridStep= 50000;
					if (m_GridInfo.hLine[2].nGridStep==0) m_GridInfo.hLine[2].nGridStep= 250000;
				}
				else
				{
					for(int K1=0;K1<3;K1++)
					{
						m_GridInfo.hLine[K1].nGridStep = m_dHorzGridTime[K1];
					}
					if (m_GridInfo.hLine[0].nGridStep==0) m_GridInfo.hLine[0].nGridStep= 6000;
					if (m_GridInfo.hLine[1].nGridStep==0) m_GridInfo.hLine[1].nGridStep= 30000;
					if (m_GridInfo.hLine[2].nGridStep==0) m_GridInfo.hLine[2].nGridStep= 60000;
				}
				
		
				int nStart = (nStartDepth + 1) / m_GridInfo.hLine[i].nGridStep - 2;
				int nEnd = (nEndDepth - 1) / m_GridInfo.hLine[i].nGridStep + 2;
				int nRight = rcTrack.right;
				if (i == 2)
					nRight -= 2;
				
				for (int j = nStart; j <= nEnd; j++)
				{
					lDepth = j * m_GridInfo.hLine[i].nGridStep;
					if (lDepth < nLStart)
						continue;

					if (lDepth > nLEnd)
						continue;
					
					int y = 0;
					switch(GetDriveMode())
					{
					case 0:
						y = -GetCoordinateFromTime(lDepth);
						break;
					case 1:
						y = GetCoordinateFromDepth(lDepth);
						break;
					default:
						break;
					}
					
					CPen* pOldPen = pDC->SelectObject(&penGrid);
					gridLine.MoveTo(rcTrack.left, y);
					gridLine.LineTo(nRight, y);
					pDC->SelectObject(pOldPen);
				}
			}
		}
	}

	//Start:画深度曲线
	for (int i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve = m_vecCurve.at(i);
		if (pCurve->IsDepthCurve() && GetDriveMode() == UL_DRIVE_DEPT)
		{
			pCurve->Draw(pDC, lStartDepth, lEndDepth);
		}
	}
	//End:画深度曲线
	
	// 画标签
//	m_TestMarker.DrawMark(pDC, lStartDepth, lEndDepth);
	
	
}

void CTrack::DrawMark(CDC* pDC, long lStartDepth, long lEndDepth)
{
	// 画标签
	m_TestMarker.DrawMark(pDC, lStartDepth, lEndDepth);
}
/***函数：DrawCurveFlag
****功能：绘制曲线标识
****作者：xwh
****创建时间：2013-9-4
****修订记录：注释 2013-9-11
****修订记录：无
*/
/*
void CTrack::DrawCurveFlag(CDC* pDC , long lStartDepth , long lEndDepth)
{
	m_TestMarker.DrawCurveFlag(pDC, lStartDepth, lEndDepth);
}*/
void CTrack::DrawBackCurve(CDC* pDC, long lStartDepth, long lEndDepth, BOOL bLogDraw)
{
	int nStartDepth = min(lStartDepth, lEndDepth);
	int nEndDepth = max(lStartDepth, lEndDepth);
	for (int i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve = m_vecCurve.at(i);
		if ((pCurve->GetCurveMode() & CURVE_BACK) == CURVE_BACK)
		{
			if (lStartDepth > lEndDepth)
			{
				nStartDepth = min(lStartDepth, pCurve->GetBKEnd());
				nEndDepth = max(lEndDepth, pCurve->GetBKBegin());
			}
			else
			{
				nStartDepth = max(lStartDepth, pCurve->GetBKBegin());
				nEndDepth = min(lEndDepth, pCurve->GetBKEnd());
			}
			//if (bLogDraw)
			//	pCurve->LogDraw(pDC, nStartDepth, nEndDepth);
			//else
			    pCurve->Draw(pDC, nStartDepth, nEndDepth);
		}

		//Start: 画射孔背景曲线
#ifdef _PERFORATION
		if ( (pCurve->GetCurveMode() & CURVE_GUN) &&
			 -1 != pCurve->m_strName.Find(_T("CCL"))  &&
			 NULL != g_pTask->m_pGraphWnd &&
			 g_pMainWnd->IsPerforation() )
		{
			g_pTask->DrawCurves(pDC , lStartDepth , lEndDepth, NULL, bLogDraw);
		}
#endif
		//End: 画射孔背景曲线
	}
}

void CTrack::DrawVertGrid(CDC* pDC, long lStart, long lEnd)
{
	int nStartPos = 0;
	int nEndPos = 0;
	switch(GetDriveMode())
	{
	case 1:
		nStartPos = GetCoordinateFromDepth(lStart);
		nEndPos   = GetCoordinateFromDepth(lEnd);
		break;
	case 0:
		nStartPos = -GetCoordinateFromTime(lStart);
		nEndPos   = -GetCoordinateFromTime(lEnd);
		break;
	}

	if (m_pProject == NULL)
	{
		switch(m_GridInfo.vLineInfo.nGridType)
		{
		case 0:
			DrawVertGridLinear(pDC, nStartPos, nEndPos);
			break;
		case 1:
			DrawVertGridLog(pDC, nStartPos, nEndPos);
			break;
		}
	}

	CRect rcTrack;
	GetRegionRect(&rcTrack, FALSE);

	// 画井道边
	CPen pen(PS_SOLID, 5/*LVWidth(m_GridInfo.vLineInfo.nMiddleGridLinewidth)*/, TRACK_EDGE_COLOR);
	CPen* pOldPen = pDC->SelectObject(&pen);
	pDC->MoveTo(rcTrack.left, nStartPos);
	pDC->LineTo(rcTrack.left, nEndPos);
	pDC->MoveTo(rcTrack.right, nStartPos);
	pDC->LineTo(rcTrack.right, nEndPos);
	pDC->SelectObject(pOldPen);
}

void CTrack::DrawVertGridLinear(CDC* pDC, int nStartPos, int nEndPos)
{
	unsigned pattern[8];
	int nLinewidth, nLinetype;
	COLORREF colorLine;
	
	// 中格子画笔
	if (LVWidth(m_GridInfo.vLineInfo.nMiddleGridLinewidth) > 5)
		nLinewidth = LVWidth(m_GridInfo.vLineInfo.nMiddleGridLinewidth);
	else
		nLinewidth = 5;
	nLinetype = m_GridInfo.vLineInfo.nMiddleGridLinetype;
	colorLine = m_GridInfo.vLineInfo.colorMiddleGrid;
	CDashLine MiddleGridPen(*pDC, pattern, CDashLine::GetPattern(pattern, 0,2 * nLinewidth, nLinetype));
	CPen penMiddleGrid(PS_SOLID, nLinewidth, colorLine);
	
	// 小格子画笔
	nLinewidth = LVWidth(m_GridInfo.vLineInfo.nSmallGridLinewidth);
	nLinetype = m_GridInfo.vLineInfo.nSmallGridLinetype;
	colorLine = m_GridInfo.vLineInfo.colorSmallGrid;
	CDashLine SmallGridPen(*pDC, pattern, CDashLine::GetPattern(pattern, 0,2 * nLinewidth, nLinetype));
	CPen penSmallGrid(PS_SOLID, nLinewidth, colorLine);
	
	if (m_GridInfo.bHasVLine)
	{
		USHORT nLines = m_GridInfo.vLineInfo.nLineCount;	// 格数
		USHORT nGroup = m_GridInfo.vLineInfo.nGroupCount;	// 组数

		CRect rcTrack;
		GetRegionRect(&rcTrack, FALSE);
		
		int nGrid = nLines* nGroup;
		int nGridWidth = (int) (rcTrack.Width() / (float) nGrid);
		int nPos;

		for (int j = 0; j < nGrid; j ++)
		{
			nPos = rcTrack.left + (int) (j * rcTrack.Width() / (float) nGrid);
			if (nPos > rcTrack.right)
				nPos = rcTrack.right;
			
			// 画垂直中格子
			if ((j % m_GridInfo.vLineInfo.nLineCount) == 0)
			{
				if (m_GridInfo.vLineInfo.bHasMiddleGrid)
				{
					CPen* pOldPen = pDC->SelectObject(&penMiddleGrid);
					MiddleGridPen.MoveTo(nPos, nStartPos);
					MiddleGridPen.LineTo(nPos, nEndPos);
					pDC->SelectObject(pOldPen);
					continue;
				}
			}

			// 画垂直小格子
			if (m_GridInfo.vLineInfo.bHasSmallGrid)
			{
				CPen* pOldPen = pDC->SelectObject(&penSmallGrid);
				SmallGridPen.MoveTo(nPos, nStartPos);
				SmallGridPen.LineTo(nPos, nEndPos);
				pDC->SelectObject(pOldPen);
			}
		}
	}
}

void CTrack::DrawVertGridLog(CDC* pDC, int nStartPos, int nEndPos)
{
	unsigned pattern[8];
	int nLinewidth, nLinetype;
	COLORREF colorLine;

	// 中格子画笔
	if (LVWidth(m_GridInfo.vLineInfo.nMiddleGridLinewidth) > 5)
		nLinewidth = LVWidth(m_GridInfo.vLineInfo.nMiddleGridLinewidth);
	else
		nLinewidth = 5;
	nLinetype = m_GridInfo.vLineInfo.nMiddleGridLinetype;
	colorLine = m_GridInfo.vLineInfo.colorMiddleGrid;
	CDashLine MiddleGridPen(*pDC, pattern, CDashLine::GetPattern(pattern, 0,
											2 * nLinewidth, nLinetype));
	CPen penMiddleGrid(PS_SOLID, nLinewidth, colorLine);

	// 小格子画笔
	nLinewidth = LVWidth(m_GridInfo.vLineInfo.nSmallGridLinewidth);
	nLinetype = m_GridInfo.vLineInfo.nSmallGridLinetype;
	colorLine = m_GridInfo.vLineInfo.colorSmallGrid;
	CDashLine SmallGridPen(*pDC, pattern, CDashLine::GetPattern(pattern, 0,
											2 * nLinewidth, nLinetype));
	CPen penSmallGrid(PS_SOLID, nLinewidth, colorLine);

	float fLeftValue;
	float fRightValue;
	int size = m_vecCurve.size();

	// 判断是否有曲线
	if (size <= 0)
	{
		fLeftValue = 0.1f;
		fRightValue = 100;
	}
	else
	{
		// 因对数井道要求所有曲线左右值相同， 故只取第一条
		ICurve* pCurve = m_vecCurve.at(0);	
		// 曲线左右值
		fLeftValue = pCurve->LeftVal(); 
		fRightValue = pCurve->RightVal();
	}

	if (m_GridInfo.bHasVLine) // add by bao 修改对数格线算法
	{
		if (fLeftValue <= fRightValue) // 左值小于右值时
		{
			// 左值为零的处理:
			if (fLeftValue <= 0)
				fLeftValue = 0.001f;
			
			if (fRightValue <= 0)
			{
				fRightValue = 1000;
			}

			CString strLeft = "";
			int nLeft = 0;
			CRect rcTrack;
			GetRegionRect(&rcTrack, FALSE);
			// 线数:由输入确定
			USHORT nLines = GetVertGridCount();
			double fwidth = 1.0 * rcTrack.Width() / (log10(fRightValue) - log10(fLeftValue));
			float x = rcTrack.left;
			float fIncream = 0.0f;	// 与前一格线的增量
			float fPower = 0;		// 当前格线的数量级
			float fStart = fLeftValue;
			while(fStart < fRightValue)
			{
				strLeft.Format("%e", fStart);
				strLeft = strLeft.Right(4);
				nLeft = atoi(strLeft);
				fPower = pow(10.0, nLeft);
				fIncream = (10.0 / nLines) * fPower;
				if (fIncream <= 0)
				{
					fIncream = 1;
				}
				x += (log10(fStart + fIncream) - log10(fStart)) * fwidth;

				// 判断是否中格子
				strLeft.Format("%e", (fStart + fIncream));
				strLeft = strLeft.Left(1);
				nLeft = atoi(strLeft);
				BOOL bMGrid = FALSE;
				if (nLeft == 1)
				{
					bMGrid = TRUE;
				}

				fStart += fIncream;
				if (x > rcTrack.right)
				{
					x = rcTrack.right;
				}
				
				// 画垂直小格子
				if (bMGrid && m_GridInfo.vLineInfo.bHasMiddleGrid)
				{
				CPen* pOldPen = pDC->SelectObject(&penMiddleGrid);
				MiddleGridPen.MoveTo(x, nStartPos);
				MiddleGridPen.LineTo(x, nEndPos);
				pDC->SelectObject(pOldPen);
				continue;
				}
			
				if (m_GridInfo.vLineInfo.bHasSmallGrid)
				{
					CPen* pOldPen = pDC->SelectObject(&penSmallGrid);
					SmallGridPen.MoveTo(x, nStartPos);
					SmallGridPen.LineTo(x, nEndPos);
					pDC->SelectObject(pOldPen);
				}
			}
		} 
		else // 左值大于右值时 fLeftValue > fRightValue
		{
			// 右值为零的处理:
			if (fRightValue <= 0)
				fRightValue = 0.001f;
			
			if (fLeftValue <= 0)
			{
				fLeftValue = 1000;
			}
			
			CString strLeft = "";
			int nLeft = 0;
			CRect rcTrack;
			GetRegionRect(&rcTrack, FALSE);
			// 线数:由输入确定
			USHORT nLines = GetVertGridCount();
			double fwidth = 1.0 * rcTrack.Width() / (log10(fLeftValue) - log10(fRightValue));
			float x = rcTrack.right;
			float fIncream = 0.0f;	// 与前一格线的增量
			float fPower = 0;		// 当前格线的数量级
			float fStart = fRightValue;
			while(fStart < fLeftValue)
			{
				strLeft.Format("%e", fStart);
				strLeft = strLeft.Right(4);
				nLeft = atoi(strLeft);
				fPower = pow(10.0, nLeft);
				fIncream = (10.0 / nLines) * fPower;
				if (fIncream <= 0)
				{
					fIncream = 1;
				}
				x -= (log10(fStart + fIncream) - log10(fStart)) * fwidth;

				// 判断是否中格子
				strLeft.Format("%e", (fStart + fIncream));
				strLeft = strLeft.Left(1);
				nLeft = atoi(strLeft);
				BOOL bMGrid = FALSE;
				if (nLeft == 1)
				{
					bMGrid = TRUE;
				}

				fStart += fIncream;
				if (x < rcTrack.left)
				{
					x = rcTrack.left;
				}
				// end
				// 画垂直小格子
				if (bMGrid && m_GridInfo.vLineInfo.bHasMiddleGrid)
				{
				CPen* pOldPen = pDC->SelectObject(&penMiddleGrid);
				MiddleGridPen.MoveTo(x, nStartPos);
				MiddleGridPen.LineTo(x, nEndPos);
				pDC->SelectObject(pOldPen);
				continue;
				}
			
				if (m_GridInfo.vLineInfo.bHasSmallGrid)
				{
					CPen* pOldPen = pDC->SelectObject(&penSmallGrid);
					SmallGridPen.MoveTo(x, nStartPos);
					SmallGridPen.LineTo(x, nEndPos);
					pDC->SelectObject(pOldPen);
				}
			}
		}
		// end
	}
}

void CTrack::PrintVertGrid(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth)
{
	switch(m_GridInfo.vLineInfo.nGridType)
	{
	case 0:
		PrintVertGridLinear(pInfo, fMaxDepth, fMinDepth);
		break;
	case 1:
		PrintVertGridLog(pInfo, fMaxDepth, fMinDepth);
	}
}

void CTrack::PrintVertGridLinear(CULPrintInfo* pInfo, double  fMaxDepth,double fMinDepth)
{
	unsigned pattern[8];
	
	// 中格子画笔
	int nLinewidth = LPWidth(m_GridInfo.vLineInfo.nMiddleGridLinewidth);
	int nLinetype = m_GridInfo.vLineInfo.nMiddleGridLinetype;
	COLORREF colorLine = m_GridInfo.vLineInfo.colorMiddleGrid;
	CDashLine MiddleGridPen(*pInfo->GetPrintDC(), pattern,
		CDashLine::GetPattern(pattern, 0, nLinewidth, nLinetype));
	CPen penMiddleGrid(PS_SOLID, nLinewidth, colorLine);
	
	// 小格子画笔
	nLinewidth = LPWidth(m_GridInfo.vLineInfo.nSmallGridLinewidth);
	nLinetype = m_GridInfo.vLineInfo.nSmallGridLinetype;
	colorLine = m_GridInfo.vLineInfo.colorSmallGrid;
	CDashLine SmallGridPen(*pInfo->GetPrintDC(), pattern,
		CDashLine::GetPattern(pattern, 0, nLinewidth, nLinetype));
	CPen penSmallGrid(PS_SOLID, nLinewidth, colorLine);

	CRect rcRegion;
	GetRegionRect(&rcRegion, TRUE); // 该测井道所占区域信息    
	rcRegion = pInfo->CoordinateConvert(rcRegion);

	// 上下边界
	int nBottom = pInfo->GetPrintDepthCoordinate(fMaxDepth);
	int nTop = pInfo->GetPrintDepthCoordinate(fMinDepth);

	rcRegion.top = nTop;
	rcRegion.bottom = nBottom;

	if (m_GridInfo.bHasVLine)
	{
		USHORT nLines = m_GridInfo.vLineInfo.nLineCount;
		USHORT nGroup = m_GridInfo.vLineInfo.nGroupCount;

		int nGrid = nLines* nGroup;
		float nGridWidth = rcRegion.Width() * 1.0 / nGrid;
		int nPos = 0;


		for (int j = 1; j <= nGrid; j ++)
		{
			nPos = rcRegion.left + j * nGridWidth;

			if (nPos > rcRegion.right)
				nPos = rcRegion.right;

			if (j == nGrid)
				continue;

			CPoint ptStart(nPos, nTop);
			CPoint ptEnd(nPos, nBottom);

			pInfo->CoordinateCalibrate(ptStart);
			pInfo->CoordinateCalibrate(ptEnd);

			// 打印垂直中格子
			if ((j % m_GridInfo.vLineInfo.nLineCount) == 0)
			{
				if (m_GridInfo.vLineInfo.bHasMiddleGrid)
				{
					CPen* pOldPen = pInfo->GetPrintDC()->SelectObject(&penMiddleGrid);
					MiddleGridPen.MoveTo(ptStart);
					MiddleGridPen.LineTo(ptEnd);
					pInfo->GetPrintDC()->SelectObject(pOldPen);
					continue;
				}
			}

			// 打印垂直小格子,每五个小格子组成一个中格子，没有大格子。
			if (m_GridInfo.vLineInfo.bHasSmallGrid)
			{
				CPen* pOldPen = pInfo->GetPrintDC()->SelectObject(&penSmallGrid);
				SmallGridPen.MoveTo(ptStart);
				SmallGridPen.LineTo(ptEnd);
				pInfo->GetPrintDC()->SelectObject(pOldPen);
			}
		}
	}

	if (m_bPrintGap)
		PrintGaps(pInfo, MTD(fMaxDepth), MTD(fMinDepth));
	else
		PrintEdge(pInfo, MTD(fMaxDepth), MTD(fMinDepth));// Not print gaps, print track edges
}

void CTrack::PrintVertGridLog(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth)
{
	unsigned pattern[8];
	int nLinewidth, nLinetype;
	COLORREF colorLine;

	// 中格子画笔
	nLinewidth = LPWidth(m_GridInfo.vLineInfo.nMiddleGridLinewidth);
	nLinetype = m_GridInfo.vLineInfo.nMiddleGridLinetype;
	colorLine = m_GridInfo.vLineInfo.colorMiddleGrid;
	CDashLine MiddleGridPen(*pInfo->GetPrintDC(), pattern,
		CDashLine::GetPattern(pattern, 0, 2 * nLinewidth, nLinetype));
	CPen penMiddleGrid(PS_SOLID, nLinewidth, colorLine);
	CPen penEdge(PS_SOLID, 1.5 * nLinewidth, colorLine);

	// 小格子画笔
	nLinewidth = LPWidth(m_GridInfo.vLineInfo.nSmallGridLinewidth);
	nLinetype = m_GridInfo.vLineInfo.nSmallGridLinetype;
	colorLine = m_GridInfo.vLineInfo.colorSmallGrid;
	CDashLine SmallGridPen(*pInfo->GetPrintDC(), pattern,
		CDashLine::GetPattern(pattern, 0, 2 * nLinewidth, nLinetype));
	CPen penSmallGrid(PS_SOLID, nLinewidth, colorLine);

	CRect rcRegion;
	GetRegionRect(rcRegion, TRUE); // 该井道所占的区域信息。
	rcRegion = pInfo->CoordinateConvert(rcRegion);

	int nBottom = pInfo->GetPrintDepthCoordinate(fMaxDepth);
	int nTop = pInfo->GetPrintDepthCoordinate(fMinDepth);

	rcRegion.top = nTop;
	rcRegion.bottom = nBottom;

	double fLeftValue;
	double fRightValue;
	int nSize = m_vecCurve.size();		
	if (nSize < 1)
	{
		fLeftValue = 0.01;
		fRightValue = 100;
	}
	else
	{
		// 由第一条曲线的左右值决定格子的画法
		ICurve* pCurve = m_vecCurve.at(0);
		if (pCurve == NULL)
			return;

		fLeftValue = pCurve->LeftVal();
		fRightValue = pCurve->RightVal();
	}

	// add by bao 修改对数井道算法
	if (m_GridInfo.bHasVLine)
	{
		if (fLeftValue <= fRightValue) // 左值小于右值时
		{
			// 左值为零的处理:
			if (fLeftValue <= 0)
				fLeftValue = 0.001f;
			
			if (fRightValue <= 0)
			{
				fRightValue = 1000;
			}
			
			CString strLeft = "";
			int nLeft = 0;

			// 线数:由输入确定
			USHORT nLines = GetVertGridCount();
			double fwidth = 1.0 * rcRegion.Width() / (log10(fRightValue) - log10(fLeftValue));
			float x = rcRegion.left;
			float fIncream = 0.0f;	// 与前一格线的增量
			float fPower = 0;		// 当前格线的数量级
			float fStart = fLeftValue;

			int iGridCount = 0;
			while(fStart <= fRightValue)
			{
				strLeft.Format("%e", fStart);
				strLeft = strLeft.Right(4);
				nLeft = atoi(strLeft);
				fPower = pow(10.0, nLeft);
				fIncream = (10.0 / nLines) * fPower;
				if (fIncream <= 0)
				{
					fIncream = 1;
				}
				x += (log10(fStart + fIncream) - log10(fStart)) * fwidth;
				
				// 判断是否中格子
				strLeft.Format("%e", (fStart + fIncream));
				strLeft = strLeft.Left(1);
				nLeft = atoi(strLeft);
				BOOL bMGrid = FALSE;
				if (nLeft == 1)
				{
					bMGrid = TRUE;
				}
				
				fStart += fIncream;
				
				float fTemp = 0.0; // 当SCROLL_UP时，临时储存x的值
				if (pInfo->GetPrintDirection() == SCROLL_UP)
				{
					fTemp = x;
					x = rcRegion.right - (x - rcRegion.left);
					if (x < rcRegion.left)
						x = rcRegion.left ;
				}
				else
				{
					if (x > rcRegion.right)
					{
						x = rcRegion.right;
					}
					fTemp = x;
				}
				
				if ((x == rcRegion.left) || (x == rcRegion.right) || (fStart == fRightValue))
				{
					x = fTemp; // 如果向上实时打印时，将X值变回来，x代表的是距离值小得格线的距离
					continue;
				}
				
				CPoint ptStart(x, nTop);
				CPoint ptEnd(x, nBottom);
				
				pInfo->CoordinateCalibrate(ptStart);
				pInfo->CoordinateCalibrate(ptEnd);
				
				iGridCount++; // 已经画的格子数
				// 打印垂直小格子
				
				if (bMGrid && m_GridInfo.vLineInfo.bHasMiddleGrid)
				{
					CPen* pOldPen = pInfo->GetPrintDC()->SelectObject(&penMiddleGrid);
					MiddleGridPen.MoveTo(ptStart);
					MiddleGridPen.LineTo(ptEnd);
					pInfo->GetPrintDC()->SelectObject(pOldPen);
					x = fTemp; // 如果向上实时打印时，将X值变回来，x代表的是距离值小得格线的距离
					continue;
				}
				
				if (m_GridInfo.vLineInfo.bHasSmallGrid)
				{
					CPen* pOldPen = pInfo->GetPrintDC()->SelectObject(&penSmallGrid);
					SmallGridPen.MoveTo(ptStart);
					SmallGridPen.LineTo(ptEnd);
					pInfo->GetPrintDC()->SelectObject(pOldPen);
					x = fTemp; // 如果向上实时打印时，将X值变回来，x代表的是距离值小得格线的距离
				}
			}
		}
		else // 左值大于右值时 fLeftValue > fRightValue
		{
			// 右值为零的处理:
			if (fRightValue <= 0)
				fRightValue = 0.001f;
			
			if (fLeftValue <= 0)
			{
				fLeftValue = 1000;
			}

			CString strLeft = "";
			int nLeft = 0;
			
			// 线数:由输入确定
			USHORT nLines = GetVertGridCount();
			double fwidth = 1.0 * rcRegion.Width() / (log10(fLeftValue) - log10(fRightValue));
			float x = rcRegion.right;
			float fIncream = 0.0f;	// 与前一格线的增量
			float fPower = 0;		// 当前格线的数量级
			float fStart = fRightValue;

			int iGridCount = 0;
			while(fStart < fLeftValue)
			{
				strLeft.Format("%e", fStart);
				strLeft = strLeft.Right(4);
				nLeft = atoi(strLeft);
				fPower = pow(10.0, nLeft);
				fIncream = (10.0 / nLines) * fPower;
				if (fIncream <= 0)
				{
					fIncream = 1;
				}
				x -= (log10(fStart + fIncream) - log10(fStart)) * fwidth;
				
				// 判断是否中格子
				strLeft.Format("%e", (fStart + fIncream));
				strLeft = strLeft.Left(1);
				nLeft = atoi(strLeft);
				BOOL bMGrid = FALSE;
				if (nLeft == 1)
				{
					bMGrid = TRUE;
				}
				
				fStart += fIncream;
				
				float fTemp = 0.0; // 当SCROLL_UP时，临时储存x的值
				if (pInfo->GetPrintDirection() == SCROLL_UP)
				{
					fTemp = x;
					x = rcRegion.left + (rcRegion.right - x);
			//		TRACE("%f x:%f right:%f left:%f\n", fStart, x, (float)rcRegion.right, (float)rcRegion.left);
					if (x > rcRegion.right)
					{
						x = rcRegion.right ;
					}
				}
				else
				{
					if (x < rcRegion.left)
					{
						x = rcRegion.left;
					}
					fTemp = x;
				}
				
				if ((x == rcRegion.left) || (x == rcRegion.right) || (fStart == fRightValue))
				{
					x = fTemp; // 如果向上实时打印时，将X值变回来，x代表的是距离值小得格线的距离
					continue;
				}
				
				CPoint ptStart(x, nTop);
				CPoint ptEnd(x, nBottom);
				
				pInfo->CoordinateCalibrate(ptStart);
				pInfo->CoordinateCalibrate(ptEnd);
				
				iGridCount++; // 已经画的格子数
				// 打印垂直小格子
				
				if (bMGrid && m_GridInfo.vLineInfo.bHasMiddleGrid)
				{
					CPen* pOldPen = pInfo->GetPrintDC()->SelectObject(&penMiddleGrid);
					MiddleGridPen.MoveTo(ptStart);
					MiddleGridPen.LineTo(ptEnd);
					pInfo->GetPrintDC()->SelectObject(pOldPen);
					x = fTemp; // 如果向上实时打印时，将X值变回来，x代表的是距离值小得格线的距离
					continue;
				}
				
				if (m_GridInfo.vLineInfo.bHasSmallGrid)
				{
					CPen* pOldPen = pInfo->GetPrintDC()->SelectObject(&penSmallGrid);
					SmallGridPen.MoveTo(ptStart);
					SmallGridPen.LineTo(ptEnd);
					pInfo->GetPrintDC()->SelectObject(pOldPen);
					x = fTemp; // 如果向上实时打印时，将X值变回来，x代表的是距离值小得格线的距离
				}
			}
		}
		// end
	}

	if (m_bPrintGap)
	{
		PrintGaps(pInfo, MTD(fMaxDepth), MTD(fMinDepth));
		return;
	}

	// 打印井道边
	PrintEdge(pInfo, MTD(fMaxDepth), MTD(fMinDepth));
}

void CTrack::PrintHorzGrid(CULPrintInfo* pInfo, double fMaxDepth,
	double fMinDepth)
{
	if (m_pProject)
	{
		long nStartDepth = MTD(fMinDepth);
		long nEndDepth = MTD(fMaxDepth);
		DrawResults(pInfo->GetPrintDC(), nStartDepth, nEndDepth, pInfo);
		return;
	}

//	m_TestMarker.PrintMark(pInfo, fMinDepth, fMaxDepth);

	if (!m_GridInfo.bHasHLine)
		return;

	unsigned pattern[8];
	int nLinewidth, nLinetype;
	COLORREF colorLine;

	int nStartDepth = MTD(fMinDepth);
	int nEndDepth = MTD(fMaxDepth);

	CRect rcRegion;
	GetRegionRect(rcRegion, TRUE);     
	rcRegion = pInfo->CoordinateConvert(rcRegion);
	for(int K1=0;K1<3;K1++)
	{
		m_GridInfo.hLine[K1].nGridStep = m_dHorzGridDepth[K1];
	}
	for (int i = 0; i < 3; i++)
	{
		if (m_GridInfo.hLine[i].bHasGrid)
		{
			nLinewidth = LPWidth(m_GridInfo.hLine[i].nGridLinewidth);
			nLinetype = m_GridInfo.hLine[i].nGridLinetype;
			colorLine = m_GridInfo.hLine[i].clrGrid;

			CDashLine GridLine(*pInfo->GetPrintDC(), pattern,
				CDashLine::GetPattern(pattern, 0, nLinewidth, nLinetype));
			CPen penGrid(PS_SOLID, nLinewidth, colorLine);

			int nStart = (nStartDepth + 1) / m_GridInfo.hLine[i].nGridStep - 1;
			int nEnd = (nEndDepth - 1) / m_GridInfo.hLine[i].nGridStep + 1;

			CPen* pOldPen = pInfo->GetPrintDC()->SelectObject(&penGrid);
			for (int j = nStart; j <= nEnd; j++)
			{
				long lDepth = j* m_GridInfo.hLine[i].nGridStep;
				if (lDepth < nStartDepth)
				{
					continue;
				}
				if (lDepth > nEndDepth)
				{
					break;
				}

				int y = pInfo->GetPrintDepthCoordinate(DTM(lDepth));
				CPoint ptStart(rcRegion.left, y);
				CPoint ptEnd(rcRegion.right, y);
				pInfo->CoordinateCalibrate(ptStart);
				pInfo->CoordinateCalibrate(ptEnd);			

				GridLine.MoveTo(ptStart);
				GridLine.LineTo(ptEnd);
			}
			pInfo->GetPrintDC()->SelectObject(pOldPen);
		}
	}
}

void CTrack::PrintMark(CULPrintInfo* pInfo, double fMaxDepth,double fMinDepth)
{
	m_TestMarker.PrintMark(pInfo, fMinDepth, fMaxDepth);
}

//////////////////////////////////////////////////////////////////////
// 鼠标测试
//////////////////////////////////////////////////////////////////////

// 判断点是否在当前井道
BOOL CTrack::PtInTrack(CPoint pt) // 逻辑座标
{
	CRect rc;
	GetRegionRect(&rc, FALSE);
	if (pt.x <rc.right && pt.x> rc.left)
		return TRUE;
	else
		return FALSE;
}

// 是否在当前井道头
BOOL CTrack::PtInTrackHead(CPoint pt)
{
	CRect rc;
	GetHeadRect(rc, FALSE);
	return rc.PtInRect(pt);
}

int CTrack::PtInCurvePos(CPoint pt)
{
	CRect rc;
	GetHeadRect(rc, FALSE);
	rc.bottom -= TRACK_MARGIN;

	CRect rcTest = rc;
	rcTest.left += 10;
	rcTest.right -= 10;

	int h = 0;	
	int i = 0;
	for (i = 0; i < m_vecCurve.size(); i ++)
	{
		CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
		int t = pCurve->CalcTitle();
		if (t == 0)
			continue;
		
		t *= 100;
		rcTest.bottom = rc.bottom - h;
		rcTest.top = rcTest.bottom - t;
		if (rcTest.PtInRect(pt))
			return i;
		
		h += t;
	}

	rcTest.top = rc.top;
	if (rcTest.PtInRect(pt))
		return i;

	return -1;
}

// 是否在井道拖动区
BOOL CTrack::PtInTrackDragWnd(CPoint pt)
{
	CRect rc;
	GetHeadRect(&rc, FALSE);
	rc.top = rc.bottom - TRACK_MARGIN;
	return  rc.PtInRect(pt);
}

// 点击井道头测试井道位置
BOOL CTrack::HitHeadTest(CPoint point, int& nIndex, BOOL& bLeftOrRight)
{
	CRect rc1, rc2, rcHit;

	GetHeadRect(&rc1, FALSE);
	rc1.top = TRACK_MARGIN * 2;
	rc1.bottom = rc1.top + 40;

	int nLeftMargin = 0;
	int nRightMargin = 0;
	nIndex = -1;
	int i = 0;
	for (i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(i);

		nLeftMargin = pCurve->LeftMargin(FALSE);
		nRightMargin = pCurve->RightMargin(FALSE);

		rc2 = rc1;
		rc2.left = nLeftMargin;
		rc2.right = nRightMargin;

		rcHit = rc2;
		rcHit.right = rc2.left + 1;
		rcHit.left = rc2.left - 1;

		rcHit.top = rc2.top + rc2.Height() / 2 + 9;
		rcHit.bottom = rc2.top + rc2.Height() / 2 + 14;

		if (rcHit.PtInRect(point))		// left
		{
			nIndex = i;
			bLeftOrRight = TRUE;
			return TRUE;
		}

		rcHit.right = rc2.right + 1;
		rcHit.left = rc2.right - 1;

		if (rcHit.PtInRect(point))		// right
		{
			nIndex = i;
			bLeftOrRight = FALSE;
			return TRUE;
		}

		rc1.OffsetRect(0, 40);
	}

	return FALSE;
}

// 测试选中曲线上某点
BOOL CTrack::HitTest(CPoint point, int& nCurveIndex, int& nCurveValueIndex, BOOL bSelect /* = TRUE */)
{
	CRect rcTrack;
	GetRegionRect(&rcTrack, FALSE);//add by gj 20130717 水平比例尺改变时，曲线点的属性框不显示

	if (point.x <rcTrack.left || point.x> rcTrack.right)
		return FALSE;


	BOOL bResult = FALSE;

	int nTop = rcTrack.top;
	int nBottom = rcTrack.bottom;

	CCurve* pCurve = NULL;

	float x = 0.0, fY = 0.0;
	int y = 0;
	int nLen = 0;
	float fXValuePerPixel = 0.0;
	int nLeftMargin = 0;
	int nRightMargin = 0;

	for (int j = 0; j < m_vecCurve.size(); j++)
	{
		pCurve = (CCurve *) m_vecCurve.at(j);
		if (!pCurve->m_bSelected)
			continue ;

		nLeftMargin = pCurve->LeftMargin(FALSE);
		nRightMargin = pCurve->RightMargin(FALSE);

		// cl 得到当前位置曲线点及判断是否在曲线上
		int nCurveDataSize = pCurve->GetDataSize();
		if (nCurveDataSize > 0) 
		{
			nCurveIndex = j;
			if (m_nDriveMode==UL_DRIVE_DEPT)
			{
				
				long lDepth = GetDepthFromCoordinate(point.y);
				nCurveValueIndex = pCurve->GetFirstIndexByDepth(lDepth);
				if (nCurveValueIndex >= 0 && nCurveValueIndex < nCurveDataSize)
				{
					if (pCurve->IsLinear() || pCurve->IsLogarithmic())
					{
						x = pCurve->GetCurveXPos(lDepth);
						if (x > point.x - 8 && x < point.x + 8)
							return TRUE;
					}
				}
			}
			else if (m_nDriveMode==UL_DRIVE_TIME)
			{	
				long lTime = GetTimeFromCoordinate(point.y);
				nCurveValueIndex = pCurve->GetFirstIndexByTime(lTime,SCROLL_DOWN);
				if (nCurveValueIndex >= 0 && nCurveValueIndex < nCurveDataSize)
				{
					if (pCurve->IsLinear() || pCurve->IsLogarithmic())
					{
						x = pCurve->GetCurveXPos(lTime);
						if (x > point.x - 8 && x < point.x + 8)
							return TRUE;
					}
				}	
			}
		}
	}
	return bResult;
}

// 当前井道头选中曲线
CCurve* CTrack::CurveHitHeadTest(CPoint pt, int& nPos)
{
	CRect rc;
	GetHeadRect(rc, FALSE);
	rc.bottom -= 2*TRACK_MARGIN;

	CRect rcTest = rc;
	rcTest.left += 10;
	rcTest.right -= 10;

	int h = 0;
    if (GetType()==UL_TRACK_TEXT)
	{
		int nCurves = m_vecCurve.size();
        int nCurveWidth = m_rcHead.Width()/nCurves;
        rcTest.right=rcTest.left+nCurveWidth;  
		for (int i = 0; i < nCurves; i ++)
		{
			CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
	
			rcTest.bottom = rc.bottom ;
			rcTest.top = rcTest.bottom -100;
			
			if (rcTest.PtInRect(pt))
			{
				nPos = i;
				return pCurve;
			}
			rcTest.left=rcTest.right;
			rcTest.right=rcTest.left+nCurveWidth;
		}	
	}
    else
	{
		int h = 0;
		for (int i = 0; i < m_vecCurve.size(); i ++)
		{
			CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
			int t = pCurve->CalcTitle();
			if (t == 0)
				continue;
			
			t *= 100;
			rcTest.bottom = rc.bottom - h;
			rcTest.top = rcTest.bottom - t;
			if (rcTest.PtInRect(pt))
			{
				nPos = i;
				return pCurve;
			}
			
			h += t;
		}
	}
	
	
	
	return NULL;
}

//////////////////////////////////////////////////////////////////////
// 打印
//////////////////////////////////////////////////////////////////////
void CTrack::PrintHead(CULPrintInfo* pPrintInfo)
{
	CString strVal;
	CPen pen(m_nLineStyle, 3, (AfxGetComConfig()->m_Plot.Colored ? m_crColor : 0));

	CPen* pOldPen = pPrintInfo->GetPrintDC()->SelectObject(&pen);	
	//////////////////////////////////////////////////////////////////////////	
	// 画整个井道矩形

	// 去掉井道头之前的空白
	CRect rectAdjust;
	GetHeadRect(rectAdjust, TRUE);

	rectAdjust.bottom += 2 * TRACK_MARGIN - 1;
	rectAdjust.top = rectAdjust.bottom - 100*m_nPrintCount - TRACK_MARGIN;

	CRect rcHead = pPrintInfo->CoordinateConvert(rectAdjust);
	rcHead.left = (int) (rcHead.left * pPrintInfo->GetPrintReviseX());
	rcHead.right = (int) (rcHead.right * pPrintInfo->GetPrintReviseX());

    //pPrintInfo->CoordinateCalibrate(rcHead);
	pPrintInfo->GetPrintDC()->Rectangle(rcHead);

	CRect rcCurve = rectAdjust;

	if (pPrintInfo->GetPrintDirection() == SCROLL_UP)
	{
		rcCurve.top += TRACK_MARGIN;
		// bao 2011/6/9
		CRect rcTrack;
		GetRegionRect(&rcTrack, TRUE);
		
		m_DrawCurve.clear();
		// bao end

		for (int i = 0; i < m_vecCurve.size(); i++)
		{
			CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
			if(!pCurve->IsPrintCurveHead())
				continue;
			
			int nHeight = pCurve->CalcTitle(CS_PRINT);
			if (nHeight == 0)
				continue;

			nHeight *= 100;

			// bao 2011/6/9
			BOOL bDraw = FALSE;
			for (int x = 0; x < m_DrawCurve.size(); x++)
			{
				CCurve* pCurve2 = (CCurve*) m_DrawCurve.at(x);
				if (pCurve2 == pCurve)
				{
					bDraw = TRUE;
					break;
				}
			}
			if (bDraw)
				continue;
			// bao end

			int nLeftMargin = pCurve->LeftMargin(TRUE);
			int nRightMargin = pCurve->RightMargin(TRUE);

			// bao 2011/6/9
			CCurve* pCompareCurve = NULL;
			if (rcTrack.left != nLeftMargin)
			{
				for (int z = i + 1; z < m_vecCurve.size(); z++)
				{
					pCompareCurve = (CCurve*) m_vecCurve.at(z);
					
					// add by bao 2013/7/31 已经与前面匹配过的曲线不再重复匹配
					BOOL bCompared = FALSE;
					for (int y = 0; y < m_DrawCurve.size(); y++)
					{
						CCurve* pCompared = m_DrawCurve.at(y);
						if (pCompareCurve == pCompared)
						{
							bCompared = TRUE;
							break;
						}
					}
					if (bCompared)
					{
						pCompareCurve = NULL;
						continue;
					}

					int iRightMargin = pCompareCurve->RightMargin(TRUE);
					if (nLeftMargin == iRightMargin)
					{
						break;
					}
					pCompareCurve = NULL;
				}
			}
			if (rcTrack.right != nRightMargin)
			{
				for (int z = i + 1; z < m_vecCurve.size(); z++)
				{
					pCompareCurve = (CCurve*) m_vecCurve.at(z);
					
					// add by bao 2013/7/31 已经与前面匹配过的曲线不再重复匹配
					BOOL bCompared = FALSE;
					for (int y = 0; y < m_DrawCurve.size(); y++)
					{
						CCurve* pCompared = m_DrawCurve.at(y);
						if (pCompareCurve == pCompared)
						{
							bCompared = TRUE;
							break;
						}
					}
					if (bCompared)
					{
						pCompareCurve = NULL;
						continue;
					}
					
					int iLeftMargin = pCompareCurve->LeftMargin(TRUE);
					if (nRightMargin == iLeftMargin)
					{
						break;
					}
					pCompareCurve = NULL;
				}
			}
			int iHeight = 0;
			if (pCompareCurve != NULL)
			{
				iHeight = pCompareCurve->CalcTitle(CS_PRINT);
				iHeight *= 100;
			}
			
			CString strOldName;
			int iSelectCurve  = 0; // 1 表示修改了pCurve的名，2表示修改了pCompareCurve的名
			if ((iHeight > nHeight) && (pCompareCurve != NULL))
			{
				strOldName = pCurve->Name();
				CString strTemp = strOldName;
				for (int h = 0; h < (iHeight - nHeight)/100; h++)
				{
					strTemp = _T("\\") + strTemp;
				}
				pCurve->SetName(strTemp);
				iSelectCurve = 1;
			}
			if ((iHeight < nHeight) && (pCompareCurve != NULL))
			{
				strOldName = pCompareCurve->Name();
				CString strTemp = strOldName;
				for (int h = 0; h < (nHeight - iHeight)/100; h++)
				{
					strTemp = _T("\\") + strTemp;
				}
				pCompareCurve->SetName(strTemp);
				iSelectCurve = 2;
			}
			
			if (iHeight != 0)
			{
				nHeight = nHeight > iHeight? nHeight:iHeight;
			}
			// bao end

			rcCurve.left = nLeftMargin;
			rcCurve.right = nRightMargin;
			rcCurve.bottom = rcCurve.top + nHeight;


			CRect rc = pPrintInfo->CoordinateConvert(rcCurve);
			//pPrintInfo->CoordinateCalibrate(rc);
			rc.left = (int) (rc.left * pPrintInfo->GetPrintReviseX());
			rc.right = (int) (rc.right * pPrintInfo->GetPrintReviseX());

			pCurve->PrintHeadCurve(pPrintInfo, rc);
			
			// bao 2011/6/9
			if (pCompareCurve != NULL && iHeight != 0)
			{
				CRect rcCurve2 = rcCurve;
				rcCurve2.left = pCompareCurve->LeftMargin(TRUE);
				rcCurve2.right = pCompareCurve->RightMargin(TRUE);
				rc = pPrintInfo->CoordinateConvert(rcCurve2);
				//pPrintInfo->CoordinateCalibrate(rc);
				rc.left = (int) (rc.left * pPrintInfo->GetPrintReviseX());
				rc.right = (int) (rc.right * pPrintInfo->GetPrintReviseX());

				pCompareCurve->PrintHeadCurve(pPrintInfo, rc);	
				m_DrawCurve.push_back(pCompareCurve);
			}
			
			switch (iSelectCurve)
			{
			case 1:
				pCurve->SetName(strOldName);
				break;
			case 2:
				pCompareCurve->SetName(strOldName);
				break;
			default:
				break;
			}
			
			// bao end

			rcCurve.top = rcCurve.bottom;				
		}
	}
	else
	{
		rcCurve.bottom -= TRACK_MARGIN;
		int nCurveCount = m_vecCurve.size(); 

		// bao 2011/6/9
		CRect rcTrack;
		GetRegionRect(&rcTrack, TRUE);
		
		m_DrawCurve.clear();
		// bao end

		for (int i = 0; i < nCurveCount; i++)
		{
			CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
			if(!pCurve->IsPrintCurveHead())
				continue;

			int nHeight = pCurve->CalcTitle(CS_PRINT);
			if (nHeight == 0)
				continue;
			
			nHeight *= 100;

			// bao 2011/6/9
			BOOL bDraw = FALSE;
			for (int x = 0; x < m_DrawCurve.size(); x++)
			{
				CCurve* pCurve2 = (CCurve*) m_DrawCurve.at(x);
				if (pCurve2 == pCurve)
				{
					bDraw = TRUE;
					break;
				}
			}
			if (bDraw)
				continue;

			// bao end
			int nLeftMargin = pCurve->LeftMargin(TRUE);
			int nRightMargin = pCurve->RightMargin(TRUE);

			// bao 2011/6/9
			CCurve* pCompareCurve = NULL;
			if (rcTrack.left != nLeftMargin)
			{
				for (int z = i + 1; z < nCurveCount; z++)
				{
					pCompareCurve = (CCurve*) m_vecCurve.at(z);
					
					// add by bao 2013/7/31 已经与前面匹配过的曲线不再重复匹配
					BOOL bCompared = FALSE;
					for (int y = 0; y < m_DrawCurve.size(); y++)
					{
						CCurve* pCompared = m_DrawCurve.at(y);
						if (pCompareCurve == pCompared)
						{
							bCompared = TRUE;
							break;
						}
					}
					if (bCompared)
					{
						pCompareCurve = NULL;
						continue;
					}
					
					int iRightMargin = pCompareCurve->RightMargin(TRUE);
					if (nLeftMargin == iRightMargin)
					{
						break;
					}
					pCompareCurve = NULL;
				}
			}
			if (rcTrack.right != nRightMargin)
			{
				for (int z = i + 1; z < nCurveCount; z++)
				{
					pCompareCurve = (CCurve*) m_vecCurve.at(z);
					
					// add by bao 2013/7/31 已经与前面匹配过的曲线不再重复匹配
					BOOL bCompared = FALSE;
					for (int y = 0; y < m_DrawCurve.size(); y++)
					{
						CCurve* pCompared = m_DrawCurve.at(y);
						if (pCompareCurve == pCompared)
						{
							bCompared = TRUE;
							break;
						}
					}
					if (bCompared)
					{
						pCompareCurve = NULL;
						continue;
					}
					
					int iLeftMargin = pCompareCurve->LeftMargin(TRUE);
					if (nRightMargin == iLeftMargin)
					{
						break;
					}
					pCompareCurve = NULL;
				}
			}
			int iHeight = 0;
			if (pCompareCurve != NULL)
			{
				iHeight = pCompareCurve->CalcTitle(CS_PRINT);
				iHeight *= 100;
			}
			
			CString strOldName;
			int iSelectCurve  = 0; // 1 表示修改了pCurve的名，2表示修改了pCompareCurve的名
			if ((iHeight > nHeight) && (pCompareCurve != NULL))
			{
				strOldName = pCurve->Name();
				CString strTemp = strOldName;
				for (int h = 0; h < (iHeight - nHeight)/100; h++)
				{
					strTemp = _T("\\") + strTemp;
				}
				pCurve->SetName(strTemp);
				iSelectCurve = 1;
			}
			if ((iHeight < nHeight) && (pCompareCurve != NULL))
			{
				strOldName = pCompareCurve->Name();
				CString strTemp = strOldName;
				for (int h = 0; h < (nHeight - iHeight)/100; h++)
				{
					strTemp = _T("\\") + strTemp;
				}
				pCompareCurve->SetName(strTemp);
				iSelectCurve = 2;
			}

			if (iHeight != 0)
			{
				nHeight = nHeight > iHeight? nHeight:iHeight;
			}
			// bao end

			rcCurve.left = nLeftMargin;
			rcCurve.right = nRightMargin;
			rcCurve.top = rcCurve.bottom - nHeight;

			CRect rc = pPrintInfo->CoordinateConvert(rcCurve);
			//pPrintInfo->CoordinateCalibrate(rc);
			rc.left = (int) (rc.left * pPrintInfo->GetPrintReviseX());
			rc.right = (int) (rc.right * pPrintInfo->GetPrintReviseX());

			pCurve->PrintHeadCurve(pPrintInfo, rc);
			
			// bao 2011/6/9
			if (pCompareCurve != NULL && iHeight != 0)
			{
				CRect rcCurve2 = rcCurve;
				rcCurve2.left = pCompareCurve->LeftMargin(TRUE);
				rcCurve2.right = pCompareCurve->RightMargin(TRUE);
				rc = pPrintInfo->CoordinateConvert(rcCurve2);
				//pPrintInfo->CoordinateCalibrate(rc);
				rc.left = (int) (rc.left * pPrintInfo->GetPrintReviseX());
				rc.right = (int) (rc.right * pPrintInfo->GetPrintReviseX());

				pCompareCurve->PrintHeadCurve(pPrintInfo, rc);	
				m_DrawCurve.push_back(pCompareCurve);	
			}
			
			switch (iSelectCurve)
			{
			case 1:
				pCurve->SetName(strOldName);
				break;
			case 2:
				pCompareCurve->SetName(strOldName);
				break;
			default:
				break;
			}

			// bao end

			rcCurve.bottom = rcCurve.top;
		}
	}

	m_DrawCurve.clear();

	pPrintInfo->GetPrintDC()->SelectObject(pOldPen);
}

void CTrack::PrintTrack(CULPrintInfo* pInfo, double  fMaxDepth,
	double fMinDepth)
{
	int nCurve = m_vecCurve.size();
	for (int i = 0; i < nCurve; i++)
	{
		CCurve* pCurve = m_vecCurve.at(i);
		if (!pCurve->IsVisible())
			continue;

		pCurve->LogPrint(pInfo, fMaxDepth, fMinDepth);
	}
}

void CTrack::PrintFill(CULPrintInfo* pInfo, double fMaxDepth,double fMinDepth)
{
	int nCurve = m_vecCurve.size();
	int i = 0;
	for ( ; i < nCurve; i++)
	{
		CCurve* pCurve = m_vecCurve.at(i);
		if (!pCurve->IsVisible())
			continue;

		if (pCurve->IsInpre())
			pCurve->Print(pInfo, fMaxDepth, fMinDepth);
	}
}

/*++

函数名称：
  
  StaticPrint

函数描述：

  曲线静态打印

函数参数：

  pInfo-CULPrintInfo类型的一个指针，用于存储打印信息和转换打印参数

  fMaxDepth-要打印的最大深度

  fMinDepth-要打印的最小深度
  
返回类型：

  void

--*/
void CTrack::StaticPrint(CULPrintInfo* pInfo, double fMaxDepth,double fMinDepth)
{
	int nCurve = m_vecCurve.size();
	int i = 0;
/*	for ( ; i < nCurve; i++)
	{
		CCurve* pCurve = m_vecCurve.at(i);
		if (!pCurve->IsVisible())
			continue;

		if (pCurve->IsInpre())
			pCurve->Print(pInfo, fMaxDepth, fMinDepth);
	}*/

	for (i = 0; i < nCurve; i++)
	{
		CCurve* pCurve = m_vecCurve.at(i);
		if (!pCurve->IsVisible())
			continue;

		if (!pCurve->IsInpre())
			pCurve->Print(pInfo, fMaxDepth, fMinDepth);
	}
}

void CTrack::PrintCurveFlags(CULPrintInfo* pInfo, long lDepth, int nCurve,
	BOOL bMode)
{
	if (nCurve < m_vecCurve.size())
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(nCurve);
		CPen* pPen = new CPen(pCurve->LineStyle(), 2/*pCurve->LineWidth()*/, pCurve->Color());
		if (!pCurve->IsPrint())
			return ;

		int nX = pCurve->GetCurveXPos(lDepth);
		if (nX < 0)
			return ;

		CString strCurve = pCurve->m_strName;
		strCurve.Remove('\\');
		// 判断是否有括号，有则只输出括号里内容，无全部输出
		m_bBracketFlags = TRUE;
		if (m_bBracketFlags)
		{
			int iLBracketIndex = strCurve.Find('(');
			int iRBracketIndex = strCurve.Find(')');
			if ((iLBracketIndex != -1) && (iRBracketIndex != -1) && (iRBracketIndex > iLBracketIndex))
			{
				strCurve = strCurve.Mid(iLBracketIndex + 1, iRBracketIndex - iLBracketIndex - 1);
			}
		}
		
		//判断，在曲线不回绕情况下，不显示标签
		BOOL bInRange = TRUE;
		if(!pCurve->IsRewind())
		{
			int nCount = pCurve->GetDataSize(); 
			int nIndex = pCurve->GetIndexByDepth(lDepth , 0 , nCount - 1);
			double fValue = pCurve->GetdblValue(nIndex);

			double fLeftValue = pCurve->LeftVal();
			double fRightValue = pCurve->RightVal();
			
			if(fValue < min(fLeftValue , fRightValue))
				bInRange = FALSE;
			if(fValue > max(fLeftValue , fRightValue))
				bInRange = FALSE;
		}

		long lSD = pCurve->GetDepth(0);
		long lED = pCurve->GetDepthED ();
		long lTopDepth = min(lED,lSD);
		long lBottomDepth = max(lED,lSD);
	
		if(lDepth >= lTopDepth && lDepth <= lBottomDepth && bInRange)
			DrawArrow(pInfo, nX, lDepth, bMode, strCurve, TRANSPARENT, pPen);
		delete	pPen;
	}
}

// 打印自动标注
BOOL CTrack::PrintCurveFirst(CULPrintInfo* pInfo, long lMaxDepth,
	long lMinDepth, BOOL bMode)
{
	LOGFONT lf;
	g_font[ftCT].GetLogFont(&lf);
//	long lTextHeight = abs(lf.lfHeight);
	long lTextHeight = 30;

	long nBottom = pInfo->GetPrintDepthCoordinate(DTM(lMaxDepth));
	long nTop = pInfo->GetPrintDepthCoordinate(DTM(lMinDepth));
	
//	CPointArray ptArray;

	int nCurve = m_vecCurve.size();
	int i = 0;
	int j = 0;
	for (i = 0, j = 0; i < nCurve; i++)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
		CPen* pPen = new CPen(pCurve->LineStyle(), 2/*pCurve->LineWidth()*/, pCurve->Color());
		if (!pCurve->IsPrint())
			continue ;

		long lFDepth = pCurve->FRDepth();
		if (lFDepth == LONG_MIN)
			continue ;

		if (pCurve->m_nPF > 1)
			continue ;

		if ((pCurve->m_nPF == 1) || IsBetween(lMaxDepth, lMinDepth, lFDepth))
		{
			int nX = pCurve->GetCurveXPos(lFDepth);
			if (nX < 0)
			{
				TRACE("Curve %s x pos is nagetive.", pCurve->m_strName);
				pCurve->FRDepth(LONG_MIN);
				continue ;
			}

			CString str;
			CString strCurve = pCurve->m_strName;
			strCurve.Remove('\\');
			// 判断是否有括号，有则只输出括号里内容，无全部输出
			m_bBracketFlags = TRUE;
			if (m_bBracketFlags)
			{
				int iLBracketIndex = strCurve.Find('(');
				int iRBracketIndex = strCurve.Find(')');
				if ((iLBracketIndex != -1) && (iRBracketIndex != -1) && (iRBracketIndex > iLBracketIndex))
				{
					strCurve = strCurve.Mid(iLBracketIndex + 1, iRBracketIndex - iLBracketIndex - 1);
				}
			}
			str.Format(_T("FR-%s@%.2lf%s"), strCurve,
					g_units->XDepth(lFDepth), g_units->DUnit());

			CPoint pt;
			pt.x = nX;
			pt.y = pInfo->GetPrintDepthCoordinate(DTM(lFDepth));
			m_ptArray.Add(pt);

			int nDY = 0;

			while(TRUE)
			{
				BOOL bCross = FALSE;
				int k = 0;
				for(k = 0 ; k < m_ptArray.GetSize() - 1; k++)
				{
					if(abs(m_ptArray[k].y - pt.y - nDY) < lTextHeight && abs(m_ptArray[k].x - pt.x ) < 800)
					{
						bCross = TRUE;
						
						if(nDY > 0)
						{
							nDY = nDY * (-1);
						}
						else
						{
							nDY = nDY * (-1) + lTextHeight;
						}

						break;
					}
				}
				
				if(bCross)
					continue;

				break;
			}

		//	DrawArrow(pInfo, nX, lFDepth, bMode, str, OPAQUE, pPen , nDY);
			DrawArrow(pInfo, nX, lFDepth, bMode, str, TRANSPARENT, pPen , nDY);
			int ptArraySize = m_ptArray.GetSize();
			m_ptArray[ptArraySize - 1].y += nDY;

			pCurve->m_nPF++;
		}

		j++;
	}
	return j;
}
BOOL CTrack::DrawArrow(CULPrintInfo* pInfo, int nX, long lDepth, BOOL bMode,
	CString strText, int nBkMode /* = TRANSPARENT */, CPen* pPen/* = new CPen(PS_SOLID, 1, RGB(0, 0, 0))*/ , int nDY , int nDX)
{
	CDC* pDC = pInfo->GetPrintDC();
	pDC->SetBkMode(nBkMode);

	CPen pen(PS_SOLID, 1, RGB(0, 0, 0));
	CPen* pOldPen = pDC->SelectObject(/*&pen*/pPen);
	CBrush* pOldBr = (CBrush*) pDC->SelectStockObject(BLACK_BRUSH);

	BOOL bReverse = FALSE;
	if (nX < m_rcRegion.CenterPoint().x)
	{
		if (bMode)
			bReverse = TRUE;
	}
	else
	{
		if (!bMode)
			bReverse = TRUE;
	}

	strText.TrimLeft(); strText.TrimRight();
	LOGFONT lf;
	g_font[ftCT].GetLogFont(&lf);
	CSize szFont(0, lf.lfHeight);
	if (bReverse)
	{
		POINT pt[4];
		pt[0].x = pInfo->CoordinateConvertX(nX);
		pt[0].x -= 1;
		pt[0].y = pInfo->GetPrintDepthCoordinate(DTM(lDepth));
		pt[1].x = pt[0].x - 22;
		pt[1].y = pt[0].y - 10;
		pt[2].x = pt[0].x - 15;
		pt[2].y = pt[0].y;
		pt[3].x = pt[0].x - 22;
		pt[3].y = pt[0].y + 10;
		pDC->Polygon(pt, 4);
		if(nDY == 0)
		{
			pDC->MoveTo(pt[0].x, pt[0].y);
			pDC->LineTo(pt[0].x - 100, pt[0].y);
		}
		else
		{
			pDC->MoveTo(pt[0].x, pt[0].y);
			pDC->LineTo(pt[0].x - 40, pt[0].y);
			pDC->LineTo(pt[0].x - 100 , pt[0].y + nDY);
		}
	
		int x = pt[0].x - 100;
	//	if (!bMode)
		{
			CFont* pOldFont = pInfo->SetFont(szFont);
			CSize szText = pInfo->GetPrintDC()->GetTextExtent(strText);
			x -= szText.cx;
			pInfo->GetPrintDC()->SelectObject(pOldFont);
		}
		if (pInfo->GetPrintDirection() == SCROLL_UP)
		{
			pInfo->PrintText(szFont, x, pt[0].y - 30 + nDY, strText, bMode ? 1800 : 0,
				nBkMode, FALSE, FW_SEMIBOLD, pPen);
		}
		else
		{
			pInfo->PrintText(szFont, x, pt[0].y + 30 + nDY, strText, bMode ? 1800 : 0,
				nBkMode, FALSE, FW_SEMIBOLD, pPen);
		}
	}
	else
	{
		POINT pt[4];
		pt[0].x = pInfo->CoordinateConvertX(nX);
		pt[0].x += 1;
		pt[0].y = pInfo->GetPrintDepthCoordinate(DTM(lDepth));
		pt[1].x = pt[0].x + 22;
		pt[1].y = pt[0].y - 10;
		pt[2].x = pt[0].x + 15;
		pt[2].y = pt[0].y;
		pt[3].x = pt[0].x + 22;
		pt[3].y = pt[0].y + 10;
		pDC->Polygon(pt, 4);
		
		if(nDY == 0)
		{
			pDC->MoveTo(pt[0].x, pt[0].y);
			pDC->LineTo(pt[0].x + 100, pt[0].y);
		}
		else
		{
			pDC->MoveTo(pt[0].x, pt[0].y);
			pDC->LineTo(pt[0].x + 40, pt[0].y);
			pDC->LineTo(pt[0].x + 100, pt[0].y + nDY);
		}

		int x = pt[0].x + 100;

		if (pInfo->GetPrintDirection() == SCROLL_UP)
		{
			pInfo->PrintText(szFont, x, pt[0].y - 30 + nDY, strText, bMode ? 1800 : 0,
				nBkMode, FALSE, FW_SEMIBOLD, pPen);
		}
		else
		{
			pInfo->PrintText(szFont, x, pt[0].y + 30 + nDY, strText, bMode ? 1800 : 0,
				nBkMode, FALSE, FW_SEMIBOLD, pPen);
		}
	}

/*	if (bReverse)
	{
		POINT pt[4];
		pt[0].x = pInfo->CoordinateConvertX(nX);
		pt[0].x -= 1;
		pt[0].y = pInfo->GetPrintDepthCoordinate(DTM(lDepth));
		pt[1].x = pt[0].x - 22;
		pt[1].y = pt[0].y - 10;
		pt[2].x = pt[0].x - 15;
		pt[2].y = pt[0].y;
		pt[3].x = pt[0].x - 22;
		pt[3].y = pt[0].y + 10;
		pDC->Polygon(pt, 4);
		pDC->MoveTo(pt[0].x, pt[0].y);
		pDC->LineTo(pt[0].x - 100, pt[0].y);
	
		int x = pt[0].x - 100;
	//	if (!bMode)
		{
			CFont* pOldFont = pInfo->SetFont(szFont);
			CSize szText = pInfo->GetPrintDC()->GetTextExtent(strText);
			x -= szText.cx;
			pInfo->GetPrintDC()->SelectObject(pOldFont);
		}
		if (pInfo->GetPrintDirection() == SCROLL_UP)
		{
			pInfo->PrintText(szFont, x, pt[0].y - 30, strText, bMode ? 1800 : 0,
				nBkMode, FALSE, FW_SEMIBOLD, pPen);
		}
		else
		{
			pInfo->PrintText(szFont, x, pt[0].y + 30, strText, bMode ? 1800 : 0,
				nBkMode, FALSE, FW_SEMIBOLD, pPen);
		}
	}
	else
	{
		POINT pt[4];
		pt[0].x = pInfo->CoordinateConvertX(nX);
		pt[0].x += 1;
		pt[0].y = pInfo->GetPrintDepthCoordinate(DTM(lDepth));
		pt[1].x = pt[0].x + 22;
		pt[1].y = pt[0].y - 10;
		pt[2].x = pt[0].x + 15;
		pt[2].y = pt[0].y;
		pt[3].x = pt[0].x + 22;
		pt[3].y = pt[0].y + 10;

		pDC->Polygon(pt, 4);
		pDC->MoveTo(pt[0].x, pt[0].y);
		pDC->LineTo(pt[0].x + 100, pt[0].y);

		int x = pt[0].x + 100;

		if (pInfo->GetPrintDirection() == SCROLL_UP)
		{
			pInfo->PrintText(szFont, x, pt[0].y - 30, strText, bMode ? 1800 : 0,
				nBkMode, FALSE, FW_SEMIBOLD, pPen);
		}
		else
		{
			pInfo->PrintText(szFont, x, pt[0].y + 30, strText, bMode ? 1800 : 0,
				nBkMode, FALSE, FW_SEMIBOLD, pPen);
		}
	}*/

	pDC->SelectObject(pOldBr);
	pDC->SelectObject(pOldPen);

	return TRUE;
}

BOOL CTrack::SortCurveFirst()
{
	int nFirstFlags = m_firstFlags.GetSize();
	if (nFirstFlags <= 0)
	{
		return FALSE;
	}

	for (int i = 0; i < nFirstFlags; i++)
	{
		FIRST First = m_firstFlags.GetAt(i);
		CULPrintInfo* pInfo = First.pInfo;
		int nX = First.nX;
		long lFDepth = First.lFDepth;
		BOOL bMode = First.bMode;
		CString str = First.strText;
		CPen* pPen = First.pPen;
		int nDirection = First.nDirection;
		
		CDC* pDC = pInfo->GetPrintDC();
		LOGFONT lf;
		g_font[ftCT].GetLogFont(&lf);
		CSize szFont(0, lf.lfHeight);
		CFont* pOldFont = pInfo->SetFont(szFont);
		CSize szText = pDC->GetTextExtent(str);
		pDC->SelectObject(pOldFont);
		int x = pInfo->CoordinateConvertX(nX);
		int y = pInfo->GetPrintDepthCoordinate(DTM(lFDepth));
		
		int iCount = 0;
		if (nX < m_rcRegion.CenterPoint().x)
		{
			int arrowDirection = 4;
			CRect rect(x, y+szText.cy/2, x+szText.cx+60, y-szText.cy/2);
		//	IsDrawFirst(rect, arrowDirection, iCount, 4, nDirection);
			int nCut = iCount;
			for ( ; ; )
			{
				IsDrawFirst(rect, arrowDirection, iCount, 4, nDirection);
				if (nCut == iCount)
				{
					break;
				}
				nCut = iCount;
				if (nDirection == 0)
				{
					rect.top += szText.cy * iCount;
 					rect.bottom += szText.cy * iCount;

				}
				else/* if(nDirection == 1)*/
				{
					rect.top -= szText.cy * iCount;
 					rect.bottom -= szText.cy * iCount;
				}
			}

			DrawCurveFirst(pInfo, nX, lFDepth, bMode, str, pPen, arrowDirection, iCount);
			
		}
		else if (nX > m_rcRegion.CenterPoint().x)
		{
			int arrowDirection = 5;
			CRect rect(x-szText.cx-60, y+szText.cy/2, x, y-szText.cy/2);
		//	IsDrawFirst(rect, arrowDirection, iCount, 5, nDirection);
			int nCut = iCount;
			for ( ; ; )
			{
				IsDrawFirst(rect, arrowDirection, iCount, 5, nDirection);
				if (nCut == iCount)
				{
					break;
				}
				nCut = iCount;
				if (nDirection == 0)
				{
					rect.top += szText.cy * iCount;
					rect.bottom += szText.cy * iCount;
					
				}
				else/* if(nDirection == 1)*/
				{
					rect.top -= szText.cy * iCount;
					rect.bottom -= szText.cy * iCount;
				}
			}
			
			DrawCurveFirst(pInfo, nX, lFDepth, bMode, str, pPen, arrowDirection, iCount);
		}
	}
	return TRUE;
}

BOOL CTrack::DrawCurveFirst(CULPrintInfo* pInfo, int nX, long lFDepth, BOOL bMode, CString str, CPen* pPen, int arrowDirection/* = 4*/, int iCount/* = 0*/)
{
	CDC* pDC = pInfo->GetPrintDC();
	pDC->SetBkMode(OPAQUE);
	
	LOGPEN logPen;
	pPen->GetLogPen(&logPen);
	CBrush* pBrush = new CBrush(logPen.lopnColor);
	CPen* pOldPen = pDC->SelectObject(pPen);
	CBrush* pOldBr = pDC->SelectObject(/*BLACK_BRUSH*/pBrush);

	str.TrimLeft(); str.TrimRight();
	LOGFONT lf;
	g_font[ftCT].GetLogFont(&lf);
	CSize szFont(0, lf.lfHeight);
	CFont* pOldFont = pInfo->SetFont(szFont);

	CSize szText = pDC->GetTextExtent(str);
	int x = pInfo->CoordinateConvertX(nX);
	int y = pInfo->GetPrintDepthCoordinate(DTM(lFDepth));
	
	POINT pt[4];
	pt[0].x = x;
	pt[0].y = y;
	CRect rect;

	int iDistance = 0; // bao 2011/6/9

	switch (arrowDirection)
	{
	case 0: // 左上斜
		pt[1].x = pt[0].x + 15;
		pt[1].y = pt[0].y - 4;
		pt[2].x = pt[0].x + 7;
		pt[2].y = pt[0].y - 7;
		pt[3].x = pt[0].x;
		pt[3].y = pt[0].y - 15;
		
		pDC->Polygon(pt, 4);
		pDC->MoveTo(pt[0].x, pt[0].y);
		pDC->LineTo(pt[0].x + 40, pt[0].y - szText.cy * iCount - szText.cy/2 - 10);
		
		x = pt[0].x + 40;
		
		pInfo->PrintText(szFont, x, pt[0].y - szText.cy * iCount - szText.cy/2 - 10, str, bMode ? 1800 : 0,
			OPAQUE, FALSE, FW_SEMIBOLD, pPen);
		
		rect.left = pt[0].x;
		rect.top = y - szText.cy * iCount;
		rect.right = pt[0].x + 40 + szText.cx;
		rect.bottom = y - szText.cy * iCount - 10;
		
		m_flagRects.Add(rect);
		break;
	case 1: // 右上斜
		pt[1].x = pt[0].x - 15;
		pt[1].y = pt[0].y - 4;
		pt[2].x = pt[0].x - 7;
		pt[2].y = pt[0].y - 7;
		pt[3].x = pt[0].x;
		pt[3].y = pt[0].y - 15;
		
		pDC->Polygon(pt, 4);
		pDC->MoveTo(pt[0].x, pt[0].y);
		pDC->LineTo(pt[0].x - 40, pt[0].y - szText.cy * iCount - szText.cy/2 - 10);
		
		x = pt[0].x - 40 - szText.cx;
	
		pInfo->PrintText(szFont, x, pt[0].y - szText.cy * iCount - szText.cy/2 - 10, str, bMode ? 1800 : 0,
			OPAQUE, FALSE, FW_SEMIBOLD, pPen);
		
		rect.left = pt[0].x - 40 - szText.cx;
		rect.top = y - szText.cy * iCount + szText.cy/2;
		rect.right = pt[0].x;
		rect.bottom = y - szText.cy * iCount - szText.cy/2 - 10;
		
		m_flagRects.Add(rect);
		break;
	case 2: // 右下斜
		pt[1].x = pt[0].x - 15;
		pt[1].y = pt[0].y + 4;
		pt[2].x = pt[0].x - 7;
		pt[2].y = pt[0].y + 7;
		pt[3].x = pt[0].x;
		pt[3].y = pt[0].y + 15;
		
		pDC->Polygon(pt, 4);
		pDC->MoveTo(pt[0].x, pt[0].y);
		pDC->LineTo(pt[0].x - 40, pt[0].y + szText.cy * iCount + 10);
		
		x = pt[0].x - 40 - szText.cx;
		
		// bao 2011/6/9 s
		iDistance = 0;
		if ( x < 0)
		{
			iDistance = 0 - x;
			x += iDistance;
		}
		// bao end

		pInfo->PrintText(szFont, x, pt[0].y + szText.cy * iCount + 10 + szText.cy / 2, str, bMode ? 1800 : 0,
			OPAQUE, FALSE, FW_SEMIBOLD, pPen);
		
		rect.left = x;
		rect.top = y + szText.cy + szText.cy * iCount + 10;
		rect.right = pt[0].x + iDistance;
		rect.bottom = y + szText.cy * iCount - szText.cy;
		
		m_flagRects.Add(rect);
		break;
	case 5: // 右指：当左边出界时按左指输出
		
		x = pt[0].x - szText.cx - 40;
		
		if (x >=0)
		{
			pt[1].x = pt[0].x - 22;
 			pt[1].y = pt[0].y + 10;
 			pt[2].x = pt[0].x - 15;
 			pt[2].y = pt[0].y;
 			pt[3].x = pt[0].x - 22;
 			pt[3].y = pt[0].y - 10;

 			pDC->Polygon(pt, 4);
 			pDC->MoveTo(pt[0].x, pt[0].y);
 			pDC->LineTo(pt[0].x - 40, pt[0].y);
			
			pInfo->PrintText(szFont, x, pt[0].y + szText.cy / 2, str, bMode ? 1800 : 0, OPAQUE, FALSE, FW_SEMIBOLD, pPen);
			
			rect.left = x;
			rect.top = y + szText.cy / 2;
			rect.right = pt[0].x;
			rect.bottom = y - szText.cy / 2;
			
			m_flagRects.Add(rect);
		}
		else
		{
			pt[1].x = pt[0].x + 22;
			pt[1].y = pt[0].y - 10;
			pt[2].x = pt[0].x + 15;
			pt[2].y = pt[0].y;
			pt[3].x = pt[0].x + 22;
			pt[3].y = pt[0].y + 10;
			
			pDC->Polygon(pt, 4);
			pDC->MoveTo(pt[0].x, pt[0].y);
			pDC->LineTo(pt[0].x + 40, pt[0].y);
			
			x = pt[0].x + 40;
			
			pInfo->PrintText(szFont, x, pt[0].y + szText.cy / 2, str, bMode ? 1800 : 0,
				OPAQUE, FALSE, FW_SEMIBOLD, pPen);
			
			rect.left = pt[0].x;
			rect.top = y+szText.cy / 2;
			rect.right = pt[0].x + 40 + szText.cx;
			rect.bottom = y - szText.cy / 2;
			
			m_flagRects.Add(rect);
		}
		break;
	case 3: // 左下斜
		pt[1].x = pt[0].x + 15;
		pt[1].y = pt[0].y + 4;
		pt[2].x = pt[0].x + 7;
		pt[2].y = pt[0].y + 7;
		pt[3].x = pt[0].x;
		pt[3].y = pt[0].y + 15;
		
		pDC->Polygon(pt, 4);
		pDC->MoveTo(pt[0].x, pt[0].y);
		pDC->LineTo(pt[0].x + 40, pt[0].y + szText.cy * iCount + 10);
		
		x = pt[0].x + 40;
		
		pInfo->PrintText(szFont, x, pt[0].y + szText.cy * iCount + 10 + szText.cy / 2, str, bMode ? 1800 : 0,
			OPAQUE, FALSE, FW_SEMIBOLD, pPen);
		
		rect.left = pt[0].x;
		rect.top = y + szText.cy / 2 + szText.cy * iCount + 10;
		rect.right = pt[0].x + 40 + szText.cx;
		rect.bottom = y + szText.cy * iCount - szText.cy / 2;

		m_flagRects.Add(rect);
		break;
	default: // 4 左指
		pt[1].x = pt[0].x + 22;
		pt[1].y = pt[0].y - 10;
		pt[2].x = pt[0].x + 15;
		pt[2].y = pt[0].y;
		pt[3].x = pt[0].x + 22;
		pt[3].y = pt[0].y + 10;
		
		pDC->Polygon(pt, 4);
		pDC->MoveTo(pt[0].x, pt[0].y);
		pDC->LineTo(pt[0].x + 40, pt[0].y);
		
		x = pt[0].x + 40;

		pInfo->PrintText(szFont, x, pt[0].y + szText.cy / 2, str, bMode ? 1800 : 0,
				OPAQUE, FALSE, FW_SEMIBOLD, pPen);

		rect.left = pt[0].x;
		rect.top = y+szText.cy / 2;
		rect.right = pt[0].x + 40 + szText.cx;
		rect.bottom = y - szText.cy / 2;

		m_flagRects.Add(rect);
		break;
	}

	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBr);
	pDC->SelectObject(pOldFont);
	return TRUE;
}

BOOL CTrack::IsDrawFirst(CRect rect, int& arrowDirection, int& iCount, int LoR, int nDirection)
{
	int nFlagRects = m_flagRects.GetSize();
	if (nFlagRects <= 0)
	{
		return FALSE;
	}
	for (int i = 0; i < nFlagRects; i++)
	{
		CRect rectOld = m_flagRects.GetAt(i);
		rectOld.NormalizeRect();
		rect.NormalizeRect();
		// 由矩形的中心点判断两矩形是否相交
		CPoint oldPoint;
		oldPoint.x = abs((rectOld.right + rectOld.left) / 2);
		oldPoint.y = abs((rectOld.top + rectOld.bottom) / 2);
		
		CPoint newPoint; // 移动后矩形中心点
		newPoint.x = abs((rect.right + rect.left) / 2);
		newPoint.y = abs((rect.top + rect.bottom) / 2);

		BOOL bWidth = abs(newPoint.x - oldPoint.x) > (abs(rectOld.Width()) / 2 + abs(rect.Width()) / 2);
		BOOL bHeight = abs(newPoint.y - oldPoint.y) > (abs(rectOld.Height()) / 2 + abs(rect.Height()) / 2);
		if (!(bWidth || bHeight))
		{
			if (LoR == 4)
			{
				if (nDirection == 0)
				{
					arrowDirection = 3;
				}
				else
				{
					arrowDirection = 0;
				}
			}
			else if (LoR == 5)
			{
				if (nDirection == 0)
				{
					arrowDirection = 2;
				}
				else
				{
					arrowDirection = 1;
				}
			}
			iCount++;
		}
	}
	
	return TRUE;
}

//////////////////////////////////////////////////////////////////////
// 系统提供的默认打印方式
//////////////////////////////////////////////////////////////////////

ULMIMP CTrack::PrintGaps(CULPrintInfo* pInfo, long lMaxDepth, long lMinDepth)
{
	int nLWidth = LPWidth(m_GridInfo.vLineInfo.nMiddleGridLinewidth);
	int nLType = m_GridInfo.vLineInfo.nMiddleGridLinetype;
	COLORREF clr = AfxGetComConfig()->m_Plot.Colored ?
		m_GridInfo.vLineInfo.colorMiddleGrid :
		0;

	unsigned pattern[8];
	CDashLine MiddleGridPen(*pInfo->GetPrintDC(), pattern,
		CDashLine::GetPattern(pattern, 0, nLWidth, nLType));
	CPen penMiddleGrid(PS_SOLID, nLWidth, clr);

	CRect rcRegion;
	GetRegionRect(rcRegion, TRUE);	
	rcRegion = pInfo->CoordinateConvert(rcRegion);

	// Top - Bottom
	int nBottom = pInfo->GetPrintDepthCoordinate(DTM(lMaxDepth));
	int nTop = pInfo->GetPrintDepthCoordinate(DTM(lMinDepth));

	rcRegion.top = nTop;
	rcRegion.bottom = nBottom;

	// Not the first or last track

	if (m_nTrackIndex == 0)
	{
		CPen* pOldPen = pInfo->GetPrintDC()->SelectObject(&penMiddleGrid);
		CPoint ptStart(rcRegion.left, rcRegion.top);
		CPoint ptEnd(rcRegion.left, rcRegion.bottom);
		pInfo->CoordinateCalibrate(ptStart);
		pInfo->CoordinateCalibrate(ptEnd);
		MiddleGridPen.MoveTo(ptStart);
		MiddleGridPen.LineTo(ptEnd);
		CPoint ptStart1(rcRegion.right, rcRegion.top);
		CPoint ptEnd1(rcRegion.right, rcRegion.bottom);
		pInfo->CoordinateCalibrate(ptStart1);
		pInfo->CoordinateCalibrate(ptEnd1);
		MiddleGridPen.MoveTo(ptStart1);
		MiddleGridPen.LineTo(ptEnd1);
		pInfo->GetPrintDC()->SelectObject(pOldPen);
		return UL_NO_ERROR;
	}

	// First or last, if both the last is ignore

	CArray<int, int> nGapDepthArray;

	if (m_bRTPrint)
	{
		if (m_pDEPT == NULL)
			return UL_ERROR;

		int nSize = m_pDEPT->GetDataSize();
		if (nSize < 1)
			return UL_ERROR;

		long nTimeIntervals = m_nGapInterval * 1000;
		long nPrevTime = 0;
		long nNextTime = 0;
		nPrevTime = m_pDEPT->GetTime(m_nGapPos);
		for (int i = m_pDEPT->GetCurGapPos(); i < nSize; i++)
		{
			nNextTime = m_pDEPT->GetTime(i);
			if (abs(nNextTime - nPrevTime) >= nTimeIntervals)
			{
				long lDepth = m_pDEPT->GetDepth(i);
				if ((lMinDepth <= lDepth) && (lDepth <= lMaxDepth))
				{
					nPrevTime = nNextTime;
					m_nGapPos = i;
					// TRACE("Gap Depth : %ld\n", lDepth);
					int nY = pInfo->GetPrintDepthCoordinate(DTM(lDepth));
					nGapDepthArray.Add(nY);
				}
			}
		}
	}
	else if (m_pGapDepthArray != NULL)
	{
		for (int i = 0; i < m_pGapDepthArray->GetSize(); i++)
		{
			long lDepth = m_pGapDepthArray->GetAt(i);

		//	Add by zy 2012 4 17 修改打印速度缺口时，lDepth等于lMinDepth 的情况下，无法打印问题
		//	if ((lMinDepth < lDepth) && (lDepth < lMaxDepth))
			if ((lMinDepth <= lDepth) && (lDepth < lMaxDepth))
			{
				int nY = pInfo->GetPrintDepthCoordinate(DTM(lDepth));
				nGapDepthArray.Add(nY);
			}
		}
	}

	// Sort gap array
	int i = 0;
	for (i = 0; i < nGapDepthArray.GetSize(); i++)
	{
		int k = i;
		for (int j = i + 1; j < nGapDepthArray.GetSize(); j++)
		{
			if (nGapDepthArray[j] > nGapDepthArray[k])
				k = j;
		}

		if (k != i)
		{
			int nTemp = nGapDepthArray[i];
			nGapDepthArray[i] = nGapDepthArray[k];
			nGapDepthArray[k] = nTemp;
		}
	}

	// Remove the same element
	for (i = 1; i < nGapDepthArray.GetSize(); i++)
	{
		if (nGapDepthArray[i] == nGapDepthArray[i - 1])
		{
			nGapDepthArray.RemoveAt(i);
			// i--;
		}
	}

// 	if (nLWidth < 4)
// 		nLWidth = 4;
	CDashLine GapPen(*pInfo->GetPrintDC(), pattern, CDashLine::GetPattern(pattern,
													0, nLWidth, nLType));
	CPen penGapGrid(PS_SOLID, nLWidth, clr);
//	CPen penGapGrid(PS_SOLID, nLWidth, RGB(255,255,255));
//	CPen penGapGrid(PS_SOLID, nLWidth, RGB(0,0,0));
	CPen* pOldPen = pInfo->GetPrintDC()->SelectObject(&penGapGrid);

	CRect rc = rcRegion;
	if (m_bRTPrint && (pInfo->GetPrintDirection() == SCROLL_UP))
	{
		rc.left = rcRegion.right;
		rc.top = rcRegion.bottom;
		rc.right = rcRegion.left;
		rc.bottom = rcRegion.top;
	}

	int nMaxCoordinate = rc.bottom;

	CPoint pt0;
	pt0.x = (m_nTrackIndex & TI_FIRST) ? rc.left : rc.right;
	pt0.y = rc.top;

	CPoint pt1 = pt0;

	if (m_bGapRemain)
	{
		pt0.y -= m_nGapRemain;
		m_bGapRemain = FALSE;
	}

	for (i = 0; i < nGapDepthArray.GetSize(); i++)
	{
	GapPen.MoveTo(pInfo->CoordCalibrate(pt0));
		pt1.y = nGapDepthArray.GetAt(i);

		GapPen.LineTo(pInfo->CoordCalibrate(pt1));
		pt0.y = pt1.y - m_nGapSize;

		if (pt0.y < nMaxCoordinate)
		{
			m_bGapRemain = TRUE;
			m_nGapRemain = nMaxCoordinate - pt0.y;
			break;
		}

	}

	if (!m_bGapRemain)
	{
		pt1.y = nMaxCoordinate;
		GapPen.MoveTo(pInfo->CoordCalibrate(pt0));
		GapPen.LineTo(pInfo->CoordCalibrate(pt1));

	}

	nGapDepthArray.RemoveAll();

	return UL_NO_ERROR;
}

ULMIMP CTrack::PrintEdge(CULPrintInfo* pInfo, long lMaxDepth, long lMinDepth)
{
	int nLWidth = LPWidth(m_GridInfo.vLineInfo.nMiddleGridLinewidth);
	COLORREF clr = AfxGetComConfig()->m_Plot.Colored ?
		m_GridInfo.vLineInfo.colorMiddleGrid :
		0;

	CRect rcRegion;
	GetRegionRect(rcRegion, TRUE);	
	rcRegion = pInfo->CoordinateConvert(rcRegion);
	int nBottom = pInfo->GetPrintDepthCoordinate(DTM(lMaxDepth));
	int nTop = pInfo->GetPrintDepthCoordinate(DTM(lMinDepth));
	rcRegion.top = nTop;
	rcRegion.bottom = nBottom;

	CPoint ptStart(rcRegion.left, rcRegion.top);
	CPoint ptEnd(rcRegion.left, rcRegion.bottom);
	pInfo->CoordinateCalibrate(ptStart);
	pInfo->CoordinateCalibrate(ptEnd);

	CPen penEdge(PS_SOLID, max(4, nLWidth), RGB(0, 0, 0));
	CPen penMiddleGrid(PS_SOLID, nLWidth, RGB(0, 0, 0));
	CPen* pOldPen = pInfo->GetPrintDC()->SelectObject(&penMiddleGrid);
	if (m_nTrackIndex & TI_FIRST)
	{
		pInfo->GetPrintDC()->SelectObject(&penEdge);
	}

	pInfo->GetPrintDC()->MoveTo(ptStart);
	pInfo->GetPrintDC()->LineTo(ptEnd);
	
	CPoint ptStart1(rcRegion.right, rcRegion.top);
	CPoint ptEnd1(rcRegion.right, rcRegion.bottom);
	pInfo->CoordinateCalibrate(ptStart1);
	pInfo->CoordinateCalibrate(ptEnd1);

	if (m_nTrackIndex & TI_LAST)
	{
		if ((m_nTrackIndex & TI_FIRST) == 0)
			pInfo->GetPrintDC()->SelectObject(&penEdge);
	}
	else if (m_nTrackIndex & TI_FIRST)
	{
		pInfo->GetPrintDC()->SelectObject(&penMiddleGrid);
	}

	pInfo->GetPrintDC()->MoveTo(ptStart1);
	pInfo->GetPrintDC()->LineTo(ptEnd1);
	pInfo->GetPrintDC()->SelectObject(pOldPen);
	return UL_NO_ERROR;
}

// 绘制缩略图
void CTrack::DrawThumb(CDC* pDC, LPRECT lpRect, long ls, double dDelta,
	int nPoint)
{
	for (int j = 0; j < m_vecCurve.size(); j++)
	{
		CCurve* pCurve = m_vecCurve.at(j);
		if (pCurve != NULL)
			pCurve->DrawThumb(pDC, lpRect, ls, dDelta, nPoint);
	}
}

void CTrack::AddCurves(CStringList* pList)
{
	// Curves name
	int nSize = m_vecCurve.size();
	for (int j = 0; j < nSize; j++)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(j);
		if (pCurve->GetCurveMode() & CURVE_BACK)
			continue ;
		
		pList->AddTail(pCurve->m_strName);
	}
}

void CTrack::Serialize(CXMLSettings& xml)
{
	if (xml.IsStoring())
	{
		xml.Write(DSF_TRACK_NAME, m_strName);
		xml.Write(DSF_TRACK_TYPE, m_nType);
		xml.Write(DSF_TRACK_COLOR, m_crColor);
		xml.Write(DSF_TRACK_LINESTYLE, m_nLineStyle);
		xml.Write(DSF_TRACK_HEADRC, m_rcHead);
		xml.Write(DSF_TRACK_REGIONRC, m_rcRegion);
		xml.Write(DSF_CURVE_COUNT, (int)m_vecCurve.size());

		// Curve Settings
		for (int j = 0; j < m_vecCurve.size(); j++)
		{
			CCurve* pCurve = (CCurve*) m_vecCurve.at(j);
			if ((pCurve->GetCurveMode() & CURVE_BACK))
				continue;

			if (xml.CreateKey(DSF_CURVE_IDFMT, j))
			{
				pCurve->Serialize(xml);
				xml.Back();
			}
		}
		
		xml.Write(DSF_HGRID_SHOW, m_GridInfo.bHasHLine);
		xml.Write(DSF_VGRID_SHOW, m_GridInfo.bHasVLine);
		// GridInfo
		// 水平格线小格子
		if (xml.CreateKey(_T("HSGird")))
		{
			xml.Write(DSF_GRID_SMALL, m_GridInfo.hLine[0].bHasGrid);
			//xml.Write(DSF_GRID_STEP, m_GridInfo.hLine[0].nGridStep);
			xml.Write(DSF_GRID_LINEWIDTH, m_GridInfo.hLine[0].nGridLinewidth);
			xml.Write(DSF_GRID_COLOR, m_GridInfo.hLine[0].clrGrid);
			xml.Write(DSF_GRID_LINETYPE, m_GridInfo.hLine[0].nGridLinetype);
			//add by xwh  区分时间驱动和深度驱动模式下的参数存储
			xml.Write(DSF_GRID_STEP, m_dHorzGridDepth[0]);
			xml.Write("TimeStep", m_dHorzGridTime[0]);
			xml.Back();
		}
		
		// 水平格线中格子
		if (xml.CreateKey(_T("HMGird")))
		{
			xml.Write(DSF_GRID_MIDDLE, m_GridInfo.hLine[1].bHasGrid);
			//xml.Write(DSF_GRID_STEP, m_GridInfo.hLine[1].nGridStep);
			xml.Write(DSF_GRID_LINEWIDTH, m_GridInfo.hLine[1].nGridLinewidth);
			xml.Write(DSF_GRID_COLOR, m_GridInfo.hLine[1].clrGrid);
			xml.Write(DSF_GRID_LINETYPE, m_GridInfo.hLine[1].nGridLinetype);
			//add by xwh  区分时间驱动和深度驱动模式下的参数存储
			xml.Write(DSF_GRID_STEP, m_dHorzGridDepth[1]);
			xml.Write("TimeStep", m_dHorzGridTime[1]);
			xml.Back();
		}

		// 水平格线大格子
		if (xml.CreateKey(_T("HBGird")))
		{
			xml.Write(DSF_GRID_BIG, m_GridInfo.hLine[2].bHasGrid);
			//xml.Write(DSF_GRID_STEP, m_GridInfo.hLine[2].nGridStep);
			xml.Write(DSF_GRID_LINEWIDTH, m_GridInfo.hLine[2].nGridLinewidth);
			xml.Write(DSF_GRID_COLOR, m_GridInfo.hLine[2].clrGrid);
			xml.Write(DSF_GRID_LINETYPE, m_GridInfo.hLine[2].nGridLinetype);
			//add by xwh  区分时间驱动和深度驱动模式下的参数存储
			xml.Write(DSF_GRID_STEP, m_dHorzGridDepth[2]);
			xml.Write("TimeStep", m_dHorzGridTime[2]);
			xml.Back();
		}


		// 竖直格线中格子
		if (xml.CreateKey(_T("VMGird")))
		{
			xml.Write(DSF_GRID_MIDDLE, m_GridInfo.vLineInfo.bHasMiddleGrid);
			xml.Write(DSF_GRID_LINEWIDTH, m_GridInfo.vLineInfo.nMiddleGridLinewidth);
			xml.Write(DSF_GRID_COLOR, m_GridInfo.vLineInfo.colorMiddleGrid);
			xml.Write(DSF_GRID_LINETYPE, m_GridInfo.vLineInfo.nMiddleGridLinetype);
			xml.Back();
		}

		// 竖直格线小格子
		if (xml.CreateKey(_T("VSGird")))
		{
			xml.Write(DSF_GRID_SMALL, m_GridInfo.vLineInfo.bHasSmallGrid);
			xml.Write(DSF_GRID_LINEWIDTH, m_GridInfo.vLineInfo.nSmallGridLinewidth);
			xml.Write(DSF_GRID_COLOR, m_GridInfo.vLineInfo.colorSmallGrid);
			xml.Write(DSF_GRID_LINETYPE, m_GridInfo.vLineInfo.nSmallGridLinetype);
			xml.Back();
		}
		
		xml.Write(DSF_GRID_VGGROUP,m_GridInfo.vLineInfo.nGroupCount);
		xml.Write(DSF_GRID_VGLINE, m_GridInfo.vLineInfo.nLineCount);
		xml.Write(DSF_GRID_VGTYPE, m_GridInfo.vLineInfo.nGridType);
		
	} // end of if (xml.IsStoring())
	else
	{
		DWORD m_dwCurve;
		xml.Read(DSF_TRACK_NAME, m_strName);
		xml.Read(DSF_TRACK_TYPE, m_nType);
		xml.Read(DSF_TRACK_COLOR, m_crColor);
		xml.Read(DSF_TRACK_LINESTYLE, m_nLineStyle);
		xml.Read(DSF_TRACK_HEADRC, m_rcHead);
		xml.Read(DSF_TRACK_REGIONRC, m_rcRegion);
		xml.Read(DSF_CURVE_COUNT, m_dwCurve);
		
		xml.Read(DSF_HGRID_SHOW, m_GridInfo.bHasHLine);
		xml.Read(DSF_VGRID_SHOW, m_GridInfo.bHasVLine);
		// Read GridInfo
		if (xml.Open(_T("HSGird")))
		{
			xml.Read(DSF_GRID_SMALL, m_GridInfo.hLine[0].bHasGrid);
		//	xml.Read(DSF_GRID_STEP, m_GridInfo.hLine[0].nGridStep);
			xml.Read(DSF_GRID_LINEWIDTH, m_GridInfo.hLine[0].nGridLinewidth);
			int nLWLevel = LWLevel(m_GridInfo.hLine[0].nGridLinewidth);
			m_GridInfo.hLine[0].nGridLinewidth = AfxGetComConfig()->SetLWLevel(nLWLevel);
			xml.Read(DSF_GRID_COLOR, m_GridInfo.hLine[0].clrGrid);
			xml.Read(DSF_GRID_LINETYPE, m_GridInfo.hLine[0].nGridLinetype);
			//add by xwh  区分时间驱动和深度驱动模式下的参数存储
			xml.Read(DSF_GRID_STEP, m_dHorzGridDepth[0]);
			xml.Read("TimeStep", m_dHorzGridTime[0]);
			xml.Back();
		}

		if (xml.Open(_T("HMGird")))
		{
			xml.Read(DSF_GRID_MIDDLE, m_GridInfo.hLine[1].bHasGrid);
		//	xml.Read(DSF_GRID_STEP, m_GridInfo.hLine[1].nGridStep);
			xml.Read(DSF_GRID_LINEWIDTH, m_GridInfo.hLine[1].nGridLinewidth);
			if (LPWidth(m_GridInfo.hLine[1].nGridLinewidth) == 0)
				m_GridInfo.hLine[1].nGridLinewidth = AfxGetComConfig()->SetLWLevel(1);
			else
			{
				int nLWLevel = LWLevel(m_GridInfo.hLine[1].nGridLinewidth);
				m_GridInfo.hLine[1].nGridLinewidth = AfxGetComConfig()->SetLWLevel(nLWLevel);
			}
			xml.Read(DSF_GRID_COLOR, m_GridInfo.hLine[1].clrGrid);
			xml.Read(DSF_GRID_LINETYPE, m_GridInfo.hLine[1].nGridLinetype);
			//add by xwh  区分时间驱动和深度驱动模式下的参数存储
			xml.Read(DSF_GRID_STEP, m_dHorzGridDepth[1]);
			xml.Read("TimeStep", m_dHorzGridTime[1]);

			xml.Back();
		}

		if (xml.Open(_T("HBGird")))
		{
			xml.Read(DSF_GRID_BIG, m_GridInfo.hLine[2].bHasGrid);
		//	xml.Read(DSF_GRID_STEP, m_GridInfo.hLine[2].nGridStep);
			xml.Read(DSF_GRID_LINEWIDTH, m_GridInfo.hLine[2].nGridLinewidth);
			if (LPWidth(m_GridInfo.hLine[2].nGridLinewidth) == 0)
				m_GridInfo.hLine[2].nGridLinewidth = AfxGetComConfig()->SetLWLevel(2);
			else
			{
				int nLWLevel = LWLevel(m_GridInfo.hLine[2].nGridLinewidth);
				m_GridInfo.hLine[2].nGridLinewidth = AfxGetComConfig()->SetLWLevel(nLWLevel);
			}
			xml.Read(DSF_GRID_COLOR, m_GridInfo.hLine[2].clrGrid);
			xml.Read(DSF_GRID_LINETYPE, m_GridInfo.hLine[2].nGridLinetype);
			//add by xwh  区分时间驱动和深度驱动模式下的参数存储
			xml.Read(DSF_GRID_STEP, m_dHorzGridDepth[2]);
			xml.Read("TimeStep", m_dHorzGridTime[2]);
	
			xml.Back();
		}

		if (xml.Open(_T("VMGird")))
		{
			xml.Read(DSF_GRID_MIDDLE, m_GridInfo.vLineInfo.bHasMiddleGrid);
			xml.Read(DSF_GRID_LINEWIDTH,
					m_GridInfo.vLineInfo.nMiddleGridLinewidth);
			if (LPWidth(m_GridInfo.vLineInfo.nMiddleGridLinewidth) == 0)
				m_GridInfo.vLineInfo.nMiddleGridLinewidth = AfxGetComConfig()->SetLWLevel(1);
			else
			{
				int nLWLevel = LWLevel(m_GridInfo.vLineInfo.nMiddleGridLinewidth);
				m_GridInfo.vLineInfo.nMiddleGridLinewidth = AfxGetComConfig()->SetLWLevel(nLWLevel);
			}
			xml.Read(DSF_GRID_COLOR, m_GridInfo.vLineInfo.colorMiddleGrid);
			xml.Read(DSF_GRID_LINETYPE,m_GridInfo.vLineInfo.nMiddleGridLinetype);
			xml.Back();
		}

		if (xml.Open(_T("VSGird")))
		{
			xml.Read(DSF_GRID_SMALL, m_GridInfo.vLineInfo.bHasSmallGrid);
			xml.Read(DSF_GRID_LINEWIDTH,
					m_GridInfo.vLineInfo.nSmallGridLinewidth);
			int nLWLevel = LWLevel(m_GridInfo.vLineInfo.nSmallGridLinewidth);
			m_GridInfo.vLineInfo.nSmallGridLinewidth = AfxGetComConfig()->SetLWLevel(nLWLevel);
			xml.Read(DSF_GRID_COLOR, m_GridInfo.vLineInfo.colorSmallGrid);
			xml.Read(DSF_GRID_LINETYPE,
					m_GridInfo.vLineInfo.nSmallGridLinetype);
			xml.Back();
		}

		if (xml.Read(DSF_GRID_VGGROUP, m_GridInfo.vLineInfo.nGroupCount))
		{
			xml.Read(DSF_GRID_VGLINE, m_GridInfo.vLineInfo.nLineCount);
		}
		else
		{
			xml.Read(DSF_GRID_GROUP, m_GridInfo.vLineInfo.nGroupCount);
			xml.Read(DSF_GRID_LINE, m_GridInfo.vLineInfo.nLineCount);
			if ((m_GridInfo.vLineInfo.nGroupCount == 1) && ((m_GridInfo.vLineInfo.nLineCount % 2) == 0))
			{
				m_GridInfo.vLineInfo.nGroupCount = 2;
				m_GridInfo.vLineInfo.nLineCount = m_GridInfo.vLineInfo.nLineCount / 2;
			}
		}

		xml.Read(DSF_GRID_VGTYPE, m_GridInfo.vLineInfo.nGridType);

		// Read Curve
		for (int j = 0; j < (int) m_dwCurve; j++)
		{
			if (xml.Open(DSF_CURVE_IDFMT, j))
			{
				CCurve* pCurve = new CCurve(NB_TRACK);
				pCurve->Serialize(xml);
				xml.Back();
				AddCurve(pCurve);
			}
		}
	}
}

void CTrack::SerializeMark(CXMLSettings& xml)
{
	if (xml.IsStoring())
	{
	    m_TestMarker.Serialize(xml);
		int nCurveCount = m_vecCurve.size();
		xml.Write("CurveCount", nCurveCount);
		for (int i = 0; i <  nCurveCount; i++)
		{
			CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
			if (xml.CreateKey("Curve%d", i))
			{
				if (pCurve->m_pData)
				{
					xml.Write("LeftBound", pCurve->m_pData->m_LeftBound);
					xml.Write("RightBound", pCurve->m_pData->m_RightBound);
					xml.Write("LeftOutCount", pCurve->m_pData->m_nLeftOutNum);
					xml.Write("RightOutCount", pCurve->m_pData->m_nRightOutNum);
				}
				xml.Back();
			}
		}
	}
	else
	{
		m_Marker.m_pTrack = this;
		m_TestMarker.m_pTrack = this;
    	m_TestMarker.Serialize(xml);
//		SetType(m_nType);
		int nCurveCount = m_vecCurve.size();
		for (int i = 0; i < nCurveCount; i++)
		{
			CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
			if (xml.Open("Curve%d", i))
			{
				if (pCurve->m_pData)
				{
					xml.Read("LeftBound", pCurve->m_pData->m_LeftBound);
					xml.Read("RightBound", pCurve->m_pData->m_RightBound);
					xml.Read("LeftOutCount", pCurve->m_pData->m_nLeftOutNum);
					xml.Read("RightOutCount", pCurve->m_pData->m_nRightOutNum);
				}
				xml.Back();
			}
		}

	}
}

ULMINTIMP CTrack::GetPrintDepthAngle()
{
	int nAngle = 1800;
	if (g_pMainWnd->m_ULPrintInfo.GetPrintDirection() == SCROLL_UP)
	{
		if (AfxGetComConfig()->m_Plot.DepthAngle == 0)
			nAngle = 1800;
		if (AfxGetComConfig()->m_Plot.DepthAngle == 1)
			nAngle = 0;
		if (AfxGetComConfig()->m_Plot.DepthAngle == 2)
			nAngle = 2700;
		if (AfxGetComConfig()->m_Plot.DepthAngle == 3)
			nAngle = 900;
	}
	else
	{
		if (AfxGetComConfig()->m_Plot.DepthAngle == 0)
			nAngle = 0;
		if (AfxGetComConfig()->m_Plot.DepthAngle == 1)
			nAngle = 1800;
		if (AfxGetComConfig()->m_Plot.DepthAngle == 2)
			nAngle = 900;
		if (AfxGetComConfig()->m_Plot.DepthAngle == 3)
			nAngle = 2700;
	}
	return nAngle;
}

ULMBOOLIMP CTrack::IsColorPrint()
{
	return AfxGetComConfig()->m_Plot.Colored;
}

ULMPTRIMP CTrack::GetDev()
{
	if (m_pDEV)
	{
		if (m_pDEV->m_pData)
		{
			if (m_pDEV->m_pData->m_vecCurve.size())
				return m_pDEV->m_pData->m_vecCurve.front();
		}
	}
	return m_pDEV;
}

ULMIMP CTrack::SetAzim(ICurve* pAZIM)
{
	if (m_pAZIM)
	{
		m_pAZIM->Release();
	}
	m_pAZIM = (CCurve*)pAZIM;
	if (m_pAZIM)
	{
		m_strAZIM = m_pAZIM->m_strName;
		m_pAZIM->AddRef();
	}
	else
		m_strAZIM.Empty();
	return UL_NO_ERROR;
}

ULMIMP CTrack::SetDev(ICurve* pDEV)
{
	if (m_pDEV)
	{
		m_pDEV->Release();
	}
	m_pDEV = (CCurve*)pDEV;
	if (m_pDEV)
	{
		m_strDEV = m_pDEV->m_strName;
		m_pDEV->AddRef();
	}
	else
		m_strDEV.Empty();
	return UL_NO_ERROR;
}

ULMINTIMP CTrack::GetDepthTitle(CStringArray* pTitles/* = NULL*/)
{
	CString string;

	CString strTemp;
	strTemp.LoadString(IDS_DEPT);

	string.Format(strTemp, g_units->DUnit());
	

	CWindowDC dc(NULL);
	int nMode = dc.SetMapMode(MM_LOMETRIC);
	CFont* pFont = (CFont*)dc.SelectObject(&g_font[ftCT]);
	int cx = dc.GetTextExtent(string).cx + 12;
	dc.SelectObject(pFont);
	dc.SetMapMode(nMode);
	if (cx < abs(m_rcRegion.Width()))
	{
		if (pTitles)
		{
			pTitles->RemoveAll();
			pTitles->Add(string);
		}
		return 1;
	}

	if (pTitles)
	{
		pTitles->Add(string.Left(4));
		pTitles->Add(string.Right(string.GetLength() - 4));
	}
	
	return 2;
}

ULMINTIMP CTrack::GetTimeTitle(CStringArray* pTitles/* = NULL*/)
{
	CString string;

	string="TIME(S)";
	
	CWindowDC dc(NULL);
	int nMode = dc.SetMapMode(MM_LOMETRIC);
	CFont* pFont = (CFont*)dc.SelectObject(&g_font[ftCT]);
	int cx = dc.GetTextExtent(string).cx + 12;
	dc.SelectObject(pFont);
	dc.SetMapMode(nMode);
	if (cx < abs(m_rcRegion.Width()))
	{
		if (pTitles)
		{
			pTitles->RemoveAll();
			pTitles->Add(string);
		}
		return 1;
	}
	
	if (pTitles)
	{
		pTitles->Add(string.Left(4));
		pTitles->Add(string.Right(string.GetLength() - 4));
	}
	
	return 2;
}

ULMPTRIMP CTrack::FindCurve(ICurve* pCurve, BOOL bLeft)
{
	if(pCurve == NULL)
		return NULL;

	FILLPROP* FillProp = (FILLPROP*)pCurve->GetFillProp();
	if (FillProp == NULL)
		return NULL;
	int i = 0;
	for (i = 0; i < m_vecCurve.size(); i++)
	{
		ICurve* curve = m_vecCurve.at(i);
		if (NULL == curve)
			continue;

		if (curve->IsInpre())
			continue;
		
		if (pCurve == curve)
			continue;

		if (bLeft)
		{
			if (_tcscmp(FillProp->szLCurveName, curve->Name()) == 0)
				return curve;
		}
		else
		{
			if (_tcscmp(FillProp->szRCurveName, curve->Name()) == 0)
				return curve;	
		}
	}

	for (i = 0; i < m_vecCurve.size(); i++)
	{
		ICurve* curve = m_vecCurve.at(i);
		if (NULL == curve)
			continue;

		if (!curve->IsInpre())
			continue;

		if (bLeft)
		{
			if (_tcscmp(FillProp->szLCurveName, curve->Name()) == 0)
				return curve;
		}
		else
		{
			if (_tcscmp(FillProp->szRCurveName, curve->Name()) == 0)
				return curve;	
		}
	}

	if (bLeft)
	{
		if (_tcscmp(FillProp->szLCurveName, pCurve->Name()) == 0)
			return pCurve;
	}
	else
	{
		if (_tcscmp(FillProp->szRCurveName, pCurve->Name()) == 0)
			return pCurve;
	}

	return NULL;
}

ULMDBLIMP CTrack::GetXDepth(long lDepth)
{
	return g_units->XDepth(lDepth);
}

//void CTrack::AddMarker(long lDepth,  int nCollarIndex/* = -1*/, int nStdCollarIndex/* = -1*/, 
//					   int nLineStyle /*= MARKS_PERFORATION_COLLOR*/, long lDepthAdjust/* = 0*/)
/*{
	CMark* pMark = new CMark;	
	pMark->m_strName.Format("%.2lf", DTM(lDepth-lDepthAdjust));
	pMark->m_lDepth = lDepth;
	pMark->m_pTrack = this;
	m_Marker.m_pTrack = this;
	pMark->m_nCollarIndex = nCollarIndex;
	pMark->m_nStdCollarIndex = nStdCollarIndex;		
	pMark->m_nShape = nLineStyle;
	pMark->m_lDepthAdjust = lDepthAdjust;
	
	m_Marker.AddMark(pMark);
}*/

void CTrack::SetProject(IProject* pProject)
{
	if (m_nType)
		m_pProject = pProject;
	else
		m_pProject = NULL;
}

void CTrack::DrawResults(CDC* pDC, long nStartDepth, long nEndDepth, CULPrintInfo* pInfo /* = NULL */)
{
	int nSelect = -1;
	CRect rcResult = m_rcRegion;
	if (pInfo != NULL)
	{
		rcResult = pInfo->CoordinateConvert(m_rcRegion);
	}

	rcResult.DeflateRect(3, 0, 3, 0);
	CArray<long, long> depths;
	CStringArray results;
	int nCount = m_pProject->GetOGResults(&depths, &results, &nSelect);
	for (int i = 0; i < nCount; i++)
	{
		int j = 2*i;
		// TRACE("%d - %d\n", depths[j], depths[j+1]);
		if (depths[j+1] < nStartDepth)
			continue;
		
		if (nEndDepth < depths[j])
			break;
		
		if (pInfo != NULL)
		{
			rcResult.top = pInfo->GetPrintDepthCoordinate(DTM(depths[j]));
			rcResult.bottom = pInfo->GetPrintDepthCoordinate(DTM(depths[j + 1]));
		}
		else
		{
			rcResult.top = GetCoordinateFromDepth(depths[j]);
			rcResult.bottom = GetCoordinateFromDepth(depths[j + 1]);
		}
		DrawResult(pDC, rcResult, results[i], (i == nSelect));			
	}
}

void CTrack::DrawResult(CDC* pDC, LPRECT lpRect, LPCTSTR pszResult, BOOL bSelected)
{
	if (pDC->IsPrinting())
		bSelected = FALSE;
	
	DrawPattern(pDC, lpRect, pszResult, RGB(0, 0, 0), RGB(255,255,255));
	
	CPen pen(PS_SOLID, 1, bSelected ? RGB(255, 0, 0) : RGB(0, 0, 0));
	CPen* pOldPen = pDC->SelectObject(&pen);
	CBrush* pOldBr = (CBrush*)pDC->SelectStockObject(NULL_BRUSH);
	pDC->Rectangle(lpRect);
	pDC->SelectObject(pOldBr);
	pDC->SelectObject(pOldPen);	
}

ULMIMP CTrack::DrawPat(CDC* pDC, LPRECT lpRect, LPCTSTR pszPat, COLORREF clrFore, COLORREF clrBkg)
{
	DrawPattern(pDC, lpRect, pszPat, clrFore, clrBkg);
	return UL_NO_ERROR;
}

ULMIMP CTrack::GetHeadRect(LPRECT lpRect, BOOL bPrint)
{
	CopyRect(lpRect, m_rcHead);

	if (bPrint)
	{
// 		lpRect->left -= PAGE_MARGIN;
// 		lpRect->right -= PAGE_MARGIN;
	}
	else
	{
		lpRect->left -= PAGE_MARGIN;
		lpRect->right -= PAGE_MARGIN;
		lpRect->left = floor(((float)lpRect->left) * m_fRatioX + .5);
		lpRect->right = floor(((float)lpRect->right) * m_fRatioX + .5);
		lpRect->left += PAGE_MARGIN;
		lpRect->right += PAGE_MARGIN;
	}

	return UL_NO_ERROR;
}

ULMIMP CTrack::GetRegionRect(LPRECT lpRect, BOOL bPrint)
{
	CopyRect(lpRect, m_rcRegion);
	if (bPrint)
	{
// 		lpRect->left -= PAGE_MARGIN;
// 		lpRect->right -= PAGE_MARGIN;
	}
	else
	{
		lpRect->left -= PAGE_MARGIN;
		lpRect->right -= PAGE_MARGIN;
		lpRect->left = floor(((float)lpRect->left) * m_fRatioX + .5);
		lpRect->right = floor(((float)lpRect->right) * m_fRatioX + .5);
		lpRect->left += PAGE_MARGIN;
		lpRect->right += PAGE_MARGIN;
	}

	return UL_NO_ERROR;
}

ULMINTIMP CTrack::GetGridWidth(int nGrid, BOOL bPrint)
{
	CRect rcTrack;
	GetRegionRect(&rcTrack, bPrint);
	
	int nCount = GetVertGridCount();
	if (nCount < 1)
		return rcTrack.left;
	
	if (nGrid < 0)
		nGrid = 0;
	else if (nGrid > nCount)
		nGrid = nCount;
	
	return rcTrack.left + ((double)nGrid/(double)nCount)*rcTrack.Width();
}

void CTrack::DrawHorzGridTime(CDC* pDC, long lStartDepth, long lEndDepth)
{
	int nStartDepth = min(lStartDepth, lEndDepth);
	int nEndDepth = max(lStartDepth, lEndDepth);
	
	if (m_pProject)
	{
		DrawResultsTime(pDC, nStartDepth, nEndDepth);
		return;
	}

	if (m_GridInfo.bHasHLine)
	{
		unsigned pattern[8];
		int nLinewidth, nLinetype;
		COLORREF colorLine;
		long lDepth;
		
		int nLwMin[3] = {0, 5, 8};
		if (m_nDriveMode)
		{
			for(int K1=0;K1<3;K1++)
			{
				m_GridInfo.hLine[K1].nGridStep = m_dHorzGridDepth[K1];
			}
		}
		else
		{
			for(int K1=0;K1<3;K1++)
			{
				m_GridInfo.hLine[K1].nGridStep = m_dHorzGridTime[K1];
			}
		}
		//int nDelta = m_GridInfo.hLine[2].nGridStep;//by xwh 时间变量修改
		int nDelta = m_dHorzGridTime[2]; 
		if (m_nDriveMode == UL_DRIVE_TIME)
		{
			nDelta /= m_fRatioY;
		}
		else
		{
			nDelta /= m_fRatioTime;
		}
		
		int nLStart = nStartDepth - nDelta;
		int nLEnd = nEndDepth + nDelta;

		if (nLStart < m_lStartTime)
			nLStart = m_lStartTime;
		if (nLEnd > m_lEndTime)
			nLEnd = m_lEndTime;
		
		CRect rcTrack;
		GetRegionRect(&rcTrack, FALSE);

		for (int i = 0; i < 3; i++)
		{
			if (m_GridInfo.hLine[i].bHasGrid)
			{
				// 线宽
				//nLinewidth = nLwMin[i];
				//if (LVWidth(m_GridInfo.hLine[i].nGridLinewidth) > nLwMin[i])
				//	nLinewidth = LVWidth(m_GridInfo.hLine[i].nGridLinewidth);
				nLinewidth = LVWidth(m_GridInfo.hLine[i].nGridLinewidth);
				// 线型
				nLinetype = m_GridInfo.hLine[i].nGridLinetype;

				// 线色
				colorLine = m_GridInfo.hLine[i].clrGrid;
				
				CDashLine gridLine(*pDC, pattern, 
					CDashLine::GetPattern(pattern,0, 2 * nLinewidth, nLinetype));
				CPen penGrid(PS_SOLID, nLinewidth, colorLine);
				
				//int nStart = (nStartDepth + 1) / m_GridInfo.hLine[i].nGridStep - 2;//COMMENT BY XWH 时间变量修改
				//int nEnd = (nEndDepth - 1) / m_GridInfo.hLine[i].nGridStep + 2;
				int nStart = (nStartDepth + 1) / m_dHorzGridTime[i]- 2;
				int nEnd = (nEndDepth - 1) / m_dHorzGridTime[i] + 2;
				int nRight = rcTrack.right;
				if (i == 2)
					nRight -= 2;
				
				for (int j = nStart; j <= nEnd; j++)
				{
				/*	lDepth = j * m_GridInfo.hLine[i].nGridStep;*/
					lDepth = j *  m_dHorzGridTime[i]; //时间变量修改
					if (lDepth < nLStart)
						continue;

					if (lDepth > nLEnd)
						continue;
					
					int y = GetCoordinateFromTime(lDepth);
					
					CPen* pOldPen = pDC->SelectObject(&penGrid);
					gridLine.MoveTo(rcTrack.left, y);
					gridLine.LineTo(nRight, y);
					pDC->SelectObject(pOldPen);
				}
			}
		}
	}

	//Start:画深度曲线
	for (int i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve = m_vecCurve.at(i);
		if (pCurve->IsTimeCurve() && GetDriveMode() == UL_DRIVE_TIME)
		{
			pCurve->Draw(pDC, lStartDepth, lEndDepth);
		}
	}	
}

void CTrack::DrawResultsTime(CDC* pDC, long nStartDepth, long nEndDepth, CULPrintInfo* pInfo /* = NULL */)
{
	int nSelect = -1;
	CRect rcResult = m_rcRegion;
	if (pInfo != NULL)
	{
		rcResult = pInfo->CoordinateConvert(m_rcRegion);
	}
	
	rcResult.DeflateRect(3, 0, 3, 0);
	CArray<long, long> depths;
	CStringArray results;
	int nCount = m_pProject->GetOGResults(&depths, &results, &nSelect);
	for (int i = 0; i < nCount; i++)
	{
		int j = 2*i;
		// TRACE("%d - %d\n", depths[j], depths[j+1]);
		if (depths[j+1] < nStartDepth)
			continue;
		
		if (nEndDepth < depths[j])
			break;
		
		if (pInfo != NULL)
		{
			rcResult.top = pInfo->GetPrintDepthCoordinate(DTM(depths[j]));
			rcResult.bottom = pInfo->GetPrintDepthCoordinate(DTM(depths[j + 1]));
		}
		else
		{
			rcResult.top = GetCoordinateFromTime(depths[j]);
			rcResult.bottom = GetCoordinateFromTime(depths[j + 1]);
		}
		DrawResult(pDC, rcResult, results[i], (i == nSelect));			
	}
}

void CTrack::DrawResultTime(CDC* pDC, LPRECT lpRect, LPCTSTR pszResult, BOOL bSelected)
{
	if (pDC->IsPrinting())
		bSelected = FALSE;
	
	DrawPattern(pDC, lpRect, pszResult, RGB(0, 0, 0), RGB(255,255,255));
	
	CPen pen(PS_SOLID, 1, bSelected ? RGB(255, 0, 0) : RGB(0, 0, 0));
	CPen* pOldPen = pDC->SelectObject(&pen);
	CBrush* pOldBr = (CBrush*)pDC->SelectStockObject(NULL_BRUSH);
	pDC->Rectangle(lpRect);
	pDC->SelectObject(pOldBr);
	pDC->SelectObject(pOldPen);	
}

void CTrack::DrawVertGridTime(CDC* pDC, long lStart, long lEnd)
{
	int nStartPos = 0;
	int nEndPos = 0;

	nStartPos = GetCoordinateFromTime(lStart);
	nEndPos   = GetCoordinateFromTime(lEnd);

	
	if (m_pProject == NULL)
	{
		switch(m_GridInfo.vLineInfo.nGridType)
		{
		case 0:
			DrawVertGridLinear(pDC, nStartPos, nEndPos);
			break;
		case 1:
			DrawVertGridLog(pDC, nStartPos, nEndPos);
			break;
		}
	}
	
	CRect rcTrack;
	GetRegionRect(&rcTrack, FALSE);
	
	// 画井道边
	CPen pen(PS_SOLID, 5/*LVWidth(m_GridInfo.vLineInfo.nMiddleGridLinewidth)*/, TRACK_EDGE_COLOR);
	CPen* pOldPen = pDC->SelectObject(&pen);
	pDC->MoveTo(rcTrack.left, nStartPos);
	pDC->LineTo(rcTrack.left, nEndPos);
	pDC->MoveTo(rcTrack.right, nStartPos);
	pDC->LineTo(rcTrack.right, nEndPos);
	pDC->SelectObject(pOldPen);
}

void CTrack::DrawVertGridLinearTime(CDC* pDC, int nStartPos, int nEndPos)
{
	unsigned pattern[8];
	int nLinewidth, nLinetype;
	COLORREF colorLine;
	
	// 中格子画笔
	if (LVWidth(m_GridInfo.vLineInfo.nMiddleGridLinewidth) > 5)
		nLinewidth = LVWidth(m_GridInfo.vLineInfo.nMiddleGridLinewidth);
	else
		nLinewidth = 5;
	nLinetype = m_GridInfo.vLineInfo.nMiddleGridLinetype;
	colorLine = m_GridInfo.vLineInfo.colorMiddleGrid;
	CDashLine MiddleGridPen(*pDC, pattern, CDashLine::GetPattern(pattern, 0,2 * nLinewidth, nLinetype));
	CPen penMiddleGrid(PS_SOLID, nLinewidth, colorLine);
	
	// 小格子画笔
	nLinewidth = LVWidth(m_GridInfo.vLineInfo.nSmallGridLinewidth);
	nLinetype = m_GridInfo.vLineInfo.nSmallGridLinetype;
	colorLine = m_GridInfo.vLineInfo.colorSmallGrid;
	CDashLine SmallGridPen(*pDC, pattern, CDashLine::GetPattern(pattern, 0,2 * nLinewidth, nLinetype));
	CPen penSmallGrid(PS_SOLID, nLinewidth, colorLine);
	
	if (m_GridInfo.bHasVLine)
	{
		USHORT nLines = m_GridInfo.vLineInfo.nLineCount;	// 格数
		USHORT nGroup = m_GridInfo.vLineInfo.nGroupCount;	// 组数

		CRect rcTrack;
		GetRegionRect(&rcTrack, FALSE);
		
		int nGrid = nLines* nGroup;
		int nGridWidth = (int) (rcTrack.Width() / (float) nGrid);
		int nPos;

		for (int j = 0; j < nGrid; j ++)
		{
			nPos = rcTrack.left + (int) (j * rcTrack.Width() / (float) nGrid);
			if (nPos > rcTrack.right)
				nPos = rcTrack.right;
			
			// 画垂直中格子
			if ((j % m_GridInfo.vLineInfo.nLineCount) == 0)
			{
				if (m_GridInfo.vLineInfo.bHasMiddleGrid)
				{
					CPen* pOldPen = pDC->SelectObject(&penMiddleGrid);
					MiddleGridPen.MoveTo(nPos, nStartPos);
					MiddleGridPen.LineTo(nPos, nEndPos);
					pDC->SelectObject(pOldPen);
					continue;
				}
			}

			// 画垂直小格子
			if (m_GridInfo.vLineInfo.bHasSmallGrid)
			{
				CPen* pOldPen = pDC->SelectObject(&penSmallGrid);
				SmallGridPen.MoveTo(nPos, nStartPos);
				SmallGridPen.LineTo(nPos, nEndPos);
				pDC->SelectObject(pOldPen);
			}
		}
	}
}

void CTrack::DrawVertGridLogTime(CDC* pDC, int nStartPos, int nEndPos)
{
	unsigned pattern[8];
	int nLinewidth, nLinetype;
	COLORREF colorLine;

	// 中格子画笔
	if (LVWidth(m_GridInfo.vLineInfo.nMiddleGridLinewidth) > 5)
		nLinewidth = LVWidth(m_GridInfo.vLineInfo.nMiddleGridLinewidth);
	else
		nLinewidth = 5;
	nLinetype = m_GridInfo.vLineInfo.nMiddleGridLinetype;
	colorLine = m_GridInfo.vLineInfo.colorMiddleGrid;
	CDashLine MiddleGridPen(*pDC, pattern, CDashLine::GetPattern(pattern, 0,
											2 * nLinewidth, nLinetype));
	CPen penMiddleGrid(PS_SOLID, nLinewidth, colorLine);

	// 小格子画笔
	nLinewidth = LVWidth(m_GridInfo.vLineInfo.nSmallGridLinewidth);
	nLinetype = m_GridInfo.vLineInfo.nSmallGridLinetype;
	colorLine = m_GridInfo.vLineInfo.colorSmallGrid;
	CDashLine SmallGridPen(*pDC, pattern, CDashLine::GetPattern(pattern, 0,
											2 * nLinewidth, nLinetype));
	CPen penSmallGrid(PS_SOLID, nLinewidth, colorLine);

	float fLeftValue;
	float fRightValue;
	int size = m_vecCurve.size();

	// 判断是否有曲线
	if (size <= 0)
	{
		fLeftValue = 0.1f;
		fRightValue = 100;
	}
	else
	{
		// 因对数井道要求所有曲线左右值相同， 故只取第一条
		ICurve* pCurve = m_vecCurve.at(0);	
		// 曲线左右值
		fLeftValue = pCurve->LeftVal(); 
		fRightValue = pCurve->RightVal();
	}

	if (m_GridInfo.bHasVLine) // add by bao 修改对数格线算法
	{
		if (fLeftValue <= fRightValue) // 左值小于右值时
		{
			// 左值为零的处理:
			if (fLeftValue <= 0)
				fLeftValue = 0.001f;
			
			if (fRightValue <= 0)
			{
				fRightValue = 1000;
			}

			CString strLeft = "";
			int nLeft = 0;
			CRect rcTrack;
			GetRegionRect(&rcTrack, FALSE);
			// 线数:由输入确定
			USHORT nLines = GetVertGridCount();
			double fwidth = 1.0 * rcTrack.Width() / (log10(fRightValue) - log10(fLeftValue));
			float x = rcTrack.left;
			float fIncream = 0.0f;	// 与前一格线的增量
			float fPower = 0;		// 当前格线的数量级
			float fStart = fLeftValue;
			while(fStart < fRightValue)
			{
				strLeft.Format("%e", fStart);
				strLeft = strLeft.Right(4);
				nLeft = atoi(strLeft);
				fPower = pow(10.0, nLeft);
				fIncream = (10.0 / nLines) * fPower;
				if (fIncream <= 0)
				{
					fIncream = 1;
				}
				x += (log10(fStart + fIncream) - log10(fStart)) * fwidth;

				// 判断是否中格子
				strLeft.Format("%e", (fStart + fIncream));
				strLeft = strLeft.Left(1);
				nLeft = atoi(strLeft);
				BOOL bMGrid = FALSE;
				if (nLeft == 1)
				{
					bMGrid = TRUE;
				}

				fStart += fIncream;
				if (x > rcTrack.right)
				{
					x = rcTrack.right;
				}
				
				// 画垂直小格子
				if (bMGrid && m_GridInfo.vLineInfo.bHasMiddleGrid)
				{
				CPen* pOldPen = pDC->SelectObject(&penMiddleGrid);
				MiddleGridPen.MoveTo(x, nStartPos);
				MiddleGridPen.LineTo(x, nEndPos);
				pDC->SelectObject(pOldPen);
				continue;
				}
			
				if (m_GridInfo.vLineInfo.bHasSmallGrid)
				{
					CPen* pOldPen = pDC->SelectObject(&penSmallGrid);
					SmallGridPen.MoveTo(x, nStartPos);
					SmallGridPen.LineTo(x, nEndPos);
					pDC->SelectObject(pOldPen);
				}
			}
		} 
		else // 左值大于右值时 fLeftValue > fRightValue
		{
			// 右值为零的处理:
			if (fRightValue <= 0)
				fRightValue = 0.001f;
			
			if (fLeftValue <= 0)
			{
				fLeftValue = 1000;
			}
			
			CString strLeft = "";
			int nLeft = 0;
			CRect rcTrack;
			GetRegionRect(&rcTrack, FALSE);
			// 线数:由输入确定
			USHORT nLines = GetVertGridCount();
			double fwidth = 1.0 * rcTrack.Width() / (log10(fLeftValue) - log10(fRightValue));
			float x = rcTrack.right;
			float fIncream = 0.0f;	// 与前一格线的增量
			float fPower = 0;		// 当前格线的数量级
			float fStart = fRightValue;
			while(fStart < fLeftValue)
			{
				strLeft.Format("%e", fStart);
				strLeft = strLeft.Right(4);
				nLeft = atoi(strLeft);
				fPower = pow(10.0, nLeft);
				fIncream = (10.0 / nLines) * fPower;
				if (fIncream <= 0)
				{
					fIncream = 1;
				}
				x -= (log10(fStart + fIncream) - log10(fStart)) * fwidth;

				// 判断是否中格子
				strLeft.Format("%e", (fStart + fIncream));
				strLeft = strLeft.Left(1);
				nLeft = atoi(strLeft);
				BOOL bMGrid = FALSE;
				if (nLeft == 1)
				{
					bMGrid = TRUE;
				}

				fStart += fIncream;
				if (x < rcTrack.left)
				{
					x = rcTrack.left;
				}
				// end
				// 画垂直小格子
				if (bMGrid && m_GridInfo.vLineInfo.bHasMiddleGrid)
				{
				CPen* pOldPen = pDC->SelectObject(&penMiddleGrid);
				MiddleGridPen.MoveTo(x, nStartPos);
				MiddleGridPen.LineTo(x, nEndPos);
				pDC->SelectObject(pOldPen);
				continue;
				}
			
				if (m_GridInfo.vLineInfo.bHasSmallGrid)
				{
					CPen* pOldPen = pDC->SelectObject(&penSmallGrid);
					SmallGridPen.MoveTo(x, nStartPos);
					SmallGridPen.LineTo(x, nEndPos);
					pDC->SelectObject(pOldPen);
				}
			}
		}
		// end
	}
}

void CTrack::DrawTrackTime(CDC* pDC, long lStartDepth, long lEndDepth, int nMode)
{
	int i = 0;
	if (nMode == 0) // 静态绘制
	{
		for (i = 0; i < m_vecCurve.size(); i++)
		{
			CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
			if (pCurve->GetCurveMode() & CURVE_BACK || (pCurve->IsTimeCurve() && GetDriveMode() == UL_DRIVE_TIME))
				continue;

			if (!pCurve->IsInpre())
				pCurve->Draw(pDC, lStartDepth , lEndDepth);
		}
	}
	else			// 动态绘制
	{
		for (i = 0; i < m_vecCurve.size(); i++)
		{
			CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
			if (pCurve->GetCurveMode() & CURVE_BACK || (pCurve->IsTimeCurve() && GetDriveMode() == UL_DRIVE_TIME))
				continue;

			if (!pCurve->IsInpre())
				pCurve->LogDraw(pDC , lStartDepth , lEndDepth);
		}
	}
}

void CTrack::DrawFillTime(CDC* pDC, long lStartDepth, long lEndDepth, int nMode)
{
	int i = 0;
	if (nMode == 0) // 静态绘制
	{
		for (i = 0; i < m_vecCurve.size(); i++)
		{
			CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
			if (pCurve->GetCurveMode() & CURVE_BACK || pCurve->IsTimeCurve())
				continue;

			if (pCurve->IsInpre())
				pCurve->Draw(pDC, lStartDepth , lEndDepth);
		}
	}
	else			// 动态绘制
	{
		for (i = 0; i < m_vecCurve.size(); i++)
		{
			CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
			if (pCurve->GetCurveMode() & CURVE_BACK || pCurve->IsTimeCurve())
				continue;
			
			if (pCurve->IsInpre())
				pCurve->LogDraw(pDC , lStartDepth , lEndDepth);
		}
	}
}

void CTrack::DrawMarkTime(CDC* pDC, long lStartDepth, long lEndDepth)
{
	// 画标签
	m_TestMarker.DrawMark(pDC, lStartDepth, lEndDepth);
}

void CTrack::DrawBackCurveTime(CDC* pDC, long lStartDepth, long lEndDepth, BOOL bLogDraw)
{
	int nStartDepth = min(lStartDepth, lEndDepth);
	int nEndDepth = max(lStartDepth, lEndDepth);
	for (int i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve = m_vecCurve.at(i);
		if ((pCurve->GetCurveMode() & CURVE_BACK) == CURVE_BACK)
		{
			if (lStartDepth > lEndDepth)
			{
				nStartDepth = min(lStartDepth, pCurve->GetBKEnd());
				nEndDepth = max(lEndDepth, pCurve->GetBKBegin());
			}
			else
			{
				nStartDepth = max(lStartDepth, pCurve->GetBKBegin());
				nEndDepth = min(lEndDepth, pCurve->GetBKEnd());
			}
			
			pCurve->Draw(pDC, nStartDepth, nEndDepth);
		}
	}
}

// 绘制缩略图
void CTrack::DrawThumbTime(CDC* pDC, LPRECT lpRect, long ls, double dDelta,
					   int nPoint)
{
	for (int j = 0; j < m_vecCurve.size(); j++)
	{
		CCurve* pCurve = m_vecCurve.at(j);
		if (pCurve != NULL)
			pCurve->DrawThumb(pDC, lpRect, ls, dDelta, nPoint);
	}
}

void CTrack::PrintVertGridTime(CULPrintInfo* pInfo, double fMaxTime, double fMinTime)
{
	switch(m_GridInfo.vLineInfo.nGridType)
	{
	case 0:
		PrintVertGridLinearTime(pInfo, fMaxTime, fMinTime);
		break;
	case 1:
		PrintVertGridLogTime(pInfo, fMaxTime, fMinTime);
	}
}

void CTrack::PrintVertGridLinearTime(CULPrintInfo* pInfo, double fMaxTime, double fMinTime)
{
	unsigned pattern[8];
	
	// 中格子画笔
	int nLinewidth = LPWidth(m_GridInfo.vLineInfo.nMiddleGridLinewidth);
	int nLinetype = m_GridInfo.vLineInfo.nMiddleGridLinetype;
	COLORREF colorLine = m_GridInfo.vLineInfo.colorMiddleGrid;
	CDashLine MiddleGridPen(*pInfo->GetPrintDC(), pattern,
		CDashLine::GetPattern(pattern, 0, nLinewidth, nLinetype));
	CPen penMiddleGrid(PS_SOLID, nLinewidth, colorLine);
	
	// 小格子画笔
	nLinewidth = LPWidth(m_GridInfo.vLineInfo.nSmallGridLinewidth);
	nLinetype = m_GridInfo.vLineInfo.nSmallGridLinetype;
	colorLine = m_GridInfo.vLineInfo.colorSmallGrid;
	CDashLine SmallGridPen(*pInfo->GetPrintDC(), pattern,
		CDashLine::GetPattern(pattern, 0, nLinewidth, nLinetype));
	CPen penSmallGrid(PS_SOLID, nLinewidth, colorLine);
	
	CRect rcRegion;
	GetRegionRect(&rcRegion, TRUE); // 该测井道所占区域信息    
	rcRegion = pInfo->CoordinateConvert(rcRegion);
	
	// 上下边界
	int nBottom = pInfo->GetPrintTimeCoordinate(fMaxTime);
	int nTop = pInfo->GetPrintTimeCoordinate(fMinTime);
	
	rcRegion.top = nTop;
	rcRegion.bottom = nBottom;
	
	if (m_GridInfo.bHasVLine)
	{
		USHORT nLines = m_GridInfo.vLineInfo.nLineCount;
		USHORT nGroup = m_GridInfo.vLineInfo.nGroupCount;
		
		int nGrid = nLines* nGroup;
		float nGridWidth = rcRegion.Width() * 1.0 / nGrid;
		int nPos = 0;
		
		
		for (int j = 1; j <= nGrid; j ++)
		{
			nPos = rcRegion.left + j * nGridWidth;
			
			if (nPos > rcRegion.right)
				nPos = rcRegion.right;
			
			if (j == nGrid)
				continue;
			
			CPoint ptStart(nPos, nTop);
			CPoint ptEnd(nPos, nBottom);
			
			pInfo->CoordinateCalibrate(ptStart);
			pInfo->CoordinateCalibrate(ptEnd);
			
			// 打印垂直中格子
			if ((j % m_GridInfo.vLineInfo.nLineCount) == 0)
			{
				if (m_GridInfo.vLineInfo.bHasMiddleGrid)
				{
					CPen* pOldPen = pInfo->GetPrintDC()->SelectObject(&penMiddleGrid);
					MiddleGridPen.MoveTo(ptStart);
					MiddleGridPen.LineTo(ptEnd);
					pInfo->GetPrintDC()->SelectObject(pOldPen);
					continue;
				}
			}
			
			// 打印垂直小格子,每五个小格子组成一个中格子，没有大格子。
			if (m_GridInfo.vLineInfo.bHasSmallGrid)
			{
				CPen* pOldPen = pInfo->GetPrintDC()->SelectObject(&penSmallGrid);
				SmallGridPen.MoveTo(ptStart);
				SmallGridPen.LineTo(ptEnd);
				pInfo->GetPrintDC()->SelectObject(pOldPen);
			}
		}
	}
	
//	if (m_bPrintGap)
//		PrintGapsTime(pInfo, (fMaxTime), (fMinTime));
//	else
		PrintEdgeTime(pInfo, (fMaxTime), (fMinTime));// Not print gaps, print track edges
}

void CTrack::PrintVertGridLogTime(CULPrintInfo* pInfo, double fMaxTime, double fMinTime)
{
		unsigned pattern[8];
	int nLinewidth, nLinetype;
	COLORREF colorLine;

	// 中格子画笔
	nLinewidth = LPWidth(m_GridInfo.vLineInfo.nMiddleGridLinewidth);
	nLinetype = m_GridInfo.vLineInfo.nMiddleGridLinetype;
	colorLine = m_GridInfo.vLineInfo.colorMiddleGrid;
	CDashLine MiddleGridPen(*pInfo->GetPrintDC(), pattern,
		CDashLine::GetPattern(pattern, 0, 2 * nLinewidth, nLinetype));
	CPen penMiddleGrid(PS_SOLID, nLinewidth, colorLine);
	CPen penEdge(PS_SOLID, 1.5 * nLinewidth, colorLine);

	// 小格子画笔
	nLinewidth = LPWidth(m_GridInfo.vLineInfo.nSmallGridLinewidth);
	nLinetype = m_GridInfo.vLineInfo.nSmallGridLinetype;
	colorLine = m_GridInfo.vLineInfo.colorSmallGrid;
	CDashLine SmallGridPen(*pInfo->GetPrintDC(), pattern,
		CDashLine::GetPattern(pattern, 0, 2 * nLinewidth, nLinetype));
	CPen penSmallGrid(PS_SOLID, nLinewidth, colorLine);

	CRect rcRegion;
	GetRegionRect(rcRegion, TRUE); // 该井道所占的区域信息。
	rcRegion = pInfo->CoordinateConvert(rcRegion);

	int nBottom = pInfo->GetPrintTimeCoordinate(fMaxTime);
	int nTop = pInfo->GetPrintTimeCoordinate(fMinTime);

	rcRegion.top = nTop;
	rcRegion.bottom = nBottom;

	double fLeftValue;
	double fRightValue;
	int nSize = m_vecCurve.size();		
	if (nSize < 1)
	{
		fLeftValue = 0.01;
		fRightValue = 100;
	}
	else
	{
		// 由第一条曲线的左右值决定格子的画法
		ICurve* pCurve = m_vecCurve.at(0);
		if (pCurve == NULL)
			return;

		fLeftValue = pCurve->LeftVal();
		fRightValue = pCurve->RightVal();
	}

	// add by bao 修改对数井道算法
	if (m_GridInfo.bHasVLine)
	{
		if (fLeftValue <= fRightValue) // 左值小于右值时
		{
			// 左值为零的处理:
			if (fLeftValue <= 0)
				fLeftValue = 0.001f;
			
			if (fRightValue <= 0)
			{
				fRightValue = 1000;
			}
			
			CString strLeft = "";
			int nLeft = 0;

			// 线数:由输入确定
			USHORT nLines = GetVertGridCount();
			double fwidth = 1.0 * rcRegion.Width() / (log10(fRightValue) - log10(fLeftValue));
			float x = rcRegion.left;
			float fIncream = 0.0f;	// 与前一格线的增量
			float fPower = 0;		// 当前格线的数量级
			float fStart = fLeftValue;

			int iGridCount = 0;
			while(fStart <= fRightValue)
			{
				strLeft.Format("%e", fStart);
				strLeft = strLeft.Right(4);
				nLeft = atoi(strLeft);
				fPower = pow(10.0, nLeft);
				fIncream = (10.0 / nLines) * fPower;
				if (fIncream <= 0)
				{
					fIncream = 1;
				}
				x += (log10(fStart + fIncream) - log10(fStart)) * fwidth;
				
				// 判断是否中格子
				strLeft.Format("%e", (fStart + fIncream));
				strLeft = strLeft.Left(1);
				nLeft = atoi(strLeft);
				BOOL bMGrid = FALSE;
				if (nLeft == 1)
				{
					bMGrid = TRUE;
				}
				
				fStart += fIncream;
				
				float fTemp = 0.0; // 当SCROLL_UP时，临时储存x的值
				if (pInfo->GetPrintDirection() == SCROLL_UP)
				{
					fTemp = x;
					x = rcRegion.right - (x - rcRegion.left);
					if (x < rcRegion.left)
						x = rcRegion.left ;
				}
				else
				{
					if (x > rcRegion.right)
					{
						x = rcRegion.right;
					}
					fTemp = x;
				}
				
				if ((x == rcRegion.left) || (x == rcRegion.right) || (fStart == fRightValue))
				{
					x = fTemp; // 如果向上实时打印时，将X值变回来，x代表的是距离值小得格线的距离
					continue;
				}
				
				CPoint ptStart(x, nTop);
				CPoint ptEnd(x, nBottom);
				
				pInfo->CoordinateCalibrate(ptStart);
				pInfo->CoordinateCalibrate(ptEnd);
				
				iGridCount++; // 已经画的格子数
				// 打印垂直小格子
				
				if (bMGrid && m_GridInfo.vLineInfo.bHasMiddleGrid)
				{
					CPen* pOldPen = pInfo->GetPrintDC()->SelectObject(&penMiddleGrid);
					MiddleGridPen.MoveTo(ptStart);
					MiddleGridPen.LineTo(ptEnd);
					pInfo->GetPrintDC()->SelectObject(pOldPen);
					x = fTemp; // 如果向上实时打印时，将X值变回来，x代表的是距离值小得格线的距离
					continue;
				}
				
				if (m_GridInfo.vLineInfo.bHasSmallGrid)
				{
					CPen* pOldPen = pInfo->GetPrintDC()->SelectObject(&penSmallGrid);
					SmallGridPen.MoveTo(ptStart);
					SmallGridPen.LineTo(ptEnd);
					pInfo->GetPrintDC()->SelectObject(pOldPen);
					x = fTemp; // 如果向上实时打印时，将X值变回来，x代表的是距离值小得格线的距离
				}
			}
		}
		else // 左值大于右值时 fLeftValue > fRightValue
		{
			// 右值为零的处理:
			if (fRightValue <= 0)
				fRightValue = 0.001f;
			
			if (fLeftValue <= 0)
			{
				fLeftValue = 1000;
			}

			CString strLeft = "";
			int nLeft = 0;
			
			// 线数:由输入确定
			USHORT nLines = GetVertGridCount();
			double fwidth = 1.0 * rcRegion.Width() / (log10(fLeftValue) - log10(fRightValue));
			float x = rcRegion.right;
			float fIncream = 0.0f;	// 与前一格线的增量
			float fPower = 0;		// 当前格线的数量级
			float fStart = fRightValue;

			int iGridCount = 0;
			while(fStart < fLeftValue)
			{
				strLeft.Format("%e", fStart);
				strLeft = strLeft.Right(4);
				nLeft = atoi(strLeft);
				fPower = pow(10.0, nLeft);
				fIncream = (10.0 / nLines) * fPower;
				if (fIncream <= 0)
				{
					fIncream = 1;
				}
				x -= (log10(fStart + fIncream) - log10(fStart)) * fwidth;
				
				// 判断是否中格子
				strLeft.Format("%e", (fStart + fIncream));
				strLeft = strLeft.Left(1);
				nLeft = atoi(strLeft);
				BOOL bMGrid = FALSE;
				if (nLeft == 1)
				{
					bMGrid = TRUE;
				}
				
				fStart += fIncream;
				
				float fTemp = 0.0; // 当SCROLL_UP时，临时储存x的值
				if (pInfo->GetPrintDirection() == SCROLL_UP)
				{
					fTemp = x;
					x = rcRegion.left + (rcRegion.right - x);
			//		TRACE("%f x:%f right:%f left:%f\n", fStart, x, (float)rcRegion.right, (float)rcRegion.left);
					if (x > rcRegion.right)
					{
						x = rcRegion.right ;
					}
				}
				else
				{
					if (x < rcRegion.left)
					{
						x = rcRegion.left;
					}
					fTemp = x;
				}
				
				if ((x == rcRegion.left) || (x == rcRegion.right) || (fStart == fRightValue))
				{
					x = fTemp; // 如果向上实时打印时，将X值变回来，x代表的是距离值小得格线的距离
					continue;
				}
				
				CPoint ptStart(x, nTop);
				CPoint ptEnd(x, nBottom);
				
				pInfo->CoordinateCalibrate(ptStart);
				pInfo->CoordinateCalibrate(ptEnd);
				
				iGridCount++; // 已经画的格子数
				// 打印垂直小格子
				
				if (bMGrid && m_GridInfo.vLineInfo.bHasMiddleGrid)
				{
					CPen* pOldPen = pInfo->GetPrintDC()->SelectObject(&penMiddleGrid);
					MiddleGridPen.MoveTo(ptStart);
					MiddleGridPen.LineTo(ptEnd);
					pInfo->GetPrintDC()->SelectObject(pOldPen);
					x = fTemp; // 如果向上实时打印时，将X值变回来，x代表的是距离值小得格线的距离
					continue;
				}
				
				if (m_GridInfo.vLineInfo.bHasSmallGrid)
				{
					CPen* pOldPen = pInfo->GetPrintDC()->SelectObject(&penSmallGrid);
					SmallGridPen.MoveTo(ptStart);
					SmallGridPen.LineTo(ptEnd);
					pInfo->GetPrintDC()->SelectObject(pOldPen);
					x = fTemp; // 如果向上实时打印时，将X值变回来，x代表的是距离值小得格线的距离
				}
			}
		}
		// end
	}

//	if (m_bPrintGap)
//	{
//		PrintGapsTime(pInfo, (fMaxTime), (fMinTime));
//		return;
//	}

	// 打印井道边
	PrintEdgeTime(pInfo, (fMaxTime), (fMinTime));
}

void CTrack::PrintHorzGridTime(CULPrintInfo* pInfo, double fMaxTime, double fMinTime)
{
	if (m_pProject)
	{
		long nStartDepth = MTD(fMinTime);
		long nEndDepth = MTD(fMaxTime);
		DrawResultsTime(pInfo->GetPrintDC(), nStartDepth, nEndDepth, pInfo);
		return;
	}
	
	//	m_TestMarker.PrintMark(pInfo, fMinTime, fMaxTime);
	
	if (!m_GridInfo.bHasHLine)
		return;
	
	unsigned pattern[8];
	int nLinewidth, nLinetype;
	COLORREF colorLine;
	
	int nStartlTime = (fMinTime);
	int nEndlTime = (fMaxTime);
	
	CRect rcRegion;
	GetRegionRect(rcRegion, TRUE);     
	rcRegion = pInfo->CoordinateConvert(rcRegion);
	
	for (int i = 0; i < 3; i++)
	{
		if (m_GridInfo.hLine[i].bHasGrid)
		{
			nLinewidth = LPWidth(m_GridInfo.hLine[i].nGridLinewidth);
			nLinetype = m_GridInfo.hLine[i].nGridLinetype;
			colorLine = m_GridInfo.hLine[i].clrGrid;
			
			CDashLine GridLine(*pInfo->GetPrintDC(), pattern,
				CDashLine::GetPattern(pattern, 0, nLinewidth, nLinetype));
			CPen penGrid(PS_SOLID, nLinewidth, colorLine);
			
			int nStart = (nStartlTime + 1) /  m_dHorzGridTime[i] - 1;
			int nEnd = (nEndlTime - 1) /  m_dHorzGridTime[i] + 1;
			
			CPen* pOldPen = pInfo->GetPrintDC()->SelectObject(&penGrid);
			for (int j = nStart; j <= nEnd; j++)
			{
				long lTime = j*  m_dHorzGridTime[i];
				if (lTime < nStartlTime)
				{
					continue;
				}
				if (lTime > nEndlTime)
				{
					break;
				}
				
				int y = pInfo->GetPrintTimeCoordinate((lTime));
				CPoint ptStart(rcRegion.left, y);
				CPoint ptEnd(rcRegion.right, y);
				pInfo->CoordinateCalibrate(ptStart);
				pInfo->CoordinateCalibrate(ptEnd);			
				
				GridLine.MoveTo(ptStart);
				GridLine.LineTo(ptEnd);
			}
			pInfo->GetPrintDC()->SelectObject(pOldPen);
		}
	}
}

ULMIMP CTrack::PrintGapsTime(CULPrintInfo* pInfo, long lMaxTime, long lMinTime)
{
	int nLWidth = LPWidth(m_GridInfo.vLineInfo.nMiddleGridLinewidth);
	int nLType = m_GridInfo.vLineInfo.nMiddleGridLinetype;
	COLORREF clr = AfxGetComConfig()->m_Plot.Colored ?
		m_GridInfo.vLineInfo.colorMiddleGrid :
		0;

	unsigned pattern[8];
	CDashLine MiddleGridPen(*pInfo->GetPrintDC(), pattern,
		CDashLine::GetPattern(pattern, 0, nLWidth, nLType));
	CPen penMiddleGrid(PS_SOLID, nLWidth, clr);

	CRect rcRegion;
	GetRegionRect(rcRegion, TRUE);	
	rcRegion = pInfo->CoordinateConvert(rcRegion);

	// Top - Bottom
	int nBottom = pInfo->GetPrintTimeCoordinate((lMaxTime));
	int nTop = pInfo->GetPrintTimeCoordinate((lMinTime));

	rcRegion.top = nTop;
	rcRegion.bottom = nBottom;

	// Not the first or last track

	if (m_nTrackIndex == 0)
	{
		CPen* pOldPen = pInfo->GetPrintDC()->SelectObject(&penMiddleGrid);
		CPoint ptStart(rcRegion.left, rcRegion.top);
		CPoint ptEnd(rcRegion.left, rcRegion.bottom);
		pInfo->CoordinateCalibrate(ptStart);
		pInfo->CoordinateCalibrate(ptEnd);
		MiddleGridPen.MoveTo(ptStart);
		MiddleGridPen.LineTo(ptEnd);
		CPoint ptStart1(rcRegion.right, rcRegion.top);
		CPoint ptEnd1(rcRegion.right, rcRegion.bottom);
		pInfo->CoordinateCalibrate(ptStart1);
		pInfo->CoordinateCalibrate(ptEnd1);
		MiddleGridPen.MoveTo(ptStart1);
		MiddleGridPen.LineTo(ptEnd1);
		pInfo->GetPrintDC()->SelectObject(pOldPen);
		return UL_NO_ERROR;
	}

	// First or last, if both the last is ignore

	CArray<int, int> nGapDepthArray;

	if (m_bRTPrint)
	{
		if (m_pDEPT == NULL)
			return UL_ERROR;

		int nSize = m_pDEPT->GetDataSize();
		if (nSize < 1)
			return UL_ERROR;

		long nTimeIntervals = m_nGapInterval * 1000;
		long nPrevTime = 0;
		long nNextTime = 0;
		nPrevTime = m_pDEPT->GetTime(m_nGapPos);
		for (int i = m_pDEPT->GetCurGapPos(); i < nSize; i++)
		{
			nNextTime = m_pDEPT->GetTime(i);
			if (abs(nNextTime - nPrevTime) >= nTimeIntervals)
			{
				long lTime = m_pDEPT->GetTime(i);
				if ((lMinTime <= lTime) && (lTime <= lMaxTime))
				{
					nPrevTime = nNextTime;
					m_nGapPos = i;
					// TRACE("Gap Depth : %ld\n", lDepth);
					int nY = pInfo->GetPrintTimeCoordinate((lTime));
					nGapDepthArray.Add(nY);
				}
			}
		}
	}
	else if (m_pGapDepthArray != NULL)
	{
		for (int i = 0; i < m_pGapDepthArray->GetSize(); i++)
		{
			long lTime = m_pGapDepthArray->GetAt(i);

		//	Add by zy 2012 4 17 修改打印速度缺口时，lDepth等于lMinTime 的情况下，无法打印问题
		//	if ((lMinTime < lDepth) && (lDepth < lMaxTime))
			if ((lMinTime <= lTime) && (lTime < lMaxTime))
			{
				int nY = pInfo->GetPrintTimeCoordinate((lTime));
				nGapDepthArray.Add(nY);
			}
		}
	}

	// Sort gap array
	int i = 0;
	for (i = 0; i < nGapDepthArray.GetSize(); i++)
	{
		int k = i;
		for (int j = i + 1; j < nGapDepthArray.GetSize(); j++)
		{
			if (nGapDepthArray[j] > nGapDepthArray[k])
				k = j;
		}

		if (k != i)
		{
			int nTemp = nGapDepthArray[i];
			nGapDepthArray[i] = nGapDepthArray[k];
			nGapDepthArray[k] = nTemp;
		}
	}

	// Remove the same element
	for (i = 1; i < nGapDepthArray.GetSize(); i++)
	{
		if (nGapDepthArray[i] == nGapDepthArray[i - 1])
		{
			nGapDepthArray.RemoveAt(i);
			// i--;
		}
	}

// 	if (nLWidth < 4)
// 		nLWidth = 4;
	CDashLine GapPen(*pInfo->GetPrintDC(), pattern, CDashLine::GetPattern(pattern,
													0, nLWidth, nLType));
	CPen penGapGrid(PS_SOLID, nLWidth, clr);
//	CPen penGapGrid(PS_SOLID, nLWidth, RGB(255,255,255));
//	CPen penGapGrid(PS_SOLID, nLWidth, RGB(0,0,0));
	CPen* pOldPen = pInfo->GetPrintDC()->SelectObject(&penGapGrid);

	CRect rc = rcRegion;
	if (m_bRTPrint && (pInfo->GetPrintDirection() == SCROLL_UP))
	{
		rc.left = rcRegion.right;
		rc.top = rcRegion.bottom;
		rc.right = rcRegion.left;
		rc.bottom = rcRegion.top;
	}

	int nMaxCoordinate = rc.bottom;

	CPoint pt0;
	pt0.x = (m_nTrackIndex & TI_FIRST) ? rc.left : rc.right;
	pt0.y = rc.top;

	CPoint pt1 = pt0;

	if (m_bGapRemain)
	{
		pt0.y -= m_nGapRemain;
		m_bGapRemain = FALSE;
	}

	for (i = 0; i < nGapDepthArray.GetSize(); i++)
	{
	GapPen.MoveTo(pInfo->CoordCalibrate(pt0));
		pt1.y = nGapDepthArray.GetAt(i);

		GapPen.LineTo(pInfo->CoordCalibrate(pt1));
		pt0.y = pt1.y - m_nGapSize;

		if (pt0.y < nMaxCoordinate)
		{
			m_bGapRemain = TRUE;
			m_nGapRemain = nMaxCoordinate - pt0.y;
			break;
		}

	}

	if (!m_bGapRemain)
	{
		pt1.y = nMaxCoordinate;
		GapPen.MoveTo(pInfo->CoordCalibrate(pt0));
		GapPen.LineTo(pInfo->CoordCalibrate(pt1));

	}

	nGapDepthArray.RemoveAll();

	return UL_NO_ERROR;
}

ULMIMP CTrack::PrintEdgeTime(CULPrintInfo* pInfo, long lMaxTime, long lMinTime)
{
	int nLWidth = LPWidth(m_GridInfo.vLineInfo.nMiddleGridLinewidth);
	COLORREF clr = AfxGetComConfig()->m_Plot.Colored ?
		m_GridInfo.vLineInfo.colorMiddleGrid :
	0;
	
	CRect rcRegion;
	GetRegionRect(rcRegion, TRUE);	
	rcRegion = pInfo->CoordinateConvert(rcRegion);
	int nBottom = pInfo->GetPrintTimeCoordinate((lMaxTime));
	int nTop = pInfo->GetPrintTimeCoordinate((lMinTime));
	rcRegion.top = nTop;
	rcRegion.bottom = nBottom;
	
	CPoint ptStart(rcRegion.left, rcRegion.top);
	CPoint ptEnd(rcRegion.left, rcRegion.bottom);
	pInfo->CoordinateCalibrate(ptStart);
	pInfo->CoordinateCalibrate(ptEnd);
	
	CPen penEdge(PS_SOLID, max(4, nLWidth), RGB(0, 0, 0));
	CPen penMiddleGrid(PS_SOLID, nLWidth, RGB(0, 0, 0));
	CPen* pOldPen = pInfo->GetPrintDC()->SelectObject(&penMiddleGrid);
	if (m_nTrackIndex & TI_FIRST)
	{
		pInfo->GetPrintDC()->SelectObject(&penEdge);
	}
	
	pInfo->GetPrintDC()->MoveTo(ptStart);
	pInfo->GetPrintDC()->LineTo(ptEnd);
	
	CPoint ptStart1(rcRegion.right, rcRegion.top);
	CPoint ptEnd1(rcRegion.right, rcRegion.bottom);
	pInfo->CoordinateCalibrate(ptStart1);
	pInfo->CoordinateCalibrate(ptEnd1);
	
	if (m_nTrackIndex & TI_LAST)
	{
		if ((m_nTrackIndex & TI_FIRST) == 0)
			pInfo->GetPrintDC()->SelectObject(&penEdge);
	}
	else if (m_nTrackIndex & TI_FIRST)
	{
		pInfo->GetPrintDC()->SelectObject(&penMiddleGrid);
	}
	
	pInfo->GetPrintDC()->MoveTo(ptStart1);
	pInfo->GetPrintDC()->LineTo(ptEnd1);
	pInfo->GetPrintDC()->SelectObject(pOldPen);
	return UL_NO_ERROR;
}
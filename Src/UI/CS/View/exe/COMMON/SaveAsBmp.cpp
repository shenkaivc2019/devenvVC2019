#include "StdAfx.h"
//#include "UL2000.h"
#include "Resource.h"
#include "SaveAsBmp.h"
#include "MyMEMDC.h"

#ifdef _TIFFSUPPORT

#include "tiffio.h"
#include "dibtiff.h"

#endif

#define TIFF_GAMMA  2.2

ImageFileTypes DetermineFileType(LPCTSTR filename)
{
	ImageFileTypes result = UnknownFileType;
	
	CString tmpFile = filename;
	tmpFile.MakeUpper();
	
	int length = tmpFile.GetLength();
	if (length > 4)
	{
		TCHAR* cptr = (TCHAR*)(LPCTSTR)tmpFile;
		
		int last4 = length - 4;
		if (last4 < 0) last4 = 0;
		int last5 = length - 5;
		if (last5 < 0) last5 = 0;
		
		if (_tcscmp(&cptr[last4], _T(".BMP")) == 0)
		{
			result = BMPFileType;
		}
		else if ((_tcscmp(&cptr[last4], _T(".JPG")) == 0) ||
			(_tcscmp(&cptr[last5], _T(".JPEG")) == 0))
		{
			result = JPEGFileType;
		}
		else if ((_tcscmp(&cptr[last4], _T(".TIF")) == 0) ||
			(_tcscmp(&cptr[last5], _T(".TIFF")) == 0))
		{
			result = TIFFFileType;
		}
		else if (_tcscmp(&cptr[last4], _T(".GIF")) == 0)
		{
			result = GIFFileType;
		}
		else if (cptr[last4] == '.')
		{
			result = OtherFileType;
		}
	}
	
	return result;
}

typedef BOOL (*lpFun)(HANDLE, LPCTSTR, BOOL, int);
BOOL WriteDCToDIB(LPCTSTR szFile, CDC* pDC)
{
	// Get Current MemDC Bitmap
	CBitmap *pBitmap = pDC->GetCurrentBitmap();

	// Create logical palette if device support a palette
	CPalette pal;
	if( pDC->GetDeviceCaps(RASTERCAPS) & RC_PALETTE )
	{
		UINT nSize = sizeof(LOGPALETTE) + (sizeof(PALETTEENTRY) * 256);
		LOGPALETTE *pLP = (LOGPALETTE *) new BYTE[nSize];
		pLP->palVersion = 0x300;

		pLP->palNumEntries = 
			GetSystemPaletteEntries( *pDC, 0, 255, pLP->palPalEntry );

		// Create the palette
		pal.CreatePalette( pLP );

		delete[] pLP;
	}

	// Convert the bitmap to a DIB
	HANDLE hDIB = DDBToDIB( *pBitmap, BI_RGB, &pal, pDC);

	if( hDIB == NULL )
		return FALSE;

	int result = 0;
	CString errMsg;

	// Write it to file
	ImageFileTypes fileType = DetermineFileType(szFile);
	switch (fileType)
	{
	case BMPFileType:
		{
			result = WriteDIB(szFile, hDIB);
		}
		break;
	case JPEGFileType:
		{
			lpFun DIBToJpg;
			HINSTANCE hInst = LoadLibrary(_T("jpgvsbmp.dll"));
			if (hInst == NULL)
			{
				CString str;
				str.LoadString(IDS_WRITEDCTODIB_LOAD_FAIL);
				AfxMessageBox(str);//加载jpgvsbmp.dll失败!
				//AfxMessageBox(_T("加载jpgvsbmp.dll失败!"));
			}
			else
			{
				DIBToJpg = (lpFun)GetProcAddress(hInst, "DIBToJpg");
				if (DIBToJpg != NULL)
				{
					result =  DIBToJpg(hDIB, szFile, TRUE, 100);
				}	
				FreeLibrary(hInst); 
			}

		 
		}
		break;
	case TIFFFileType:
		{
#ifdef _TIFFSUPPORT
			LPBITMAPINFOHEADER lpbi = (LPBITMAPINFOHEADER)hDIB;
			result = WriteTIF(szFile, lpbi, (LPBYTE)lpbi + 40);
#else
			errMsg = "TIF files not supported";
#endif
		}
		break;
	case GIFFileType:
		{
			errMsg = "GIF files not supported";
		}
		break;
	case OtherFileType:
		{
			errMsg = "File type not supported";
		}
		break;
	case UnknownFileType:
		{
			errMsg = "Unknown file type";
		}
		break;
	}

	// Free the memory allocated by DDBToDIB for the DIB
	GlobalFree( hDIB );
	return result;
}

BOOL WriteWindowToDIB( LPCTSTR szFile, CWnd *pWnd )
{
	CBitmap 	bitmap;
	CWindowDC	dc(pWnd);
	CDC 		memDC;
	CRect		rect;
	
	memDC.CreateCompatibleDC(&dc); 
	
	pWnd->GetWindowRect(rect);
	
	bitmap.CreateCompatibleBitmap(&dc, rect.Width(),rect.Height() );
	
	CBitmap* pOldBitmap = memDC.SelectObject(&bitmap);
	memDC.BitBlt(0, 0, rect.Width(), rect.Height(), &dc, 0, 0, SRCCOPY);
	
	// Create logical palette if device support a palette
	CPalette pal;
	if( dc.GetDeviceCaps(RASTERCAPS) & RC_PALETTE )
	{
		UINT nSize = sizeof(LOGPALETTE) + (sizeof(PALETTEENTRY) * 256);
		LOGPALETTE *pLP = (LOGPALETTE *) new BYTE[nSize];
		pLP->palVersion = 0x300;
		
		pLP->palNumEntries = 
			GetSystemPaletteEntries( dc, 0, 255, pLP->palPalEntry );
		
		// Create the palette
		pal.CreatePalette( pLP );
		
		delete[] pLP;
	}
	
	memDC.SelectObject(pOldBitmap);
	
	// Convert the bitmap to a DIB
	HANDLE hDIB = DDBToDIB( bitmap, BI_RGB, &pal );
	
	if( hDIB == NULL )
		return FALSE;
	
	// Write it to file
	WriteDIB( szFile, hDIB );
	
	// Free the memory allocated by DDBToDIB for the DIB
	GlobalFree( hDIB );
	return TRUE;
}

BOOL WriteWindowToDC( CDC* pDC, LPRECT lpRect, CWnd *pWnd )
{
	CBitmap 	bitmap;
	CWindowDC	dc(pWnd);
	CDC 		memDC;
	CRect		rect;
	
	memDC.CreateCompatibleDC(&dc); 
	
	pWnd->GetWindowRect(rect);
	
	bitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height());
	
	CBitmap* pOldBitmap = memDC.SelectObject(&bitmap);
	memDC.BitBlt(0, 0, rect.Width(), rect.Height(), &dc, 0, 0, SRCCOPY);
	
	// Create logical palette if device support a palette
	CPalette pal;
	if( dc.GetDeviceCaps(RASTERCAPS) & RC_PALETTE )
	{
		UINT nSize = sizeof(LOGPALETTE) + (sizeof(PALETTEENTRY) * 256);
		LOGPALETTE *pLP = (LOGPALETTE *) new BYTE[nSize];
		pLP->palVersion = 0x300;
		
		pLP->palNumEntries = 
			GetSystemPaletteEntries( dc, 0, 255, pLP->palPalEntry );
		
		// Create the palette
		pal.CreatePalette( pLP );
		
		delete[] pLP;
	}
	
	memDC.SelectObject(pOldBitmap);

	BITMAP bm;
	bitmap.GetBitmap(&bm);
	
	LPBYTE lpData = new BYTE[bm.bmWidthBytes * bm.bmHeight];
	ZeroMemory(lpData, bm.bmWidthBytes * bm.bmHeight);
	DWORD re = bitmap.GetBitmapBits(bm.bmWidthBytes * bm.bmHeight,(LPVOID)lpData);
	BITMAPINFOHEADER bi;
	bi.biSize		= sizeof(BITMAPINFOHEADER);
	bi.biWidth		= bm.bmWidth;
	bi.biHeight 		= bm.bmHeight;
	bi.biPlanes 		= 1;
	bi.biBitCount		= 32;
	bi.biCompression	= BI_RGB;
	bi.biSizeImage = ((((bi.biWidth * bi.biBitCount) + 31) & ~31) / 8) * bi.biHeight;
	bi.biXPelsPerMeter	= 0;
	bi.biYPelsPerMeter	= 0;
	bi.biClrUsed		= 0;
	bi.biClrImportant	= 0;

	CRect rcDest = lpRect;
	::StretchDIBits(pDC->GetSafeHdc(), rcDest.left, rcDest.bottom, rcDest.Width(), - rcDest.Height(),
		0, 0, bm.bmWidth, bm.bmHeight, (LPVOID)lpData, (BITMAPINFO*)&bi, DIB_RGB_COLORS, SRCCOPY);

	// Free the memory allocated by DDBToDIB for the DIB
	delete[] lpData;
	return TRUE;
}

BOOL WriteWindowToDC( CDC* pDC, LPRECT lpRect, CWnd *pWnd, CRect rcWnd)
{
	CBitmap 	bitmap;
	CWindowDC	dc(pWnd);
	CDC 		memDC;
	CRect		rect;
	
	memDC.CreateCompatibleDC(&dc); 
	
	pWnd->GetWindowRect(rect);
	rect.DeflateRect(rcWnd.left, rcWnd.top, 0, 0);
	rect.right = rect.left + rcWnd.Width();
	rect.bottom = rect.top + rcWnd.Height();
	
	if (!bitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()))
		return FALSE;
	
	CBitmap* pOldBitmap = memDC.SelectObject(&bitmap);
	memDC.BitBlt(0, 0, rect.Width(), rect.Height(), &dc, rcWnd.left, rcWnd.top, SRCCOPY);
	
	// Create logical palette if device support a palette
	CPalette pal;
	if( dc.GetDeviceCaps(RASTERCAPS) & RC_PALETTE )
	{
		UINT nSize = sizeof(LOGPALETTE) + (sizeof(PALETTEENTRY) * 256);
		LOGPALETTE *pLP = (LOGPALETTE *) new BYTE[nSize];
		pLP->palVersion = 0x300;
		
		pLP->palNumEntries = 
			GetSystemPaletteEntries( dc, 0, 255, pLP->palPalEntry );
		
		// Create the palette
		pal.CreatePalette( pLP );
		
		delete[] pLP;
	}
	
	memDC.SelectObject(pOldBitmap);
	
	BITMAP bm;
	bitmap.GetBitmap(&bm);
	
	LPBYTE lpData = new BYTE[bm.bmWidthBytes * bm.bmHeight];
	ZeroMemory(lpData, bm.bmWidthBytes * bm.bmHeight);
	DWORD re = bitmap.GetBitmapBits(bm.bmWidthBytes * bm.bmHeight,(LPVOID)lpData);
	BITMAPINFOHEADER bi;
	bi.biSize		= sizeof(BITMAPINFOHEADER);
	bi.biWidth		= bm.bmWidth;
	bi.biHeight 		= bm.bmHeight;
	bi.biPlanes 		= 1;
	bi.biBitCount		= 32;
	bi.biCompression	= BI_RGB;
	bi.biSizeImage = ((((bi.biWidth * bi.biBitCount) + 31) & ~31) / 8) * bi.biHeight;
	bi.biXPelsPerMeter	= 0;
	bi.biYPelsPerMeter	= 0;
	bi.biClrUsed		= 0;
	bi.biClrImportant	= 0;
	
	CRect rcDest = lpRect;
	::StretchDIBits(pDC->GetSafeHdc(), rcDest.left, rcDest.bottom, rcDest.Width(), - rcDest.Height(),
		0, 0, bm.bmWidth, bm.bmHeight, (LPVOID)lpData, (BITMAPINFO*)&bi, DIB_RGB_COLORS, SRCCOPY);
	
	// Free the memory allocated by DDBToDIB for the DIB
	delete[] lpData;
	return TRUE;
}

BOOL WriteClientToDC( CDC* pDC, LPRECT lpRect, CWnd *pWnd )
{
	CBitmap 	bitmap;
	CClientDC	dc(pWnd);
	CDC 		memDC;
	CRect		rect;
	
	memDC.CreateCompatibleDC(&dc); 
	
	pWnd->GetClientRect(rect);
	
	bitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height());
	
	CBitmap* pOldBitmap = memDC.SelectObject(&bitmap);
	memDC.BitBlt(0, 0, rect.Width(), rect.Height(), &dc, 0, 0, SRCCOPY);
	
	// Create logical palette if device support a palette
	CPalette pal;
	if( dc.GetDeviceCaps(RASTERCAPS) & RC_PALETTE )
	{
		UINT nSize = sizeof(LOGPALETTE) + (sizeof(PALETTEENTRY) * 256);
		LOGPALETTE *pLP = (LOGPALETTE *) new BYTE[nSize];
		pLP->palVersion = 0x300;
		
		pLP->palNumEntries = 
			GetSystemPaletteEntries( dc, 0, 255, pLP->palPalEntry );
		
		// Create the palette
		pal.CreatePalette( pLP );
		
		delete[] pLP;
	}
	
	memDC.SelectObject(pOldBitmap);
	
	BITMAP bm;
	bitmap.GetBitmap(&bm);
	
	LPBYTE lpData = new BYTE[bm.bmWidthBytes * bm.bmHeight];
	ZeroMemory(lpData, bm.bmWidthBytes * bm.bmHeight);
	DWORD re = bitmap.GetBitmapBits(bm.bmWidthBytes * bm.bmHeight,(LPVOID)lpData);
	BITMAPINFOHEADER bi;
	bi.biSize		= sizeof(BITMAPINFOHEADER);
	bi.biWidth		= bm.bmWidth;
	bi.biHeight 		= bm.bmHeight;
	bi.biPlanes 		= 1;
	bi.biBitCount		= 32;
	bi.biCompression	= BI_RGB;
	bi.biSizeImage = ((((bi.biWidth * bi.biBitCount) + 31) & ~31) / 8) * bi.biHeight;
	bi.biXPelsPerMeter	= 0;
	bi.biYPelsPerMeter	= 0;
	bi.biClrUsed		= 0;
	bi.biClrImportant	= 0;
	
	CRect rcDest = lpRect;
	::StretchDIBits(pDC->GetSafeHdc(), rcDest.left, rcDest.bottom, rcDest.Width(), - rcDest.Height(),
		0, 0, bm.bmWidth, bm.bmHeight, (LPVOID)lpData, (BITMAPINFO*)&bi, DIB_RGB_COLORS, SRCCOPY);
	
	// Free the memory allocated by DDBToDIB for the DIB
	delete[] lpData;
	return TRUE;
}

// DDBToDIB		- Creates a DIB from a DDB
// bitmap		- Device dependent bitmap
// dwCompression	- Type of compression - see BITMAPINFOHEADER
// pPal			- Logical palette
HANDLE DDBToDIB( CBitmap& bitmap, DWORD dwCompression, CPalette* pPal, CDC* pDC /* = NULL  */)
{
	BITMAP			bm;
	BITMAPINFOHEADER	bi;
	LPBITMAPINFOHEADER 	lpbi;
	DWORD			dwLen;
	HANDLE			hDIB;
	HANDLE			handle;
	HDC 			hDC;
	HPALETTE		hPal;
	
	
	ASSERT( bitmap.GetSafeHandle() );
	
	// The function has no arg for bitfields
	if( dwCompression == BI_BITFIELDS )
		return NULL;
	
	// If a palette has not been supplied use defaul palette
	hPal = (HPALETTE) pPal->GetSafeHandle();
	if (hPal == NULL)
		hPal = (HPALETTE) GetStockObject(DEFAULT_PALETTE);
	
	// Get bitmap information
	bitmap.GetObject(sizeof(bm),(LPSTR)&bm);
	
	// Initialize the bitmapinfoheader
	bi.biSize		= sizeof(BITMAPINFOHEADER);
	bi.biWidth		= bm.bmWidth;
	bi.biHeight 		= bm.bmHeight;
	bi.biPlanes 		= 1;
	bi.biBitCount		= bm.bmPlanes * bm.bmBitsPixel;
	bi.biCompression	= dwCompression;
	bi.biSizeImage		= 0;
	bi.biXPelsPerMeter	= 0;
	bi.biYPelsPerMeter	= 0;
	bi.biClrUsed		= 0;
	bi.biClrImportant	= 0;
	
	// Compute the size of the  infoheader and the color table
	int nColors = (1 << bi.biBitCount);
	if( nColors > 256 ) 
		nColors = 0;
	dwLen  = bi.biSize + nColors * sizeof(RGBQUAD);
	
	// We need a device context to get the DIB from
	//hDC = GetDC(NULL);
	if (pDC == NULL)
		hDC = GetDC(NULL);
	else
		hDC = pDC->m_hDC;

	hPal = SelectPalette(hDC,hPal,FALSE);
	RealizePalette(hDC);
	
	// Allocate enough memory to hold bitmapinfoheader and color table
	hDIB = GlobalAlloc(GMEM_FIXED, dwLen);
	
	if (!hDIB)
	{
		TRACE(_T("Error : Out of memory!\n"));
		SelectPalette(hDC,hPal,FALSE);
		ReleaseDC(NULL,hDC);
		return NULL;
	}
	
	lpbi = (LPBITMAPINFOHEADER)hDIB;
	
	*lpbi = bi;
	
	// Call GetDIBits with a NULL lpBits param, so the device driver 
	// will calculate the biSizeImage field 
	GetDIBits(hDC, (HBITMAP)bitmap.GetSafeHandle(), 0L, (DWORD)bi.biHeight,
		(LPBYTE)NULL, (LPBITMAPINFO)lpbi, (DWORD)DIB_RGB_COLORS);
	
	bi = *lpbi;
	
	// If the driver did not fill in the biSizeImage field, then compute it
	// Each scan line of the image is aligned on a DWORD (32bit) boundary
	if (bi.biSizeImage == 0){
		bi.biSizeImage = ((((bi.biWidth * bi.biBitCount) + 31) & ~31) / 8) 
			* bi.biHeight;
		
		// If a compression scheme is used the result may infact be larger
		// Increase the size to account for this.
		if (dwCompression != BI_RGB)
			bi.biSizeImage = (bi.biSizeImage * 3) / 2;
	}
	
	// Realloc the buffer so that it can hold all the bits
	dwLen += bi.biSizeImage;
	if (handle = GlobalReAlloc(hDIB, dwLen, GMEM_MOVEABLE))
		hDIB = handle;
	else{
		GlobalFree(hDIB);
		
		// Reselect the original palette
		SelectPalette(hDC,hPal,FALSE);
		ReleaseDC(NULL,hDC);
		return NULL;
	}
	
	// Get the bitmap bits
	lpbi = (LPBITMAPINFOHEADER)hDIB;
	
	// FINALLY get the DIB
	BOOL bGotBits = GetDIBits( hDC, (HBITMAP)bitmap.GetSafeHandle(),
		0L,				// Start scan line
		(DWORD)bi.biHeight,		// # of scan lines
		(LPBYTE)lpbi 			// address for bitmap bits
		+ (bi.biSize + nColors * sizeof(RGBQUAD)),
		(LPBITMAPINFO)lpbi,		// address of bitmapinfo
		(DWORD)DIB_RGB_COLORS);		// Use RGB for color table
	
	if( !bGotBits )
	{
		TRACE(_T("Error : Faile to get DIBits!\n"));
		GlobalFree(hDIB);
		
		SelectPalette(hDC,hPal,FALSE);
		ReleaseDC(NULL,hDC);
		return NULL;
	}
	
	SelectPalette(hDC,hPal,FALSE);
	ReleaseDC(NULL,hDC);
	return hDIB;
}

// WriteDIB		- Writes a DIB to file
// Returns		- TRUE on success
// szFile		- Name of file to write to
// hDIB			- Handle of the DIB
BOOL WriteDIB(LPCTSTR szFile, HANDLE hDIB)
{
	BITMAPFILEHEADER	hdr;
	LPBITMAPINFOHEADER	lpbi;
	
	if (!hDIB)
		return FALSE;
	
	CFile file;
	if( !file.Open( szFile, CFile::modeWrite|CFile::modeCreate) )
		return FALSE;
	
	lpbi = (LPBITMAPINFOHEADER)hDIB;
	
	int nColors = 1 << lpbi->biBitCount;
	
	// Fill in the fields of the file header 
	hdr.bfType		= ((WORD) ('M' << 8) | 'B');	// is always "BM"
	hdr.bfSize		= GlobalSize (hDIB) + sizeof( hdr );
	hdr.bfReserved1 	= 0;
	hdr.bfReserved2 	= 0;
	hdr.bfOffBits		= (DWORD) (sizeof( hdr ) + lpbi->biSize +
		nColors * sizeof(RGBQUAD));
	
	// Write the file header 
	file.Write( &hdr, sizeof(hdr) );
	
	// Write the DIB header and the bits 
	file.Write(lpbi, GlobalSize(hDIB));
	
	return TRUE;
}

BOOL WriteBitmap(LPCTSTR pszFile, CBitmap& bitmap)
{
	CDC dc;
	dc.Attach(GetDC(NULL));
	
	BITMAP bm;
	bitmap.GetBitmap(&bm);
	CRect rect(0, 0, bm.bmWidth, bm.bmHeight);
	
	CMyMemDC dcMem(&dc, &rect);
	dcMem.SelectObject(&bitmap);
	dcMem.FillSolidRect(rect, RGB(255,255,255));
	BOOL bResult = WriteDCToDIB(pszFile, &dcMem);
	dcMem.DeleteDC();
	return bResult;
}

#ifdef _TIFFSUPPORT

int WriteTIF(LPCTSTR szFile, LPBITMAPINFOHEADER lpbi, LPBYTE pBits)
{	
	int result = 0;
	
	UINT32 w = lpbi->biWidth;
	UINT32 h = lpbi->biHeight;
	UINT32 total_width = ((((int) w * 8) + 31) & ~31) >> 3;

	UINT32 bitcount = 24;
	UINT32 bytecount = bitcount / 8;
	
	if ((w > 0) && (h > 0))
	{	
		double	image_gamma = TIFF_GAMMA;
		
		TIFF * tif;
		
		if ((tif = TIFFOpen(szFile, "w")) == NULL)
		{
			return result;
		}
		
		// setup the image tags
		TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, w);
		TIFFSetField(tif, TIFFTAG_IMAGELENGTH, h);
		TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 8);
		TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_PACKBITS);
		TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
		TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 3);
		TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, 1);
		TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
		TIFFSetField(tif, TIFFTAG_RESOLUTIONUNIT, RESUNIT_NONE);
		TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
		
		unsigned char * psrc = (unsigned char *)(pBits);
		PBYTE ptr1, ptr2;   
		ptr1 = ptr2 = psrc;
		int off = (total_width - w)*3;
		for (int r = 0; r < h; r++)   
		{
			for (int j = 0; j < w; j++)
			{
				*ptr1++ = *(ptr2);
				*ptr1++ = *(ptr2+1);
				*ptr1++ = *(ptr2+2);
				ptr2+=4;
			}

			ptr1+=off;
		}

		unsigned char * pdst = new unsigned char[(w * 3)];
		
		UINT32 src_index;
		UINT32 dst_index;
		
		// now go line by line to write out the image data
		for (int row = 0; row < h; row++ ){
			
			// initialize the scan line to zero
			memset(pdst,0,(size_t)(w * 3));
			
			// moving the data from the dib to a row structure that can
			// be used by the tiff library
			for (int col = 0; col < w; col++){
				src_index = (h - row - 1) * total_width * bytecount + col * bytecount;
				dst_index = col * 3;
				pdst[dst_index++] = psrc[src_index+2];
				pdst[dst_index++] = psrc[src_index+1];
				pdst[dst_index] = psrc[src_index];
				result++;
			}
			
			// now actually write the row data
			TIFFWriteScanline(tif, pdst, row, 0);
		}
		
		TIFFClose(tif);
	}
	
	return result;
}
#endif

BOOL DrawBitmap(CDC* pDC, CBitmap& bitmap, CRect rect)
{
	if (!bitmap.GetSafeHandle())
		return FALSE;
	
	rect.NormalizeRect();
	
	BITMAP bm;
	bitmap.GetBitmap(&bm);		
	
	LPBYTE lpData = new BYTE[bm.bmWidthBytes * bm.bmHeight];
	ZeroMemory(lpData, bm.bmWidthBytes * bm.bmHeight);
	if (bitmap.GetBitmapBits(bm.bmWidthBytes * bm.bmHeight, (LPVOID)lpData) == 0)
		return FALSE;

	BITMAPINFOHEADER bi;
	bi.biSize	= sizeof(BITMAPINFOHEADER);
	bi.biWidth	= bm.bmWidth;
	bi.biHeight = bm.bmHeight;
	bi.biPlanes = 1;
	bi.biBitCount		= 32;
	bi.biCompression	= BI_RGB;
	bi.biSizeImage	= ((((bi.biWidth * bi.biBitCount) + 31) & ~31) / 8) * bi.biHeight;
	bi.biXPelsPerMeter	= 0;
	bi.biYPelsPerMeter	= 0;
	bi.biClrUsed		= 0;
	bi.biClrImportant	= 0;
	
	::StretchDIBits(pDC->GetSafeHdc(), rect.left, rect.top, 
		rect.Width(), rect.Height(), 0, 0, bm.bmWidth, bm.bmHeight, (LPVOID)lpData, 
		(BITMAPINFO*)&bi, DIB_RGB_COLORS, SRCCOPY);

	delete []lpData;

	return TRUE;
}

BOOL DrawTransparent(CDC* pDC, CBitmap& bitmap, CRect rect, COLORREF clrTransparent)
{
/*	if (!bitmap.GetSafeHandle())
		return FALSE;
	
	rect.NormalizeRect();
	
	BITMAP bm;
	bitmap.GetBitmap(&bm);
	
	CDC dcMem;
	CBitmap bmpMem;
	CBitmap* pBmpOriginal;

	dcMem.CreateCompatibleDC (pDC);	// Assume display!
	dcMem.SetMapMode(pDC->GetMapMode());
	bmpMem.CreateCompatibleBitmap (pDC, bm.bmWidth, bm.bmHeight);
	pBmpOriginal = dcMem.SelectObject (&bmpMem);
	
	CMFCToolBarImages::TransparentBlt (pDC->GetSafeHdc (), rect.left, rect.top, 
		rect.Width(), -rect.Height(), 
		&dcMem, 0, 0, 
		clrTransparent,	bm.bmWidth, bm.bmHeight);

	dcMem.SelectObject(pBmpOriginal);
	return TRUE;

	
	CDC ImageDC, MaskDC;
	ImageDC.CreateCompatibleDC(pDC);
	ImageDC.SetMapMode(pDC->GetMapMode());
	CBitmap* pOldImageBMP = ImageDC.SelectObject(&bitmap);

	// 建立单色位图
	CBitmap mask;
	mask.CreateBitmap(bm.bmWidth, bm.bmHeight, 1, 1, NULL);
	MaskDC.CreateCompatibleDC(pDC);
	MaskDC.SetMapMode(pDC->GetMapMode());
	CBitmap* pOldMaskBMP = MaskDC.SelectObject(&mask);

	ImageDC.SetBkColor(clrTransparent); // 设置背景色为要透明的色

	// 拷贝到hMaskDC
	MaskDC.BitBlt(0, 0, bm.bmWidth, bm.bmHeight, &ImageDC, 0, 0, SRCCOPY);

	ImageDC.SetBkColor(RGB( 0, 0, 0));
	ImageDC.SetTextColor(RGB(255,255,255));
	ImageDC.BitBlt(0, 0, bm.bmWidth, bm.bmHeight, &MaskDC, 0, 0, SRCAND);

	COLORREF clrBk = pDC->SetBkColor(RGB(255,255,255));
	COLORREF clrText = pDC->SetTextColor(RGB( 0, 0, 0));

	CBitmap* pMaskBMP = MaskDC.SelectObject(pOldMaskBMP);
	LPBYTE lpData = new BYTE[bm.bmWidthBytes * bm.bmHeight];
	ZeroMemory(lpData, bm.bmWidthBytes * bm.bmHeight);
	if (pMaskBMP->GetBitmapBits(bm.bmWidthBytes * bm.bmHeight, (LPVOID)lpData) == 0)
	{
		delete []lpData;
		return FALSE;
	}

	BITMAPINFOHEADER bi;
	bi.biSize	= sizeof(BITMAPINFOHEADER);
	bi.biWidth	= bm.bmWidth;
	bi.biHeight = bm.bmHeight;
	bi.biPlanes = 1;
	bi.biBitCount		= 32;
	bi.biCompression	= BI_RGB;
	bi.biSizeImage	= ((((bi.biWidth * bi.biBitCount) + 31) & ~31) / 8) * bi.biHeight;
	bi.biXPelsPerMeter	= 0;
	bi.biYPelsPerMeter	= 0;
	bi.biClrUsed		= 0;
	bi.biClrImportant	= 0;

	::StretchDIBits(pDC->GetSafeHdc(), rect.left, rect.top, 
		rect.Width(), rect.Height(), 0, 0, bm.bmWidth, bm.bmHeight, (LPVOID)lpData,
		(BITMAPINFO*)&bi, DIB_RGB_COLORS, SRCAND);

	CBitmap* pImageBMP = ImageDC.SelectObject(pOldImageBMP);
	ZeroMemory(lpData, bm.bmWidthBytes * bm.bmHeight);
	if (pImageBMP->GetBitmapBits(bm.bmWidthBytes * bm.bmHeight, (LPVOID)lpData) == 0)
	{
		delete []lpData;
		return FALSE;
	}

	bi.biBitCount = 32;
	bi.biSizeImage = ((((bi.biWidth * bi.biBitCount) + 31) & ~31) / 8) * bi.biHeight;
	::StretchDIBits(pDC->GetSafeHdc(), rect.left, rect.top, 
		rect.Width(), rect.Height(), 0, 0, bm.bmWidth, bm.bmHeight, (LPVOID)lpData, 
		(BITMAPINFO*)&bi, DIB_RGB_COLORS, SRCPAINT);

	delete []lpData;
*/	return TRUE;
}

WORD WINAPI DibNumColors (VOID FAR *pv)
{
    int                 bits;
    LPBITMAPINFOHEADER  lpbi;
    LPBITMAPCOREHEADER  lpbc;

    lpbi = ((LPBITMAPINFOHEADER)pv);
    lpbc = ((LPBITMAPCOREHEADER)pv);

    /*  With the BITMAPINFO format headers, the size of the palette
     *  is in biClrUsed, whereas in the BITMAPCORE - style headers, it
     *  is dependent on the bits per pixel ( = 2 raised to the power of
     *  bits/pixel).
     */
    if (lpbi->biSize != sizeof(BITMAPCOREHEADER)){
        if (lpbi->biClrUsed != 0)
            return (WORD)lpbi->biClrUsed;
        bits = lpbi->biBitCount;
    }
    else
        bits = lpbc->bcBitCount;

    switch (bits){
        case 1:
                return 2;
        case 4:
                return 16;
        case 8:
                return 256;
        default:
                /* A 24 bitcount DIB has no color table */
                return 0;
    }
}

WORD WINAPI PaletteSize (VOID FAR *pv)
{
    LPBITMAPINFOHEADER lpbi;
    WORD               NumColors;

    lpbi      = (LPBITMAPINFOHEADER)pv;
    NumColors = DibNumColors(lpbi);

    if (lpbi->biSize == sizeof(BITMAPCOREHEADER))
        return NumColors * sizeof(RGBTRIPLE);
    else
        return NumColors * sizeof(RGBQUAD);
}

BOOL WINAPI StretchDibBlt (HDC hdc, int x, int y, int dx, int dy,HANDLE  hdib,int x0, int y0,int dx0, int dy0, LONG rop)

{
    LPBITMAPINFOHEADER lpbi;
    LPSTR        pBuf;
    BOOL         f;

    lpbi = (LPBITMAPINFOHEADER)GlobalLock(hdib);

    if (!lpbi)
        return FALSE;

    pBuf = (LPSTR)lpbi + (WORD)lpbi->biSize + PaletteSize(lpbi);

    f = StretchDIBits ( hdc,
                        x, y,
                        dx, dy,
                        x0, y0,
                        dx0, dy0,
                        pBuf, (LPBITMAPINFO)lpbi,
                        DIB_RGB_COLORS,
                        rop);

    GlobalUnlock(hdib);
    return f;
}

#define WIDTHBYTES(i)   ((i+31)/32*4)

BOOL WINAPI DibInfo (HANDLE hbi,LPBITMAPINFOHEADER lpbi)
{
    if (hbi){
        *lpbi = *(LPBITMAPINFOHEADER)GlobalLock (hbi);

        /* fill in the default fields */
        if (lpbi->biSize != sizeof (BITMAPCOREHEADER)){
            if (lpbi->biSizeImage == 0L)
                lpbi->biSizeImage =
                    WIDTHBYTES(lpbi->biWidth*lpbi->biBitCount) * lpbi->biHeight;

            if (lpbi->biClrUsed == 0L)
                lpbi->biClrUsed = DibNumColors (lpbi);
        }
        GlobalUnlock (hbi);
        return TRUE;
    }
    return FALSE;
}

void WINAPI PrintDIB (CDC* pDC, HANDLE hDIB, CRect& rect, LONG rop)   
{   
    BITMAPINFOHEADER   bi;   
    int   dibX,     dibY;   
    int   dibDX,   dibDY;   

    DibInfo   (hDIB,   &bi);   

    dibX     =   0;   
    dibY     =   0;   
	dibDX   =   (int)bi.biWidth;   
    dibDY   =   (int)bi.biHeight;   

    if (hDIB)
	{   
          /*   Stretch   the   DIB   to   printer   DC   */   
          StretchDibBlt(pDC->GetSafeHdc(),   
                                          rect.left,   
                                          rect.top,   
                                          rect.Width(),   
                                          rect.Height(),   
                                          hDIB,   
                                          dibX,   
                                          dibY,   
                                          dibDX,   
                                          dibDY,   
                                          rop);   
	}   
}

HPALETTE CreateDIBPalette(HANDLE hDIB) //创建DIB调色板
{ 
    HPALETTE            hPal = NULL;    // handle to a palette 
    LPBYTE              lpbi;           // pointer to packed-DIB 

    // if handle to DIB is invalid, return NULL 

    if (!hDIB) 
        return NULL; 

    // lock DIB memory block and get a pointer to it 

    lpbi = (LPBYTE)GlobalLock(hDIB); 

    hPal = CreateDIBPalette(lpbi);

// Unlock hDIB
    GlobalUnlock(hDIB); 

    // return handle to DIB's palette 
    return hPal; 
} 


/************************************************************************* 
* 
* BitmapToDIB() 
* 
* Parameters: 
* 
* HBITMAP hBitmap - specifies the bitmap to convert 
* 
* HPALETTE hPal    - specifies the palette to use with the bitmap 
* 
* Return Value: 
* 
* HDIB             - identifies the device-dependent bitmap 
* 
* Description: 
* 
* This function creates a DIB from a bitmap using the specified palette. 
* 
************************************************************************/ 
HANDLE BitmapToDIB(HBITMAP hBitmap, HPALETTE hPal) 
{ 
    BITMAP              bm;         // bitmap structure 
    BITMAPINFOHEADER    bi;         // bitmap header 
    LPBITMAPINFOHEADER lpbi;       // pointer to BITMAPINFOHEADER 
    DWORD               dwLen;      // size of memory block 
    HANDLE              hDIB, h;    // handle to DIB, temp handle 
    HDC                 hDC;        // handle to DC 
    WORD                biBits;     // bits per pixel 

    // check if bitmap handle is valid 

    if (!hBitmap) 
        return NULL; 

    // fill in BITMAP structure, return NULL if it didn't work 

    if (!GetObject(hBitmap, sizeof(bm), (LPBYTE)&bm)) 
        return NULL; 

    // if no palette is specified, use default palette 

    if (hPal == NULL) 
        hPal = (HPALETTE)GetStockObject(DEFAULT_PALETTE); 

    // calculate bits per pixel 

    biBits = bm.bmPlanes * bm.bmBitsPixel; 

    // make sure bits per pixel is valid 

    if (biBits <= 1) 
        biBits = 1; 
    else if (biBits <= 4) 
        biBits = 4; 
    else if (biBits <= 8) 
        biBits = 8; 
    else // if greater than 8-bit, force to 24-bit 
        biBits = 24; 

    // initialize BITMAPINFOHEADER 

    bi.biSize = sizeof(BITMAPINFOHEADER); 
    bi.biWidth = bm.bmWidth; 
    bi.biHeight = bm.bmHeight; 
    bi.biPlanes = 1; 
    bi.biBitCount = biBits; 
    bi.biCompression = BI_RGB; 
    bi.biSizeImage = 0; 
    bi.biXPelsPerMeter = 0; 
    bi.biYPelsPerMeter = 0; 
    bi.biClrUsed = 0; 
    bi.biClrImportant = 0; 

    // calculate size of memory block required to store BITMAPINFO 

    dwLen = bi.biSize + PaletteSize((LPBYTE)&bi); 

    // get a DC 

    hDC = GetDC(NULL); 

    // select and realize our palette 

    hPal = SelectPalette(hDC, hPal, FALSE); 
    RealizePalette(hDC); 

    // alloc memory block to store our bitmap 

    hDIB = GlobalAlloc(GHND, dwLen); 

    // if we couldn't get memory block 

    if (!hDIB) 
    { 
      // clean up and return NULL 

      SelectPalette(hDC, hPal, TRUE); 
      RealizePalette(hDC); 
      ReleaseDC(NULL, hDC); 
      return NULL; 
    } 

    // lock memory and get pointer to it 

    lpbi = (LPBITMAPINFOHEADER)GlobalLock(hDIB); 

    /// use our bitmap info. to fill BITMAPINFOHEADER 

    *lpbi = bi; 

    // call GetDIBits with a NULL lpBits param, so it will calculate the 
    // biSizeImage field for us     

    GetDIBits(hDC, hBitmap, 0, (UINT)bi.biHeight, NULL, (LPBITMAPINFO)lpbi, 
        DIB_RGB_COLORS); //从DDB中产生DIB

    // get the info. returned by GetDIBits and unlock memory block 

    bi = *lpbi; 
    GlobalUnlock(hDIB); 

    // if the driver did not fill in the biSizeImage field, make one up 
    if (bi.biSizeImage == 0) 
        bi.biSizeImage = WIDTHBYTES((DWORD)bm.bmWidth * biBits) * bm.bmHeight; 

    // realloc the buffer big enough to hold all the bits 

    dwLen = bi.biSize + PaletteSize((LPBYTE)&bi) + bi.biSizeImage; 

    if (h = GlobalReAlloc(hDIB, dwLen, 0)) 
        hDIB = h; 
    else 
    { 
        // clean up and return NULL 

        GlobalFree(hDIB); 
        hDIB = NULL; 
        SelectPalette(hDC, hPal, TRUE); 
        RealizePalette(hDC); 
        ReleaseDC(NULL, hDC); 
        return NULL; 
    } 

    // lock memory block and get pointer to it */ 

    lpbi = (LPBITMAPINFOHEADER)GlobalLock(hDIB); 

    // call GetDIBits with a NON-NULL lpBits param, and actualy get the 
    // bits this time 

    if (GetDIBits(hDC, hBitmap, 0, (UINT)bi.biHeight, (LPBYTE)lpbi + 
            (WORD)lpbi->biSize + PaletteSize((LPBYTE)lpbi), (LPBITMAPINFO)lpbi, 
            DIB_RGB_COLORS) == 0) 
    { 
        // clean up and return NULL 

        GlobalUnlock(hDIB); 
        hDIB = NULL; 
        SelectPalette(hDC, hPal, TRUE); 
        RealizePalette(hDC); 
        ReleaseDC(NULL, hDC); 
        return NULL; 
    } 

    bi = *lpbi; 

    // clean up 
    GlobalUnlock(hDIB); 
    SelectPalette(hDC, hPal, TRUE); 
    RealizePalette(hDC); 
    ReleaseDC(NULL, hDC); 

    // return handle to the DIB 
    return hDIB; 
} 


BOOL PrintBitmap(CDC* pDC, CBitmap& bitmap, CRect& rect, LONG rop)
{
	if (!bitmap.GetSafeHandle())
		return FALSE;
	
	BITMAP bm;
	bitmap.GetBitmap(&bm);		

	HANDLE hDIB = BitmapToDIB((HBITMAP)bitmap, NULL);

	// Convert the bitmap to a DIB

	if (hDIB != NULL)
	{
		PrintDIB(pDC, hDIB, rect, rop);
		GlobalFree( hDIB );	
		return TRUE;
	}

	return FALSE;
}
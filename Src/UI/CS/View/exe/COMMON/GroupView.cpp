// GroupView.cpp : implementation file
//

#include "stdafx.h"
#ifdef _LOGIC
#include "Logic.h"
#else
#include "ul2000.h"

#include "RftDataGroupView.h"
#include "RftProfileGroupView.h"

#include "TableInfoView.h"
#endif

#include "GroupView.h"
#include "MyMEMDC.h"
#include "PrintOrderFile.h"
#include "PrintOrderDlg.h"
#include "Sheet.h"
#include "Curve.h"
#include "ChildFrm.h"
#include "ULFile.h"
#include "MainFrm.h"
#include "SaveAsBmp.h"
#include "ServiceTableItem.h"
#include "ComConfig.h"
#include "GraphHeaderView.h"
#include "LogInformationManager.h"
#include "ParamsView.h"
#include "CalSummaryView.h"
#include "CalSummaryViewEx.h"
#include "ToolInfoview.h"
#include "Outputstableview.h"
#include "CalCoefView.h"
#include "CalUserView.h"
#include "CalChartView.h"
#include "FactorView.h"
#include "Project.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define idCellStart		256
#define idCellLast		511
#define UM_CELL_EDIT	(WM_USER + 2010)

/////////////////////////////////////////////////////////////////////////////
// CGroupView

IMPLEMENT_DYNCREATE(CGroupView, CULView)

CGroupView::CGroupView(LPVOID lpParam)
{
	m_pPofInfo = (CPofInfo*)lpParam;
	m_pPOFile = m_pPofInfo->pPof;
	m_bRefresh= TRUE;
	m_hItem = NULL;
	m_nCellID = idCellStart;
	m_pRftDataGroupView = NULL;
	m_pRftProfileGroupView = NULL;
}

CGroupView::~CGroupView()
{
	while (m_listList.GetCount())
		delete m_listList.RemoveHead();

	ClearCells();

#ifndef _LOGIC
	if (m_pRftDataGroupView != NULL)
		delete m_pRftDataGroupView;
	
	if (m_pRftProfileGroupView != NULL)
		delete m_pRftProfileGroupView;
#endif
}


BEGIN_MESSAGE_MAP(CGroupView, CULView)
	//{{AFX_MSG_MAP(CGroupView)
	ON_WM_VSCROLL()
	ON_WM_LBUTTONDBLCLK()
	//}}AFX_MSG_MAP
	ON_MESSAGE(UM_CELL_EDIT, OnCellEdit)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGroupView drawing

void CGroupView::OnDraw(CDC* pDC)
{
	CRect rect;
	GetClientRect(rect);
	pDC->DPtoLP(&rect);

	if (m_bRefresh)
	{
		PrepareDraw(pDC, rect);
		SetScrollInformation();
		m_bRefresh = FALSE;
	}

	CMyMemDC memDC(pDC, &rect);
	DrawPage(&memDC, rect);
	
	if (m_pPOFile == NULL)
		return ;
	
	rect.left = m_rcPage.left;
	rect.right= m_rcPage.right;
	Draw(&memDC, rect);
}

void CGroupView::OnUpdate()
{
	m_pPOFile = m_pPofInfo->pPof;
	m_bRefresh = TRUE;
	Invalidate();
}

void CGroupView::OnUpdateCell()
{
	for (POSITION pos = m_cellList.GetHeadPosition(); pos != NULL;)
	{
		CCell2000* pCell = (CCell2000*)m_cellList.GetNext(pos);
		if (pCell->GetSafeHwnd())
		{
			if (pCell->GetDlgCtrlID() == AFX_IDC_CELL)
				continue ;
			
			long  col, row;
			CString  strCellNote;
			col = pCell->GetCols(0);
			row = pCell->GetRows(0);
			for (int i = 1; i < col; i++)
			{
				for (int j = 1; j < row; j++)
				{
					strCellNote = pCell->GetCellNote(i, j, 0);
					if (strCellNote.GetLength())
					{
						strCellNote = strCellNote.Left(strCellNote.Find("@"));
						pCell->MoveToCell(i,j);
						long col1,col2,row1,row2;
						pCell->GetSelectRange(&col1,&row1,&col2,&row2);
						pCell->ClearArea(i, j , i ,j, 0, 1);
						pCell->SetCellString(i, j, 0, strCellNote);
						pCell->MergeCells(col1,row1,col2,row2);					
					}
				}
			}
			
			InvalidateCell(pCell);	
		}
	}
}

int CGroupView::CalcPageHeight()
{
	int nSize = m_pagePos.GetSize();
	if (nSize > 0)
		return (int)abs(m_pagePos.GetAt(nSize - 1));
	
	return 2*PAGE_MARGIN;
}

void CGroupView::GetViewRange(LPRECT lpRect, int& nStart, int& nEnd)
{
	nStart = 0;
	nEnd = m_pagePos.GetSize() - 1;
	if (nEnd < 0)
		return ;

	int i;
	for (i = 1; i<nEnd; i++)
	{
		if (lpRect->top < m_pagePos.GetAt(i))
			nStart++;
		else
			break;
	}

	for (i = nEnd; i > nStart; i--)
	{
		if (lpRect->bottom > m_pagePos.GetAt(i-1))
			nEnd--;
		else
			break;
	}
}

void* CGroupView::GetParam(int iIndex, int& nOff)
{
	nOff = 0;
	int i;
	for (i = m_paramPos.GetSize() - 1; i > -1; i--)
	{
		if (m_paramPos.GetAt(i) <= iIndex)
		{
			nOff = iIndex - m_paramPos.GetAt(i);
			break;
		}
	}

	if (i < 0)
		return NULL;

	
	if (i < m_pPOFile->m_ParamArray.GetSize())
		return m_pPOFile->m_ParamArray.GetAt(i);

	return NULL;
}

void CGroupView::Draw(CDC* pDC, LPRECT lpRect)
{
	CRect rect = lpRect;
	
	int nStart,	nEnd;
	GetViewRange(rect, nStart, nEnd);

	CULDoc* pDoc = NULL;
	CULView* pULView = NULL;
	for (int i = nStart; i<nEnd; i++)
	{
		int nOff = 0;
		CPrintParam* pParam = (CPrintParam*)GetParam(i, nOff);
		if (pParam == NULL)
			continue ;
		
		int nFx = pParam->m_nFx;
		if (nFx > 0)		// Is a file
		{
			pDoc = CULDoc::GetDoc(--nFx);
			if (pDoc == NULL)
				continue ;

			pULView = pDoc->GetPPView(pParam);
		}
		else if (nFx == 0)	// Is a project
		{
			continue ;
		}
		else if (nFx == -1)	// Is a general template
		{
			pDoc = m_pDoc;
			if (pDoc != NULL)
			{
				pULView = pDoc->GetPPView(pParam);
			}
			else
				continue ;
		}
		else if (nFx == -2)	// Is a circle
		{
			pDoc = CULDoc::GetDoc(nOff);
			if (pDoc == NULL)
				continue ;

			pULView = pDoc->GetPPView(pParam);
		}

		CRect rcView = rect;
		rcView.top = m_pagePos.GetAt(i);
		rcView.bottom = m_pagePos.GetAt(i+1);	
		rcView.DeflateRect(PAGE_MARGIN, 0, PAGE_MARGIN, 0);
		if (rcView.bottom != rcView.top)
		{
			switch (pParam->m_dwType)
			{
			case ULV_CUSTOMIZEVIEW:
			case ULV_TOOLS:	
			case ULV_CONSTRUCT:
			case ULV_GRAPHCURVE:
			case ULV_PARAM:
			case ULV_CHARTS:
			case ULV_CALUSER:
			case ULV_RUNDATA:	//run data
#ifdef _NO_CALCELL
			case ULV_CALCHART:
#endif
				{
					if (pULView)
						pULView->Draw(pDC, rcView);
				}			
				break;
			case ULV_GRAPH:
				{
					CGraphWnd* pGraph = (CGraphWnd*)pULView;
					if (pGraph == NULL)
						break;
					
					//lxc
					CRect rcInfo = rcView;
					CRect rcGH = rcView;
					if (Gbl_ConfigInfo.m_PrintInfo.bPrintLogInfoHead)
					{
						//pGraph->DrawLogInfo(pDC, rcInfo, pDoc);
						rcView.top -= CLogInformationManager::m_lLogInfo_Height;	// DEFAULT_LOGINFO_HEIGHT;;
					}
					
					if (Gbl_ConfigInfo.m_PrintInfo.bPrintLogInfoTail)
					{
						rcView.bottom += CLogInformationManager::m_lLogInfo_Height;	// DEFAULT_LOGINFO_HEIGHT;;
					}
					
					long lTop, lBottom;
					if (pGraph->CalcPageHeight(lTop, lBottom))
					{
						pGraph->m_pSheet->FitPage(CalcPageWidth());//XWH调整组合图打印大小
						pGraph->m_pSheet->DrawGraph(pDC, rcView, rect,
							lTop, lBottom, Gbl_ConfigInfo.m_PrintInfo.bPrintTrackHeadTwice);
					}
						
					if (Gbl_ConfigInfo.m_PrintInfo.bPrintLogInfoHead)
					{
						pGraph->DrawLogInfo(pDC, rcGH, pDoc);
					}

					if (Gbl_ConfigInfo.m_PrintInfo.bPrintLogInfoTail)
					{
						rcInfo.top = rcView.bottom /*+ DEFAULT_LOGINFO_HEIGHT*/;
						pGraph->DrawLogInfo(pDC, rcInfo, pDoc);
					}
					else
					{
						CRect rcErase = rcView;
						rcErase.top = rcView.bottom;
						rcErase.bottom = max(lpRect->bottom, m_rcPage.bottom + 4);
						if (rcErase.bottom < rcErase.top)
						{
							rcErase.DeflateRect(4, 0, 4, 0);
							pDC->FillSolidRect(rcErase, RGB(255,255,255));
						}
						
						CPen pen(PS_SOLID, PEN_BORDER_SIZE, RGB(0, 0, 0));
						CPen* pPen0 = pDC->SelectObject(&pen);
						pDC->MoveTo(rcInfo.left, rcErase.top);
						pDC->LineTo(rcInfo.right, rcErase.top);
						pDC->SelectObject(pPen0);
					}
				}
				break;
			case ULV_GRAPHHEAD:
				{
					if (pULView->GetSafeHwnd() == NULL)
						break;
					
					CCell2000* pCell = GetCell(i, nOff);
					if (pCell == NULL)
						break;
					
					CGraphHeaderView* pFView = (CGraphHeaderView*)pULView;
					POSITION pos = pFView->m_cellGroup.Find(pCell);
					if (pos == NULL)
						break;
					
					void* md = NULL;
					pFView->m_cellMd.Lookup((void*)pos, (void*&)md);
					if (((DWORD_PTR)md) >= pFView->m_dwMdCount)
						break;
					
					VARIANT data;
					if (pULView->m_pEdit->SaveToBuffer(&data) < 1)
						break;
					
					pCell->ReadFromBuffer(data);
					pFView->m_cellMd.SetAt((void*)pos, (void*)pFView->m_dwMdCount);
				}
				break;
			case ULV_CALSUMMARY:
				{
				}
				break;

			case ULV_CALCOEF:
				{
					if (rcView.Height() != 0)
					{
						CCalCoefView CCView;
						CCView.Draw(pDC, rcView);
					}
				}
				break;
// 2020.3.9 Ver1.6.0 TASK【002】 Start
//#ifndef _LOGIC
//			case ULV_RFT_DATAGROUP:
//				{
//					if(rcView.Height () != 0)
//					{
//						m_pRftDataGroupView->Draw (pDC , rcView);
//					}
//				}
//				break;
//			case ULV_RFT_PROFILEGROUP:
//				{
//					if(rcView.Height () != 0)
//					{
//						m_pRftProfileGroupView->Draw (pDC , rcView);
//					}
//				}
//				break;
//#endif
// 2020.3.9 Ver1.6.0 TASK【002】 End  
			default:
				break;
		}
		}
		
		int nNext = i + 1;
		if (nNext < m_pagePos.GetSize())
		{
			rect.top = m_pagePos.GetAt(nNext);
			if (rect.bottom < rect.top)
				continue ;
		}

		break;
	}
}

void CGroupView::Print()
{
	if (NULL == m_pPOFile)
		return;

	int nPrintedPage = 0; //已打印的页数
	CDC* pDC = &m_pPrintInfo->m_printDC;
	pDC->StartPage();
	pDC->SaveDC();
	pDC->SetMapMode(MM_LOMETRIC);
	pDC->SetWindowOrg(0, 0);
	nPrintedPage++;

	CString strDocName;
	CString strFileName;
	int nDocIndex = 0;

	strDocName.LoadString(IDS_LOGDATAGRAPH);
	m_pdfPrintInfo.RemoveAll();
	CRect rcPage = m_pPrintInfo->SetPageInfo();
	rcPage.top = -rcPage.top;
	rcPage.bottom = -rcPage.bottom;
	rcPage.DeflateRect(PAGE_MARGIN, 0, PAGE_MARGIN, 0);
	const int nPageHeight = rcPage.Height();  // 纸的高度, 负值
	int nPrintingPos = -PAGE_MARGIN; // 打印位置, 负值, 起始位置空一个页面边距

	CULDoc* pDoc = NULL;

	for (int i = 0; i < m_pPOFile->m_ParamArray.GetSize(); i++)
	{
		CPrintParam* pParam = (CPrintParam*) m_pPOFile->m_ParamArray.GetAt(i);

		CULView* pULView = NULL;
		CProject* pProject = NULL;
		int nFx = pParam->m_nFx;
		POSITION pos = CULDoc::m_DocList.GetHeadPosition();
		if (nFx > 0)	// It is a file
		{
			pDoc = CULDoc::GetDoc(--nFx);
			if (pDoc == NULL)
				continue ;

			pULView = (CULView *) pDoc->GetPPView(pParam);
			pProject = pDoc->m_pProject;
		}
		else if (nFx == 0)	// It is a project
		{
			continue ;
		}
		else if (nFx == -1)	// It is self
		{
			 	pDoc = m_pDoc;
			if (pDoc != NULL)
			{
				pULView = (CULView *) pDoc->GetFirstView(pParam->m_dwType,
												pParam->m_strParam);
				pProject = pDoc->m_pProject;
			}
			else
			{
				continue ;
			}
		}			

		for (; pos != NULL;)
		{
			if (pParam->m_nFx == -2) // Is a circle
			{
				pDoc = CULDoc::m_DocList.GetNext(pos);
				if (pDoc == NULL)
					continue ;

				pULView = (CULView *) pDoc->GetPPView(pParam);
				pProject = pDoc->m_pProject;
			}
			else
				pos = NULL; // Just do once
// 2020.3.9 Ver1.6.0 TASK【002】 Start
//#ifndef _LOGIC
//			if (pParam->m_dwType == ULV_RFT_DATAGROUP && pULView == NULL)
//			{
//				pULView = m_pRftDataGroupView;
//			}
//			else if (pParam->m_dwType == ULV_RFT_PROFILEGROUP && pULView == NULL)
//			{
//				pULView = m_pRftProfileGroupView;
//			}
//#endif
// 2020.3.9 Ver1.6.0 TASK【002】 End
			if (pULView == NULL )
				continue;
			//计算当前打印图件的高度，正值
			int nObjectHeight = 0; 
			if (pParam->m_dwType == ULV_GRAPHHEAD)
			{
				CGraphHeaderView* pView = (CGraphHeaderView*)pULView;
				CCell2000* pCell = (CCell2000*)pView->m_cellGroup.GetTail();
				nObjectHeight = 0;
				for (int i = 0; i < pCell->GetRows(0); i++)
					nObjectHeight += pCell->GetRowHeight(0, i, 0);
			}
			else if (pULView != NULL)
				nObjectHeight = pULView->CalcPrintHeight();

			if (pParam->m_dwType == ULV_GRAPH)
			{
				CGraphWnd* pGraphWnd = (CGraphWnd*) pULView;
				if (pGraphWnd != NULL)
					pGraphWnd->PreparePrint();
			}
			int nCurPageStart = nPrintingPos - (nPageHeight * (nPrintedPage - 1));
			int nPageCount = (nCurPageStart - nObjectHeight + 1) / 
				                    nPageHeight + 1; // 当前图件需要打印的页数
			CRect rcObj = rcPage;
			rcObj.top = nPrintingPos;
			rcObj.bottom = nPrintingPos - nObjectHeight;

			for (int n = 0; n < nPageCount; n++)
			{
				if (n != 0) // 开始打印与n != 0时是新的一页
				{
					pDC->StartPage();
					pDC->SaveDC();
					pDC->SetMapMode(MM_LOMETRIC);
					pDC->SetWindowOrg(0, nPrintedPage * nPageHeight);
					nPrintedPage++;
				}
				switch (pParam->m_dwType)
				{
				case ULV_HOLLOW:
					{
						PrintBlank(pParam->m_lParam);
						int nHeight = pParam->m_lParam;
						strFileName = "Blank";
						AddPdfPrintInfo(nHeight, strFileName);
					}
					break;
				case ULV_LOGINFO:
					{
						PrintLogInfo(pDoc, NULL, pParam->m_bParam);	
					}
					break;
				case ULV_GRAPH:
					{
						CGraphWnd* pGraphWnd = (CGraphWnd*) pULView;
						if (pGraphWnd == NULL)
							break;

						int nHeight = pGraphWnd->CalcPageHeight();
						if (nHeight == 0)
							break;

						m_pPrintInfo->m_dwMode = PM_FILE;
						pGraphWnd->PreparePage(n, -nCurPageStart, abs(nPageHeight));
						pGraphWnd->Print(pDC, rcObj);
					}
					break;
				case ULV_CALREPORT:
					{
						if (pProject != NULL)
							PrintCalReport(pProject);
						nDocIndex++;
					}
					break;
				case ULV_RUNDATA:	//?
				case ULV_CALCHART:
				case ULV_CALUSER:
				case ULV_CALSUMMARY:
				case ULV_TOOLINFOVIEW:
				case ULV_OUTPUTSTABLE:
				case ULV_GRAPHHEAD:
				case ULV_PARAM:
				case ULV_CALCOEF:
				case ULV_SCHEDULE:
				case ULV_TOOLS:
				case ULV_FACTOR:
				case ULV_TABLEINFOVIEW:
				case ULV_CONSTRUCT:	
// 2020.3.9 Ver1.6.0 TASK【002】 Start
//				case ULV_RFT_DATAGROUP:	
//				case ULV_RFT_PROFILEGROUP:
// 2020.3.9 Ver1.6.0 TASK【002】 End
				default:
					{
						if (pULView != NULL)
						{
							pULView->Print(pDC, rcObj);
						}
					}
					break;
				}
				if (n != nPageCount - 1) //末尾一页不endpage,其余页endpage
				{
					pDC->RestoreDC(-1);
					pDC->EndPage();
				}
			}  // 当前图件按页数循环完毕
			nPrintingPos -= nObjectHeight;
		}
	}
}

//统一要求各视图上面有PAGE_MAGIN,下面没有PAGE_MAGIN,不管中间过程，只要最后结果
//两种方法，一种是在计算高度时，另一种是在绘画过程中
void CGroupView::PrepareDraw(CDC* pDC, LPRECT lpRect)
{
	// Clear all lists

	while (m_listList.GetCount())
		delete m_listList.RemoveHead();
	m_listPos.RemoveAll();
	
	// Clear all cells
	ClearCells();

	// Init rect, pos, file and service
	
	CRect rect;
	rect.left = PAGE_MARGIN;
	rect.top = -PAGE_MARGIN;
//	int nWidth = DEFAULT_PAGE_WIDTH;
//	if (m_pPrintInfo && m_pPrintInfo->GetPrintDC())
//		nWidth = m_pPrintInfo->GetPrinterPageSize().cx;
	int nWidth = CalcPageWidth();
	rect.right = rect.left + nWidth;
	rect.DeflateRect(PAGE_MARGIN, 0, PAGE_MARGIN, 0);

	m_pagePos.RemoveAll();
	m_paramPos.RemoveAll();
	m_ctrlsofp.clear();
	
	int nPos = -2*PAGE_MARGIN;
	m_pagePos.Add(nPos);

	CULDoc* pDoc = NULL;
	if (m_pPOFile == NULL)
		return ;

	// ---------------------------------
	//	Accord each print param to calc rect and pos:
	// --------------------------------- 

	for (int i = 0; i<m_pPOFile->m_ParamArray.GetSize(); i++)
	{
		CPrintParam* pParam = (CPrintParam*)m_pPOFile->m_ParamArray.GetAt(i);

		CRect rcView = rect;
		int nHeight = 0;
		
		while(i >= m_pagePos.GetSize())	// To match size with the param array
			m_pagePos.Add(nPos);		// add the last pos to make zero height			

		int n = m_pagePos.GetSize() - 1;
		m_paramPos.Add(n);
		m_ctrlsofp.push_back(0);
		rcView.top = m_pagePos.GetAt(n);
		
		int nFx = pParam->m_nFx;
		
		CULView* pULView = NULL;
		CProject* pProject = NULL;
		POSITION pos = CULDoc::m_DocList.GetHeadPosition();
		if (nFx > 0)	// It is a file
		{
			pDoc = CULDoc::GetDoc(--nFx);
			if (pDoc == NULL)
				continue ;	

			pULView = pDoc->GetPPView(pParam, TRUE);
			pProject = pDoc->m_pProject;
		}
		else if (nFx == 0)	// It is a project
		{
			continue ;
		}
		else if (nFx == -1)	// It is self
		{
			pDoc = m_pDoc;
			if (pDoc != NULL)
			{
				pULView = pDoc->GetPPView(pParam, TRUE);
				pProject = pDoc->m_pProject;
			}
			else
			{
				continue ;
			}			
		}			
		
		for ( ; pos != NULL; )
		{
			if (pParam->m_nFx == -2) // Is a circle
			{
				pDoc = CULDoc::m_DocList.GetNext(pos);
				if (pDoc == NULL)
					continue ;
				
				pULView = pDoc->GetPPView(pParam, TRUE);
				pProject = pDoc->m_pProject;
			}
			else
				pos = NULL; // Just do once

			// 当Cell程序被卸载或被反注册时，所有依赖Cell的视图将不能正确加载
			// 在这个时候，pULView指针为空。所以各处使用时，应该加以判断。
			switch (pParam->m_dwType)
			{
			case ULV_HOLLOW:
				nHeight = pParam->m_lParam;
				break;
			case ULV_GRAPHHEAD:
				{
					CGraphHeaderView* pHeader = (CGraphHeaderView*)pULView;
					if (pHeader == NULL)
						break;
					
					CCell2000* pCell = pHeader->AddCell(this, m_nCellID++);
					m_cellList.AddTail(pCell);
					if (m_ctrlsofp.back()++ == 0)	// Set the first pos only
						m_cellPos[i] = m_cellList.GetTailPosition();
					SetCellFitSize(pCell);
					CRect rcCell = rcView;
					nHeight = pHeader->GetCellSize(pCell).cy;
					rcCell.bottom = rcCell.top - nHeight;
					pDC->LPtoDP(rcCell);
					pCell->MoveWindow(rcCell);
					InvalidateCell(pCell, TRUE, pProject);
					pCell->SetLeftCol(0);
					pCell->SetTopRow(0);
				}
				break;
			case ULV_TOOLS:	
			case ULV_CONSTRUCT:
			case ULV_CUSTOMIZEVIEW:
			case ULV_CHARTS:
				if (pULView != NULL)
				{
					nHeight = pULView->CalcPageHeight();
					nHeight -= PAGE_MARGIN;
				}
				break;
			case ULV_GRAPH:
				if (pULView != NULL)
				{
					nHeight = pULView->CalcPageHeight();
				}
//				nHeight += PAGE_MARGIN;
				break;

			case ULV_PARAM:
				{
					CParamsView* pParamsView = (CParamsView*)pULView;
					if (pParamsView)
					{
						CBCGPListCtrl* pList = new CBCGPListCtrl;
						CBCGPListCtrl* pExList = new CBCGPListCtrl;
						pParamsView->CreateList(pList, pExList, this);
						if (pParamsView->m_pTPTree != NULL)
							pParamsView->InitList(pList, pExList);
						
						m_listList.AddTail(pList);
						if (m_ctrlsofp.back()++ == 0)
							m_listPos[i] = m_listList.GetTailPosition();
						m_listList.AddTail(pExList);
						CRect rcList = rcView;
						nHeight = pParamsView->CalcPageHeight();
						rcList.bottom = rcList.top - nHeight;
						rcList.top -= DEFAULT_TITLE_HEIGHT;
	                    int nShowCount = pParamsView->GetShowCount ();
						rcList.bottom=rcList.top- (nShowCount+2) *pParamsView->m_nItemHeight+12;
	                    CRect rcExList;
               					
  						if (pParamsView->m_exToCreate && Gbl_ConfigInfo.m_Plot .ModifyParam)
						{
							rcExList= rcList;
							rcExList.top=rcList.bottom-PAGE_MARGIN/*-DEFAULT_TITLE_HEIGHT*/;
							int nShowModifyCount = pParamsView->GetShowModifyCount ();
							rcExList.bottom=rcExList.top-(nShowModifyCount+2)*pParamsView->m_nItemHeight+12;
	                    	//CRect rcLink=rcExList;
	                        //rcLink.bottom=rcExList.top;
	                        //rcLink.top=rcList.bottom;
	                        //pDC->Rectangle(rcLink);						
							pDC->LPtoDP(rcExList);
		                    //pExList->MoveWindow(rcExList);
						   
						}
                        
						pDC->LPtoDP(rcList);
						//pList->MoveWindow(rcList);
					}
				}
				break;

			case ULV_CALSUMMARY:
				{
					if ((pProject == NULL) || (pULView == NULL))
						break;
					if (Gbl_ConfigInfo.m_Other.nCalSummaryType==0)
					{
						CCalSummaryView * pCDView = (CCalSummaryView *)pULView;
						pCDView->m_ArrTools.Copy(pProject->m_ULKernel.m_arrTools) ;
						CCell2000* pCell = new CCell2000;
						CreateCell(pCell, this, FALSE);
						pCDView->OpenCellFile(pCell);
						m_cellList.AddTail(pCell);
						if (m_ctrlsofp.back()++ == 0)	// Set the first pos only
							m_cellPos[i] = m_cellList.GetTailPosition();
						
						SetCellFitSize(pCell);
						CRect rcCell = rcView;
						nHeight = pCDView->GetCellSize(pCell).cy;
						rcCell.bottom = rcCell.top - nHeight;
						pDC->LPtoDP(rcCell);
						pCell->MoveWindow(rcCell);
						InvalidateCell(pCell, TRUE);
						pCell->SetLeftCol(0);
						pCell->SetTopRow(0);
					}
					else if (Gbl_ConfigInfo.m_Other.nCalSummaryType==1)
					{
						CCalSummaryViewEx * pCDView = (CCalSummaryViewEx *)pULView;
						pCDView->m_ArrTools.Copy(pProject->m_ULKernel.m_arrTools) ;
						CCell2000* pCell = new CCell2000;
						CreateCell(pCell, this, FALSE);
						pCDView->OpenCellFile(pCell);
						m_cellList.AddTail(pCell);
						if (m_ctrlsofp.back()++ == 0)	// Set the first pos only
							m_cellPos[i] = m_cellList.GetTailPosition();
						
						SetCellFitSize(pCell);
						CRect rcCell = rcView;
						nHeight = pCDView->GetCellSize(pCell).cy;
						rcCell.bottom = rcCell.top - nHeight;
						pDC->LPtoDP(rcCell);
						pCell->MoveWindow(rcCell);
						InvalidateCell(pCell, TRUE);
						pCell->SetLeftCol(0);
						pCell->SetTopRow(0);
					}
				}
				break;

			case ULV_TOOLINFOVIEW:
				{					
					if ((pProject == NULL) || (pULView == NULL))
						break;
					
					CToolInfoView * pCDView = (CToolInfoView *)pULView;
					pCDView->m_pArrTools = &pProject->m_ULKernel.m_arrTools;
					
					CCell2000* pCell = new CCell2000;
					CreateCell(pCell, this, FALSE);
					pCDView->OpenCellFile(pCell);
					m_cellList.AddTail(pCell);
					if (m_ctrlsofp.back()++ == 0)	// Set the first pos only
						m_cellPos[i] = m_cellList.GetTailPosition();
					
					SetCellFitSize(pCell);
					CRect rcCell = rcView;
					nHeight = pCDView->GetCellSize(pCell).cy;
					rcCell.bottom = rcCell.top - nHeight;
					pDC->LPtoDP(rcCell);
					pCell->MoveWindow(rcCell);
					InvalidateCell(pCell, TRUE);
					pCell->SetLeftCol(0);
					pCell->SetTopRow(0);
				}
				break;

			case ULV_OUTPUTSTABLE:
				{
					if ((pProject == NULL) || (pULView == NULL))
						break;
					
					COutputstableView * pCDView = (COutputstableView *)pULView;
					pCDView->m_pvecCurve = &m_pDoc->m_vecCurve;//&pProject->m_m_ULKernel.m_vecm_arrTools;
					
					CCell2000* pCell = new CCell2000;
					CreateCell(pCell, this, FALSE);
					pCDView->OpenCellFile(pCell);
					m_cellList.AddTail(pCell);
					if (m_ctrlsofp.back()++ == 0)	// Set the first pos only
						m_cellPos[i] = m_cellList.GetTailPosition();
					
					SetCellFitSize(pCell);
					CRect rcCell = rcView;
					nHeight = pCDView->GetCellSize(pCell).cy;
					rcCell.bottom = rcCell.top - nHeight;
					pDC->LPtoDP(rcCell);
					pCell->MoveWindow(rcCell);
					InvalidateCell(pCell, TRUE);
					pCell->SetLeftCol(0);
					pCell->SetTopRow(0);
				}
				break;

			case ULV_TABLEINFOVIEW:
				{
					if ((pProject == NULL) || (pULView == NULL))
						break;
					
					CTableInfoView * pCDView = (CTableInfoView *)pULView;
					pCDView->m_pArrTools = &pProject->m_ULKernel.m_arrTools;
					
					CCell2000* pCell = new CCell2000;
					CreateCell(pCell, this, FALSE);
					pCDView->OpenCellFile(pCell);
					m_cellList.AddTail(pCell);
					if (m_ctrlsofp.back()++ == 0)	// Set the first pos only
						m_cellPos[i] = m_cellList.GetTailPosition();
					
					SetCellFitSize(pCell);
					CRect rcCell = rcView;
					nHeight = pCDView->GetCellSize(pCell).cy;
					rcCell.bottom = rcCell.top - nHeight;
					pDC->LPtoDP(rcCell);
					pCell->MoveWindow(rcCell);
					InvalidateCell(pCell, TRUE);
					pCell->SetLeftCol(0);
					pCell->SetTopRow(0);
					
				}
				break;

			case ULV_CALCOEF:
				{
					if ((pProject == NULL) || (pULView == NULL))
						break;
					
					CCalCoefView * pCCView = (CCalCoefView *)pULView;
					if (pCCView == NULL)
						break;
					
					pCCView->m_pArrTools = &pProject->m_ULKernel.m_arrTools;
					
					CBCGPListCtrl* pList = new CBCGPListCtrl;
					pCCView->CreateList(pList, this);
					pCCView->InitList(pList);
					m_listList.AddTail(pList);
					if (m_ctrlsofp.back()++ == 0)
						m_listPos[i] = m_listList.GetTailPosition();
					CRect rcList = rcView;
					nHeight = pCCView->CalcPageHeight();
					rcList.bottom = rcList.top - nHeight;
					rcList.top -= 300;
					pDC->LPtoDP(rcList);
					pList->MoveWindow(rcList);
				}
				break;

			case ULV_CALUSER:
				{
					CCalUserView* pReport = (CCalUserView*)pULView;
					if(pReport!=NULL)
						nHeight = pReport->CalcPageHeight() - PAGE_MARGIN;
				}
				break;

			case ULV_CALCHART:
				{
					CCalChartView* pReport = (CCalChartView*)pULView;
#ifdef _NO_CALCELL
					if (pReport != NULL)
					{
						nHeight = pReport->CalcPageHeight();
						if (nHeight == 2*PAGE_MARGIN)
						{
							pReport->InitReport();
							nHeight = pReport->CalcPageHeight();
						}
						nHeight -= PAGE_MARGIN;
					}
#else
					
					if (pReport)
					{
						CCell2000* pCell = new CCell2000;
						CreateCell(pCell, this, FALSE);
						m_cellList.AddTail(pCell);
						if (m_ctrlsofp.back()++ == 0)	// Set the first pos only
							m_cellPos[i] = m_cellList.GetTailPosition();
						
						pReport->InitReport(pCell);
						InvalidateCell(pCell, TRUE, pProject);
						SetCellFitSize(pCell);
						CRect rcCell = rcView;
						nHeight = GetCellSize(pCell).cy;
						rcCell.bottom = rcCell.top - nHeight;
						pDC->LPtoDP(rcCell);
						rcCell.left++;
						pCell->MoveWindow(rcCell);
						
						pCell->SetLeftCol(0);
						pCell->SetTopRow(0);
					}
#endif
				}
				break;

			case ULV_FACTOR:
				{
					if (pProject == NULL || pULView == NULL)
						break;
					
					CFactorView * pCDView = (CFactorView *)pULView;
					pCDView->m_pToolArray = &pProject->m_ULKernel.m_arrTools;

					CCell2000* pCell = new CCell2000;
					CreateCell(pCell, this, FALSE);
					pCDView->OpenCellFile(pCell);
					m_cellList.AddTail(pCell);
					if (m_ctrlsofp.back()++ == 0)	// Set the first pos only
						m_cellPos[i] = m_cellList.GetTailPosition();
					
					SetCellFitSize(pCell);
					CRect rcCell = rcView;
					nHeight = pCDView->GetCellSize(pCell).cy;
					rcCell.bottom = rcCell.top - nHeight;
					pDC->LPtoDP(rcCell);
					pCell->MoveWindow(rcCell);
					InvalidateCell(pCell, TRUE);
					pCell->SetLeftCol(0);
					pCell->SetTopRow(0);
				}
				break;
// 2020.3.9 Ver1.6.0 TASK【002】 Start
//#ifndef _LOGIC
//			case ULV_RFT_DATAGROUP:
//				{
//					m_pRftDataGroupView = new CRftDataGroupView ;
//					nHeight = m_pRftDataGroupView->CalcPageHeight ();
//				}
//				break;
//				
//			case ULV_RFT_PROFILEGROUP:
//				{
//					m_pRftProfileGroupView = new CRftProfileGroupView ;
//					m_pRftProfileGroupView->Prepare(this , pDC , rcView);
//					nHeight = m_pRftProfileGroupView->CalcPageHeight ();
//				}
//				break;
//#endif
// 2020.3.9 Ver1.6.0 TASK【002】 End
			default:
				{
					if (pULView != NULL)
						nHeight = pULView->CalcPageHeight();
				}
				break;
			}
			
			nPos -= nHeight;
			m_pagePos.Add(nPos);
		}
	}
}

CCell2000* CGroupView::GetCell(int iIndex, int nOff /* = 0 */)
{
	POSITION pos = NULL;
	if (m_cellPos.Lookup(iIndex, (void*&)pos))
	{
		for(int i = 0; i < nOff; i++)
		{
			if (pos == NULL)
				return NULL;

			m_cellList.GetNext(pos);
		}

		if (pos != NULL)
			return m_cellList.GetAt(pos);
	}
	return NULL;
}

CBCGPListCtrl* CGroupView::GetList(int iIndex, int nOff /* = 0 */)
{
	POSITION pos = NULL;
	if (m_listPos.Lookup(iIndex, (void*&)pos))
	{
		for(int i = 0; i < nOff; i++)
		{
			if (pos == NULL)
				return NULL;
			
			m_listList.GetNext(pos);
		}

		if (pos != NULL)
			return (CBCGPListCtrl*)(m_listList.GetAt(pos));
	}
	return NULL;
}

/////////////////////////////////////////////////////////////////////////////
// CGroupView diagnostics

#ifdef _DEBUG
void CGroupView::AssertValid() const
{
	CULView::AssertValid();
}

void CGroupView::Dump(CDumpContext& dc) const
{
	CULView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CGroupView message handlers

void CGroupView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	CPrintOrderDlg dlg(POF_MDF);
	dlg.m_pPofInfo = m_pPofInfo;

	if (dlg.DoModal() == IDOK)
	{
		if (m_pDoc != NULL)
			m_pMainWnd->m_wndWorkspace.m_wndTree.RefreshOrder(m_hItem, m_pDoc, m_pPofInfo);

		OnUpdate();
	}

	CULView::OnLButtonDblClk(nFlags, point);
}

int CGroupView::CalcPrintHeight()
{
	int nTotalHeight = 2*PAGE_MARGIN;
	CULDoc* pDoc = NULL;

	for (int i = 0; i<m_pPOFile->m_ParamArray.GetSize(); i++)
	{
		CPrintParam* pParam = (CPrintParam*)m_pPOFile->m_ParamArray.GetAt(i);

		int nFx = pParam->m_nFx;
		
		CULView* pULView = NULL;
		CProject* pProject = NULL;
		POSITION pos = CULDoc::m_DocList.GetHeadPosition();
		if (nFx > 0)	// It is a file
		{
			pDoc = CULDoc::GetDoc(--nFx);
			if (pDoc == NULL)
				continue ;
			
			pULView = (CULView*)pDoc->GetPPView(pParam);
			pProject = pDoc->m_pProject;
		}
		else if (nFx == 0)	// It is a project
		{
			continue ;
		}
		else if (nFx == -1)	// It is self
		{
			pDoc = m_pDoc;
			if (pDoc != NULL)
			{
				pULView = (CULView*)pDoc->GetPPView(pParam);
				pProject = pDoc->m_pProject;
			}
			else
			{
				continue ;
			}			
		}
		
		for (int nOff = 0; pos != NULL; nOff++)
		{
			if (pParam->m_nFx == -2) // Is a circle
			{
				pDoc = CULDoc::m_DocList.GetNext(pos);
				if (pDoc == NULL)
					continue ;
				
				pULView = (CULView*)pDoc->GetPPView(pParam);
				pProject = pDoc->m_pProject;
			}
			else
				pos = NULL; // Just do once

			switch (pParam->m_dwType)
			{
			case ULV_TOOLS:
			case ULV_CONSTRUCT:
			case ULV_CUSTOMIZEVIEW:
			case ULV_CALUSER:
			case ULV_CHARTS:
				{
					if (pULView)
					{
						int nHeight = pULView->CalcPageHeight() - PAGE_MARGIN;
						nTotalHeight += nHeight;
					}
				}			
				break;
			case ULV_GRAPH:
				{
					CGraphWnd* pGraph = (CGraphWnd*)pULView;
					if (pGraph == NULL)
						break;
					
					int nHeight = pGraph->CalcPageHeight();
					nTotalHeight += nHeight;
				}
				break;

			case ULV_PARAM:
				{
					CParamsView* pParamsView = (CParamsView*)pULView;
					if (pParamsView)
					{
						int nHeight = pParamsView->CalcPrintHeight();
						nTotalHeight += nHeight;
					}
				}
				break;			
			case ULV_CALCOEF:
				{
					CBCGPListCtrl* pList = GetList(i, nOff);
					if (pList != NULL)
					{
						CRect rcList;
						pList->GetWindowRect(&rcList);
						CSize size(rcList.Width(), rcList.Height());
						CDC* pDC = GetDC();
						pDC->DPtoLP(&size);
						ReleaseDC(pDC);
						int nHeight = abs(size.cy) + DEFAULT_TITLE_HEIGHT;
						nTotalHeight += nHeight;
					}
				}
				break;
#ifdef _NO_CALCELL
			case ULV_CALCHART:
				if (pULView)
					{
						int nHeight = pULView->CalcPageHeight() - PAGE_MARGIN;
						nTotalHeight += nHeight;
					}
				break;
#else
			case ULV_CALCHART:
#endif
			
			case ULV_RUNDATA:
				if (pULView)
				{
					int nHeight = pULView->CalcPageHeight() - PAGE_MARGIN;
					nTotalHeight += nHeight;
				}
				break;
			case ULV_CALSUMMARY:
			case ULV_GRAPHHEAD:
			case ULV_TABLEINFOVIEW:
			case ULV_TOOLINFOVIEW:
			case ULV_OUTPUTSTABLE:
				{
					CCell2000* pCell = GetCell(i, nOff);
					if (pCell != NULL)
					{
						int nHeight = GetCellSize(pCell).cy;
						nTotalHeight += nHeight;
					}
				}
				break;
			default:
				break;
			}
		}
	}

	return nTotalHeight;
}

void CGroupView::SaveAsBitmap(CDC* pDC, LPCTSTR pszFile)
{
	if (m_pPOFile == NULL)
		return;

	CRect rect = m_rcPage;
	rect.bottom = rect.top - CalcPrintHeight();
	CMyMemDC dc(pDC, &rect, -1);
	if (dc.m_hFile == NULL)
	{
		AfxMessageBox(IDS_CREATE_BITMAP_FAILED);
		return;
	}

	CWaitCursor wait;
	int nFS = -1;
	CArray<long, long> gridLines;
	if (m_listList.GetCount() || m_cellList.GetCount())
	{
		gridLines.SetSize(m_cellList.GetCount());
		POSITION pos = m_cellList.GetHeadPosition();
		for (int i = 0; pos != NULL; i++)
		{
			CCell2000* pCell = m_cellList.GetNext(pos);
			pCell->SelectRange(-1, -1, -1, -1);
			pCell->SetLeftCol(0);
			pCell->SetTopRow(0);
			gridLines[i] = pCell->GetGridLineState(0);
			if (gridLines[i])
				pCell->ShowGridLine(0, 0);
		}
		nFS = BeginCapture();
	}

	rect.top -= PAGE_MARGIN;
	rect.DeflateRect(PAGE_MARGIN, 0, PAGE_MARGIN, 0);

	CULDoc* pDoc = NULL;

	for (int i = 0; i<m_pPOFile->m_ParamArray.GetSize(); i++)
	{
		CPrintParam* pParam = (CPrintParam*)m_pPOFile->m_ParamArray.GetAt(i);

		int nFx = pParam->m_nFx;
		
		CULView* pULView = NULL;
		CProject* pProject = NULL;
		POSITION pos = CULDoc::m_DocList.GetHeadPosition();
		if (nFx > 0)	// It is a file
		{
			pDoc = CULDoc::GetDoc(--nFx);
			if (pDoc == NULL)
				continue ;
			
			pULView = (CULView*)pDoc->GetPPView(pParam);
			pProject = pDoc->m_pProject;
		}
		else if (nFx == 0)	// It is a project
		{
			continue ;
		}
		else if (nFx == -1)	// It is self
		{
			pDoc = m_pDoc;
			if (pDoc != NULL)
			{
				pULView = (CULView*)pDoc->GetPPView(pParam);
				pProject = pDoc->m_pProject;
			}
			else
			{
				continue ;
			}			
		}
		
		for (int nOff = 0; pos != NULL; nOff++)
		{
			if (pParam->m_nFx == -2) // Is a circle
			{
				pDoc = CULDoc::m_DocList.GetNext(pos);
				if (pDoc == NULL)
					continue ;
				
				pULView = (CULView*)pDoc->GetPPView(pParam);
				pProject = pDoc->m_pProject;
			}
			else
				pos = NULL; // Just do once

			switch (pParam->m_dwType)
			{
			case ULV_TOOLS:
			case ULV_CONSTRUCT:
			case ULV_CUSTOMIZEVIEW:
			case ULV_CALUSER:
			case ULV_CHARTS:
				{
					if (pULView)
					{
						int nHeight = pULView->CalcPageHeight();
						rect.bottom = rect.top - nHeight + PAGE_MARGIN;
						//rect.InflateRect(PAGE_MARGIN, 0, PAGE_MARGIN, 0);
						pULView->Draw(&dc, rect);
						//rect.DeflateRect(PAGE_MARGIN, 0, PAGE_MARGIN, 0);
						rect.top = rect.bottom;
					}
				}			
				break;
			case ULV_GRAPH:
				{
					CGraphWnd* pGraph = (CGraphWnd*)pULView;
					if (pGraph == NULL)
						break;
					
					long lTop, lBottom;
					int nHeight = pGraph->CalcPageHeight(lTop, lBottom);
					if (nHeight == 0)
						break;

					rect.bottom = rect.top - nHeight;
					CRect rectInfo = rect;
					
					if (Gbl_ConfigInfo.m_PrintInfo.bPrintLogInfoHead)
					{
						rectInfo.bottom = rectInfo.top - CLogInformationManager::m_lLogInfo_Height;	// DEFAULT_LOGINFO_HEIGHT;
						pGraph->DrawLogInfo(&dc, rectInfo, m_pDoc);
						rect.top = rectInfo.bottom;
					}
					
					if (Gbl_ConfigInfo.m_PrintInfo.bPrintLogInfoTail)
					{
						rect.bottom += CLogInformationManager::m_lLogInfo_Height; //DEFAULT_LOGINFO_HEIGHT;
					}

					pGraph->m_pSheet->DrawGraph(&dc, &rect, NULL, 
						lTop, lBottom, Gbl_ConfigInfo.m_PrintInfo.bPrintTrackHeadTwice);

					rectInfo.top = rect.bottom;
					if (Gbl_ConfigInfo.m_PrintInfo.bPrintLogInfoTail)
					{
						rectInfo.bottom = rectInfo.top - CLogInformationManager::m_lLogInfo_Height;	// DEFAULT_LOGINFO_HEIGHT;;
						pGraph->DrawLogInfo(&dc, rectInfo, m_pDoc);
						rect.bottom = rectInfo.bottom;
					}

					rect.top = rect.bottom;
				}
				break;
#ifndef _LOGIC
			case ULV_PARAM:
				{
					CParamsView* pParamsView = (CParamsView*)pULView;
					if (pParamsView)
					{
						int nHeight = pParamsView->CalcPrintHeight();
						rect.bottom = rect.top - nHeight;
						//rect.InflateRect(PAGE_MARGIN, 0, PAGE_MARGIN, 0);
						dc.m_bPrinting = TRUE;
						pParamsView->Draw(&dc, rect);
						dc.m_bPrinting = FALSE;
						//rect.DeflateRect(PAGE_MARGIN, 0, PAGE_MARGIN, 0);
						rect.top = rect.bottom;
					}
				}
				break;
			case ULV_CALCOEF:
				{
					CCalCoefView CCView;
					if (pProject)
						CCView.m_pArrTools = &pProject->m_ULKernel.m_arrTools;
				
					int nHeight = CCView.CalcPageHeight();
					rect.bottom = rect.top - nHeight;
					CCView.Draw(&dc, rect);
					rect.top -= DEFAULT_TITLE_HEIGHT;
				
					CBCGPListCtrl* pList = GetList(i, nOff);
					if (pList != NULL)
						CaptureWnd(&dc, pList, rect, m_pagePos.GetAt(i) - DEFAULT_TITLE_HEIGHT);
					rect.top = rect.bottom;
				}
				break;
#ifdef _NO_CALCELL
			case ULV_CALCHART:
				if (pULView)
				{
					int nHeight = pULView->CalcPageHeight();
					rect.bottom = rect.top - nHeight + PAGE_MARGIN;
					pULView->Draw(&dc, rect);
					rect.top = rect.bottom;
				}
				break;
#else
			case ULV_CALCHART:
#endif
			case ULV_RUNDATA:
				if (pULView)
				{
					int nHeight = pULView->CalcPageHeight();
					rect.bottom = rect.top - nHeight + PAGE_MARGIN;
					pULView->Draw(&dc, rect);
					rect.top = rect.bottom;
				}
				break;
			case ULV_CALSUMMARY:
#endif
			case ULV_GRAPHHEAD:
			case ULV_TABLEINFOVIEW:
			case ULV_TOOLINFOVIEW:
			case ULV_OUTPUTSTABLE:
				{
					CCell2000* pCell = GetCell(i, nOff);
					if (pCell != NULL)
					{
						int nHeight = GetCellSize(pCell).cy;
						rect.bottom = rect.top - nHeight;
						CaptureWnd(&dc, pCell, rect, m_pagePos.GetAt(i));
						rect.top = rect.bottom;
					}
				}
				break;
			default:
				break;
			}
		}
	}

	if (nFS != -1)
		EndCapture(nFS);

	POSITION pos = m_cellList.GetHeadPosition();
	for (int j = 0; pos != NULL; j++)
	{
		CCell2000* pCell = m_cellList.GetNext(pos);
		if (gridLines[j])
			pCell->ShowGridLine(1, 0);
	}

	if (dc.m_pBits != NULL)
	{
		ImageFileTypes fileType = DetermineFileType(pszFile);
		switch (fileType)
		{
		case BMPFileType:
			{
				BITMAPFILEHEADER	hdr;
				LPBITMAPINFOHEADER	lpbi;
				
				CFile file;
				if (!file.Open(pszFile, CFile::modeWrite|CFile::modeCreate))
					return;
				
				lpbi = (LPBITMAPINFOHEADER)dc.m_pbi;
				int nColors = 1 << lpbi->biBitCount;
				int sizeHdr = sizeof(BITMAPINFOHEADER) + 
					sizeof(RGBQUAD) * nColors;
				
				// Fill in the fields of the file header 
				hdr.bfType		= ((WORD) ('M' << 8) | 'B');	// is always "BM"
				hdr.bfSize		= 0; // lpbi->biSizeImage + (DWORD) (sizeof(hdr) + sizeHdr);
				hdr.bfReserved1 = 0;
				hdr.bfReserved2 = 0;
				hdr.bfOffBits	= (DWORD) (sizeof(hdr) + sizeHdr);
				
				try 
				{
					file.Write((LPVOID) &hdr, sizeof(BITMAPFILEHEADER));
					file.Write((LPVOID) lpbi, sizeHdr);
					file.Write((LPVOID) dc.m_pBits, lpbi->biSizeImage);
				}
				catch(CException* pe)
				{
					pe->ReportError();
					pe->Delete();
					AfxMessageBox("Failed to write bitmap file!\n");
				}
			}
			break;
		case JPEGFileType:
			{
				// result = WriteJPEG( szFile, hDIB);
			}
			break;
		case TIFFFileType:
			{
				WriteTIF(pszFile, dc.m_pbi, (LPBYTE)dc.m_pBits);
			}
			break;
/*		case GIFFileType:
			{
				errMsg = "GIF files not supported";
			}
			break;
		case OtherFileType:
			{
				errMsg = "File type not supported";
			}
			break;
		case UnknownFileType:
			{
				errMsg = "Unknown file type";
			}
			break;
*/		}

		return;
	}
	
	if (!WriteDCToDIB(pszFile, &dc))
	{
		AfxMessageBox(IDS_WRITE_BITMAP_FAILD);
	}
}

POSITION CGroupView::FindCell(CCell2000* pCell, CGraphHeaderView** ppFView)
{
	POSITION pos = CULDoc::m_DocList.GetHeadPosition();
	for ( ; pos != NULL; )
	{
		CULDoc* pDoc = CULDoc::m_DocList.GetNext(pos);
		if (pDoc == NULL)
			continue;

		POSITION pos1 = pDoc->FindCell(pCell, ppFView);
		if (pos1 != NULL)
			return pos1;
	}

	return NULL;
}

void CGroupView::ClearCells()
{
	m_pEdit = NULL;
	while (m_cellList.GetCount())
	{
		CCell2000* pCell = m_cellList.RemoveHead();
		CGraphHeaderView* pFView = NULL;
		POSITION pos = FindCell(pCell, &pFView);
		if (pos != NULL)
		{
			pFView->m_cellMd.RemoveKey((void*)pos);
			pFView->m_cellGroup.RemoveAt(pos);
		}
		delete pCell;
	}
	
	m_nCellID = idCellStart;
	m_cellPos.RemoveAll();
}


void CGroupView::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	if (pScrollBar != NULL && pScrollBar->SendChildNotifyLastMsg())
		return;     // eat it
	
	// ignore scroll bar msgs from other controls
	if (pScrollBar != GetScrollBarCtrl(SB_VERT))
		return;

	BOOL bRepos = FALSE;
	switch (nSBCode)
	{
	case SB_THUMBTRACK: // 由于在响应该消息时，工作不正常
		{
			nPos = GetScrollPos32(SB_VERT, TRUE);
		}
		break;
	case SB_ENDSCROLL:
		{
			bRepos = TRUE;
		}
		break;
	default:
		break;
	}
	
	OnScroll(MAKEWORD(-1, nSBCode), nPos);
	if (bRepos)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE|RDW_ERASE|RDW_ALLCHILDREN|RDW_UPDATENOW);
}

BEGIN_EVENTSINK_MAP(CGroupView, CULView)
	ON_EVENT_RANGE(CGroupView, idCellStart, idCellLast, 15 /* EditFinish */, OnEditFinishCell50, VTS_I4 VTS_BSTR VTS_PI4)
END_EVENTSINK_MAP()

void CGroupView::OnEditFinishCell50(UINT nID, LPCTSTR text, long FAR* approve)
{	
	POSITION pos = m_cellList.FindIndex(nID - idCellStart);
    if (pos == NULL)
		return;

	m_pEdit = (CCell2000*)m_cellList.GetAt(pos);
	
	CGraphHeaderView* pFView = NULL;
	pos = FindCell(m_pEdit, &pFView);
	if (pos == NULL)
		return;

	long col = m_pEdit->GetCurrentCol();
	long row = m_pEdit->GetCurrentRow();
	if (pFView->SaveCell(m_pEdit, col, row, 0))
	{
		pFView->m_pDoc->UpdateCells(ULV_GRAPHHEAD | ULV_GROUP);
		return;
	}

	PostMessage(UM_CELL_EDIT, (WPARAM)pFView, (LPARAM)pos);
}

LRESULT CGroupView::OnCellEdit(WPARAM wp, LPARAM lp)
{
	CGraphHeaderView* pFView = (CGraphHeaderView*)wp;
	CCell2000* pEdit = (CCell2000*)lp;
	if (pFView->m_Cell.GetSafeHwnd() == NULL)
		return 0;
	
	VARIANT data;
	if (m_pEdit->SaveToBuffer(&data) < 1)
		return 0;
	
	pFView->m_Cell.ReadFromBuffer(data);
	pFView->m_dwMdCount++;
	pFView->m_cellMd.SetAt((void*)lp, (void*)pFView->m_dwMdCount);

	return 0;
}

// GfxPageGrid.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "GfxPageGrid.h"
#include "Track.h"
#include "GraphWnd.h"
#include "Units.h"
#include "ComConfig.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CUnits* g_units;

/////////////////////////////////////////////////////////////////////////////
// CGfxPageGrid dialog


CGfxPageGrid::CGfxPageGrid(CWnd* pParent /*=NULL*/)
               : CGfxPage(IDD_GFX_PAGE_GRID, pParent)
{
	//{{AFX_DATA_INIT(CGfxPageGrid)
	m_bHLine = FALSE;
	m_bVLine = FALSE;
	m_nGroup = 0;
	m_nLineCount = 0;
	m_bHasVerticalSmallGrid = FALSE;
	m_bHasVerticalMiddleGrid = FALSE;
	//}}AFX_DATA_INIT
	m_bHorzGrid[0] = FALSE;
	m_bHorzGrid[1] = FALSE;
	m_bHorzGrid[2] = FALSE;
	m_nHorzGrid[0] = 0;
	m_nHorzGrid[1] = 0;
	m_nHorzGrid[2] = 0;

 	m_nHorzGridTime[0] = 10000; //add by gj 20121220
 	m_nHorzGridTime[1] = 30000;
 	m_nHorzGridTime[2] = 60000;
	for (int i =0;i<3;i++)//by xwh 预备保存深度步长变量
	{
		m_nHorzGridDepth[i] = 0;
	}

	m_nDriveMode = 1;  
}


void CGfxPageGrid::DoDataExchange(CDataExchange* pDX)
{
	CGfxPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGfxPageGrid)
	DDX_Control(pDX, IDC_COMBO_VGRIDTYPE, m_cbVerticalGridType);
	DDX_Control(pDX, IDC_CMB_VERT_MIDDLE_LINETYPE, m_cbVerticalMiddleLinetype);
	DDX_Control(pDX, IDC_CMB_VERT_SMALL_LINETYPE, m_cbVerticalSmallLinetype);
	DDX_Control(pDX, IDC_BTN_VERT_SMALL_LINECOLOR, m_btnVerticalSmallLineColor);
	DDX_Control(pDX, IDC_BTN_VERT_MIDDLE_LINECOLOR, m_btnVerticalMiddleLineColor);
	DDX_Check(pDX, IDC_CHECK_HLINE, m_bHLine);
	DDX_Check(pDX, IDC_CHECK_VLINE, m_bVLine);
	DDX_Text(pDX, IDC_EDIT_GROUP, m_nGroup);
	DDV_MinMaxInt(pDX, m_nGroup, 1, 999);
	DDX_Text(pDX, IDC_EDIT_LINE_COUNT, m_nLineCount);
	DDV_MinMaxInt(pDX, m_nLineCount, 1, 99999);
	DDX_Check(pDX, IDC_CHECK_VERT_SMALL_GRID, m_bHasVerticalSmallGrid);
	DDX_Check(pDX, IDC_CHECK_VERT_MIDDLE_GRID, m_bHasVerticalMiddleGrid);
	//}}AFX_DATA_MAP
	DDX_Check(pDX, IDC_CHECK_SMALL_GRID, m_bHorzGrid[0]);
	DDX_Check(pDX, IDC_CHECK_MIDDLE_GRID, m_bHorzGrid[1]);
	DDX_Check(pDX, IDC_CHECK_BIG_GRID, m_bHorzGrid[2]);
	DDX_Control(pDX, IDC_COMBO1, m_cbHorzLineWidth[0]);
	DDX_Control(pDX, IDC_COMBO2, m_cbHorzLineWidth[1]);
	DDX_Control(pDX, IDC_COMBO3, m_cbHorzLineWidth[2]);
	DDX_Control(pDX, IDC_COMBO4, m_cbVerticalSmallLinewidth);
	DDX_Control(pDX, IDC_COMBO5, m_cbVerticalMiddleLinewidth);
	DDX_Control(pDX, IDC_CMB_HORZ_SMALL_LINETYPE, m_cbHorzLinetype[0]);
	DDX_Control(pDX, IDC_CMB_HORZ_MIDDLE_LINETYPE, m_cbHorzLinetype[1]);
	DDX_Control(pDX, IDC_CMB_HORZ_BIG_LINETYPE, m_cbHorzLinetype[2]);
	DDX_Control(pDX, IDC_BTN_HORZ_SMALL_LINECOLOR, m_btnHorzLineColor[0]);
	DDX_Control(pDX, IDC_BTN_HORZ_MIDDLE_LINECOLOR, m_btnHorzLineColor[1]);
	DDX_Control(pDX, IDC_BTN_HORZ_BIG_LINECOLOR, m_btnHorzLineColor[2]);
	if (m_nDriveMode)   //add by gj 20121220
	{
		g_units->DDXVN_Text(pDX, IDC_EDIT1, IDC_STATIC1, m_nHorzGrid[0], 
			_T("LMetric"), 100, 2000000, _T("Depth"), _T("%.4lf"));
		g_units->DDXVN_Text(pDX, IDC_EDIT2, IDC_STATIC2, m_nHorzGrid[1],
			_T("LMetric"), 100, 2000000, _T("Depth"), _T("%.4lf"));
		g_units->DDXVN_Text(pDX, IDC_EDIT3, IDC_STATIC3, m_nHorzGrid[2],
			_T("LMetric"), 100, 2000000, _T("Depth"), _T("%.4lf"));
	}
	else//add by gj 20121220
	{
// COMMENT BY xwh 解决切换驱动模式时，参数保存互相影响
// 		m_nHorzGridTime[0] = m_nHorzGrid[0] / 1000;
// 		m_nHorzGridTime[1] = m_nHorzGrid[1] / 1000;
// 		m_nHorzGridTime[2] = m_nHorzGrid[2] / 1000;

		DDX_Text(pDX, IDC_EDIT1, m_nHorzGridTime[0]);
		DDX_Text(pDX, IDC_EDIT2, m_nHorzGridTime[1]);
		DDX_Text(pDX, IDC_EDIT3, m_nHorzGridTime[2]);
		
		CStatic* pStatic = (CStatic*)GetDlgItem(IDC_STATIC1);
		pStatic->SetWindowText("ms");
		pStatic = (CStatic*)GetDlgItem(IDC_STATIC2);
		pStatic->SetWindowText("ms");
		pStatic = (CStatic*)GetDlgItem(IDC_STATIC3);
		pStatic->SetWindowText("ms");

		CString str1,str2,str3;
		str1.Format("%.2lf",m_nHorzGridTime[0]);
		SetDlgItemText(IDC_EDIT1,str1);
		str2.Format("%.2lf",m_nHorzGridTime[1]);
		SetDlgItemText(IDC_EDIT2,str2);
		str3.Format("%.2lf",m_nHorzGridTime[2]);
		SetDlgItemText(IDC_EDIT3,str3);	
		
// 		m_nHorzGrid[0] = m_nHorzGridTime[0] * 1000;
// 		m_nHorzGrid[1] = m_nHorzGridTime[1] * 1000;
// 		m_nHorzGrid[2] = m_nHorzGridTime[2] * 1000;

	}

//	m_bFirst = FALSE;

	if (pDX->m_bSaveAndValidate)
	{
		for (int i = 0; i < 3; i++)
		{
			if (m_nDriveMode) //add by gj
			{
				m_nHorzGrid[i] = floor(m_nHorzGrid[i] + .5);
			}
			else
				m_nHorzGridTime[i] = m_nHorzGridTime[i] ;
		}
	}
}


BEGIN_MESSAGE_MAP(CGfxPageGrid, CGfxPage)
	//{{AFX_MSG_MAP(CGfxPageGrid)
	ON_BN_CLICKED(IDC_CHECK_HLINE, RefreshData)
	ON_BN_CLICKED(IDC_CHECK_VLINE, RefreshData)
	ON_BN_CLICKED(IDC_CHECK_SMALL_GRID, RefreshData)
	ON_BN_CLICKED(IDC_CHECK_MIDDLE_GRID, RefreshData)
	ON_BN_CLICKED(IDC_CHECK_BIG_GRID, RefreshData)
	ON_EN_CHANGE(IDC_EDIT_GROUP, OnChangeEditGroup)
	ON_EN_CHANGE(IDC_EDIT_LINE_COUNT, OnChangeEditLineCount)
	ON_BN_CLICKED(IDC_CHECK_VERT_SMALL_GRID, RefreshData)
	ON_BN_CLICKED(IDC_CHECK_VERT_MIDDLE_GRID, RefreshData)
	ON_CBN_SELCHANGE(IDC_COMBO1, RefreshData)
	ON_CBN_SELCHANGE(IDC_COMBO2, RefreshData)
	ON_CBN_SELCHANGE(IDC_COMBO3, RefreshData)
	ON_CBN_SELCHANGE(IDC_COMBO4, RefreshData)
	ON_CBN_SELCHANGE(IDC_COMBO5, RefreshData)
	ON_CBN_SELCHANGE(IDC_CMB_HORZ_SMALL_LINETYPE, RefreshData)
	ON_CBN_SELCHANGE(IDC_CMB_HORZ_MIDDLE_LINETYPE, RefreshData)
	ON_CBN_SELCHANGE(IDC_CMB_HORZ_BIG_LINETYPE, RefreshData)
	ON_CBN_SELCHANGE(IDC_CMB_VERT_SMALL_LINETYPE, RefreshData)
	ON_CBN_SELCHANGE(IDC_CMB_VERT_MIDDLE_LINETYPE, RefreshData)
	ON_EN_KILLFOCUS(IDC_EDIT1, RefreshData)
	ON_EN_KILLFOCUS(IDC_EDIT2, RefreshData)
	ON_EN_KILLFOCUS(IDC_EDIT3, RefreshData)
	ON_EN_KILLFOCUS(IDC_EDIT_LINE_COUNT, OnKillfocusEditLineCount)
	ON_EN_KILLFOCUS(IDC_EDIT_GROUP, OnKillfocusEditGroup)
	ON_BN_CLICKED(IDC_BTN_HORZ_SMALL_LINECOLOR, RefreshData)
	ON_BN_CLICKED(IDC_BTN_HORZ_MIDDLE_LINECOLOR, RefreshData)
	ON_BN_CLICKED(IDC_BTN_HORZ_BIG_LINECOLOR, RefreshData)
	ON_BN_CLICKED(IDC_BTN_VERT_SMALL_LINECOLOR, RefreshData)
	ON_BN_CLICKED(IDC_BTN_VERT_MIDDLE_LINECOLOR, RefreshData)
	ON_WM_DESTROY()
	ON_CBN_SELCHANGE(IDC_COMBO_VGRIDTYPE, RefreshData)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGfxPageGrid message handlers

void CGfxPageGrid::InitTrack()
{
	if (m_pTrack != NULL)
	{
		GridProp* pGrids = &m_pTrack->m_GridInfo;
		m_bHLine = pGrids->bHasHLine;

		for (int i = 0; i < 3; i++)
		{
			m_bHorzGrid[i] = pGrids->hLine[i].bHasGrid;
		//	m_nHorzGrid[i] = pGrids->hLine[i].nGridStep;
			m_nHorzGrid[i] = m_pTrack->m_dHorzGridDepth[i];//by xwh 　读取时间驱动模式时间步长
			m_nHorzGridTime[i] = m_pTrack->m_dHorzGridTime[i];

	
			m_cbHorzLineWidth[i].SetCurSel(LWLevel(pGrids->hLine[i].nGridLinewidth));
			m_cbHorzLinetype[i].SetSelectedLineStyle(pGrids->hLine[i].nGridLinetype);
			m_btnHorzLineColor[i].SetColor(pGrids->hLine[i].clrGrid);
		}

		m_bVLine = pGrids->bHasVLine;
		m_nGroup = pGrids->vLineInfo.nGroupCount;
		m_nLineCount = pGrids->vLineInfo.nLineCount;

		int nLWLevel = LWLevel(pGrids->vLineInfo.nSmallGridLinewidth);
		m_cbVerticalSmallLinewidth.SetCurSel(nLWLevel);
		nLWLevel = LWLevel(pGrids->vLineInfo.nMiddleGridLinewidth);
		m_cbVerticalMiddleLinewidth.SetCurSel(nLWLevel);
		m_cbVerticalGridType.SetCurSel(pGrids->vLineInfo.nGridType);

		m_bHasVerticalSmallGrid = pGrids->vLineInfo.bHasSmallGrid;
		m_bHasVerticalMiddleGrid = pGrids->vLineInfo.bHasMiddleGrid;

		//获得格线线型设置
		m_cbVerticalMiddleLinetype.SetSelectedLineStyle(pGrids->vLineInfo.nMiddleGridLinetype);
		m_cbVerticalSmallLinetype.SetSelectedLineStyle(pGrids->vLineInfo.nSmallGridLinetype);

		//获得格线颜色设置
		m_btnVerticalMiddleLineColor.SetColor(pGrids->vLineInfo.colorMiddleGrid);
		m_btnVerticalSmallLineColor.SetColor(pGrids->vLineInfo.colorSmallGrid);

			// 此测井视图被实时打印时, 禁止对格子的修改 
		BOOL bLogPrint = FALSE;
		if (m_pGraph != NULL && m_pGraph->m_bLoggingPrint)
			bLogPrint = TRUE;
		if (bLogPrint)
		{
			EnableDlgItem(IDC_CHECK_HLINE, FALSE);
			EnableHorzGridItem(FALSE);
			EnableDlgItem(IDC_CHECK_VLINE, FALSE);
			EnableVertGirdItem(FALSE);
		}
		/*else
		{
			EnableHorzGridItem(m_bHLine);
			EnableVertGirdItem(m_bVLine);
		}*/

		UpdateData(FALSE);
	}
}

void CGfxPageGrid::EnableDlgItem(UINT nID, BOOL bEnable)
{
	CWnd* pWnd = GetDlgItem(nID);
	if (pWnd != NULL)
	{
		pWnd->EnableWindow(bEnable);
	}
}

void CGfxPageGrid::EnableHorzGridItem(BOOL bHLine)
{
	EnableDlgItem(IDC_CHECK_SMALL_GRID, bHLine);
	EnableDlgItem(IDC_COMBO1, bHLine);
	EnableDlgItem(IDC_CMB_HORZ_SMALL_LINETYPE, bHLine);
	EnableDlgItem(IDC_BTN_HORZ_SMALL_LINECOLOR, bHLine);
	EnableDlgItem(IDC_EDIT1, bHLine);
	EnableDlgItem(IDC_CHECK_MIDDLE_GRID, bHLine); 
	EnableDlgItem(IDC_COMBO2, bHLine);
	EnableDlgItem(IDC_CMB_HORZ_MIDDLE_LINETYPE, bHLine);
	EnableDlgItem(IDC_BTN_HORZ_MIDDLE_LINECOLOR, bHLine);
	EnableDlgItem(IDC_EDIT2, bHLine);
	EnableDlgItem(IDC_CHECK_BIG_GRID, bHLine);
	EnableDlgItem(IDC_COMBO3, bHLine);
	EnableDlgItem(IDC_CMB_HORZ_BIG_LINETYPE, bHLine);
	EnableDlgItem(IDC_BTN_HORZ_BIG_LINECOLOR, bHLine);
	EnableDlgItem(IDC_EDIT3, bHLine);
}

void CGfxPageGrid::EnableVertGirdItem(BOOL bVLine)
{
	EnableDlgItem(IDC_CHECK_VERT_SMALL_GRID, bVLine);
	EnableDlgItem(IDC_COMBO4, bVLine);
	EnableDlgItem(IDC_CMB_VERT_SMALL_LINETYPE, bVLine);
	EnableDlgItem(IDC_BTN_VERT_SMALL_LINECOLOR, bVLine);
	EnableDlgItem(IDC_EDIT_GROUP, bVLine);
	EnableDlgItem(IDC_CHECK_VERT_MIDDLE_GRID, bVLine);
	EnableDlgItem(IDC_COMBO5, bVLine);
	EnableDlgItem(IDC_CMB_VERT_MIDDLE_LINETYPE, bVLine);
	EnableDlgItem(IDC_BTN_VERT_MIDDLE_LINECOLOR, bVLine);
	EnableDlgItem(IDC_COMBO_VGRIDTYPE, bVLine);
	EnableDlgItem(IDC_EDIT_LINE_COUNT, bVLine);
}

void CGfxPageGrid::RefreshData()
{
	UpdateData();
	if (m_pTrack != NULL)
	{
		GridProp* pGrids = &m_pTrack->m_GridInfo;
		pGrids->bHasHLine = m_bHLine;	

		if (m_nDriveMode)
		{
			if (m_nHorzGrid[2] == 0) m_nHorzGrid[2] = 250000;
			if (m_nHorzGrid[1] == 0) m_nHorzGrid[1] = 50000;		
			if (m_nHorzGrid[0] == 0) m_nHorzGrid[0] = 10000;
		}
		else
		{
			if (m_nHorzGridTime[2] == 0) m_nHorzGridTime[2] = 600000;
			if (m_nHorzGridTime[1] == 0) m_nHorzGridTime[1] = 300000;		
			if (m_nHorzGridTime[0] == 0) m_nHorzGridTime[0] = 100000;
		}
		
		for (int i = 0; i < 3; i++)
		{
			pGrids->hLine[i].bHasGrid = m_bHorzGrid[i];
			
			//	pGrids->hLine[i].nGridStep = m_nHorzGrid[i];
			m_pTrack->m_dHorzGridDepth[i] = m_nHorzGrid[i];//by xwh 　保存时间驱动模式时间步长
			m_pTrack->m_dHorzGridTime[i] = m_nHorzGridTime[i];
	
			pGrids->hLine[i].nGridLinewidth = AfxGetComConfig()->SetLWLevel(m_cbHorzLineWidth[i].GetCurSel());
			pGrids->hLine[i].nGridLinetype = m_cbHorzLinetype[i].GetSelectedLineStyle();
			pGrids->hLine[i].clrGrid = m_btnHorzLineColor[i].GetColor();			
		}

		pGrids->bHasVLine = m_bVLine;
		
		int nCount0 = pGrids->vLineInfo.nGroupCount*pGrids->vLineInfo.nLineCount;
		pGrids->vLineInfo.nGroupCount = m_nGroup;
		pGrids->vLineInfo.nLineCount = m_nLineCount;

		int nLWLevel = m_cbVerticalSmallLinewidth.GetCurSel();
		pGrids->vLineInfo.nSmallGridLinewidth = AfxGetComConfig()->SetLWLevel(nLWLevel);
		nLWLevel = m_cbVerticalMiddleLinewidth.GetCurSel();
		pGrids->vLineInfo.nMiddleGridLinewidth = AfxGetComConfig()->SetLWLevel(nLWLevel);

		if(m_cbVerticalGridType.GetCurSel() != CB_ERR)
			pGrids->vLineInfo.nGridType = m_cbVerticalGridType.GetCurSel();
		pGrids->vLineInfo.bHasSmallGrid = m_bHasVerticalSmallGrid;
		pGrids->vLineInfo.bHasMiddleGrid = m_bHasVerticalMiddleGrid;

		// 获得格线线型设置
		pGrids->vLineInfo.nMiddleGridLinetype = m_cbVerticalMiddleLinetype.GetSelectedLineStyle();
		pGrids->vLineInfo.nSmallGridLinetype = m_cbVerticalSmallLinetype.GetSelectedLineStyle();

		// 获得格线颜色设置
		pGrids->vLineInfo.colorMiddleGrid = m_btnVerticalMiddleLineColor.GetColor();
		pGrids->vLineInfo.colorSmallGrid = m_btnVerticalSmallLineColor.GetColor();
		m_pTrack->ChangeGridCount(nCount0);
	}

	if (m_pGraph!= NULL)
		m_pGraph->InvalidateAll();
}

void CGfxPageGrid::OnChangeEditGroup()
{
	CString str;
	GetDlgItem(IDC_EDIT_GROUP)->GetWindowText(str);

	if (str.IsEmpty())
		return;

	if (atoi(str) >= 1 && atoi(str) <= 20)
		RefreshData();
}

void CGfxPageGrid::OnChangeEditLineCount()
{
	CString str;
	GetDlgItem(IDC_EDIT_LINE_COUNT)->GetWindowText(str);

	if (str.IsEmpty())
		return;

	if (atoi(str) >= 1 && atoi(str) <= 1000)
		RefreshData();
}

BOOL CGfxPageGrid::OnInitDialog()
{
	CGfxPage::OnInitDialog();
	
	m_btnVerticalSmallLineColor.EnableOtherButton ("");
	m_btnVerticalSmallLineColor.SetColor ((COLORREF)-1);
	m_btnVerticalSmallLineColor.SetColumnsNumber (10);

	m_btnVerticalMiddleLineColor.EnableOtherButton ("");
	m_btnVerticalMiddleLineColor.SetColor ((COLORREF)-1);
	m_btnVerticalMiddleLineColor.SetColumnsNumber (10);

	//初始化选择线型的控件
	m_cbVerticalMiddleLinetype.Initialize();
	m_cbVerticalSmallLinetype.Initialize();
	m_cbVerticalSmallLinewidth.Initialize();
	m_cbVerticalMiddleLinewidth.Initialize();

	for (int i = 0; i < 3; i++)
	{
		m_cbHorzLineWidth[i].Initialize();
		m_cbHorzLinetype[i].Initialize();
		m_btnHorzLineColor[i].EnableOtherButton ("");
		m_btnHorzLineColor[i].SetColor ((COLORREF)-1);
		m_btnHorzLineColor[i].SetColumnsNumber (10);
	}
	

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CGfxPageGrid::OnKillfocusEditLineCount()
{
	CString str;
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_LINE_COUNT);
	pEdit->GetWindowText(str);
	if (str.IsEmpty())
	{
		str.Format("%d", m_nLineCount);
		pEdit->SetWindowText(str);
	}
}

void CGfxPageGrid::OnKillfocusEditGroup()
{
	CString str;
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_GROUP);
	pEdit->GetWindowText(str);
	if (str.IsEmpty())
	{
		str.Format("%d", m_nGroup);
		pEdit->SetWindowText(str);
	}
}

void CGfxPageGrid::OnDestroy() 
{
	CGfxPage::OnDestroy();
	
	// TODO: Add your message handler code here
	m_pTrack = NULL;
	m_pGraph = NULL;
}

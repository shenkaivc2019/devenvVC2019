// ULFilter.cpp: implementation of the CULFilter class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ULFilter.h"
#include "ComConfig.h"
#include "..\UL2000\resource.h"
#include "XMLSettings.h"
#include "AllInfoPack.h" //add by gj 20121212 为修改服务项目中滤波

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

typedef IUnknown*(FAR PASCAL* _CREATEFILTER)();

CPtrArray CULFilter::m_arFilters;
CComConfig* CULFilter::m_pConfigInfo = NULL;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CULFilter::CULFilter()
{
	m_pIFilter = NULL;
	m_strTypeName.LoadString(IDS_NULL);
	m_pCurve = NULL;
	m_pULFile = NULL; // 2011/9/13 add by bao
	m_CurveInfoPack = NULL;//add by gj 20121212 为修改服务项目中滤波
}

CULFilter::~CULFilter()
{
	if (m_pIFilter != NULL)
	{
		m_pIFilter->Release();
		FreeLibrary(m_hDllInst);
		m_pIFilter = NULL;
	}
}

// -----------------------------------------------------------------------
//  Interface Implementation
// -----------------------------------------------------------------------
int CULFilter::GetFilters()
{
	if (m_pConfigInfo == NULL)
	{
		// TRACE("Warning : Filter config pointer is null!\n");
		return 0;
	}

	ClearFilters();
	int nFilter = m_pConfigInfo->m_filterArray.GetSize();
	int nResult = 0;
	for (int i = 0; i < nFilter; i++)
	{
		CULFilter* pFilter = new CULFilter;
		if (pFilter->LoadFilter(m_pConfigInfo->m_filterArray[i].FilterName))
		{
			m_arFilters.Add(pFilter);
			nResult++;
		}
		else
		{
			TRACE(_T("Warning : Load filter %s failed!\n"),
				m_pConfigInfo->m_filterArray[i].FilterName);
			delete pFilter;
		}
	}

	return nResult;
}

CULFilter* CULFilter::GetFilter(int nIndex)
{
	if (nIndex < 0)
		return NULL;

	if (nIndex < m_arFilters.GetSize())
		return (CULFilter *) m_arFilters[nIndex];

	return NULL;
}

CULFilter* CULFilter::GetFilter(LPCTSTR lpszFilterType)
{
	for (int i = 0; i < m_arFilters.GetSize(); i++)
	{
		CULFilter* pFilter = (CULFilter*) m_arFilters.GetAt(i);
		if (pFilter->m_strTypeName == lpszFilterType)
			return pFilter;
	}

	CULFilter* pFilter = new CULFilter;
	if (pFilter->LoadFilter(lpszFilterType))
	{
		m_arFilters.Add(pFilter);
		return pFilter;
	}

	TRACE(_T("Warning : Load filter %s failed!\n"), lpszFilterType);
	delete pFilter;

	return NULL;
}

void CULFilter::ClearFilters()
{
	for (int i = 0; i < m_arFilters.GetSize(); i++)
	{
		CULFilter* pFilter = (CULFilter*) m_arFilters.GetAt(i);
		if (AfxIsValidAddress(pFilter, sizeof(CULFilter)))
			delete pFilter;
	}
	m_arFilters.RemoveAll();
}

ULMBOOLIMP CULFilter::LoadFilter(LPCTSTR strTypeName)
{
	ReleaseFilter();

	CString strName = strTypeName;
	strName.TrimLeft();
	strName.TrimRight();
	strName.MakeUpper();

	if (m_pConfigInfo == NULL)
	{
		// TRACE("Warning : Filter config pointer is null!\n");
		return FALSE;
	}

	FILTER_INFO* pFilterInfo = m_pConfigInfo->GetFilterInfo(strName);
	if (pFilterInfo == NULL)
	{
		// TRACE("Warning : Filter " + strName + " not found, please install it first!\n");
		return FALSE;
	}

	CString str = m_pConfigInfo->m_strAppPath +
		_T("\\FILTERS\\") +
		pFilterInfo->ManuFactory +
		_T("\\") +
		pFilterInfo->FilterName + _T("\\"); 

	CString strDllPath = str + pFilterInfo->dllName;

	m_hDllInst = LoadLibrary(strDllPath);
	if (m_hDllInst == NULL)
	{
		TRACE(str + " failed to load!\n");
		return FALSE;
	}

	_CREATEFILTER CreateFilter = (_CREATEFILTER) GetProcAddress(m_hDllInst,
													"CreateFilter");
	if (CreateFilter == NULL)
	{
		TRACE(str + " failed to access the interface!\n");
		FreeLibrary(m_hDllInst);
		m_hDllInst = NULL;
		return FALSE;
	}

	m_pIFilter = (IFilterBase *) CreateFilter();
	HRESULT hr;
	hr = m_pIFilter->QueryInterface(IID_IFilter, (void * *) &m_pIFilter);
	if (FAILED(hr))
	{
		ReleaseFilter();
		return FALSE;
	}

	m_strTypeName = strName;
	m_pIFilter->AdviseFilter(this);
	  
	CString strIniPath = str + _T("Filter.ini");
	CXMLSettings xml;

	int nCount = 0;
	if( xml.ReadXMLFromFile(strIniPath) && xml.Read(_T("ParamCount"), nCount) && nCount > 0 )
	{
		m_Params.ClearParams();
		CString strIndex;
		CString strValue;
		for(int i=0; i<nCount; i++)
		{
			CString strName;
			double dValue = 0;
			CString strDesc;
			strIndex.Format( _T("%d"), i+1 );
			if (xml.Open(strIndex))
			{
				xml.Read( _T("Name"), strName);
				xml.Read( _T("Value"), strValue);
				
				LPTSTR stop;
				dValue = _tcstod(strValue, &stop);
				xml.Read( _T("Description"), strDesc);
				xml.Back();
			}
			AddParam( strName, dValue, strDesc);
		}
	}
	else
	{
		m_pIFilter->InitFilter();
	}
		
	return TRUE;
}

void CULFilter::ReleaseFilter()
{
	if (m_pIFilter)
	{
		ASSERT(m_hDllInst);
		m_pIFilter->Release();
		FreeLibrary(m_hDllInst);
		m_hDllInst = NULL;
		m_pIFilter = NULL;
	}
}

void CULFilter::Assign(CULFilter& srcFilter)
{
	// 不同的滤波器之间不能相互拷贝
	ASSERT(GetFilterName() == srcFilter.GetFilterName());
	m_Params.Copy(&srcFilter.m_Params);
}

void CULFilter::Serialize(CXMLSettings& xml)
{
	if (xml.IsStoring())
	{
		m_Params.m_strFilter = m_strTypeName;
		m_Params.Serialize(xml);
	}
	else
	{
		m_Params.Serialize(xml);
		m_strTypeName = m_Params.m_strFilter;
	}
}

short CULFilter::ParamSetting()
{
	if (m_pIFilter)
		return m_pIFilter->ParamSetting();

	return UL_ERROR;
}

ULMIMP CULFilter::Filter(ICurve* pCurve, int nStartPos, int nEndPos)
{
	if (m_pIFilter)
		return m_pIFilter->Filter(pCurve, nStartPos, nEndPos);

	return UL_NO_ERROR;
}

//////////////////////////////////////////////////////////////////////////

void CULFilter::SetParams()
{
	CFilterParams oldParams;
	//保存参数备份
	oldParams.Copy(&m_Params);

	FILTER_INFO* pFilterInfo = m_pConfigInfo->GetFilterInfo(m_strTypeName);
	 if (pFilterInfo != NULL)
	{
			CString strFilterPath = m_pConfigInfo->m_strAppPath +
				          "\\FILTERS\\" +
				          pFilterInfo->ManuFactory +
				          "\\" +
				          pFilterInfo->FilterName +
				          "\\";
			strFilterPath += "Filter.ini";

			SaveCurParam();
			if (ParamSetting() == UL_NO_ERROR)
			{
				if (pFilterInfo != NULL)
				{
					CXMLSettings xml( FALSE, _T("FilterParams") );
					int nCount = GetParamsCount();
					if(nCount > 0)
					{
						xml.Write( _T("ParamCount"), nCount);
						
						CString strValue;
						CString strIndex;
						for(int i=0; i<nCount; i++)
						{
							strIndex.Format(_T("%d"), i+1);
							xml.CreateKey( strIndex );
							xml.Write( _T("Name"), GetParamName(i) );
							double dValue = GetParamValue(i);
							strValue.Format(_T("%f"), dValue );
							xml.Write( _T("Value"), strValue );
							xml.Write( _T("Description"), GetParamDescription(i) );
							xml.Back(-1);
						}
					}  
					xml.WriteXMLToFile(strFilterPath);
				}
			}
			else
			{
				//取消设定时，恢复参数备份
				m_Params.Attach(&oldParams);
			}			
	 }
}

ULMTSTRIMP CULFilter::GetFilterPath()
{
	TCHAR szPath[_MAX_PATH];
	GetModuleFileName(m_hDllInst, szPath, _MAX_PATH);
 	CString strPath = szPath;
	strPath = strPath.Left(strPath.ReverseFind('\\'));
	m_strFilePath = strPath;
	return (LPTSTR)(LPCTSTR)m_strFilePath;
}

// 获取指定曲线指针 2011/9/13 add by bao
ULMPTRIMP CULFilter::GetCurvePtr(LPCTSTR pszName)
{
	return ((CULFile*)m_pULFile)->GetCurve(pszName);
}
void CULFilter::SetULFilePtr(CDocument *pULFile)
{
	m_pULFile = (CULFile*)pULFile;
}

BOOL CULFilter::SaveFilter(CFile* pFile, BOOL bRP /* = FALSE */, BOOL bDelete /* = TRUE */)
{
	CXMLSettings xml(FALSE);
	Serialize(xml);
	if (xml.WriteXMLToFile(pFile))
	{
		return TRUE;
	}		

	return FALSE;
}

BOOL CULFilter::ReadFilter(CFile* pFile, BOOL bRP /* = FALSE */, BOOL bDelete /* = TRUE */)
{
	CXMLSettings xml;
	if (xml.ReadXMLFromFile(pFile))
	{
		RemoveAllParams();
		Serialize(xml);
		LoadFileFilter(m_strTypeName);
		return TRUE;
	}
	

	return FALSE;
}

BOOL CULFilter::LoadFileFilter(LPCTSTR strTypeName)
{
	ReleaseFilter();
	
	CString strName = strTypeName;
	strName.TrimLeft();
	strName.TrimRight();
	strName.MakeUpper();
	
	if (m_pConfigInfo == NULL)
	{
		// TRACE("Warning : Filter config pointer is null!\n");
		return FALSE;
	}
	
	FILTER_INFO* pFilterInfo = m_pConfigInfo->GetFilterInfo(strName);
	if (pFilterInfo == NULL)
	{
		// TRACE("Warning : Filter " + strName + " not found, please install it first!\n");
		return FALSE;
	}
	
	CString str = m_pConfigInfo->m_strAppPath +
		_T("\\FILTERS\\") +
		pFilterInfo->ManuFactory +
		_T("\\") +
		pFilterInfo->FilterName + _T("\\"); 
	
	CString strDllPath = str + pFilterInfo->dllName;
	
	m_hDllInst = LoadLibrary(strDllPath);
	if (m_hDllInst == NULL)
	{
		TRACE(str + " failed to load!\n");
		return FALSE;
	}
	
	_CREATEFILTER CreateFilter = (_CREATEFILTER) GetProcAddress(m_hDllInst,
		"CreateFilter");
	if (CreateFilter == NULL)
	{
		TRACE(str + " failed to access the interface!\n");
		FreeLibrary(m_hDllInst);
		m_hDllInst = NULL;
		return FALSE;
	}
	
	m_pIFilter = (IFilterBase *) CreateFilter();
	HRESULT hr;
	hr = m_pIFilter->QueryInterface(IID_IFilter, (void * *) &m_pIFilter);
	if (FAILED(hr))
	{
		ReleaseFilter();
		return FALSE;
	}
	
	m_strTypeName = strName;
	m_pIFilter->AdviseFilter(this);
	
	return TRUE;
}

BOOL CULFilter::SaveCurParam()
{
	if (m_pCurve == NULL && m_CurveInfoPack == NULL)//add by gj 20121212 为修改服务项目中滤波
	{
		return FALSE;
	}
	TCHAR szPath[_MAX_PATH];
	GetModuleFileName(m_hDllInst, szPath, _MAX_PATH);
	CString strPath = szPath;
	strPath = strPath.Left(strPath.ReverseFind('\\'));

	strPath += "\\Param.ini";

	CXMLSettings xml(TRUE);
	if (xml.ReadXMLFromFile(strPath))
	{
		int nCount = 0;
		xml.Read("Count", nCount);
		if (nCount > 0)
		{
			int i = 0;
			for (i = 0; i < m_ParamArray.GetSize(); i++)
			{
				Param p = m_ParamArray.GetAt(i);
				delete p.pParam;
			}
			if (m_ParamArray.GetSize() <= 0)
			{
				m_ParamArray.SetSize(nCount);
			}
			//m_ParamArray.RemoveAll();
			for (i = 0; i < nCount; i++)
			{	
				if (xml.Open("Param%d", i))
				{
					Param p;
					p.nSize = 0;
					p.pParam = NULL;
					xml.Read("ParamSize", p.nSize);
					p.pParam = new double[p.nSize];
					UINT nBytes = 0;
					xml.Read("ParamList", (LPBYTE)p.pParam, nBytes);
					xml.Back();
					m_ParamArray.SetAt(i, p);
				}
			}
		}
	}

	//modify by gj 20121212 为修改服务项目中滤波
	int nSel = 0;
	if (m_pCurve != NULL)
	{
		nSel = m_pCurve->GetFilterTemplate();
	}
	else if (m_CurveInfoPack != NULL)
	{
		nSel = m_CurveInfoPack->m_nFilterTempl;
	}//

/*	if (nSel > -1 && m_ParamArray.GetSize() > 0)
	{
		Param p = m_ParamArray.GetAt(nSel);
		delete p.pParam;
		p.nSize = GetParamsCount();
		p.pParam = new double[p.nSize];
		for (int j = 0; j < p.nSize; j++)
		{
			double dValue = 1.0/p.nSize;
			p.pParam[j] = dValue;
		}
		m_ParamArray.SetAt(nSel, p);
	}*/

	if (nSel > -1 && m_ParamArray.GetSize() > 0)
	{
		Param p = m_ParamArray.GetAt(nSel);
		delete p.pParam;
		p.nSize = GetParamsCount();
		p.pParam = new double[p.nSize];
		for (int j = 0; j < p.nSize; j++)
		{
			double dValue = GetParamValue(j);//1.0/p.nSize;
			p.pParam[j] = dValue;
		}
		m_ParamArray.SetAt(nSel, p);
	}

	xml.m_bReadOnly = FALSE;
	if (xml.IsStoring())
	{
		//m_xmlSettings.Write("Sel", m_nSel);
		int nCount = m_ParamArray.GetSize();
		xml.Write("Count", nCount);
		for (int i = 0; i < m_ParamArray.GetSize(); i++)
		{
			Param p = m_ParamArray.GetAt(i);
			if (xml.CreateKey("Param%d", i))
			{
				xml.Write("ParamSize", p.nSize);
				xml.Write("ParamList", (LPBYTE)p.pParam, sizeof(double) * p.nSize);
				xml.Back();
			}
		}
	}
	xml.WriteXMLToFile(strPath);

	return TRUE;
}

//modify by gj 20121212 从.h文件中移来
ULMIMP CULFilter::SetTemplate(int nTemplate)
{
	if (m_pCurve != NULL)
		return m_pCurve->SetFilterTemplate(nTemplate);
	else if (m_CurveInfoPack != NULL)
	{
		m_CurveInfoPack->m_nFilterTempl = nTemplate;
		return UL_NO_ERROR;
	}
	else
		return UL_ERROR;
}
//modify by gj 20121212 从.h文件中移来
ULMINTIMP CULFilter::GetTemplate()
{
	if (m_pCurve != NULL)
		return m_pCurve->GetFilterTemplate();
	else if (m_CurveInfoPack != NULL)
	{
		return m_CurveInfoPack->m_nFilterTempl;
	}
	else
		return 0;
}
// GfxPageMark.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "GfxPageMark.h"
#include "GraphWnd.h"
#include "UserDefinedMark.h"
#include "MainFrm.h"
#include "Curve.h"
#include "Sheet.h"
#include "ChildFrm.h"
#include "BaseMark.h"
#include "BaseMarker.h"
#include "Units.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CUnits* g_units;

/////////////////////////////////////////////////////////////////////////////
// CGfxPageMark dialog

CGfxPageMark::CGfxPageMark(CWnd* pParent /*=NULL*/) : CGfxPage(IDD_GFX_PAGE_MARK, pParent)
{
	//{{AFX_DATA_INIT(CGfxPageMark)
	m_strName = _T("");
	//}}AFX_DATA_INIT
	m_lDepth = 0;
	m_nTrackNO = 0;
	m_markColor = RGB(0, 0, 0);
}


void CGfxPageMark::DoDataExchange(CDataExchange* pDX)
{
	CGfxPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGfxPageMark)
	DDX_Control(pDX, IDC_MARK_COLOR, m_MarkColorBtn);
	DDX_Control(pDX, IDC_COMBO_PAINTMODE, m_cbPaintMode);
	DDX_Control(pDX, IDC_LIST1, m_markList);
	DDX_Text(pDX, IDC_EDIT1, m_strName);
	//}}AFX_DATA_MAP
	g_units->DDX_Depth(pDX, IDC_EDIT2, IDC_STATIC1, m_lDepth);
}


BEGIN_MESSAGE_MAP(CGfxPageMark, CGfxPage)
	//{{AFX_MSG_MAP(CGfxPageMark)
	ON_BN_CLICKED(IDC_BTN_ADD, OnMarkAdd)
	ON_BN_CLICKED(IDC_MARK_GOTO, OnMarkGoto)
	ON_BN_CLICKED(IDC_BTN_DEL, OnMarkDelete)
	ON_BN_CLICKED(IDC_BTN_COLOR, OnGetMarkColor)
	ON_BN_CLICKED(IDC_AUTO_MARK, OnAutoMark)
	ON_BN_CLICKED(IDC_BTN_AUTO_FR, OnAutoFR)
	ON_BN_CLICKED(IDC_BTN_CurveFlag,OnCurveFlag)
	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST1, OnKeydownList1)
	ON_NOTIFY(NM_CLICK, IDC_LIST1, OnClickList1)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST1, OnDblclkList1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGfxPageMark message handlers

void CGfxPageMark::OnMarkAdd()
{
	UpdateData();
	
	if (m_strName.IsEmpty())
	{
		return;
	}

	if (m_pGraph == NULL)
		return;

	CTrack* pTrack = (CTrack*)(m_pGraph->m_pSheet->m_TrackList.GetAt(m_nTrackNO));
	CBaseMark* pMark = new CBaseMark;
	pMark->m_nTrackNO = m_nTrackNO;
	pMark->m_bWatch = FALSE;
	pMark->m_nShape = m_cbPaintMode.GetCurSel();
	pMark->m_crColor = m_MarkColorBtn.GetColor(); 
	pMark->m_crText = m_MarkColorBtn.GetColor();

	CRect rcTrack;
	pTrack->GetRegionRect(&rcTrack, FALSE);

	pMark->m_fMarkXCoordinate = m_ptClick.x + rcTrack.left;
	pMark->m_fRawXCoordinate = pMark->m_fMarkXCoordinate;
	pTrack->m_TestMarker.m_pTrack = pTrack;
	pMark->m_pTrack = pTrack;
	pMark->m_nPointer = 0;

	CString strEdit2;
	GetDlgItemText(IDC_EDIT2, strEdit2);
	pMark->m_bDepthChange = (strEdit2.CompareNoCase(m_strName) == 0);
	
	pMark->m_strName = m_strName;

	if(pMark->m_nShape == MARKS_TD)
		pMark->m_strName = "TD";
	if(pMark->m_nShape == MARKS_CS)
		pMark->m_strName = "Casing Shoe";

	
	pMark->m_lDepth = m_lDepth;
	pMark->m_lRawDepth = m_lDepth;
	AddMarkToList(pMark);
	pTrack->m_TestMarker.AddMark(pMark);
	if (pMark->m_bDepthChange)
	{
		OnMarkGoto();
	}

	m_pGraph->Invalidate();
}

BOOL CGfxPageMark::OnInitDialog()
{
	CGfxPage::OnInitDialog();
	CString strName;
	strName.LoadString(IDS_NAME);

	m_markList.InsertColumn(0, strName);
	CString strDepth;
	strDepth.LoadString(IDS_DEPTH);
	strDepth += _T("(");
	strDepth += g_units->DUnit();
	strDepth += _T(")");
	m_markList.InsertColumn(1, strDepth);
	DWORD style = m_markList.GetExtendedStyle();
	m_markList.SetExtendedStyle(style |
				LVS_EX_FULLROWSELECT/* | LVS_EX_GRIDLINES*/);
	m_markList.SetColumnWidth(0, 150);
	m_markList.SetColumnWidth(1, 150);

	m_MarkColorBtn.EnableOtherButton("");
	m_MarkColorBtn.SetColor(0);
	m_MarkColorBtn.SetColumnsNumber(10);

	m_cbPaintMode.SetCurSel(0);

	return TRUE;
}

void CGfxPageMark::InitMark()
{
	m_markList.DeleteAllItems();
	if (m_pGraph != NULL)
	{
		CTrack* pTrack = (CTrack*)
			m_pGraph->m_pSheet->m_TrackList.GetAt(m_nTrackNO);
		int nCount = pTrack->m_TestMarker.m_MarkList.GetSize();
		for (int i = 0 ; i < nCount ; i++)
		{
			CBaseMark* pMark = (CBaseMark*) pTrack->m_TestMarker.m_MarkList.GetAt(i);
			//if (pMark->m_nTrackNO == m_nTrackNO)
			{
				AddMarkToList(pMark);
			}
		}
		m_cbPaintMode.SetCurSel(0);
		m_lDepth = m_pGraph->GetDepthFromCoordinate(m_ptClick.y);
		m_strName = g_units->FormatLM("%.4lf%s", m_lDepth);
		if (nCount > 0)
		{
			CBaseMark* pMark = (CBaseMark*)pTrack->m_TestMarker.m_MarkList.GetAt(--nCount);
			m_markColor = pMark->m_crColor;
			m_MarkColorBtn.SetColor(m_markColor);
		}
	}
	UpdateData(FALSE);
}

void CGfxPageMark::AddMarkToList(CBaseMark* pMark)
{
	CString strDepth;
	strDepth.Format("%.4lf", g_units->XDepth(pMark->m_lDepth));
	m_markList.InsertItem(0, strDepth);
	m_markList.SetItemText(0, 0, pMark->m_strName);
	CString strWatch;
	strWatch.LoadString(IDS_WATCH);
	if (pMark->m_bWatch)
	    m_markList.SetItemText(0, 1, strWatch);
	else
	    m_markList.SetItemText(0, 1, strDepth);
	m_markList.SetItemData(0, (DWORD_PTR) pMark);
}

void CGfxPageMark::OnMarkGoto()
{
	POSITION pos = m_markList.GetFirstSelectedItemPosition();
	if (pos != NULL)
	{
		int nItem = m_markList.GetNextSelectedItem(pos);
		CBaseMark* pMark = (CBaseMark*) m_markList.GetItemData(nItem);
		long nDepth = pMark->m_lDepth;
		m_pGraph->GoToDepth(nDepth - 100000);
	}
	else if (m_markList.GetItemCount())
	{
		CBaseMark* pMark = (CBaseMark*) m_markList.GetItemData(0);
		if (pMark != NULL)
		{
			long nDepth = pMark->m_lDepth;
			m_pGraph->GoToDepth(nDepth - 100000);
		}
	}
}

void CGfxPageMark::OnMarkDelete()
{
	POSITION pos = m_markList.GetFirstSelectedItemPosition();
	CTrack* pTrack = (CTrack*)m_pGraph->m_pSheet->m_TrackList.GetAt(m_nTrackNO);
	if (pos != NULL)
	{
		int nItem = m_markList.GetNextSelectedItem(pos);
		//	CString strName = m_markList.GetItemText(nItem , 0);
		//	CString strDepth = m_markList.GetItemText(nItem , 1);
		//	long lDepth = atol(strDepth);
		CBaseMark* pMark = (CBaseMark*) m_markList.GetItemData(nItem);
		//CString strName = pMark->m_strName;
		//long lDepth = pMark->m_lDepth;
		//pTrack->m_Marker.DeleteMark(strName, lDepth, m_nTrackNO);
		pTrack->m_TestMarker.DeleteMark(pMark);
		m_markList.DeleteItem(nItem);
		m_pGraph->Invalidate(TRUE);
	}
}

void CGfxPageMark::OnGetMarkColor()
{
	m_markColor = m_MarkColorBtn.GetColor();
}

//
//函数OnAutoMark()在曲线值满足条件的连续深度段内选择极值(最大或最小)，在此极值处添加标签
//
void CGfxPageMark::OnAutoMark()
{
	int nTrackNO = m_nTrackNO;
	CTrack* pTrack = (CTrack*)m_pGraph->m_pSheet->m_TrackList.GetAt(nTrackNO);
	if (!pTrack->m_vecCurve.size())
	{
		AfxMessageBox(IDS_NO_MATCH_CURVE, MB_OK | MB_ICONINFORMATION);
		return;
	}

	CUserDefinedMark dlg(this);
	// 先保存属性对话框的锁定状态
	bool bLocked = g_GfxProp.GetLockingProperty();
	// 锁定属性对话框
	g_GfxProp.LockingProperty(TRUE);
	if ((dlg.DoModal() == IDOK) && dlg.m_pCurve)
	{
		CRect rcTrack;
		pTrack->GetRegionRect(&rcTrack, FALSE);

		if (dlg.m_bWatch) // 添加监视标签
		{
			CBaseMark* pMark = new CBaseMark;

			pMark->m_bWatch = TRUE;
		    pMark->m_nShape = dlg.m_nStyle;
			pMark->m_crColor = dlg.m_clrMark;	
			pMark->m_nPointer = dlg.m_bRightLimit ? 1 : 0; //左右
			pMark->m_lDepth = 0;
			if (pMark->m_nPointer == 0)
			    pMark->m_strName.Format("%s %s %.4f", dlg.m_pCurve->m_strName, "<=", dlg.m_LeftValue);
			else
				pMark->m_strName.Format("%s %s %.4f", dlg.m_pCurve->m_strName, ">=", dlg.m_RightValue);
			pMark->m_nTrackNO = m_nTrackNO;
						                     					
			pMark->m_fXScale = pMark->m_fMarkXCoordinate /
					(float) rcTrack.Width();
            AddMarkToList(pMark);
	
			pTrack->m_TestMarker.AddMark(pMark);
			pTrack->m_TestMarker.m_pTrack = pTrack;
		}
		else
		{
			CCurve* pCurveSel = dlg.m_pCurve;
			float fXValuePerPixel = (pCurveSel->RLdVal() /
				(float) (rcTrack.Width()));

			double LeftValue, RightValue, CurrentValue;
			double MaxValue = 0, MinValue = 0;
			LeftValue = dlg.m_LeftValue;
			RightValue = dlg.m_RightValue;
			long lCurrentDepth = 0;
			int TempIndex = 0;
			for (int i = 0; i < pCurveSel->GetDataSize(); i++)
			{
				CurrentValue = pCurveSel->GetValue(i);
				BOOL bIAdded = FALSE;
				if (dlg.m_bRightLimit &&
					CurrentValue >= RightValue)
				{
					MaxValue = RightValue;
					while (CurrentValue >= RightValue &&
						i < pCurveSel->GetDataSize())
					{
						CurrentValue = pCurveSel->GetValue(i);
						if (CurrentValue > MaxValue)
						{
							MaxValue = CurrentValue;
							TempIndex = i;
						}
						i++;
						bIAdded = TRUE;
					}
					if (bIAdded)
					{
						i--;
						bIAdded = FALSE;
					}
					CBaseMark* pMark = new CBaseMark;
					lCurrentDepth = pCurveSel->GetDepth(TempIndex);
					pMark->m_nShape = dlg.m_nStyle;
					pMark->m_bWatch = dlg.m_bWatch;
					pMark->m_crColor = dlg.m_clrMark;	
					pMark->m_lDepth = lCurrentDepth;
					pMark->m_strName = g_units->FormatLM("%.4lf%s", lCurrentDepth);
					pMark->m_nTrackNO = m_nTrackNO;
					pMark->m_nPointer = 0;
					pMark->m_bDepthChange = TRUE;
					pMark->m_pTrack = pTrack;	
					
					pMark->m_fMarkXCoordinate = pCurveSel->GetCurveXPos(lCurrentDepth);
						                        
										
					pMark->m_fXScale = pMark->m_fMarkXCoordinate /
						(float) rcTrack.Width();
					AddMarkToList(pMark);
					pTrack->m_TestMarker.AddMark(pMark);
					pTrack->m_TestMarker.m_pTrack = pTrack;
				}
				if (!dlg.m_bRightLimit &&
					CurrentValue <= LeftValue)
				{
					MinValue = LeftValue;
					while (CurrentValue <= LeftValue &&
						i < pCurveSel->GetDataSize())
					{
						CurrentValue = pCurveSel->GetValue(i);
						if (CurrentValue < MinValue)
						{
							MinValue = CurrentValue;
							TempIndex = i;
						}
						i++;
						bIAdded = TRUE;
					}
					if (bIAdded)
					{
						i--;
						bIAdded = FALSE;
					}
					CBaseMark* pMark = new CBaseMark;
					lCurrentDepth = pCurveSel->GetDepth(TempIndex);
					pMark->m_nShape = dlg.m_nStyle;
					pMark->m_crColor = dlg.m_clrMark;	
					pMark->m_bWatch = dlg.m_bWatch;
					pMark->m_lDepth = lCurrentDepth;
					pMark->m_strName = g_units->FormatLM("%.4lf%s", lCurrentDepth);
					pMark->m_nTrackNO = m_nTrackNO;
					pMark->m_nPointer = 1;
					pMark->m_bDepthChange = TRUE;
					pMark->m_pTrack = pTrack;
					
					pMark->m_fMarkXCoordinate = pCurveSel->GetCurveXPos(lCurrentDepth);
						                        
					pMark->m_fXScale = pMark->m_fMarkXCoordinate /
						(float) rcTrack.Width();
					AddMarkToList(pMark);
					pTrack->m_TestMarker.AddMark(pMark);
					pTrack->m_TestMarker.m_pTrack = pTrack;
				}
			}
		}

		m_pGraph->Invalidate(TRUE);
		m_markList.RedrawItems(0, m_markList.GetItemCount());
	}
	g_GfxProp.LockingProperty(bLocked);
}

void CGfxPageMark::OnItemSelChanged()
{
	POSITION pos = m_markList.GetFirstSelectedItemPosition();
	if (pos != NULL)
	{
		int nItem = m_markList.GetNextSelectedItem(pos);
		CBaseMark* pMark = (CBaseMark*)m_markList.GetItemData(nItem);
		m_cbPaintMode.SetCurSel(pMark->m_nShape);
		m_MarkColorBtn.SetColor(pMark->m_crColor);
		if (pMark->m_bWatch)
			GetDlgItem(IDC_MARK_GOTO)->EnableWindow(FALSE);
		else
			GetDlgItem(IDC_MARK_GOTO)->EnableWindow(TRUE);

		m_strName = pMark->m_strName;
		m_lDepth = pMark->m_lDepth;
		UpdateData(FALSE);
	}
}

void CGfxPageMark::OnKeydownList1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_KEYDOWN* pLVKeyDow = (LV_KEYDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
	if (pLVKeyDow->wVKey == 46)  // 按下del键
	{
		CTrack* pTrack = (CTrack*)m_pGraph->m_pSheet->m_TrackList.GetAt(m_nTrackNO);
		POSITION pos = m_markList.GetFirstSelectedItemPosition();
		for (; pos != NULL; )
		{
			int nItem = m_markList.GetNextSelectedItem(pos);
			CBaseMark* pMark = (CBaseMark*)m_markList.GetItemData(nItem);
			pTrack->m_TestMarker.DeleteMark(pMark);
			m_markList.DeleteItem(nItem);
		}
		m_pGraph->Invalidate(TRUE);
	}
	else if (pLVKeyDow->wVKey == 40 || pLVKeyDow->wVKey == 38)
	{
		OnItemSelChanged();
	}
	*pResult = 0;
}

void CGfxPageMark::OnClickList1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnItemSelChanged();
	
	*pResult = 0;
}

void CGfxPageMark::RefreshMark()
{
	UpdateData(TRUE);
	POSITION pos = m_markList.GetFirstSelectedItemPosition();
	if (pos != NULL)
	{
		int nItem = m_markList.GetNextSelectedItem(pos);
		CBaseMark* pMark = (CBaseMark*)m_markList.GetItemData(nItem);
		pMark->m_strName = m_strName;
		pMark->m_lDepth = m_lDepth;
		pMark->m_nShape = m_cbPaintMode.GetCurSel();
		pMark->m_crColor = m_MarkColorBtn.GetColor();
	}
}

void CGfxPageMark::OnDblclkList1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	*pResult = 0;
}
/***函数：OnCurveFlag
****功能：曲线属性框新增曲线标识按钮相应函数
****作者：add by xwh
****创建时间：2013-9-3
****修改记录：无
****/
void CGfxPageMark::OnCurveFlag()
{
	CSheet *pSheet = m_pGraph->m_pSheet;
	CCurve *pDCurve = pSheet->GetDepthCurve();
	if (pDCurve == NULL)
	    return;
	long lSD = pDCurve->GetDepth(0);
	long lED = pDCurve->GetDepthED ();
	long lBottomDepth = max(lSD,lED);
	int i = 0;
//	CBaseMark *pTD = NULL;
	for(i = 0 ; i < pSheet->m_TrackList.GetSize() ; i++)
	{
		CTrack *pTrack = (CTrack *)pSheet->m_TrackList.GetAt(i);

		for(int j = pTrack->m_TestMarker.m_MarkList.GetSize() - 1 ; j >=0 ; j--)
		{
			CBaseMark *pMark = (CBaseMark *)pTrack->m_TestMarker.m_MarkList.GetAt(j);

			CString strMarkName = pMark->m_strName;
			strMarkName.MakeUpper();
			strMarkName = strMarkName.Left(2);
			if(pMark->m_nShape == MARK_Curve)
			{
				delete pMark;
				pTrack->m_TestMarker.m_MarkList.RemoveAt(j);
				if(i == m_nTrackNO)
					m_markList.DeleteItem(j);
			}
		}
	}


	for( i = 0 ; i < pSheet->m_TrackList.GetSize() ; i++)
	{
		CTrack *pTrack = (CTrack *)pSheet->m_TrackList.GetAt(i);

		for(int j = 0 ; j < pTrack->m_vecCurve.size() ; j++)
		{
			CCurve *pCurve =  pTrack->m_vecCurve.at(j);

			if(!pCurve->IsVisible())
				continue;

			if(pCurve->FRDepth() < 0)
				continue;
			
			if(pCurve->m_strName != "DEPT" && pCurve->m_strName != "DEPTH" && pCurve->m_strName != "TIME")
			{
				CBaseMark *pMark = new CBaseMark;

				pMark->m_nTrackNO = i;
				pMark->m_bWatch = FALSE;
				pMark->m_nShape = MARK_Curve;
				pMark->m_crColor = pCurve->m_crColor; 
				pMark->m_crText = pCurve->m_crColor;

				pTrack->m_TestMarker.m_pTrack = pTrack;
				pMark->m_pTrack = pTrack;
				pMark->m_nPointer = 0;
				pMark->m_bDepthChange = FALSE;
				pMark->m_strName = pCurve->m_strName;

				int nPos = pMark->m_strName.Find('(');
				if(nPos >= 0)
				{
					pMark->m_strName = pMark->m_strName.Right(pMark->m_strName.GetLength() - nPos - 1);
					nPos = pMark->m_strName.Find(')');
					if(nPos >= 0)
						pMark->m_strName = pMark->m_strName.Left(nPos);
				}

				nPos = pMark->m_strName.Find('\\');
				if(nPos >= 0)
					pMark->m_strName = pMark->m_strName.Left(nPos);

				pMark->m_lDepth = pCurve->FRDepth() /*- lOffset*/;

				double fValue = pCurve->GetdblValue(0);
				
				int nLeftMargin = pCurve->LeftMargin(false);	// 左格子宽度
				int nRightMargin = pCurve->RightMargin(false);	// 右格子宽度
				int nMargin = nRightMargin - nLeftMargin;
				int nXCoordinate = 0;
				if(pTrack->m_GridInfo.vLineInfo.nGridType == 0) //线性道
				{
					double fXValuePerPixel = pCurve->RLdVal()/(float)(nRightMargin - nLeftMargin);
					nXCoordinate = (double)((fValue - pCurve->LeftVal()) / fXValuePerPixel);
				}
				else
				{
					double fLeftValue = pCurve->LeftVal();
					double fRightValue = pCurve->RightVal();
					if (fLeftValue <= 0) 
						fLeftValue = 0.001;
					if (fRightValue <= fLeftValue)
						fRightValue = fLeftValue + 0.001; 
					
					double fTemp = fLeftValue;
					while (fTemp < fRightValue)
						fTemp = fTemp*10;

					double fXValuePerPixel = (log10(fTemp) - log10(fLeftValue)) / (float)(nRightMargin - nLeftMargin);
					nXCoordinate = ((log10(fValue) - log10(fLeftValue)) / fXValuePerPixel);
				}
						
				pMark->m_fMarkXCoordinate = (long)(nXCoordinate) % nMargin;
				if(pMark->m_fMarkXCoordinate < 0)
					pMark->m_fMarkXCoordinate += nMargin;
				pMark->m_fMarkXCoordinate += nLeftMargin;
				pMark->m_fRawXCoordinate = pMark->m_fMarkXCoordinate;
			
			//	pMark->m_fLeftMargin = nLeftMargin;
			//	pMark->m_fRightMargin = nRightMargin;
				pMark->m_fLeftMargin = pTrack->RegionLeft ();
				pMark->m_fRightMargin = pTrack->RegionRight();

				if(i == m_nTrackNO)
					AddMarkToList(pMark);
				pTrack->m_TestMarker.AddMark(pMark);
			}
		}
	}
	m_pGraph->Invalidate();
}
void CGfxPageMark::OnAutoFR()
{
	CSheet *pSheet = m_pGraph->m_pSheet;
	CCurve *pDCurve = pSheet->GetDepthCurve();
	if (pDCurve == NULL)
	    return;
	long lSD = pDCurve->GetDepth(0);
	long lED = pDCurve->GetDepthED ();
	long lBottomDepth = max(lSD,lED);
	int i = 0;
	CBaseMark *pTD = NULL;
	for(i = 0 ; i < pSheet->m_TrackList.GetSize() ; i++)
	{
		CTrack *pTrack = (CTrack *)pSheet->m_TrackList.GetAt(i);

		for(int j = pTrack->m_TestMarker.m_MarkList.GetSize() - 1 ; j >=0 ; j--)
		{
			CBaseMark *pMark = (CBaseMark *)pTrack->m_TestMarker.m_MarkList.GetAt(j);

			CString strMarkName = pMark->m_strName;
			strMarkName.MakeUpper();
			strMarkName = strMarkName.Left(2);
			if(strMarkName == "TD")
			{
				pTD = pMark;
			}

			if(pMark->m_nShape == MARKS_FR)
			{
				delete pMark;
				pTrack->m_TestMarker.m_MarkList.RemoveAt(j);
				if(i == m_nTrackNO)
					m_markList.DeleteItem(j);
			}
		}
	}

	if(pTD == NULL)
		return;
	long lOffset = lBottomDepth - pTD->m_lDepth;

	for( i = 0 ; i < pSheet->m_TrackList.GetSize() ; i++)
	{
		CTrack *pTrack = (CTrack *)pSheet->m_TrackList.GetAt(i);

		for(int j = 0 ; j < pTrack->m_vecCurve.size() ; j++)
		{
			CCurve *pCurve =  pTrack->m_vecCurve.at(j);

			if(!pCurve->IsVisible())
				continue;

			if(pCurve->FRDepth() < 0)
				continue;
			
			if(pCurve->m_strName != "DEPT" && pCurve->m_strName != "DEPTH" && pCurve->m_strName != "TIME")
			{
				CBaseMark *pFRMark = new CBaseMark;

				pFRMark->m_nTrackNO = i;
				pFRMark->m_bWatch = FALSE;
				pFRMark->m_nShape = MARKS_FR;
				pFRMark->m_crColor = pCurve->m_crColor; 
				pFRMark->m_crText = pCurve->m_crColor;

				pTrack->m_TestMarker.m_pTrack = pTrack;
				pFRMark->m_pTrack = pTrack;
				pFRMark->m_nPointer = 0;
				pFRMark->m_bDepthChange = FALSE;
				pFRMark->m_strName = pCurve->m_strName;

				int nPos = pFRMark->m_strName.Find('(');
				if(nPos >= 0)
				{
					pFRMark->m_strName = pFRMark->m_strName.Right(pFRMark->m_strName.GetLength() - nPos - 1);
					nPos = pFRMark->m_strName.Find(')');
					if(nPos >= 0)
						pFRMark->m_strName = pFRMark->m_strName.Left(nPos);
				}

				nPos = pFRMark->m_strName.Find('\\');
				if(nPos >= 0)
					pFRMark->m_strName = pFRMark->m_strName.Left(nPos);

				

				pFRMark->m_lDepth = pCurve->FRDepth() /*- lOffset*/;
				pFRMark->m_lRawDepth = pFRMark->m_lDepth;

				int nIndex = pCurve->GetFirstIndexByDepth(pFRMark->m_lDepth);
				if(nIndex < 0)
					continue;

				double fValue = pCurve->GetdblValue(nIndex);
				
				
				
				int nLeftMargin = pCurve->LeftMargin(false);	// 左格子宽度
				int nRightMargin = pCurve->RightMargin(false);	// 右格子宽度
				int nMargin = nRightMargin - nLeftMargin;
				int nXCoordinate = 0;
				if(pTrack->m_GridInfo.vLineInfo.nGridType == 0) //线性道
				{
					double fXValuePerPixel = pCurve->RLdVal()/(float)(nRightMargin - nLeftMargin);
					nXCoordinate = (double)((fValue - pCurve->LeftVal()) / fXValuePerPixel);
				}
				else
				{
					double fLeftValue = pCurve->LeftVal();
					double fRightValue = pCurve->RightVal();
					if (fLeftValue <= 0) 
						fLeftValue = 0.001;
					if (fRightValue <= fLeftValue)
						fRightValue = fLeftValue + 0.001; 
					
					double fTemp = fLeftValue;
					while (fTemp < fRightValue)
						fTemp = fTemp*10;

					double fXValuePerPixel = (log10(fTemp) - log10(fLeftValue)) / (float)(nRightMargin - nLeftMargin);
					nXCoordinate = ((log10(fValue) - log10(fLeftValue)) / fXValuePerPixel);
				}
				
				pFRMark->m_fMarkXCoordinate = (long)(nXCoordinate) % nMargin;
				if(pFRMark->m_fMarkXCoordinate < 0)
					pFRMark->m_fMarkXCoordinate += nMargin;
				pFRMark->m_fMarkXCoordinate += nLeftMargin;
				pFRMark->m_fMarkXCoordinate /=pTrack->m_fRatioX;
				pFRMark->m_fRawXCoordinate = pFRMark->m_fMarkXCoordinate;
			
			//	pFRMark->m_fLeftMargin = nLeftMargin;
			//	pFRMark->m_fRightMargin = nRightMargin;
				pFRMark->m_fLeftMargin = pTrack->RegionLeft ();
				pFRMark->m_fRightMargin = pTrack->RegionRight();

				if(i == m_nTrackNO)
					AddMarkToList(pFRMark);

				pTrack->m_TestMarker.AddMark(pFRMark);
			}
		}
	}

	m_pGraph->Invalidate();
}

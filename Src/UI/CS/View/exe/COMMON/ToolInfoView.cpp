// ToolInfoView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "ToolInfoView.h"
#include "MyMEMDC.h"
#include "comconfig.h"
#include "ultool.h"
#include "units.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CToolInfoView

IMPLEMENT_DYNCREATE(CToolInfoView, CULView)
extern CUnits* g_units;
CToolInfoView::CToolInfoView(LPVOID pTools /* = NULL */)
{
	m_pArrTools = (CPtrArray*)pTools;
	m_pEdit = &m_Cell;
}

CToolInfoView::~CToolInfoView()
{
	Clear();
}


BEGIN_MESSAGE_MAP(CToolInfoView, CULView)
	//{{AFX_MSG_MAP(CToolInfoView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CToolInfoView drawing

void CToolInfoView::OnDraw(CDC* pDC)
{
	// TODO: add draw code here
	CRect rect;
	GetClientRect(rect);
	pDC->DPtoLP(rect);

	if (m_nRefresh)
	{
		PrepareCell(&m_Cell, pDC, rect);
		SetScrollInformation();
		m_nRefresh = 0;
	}

	CMyMemDC memDC(pDC, &rect);
	DrawPage(&memDC, rect);
}

void CToolInfoView::Clear ()
{
	for(int i = 0; i < m_ToolInfoList.GetSize (); i++)
	{
		TOOL *pInfo = (TOOL *)m_ToolInfoList.GetAt (i);
		delete pInfo;
	}
	m_ToolInfoList.RemoveAll ();
}

void CToolInfoView::InitToolInfo ()
{
	Clear();

	for (int i = 0; i < m_pArrTools->GetSize(); i++)
	{
		CULTool* pULTool = (CULTool*)m_pArrTools->GetAt(i);

		TOOL *pInfo = new TOOL;
		pULTool->GetToolProp(pInfo); 
		m_ToolInfoList.Add(pInfo);
	}
}

int CToolInfoView::CalcPageHeight()
{
	if(NULL == m_Cell.m_hWnd)
		return CULView::CalcPageHeight();
	return (int)GetCellSize(&m_Cell).cy + 2*PAGE_MARGIN;
}

int CToolInfoView::CalcPrintHeight()
{
	return CalcPageHeight();
}
/////////////////////////////////////////////////////////////////////////////
// CToolInfoView diagnostics

#ifdef _DEBUG
void CToolInfoView::AssertValid() const
{
	CULView::AssertValid();
}

void CToolInfoView::Dump(CDumpContext& dc) const
{
	CULView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CToolInfoView message handlers

int CToolInfoView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CULView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	if (!CreateCell(&m_Cell, this))
		return -1;
	
	OpenCellFile();
	
	return 0;
}

void CToolInfoView::Print()
{
	//Modify by xx 直接调用父类函数 20161220
	CULView::Print();
	//PrintCell(&m_Cell);


// 	if (m_Cell.GetSafeHwnd() == NULL)
// 	{
// 		TRACE("Warning : Load child frame failed!\n");
// 		return ;
// 	}
// 
// 	PrintCell(&m_Cell);
}
void CToolInfoView::OpenCellFile()
{
	// InitToolInfo();
	SetScrollInformation();
	InitCellData();
}

void CToolInfoView::OpenCellFile(CCell2000 *pCell)
{
	// InitToolInfo();
	InitCellData(pCell);
	if (pCell != &m_Cell)
		InitCellData(&m_Cell);
}

//extern CComConfig AfxGetComConfig();
void CToolInfoView::InitCellData(CCell2000* pCell)
{
	InitToolInfo();

	CCell2000 *pWorkCell = NULL;
	if (pCell != NULL)
		pWorkCell = pCell;
	else
		pWorkCell = &m_Cell;

	LOGFONT lf;
	afxGlobalData.fontRegular.GetLogFont(&lf);
	long para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftR].lfFaceName, 1);
	pWorkCell->SetDefaultFont (para,AfxGetComConfig()->m_Watch.FFont[ftR].cy+1);//AfxGetComConfig()->m_Watch.FontSize [2] );//12);

	pWorkCell->ShowHScroll(0, 0);   // 不显示水平滚动条
	pWorkCell->ShowVScroll(0, 0);   // 不显示竖直滚动条
	pWorkCell->ShowTopLabel(0, 0);  // 不显示列标
	pWorkCell->ShowSideLabel(0, 0); // 不显示行标
	pWorkCell->ShowSheetLabel(0, 0);// 隐藏页签
	pWorkCell->SetWorkbookReadonly(TRUE); //设为只读
	pWorkCell->SetAllowSizeColInGrid (FALSE);//不允许调整列宽
	pWorkCell->SetAllowSizeRowInGrid (FALSE);//不允许调整行宽
//	pWorkCell->PrintPageBreak(1);
	pWorkCell->ShowPageBreak(0); //不显示打印分页线
	pWorkCell->ShowGridLine(0, 0); //不显示格线		
	
	para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftT1].lfFaceName, 1);
	pWorkCell->SetCellFont(2, 1, 0, para);
	pWorkCell->SetCellFontSize(2, 1, 0, AfxGetComConfig()->m_Watch.FFont[ftT1].cy);
//	pWorkCell->SetCellFontStyle(2, 1, 0, para);
	
	int nPos = 3;
	int nRows = m_ToolInfoList.GetSize() + nPos + 4;
	pWorkCell->SetRowHeight (0, CELL_NULL_HEIGHT, nPos, 0);
	pWorkCell->SetRowHeight (0, CELL_NULL_HEIGHT, nRows-3, 0);
	pWorkCell->SetRows (nRows, 0);
	pWorkCell->SetCols(9,0);
	pWorkCell->SetColWidth(0,0.5*PAGE_MARGIN,1,0);
	pWorkCell->SetColWidth(0,8*PAGE_MARGIN,3,0);
	pWorkCell->SetColWidth(0,10*PAGE_MARGIN,7,0);
	pWorkCell->SetColWidth(0,0.5*PAGE_MARGIN,8,0);
	

	para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftT1].lfFaceName, 1);
	pWorkCell->SetCellFont(3, 1, 0, para);
	pWorkCell->SetCellFontSize(3, 1, 0, AfxGetComConfig()->m_Watch.FFont[ftT1].cy);
//	pWorkCell->SetCellFontStyle(3, 1, 0, para);

	//绘制标题
	pWorkCell->MergeCells(2,1,7,3);
	CString str;
	str.LoadString(IDS_CTOOLINFOVIEW_TOOL_INFO);//Tool Informations
	pWorkCell->SetCellString(2,1,0,str);
	pWorkCell->SetCellAlign(2,1,0,32+4);

	//绘制列名
//	para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftT2].lfFaceName, 1);
//	for(int i=2; i<10; i++)
	{
	//	pWorkCell->SetCellFont(i, 4, 0, para);
//		pWorkCell->SetCellFontSize(i, 4, 0, 11);
	//	pWorkCell->SetCellFontStyle(i, 4, 0, 2);
	}
	

	nPos++;
	str.LoadString(IDS_TOOLINFO_MNEMONIC);
//	pWorkCell->SetCellAlign(2,nPos,0,32+4);
	
	para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftT2].lfFaceName, 1);
	pWorkCell->SetCellFont(2, nPos, 0, para);
	pWorkCell->SetCellFontSize(2, nPos, 0, AfxGetComConfig()->m_Watch.FFont[ftT2].cy);
//	pWorkCell->SetCellFontStyle(2, nPos, 0, para);
	pWorkCell->SetCellString(2,nPos,0,str);
	str.LoadString(IDS_TOOLINFO_NAME);
//	pWorkCell->SetCellAlign(3,nPos,0,32+4);
	para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftT2].lfFaceName, 1);
	pWorkCell->SetCellFont(3, nPos, 0, para);
	pWorkCell->SetCellFontSize(3, nPos, 0, AfxGetComConfig()->m_Watch.FFont[ftT2].cy);
//	pWorkCell->SetCellFontStyle(3, nPos, 0, para);
	pWorkCell->SetCellString(3,nPos,0,str);
	
	str.LoadString(IDS_TOOLINFO_NUMBER);
//	pWorkCell->SetCellAlign(4,nPos,0,32+4);
	para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftT2].lfFaceName, 1);
	pWorkCell->SetCellFont(4, nPos, 0, para);
	pWorkCell->SetCellFontSize(4, nPos, 0, AfxGetComConfig()->m_Watch.FFont[ftT2].cy);
//	pWorkCell->SetCellFontStyle(4, nPos, 0, para);
	pWorkCell->SetCellString(4,nPos,0,str);
	
	str.LoadString(IDS_TOOLINFO_WEIGHT);
	para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftT2].lfFaceName, 1);
	pWorkCell->SetCellFont(5, nPos, 0, para);
	pWorkCell->SetCellFontSize(5, nPos, 0, AfxGetComConfig()->m_Watch.FFont[ftT2].cy);
//	pWorkCell->SetCellFontStyle(5, nPos, 0, para);
	pWorkCell->SetCellAlign(5,nPos,0,2);
	CString strInfo;
//	strInfo = g_units->XUnit(_T("Mass"), _T("Weight"));
	strInfo = _T("kg");
	str	=str +_T("(")+ strInfo+_T(")");
	pWorkCell->SetCellString(5,nPos,0,str);

	str.LoadString(IDS_TOOLINFO_LENGTH);
	para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftT2].lfFaceName, 1);
	pWorkCell->SetCellFont(6, nPos, 0, para);
	pWorkCell->SetCellFontSize(6, nPos, 0, AfxGetComConfig()->m_Watch.FFont[ftT2].cy);
//	pWorkCell->SetCellFontStyle(6, nPos, 0, para);
	pWorkCell->SetCellAlign(6,nPos,0,2);
	//strInfo = g_units->XUnit(_T("Length"), _T("Length"));
	strInfo = _T("m");
	str	=str +_T("(") +strInfo+_T(")");
	pWorkCell->SetCellString(6,nPos,0,str);

	str.LoadString(IDS_TOOLINFO_UPDELAY);
	para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftT2].lfFaceName, 1);
	pWorkCell->SetCellFont(7, nPos, 0, para);
	pWorkCell->SetCellFontSize(7, nPos, 0, AfxGetComConfig()->m_Watch.FFont[ftT2].cy);
//	pWorkCell->SetCellFontStyle(7, nPos, 0, para);
	pWorkCell->SetCellAlign(7,nPos,0,2);
	str	=str +_T("(") +strInfo+_T(")");
	pWorkCell->SetCellString(7,nPos,0,str);

// 	str.LoadString(IDS_TOOLINFO_MAXLOGGING);
// 	pWorkCell->SetCellAlign(8,nPos,0,2);
// 	para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftT2].lfFaceName, 1);
// 	pWorkCell->SetCellFont(8, nPos, 0, para);
// 	pWorkCell->SetCellFontSize(8, nPos, 0, AfxGetComConfig()->m_Watch.FFont[ftT2].cy);
// //	pWorkCell->SetCellFontStyle(8, nPos, 0, para);
// //	strInfo = g_units->XUnit(_T("Velocity"), _T("Speed"));
// 	strInfo = _T("m/h");
// 	str	=str +_T("(") +strInfo+_T(")");
// 	pWorkCell->SetCellString(8,nPos,0,str);

	long lTotolLen = 0;
	double fTotolWeight = 0;
	int i = 0;
	for(i = 0; i < m_ToolInfoList.GetSize (); i++)
	{
		TOOL *pInfo = (TOOL *)m_ToolInfoList.GetAt (i);
		lTotolLen += pInfo->lLength;
		fTotolWeight += pInfo->fWeight;
	}
	
	long lUpDelay = lTotolLen;
	for(i = 0; i < m_ToolInfoList.GetSize (); i++)
	{
		TOOL *pInfo = (TOOL *)m_ToolInfoList.GetAt (i);
		//if(pInfo->strToolName=="DEPTH")
			//continue;
		if(pInfo->strToolName=="DEPTH")
			continue;
		nPos++;
		lUpDelay -= pInfo->lLength;

		pWorkCell->SetRowHeight(1,20,nPos,0);
		
		pWorkCell->SetCellString(2,nPos,0,pInfo->strToolName);	// Tool Mnemonic
		pWorkCell->SetCellString(3,nPos,0,pInfo->strENName);	// Tool Name
		pWorkCell->SetCellString(4,nPos,0,pInfo->strSN);	    // Serial Number

		pWorkCell->SetCellAlign(5,nPos,0,2);
		pWorkCell->SetCellDouble(5,nPos,0,pInfo->fWeight);	    // Weight
		pWorkCell->SetCellAlign(6,nPos,0,2);
		pWorkCell->SetCellDouble(6,nPos,0,DTM(pInfo->lLength));	    // Length
		pWorkCell->SetCellAlign(7,nPos,0,2);
		pWorkCell->SetCellDouble(7,nPos,0,DTM(lUpDelay));	        // Up Delay
// 		pWorkCell->SetCellAlign(8,nPos,0,2);
// 		pWorkCell->SetCellNumType(8,nPos,0,1);
// 		pWorkCell->SetCellDigital(8,nPos,0,3);
// 		pWorkCell->SetCellDouble(8,nPos,0,(pInfo->fMaxSpeed)*3600.0f);	// Max Logging
	}
     //pWorkCell->DeleteRow(nPos+1,1,0);
	//pWorkCell->DeleteRow(nPos+1,1,0);
	pWorkCell->DrawGridLine(1,nPos+1,9,nPos+1,5,2,0);
	//绘制页尾
	nPos += 2;
	pWorkCell->SetCellAlign(2,nPos,0,32);
	str.LoadString(IDS_CTOOLINFOVIEW_TOTAL);//Total
	pWorkCell->SetCellString(2,nPos,0,str);
	pWorkCell->SetCellAlign(5,nPos,0,2+32);
	pWorkCell->SetCellDouble(5,nPos,0,fTotolWeight);
	pWorkCell->SetCellAlign(6,nPos,0,2+32);
	pWorkCell->SetCellDouble(6,nPos,0,DTM(lTotolLen));

	nPos++;
	pWorkCell->MergeCells(2,nPos,5,nPos);
	pWorkCell->MergeCells(6,nPos,7,nPos);
	pWorkCell->SetCellAlign(2,nPos,0,32);
	str.LoadString(IDS_CTOOLINFOVIEW_DATA);//Data:RUN1
	pWorkCell->SetCellString(2,nPos,0,str);
	str.LoadString(IDS_CTOOLINFOVIEW_DATE);//Date:
	CTime time;
	time = CTime::GetCurrentTime();
	str += time.Format(_T("%Y-%m-%d %H:%M:%S"));
	pWorkCell->SetCellAlign(6,nPos,0,2+32);
	pWorkCell->SetCellString(6,nPos,0,str);

	pWorkCell->DrawGridLine (1, 4, 8, 4, 1,  2, 0);	
	pWorkCell->DrawGridLine (1, 1, 8, 3, 1,  3, 0);
	pWorkCell->DrawGridLine (1, 1, 8, nRows-3, 1,  2, 0);
	pWorkCell->DrawGridLine (1, 1, 8, nRows, 1,  3, 0);
	pWorkCell->DrawGridLine (1, nRows, 8, nRows, 1,  3, 0);
  

}

void CToolInfoView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: Add your specialized code here and/or call the base class
	InitToolInfo();
	InitCellData();	
	m_nRefresh = 1;
	Invalidate();
}

void CToolInfoView::SaveAsBitmap(CDC* pDC, LPCTSTR pszFile)
{
	SaveCellAsBitmap(pDC, pszFile, &m_Cell);
}


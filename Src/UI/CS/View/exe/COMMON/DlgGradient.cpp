// DlgGradient.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "DlgGradient.h"
#include "XMLSettings.h"
#include "MyFileDialog.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgGradient dialog


CDlgGradient::CDlgGradient(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgGradient::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgGradient)
	m_nLocat = 0;
	m_nColorR = 0;
	m_nColorG = 0;
	m_nColorB = 0;
	//}}AFX_DATA_INIT
	m_bSet = TRUE;
}


void CDlgGradient::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgGradient)
	DDX_Control(pDX, IDC_LIST1, m_lstGradient);
	DDX_Control(pDX, IDC_STATIC1, m_stc1);
	DDX_Control(pDX, IDC_BTN_COLOR, m_btnColor);
	DDX_Text(pDX, IDC_EDIT2, m_nLocat);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgGradient, CDialog)
	//{{AFX_MSG_MAP(CDlgGradient)
	ON_BN_CLICKED(IDC_BTN_DELETE, OnBtnDelete)
	ON_BN_CLICKED(IDC_BTN_COLOR, OnBtnColor)
	ON_LBN_SELCHANGE(IDC_LIST1, OnSelchangeList1)
	ON_BN_CLICKED(IDC_BTN_NEW, OnBtnNew)
	ON_EN_CHANGE(IDC_EDIT2, OnChangeEdit2)
	ON_BN_CLICKED(IDC_BTN_DEL, OnBtnDel)
	ON_BN_CLICKED(IDC_BTN_LOAD, OnBtnLoad)
	ON_BN_CLICKED(IDC_BTN_SAVEAS, OnBtnSaveas)
	ON_BN_CLICKED(IDC_BTN_ADD,OnAdd)
	//}}AFX_MSG_MAP
	ON_MESSAGE(UM_SELECTCHANGED, OnSelectedChanged)
	ON_MESSAGE(UM_POSCHANGED, OnPosChanged)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgGradient message handlers

void CDlgGradient::OnOK() 
{
	SaveTo(m_strTables);
	CDialog::OnOK();
}

BOOL CDlgGradient::SaveTo(LPCTSTR pszFile)
{
	CXMLSettings xml(FALSE, _T("ColorTables"));
	for (int i = 0; i < m_lstGradient.GetCount(); i++)
	{
		CString strKey;
		m_lstGradient.GetText(i, strKey);
		if (xml.CreateKey(strKey))
		{
			CGradientItem* pGradient = (CGradientItem*)m_lstGradient.GetItemData(i);
			xml.Write(_T("Location"), pGradient->tblLocat);
			int nMaxSize = pGradient->tblColor.GetSize();
			xml.Write(_T("ColorSize"),nMaxSize);
			CString str,strColor;
			for(int i = 0;i<nMaxSize;i++)
			{
				str.Format("ColorIndex %d",i);
				strColor.Format("%06x",pGradient->tblColor[i]);
				xml.Write(str,strColor);
			}
		//	xml.Write(_T("ColorTable"), pGradient->tblColor);
			xml.Back();
		}
	}
	return xml.WriteXMLToFile(pszFile);
}

BOOL CDlgGradient::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CRect rect;
	m_stc1.GetWindowRect(&rect);
	ScreenToClient(&rect);
	rect.right = rect.left + 256 + 12;
	m_stcGradient.Create(NULL, NULL, WS_CHILD|WS_VISIBLE, rect, this, 11);

	m_btnColor.SetColumnsNumber(10);
	m_btnColor.EnableOtherButton(_T("More..."));

	// Search gradients from colortable.xml
	TCHAR szPath[_MAX_PATH];
	GetModuleFileName(GetModuleHandle(AfxGetAppName()), szPath, _MAX_PATH);
	CString strPath = szPath;
	//应该保存在config目录下
	strPath = strPath.Left(strPath.ReverseFind('\\'));
	strPath = strPath.Left(strPath.ReverseFind('\\'));
//	m_strTables = strPath + _T("\\colortables.xml");
	m_strTables = strPath +_T("\\Config\\colortables.xml");
	LoadFrom(m_strTables);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CDlgGradient::LoadFrom(LPCTSTR pszFile)
{
	CXMLSettings xml;
	if (xml.ReadXMLFromFile(pszFile))
	{
		m_lstGradient.ResetContent();

		CXMLNode* pTree = xml.GetTree();
		POSITION pos = pTree->m_lstChildren.GetHeadPosition();
		for ( ; pos != NULL; )
		{
			CXMLNode* pNode = pTree->m_lstChildren.GetNext(pos);
			if (xml.Open(pNode->m_strName))
			{
				CDWordArray tblLocat, tblColor;
				xml.Read(_T("Location"), tblLocat);
				//xml.Read(_T("ColorTable"), tblColor);
				int nMaxSize;
				xml.Read(_T("ColorSize"),nMaxSize);
				CString str,strColor;
				for(int i = 0;i<nMaxSize;i++)
				{
					str.Format("ColorIndex  %d",i);
					xml.Read(str,strColor);
					LPTSTR lp = strColor.GetBuffer(12);
					lstrcpy(lp,strColor);
					tblColor.Add(AnsiHex2DWORD(lp)) ;
					strColor.ReleaseBuffer();
					
				}
				m_lstGradient.AddGradient(pNode->m_strName, tblLocat, tblColor);
				xml.Back();
			}
		}
		return TRUE;
	}
	return FALSE;
}

void CDlgGradient::OnBtnDelete() 
{
	m_stcGradient.DeleteSelected();
	m_btnColor.EnableWindow(FALSE);
	GetDlgItem(IDC_EDIT2)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_DELETE)->EnableWindow(FALSE);
}

void CDlgGradient::OnBtnColor() 
{
	CStopCtrl* pStop = m_stcGradient.GetSelected();
	if (pStop != NULL)
	{
		pStop->m_clrFace = m_btnColor.GetColor();
// 		m_nColorB = (pStop->m_clrFace)>>16;
// 		m_nColorG = ((pStop->m_clrFace)>>8)&0xff;
// 		m_nColorR = (pStop->m_clrFace)&0xff;
// 		SetDlgItemInt(IDC_EDIT_ColorR,m_nColorR);
// 		SetDlgItemInt(IDC_EDIT_ColorG,m_nColorG);
// 		SetDlgItemInt(IDC_EDIT_ColorB,m_nColorB);
		m_stcGradient.Invalidate();
	}
}

//void CDlgGradient::OnSelectedChanged(WPARAM wp)
LRESULT CDlgGradient::OnSelectedChanged(WPARAM wp, LPARAM lp)
{
	CStopCtrl* pStop = m_stcGradient.GetSelected();
	if (pStop != NULL)
	{
		if (!m_btnColor.IsWindowEnabled())
		{
			m_btnColor.EnableWindow();
			GetDlgItem(IDC_EDIT2)->EnableWindow();
			GetDlgItem(IDC_BTN_DELETE)->EnableWindow();
		}

		m_btnColor.SetColor(pStop->m_clrFace);
// 		m_nColorB = (pStop->m_clrFace)>>16;
// 		m_nColorG = ((pStop->m_clrFace)>>8)&0xff;
// 		m_nColorR = (pStop->m_clrFace)&0xff;
// 		SetDlgItemInt(IDC_EDIT_ColorR,m_nColorR);
// 		SetDlgItemInt(IDC_EDIT_ColorG,m_nColorG);
// 		SetDlgItemInt(IDC_EDIT_ColorB,m_nColorB);
		CRect rect;
		pStop->GetWindowRect(&rect);
		m_stcGradient.ScreenToClient(&rect);
		m_bSet = TRUE;
		SetDlgItemInt(IDC_EDIT2, rect.left);
		m_bSet = FALSE;
	}
	else
	{
		m_btnColor.EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT2)->EnableWindow(FALSE);
		GetDlgItem(IDC_BTN_DELETE)->EnableWindow(FALSE);
	}
	return 0;
}

void CDlgGradient::OnSelchangeList1() 
//LRESULT CDlgGradient::OnSelchangeList1(WPARAM wp, LPARAM lp)
{
	int iItem = m_lstGradient.GetCurSel();
	if (iItem < 0)
		return;

	CString strText;
	m_lstGradient.GetText(iItem, strText);
	SetDlgItemText(IDC_EDIT1, strText);
	CGradientItem* pGradient = (CGradientItem*)m_lstGradient.GetItemData(iItem);
	m_stcGradient.ResetColors(pGradient->tblLocat, pGradient->tblColor);
	m_stcGradient.Invalidate();
	//return 0;
}

void CDlgGradient::OnBtnNew() 
{
	CString strText;
	GetDlgItemText(IDC_EDIT1, strText);
	if (strText.IsEmpty())
	{
		CString str;
		str.LoadString(IDS_NAME_INVALIDATE);//The name is invalidate, please input again.
		AfxMessageBox(str);
		GetDlgItem(IDC_EDIT1)->SetFocus();
		return;
	}
	CString	str;
	str.LoadString(IDS_ERR_NAME_EXIST);//The name is already used, replace it or not?
	int nItem = m_lstGradient.FindString(0, strText);
	if (nItem < 0)
	{
		m_lstGradient.AddGradient(strText, m_stcGradient.m_tblLocat, m_stcGradient.m_tblColor);
	}
	else if (AfxMessageBox(str, MB_OKCANCEL) == IDOK)
	{
		m_lstGradient.SetGradient(nItem, m_stcGradient.m_tblLocat, m_stcGradient.m_tblColor);
	}
}

//void CDlgGradient::OnPosChanged(WPARAM wp, LPARAM lp)
LRESULT CDlgGradient::OnPosChanged(WPARAM wp, LPARAM lp)
{
	m_bSet = TRUE;
	SetDlgItemInt(IDC_EDIT2, lp);
	m_bSet = FALSE;
	return 0;
}

void CDlgGradient::OnChangeEdit2() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	if (m_bSet)
	{
		return;
	}

	CStopCtrl* pStop = m_stcGradient.GetSelected();
	if (pStop != NULL)
	{
		int nPos = GetDlgItemInt(IDC_EDIT2);
		if (nPos < 0)
		{
			nPos = 0;
			m_bSet = TRUE;
			SetDlgItemInt(IDC_EDIT2, nPos);
			m_bSet = FALSE;
		}
		else if (nPos > 256)
		{
			nPos = 256;
			m_bSet = TRUE;
			SetDlgItemInt(IDC_EDIT2, nPos);
			m_bSet = FALSE;
		}
		
		m_stcGradient.SetStopPos(nPos);
	}
}

void CDlgGradient::OnBtnDel() 
{
	int iItem = m_lstGradient.GetCurSel();
	if (iItem > -1)
	{
		m_lstGradient.DeleteString(iItem);
		if (iItem < m_lstGradient.GetCount())
		{
			m_lstGradient.SetCurSel(iItem);
			OnSelchangeList1();
		}
		else if (m_lstGradient.GetCount())
		{
			m_lstGradient.SetCurSel(0);
			OnSelchangeList1();
		}
	}
}

void CDlgGradient::OnBtnLoad() 
{
	CMyFileDialog dlg(TRUE, _T("colortables.xml"), _T("COLORTABLES_TEMPL"), OFN_EXPLORER | OFN_FILEMUSTEXIST, _T("XML File (*.xml)|*.xml||"));
	if (dlg.DoModal() == IDOK)
	{
		LoadFrom(dlg.GetPathName());
	}
}

void CDlgGradient::OnBtnSaveas() 
{
	CMyFileDialog dlg(FALSE, _T("colortables.xml"), _T("COLORTABLES_TEMPL"), OFN_OVERWRITEPROMPT, _T("XML File (*.xml)|*.xml||"), this);
	if (dlg.DoModal() == IDOK)
	{
		SaveTo(dlg.GetPathName());
	}
}
void CDlgGradient::OnAdd()
{
	CString str;
	GetDlgItemText(IDC_EDIT_ColorR,str);
	m_nColorR = atoi(str);
	GetDlgItemText(IDC_EDIT_ColorG,str);
	m_nColorG = atoi(str);
	GetDlgItemText(IDC_EDIT_ColorB,str);
	m_nColorB = atoi(str);
	GetDlgItemText(IDC_EDIT2,str);
	m_nLocat = atoi(str);
}
DWORD CDlgGradient::AnsiHex2DWORD(LPTSTR s)
{
	DWORD i,ret,v;
	TCHAR c;	
	for(i=0,ret=0;c=*(s+i),c!=(TCHAR)0;i++)
	{
		if(c>='0'&&c<='9')
			v=c-'0';
		else if(c>='a'&&c<='f')
			v=c-'a'+10;
		else if (c>='A'&&c<='F')
		{
			v=c-'A'+10;
		}
		else
			TRACE("AnsiHex2DWORD Error");
		
		ret=(ret<<4) + v;
	}
	return ret;
 } 

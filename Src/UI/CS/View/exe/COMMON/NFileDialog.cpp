// NFileDialog.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "NFileDialog.h"
#include "SaveOptionDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNFileDialog

IMPLEMENT_DYNAMIC(CNFileDialog, CFileDialog)

CNFileDialog::CNFileDialog(BOOL bOpenFileDialog, LPCTSTR lpszDefExt, LPCTSTR lpszFileName,
		DWORD dwFlags, LPCTSTR lpszFilter, CWnd* pParentWnd) :
		CFileDialog(bOpenFileDialog, lpszDefExt, lpszFileName, dwFlags, lpszFilter, pParentWnd)
{
	m_bCopy = TRUE;
	m_bForm = FALSE;
	m_bScout= FALSE;
	m_ofn.lStructSize = 88;
	m_pParentWnd = pParentWnd;
	m_pCurves = NULL;
	m_pSaveOP = new SAVEOPT;
}

BEGIN_MESSAGE_MAP(CNFileDialog, CFileDialog)
	//{{AFX_MSG_MAP(CNFileDialog)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_CHECK_COPY, OnCheckCopy)
	ON_BN_CLICKED(IDC_CHECK_FORM, OnCheckForm)
	ON_BN_CLICKED(IDC_CHECK_SCOUT, OnCheckScout)
	ON_BN_CLICKED(IDC_BTN_ADVANCED, OnBtnAdvanced)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


BOOL CNFileDialog::OnInitDialog() 
{
	// Subclass controls...
	m_btnCopy.SubclassDlgItem(IDC_CHECK_COPY, this);
	m_btnForm.SubclassDlgItem(IDC_CHECK_FORM, this);
	m_btnScout.SubclassDlgItem(IDC_CHECK_SCOUT, this);
    m_btnSetting.SubclassDlgItem(IDC_BTN_ADVANCED,this);
	CheckButtons(m_ofn.lpstrFilter);
	
	// Initialize helper
	m_dlgHelper.Init(this);
	

	return CFileDialog::OnInitDialog();
}

int CNFileDialog::DoModal()
{
	m_ofn.lpTemplateName = MAKEINTRESOURCE(IDD_NEWFILE);
	m_ofn.Flags |= OFN_ENABLETEMPLATE;
	return CFileDialog::DoModal();
}

// Dialog was sized, reposition my controls
//
void CNFileDialog::OnSize(UINT nType, int cx, int cy) 
{
	CWnd* pDlg = GetParent();
	CRect rcDlg;
	pDlg->GetWindowRect(&rcDlg);
	
	CWnd* pCancel = pDlg->GetDlgItem(IDCANCEL);
	ASSERT(pCancel);
	CRect rcCancel;
	pCancel->GetWindowRect(&rcCancel);
	
	int cxRightMargin = rcDlg.right - rcCancel.right;
	
/*	for (int i = IDC_NFILE_ST; i< IDC_NFILE_ED; i++) 
	{
		CWnd* pInfoWnd = GetDlgItem(i);
		ASSERT(pInfoWnd);
		pInfoWnd->GetWindowRect(&rc);
		rc.right = rcDlg.right - cxRightMargin;
		ScreenToClient(&rc);
		pInfoWnd->SetWindowPos(NULL, rc.left, rc.top, rc.Width(), rc.Height(), 0);
	}
*/
}

// User selected new file (CDN_SELCHANGE)
//
void CNFileDialog::OnFileNameChange()
{
	
}

// User selected new folder (CDN_SELCHANGE)
//
void CNFileDialog::OnFolderChange()
{
	ShowFileInfo();
}

// User selected new file type (from drop-down)
//
void CNFileDialog::OnTypeChange()
{
	CComboBox* pCombo = (CComboBox*)GetParent()->GetDlgItem(cmb1);
	if (pCombo)
	{
		pCombo->GetLBText(m_ofn.nFilterIndex - 1, m_strExt);
	}
	m_strExt.MakeLower();
	CheckButtons(m_strExt);
}

void CNFileDialog::CheckButtons(CString strExt)
{
	if (strExt.Find("*.axp") > -1)
	{
		m_bForm = 1;
		m_bScout = 2;
		m_btnForm.EnableWindow();
		m_btnScout.EnableWindow();
	}
	else
	{
		m_bForm = 0;
		m_bScout= 0;
		m_btnForm.EnableWindow(FALSE);
		m_btnScout.EnableWindow(FALSE);
	}
	
	// 保存为位图时，灰掉高级按钮
    if ((strExt.Find("*.bmp") > -1) || (strExt.Find("*.tif") > -1))
		m_btnSetting.EnableWindow(FALSE);
	else 
        m_btnSetting.EnableWindow(TRUE);

	m_btnCopy.SetCheck(m_bCopy);
	m_btnForm.SetCheck(m_bForm);
	m_btnScout.SetCheck(m_bScout);
}

// Common helper: show information in the preview and debug panes
//
void CNFileDialog::ShowFileInfo()
{
	CFileDlgHelper& fdh = m_dlgHelper;
	CString path = GetPathName();
	CString fldr = GetFolderPath();
	
	// Create debug message
	//
	CString s;
	s.Format(_T("GetPathName=%s\nGetFolderPath=%s\n"), (LPCTSTR)path, (LPCTSTR)fldr);
	
	CListCtrl* plc = fdh.GetListCtrl();

	CString str;
	str.LoadString(IDS_SELECTED);//选中:\n
	s += str;
	int nSelected = 0;
	POSITION pos = plc->GetFirstSelectedItemPosition();
	while (pos) 
	{
		int i = plc->GetNextSelectedItem(pos);
		CString temp;
		temp.Format(_T(" %s %s = %s\n"),
			(LPCTSTR)fdh.GetItemText(i),
			fdh.IsItemFolder(i) ? _T("(FOLDER)") : _T(""),
			(LPCTSTR)fdh.GetItemPathName(i));
		s += temp;
		nSelected++;
	}
	
	// Create preview text
	//
	s.Empty();
	/*
	if (nSelected==1 && IsTextFileName(path)) {
		s = GetTextPreview(path);
	} else if (nSelected>1) {
		s = _T("[选中多个]");
	} else if (nSelected==0) {
		s = _T("[没有选中]");
	}*/
}

void CNFileDialog::OnCheckCopy()
{
	m_bCopy = m_btnCopy.GetCheck();
}

void CNFileDialog::OnCheckForm()
{
	m_bForm = m_btnForm.GetCheck();
}

void CNFileDialog::OnCheckScout()
{
	m_bScout = m_btnScout.GetCheck();
}

/*函数名称：OnBtnAdvanced
 *函数描述：打开高级选项中的设置对话框
 *返回类型：void
 *函数参数：void
 */
void CNFileDialog::OnBtnAdvanced() 
{
	if (m_pCurves != NULL)
	{
		CSaveOptionDlg dlg(m_pCurves, this);
		dlg.DoModal();
	}
}

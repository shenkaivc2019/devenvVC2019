// ChartView.cpp: implementation of the CChartView class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "resource.h"
#include "ChartView.h"
#include "ULFile.h"
#include "VIWWell.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNAMIC(CChartView, CCustomizeView)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CChartView::CChartView(CULFile* pFile /* = NULL */)
{
	m_pDoc = pFile;
}

CChartView::~CChartView()
{

}

BEGIN_MESSAGE_MAP(CChartView, CCustomizeView)
	//{{AFX_MSG_MAP(CChartView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChartView diagnostics

#ifdef _DEBUG
void CChartView::AssertValid() const
{
	CCustomizeView::AssertValid();
}

void CChartView::Dump(CDumpContext& dc) const
{
	CCustomizeView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CChartView message handlers

int CChartView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CULFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	CString strFile = m_pDoc->GetPathName();
	if (strFile.GetLength())
	{
		int iFind = strFile.ReverseFind(_T('.'));
		if (iFind > 0)
			strFile = strFile.Left(iFind);

		strFile += _T(".tpl");

		VARIANT vtDoc;
		vtDoc.vt = VT_BYREF;
		vtDoc.byref = (IULFile*)DYNAMIC_DOWNCAST(CULFile, m_pDoc);
		m_pViwWell->SetDoc(vtDoc);

		CFileFind ff;
		if (ff.FindFile(strFile))
			m_pViwWell->Load(strFile);
		else
		{
			CString strDefault = Gbl_AppPath;
			strDefault += _T("\\Template\\OutPut\\Default.tpl");
			m_pViwWell->Load(strDefault);
		}
	}
	
	return 0;
}

void CChartView::SaveAsTempl(LPCTSTR pszFile)
{
	if (m_pViwWell->GetSafeHwnd())
	{
		CString strTempl = pszFile;
		if (strTempl.Right(4).CompareNoCase(_T(".adc")))
			strTempl += _T(".adc");
		m_pViwWell->Save(strTempl);
	}
}

// CustomizeView.cpp : implementation file

#include "stdafx.h"
#include "CustomizeView.h"
#include "viwwell1.h"
#include "ULDoc.H"
#include "ComConfig.h"
#include "resource.h"
#include "Project.h"
#include "Utility.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCustomizeView

IMPLEMENT_DYNAMIC(CCustomizeView, CULFormView)

CCustomizeView::CCustomizeView(LPVOID pCst /* = NULL */) : CULFormView(IDD_FORMVIEW)
{
	m_pViwWell = NULL;
	m_bUpdate = TRUE;
	m_pCst = (CCstInfo*)pCst;
}

CCustomizeView::~CCustomizeView()
{

}


BEGIN_MESSAGE_MAP(CCustomizeView, CULFormView)
	//{{AFX_MSG_MAP(CCustomizeView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CCustomizeView::OnUpdate()
{
	if (m_pDoc)
		m_pDoc->UpdateViews(ULV_GROUP);
}

// 计算打印高度, 不包括上下两个PAGE_MARGIN, 方便组合图的计算
int CCustomizeView::CalcPrintHeight()
{
	return CalcPageHeight() - 2 * PAGE_MARGIN;
}

#ifdef _DEBUG
void CCustomizeView::AssertValid() const
{
	CULFormView::AssertValid();
}

void CCustomizeView::Dump(CDumpContext& dc) const
{
	CULFormView::Dump(dc);
}
#endif //_DEBUG

int CCustomizeView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CULFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_pCst != NULL)
	{
		m_pViwWell->Load(m_pCst->strFile);
	}
	
	return 0;
}

BEGIN_EVENTSINK_MAP(CCustomizeView, CULFormView)
//{{AFX_EVENTSINK_MAP(CASCOPEDlg)
ON_EVENT(CCustomizeView, AFX_IDC_ACTXCTRL, 1 /* DataChanged */, OnDataChangedVIWWELL1CTRL1, VTS_NONE)
//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

void CCustomizeView::OnDataChangedVIWWELL1CTRL1()
{	
	m_bUpdate = FALSE;
	OnUpdate();
	m_bUpdate = TRUE;

	if (m_pDoc)
		m_pDoc->SetModifiedFlag();
}

void CCustomizeView::LoadTempl(LPCTSTR pszFile)
{
	if (m_pViwWell->GetSafeHwnd())
	{
		CString strTempl = pszFile;
		if (m_pViwWell->Load(strTempl))
		{
			strTempl = strTempl.Right(strTempl.GetLength() - strTempl.ReverseFind(_T('\\')) - 1);
			strTempl = _T("[U]") + strTempl.Left(strTempl.GetLength() - 4);
			GetParent()->SetWindowText(strTempl);
		}
	}
}

void CCustomizeView::SaveAsTempl(LPCTSTR pszFile)
{
	if (m_pViwWell->GetSafeHwnd())
	{
		CString strTempl = pszFile;
		if (strTempl.Right(4).CompareNoCase(_T(".acv")))
			strTempl += _T(".acv");
		m_pViwWell->Save(strTempl);
	}
}

void CCustomizeView::SaveService()
{
	
}

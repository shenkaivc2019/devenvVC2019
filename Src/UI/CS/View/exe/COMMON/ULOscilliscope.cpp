#include "stdafx.h"

#include "ULOscilliscope.h"

#ifdef _OLD_SCOPES
ULScopePara*  CULOscilliscope::OpenOscilliscope(CString strName, UINT nType)
{
	/*
	m_ScopePara.nType = nType ;
	memcpy(m_ScopePara.szName,strName ,sizeof(m_ScopePara.szName ));
	*/
	m_ScopePara.pDlg = NULL;
	return NULL;
}

BOOL CULOscilliscope::OpenOscilliscope(ULScopePara* pScopeData)
{
	m_ScopePara.nType = pScopeData->nType ;
	m_ScopePara.pDlg = NULL;
	memcpy(m_ScopePara.szName,pScopeData->szName ,sizeof(pScopeData->szName ));
	memcpy(m_ScopePara.Reserved,pScopeData->Reserved ,sizeof(pScopeData->Reserved ));
	return TRUE;
}

BOOL CULOscilliscope::CloseOscilliscope(ULScopePara* pScopeData)
{
	return TRUE;
}

BOOL CULOscilliscope::RefreshOscilliscopePara(ULScopePara* pScopeData)
{
	return TRUE;
}

#endif 
// OGResultView.cpp : implementation file

#include "stdafx.h"
#include "OGResultView.h"
#include "viwwell1.h"
#include "ULDoc.H"
#include "ComConfig.h"
#include "resource.h"
#include "Project.h"
#include "Utility.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COGResultView

IMPLEMENT_DYNAMIC(COGResultView, CULFormView)

COGResultView::COGResultView(LPVOID pCst /* = NULL */) : CULFormView(IDD_FORMVIEW)
{
	m_pViwWell = NULL;
	m_bUpdate = TRUE;
}

COGResultView::~COGResultView()
{

}


BEGIN_MESSAGE_MAP(COGResultView, CULFormView)
	//{{AFX_MSG_MAP(COGResultView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void COGResultView::OnUpdate()
{
	if (m_pDoc)
		m_pDoc->UpdateViews(ULV_GROUP);
}

// 计算打印高度, 不包括上下两个PAGE_MARGIN, 方便组合图的计算
int COGResultView::CalcPrintHeight()
{
	return CalcPageHeight() - 2 * PAGE_MARGIN;
}

#ifdef _DEBUG
void COGResultView::AssertValid() const
{
	CULFormView::AssertValid();
}

void COGResultView::Dump(CDumpContext& dc) const
{
	CULFormView::Dump(dc);
}
#endif //_DEBUG

int COGResultView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CULFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_pDoc)
		return 0;

	CString strFile = m_pDoc->GetPathName();
	CString strExt = strFile.Right(4);
	
	BOOL bLoadFromFile = FALSE;
	CString strATS = strFile + _T(".arv");
	CFileFind ff;
	if (ff.FindFile(strATS))
	{
		bLoadFromFile = TRUE;
	}

	if (bLoadFromFile)
	{
		m_pViwWell->Load(strATS);
	}
	
	return 0;
}

BEGIN_EVENTSINK_MAP(COGResultView, CULFormView)
//{{AFX_EVENTSINK_MAP(CASCOPEDlg)
ON_EVENT(COGResultView, AFX_IDC_ACTXCTRL, 1 /* DataChanged */, OnDataChangedVIWWELL1CTRL1, VTS_NONE)
//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

void COGResultView::OnDataChangedVIWWELL1CTRL1()
{	
	m_bUpdate = FALSE;
	OnUpdate();
	m_bUpdate = TRUE;

	if (m_pDoc)
		m_pDoc->SetModifiedFlag();
}

void COGResultView::LoadTempl(LPCTSTR pszFile)
{
	if (m_pViwWell->GetSafeHwnd())
	{
		CString strTempl = pszFile;
		if (m_pViwWell->Load(strTempl))
		{
			strTempl = strTempl.Right(strTempl.GetLength() - strTempl.ReverseFind(_T('\\')) - 1);
			strTempl = _T("[R]") + strTempl.Left(strTempl.GetLength() - 4);
			GetParent()->SetWindowText(strTempl);
		}
	}
}

void COGResultView::SaveAsTempl(LPCTSTR pszFile)
{
	if (m_pViwWell->GetSafeHwnd())
	{
		CString strTempl = pszFile;
		if (strTempl.Right(4).CompareNoCase(_T(".arv")))
			strTempl += _T(".arv");
		m_pViwWell->Save(strTempl);
	}
}

void COGResultView::SaveService()
{
	
}

// TView.cpp : implementation file
// 仪器组合视图

#include "stdafx.h"
#include "resource.h"
#include "TView.h"
#include "MainFrm.h"
#include "ServiceTableItem.h"
#include "Project.h"
#include "ULTool.h"
#include "ComConfig.h"
#include "Units.h"
#include "viwwell2.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define PAGE_HEIGHT		2400

extern CString Gbl_AppPath;
extern CUnits* g_units;

/////////////////////////////////////////////////////////////////////////////
// CTView

IMPLEMENT_DYNAMIC(CTView, CULFormView)

CTView::CTView(LPVOID lpParam /* = NULL */) : CULFormView(IDD_FORMVIEW)
{
	m_pWellPI = (CProject *) lpParam;
}

CTView::~CTView()
{
	
}


BEGIN_MESSAGE_MAP(CTView, CULFormView)
	//{{AFX_MSG_MAP(CTView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTView drawing

void CTView::OnUpdate()
{
	if (m_pDoc)
		m_pDoc->UpdateViews(ULV_GROUP);
}

/////////////////////////////////////////////////////////////////////////////
// CTView diagnostics

#ifdef _DEBUG
void CTView::AssertValid() const
{
	CULFormView::AssertValid();
}

void CTView::Dump(CDumpContext& dc) const
{
	CULFormView::Dump(dc);
}
#endif //_DEBUG

int CTView::CalcPrintHeight()
{
	return CalcPageHeight();
}

void CTView::GetCompData(BOOL bUpdate /* = FALSE */)
{
	CXMLSettings xml(FALSE, _T("ToolStrings"));

	m_arrTools.RemoveAll();
	UINT nCount = m_pWellPI->m_ULKernel.m_arrTools.GetSize();	// 得到仪器数量	
	UINT i = 0;
	for (; i < nCount; i++)
	{
		CULTool* pTool = (CULTool*) m_pWellPI->m_ULKernel.m_arrTools.GetAt(i);	
		if (pTool->m_pToolInfoPack != NULL)
		{
			CString strDllPath = Gbl_AppPath + pTool->strToolDllDir;
			CString strPath = strDllPath.Left(strDllPath.ReverseFind('\\'));
			strPath += "\\Tool.ini";
			if (xml.CreateKey(_T("NewT%04d"), i))
			{
				CXMLNode* pCurr = xml.m_pCurrNode;
				xml.ReadCurrNodeFromFile(strPath);
				xml.m_pCurrNode = pCurr;
				//xml.Write(_T("Tool"), pTool->strToolName);
				xml.m_bReadOnly = TRUE;
				pTool->SerializeProps(xml, 0x02);
				xml.m_bReadOnly = FALSE;
			}
			xml.Back(-1);	
		}

		CATool* pATool = (CATool*)pTool;
		m_arrTools.Add(pATool);
	}

	// Initialize the check properties

	UINT n = 9;
	xml.Write(_T("ToolProp"), AfxGetComConfig()->m_Other.ToolProperties, n);
	n = 5;
	xml.Write(_T("TotalProp"), AfxGetComConfig()->m_Other.ToolsProperties, n);

	xml.m_bReadOnly = TRUE;
	xml.Back(-1);
	// m_pViwWell->ReadXML(&xml);
	
	((CVIWWell2*)m_pViwWell)->SetTools(&m_arrTools);

	if (bUpdate)
	{
// 		OnUpdate();
//
	}
}

void CTView::SetCompData()
{
	UINT nCount = m_pWellPI->m_ULKernel.m_arrTools.GetSize();	// 得到仪器数量	
	UINT i = 0;
	for (; i < nCount; i++)
	{
		CULTool* pTool = (CULTool*) m_pWellPI->m_ULKernel.m_arrTools.GetAt(i);	
		if (pTool->m_pToolInfoPack != NULL)
		{
			CString strDllPath = Gbl_AppPath + pTool->strToolDllDir;
			CString strPath = strDllPath.Left(strDllPath.ReverseFind('\\'));
			strPath += "\\Tool.ini";
			CXMLSettings xml(FALSE, pTool->strToolName);
			pTool->m_pToolInfoPack->m_Tool.strSN = pTool->strSN;
			pTool->SerializeProps(xml);
			xml.WriteXMLToFile(strPath);
		}
	}
}

int CTView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CULFormView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_pDoc)
		return 0;

	CString strFile = m_pDoc->GetPathName();
	CString strExt = strFile.Right(4);
	
	BOOL bLoadFromFile = FALSE;
	CString strATS = strFile + _T(".ats");
	CFileFind ff;
	if (ff.FindFile(strATS))
	{
		bLoadFromFile = TRUE;
	}
	if (strFile.GetLength()  && strExt.CompareNoCase(_T(".RAW")) && bLoadFromFile)
	{
		m_pViwWell->Load(strATS);
	}
	else if (m_pWellPI)
	{
		CServiceTableItem* pService = DYNAMIC_DOWNCAST(CServiceTableItem,m_pDoc);
		
		if (pService != NULL)
		{
			// Refresh tool data from service
			if (pService->m_toolstrings.vt != VT_EMPTY)
				m_pViwWell->ReadFromBuffer(&pService->m_toolstrings);
			// else
				GetCompData();
		}
		else if ((strExt.CompareNoCase(_T(".AXP")) == 0) || strExt.CompareNoCase(_T(".ALD")) == 0)
		{
			
			VARIANT data;

			m_pWellPI->m_xmlInfo.m_bReadOnly = TRUE;
			m_pWellPI->m_xmlInfo.Back(-1);
			if (m_pWellPI->m_xmlInfo.Read(_T("ToolStrings"), data))
				m_pViwWell->ReadFromBuffer(&data);
			else
				GetCompData();
				
			((CVIWWell2*)m_pViwWell)->SetTools(NULL);
			
		}
	}

	return 0;
}

BEGIN_EVENTSINK_MAP(CTView, CULFormView)
//{{AFX_EVENTSINK_MAP(CASCOPEDlg)
ON_EVENT(CTView, AFX_IDC_ACTXCTRL, 1 /* DataChanged */, OnDataChangedVIWWELL1CTRL1, VTS_NONE)
//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

void CTView::OnDataChangedVIWWELL1CTRL1()
{
	OnUpdate();

	if (m_pWellPI)
	{
		CString strFile = m_pDoc->GetPathName();
		if (strFile.IsEmpty())
		{
			CServiceTableItem* pService = DYNAMIC_DOWNCAST(CServiceTableItem,
											m_pDoc);
			if (pService != NULL)
			{
				m_pViwWell->WriteToBuffer(&pService->m_toolstrings);
				SetCompData();
			}
			else
			{
				VARIANT data;
				m_pViwWell->WriteToBuffer(&data);
				m_pWellPI->m_xmlInfo.m_bReadOnly = FALSE;
				m_pWellPI->m_xmlInfo.Back(-1);
				m_pWellPI->m_xmlInfo.Write(_T("ToolStrings"), data);
			}
		}

		m_pMainWnd->m_wndWorkspace.m_wndTree.RefreshTools();
	}

	if (m_pDoc)
		m_pDoc->SetModifiedFlag();
}

void CTView::LoadTempl(LPCTSTR pszFile)
{
	if (m_pViwWell->GetSafeHwnd())
	{
		CString strTempl = pszFile;
		if (m_pViwWell->Load(strTempl))
		{
			strTempl = strTempl.Right(strTempl.GetLength() - strTempl.ReverseFind(_T('\\')) - 1);
			strTempl = _T("[T]") + strTempl.Left(strTempl.GetLength() - 4);
			GetParent()->SetWindowText(strTempl);
		}
	}
}

void CTView::SaveAsTempl(LPCTSTR pszFile)
{
	if (m_pViwWell->GetSafeHwnd())
	{
		CString strTempl = pszFile;
		if (strTempl.Right(4).CompareNoCase(_T(".ats")))
			strTempl += _T(".ats");
		m_pViwWell->Save(strTempl);
	}
}

void CTView::SaveService()
{
	if (m_pDoc && ::IsWindow(m_pViwWell->GetSafeHwnd()))
	{
		CString strFile = m_pDoc->GetPathName();
		if (strFile.IsEmpty())
		{
			CServiceTableItem* pService = DYNAMIC_DOWNCAST(CServiceTableItem,
											m_pDoc);
			if (pService != NULL)
			{
				m_pViwWell->WriteToBuffer(&pService->m_toolstrings);
			}
		}
	}
}
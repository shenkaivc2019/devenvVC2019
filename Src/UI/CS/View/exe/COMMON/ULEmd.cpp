// ULEmd.cpp: implementation of the CULEmd class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "Units.h"
#include "ULEmd.h"
#include "Curve.h"
#include "Utility.h"
#include "DlgEmdParam.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

typedef		DWORD(FAR PASCAL* _ULGETVERSION)();
typedef		IUnknown*(FAR PASCAL* _NEWEMD)();

extern CUnits* g_units;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CULEmd::CULEmd(LPCTSTR pszEmd)
{
	TCHAR szPath[_MAX_PATH];
	GetModuleFileName(NULL, szPath, _MAX_PATH);
	CString strPath = szPath;
	strPath = strPath.Left(strPath.ReverseFind('\\'));
	strPath = strPath.Left(strPath.ReverseFind('\\'));
	strPath += _T("\\");
	strPath += pszEmd;
	m_pEmd = NULL;
	m_hEmd = NULL;

	RecurseFind(m_lstFiles, strPath, _T("*.dll"));
	for (POSITION pos = m_lstFiles.GetHeadPosition(); pos != NULL; )
	{
		POSITION pos0 = pos;
		CString strFile = m_lstFiles.GetNext(pos);
		if (LoadEmd(strFile))
		{
			POSITION pos = m_lstEmd.GetHeadPosition();
			for (UINT nIndex = 0; pos != NULL; nIndex++)
			{
				m_pEmd = m_lstEmd.GetNext(pos);
				m_lstNames.AddTail(m_pEmd->IDName());
				m_lstEmdFiles.AddTail(strFile);
				m_arrEmdIndex.Add(nIndex);
			}
			ReleaseEmd();
		}
		else
		{
			m_lstFiles.RemoveAt(pos0);
		}
	}

	m_pULFile = NULL;
	m_pPNode = NULL;
	m_iDepth = -1;
}

CULEmd::~CULEmd()
{
	ReleaseEmd();
}

ULMPTRIMP CULEmd::GetICurve(LPCTSTR pszCName)
{
	if (m_pULFile == NULL)
		return NULL;

	CString strName = pszCName;
	strName.MakeUpper();
	if (strName == _T("DEPT"))
	{
		return m_pULFile->GetDEPT();
	}

	CXMLNode* pCurrNode = m_xml.m_pCurrNode;
	m_xml.Back(-1);
	m_xml.m_bReadOnly = TRUE;
	if (m_xml.Open(_T("Input")))
	{
		if (m_xml.Open(pszCName))
		{
			CString strCName;
			m_xml.Read(_T("Reset"), strCName);
			if (strCName.GetLength())
			{
				m_xml.SetCurrNode(pCurrNode);
				return m_pULFile->GetCurve(strCName);
			}

			m_xml.SetCurrNode(pCurrNode);
			return m_pULFile->GetCurve(pszCName);
		}
	}

	m_xml.SetCurrNode(pCurrNode);
	return NULL;
}

ULMPTRIMP CULEmd::GetOCurve(LPCTSTR pszCName, int nValueType /* = VT_R4 */ , int nDimension /* = 1*/)
{
	if (m_pULFile == NULL)
		return NULL;

	ICurve* pCurve = NULL;
	CXMLNode* pCurrNode = m_xml.m_pCurrNode;
	m_xml.Back(-1);
	m_xml.m_bReadOnly = TRUE;
	if (m_xml.Open(_T("Output")))
	{
		if (m_xml.Open(pszCName))
		{
			CString strCName;
			m_xml.Read(_T("Reset"), strCName);

			m_xml.SetCurrNode(pCurrNode);
			if (strCName.GetLength())
			{
				strCName += m_strSuffix;
				pCurve = (ICurve*)m_pULFile->GetCurve(strCName);
				if (pCurve)
					return pCurve;
				
				return m_pULFile->CreateCurve(strCName, nValueType , nDimension);
			}

			strCName = pszCName;
			strCName += m_strSuffix;

			pCurve = (ICurve*)m_pULFile->GetCurve(strCName);
			if (pCurve)
				return pCurve;

			return m_pULFile->CreateCurve(strCName, nValueType , nDimension);
		}
	}
	
	m_xml.SetCurrNode(pCurrNode);
	return NULL;	
}

ULMINTIMP CULEmd::GetParamIndex(LPCTSTR pszPName)
{
	if (pszPName == NULL)
		return -1;
	for (int i = 0; i < m_params.GetSize(); i++)
	{
		CXMLNode* pNode = (CXMLNode*)m_params.GetAt(i);
		if (pNode->m_strName == pszPName)
		{
			return i;
		}
	}

	return -1;
}

ULMDBLIMP CULEmd::GetParamVal(int nIndex)
{
	m_xml.m_bReadOnly = TRUE;
	m_xml.m_pCurrNode = (CXMLNode*)m_params.GetAt(nIndex);
	CString strText;
	if (m_xml.Read(_T("Value"), strText))
	{
		LPTSTR stop;
		return _tcstod(strText, &stop);
	}

	return 0.0;

// 	LPTSTR stop;
// 	CStringList* pValues = (CStringList*)m_values.GetAt(nIndex);
// 	return _tcstod(pValues->GetHead(), &stop);
}

ULMTSTRIMP CULEmd::GetParamStr(int nIndex)
{
	/*m_xml.m_bReadOnly = TRUE;
	m_xml.m_pCurrNode = (CXMLNode*)m_params.GetAt(nIndex);
	CString strText;
	if (m_xml.Read(_T("Value"), strText))
	{
		return (LPTSTR)(LPCTSTR)strText;
	}
	return _T("");*/

 	CStringList* pValues = (CStringList*)m_values.GetAt(nIndex);
	if ((pValues != NULL) && (pValues->GetCount()))
		return (LPTSTR)(LPCTSTR)pValues->GetHead();
	return _T("");
}

ULMINTIMP  CULEmd::GetParamInt(int nIndex)
{
	m_xml.m_bReadOnly = TRUE;
	m_xml.m_pCurrNode = (CXMLNode*)m_params.GetAt(nIndex);
	CString strText;
	if (m_xml.Read(_T("Value"), strText))
	{
		return _ttoi(strText);
	}
	return -1;
	
// 	CStringList* pValues = (CStringList*)m_values.GetAt(nIndex);
// 	return _ttoi(pValues->GetHead());
}

ULMDBLIMP CULEmd::GetParamVald(int nIndex, long lDepth)
{
	CString strText = GetParamStrd(nIndex, lDepth);
	if (strText.GetLength())
	{
		LPTSTR stop;
		return _tcstod(strText, &stop);
	}
	return 0.0;
}

ULMTSTRIMP CULEmd::GetParamStrd(int nIndex, long lDepth)
{
	CStringList* pValues = (CStringList*)m_values.GetAt(nIndex);
	int iDepth = GetDepthIndex(lDepth);

	// when the lDepth is not in range, we return the default value
	if (iDepth < 0)
		return GetParamStr(nIndex);

	// The first value is default value
	POSITION pos = pValues->FindIndex(++iDepth);

	// get the previous value if the current value is empty
	while (pos != NULL)
	{
		CString strText = pValues->GetPrev(pos);
		if (strText.GetLength())
		{
			return (LPTSTR)(LPCTSTR)strText;
		}
	}
	
	// get default value if nothing found
	return GetParamStr(nIndex);
}

ULMINTIMP  CULEmd::GetParamIntd(int nIndex, long lDepth)
{
	CString strText = GetParamStrd(nIndex, lDepth);
	if (strText.GetLength())
	{
		return _ttoi(strText);
	}
	return -1;
}

ULMTSTRIMP  CULEmd::GetParamUnit(int nIndex)
{
	m_xml.m_bReadOnly = TRUE;
	m_xml.m_pCurrNode = (CXMLNode*)m_params.GetAt(nIndex);
	CString strText;
	if (m_xml.Read(_T("Unit"), strText))
	{
		return (LPTSTR)(LPCTSTR)strText;
	}

	return _T("");
}

ULMTSTRIMP  CULEmd::GetParamDesc(int nIndex)
{
	m_xml.m_bReadOnly = TRUE;
	m_xml.m_pCurrNode = (CXMLNode*)m_params.GetAt(nIndex);
	CString strText;
	if (m_xml.Read(_T("Desc"), strText))
	{
		return (LPTSTR)(LPCTSTR)strText;
	}

	return _T("");
}

ULMDBLIMP CULEmd::GetParamVal(LPCTSTR pszPName)
{
	m_xml.m_bReadOnly = TRUE;
	m_xml.m_pCurrNode = m_pPNode;
	CString strText;
	if (m_xml.Open(pszPName))
	{
		if (m_xml.Read(_T("Value"), strText))
		{
			LPTSTR stop;
			return _tcstod(strText, &stop);
		}
	}

	return 0.0;
}

ULMTSTRIMP CULEmd::GetParamStr(LPCTSTR pszPName)
{
	// 获得参数字符值(缺省)，不存在时返回空串
	m_xml.m_bReadOnly = TRUE;
	m_xml.m_pCurrNode = m_pPNode;
	CString strText;
	if (m_xml.Open(pszPName))
	{
		if (m_xml.Read(_T("Value"), strText))
		{
			return (LPTSTR)(LPCTSTR)strText;
		}
	}

	return _T("");
}

ULMINTIMP  CULEmd::GetParamInt(LPCTSTR pszPName)
{
	m_xml.m_bReadOnly = TRUE;
	m_xml.m_pCurrNode = m_pPNode;
	CString strText;
	if (m_xml.Open(pszPName))
	{
		if (m_xml.Read(_T("Value"), strText))
		{
			return _ttoi(strText);
		}
	}

	return -1;
}

ULMDBLIMP CULEmd::GetParamVald(LPCTSTR pszPName, long lDepth)
{
	int nIndex = GetParamIndex(pszPName);
	if (nIndex < 0)
		return 0.0;
	
	return GetParamVald(nIndex, lDepth);
}

ULMTSTRIMP CULEmd::GetParamStrd(LPCTSTR pszPName, long lDepth)
{
	int nIndex = GetParamIndex(pszPName);
	if (nIndex < 0)
		return _T("");

	return GetParamStrd(nIndex, lDepth);
}

ULMINTIMP  CULEmd::GetParamIntd(LPCTSTR pszPName, long lDepth)
{
	int nIndex = GetParamIndex(pszPName);
	if (nIndex < 0)
		return -1;

	return GetParamIntd(nIndex, lDepth);
}

ULMTSTRIMP  CULEmd::GetParamUnit(LPCTSTR pszPName)
{
	m_xml.m_bReadOnly = TRUE;
	m_xml.m_pCurrNode = m_pPNode;
	CString strText;
	if (m_xml.Open(pszPName))
	{
		if (m_xml.Read(_T("Unit"), strText))
		{
			return (LPTSTR)(LPCTSTR)strText;
		}
	}
	
	return _T("");
}

ULMTSTRIMP  CULEmd::GetParamDesc(LPCTSTR pszPName)
{
	m_xml.m_bReadOnly = TRUE;
	m_xml.m_pCurrNode = m_pPNode;
	CString strText;
	if (m_xml.Open(pszPName))
	{
		if (m_xml.Read(_T("Desc"), strText))
		{
			return (LPTSTR)(LPCTSTR)strText;
		}
	}

	return _T("");
}

void CULEmd::UpdateMenu(CCmdUI* pCmdUIx, long lMax)
{
	CMenu* pMenu = pCmdUIx->m_pMenu;
	if (pMenu == NULL)
		return;

	if (m_strMenu.IsEmpty() && pMenu != NULL)
		pMenu->GetMenuString(pCmdUIx->m_nID, m_strMenu, MF_BYCOMMAND);

	ASSERT(pMenu == pCmdUIx->m_pMenu);  // make sure preceding code didn't mess with it
	ASSERT(pMenu->m_hMenu); 			// look for a submenu to use instead
	CMenu* pSubMenu;
	if (pMenu)
		pSubMenu = pMenu->GetSubMenu(pCmdUIx->m_nIndex);

	if (pSubMenu)
	{
		ASSERT(pSubMenu->m_hMenu);
		pMenu = pSubMenu;
	}
	ASSERT(pMenu->m_hMenu);

	int nSize = lMax;
	int i;
	for (i = 0; i < nSize; i++)
	{
		pMenu->RemoveMenu(pCmdUIx->m_nID + i, MF_BYCOMMAND);
	}

	CString strName;
	CString strTemp;
	POSITION pos = m_lstNames.GetHeadPosition();
	for (i = 0; i < nSize; i++)
	{
		if (pos == NULL)
			break;

		strName = m_lstNames.GetNext(pos);
		
		strTemp = strName;
		// Note we use our pMenu which may not be pCmdUI->m_pMenu
		pMenu->InsertMenu(pCmdUIx->m_nIndex++, MF_STRING | MF_BYPOSITION,
				pCmdUIx->m_nID++, strTemp);
	}

	if (pCmdUIx->m_nIndex > 0)
	{
		// update end menu count
		pCmdUIx->m_nIndex--; // point to last menu added
		pCmdUIx->m_nIndexMax = pMenu->GetMenuItemCount();
		pCmdUIx->m_bEnableChanged = TRUE;	// all the added items are enabled
	}
}

void CULEmd::GetICurvesName(CStringList* pList)
{
	if (m_pULFile == NULL)
		return;

	int nCount = m_pULFile->GetCurveCount();
	for (int i = 0; i < nCount; i++)
	{
		ICurve* pCurve = (ICurve*)m_pULFile->GetCurve(i);
		pList->AddTail(pCurve->Name());
	}
}

BOOL CULEmd::LoadEmd(LPCTSTR pszPath, UINT nIndex /* = -1 */)
{
	m_bUserProcess = FALSE;

	CString strEmd = pszPath;
	int nPos = strEmd.ReverseFind('\\');
	CString strEmdPath; 
	if (nPos > 0)
	{
		strEmdPath = strEmd.Left(nPos);
	}

	TCHAR szCurPath[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, szCurPath); 
	SetCurrentDirectory(strEmdPath);
    m_hEmd = LoadLibrary(strEmd);	
	SetCurrentDirectory(szCurPath);

    if (m_hEmd == NULL)
		return FALSE;

	// 查询动态库版本号
	_ULGETVERSION ULGetVersion = (_ULGETVERSION)GetProcAddress(m_hEmd, "ULGetVersion");
	if (ULGetVersion == NULL)
	{
		FreeLibrary(m_hEmd);
		m_hEmd = NULL;
		return FALSE;
	}
	
	DWORD dwVersion = ULGetVersion();
	if (dwVersion < MAKEWORD(_UL2000_VERSION_MINOR, _UL2000_VERSION_MAJOR))
	{
		FreeLibrary(m_hEmd);
		m_hEmd = NULL;
		return FALSE;
	}	
	
	// 查询接口
	_NEWEMD CreateEmd = (_NEWEMD) GetProcAddress(m_hEmd, "CreateEmd");
	if (CreateEmd == NULL)
	{
		FreeLibrary(m_hEmd);
		m_hEmd = NULL;
		return FALSE;
	}
	
	m_pEmd = (IEmd*)CreateEmd();
	if (m_pEmd == NULL)
	{
		FreeLibrary(m_hEmd);
		m_hEmd = NULL;
		return FALSE;
	}
	
	// To avoid unlimited loop, we check the IDNAME, if same, break and remove the last
	// and limit the loop times < 32
	CString strFirst;
	CString strCurrent;
	int iCounter = 0;
	if (nIndex == (UINT)-1)
	{
		m_def.m_bReadOnly = TRUE;
		while (m_pEmd != NULL)
		{
			HRESULT hr = m_pEmd->QueryInterface(IID_IEmd, (void**)&m_pEmd);
			if (SUCCEEDED(hr))
			{
				if (strFirst.IsEmpty())
					strFirst = m_pEmd->IDName();
				else
				{
					strCurrent = m_pEmd->IDName();
					if (strFirst == strCurrent)
					{
						m_pEmd->Release();
						m_pEmd = m_lstEmd.GetTail();
						break;
					}
				}
				m_lstEmd.AddTail(m_pEmd);
			}
			else
				m_pEmd->Release();
			
			iCounter++;
			if (iCounter > 32)
				break;

			m_pEmd = (IEmd*)CreateEmd();
		}
	}
	else
	{
		m_def.m_bReadOnly = FALSE;
		m_def.DeleteAllKeys();

		UINT nFind = nIndex + 1;
		while ((m_pEmd != NULL) && (nFind))
		{
			HRESULT hr = m_pEmd->QueryInterface(IID_IEmd, (void**)&m_pEmd);
			if (SUCCEEDED(hr))
			{
				nFind--;
				if (nFind == 0)
					break;
			}

			m_pEmd->Release();
			iCounter++;
			if (iCounter > 32)
				break;

			m_pEmd = (IEmd*)CreateEmd();
		}

		if (m_pEmd != NULL)
			m_lstEmd.AddTail(m_pEmd);
	}	

	if (m_lstEmd.GetCount() == 0)
	{
		FreeLibrary(m_hEmd);
		m_hEmd = NULL;
		m_pEmd = NULL;
		return FALSE;
	}

	m_strEmd = pszPath;
	
	HINSTANCE hRes = AfxGetResourceHandle();
	AfxSetResourceHandle(m_hEmd);

	POSITION pos = m_lstEmd.GetHeadPosition();
	for ( ; pos != NULL; )
	{
		m_pEmd = m_lstEmd.GetNext(pos);
		try
		{
			m_pEmd->AdviseEmd(this);
		}
		catch (CException* e)
		{
			e->ReportError();
			e->Delete();
		}
		catch (...) 
		{
			
		}
	}
	AfxSetResourceHandle(hRes);

	if (nIndex != (UINT)-1)
	{
		m_xml.m_bReadOnly = FALSE;
		m_xml.DeleteAllKeys();
		m_bLock = FALSE;
		CString strDefault = m_strEmd.Left(m_strEmd.GetLength() - 4);
		m_strDef.Format(_T("%s_%u.xml"), strDefault, nIndex);
		CFileFind ff;
		if (!ff.FindFile(m_strDef))
			m_def.WriteXMLToFile(m_strDef);

		// strDefault += _T(".xml");
		m_bLock = m_xml.ReadXMLFromFile(m_strDef);
	}

	return TRUE;
}

void CULEmd::ReleaseEmd()
{
	m_bUserProcess = FALSE;

	while (m_lstEmd.GetCount())
	{
		IEmd* pEmd = m_lstEmd.RemoveHead();
		pEmd->Release();
	}
	
	if (m_hEmd)
		::FreeLibrary(m_hEmd);

	m_pEmd = NULL;
	m_hEmd = NULL;

	m_strEmd.Empty();
	ClearParamValues();
}

void CULEmd::SaveParamValues()
{
	m_xml.Back(-1);
	if (m_xml.CreateKey(_T("Params")))
	{
		m_pPNode = m_xml.m_pCurrNode;
		POSITION pos = m_pPNode->m_lstChildren.GetHeadPosition();
		for (int i = 0; pos != NULL; i++)
		{
			CXMLNode* pNode = m_pPNode->m_lstChildren.GetNext(pos);
			m_xml.m_pCurrNode = pNode;
			CStringList* pValues = (CStringList*)m_values.GetAt(i);
			m_xml.Write(_T("Values"), *pValues);
		}
		m_xml.m_pCurrNode = m_pPNode;
		m_xml.Back();
	}
}

void CULEmd::InitParamValues()
{
	m_params.RemoveAll();

	for (int i = 0; i < m_values.GetSize(); i++)
	{
		CStringList* pValues = (CStringList*)m_values.GetAt(i);
		delete pValues;
	}
	m_values.RemoveAll();

	m_xml.Back(-1);
	m_xml.m_bReadOnly = TRUE;
	if (m_xml.Open(_T("Params")))
	{
		m_pPNode = m_xml.m_pCurrNode;
		POSITION pos = m_pPNode->m_lstChildren.GetHeadPosition();
		for (; pos != NULL; )
		{
			CXMLNode* pNode = m_pPNode->m_lstChildren.GetNext(pos);
			m_params.Add(pNode);
			m_xml.m_pCurrNode = pNode;
			CString strText;
			m_xml.Read(_T("Value"), strText);
			CStringList* pValues = new CStringList;
			m_xml.Read(_T("Values"), *pValues);
			if (pValues->GetCount() == 0)
				pValues->AddHead(strText);
			m_values.Add(pValues);
		}
		m_xml.m_pCurrNode = m_pPNode;
		m_xml.Back();
	}
}

void CULEmd::ClearParamValues()
{
	for (int i = 0; i < m_values.GetSize(); i++)
	{
		CStringList* pValues = (CStringList*)m_values.GetAt(i);
		delete pValues;
	}
	m_values.RemoveAll();
}

BOOL CULEmd::ProcessEmd()
{
	if (m_pEmd == NULL)
		return FALSE;

	if (m_pEmd->InitEmd())
		return FALSE;

	ICurve* m_pDEPT = (ICurve*)GetICurve(_T("DEPT"));
	if (NULL == m_pDEPT)
		return FALSE;

	long iStart = 0;
	long iEnd = m_pDEPT->GetDataSize();
	if (iEnd == 0)
		return TRUE;

	CXMLNode* pCurrNode = m_xml.m_pCurrNode;
	m_xml.Back(-1);
	m_xml.m_bReadOnly = TRUE;

	int iInput = 0;
	int iOutput = 0;
	int iParams = 0;

	CPtrArray pInput;
	CPtrArray pOutput;
	CPtrArray pOutputx;	// Temp output

	if (m_xml.Open(_T("Input")))
	{	
		iInput = m_xml.m_pCurrNode->m_lstChildren.GetCount();
		POSITION pos = m_xml.m_pCurrNode->m_lstChildren.GetHeadPosition();
		for (; pos != NULL; )
		{
			CXMLNode* pNode = m_xml.m_pCurrNode->m_lstChildren.GetNext(pos);
			ICurve* pCurve = (ICurve*)GetICurve(pNode->m_strName);
			pInput.Add(pCurve);
		}
		m_xml.Back();
	}

	if (m_xml.Open(_T("Output")))
	{
		iOutput = m_xml.m_pCurrNode->m_lstChildren.GetCount();
		POSITION pos = m_xml.m_pCurrNode->m_lstChildren.GetHeadPosition();
		for (; pos != NULL; )
		{
			CXMLNode* pNode = m_xml.m_pCurrNode->m_lstChildren.GetNext(pos);
			ICurve* pCurve = (ICurve*)GetOCurve(pNode->m_strName);
			pOutput.Add(pCurve);
			
			CCurve* pNCurve = new CCurve(NB_FILE, VT_R4, NULL, pNode->m_strName);
			pNCurve->m_pData->m_nDirection = SCROLL_DOWN;
			pNCurve->m_pData->MInit();
			pOutputx.Add(pNCurve);
		}
		m_xml.Back();
	}

	if (m_xml.Open(_T("Params")))
	{
		iParams = m_xml.m_pCurrNode->m_lstChildren.GetCount();
		m_xml.Back();
	}

	// To avoid delete error when is too litter
	iInput += 4;
	iOutput += 4;
	iParams += 4;
	
	double* fIVal = new double[iInput];
	ZeroMemory(fIVal, iInput*sizeof(double));
	double* fOVal = new double[iOutput];
	ZeroMemory(fOVal, iOutput*sizeof(double));
	double* pParam = new double[iParams];
	ZeroMemory(pParam, iParams*sizeof(double));

	iInput -= 4;
	iOutput -= 4;
	iParams -= 4;

	long lDepth0 = m_pDEPT->GetDepth(0);
	long lDepthED = m_pDEPT->GetDepth(iEnd-1);
	
	int j, k = 0;
	
	ICurve *pFirstCurve = NULL;
	if(pInput.GetSize() > 0)
	{
		pFirstCurve = (ICurve *)pInput.GetAt(0);
		iStart = 0;
		iEnd = pFirstCurve->GetDataSize();
	}

	BOOL bReplace = FALSE;
	short shReturn = 0; // add by bao
	int iDepthx = -1;
	if (lDepth0 < lDepthED) // Down
	{
		int i = 0;
		for (i = iStart; i < iEnd; i++)
		{
			long lDepth = 0;
			long lTime = 0;

			if(pFirstCurve != NULL)
			{
				lDepth = pFirstCurve->GetDepth(i);
				lTime = pFirstCurve->GetTime(i);
			}
			else
			{
				lDepth = m_pDEPT->GetDepth(i);
				lTime = m_pDEPT->GetTime(i);
			}
			
			int nCount = GetParamsVald(pParam, lDepth);
			if (nCount < 0)
				continue;

			if (iDepthx != m_iDepth)
			{
				k = 0;
				iDepthx = m_iDepth;
			}
			
			for (j = 0; j < iInput; j++)
			{
				ICurve* pCurve = (ICurve*)pInput[j];
				if (pCurve != NULL)
					fIVal[j] = pCurve->GetValueAtDepth(lDepth);
			}
			
			// add by bao
			shReturn = m_pEmd->ProcessPer(fIVal, fOVal, pParam, lDepth, k++);
			if (shReturn == UL_FINISH) // 已经添加数据完成,不需要后续添加OutPut。
				continue;
			if (shReturn == UL_ERROR) // 数据处理错误
				break;
			// end
			
			for (j = 0; j < iOutput; j++)
			{
				CCurve* pNCurve = (CCurve*)pOutputx[j];
			//	if (NULL != pNCurve)
			//		pNCurve->AddData(lDepth, fOVal[j], lTime);
			}
		}

		if (i == iEnd && shReturn != UL_FINISH) // add by bao
			bReplace = TRUE;
	}
	else // Up
	{
		int i = 0;
	//	for (int i = iEnd - 1; i >= iStart; i--)
		for (i = iStart; i < iEnd; i++)
		{
			long lDepth = 0;
			long lTime = 0;

			if(pFirstCurve != NULL)
			{
				lDepth = pFirstCurve->GetDepth(i);
				lTime = pFirstCurve->GetTime(i);
			}
			else
			{
				lDepth = m_pDEPT->GetDepth(i);
				lTime = m_pDEPT->GetTime(i);
			}
			
			int nCount = GetParamsVald(pParam, lDepth);
			if (nCount < 0)
				continue;
			
			if (iDepthx != m_iDepth)
			{
				k = 0;
				iDepthx = m_iDepth;
			}
			
			for (j = 0; j < iInput; j++)
			{
				ICurve* pCurve = (ICurve*)pInput[j];
				if (pCurve != NULL)
					fIVal[j] = pCurve->GetValueAtDepth(lDepth);
			}
			
			// add by bao
			shReturn = m_pEmd->ProcessPer(fIVal, fOVal, pParam, lDepth, k++);
			if (shReturn == UL_FINISH) // 已经添加数据完成,不需要后续添加OutPut。
				continue;
			if (shReturn == UL_ERROR) // 数据处理错误
				break;						   // 同样不需要管OutPut数据
			// end
			
			for (j = 0; j < iOutput; j++)
			{
				CCurve* pNCurve = (CCurve*)pOutputx[j];
			//	if (NULL != pNCurve)
			//		pNCurve->AddData(lDepth, fOVal[j], lTime);
			}
		}

		if (i == (iStart - 1) && shReturn != UL_FINISH) // add by bao
			bReplace = TRUE;
	}
	
	if (bReplace)
	{
		int nDirection = m_pDEPT->Direction();
		int iDepth = m_depths.GetSize() - 1;
		for (j = 0; j < iOutput; j++)
		{
			ICurve* pCurve = (ICurve*)pOutput[j];
			CCurve* pNCurve = (CCurve*)pOutputx[j];
			for (int k = 1; k < iDepth; k+=2)
				pCurve->ReplaceData(pNCurve, m_depths[k], m_depths[k+1]);
			
			if (pCurve->Direction() != nDirection)
			{
				TRACE("Direction is diffient.\n");
			}
		}
	}

	for (j = 0; j < iOutput; j++)
	{
		CCurve* pNCurve = (CCurve*)pOutputx[j];
		pNCurve->Release();
	}

	m_pEmd->TermEmd();
	
	delete [] fIVal;
	delete [] fOVal;
	delete [] pParam;

	return TRUE;
}

BOOL CULEmd::InitList(CBCGPListCtrl* pList, LPCTSTR pszKey)
{
	m_xml.m_bReadOnly = TRUE;
	m_xml.Back(-1);
	if (m_xml.Open(pszKey))
	{
		pList->DeleteAllItems();
		CXMLNode* pCurr = m_xml.m_pCurrNode;
		POSITION pos = pCurr->m_lstChildren.GetHeadPosition();
		for (int i = 0; pos != NULL; i++)
		{
			CXMLNode* pNode = pCurr->m_lstChildren.GetNext(pos);
			m_xml.SetCurrNode(pNode);
			CString strText;
			if (_tcscmp(pszKey, _T("Params")))
			{
				pList->InsertItem(i, pNode->m_strName);
				CString strReset;
				m_xml.Read(_T("Reset"), strReset);
				if (strReset.GetLength())
				{
					if (pList->m_lstOptions.GetCount())
					{
						if (pList->m_lstOptions.Find(strReset))
							pList->SetItemText(i, 1, strReset);
						else if (pList->m_lstOptions.Find(pNode->m_strName))
							pList->SetItemText(i, 1, pNode->m_strName);
					}
					else
					{
						pList->SetItemText(i, 1, strReset);
					}
				}
				else
				{
					if (pList->m_lstOptions.GetCount())
					{
						if (pList->m_lstOptions.Find(pNode->m_strName))
							pList->SetItemText(i, 1, pNode->m_strName);
					}
					else
					{
						pList->SetItemText(i, 1, pNode->m_strName);
					}
				}
				m_xml.Read(_T("Remark"), strText);
				pList->SetItemText(i, 2, strText);
			}
			else
			{
				strText.Format(_T("%d"), i);
				pList->InsertItem(i, strText);
				pList->SetItemText(i, CDlgEmdParam::pname, pNode->m_strName);
				if (m_xml.Read(_T("Value"), strText))
				{
					pList->SetItemText(i, CDlgEmdParam::ppvalue, strText);
					// pList->SetItemText(i, CDlgEmdParam::pvalue, strText);
				}

				if (m_xml.Read(_T("Unit"), strText))
					pList->SetItemText(i, CDlgEmdParam::punit, strText);
				if (m_xml.Read(_T("Desc"), strText))
					pList->SetItemText(i, CDlgEmdParam::pcomment, strText);
			}
			
		}
		m_xml.m_pCurrNode = pCurr;
		m_xml.Back();
	}

	if (m_bLock)
	{
		if (_tcscmp(pszKey, _T("Params")))
		{
			pList->EnableEditColumn(0, FALSE);
		}
		else if (pList->GetItemCount())
		{
			pList->EnableEditColumn(CDlgEmdParam::pname, FALSE);
			pList->EnableEditColumn(CDlgEmdParam::punit, FALSE);
		}
	}
	
	return m_bLock;
}

void CULEmd::InitDepths(CListBox* pList)
{
	pList->ResetContent();
	int n = m_depths.GetSize() - 1;
	for (int i = 1; i < n; i++)
	{
		CString strText;
		//strText = g_units->FormatLM(_T("%.4lf~"), m_depths[i++]);
		//strText += g_units->FormatLM(_T("%.4lf"), m_depths[i]);
		strText = AfxGetUnits()->FormatLM(_T("%.4lf~"), m_depths[i++]);
		strText += AfxGetUnits()->FormatLM(_T("%.4lf"), m_depths[i]);
		pList->AddString(strText);
	}
}

int CULEmd::InsertDepth(CListBox* pList, int iDepth)
{
	ASSERT(iDepth < m_depths.GetSize()/2);
	
	// insert values first
	for (int i = 0; i < m_values.GetSize(); i++)
	{
		CStringList* pValues = (CStringList*)m_values.GetAt(i);
		POSITION pos = pValues->FindIndex(iDepth+1);
		if (pos != NULL)
		{
			// we set it empty to use the prevalue
			CString strVal = _T("");
			// CString strVal = pValues->GetAt(pos);
			pValues->InsertAfter(pos, strVal);
		}
	}

	iDepth *= 2;
	long lDepth0 = m_depths[++iDepth];
	long lDepth2 = m_depths[++iDepth];
	long lDepth1 = (lDepth0+lDepth2)/2;
	
	m_depths.InsertAt(iDepth, lDepth1);
	m_depths.InsertAt(iDepth, lDepth1);

	InitDepths(pList);
	return ((iDepth-1)/2);
}

int CULEmd::SetDepth(CListBox* pList, int iDepth, long lDepth0, long lDepth1)
{
	iDepth *= 2;
	m_depths[++iDepth] = lDepth0;
	m_depths[++iDepth] = lDepth1;
	InitDepths(pList);
	return ((iDepth-1)/2);
}

int CULEmd::SetDepth(CListBox* pList, int iDepth, long lDepth0, long lDepth1, DWORD dwSet)
{
	iDepth *= 2;
	++iDepth;
	if (dwSet & 0x01)
	{
		if (lDepth0 < m_depths[iDepth-1])
		{
			if (lDepth0 < m_depths[0])
				lDepth0 = m_depths[0];

			int iDepthx = iDepth;
			do
			{
				m_depths[iDepthx] = lDepth0;
			}
			while (lDepth0 < m_depths[--iDepthx]);
		}
		else if (lDepth0 > m_depths[iDepth+1])
		{
			int iEnd = m_depths.GetSize() - 1;
			if (lDepth0 > m_depths[iEnd])
				lDepth0 = m_depths[iEnd];

			int iDepthx = iDepth;
			do 
			{
				m_depths[iDepthx] = lDepth0;
			}
			while (lDepth0 > m_depths[++iDepthx]);
		}
		else
		{
			m_depths[iDepth] = lDepth0;
			//m_depths[iDepth-1] = lDepth0;
		}
	}

	++iDepth;
	if (dwSet & 0x02)
	{
		if (lDepth1 < m_depths[iDepth-1])
		{
			if (lDepth1 < m_depths[0])
				lDepth1 = m_depths[0];

			int iDepthx = iDepth;
			do
			{
				m_depths[iDepthx] = lDepth1;
			}
			while (lDepth1 < m_depths[--iDepthx]);
		}
		else if (lDepth1 > m_depths[iDepth+1])
		{
			int iEnd = m_depths.GetSize() - 1;
			if (lDepth1 > m_depths[iEnd])
				lDepth1 = m_depths[iEnd];

			int iDepthx = iDepth;
			do 
			{
				m_depths[iDepthx] = lDepth1;
			}
			while (lDepth1 > m_depths[++iDepthx]);
		}
		else
		{
			m_depths[iDepth] = lDepth1;
			//m_depths[iDepth+1] = lDepth1;
		}
	}
	InitDepths(pList);
	return ((iDepth-1)/2);
}

int CULEmd::DeleteDepth(CListBox* pList, int iDepth)
{
	if (pList != NULL)
	{
		if (m_depths.GetSize() <= 4)
			return 0;
	}
	
	// delete values first
	for (int i = 0; i < m_values.GetSize(); i++)
	{
		CStringList* pValues = (CStringList*)m_values.GetAt(i);
		POSITION pos = pValues->FindIndex(iDepth+1);
		if (pos != NULL)
		{
			pValues->RemoveAt(pos);
		}
	}

	// Delete depths
	iDepth *= 2;
	++iDepth;

	if (iDepth < m_depths.GetSize() - 2)
	{
		long lDepth = m_depths[iDepth];
		m_depths.RemoveAt(iDepth, 2);
		if (iDepth == (m_depths.GetSize() - 1))
		{
			lDepth = m_depths[iDepth];
			m_depths[--iDepth] = lDepth;
		}
		else
			m_depths[iDepth] = lDepth;
	}
	
	if (pList->GetSafeHwnd())
		InitDepths(pList);
	
	if (iDepth == (m_depths.GetSize()-1))
		return 0;
	
	return ((iDepth-1)/2);
}

void CULEmd::GetDepthPart(int iDepth, long& lDepth0, long& lDepth1, long& lDepthA0, long& lDepthA1)
{
	int i = iDepth*2 + 1;
	lDepth0 = m_depths[i];
	lDepthA0 = m_depths[(i > 0) ? (i-1) : i];
	lDepth1 = m_depths[++i];
	int j = m_depths.GetSize()-1;
	lDepthA1 = m_depths[(i < j) ? (i+1) : i];
}

void CULEmd::SaveParam(CBCGPListCtrl* pList, int iDepth)
{
	for (int i = 0; i < pList->GetItemCount(); i++)
	{
		CStringList* pValues = (CStringList*)m_values.GetAt(i);
		POSITION pos = pValues->FindIndex(iDepth+1);
		if (pos != NULL)
		{
			pValues->SetAt(pos, pList->GetItemText(i, CDlgEmdParam::pvalue));
		}
		else
		{
			pValues->AddTail(pList->GetItemText(i, CDlgEmdParam::pvalue));
		}
	}
}

void CULEmd::InitParam(CBCGPListCtrl* pList, int iDepth)
{
	if (m_values.GetSize() == 0)
		InitParamValues();
	
	for (int i = 0; i < pList->GetItemCount(); i++)
	{
		CStringList* pValues = (CStringList*)m_values.GetAt(i);
		POSITION pos = pValues->FindIndex(iDepth+1);
		if (pos != NULL)
		{
			CString strText = pValues->GetPrev(pos);
			pList->SetItemText(i, CDlgEmdParam::pvalue, strText);
			while (pos != NULL)
			{
				strText = pValues->GetPrev(pos);
				if (strText.GetLength())
					break;
			}
			pList->SetItemText(i, CDlgEmdParam::ppvalue, strText);
		}
		else
		{
			CString strText = pValues->GetHead();
			// pList->SetItemText(i, CDlgEmdParam::pvalue, strText);
			pList->SetItemText(i, CDlgEmdParam::ppvalue, strText);
		}
	}
}

void CULEmd::SaveList(CBCGPListCtrl* pList, LPCTSTR pszKey)
{
	m_xml.m_bReadOnly = FALSE;
	m_xml.Back(-1);
	if (m_xml.CreateKey(pszKey))
	{
		if (_tcscmp(pszKey, _T("Params")))
		{
			for (int i = 0; i < pList->GetItemCount(); i++)
			{
				CString strText = pList->GetItemText(i, 0);
				if (strText.IsEmpty())
					continue;
				
				
				if (m_xml.CreateKey(strText))
				{
					m_xml.Write(_T("Reset"), pList->GetItemText(i, 1));
					m_xml.Write(_T("Remark"), pList->GetItemText(i, 2));
					m_xml.Back();
				}
			}
		}
		else
		{
			for (int i = 0; i < pList->GetItemCount(); i++)
			{
				CString strText = pList->GetItemText(i, 1);
				if (strText.IsEmpty())
					continue;
				
				if (m_xml.CreateKey(strText))
				{
					m_xml.Write(_T("Value"), pList->GetItemText(i, CDlgEmdParam::pvalue));
					m_xml.Write(_T("Unit"), pList->GetItemText(i, CDlgEmdParam::punit));
					m_xml.Write(_T("Desc"), pList->GetItemText(i, CDlgEmdParam::pcomment));
					m_xml.Back();
				}
			}
		}
		
		m_xml.Back();
	}
}

BOOL CULEmd::SaveParams(LPCTSTR pszFile)
{
	m_xml.m_bReadOnly = FALSE;
	m_xml.Back(-1);
	LPBYTE pByte = (LPBYTE)m_depths.GetData();
	pByte += sizeof(long);
	UINT nSize = (m_depths.GetSize()-2)*sizeof(long);
	if (nSize > 0)
	{
		m_xml.Write(_T("Depths"), pByte, nSize);
	}
	SaveParamValues();

	return m_xml.WriteXMLToFile(pszFile);
}

BOOL CULEmd::LoadParams(LPCTSTR pszFile)
{
	m_xml.m_bReadOnly = TRUE;
	if (m_xml.ReadXMLFromFile(pszFile))
	{
		long lDepth0, lDepth1;
		GetDepthRange(lDepth0, lDepth1);

		if (m_depths.GetSize() > 2)
		{
			m_depths.RemoveAt(1, m_depths.GetSize() - 2);
			m_depths[0] = lDepth0;
			m_depths[1] = lDepth1;
		}
		else
		{
			m_depths.RemoveAll();
			m_depths.Add(lDepth0);
			m_depths.Add(lDepth1);
		}

		LPBYTE pByte = NULL;
		UINT nSize = 0;
		m_xml.Read(_T("Depths"), &pByte, &nSize);
		nSize /= sizeof(long);
		if (pByte != NULL)
		{
			for (int i = nSize - 1; i > -1; i--)
				m_depths.InsertAt(1, ((long*)pByte)[i]);
			delete[] pByte;
		}
		else
		{
			if (m_depths.GetSize() < 3)
			{
				m_depths.InsertAt(1, lDepth1);
				m_depths.InsertAt(1, lDepth0);
			}
		}
		
		InitParamValues();
		
		// We modify the depths range
		int nDepth = m_depths.GetSize();
		if (nDepth > 3)
		{
			// Modify the depths force in range
			int i = 1;
			for ( ; i < nDepth; i++)
			{
				if (m_depths[0] > m_depths[i])
				{
					m_depths[i] = m_depths[0];
				}
				else
					break;
			}

			i--;
			for (int j = nDepth - 2; j > i; j--)
			{
				if (m_depths[nDepth-1] < m_depths[j])
				{
					m_depths[j] = m_depths[nDepth-1];
				}
				else
					break;
			}

			// Delete the depths not in range
			/*
			while (m_depths[0] > m_depths[2])
			{
				DeleteDepth(NULL, 0);
				nDepth = m_depths.GetSize();
				if (nDepth < 3)
					break;
			}

			if (nDepth > 2)
			{
				while (m_depths[nDepth-1] < m_depths[nDepth-3])
				{
					DeleteDepth(NULL, nDepth/2-2);
					nDepth = m_depths.GetSize();
					if (nDepth < 3)
						break;
				}
			}

			nDepth = m_depths.GetSize();
			if (nDepth > 3)
			{
				if (m_depths[1] < m_depths[0])
					m_depths[1] = m_depths[0];
				
				if (m_depths[nDepth-2] > m_depths[nDepth-1])
					m_depths[nDepth-2] = m_depths[nDepth-1];
			}
			else
			{
				m_depths.RemoveAll();
				m_depths.Add(lDepth0);
				m_depths.Add(lDepth0);
				m_depths.Add(lDepth1);
				m_depths.Add(lDepth1);
			}
			*/
		}
		
		return TRUE;
	}
	return FALSE;
}

BOOL CULEmd::SaveDefault()
{
	if (m_strEmd.IsEmpty())
		return FALSE;

// 	CString strDefault = m_strEmd.Left(m_strEmd.GetLength() - 4);
// 	strDefault += _T(".xml");

	m_xml.m_bReadOnly = FALSE;

	// when save default, don't save depth range

	return m_xml.WriteXMLToFile(m_strDef);
}

int CULEmd::GetDepthIndex(long lDepth)
{
	int n = m_depths.GetSize()-1;
	for (int i = 1; i < n; i++)
	{
		if (lDepth < m_depths[i])
		{
			if ((i % 2) == 1)
			{
				return -1;
			}
			return (i/2-1);
		}
		else if ((lDepth == m_depths[i]) && (i == (n-1)))
			return (i/2-1);
	}

	return -1;
}

void CULEmd::GetDepthRange(long& lDepth0, long& lDepth1)
{
	if (m_pULFile == NULL)
		return;

	ICurve* pCurve = (ICurve*)m_pULFile->GetCurve(0);
	if (pCurve == NULL)
		return;

/*	lDepth0 = 0;
	lDepth1 = MTD(10000);
*/	
	int nData = pCurve->GetDataSize();
	if (nData > 0)
	{
		lDepth0 = pCurve->GetDepth(0);
		lDepth1 = pCurve->GetDepth(nData-1);
		if (lDepth0 > lDepth1)
		{
			long l = lDepth0;
			lDepth0 = lDepth1;
			lDepth1 = l;
		}
	}
}

ULMINTIMP CULEmd::GetDepthsCount()
{
	return m_depths.GetSize();
}

ULMINTIMP CULEmd::GetDepths(long* pDepths)
{
	UINT nBytes = m_depths.GetSize()*sizeof(long);
	if (AfxIsValidAddress(pDepths, sizeof(long)))
	{
		memcpy(pDepths, m_depths.GetData(), nBytes);
	}
	return m_depths.GetSize();
}

ULMINTIMP CULEmd::GetParamsVal(double* pValues, int iStart /* = 0 */, int nCount /* = -1 */)
{
	m_xml.m_bReadOnly = TRUE;
	if (iStart < 0)
		iStart = 0;
	int iEnd = iStart + nCount;
	if ((nCount < 0) || (iEnd > m_params.GetSize()))
		iEnd = m_params.GetSize();
	
	nCount = iEnd - iStart;
	UINT nBytes = nCount*sizeof(double);
	if (AfxIsValidAddress(pValues, nBytes))
	{
		int j = 0;
		for (int i = iStart; i < iEnd; i++)
		{
			m_xml.m_pCurrNode = (CXMLNode*)m_params.GetAt(i);
			CStringList strText;
			if (m_xml.Read(_T("Values"), strText))
			{
				if (strText.GetCount())
				{
					LPTSTR stop;
					pValues[j++] = _tcstod(strText.GetHead(), &stop);
				}
			}
			else
				pValues[j++] = 0.0;
		}

		return nCount;
	}
	return 0;
}

ULMINTIMP CULEmd::GetParamsVald(double* pValues, long lDepth, int iStart /* = 0 */, int nCount /* = -1 */)
{
	m_iDepth = GetDepthIndex(lDepth);

	// when the lDepth is not in range, we return -1, don't calculate
	if (m_iDepth < 0)
		return -1;
/*
	// when the lDepth is not in range, we return the default value
	if (iDepth < 0)
		return GetParamsVal(pValues, iStart, nCount);
*/
	if (iStart < 0)
		iStart = 0;
	int iEnd = iStart + nCount;
	if ((nCount < 0) || (iEnd > m_params.GetSize()))
		iEnd = m_params.GetSize();
	
	nCount = iEnd - iStart;
	UINT nBytes = nCount*sizeof(double);
	if (AfxIsValidAddress(pValues, nBytes))
	{
		int j = 0;
		for (int i = iStart; i < iEnd; i++)
		{
			CStringList* pValuesi = (CStringList*)m_values.GetAt(i);
			POSITION pos = pValuesi->FindIndex(m_iDepth+1);
			int k = 1;
			while (pos != NULL)
			{
				CString strText = pValuesi->GetPrev(pos);
				if (strText.GetLength())
				{
					LPTSTR stop;
					pValues[j++] = _tcstod(strText, &stop);
					k = 0;
					break;
				}
			}
			if (k)
				pValues[j++] = GetParamVal(i);
		}

		return nCount;
	}

	return 0;
}

ULMINTIMP CULEmd::DefICurves(int nCount, CString* pICurves, CString* pRemarks)
{
	if (!m_def.CreateKey(_T("Input")))
		return 0;

	try
	{
		for (int i = 0; i < nCount; i++)
		{
			if (m_def.CreateKey(pICurves[i]))
			{
				if (pRemarks != NULL)
					m_def.Write(_T("Remark"), pRemarks[i]);
				m_def.Back();
			}
		}
	}
	catch (CException* e)
	{
		e->ReportError();
		e->Delete();
	}
	catch (...)
	{
	}

	m_def.Back();
	return 0;
}

ULMINTIMP CULEmd::DefOCurves(int nCount, CString* pOCurves, CString* pRemarks)
{
	if (!m_def.CreateKey(_T("Output")))
		return 0;

	try
	{
		for (int i = 0; i < nCount; i++)
		{
			if (m_def.CreateKey(pOCurves[i]))
			{
				if (pRemarks != NULL)
					m_def.Write(_T("Remark"), pRemarks[i]);
				m_def.Back();
			}
		}
	}
	catch (CException* e)
	{
		e->ReportError();
		e->Delete();
	}
	catch (...)
	{
	}

	m_def.Back();
	return 0;
}

ULMINTIMP CULEmd::DefParams(int nCount, CString* pParams, double* pValues, CString* pUnits, CString* pDescs)
{
	if (!m_def.CreateKey(_T("Params")))
		return 0;

	try
	{
		for (int i = 0; i < nCount; i++)
		{
			if (m_def.CreateKey(pParams[i]))
			{
				if (pValues != NULL)
				{
					TCHAR szVal[32];
					_stprintf(szVal, _T("%.*g"), DBL_DIG, pValues[i]);
					m_def.Write(_T("Value"), szVal);
				}

				if (pUnits != NULL)
					m_def.Write(_T("Unit"), pUnits[i]);
				if (pDescs != NULL)
					m_def.Write(_T("Desc"), pDescs[i]);
				m_def.Back();
			}
		}
	}
	catch (CException* e)
	{
		e->ReportError();
		e->Delete();
	}
	catch (...)
	{
	}

	m_def.Back();
	return 0;
}

BOOL CULEmd::UserDefinedProcess()
{
	if (m_bUserProcess)
	{
		if (m_pEmd == NULL)
			return FALSE;
		
		if (m_pEmd->InitEmd())
			return FALSE;
		
		int iParams = 0;
		
		CXMLNode* pCurrNode = m_xml.m_pCurrNode;
		m_xml.Back(-1);
		m_xml.m_bReadOnly = TRUE;

		if (m_xml.Open(_T("Params")))
		{
			iParams = m_xml.m_pCurrNode->m_lstChildren.GetCount();
			m_xml.Back();
		}
		m_xml.SetCurrNode(pCurrNode);

		// To avoid delete error when is too litter
		double* pParam = new double[iParams];
		ZeroMemory(pParam, iParams*sizeof(double));
		
		iParams -= 4;
		GetParamsVal(pParam);

		m_pEmd->ProcessPer(NULL, NULL, pParam, 0, 0);

		m_pEmd->TermEmd();
		delete [] pParam;
		
		return TRUE;
	}

	return FALSE;
}

ICurve *CULEmd::GetICurve(UINT nIndex)
{
	ICurve *pCurve = NULL;

	CStringList CurvesName;
	GetICurvesName(&CurvesName);

	if(CurvesName.GetCount() <= nIndex)
		return pCurve;

	POSITION pos = CurvesName.GetHeadPosition();
	CString strCurveName;
	for(int i = 0 ; pos != NULL; i++)
	{
		strCurveName = CurvesName.GetNext(pos);
		if(i = nIndex)
			break;
	}
	
	pCurve = (ICurve *)GetICurve(strCurveName);

	return pCurve;
}
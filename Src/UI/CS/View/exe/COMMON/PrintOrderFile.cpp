// PrintOrderFile.cpp: implementation of the CPof class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#ifdef _LOGIC
#include "Logic.h"
#else
#include "ul2000.h"
#endif

#include "PrintOrderFile.h"
#include "ULCOMMDEF.H"
#include "PrintOrderDlg.h"
#include "GraphHeaderView.h"
#include "ChildFrm.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


CPofInfo::~CPofInfo()
{
	if (m_pFrame->GetSafeHwnd())
	{
		((CChildFrame*)m_pFrame)->m_bDestroy = TRUE;
		m_pFrame->SendMessage(WM_CLOSE);
		m_pFrame = NULL;
	}

	if (pPof != NULL)
		delete pPof;
}

void CPrintParam::Serialize(CXMLSettings& xml)
{
	if (xml.IsStoring())
	{
		xml.Write("Fx", m_nFx);
		xml.Write("Type", m_dwType);
		xml.Write("PInt", m_nParam);
		xml.Write("PStr", m_strParam);
		xml.Write("PBool", m_bParam);
		xml.Write("PLng", m_lParam);
		xml.Write("PLng2", m_lParam2);
	}
	else
	{
		xml.Read("Fx", m_nFx);
		xml.Read("Type", m_dwType);
		xml.Read("PInt", m_nParam);
		xml.Read("PStr", m_strParam);
		if (m_strParam.Right(4).Left(3).CompareNoCase(".ul"))
		{
			m_strParam += ".ulh";
		}
		
		xml.Read("PBool", m_bParam);
		xml.Read("PLng", m_lParam);
		xml.Read("PLng2", m_lParam2);
	}
}

void CPof::PreIndex(CPtrArray* pParamArray)
{
	int gIndex[10];
	int cIndex[10];
	ZeroMemory(gIndex, sizeof(int)*10);
	ZeroMemory(cIndex, sizeof(int)*10);
	int nCount = pParamArray->GetSize();
	for (int i = 0; i < nCount; i++)
	{
		CPrintParam* pParam = (CPrintParam*)pParamArray->GetAt(i);
		if ((pParam == NULL) || (pParam->m_nFx == -1))
			continue;

		if (pParam->m_dwType & ULV_GRAPHS)
		{
			int j = (pParam->m_nFx < 0) ? 0 : pParam->m_nFx;
			pParam->m_nParam = gIndex[j%10]++;
		}
		else if (pParam->m_dwType & ULV_CUSTOMIZEVIEW)
		{
			int j = (pParam->m_nFx < 0) ? 0 : pParam->m_nFx;
			pParam->m_nParam = cIndex[j%10]++;
		}
	}
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPof::CPof()
{
	m_strFile.Empty();
}

CPof::~CPof()
{
	ClearParams();
}

BOOL CPof::SaveOrder(LPCTSTR lpszFile)
{
	CXMLSettings xml(FALSE, "POF1.0");
	for (UINT i = 0; i < m_ParamArray.GetSize(); i++)
	{
		CPrintParam* pParam = (CPrintParam*)m_ParamArray.GetAt(i);
		if (pParam == NULL)
			continue ;
		
		if (xml.CreateKey("Paramx%03u", i))
		{
			pParam->Serialize(xml);
			xml.Back();
		}
	}

	return xml.WriteXMLToFile(lpszFile);
}

BOOL CPof::LoadOrder(LPCTSTR lpszFile)
{
	ClearParams();

	m_strFile = lpszFile;
	if (m_strFile.IsEmpty())
		return FALSE;

	CXMLSettings xml;
	if (xml.ReadXMLFromFile(m_strFile))
	{
		UINT n = xml.GetTree()->m_lstChildren.GetCount();
		UINT c = 2*n;
		for (UINT i = 0; i < c; i++)
		{
			if (xml.Open("Paramx%03u", i))
			{
				CPrintParam* pParam = new CPrintParam;
				pParam->Serialize(xml);
				if (pParam->m_dwType == ULV_GRAPH)
					pParam->m_bParam = FALSE;
				m_ParamArray.Add(pParam);
				xml.Back();
				if (--n == 0)
					break;
			}
		}

		PreIndex(&m_ParamArray);
		return TRUE;
	}

	m_strFile.Empty();
	AfxMessageBox(IDS_FAIL_PRINTFILE);
	return FALSE;
}

void CPof::AddHead(CStringList* pList, CCellInfoList* pCellList)
{
	for (int i = m_ParamArray.GetSize() - 1; i > -1; i--)
	{
		CPrintParam* pParam = (CPrintParam*)m_ParamArray.GetAt(i);
		if ((pParam->m_dwType == ULV_GRAPHHEAD) && pParam->m_strParam.GetLength())
		{
			if (pList->Find(pParam->m_strParam))
				continue ;

			pList->AddTail(pParam->m_strParam);
			CCellInfo* pCellInfo = new CCellInfo;
			pCellInfo->strTempl = pParam->m_strParam;
			pCellInfo->strFile = HTS_FILE(pParam->m_strParam);
			pCellInfo->m_pFrame = NULL;
			pCellList->AddTail(pCellInfo);
		}
	}
}

int CPof::GetOrderCount()
{
	return m_ParamArray.GetSize();
}

void CPof::ClearParams()
{
	for (int i = 0; i<m_ParamArray.GetSize(); i++)
	{
		CPrintParam* pParam = (CPrintParam*)m_ParamArray.GetAt(i);
		if (pParam)
		{
			delete pParam;
			pParam = NULL;
		}
	}
	m_ParamArray.RemoveAll();
}

void CPof::InitList(CListCtrl* pList)
{
	LV_ITEM item;
	item.iSubItem = 0;
	item.mask = LVIF_TEXT|LVIF_PARAM;
	int nCount = m_ParamArray.GetSize();
	for (int i = 0; i < nCount; i++)
	{
		CPrintParam* pParam = (CPrintParam*)m_ParamArray.GetAt(i);
		if (pParam == NULL)
			continue ;
		
		UINT nID = 0;
		switch (pParam->m_dwType)
		{
		case ULV_GRAPH:
			nID = IDS_LOG_PLOT;
			break;
		case ULV_GRAPHHEAD:
			nID = ULV_GRAPHHEAD;
			break;
		case ULV_CALCHART:
			nID = IDS_CALIBRATE_CHART;
			break;
		case ULV_CALUSER:
			nID = IDS_CALIBRATE_USER;
			break;
		case ULV_HOLLOW:
			nID = IDS_BLANK_PAGE;
			break;
		case ULV_TOOLS:
			nID = IDS_TOOL_SHAPE;
			break;
		case ULV_PARAM:
			nID = IDS_TOOL_PARA_TABLE;
			break;
		case ULV_TABLEINFOVIEW:
			nID=IDS_TABLEINFOVIEW;
			break;
		case ULV_TOOLINFOVIEW:
			nID=IDS_TOOLINFOVIEW;
			break;
		case ULV_OUTPUTSTABLE:
			nID=IDS_OUTPUTSTABLE;
			break;
		case ULV_CONSTRUCT:
			nID = IDS_WELL_SCHEMATIC;
			break;	
		case ULV_FACTOR:
			nID = IDS_CURVE_FACTOR_TABLE;
			break;
		case ULV_CALSUMMARY:
			nID = IDS_CALIBRATE_SUMMARY;
			break;
		case ULV_CALCOEF:
			nID = IDS_CALCOEF_REPORT;
			break;
		case ULV_SCHEDULE:
			nID = IDS_PERFORATION_SCHEDULE;
			break;
// 2020.3.9 Ver1.6.0 TASK��002�� Start
//		case ULV_RFT_DATAGROUP:
//			nID = ULV_RFT_DATAGROUP;
//			break;
//		case ULV_RFT_PROFILEGROUP:
//			nID = ULV_RFT_PROFILEGROUP;
//			break;
// 2020.3.9 Ver1.6.0 TASK��002�� End
		case ULV_CUSTOMIZEVIEW:
			nID = IDS_CUSTOMIZE;
			break;
		case ULV_CHARTS:
			nID = IDS_TVD_GRAPH;
			break;
		case ULV_RUNDATA:
			nID = IDS_RUNDATAVIEW;
			break;								
		}
		
		if (nID == 0)
			continue;
		
		CString str;
		if (nID == ULV_GRAPHHEAD )
		{
			int iLen = pParam->m_strParam.GetLength();
			if (iLen > 4)
				str = pParam->m_strParam.Left(iLen - 4);
		}
// 2020.3.9 Ver1.6.0 TASK��002�� Start
//		else if(nID == ULV_RFT_DATAGROUP)
//		{
//			str = "Rft_Data";
//		}
//		else if(nID == ULV_RFT_PROFILEGROUP)
//		{
//			str = "Rft_Profile";
//		}
//		else
//			str.LoadString(nID);
// 2020.3.9 Ver1.6.0 TASK��002�� End		
		CString strF = _T("Fx");
		if (pParam->m_nFx >= 0)
			strF.Format("F%d", pParam->m_nFx);
		else if (pParam->m_nFx == -1)
			strF = _T("Fx");
		else if (pParam->m_nFx == -2)
			strF = _T("Fn");
		
		item.iItem = pList->GetItemCount();
		item.pszText = (LPSTR)(LPCTSTR)strF;
		item.lParam = (LPARAM)pParam;
		pList->InsertItem(&item);
		pList->SetItemText(item.iItem, 1, str);
	}
}

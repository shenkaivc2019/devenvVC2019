// Mark.cpp: implementation of the CMark class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "Mark.h"
#include "Sheet.h"
#include "Utility.h"
#include "XMLSettings.h"

#ifdef _PERFORATION
#include "Perforation.h"
#endif

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
#define FIRE_LINE_WIDTH 6
CMark::CMark()
{
	m_strName="MARK";
	m_nShape = MARKS_HORIZONTAL_LINE;
	m_nPointer = 0;
	m_lArrowPos = 0;
	m_bHot = FALSE;
	m_bLock = FALSE;
	m_rcMark.SetRectEmpty();
	m_crColor = RGB(0, 0, 255);
	m_crText = RGB(0, 0, 255);
	m_nTrackNO = -1;
	m_bDepthChange = TRUE;
	m_fLimit  = 0;
	m_fLValue = 0;
	m_fRValue = 0;
	m_bWatch = FALSE;

	m_nCollarIndex = -1;
	m_nStdCollarIndex = SC_ERR;
	m_lDepthAdjust = 0;
}

CMark::CMark(CMark* pMark,CTrack* pTrack/* = NULL*/)
{
	m_lDepthAdjust = pMark->m_lDepthAdjust;
	m_strName = pMark->m_strName;			// 标记名称
	
	m_pTrack = pTrack;			// 标记井道
	m_pCurve = NULL;			// 标记曲线
	m_lDepth = pMark->m_lDepth;			// 标记深度
	m_lArrowPos = pMark->m_lArrowPos;
	m_rect = pMark->m_rect;
	
	m_nPointer = pMark->m_nPointer;			// 标记指向	0左，1右
	m_nShape = pMark->m_nShape; 	 		// 标记形状
	m_rcMark = pMark->m_rcMark;			// 标记范围
	m_crColor = pMark->m_crColor;          // 标记颜色
	m_crText = pMark->m_crText;			// 标记文本颜色
	
	m_strText = pMark->m_strText;			// 标记说明
	m_nTrackNO = pMark->m_nTrackNO;			// 通道号 
	
	m_bHot = pMark->m_bHot;				// 标记选中
	m_bLock = pMark->m_bLock;
	
	m_bDepthChange = pMark->m_bDepthChange;		// 拖动标签时名称是否改变
	m_fMarkXCoordinate = pMark->m_fMarkXCoordinate;	// 标记的横坐标
	m_fXScale = pMark->m_fXScale;
	
	m_nCollarIndex = pMark->m_nCollarIndex;		// 节箍索引
	m_nStdCollarIndex = pMark->m_nStdCollarIndex;	// 第n枪的标箍号
	m_fLimit = pMark->m_fLimit;           // 垂直线的位置
	m_fLValue = pMark->m_fLValue;          // CCL曲线的左值
	m_fRValue = pMark->m_fRValue;          // CCL曲线的右值
	
	m_bWatch = pMark->m_bWatch;           // 监视型标签
	m_dwTime = pMark->m_dwTime;           // 标记出现的时间	
}

CMark::~CMark()
{
	//TRACE("I have Destroyed\n");
}

/* 函数名称：Draw
 * 函数描述：绘制标签
 * 返回类型：void
 * 函数参数：
 */
void CMark::Draw(CDC* pDC, long lStartDepth, long lEndDepth)
{
	// TRACE(_T("%ld,%ld,%ld\n"),lStartDepth,m_lDepth,lEndDepth);
	if(m_lDepth < lStartDepth ||  m_lDepth > lEndDepth)
		return ;

	CPen pen(PS_SOLID, 1, m_crColor);
	CPen hotPen(PS_SOLID, 5, m_crColor);
	CPen* pOrdPen = pDC->SelectObject(&pen);

	CFont myFont,*pOldFont;
	myFont.CreatePointFont(100,"Arial",pDC);
	pOldFont = pDC->SelectObject(&myFont);

	pDC->SetBkMode(TRANSPARENT);
	//COLORREF clr = pDC->SetTextColor(m_crText);

	CRect rcTrack;
	m_pTrack->GetRegionRect(&rcTrack, FALSE);

	int nLeftMargin = rcTrack.left;
	int nRightMargin = rcTrack.right;

	int nPos = m_pTrack->GetCoordinateFromDepth(m_lDepth);
	m_rcMark.top = nPos + 1;
	m_rcMark.bottom = nPos - 1;
	
	CSize szText = pDC->GetTextExtent(m_strName);
	CBrush brushRed(m_crColor);
	CBrush* pOldBrush = pDC->SelectObject(&brushRed);
	switch(m_nShape)
	{
	case MARKS_PERFORATION_COLLOR:
		{
			m_rcMark.left  = nLeftMargin;
			m_rcMark.right = nRightMargin;

			CPen pen(PS_SOLID, 1, RGB(0,0,0));
			CPen* pOldPen = pDC->SelectObject(&pen);
			pDC->MoveTo(nLeftMargin , nPos );
			pDC->LineTo(nRightMargin , nPos);
			pDC->SelectObject(pOldPen);
	
			pDC->TextOut(nLeftMargin + 10 , nPos + szText.cy, m_strName);

			if (m_nCollarIndex != CI_ERR && m_nStdCollarIndex == SC_ERR)
			{
				CPen pen(PS_SOLID, 1, RGB(0,0,0));
				CPen* pOldPen = pDC->SelectObject(&pen);
				pDC->MoveTo(nLeftMargin , nPos );
				pDC->LineTo(nRightMargin , nPos);
				pDC->SelectObject(pOldPen);
				pDC->TextOut(nLeftMargin + 10 , nPos + szText.cy, m_strName);					
				pDC->TextOut(nRightMargin - 40, nPos + szText.cy,
					toString("%d", m_nCollarIndex));
			}
			if (m_nStdCollarIndex != SC_ERR)
			{
				CPen pen(PS_SOLID, 1, RGB(255,0,0));
				CPen* pOldPen = pDC->SelectObject(&pen);
				pDC->MoveTo(nLeftMargin , nPos );
				pDC->LineTo(nRightMargin , nPos);
				pDC->SelectObject(pOldPen);
				
				COLORREF oldBkColor = pDC->SetTextColor(RGB(255,0,0));
				pDC->TextOut(nLeftMargin + 10 , nPos + szText.cy, m_strName);
				pDC->TextOut(nLeftMargin + 20, nPos + szText.cy  - 50, 
					toString("标 %d", m_nStdCollarIndex));
				pDC->TextOut(nRightMargin - 40, nPos + szText.cy,
					toString("%d", m_nCollarIndex));
				pDC->SetTextColor(oldBkColor);
			}
			
						
			if(m_bHot)
			{
				//pOldPen = pDC->SelectObject(&hotPen);
				pDC->SelectObject(&hotPen);
				pDC->MoveTo(nLeftMargin,nPos+1);
				pDC->LineTo(nRightMargin,nPos+1);
				pDC->MoveTo(nLeftMargin,nPos-1);
				pDC->LineTo(nRightMargin,nPos-1);
			}
		}	
		break;
	case MARKS_HORIZONTAL_LINE:
		{
			m_rcMark.left  = nLeftMargin;
			m_rcMark.right = nRightMargin;
			pDC->MoveTo(nLeftMargin , nPos );
			pDC->LineTo(nRightMargin , nPos);
			if(m_bHot)
			{
				//pOldPen = pDC->SelectObject(&hotPen);
				pDC->SelectObject(&hotPen);
				pDC->MoveTo(nLeftMargin,nPos+1);
				pDC->LineTo(nRightMargin,nPos+1);
				pDC->MoveTo(nLeftMargin,nPos-1);
				pDC->LineTo(nRightMargin,nPos-1);
			}
			pDC->TextOut(nLeftMargin + 10 , nPos + szText.cy, m_strName);

			/*if (m_nCollarIndex != CI_ERR)
			{
				pDC->TextOut(nRightMargin - 40, nPos + szText.cy,
					toString("%d", m_nCollarIndex));
			}
			if (m_nStdCollarIndex != SC_ERR)
			{
				pDC->TextOut(nLeftMargin + 20, nPos + szText.cy  - 50, 
					toString("标 %d", m_nStdCollarIndex));
			}*/
		}	
		break;
	case MARKS_VERTICAL_LINE:
#ifdef _PERFORATION
		{
			CCurve* pCurve = g_pTask->m_pCurve;
			CURVEPROPERTY* pProperty = new CURVEPROPERTY;
	
			if(NULL != pCurve)
			{
				pCurve->GetCurveProp(pProperty);
				if(NULL != pProperty)
				{
					m_fLValue = pProperty->fLeftValue;
					m_fRValue = pProperty->fRightValue;
					if(m_fLValue - m_fRValue != 0)
					{
						double fRadio = fabs((g_pTask->m_LeftValue - g_pTask->m_RightValue)/(m_fLValue - m_fRValue));
						m_rcMark.left  = nLeftMargin;
						m_rcMark.right = nRightMargin;
						CPen redPen(PS_SOLID, 1, RGB(255,0,0));
						CPen *pOldPen =  pDC->SelectObject(&redPen);

						pDC->MoveTo(nLeftMargin+(1-fRadio)*(nRightMargin-nLeftMargin)/2.0, nPos-10);
						pDC->LineTo(nLeftMargin+(1-fRadio)*(nRightMargin-nLeftMargin)/2.0, nPos+10);
						
						pDC->MoveTo(nRightMargin+(1-fRadio)*(nLeftMargin-nRightMargin)/2.0, nPos-10);
						pDC->LineTo(nRightMargin+(1-fRadio)*(nLeftMargin-nRightMargin)/2.0, nPos+10);

						pDC->SelectObject(pOldPen);
					}
				}
				
			}
			
			delete pProperty;
		}
#endif
		break;
	case MARKS_TRIANGLE:
		{
			CPoint points[3];
			
			if(m_nPointer == 0)
			{
				m_rcMark.left =  m_fMarkXCoordinate;
				m_rcMark.right= m_rcMark.left + 50;
				m_rcMark.top = nPos - 10;
				m_rcMark.bottom=nPos + 10;
				
				points[0].x = m_rcMark.left;
				points[0].y = nPos ;
				
				points[1].x = m_rcMark.right;
				points[1].y = nPos -  abs(m_rcMark.Height()/2);
				
				points[2].x = m_rcMark.right;
				points[2].y = nPos +  abs(m_rcMark.Height()/2);
				
				pDC->TextOut(points[1].x + 20 , nPos + szText.cy / 2 , m_strName);
			}
			else
			{
				m_rcMark.right =  m_fMarkXCoordinate;
				m_rcMark.left = m_rcMark.right - 50;
				
				points[0].x = m_rcMark.right;
				points[0].y = nPos;
				
				points[1].x = m_rcMark.left;
				points[1].y = nPos - 10;
				
				points[2].x = m_rcMark.left;
				points[2].y = nPos + 10;
				pDC->TextOut(points[1].x - 20 - szText.cx, nPos + szText.cy / 2 , m_strName);
			}
			if(m_bHot)
			{
				//pOldPen = pDC->SelectObject(&hotPen);
				pDC->SelectObject(&hotPen);
				pDC->MoveTo(points[0]);
				pDC->LineTo(points[1]);
				pDC->LineTo(points[2]);
				pDC->LineTo(points[0]);
			}
		   	pDC->Polygon(points, 3);
		}
		break;
	case MARKS_ARROW:
		{
			CPoint points[4];
			if(m_nPointer == 0)
			{
				m_rcMark.left =  m_fMarkXCoordinate;
				m_rcMark.right= m_rcMark.left + 100;
				
				points[0].x = m_rcMark.left;
				points[0].y = nPos ;
				
				points[1].x = m_rcMark.left + 30;
				points[1].y = nPos -  15;
				
				points[3].x = m_rcMark.left + 30;
				points[3].y = nPos +  15;
				
				points[2].x = (m_rcMark.left + m_rcMark.right)/2 + 10;
				points[2].y = nPos ;
				pDC->TextOut(points[3].x + 50 , nPos + szText.cy / 2 , m_strName);
			}
			else
			{
				m_rcMark.right =  m_fMarkXCoordinate;
				m_rcMark.left= m_rcMark.right - 100;

				points[2].x = m_rcMark.left;
				points[2].y = nPos ;

				points[0].x = m_rcMark.right;
				points[0].y = nPos;
			
				points[1].x = m_rcMark.right - 30;
				points[1].y = nPos - 15;

				points[3].x = m_rcMark.right - 30;
				points[3].y = nPos + 15;

				pDC->TextOut(m_rcMark.left - 20 - szText.cx , nPos + szText.cy / 2 , m_strName);
			}
			if(m_bHot)
			{
				//pOldPen = pDC->SelectObject(&hotPen);
				pDC->SelectObject(&hotPen);
				pDC->MoveTo(points[1]);
				pDC->LineTo(points[0]);
				pDC->LineTo(points[3]);
				pDC->MoveTo(points[2]);
				pDC->LineTo(points[0]);
			}
			//pOldPen = pDC->SelectObject(&hotPen);
			pDC->SelectObject(&hotPen);
			pDC->MoveTo(points[0]);
			pDC->LineTo(points[1]);
			
			pDC->MoveTo(points[0]);
			pDC->LineTo(points[2]);
				
			pDC->MoveTo(points[0]);
			pDC->LineTo(points[3]);			
		}
		break;
	case MARKS_FIRE_LINE:
		{
			m_rcMark.left  = nLeftMargin;
			m_rcMark.right = nRightMargin;

			CPen newPenLight(PS_SOLID, 1, RGB(255,0,0));
			CPen *pOldPen = pDC->SelectObject(&newPenLight);
			pDC->MoveTo( nLeftMargin, nPos );
			pDC->LineTo( nRightMargin, nPos);
			pDC->SelectObject(pOldPen);

			CPen newPen(PS_SOLID, FIRE_LINE_WIDTH, RGB(255,0,0));
			pOldPen = pDC->SelectObject(&newPen);
			pDC->MoveTo(nLeftMargin+(nRightMargin-nLeftMargin)/3, nPos );
			pDC->LineTo(nRightMargin-(nRightMargin-nLeftMargin)/3, nPos);
			pDC->SelectObject(pOldPen);
			COLORREF  oldColor = pDC->SetTextColor(RGB(255,0,0));

			if(m_bHot)
			{
				//pOldPen = pDC->SelectObject(&hotPen);
				pDC->SelectObject(&hotPen);
				pDC->MoveTo(nLeftMargin,nPos+1);
				pDC->LineTo(nRightMargin,nPos+1);
				pDC->MoveTo(nLeftMargin,nPos-1);
				pDC->LineTo(nRightMargin,nPos-1);
			}
			pDC->TextOut(nLeftMargin+(nRightMargin-nLeftMargin)/6, nPos + szText.cy, m_strName);
  			pDC->TextOut((nRightMargin+nLeftMargin)/2+10, nPos + szText.cy,_T("点火线"));
			
			pDC->SetTextColor(oldColor);
		}
		break;
	case MARKS_FIRE_LINE1:
		{
			m_rcMark.left  = nLeftMargin;
			m_rcMark.right = nRightMargin;

			CPen newPenLight(PS_SOLID, 1, RGB(255,0,0));
			CPen *pOldPen = pDC->SelectObject(&newPenLight);
			pDC->MoveTo( nLeftMargin, nPos );
			pDC->LineTo( nRightMargin, nPos);
			pDC->SelectObject(pOldPen);

			/*CPen newPen(PS_SOLID, FIRE_LINE_WIDTH, RGB(255,0,0));
			pOldPen = pDC->SelectObject(&newPen);
			pDC->MoveTo(nLeftMargin+(nRightMargin-nLeftMargin)/3, nPos );
			pDC->LineTo(nRightMargin-(nRightMargin-nLeftMargin)/3, nPos);
			pDC->SelectObject(pOldPen);*/

			COLORREF  oldColor = pDC->SetTextColor(RGB(255,0,0));

			if(m_bHot)
			{
				//pOldPen = pDC->SelectObject(&hotPen);
				pDC->SelectObject(&hotPen);
				pDC->MoveTo(nLeftMargin,nPos+1);
				pDC->LineTo(nRightMargin,nPos+1);
				pDC->MoveTo(nLeftMargin,nPos-1);
				pDC->LineTo(nRightMargin,nPos-1);
			}
			pDC->TextOut(nLeftMargin+(nRightMargin-nLeftMargin)/6, nPos + szText.cy, m_strName);
  			pDC->TextOut((nRightMargin+nLeftMargin)/2+10, nPos + szText.cy,_T("点火线"));
			
			pDC->SetTextColor(oldColor);
		}
		break;
	case MARKS_MARK_LINE:
		{
			m_rcMark.left  = nLeftMargin;
			m_rcMark.right = nRightMargin;
		/*	CPen newPen(PS_SOLID, FIRE_LINE_WIDTH, RGB(255,0,0));
			CPen *pOldPen = pDC->SelectObject(&newPen);
			pDC->MoveTo(nLeftMargin+(nRightMargin-nLeftMargin)/6 , nPos );
			pDC->LineTo(nRightMargin-(nRightMargin-nLeftMargin)/6 , nPos);
			pDC->SelectObject(pOldPen);*/

			CPen newPenLight(PS_SOLID, 1, RGB(255,0,0));
			CPen *pOldPen = pDC->SelectObject(&newPenLight);
			pDC->MoveTo( nLeftMargin, nPos );
			pDC->LineTo( nRightMargin, nPos);
			pDC->SelectObject(pOldPen);

			CPen newPen(PS_SOLID, FIRE_LINE_WIDTH, RGB(255,0,0));
			pOldPen = pDC->SelectObject(&newPen);
			pDC->MoveTo(nLeftMargin+(nRightMargin-nLeftMargin)/3, nPos );
			pDC->LineTo(nRightMargin-(nRightMargin-nLeftMargin)/3, nPos);
			pDC->SelectObject(pOldPen);
			COLORREF  oldColor = pDC->SetTextColor(RGB(255,0,0));

			if(m_bHot)
			{
				//pOldPen = pDC->SelectObject(&hotPen);
				pDC->SelectObject(&hotPen);
				pDC->MoveTo(nLeftMargin,nPos+1);
				pDC->LineTo(nRightMargin,nPos+1);
				pDC->MoveTo(nLeftMargin,nPos-1);
				pDC->LineTo(nRightMargin,nPos-1);
			}
			pDC->TextOut(nLeftMargin+(nRightMargin-nLeftMargin)/6 , nPos + szText.cy, m_strName);
  			pDC->TextOut((nRightMargin+nLeftMargin)/2+10, nPos + szText.cy,_T("扎记号"));
			
			pDC->SetTextColor(oldColor);
		}
		break;
	case MARKS_MARK_LINE1:
		{
			m_rcMark.left  = nLeftMargin;
			m_rcMark.right = nRightMargin;
		/*	CPen newPen(PS_SOLID, FIRE_LINE_WIDTH, RGB(255,0,0));
			CPen *pOldPen = pDC->SelectObject(&newPen);
			pDC->MoveTo(nLeftMargin+(nRightMargin-nLeftMargin)/6 , nPos );
			pDC->LineTo(nRightMargin-(nRightMargin-nLeftMargin)/6 , nPos);
			pDC->SelectObject(pOldPen);*/

			CPen newPenLight(PS_SOLID, 1, RGB(255,0,0));
			CPen *pOldPen = pDC->SelectObject(&newPenLight);
			pDC->MoveTo( nLeftMargin, nPos );
			pDC->LineTo( nRightMargin, nPos);
			pDC->SelectObject(pOldPen);

			/*CPen newPen(PS_SOLID, FIRE_LINE_WIDTH, RGB(255,0,0));
			pOldPen = pDC->SelectObject(&newPen);
			pDC->MoveTo(nLeftMargin+(nRightMargin-nLeftMargin)/3, nPos );
			pDC->LineTo(nRightMargin-(nRightMargin-nLeftMargin)/3, nPos);
			pDC->SelectObject(pOldPen);*/

			COLORREF  oldColor = pDC->SetTextColor(RGB(255,0,0));

			if(m_bHot)
			{
				//pOldPen = pDC->SelectObject(&hotPen);
				pDC->SelectObject(&hotPen);
				pDC->MoveTo(nLeftMargin,nPos+1);
				pDC->LineTo(nRightMargin,nPos+1);
				pDC->MoveTo(nLeftMargin,nPos-1);
				pDC->LineTo(nRightMargin,nPos-1);
			}
			pDC->TextOut(nLeftMargin+(nRightMargin-nLeftMargin)/6 , nPos + szText.cy, m_strName);
  			pDC->TextOut((nRightMargin+nLeftMargin)/2+10, nPos + szText.cy,_T("扎记号"));
			
			pDC->SetTextColor(oldColor);
		}
		break;
	case MARKS_BLOCK_CURVE:
	case MARKS_MAX_NULL:
	case MARKS_MIN_NULL:
	default:
		break;
	}
	pDC->SelectObject(pOldFont);
	pDC->SelectObject(pOldBrush);
}

/* 函数名称：PrintMark
 * 函数描述：打印标签
 * 返回类型：void
 * 函数参数：CULPrintInfo
 */
void CMark::Print(CULPrintInfo* pInfo)
{
	// 得到打印DC
	CDC* pDC = pInfo->GetPrintDC();
	ASSERT(pDC != NULL);
	pDC->SetBkMode(TRANSPARENT);

	CFont myFont,*pOldFont;
	myFont.CreatePointFont(100,"Arial",pDC);
	pOldFont = pDC->SelectObject(&myFont);
	
	CRect rcTrack;
	m_pTrack->GetRegionRect(&rcTrack, TRUE);
	CRect rt = pInfo->CoordinateConvert(rcTrack);
	int nLeftMargin = rt.left;
	int nRightMargin = rt.right;
	int nPos = pInfo->GetPrintDepthCoordinate(DTM(m_lDepth));
	CSize szText = pDC->GetTextExtent(m_strName);
	
	//Start:修改 By Sjh
	switch(m_nShape)
	{
	case MARKS_PERFORATION_COLLOR:
		{
			pDC->MoveTo(nLeftMargin, nPos);
			pDC->LineTo(nRightMargin, nPos);
			pDC->TextOut(nLeftMargin + 20 , nPos + szText.cy, m_strName);	
			
			if (m_nCollarIndex != CI_ERR)
			{
				pInfo->PrintText(toString("%d", m_nCollarIndex), 
					nRightMargin - 30, nPos + szText.cy);
			}
			if (m_nStdCollarIndex != SC_ERR)
			{
				pInfo->PrintText(toString("标 %d", m_nStdCollarIndex), 
					nLeftMargin + 20, nPos + szText.cy - 50);
			}
		}	
		break;
	case MARKS_HORIZONTAL_LINE:
		{	
			pDC->MoveTo(nLeftMargin, nPos);
			pDC->LineTo(nRightMargin, nPos);
			pDC->TextOut(nLeftMargin + 20 , nPos + szText.cy, m_strName);	
			
			/*if (m_nCollarIndex != CI_ERR)
			{
				pInfo->PrintText(toString("%d", m_nCollarIndex), 
					nRightMargin - 30, nPos + szText.cy);
			}
			if (m_nStdCollarIndex != SC_ERR)
			{
				pInfo->PrintText(toString("标 %d", m_nStdCollarIndex), 
					nLeftMargin + 20, nPos + szText.cy - 50);
			}*/
		}	
		break;
	case MARKS_FIRE_LINE:
		{
			m_rcMark.left  = nLeftMargin;
			m_rcMark.right = nRightMargin;

			CPen newPenLight(PS_SOLID, 1, RGB(0,0,0));
			CPen *pOldPen = pDC->SelectObject(&newPenLight);
			pDC->MoveTo( nLeftMargin, nPos );
			pDC->LineTo( nRightMargin, nPos);
			pDC->SelectObject(pOldPen);

			CPen newPen(PS_SOLID, FIRE_LINE_WIDTH, RGB(0,0,0));
			pOldPen = pDC->SelectObject(&newPen);
			pDC->MoveTo(nLeftMargin+(nRightMargin-nLeftMargin)/3 , nPos );
			pDC->LineTo(nRightMargin-(nRightMargin-nLeftMargin)/3 , nPos);
			pDC->SelectObject(pOldPen);
			COLORREF  oldColor = pDC->SetTextColor(RGB(0,0,0));

			pInfo->PrintText( m_strName, nLeftMargin+(nRightMargin-nLeftMargin)/6, nPos+szText.cy );
  			pInfo->PrintText( _T("点火线"), (nRightMargin+nLeftMargin)/2+10, nPos+szText.cy );
			
			pDC->SetTextColor(oldColor);
		}
		break;
	case MARKS_FIRE_LINE1:
		{
			m_rcMark.left  = nLeftMargin;
			m_rcMark.right = nRightMargin;

			CPen newPenLight(PS_SOLID, 1, RGB(0,0,0));
			CPen *pOldPen = pDC->SelectObject(&newPenLight);
			pDC->MoveTo( nLeftMargin, nPos );
			pDC->LineTo( nRightMargin, nPos);
			pDC->SelectObject(pOldPen);

			/*CPen newPen(PS_SOLID, FIRE_LINE_WIDTH, RGB(0,0,0));
			pOldPen = pDC->SelectObject(&newPen);
			pDC->MoveTo(nLeftMargin+(nRightMargin-nLeftMargin)/3 , nPos );
			pDC->LineTo(nRightMargin-(nRightMargin-nLeftMargin)/3 , nPos);
			pDC->SelectObject(pOldPen);*/

			COLORREF  oldColor = pDC->SetTextColor(RGB(0,0,0));

			pInfo->PrintText( m_strName, nLeftMargin+(nRightMargin-nLeftMargin)/6, nPos+szText.cy );
  			pInfo->PrintText( _T("点火线"), (nRightMargin+nLeftMargin)/2+10, nPos+szText.cy );
			
			pDC->SetTextColor(oldColor);
		}
		break;
	case MARKS_MARK_LINE:
		{
			m_rcMark.left  = nLeftMargin;
			m_rcMark.right = nRightMargin;
			/*CPen newPen(PS_SOLID, FIRE_LINE_WIDTH, RGB(0,0,0));
			CPen *pOldPen = pDC->SelectObject(&newPen);
			pDC->MoveTo(nLeftMargin+(nRightMargin-nLeftMargin)/6 , nPos );
			pDC->LineTo(nRightMargin-(nRightMargin-nLeftMargin)/6 , nPos);
			pDC->SelectObject(pOldPen);*/

			CPen newPenLight(PS_SOLID, 1, RGB(0,0,0));
			CPen *pOldPen = pDC->SelectObject(&newPenLight);
			pDC->MoveTo( nLeftMargin, nPos );
			pDC->LineTo( nRightMargin, nPos);
			pDC->SelectObject(pOldPen);

			CPen newPen(PS_SOLID, FIRE_LINE_WIDTH, RGB(0,0,0));
			pOldPen = pDC->SelectObject(&newPen);
			pDC->MoveTo(nLeftMargin+(nRightMargin-nLeftMargin)/3 , nPos );
			pDC->LineTo(nRightMargin-(nRightMargin-nLeftMargin)/3 , nPos);
			pDC->SelectObject(pOldPen);

			COLORREF  oldColor = pDC->SetTextColor(RGB(0,0,0));

			pInfo->PrintText( m_strName, nLeftMargin+(nRightMargin-nLeftMargin)/6 , nPos + szText.cy );
  			pInfo->PrintText( _T("扎记号"), (nRightMargin+nLeftMargin)/2+10, nPos + szText.cy );
			
			pDC->SetTextColor(oldColor);
		}
		break;
	case MARKS_MARK_LINE1:
		{
			m_rcMark.left  = nLeftMargin;
			m_rcMark.right = nRightMargin;
		/*	CPen newPen(PS_SOLID, FIRE_LINE_WIDTH, RGB(0,0,0));
			CPen *pOldPen = pDC->SelectObject(&newPen);
			pDC->MoveTo(nLeftMargin+(nRightMargin-nLeftMargin)/6 , nPos );
			pDC->LineTo(nRightMargin-(nRightMargin-nLeftMargin)/6 , nPos);
			pDC->SelectObject(pOldPen);*/

			CPen newPenLight(PS_SOLID, 1, RGB(0,0,0));
			CPen *pOldPen = pDC->SelectObject(&newPenLight);
			pDC->MoveTo( nLeftMargin, nPos );
			pDC->LineTo( nRightMargin, nPos);
			pDC->SelectObject(pOldPen);

			/*CPen newPen(PS_SOLID, FIRE_LINE_WIDTH, RGB(0,0,0));
			pOldPen = pDC->SelectObject(&newPen);
			pDC->MoveTo(nLeftMargin+(nRightMargin-nLeftMargin)/3 , nPos );
			pDC->LineTo(nRightMargin-(nRightMargin-nLeftMargin)/3 , nPos);
			pDC->SelectObject(pOldPen);*/

			COLORREF  oldColor = pDC->SetTextColor(RGB(0,0,0));

			pInfo->PrintText( m_strName, nLeftMargin+(nRightMargin-nLeftMargin)/6 , nPos + szText.cy );
  			pInfo->PrintText( _T("扎记号"), (nRightMargin+nLeftMargin)/2+10, nPos + szText.cy );
			
			pDC->SetTextColor(oldColor);
		}
		break;
	default:
		break;
	}
    pDC->SelectObject(pOldFont);
	//End: 修改
}

/* 函数名称：IsPtInShape
 * 函数描述：判断当前坐标点是否在标签所在矩形区内
 * 返回类型：BOOL：
 * 函数参数：CPoint pt：当前坐标点
 */
BOOL CMark::IsPtInShape(CPoint pt)
{
	BOOL bIsInShape = FALSE;
	int nPos = /*m_pCurve->*/m_pTrack->GetCoordinateFromDepth(m_lDepth);
	switch(m_nShape)
	{
	// 对于直线标签来说，其矩形区为
	// Rect(标签位置-2，标签Left，标签位置+2，标签Right)
	case MARKS_HORIZONTAL_LINE:
	case MARKS_FIRE_LINE:
		{
			CRect rcline;
			rcline.left  = m_rcMark.left;
			rcline.right = m_rcMark.right;
			
			rcline.top = nPos + 2;
			rcline.bottom = nPos - 2;
			if(rcline.PtInRect(pt))
				bIsInShape = TRUE;
		}
		break;
	case MARKS_TRIANGLE:
		{
			if(m_rcMark.PtInRect(pt))
				bIsInShape = TRUE;
		}
		break;
	case MARKS_ARROW:
		{
			CRect rcArrow;
			rcArrow.left = m_rcMark.left;
			rcArrow.right = m_rcMark.right;
			rcArrow.top = nPos + 10;
			rcArrow.bottom = nPos - 10;
			if(rcArrow.PtInRect(pt))
				bIsInShape = TRUE;
		}
		break;
	
	default:
		bIsInShape = FALSE;
		break;
	}
	return bIsInShape;
}

void CMark::Serialize(CXMLSettings& xml)
{
	if (xml.IsStoring())
	{
	
		xml.Write(_T("MarkName"), m_strName);			// 标记名称

		xml.Write(_T("MarkDepth"), m_lDepth);			// 标记深度
		xml.Write(_T("MarkArrowPos"), m_lArrowPos);

		xml.Write(_T("MarkRect"), m_rect);
			
		xml.Write(_T("MarkPoint"), m_nPointer);			// 标记指向	0左，1右
		xml.Write(_T("MarkShape"), m_nShape); 	 		// 标记形状
		xml.Write(_T("MarkrcMark"), m_rcMark);			// 标记范围
		xml.Write(_T("MarkColor"), m_crColor);          // 标记颜色
		xml.Write(_T("MarkText"), m_crText);			// 标记文本颜色
		
		xml.Write(_T("MarkText"), m_strText);			// 标记说明
		xml.Write(_T("MarkTrackNO"), m_nTrackNO);			// 通道号

		xml.Write(_T("MarkbHot"), m_bHot);				// 标记选中
		xml.Write(_T("MarkbLock"), m_bLock);

		xml.Write(_T("MrakbDepthChange"), m_bDepthChange);		// 拖动标签时名称是否改变
		xml.Write(_T("MarkXCoordinate"), m_fMarkXCoordinate);	// 标记的横坐标
		xml.Write(_T("MarkXScale"), m_fXScale);

		xml.Write(_T("MarkCollarIndex"), m_nCollarIndex);		// 节箍索引
		xml.Write(_T("MarkStdCollarIndex"), m_nStdCollarIndex);	// 第n枪的标箍号
		xml.Write(_T("MarkLimit"), m_fLimit);           // 垂直线的位置
		xml.Write(_T("MarkLValue"), m_fLValue);          // CCL曲线的左值
		xml.Write(_T("MarkRValue"), m_fRValue);          // CCL曲线的右值

		xml.Write(_T("MarkbWatch"), m_bWatch);
		xml.Write(_T("MarkTime"), m_dwTime);           // 标记出现的时间
	}
	else
	{
		xml.Read(_T("MarkName"), m_strName);			// 标记名称

		xml.Read(_T("MarkDepth"), m_lDepth);			// 标记深度
		xml.Read(_T("MarkArrowPos"), m_lArrowPos);

		xml.Read(_T("MarkRect"), m_rect);
			
		xml.Read(_T("MarkPoint"), m_nPointer);			// 标记指向	0左，1右
		xml.Read(_T("MarkShape"), m_nShape); 	 		// 标记形状
		xml.Read(_T("MarkrcMark"), m_rcMark);			// 标记范围
		xml.Read(_T("MarkColor"), m_crColor);          // 标记颜色
		xml.Read(_T("MarkText"), m_crText);			// 标记文本颜色
		
		xml.Read(_T("MarkText"), m_strText);			// 标记说明
		xml.Read(_T("MarkTrackNO"), m_nTrackNO);			// 通道号

		xml.Read(_T("MarkbHot"), m_bHot);				// 标记选中
		xml.Read(_T("MarkbLock"), m_bLock);

		xml.Read(_T("MrakbDepthChange"), m_bDepthChange);		// 拖动标签时名称是否改变
		xml.Read(_T("MarkXCoordinate"), m_fMarkXCoordinate);	// 标记的横坐标
		xml.Read(_T("MarkXScale"), m_fXScale);

		xml.Read(_T("MarkCollarIndex"), m_nCollarIndex);		// 节箍索引
		xml.Read(_T("MarkStdCollarIndex"), m_nStdCollarIndex);	// 第n枪的标箍号
		xml.Read(_T("MarkLimit"), m_fLimit);           // 垂直线的位置
		xml.Read(_T("MarkLValue"), m_fLValue);          // CCL曲线的左值
		xml.Read(_T("MarkRValue"), m_fRValue);          // CCL曲线的右值
		
		xml.Read(_T("MarkbWatch"), m_bWatch);
		xml.Read(_T("MarkTime"), m_dwTime);           // 标记出现的时间
	}
}

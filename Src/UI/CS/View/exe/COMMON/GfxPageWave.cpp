// GfxPageWave.cpp : implementation file

#include "stdafx.h"
#include "resource.h"
#include "GfxPageWave.h"
#include "Curve.h"
#include "GraphWnd.h"
#include "Units.h"
#include "Sheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CUnits* g_units;

/////////////////////////////////////////////////////////////////////////////
// CGfxPageWave dialog

CGfxPageWave::CGfxPageWave(CWnd* pParent /*=NULL*/)
	: CGfxPage(IDD_GFX_PAGE_WAVE, pParent)
{
	//{{AFX_DATA_INIT(CGfxPageWave)
	m_bDisplay = FALSE;
	m_bPrint = FALSE;
	m_bSave = FALSE;
	m_lDepthDelay = 0;
	m_lDepthDistance = 0;
	m_lDepthOffset = 0;
	m_iEndPosition = 0;
	m_iStartPosition = 0;
	m_fLowerValue = 0.0;
	m_strName = _T("");
	m_strUnit = _T("");
	m_fUpperValue = 0.0;
	m_iTimeInter = 0;
	m_lTimeDelay = 0;
	m_nPlotStyle = 0;
	m_nDistribution = 0;
	//}}AFX_DATA_INIT
}

void CGfxPageWave::DoDataExchange(CDataExchange* pDX)
{
	CGfxPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGfxPageWave)
	DDX_Control(pDX, IDC_BTN_COLOR, m_btnCurveColor);
	DDX_Check(pDX, IDC_CHECK_DISPLAY, m_bDisplay);
	DDX_Check(pDX, IDC_CHECK_PRINT, m_bPrint);
	DDX_Check(pDX, IDC_CHECK_SAVE, m_bSave);
	DDX_Text(pDX, IDC_EDIT_ENDPOS, m_iEndPosition);
	DDX_Text(pDX, IDC_EDIT_STARTPOS, m_iStartPosition);
	DDX_Text(pDX, IDC_EDIT_LOWERVALUE, m_fLowerValue);
	DDX_Text(pDX, IDC_EDIT_NAME, m_strName);
	DDX_Text(pDX, IDC_EDIT_NAME2, m_strName2);
	DDX_Text(pDX, IDC_EDIT_UNIT, m_strUnit);
	DDX_Text(pDX, IDC_EDIT_UPPERVALUE, m_fUpperValue);
	DDX_Text(pDX, IDC_EDIT_TIMEINTER, m_iTimeInter);
	DDX_Text(pDX, IDC_EDIT_TIMEDELAY, m_lTimeDelay);
	DDX_Radio(pDX, IDC_RADIO1, m_nPlotStyle);
	DDX_Radio(pDX, IDC_RADIO3, m_nDistribution);
	//}}AFX_DATA_MAP
	g_units->DDX_Depth(pDX, IDC_DEPTHDELAY, m_lDepthDelay);
	g_units->DDX_Depth(pDX, IDC_DEPTHOFFSET, m_lDepthOffset);

	if (m_pCurve)
	{
		CString strPlotStyle = m_pCurve->m_PlotInfo.PlotENName;
		if (strPlotStyle.CompareNoCase(_T("Wave")) == 0)
		{
			g_units->DDX_Depth(pDX, IDC_DEPTHDISTANCE, m_lDepthDistance);
			DDX_Text(pDX, IDC_TIMEDISTANCE, m_lTimeDistance);
		}
		else
		{
			DDX_Text(pDX, IDC_DEPTHDISTANCE, m_lDepthDistance);
			DDX_Text(pDX, IDC_TIMEDISTANCE, m_lTimeDistance);
		}
	}
}

BEGIN_MESSAGE_MAP(CGfxPageWave, CGfxPage)
	//{{AFX_MSG_MAP(CGfxPageWave)
	ON_EN_CHANGE(IDC_EDIT_ENDPOS, OnChangeEditEndpos)
	ON_EN_CHANGE(IDC_EDIT_STARTPOS, OnChangeEditStartpos)
	ON_BN_CLICKED(IDC_CHECK_DISPLAY, RefreshData)
	ON_BN_CLICKED(IDC_CHECK_PRINT, RefreshData)
	ON_BN_CLICKED(IDC_CHECK_SAVE, RefreshData)
	ON_EN_CHANGE(IDC_EDIT_UPPERVALUE, OnChangeEditUppervalue)
	ON_EN_CHANGE(IDC_EDIT_NAME, OnChangeEditName)
	ON_EN_CHANGE(IDC_EDIT_LOWERVALUE, OnChangeEditLowervalue)
	ON_EN_CHANGE(IDC_EDIT_UNIT, OnChangeEditUnit)
	ON_EN_KILLFOCUS(IDC_EDIT_UPPERVALUE, OnKillfocusEditUppervalue)
	ON_EN_KILLFOCUS(IDC_EDIT_LOWERVALUE, OnKillfocusEditLowervalue)
	ON_EN_KILLFOCUS(IDC_DEPTHOFFSET, RefreshData)
	ON_EN_KILLFOCUS(IDC_EDIT_STARTPOS, OnKillfocusEditStartpos)
	ON_EN_KILLFOCUS(IDC_EDIT_ENDPOS, OnKillfocusEditEndpos)
	ON_EN_KILLFOCUS(IDC_DEPTHDISTANCE, RefreshData)
	ON_EN_KILLFOCUS(IDC_TIMEDISTANCE, RefreshData)
	ON_EN_KILLFOCUS(IDC_DEPTHDELAY, RefreshData)
	ON_EN_KILLFOCUS(IDC_EDIT_TIMEINTER, RefreshData)
	ON_BN_CLICKED(IDC_BTN_COLOR, RefreshData)
	ON_WM_DESTROY()
	ON_EN_KILLFOCUS(IDC_EDIT_TIMEDELAY, RefreshData)
	ON_BN_CLICKED(IDC_RADIO1, RefreshData)
	ON_BN_CLICKED(IDC_RADIO2, RefreshData)
	ON_BN_CLICKED(IDC_RADIO3, RefreshData)
	ON_BN_CLICKED(IDC_RADIO4, RefreshData)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGfxPageWave message handlers

void CGfxPageWave::InitCurve()
{
	if (m_pCurve == NULL)
		return;

	m_bDisplay = m_pCurve->IsVisible();
	m_bPrint = m_pCurve->IsPrint();
	m_bSave = m_pCurve->IsRewind();
	m_strName = m_pCurve->m_strName;
	m_strName2 = m_pCurve->m_strIDName;
	m_strUnit = m_pCurve->Unit();
	m_lDepthOffset = m_pCurve->DepthOffset();

	CString strPlotStyle = m_pCurve->m_PlotInfo.PlotENName;
	if (strPlotStyle.CompareNoCase(_T("Wave")) == 0)
	{
		GetDlgItem(IDC_STATIC_POINT_INTERVAL)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_DRAWSTYLE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_CURVEPOS)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RADIO1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RADIO2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RADIO3)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RADIO4)->ShowWindow(SW_HIDE);
		if (m_pGraph!=NULL)
		{
			if (m_pGraph->m_pSheet->m_nDriveMode)
			{
#ifndef _LOGIC
				GetDlgItem(IDC_STATIC_TIME_INTERVAL)->ShowWindow(SW_HIDE);
				GetDlgItem(IDC_TIMEDISTANCE)->ShowWindow(SW_HIDE);
#endif
				GetDlgItem(IDC_DEPTHDISTANCE)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_STATIC_DEPTH_INTERVAL)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_STATIC_DEPTHDELAY)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_DEPTHDELAY)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_STATIC_DEPTHOFFSET)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_DEPTHOFFSET)->ShowWindow(SW_SHOW);
				m_lDepthDistance = m_pCurve->m_lDepthDistance;

			}
			else
			{
				GetDlgItem(IDC_DEPTHDISTANCE)->ShowWindow(SW_HIDE);
				GetDlgItem(IDC_STATIC_DEPTH_INTERVAL)->ShowWindow(SW_HIDE);
				GetDlgItem(IDC_STATIC_DEPTHDELAY)->ShowWindow(SW_HIDE);
				GetDlgItem(IDC_DEPTHDELAY)->ShowWindow(SW_HIDE);
				GetDlgItem(IDC_STATIC_DEPTHOFFSET)->ShowWindow(SW_HIDE);
				GetDlgItem(IDC_DEPTHOFFSET)->ShowWindow(SW_HIDE);
#ifndef _LOGIC
				GetDlgItem(IDC_STATIC_TIME_INTERVAL)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_TIMEDISTANCE)->ShowWindow(SW_SHOW);
#endif
				m_lTimeDistance = m_pCurve->m_iTimeDistance;
	//			m_lDepthDistance = m_pCurve->m_lDepthDistance;
			}
		}

	}
	else
	{
		if (m_pGraph->m_pSheet->m_nDriveMode)
		{
#ifndef _LOGIC
			GetDlgItem(IDC_STATIC_TIME_INTERVAL)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_TIMEDISTANCE)->ShowWindow(SW_HIDE);
#endif
			GetDlgItem(IDC_DEPTHDISTANCE)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STATIC_DEPTH_INTERVAL)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_POINT_INTERVAL)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STATIC_DEPTHDELAY)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_DEPTHDELAY)->ShowWindow(SW_HIDE);
			m_lDepthDistance = m_pCurve->m_nPointDivision;
			GetDlgItem(IDC_STATIC_DRAWSTYLE)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STATIC_CURVEPOS)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_RADIO1)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_RADIO2)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_RADIO3)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_RADIO4)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STATIC_DEPTHOFFSET)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_DEPTHOFFSET)->ShowWindow(SW_SHOW);
		}
		else
		{
#ifndef _LOGIC
			GetDlgItem(IDC_STATIC_TIME_INTERVAL)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_TIMEDISTANCE)->ShowWindow(SW_SHOW);
#endif
			GetDlgItem(IDC_DEPTHDISTANCE)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_DEPTH_INTERVAL)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_POINT_INTERVAL)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STATIC_DEPTHDELAY)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_DEPTHDELAY)->ShowWindow(SW_HIDE);
			m_lTimeDistance = m_pCurve->m_iTimeDistance;
			GetDlgItem(IDC_STATIC_DRAWSTYLE)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STATIC_CURVEPOS)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_RADIO1)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_RADIO2)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_RADIO3)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_RADIO4)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STATIC_DEPTHOFFSET)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_DEPTHOFFSET)->ShowWindow(SW_HIDE);

		}

	}

	m_iTimeInter = m_pCurve->TimeInter();
	m_lTimeDelay = m_pCurve->TimeDelay();

	m_lDepthDelay = m_pCurve->m_lDepthDelay;
	m_fLowerValue = m_pCurve->m_LeftValue;
	m_fUpperValue = m_pCurve->m_RightValue;

	m_iStartPosition = (int)m_pCurve->m_dStartPos;
	m_iEndPosition = (int)m_pCurve->m_dEndPos;

	m_btnCurveColor.SetColor(m_pCurve->m_crColor);

	m_nPlotStyle = m_pCurve->m_nWavePlotStyle;
	m_nDistribution = m_pCurve->m_nWaveDistribution;

	UpdateData(FALSE);
}

void CGfxPageWave::RefreshData()
{
	UpdateData(TRUE);
	if (m_pCurve==NULL)
		return;

	m_pCurve->m_strName = m_strName;
	m_pCurve->m_strIDName = m_strName2;
	m_pCurve->SetUnit(m_strUnit.GetBuffer(m_strUnit.GetLength()));
	m_strUnit.ReleaseBuffer();
	m_pCurve->m_LeftValue = m_fLowerValue;
	m_pCurve->m_RightValue = m_fUpperValue;
	if (m_pCurve->m_pData)
		m_pCurve->m_pData->m_lDepthOffset = m_lDepthOffset;

	CString strPlotStyle = m_pCurve->m_PlotInfo.PlotENName;
	if (strPlotStyle.CompareNoCase(_T("Wave")) == 0)
	{
		if (m_pGraph->m_pSheet->m_nDriveMode)
			m_pCurve->m_lDepthDistance = m_lDepthDistance;
		else
			m_pCurve->m_iTimeDistance = m_lTimeDistance;
	}
	else
	{
		if (m_pGraph->m_pSheet->m_nDriveMode)
			m_pCurve->m_nPointDivision = m_lDepthDistance;
		else
			m_pCurve->m_nPointDivision = m_lTimeDistance;
	}

	m_pCurve->m_lDepthDelay = m_lDepthDelay;
	m_pCurve->Visible(m_bDisplay);
	m_pCurve->Print(m_bPrint);
	m_pCurve->Save(m_bSave);
	m_pCurve->TimeInter(m_iTimeInter);
	m_pCurve->TimeDelay(m_lTimeDelay);
	
	m_pCurve->m_crColor = m_btnCurveColor.GetColor();

	if (m_iEndPosition >= m_pCurve->GetPointFrame())
	{
		m_pCurve->m_dEndPos = m_pCurve->GetPointFrame() - 1;
	}
	else
	{
		m_pCurve->m_dEndPos = m_iEndPosition;
	}
	if (m_iStartPosition > m_iEndPosition)
	{
		m_pCurve->m_dStartPos = m_iEndPosition;
	}
	else
	{
		m_pCurve->m_dStartPos = m_iStartPosition;
	}

	m_pCurve->m_nWavePlotStyle = m_nPlotStyle;
	m_pCurve->m_nWaveDistribution = m_nDistribution;

	if (m_pGraph != NULL)
		m_pGraph->InvalidateAll();
}

void CGfxPageWave::OnChangeEditEndpos()
{
	CString str="";
	GetDlgItem(IDC_EDIT_ENDPOS)->GetWindowText(str);
	if (str.IsEmpty())
	{
		return;
	}
	RefreshData();
}

void CGfxPageWave::OnChangeEditStartpos()
{
	CString str="";
	GetDlgItem(IDC_EDIT_STARTPOS)->GetWindowText(str);
	if (str.IsEmpty())
	{
		return;
	}
	RefreshData();
}

void CGfxPageWave::OnChangeEditLowervalue()
{
	CString strText;
	GetDlgItem(IDC_EDIT_LOWERVALUE)->GetWindowText(strText);
	if (!IsNumber(strText))
		return;
	RefreshData();
}

void CGfxPageWave::OnChangeEditName()
{
	CString str="";
	GetDlgItem(IDC_EDIT_NAME)->GetWindowText(str);
	if (str.IsEmpty())
	{
		return;
	}
	RefreshData();
}

void CGfxPageWave::OnChangeEditUppervalue()
{
	CString strText;
	GetDlgItem(IDC_EDIT_UPPERVALUE)->GetWindowText(strText);
	if (!IsNumber(strText))
		return;
	RefreshData();
}

void CGfxPageWave::OnChangeEditUnit()
{
	CString str="";
	GetDlgItem(IDC_DEPTHOFFSET)->GetWindowText(str);
	if (str.IsEmpty())
		return;
	RefreshData();
}

BOOL CGfxPageWave::IsNumber(CString strNum)
{
	if (strNum.IsEmpty())
		return FALSE;

	if (strNum == "-" || strNum == ".")
		return FALSE;

	double fTest = 5.0;
	fTest = atof(strNum);
	if (fTest == 0.0)
	{
		fTest = atof(strNum + "5");
		if (fTest == 0.0)
			return FALSE;
		else
		{
			if (strNum[0] == '-')
				return FALSE;
		}
	}

	CString strNumChar = "-.0123456789";
	int iDot=0, iMinus=0;
	for (int i=0; i < strNum.GetLength(); i++)
	{
		//含有无效字符
		if (strNumChar.Find(strNum[i]) == -1)
			return FALSE;
		if (strNum[i] == '.')
			iDot++;
		if (strNum[i] == '-')
		{
			if (i!=0)
				return FALSE;
			iMinus++;
		}
	}
	if (iDot>1 || iMinus>1)
		return FALSE;

	return TRUE;
}

void CGfxPageWave::OnKillfocusEditUppervalue()
{
	//防止输入无效参数
	UpdateData(FALSE);
}

void CGfxPageWave::OnKillfocusEditLowervalue()
{
	//防止输入无效参数
	UpdateData(FALSE);
}

void CGfxPageWave::OnKillfocusEditStartpos() 
{
	//防止输入无效参数
	UpdateData(FALSE);
}

void CGfxPageWave::OnKillfocusEditEndpos() 
{
	//防止输入无效参数
	UpdateData(FALSE);
}

BOOL CGfxPageWave::OnInitDialog() 
{
	CGfxPage::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_btnCurveColor.EnableOtherButton(_T("More"));
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CGfxPageWave::OnDestroy() 
{
	CGfxPage::OnDestroy();
	
	// TODO: Add your message handler code here
	m_pCurve = NULL;
	m_pGraph = NULL;
}


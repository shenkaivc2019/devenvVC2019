// PrintOrderDlg.cpp : implementation file
//

#include "stdafx.h"
#ifdef _LOGIC
#include "Logic.h"
#else
#include "ul2000.h"
#include "CalSelectDlg.h"
#include "ReportSetDlg.h"
#endif
#include "ULCOMMDEF.h"
#include "InputBox.h"

#include "PrintOrderDlg.h"
#include "PrintOrderFile.h"
#include "ChildFrm.h"
#include "MainFrm.h"
#include "ULTool.h"
#include "IsValidFileName.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define ULV_GRAPHTAIL	(ULV_GRAPHHEAD + 1)
#define ULV_GRAPHSEP	(ULV_GRAPHHEAD + 2)

/////////////////////////////////////////////////////////////////////////////
// CPrintOrderDlg dialog

CPrintOrderDlg::CPrintOrderDlg(int nMode /* = POF_ADD */, CWnd* pParent /* = NULL */)
	: CDialog(CPrintOrderDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPrintOrderDlg)
	//}}AFX_DATA_INIT
	m_nMode = nMode;
	m_strPOF.Empty();
	m_pPofInfo = NULL;
}

CPrintOrderDlg::~CPrintOrderDlg()
{
}

void CPrintOrderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrintOrderDlg)
	DDX_Control(pDX, IDC_BUTTON5, m_btnDown);
	DDX_Control(pDX, IDC_BUTTON4, m_btnUp);
	DDX_Control(pDX, IDC_COMBO1, m_wndCombo);
	DDX_Control(pDX, AFX_IDC_TREECTRL, m_ViewTree);
	DDX_Control(pDX, IDC_PRINTORDER_LIST, m_PrintOrderList);
	DDX_Text(pDX, IDC_ORDERFILENAME, m_strPOF);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPrintOrderDlg, CDialog)
	//{{AFX_MSG_MAP(CPrintOrderDlg)
	ON_BN_CLICKED(IDC_BUTTON1, OnBtnAdd)
	ON_BN_CLICKED(IDC_BUTTON2, OnBtnDel)
	ON_BN_CLICKED(IDC_BUTTON3, OnBtnReplace)
	ON_WM_DESTROY()
	ON_NOTIFY(NM_DBLCLK, AFX_IDC_TREECTRL, OnDblclkViewTree)
	ON_NOTIFY(NM_DBLCLK, IDC_PRINTORDER_LIST, OnDblclkPrintorderList)
	ON_WM_CONTEXTMENU()
	ON_BN_CLICKED(IDC_BUTTON4, OnBtnMoveUp)
	ON_BN_CLICKED(IDC_BUTTON5, OnBtnMoveDown)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrintOrderDlg message handlers

BOOL CPrintOrderDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	InitViewTree();

	CString strF(MAKEINTRESOURCE(IDS_LIST_FILE));
	CString strT(MAKEINTRESOURCE(IDS_LIST_TABLE));
	CRect rect;
	m_PrintOrderList.GetWindowRect(&rect);
	m_PrintOrderList.InsertColumn(0, strF, LVCFMT_LEFT, 45);
	m_PrintOrderList.InsertColumn(1, strT, LVCFMT_LEFT, rect.Width() - 70);
	m_PrintOrderList.SendMessage(LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_PrintOrderList.EnableDrag();

	if (m_pPofInfo != NULL)
	{
		m_strPOF = m_pPofInfo->strTempl;
		UpdateData(FALSE);
		if (m_pPofInfo->strFile.GetLength())
		{	
			CPof POFile;
			if (POFile.LoadOrder(m_pPofInfo->strFile))
			{
				InitPrintOrderList(&POFile);
				POFile.m_ParamArray.RemoveAll();
			}
		}
	}

	m_wndCombo.SetCurSel(0);
	m_btnDown.SetImage(IDB_AFXBARRES_DOWN32);
	m_btnDown.m_nFlatStyle = CMFCButton::BUTTONSTYLE_FLAT;

	CString strTemp;
	strTemp.LoadString(IDS_MOVE_UP_CURRENT);
	m_btnDown.SetTooltip(strTemp);//Move Up Current Item.
	m_btnUp.SetImage(IDB_AFXBARRES_UP32);
	m_btnUp.m_nFlatStyle = CMFCButton::BUTTONSTYLE_FLAT;
	strTemp.LoadString(IDS_TOOL_TIP);
	m_btnUp.SetTooltip(strTemp);//"Move Down Current Item"

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPrintOrderDlg::InitViewTree()
{
	// 主视图
	int nItemCount = 0;
	m_ViewTree.SetImageList(&theApp.m_wkspcImages, TVSIL_NORMAL);

	CString strItem;
	strItem.LoadString(IDS_GRAPH_HEAD);
	HTREEITEM hHead = m_ViewTree.InsertItem(strItem, IDI_IPRIFILE, IDI_SPRIFILE);

	strItem.LoadString(IDS_GRAPH_TAIL);
	HTREEITEM hTail = m_ViewTree.InsertItem(strItem, IDI_IPRIFILE, IDI_SPRIFILE);

	strItem.LoadString(IDS_GRAPH_SEPARATOR);
	HTREEITEM hSep = m_ViewTree.InsertItem(strItem, IDI_IPRIFILE, IDI_SPRIFILE);

	strItem.LoadString(IDS_CALIBRATION_REPORT);
	HTREEITEM hReport = m_ViewTree.InsertItem(strItem, IDI_IPRIFILE, IDI_SPRIFILE);

    HTREEITEM hRftGroup,hItem;
#ifdef _LWD
	{

	}
#else
	{
		hRftGroup = m_ViewTree.InsertItem("Rft", IDI_IPRIFILE, IDI_SPRIFILE);
		hItem = m_ViewTree.InsertItem("Rft_Data", IDI_ICELL, IDI_SCELL, hRftGroup);
		m_ViewTree.SetItemData(hItem, ULV_RFT_DATAGROUP);
		hItem = m_ViewTree.InsertItem("Rft_Profile", IDI_ICELL, IDI_SCELL, hRftGroup);
		m_ViewTree.SetItemData(hItem, ULV_RFT_PROFILEGROUP);
	}
#endif
	
	// 加入图表	
	CString strPath = Gbl_AppPath + "\\Template\\Headers\\*.*";	
	CString strName, strExt;
	WIN32_FIND_DATA fd;
	
	HANDLE hFind = ::FindFirstFile(strPath, &fd);
	BOOL bFindNext = (hFind != INVALID_HANDLE_VALUE);
	while (bFindNext)
	{
		strName = fd.cFileName;
		int nPos = strName.ReverseFind('.');
		if (nPos < 0)
		{
			bFindNext = ::FindNextFile(hFind, &fd);
			continue ;
		}
		
		strExt = strName.Right(strName.GetLength() - nPos);
		if (strExt.GetLength() != 4)
		{
			bFindNext = ::FindNextFile(hFind, &fd);
			continue ;
		}

		if (strExt.Left(3).CompareNoCase(".ul"))
		{
			bFindNext = ::FindNextFile(hFind, &fd);
			continue ;
		}

		strName = strName.Left(nPos);
		switch(strExt[3])
		{
		case 'h':
		case 'H':
			{
				HTREEITEM hItem = m_ViewTree.InsertItem(strName, IDI_ICELL, IDI_SCELL, hHead);
				m_ViewTree.SetItemData(hItem, ULV_GRAPHHEAD);
			}
			break;
		case 't':
		case 'T':
			{
				HTREEITEM hItem = m_ViewTree.InsertItem(strName, IDI_ICELL, IDI_SCELL, hTail);
				m_ViewTree.SetItemData(hItem, ULV_GRAPHTAIL);
			}
			break;
		case 'r':
		case 'R':
			{
				HTREEITEM hItem = m_ViewTree.InsertItem(strName, IDI_ITYPE, IDI_STYPE, hSep);
				m_ViewTree.SetItemData(hItem, ULV_GRAPHSEP);
			}
			break;
		default:
			 break;
		}

		bFindNext = ::FindNextFile(hFind, &fd);
	}
	::FindClose(hFind);
	
// 	strItem.LoadString(IDS_BLANK_PAGE);
// 	HTREEITEM hItem = m_ViewTree.InsertItem(strItem, IDI_ITYPE, IDI_STYPE, hSep);
// 	m_ViewTree.SetItemData(hItem, ULV_HOLLOW);
	
	strItem.LoadString(IDS_LOG_DATA);
	HTREEITEM hData = m_ViewTree.InsertItem(strItem, IDI_IPRIFILE, IDI_SPRIFILE);
	strItem.LoadString(IDS_TOOL_INFO);
	HTREEITEM hTool = m_ViewTree.InsertItem(strItem, IDI_IPRIFILE, IDI_SPRIFILE);


	UINT nID[] = { IDS_LOG_PLOT, IDS_WELL_SCHEMATIC, 
		IDS_PERFORATION_SCHEDULE, IDS_TVD_GRAPH,
		IDS_TOOL_SHAPE, IDS_TOOL_PARA_TABLE, 
		IDS_TOOLINFOVIEW, IDS_OUTPUTSTABLE,
		IDS_CURVE_FACTOR_TABLE, IDS_CALIBRATE_CHART, 
		IDS_CALIBRATE_SUMMARY, IDS_CALIBRATE_USER, IDS_CALCOEF_REPORT};
	
	LONG lVT[] = { ULV_GRAPH, ULV_CONSTRUCT, ULV_SCHEDULE,  ULV_CHARTS,
		ULV_TOOLS, ULV_PARAM,ULV_TOOLINFOVIEW, ULV_OUTPUTSTABLE,
		ULV_FACTOR, ULV_CALCHART, ULV_CALSUMMARY, ULV_CALUSER, ULV_CALCOEF };
	int i = 0;
	for (i = 0; i < 4; i++)
	{
#ifdef _LWD
		if (i == 2)
		{
			continue;
		}
#endif
		strItem.LoadString(nID[i]);
		HTREEITEM hItem = m_ViewTree.InsertItem(strItem, IDI_IGRAPH, IDI_SGRAPH, hData);
		m_ViewTree.SetItemData(hItem, lVT[i]);
	}

	for ( ; i < 8; i++)
	{
		strItem.LoadString(nID[i]);
		HTREEITEM hItem = m_ViewTree.InsertItem(strItem, IDI_ITYPE, IDI_STYPE, hTool);
		m_ViewTree.SetItemData(hItem, lVT[i]);
	}

	for ( ; i < 12; i++)
	{
		strItem.LoadString(nID[i]);
		HTREEITEM hItem = m_ViewTree.InsertItem(strItem, IDI_ITYPE, IDI_STYPE, hReport);
		m_ViewTree.SetItemData(hItem, lVT[i]);
	}
	
	strItem.Format("Run");
	HTREEITEM hRunData = m_ViewTree.InsertItem(strItem, IDI_IPRIFILE, IDI_SPRIFILE);
	hItem = m_ViewTree.InsertItem("Run Data", IDI_ITYPE, IDI_ITYPE, hRunData);
	m_ViewTree.SetItemData(hItem, ULV_RUNDATA);

	CString str;
	str.LoadString(IDS_FILE_INFO);
	HTREEITEM hFileInfo = m_ViewTree.InsertItem(str, IDI_IPRIFILE, IDI_SPRIFILE);
	//File Information
	
	str.LoadString(IDS_FILE_INFO);
	hItem = m_ViewTree.InsertItem(str, IDI_ITYPE, IDI_STYPE, hFileInfo);
	m_ViewTree.SetItemData(hItem, ULV_TABLEINFOVIEW);
	str.LoadString(IDS_CUSTOMIZE);
	hItem = m_ViewTree.InsertItem(str, IDI_ITYPE, IDI_ITYPE, hFileInfo);
	m_ViewTree.SetItemData(hItem, ULV_CUSTOMIZEVIEW);	

    strItem.LoadString(IDS_CROSS_PLOT); 
    HTREEITEM hCross = m_ViewTree.InsertItem(strItem, IDI_IPRIFILE, IDI_SPRIFILE); 
    
	strPath = Gbl_AppPath + "\\Template\\Charts\\*.cht";		
	hFind = ::FindFirstFile(strPath, &fd);
	bFindNext = (hFind != INVALID_HANDLE_VALUE);
	while (bFindNext)
	{
        strName = fd.cFileName; 
        int n = strName.ReverseFind('.'); 
        if(n >= 0)
            strName = strName.Left(n); 

        HTREEITEM hItem = m_ViewTree.InsertItem(strName, IDI_ITYPE, IDI_STYPE, hCross);
        m_ViewTree.SetItemData(hItem, ULV_CROSS_PLOT);
		bFindNext = ::FindNextFile(hFind, &fd);
	}
	::FindClose(hFind);

	m_ViewTree.Expand(hHead, TVE_EXPAND);
	m_ViewTree.Expand(hTail, TVE_EXPAND);
	m_ViewTree.Expand(hSep, TVE_EXPAND);
	m_ViewTree.Expand(hData, TVE_EXPAND);
	m_ViewTree.Expand(hTool, TVE_EXPAND);
	m_ViewTree.Expand(hReport, TVE_EXPAND);
	m_ViewTree.Expand(hRftGroup, TVE_EXPAND);
	m_ViewTree.Expand(hRunData, TVE_EXPAND);
	m_ViewTree.Expand(hFileInfo, TVE_EXPAND);
}

void CPrintOrderDlg::InitPrintOrderList(CPof* pPOFile)
{
	pPOFile->InitList(&m_PrintOrderList);
}

void CPrintOrderDlg::AddView(UINT iItem /* = -1 */) 
{
	HTREEITEM hItem = m_ViewTree.GetSelectedItem();
	if (hItem == NULL)
		return;
	
	DWORD dwType = m_ViewTree.GetItemData(hItem);
	if (dwType == 0)
		return;
	
	CString str = m_ViewTree.GetItemText(hItem);
	
	CPrintParam* pParam = NULL;
	switch (dwType)
	{
	case ULV_HOLLOW:
		{
			CInputBox dlg(IDS_PRINT_OPT, IDS_BLANK_HEIGHT, NULL, IM_PAGESIZE);			
			pParam = new CPrintParam;
			pParam->m_dwType = dwType;
			pParam->m_lParam = 0;
			while (dlg.DoModal() == IDOK)
			{
				if (dlg.m_lDepth < 0)
					AfxMessageBox(IDS_INFO_UINT);
				else
				{
					pParam->m_lParam = dlg.m_lDepth;
					break;
				}
			}
			
			if (pParam->m_lParam < 0)
			{
				delete pParam;
				return;
			}
			
		}
		break;
	case ULV_GRAPHHEAD:
		{
			pParam = new CPrintParam;
			pParam->m_dwType = dwType;
			pParam->m_strParam = str + ".ulh";
		}
		break;
	case ULV_GRAPHTAIL:
		{
			pParam = new CPrintParam;
			pParam->m_dwType = ULV_GRAPHHEAD;
			pParam->m_strParam = str + ".ult";
		}
		break;
	case ULV_GRAPHSEP:
		{
			pParam = new CPrintParam;
			pParam->m_dwType = ULV_GRAPHHEAD;
			pParam->m_strParam = str + ".ulr";
		}
		break;
// 2020.3.9 Ver1.6.0 TASK【002】 Start
//	case ULV_RFT_DATAGROUP:
//		{
//			pParam = new CPrintParam;
//			pParam->m_dwType = ULV_RFT_DATAGROUP;
//			pParam->m_strParam = "Rft_Data";
//		}
//		break;
//	case ULV_RFT_PROFILEGROUP:
//		{
//			pParam = new CPrintParam;
//			pParam->m_dwType = ULV_RFT_PROFILEGROUP;
//			pParam->m_strParam = "Rft_Profile";
//		}
//		break;
// 2020.3.9 Ver1.6.0 TASK【002】 End
	case ULV_CALCHART:
	case ULV_CALUSER:
	default:
		{
			pParam = new CPrintParam;
			pParam->m_dwType = dwType;
		}
		break;
	}
	
	if (pParam == NULL)
		return;
	
	CString strText;
	m_wndCombo.GetWindowText(strText);

	int nItem = m_PrintOrderList.GetItemCount();

	LV_ITEM item;
	item.mask = LVIF_TEXT|LVIF_PARAM;
	item.iSubItem = 0;
	item.pszText = (LPTSTR)(LPCTSTR)strText;
	item.lParam = (LPARAM)pParam;
	
	if (iItem < nItem)
	{
		item.iItem = iItem;
		CPrintParam* pPP = (CPrintParam*)m_PrintOrderList.GetItemData(iItem);
		m_PrintOrderList.SetItem(&item);
		m_PrintOrderList.SetItemText(iItem, 1, str);
		if (pPP != NULL)	
			delete pPP;
	}
	else
	{
		item.iItem = nItem;
		m_PrintOrderList.InsertItem(&item);
		m_PrintOrderList.SetItemText(nItem, 1, str);
	}
}

void CPrintOrderDlg::OnBtnAdd() 
{
	int nItem = m_PrintOrderList.GetItemCount();
	AddView(nItem);
}

void CPrintOrderDlg::OnBtnDel() 
{
	POSITION pos = m_PrintOrderList.GetFirstSelectedItemPosition();
	CArray<int,int> arItems;
	arItems.Add(-1);
	while (pos != NULL)
	{
		int iItem = m_PrintOrderList.GetNextSelectedItem(pos);
		for (int i=0; i<arItems.GetSize(); i++)
		{
			if(iItem > arItems[i])
			{
				arItems.InsertAt(i, iItem);
				break;
			}
		}
	}
	
	int nItems = arItems.GetSize()-1;
	for (int i=0; i<nItems; i++)
	{
		CPrintParam* pParam = (CPrintParam*)m_PrintOrderList.GetItemData(arItems[i]);
		if (pParam != NULL)	
			delete pParam;
		m_PrintOrderList.DeleteItem(arItems[i]);
	}
}

void CPrintOrderDlg::OnBtnReplace() 
{
	POSITION pos = m_PrintOrderList.GetFirstSelectedItemPosition();
	if (pos != NULL)
	{		
		int iItem = m_PrintOrderList.GetNextSelectedItem(pos);
		AddView(iItem);
	}
}

void CPrintOrderDlg::OnOK() 
{
	UpdateData();

	if (m_strPOF.IsEmpty())
	{
		AfxMessageBox(IDS_NOEXSIT_NOTEMPNAME);
		return ;
	}

	if (IsValidFileName(m_strPOF) != 0)
	{
		AfxMessageBox(IDS_INVALID_FILENAME);
		return;
	}

	int nCount = m_PrintOrderList.GetItemCount();
	if (nCount < 1)
		return ;

	CString strPath = Gbl_AppPath + "\\Template\\Prints\\";
	if (_tchdir(strPath) == -1)
		if (_tmkdir(strPath) == -1)
			return ;

	CPof POFile;
	for (int i = 0; i < nCount; i++)
	{
		CPrintParam* pParam = (CPrintParam*)m_PrintOrderList.GetItemData(i);
		if (pParam == NULL)
			continue ;

		CString str = m_PrintOrderList.GetItemText(i, 0);
		if (str == "Fx")
			pParam->m_nFx = -1;
		else if (str == "Fn")
			pParam->m_nFx = -2;
		else
		{
			str = str.Right(str.GetLength()-1);
			pParam->m_nFx = _ttol(str);
		}
		
		POFile.m_ParamArray.Add(pParam);
	}

	if (POFile.m_ParamArray.GetSize())
	{
		if (m_pPofInfo && m_pPofInfo->strFile.GetLength())
		{
			if(m_pPofInfo->strTempl == m_strPOF)
				strPath = m_pPofInfo->strFile;
			else
				strPath = strPath + m_strPOF + ".ulo";
		}
		else
			strPath = strPath + m_strPOF + ".ulo";

		if (m_nMode & POF_ADD)
		{
			CFileFind ff;
			if (ff.FindFile(strPath))
			{
				CString strPrompt;
				AfxFormatString1(strPrompt, IDS_EXSIT_FILE, strPath);
				if (AfxMessageBox(strPrompt, MB_OKCANCEL) != IDOK)
				{
					POFile.m_ParamArray.RemoveAll();
					return ;
				}
			}
		}

		if (!POFile.SaveOrder(strPath))
		{
			POFile.m_ParamArray.RemoveAll();
			return ;
		}

		if (m_pPofInfo && m_pPofInfo->pPof)
			m_pPofInfo->pPof->LoadOrder(strPath);
	}

	POFile.m_ParamArray.RemoveAll();
	CDialog::OnOK();
}

void CPrintOrderDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	int nCount = m_PrintOrderList.GetItemCount();
	for (int i = 0; i < nCount; i++)
	{
		CPrintParam* pParam = (CPrintParam*)m_PrintOrderList.GetItemData(i);
		if (pParam != NULL)	
			delete pParam;
	}	
}

void CPrintOrderDlg::OnDblclkViewTree(NMHDR* pNMHDR, LRESULT* pResult)
{
	OnBtnAdd();

	*pResult = 0;
}

void CPrintOrderDlg::OnDblclkPrintorderList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnBtnDel();
	
	*pResult = 0;
}

void CPrintOrderDlg::OnContextMenu(CWnd* pWnd, CPoint point) 
{
//	if (pWnd != &m_PrintOrderList)
	{
		CDialog::OnContextMenu(pWnd, point);
		return;
	}
	
	if (point != CPoint (-1, -1))
	{
		//---------------------
		// Select clicked item:
		//---------------------
		CPoint ptList = point;
		m_PrintOrderList.ScreenToClient (&ptList);
		
		int nHitItem = m_PrintOrderList.HitTest (point);
	}
	
	m_PrintOrderList.SetFocus ();
// 	theApp.GetContextMenuManager ()->ShowPopupMenu(IDR_WATCHED_VARIABLE,
// 		point.x, point.y, this, TRUE);
}

void CPrintOrderDlg::OnBtnMoveUp() 
{
	// Up
	POSITION pos = m_PrintOrderList.GetFirstSelectedItemPosition();
	if (pos != NULL)
	{
		int nItem = m_PrintOrderList.GetNextSelectedItem(pos);
		if (nItem == 0)
			return;
		
		int nTo = nItem - 1;
		m_PrintOrderList.DropItemTo(nItem, nTo);
	}
}

void CPrintOrderDlg::OnBtnMoveDown() 
{
	// Down
	POSITION pos = m_PrintOrderList.GetFirstSelectedItemPosition();
	if (pos != NULL)
	{
		int nItem = m_PrintOrderList.GetNextSelectedItem(pos);
		int nTo = nItem + 2;
		if (nTo > m_PrintOrderList.GetItemCount())
			return;

		m_PrintOrderList.DropItemTo(nItem, nTo);
	}
}


// Sheet.cpp: implementation of the CSheet class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Sheet.h"
#include "DSF.h"
#include "XMLSettings.h"
#include "MainFrm.h"

#include "GraphView.h"
#include "ScoutFunctionDef.h"
#include "ULTool.h"
#include "Kernel.h"
#include "GraphWnd.h"
#include "ChildFrm.h"
#include <io.h>
#include "resource.h"

typedef	std::vector<ICurve*>	vector_icurve;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSheet::CSheet()
{
	InitSheet();
}

CSheet::CSheet(int nMode)
{
	InitSheet();
	m_nGraphMode = nMode;
}

CSheet::CSheet(LPCTSTR pszPathName)
{
	InitSheet();
	if (!LoadSheet(pszPathName))
		return;
}

CSheet::CSheet(CSheet* pSheet)
{
	InitSheet();
	CXMLSettings xml(FALSE);
	pSheet->Serialize(xml);
	xml.m_bReadOnly = TRUE;
	Serialize(xml);
}

void CSheet::InitSheet()
{
	m_nRef = 0;
	
	m_strFile = "";
	m_crBack  = RGB(255,255,255);
	//	m_crBack  = RGB(  0,  0,  0);
	m_nRatioX = 1.0;
	m_nRatioY = 200;
	m_lStartDepth = _DEF_SDEPTH;
	m_lEndDepth = _DEF_EDEPTH;
	m_lJumpDepth = 0;
	m_nDriveMode = UL_DRIVE_DEPT;
	
	m_bAutoSize = TRUE;
	
	m_szPage = CSize(2160, 3000);
	m_nGraphMode = MODE_VERT;
	
	m_strName = "NONAME";
	m_TrackList.RemoveAll();
	m_selCurves.clear();
	m_dDelta = 0;
	m_nMaxCurve = 0;
	
	m_nHotDivider = -1;
	m_crHotDivider = RGB(0, 128, 128);

	m_lDepthOffset = 0;			//Add by zy 2011 9 19

	m_bTimeTop = TRUE;			// add by bao 2012 12 17
	m_lJumpTime = 0;
	m_fRatioTime = 100;
	m_lStartTimeLogic = 0;
	m_lStartTime = _DEF_STIME;
	m_lEndTime = _DEF_ETIME;

	m_bBrowseFlag = FALSE;
}

CSheet::~CSheet()
{
	ClearTracks();
	m_vecGraph.clear();
	m_selCurves.clear();
}

void CSheet::SetRatioX(float nRatio)
{
	m_nRatioX = nRatio;
	for(int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->m_fRatioX = nRatio;	
	}
}

void CSheet::SetRatioY(int nRatio)
{
	m_nRatioY = nRatio;	
	int i = 0;
	for(i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->m_fRatioY = nRatio;
	}
	
	// 通知当前所有与本绘图相关的视图更新页面大小
	for( i=0; i<m_vecGraph.size(); i++)
		m_vecGraph.at(i)->m_pGraphWnd->SetScrollInformation();
}

void CSheet::SetRatioTimeY(float nRatio)
{
	m_fRatioTime = nRatio;	
	int i = 0;
	for(i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->m_fRatioTime = nRatio;
	}
	
	// 通知当前所有与本绘图相关的视图更新页面大小
	for( i=0; i<m_vecGraph.size(); i++)
		m_vecGraph.at(i)->m_pGraphWnd->SetScrollInformation();
}

void CSheet::SetTimeStart(BOOL bTop)
{
	m_bTimeTop = bTop;	
	for(int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->m_bTimeTop = bTop;
	}
}

void CSheet::SetStartTimeLogic(long lStartTime/* = 0*/, BOOL bTrue/* = TRUE*/)
{
#ifndef _LOGIC
	m_lStartTimeLogic = lStartTime/m_fRatioTime;	
	int i = 0;
	for(i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->m_bTimeTop = m_bTimeTop;
		pTrack->m_lStartTimeLogic = lStartTime/m_fRatioTime;
	}

	SetDepthRange(m_lStartTime, m_lEndTime);

	for( i=0; i<m_vecGraph.size(); i++)
	{
		CGraphWnd* pGraph = m_vecGraph.at(i)->m_pGraphWnd;
		if(bTrue) pGraph->m_bTimeTopModify = TRUE;
		
		pGraph->InvalidateAll(TRUE);
		
		if (!m_nDriveMode)
		{
			int nPos = pGraph->GetCoordinateFromTime(m_lStartTime);
			CPoint pt = pGraph->GetScrollPosition();
			pGraph->ScrollToPosition(CPoint(pt.x, nPos));
		}
	}
#endif
}

void CSheet::SetGraphMode(int nMode)
{
	int i = 0;
	for (i = 0; i < m_TrackList.GetSize() ; i++)
	{
		CTrack* pTrack = m_TrackList.GetAt(i);
		pTrack->m_nDriveMode = m_nDriveMode; 
	
		if (m_nDriveMode == 0)//初始化横格线
		{	
			pTrack->m_GridInfo.hLine[0].nGridStep = 6000;  //modify by gj
			pTrack->m_GridInfo.hLine[1].nGridStep = 30000;
			pTrack->m_GridInfo.hLine[2].nGridStep = 60000;
		}
		else
		{
			pTrack->m_GridInfo.hLine[0].nGridStep = MTD(1);
			pTrack->m_GridInfo.hLine[1].nGridStep = MTD(5);
			pTrack->m_GridInfo.hLine[2].nGridStep = MTD(25);		
		}
	}

	for( i=0; i<m_vecGraph.size(); i++)
	{
		CGraphWnd* pGraph = m_vecGraph.at(i)->m_pGraphWnd;
		
		pGraph->m_nDriveMode = m_nDriveMode;
	}
	
	switch(m_nDriveMode)
	{
	case 0:
		SetDepthRange(m_lStartTime, m_lEndTime);
		break;
	case 1:
		SetDepthRange(m_lStartDepth, m_lEndDepth);
		break;
	default:
		break;
	}	
	
	SetRatioX(m_nRatioX);
	if (m_nDriveMode)
	{
		SetRatioY(m_nRatioY);
	}
	else
	{
		SetRatioTimeY(m_fRatioTime);
	}
}

void CSheet::MRatioY(double dK)
{
	m_nRatioY*=dK;
	int i = 0;
	for(i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->m_fRatioY = m_nRatioY;
	}
	
	// 通知当前所有与本绘图相关的视图更新页面大小
	for( i=0; i<m_vecGraph.size(); i++)
		m_vecGraph.at(i)->m_pGraphWnd->SetScrollInformation();
}

void CSheet::AddTrack(CTrack* pTrack)
{
	ASSERT(pTrack != NULL);
	m_TrackList.Add(pTrack);
	pTrack->m_fRatioX = m_nRatioX;
	pTrack->m_fRatioY = m_nRatioY;
}

void CSheet::AddNewTrack(CTrack* pTrack)
{
	ASSERT (pTrack != NULL);
	m_TrackList.Add(pTrack);
	pTrack->m_fRatioX = m_nRatioX;
	pTrack->m_fRatioY = m_nRatioY;
	
	AutoSize();
}

void CSheet::InsertTrack(int nIndex, int nTrackType)
{
	if (nIndex < 0)
		return;
	
	int nTrack = m_TrackList.GetSize();
	if (nIndex > nTrack)
		return;
	
	CTrack* pTrack = new CTrack;
	pTrack->SetType(nTrackType);
	pTrack->m_strName.Format("T%d", ++nTrack);
	m_TrackList.InsertAt(nIndex, pTrack);
	pTrack->m_fRatioX = m_nRatioX;
	pTrack->m_fRatioY = m_nRatioY;
	
	// AutoSize();
	RecalcLayout();
}

void CSheet::RemoveTrack(int nIndex)
{
	if (nIndex < 0)
		return ;
	
	if (nIndex < m_TrackList.GetSize())
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(nIndex);
		m_TrackList.RemoveAt(nIndex);
		if (pTrack != NULL)
		{
			for (int i = m_selCurves.size() - 1; i > -1; i--)
			{
				CCurve* pCurve = (CCurve*)m_selCurves.at(i);
				if (pCurve != NULL && pCurve->m_pTrack == pTrack)
					m_selCurves.erase(m_selCurves.begin() + i);
			}
			
			if (g_pMainWnd->m_wndPropertiesBar.m_pTrack == pTrack)
			{
				g_pMainWnd->m_wndPropertiesBar.m_pTrack = NULL;
			}
			delete pTrack;
		}
		
		// AutoSize();
		RecalcLayout();
	}
}

BOOL CSheet::RemoveTrack(CTrack* pTrack)
{
	for( int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack1 = (CTrack*)m_TrackList.GetAt(i);
		if (pTrack == pTrack1)
		{
			m_TrackList.RemoveAt(i);
			for (int i = m_selCurves.size() - 1; i > -1; i--)
			{
				CCurve* pCurve = (CCurve*)m_selCurves.at(i);
				if (pCurve != NULL && pCurve->m_pTrack == pTrack1)
					m_selCurves.erase(m_selCurves.begin() + i);
			}
			if (g_pMainWnd->m_wndPropertiesBar.m_pTrack == pTrack1)
			{
				g_pMainWnd->m_wndPropertiesBar.m_pTrack = NULL;
			}
			delete pTrack1;
			// AutoSize();
			RecalcLayout();
			return TRUE;
		}
	}
	return FALSE;
}

CRect CSheet::GetTrackRect(int nIndex)
{
	CRect rc;
	rc.SetRectEmpty();
	if (0 <= nIndex && nIndex < m_TrackList.GetSize())
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(nIndex);
		pTrack->GetRegionRect(&rc, TRUE);
	}
	return rc;
}

void CSheet::SetTrackRect(int nIndex, LPRECT lpRect)
{
	if (0 <= nIndex && nIndex < m_TrackList.GetSize())
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(nIndex);
		pTrack->SetRegionRect(lpRect);	
		pTrack->SetHeadRectLR(lpRect);
	}
	
}

int CSheet::GetTrackIndex(CTrack* pTrack, LPRECT lpRect)
{
	lpRect->left = 10;

	CRect rect1;
	int nTrack = m_TrackList.GetSize();
	for (int i = 0; i < nTrack; i++)
	{
		CTrack* pTrack1 = (CTrack*)m_TrackList.GetAt(i);
		if (pTrack1 == pTrack)
		{
			if (i < (nTrack-1))
			{
				pTrack1 = (CTrack*)m_TrackList.GetAt(i+1);
				pTrack1->GetRegionRect(rect1, TRUE);
				lpRect->right = rect1.right - PAGE_MARGIN;
			}
			else
				lpRect->right = 10000 - 10;

			return i;
		}

		pTrack1->GetRegionRect(rect1, TRUE);
		lpRect->left = rect1.left - PAGE_MARGIN;
	}

	return -1;
}

CRect CSheet::GetHeadRect(int nIndex)
{
	CRect rc;
	rc.SetRectEmpty();
	if (0 <= nIndex && nIndex < m_TrackList.GetSize())
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(nIndex);
		pTrack->GetHeadRect(rc, FALSE);
	}
	return rc;
}

void CSheet::SetHeadRect(int nIndex, CRect rc)
{
	if (0 <= nIndex && nIndex < m_TrackList.GetSize())
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(nIndex);
		pTrack->SetHeadRect(rc);
	}
}

//------------------------
// Curve Operatin :
//------------------------
void CSheet::ResetAllCurveData()
{
	for(int i=0; i<m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		for(int j=0; j<pTrack->m_vecCurve.size(); j++)
		{
			CCurve* pCurve = (CCurve*)pTrack->m_vecCurve.at(j);
			pCurve->ResetData();
		}
	}
	
}

void CSheet::RemapTrackCurve(CULKernel* pULKernel)
{
	if (pULKernel == NULL)
		return;
	
	m_BackCurveList.RemoveAll();
	
	// 通过仪器数组得到仪器上所对应的曲线
	for(int i = 0; i < pULKernel->m_arrTools.GetSize(); i++)
	{
		CULTool* pULTool = (CULTool*)pULKernel->m_arrTools.GetAt(i);
		
		for( int j = 0; j < pULTool->m_Curves.GetSize(); j++)
		{
			CCurve* pCurve = (CCurve*)pULTool->m_Curves.GetAt(j);
			m_BackCurveList.Add (pCurve);
			
			if (m_nGraphMode == MODE_VERT)
			{
				for( int k = 0; k < m_TrackList.GetSize(); k++)
				{
					CTrack* pTrack = (CTrack*)m_TrackList.GetAt(k);
					pTrack->ApplyNewCurve(pCurve);
				}
			}
		}
	}	
}

void CSheet::RemapTrackCurve()
{
	for(int i = 0; i < m_BackCurveList.GetSize(); i++)
	{
		CCurve* pCurve = (CCurve*)m_BackCurveList.GetAt(i);
		if (m_nGraphMode == MODE_VERT)
		{
			for( int k = 0; k < m_TrackList.GetSize(); k++)
			{
				CTrack* pTrack = (CTrack*)m_TrackList.GetAt(k);
				pTrack->ApplyNewCurve(pCurve);
			}
		}
	}	
}

void CSheet::RemapTrackCurve(std::vector<ICurve*>& vecCurve)
{
	for(int i = 0; i < vecCurve.size(); i++)
	{
		CCurve* pCurve = (CCurve*)vecCurve.at(i);
		if(pCurve->m_strName == _T("AziRe"))
		{
			TRACE(pCurve->m_strName);
		}
		if (m_nGraphMode == MODE_VERT)
		{
			for( int k = 0; k < m_TrackList.GetSize(); k++)
			{
				CTrack* pTrack = (CTrack*)m_TrackList.GetAt(k);
				pTrack->ApplyNewCurve(pCurve);
			}
		}
	}
}

void CSheet::RemapTrackCurve(vec_data* pDatas)
{
	for (int i = 0; i < pDatas->size(); i++)
	{
		CCurveData* pData = pDatas->at(i);
		for (int j = 0; j < m_TrackList.GetSize(); j++)
		{
			CTrack* pTrack = (CTrack*)m_TrackList.GetAt(j);
			pTrack->ApplyNewData(pData);
		}
	}
}

void CSheet::AutoMap(CURVES& vecCurve)
{
	if (m_TrackList.GetSize() < 1)
		return;
	
	CTrack* pTrack = m_TrackList.GetAt(0);
	if (pTrack == NULL)
		return;
	
	for (int i = 0; i < vecCurve.size(); i++)	
	{
		CCurve* pCurve = (CCurve*)vecCurve.at(i);
		if (pCurve->IsDepthCurve() /*&& (pTrack->m_nType != UL_TRACK_DEPTH)*/)
		{
			continue;
		}
		
		CCurve* pNewCurve = new CCurve(NB_TRACK);
		pNewCurve->Assign(*pCurve);
		pNewCurve->AttachData(pCurve->m_pData);
		pTrack->AddCurve(pNewCurve);	
	}
}

void CSheet::ReMapPerforationCurve(vec_data* pDatas)
{
	// 查找射孔专用绘图模板，如果存在，就加载使用	
	if (_access(SHT_FILE("Perforation"), 0) != -1)
	{
		LoadSheet("Perforation", TRUE);
	}
	
/*	CURVES vecCurve;
	for (int i = 0; i < pDatas->size(); i++)
	{
		CCurveData* pData = pDatas->at(i);
		vecCurve.push_back(pData->m_pSrcCurve);
	}
	ReMapPerforationCurve(vecCurve);*/
}


// 调用者：CULFile
// 当打开的数据文件为射孔数据文件时调用该函数。
// 函数功能：重新布局射孔数据曲线
void CSheet::ReMapPerforationCurve(CURVES& vecCurve)
{
	if (m_TrackList.GetSize() < 1)
		return;

	int iCount = 0;
	int nTrackCount = m_TrackList.GetSize();

	// 井道中只有一条CCL曲线，首先需要把这条CCL曲线从井道中删除掉
	int i = 0;
	for (i = 0; i < nTrackCount; i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		if (pTrack == NULL)
			continue;

		BOOL bFound = FALSE;
		for (int j = 0; j < pTrack->m_vecCurve.size(); j++)
		{
			CCurve* pCurve = (CCurve*)pTrack->m_vecCurve.at(j);
			if ((pCurve != NULL) && (pCurve->GetCurveMode() & CURVE_GUN))
			{
				pTrack->RemoveCurve(pCurve);
				bFound = TRUE;
				break;
			}
		}
		if (bFound)
			break;
	}

	// 遍历数据曲线列表
	// 如果数据曲线列表中的曲线在井道中存在，就将它显示出来
	// 如果数据曲线列表中存在多条CCL曲线，就使用固定格式将它显示
	for (i = 0; i < vecCurve.size(); i++)	
	{
		CCurve* pCurve = (CCurve*)vecCurve.at(i);
		if (pCurve == NULL)
			continue;

		// 如果是CCL曲线，将曲线按照固定格式进行放置
		if (pCurve->GetCurveMode() & CURVE_GUN)
		{			
			iCount++;
			if (2*iCount < (nTrackCount-1))
			{
				// 对于往返多次测井数据来说，每间隔一个井道放次一个CCL数据
				CTrack* pTrack = m_TrackList.GetAt(2*iCount);	
				if (NULL != pTrack)
				{
					CCurve* pNewCurve = new CCurve(NB_TRACK);
					pNewCurve->Assign(*pCurve);
					pNewCurve->AttachData(pCurve->m_pData);
					pTrack->AddCurve(pNewCurve);
				}
			}
			continue;
		}

		// 如果是其它曲线，只是应用曲线即可
		for (int j = 0; j < nTrackCount; j++)
		{
			CTrack* pTrack = (CTrack*)m_TrackList.GetAt(j);
			if (pTrack == NULL)
				continue;

			pTrack->ApplyNewCurve(pCurve);
		}
	}

	// 将没有曲线并没有节箍数据的井道删除掉
/*	for (i = nTrackCount-1; i >= 0; i--)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		if ((pTrack != NULL) && (pTrack->m_vecCurve.size()==0) 
			&& (pTrack->m_Marker.GetMarkSize() == 0))
		{
			RemoveTrack(pTrack);
		}
	}*/
}


// 调用者：void CGraphWnd::StopWatch(CGraphWnd* pGraph)
// 当射孔测井点击快照时调用该函数。
// 函数功能：重新布局绘图模板上射孔数据曲线
//void CSheet::ReMapPerforationCurve(CPerforationCurves* pstCurve, int nSize)
void CSheet::ReMapPerforationSheet(CURVES& vecCurve)
{
	//Start:查找射孔专用绘图模板，如果存在，就加载使用	
	if(_access(SHT_FILE("Perforation"), 0) != -1)
	{
		LoadSheet("Perforation", TRUE);
	}
	//End:查找射孔专用绘图模板，如果存在，就加载使用	


	//Start: 清除绘图模板
	int nTrackCount = m_TrackList.GetSize();
	if (nTrackCount < 1)
		return;
	int i = 0;
	for (i = 0; i < nTrackCount; i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		if (pTrack == NULL)
			continue;

		UINT nSizeCurve = pTrack->m_vecCurve.size();
		for (int j = 0; j < nSizeCurve; j++)
		{
			CCurve* pCurve = pTrack->m_vecCurve.at(j);
			if( NULL==pCurve ||pCurve->IsDepthCurve() )
				continue;
			pTrack->m_vecCurve.erase(pTrack->m_vecCurve.begin()+j);
		}
	}
	//End: 清除绘图模板


	//Start:将CCL曲线添加到绘图模板
	int iCount = 0;
	for (i = 0; i < vecCurve.size(); i++)	
	{
		CCurve* pCurve = (CCurve*)vecCurve.at(i);
		if (pCurve == NULL)
			continue;
		
		iCount++;
		if (2*iCount < (nTrackCount-1))
		{
			// 对于往返多次测井数据来说，每间隔一个井道放次一个CCL数据
			CTrack* pTrack = m_TrackList.GetAt(2*iCount);	
			if (NULL != pTrack)
			{
				pTrack->AddCurve(pCurve);
			}
		}
	}
	//End:将CCL曲线添加到绘图模板
	

	// 井道中只有一条CCL曲线，首先需要把这条CCL曲线从井道中删除掉
/*	for (int i = 0; i < nTrackCount; i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		if (pTrack == NULL)
			continue;

		BOOL bFound = FALSE;
		for (int j = 0; j < pTrack->m_vecCurve.size(); j++)
		{
			CCurve* pCurve = (CCurve*)pTrack->m_vecCurve.at(j);
			if ((pCurve != NULL) && (pCurve->GetCurveMode() & CURVE_GUN))
			{
				pTrack->RemoveCurve(pCurve);
				bFound = TRUE;
				break;
			}
		}
		if (bFound)
			break;
	}

	// 遍历数据曲线列表
	// 如果数据曲线列表中的曲线在井道中存在，就将它显示出来
	// 如果数据曲线列表中存在多条CCL曲线，就使用固定格式将它显示
	for (i = 0; i < vecCurve.size(); i++)	
	{
		CCurve* pCurve = (CCurve*)vecCurve.at(i);
		if (pCurve == NULL)
			continue;

		// 如果是CCL曲线，将曲线按照固定格式进行放置
		if (pCurve->GetCurveMode() & CURVE_GUN)
		{			
			iCount++;
			if (2*iCount < (nTrackCount-1))
			{
				// 对于往返多次测井数据来说，每间隔一个井道放次一个CCL数据
				CTrack* pTrack = m_TrackList.GetAt(2*iCount);	
				if (NULL != pTrack)
				{
					CCurve* pNewCurve = new CCurve(NB_TRACK);
					pNewCurve->Assign(*pCurve);
					pNewCurve->AttachData(pCurve->m_pData);
					pTrack->AddCurve(pNewCurve);
				}
			}
			continue;
		}

		// 如果是其它曲线，只是应用曲线即可
		for (int j = 0; j < nTrackCount; j++)
		{
			CTrack* pTrack = (CTrack*)m_TrackList.GetAt(j);
			if (pTrack == NULL)
				continue;

			pTrack->ApplyNewCurve(pCurve);
		}
	}*/

	// 将没有曲线并没有节箍数据的井道删除掉
/*	for (i = nTrackCount-1; i >= 0; i--)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		if ((pTrack != NULL) && (pTrack->m_vecCurve.size()==0) 
			&& (pTrack->m_Marker.GetMarkSize() == 0))
		{
			RemoveTrack(pTrack);
		}
	}*/
}

BOOL CSheet::AddCurveToTrack(CCurve* pCurve, int nIndex)
{
	if (nIndex < m_TrackList.GetSize())
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(nIndex);
		pTrack->AddCurve(pCurve);
		return TRUE;
	}
	
	return FALSE;
}

void CSheet::RemoveCurve(CCurve* pCurve)
{
	for( int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);		
		if(pTrack->RemoveCurve(pCurve))
			return ;
	}	
}

void CSheet::RemoveAllCurve()
{
	for( int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);	
		pTrack->m_vecCurve.clear();		
	}	
}

void CSheet::GetAllCurve(CPtrArray& arrCurve)
{
	arrCurve.RemoveAll();
	for( int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);	
		for( int j=0; j < pTrack->m_vecCurve.size(); j++)
			arrCurve.Add( pTrack->m_vecCurve.at(j) );
	}
}

int CSheet::GetAllSeledCurve(CPtrArray& arrCurve)
{
	arrCurve.RemoveAll();
	int nCount = 0;
	for(int i=0; i<m_TrackList.GetSize(); i++)
	{
		CTrack *pTrack = (CTrack *)m_TrackList.GetAt(i);
		
		if(pTrack->GetType() == UL_TRACK_DEPTH) // 略过深度道
			continue;
		
		for(int j=0; j<pTrack->m_vecCurve.size(); j++)
		{
			CCurve* pCurve = (CCurve*)pTrack->m_vecCurve.at(j);
			if(pCurve->m_bSelected)
			{
				arrCurve.Add(pCurve);
				nCount++;
			}
		}
	}
	return nCount;	
}

CString CSheet::GetAllSeledCurveName()
{
	CString strName = "";
	for(int i=0; i<m_TrackList.GetSize(); i++)
	{
		CTrack *pTrack = (CTrack *)m_TrackList.GetAt(i);
		if (pTrack->GetType() == UL_TRACK_DEPTH) // 略过深度道
			continue ;
		
		for(int j=0; j<pTrack->m_vecCurve.size(); j++)
		{
			CCurve* pCurve = (CCurve*)pTrack->m_vecCurve.at(j);
			if(pCurve->m_bSelected)
				strName += pCurve->m_strName + ", ";
		}
	}
	
	if(strName.GetLength() > 3)
	{
		strName = strName.Left(strName.GetLength() - 2);
		strName += ". ";
	}
	else
		strName = "(NULL).";
	return strName;
}

void CSheet::SelectAllCurve()
{
	m_selCurves.clear();
	for(int i=0; i<m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		for (int j=0; j<pTrack->m_vecCurve.size(); j++)
		{
			CCurve* pCurve = (CCurve*)pTrack->m_vecCurve.at(j);
			pCurve->m_bSelected = TRUE;
			m_selCurves.push_back(pCurve);
		}
	}
}

void CSheet::InvertSelCurve()
{
	m_selCurves.clear();
	for(int i=0; i<m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		for (int j=0; j<pTrack->m_vecCurve.size(); j++)
		{
			CCurve* pCurve = (CCurve*)pTrack->m_vecCurve.at(j);
			pCurve->m_bSelected = !pCurve->m_bSelected;
			if(pCurve->m_bSelected)
				m_selCurves.push_back(pCurve);
		}
	}
}

/*++

函数名称：

  Default

函数描述：

  加载系统默认的绘图模板。

函数参数：

  void

返回类型：

  void

--*/
void CSheet::Default()
{
	// 系统创建一个模板
	int nTotalWidth = m_szPage.cx;
	int nAverWidth = (nTotalWidth - (PAGE_MARGIN*2)) / 4;
	CRect rc;
	rc.SetRectEmpty();
	
	m_bAutoSize = FALSE;
	{
		CTrack* pTrack = new CTrack();
		pTrack->SetColor(RGB(0, 0, 0));
		//pTrack->SetType(UL_TRACK_LINEAR);
		pTrack->SetName("T1");
		AddNewTrack(pTrack);
	}
	
	{
		CTrack* pTrack = new CTrack();
		pTrack->SetColor(RGB(0, 0, 0));
		//pTrack->SetType(UL_TRACK_DEPTH);
		pTrack->m_GridInfo.bHasHLine = FALSE;
		pTrack->m_GridInfo.bHasVLine = FALSE;
		pTrack->SetName("T2");
		AddNewTrack(pTrack);
	}
	
	{
		CTrack* pTrack = new CTrack();
		pTrack->SetColor(RGB(0, 0, 0));
		pTrack->SetName("T3");
		//pTrack->SetType(UL_TRACK_LINEAR);
		AddNewTrack(pTrack);
		
	}
	
	/*	{
	CTrack* pTrack = new CTrack();
	pTrack->SetColor(RGB(128, 0, 128));
	pTrack->SetName("T4");
	pTrack->SetType(UL_TRACK_LINEAR);
	AddNewTrack(pTrack);
	}
	
	*/	
	m_bAutoSize = TRUE;
	{
		CTrack* pTrack = new CTrack();
		//pTrack->SetType(UL_TRACK_IMAGE);
		pTrack->SetColor(RGB(0, 0, 0));		
		pTrack->SetName("T4");
		AddNewTrack(pTrack);
	}
	/*
	for(int i=0; i<4; i++)
	{
	CTrack *pTrack = new CTrack;
	m_TrackList.Add(pTrack);
	pTrack->m_nRatioX = m_nRatioX;
	pTrack->m_nRatioY = m_nRatioY;
	}
	*/
}

void CSheet::AutoSize()
{
	int nLen = m_TrackList.GetSize();   // 井道数
	if (nLen < 1)
		return;					// 没有井道
	
	if (!m_bAutoSize)
		return;

	// ------------------------------------------------------------------------
	m_szPage.cx = DEFAULT_PAGE_WIDTH;
	int nTotalWidth = m_szPage.cx; // 总宽度
	
	int nAverWidth, nDepthTrackCount = 0;
	for (int k = 0; k < nLen; k++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(k);
		if (pTrack->GetType() == UL_TRACK_DEPTH)
			nDepthTrackCount++;
	}
	if (nLen != nDepthTrackCount)
		nAverWidth = (nTotalWidth - (PAGE_MARGIN * 2) - DEFAULT_DEPTH_TRACK_WIDTH * nDepthTrackCount) / (nLen - nDepthTrackCount);	// 井道的平均宽度
	
	// 井道的平均宽度
	
	CRect rc, rc1;  
	rc.SetRectEmpty();	// 临时变量
	
	// 计算第一个井道宽度
	rc = GetTrackRect(0);    
	rc.right = PAGE_MARGIN * 2;   //一个纸到到左边边界的margin,一个井道到纸的margin
	CRect rcHead;
	
	// 计算其他井道的宽度
	for( int i = 0; i < nLen; i++)
	{	
		rc1 = rc;
		rc1.left = rc.right;
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		if (pTrack->GetType() == UL_TRACK_DEPTH)
			rc1.right = rc1.left + DEFAULT_DEPTH_TRACK_WIDTH;
		else
			rc1.right = rc1.left + nAverWidth;
		SetTrackRect(i, rc1);
		rcHead = GetHeadRect(i);
		rcHead.top = -2 * PAGE_MARGIN;
		rcHead.bottom = rcHead.top - GetScrollMax() - 2*PAGE_MARGIN;
		SetHeadRect(i,rcHead);
		rc = rc1;
	}
}

void CSheet::RecalcLayout()
{
	int nTrack = m_TrackList.GetSize();
	if (nTrack < 1)
		return;

	CRect rcLeft;
	CTrack* pTrack = (CTrack*)m_TrackList.GetAt(0);
	pTrack->GetRegionRect(rcLeft, TRUE);

	CRect rcTrack;
	// 计算其他井道的宽度
	for (int i = 1; i < nTrack; i++)
	{
		pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->GetRegionRect(rcTrack, TRUE);
		rcTrack.right = rcLeft.right + rcTrack.Width();
		rcTrack.left = rcLeft.right;
		pTrack->SetHeadRectLR(rcTrack);
		pTrack->SetRegionRect(rcTrack);
		rcLeft = rcTrack;
	}
}

int CSheet::CalcPageWidth()
{
	int nTrack = m_TrackList.GetSize();
	if (nTrack)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(nTrack-1);
		nTrack = pTrack->RegionRight();
	}
//Modify by xx 2015-12-16
//防止设置道宽过小时，打印后道宽会改变
// 	if (nTrack < DEFAULT_PAGE_WIDTH)
// 		nTrack = DEFAULT_PAGE_WIDTH;

	return nTrack;
}

int CSheet::GetScrollMax()
{
	m_nMaxCurve = 1;
	
	CTrack* pTrack = NULL;
	for (int i = 0; i < m_TrackList.GetSize(); i++)
	{
		pTrack = (CTrack*)m_TrackList.GetAt(i);
		if(pTrack->GetType()==UL_TRACK_TEXT)
		{
			if(m_nMaxCurve<1)
			{
				m_nMaxCurve=1;		
			}
			continue;
		}
		
		int nSize =  pTrack->m_vecCurve.size();
		int nShowCount = 0;
		for (int j = 0; j < nSize; j++)
		{
			ICurve* pCurve = pTrack->m_vecCurve.at(j);
			
			nShowCount += pCurve->CalcTitle();
		}
		
		if (m_nMaxCurve < nShowCount)
			m_nMaxCurve = nShowCount;
	}
	
	return (m_nMaxCurve + 1) * 100;
}

void CSheet::ExchangeTrackPosition(int nFrom, int nTo)
{
	int nLen = m_TrackList.GetSize();
	if (nTo < 0 || nFrom < 0)
		return ;
	if (nFrom >= nLen)
		return ;
	
	if (nTo > nLen)
		nTo = nLen;
	
	CTrack* pTrackFrom = (CTrack*)m_TrackList.GetAt(nFrom);
	CRect rc0;
	((CTrack*)m_TrackList.GetAt(0))->GetHeadRect(rc0, TRUE);
	
	CRect rc;
	if (nTo == 0)
	{
		pTrackFrom->GetRegionRect(&rc, TRUE);	// 得到要交换的井道大小信息
		rc.left = rc0.left;
		rc.right = rc.left + pTrackFrom->RegionWidth();
		pTrackFrom->SetRegionRect(rc);
		
		pTrackFrom->GetHeadRect(rc, TRUE);
		rc.left = rc0.left;
		rc.right = rc.left + pTrackFrom->RegionWidth();
		pTrackFrom->SetHeadRect(rc);
	}
	
	if (nTo < nFrom)
	{
		m_TrackList.RemoveAt(nFrom);
		m_TrackList.InsertAt(nTo, pTrackFrom);
	}
	else
	{
		m_TrackList.InsertAt(nTo, pTrackFrom);
		m_TrackList.RemoveAt(nFrom);
	}
	
	// Recalibrate Layout
	
	int nWidth = 0;
	CRect rc1;
	rc = GetTrackRect(0);
	
	if (nFrom == 0)
	{
		rc1 = rc;
		rc1.left = rc0.left;
		rc1.right = rc1.left + rc.Width();
		SetTrackRect(0, rc1);
		rc = rc1;
	}
	
	for( int i = 1; i < nLen; i++)
	{
		rc1 = GetTrackRect(i);
		nWidth = rc1.Width();
		
		rc1 = rc;
		rc1.left = rc.right;
		rc1.right = rc1.left + nWidth;
		
		SetTrackRect(i, rc1);
		
		rc = rc1;
	}
}

void CSheet::SetTrackEdge(int nEdge, int x, BOOL bDP /* = TRUE */)
{
	if (bDP)
		x = PAGE_MARGIN + floor(((double)(x - PAGE_MARGIN))/m_nRatioX + .5);
	
	CRect rc = GetTrackRect(nEdge);
	rc.left = x;
	SetTrackRect(nEdge, rc);

	rc = GetTrackRect(nEdge-1);
	rc.right = x;
	SetTrackRect(nEdge-1, rc);
}

// -----------------------------------------------------------------------
//	显示模式
// -----------------------------------------------------------------------

void CSheet::SetLogMode(int nWorkMode, int nDriveMode)
{
	for( int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->SetLogMode(nWorkMode, nDriveMode);
	}
}

void CSheet::SetModeShow()
{
	for (int i=0;i<m_TrackList.GetSize();i++)
	{
		CTrack *pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->SetModeShow(m_bShowTime, m_bShowDepth);
	}
}

void CSheet::SetDepthRange(long  lStart, long  lEnd)
{
	for( int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		switch(m_nDriveMode)
		{
		case 0:
			m_lStartTime = lStart;
			m_lEndTime = lEnd;
			pTrack->m_lStartTime = lStart;
			pTrack->m_lEndTime   = lEnd;
			break;
		case 1:
			m_lStartDepth = lStart;
			m_lEndDepth = lEnd;
			pTrack->m_lStartDepth = lStart; 
			pTrack->m_lEndDepth = lEnd;
			break;
		default:
			break;
		}
	}
}

void CSheet::DrawHead(CDC* pDC, LPRECT lpRect, BOOL bTitle /* = TRUE */)
{
	CFont font,*pOldFont;
	font. CreateFont (40, 
		0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, 
		OUT_STROKE_PRECIS, CLIP_STROKE_PRECIS, DRAFT_QUALITY, 
		VARIABLE_PITCH | FF_SWISS, _T ("Arial")); 
	
	pOldFont = pDC->SelectObject (&font);
	
	for( int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->SetHeadRectTB(lpRect);
		pTrack->DrawHead(pDC, bTitle);
	}
	
	if (m_nHotDivider > -1)
		DrawHotDivider(pDC);
	
	pDC->SelectObject (pOldFont);
}

void CSheet::DrawHotDivider(CDC* pDC)
{
	pDC->SetROP2(R2_XORPEN);
	
	int nOffset = 0;
	int nItems = m_TrackList.GetSize();
	CRect rectItem;
	if (m_nHotDivider < nItems)
	{
		rectItem = GetHeadRect(m_nHotDivider);
		nOffset = rectItem.left - 1;
	}
	else
	{
		rectItem = GetHeadRect(nItems - 1);
		nOffset = rectItem.right;
	}
	
	rectItem.top = rectItem.bottom - 50;
	
	CPen penDivider(PS_SOLID, 2, m_crHotDivider);
	CPen* pOldPen = pDC->SelectObject(&penDivider);
	
	CBrush brushDivider(m_crHotDivider);
	CBrush* pOldBrush = pDC->SelectObject(&brushDivider);
	
	CPoint points[3];
	int nX = 26, nY = 25;
	points[0] = CPoint(nOffset - nX, rectItem.top - nY);
	points[1] = CPoint(nOffset, rectItem.top);
	points[2] = CPoint(nOffset + nX, rectItem.top - nY);
	
	pDC->Polygon(points, 3);
	
	points[0] = CPoint(nOffset - nX, rectItem.bottom + nY + 1);
	points[1] = CPoint(nOffset, rectItem.bottom);
	points[2] = CPoint(nOffset + nX, rectItem.bottom + nY + 1);
	
	pDC->Polygon(points, 3);
	
	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
}

void CSheet::DrawHorzGrid(CDC* pDC, long lStartDepth, long lEndDepth)
{
	for( int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		/*CFont *font=pDC->GetCurrentFont();// 改变深度道深度的显示字体
		LOGFONT logFont;
		font->GetLogFont(&logFont);
		logFont.lfHeight=40;
		strcpy(logFont.lfFaceName,"Arial");
		CFont newFont;
		newFont.CreateFontIndirect(&logFont);
		CFont *pOldFont=pDC->SelectObject(&newFont);*/
		if (m_nDriveMode == UL_DRIVE_DEPT)
		{
			pTrack->DrawHorzGrid(pDC,  lStartDepth, lEndDepth);
		} 
		else
		{
			pTrack->DrawHorzGridTime(pDC,  lStartDepth, lEndDepth);
		}
        //   pDC->SelectObject(pOldFont);
		
	}
}

void CSheet::DrawMark(CDC* pDC, long lStartDepth, long lEndDepth)
{
	for( int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		if (m_nDriveMode == UL_DRIVE_DEPT)
		{
			pTrack->DrawMark(pDC,  lStartDepth, lEndDepth);
		} 
		else
		{
		//	pTrack->DrawMarkTime(pDC,  lStartDepth, lEndDepth);
		}
	}
}
/***函数：DrawCurveFlag
****功能：绘制曲线标识
****作者：xwh
****创建时间：2013-9-5
****修订记录：XWH2013-9-10注释 由曲线属性框触发绘制曲线标识，原方法暂时注释
****修订记录：无
****/
/*
void CSheet::DrawCurveFlag(CDC* pDC , long lStartDepth , long lEndDepth)
{
	for( int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		if (m_nDriveMode == UL_DRIVE_DEPT)
		{
			pTrack->DrawCurveFlag(pDC,  lStartDepth, lEndDepth);
		} 
		else
		{
			//	pTrack->DrawMarkTime(pDC,  lStartDepth, lEndDepth);
		}
	}
}*/
void CSheet::DrawVertGrid(CDC* pDC , long lStartDepth , long lEndDepth)
{
	// 为了兼容以前的文件，可以考虑如果含有深度曲线就不画格线
	for( int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		if (pTrack == NULL)
			continue;

		BOOL bHasDepth = FALSE;
		for (int j = 0; j < pTrack->m_vecCurve.size(); j++)
		{
			CCurve* pCurve = pTrack->m_vecCurve.at(j);
			if (pCurve == NULL)
				continue;
			if ((pCurve->IsDepthCurve() && m_nDriveMode == UL_DRIVE_DEPT) || (pCurve->IsTimeCurve() && m_nDriveMode == UL_DRIVE_TIME))
			{
				bHasDepth = TRUE;
			}
		}
		if (!bHasDepth)
		{
			if(m_nDriveMode == UL_DRIVE_DEPT)
				pTrack->DrawVertGrid(pDC,  lStartDepth, lEndDepth);
			else
				pTrack->DrawVertGridTime(pDC, lStartDepth, lEndDepth);
		}
	}
}

void CSheet::DrawSheet(CDC* pDC, long lStart, long lEnd, int nMode/* =0 */)
{
	for( int i = 0; i < m_TrackList.GetSize(); i++)
	{ 
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		if (m_nDriveMode == UL_DRIVE_DEPT)
		{
			pTrack->DrawTrack(pDC, lStart, lEnd, nMode);
			//pTrack->DrawArea(pDC, lStart, lEnd, nMode);
			pTrack->m_Marker.DrawMark(pDC, m_lStartDepth, m_lEndDepth);
		} 
		else
		{		
			pTrack->DrawTrackTime(pDC, lStart, lEnd, nMode);
//			pTrack->m_Marker.DrawMark(pDC, m_lStartDepth, m_lEndDepth);

		}
	}
}

void CSheet::DrawFill(CDC* pDC, long lStart, long lEnd, int nMode/* =0 */)
{
	for( int i = 0; i < m_TrackList.GetSize(); i++)
	{ 
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		if (m_nDriveMode == UL_DRIVE_DEPT)
		{
			pTrack->DrawFill(pDC, lStart, lEnd, nMode);
		} 
		else
		{//modify by gj 20130123
			pTrack->DrawFillTime(pDC, lStart, lEnd, nMode);
		}
	}
}

CRect CSheet::DrawThumb(CDC* pDC, LPRECT lpRect, long lt, long lb)
{
	int nTrack = m_TrackList.GetSize();
	int nWidth = 0;
	
	std::vector<int> vecWidth;
	
	CCurve* pDCurve = NULL;
	CCurve* pCurve0 = NULL;
	CRect rcTrack;
	int i = 0;
	for (i = 0; i < nTrack; i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->GetRegionRect(&rcTrack, FALSE);
		vecWidth.push_back(rcTrack.Width());
		nWidth += vecWidth[i];
		
		for (int j = 0; j < pTrack->m_vecCurve.size(); j++)
		{
			CCurve* pCurve = (CCurve*)pTrack->m_vecCurve[j];
			if (pCurve->IsDepthCurve() && pCurve->m_pData)
				pDCurve = pCurve;
			else if (pCurve->m_pData)
				pCurve0 = pCurve;
		}
	}

	if (NULL == pDCurve)
		pDCurve = pCurve0;
	
	if (pDCurve == NULL || pDCurve->GetDataSize() < 2)
		return CRect(0, 0, 0, 0);
	
	// 计算纵向比例
	m_ls = pDCurve->GetDepth(0);
	long le = pDCurve->GetDepthED();
	if (le < m_ls)
	{
		long l = m_ls;
		m_ls = le;
		le = l;
	}
	int nPoint = abs(lpRect->top - lpRect->bottom);
	m_dDelta = abs(le - m_ls)/(double)nPoint;
	
	// 计算横向比例
	CPoint ptTop(0, lpRect->top);
	CPoint ptBottom(0, lpRect->bottom);
	
	CPen pen(PS_SOLID, 2, RGB(0, 0, 0)/*::GetSysColor(COLOR_INFOBK)*/);
	pDC->SelectObject(&pen);
	int rcWidth = abs(lpRect->right - lpRect->left);
	double dk = (double)rcWidth/(double)nWidth;
	
	std::vector<CRect> vecRect;
	CRect rc(ptTop, ptBottom);
	for(i=0; i<nTrack; i++)
	{
		if(i)
		{
			pDC->MoveTo(ptTop);
			pDC->LineTo(ptBottom);
		}
		
		int nT = (int)floor(vecWidth[i]*dk + .5);
		ptTop.x += nT;
		ptBottom.x += nT;
		rc.right = ptTop.x;
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->DrawThumb(pDC, rc, m_ls, m_dDelta, nPoint);
		rc.left = rc.right;
	}
	
	
	rc.left = lpRect->left;
	rc.right= lpRect->right;
	rc.top = lpRect->top + (lt - m_ls)/m_dDelta;
	rc.bottom = lpRect->top + (lb - m_ls)/m_dDelta;
	
	if(rc.bottom < lpRect->top)
	{
		rc.top = lpRect->top;
		rc.bottom = lpRect->top;
	}
	
	if(rc.top > lpRect->bottom)
	{
		rc.top = lpRect->bottom;
		rc.bottom = lpRect->bottom;
	}
	
	return rc;
}

CRect CSheet::CalcThumb(LPRECT lpRect, long lt, long lb)
{
	int nTrack = m_TrackList.GetSize();
	int nWidth = 0;
	
	std::vector<int> vecWidth;
	
	CCurve* pDepthCurve = NULL;
	CRect rcTrack;
	for(int i=0; i<nTrack; i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->GetRegionRect(&rcTrack, FALSE);
		vecWidth.push_back(rcTrack.Width());
		nWidth += vecWidth[i];
		
		for(int j=0; j<pTrack->m_vecCurve.size(); j++)
		{
			CCurve* pCurve = (CCurve*)pTrack->m_vecCurve[j];
			if((pCurve->m_strName=="DEPT")||(pCurve->m_strName=="DEPTH"))
				pDepthCurve = pCurve;
		}
	}
	
	if(pDepthCurve == NULL || pDepthCurve->GetDataSize() < 2)
		return CRect(0, 0, 0, 0);
	
	// 计算纵向比例
	m_ls = pDepthCurve->GetDepth(0);
	long le = pDepthCurve->GetDepthED();
	if(le < m_ls)
	{
		long l = m_ls;
		m_ls = le;
		le = l;
	}
	int nPoint = abs(lpRect->top - lpRect->bottom);
	m_dDelta = abs(le - m_ls)/(double)nPoint;
	
	CRect rc;
	rc.left = lpRect->left;
	rc.right= lpRect->right;
	rc.top = lpRect->top + (lt - m_ls)/m_dDelta;
	rc.bottom = lpRect->top + (lb - m_ls)/m_dDelta;
	
	if(rc.bottom < lpRect->top)
	{
		rc.top = lpRect->top;
		rc.bottom = lpRect->top;
	}
	
	if(rc.top > lpRect->bottom)
	{
		rc.top = lpRect->bottom;
		rc.bottom = lpRect->bottom;
	}
	
	return rc;
}

CRect CSheet::CalcThumb(LPRECT lpRect, CPoint pt, int nMode, long& lt, long& lb)
{
	if(m_dDelta == 0)
		return CRect(0, 0, 0, 0);
	
	CRect rc;
	rc.left = lpRect->left;
	rc.right= lpRect->right;
	
	if(nMode & dragWnd)
	{
		double dh = abs(lt - lb)/(2*m_dDelta);
		rc.top = pt.y - dh;
		rc.bottom = pt.y + dh;
		lt = m_ls + rc.top*m_dDelta;
		lb = m_ls + rc.bottom*m_dDelta;
	}
	
	if(nMode & dragTop)
	{
		rc.top = pt.y;
		rc.bottom = (lb - m_ls)/m_dDelta;
		lt = m_ls + rc.top*m_dDelta;
	}
	
	if(nMode & dragBtm)
	{
		rc.bottom = pt.y;
		rc.top = (lt - m_ls)/m_dDelta;
		lb = m_ls + rc.bottom*m_dDelta;
	}
	
	return rc;
}

BOOL CSheet::IsCurveSelected()
{
	for( int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		if (pTrack->IsCurveSelected())
			return TRUE;
	}
	return FALSE;
}

BOOL CSheet::IsSeledCurvesDel()
{
	for (int i=0; i<m_selCurves.size(); i++)
	{
		if(!m_selCurves[i]->m_bDelete)
			return FALSE;
	}
	
	return TRUE;
}

BOOL CSheet::IsSeledCurvesExt()
{
	for (int i=0; i<m_selCurves.size(); i++)
	{
		if(!m_selCurves[i]->m_bDelete)
			return TRUE;
	}
	return FALSE;
}

void CSheet::EraseAllSeledCurve()
{
	for (int i = 0; i < m_selCurves.size(); i++)
	{
		CCurve* pCurve = (CCurve*)m_selCurves.at(i);
		pCurve->m_bSelected = FALSE;
	}
	m_selCurves.clear();
}

void CSheet::EraseSeledCurve(CCurve* pCurve)
{
	for (int i=0; i<m_selCurves.size(); i++)
	{
		if (m_selCurves[i] == pCurve)
		{
			m_selCurves.erase(m_selCurves.begin() + i);
			break;
		}
	}
}

/* -------------- *
*  得到深度曲线
* -------------- */
CCurve* CSheet::GetDepthCurve()
{
	for (int i=0; i<m_TrackList.GetSize(); i++)
	{
		CTrack * pTrack = (CTrack *) m_TrackList.GetAt(i);
		for (int j=0; j<pTrack->m_vecCurve.size(); j++)
		{
			CCurve * pCurve = (CCurve *) pTrack->m_vecCurve.at(j);
			CString strName = pCurve->m_strName;
			strName = strName.Left(4);
			strName.MakeUpper ();
			if(strName == "DEPT")
				return pCurve;
		}
	}
	return NULL;
}

//
//函数名：GetDepthCurve
//功能：得到深度值和所有曲线
//
//

CCurve* CSheet::GetDepthCurve(CPtrArray& CurveList)
{
	CCurve* pDepthCurve = NULL;
	CurveList.RemoveAll();
	for (int i=0; i<m_TrackList.GetSize(); i++)
	{
		CTrack * pTrack = (CTrack *) m_TrackList.GetAt(i);
		for (int j=0; j<pTrack->m_vecCurve.size(); j++)
		{
			CCurve * pCurve = (CCurve *) pTrack->m_vecCurve.at(j);
			CurveList.Add(pCurve);
			if(pCurve->m_strName == "DEPT"||pCurve->m_strName == "DEPTH")
				pDepthCurve = pCurve;
		}
	}
	return pDepthCurve;
}

CCurve* CSheet::GetDepthCurve(CURVES& vecCurve)
{
	CCurve* pDepthCurve = NULL;
	vecCurve.clear();
	for (int i=0; i<m_TrackList.GetSize(); i++)
	{
		CTrack * pTrack = (CTrack *) m_TrackList.GetAt(i);
		for (int j=0; j<pTrack->m_vecCurve.size(); j++)
		{
			CCurve* pCurve = (CCurve *) pTrack->m_vecCurve.at(j);
			vecCurve.push_back(pCurve);
			if (pCurve->IsDepthCurve())
				pDepthCurve = pCurve;
		}
	}
	
	return pDepthCurve;
}

CCurve* CSheet::GetCurve(CString strCurveName)
{
	for (int i=0; i<m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		for (int j=0; j<pTrack->m_vecCurve.size(); j++)
		{
			CCurve* pCurve = (CCurve*)pTrack->m_vecCurve.at(j);
			if(pCurve->m_strName == strCurveName)
				return pCurve;
		}
	}
	return NULL;
}

BOOL CSheet::SaveSheet(LPCTSTR strPath /* = NULL */, BOOL bRP /* = FALSE */, BOOL bDelete /* = TRUE */)
{
	CString strSheet = strPath;
	if (strSheet.GetLength())
	{
		if (bRP)
			strSheet = SHT_FILE(strSheet);	
	}
	else
	{
		strSheet = m_strFile;
		if (strSheet.IsEmpty())
			strSheet = SHT_FILE(m_strName);
	}
	
	CXMLSettings xml(FALSE);
	Serialize(xml);
	if (xml.WriteXMLToFile(strSheet))
	{
		if( bDelete && m_strFile.CompareNoCase(strSheet))
			DeleteFile(m_strFile);
		
		AEI(IDS_SAVE, AfxGetAppName(), ET_OTHER, IDS_SAVE_SHEET, _T("OK"), strSheet);
		return TRUE;
	}		
	
	AfxMessageBox(IDS_FAIL_SAVESHEET);
	AEI(IDS_SAVE, AfxGetAppName(), ET_OTHER|ES_WARNING, IDS_SAVE_SHEET, _T("Failed"), strSheet);
	return FALSE;
}

BOOL CSheet::LoadSheet(LPCTSTR pszPath, BOOL bRP /* = FALSE */)
{
	CString strSheet = bRP ? SHT_FILE(pszPath) : pszPath;
	
	CXMLSettings xml;
	if (!xml.ReadXMLFromFile (strSheet))
	{
		AfxMessageBox(IDS_FAIL_LOADSHEET);
		AEI(IDS_LOAD_SHEET, AfxGetAppName(), ET_OTHER|ES_WARNING, IDS_LOAD_SHEET, _T("Failed"), strSheet);
		ClearTracks();
		return FALSE;
	}
	
	Serialize(xml);
	m_strFile = strSheet;
	if (m_strName.IsEmpty() || (m_strFile.Find(SHT_PATH) == 0))
	{
		CString str = strSheet.Right(strSheet.GetLength() - strSheet.ReverseFind('\\') - 1);
		m_strName = str.Left(str.GetLength() - 4);
	}
	
	return TRUE;
}
/*++

函数名称：

  MatchSheet

函数描述：

  根据曲线列表匹配最佳模板并加载模板

函数参数：

  vecCurve - 要匹配的曲线列表

  nCount -   进行匹配的次数，如果nCount小于0表示不进行匹配，直接使用系统默认模板
             (这将导致Default函数的调用)；如果nCount大于0，表示匹配nCount次后退出
			 (不管有没有匹配结果，对于模板数量很多的情况，能节省很多时间)

返回类型：

  UINT - 当前匹配的模板的个数
--*/
UINT CSheet::MatchSheet(vec_ic& vecCurve, UINT nCount /* = -1 */)
{
	CStringList strCurves;
	int nSize = vecCurve.size();
	for (int i = 0; i < nSize; i++)
	{
		strCurves.AddTail(vecCurve.at(i)->Name());
	}
	
	CFileFind ff;
	
	// build a string with wildcards
	CString strPath = SHT_FILE("*");
	
	// start working for files
	BOOL bWorking = ff.FindFile(strPath);
	UINT nMax = 0;
	UINT nFind = 0;
	CXMLSettings xml;
	CString strMap;
	while (bWorking)
	{
		if (nFind > nCount)
			break;
		
		bWorking = ff.FindNextFile();
		
		// skip . and .. files; otherwise, we'd
		// recur infinitely!
		
		if (ff.IsDots())
			continue;
		
		// if it's a directory, recursively search it
		if (ff.IsDirectory())
			continue;
		
		nFind++;
		CString strFile = ff.GetFilePath();
		if (xml.ReadXMLFromFile(strFile, _T("AllCurves")))
		{
			CStringList strFinds;
			xml.Read(_T(""), strFinds);
			UINT nMaped = 0;
			for (POSITION pos = strFinds.GetHeadPosition(); pos != NULL; )
			{
				if (strCurves.Find(strFinds.GetNext(pos)))
					nMaped++;
			}
			
			if (nMaped > nMax)
			{
				nMax = nMaped;
				strMap = strFile;
			}
		}
	}
	
	if (nMax >= 1)
	{
		if (LoadSheet(strMap))
		{
			RemapTrackCurve(vecCurve);
			return nMax;
		}
	}
	
	Default();

	// 对于系统创建的模板，AutoMap函数会导致文件中
	// 所有曲线(除Depth外)都被添加到新创建模板的第
	// 一井道.如果不需要此特性，使用RemapTrackCurve
	AutoMap(vecCurve);
	return 0;
}

void CSheet::Serialize(CXMLSettings& xml)
{
	if (xml.IsStoring())
	{
		if (xml.CreateKey(DSF_ROOT))
		{
			xml.Write(_T("Version"), (DWORD)MAKEWORD(_UL2000_VERSION_MINOR, _UL2000_VERSION_MAJOR));
			xml.Write(_T("Name"), m_strName);
			xml.Write(_T("PageSize"), m_szPage);
			xml.Write(_T("BackColor"), m_crBack);
			xml.Write(_T("DriveMode"), m_nDriveMode);
			xml.Write(_T("GraphMode"), m_nGraphMode);
			xml.Write(_T("RatioX"), m_nRatioX);
			xml.Write(_T("RatioY"), m_nRatioY);
			xml.Write(_T("JumpDepth"), m_lJumpDepth);
			xml.Write(_T("DepthOffset"), m_lDepthOffset);
			if ((m_lStartDepth != _DEF_SDEPTH) || (m_lEndDepth != _DEF_EDEPTH))
			{
				xml.Write(_T("ST_Depth"), m_lStartDepth);
				xml.Write(_T("ED_Depth"), m_lEndDepth);
			}
			xml.Write(_T("ST_Time"),m_lStartTime);
			xml.Write(_T("ED_Time"),m_lEndTime);
			xml.Write(_T("TrackCount"), m_TrackList.GetSize());
			xml.Back();
		}
		
		// Tracks
		CStringList strCurves;
		for (int i = 0; i < m_TrackList.GetSize(); i++)
		{
			CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
			if (xml.CreateKey(DSF_TRACK_IDFMT, i))
			//if (xml.CreateKey("Track"))
			{
				pTrack->Serialize(xml);
				pTrack->AddCurves(&strCurves);
				xml.Back();
			}
		}
		
		xml.Write(_T("AllCurves"), strCurves); 

		xml.Write(_T("RatioTimeY"), m_fRatioTime);
		xml.Write(_T("BTop"), m_bTimeTop);
		xml.Write(_T("StartTimeLogic"), m_lStartTimeLogic);
		xml.Write(_T("JumpTime"), m_lJumpTime);
	}
	else
	{
		ClearTracks();
		
		DWORD m_dwTrack = 0;
		
		if (xml.Open (DSF_ROOT))
		{
			m_strName.Empty();
			xml.Read(_T("Name"), m_strName);
			xml.Read(_T("PageSize"), m_szPage);
			xml.Read(_T("BackColor"), m_crBack);
			xml.Read(_T("DriveMode"),m_nDriveMode);
			m_nGraphMode = MODE_VERT;
			xml.Read(_T("RatioX"), m_nRatioX);
			xml.Read(_T("RatioY"), m_nRatioY);
			if (m_nRatioY < 1 || m_nRatioY > 1001)
			{
				USHORT us = 200;
				xml.Read(_T("RatioY"), us);
				m_nRatioY = us;
			}
			xml.Read(_T("JumpDepth"), m_lJumpDepth);
			xml.Read(_T("ST_Depth"), m_lStartDepth);
			xml.Read(_T("ED_Depth"), m_lEndDepth);
			xml.Read(_T("ST_Time"),m_lStartTime);
			xml.Read(_T("ED_Time"),m_lEndTime);
			xml.Read(_T("DepthOffset"), m_lDepthOffset);
			xml.Read(_T("TrackCount"), m_dwTrack);
			xml.Back();
		}
		int i = 0;
		for (i = 0; i < (int)m_dwTrack; i++)
		{
			CTrack* pTrack = new CTrack;
			if (xml.Open(DSF_TRACK_IDFMT, i))
			//if (xml.Open("Track"))
			{
				pTrack->Serialize(xml);
				xml.Back();
			}
			pTrack->m_nDriveMode = m_nDriveMode; // bao add 使井道能以时间驱动绘画
			
			AddTrack(pTrack);
		}

		CStringList strCurves;
		xml.Read(_T("AllCurves"), strCurves); 
		
		xml.Read(_T("RatioTimeY"), m_fRatioTime);
		xml.Read(_T("BTop"), m_bTimeTop);
		xml.Read(_T("StartTimeLogic"), m_lStartTimeLogic);
		xml.Read(_T("JumpTime"), m_lJumpTime);
		for (i = 0; i < m_TrackList.GetSize(); i++)
		{
			CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
			pTrack->m_fRatioTime = m_fRatioTime;
			pTrack->m_bTimeTop = m_bTimeTop;
			pTrack->m_lStartTimeLogic = m_lStartTimeLogic;
		}
		
		switch(m_nDriveMode)
		{
		case 0:
			SetDepthRange(m_lStartTime, m_lEndTime);
			break;
		case 1:
			SetDepthRange(m_lStartDepth, m_lEndDepth);
			break;
		default:
			break;
		}		
		SetRatioX(m_nRatioX);
		SetRatioY(m_nRatioY);
	}
}

void CSheet::FitPage(int nPageWidth)
{
	int nTracksWidth = 0;
	int i = 0;
	for (i = 0; i < m_TrackList.GetSize (); i++)
	{
		CTrack *pTrack = (CTrack *)m_TrackList.GetAt (i);
		CRect rcRegion;
		pTrack->GetRegionRect(&rcRegion, TRUE);
		nTracksWidth += rcRegion.Width ();
	}

	int nPageSize = nPageWidth - 2 * PAGE_MARGIN;
	if (nPageSize == nTracksWidth) // Adjust once
		return;
	double k = nPageSize * 1.0 / nTracksWidth;
	int nCurPos = PAGE_MARGIN * 2;
	for (i = 0 ; i < m_TrackList.GetSize (); i++)
	{
		CTrack *pTrack = (CTrack *)m_TrackList.GetAt (i);
		CRect rcRegion;
		pTrack->GetRegionRect (&rcRegion, TRUE);
		int nTrackWidth = rcRegion.Width ();
		rcRegion.left = nCurPos;
		nCurPos = (int)(nCurPos + nTrackWidth * k);
		if (i == m_TrackList.GetSize () - 1)
			rcRegion.right = nPageSize + PAGE_MARGIN * 2;
		else
			rcRegion.right = nCurPos;
		pTrack->SetHeadRectLR(rcRegion);
		pTrack->SetRegionRect(rcRegion);
	}
}

int CSheet::GetMaxHeadCurveNum()
{
	int nMaxPrint = 1;
	CTrack* pTrack = NULL;
	for (int i = 0; i < m_TrackList.GetSize(); i++)
	{
		pTrack = (CTrack*)m_TrackList.GetAt(i);
		if(pTrack->GetType()==UL_TRACK_TEXT)
		{
			if(m_nMaxCurve<2)
			{
				m_nMaxCurve=2;	
			}
			continue;
		}
		int nSize =  pTrack->m_vecCurve.size();
		int nPrint = 0;
		pTrack->m_ControlNum.RemoveAll(); // bao 2011/6/9
		for (int j = 0; j < nSize; j++)
		{
			ICurve* pCurve = pTrack->m_vecCurve.at(j);
			if(!pCurve->IsPrintCurveHead())
				continue;
			
			nPrint += pCurve->CalcTitle(CS_PRINT);

			// bao 2011/6/9
			int nLeft = pCurve->LeftMargin(TRUE);
			int nRight = pCurve->RightMargin(TRUE);
			BOOL bExist = FALSE;
			for (int z = 0; z < pTrack->m_ControlNum.GetSize(); z++)
			{
				CONTROLNUM controlNum = pTrack->m_ControlNum.GetAt(z);
				if (controlNum.nRight == nLeft)
				{
					nPrint -= pCurve->CalcTitle(CS_PRINT);
					controlNum.nRight = nRight;
					pTrack->m_ControlNum.RemoveAt(z);
					pTrack->m_ControlNum.InsertAt(z, controlNum, 1);
					bExist = TRUE;
					break;
				}
				if (controlNum.nLeft == nRight)
				{
					nPrint -= pCurve->CalcTitle(CS_PRINT);
					controlNum.nLeft = nLeft;
					pTrack->m_ControlNum.RemoveAt(z);
					pTrack->m_ControlNum.InsertAt(z, controlNum, 1);
					bExist = TRUE;
					break;
				}
			}
			if (!bExist)
			{
				CONTROLNUM newControlNum;
				newControlNum.nLeft =nLeft;
				newControlNum.nRight = nRight;
				pTrack->m_ControlNum.Add(newControlNum);
			}
			// bao end
		}
		
		if (nMaxPrint < nPrint)
			nMaxPrint = nPrint;
	}
	return nMaxPrint;
}
// ---------------------------------
//	打印初始化，返回要井道头打印的高度
// ---------------------------------
int CSheet::PrintHeadInit(int nPrintPos, int nHeadWidth)
{

	CTrack* pTrack = NULL;
	int nMaxPrint = GetMaxHeadCurveNum();
	// Calculate head height
	int nPagesHeight = (++nMaxPrint)*100 + TRACK_MARGIN;
	for (int i = 0; i < m_TrackList.GetSize(); i++)
	{
		pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->m_nPrintCount = nMaxPrint;
		pTrack->SetHeadRectTB(nPrintPos -nPagesHeight - 2*TRACK_MARGIN, 
			nPrintPos - 2*TRACK_MARGIN);
	}
	if (nHeadWidth > 0)
		FitPage(nHeadWidth);
	return nPagesHeight;
}

int CSheet::PrintHead(CULPrintInfo* pPrintInfo, int nY)
{
#define multiline
#ifdef multiline
	
	// Calculate head height
	int nPagesHeight = PrintHeadInit();
	
	// Calculate pages
	CRect rect = pPrintInfo->GetPageInfo();
	if (nY == 0)
		rect = pPrintInfo->SetPageInfo();
	
	int nPageHeight = abs(rect.Height());
	int nPageCount = (nPagesHeight + nY - 1)/nPageHeight;
	
	// Print front pages
	int n = 0;
	for (n = 0; n < nPageCount; n++)
	{
		pPrintInfo->m_printDC.StartPage();
		pPrintInfo->m_printDC.SaveDC();
		pPrintInfo->m_printDC.SetMapMode(MM_LOMETRIC);
		pPrintInfo->m_printDC.SetWindowOrg( 0, - n*nPageHeight + nY);
		for (int i = 0; i < m_TrackList.GetSize(); i++)
		{
			CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
			pTrack->PrintHead(pPrintInfo);
		}
		pPrintInfo->m_printDC.RestoreDC(-1);
		pPrintInfo->m_printDC.EndPage();
	}
	
	// Print last page don't end the page
	pPrintInfo->m_printDC.StartPage();
	pPrintInfo->m_printDC.SaveDC();
	pPrintInfo->m_printDC.SetMapMode(MM_LOMETRIC);
	pPrintInfo->m_printDC.SetWindowOrg( 0, - n*nPageHeight + nY);
	int i = 0;
	for (i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->PrintHead(pPrintInfo);
	}
	
	// Calculate the remain height
	// if the height is zero, end the page
	nY = nPagesHeight + nY - (n*nPageHeight);
	if (nY == 0 || (pPrintInfo->m_dwMode == PM_RTIME))
	{
		pPrintInfo->m_printDC.RestoreDC(-1);
		pPrintInfo->m_printDC.EndPage();
	}
#else
	// Calculate max curve count print in a track
	
	int nNotPrintCount = 0, 
		nPrintCount = 0;
	
	int nCurveNotPrint, nCurvePrint;
	
	for (int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		nCurveNotPrint = 0;
		nCurvePrint = 0;
		
		for (int j = 0; j < pTrack->m_vecCurve.size(); j++)
		{
			CCurve* pCurve = (CCurve*)pTrack->m_vecCurve.at(j);
			if (pCurve->m_bPrint)
				nCurvePrint++;
			else
				nCurveNotPrint++;
		}
		
		// 按最多打印曲线的井道计算
		if (nCurvePrint > nPrintCount)
		{
			nPrintCount = nCurvePrint;
			nNotPrintCount = nCurveNotPrint;
		}
	}
	
	// Calculate head height
	int nPagesHeight = (nPrintCount + 1)*100;
	
	for (i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->m_nNotPrintCount = nNotPrintCount;
		pTrack->m_nPrintCount = nPrintCount;
		pTrack->SetHeadRectTB( -nPagesHeight - 2*TRACK_MARGIN, - 2*TRACK_MARGIN);
	}
	
	// Calculate pages
	CRect rect = pPrintInfo->GetPageInfo();
	if (nY == 0)
		rect = pPrintInfo->SetPageInfo();
	
	int nPageHeight = abs(rect.Height());
	int nPageCount = (nPagesHeight + nY - 1)/nPageHeight;
	
	// Print front pages
	for (int n = 0; n < nPageCount; n++)
	{
		pPrintInfo->m_printDC.StartPage();
		pPrintInfo->m_printDC.SaveDC();
		pPrintInfo->m_printDC.SetMapMode(MM_LOMETRIC);
		pPrintInfo->m_printDC.SetWindowOrg( 0, - n*nPageHeight + nY);
		for (i = 0; i < m_TrackList.GetSize(); i++)
		{
			CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
			pTrack->PrintHead(pPrintInfo);
		}
		pPrintInfo->m_printDC.RestoreDC(-1);
		pPrintInfo->m_printDC.EndPage();
	}
	
	// Print last page don't end the page
	pPrintInfo->m_printDC.StartPage();
	pPrintInfo->m_printDC.SaveDC();
	pPrintInfo->m_printDC.SetMapMode(MM_LOMETRIC);
	pPrintInfo->m_printDC.SetWindowOrg( 0, - n*nPageHeight + nY);
	for (i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->PrintHead(pPrintInfo);
	}
	
	// Calculate the remain height
	// if the height is zero, end the page
	nY = nPagesHeight + nY - (n*nPageHeight);
	if (nY == 0 || (pPrintInfo->m_dwMode == PM_RTIME))
	{
		pPrintInfo->m_printDC.RestoreDC(-1);
		pPrintInfo->m_printDC.EndPage();
	}
#endif
	return nY;
}

void CSheet::PrintVertGrid(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth)
{
	for (int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		if (m_nDriveMode == UL_DRIVE_DEPT)
		{
			pTrack->PrintVertGrid(pInfo, fMaxDepth, fMinDepth);
		}
		else
		{
			pTrack->PrintVertGridTime(pInfo, fMaxDepth, fMinDepth);
		}
	}
}

#define ISBICURVE(a) (a->m_strName == "BI")

void CSheet::PrintLine(CULPrintInfo* pInfo, double fDepth)
{
	int nTrack = m_TrackList.GetSize();
	if (nTrack < 1)
		return;
	
	
	CTrack* pTFirst = m_TrackList.GetAt(0);
	CTrack* pTLast = m_TrackList.GetAt(nTrack-1);
	
	CRect rcRegion;
	pTFirst->GetRegionRect(rcRegion, TRUE);
	rcRegion.right = pTLast->RegionRight();
	rcRegion = pInfo->CoordinateConvert(rcRegion);
	
	CPen penGrid(PS_SOLID, PEN_BORDER_SIZE, RGB(0, 0, 0));
	CPen* pOldPen = pInfo->m_printDC.SelectObject(&penGrid);
	int y = pInfo->GetPrintDepthCoordinate(fDepth);
	CPoint ptStart(rcRegion.left, y);
	CPoint ptEnd(rcRegion.right, y);
	pInfo->CoordinateCalibrate(ptStart);
	pInfo->CoordinateCalibrate(ptEnd);
	pInfo->m_printDC.MoveTo(ptStart);
	pInfo->m_printDC.LineTo(ptEnd);
	pInfo->m_printDC.SelectObject(pOldPen);
}

void CSheet::PrintHorzGrid(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth)
{
	CFont *font=pInfo->m_printDC.GetCurrentFont();
	LOGFONT logFont;
	font->GetLogFont(&logFont);
	logFont.lfHeight=5;
	logFont.lfWidth=2;
	CFont newFont;
	newFont.CreateFontIndirect(&logFont);
	CFont *pOldFont=pInfo->m_printDC.SelectObject(&newFont);
	
	for( int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		if (m_nDriveMode == UL_DRIVE_DEPT)
		{
			pTrack->PrintHorzGrid(pInfo, fMaxDepth, fMinDepth);
		} 
		else
		{
			pTrack->PrintHorzGridTime(pInfo, fMaxDepth, fMinDepth);
		}	
        
		/*
		// 在射孔枪道中打印点火的位置
		if ((m_pTask->m_pGunCurve != NULL) && (m_pTask->m_pGunCurve->m_pTrack != NULL)) 
		{
			if ( m_pTask->m_pGunCurve->m_pTrack == pTrack )
			{
				int nIndex = m_pTask->m_nActive;
				if ( nIndex == -1 )
					break;

				// 得到当前任务
				CShootTaskItem * pShootTaskItem = ( CShootTaskItem * ) m_pTask->m_ItemList[ nIndex ];
				if (!pShootTaskItem->m_bFinished)
					break;

				CRect rcTrack = pTrack->GetRegionRect() ;
				rcTrack = pInfo->CoordinateConvert( rcTrack );
				float fDepth = DTM(pShootTaskItem->m_lFiredDepth - pShootTaskItem->m_lGunTopLength);
				
				//不判断深度范围打印，防止m_dFiredDepth不在深度范围时，而输出的字体高度却要
				//超出当前的深度范围，这样会被剪切掉，而下一页又不打印该文字，导致文字不全
				int y = pInfo->GetPrintDepthCoordinate( fDepth );
				CPen Pen( PS_SOLID, 5, RGB( 0 , 0 , 0 ) );
				CPen * pOldPen = pInfo->m_printDC.SelectObject( & Pen );										
				pInfo->m_printDC.MoveTo( rcTrack.left , y );
				pInfo->m_printDC.LineTo( rcTrack.right, y );
				CString strFire;
				strFire.Format( "%8.2f" , fDepth );
				strFire = "点火位置:" + strFire;
				CSize szText = pInfo->m_printDC.GetTextExtent( strFire ); 
				pInfo->m_printDC.SetBkMode( TRANSPARENT );	 
				
				if ( pInfo->GetPrintDirection() == SCROLL_UP )
				{	
					int x = rcTrack.left + ( rcTrack.right - rcTrack.left ) * 1.0 / 8.0;
					pInfo->SetPrintDirection(SCROLL_DOWN);
					pInfo->PrintText( CSize( 0 , 40 ) , x, y , strFire );
					strFire.Format("倒计数：%.2f", fabs(fDepth-pShootTaskItem->m_dActualStdCol));
					y -= szText.cy ;
					pInfo->PrintText( CSize( 0 , 40 ) , x, y , strFire );
					pInfo->SetPrintDirection(SCROLL_UP);
				}
				else
				{
					y += szText.cy ;  
					int x = rcTrack.left + ( rcTrack.right - rcTrack.left ) * 1.0 / 8.0;
					pInfo->PrintText( CSize( 0 , 40 ) , x, y , strFire );
					
					y += szText.cy ;
					strFire.Format("倒计数：%.2f", fabs(fDepth-pShootTaskItem->m_dActualStdCol));
					pInfo->PrintText( CSize( 0 , 40 ) , x, y , strFire );
					
				}  			
				pInfo->m_printDC.SelectObject( pOldPen );
				
				//画枪
				CRect rect;
				rect.left = (rcTrack.left+rcTrack.right)/2;
				rect.right = rect.left + 20;
				rect.top = pInfo->GetPrintDepthCoordinate( fDepth + DTM(pShootTaskItem->m_lGunTopLength + pShootTaskItem->m_lTotalCorrection));
				rect.bottom = pInfo->GetPrintDepthCoordinate(fDepth + DTM(m_pTask->GetActiveTask()->m_lGunLength + pShootTaskItem->m_lGunTopLength+ pShootTaskItem->m_lTotalCorrection) );
				
				CBrush b(RGB(0,0,0));
				CBrush* pb = pInfo->m_printDC.SelectObject(&b);
				pInfo->m_printDC.FillRect(&rect, &b);
				pInfo->m_printDC.SelectObject(pb);
			}
		}																		  		
		
		// 在深度道中打印油层,根据油层的上顶于下顶之间的距离，进行打印。  Gzh.2004.04.06.
		// 在射孔取芯的时候，需要进行油层的目标层位的显示与打印处理。		
		if ( pTrack->GetType() == UL_TRACK_DEPTH )				// if it is depth track.
		{	
			CRect rcRegion;
			rcRegion.CopyRect( & pTrack->GetRegionRect() );     
			rcRegion = pInfo->CoordinateConvert( rcRegion );
			int nBottom1 = pInfo->GetPrintDepthCoordinate( fMaxDepth );
			int nTop1	 = pInfo->GetPrintDepthCoordinate( fMinDepth );
			
			CCurve* pBiCurve = NULL;
			//寻找BI曲线
			for(int c=0; c<this->m_TrackList.GetSize(); c++)
			{
				if(pBiCurve != NULL)
				{
					break;
				}
				CTrack* pTrack  = (CTrack*)m_TrackList[c];
				for(int d=0; d<pTrack->m_vecCurve.size(); d++)
				{
					CCurve* pCurve = (CCurve*)pTrack->m_vecCurve.at(d);
					if(ISBICURVE(pCurve))
					{
						pBiCurve = pCurve;
						break;
					}
				}
			}
			if(pBiCurve != NULL)
			{
				rcRegion.right = rcRegion.left + ( rcRegion.right - rcRegion.left ) * 1.0 / 5.0;
				
				int nBottom = pBiCurve->GetFirstIndexByDepth(fMaxDepth);
				int nTop = pBiCurve->GetFirstIndexByDepth(fMinDepth);
				int nMaxIndex = max(nBottom, nTop);
				int nMinIndex = min(nBottom, nTop);
				
				int nIndex = nMinIndex;
				BOOL bBegin = FALSE;
				int nBeginIndex = 0;
				int nMode = 0;
				while(nIndex<=nMaxIndex)
				{
					//CCurveValue* pCurveValue = (CCurveValue*)(pBiCurve->m_DataList[nIndex]);
					double dblval = pBiCurve->m_pData->GetdblValue(nIndex);
					long   lDepth = pBiCurve->m_pData->GetDepth(nIndex);
					
					if(bBegin == FALSE)
					{
						if(dblval>=0.4)
						{
							bBegin = TRUE;
							nBeginIndex = nIndex;
							rcRegion.bottom	= pInfo->GetPrintDepthCoordinate(lDepth);
							if(dblval < 0.8)
							{
								nMode = 1;
							}
							else
							{
								nMode = 2;
							}
						}
					}
					else
					{
						if(nMode == 1)
						{
							if(dblval<=0.4 || dblval>=0.8 || nIndex==nMaxIndex)
							{
								rcRegion.top	= pInfo->GetPrintDepthCoordinate(lDepth);
								CBrush brush( RGB( 128, 128, 128 ) );
								pInfo->m_printDC.FillRect( rcRegion , & brush );
								bBegin = FALSE;
								nMode = 0;
							}							
						}
						if(nMode == 2)
						{
							if(dblval <=0.8 || nIndex==nMaxIndex)
							{
								rcRegion.top	= pInfo->GetPrintDepthCoordinate(lDepth);
								CBrush brush( RGB( 0, 0, 0 ) );
								pInfo->m_printDC.FillRect( rcRegion , & brush );
								bBegin = FALSE;
								nMode = 0;
							}
						}
					}
					
					nIndex++;
					
				}
			}
			
			int nSize = m_pTask->m_ItemList.GetSize();	// Get the shoot item list.
			if ( nSize > 0 )
			{	 
				// 如果是预览，则打印所有的油层
				if ( m_pTask->m_nStatus == CShootTask::SHOOT_INPREVIEW )  
				{  
					for ( int i = 0 ; i < nSize ; ++ i )
					{
						CShootTaskItem* pShootTaskItem = (CShootTaskItem*)m_pTask->m_ItemList.GetAt( i );
						if ( AfxIsValidAddress( pShootTaskItem , sizeof( CShootTaskItem ) ) ) 
						{
							long lDepthTop = min( pShootTaskItem->m_lOilTop , pShootTaskItem->m_lOilBottom );
							long lDepthBottom = max( pShootTaskItem->m_lOilTop , pShootTaskItem->m_lOilBottom );
							if ( fMaxDepth > lDepthTop && fMinDepth < lDepthBottom )
							{	
								lDepthTop = max( lDepthTop, fMinDepth );
								lDepthBottom = min( fMaxDepth , lDepthBottom );
								
								// fill the oil field with black brush. 
								rcRegion.bottom	= pInfo->GetPrintDepthCoordinate( lDepthBottom );
								rcRegion.top	= pInfo->GetPrintDepthCoordinate( lDepthTop );
								if ( pInfo->GetPrintDirection() == SCROLL_UP )
								{	   
									if ( i == 0 )
									{
										rcRegion.right = rcRegion.left + ( rcRegion.right - rcRegion.left ) * 1.0 / 8.0; 
									}
								}
								else
								{
									if ( i == 0 )
									{
										rcRegion.left = rcRegion.right - ( rcRegion.right - rcRegion.left ) * 1.0 / 8.0; 
									}
								}
								CBrush brush( RGB( 0, 0, 0 ) );
								pInfo->m_printDC.FillRect( rcRegion , & brush );
							}							   
						}
					}
				}       // 若不是预览，则只打印当前活动的油层。
				else if ( m_pTask->m_nActive >=0 && m_pTask->m_nActive < nSize ) 
				{	
					CShootTaskItem* pShootTaskItem = (CShootTaskItem * ) m_pTask->m_ItemList.GetAt( m_pTask->m_nActive );
					if ( AfxIsValidAddress( pShootTaskItem , sizeof( CShootTaskItem ) ) ) 
					{
						double fDepthTop = DTM(min( pShootTaskItem->m_lOilTop , pShootTaskItem->m_lOilBottom ));
						double fDepthBottom = DTM(max( pShootTaskItem->m_lOilTop , pShootTaskItem->m_lOilBottom ));
						if ( fMaxDepth > fDepthTop && fMinDepth < fDepthBottom )
						{	
							fDepthTop	 = max( fDepthTop, fMinDepth );
							fDepthBottom = min( fMaxDepth , fDepthBottom );
							
							// fill the oil field with black brush. 
							int nBottom	    = pInfo->GetPrintDepthCoordinate( fDepthBottom );
							int nTop	    = pInfo->GetPrintDepthCoordinate( fDepthTop );
							rcRegion.top    = nTop;
							rcRegion.bottom = nBottom;
							
							//打印油层不分上下，一律靠右
							rcRegion.left = rcRegion.right - ( rcRegion.right - rcRegion.left ) * 1.0 / 8.0; 
							CBrush brush( RGB( 0, 0, 0 ) );
							pInfo->m_printDC.FillRect( rcRegion , & brush );
						}	
					}
				}
			}
		}*/
	}
	
	pInfo->m_printDC.SelectObject(pOldFont);
	newFont.DeleteObject();
}

void CSheet::PrintMark(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth)
{
	CFont *font=pInfo->m_printDC.GetCurrentFont();
	LOGFONT logFont;
	font->GetLogFont(&logFont);
	logFont.lfHeight=5;
	logFont.lfWidth=2;
	CFont newFont;
	newFont.CreateFontIndirect(&logFont);
	CFont *pOldFont=pInfo->m_printDC.SelectObject(&newFont);
	
	for( int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		
		pTrack->PrintMark(pInfo, fMaxDepth, fMinDepth);
	}
	
	pInfo->m_printDC.SelectObject(pOldFont);
}


void CSheet::PrintSheet(CULPrintInfo* pInfo, double  fMaxDepth, double fMinDepth)
{
	for( int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->PrintTrack(pInfo, fMaxDepth , fMinDepth );
	}
}

void CSheet::StaticPrint(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth)
{
	for(int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->StaticPrint(pInfo, fMaxDepth, fMinDepth);
		pTrack->m_Marker.PrintMark(pInfo);
	}
}

void CSheet::PrintFill(CULPrintInfo* pInfo, double fMaxDepth, double fMinDepth)
{
	for(int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->PrintFill(pInfo, fMaxDepth, fMinDepth);
	}
}

void CSheet::Watch(BOOL bTrace /* = TRUE */)
{
	int nTracks = m_TrackList.GetSize();
	for(int i=0; i<nTracks; i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		int nCurves = pTrack->m_vecCurve.size();
		for(int j=0; j<nCurves; j++)
		{
			CCurve* pCurve = (CCurve*)pTrack->m_vecCurve.at(j);
			// do sth ...
			TRACE("%d, %d, %s, %s\n", i, j, pTrack->m_strName, pCurve->m_strName);
		}
	}
}

void CSheet::PrintCurveFlags(CULPrintInfo* pInfo, long lDepth, int nCurve, BOOL bMode)
{
	if(m_nRatioY == 1000 && !AfxGetComConfig()->m_Plot .bPrintMark1000)
		return;
	if(m_nRatioY == 500 && !AfxGetComConfig()->m_Plot .bPrintMark500)
		return;


	for( int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->PrintCurveFlags(pInfo, lDepth, nCurve, bMode);
	}
}

// 打印自动标注，该函数最终会调用CTrack::PrintCurveFirst
BOOL CSheet::PrintCurveFirst(CULPrintInfo* pInfo, long lMaxDepth, long lMinDepth, BOOL bMode)
{
	BOOL bResult = FALSE;
	int i = 0;
	for(i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->m_ptArray.RemoveAll();
	}

	for (i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		if(i >0)
		{
			CTrack *pTempTrack = (CTrack*)m_TrackList.GetAt(i - 1);
			for(int j = 0 ; j < pTempTrack->m_ptArray.GetSize() ; j++)
				pTrack->m_ptArray.Add(pTempTrack->m_ptArray[j]);

		}

		pTrack->PrintCurveFirst(pInfo, lMaxDepth, lMinDepth, bMode);
	}
	return bResult;
}

void CSheet::DrawGraph(CDC* pDC, LPRECT lpRect, LPRECT lpRectDraw, long lTop, long lBottom, BOOL bTwice /* = TRUE */)
{
	CRect rect = lpRect;
	
	int nHH = GetScrollMax() + 2*PAGE_MARGIN;
	CRect rc1 = rect;
	rc1.bottom = rc1.top - nHH;
	rc1.NormalizeRect();
	
	CRect rc2 = rect;
	if (bTwice)
	{
		rc2.top = rc2.bottom + nHH;
		rc2.NormalizeRect();
		rect.bottom = rc2.bottom;
	}
	else
		rc2.top = rect.bottom;
	
	rect.top = rc1.top;
	
	long lBegin = 0;
	switch(m_nDriveMode)
	{
	case 0:
		lBegin = lTop + rect.top * m_fRatioTime;
		break;
	case 1:
		lBegin = lTop + rect.top * m_nRatioY;
		break;
	default:
		break;
	}
	int i = 0;
	// Prepare set track region
	for(i = 0; i < m_TrackList.GetSize(); i ++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		CRect rcRegion;
		pTrack->GetRegionRect(rcRegion, TRUE);
		
		rcRegion.top = rect.top;
		rcRegion.bottom = rect.bottom;
		pTrack->SetRegionRect(rcRegion, FALSE);
		switch(m_nDriveMode)
		{
		case 0:
			pTrack->m_lStartTime  = lBegin;
			break;
		case 1:
			pTrack->m_lStartDepth = lBegin;
			break;
		default:
			break;
		}
	}
	
	// 计算显示深度范围
	long lStart = lTop;
	long lEnd = lBottom;
	if ((lpRectDraw != NULL) && (m_TrackList.GetSize () > 0))
	{
		CTrack* pTrack = (CTrack *)m_TrackList.GetAt(0);
		CRect rcDraw = lpRectDraw;
		rcDraw.top += PAGE_MARGIN;
		rcDraw.bottom -= PAGE_MARGIN;
		
		switch(m_nDriveMode)
		{
		case 0:
			if (rect.top < rcDraw.top)
				lStart = pTrack->GetTimeFromCoordinate(rect.top);
			else
				lStart = pTrack->GetTimeFromCoordinate(rcDraw.top);
			
			if (rect.bottom > rcDraw.bottom)
				lEnd = pTrack->GetTimeFromCoordinate(rect.bottom);
			else
				lEnd = pTrack->GetTimeFromCoordinate(rcDraw.bottom);
			break;
		case 1:
			if (rect.top < rcDraw.top)
				lStart = pTrack->GetDepthFromCoordinate(rect.top);
			else
				lStart = pTrack->GetDepthFromCoordinate(rcDraw.top);
			
			if (rect.bottom > rcDraw.bottom)
				lEnd = pTrack->GetDepthFromCoordinate(rect.bottom);
			else
				lEnd = pTrack->GetDepthFromCoordinate(rcDraw.bottom);
			break;
		default:
			break;
		}	
	}
	
	CCurve::m_bSavingbmp = TRUE;
	
//	DrawVertGrid(pDC, lStart, lEnd);
//	DrawHorzGrid(pDC, lStart, lEnd);
//	DrawSheet(pDC, lStart, lEnd, 0);

	if(AfxGetComConfig()->m_Watch.nLayerCount == 0) // 默认情况
	{
		DrawFill(pDC, lStart, lEnd, 0);
		DrawVertGrid(pDC, lStart, lEnd);
		DrawHorzGrid(pDC, lStart, lEnd);
		DrawSheet(pDC, lStart, lEnd, 0);
		DrawMark(pDC, lStart, lEnd);		
	}
	else
	{
		int i = 0;
		for(i = 0 ; i < AfxGetComConfig()->m_Watch.nLayerCount ; i++)
		{
			switch(AfxGetComConfig()->m_Watch.Layer[i])
			{
				case LAYER_FILL:
					DrawFill(pDC, lStart, lEnd, 0);
					break;

				case LAYER_HORZ_LINE:
					DrawHorzGrid(pDC, lStart, lEnd);
					break;

				case LAYER_VERT_LINE:
					DrawVertGrid(pDC, lStart, lEnd);
					break;

				case LAYER_CURVE:
					DrawSheet(pDC, lStart, lEnd, 0);
					break;

				case LAYER_MARK:
					DrawMark(pDC, lStart, lEnd);
					break;
					
				default:
					break;
			}
		}
	}
	
	CRect rcErase = rc1;
	rcErase.DeflateRect(4, 0, 4, 0);
	pDC->FillSolidRect(rcErase, RGB(255,255,255));
	DrawHead(pDC, rc1, FALSE);
	if (bTwice)
	{
		rcErase = rc2;
		rcErase.DeflateRect(4, 0, 4, 0);
		pDC->FillSolidRect(rcErase, RGB(255,255,255));
		DrawHead(pDC, rc2, FALSE);
	}
	CCurve::m_bSavingbmp = FALSE;
	lpRect->bottom = rc2.top;
}

void CSheet::RecalAllTR()
{
	for (int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CRect rc = GetHeadRect(i);
		CRect rcRegion = GetTrackRect(i);
		rc.left = rcRegion.left;
		rc.right = rcRegion.right;
		SetHeadRect(i, rc);
	}
}

void CSheet::UpdateGrids()
{
	for (int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = m_TrackList.GetAt(i);
		pTrack->UpdateGrids();
	}
}
int i = 0;
#define initDepths()	CArray<long, long> depths;\
	int nTrack = m_TrackList.GetSize();\
	for (i = 0; i < nTrack; i++)\
{\
	CTrack* pTrack = m_TrackList.GetAt(i);\
	int nCurve = pTrack->m_vecCurve.size();\
	for (int j = 0; j < nCurve; j++)\
{\
	ICurve* pCurve = pTrack->m_vecCurve.at(j);\
	if (!pCurve->IsVisible())\
	continue;\
	int nData = pCurve->GetDataSize();\
	if (nData < 1)\
	continue;\
	long lDepth0 = pCurve->GetDepth(0);\
	long lDepthE = pCurve->GetDepth(nData - 1);\
	if (lDepth0 != INVALID_DEPTH)\
	depths.Add(lDepth0);\
	if (lDepthE != INVALID_DEPTH)\
	depths.Add(lDepthE);\
}\
}\
int nDepth = depths.GetSize()

//
//函数名：GetCurvesTop
//功能：该函数得到所有曲线的最小深度值
//参数：lTop-返回最小深度值
//
BOOL CSheet::GetCurvesTop(long& lTop)
{
	initDepths();
	long lT = LONG_MAX;
	int i = 0;
	for (i = 0; i < nDepth; i++)
	{
		if (depths[i] < lT)
		{
			lT = depths[i];
			lTop = depths[i];
		}
	}
	
	return (lT != LONG_MAX);
}

//
//函数名：GetCurvesBottom
//功能：该函数得到所有曲线的最大深度值
//参数：lBottom-返回最大深度值
//
BOOL CSheet::GetCurvesBottom(long& lBottom)
{
	initDepths();
	long lB = LONG_MIN;
	for (i = 0; i < nDepth; i++)
	{
		if (depths[i] > lB)
		{
			lB = depths[i];
			lBottom = depths[i];
		}
	}
	
	return (lB != LONG_MIN);
}

//
//函数名：DrawBackCurve
//功能：画背景曲线
//参数：pDC- CDC*
//      lStart,lEnd-要绘制的起始终止深度
//

void CSheet::DrawBackCurve(CDC *pDC, long lStart, long lEnd, BOOL bLogDraw)
{
	for( int i = 0; i < m_TrackList.GetSize(); i++)
	{ 
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		if (m_nDriveMode == UL_DRIVE_DEPT)
		{
			pTrack->DrawBackCurve(pDC, lStart, lEnd, bLogDraw);
		} 
		else
		{
			pTrack->DrawBackCurve(pDC, lStart, lEnd, bLogDraw);
		}
	}
}

void CSheet::SetProject(IProject* pProject)
{
	for (int i = 0; i < m_TrackList.GetSize(); i++)
	{
		CTrack* pTrack = (CTrack*)m_TrackList.GetAt(i);
		pTrack->SetProject(pProject);
	}
}

CCurve* CSheet::GetTimeCurve()
{
	for (int i=0; i<m_TrackList.GetSize(); i++)
	{
		CTrack * pTrack = (CTrack *) m_TrackList.GetAt(i);
		for (int j=0; j<pTrack->m_vecCurve.size(); j++)
		{
			CCurve * pCurve = (CCurve *) pTrack->m_vecCurve.at(j);
			CString strName = pCurve->m_strName;
			strName = strName.Left(4);
			strName.MakeUpper ();
			if(strName == "TIME")
				return pCurve;
		}
	}
	return NULL;

}

CCurve* CSheet::GetTimeCurve(CPtrArray& CurveList)
{
	CCurve* pTimeCurve = NULL;
	CurveList.RemoveAll();
	for (int i=0; i<m_TrackList.GetSize(); i++)
	{
		CTrack * pTrack = (CTrack *) m_TrackList.GetAt(i);
		for (int j=0; j<pTrack->m_vecCurve.size(); j++)
		{
			CCurve * pCurve = (CCurve *) pTrack->m_vecCurve.at(j);
			CurveList.Add(pCurve);
			CString strName = pCurve->m_strName;
			strName = strName.Left(4);
			strName.MakeUpper ();
			if(strName == "TIME"|| strName == "ETIME")
				pTimeCurve = pCurve;
		}
	}
	return pTimeCurve;

}

CCurve* CSheet::GetTimeCurve(CURVES& vecCurve)
{
	CCurve* pTimeCurve = NULL;
	vecCurve.clear();
	for (int i=0; i<m_TrackList.GetSize(); i++)
	{
		CTrack * pTrack = (CTrack *) m_TrackList.GetAt(i);
		for (int j=0; j<pTrack->m_vecCurve.size(); j++)
		{
			CCurve* pCurve = (CCurve *) pTrack->m_vecCurve.at(j);
			vecCurve.push_back(pCurve);
			if (pCurve->IsTimeCurve())
				pTimeCurve = pCurve;
		}
	}
	
	return pTimeCurve;
}

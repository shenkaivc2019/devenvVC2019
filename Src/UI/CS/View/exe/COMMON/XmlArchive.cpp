// XmlArchive.cpp: implementation of the CXmlArchive class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "XmlArchive.h"
//#include "resource.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CCrossException::CCrossException(LPCTSTR lpszMessage)
{
    m_strMessage = lpszMessage; 
}

CCrossException::~CCrossException()
{
}

BOOL CCrossException::GetErrorMessage(LPTSTR lpszError, UINT nMaxError, PUINT pnHelpContext/* = NULL*/)
{
    lstrcpyn(lpszError, (LPCTSTR)m_strMessage, nMaxError); 
    return TRUE; 
}


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CXmlArchive::CXmlArchive(CStdioFile* pFile, BOOL bStoring)
{
    m_pFile = pFile; 
    m_bStoring = bStoring; 
    m_nIndent = 0; 
    m_nLineNO = 0; 
}

CXmlArchive::~CXmlArchive()
{

}

BOOL CXmlArchive::IsStoring()
{
    return m_bStoring; 
}

void CXmlArchive::WriteLine(LPCTSTR lpszText)
{
    CString strTab(' ', m_nIndent); 
    m_pFile->WriteString(strTab + CString(lpszText)); 
}

void CXmlArchive::WriteKey(LPCTSTR lpszKeyName, BOOL bBegin/* = TRUE*/)
{
    if(!bBegin)
        m_nIndent -= 4; 
    CString strTab(' ', m_nIndent); 
    CString strName = lpszKeyName; 
    if(bBegin)
        strName = strTab + "<" + strName + ">\n"; 
    else 
        strName = strTab + "</" + strName + ">\n"; 
    m_pFile->WriteString(strName); 
    if(bBegin)
        m_nIndent += 4; 
}

void CXmlArchive::WriteValue(LPCTSTR lpszKeyName, LPCTSTR lpszValue)
{
    CString strTab(' ', m_nIndent); 
    CString strName = lpszKeyName;
    strName = strTab + _T("<") + strName + _T(">") + CString(lpszValue) + _T("</") + strName + _T(">\n"); 
    m_pFile->WriteString(strName); 
}

void CXmlArchive::WriteValue(LPCTSTR lpszKeyName, DWORD dwValue)
{
    CString strValue; 
    strValue.Format(_T("%d"), dwValue); 
    WriteValue(lpszKeyName, strValue); 
}

void CXmlArchive::WriteValue(LPCTSTR lpszKeyName, double fValue)
{
    CString strValue; 
    strValue.Format(_T("%f"), fValue); 
    WriteValue(lpszKeyName, strValue); 
}



void CXmlArchive::ReadLine(CString& strText/* = CString()*/)
{
    m_nLineNO++; 
    if(!m_pFile->ReadString(strText))
        ThrowXmlException(XMLE_REACH_FILEEND); 
}

void CXmlArchive::ReadKey(CString strKeyName, BOOL bBegin/* = TRUE*/)
{
    m_nLineNO++; 
    CString strName; 
    if(!m_pFile->ReadString(strName))
        ThrowXmlException(XMLE_REACH_FILEEND); 

    strName.TrimLeft(); 
    strName.TrimRight(); 
    LPTSTR lpszHead = bBegin ? _T("<") : _T("</"); 
    int nLen = bBegin ? 1 : 2; 
    int left = strName.Find(lpszHead); 
    if(left == -1)
        ThrowXmlException(bBegin ? XMLE_MISS_BEGIN_KEY : XMLE_MISS_END_KEY, strKeyName);  

    int right = strName.Find(_T(">"), left + nLen); 
    if(right == -1)
        ThrowXmlException(bBegin ? XMLE_MISS_BEGIN_KEY : XMLE_MISS_END_KEY, strKeyName); 

    strName = strName.Mid(left + nLen, right - left - nLen); 
    if(strName != strKeyName)
        ThrowXmlException(bBegin ? XMLE_MISMATCH_BEGIN_KEY : XMLE_MISMATCH_END_KEY, strName, strKeyName); 
}

void CXmlArchive::ReadValue(CString strKeyName, CString& strValue, BOOL bMultiLine/* = TRUE*/)
{
    m_nLineNO++; 
    CString strName; 
    if(!m_pFile->ReadString(strName))
        ThrowXmlException(XMLE_REACH_FILEEND); 

    strName.TrimLeft(); 
    strName.TrimRight(); 
    int left1 = strName.Find(_T("<")); 
    if(left1 == -1)
        ThrowXmlException(XMLE_MISS_BEGIN_KEY, strKeyName); 

    int right1 = strName.Find(_T(">"), left1 + 1); 
    if(right1 == -1)
        ThrowXmlException(XMLE_MISS_BEGIN_KEY, strKeyName); 

    CString strHead = strName.Mid(left1 + 1, right1 - left1 - 1); 
    if(strHead != strKeyName)
        ThrowXmlException(XMLE_MISMATCH_BEGIN_KEY, strHead, strKeyName); 

    int left2 = strName.Find(_T("</"), right1 + 1); 
    if(left2 == -1)
    {
        if(bMultiLine)
        {
            BOOL bFind = FALSE; 
            CString tmp; 
            while(left2 == -1)
            {
                m_nLineNO++; 
                if(!m_pFile->ReadString(tmp))
                    ThrowXmlException(XMLE_REACH_FILEEND); 
                strName += _T("\n") + tmp; 
                left2 = strName.Find(_T("</"), right1 + 1); 
            }
        }
        else 
            ThrowXmlException(XMLE_MISS_END_KEY, strKeyName); 
    }

    int right2 = strName.Find(_T(">"), left2 + 2); 
    if(right2 == -1)
        ThrowXmlException(XMLE_MISS_END_KEY, strKeyName); 

    CString strTail = strName.Mid(left2 + 2, right2 - left2 - 2); 
    if(strTail != strKeyName)
        ThrowXmlException(XMLE_MISMATCH_END_KEY, strHead, strKeyName); 

    strValue = strName.Mid(right1 + 1, left2 - right1 - 1); 
}

void CXmlArchive::ReadValue(CString strKeyName, DWORD* pValue)
{
    CString strValue; 
    ReadValue(strKeyName, strValue, FALSE);
    *pValue = _ttoi(strValue);
}

void CXmlArchive::ReadValue(CString strKeyName, double* pValue)
{
    CString strValue; 
    ReadValue(strKeyName, strValue, FALSE); 

    LPTSTR stop; 
    *pValue = _tcstod(strValue, &stop);
}


//void CXmlArchive::AddClass(CRuntimeClass* pClass)
//{
//    if(m_listClass.Find(pClass) == NULL)
//        m_listClass.AddTail(pClass); 
//}

CObject* CXmlArchive::ReadClass(CString& strKeyName/* = CString()*/)
{
    m_nLineNO++; 
    CString strName; 
    if(!m_pFile->ReadString(strName))
        ThrowXmlException(XMLE_REACH_FILEEND); 

    strName.TrimLeft(); 
    strName.TrimRight(); 
    int left = strName.Find(_T("<")); 
    if(left == -1)
        ThrowXmlException(XMLE_MISS_BEGIN_KEY, _T("UnknownClass")); 

    int right = strName.Find(_T(">"), left + 1); 
    if(right == -1)
        ThrowXmlException(XMLE_MISS_BEGIN_KEY, _T("UnknownClass")); 

    strKeyName = strName.Mid(left + 1, right - left - 1); 
    CRuntimeClass* pClass = FindClass(strKeyName); 
    if(pClass == NULL)
    {
        ThrowXmlException(XMLE_UNKNOWN_CLASS_NAME, strKeyName); 
        return NULL; 
    }
    else 
        return pClass->CreateObject(); 
}

#ifdef _MT
void AFXAPI AfxLockGlobals(int nLockType);
void AFXAPI AfxUnlockGlobals(int nLockType);
BOOL AFXAPI AfxCriticalInit();
void AFXAPI AfxCriticalTerm();
#else
#define AfxLockGlobals(nLockType)
#define AfxUnlockGlobals(nLockType)
#define AfxCriticalInit() (TRUE)
#define AfxCriticalTerm()
#endif

#define CRIT_DYNLINKLIST    0
#define CRIT_RUNTIMECLASSLIST   0
#define CRIT_OBJECTFACTORYLIST  0
#define CRIT_LOCKSHARED 0

CRuntimeClass* CXmlArchive::FindClass(LPCTSTR lpszClassName)
{
    //POSITION pos = m_listClass.GetHeadPosition(); 
    //while(pos != NULL)
    //{
    //    CRuntimeClass* pClass = (CRuntimeClass*)m_listClass.GetNext(pos); 
    //    if(lstrcmp(CString(pClass->m_lpszClassName), lpszClassName) == 0)
    //        return pClass; 
    //}    
    
	AFX_MODULE_STATE* pModuleState = AfxGetModuleState();
	AfxLockGlobals(CRIT_RUNTIMECLASSLIST);
	CRuntimeClass* pClass = NULL;
	for (pClass = pModuleState->m_classList; pClass != NULL;
		pClass = pClass->m_pNextClass)
	{
        if(strcmp(pClass->m_lpszClassName, lpszClassName) == 0)
        {
            AfxUnlockGlobals(CRIT_RUNTIMECLASSLIST); 
            return pClass; 
        }
	}
	AfxUnlockGlobals(CRIT_RUNTIMECLASSLIST);

#ifdef _AFXDLL
	// walk through the list of dynlink library registered classes
	AfxLockGlobals(CRIT_DYNLINKLIST);
	for (CDynLinkLibrary* pDLL = pModuleState->m_libraryList; pDLL != NULL;
		pDLL = pDLL->m_pNextDLL)
	{
		for (pClass = pDLL->m_classList; pClass != NULL;
			pClass = pClass->m_pNextClass)
		{
            if(strcmp(pClass->m_lpszClassName, lpszClassName) == 0)
            {
                AfxUnlockGlobals(CRIT_DYNLINKLIST); 
                return pClass; 
            }
		}
	}
	AfxUnlockGlobals(CRIT_DYNLINKLIST);
#endif

    return NULL; 
}

void CXmlArchive::ThrowXmlException(UINT nType, LPCTSTR lpszText1/* = NULL*/, LPCTSTR lpszText2/* = NULL*/)
{
    CString strError;
    CString strMessage = _T("未知错误"); 
    switch(nType)
    {
        case XMLE_REACH_FILEEND:
            //_T("在读取过程中遇到了文件尾.")
            //strMessage.LoadString(IDS_CROSSERROR_REACH_FILE_END); 
            strMessage = _T("在读取过程中遇到了文件尾."); 
            break; 
        case XMLE_MISS_BEGIN_KEY:
            //_T("行[%d]: 未找到起始键名[%s]")
            //strError.LoadString(IDS_CROSSERROR_MISS_BEGIN_KEY); 
            strError = _T("行[%d]: 未找到起始键名[%s]"); 
            strMessage.Format(strError, m_nLineNO, lpszText1); 
            break; 
        case XMLE_MISS_END_KEY:
            //_T("行[%d]: 未找到结束键名[%s]")
            //strError.LoadString(IDS_CROSSERROR_MISS_END_KEY); 
            strError = _T("行[%d]: 未找到结束键名[%s]"); 
            strMessage.Format(strError, m_nLineNO, lpszText1); 
            break; 
        case XMLE_MISMATCH_BEGIN_KEY:
            //_T("行[%d]: 起始键名[%s]与预期[%s]不匹配")
            //strError.LoadString(IDS_CROSSERROR_MISMATCH_BEGIN_KEY); 
            strError = _T("行[%d]: 起始键名[%s]与预期[%s]不匹配");
            strMessage.Format(strError, m_nLineNO, lpszText1, lpszText2); 
            break; 
        case XMLE_MISMATCH_END_KEY:
            //_T("行[%d]: 结束键名[%s]与预期[%s]不匹配")
            //strError.LoadString(IDS_CROSSERROR_MISMATCH_END_KEY); 
            strError = _T("行[%d]: 结束键名[%s]与预期[%s]不匹配"); 
            strMessage.Format(strError, m_nLineNO, lpszText1, lpszText2); 
            break; 
        case XMLE_UNKNOWN_CLASS_NAME:
            //_T("行[%d]: 未知类名[%s]")
            //strError.LoadString(IDS_CROSSERROR_UNKNOWN_CLASS_NAME); 
            strError = _T("行[%d]: 未知类名[%s]"); 
            strMessage.Format(strError, m_nLineNO, lpszText1);
            break; 
    }

    THROW(new CCrossException(strMessage)); 
}


void CXmlArchive::SerializeObject(LPRECT lpRect, LPCTSTR lpszKeyName /*=NULL*/)
{
    CString strKey = (lpszKeyName == NULL) ? _T("Rectangle") : lpszKeyName; 
    if(IsStoring())
    {
        WriteKey(strKey); 
        WriteValue(_T("left"), (DWORD)lpRect->left); 
        WriteValue(_T("top"), (DWORD)lpRect->top); 
        WriteValue(_T("right"), (DWORD)lpRect->right); 
        WriteValue(_T("bottom"), (DWORD)lpRect->bottom); 
        WriteKey(strKey, FALSE); 
    }
    else 
    {
        ReadKey(strKey); 
        ReadValue(_T("left"), (DWORD*)&(lpRect->left)); 
        ReadValue(_T("top"), (DWORD*)&(lpRect->top)); 
        ReadValue(_T("right"), (DWORD*)&(lpRect->right)); 
        ReadValue(_T("bottom"), (DWORD*)&(lpRect->bottom)); 
        ReadKey(strKey, FALSE); 
    }
}

void CXmlArchive::SerializeObject(LPPOINT pPoint, LPCTSTR lpszKeyName /*=NULL*/)
{
    CString strKey = (lpszKeyName == NULL) ? _T("Point") : lpszKeyName; 
    if(IsStoring())
    {
        WriteKey(strKey); 
        WriteValue(_T("X"), (DWORD)pPoint->x); 
        WriteValue(_T("Y"), (DWORD)pPoint->y); 
        WriteKey(strKey, FALSE); 
    }
    else 
    {
        ReadKey(strKey); 
        ReadValue(_T("X"), (DWORD*)&(pPoint->x)); 
        ReadValue(_T("Y"), (DWORD*)&(pPoint->y)); 
        ReadKey(strKey, FALSE); 
    }
}

void CXmlArchive::SerializeObject(LPSIZE pSize, LPCTSTR lpszKeyName /*=NULL*/)
{
    CString strKey = (lpszKeyName == NULL) ? _T("Size") : lpszKeyName; 
    if(IsStoring())
    {
        WriteKey(strKey); 
        WriteValue(_T("CX"), (DWORD)pSize->cx); 
        WriteValue(_T("CY"), (DWORD)pSize->cy); 
        WriteKey(strKey, FALSE); 
    }
    else 
    {
        ReadKey(strKey); 
        ReadValue(_T("CX"), (DWORD*)&(pSize->cx)); 
        ReadValue(_T("CY"), (DWORD*)&(pSize->cy)); 
        ReadKey(strKey, FALSE); 
    }
}

void CXmlArchive::SerializeObject(LPLOGPEN pPen, LPCTSTR lpszKeyName /*=NULL*/)
{
    CString strKey = (lpszKeyName == NULL) ? _T("Line") : lpszKeyName; 
    if(IsStoring())
    {
        WriteKey(strKey); 
        WriteValue(_T("Color"), (DWORD)pPen->lopnColor); 
        WriteValue(_T("Style"), (DWORD)pPen->lopnStyle); 
        WriteValue(_T("Width"), (DWORD)pPen->lopnWidth.x);
        WriteKey(strKey, FALSE); 
    }
    else 
    {
        ReadKey(strKey); 
        ReadValue(_T("Color"), (DWORD*)&(pPen->lopnColor)); 
        ReadValue(_T("Style"), (DWORD*)&(pPen->lopnStyle)); 
        ReadValue(_T("Width"), (DWORD*)&(pPen->lopnWidth.x)); 
        ReadKey(strKey, FALSE); 
    }
}

void CXmlArchive::SerializeObject(LPLOGBRUSH pBrush, LPCTSTR lpszKeyName /*=NULL*/)
{
    CString strKey = (lpszKeyName == NULL) ? _T("Fill") : lpszKeyName; 
    if(IsStoring())
    {       
        WriteKey(strKey); 
        WriteValue(_T("Color"), (DWORD)pBrush->lbColor); 
        WriteValue(_T("Hatch"), (DWORD)pBrush->lbHatch); 
        WriteValue(_T("Style"), (DWORD)pBrush->lbStyle); 
        WriteKey(strKey, FALSE); 
    }
    else 
    {
        ReadKey(strKey); 
        ReadValue(_T("Color"), (DWORD*)&(pBrush->lbColor)); 
        ReadValue(_T("Hatch"), (DWORD*)&(pBrush->lbHatch)); 
        ReadValue(_T("Style"), (DWORD*)&(pBrush->lbStyle)); 
        ReadKey(strKey, FALSE); 
    }
}

void CXmlArchive::SerializeObject(LPLOGFONT pFont, LPCTSTR lpszKeyName /*=NULL*/)
{
    CString strName; 
    CString strKey = (lpszKeyName == NULL) ? _T("Font") : lpszKeyName; 
    if(IsStoring())
    {
        WriteKey(strKey); 
        WriteValue(_T("Height"), (DWORD)pFont->lfHeight); 
        WriteValue(_T("Width"), (DWORD)pFont->lfWidth); 
        WriteValue(_T("Escapement"), (DWORD)pFont->lfEscapement); 
        WriteValue(_T("Orientation"), (DWORD)pFont->lfOrientation); 
        WriteValue(_T("Weight"), (DWORD)pFont->lfWeight); 
        WriteValue(_T("Italic"), (DWORD)pFont->lfItalic); 
        WriteValue(_T("Underline"), (DWORD)pFont->lfUnderline); 
        WriteValue(_T("StrikeOut"), (DWORD)pFont->lfStrikeOut); 
        WriteValue(_T("CharSet"), (DWORD)pFont->lfCharSet); 
        WriteValue(_T("OutPrecision"), (DWORD)pFont->lfOutPrecision);
        WriteValue(_T("ClipPrecision"), (DWORD)pFont->lfClipPrecision); 
        WriteValue(_T("Quality"), (DWORD)pFont->lfQuality); 
        WriteValue(_T("PitchAndFamily"), (DWORD)pFont->lfPitchAndFamily); 
        WriteValue(_T("FaceName"), pFont->lfFaceName);  
        WriteKey(strKey, FALSE); 
    }
    else 
    {
        ReadKey(strKey); 
        ReadValue(_T("Height"), (DWORD*)&(pFont->lfHeight)); 
        ReadValue(_T("Width"), (DWORD*)&(pFont->lfWidth)); 
        ReadValue(_T("Escapement"), (DWORD*)&(pFont->lfEscapement)); 
        ReadValue(_T("Orientation"), (DWORD*)&(pFont->lfOrientation)); 
        ReadValue(_T("Weight"), (DWORD*)&(pFont->lfWeight)); 
        ReadValue(_T("Italic"), (DWORD*)&(pFont->lfItalic)); 
        ReadValue(_T("Underline"), (DWORD*)&(pFont->lfUnderline)); 
        ReadValue(_T("StrikeOut"), (DWORD*)&(pFont->lfStrikeOut)); 
        ReadValue(_T("CharSet"), (DWORD*)&(pFont->lfCharSet)); 
        ReadValue(_T("OutPrecision"), (DWORD*)&(pFont->lfOutPrecision));
        ReadValue(_T("ClipPrecision"), (DWORD*)&(pFont->lfClipPrecision)); 
        ReadValue(_T("Quality"), (DWORD*)&(pFont->lfQuality)); 
        ReadValue(_T("PitchAndFamily"), (DWORD*)&(pFont->lfPitchAndFamily)); 
        ReadValue(_T("FaceName"), strName);  
        lstrcpyn(pFont->lfFaceName, strName, LF_FACESIZE); 
        ReadKey(strKey, FALSE); 
    }
}

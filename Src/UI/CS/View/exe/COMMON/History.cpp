// History.cpp: implementation of the CHistory class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "History.h"
#include "XMLSettings.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CString Gbl_AppPath;

int CHistory::m_nLimit = 6;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHistory::CHistory(CDialog* pDialog /* = NULL */, int nCtrlID /* = -1 */)
{
	if (pDialog == NULL)
	{
		m_pXML = new CXMLSettings();
		m_pXML->ReadXMLFromFile(HISTORY_FILE);
		return;
	}

	m_pXML = NULL;
	CXMLSettings xml;
	if (!xml.ReadXMLFromFile(HISTORY_FILE))
		return;

	UINT nID = pDialog->SendMessage(WM_HELPHITTEST);
	if (nID == 0)
		return;
	
	nID -= HID_BASE_RESOURCE;
	CString strText;
	strText.Format("%d", nID);
	if (!xml.Open(strText))
		return;

	if (nCtrlID != -1)
	{
		CWnd* pWndChild = pDialog->GetDlgItem(nCtrlID);
		if (!pWndChild->GetSafeHwnd())
		{
			return;
		}

		TCHAR szClass [256];
		::GetClassName (pWndChild->GetSafeHwnd(), szClass, 255);
		
		CString strClass = szClass;
		if (strClass == _T("ComboBox"))
		{
			CComboBox* pCombo = (CComboBox*)pWndChild;

			CString strName;
			strName.Format(_T("%d"), nCtrlID);

			CStringList strList;
			xml.Read(strName, strList);
			int nCount = strList.GetCount();
			if (nCount > m_nLimit)
				nCount = m_nLimit;
			
			POSITION pos1 = strList.GetHeadPosition();
			for (int i = 0; i < nCount; i++)
			{
				pCombo->AddString(strList.GetNext(pos1));
			}
		}

		return;
	}

	CXMLNodes* pNodes = &xml.m_pCurrNode->m_lstChildren;
	POSITION pos = pNodes->GetHeadPosition();
	for ( ; pos != NULL; )
	{
		CXMLNode* pNode = pNodes->GetNext(pos);
		xml.m_pCurrNode = pNode;
		nID = _ttoi(pNode->m_strName);
		CWnd* pWndChild = pDialog->GetDlgItem(nID);
		HWND hWnd = pWndChild->GetSafeHwnd ();
		if (hWnd == NULL)
			continue;

		TCHAR szClass [256];
		::GetClassName (hWnd, szClass, 255);
		
		CString strClass = szClass;
		if (strClass != _T("ComboBox"))
			continue;

		CComboBox* pCombo = (CComboBox*)pWndChild;
		if ((pCombo->GetStyle() & CBS_DROPDOWN) == 0)
			continue;

/*		CString strText;
		pCombo->GetWindowText(strText);
		pCombo->ResetContent();
*/			
		CStringList strList;
		xml.Read(_T(""), strList);
		int nCount = strList.GetCount();
		if (nCount > m_nLimit)
			nCount = m_nLimit;
		
		POSITION pos1 = strList.GetHeadPosition();
		for (int i = 0; i < nCount; i++)
		{
			pCombo->AddString(strList.GetNext(pos1));
		}

// 		if (strText.GetLength())
// 			pCombo->SetWindowText(strText);
	}
}

CHistory::~CHistory()
{
	if (m_pXML != NULL)
	{
		m_pXML->m_bReadOnly = FALSE;
		m_pXML->WriteXMLToFile(HISTORY_FILE);
		delete m_pXML;
		m_pXML = NULL;
	}
}

void CHistory::Save(CDialog* pDialog, int nCtrlID /* = -1 */, int nCount /* = 0 */)
{
	if (!pDialog->GetSafeHwnd())
		return;

	if (m_pXML == NULL)
	{
		m_pXML = new CXMLSettings();
		m_pXML->ReadXMLFromFile(HISTORY_FILE);
	}

	UINT nID = pDialog->SendMessage(WM_HELPHITTEST);
	if (nID == 0)
		return;
	
	nID -= HID_BASE_RESOURCE;
	CString strText;
	strText.Format("%d", nID);
	m_pXML->m_bReadOnly = FALSE;
	if (!m_pXML->CreateKey(strText))
		return;

	if (nCount < m_nLimit)
		nCount = m_nLimit;

	if (nCtrlID != -1)
	{
		TCHAR szClass [256];
		CWnd* pWndChild = pDialog->GetDlgItem(nCtrlID);
		::GetClassName (pWndChild->GetSafeHwnd (), szClass, 255);
		
		CString strClass = szClass;
		if (strClass == _T("ComboBox"))
		{
			CComboBox* pCombo = (CComboBox*)pWndChild;
			pCombo->GetWindowText(strText);
			if (strText.GetLength())
			{
				CString strName;
				strName.Format(_T("%d"), nCtrlID);
				CStringList strValue;
				m_pXML->m_bReadOnly = TRUE;
				m_pXML->Read(strName, strValue);
				POSITION pos = strValue.Find(strText);
				if (pos != NULL)
					strValue.RemoveAt(pos);
				strValue.AddHead(strText);
				while (strValue.GetCount() > nCount)
				{
					strValue.RemoveTail();
				}
				m_pXML->m_bReadOnly = FALSE;
				m_pXML->Write(strName, strValue);
			}
		}

		return;
	}

	CWnd* pWndChild = pDialog->GetWindow (GW_CHILD);
	while (pWndChild != NULL)
	{
		TCHAR szClass [256];
		::GetClassName (pWndChild->GetSafeHwnd (), szClass, 255);
		
		CString strClass = szClass;
		if (strClass == _T("ComboBox"))
		{
			CComboBox* pCombo = (CComboBox*)pWndChild;
			if ((pCombo->GetStyle() & CBS_DROPDOWN) == 0)
				continue;

			pCombo->GetWindowText(strText);
			if (strText.GetLength())
			{
				nID = pWndChild->GetDlgCtrlID();
				CString strName;
				strName.Format(_T("%d"), nID);
				CStringList strValue;
				m_pXML->m_bReadOnly = TRUE;
				m_pXML->Read(strName, strValue);
				POSITION pos = strValue.Find(strText);
				if (pos != NULL)
					strValue.RemoveAt(pos);
				strValue.AddHead(strText);
				while (strValue.GetCount() > nCount)
				{
					strValue.RemoveTail();
				}
				m_pXML->m_bReadOnly = FALSE;
				m_pXML->Write(strName, strValue);
			}
		}
		pWndChild = pWndChild->GetNextWindow ();
	}

	m_pXML->Back();
}

void CHistory::Reset(CDialog* pDialog, int nCtrlID)
{
	if (!pDialog->GetSafeHwnd())
		return;

	if (m_pXML == NULL)
	{
		m_pXML = new CXMLSettings();
		m_pXML->ReadXMLFromFile(HISTORY_FILE);
	}

	UINT nID = pDialog->SendMessage(WM_HELPHITTEST);
	if (nID == 0)
		return;
	
	nID -= HID_BASE_RESOURCE;
	CString strText;
	strText.Format("%d", nID);
	m_pXML->m_bReadOnly = FALSE;
	if (!m_pXML->CreateKey(strText))
		return;

	int nCount = 0;
	if (nCount < m_nLimit)
		nCount = m_nLimit;

	ASSERT (nCtrlID != -1);
	
	TCHAR szClass [256];
	CWnd* pWndChild = pDialog->GetDlgItem(nCtrlID);
	::GetClassName (pWndChild->GetSafeHwnd (), szClass, 255);
	
	CString strClass = szClass;
	if (strClass != _T("ComboBox"))
		return;

	CString strName;
	strName.Format(_T("%d"), nCtrlID);

	CComboBox* pCombo = (CComboBox*)pWndChild;
	
	CStringList strValue;
	for (int i = 0; i < pCombo->GetCount(); i++)
	{
		pCombo->GetLBText(i, strText);
		strValue.AddTail(strText);
	}

	pCombo->GetWindowText(strText);
	if (strText.GetLength())
		strValue.AddHead(strText);
	
	while (strValue.GetCount() > nCount)
	{
		strValue.RemoveTail();
	}
	m_pXML->m_bReadOnly = FALSE;
	m_pXML->Write(strName, strValue);
}

CString CHistory::GetLastest(int nDialog, int nID)
{
	if (m_pXML == NULL)
	{
		m_pXML = new CXMLSettings();
		m_pXML->ReadXMLFromFile(HISTORY_FILE);
	}

	CString strKey;
	strKey.Format(_T("%d\\%d"), nDialog, nID);
	if (m_pXML->Open(strKey))
	{
		CStringList strList;
		m_pXML->Read(_T(""), strList);
		if (strList.GetCount())
		{
			return strList.GetHead();
		}
	}
	
	strKey = _T("");
	return strKey;
}

// OutputstableView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "OutputstableView.h"
#include "MyMEMDC.h"
#include "comconfig.h"
#include "Curve.h"
#include "ulcommdef.h"
#include "Utility.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CString Gbl_AppPath;

/////////////////////////////////////////////////////////////////////////////
// COutputstableView

IMPLEMENT_DYNCREATE(COutputstableView, CULView)

COutputstableView::COutputstableView(LPVOID pvecCurve /* = NULL */)
{
	m_pvecCurve = (vec_ic*)pvecCurve;
	m_pEdit = &m_Cell;
}

COutputstableView::~COutputstableView()
{
	Clear();
}


BEGIN_MESSAGE_MAP(COutputstableView, CULView)
	//{{AFX_MSG_MAP(COutputstableView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COutputstableView drawing

void COutputstableView::OnDraw(CDC* pDC)
{
	// TODO: add draw code here
	CRect rect;
	GetClientRect(rect);
	pDC->DPtoLP(rect);

	if (m_nRefresh)
	{
		PrepareCell(&m_Cell, pDC, rect);
		SetScrollInformation();
		m_nRefresh = 0;
	}

	CMyMemDC memDC(pDC, &rect);
	DrawPage(&memDC, rect);
}

/////////////////////////////////////////////////////////////////////////////
// COutputstableView diagnostics

#ifdef _DEBUG
void COutputstableView::AssertValid() const
{
	CULView::AssertValid();
}

void COutputstableView::Dump(CDumpContext& dc) const
{
	CULView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// COutputstableView message handlers

void COutputstableView::Clear ()
{
	for(int i = 0; i < m_CurveInfoList.GetSize(); i++)
	{
		CURVEPROPERTY *pInfo = (CURVEPROPERTY *)m_CurveInfoList.GetAt(i);
		delete pInfo;
	}
	m_CurveInfoList.RemoveAll();
}

void COutputstableView::InitCurveInfo ()
{
	Clear();

	for (int i = 0; i < m_pvecCurve->size(); i++)
	{
		ICurve* pCurve = m_pvecCurve->at(i);
		if(NULL == pCurve)
			continue;
		CURVEPROPERTY* pInfo = new CURVEPROPERTY;
		pCurve->GetCurveProp(pInfo);
		m_CurveInfoList.Add(pInfo);
	}
}

int COutputstableView::CalcPageHeight()
{
	if(NULL == m_Cell.m_hWnd)
		return CULView::CalcPageHeight();
	return (int)GetCellSize(&m_Cell).cy + 2*PAGE_MARGIN;
}

int COutputstableView::CalcPrintHeight()
{
	return CalcPageHeight();
}

int COutputstableView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CULView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	if (!CreateCell(&m_Cell, this))
		return -1;
	
	OpenCellFile();
	
	return 0;
}

void COutputstableView::Print()
{
	//CULView::Print();
	//PrintCell(&m_Cell);
	if (m_Cell.GetSafeHwnd() == NULL)
	{
		TRACE("Warning : Load child frame failed!\n");
		return ;
	}

	PrintCell(&m_Cell);
}
void COutputstableView::OpenCellFile()
{
	InitCurveInfo();
	SetScrollInformation();
	InitCellData();
}

void COutputstableView::OpenCellFile(CCell2000 *pCell)
{
	InitCurveInfo();
	InitCellData(pCell);
	if (pCell != &m_Cell)
		InitCellData(&m_Cell);
}
void COutputstableView::SaveAsBitmap(CDC* pDC, LPCTSTR pszFile)
{
	SaveCellAsBitmap(pDC, pszFile, &m_Cell);
}



//extern CComConfig AfxGetComConfig();
void COutputstableView::InitCellData(CCell2000* pCell)
{
	InitCurveInfo();

	CCell2000 *pWorkCell = NULL;
	if (pCell != NULL)
		pWorkCell = pCell;
	else
		pWorkCell = &m_Cell;

	LOGFONT lf;
	afxGlobalData.fontRegular.GetLogFont(&lf);
	long para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftR].lfFaceName, 1);
	pWorkCell->SetDefaultFont (para,AfxGetComConfig()->m_Watch.FFont[ftR].cy+1);//AfxGetComConfig()->m_Watch.FontSize [2] );//12);

	pWorkCell->ShowHScroll(0, 0);   // 不显示水平滚动条
	pWorkCell->ShowVScroll(0, 0);   // 不显示竖直滚动条
	pWorkCell->ShowTopLabel(0, 0);  // 不显示列标
	pWorkCell->ShowSideLabel(0, 0); // 不显示行标
	pWorkCell->ShowSheetLabel(0, 0);// 隐藏页签
//	pWorkCell->SetWorkbookReadonly(TRUE); //设为只读
	pWorkCell->SetAllowSizeColInGrid (FALSE);//不允许调整列宽
	pWorkCell->SetAllowSizeRowInGrid (FALSE);//不允许调整行宽
//	pWorkCell->PrintPageBreak(1);
	pWorkCell->ShowPageBreak(0); //不显示打印分页线
	pWorkCell->ShowGridLine(0, 0); //不显示格线		
	
	para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftT1].lfFaceName, 1);
	pWorkCell->SetCellFont(2, 1, 0, para);
	pWorkCell->SetCellFontSize(2, 1, 0, AfxGetComConfig()->m_Watch.FFont[ftT1].cy);
//	pWorkCell->SetCellFontStyle(2, 1, 0, para);

	int nPos = 3;
	int nRows = m_CurveInfoList.GetSize() + nPos + 2;
	pWorkCell->SetRowHeight (0, CELL_NULL_HEIGHT, nPos, 0);
	pWorkCell->SetRowHeight (0, CELL_NULL_HEIGHT, nRows-3, 0);
	pWorkCell->SetRows (nRows, 0);
	pWorkCell->SetCols(7,0);
	pWorkCell->SetColWidth(0, PAGE_MARGIN, 1, 0);
	pWorkCell->SetColWidth(0, 5 * PAGE_MARGIN, 2, 0);
	pWorkCell->SetColWidth(0, 12 * PAGE_MARGIN, 3, 0);
	pWorkCell->SetColWidth(0, PAGE_MARGIN, 6, 0);
	int i = 0;
	//设置只读属性
	for(i=1; i<=nRows; i++)
	{
		for(int j=1; j<=3; j++)
		{
			pWorkCell->SetCellInput(j,i,0,5);
			if(4 == i)
			{
				pWorkCell->SetCellInput(4,4,0,5);
				pWorkCell->SetCellInput(5,4,0,5);
			}
		}
		pWorkCell->SetCellInput(6,i,0,5);
		pWorkCell->SetCellBackColor(-1,i,0,pWorkCell->FindColorIndex(RGB(255,255,255),1));
	}
	para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftT2].lfFaceName, 1);
	pWorkCell->SetCellFont(3, 1, 0, para);
	pWorkCell->SetCellFontSize(3, 1, 0, AfxGetComConfig()->m_Watch.FFont[ftT2].cy);
//	pWorkCell->SetCellFontStyle(3, 1, 0, para);
	para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftT2].lfFaceName, 1);
	pWorkCell->SetCellFont(4, 1, 0, para);
	pWorkCell->SetCellFontSize(4, 1, 0, AfxGetComConfig()->m_Watch.FFont[ftT2].cy);
//	pWorkCell->SetCellFontStyle(4, 1, 0, para);
	//绘制标题
	pWorkCell->MergeCells(2,1,5,3);
	pWorkCell->SetCellString(2,1,0,_T("Outputs Table"));
	pWorkCell->SetCellAlign(2,1,0,32+4);

	//绘制列名
//	para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftT2].lfFaceName, 1);
//	for(int i=2; i<10; i++)
	{
	//	pWorkCell->SetCellFont(i, 4, 0, para);
//		pWorkCell->SetCellFontSize(i, 4, 0, 11);
	//	pWorkCell->SetCellFontStyle(i, 4, 0, 2);
	}
	

	nPos++;
	CString str;
	str.LoadString(IDS_OUTPUTTABLE_MNEMONIC);
//	pWorkCell->SetCellAlign(2,nPos,0,32+4);
	para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftT2].lfFaceName, 1);
	pWorkCell->SetCellFont(2, nPos, 0, para);
	pWorkCell->SetCellFontSize(2, nPos, 0, AfxGetComConfig()->m_Watch.FFont[ftT2].cy);
//	pWorkCell->SetCellFontStyle(2, nPos, 0, para);
	pWorkCell->SetCellString(2,nPos,0,str);

	str.LoadString(IDS_OUTPUTTABLE_DESCRIPTION);
//	pWorkCell->SetCellAlign(3,nPos,0,32+4);
	para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftT2].lfFaceName, 1);
	pWorkCell->SetCellFont(3, nPos, 0, para);
	pWorkCell->SetCellFontSize(3, nPos, 0, AfxGetComConfig()->m_Watch.FFont[ftT2].cy);
//	pWorkCell->SetCellFontStyle(3, nPos, 0, para);
	pWorkCell->SetCellString(3,nPos,0,str);

	//str.LoadString(IDS_OUTPUTTABLE_LENGTH);
//	pWorkCell->SetCellAlign(4,nPos,0,32+4);
	para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftT2].lfFaceName, 1);
	pWorkCell->SetCellFont(4, nPos, 0, para);
	pWorkCell->SetCellFontSize(4, nPos, 0, AfxGetComConfig()->m_Watch.FFont[ftT2].cy);
//	pWorkCell->SetCellFontStyle(4, nPos, 0, para);
	pWorkCell->SetCellString(4,4,0,"Filter Length");

	str.LoadString(IDS_OUTPUTTABLE_TYPE);
//	pWorkCell->SetCellAlign(5,nPos,0,2);
	para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftT2].lfFaceName, 1);
	pWorkCell->SetCellFont(5, nPos, 0, para);
	pWorkCell->SetCellFontSize(5, nPos, 0, AfxGetComConfig()->m_Watch.FFont[ftT2].cy);
//	pWorkCell->SetCellFontStyle(5, nPos, 0, para);
	pWorkCell->SetCellString(5,nPos,0,str);

	CString strPath(Gbl_AppPath+_T("\\Config\\CurveDescription.xls"));
	for(i = 0; i < m_CurveInfoList.GetSize (); i++)
	{
		CURVEPROPERTY *pInfo = (CURVEPROPERTY *)m_CurveInfoList.GetAt (i);
		
		nPos++;
		pWorkCell->SetRowHeight(1,20,nPos,0);
		
		pWorkCell->SetCellString(2,nPos,0,pInfo->lpszName);	// Mnemonic
		pWorkCell->SetCellString(3,nPos,0,GetToolDescription(pInfo->lpszName,strPath));	// Output Description

		pWorkCell->SetCellString(5,nPos,0,_T("NO"));	    // Filter Type
	}

	pWorkCell->DrawGridLine (1, 4, 6, 4, 1,  2, 0);	
	pWorkCell->DrawGridLine (1, 1, 6, 3, 1,  3, 0);
	pWorkCell->DrawGridLine (1, 1, 6, nRows, 1,  3, 0);

}

void COutputstableView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: Add your specialized code here and/or call the base class
	InitCurveInfo();
	InitCellData();	
	m_nRefresh = 1;
	Invalidate();
}

CString COutputstableView::GetToolDescription(CString strName,CString strPath)
{
	CStdioFile file;
	if (!file.Open(strPath, CFile::shareDenyNone|CFile::typeText))
	{
		return _T("");
	}
	CString str;
	while (file.ReadString (str))
	{
		str.TrimLeft();
		if (str.IsEmpty())
			continue;
		
		CStringArray strArray;
		SplitString(str, '\t', strArray);
		int nString = strArray.GetSize();
		if(nString < 1)
			continue;
		if(2 == nString)
		{
			strName.TrimLeft();
			strName.TrimRight();
			strArray[0].TrimLeft();
			strArray[0].TrimRight();
			if(!strName.CompareNoCase(strArray[0]))
			    return strArray[1];
		}
	}
	return _T("");
}

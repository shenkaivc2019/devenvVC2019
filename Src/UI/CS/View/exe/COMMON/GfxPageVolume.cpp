// GfxPageVolume.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "GfxPageVolume.h"
#include "Track.h"
#include "ULDoc.h"
#include "GraphWnd.h"
#include "Units.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGfxPageVolume dialog

//Add to support FilterList

CGfxPageVolume::CGfxPageVolume(CWnd* pParent /*=NULL*/)
	: CGfxPage(IDD_GFX_PAGE_VOLUME, pParent)
{
	//{{AFX_DATA_INIT(CGfxPageVolume)
	m_radioShow = 0;
	m_nHVMajorPip = 100;
	m_nHVMinorPip = 10;
	m_nCVMajorPip = 100;
	m_nCVMinorPip = 10;
	m_fMajorPip = 100.0f;
	m_fMinorPip = 10.0f;
	//}}AFX_DATA_INIT

}

void CGfxPageVolume::DoDataExchange(CDataExchange* pDX)
{
	CGfxPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGfxPageVolume)
	//DDX_Control(pDX, IDC_CB_CAL, m_cbCal);
	//DDX_Control(pDX, IDC_CB_TT, m_cbTT);
	//DDX_Text(pDX, IDC_EDIT1, m_nHVMajorPip);
	//DDX_Text(pDX, IDC_EDIT2, m_nHVMinorPip);
	//DDX_Text(pDX, IDC_EDIT3, m_nCVMajorPip);
	//DDX_Text(pDX, IDC_EDIT4, m_nCVMinorPip);
	DDX_Text(pDX, IDC_EDIT5, m_fMajorPip);
	DDX_Text(pDX, IDC_EDIT6, m_fMinorPip);
	DDX_Radio(pDX, IDC_RADIO1, m_radioShow);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CGfxPageVolume, CGfxPage)
	//{{AFX_MSG_MAP(CGfxPageVolume)
	//ON_CBN_SELCHANGE(IDC_CB_CAL, OnSelchangeCbCal)
	//ON_CBN_SELCHANGE(IDC_CB_TT, OnSelchangeCbTT)
	//ON_BN_CLICKED(IDC_CHECK1, OnCheck1)
	//ON_BN_CLICKED(IDC_CHECK2, OnCheck2)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	ON_BN_CLICKED(IDC_RADIO3, OnRadio3)
	//ON_EN_KILLFOCUS(IDC_EDIT1, OnKillfocusEdit1)
	//ON_EN_KILLFOCUS(IDC_EDIT2, OnKillfocusEdit2)
	//ON_EN_KILLFOCUS(IDC_EDIT3, OnKillfocusEdit3)
	//ON_EN_KILLFOCUS(IDC_EDIT4, OnKillfocusEdit4)
	ON_EN_KILLFOCUS(IDC_EDIT5, OnKillfocusEdit5)
	ON_EN_KILLFOCUS(IDC_EDIT6, OnKillfocusEdit6)
	ON_EN_CHANGE(IDC_EDIT7, OnChangeEdit7)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGfxPageVolume message handlers

void CGfxPageVolume::Refresh()
{
	if (m_pGraph->GetSafeHwnd())
	{
		CString strNames[] = { _T("IHV"), _T("ICV"), _T("ITT") };
		
		if ((m_pCurve) && (m_pCurve->m_pData))
		{
			CULFile* pFile = DYNAMIC_DOWNCAST(CULFile, m_pGraph->m_pDoc);
			CCurve* pCurve = m_pCurve->m_pData->m_pSrcCurve;
			if (pCurve && pFile)
			{
				for (int i = 0; i < 3; i++)
				{
					if (pCurve->m_strName.CompareNoCase(strNames[i]) == 0)
					{
						pFile->UpdateInfors();
						break;
					}
				}
			}
			m_pGraph->Invalidate();
		}
		m_pGraph->m_pGraphHeaderWnd->Invalidate();
	}
}

void CGfxPageVolume::ShowDepthItem(int nShow)
{
/*	if (m_pCurve != NULL && AfxIsValidString(m_pCurve->m_strName))
	{
		m_radioShow = m_pTrack->m_nTTShow;
		GetDlgItem(IDC_EDIT5)->EnableWindow(m_radioShow != 0);
		GetDlgItem(IDC_EDIT6)->EnableWindow(m_radioShow != 0);
		m_fMajorPip = m_pCurve->m_fMajorPip;
		m_fMinorPip = m_pCurve->m_fMinorPip;
		UpdateData(FALSE);
	}

	GetDlgItem(IDC_STATIC_CAL)->ShowWindow(nShow);
    m_cbCal.ShowWindow(nShow);

	GetDlgItem(IDC_CHECK1)->ShowWindow(nShow);
	GetDlgItem(IDC_CHECK2)->ShowWindow(nShow);
	m_nHVMajorPip = m_pTrack->m_nHVMajorPip;
	m_nHVMinorPip = m_pTrack->m_nHVMinorPip;
	m_nCVMajorPip = m_pTrack->m_nCVMajorPip;
	m_nCVMinorPip = m_pTrack->m_nCVMinorPip;
	m_nTTMajorPip = m_pTrack->m_nTTMajorPip;
	m_nTTMinorPip = m_pTrack->m_nTTMinorPip;
	if (nShow != SW_HIDE)
	{
		//水泥体积在右边
		CheckDlgButton(IDC_CHECK1, m_pTrack->m_bShowCement);
		GetDlgItem(IDC_EDIT3)->EnableWindow(m_pTrack->m_bShowCement);
		GetDlgItem(IDC_EDIT4)->EnableWindow(m_pTrack->m_bShowCement);
		
		CheckDlgButton(IDC_CHECK2, m_pTrack->m_bShowHole);
		GetDlgItem(IDC_EDIT1)->EnableWindow(m_pTrack->m_bShowHole);
		GetDlgItem(IDC_EDIT2)->EnableWindow(m_pTrack->m_bShowHole);

		// 往井径曲线下拉框里添加所有曲线
		CULDoc* pDoc = m_pGraph->m_pGraphHeaderWnd->m_pDoc;
		if (pDoc == NULL)
			return;

		vec_ic datas;
		vec_ic* pDatas = NULL;
		CULFile* pFile = DYNAMIC_DOWNCAST(CULFile, pDoc);
		if (pFile == NULL)
			return;

		if (pFile != NULL)
		{
			pDatas = &pFile->m_vecCurve;
		}
		else
			pDatas = &pDoc->m_vecCurve;
		
		if (pDatas == NULL)
			return;
		m_cbCal.ResetContent();		
		m_cbTT.ResetContent();
		int nItem = m_cbCal.AddString(_T(""));
		m_cbCal.SetItemData(nItem, 0);
		nItem = m_cbTT.AddString(_T(""));
		m_cbTT.SetItemData(nItem, 0);
		int nCount = pDatas->size();
		for (int i = 0; i < nCount; i++)
		{
			CCurve* pCurve = (CCurve*)pDatas->at(i);
			nItem = m_cbCal.AddString(pCurve->m_strName);
			m_cbCal.SetItemData(nItem, (DWORD_PTR)pCurve);
			nItem = m_cbTT.AddString(pCurve->m_strName);
			m_cbTT.SetItemData(nItem, (DWORD_PTR)pCurve);
		}

		m_cbCal.SelectString(-1, m_pTrack->m_strCAL);
		m_cbTT.SelectString(-1, m_pTrack->m_strTT);
	}
	UpdateData(FALSE);*/
}

/*
void CGfxPageVolume::OnSelchangeCbCal() 
{
	int nSelect = m_cbCal.GetCurSel();
	if (nSelect == CB_ERR)
		return;
	CCurve* pCurve = (CCurve*)m_cbCal.GetItemData(nSelect);
	if (pCurve && AfxIsValidString(pCurve->m_strName))
	{
		 if (m_pTrack->m_strCAL != pCurve->m_strName)
				m_pTrack->SetCal(pCurve);
	}
	else
		m_pTrack->SetCal(NULL);
   
	if (m_pGraph->GetSafeHwnd())
	{
		m_pGraph->Invalidate();
		m_pGraph->m_pGraphHeaderWnd->Invalidate();
	}
}

// 
void CGfxPageVolume::OnSelchangeCbTT() 
{
	int nSelect = m_cbTT.GetCurSel();
	if (nSelect == CB_ERR)
		return;
	CCurve* pCurve = (CCurve*)m_cbTT.GetItemData(nSelect);
	if (pCurve && AfxIsValidString(pCurve->m_strName))
	{
		 if (m_pTrack->m_strTT != pCurve->m_strName)
				m_pTrack->SetTT(pCurve);
	}
	else
		m_pTrack->SetTT(NULL);
   
	if (m_pGraph->GetSafeHwnd())
	{
		m_pGraph->Invalidate();
		m_pGraph->m_pGraphHeaderWnd->Invalidate();
	}
}

// 水泥体积右边
void CGfxPageVolume::OnCheck1() 
{
	m_pTrack->m_bShowCement = !m_pTrack->m_bShowCement;
	GetDlgItem(IDC_EDIT3)->EnableWindow(m_pTrack->m_bShowCement);
	GetDlgItem(IDC_EDIT4)->EnableWindow(m_pTrack->m_bShowCement);

	if (m_pTrack->m_bShowCement)
	{
		if (m_pTrack->m_bShowHole)
		{
			m_pTrack->m_nTTShow = 0;
			m_radioShow = 0;
		}
		else if (m_pTrack->m_nTTShow != 0)
		{
			m_pTrack->m_nTTShow = 1;
			m_radioShow = 1;
		}
		UpdateData(FALSE);
	}
	if (m_pGraph->GetSafeHwnd())
	{
		m_pGraph->RefreshVolumeData();
		m_pGraph->m_pGraphHeaderWnd->Invalidate();
	}
}

// 井眼体积
void CGfxPageVolume::OnCheck2() 
{
	UpdateData(TRUE);
	m_pTrack->m_bShowHole = !m_pTrack->m_bShowHole;
	GetDlgItem(IDC_EDIT1)->EnableWindow(m_pTrack->m_bShowHole);
	GetDlgItem(IDC_EDIT2)->EnableWindow(m_pTrack->m_bShowHole);
	if (m_pTrack->m_bShowHole)
	{
		if (m_pTrack->m_bShowCement)
		{
			m_pTrack->m_nTTShow = 0;
			m_radioShow = 0;
		}
		else if (m_pTrack->m_nTTShow != 0)
		{
			m_pTrack->m_nTTShow = 2;
			m_radioShow = 2;
		}
		UpdateData(FALSE);
	}
	if (m_pGraph->GetSafeHwnd())
	{
		m_pGraph->RefreshVolumeData();
		m_pGraph->m_pGraphHeaderWnd->Invalidate();
	}
}
*/

void CGfxPageVolume::InitCurve()
{
	if (m_pCurve != NULL && AfxIsValidString(m_pCurve->m_strName))
	{
		SetDlgItemText(IDC_EDIT7, m_pCurve->m_strName);
		m_radioShow = m_pCurve->m_nPipShow;
		GetDlgItem(IDC_EDIT5)->EnableWindow(m_radioShow != 0);
		GetDlgItem(IDC_EDIT6)->EnableWindow(m_radioShow != 0);
		m_fMajorPip = m_pCurve->m_fMajorPip;
		m_fMinorPip = m_pCurve->m_fMinorPip;
		UpdateData(FALSE);
	}
}


void CGfxPageVolume::OnRadio1() 
{
	if (m_pCurve != NULL && AfxIsValidString(m_pCurve->m_strName))
		m_pCurve->m_nPipShow = 0;
	GetDlgItem(IDC_EDIT5)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDIT6)->EnableWindow(FALSE);

	Refresh();
}

// 在左边显示
void CGfxPageVolume::OnRadio2() 
{
	if (m_pCurve != NULL && AfxIsValidString(m_pCurve->m_strName))
		m_pCurve->m_nPipShow = 1;
	GetDlgItem(IDC_EDIT5)->EnableWindow(TRUE);
	GetDlgItem(IDC_EDIT6)->EnableWindow(TRUE);	
	
	Refresh();
}

// 在右边显示
void CGfxPageVolume::OnRadio3() 
{
	if (m_pCurve != NULL && AfxIsValidString(m_pCurve->m_strName))
		m_pCurve->m_nPipShow = 2;
	GetDlgItem(IDC_EDIT5)->EnableWindow(TRUE);
	GetDlgItem(IDC_EDIT6)->EnableWindow(TRUE);

	Refresh();
}
/*
void CGfxPageVolume::OnKillfocusEdit1() 
{
	UpdateData(TRUE);
	m_pTrack->m_nHVMajorPip = m_nHVMajorPip;
	
}

void CGfxPageVolume::OnKillfocusEdit2() 
{
	UpdateData(TRUE);
	m_pTrack->m_nHVMinorPip = m_nHVMinorPip;
}

void CGfxPageVolume::OnKillfocusEdit3() 
{
	UpdateData(TRUE);
	m_pTrack->m_nCVMajorPip = m_nCVMajorPip;
}

void CGfxPageVolume::OnKillfocusEdit4() 
{
	UpdateData(TRUE);
	m_pTrack->m_nCVMinorPip = m_nCVMinorPip;	
}*/

void CGfxPageVolume::OnKillfocusEdit5() 
{
	UpdateData(TRUE);
	if (m_pCurve != NULL && AfxIsValidString(m_pCurve->m_strName))
		m_pCurve->m_fMajorPip = m_fMajorPip;
	
	Refresh();
}

void CGfxPageVolume::OnKillfocusEdit6() 
{	
	UpdateData(TRUE);
	if (m_pCurve != NULL && AfxIsValidString(m_pCurve->m_strName))
		m_pCurve->m_fMinorPip = m_fMinorPip;

	Refresh();
}

// 曲线头 (Title)
void CGfxPageVolume::OnChangeEdit7() 
{
	if (m_pCurve != NULL && AfxIsValidString(m_pCurve->m_strName))
	{
		CString strTitle;
		GetDlgItemText(IDC_EDIT7, strTitle);
		m_pCurve->SetName(strTitle);
	}

	if (m_pGraph->GetSafeHwnd() != NULL)
		m_pGraph->m_pGraphHeaderWnd->Invalidate();
}

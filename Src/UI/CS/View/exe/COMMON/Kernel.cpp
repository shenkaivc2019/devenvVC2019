//Kernel.cpp
#include "stdafx.h"
#include "Kernel.h"
#include "CurveData.h"
#include "ULTool.h"
#include "Project.h"
#include "ULFile.h"
#include "MainFrm.h"
#include "ServiceTableItem.h"
#include "Units.h"
#include "GraphWnd.h"
#include "ComConfig.h"
#include "ULDataEditClass.h"
#include "ChildFrm.h"
#include "resource.h"
#ifdef _PERFORATION
#include "FileTrace.h"
#include "Perforation.h"
#endif

#ifdef _LOGIC
#include "Logic.h"
#else
#include "UL2000.h"
#include "ParamsView.h"
#include "RawFile.h"
#include "Alarm.h"
#endif

#include "Sheet.h"

IMPLEMENT_SERIAL(CULKernel, CObject, 1)

static int Cir_Count = 0;

UINT KernelThreadProc(LPVOID pParam)
{
	//TRACE("Kernel Show Thread Start\n");
	CULKernel* pULKernel = (CULKernel*)pParam;
	
	if (pULKernel == NULL)
	{
		AfxEndThread(0);
		return 0;
	}

#ifndef _LOGIC
	int nCount = pULKernel->m_arrTools.GetSize();
	if (nCount < 1)
	{
		AfxEndThread(0);
		return 0;
	}
	
	HANDLE* pHandle = new HANDLE[nCount];
	for (int i = 0; i < pULKernel->m_arrTools.GetSize(); i++)
	{
		CULTool* pULTool = (CULTool*)pULKernel->m_arrTools.GetAt(i);
		if (pULTool && pULTool->m_pEvent)
			pHandle[i] = pULTool->m_pEvent->m_hObject;
	}
	
	while (pULKernel->m_bRunThread)
	{
		// 等待所有的仪器将数据准备好
		// 当其中一个仪器出问题时，会出现无限等待的错误。
		//int nRetCode = ::WaitForMultipleObjects(nCount, pHandle, TRUE, INFINITE);
		int nRetCode = ::WaitForSingleObject(pHandle[0], INFINITE);
		if (nRetCode == WAIT_OBJECT_0)		//开始处理数据
		{
			if (!pULKernel->m_bRunThread)
				break;
			
			if (pULKernel->m_nWorkMode != WORKMODE_CAL && pULKernel->m_nAllToolState == UL_ALLREADY )
				pULKernel->DispatchData();
		}
		else if (nRetCode == WAIT_FAILED)
			pULKernel->m_bRunThread = FALSE;
	}
	delete []pHandle;
#endif

	AfxEndThread(0);
	TRACE("Kernel Show Thread Exit\n");
	return 0;
}

CULKernel::CULKernel()
{
	m_bBoardsSet = FALSE;
	m_nWorkMode = WORKMODE_IDLE;
	m_bRunThread = FALSE;
	m_uMessageID   = 0;

	m_pChannelEvent = NULL;
	m_hExchangeFile = NULL;
	m_lpCodeBuffer  = NULL;

	////////////////////////////////////////////////
	m_logInfo.nMode = 1;	
	m_logInfo.nDirection = 0;
	m_logInfo.depthInfo.fPoint = 0.0;
	m_logInfo.depthInfo.bAdjust = FALSE;
	m_logInfo.depthInfo.bAdjustPoint = 0.0;

	m_logInfo.timeInfo.fPoint = 0.0;
	m_logInfo.timeInfo.bAdjust = FALSE;
	m_logInfo.timeInfo.bAdjustTime = 0.0;

	m_nAllToolState = UL_NOTALLREADY;
	m_nCurDisplayMode =	DISPLAYMODE_LOG;

	m_bLoggingSave = FALSE;
	m_pDepthData = NULL;
	m_pSaveInfo = NULL;

	m_lDepthOff = 0;
	m_nToolIndex = -1;
	MapExchangeFile();
	m_nReplayDireciton = 0;
	m_nDebug = 0;

	//add by gj 20121228
	m_dwReplayLength = 0;//用于回放进度条显示
	m_dwHeadLength = 0;
	m_dwCurrLength = 0;
	m_bFirstReplay = TRUE;
	m_bLockCheck =FALSE;
	m_RefreshCount = 0;//绘图刷新次数
	m_bWriteFinish = FALSE;

#ifdef _LWD
	m_pRTFile = NULL;
#endif
}
	
CULKernel::~CULKernel()
{	
	UnMapExchangeFile();
	m_bRunThread = FALSE;
	ClearTools();
}

void CULKernel::Serialize(CArchive& ar)
{
	BYTE btReversed[2048];
	CObject::Serialize( ar );		// Always call base class Serialize.
	if ( ar.IsStoring() )			// Store other members : <<
	{
		ar.Write(btReversed,2048);
	}
	else							// load other members : >>
	{
		ar.Read(btReversed,2048);
	}
}

void CULKernel::ClearTools()
{
#ifdef _PERFORATION
	Gbl_DataEx.ClearMap();
#endif
	for( int i = 0; i < m_arrTools.GetSize(); i++)
	{
		CString prompt;
		try
		{
			CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
			if (pULTool != NULL)
			{
				CString str;
				str.LoadString(IDS_TOOL_ERR);//Error occurred on tool %s delete!
				prompt.Format(str, pULTool->strToolName);
				delete pULTool;
			}
		}
		catch (...)
		{
			AfxMessageBox(prompt, MB_ICONEXCLAMATION);
		}
	}
	m_arrTools.RemoveAll();
}

/////////////////////////////////////////////////////////////////////////////
// 增加测井仪器的步骤
//
short CULKernel::AddTool(CULTool* pULTool)
{
	for( int i = 0; i < m_arrTools.GetSize(); i++)
	{
		CULTool* pULTool1 = (CULTool*)m_arrTools.GetAt(i);
// 		if (pULTool1->m_strToolDllName == pULTool->m_strToolDllName)
// 			return UL_ERROR;
	}
	m_arrTools.Add(pULTool);

	return UL_NO_ERROR;
}

short CULKernel::AddTool(CString&  strToolName)
{
	strToolName = "tl" + strToolName + ".dll";
	strToolName.MakeUpper();

	for( int i = 0; i < m_arrTools.GetSize(); i++)
	{
		CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
		if (pULTool->m_strToolDllName == strToolName)
			return UL_ERROR;
	}

	if (!PreCheckFile(strToolName))
		return UL_ERROR;

	CULTool* pULTool = new CULTool;
	if (pULTool->LoadTool(strToolName))
		m_arrTools.Add(pULTool);
	else
	{
		delete pULTool;
		return UL_ERROR;
	}

	return UL_NO_ERROR;
}

void CULKernel::SetToolInterface()
{
	 /*
     * add the opened tool to the group
     */
	CULTool::m_vecTools.clear();
	
	int nTools = m_arrTools.GetSize();
	for (int i = 0; i < nTools; i++)
	{
        CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
		ULTOOL oTool;
		oTool.pIULTool = static_cast <IULTool*>(pULTool);
		oTool.pITool = pULTool->m_pITool;
		CULTool::m_vecTools.push_back(oTool);
	}
}

short CULKernel::OpenTool()
{
	int nTools = m_arrTools.GetSize();
/*	int nBoard = 0;
	IToolBoard* pBoard = NULL;
	CString strTools;
	for (int i = 0; i < nTools; i++)
	{
		CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
		if (pULTool == NULL)
			continue;

		if (pULTool->m_pIBoard != NULL)
		{
			if (nBoard)
				strTools += ",";

			strTools += pULTool->strToolName;
			pBoard = pULTool->m_pIBoard;
			nBoard ++;
		}
	}

	if (nBoard > 1)
	{
		CString strPrompt;
		// AfxFormatString1(strPrompt, IDS_TOOL_BOARDS, strTools);
		AfxMessageBox(strPrompt);
	}
	else if (nBoard == 1)
	{
		CStringList strBoards;
		int nBoards = Gbl_ConfigInfo.EnumBoardKeys(strBoards);
		DWORD dwParam = 0;
		ASSERT(pBoard != NULL);
		pBoard->OpenBoards(&strBoards, &dwParam);
		if (nBoards != strBoards.GetCount())
		{
			Gbl_ConfigInfo.SetBoards(&strBoards);
			CWnd* pWnd = theApp.GetDMWnd();
			if (pWnd != NULL)
			{
				pWnd->SendMessage(UM_BOARDS_SET, 1, dwParam);
				m_bBoardsSet = TRUE;
			}
		}
	}
*/	
	g_Message.Empty();

	m_strSetChannels.RemoveAll();
	SetChannelOffset();
	SetToolInterface();
	int i = 0;
	for (i=0;i < nTools; i++)
	{
		CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
		int nSameTool=0;
		int nPreSameTool=0;
		if (pULTool)
		{
			CULTool *psULTool;
			BOOL bFirst = TRUE;
			int j = 0;
			for(j=0;j<i;j++)
			{
				psULTool=(CULTool*)m_arrTools.GetAt(j);
				if(pULTool->strToolName==psULTool->strToolName)
				{
					if (bFirst)
					{
						psULTool->m_nSameToolIndex = 1;
						for(int nCurve=0;nCurve<psULTool->m_Curves.GetSize();nCurve++)
						{
							CCurve* pCurve=(CCurve*)psULTool->m_Curves.GetAt(nCurve);
							pCurve->m_nTLIndex=1;
						}
						bFirst = FALSE;
					}
					nPreSameTool++;
				}
			}

			if(nPreSameTool>0)
			{
				pULTool->m_nSameToolIndex = nPreSameTool + 1;
				for(int nCurve=0;nCurve<pULTool->m_Curves.GetSize();nCurve++)
				{
					CCurve* pCurve=(CCurve*)pULTool->m_Curves.GetAt(nCurve);
					pCurve->m_nTLIndex=nPreSameTool + 1;
				}
			}
		
			for(j=0;j<pULTool->m_Channels.GetSize();j++)
			{
				CChannel * pChannel=(CChannel*)pULTool->m_Channels.GetAt(j);
				SetChannelBufSize(pChannel);
			}

			pULTool->OpenTool();
		}
	}

	// 统一显示出错信息
	if (g_Message.GetLength())
	{
		//::MessageBox(NULL, g_Message, AfxGetAppName(), MB_OK | MB_APPLMODAL);
		AfxMessageBox(g_Message); // cl 修正不点确定对工程服务项目操作再次操作出错的问题
		g_Message.Empty();
	}

	// 启动线程
	m_bRunThread = TRUE;	
	if (AfxBeginThread(KernelThreadProc, this) == NULL)
		return UL_ERROR;

	return UL_NO_ERROR;
}

#ifndef _LOGIC

void CULKernel::ReplayOpenTool()
{
	SetChannelOffset();
	SetToolInterface();
	int i;
	for (i = 0; i < m_arrTools.GetSize(); i++)
	{
		CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
		for (int j = 0; j < pULTool->m_Curves.GetSize(); j++)
		{
			CCurve* pCurve = pULTool->m_Curves.GetAt(j);
			pCurve->Filter(Gbl_ConfigInfo.m_System.RelogFilter);
		}
		CToolInfoPack* pToolInfo = (CToolInfoPack*)g_pActPrj->m_RawFile.m_ToolInfoPackList.GetAt(i);
		if (pToolInfo != NULL)
			pToolInfo->ReplayApplyToolInfo(pULTool);
		
		int nSameTool = 0;
		int nPreSameTool = 0;
		if (pULTool)
		{
			CULTool *psULTool;
			BOOL bFirst = TRUE;
			int j = 0;
			for(j = 0; j < i; j++)
			{
				psULTool = (CULTool*)m_arrTools.GetAt(j);
				if (pULTool->strToolName == psULTool->strToolName)
				{
					if (bFirst)
					{
						psULTool->m_nSameToolIndex = 1;
						for(int nCurve=0; nCurve < psULTool->m_Curves.GetSize(); nCurve++)
						{
							CCurve* pCurve = (CCurve*)psULTool->m_Curves.GetAt(nCurve);
							pCurve->m_nTLIndex = 1;
						}
						bFirst = FALSE;
					}
					nPreSameTool++;
				}
			}

			if (nPreSameTool > 0)
			{
				pULTool->m_nSameToolIndex = nPreSameTool + 1;
				for(int nCurve=0;nCurve<pULTool->m_Curves.GetSize();nCurve++)
				{
					CCurve* pCurve=(CCurve*)pULTool->m_Curves.GetAt(nCurve);
					pCurve->m_nTLIndex=nPreSameTool + 1;
				}
			}
		
			for(j = 0;j < pULTool->m_Channels.GetSize(); j++)
			{
				CChannel * pChannel=(CChannel*)pULTool->m_Channels.GetAt(j);
				SetChannelBufSize(pChannel);
			}	

		    pULTool->ReplayOpenTool();
		}
	}
}

#endif

short CULKernel::CloseTool()
{
	InterlockedExchange(&m_bRunThread, 0);

	for( int i = 0; i < m_arrTools.GetSize(); i++)
	{
		CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
		if (pULTool)
			pULTool->CloseTool();
	}

	AskCommand cmd;	
	cmd.nCommandType = UL_CLOSEALLCHANNEL;	
	SetCommand(&cmd);
#ifdef _PERFORATION
	Gbl_DataEx.ClearMap();
#endif
	Sleep(100);

/*	if (m_bBoardsSet)
	{
		CWnd* pWnd = theApp.GetDMWnd();
		if (pWnd !=  NULL)
		{
			pWnd->SendMessage(UM_BOARDS_SET);
			m_bBoardsSet = FALSE;
		}
		Sleep(10);
	}
*/	
	return UL_NO_ERROR;
}

short CULKernel::ReplayCloseTool()
{
	for (int i = 0; i < m_arrTools.GetSize(); i++)
	{
		CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
		if (pULTool)
			pULTool->ReplayCloseTool();
	}

	return UL_NO_ERROR;
}

// 控制屏幕刷新频率
BOOL CULKernel::CanRefresh()
{
	if (m_nRefreshCal <= 0)
	{
		//刚开始
		m_nRefreshCal = 0;
		m_dwStartTick = GetTickCount();
		m_nRefreshCal++;
		return TRUE;
	}
	else if (m_nRefreshCal == 1)
	{
		//第2次 
		//实际每秒刷新几次
		float frequence = 1000.0f / (GetTickCount() - m_dwStartTick); 
		Cir_Count = 0;
		if (Gbl_ConfigInfo.m_Watch.Refresh != 0)
			Cir_Count = frequence / Gbl_ConfigInfo.m_Watch.Refresh;
	}

	if (m_nRefreshCal >= Cir_Count)
	{
		m_nRefreshCal = 0;
		return TRUE;
	}
	else
	{
		m_nRefreshCal++;
		return FALSE;
	}
}

BOOL CULKernel::PreCheckFile(CString& strToolDllName)
{
	BOOL bResult = TRUE;
	
	// 1、如果仪器动态库不存在，错误
	HINSTANCE hDLL = LoadLibrary(strToolDllName);
	if (hDLL == NULL)
	{
		CString strInfo;
		strInfo.LoadString(IDS_NOEXSIT_NODLL);
		strInfo = strToolDllName + strInfo;
		AfxMessageBox(strInfo);
		bResult = FALSE;
		return bResult;
	}
	FreeLibrary(hDLL);

	return bResult;
}

CULTool* CULKernel::GetTool(CString& strToolName)
{
	for( int i = 0; i < m_arrTools.GetSize(); i++)
	{
		CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
		if (pULTool->strToolName == strToolName)
			return pULTool;
	}
	return NULL;
}

short CULKernel::DeleteTool(CString& strToolName)
{
	
	BOOL bFound = FALSE;
	strToolName.MakeUpper();

	CULTool* pULTool = NULL;
	for( int i = 0; i < m_arrTools.GetSize(); i++)
	{
		pULTool = (CULTool*)m_arrTools.GetAt(i);
		if (pULTool->strToolName == strToolName)
		{
			delete pULTool;
			pULTool = NULL;
			m_arrTools.RemoveAt(i);
			return UL_NO_ERROR;
		}
	}

	return UL_ERROR;	
}

CCurve* CULKernel::GetDepthCurve(CURVES& vecCurve)
{
	CCurve* pDepthCurve = NULL;
	vecCurve.clear();
	CMainFrame* pMainFrame = (CMainFrame*) theApp.m_pMainWnd;
	CCurve* pCurve = NULL;
	if(pMainFrame->m_bBrowseFlag)
	{
		CChildFrame* pChild = DYNAMIC_DOWNCAST(CChildFrame, pMainFrame->GetActiveFrame());
		ASSERT(pChild != NULL);

		if(m_pRTFile != NULL)
		{
			for(int j = 0 ; j < ((CULFile*)m_pRTFile)->m_vecRTCurve.size() ; j++)
			{
				pCurve = (CCurve *)((CULFile*)m_pRTFile)->m_vecRTCurve.at(j);
				
				if(!pMainFrame->m_bTVDFlag)		//非TVD数据曲线
					vecCurve.push_back(pCurve);

				if (pCurve->IsDepthCurve())
					pDepthCurve = pCurve;
			}

			if(pMainFrame->m_bTVDFlag)
			{
				vecCurve.push_back(pDepthCurve);
				for(int j = 0 ; j < pChild->m_vecTVDCurve.size() ; j++)
				{
					pCurve = (CCurve *)pChild->m_vecTVDCurve.at(j);
					
					if (pCurve->IsDepthCurve())
						continue;
					
					vecCurve.push_back(pCurve);
				}
			}
		}
	}
	else
	{
		for (int i=0; i<m_arrTools.GetSize(); i++)
		{
			CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
			for (int j=0; j<pULTool->m_Curves.GetSize(); j++)
			{
				pCurve = (CCurve *)pULTool->m_Curves.GetAt(j);
				
				vecCurve.push_back(pCurve);
				if (pCurve->IsDepthCurve())
					pDepthCurve = pCurve;
			}
		}
	}

	return pDepthCurve;
}

CCurveData* CULKernel::GetDepthCurve(vec_data& vecCurve, vec_data& vecStatus)
{
	CCurveData* pDepthCurve = NULL;
	vecCurve.clear();
	for (int i=0; i<m_arrTools.GetSize(); i++)
	{
		CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
		for (int j=0; j<pULTool->m_Curves.GetSize(); j++)
		{
			CCurve* pCurve = (CCurve *)pULTool->m_Curves.GetAt(j);
			if (pCurve->IsDepthCurve())
			{
				vecCurve.insert(vecCurve.begin(), pCurve->m_pData);
				pDepthCurve = pCurve->m_pData;
				continue;
			}
			
			vecCurve.push_back(pCurve->m_pData);
		}
	}

	return pDepthCurve;
}

CCurve* CULKernel::GetDepthCurve()
{
	for (int i=0; i<m_arrTools.GetSize(); i++)
	{
		CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
		for (int j=0; j<pULTool->m_Curves.GetSize(); j++)
		{
			CCurve * pCurve = (CCurve *)pULTool->m_Curves.GetAt(j);
			if (pCurve->IsDepthCurve())
				return pCurve;
		}
	}
	return NULL;
}

CCurve* CULKernel::GetTimeCurve()
{
	for (int i=0; i<m_arrTools.GetSize(); i++)
	{
		CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
		for (int j=0; j<pULTool->m_Curves.GetSize(); j++)
		{
			CCurve * pCurve = (CCurve *)pULTool->m_Curves.GetAt(j);
			if (pCurve->IsTimeCurve())
				return pCurve;
		}
	}
	return NULL;
}

short CULKernel::GetAllCurve(std::vector<ICurve*>& vecCurve)
{
	vecCurve.clear();
	CMainFrame* pMainFrame = (CMainFrame*) theApp.m_pMainWnd;
	ICurve* pCurve = NULL;
	if(pMainFrame->m_bBrowseFlag)
	{
		if(m_pRTFile != NULL)
		{
			for(int j = 0 ; j < ((CULFile*)m_pRTFile)->m_vecRTCurve.size() ; j++)
			{
				pCurve = (ICurve *)((CULFile*)m_pRTFile)->m_vecRTCurve.at(j);
				vecCurve.push_back(pCurve);
			}
		}
	}
	else
	{	
		for (int i=0; i<m_arrTools.GetSize(); i++)
		{
			CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
			for (int j=0; j<pULTool->m_Curves.GetSize(); j++)
			{
				pCurve = (ICurve*)pULTool->m_Curves.GetAt(j);
				vecCurve.push_back(pCurve);
			}
		}
	}
	
	return UL_NO_ERROR;
}

short CULKernel::GetAllCurveProps(CURVEPROPS& vecCurve, BOOL bDelAdd /* = FALSE */)
{
	vecCurve.clear();
	for (int i=0; i<m_arrTools.GetSize(); i++)
	{
		CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
		for (int j=0; j<pULTool->m_Curves.GetSize(); j++)
		{
			CCurve* pCurve = (CCurve*)pULTool->m_Curves.GetAt(j);
			if( !pCurve->m_bDelete || (bDelAdd && pCurve->m_bDelete) )
			{
				CURVEPROPERTY* curve = new CURVEPROPERTY;
				pCurve->GetCurveProp(curve);
				vecCurve.push_back(curve);
			}
		}
	}

	return UL_NO_ERROR;
}

// 查找相应的动态库标识，然后执行
short CULKernel::OpenTool(CString& strToolName)
{
	strToolName.MakeUpper();

	CULTool* pULTool = NULL;
	for( int i = 0; i < m_arrTools.GetSize(); i++)
	{
		pULTool = (CULTool*)m_arrTools.GetAt(i);
		if (pULTool->strToolName == strToolName)
			return UL_NO_ERROR;
	}

	return UL_ERROR;
}

void CULKernel::SetWorkMode(int nWorkMode)
{
	m_nWorkMode = nWorkMode;

	SetBoardMode(nWorkMode);
	CULTool* pULTool = NULL;
	for( int i = 0; i < m_arrTools.GetSize(); i++)
	{
		pULTool = (CULTool*)m_arrTools.GetAt(i);
		pULTool->SetWorkMode(nWorkMode);
	}	
}

void CULKernel::SetBoardMode(int nWorkMode)
{		
	AskCommand cmd;	
	cmd.nCommandType = UL_SETWORKMODE;
	cmd.ex_context .workmode_param.nWorkMode = nWorkMode;
	SetCommand(&cmd);	

}

void CULKernel::SetAllUseBoard(int nCmd)
{	
	AskCommand cmd;	
	cmd.nCommandType = (CommandType)nCmd;
	SetCommand(&cmd);
}

BOOL CULKernel::SetCommand(AskCommand * pCmd)
{
	if (m_pChannelEvent == NULL || m_hExchangeFile == NULL)
		return FALSE;	
	memcpy(m_lpCodeBuffer, pCmd, sizeof(AskCommand));
	m_pChannelEvent->SetEvent();
	return TRUE;
}

void CULKernel::CalcCalGene()
{
	CULTool* pULTool = NULL;
	for( int i = 0; i < m_arrTools.GetSize(); i++)
	{
		pULTool = (CULTool*)m_arrTools.GetAt(i);

		BOOL bCal = TRUE;
		for(int j=0; j<pULTool->m_CalCurves[pULTool->m_uCalPhase].GetSize(); j++)
		{
			CULCalCurve *pCalCurve = (CULCalCurve*)pULTool->m_CalCurves[pULTool->m_uCalPhase].GetAt(j);
			if(pCalCurve->m_bCaled == FALSE)
			{
				bCal = FALSE;
				break;
			}
		}
		if (bCal)
			pULTool->ComputeCalCoefficient();
	}
}

void CULKernel::PrePareGroupInfo()
{
	for (int i = 0; i < m_arrTools.GetSize(); i++)
	{
		CULTool* pTool = (CULTool*)m_arrTools.GetAt(i);
		if (pTool->m_strGroupTo.GetLength())
		{
			CULTool* pGTool = (CULTool*)GetTool(pTool->m_strGroupTo);
			if (pGTool != NULL)
			{
				pGTool->m_strGroupFrom = pTool->strToolName;
				pTool->lLength = 0;	// 如果与其他仪器组合，长度为0
			}
		}
	}
}

void CULKernel::PrePareLogInfo()		//设置测井曲线参数
{
#ifdef _PERFORATION
	g_alarm.InitMap(&m_vecStatus);
	g_alarm.InitMap(&m_vecData, FALSE);
#endif
#ifdef _DATA_TRACE
//	TRACE("================ %d ===============\n", g_count++);
#endif	
	
	PrePareGroupInfo();
	m_lDepthOff = 0;
	long	lLength = 0;
	long	minOffsetDepth = 0; 
	long	maxOffsetDepth = 0;	
	BOOL	bCalcDepthOffset = FALSE;
	if (m_logInfo.nMode && (m_logInfo.nWorkMode != DISPLAYMODE_WATCH))
	{
		if (Gbl_ConfigInfo.m_Other.bCalcDepthOffset)
		{
			if (Gbl_ConfigInfo.m_Other.bOffsetValueSet)
				bCalcDepthOffset = 2;
			else
				bCalcDepthOffset = 1;
		}
	}
	//lxc
	//add by xwh 2012/11/16
	//修改：监视测井回放文件第二次回放时m_logInfo.nDirection的值被改变，导致第二次回放出现偏移
	//m_nReplayDireciton 作为监视测井的第二个标志，-1表示为监视测井，不偏移
	if(m_logInfo.nDirection == -1)
	{
		bCalcDepthOffset = FALSE;
		m_nReplayDireciton = -1;
	}
	if (m_nWorkMode == WORKMODE_COMPUTEPLAYBACK &&m_nReplayDireciton == -1)
	{
		bCalcDepthOffset = FALSE;
	}
	int nTools = m_arrTools.GetSize();
	BOOL bFirstTool = FALSE;
	
	// 计算仪器偏移,第一个仪器是深度仪器,服务项目中的仪器的实际下井顺序是按从下到上排的
	// 深度仪器相当于最下边的一个仪器，而服务项目最下边的仪器对应实际仪器组合最上面的仪器
	int i = 0;
	for (i = 0; i < nTools; i++)
	{
		CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);

		// 计算仪器偏移：仪器本身相对于整个仪器串底部的深度偏移
		if (pULTool->lLength > MAX_TOOL_LENGTH)
		{
			CString strMessage;
			CString str;
			str.LoadString(IDS_TOOL_LENGTH_ERROR);//The tool %s length is error, please check!
			strMessage.Format (str, pULTool->strToolName);
			AfxMessageBox(strMessage);
		}

		pULTool->m_lOffset = lLength; 
		lLength += pULTool->lLength;
		pULTool->m_pLogInfo = &m_logInfo;
	}

	// 根据组合重调整仪器偏移
	for (i = 0; i < nTools; i++)
	{
		CULTool* pTool = (CULTool*)m_arrTools.GetAt(i);
		if (pTool->m_strGroupTo.GetLength())
		{
			CULTool* pGTool = (CULTool*)GetTool(pTool->m_strGroupTo);
			if (pGTool != NULL)
			{
				if (pGTool->m_strGroupFrom.GetLength())
				{
					pTool->m_lOffset = pGTool->m_lOffset;
				}
			}
		}

		for (int j = 0; j < pTool->m_Curves.GetSize(); j++)
		{
			CCurve* pCurve = (CCurve*)pTool->m_Curves.GetAt(j);
			CCurveData* pData = pCurve->m_pData;
			switch (pData->m_nOffsetMode)
			{
			case OM_TB:
				pData->m_lTBOffset = pData->m_lDepthOffset;
				break;
			case OM_TT:
				pData->m_lTBOffset = pTool->lLength + pData->m_lDepthOffset;
				break;
			case OM_TSB:
				pData->m_lTBOffset = pData->m_lDepthOffset - pTool->m_lOffset;
				break;
			case OM_TST:
				pData->m_lTBOffset = lLength + pData->m_lDepthOffset - pTool->m_lOffset;
				break;
			}
		}
	}
	CServiceTableItem* pService = g_pActPrj->m_pService;
//一种在服务项计算的 曲线偏移结果 保存在服务项中 但是存在重计算问题
//	long  lAliCurDepOffSet = pService->m_lAliCurDepOffSet;
//一种在kernel中 每次测井前重新计算
	long  lAliCurDepOffSet = 0;
	BOOL  bAligCurve =FALSE;
	BOOL  bTopTool = pService->m_bTop;
	if (bTopTool)
	{
		for (int i =0;i<nTools;i++)
		{
			CULTool* pTool = (CULTool*)m_arrTools.GetAt(i);
			lAliCurDepOffSet += pTool->lLength;
			bAligCurve = TRUE;
		}
	}
	else
	{	
		int k = 0;
		for (k=0;k<nTools;k++)
		{
			CULTool* pTool = (CULTool*)m_arrTools.GetAt(k);		
			if (!strcmp(pTool->strENName,g_pActPrj->m_pService->m_strToolName))
			{
				for (int k1=0;k1<pTool->m_Curves.GetSize();k1++)
				{	
					if (!strcmp(pTool->m_Curves.GetAt(k1)->m_strName,g_pActPrj->m_pService->m_strCurveName))
					{
						lAliCurDepOffSet += pTool->m_Curves.GetAt(k1)->DepthOffset();
						bAligCurve = TRUE;	
					}
				}
				break;
			}
			lAliCurDepOffSet += pTool->lLength;
		}
	}
	if (!bAligCurve)
	{
		lAliCurDepOffSet = 0; 
	}
	lLength = 0;
	for (i = 0; i < nTools; i++)
	{
		CULTool *pULTool = (CULTool*)m_arrTools.GetAt(i);
		lLength += pULTool->lLength;
		if ((lLength > 0) && (minOffsetDepth == 0))
		{	
			bFirstTool = TRUE;
			// 最小深度偏移初值
			minOffsetDepth = pULTool->lLength;
		}
		else
			bFirstTool = FALSE;
		
		for (int j= 0; j < pULTool->m_Curves.GetSize(); j++)
		{
			CCurve* pCurve = (CCurve*)pULTool->m_Curves.GetAt(j);
			CCurveData* pData = pCurve->m_pData;
			if ((pData->m_nMode & CURVE_TOOL) == 0)
			{
#ifdef _RUNIT
				pData->CalcUnit(g_units, pULTool->strInfoPath);
#endif // _RUNIT
#ifndef	_LWD
				pData->Clear();	//在随钻测井时，不删除实时数据
#endif
			}
			else
			{
				if (pCurve->AutoDirection() != m_logInfo.nDirection)
					pCurve->InvertCurveDirection();
#ifdef _RUNIT
				pData->m_bRecUnit = FALSE;
#endif // _RUNIT
			}
			pData->m_nPointIndex = 0;		// 第一点
			pData->m_lFirstDepth = INVALID_DEPTH;
			pData->m_lToolOffset = pULTool->m_lOffset - lAliCurDepOffSet;			// 仪器偏移
			pData->m_fFeedPoint = m_logInfo.depthInfo.fPoint;	// 各个曲线采样率可能不同，各曲线计算自己的偏移点数
			pData->m_bCalcOffset = bCalcDepthOffset ;
	
			
// 计算曲线偏移：仪器本身相对于整个仪器串底部的深度偏移 + 曲线本身相对于仪器底部的偏移
			if (bCalcDepthOffset)
			{
				pCurve->DepthOffset(); // 曲线本身相对于仪器底部的偏移
				// 最小深度偏移
				if (bFirstTool)
				{	
					if (pData->m_lTBOffset < minOffsetDepth)
						minOffsetDepth = pData->m_lTBOffset;
				}
				
				// 最大深度偏移
				if (pData->m_lToolOffset + pData->m_lTBOffset > maxOffsetDepth)
					maxOffsetDepth = pData->m_lToolOffset + pData->m_lTBOffset;
				pData->m_bDrawOffset = Gbl_ConfigInfo.m_Other.bDrawDepthOffset; 
			}
			else
				pData->m_bDrawOffset = FALSE;

			// 重置数据索引
			pData->m_nCurSavePos = 0;			// 重置数据源存盘索引
			pData->SetCurDisplayPosSame(0);		// 重置所有与此仪器相关绘图曲线的显示索引
			pData->SetCurPrintPosSame(0);		// 重置所有与此仪器相关绘图曲线的打印索引

			// 重置曲线方向
			pData->m_nDirection = m_logInfo.nDirection;			
		}
	}

	if (!bCalcDepthOffset) // 非深度驱动或不计算偏移
	{
		for (i = 0; i < nTools; i++)
		{
			CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
			if (pULTool == NULL)
				continue ;
			
			for (int j= 0; j < pULTool->m_Curves.GetSize(); j++)
			{
				CCurve* pCurve = (CCurve*)pULTool->m_Curves.GetAt(j);
				pCurve->m_pData->m_bAlignTopOffset = FALSE;
				pCurve->m_pData->m_lAllToolLength = lLength;
				pCurve->m_pData->m_lMaxDepthOffset = 0;
				pCurve->m_pData->m_lCalcOffset = 0;
			}
		}
		
		return ;
	}

#ifdef _DATA_TRACE
	TRACE("Direction : %d\n", m_logInfo.nDirection);
#endif
			
	// 深度驱动，计算偏移对齐方式
	CCurveData::m_fOffValue = Gbl_ConfigInfo.m_Other.fOffsetValue;
	CCurveData::m_fOffDelta = Gbl_ConfigInfo.m_Other.lOffsetDelta;
	CCurveData::m_lFDDepth = INVALID_DEPTH;
	BOOL bAlignTop = (Gbl_ConfigInfo.m_Other.AlignTopOffset == 0);
	if (bAlignTop)
	{
		maxOffsetDepth = lLength - minOffsetDepth;
		if (m_logInfo.nDirection == SCROLL_UP)
			m_lDepthOff = maxOffsetDepth ;
	}
	else
	{
		if (m_logInfo.nDirection == SCROLL_DOWN)
			m_lDepthOff = -maxOffsetDepth;
	}
	
	for (i = 0; i < nTools; i++)
	{
		CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
		
		for (int j= 0; j < pULTool->m_Curves.GetSize(); j++)
		{
			CCurve* pCurve = (CCurve*)pULTool->m_Curves.GetAt(j);
			pCurve->m_pData->m_bAlignTopOffset = bAlignTop;
			pCurve->m_pData->m_lAllToolLength = lLength;
			pCurve->m_pData->m_lMaxDepthOffset = maxOffsetDepth;
		}
	}
}

short CULKernel::BeforeCalibrate()
{
	SetWorkMode(WORKMODE_CAL);
	SetAllUseBoard(UL_BOARDBEFOREIO);	
	Sleep(10);
	m_nAllToolState = UL_ALLREADY;

	return UL_NO_ERROR;
}

short CULKernel::AfterCalibrate()
{
	CalcCalGene();
	m_nAllToolState = UL_NOTALLREADY ;

	SetAllUseBoard( UL_BOARDAFTERIO );
	Sleep(50);
	
	for (int i = 0; i < m_arrTools.GetSize(); i++)
	{
		CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
		if ( NULL != pULTool )
		{
			pULTool->SetSysCalCurveState();
			if (pULTool->m_pIParam)
				pULTool->m_pIParam->OnSaveParam();
		}
	}

	SetWorkMode(WORKMODE_IDLE);
	return UL_NO_ERROR;
}

short CULKernel::BeforeVerify()
{
	SetWorkMode(WORKMODE_VERIFY);
	SetAllUseBoard(UL_BOARDBEFOREIO);	
	Sleep(10);
	m_nAllToolState = UL_ALLREADY ;

	return UL_NO_ERROR;
}

short CULKernel::AfterVerify()
{
	m_nAllToolState = UL_NOTALLREADY ;
	SetAllUseBoard(UL_BOARDAFTERIO);
	Sleep(50);

	SetWorkMode(WORKMODE_IDLE);
	return UL_NO_ERROR;
}

BOOL CULKernel::InterpolateFile(LPCTSTR lpszFile)
{
	// 如果插补间隔输入无效，则返回
	if (Gbl_ConfigInfo.m_Record.lInterVal < 1)
	{
		AfxMessageBox("Invalid interpolate parameter!", MB_OK|MB_ICONWARNING);
		return FALSE;
	}

	CString strFile = lpszFile;
	::CopyFile(strFile, (strFile + "bak"), FALSE);
	CULFile* pULFile = new CULFile;
	if (pULFile->Read(lpszFile))
	{
		// 初始化设置
		// ulFile.SetDataBufferSize(Gbl_ConfigInfo.m_Record.Buffer);

		// 使用全局曲线编辑对象
		
		// 调用插补算法
		DATA_EDIT_OPERATE opCode;
		opCode.nOperateCode = INTERPOLATEION;
		opCode.nOperateType = 0; // 整条曲线
		opCode.unionParam1.lParam = Gbl_ConfigInfo.m_Record.lInterVal;

		int nCurve = pULFile->m_vecCurve.size();
		for (int i = 0; i<nCurve; i++)
		{
			CCurve* pCurve = (CCurve*)pULFile->m_vecCurve.at(i);
			// if (pCurve->Dimension() == 1)
			{
				opCode.pCurve = pCurve;
				opCode.pCurveData = pCurve->m_pData;
				Gbl_DataEditPerform.DataEditManager(opCode);
			}
		}
		pULFile->m_bAutoDelete = TRUE;
		pULFile->Save();
		return TRUE;
	}
	
	return FALSE;		
}

// 保存标签信息
void CULKernel::SaveCurveMarks(LPCTSTR pszFile)
{
	CXMLSettings xml(FALSE);
	for (int i = 0; i < m_vecData.size(); i++)
	{
		CCurve* pCurve = m_vecData.at(i)->m_pSrcCurve;
		if (xml.CreateKey(pCurve->m_strSource))
		{
			if (xml.CreateKey(pCurve->m_strName))
			{
				xml.Write("FRDepth", pCurve->m_pData->m_lFRDepth);
				xml.Back();
			}

			xml.Back();
		}
	}

	//
	// 节箍数据保存
	//
#ifdef _PERFORATION
	if ((g_pTask.get() != NULL) && (g_pTask->GetWorkMode() == CPerforation::WORK_SHOOT))
		g_pTask->Serialize(xml);
#endif

	if (xml.CreateKey("CurveBoundsMarks"))
	{
		if (g_pActPrj != NULL && g_pActPrj->m_pService != NULL)
		{
			POSITION pos = g_pActPrj->m_pService->m_sheetiList.GetHeadPosition();
		    int nSheet = 0;
			for ( ; pos != NULL; )
			{
		        CSheetInfo* pInfo = (CSheetInfo*)g_pActPrj->m_pService->m_sheetiList.GetNext(pos);
				if (xml.CreateKey("Sheet%d", nSheet))
				{
					int nTrackCount = pInfo->m_pSheet->m_TrackList.GetSize();
					for (int j = 0; j < nTrackCount; j++)
					{
						CTrack* pTrack = (CTrack*)pInfo->m_pSheet->m_TrackList.GetAt(j);
						if (xml.CreateKey("Track%d", j))
						{
							pTrack->SerializeMark(xml);
							xml.Back();
						}
					}

					xml.Back();
				}
			}
		}
		xml.Back();
	}

	CFile file;
	if (file.Open(pszFile, CFile::modeCreate|CFile::modeWrite))
	{
		BOOL b = xml.WriteXMLToFile(&file);
	}
}

#ifndef _LOGIC

void CULKernel::SaveData()
{
	m_MemPerformFile.SetCurDispDepth();
	m_MemPerformFile.SaveCurPage();
//	m_MemPerformFile.FreeUnusedData(m_nArrivePoint);	
}

void CULKernel::LoggingSave()
{
	m_bLoggingSave = TRUE;
	
	// 设置测井标志
	BOOL bIsLogging = (m_nWorkMode == WORKMODE_LOG);
		
	if (bIsLogging)
		LoggingSaveBegin();
	
	/*
	// 得到深度曲线指针
	m_pDepthCurve = m_pSheet->GetDepthCurve(m_AllCurveList);
	for(int i = 0; i < m_AllCurveList.GetSize(); i++)
	{
	CCurve *pCurve = (CCurve *)m_AllCurveList.GetAt (i);
	pCurve->SetSaveSync(0);
	}
	*/
}

short CULKernel::BeforeLog()
{
#ifdef _LWD
	m_pRTFile = NULL;
#endif

	SetWorkMode(WORKMODE_LOG);
	CProject* pProject = g_pMainWnd->GetCurProject();
	pProject->ClearSerialConfig();	//刷新序列配置
//	if (pProject != NULL && pProject->m_pService != NULL)
//		m_logInfo.nMode = pProject->m_pService->m_nDriveMode;
#ifndef _LWD
	if (g_pMainWnd->m_wndPropertiesBar.GetSafeHwnd())
	{
		g_pMainWnd->m_wndPropertiesBar.EnableItem(idDriveMode, FALSE);
	}
#endif
	m_nArrivePoint = 0;
	m_nRefreshCal = 0;
	m_pDepthData = GetDepthCurve(m_vecData, m_vecStatus);
	if (m_pDepthData == NULL && m_vecData.size())
		m_pDepthData = m_vecData.at(0);

// 	// 存盘
// 	if ( m_bLoggingSave )
// 		LoggingSaveBegin();
// 	m_MemPerformFile.PrePareAllWork(this);
	PrePareLogInfo();
	CULTool::m_lCurDepth = LONG_MAX;
	CULTool::m_pIProject = pProject;
	CULTool::m_lCurDepthIndex = 0;
	////////////////
	for( int i = 0; i < m_arrTools.GetSize(); i++)
	{
		CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);

		if (pULTool)
			pULTool->BeforeLog(&m_logInfo);

		pULTool->m_strSavePath = m_pSaveInfo->strFilePath;
	}

	//Modify by xx 移动存盘代码的位置，以便仪器库中BeforeLog函数中的改动能影响存盘
	// 存盘
	if ( m_bLoggingSave )
		LoggingSaveBegin();
	m_MemPerformFile.PrePareAllWork(this);

#ifdef _DATA_TRACE
	TRACE("-=====- Before log End -=====-\n");
#endif
	
	////////////////
	m_nAllToolState = UL_ALLREADY;
	SetAllUseBoard(UL_BOARDBEFOREIO);
	return UL_NO_ERROR;
}

short CULKernel::AfterLog()
{
#ifdef _LWD
	m_pRTFile = NULL;
#endif
	int i = 0;
	for (i = 0; i < m_arrTools.GetSize(); i++)
	{
		CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
		if (pULTool)
		{
			pULTool->AfterLog();
		}
	}
	m_nAllToolState = UL_NOTALLREADY;

	SetAllUseBoard(UL_BOARDAFTERIO);
	Sleep(50);
	SetWorkMode(WORKMODE_IDLE);
	int nEnd = 0;
	if (Gbl_ConfigInfo.m_Record.bSaveTail)
	{
		if (m_pDepthData && m_pDepthData->m_lMaxDepthOffset)
			nEnd = m_pDepthData->GetSize();
	
	}
	else
	{
		if (m_pDepthData && m_pDepthData->m_lMaxDepthOffset)
			nEnd = - m_pDepthData->GetSize();
	}

	if (nEnd > 1)
	{
		double lDepth0 = m_pDepthData->GetDepth(0);
		double lDepth1 = m_pDepthData->GetDepth(--nEnd);
		double fDelta = fabs(lDepth0 - lDepth1) / nEnd;
		long lTime = m_pDepthData->GetTime(nEnd);
		if (m_logInfo.nDirection == SCROLL_UP)
		{
			long lDepth = CCurveData::GetDepthEDMin(m_vecData, lDepth1);
			// 填充结束部分
			while (lDepth1 > lDepth)
			{
				lDepth1 -= fDelta;
				m_pDepthData->Add((long)(lDepth1 + .5), DTM(lDepth1), lTime);
			}
		}
		else
		{
			long lDepth = CCurveData::GetDepthEDMax(m_vecData, lDepth1);
			
			// 填充结束部分
			while (lDepth1 < lDepth)
			{
				lDepth1 += fDelta;
				m_pDepthData->Add((long)(lDepth1 + .5), DTM(lDepth1), lTime);
			}
		}
	}
	else if (nEnd < 0)
	{
		nEnd = (-nEnd) - 1;
		double lDepth0 = m_pDepthData->GetDepth(0);
		double lDepth1 = m_pDepthData->GetDepth(nEnd);
		double fDelta = fabs(lDepth0 - lDepth1) / nEnd;
		long lTime = m_pDepthData->GetTime(nEnd);
		int nCount = 0;
		long lDepth;
		if (m_logInfo.nDirection == SCROLL_UP)
		{
			lDepth = CCurveData::GetDepthEDMax(m_vecData, lDepth1);

			// 去除结束部分
			while (lDepth1 < lDepth)
			{
				lDepth1 += fDelta;
				nCount++;
			}
		}
		else
		{
			lDepth = CCurveData::GetDepthEDMin(m_vecData, lDepth1);

			// 去除结束部分
			while (lDepth1 > lDepth)
			{
				lDepth1 -= fDelta;
				nCount++;
			}
		}
		if (nCount)
		{
			m_pDepthData->Delete(nEnd - nCount + 1, nCount);
		}
		lDepth = m_pDepthData->GetDepthED();
		int nCurves = m_vecData.size();
		if (m_logInfo.nDirection == SCROLL_UP)
		{
			for (i = 0; i < nCurves; i++)
			{
				CCurveData* pData = m_vecData.at(i);
				if (pData == m_pDepthData)
					continue;
				
				int nSize = pData->GetSize() - 1;
				if (nSize < 0)
					continue;
				
				long lDepth1 = pData->GetDepth(nSize);
				while (lDepth > lDepth1)
				{
					pData->m_arrData.RemoveAt(nSize--);
					if (nSize < 0)
						break;
					
					lDepth1 = pData->GetDepth(nSize);
				}
			}
		}
		else
		{
			for (i = 0; i < nCurves; i++)
			{
				CCurveData* pData = m_vecData.at(i);
				if (pData == m_pDepthData)
					continue;
				
				int nSize = pData->GetSize() - 1;
				if (nSize < 0)
					continue;
				
				long lDepth1 = pData->GetDepth(nSize);
				while (lDepth < lDepth1)
				{
					pData->m_arrData.RemoveAt(nSize--);
					if (nSize < 0)
						break;
					
					lDepth1 = pData->GetDepth(nSize);
				}
			}
		}
	}
	// 对未存储数据的处理	
	m_MemPerformFile.AfterAllWork();
	m_MemPerformFile.DataPerformAll();
	if (m_bLoggingSave)
		LoggingSaveEnd();
	if (Gbl_ConfigInfo.m_Record.bSaveTail)
	{
		if (m_pDepthData && m_pDepthData->m_lMaxDepthOffset)
			nEnd = m_pDepthData->GetSize();

		if (nEnd > 0)
		{
			double lDepth0 = m_pDepthData->GetDepth(0);
			double lDepth1 = m_pDepthData->GetDepth(--nEnd);
			double fDelta = fabs(lDepth0 - lDepth1) / nEnd;
			long lTime = m_pDepthData->GetTime(nEnd);		
			
			int nCurves = m_vecData.size();
			for (i = 0; i < nCurves; i++)
			{
				CCurveData* pData = (CCurveData*)m_vecData.at(i);
				if (pData == m_pDepthData)
					continue;

				int nSize = pData->GetSize();
				long lDepth = pData->GetDepth (nSize - 1);
				long lToolOffset = pData->m_lToolOffset;
				long lCurveOffset = pData->m_lDepthOffset;
				void* pValue = pData->GetValuePtr(nSize - 1);
				if (pValue == NULL)
					continue;

				if (m_logInfo.nDirection == SCROLL_UP)
				{
					// 填充结束部分
					while (lDepth > lDepth1 )
					{
						lDepth -= fDelta;
						pData->Add(lDepth - pData ->m_lCalcOffset, pValue, lTime);
					}
				}
				else
				{
					// 填充结束部分
					while (lDepth < lDepth1)
					{
						lDepth += fDelta;
						pData->Add(lDepth - pData ->m_lCalcOffset, pValue, lTime);
					}
				}
			}
		}
	}
	m_nArrivePoint = 0;
	if (m_bLockCheck ==TRUE)
	{
		m_Cs.Unlock();
		m_bLockCheck =FALSE;
	}
//	m_Cs.Lock();
	m_hSendArray.RemoveAll();
//	m_Cs.Unlock();
	m_MemPerformFile.m_vecGraph.clear();
	// 判断是否进行插补
	if (Gbl_ConfigInfo.m_Record.bInterpolation)
	{
		int nCount = m_filePathArray.GetSize();
		for (int i = 0; i < nCount; i++)
		{
			CString curFilePath = m_filePathArray.GetAt(0);
			m_filePathArray.RemoveAt(0);
			InterpolateFile(curFilePath);
		}
	}
#ifndef _LWD
	if (g_pMainWnd->m_wndPropertiesBar.GetSafeHwnd())
	{
		g_pMainWnd->m_wndPropertiesBar.EnableItem(idDriveMode, TRUE);
	}
#else
	if(!g_pMainWnd->m_bLogFlash)
	{
		//SK 定时重启测井功能 测试时会有崩溃错误，原因不明，从错误信息判断为窗口无效
/*		if (g_pMainWnd->m_wndPropertiesBar.GetSafeHwnd())
		{
			g_pMainWnd->m_wndPropertiesBar.EnableItem(idDriveMode, TRUE);
		}*/
	}
	else
	{
		
	}

#endif
	return UL_NO_ERROR;
}

void CULKernel::DispatchData()
{
	m_nArrivePoint++;
	
	// SendMessage to display, print, save
	CULTool* pULTool = (CULTool*)m_arrTools.GetAt(0);
	if (pULTool == NULL) 
		return ;

	// 通知仪器当前测井信息
	m_lDepth = 0;
	m_lTime = 0;

	if (m_pDepthData==NULL)
		return ;
	
	int nLen = m_pDepthData->GetSize();
	if (nLen < 1)
		return ;
	
	if (!CanRefresh())
	{
		if (g_pMainWnd->m_nLog & LOG_ALLWATCH)
		{
			if (m_bLoggingSave)
			{
				if (Gbl_ConfigInfo.m_Record.RawData)
					SaveRawData();
			//	for(int i=0; i<2; i++)
					SaveData();
			}
		}
		return;
	}
	m_lDepth = m_pDepthData->GetDepth(nLen-1);
	m_lTime = m_pDepthData->GetTime(nLen-1);
//	m_lTime = ((m_lTime*1.0)/1000 - 100) * 10000;
	
//	m_Cs.Lock();
//	m_bLockCheck =TRUE;
	HWND hWndSend = NULL;
	for (int i = 0; i < m_hSendArray.GetSize(); i++) 
	{
		hWndSend = m_hSendArray.GetAt(i);
		if (::IsWindow (hWndSend))
			::SendMessage(hWndSend, m_uMessageID, (WPARAM)&m_lDepth, (LPARAM)&m_lTime); 
	}	
//	m_Cs.Unlock();
	m_bLockCheck =FALSE;
}

void CULKernel::LoggingSaveBegin()
{
	if (m_pRawFile->m_bFileIsOpen)
		return ;

	CCurve::SetRTSave(TRUE);
	CULTool::SetSavRawData(Gbl_ConfigInfo.m_Record.RawData);
	SaveFileHeaderToDisk(m_pRawFile->m_pProject, m_pSaveInfo->strDefaultPath);
	m_MemPerformFile.m_bSave = TRUE;
}

void CULKernel::SaveFileHeaderToDisk(CProject* pProject, CString strFileName)
{
	if (Gbl_ConfigInfo.m_Record.RawData)
	{
		//Modify by xx 2016-01-11
		//原始文件文件名添加创建时间信息
		CString strTime;
		SYSTEMTIME time;
		GetLocalTime(&time);
		strTime.Format(_T("(%02d-%02d-%02d-%02d-%02d-%02d)"),time.wYear%100,time.wMonth,time.wDay,time.wHour,time.wMinute,time.wSecond);
		if (m_pRawFile->Open(strFileName + strTime + ".raw"))  
		{
			// memcpy(m_pRawFile->m_pWellProjectInfo, &pProject->m_NewProjectInfo, sizeof(WELLPROJECTINFO));
			m_pRawFile->SaveHead();
		}
		else
		{
			AfxMessageBox(IDS_ERR_OPENFILE);
		}
	}
	
	if (Gbl_ConfigInfo.m_Record.Engineer)
	{
		// 创建工程文件
		CString strFileFormat;

		int iCount = Gbl_ConfigInfo.m_fileState.GetSize();
		
		// 确定当前工程文件链表为空
		if (m_vecULFile.size() != 0)
			ClearULFiles();

		m_filePathArray.RemoveAll();
		
		CString strScout = pProject->GetScout();
		// for (int i = 0; i < iCount; i++)
		{
#ifdef _PERFORATION
			if ((g_pTask.get() != NULL) && (g_pTask->GetWorkMode() == CPerforation::WORK_SHOOT))
				strFileFormat = "PEF"; // Gbl_ConfigInfo.m_fileState.GetAt(i);
			else
#endif
				strFileFormat = "AXP"; // Gbl_ConfigInfo.m_fileState.GetAt(i);

			CULFile* pULFile = new CULFile();
			CString strFile = strFileName;
			if (pULFile->Create(strFileFormat, strFile))
			{	
				pULFile->SetPathName(strFile, FALSE);
				m_filePathArray.Add(strFile);
				pULFile->SetTime(pProject->m_pService->m_tmLogging.GetTime(), FT_CRTMDF);
				pULFile->SetScout(strScout);
				pULFile->SaveFileHead(pProject, pProject->m_pService);
				pULFile->SaveDataBegin(this);
				m_vecULFile.push_back(pULFile);

#ifdef _LWD
				m_pRTFile = pULFile;
#endif
			}
			
			strFileFormat = "ALD";
			pULFile = new CULFile();
			strFile = strFileName;
			if (pULFile->Create(strFileFormat, strFile))
			{	
				pULFile->SetPathName(strFile, FALSE);
				m_filePathArray.Add(strFile);
				pULFile->SetTime(pProject->m_pService->m_tmLogging.GetTime(), FT_CRTMDF);
				pULFile->SetScout(strScout);
				pULFile->SaveFileHead(pProject, pProject->m_pService);
				pULFile->SaveDataBegin(this);
				m_vecULFile.push_back(pULFile);
			}

/*#ifdef _LWD
			strFileFormat = "mdf";
			pULFile = new CULFile();
			strFile = strFileName;
			if (pULFile->Create(strFileFormat, strFile))
			{	
				pULFile->SetPathName(strFile, FALSE);
				m_filePathArray.Add(strFile);
				pULFile->SetTime(pProject->m_pService->m_tmLogging.GetTime(), FT_CRTMDF);
				pULFile->SetScout(strScout);
				pULFile->SaveFileHead(pProject, pProject->m_pService);
				pULFile->SaveDataBegin(this);
				m_vecULFile.push_back(pULFile);
			}
#endif*/

		}	
	}
}

void CULKernel::LoggingSaveEnd()
{
	if (!m_bLoggingSave)
		return ;

	CCurve::SetRTSave(FALSE);
	CULTool::SetSavRawData(FALSE);

	m_bLoggingSave = FALSE;
	m_MemPerformFile.m_bSave = FALSE;
	
	if (Gbl_ConfigInfo.m_Record.Engineer)
	{	
		// 保存缓存中测井停止时没有保存的数据
		CString strMarks = Gbl_AppPath + "\\Temp\\cmark.tmp";
		SaveCurveMarks(strMarks);
		ClearULFiles(strMarks);
	}
	
	if (Gbl_ConfigInfo.m_Record.RawData)
	{
		if (m_pRawFile != NULL)
		{
			// 保存缓存中测井停止时没有保存的数据
			CleanRawBuffer();
			m_pRawFile->Close();
		}		
	}	
	
	// 清除状态栏上的存盘信息
	g_pMainWnd->SendMessage(UM_DATA_SAVE, 5);
}

void CULKernel::PrepareReadBuffer(CULTool* pTool)
{
	int nBufferSize = pTool->GetAllChannelSize();
	IOBuffer buf;
	if (m_pRawFile != NULL && m_pRawFile->m_fVersion > 2.300f)
	{
		buf.pLength = new int(nBufferSize + sizeof(long) + sizeof(long) + sizeof(long));
	}
	else
		buf.pLength = new int(nBufferSize + sizeof(long));
	buf.pBuffer = (BYTE*)new BYTE[*buf.pLength];
	buf.nChannelCount = pTool->m_Channels.GetSize();
	buf.pChannelLength = NULL;
	if (buf.nChannelCount > 0)
	{
		buf.pChannelLength = new int[buf.nChannelCount];
		for (int j = 0; j < buf.nChannelCount; j++)
		{
			buf.pChannelLength[j] = pTool->GetChannelLength(j);
		}
	}
	pTool->m_BufReadArray.Add(buf);
}

void CULKernel::PrepareToolBuffer(CULTool* pTool)
{
	int nBufferSize = pTool->GetAllChannelSize();
	IOBuffer buf;
	if (m_pRawFile != NULL && m_pRawFile->m_fVersion > 2.300f)
	{
		buf.pLength = new int(nBufferSize + sizeof(long) + sizeof(long) + sizeof(long));
	}
	else
		buf.pLength = new int(nBufferSize + sizeof(long));
	buf.pBuffer = (BYTE*)new BYTE[*buf.pLength];
	buf.nChannelCount = pTool->m_Channels.GetSize();
	buf.pChannelLength = NULL;
	if (buf.nChannelCount > 0)
	{
		buf.pChannelLength = new int[buf.nChannelCount];
		for (int j = 0; j < buf.nChannelCount; j++)
		{
			buf.pChannelLength[j] = pTool->GetChannelLength(j);
		}
	}
	pTool->m_BufferArray.Add(buf);
}

short CULKernel::BeforeReplay()
{
	SetWorkMode(WORKMODE_COMPUTEPLAYBACK);
	m_nArrivePoint = 0;
	m_nRefreshCal = 0;
	m_pDepthData = GetDepthCurve(m_vecData, m_vecStatus);
	if (m_pDepthData == NULL && m_vecData.size())
		m_pDepthData = m_vecData.at(0);

	int i = 0;
/*	for (; i < m_vecData.size(); i++)
	{
		m_vecData.at(i)->Clear();
	}
*/
	CProject* pProject = g_pMainWnd->GetCurProject();
	pProject->ClearSerialConfig();	//刷新序列配置
	CULTool::m_pIProject = pProject;
	
	PrePareLogInfo();
	////////////////

	int nTools = m_arrTools.GetSize();
	CULTool* pTool = NULL;
	if (Gbl_ConfigInfo.m_System.RelogMode)
	{
		for (i = 0; i < nTools; i++)
		{
			CULTool* pTool = (CULTool*)m_arrTools.GetAt(i);
			if (pTool)
				pTool->BeforeLog(&m_logInfo);
		}
	}
	else
	{
		for (i = 0; i < nTools; i++)
		{
			CULTool* pTool = (CULTool*)m_arrTools.GetAt(i);
			if (pTool)
				pTool->BeforeReplay();
		}
	}

	////////////////
	m_nAllToolState = UL_ALLREADY;

	//m_strToolName = m_pRawFile->GetFirstToolName();
	m_nToolIndex =  m_pRawFile->GetFirstToolIndex();

	for (i = 0; i <nTools; i++)
	{
		pTool = (CULTool*)m_arrTools.GetAt(i);
		pTool->m_nUseCount = 0;
		m_mapTool[pTool->strToolName] = pTool;
		if (pTool->m_BufReadArray.GetSize() <= 0)
		{
			PrepareReadBuffer(pTool);//modify by gj20130627 
		}
	}

	SaveParamFirst();
	return UL_NO_ERROR;
}

BOOL CULKernel::SaveRawData ()
{
#ifdef _LWD
	if(m_pRawFile->NeedSaveNewFile())
		m_pRawFile->SaveNewFile();
#endif
	
	TCHAR szMark[256];
	ZeroMemory(szMark, 256);
	DATAHEAD DataHead;
	ZeroMemory(&DataHead, sizeof(DataHead));
	CULTool* pULTool = NULL;
	int i = 0;
	for (i = 0; i < m_arrTools.GetSize(); i++)
	{
		pULTool = (CULTool*)m_arrTools.GetAt(i);
		pULTool->m_CS.Lock();
		DataHead.m_nToolCount += pULTool->m_nUseCount;
		int nChannels = pULTool->m_Channels.GetSize();
		DataHead.m_nDataSize = 2;
		int i = 0;
		for (i = 0; i < pULTool->m_nUseCount; i++)
		{
			IOBuffer data = pULTool->m_BufferArray.GetAt(i);
			if (m_pRawFile != NULL && m_pRawFile->m_fVersion > 2.300f)
			{
				DataHead.m_nDataSize += (*data.pLength - sizeof(long) - sizeof(long) - sizeof(long));
			} 
			else
			{
				DataHead.m_nDataSize += (*data.pLength - sizeof(long));
			}
		}
	
		CString str;	
		str = pULTool->strENName; 
		if (str == "DEPT" || str == "DEPTH")
			DataHead.m_lDepth = pULTool->GetCurDepth();
	}

#ifdef _SAVE_TRACE
	TRACE("R << %ld\n", DataHead.m_lDepth);
#endif

	if (DataHead.m_nToolCount < 1)
	{
		for (i = 0; i < m_arrTools.GetSize(); i++)
		{
			pULTool = (CULTool*)m_arrTools.GetAt(i);
			pULTool->m_CS.Unlock();
		}
		return FALSE;
	}

	DATAHEADEX DataHeadEx;
	ZeroMemory(&DataHeadEx, sizeof(DATAHEADEX));
	COleDateTime oleTime(CProject::m_sysCurTime);
	DataHeadEx.m_oleTime = oleTime;	//数据帧时间

	m_pRawFile->SaveDataHead(&DataHead, &DataHeadEx, szMark);
	SaveParams(DataHead.m_lDepth, m_MemPerformFile.GetParamFile(), &m_pRawFile->m_File);

	for (i = 0; i < m_arrTools.GetSize(); i++)
	{
		pULTool = (CULTool*)m_arrTools.GetAt(i);
		if (pULTool->m_nUseCount <= 0 )
		{
			pULTool->m_CS.Unlock();
			continue ;
		}
		
		/* Test the max buffer count needed.
		if ( pTool->m_nUseCount >= nSizeSave )
		{
		nSizeSave = pTool->m_nUseCount;	
		}
		if ( pTool->m_nUseCount >= 1 )
		{	 
		TRACE("最大需要 : %d个缓冲块！\n", nSizeSave );
		TRACE1( pTool->strToolName + "--WARNING : 有%d个缓冲块！\n", pTool->m_nUseCount );
	}*/
		
		for (int j = 0; j < pULTool->m_nUseCount; j++)
			m_pRawFile->SaveData(i, pULTool, pULTool->m_BufferArray.GetAt(j));
		
		pULTool->m_nUseCount = 0;
		pULTool->m_CS.Unlock();
	}
	
	return TRUE;
}

// 原有计算回放有可能会造成深度不精确
// 原因是一次保存了某个仪器库的多帧数据
// 下面的代码修复了不精确的问题
#define __ReplayCorrectDepth
#ifdef __ReplayCorrectDepth
BOOL CULKernel::ReplayRawFileOnce()
{
	CULTool* pTool = NULL;
	if (m_nToolIndex < 0)
		return TRUE;
	CWaitCursor wcs;
	//m_mapTool.Lookup(m_strToolName, (void*&)pTool);
	pTool = (CULTool*)m_arrTools.GetAt(m_nToolIndex);
	while (TRUE)
	{

		while (TRUE)
		{
			pTool = (CULTool*)m_arrTools.GetAt(m_nToolIndex);
			if (pTool->m_nUseCount >= pTool->m_BufferArray.GetSize())
				PrepareToolBuffer(pTool);
			int nResult = m_pRawFile->GetData(
				&pTool->m_BufferArray[pTool->m_nUseCount], 
				m_nToolIndex);	 // 对一次LogIO数据
		
			if (nResult > -3) // 通道中的数据长度不为零
			{
				pTool->m_nUseCount++;
			}
			if (nResult == -1 || nResult == -4) // 回放结束
			{
				return TRUE;
			}
			if (nResult == -2 || nResult == -5) // 一幁结束
			{
				break;
			}
		}

		//Add by zy 2011 9 24
		//修正在计算回放时，由于深度仪器数据有时会分成两帧保存，导致各通道数据错位，深度出现错误点，重新另存AXP，错误点以后部分会变成直线。
		//以下代码不是最优方法
		pTool = (CULTool*)m_arrTools.GetAt(0);
		
		if(pTool->m_nUseCount > 1)
		{
			int nBufferSize = pTool->GetAllChannelSize();
			IOBuffer buf = pTool->m_BufferArray.GetAt(0);//按照最大长度分配缓冲
			if(nBufferSize < *buf.pLength)
				nBufferSize = *buf.pLength;

			BYTE *pBuff = new BYTE[nBufferSize]; 
			int nChannelCount = pTool->m_Channels.GetSize();
			int *pChannelLength = new int[nChannelCount];

			for(int i = 0 ; i < pTool->m_nUseCount ; i++)
			{
				int nOffset = 0;
				int nBufOffset = 0;
				int nBufCmpOffset = 0;
				BOOL bUpdate = FALSE;
				IOBuffer buf = pTool->m_BufferArray.GetAt(i);
				IOBuffer bufCmp;
				if(i == 0)
					bufCmp = pTool->m_BufferArray.GetAt(1);
				else
					bufCmp = pTool->m_BufferArray.GetAt(i - 1);

				for(int j = 0 ; j < nChannelCount ; j++)
				{
					if(buf.pChannelLength[j] == 0)
					{
						memcpy(&pBuff[nOffset] , &bufCmp.pBuffer[nBufCmpOffset] , bufCmp.pChannelLength[j]);
						pChannelLength[j] = bufCmp.pChannelLength[j];
						bUpdate = TRUE;
					}
					else
					{
						memcpy(&pBuff[nOffset] , &buf.pBuffer[nBufOffset] , buf.pChannelLength[j]);
						pChannelLength[j] = buf.pChannelLength[j];
					}
					nOffset += pChannelLength[j];
					nBufOffset += buf.pChannelLength[j];
					nBufCmpOffset += bufCmp.pChannelLength[j];
				}

				if(bUpdate)
				{
					memcpy(buf.pBuffer , pBuff , nOffset);
					memcpy(buf.pChannelLength , pChannelLength , sizeof(int) * nChannelCount);
					if (m_pRawFile != NULL && m_pRawFile->m_fVersion > 2.300f)
					{
						*buf.pLength = nOffset + sizeof(long) + sizeof(long) + sizeof(long);
					} 
					else
					{
						*buf.pLength = nOffset + sizeof(long);
					}
				}
			}

			delete []pBuff;
			delete []pChannelLength;
		}
		
		//End		

		while (CanReplayDispatchData())
		{
			int n = m_arrTools.GetSize();
			for (int i = 0; i < n; i++)
			{
				CULTool* pTool = (CULTool*)m_arrTools.GetAt(i);
				CToolInfoPack* pToolInfo = 
					(CToolInfoPack*)m_pRawFile->m_ToolInfoPackList.GetAt(i);
				if (pToolInfo->m_Tool.nBufferSize > 0)
				{
					pTool->LogIO();
					pTool->m_nUseCount--;
					IOBuffer buf = pTool->m_BufferArray.GetAt(0);
					delete buf.pBuffer;
					if (buf.pChannelLength)
						delete buf.pChannelLength;
					delete buf.pLength;
					pTool->m_BufferArray.RemoveAt(0);
				}
			}
		}
	}
	return TRUE;
}

BOOL CULKernel::CanReplayDispatchData()
{
	int n = m_arrTools.GetSize();
	for (int i = 0; i < n; i++)
	{
		CULTool* pTool = (CULTool*)m_arrTools.GetAt(i);
		CToolInfoPack* pToolInfo = 
			(CToolInfoPack*)m_pRawFile->m_ToolInfoPackList.GetAt(i);
		if (pTool->m_nUseCount == 0 && pToolInfo->m_Tool.nBufferSize > 0)
			return FALSE;
	}
	return TRUE;
}

// Return is finish : TRUE, or not : FALSE
BOOL CULKernel::ReplayRawFile()
{
	if (m_nToolIndex < 0)
		return TRUE;

	int nTools = m_arrTools.GetSize();
	
	CULTool* pTool = NULL;
	BOOL bReadFinish = FALSE;

	CMainFrame* pMainFrame = (CMainFrame*) theApp.m_pMainWnd;

	while (TRUE)
	{
		pTool = (CULTool*)m_arrTools.GetAt(m_nToolIndex);

		if (pTool->m_nUseCount >= pTool->m_BufferArray.GetSize())
			PrepareToolBuffer(pTool);
		int nResult = m_pRawFile->GetData(
			&pTool->m_BufferArray[pTool->m_nUseCount], 
			m_nToolIndex);	 // 对一次LogIO数据
	
		if (nResult > -3) // 通道中的数据长度不为零
		{
			pTool->m_nUseCount++;
		}
		if (nResult == -1 || nResult == -4) // 回放结束
		{
			bReadFinish = TRUE;
			break;
		}
		if (nResult == -2 || nResult == -5) // 一幁结束
		{
			break;
		}
	}

	ReplayRawCheckBuff();
	
	//End
	while (CanReplayDispatchData())
	{
		int n = m_arrTools.GetSize();
		for (int i = 0; i < n; i++)
		{
			CULTool* pTool = (CULTool*)m_arrTools.GetAt(i);
			CToolInfoPack* pToolInfo = 
				(CToolInfoPack*)m_pRawFile->m_ToolInfoPackList.GetAt(i);
			if (pToolInfo->m_Tool.nBufferSize > 0)
			{
				pTool->LogIO();
				pTool->m_nUseCount--;
				IOBuffer buf = pTool->m_BufferArray.GetAt(0);
				delete buf.pBuffer;
				if (buf.pChannelLength)
					delete buf.pChannelLength;
				delete buf.pLength;
				pTool->m_BufferArray.RemoveAt(0);
			}
		}
		DispatchData();
	}

	//add by gj 20121228 回放进度条显示
	if (m_bFirstReplay)
	{
		m_dwReplayLength = m_pRawFile->m_File.GetLength();
		m_dwHeadLength = m_pRawFile->m_File.GetPosition();
		pMainFrame->SendMessage(UM_REPLAY);
		m_bFirstReplay = FALSE;
	}//
	m_dwCurrLength = m_pRawFile->m_File.GetPosition();
	pMainFrame->SendMessage(UM_REPLAY,1,m_dwCurrLength - m_dwHeadLength);

	//Add by xx 2016-10-13	回放时间显示

	return bReadFinish;
}

BOOL CULKernel::ReplayRawCheckBuff()
{
	//Add by zy 2011 9 24
	//修正在计算回放时，由于深度仪器数据有时会分成两帧保存，导致各通道数据错位，深度出现错误点，重新另存AXP，错误点以后部分会变成直线。
	//以下代码不是最优方法
	CULTool *pTool = (CULTool*)m_arrTools.GetAt(0);

	if(pTool->m_nUseCount > 1)
	{
		int nBufferSize = pTool->GetAllChannelSize();
		IOBuffer buf = pTool->m_BufferArray.GetAt(0);//按照最大长度分配缓冲
		if(nBufferSize < *buf.pLength)
			nBufferSize = *buf.pLength;

		BYTE *pBuff = new BYTE[nBufferSize]; 
		int nChannelCount = pTool->m_Channels.GetSize();
		int *pChannelLength = new int[nChannelCount];

		for(int i = 0 ; i < pTool->m_nUseCount ; i++)
		{
			int nOffset = 0;
			int nBufOffset = 0;
			int nBufCmpOffset = 0;
			BOOL bUpdate = FALSE;
			IOBuffer buf = pTool->m_BufferArray.GetAt(i);
			IOBuffer bufCmp;
			if(i == 0)
				bufCmp = pTool->m_BufferArray.GetAt(1);
			else
				bufCmp = pTool->m_BufferArray.GetAt(i - 1);

			for(int j = 0 ; j < nChannelCount ; j++)
			{
				if(buf.pChannelLength[j] == 0)
				{
					memcpy(&pBuff[nOffset] , &bufCmp.pBuffer[nBufCmpOffset] , bufCmp.pChannelLength[j]);
					pChannelLength[j] = bufCmp.pChannelLength[j];
					bUpdate = TRUE;
				}
				else
				{
					memcpy(&pBuff[nOffset] , &buf.pBuffer[nBufOffset] , buf.pChannelLength[j]);
					pChannelLength[j] = buf.pChannelLength[j];
				}
				nOffset += pChannelLength[j];
				nBufOffset += buf.pChannelLength[j];
				nBufCmpOffset += bufCmp.pChannelLength[j];
			}

			if(bUpdate)
			{
				memcpy(buf.pBuffer , pBuff , nOffset);
				memcpy(buf.pChannelLength , pChannelLength , sizeof(int) * nChannelCount);
				if (m_pRawFile != NULL && m_pRawFile->m_fVersion > 2.300f)
				{
					*buf.pLength = nOffset + sizeof(long) + sizeof(long) + sizeof(long);
				} 
				else
				{
					*buf.pLength = nOffset + sizeof(long);
				}
			}
		}

		delete []pBuff;
		delete []pChannelLength;
	}

	return TRUE;
}

BOOL CULKernel::ReplayRawFileReadFile()
{
	if (m_nToolIndex < 0)
		return TRUE;

	int nTools = m_arrTools.GetSize();
	
	CULTool* pTool = NULL;
	m_bReadFinish = FALSE;
	CMainFrame* pMainFrame = (CMainFrame*) theApp.m_pMainWnd;

	while (TRUE)
	{
		pTool = (CULTool*)m_arrTools.GetAt(m_nToolIndex);

		if (pTool->m_nReadUseCount >= pTool->m_BufReadArray.GetSize())
		{
			PrepareReadBuffer(pTool);
		}
		int nResult = m_pRawFile->GetData(
			&(pTool->m_BufReadArray[pTool->m_nReadUseCount]), 
			m_nToolIndex);	 // 对一次LogIO数据
	
		if (nResult > -3) // 通道中的数据长度不为零
		{
			pTool->m_nReadUseCount++;
		}
		if (nResult == -1 || nResult == -4) // 回放结束
		{
			m_bReadFinish = TRUE;
			break;
		}
		if (nResult == -2 || nResult == -5) // 一幁结束
		{
			break;
		}
		if (nResult == -100)	//通道中未存入数据 继续
		{
			continue;
		}
	}

	//回放进度条显示
	if (m_bFirstReplay)
	{
// 		m_dwReplayLength = m_pRawFile->m_File.GetLength();
// 		m_dwHeadLength = m_pRawFile->m_File.GetPosition();
		m_dwReplayLength = m_pRawFile->GetReplayLen();
 		m_dwHeadLength = m_pRawFile->GetReplayHeadPos();
		pMainFrame->SendMessage(UM_REPLAY);
		m_bFirstReplay = FALSE;
	}
	m_dwCurrLength = m_pRawFile->m_File.GetPosition();
	pMainFrame->SendMessage(UM_REPLAY,1,m_dwCurrLength - m_dwHeadLength);

	//Add by xx 2016-10-13 回放时间显示
	SYSTEMTIME sysTime = CProject::m_sysCurTime;
	if (sysTime.wYear > 0)
	{
		pMainFrame->SendMessage(UM_REPLAY, 3, (DWORD)&sysTime);
	}


	return m_bReadFinish;
}

BOOL CULKernel::ReplayRawFileCompute(DWORD m_uiReplayRate)
{
	if (m_nToolIndex < 0)
		return TRUE;
	m_bWriteFinish = FALSE;

	if (m_bReadFinish)
		m_bWriteFinish = TRUE;

	int nTools = m_arrTools.GetSize();
	CMainFrame* pMainFrame = (CMainFrame*) theApp.m_pMainWnd;
	m_RefreshCount++;

	//深度计算
	CULTool* pTool = (CULTool*)m_arrTools.GetAt(0);
	CToolInfoPack* pToolInfo = (CToolInfoPack*)m_pRawFile->m_ToolInfoPackList.GetAt(0);
	
	if ((pToolInfo->m_Tool.nBufferSize > 0) && (pTool->m_BufferArray.GetSize() > 0))
	{
		pTool->LogIO();
		pTool->m_nUseCount--;
		IOBuffer buf = pTool->m_BufferArray.GetAt(0);

		// 得到测井时记录的索引号
		if (g_pActPrj->m_ULKernel.m_pRawFile->m_fVersion > 2.300f)
		{
			int nSize = *buf.pLength - 4; 
			long lIndex = 0;
			BYTE* pBuf = (BYTE*)buf.pBuffer;
			memcpy(&lIndex, (pBuf + nSize), sizeof(long));
			CULTool::m_lCurDepthIndex = lIndex;
		}
		// end

		delete buf.pBuffer;
		if (buf.pChannelLength)
			delete buf.pChannelLength;
		delete buf.pLength;
		pTool->m_BufferArray.RemoveAt(0);	
	}

	for (int i = 0;i < nTools-1; i++)
	{

		::SetEvent(DepthEvent[i]);
	}

	if(nTools <= 1)
	{
		if (Gbl_ConfigInfo.m_System.nReplayDrawCurve) // add by bao 可以控制是否显示曲线
		{
			DispatchData();
		}
	}
	else
	{
	//	DWORD dw = WaitForMultipleObjects(nTools-1,ToolEvent,TRUE,2000);//等待其他仪器计算完毕
		DWORD dw = WaitForMultipleObjects(nTools-1,ToolEvent,TRUE,INFINITE);//等待其他仪器计算完毕

		switch (dw)	
		{
		case WAIT_OBJECT_0:
			if (m_uiReplayRate * m_RefreshCount > 10)
			{
				m_RefreshCount = 0;
				if (Gbl_ConfigInfo.m_System.nReplayDrawCurve) // add by bao 可以控制是否显示曲线
				{
					DispatchData();
				}
			}

			break;
	
		case WAIT_TIMEOUT:

			break;
			
		case WAIT_FAILED:
			//函数调用失败，比如传递了一个无效的句柄
			break;
		}			
	}

	return m_bWriteFinish;
}

#else
BOOL CULKernel::ReplayRawFileOnce()
{
	CULTool* pTool = NULL;
	if (m_nToolIndex < 0)
		return TRUE;
	CWaitCursor wcs;
	//m_mapTool.Lookup(m_strToolName, (void*&)pTool);
	pTool = (CULTool*)m_arrTools.GetAt(m_nToolIndex);
	while (pTool != NULL)
	{		
		int nResult = m_pRawFile->GetData(&pTool->m_BufferArray[0], m_nToolIndex); // 对一次LogIO数据
		if (nResult == -1 || nResult == -4)
			return TRUE;
		if (nResult > -3)
		pTool->LogIO();
		//m_mapTool.Lookup(m_strToolName, (void*&)pTool);
	    pTool = (CULTool*)m_arrTools.GetAt(m_nToolIndex);
	}
	
	return TRUE;
}

BOOL CULKernel::ReplayRawFile()
{
	if (m_nToolIndex < 0)
		return TRUE;

	int nTools = m_arrTools.GetSize();
	
	CULTool* pTool = NULL;
	while (TRUE)
	{
		pTool = (CULTool*)m_arrTools.GetAt(m_nToolIndex);
		int nResult = m_pRawFile->GetData(&pTool->m_BufferArray[0], m_nToolIndex);	 	 // 对一次LogIO数据
	
		if (nResult > -3)  // 通道中的数据长度不为零
			pTool->LogIO();
		if (nResult == -1 || nResult == -4) // 回放结束
			return TRUE;
		
		// DispatchData();
		if (nResult == -2 || nResult == -5) // 一幁结束
		{
			break;
		}
	}
	DispatchData();

	return FALSE;
}

#endif

short CULKernel::AfterReplay()
{
	SetWorkMode(WORKMODE_IDLE);
	m_nArrivePoint = 0;
	
	int nTools = m_arrTools.GetSize();
	if (Gbl_ConfigInfo.m_System.RelogMode)
	{
		for (int i = 0; i < nTools; i++)
		{
			CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
			if (pULTool)
				pULTool->AfterLog();
		}
	}
	else
	{
		for (int i = 0; i < nTools; i++)
		{
			CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
			if (pULTool)
				pULTool->AfterReplay();
		}
	}

	if (m_MemPerformFile.m_nTempFileOpen & 2)
	{
		CFile* pFile = m_MemPerformFile.GetParamFile();
		if (pFile != NULL)
			pFile->Flush();
	}

	int nEnd = 0;
	if (Gbl_ConfigInfo.m_Record.bSaveTail)
	{
		if (m_pDepthData && m_pDepthData->m_lMaxDepthOffset)
			nEnd = m_pDepthData->GetSize();
	}
	else
	{
		if (m_pDepthData && m_pDepthData->m_lMaxDepthOffset)
			nEnd = - m_pDepthData->GetSize();
	}
	if (nEnd > 0)
	{
		double lDepth0 = m_pDepthData->GetDepth(0);
		double lDepth1 = m_pDepthData->GetDepth(--nEnd);
		double fDelta = fabs(lDepth0 - lDepth1) / nEnd;
		long lTime = m_pDepthData->GetTime(nEnd);
		if (m_logInfo.nDirection == SCROLL_UP)
		{
			long lDepth = CCurveData::GetDepthEDMin(m_vecData, lDepth1);
			// 填充结束部分
			while (lDepth1 > lDepth)
			{
				lDepth1 -= fDelta;
				m_pDepthData->Add((long)(lDepth1 + .5), DTM(lDepth1), lTime);
			}
		}
		else
		{
			long lDepth = CCurveData::GetDepthEDMax(m_vecData, lDepth1);
			
			// 填充结束部分
			while (lDepth1 < lDepth)
			{
				lDepth1 += fDelta;
				m_pDepthData->Add((long)(lDepth1 + .5), DTM(lDepth1), lTime);
			}
		}
	}
	else if (nEnd < 0)
	{
		nEnd = (-nEnd) - 1;
		double lDepth0 = m_pDepthData->GetDepth(0);
		double lDepth1 = m_pDepthData->GetDepth(nEnd);
		double fDelta = fabs(lDepth0 - lDepth1) / nEnd;
		long lTime = m_pDepthData->GetTime(nEnd);
		int nCount = 0;
		long lDepth;
		if (m_logInfo.nDirection == SCROLL_UP)
		{
			lDepth = CCurveData::GetDepthEDMax(m_vecData, lDepth1);

			// 去除结束部分
			while (lDepth1 < lDepth)
			{
				lDepth1 += fDelta;
				nCount++;
			}
		}
		else
		{
			lDepth = CCurveData::GetDepthEDMin(m_vecData, lDepth1);

			// 去除结束部分
			while (lDepth1 > lDepth)
			{
				lDepth1 -= fDelta;
				nCount++;
			}
		}

		if (nCount)
		{
			m_pDepthData->Delete(nEnd - nCount + 1, nCount);
		}

		lDepth = m_pDepthData->GetDepthED();
		int nCurves = m_vecData.size();
		if (m_logInfo.nDirection == SCROLL_UP)
		{
			for (int i = 0; i < nCurves; i++)
			{
				CCurveData* pData = m_vecData.at(i);
				if (pData == m_pDepthData)
					continue;
				
				int nSize = pData->GetSize() - 1;
				if (nSize < 0)
					continue;
				
				long lDepth1 = pData->GetDepth(nSize);
				while (lDepth > lDepth1)
				{
					pData->m_arrData.RemoveAt(nSize--);
					if (nSize < 0)
						break;
					
					lDepth1 = pData->GetDepth(nSize);
				}
			}
		}
		else
		{
			for (int i = 0; i < nCurves; i++)
			{
				CCurveData* pData = m_vecData.at(i);
				if (pData == m_pDepthData)
					continue;
				
				int nSize = pData->GetSize() - 1;
				if (nSize < 0)
					continue;
				
				long lDepth1 = pData->GetDepth(nSize);
				while (lDepth < lDepth1)
				{
					pData->m_arrData.RemoveAt(nSize--);
					if (nSize < 0)
						break;
					
					lDepth1 = pData->GetDepth(nSize);
				}
			}
		}
	}
	return UL_NO_ERROR;
}

void CULKernel::SaveParamFirst()
{
	m_MemPerformFile.m_nTempFileOpen = 0;
	int nTools = m_arrTools.GetSize();
	if (nTools < 1)
		return;

	m_MemPerformFile.CreateTempFile(2);

	long lDepth = INVALID_DEPTH;
	CULTool* pULTool = (CULTool*)m_arrTools.GetAt(0);
	if (pULTool != NULL)
		lDepth = pULTool->GetCurDepth();
	
	for (int i = 0; i < m_arrTools.GetSize(); i++)
	{
		CULTool* pULTool = (CULTool*) m_arrTools.GetAt(i);
		if (pULTool->m_pIParam)
		{
			pULTool->m_Params.m_strTool = pULTool->strToolName;
			pULTool->m_Params.SaveFirst();
			CXMLSettings xml(FALSE, pULTool->strToolName);
			if (pULTool->m_Params.WriteTo(xml, lDepth))
			{
				xml.WriteXMLToFiles(m_MemPerformFile.GetParamFile(), NULL, NULL); 
			}
		}
	}
}

void CULKernel::SaveParam()
{
	long lDepth = m_lDepth;
	CULTool* pULTool = (CULTool*)m_arrTools.GetAt(0);
	if (pULTool != NULL)
		lDepth = pULTool->GetCurDepth();

	SaveParams(lDepth, m_MemPerformFile.GetParamFile());
}

void CULKernel::SaveParams(long lDepth, CFile* pFile /* = NULL */, CFile* pRaw /* = NULL */)
{
	BOOL bChange = FALSE;
	for (int i = 0; i < m_arrTools.GetSize(); i++)
	{
		CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
		if (pULTool->m_pIParam)
		{
			pULTool->m_Params.m_strTool = pULTool->strToolName;
			CXMLSettings xml(FALSE, pULTool->strToolName);
			if (pULTool->m_Params.WriteTo(xml, lDepth))
			{
				bChange = TRUE;
				int nIndex = -(i + 1);
				// TRACE("index is %d\n", nIndex);
				
				// Write to raw file, temp file
				if (pRaw != NULL)
					pRaw->Write(&nIndex, sizeof(nIndex));
				xml.WriteXMLToFiles(pFile, pRaw, NULL);
			}
		}
	}
	
	if (bChange)
	{
		CMainFrame* pMainFrame = (CMainFrame*) theApp.m_pMainWnd;
		if (pMainFrame->m_pChildParam->GetSafeHwnd())
			((CParamsView*) pMainFrame->m_pChildParam->m_pULView)->OnParamsChange();
	}
}

void CULKernel::RemoveGraph(CGraphWnd* pGraph)
{
	int i = m_hSendArray.GetSize() - 1;
	for ( ; i > -1; i--)
	{
		if (m_hSendArray.GetAt(i) == pGraph->GetParent()->GetSafeHwnd())
			m_hSendArray.RemoveAt(i);
	}
	
	i = m_MemPerformFile.m_vecGraph.size() - 1;
	for ( ; i > -1; i--)
	{
		if (m_MemPerformFile.m_vecGraph.at(i) == pGraph)
			m_MemPerformFile.m_vecGraph.erase(m_MemPerformFile.m_vecGraph.begin() + i);
	}
}


#endif

BOOL CULKernel::MapExchangeFile()
{
	if (m_pChannelEvent == NULL)
		m_pChannelEvent = new CEvent(FALSE, FALSE, "AskChannel", NULL);

	//建立数据交换映射文件 
	if (m_hExchangeFile == NULL)
	{
		m_hExchangeFile = CreateFileMapping((HANDLE)0xFFFFFFFF,
			0,
			PAGE_READWRITE,
			0,
			4096,
			"AskChannelData.Map");

		m_lpCodeBuffer = MapViewOfFile(
			m_hExchangeFile, 
			FILE_MAP_ALL_ACCESS, 
			0,
			0,
			4096);

		if (m_lpCodeBuffer == NULL)
		{
			CloseHandle(m_hExchangeFile);
			return FALSE;
		}
	}	
	return TRUE;
}

BOOL CULKernel::UnMapExchangeFile()
{
	if (m_pChannelEvent != NULL)
	{
		delete m_pChannelEvent;
		m_pChannelEvent = NULL;
	}
	if (m_hExchangeFile != NULL)
	{
		UnmapViewOfFile(m_lpCodeBuffer);
		CloseHandle(m_hExchangeFile);
		m_lpCodeBuffer = NULL;
		m_hExchangeFile = NULL;
	}
	return TRUE;
}

int	CULKernel::SetCurDisplayMode(int nDispMode) 
{
	m_nCurDisplayMode = nDispMode;
  	CULTool* pULTool = NULL;
	for( int i = 0; i < m_arrTools.GetSize(); i++)
	{
		pULTool = (CULTool*)m_arrTools.GetAt(i);
		pULTool->SetCurDisplayMode(nDispMode);
	}	
	m_logInfo.nWorkMode = nDispMode;
	return nDispMode;
}

int nSizeSave = 0;
void CULKernel::CleanRawBuffer()
{
	for(int i = 0 ; i < m_arrTools.GetSize() ; i++)
	{
		CULTool* pTool = (CULTool*)m_arrTools.GetAt(i);	
		pTool->m_CS.Lock();
		pTool->SetSavRawData(FALSE);
		pTool->m_nUseCount = 0;
		pTool->m_CS.Unlock ();
	}
}

/* --------------------- *
 *  ULFile Operations
 * --------------------- */


BOOL CULKernel::SaveCurveValue()
{
	if (Gbl_ConfigInfo.m_Record.Engineer)
	{		
		for (int i = 0; i < m_vecULFile.size(); i++)
			((CULFile*)m_vecULFile.at(i))->SaveDataFrame();
	}	
	return TRUE;
}

void CULKernel::ClearULFiles(LPCTSTR pszMarks /* = NULL */)
{
	int nSize = m_vecULFile.size();
	for (int i = 0; i < nSize; i++)
	{
		CULFile* pULFile = (CULFile*)m_vecULFile.at(i);
		if (pULFile != NULL)
		{
			pULFile->SaveDataEnd(pszMarks);
			// 先关闭文件
			if (pULFile->m_bOpened)
				pULFile->Close();
			delete pULFile;
		}
	}
	// finally, clear all elements from the array
	m_vecULFile.clear();	
#ifdef _LWD
	m_pRTFile = NULL;
#endif
}

long CULKernel::GetCurSaveDepth()
{
	if (m_pDepthData)
		return m_pDepthData->GetDepth(m_pDepthData->m_nCurSavePos);
	return INVALID_DEPTH;
}

void CULKernel::ApplyCurve(CCurve* pCurve)
{
	for (int i = 0; i < m_arrTools.GetSize(); i++)
	{
		CULTool* pTool = (CULTool*)m_arrTools.GetAt(i);
		if (pTool == NULL)
			continue ;

		for (int j = 0; j < pTool->m_Curves.GetSize(); j++)
		{
			CCurve* pCurve1 = (CCurve*)pTool->m_Curves.GetAt(j);
			if (pCurve1->m_strName == pCurve->m_strName && pCurve1->m_strSource == pCurve->m_strSource)
			{
				pCurve->AttachData(pCurve1->m_pData);
			}
		}
	}
}

long CULKernel::GetFDepth()
{
	if (m_pDepthData && m_pDepthData->GetSize())
	{
		return m_pDepthData->GetDepth(0);
	}

	return INVALID_DEPTH;
}

BOOL CULKernel::SetChannelOffset()
{
	int nTools = m_arrTools.GetSize();
	for(int i=0;i<nTools;i++)
	{
		CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
		for(int nChannels=0;nChannels<pULTool->m_Channels.GetSize();nChannels++)
		{
			CChannel *pChannel=(CChannel*)pULTool->m_Channels.GetAt(nChannels);
			if(pChannel->strType.Find("/s")<0)
				continue;
			UINT nPreChannelsSize=0;

			for(int j=0;j<i;j++)
			{
				CULTool * psULTool=(CULTool*)m_arrTools.GetAt(j);
				for(int nsChannels=0;nsChannels<psULTool->m_Channels.GetSize();nsChannels++)
				{
					CChannel *psChannel=(CChannel*)psULTool->m_Channels.GetAt(nsChannels);
					if(pChannel->strType==psChannel->strType)
					{
						nPreChannelsSize+=psChannel->dwBufferSize;
					}
				}
			}
			pChannel->nOffset=nPreChannelsSize;			
		}
	}
	
	return TRUE;
}

BOOL CULKernel::SetChannelBufSize(CChannel *pChannel)
{

	if(pChannel->strType.Find("/s")<0)
		return FALSE;
	for(int n=0;n<m_strSetChannels.GetSize();n++)
	{
       CString strTmp=m_strSetChannels.GetAt(n);
	   if(strTmp==pChannel->strType)
	   {
		  pChannel->strType.Delete(pChannel->strType.Find("/s"),2);
          return FALSE;
	   }
		  
	}

	int nTools = m_arrTools.GetSize();
	UINT nTotalChannelSize=0;
	CPtrArray ptrCHArray;
	int i = 0;
	for(i=0;i<nTools;i++)
	{
		CULTool* pULTool = (CULTool*)m_arrTools.GetAt(i);
		for(int nChannels=0;nChannels<pULTool->m_Channels.GetSize();nChannels++)
		{
			CChannel *psChannel=(CChannel*)pULTool->m_Channels.GetAt(nChannels);
			if(pChannel->strType==psChannel->strType)
			{
				nTotalChannelSize+=psChannel->dwBufferSize;
				ptrCHArray.Add(psChannel);
			}
		}
	}

	m_strSetChannels.Add(pChannel->strType);

	
	for(i=0;i<ptrCHArray.GetSize();i++)
	{
		CChannel *pCH=(CChannel*)ptrCHArray.GetAt(i);
		pCH->dwBufferSize=nTotalChannelSize;
	}

	ptrCHArray.RemoveAll();	
	pChannel->strType.Delete(pChannel->strType.Find("/s"),2);
	return TRUE;
}

void CULKernel::ReleaseCurvesData()
{
	if (m_nArrivePoint > 512)
	{
		for (int i = 0; i < m_vecData.size(); i++)
		{
			CCurveData* pData = m_vecData.at(i);
			if (pData != m_pDepthData)
				pData->MRelease();
		}
	}
}

BOOL CULKernel::SaveCurveValueTime()
{
	if (Gbl_ConfigInfo.m_Record.Engineer)
	{		
		for (int i = 0; i < m_vecULFile.size(); i++)
			((CULFile*)m_vecULFile.at(i))->SaveDataFrameTime();
	}	
	return TRUE;
}

void CULKernel::ApplyUnitSetting()
{
#ifdef _RUNIT
	int nTools = m_arrTools.GetSize();
	int i = 0;
	for (i = 0; i < nTools; i++)
	{
		CULTool *pULTool = (CULTool*)m_arrTools.GetAt(i);
		for (int j= 0; j < pULTool->m_Curves.GetSize(); j++)
		{
			CCurve* pCurve = (CCurve*)pULTool->m_Curves.GetAt(j);
			CCurveData* pData = pCurve->m_pData;
			if ((pData->m_nMode & CURVE_TOOL) == 0)
			{
				pData->CalcUnit(g_units, pULTool->strInfoPath);
			}
			else
			{
				pData->m_bRecUnit = FALSE;
			}
		}
	}
#endif // _RUNIT
}

// ULVerify.cpp: implementation of the CULVerify class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ULVerify.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CULVerify::CULVerify()
{

}

CULVerify::~CULVerify()
{

}

/*=============================
       刻度点类成员函数
==============================*/

IMPLEMENT_SERIAL(CULVerifyDot, CObject, 1)

CULVerifyDot::CULVerifyDot()
{
	m_fMeas	= 0.0;
	m_fEng	= 0.0;
	m_fMinStand = 0.0;
	m_fMaxStand = 0.0;
	m_fFineStand = 0.0;
	m_bVerify	= FALSE;
	m_bEditMeasure = FALSE; 
}

CULVerifyDot::CULVerifyDot(CString Name,CString Alias,int uPri,int Destime,CString tip)
{
	m_strName	= Name;
	m_strAlias  = Alias;
	m_iPriority = uPri;
	m_iDesTime	= Destime; 
	m_strTip	= tip;
	m_bVerify	= FALSE;
	m_fMeas = 0.0;
	m_fEng = 0.0;
	m_fMinStand = 0.0;
	m_fMaxStand = 0.0;
	m_fFineStand = 0.0;

	m_bEditMeasure = FALSE ;
}

CULVerifyDot::~CULVerifyDot()
{
	m_Buffer.RemoveAll();
}

void CULVerifyDot::Serialize(CArchive& ar)
{
	CObject::Serialize( ar );		// Always call base class Serialize.
	if ( ar.IsStoring() )			// Store other members : <<
	{
		ar << m_fMeas;
		ar << m_fEng;
		ar << m_fMinStand;
		ar << m_fMaxStand;
		ar << m_fFineStand;
		DWORD dwLen = m_Buffer.GetSize();
		ar << dwLen;
		double d;
		for(DWORD dwPos = 0; dwPos < dwLen; dwPos++)
		{
			d = m_Buffer.GetAt(dwPos);
			ar << d;
		}
		ar << m_strName;
		ar << m_strAlias;
		ar << m_iPriority;
		ar << m_iDesTime;
		ar << m_strTip;
		ar << m_strTime;      
        ar << m_dwIndex;             //Added for the Seriealize the Index of this dot,Gzh.2003.3.27
	}
	else							// load other members : >>
	{
		ar >> m_fMeas;
		ar >> m_fEng;
		ar >> m_fMinStand;
		ar >> m_fMaxStand;
		ar >> m_fFineStand;
		DWORD dwLen = 0;
		ar >> dwLen;
		double d;
		for(DWORD dwPos = 0; dwPos < dwLen; dwPos++)
		{
			ar >> d;
			m_Buffer.Add(d);
		}
		ar >> m_strName;
		ar >> m_strAlias;
		ar >> m_iPriority;
		m_bVerify = FALSE;
		ar >> m_iDesTime;
		ar >> m_strTip;
		ar >> m_strTime;              //the time of the calibration 
		ar >> m_dwIndex;             //Added for the Seriealize the Index of this dot,Gzh.2003.3.27
	}
}

void CULVerifyDot::operator=(const CULVerifyDot &Dot)
{

	m_strName = Dot.m_strName;
	m_strAlias = Dot.m_strAlias;
	m_iPriority = Dot.m_iPriority;
	m_bVerify = Dot.m_bVerify;
	m_strTip = Dot.m_strTip;
	m_fMeas = Dot.m_fMeas;
	m_fEng = Dot.m_fEng;
	m_fMinStand = Dot.m_fMinStand;
	m_fMaxStand = Dot.m_fMaxStand;
	m_fFineStand = Dot.m_fFineStand;
	m_strTime     =Dot.m_strTime;
	for(int i = 0 ; i < Dot.m_Buffer .GetSize () ; i++)
	{
		double fTemp = Dot.m_Buffer [i];
		m_Buffer.Add (fTemp);
	}
}

/*====================================
	设置仪器刻度日期、时间
=====================================*/
void CULVerifyDot::SetDateTm()
{
	CTime tm;
	tm = CTime::GetCurrentTime();
	m_strTime.Format("%d:%d,%d,%d,%d",
		 tm.GetHour(),tm.GetMinute(),tm.GetYear(),tm.GetMonth(),tm.GetDay());
}
/*====================================
	设置刻度项目状态
=====================================*/
void CULVerifyDot::SetCalStatus(BOOL bStatus)
{
	m_bVerify = bStatus;
}

/*====================================
	返回校验点状态
=====================================*/
BOOL CULVerifyDot::GetCalStatus()
{
	return m_bVerify;
}

/*====================================
	预置校验时间
=====================================*/
void CULVerifyDot::SetDestime(int uDesTime)
{
	m_iDesTime = uDesTime;
}

/*====================================
	返回校验预置时间
=====================================*/
int CULVerifyDot::GetDestime()
{
	return m_iDesTime;
}

/*====================================
	返回校验优先级
=====================================*/
int CULVerifyDot::GetPriority()
{
	return m_iPriority;
}

void CULVerifyDot::SetTip(CString tip)
{
	m_strTip = tip;
}

void CULVerifyDot::GetTip(CString &tip)
{
	tip = m_strTip;
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
IMPLEMENT_SERIAL(CULVerifyCurve, CObject, 1)

/*==============================
    刻度参数类成员函数
===============================*/
CULVerifyCurve::CULVerifyCurve()
{
	m_strVersion = "1.0";	// version:1.00
	m_strName = "";		// name
	m_strUnit = "";		// unit
	m_strTip  = "";		// tip
	m_bSelected   = FALSE;
	m_bVerify = FALSE; 
	m_nVerifyMethod = TRUE;
}

CULVerifyCurve::~CULVerifyCurve()
{
	ResetVerifyDotList();
}


void CULVerifyCurve::Serialize(CArchive& ar)
{
	CObject::Serialize( ar );		// Always call base class Serialize.
	if ( ar.IsStoring() )			// Store other members : <<
	{
		ar << m_strVersion;
		ar << m_strName;
		ar << m_strUnit;
		ar << m_strTip;
		ar << m_nVerifyMethod;

		int nLen = m_VerifyDotList.GetSize ();
		ar << nLen ;
		for(int i = 0 ; i < nLen ; i++)
		{
			CULVerifyDot *pVerifyDot = (CULVerifyDot *)m_VerifyDotList.GetAt (i);
			ar << pVerifyDot;
		}
	}
	else							// load other members : >>
	{
		ar >> m_strVersion;
		ar >> m_strName;
		ar >> m_strUnit;
		m_bVerify = FALSE;
		ar >> m_strTip;
		ar >> m_nVerifyMethod;

		int nLen = 0;
		ar >> nLen ;
		for(int i = 0 ; i < nLen ; i++)
		{
			CULVerifyDot *pVerifyDot = new CULVerifyDot;
			ar >> pVerifyDot;
			m_VerifyDotList.Add (pVerifyDot);
		}
	}
}

void CULVerifyCurve::operator=(const CULVerifyCurve &Curve)
{
	m_strVersion = Curve.m_strVersion;
	m_strName = Curve.m_strName;
	m_strUnit = Curve.m_strUnit;
	m_bVerify = Curve.m_bVerify;
	m_strTip = Curve.m_strTip;

	ResetVerifyDotList();
	for(int i = 0 ; i < Curve.m_VerifyDotList .GetSize () ; i++)
	{
		CULVerifyDot *pVerifyDot = new CULVerifyDot;
		CULVerifyDot *pDot = (CULVerifyDot *)Curve.m_VerifyDotList .GetAt (i);
		*pVerifyDot = *pDot;
		m_VerifyDotList.Add (pVerifyDot);
	}
}

/*===================================
	得到校验曲线上校验点的总数
====================================*/
int	CULVerifyCurve::GetDotCount()
{
	int nResult = 0;
	nResult = m_VerifyDotList.GetSize ();
	return nResult;
}



/*=============================================
	设定校验曲线校验状态(校验完为真)
==============================================*/
void CULVerifyCurve::SetCurveVerifyStatus()
{
	for(int i=0 ; i<m_VerifyDotList.GetSize() ; i++)
	{	
		CULVerifyDot* pVerifyDot = (CULVerifyDot*)m_VerifyDotList.GetAt(i);
		if(!pVerifyDot->m_bVerify)
		{
			m_bVerify = FALSE;
			return;
		}
	}
	m_bVerify = TRUE;			
}

void CULVerifyCurve::ResetVerifyDotList ()
{
	for(int i = 0 ; i < m_VerifyDotList.GetSize () ; i++)
	{
		CULVerifyDot *pVerifyDot = (CULVerifyDot *)m_VerifyDotList.GetAt (i);
		delete pVerifyDot;
	}
	m_VerifyDotList.RemoveAll ();
}


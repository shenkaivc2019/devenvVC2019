// GfxPageTrack.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "GfxPageTrack.h"
#include "Track.h"
#include "GraphWnd.h"
#include "GraphHeaderWnd.h"
#include "GraphView.h"
#include "MainFrm.h"
#include "Sheet.h"
#include "ChildFrm.h"
#include "Units.h"
#include "Project.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CUnits* g_units;

/////////////////////////////////////////////////////////////////////////////
// CGfxPageTrack dialog

CGfxPageTrack::CGfxPageTrack(CWnd* pParent /*=NULL*/) : CGfxPage(IDD_GFX_PAGE_TRACK, pParent)
{
	//{{AFX_DATA_INIT(CGfxPageTrack)
	m_lTrackLeft = 0;
	m_lTrackRight = 0;
	m_strTrackName = _T("");
	//}}AFX_DATA_INIT
}

void CGfxPageTrack::DoDataExchange(CDataExchange* pDX)
{
	CGfxPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGfxPageTrack)
	DDX_Control(pDX, IDC_CHECK1, m_chkResult);
	DDX_Control(pDX, IDC_COMBO3, m_cbDev);
	DDX_Control(pDX, IDC_COMBO4, m_cbAzim);
	DDX_Text(pDX, IDC_TRACK_NAME, m_strTrackName);
	//}}AFX_DATA_MAP
// 	g_units->DDXIVN_Text(pDX, IDC_EDIT1, IDC_STATIC1, m_lTrackLeft,
// 				_T("LMetric"), 0, DEFAULT_PAGE_WIDTH, _T("in"));
// 	g_units->DDXIVN_Text(pDX, IDC_EDIT2, IDC_STATIC2, m_lTrackRight,
// 				_T("LMetric"), 0, DEFAULT_PAGE_WIDTH, _T("in"));

	g_units->DDXVN_Text(pDX, IDC_EDIT1, IDC_STATIC1, m_lTrackLeft, _T("LMetric"), 0, 10000, _T("PageSize"));
	g_units->DDXVN_Text(pDX, IDC_EDIT2, IDC_STATIC2, m_lTrackRight, _T("LMetric"), 0, 10000, _T("PageSize"));

	if (pDX->m_bSaveAndValidate)
	{
		m_lTrackLeft = floor(m_lTrackLeft + .5);
		m_lTrackRight= floor(m_lTrackRight + .5);
	}
}

BEGIN_MESSAGE_MAP(CGfxPageTrack, CGfxPage)
	//{{AFX_MSG_MAP(CGfxPageTrack)
	ON_BN_CLICKED(IDC_CHECK1, OnCheck1)
	ON_CBN_SELCHANGE(IDC_COMBO3, OnSelchangeDev)
	ON_CBN_SELCHANGE(IDC_COMBO4, OnSelchangeAzim)
	ON_EN_CHANGE(IDC_TRACK_NAME, OnChangeTrackName)
	ON_EN_KILLFOCUS(IDC_EDIT1, OnKillfocusTrackLeftpos)
	ON_EN_KILLFOCUS(IDC_EDIT2, OnKillfocusTrackRightpos)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGfxPageTrack message handlers

BOOL CGfxPageTrack::OnInitDialog()
{
	CGfxPage::OnInitDialog();

	InitTrack();

	return TRUE;  // return TRUE unless you set the focus to a control
}

void CGfxPageTrack::SetTrackEdge(int iRight)
{
	if (m_pGraph == NULL)
		return;

	if (m_pTrack == NULL)
		return;

	CRect rect;
	int nEdge = m_pGraph->m_pSheet->GetTrackIndex(m_pTrack, rect);
	if (nEdge < 0)
		return;

	if (!UpdateData())
		return;

	if (iRight)
	{
		if (m_lTrackRight > (rect.right - 10))
		{
			m_lTrackRight = rect.right - 10;
			UpdateData(FALSE);
		}
	}
	else if (m_lTrackLeft < (rect.left + 10))
	{
		m_lTrackLeft = rect.left + 10;
		UpdateData(FALSE);
	}

	if (m_lTrackLeft > (m_lTrackRight - 10))
	{
		if (iRight)
			m_lTrackRight = m_lTrackLeft + 10;
		else
			m_lTrackLeft = m_lTrackRight - 10;
		UpdateData(FALSE);
	}
	
	int x = iRight ? m_lTrackRight : m_lTrackLeft;
	x += PAGE_MARGIN;

	x *= m_pGraph->m_pSheet->m_nRatioX;
	m_pGraph->m_pSheet->SetTrackEdge(nEdge + iRight, x);
	m_pGraph->InvalidateAll(TRUE);

	CMainFrame* pMainFrame = (CMainFrame*) AfxGetMainWnd();
	if (pMainFrame->m_wndPropertiesBar.GetSafeHwnd())
		pMainFrame->m_wndPropertiesBar.PostMessage(UM_PROP_UPDATE, PT_ITEM,
										idTrackRight);
}

void CGfxPageTrack::InitTrack()
{
	if (m_pTrack != NULL)
	{
		CRect rc;

		m_strTrackName = m_pTrack->GetName();

		m_pTrack->GetRegionRect(&rc, TRUE);
		m_lTrackLeft = rc.left - PAGE_MARGIN;
		m_lTrackRight = rc.right - PAGE_MARGIN;
		m_chkResult.SetCheck(m_pTrack->m_nType);

		m_cbAzim.ResetContent();
		m_cbDev.ResetContent();
		CULDoc* pDoc = m_pGraph->m_pDoc;
		
		
		vec_ic datas;
		vec_ic* pDatas = NULL;
		
		if (pDoc != NULL)
		{
			CULFile* pFile = DYNAMIC_DOWNCAST(CULFile, pDoc);
			if (pFile != NULL)
			{
				pDatas = &pFile->m_vecCurve;
			}
			else if (pDoc->m_pProject != NULL)
			{
				pDoc->m_pProject->m_ULKernel.GetAllCurve(datas);
				pDatas = &datas;
			}
			else
				pDatas = &pDoc->m_vecCurve;
		}
		
		if (pDatas != NULL)
		{
			int nItem = m_cbAzim.AddString(_T(""));
			m_cbAzim.SetItemData(nItem, 0);
			nItem = m_cbDev.AddString(_T(""));
			m_cbDev.SetItemData(nItem, 0);
			int nCount = pDatas->size();
			for (int i = 0; i < nCount; i++)
			{
				CCurve* pCurve = (CCurve*)pDatas->at(i);
				nItem = m_cbAzim.AddString(pCurve->m_strName);
				m_cbAzim.SetItemData(nItem, (DWORD_PTR)pCurve);
				nItem = m_cbDev.AddString(pCurve->m_strName);
				m_cbDev.SetItemData(nItem, (DWORD_PTR)pCurve);
			}
			
			m_cbAzim.SelectString(-1, m_pTrack->m_strAZIM);
			m_cbDev.SelectString(-1, m_pTrack->m_strDEV);
		}
	}

	UpdateData(FALSE);
}

void CGfxPageTrack::OnSelchangeDev()
{
	int nSelect = m_cbDev.GetCurSel();
	if (nSelect == CB_ERR)
		return;

	CCurve* pCurve = (CCurve*)m_cbDev.GetItemData(nSelect);
	if (pCurve && AfxIsValidString(pCurve->m_strName))
	{
		if (m_pTrack->m_pDEV != pCurve)
			m_pTrack->SetDev(pCurve);
	}
	else
		m_pTrack->SetDev(NULL);

	if (m_pGraph->GetSafeHwnd())
		m_pGraph->Invalidate();
}

void CGfxPageTrack::OnSelchangeAzim()
{
	int nSelect = m_cbAzim.GetCurSel();
	if (nSelect == CB_ERR)
		return;

	CCurve* pCurve = (CCurve*)m_cbAzim.GetItemData(nSelect);
	if (pCurve && AfxIsValidString(pCurve->m_strName))
	{
		 if (m_pTrack->m_pAZIM != pCurve)
			m_pTrack->SetAzim(pCurve);
	}
	else
		m_pTrack->SetAzim(NULL);
   
	if (m_pGraph->GetSafeHwnd())
		m_pGraph->Invalidate();
}

void CGfxPageTrack::OnChangeTrackName()
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CGfxPage::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO: Add your control notification handler code here
	if (m_pTrack != NULL)
	{
		UpdateData(TRUE);
		if (m_strTrackName.GetLength())
		{
			m_pTrack->SetName(m_strTrackName);
			if (m_pGraph != NULL)
				m_pGraph->InvalidateHead();
		}
	}
}

void CGfxPageTrack::OnKillfocusTrackLeftpos()
{
	SetTrackEdge(0);
}

void CGfxPageTrack::OnKillfocusTrackRightpos()
{
	SetTrackEdge(1);
}

void CGfxPageTrack::OnDestroy()
{
	CGfxPage::OnDestroy();

	// TODO: Add your message handler code here
	m_pTrack = NULL;
	m_pGraph = NULL;
}

void CGfxPageTrack::OnCheck1()
{
	if (m_pTrack)
	{
		m_pTrack->SetType(m_chkResult.GetCheck());
		if (m_pGraph && m_pGraph->m_pDoc)
		{
			IProject* pProject = m_pTrack->m_pProject;
			m_pTrack->SetProject(m_pGraph->m_pDoc->m_pProject);
			if (pProject != m_pTrack->m_pProject)
				m_pGraph->Invalidate();
		}
		else
			m_pTrack->SetProject(NULL);
	}
}

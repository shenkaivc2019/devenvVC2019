// ULDoc.cpp: implementation of the CULDoc class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#ifdef _LOGIC
#include "Logic.h"
#else
#include "ul2000.h"
#endif
#include "ToolParam.h"
#include "PrintOrderFile.h"
#include "ULDoc.h"
#include "ChildFrm.h"
#include "GraphHeaderView.h"
#include "GraphWnd.h"
#include "Sheet.h"
#include "Curve.h"
#include "ComConfig.h"
#include "DlgRange.h"
#include "MainFrm.h"
#include "ScoutFunctionDef.h"
#include "Project.h"
#include "Unitx.h"
#include "LogInformationManager.h"
#include "DlgRangeTime.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

UINT CloseThreadProc(LPVOID pParam)
{
	CULFile* pULFile = (CULFile*)pParam;
	if (pULFile)
		delete pULFile;
	
	return 0;
}

CSheetInfo::~CSheetInfo()
{
	if (pFrame->GetSafeHwnd())
	{
		pFrame->m_dwLocked &= ~LT_LOGING;
		pFrame->m_bDestroy = TRUE;
		pFrame->SendMessage(WM_CLOSE);
		pFrame = NULL;
	}
	
	if (pSheet)
	{
		pSheet->Release();
		pSheet = NULL;
	}
}

IMPLEMENT_DYNCREATE(CULDoc, CDocument)

BEGIN_MESSAGE_MAP(CULDoc, CDocument)
END_MESSAGE_MAP()

CULDocList	CULDoc::m_DocList;

CULDoc* CULDoc::GetDoc(LPCTSTR pszPathName)
{
	for (POSITION pos = m_DocList.GetHeadPosition(); pos != NULL; )
	{
		CULDoc* pDoc = m_DocList.GetNext(pos);
		if (pDoc->m_strPathName.CompareNoCase(pszPathName) == 0)
			return pDoc;
	}
	
	return NULL;
}

CULDoc* CULDoc::GetDoc(int nIndex)
{
	POSITION pos = m_DocList.FindIndex(nIndex);
	if (pos != NULL)
		return m_DocList.GetAt(pos);
	return NULL;
}

CULFile* CULDoc::CloseFiles()
{
	// 如果只有一个文件，提示是否保存
	if (m_DocList.GetCount() == 1)
	{
		CULFile* pFile = DYNAMIC_DOWNCAST(CULFile, m_DocList.GetHead());
		if (!CloseFile(pFile))
			return pFile;
		return NULL;
	}

	// 对于多个文件，使用CloseFileDlg来提示是否保存
	//Add by zy 2008 08 28
	CLOSE_ALL_FILE caf;
	ZeroMemory(&caf , sizeof(CLOSE_ALL_FILE));
	for(int i = m_DocList.GetCount() - 1 ; i >=0 ; i--)
	{
		POSITION pos = m_DocList.FindIndex(i);
		CULFile* pFile = DYNAMIC_DOWNCAST(CULFile, m_DocList.GetAt(pos));
		if(pFile == NULL)
			continue;
		if (!CloseFile(pFile , &caf))
			return pFile;
	}

	return NULL;
/*	while (m_DocList.GetCount())
	{
		CULFile* pFile = DYNAMIC_DOWNCAST(CULFile, m_DocList.GetHead());
		if (!CloseFile(pFile))
			return pFile;
	}

	return NULL;*/
}

BOOL CULDoc::CloseFile(CULFile* pULFile, CLOSE_ALL_FILE *pInfo)
{
	if(pULFile == NULL)
		return TRUE;
	if(pInfo != NULL && pInfo->bCancel)	
		return TRUE;
	try
	{
		if (pULFile->m_nRead != 2)
			return FALSE;
		
		if (pULFile->CanCloseFrame(pInfo))
		{
			g_pMainWnd->m_wndWorkspace.m_wndTree.DelFileItem(pULFile);
			pULFile->CloseFrames();
			pULFile->Close();
			CULDoc::EarseDoc(pULFile);
#ifdef _FileThread
			AfxBeginThread(CloseThreadProc, pULFile);
#else
			delete pULFile;
#endif	
		}
		else
			return FALSE;
		
		return TRUE;
	}
	catch (CException* e)
	{
		e->ReportError();
		e->Delete();
	}
	catch (...) 
	{
		AfxMessageBox(IDS_CULDOC_UNKNOWN_ERROR, MB_ICONEXCLAMATION);//Unknown error!
	}

	return TRUE;
/*	if (pULFile == NULL)
		return TRUE;

	try
	{
		if (pULFile->m_nRead != 2)
			return FALSE;
		
		if (pULFile->CanCloseFrame())
		{
			g_pMainWnd->m_wndWorkspace.m_wndTree.DelFileItem(pULFile);
			pULFile->CloseFrames();
			pULFile->Close();
			CULDoc::EarseDoc(pULFile);
#ifdef _FileThread
			AfxBeginThread(CloseThreadProc, pULFile);
#else
			delete pULFile;
#endif	
		}
		else
			return FALSE;
		
		return TRUE;
	}
	catch (CException* e)
	{
		e->ReportError();
		e->Delete();
	}
	catch (...) 
	{
		AfxMessageBox("Unknown error!", MB_ICONEXCLAMATION);
	}

	return TRUE;*/
}

void CULDoc::EarseDoc(CULDoc* pDoc)
{
	POSITION pos = m_DocList.Find(pDoc);
	if (pos != NULL)
		m_DocList.RemoveAt(pos);
	if (m_DocList.GetCount() == 0)
	{
		g_pMainWnd->SendMessage(UM_DATA_SAVE, 7);
	}
}

CULFile* CULDoc::GetFile(int nIndex /* = 0 */)
{
	POSITION pos = m_DocList.GetHeadPosition();
	for (; pos != NULL;)
	{
		CULFile* pFile = DYNAMIC_DOWNCAST(CULFile, m_DocList.GetNext(pos));
		if (pFile != NULL)
			return pFile;
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CULDoc::CULDoc()
{
	// EnableCompoundFile();

	m_pProject = NULL;
	m_nRead = 0;
	m_bModified = FALSE;
	m_dwModified = 0;
	m_pDCurve = NULL;
	m_pTCurve = NULL;

	m_nDriveMode = UL_DRIVE_DEPT;
}

CULDoc::~CULDoc()
{
	ClearSheetInfo();
	ClearCellInfo();
	ClearCstInfo();
	ClearPofInfo();
	ClearSolidInfo();
}

BOOL CULDoc::AddSheetTempl(LPCTSTR pszTempl, DWORD dwAdd /* = 0 */)
{	
	CSheetInfo* pSheetInfo = GetSheetInfo(pszTempl);
	if (pSheetInfo != NULL)
		return FALSE;
	
	pSheetInfo = new CSheetInfo;
	pSheetInfo->strTempl = pszTempl;
	pSheetInfo->strFile = SHT_FILE(pszTempl);
	if (dwAdd > 0)
	{
		pSheetInfo->pSheet = new CSheet(pSheetInfo->strFile);
		pSheetInfo->pSheet->AddRef();

		if (dwAdd > AT_NEW)
		{
			CULFile* pFile = DYNAMIC_DOWNCAST(CULFile, this);
			if (pFile != NULL)
			{
				int nDirection = pFile->m_nDirection;
				int nDriveMode = pFile->m_nDriveMode;
				pSheetInfo->pSheet->SetLogMode(WORKMODE_IDLE, nDriveMode);
				pSheetInfo->pSheet->RemapTrackCurve(pFile->m_vecCurve);				
			}

			pSheetInfo->pFrame = new CChildFrame(ULV_GRAPHS, pSheetInfo->pSheet, this);
		}
	}

	m_sheetiList.AddTail(pSheetInfo);
	
	return TRUE;
}

CCellInfo* CULDoc::AddCellTempl(LPCTSTR pszTempl, DWORD dwAdd /* = 0 */)
{
	CCellInfo* pCellInfo = GetCellInfo(pszTempl);
	if (pCellInfo != NULL)
	{
		if ((dwAdd > AT_NEW) && (pCellInfo->pFrame == NULL))
		{
			pCellInfo->pFrame = new CChildFrame(ULV_GRAPHHEAD, pCellInfo, this);
			return pCellInfo;
		}
		return NULL;
	}
	
	pCellInfo = new CCellInfo;
	pCellInfo->strTempl = pszTempl;
	pCellInfo->strFile  = HTS_FILE(pszTempl);
	if (dwAdd > AT_NEW)
	{
		pCellInfo->pFrame = new CChildFrame(ULV_GRAPHHEAD, pCellInfo, this);
	}

	m_celliList.AddTail(pCellInfo);
	return pCellInfo;
}

CCstInfo* CULDoc::AddCstTempl(LPCTSTR pszTempl, DWORD dwAdd /* = 0 */)
{
	CCstInfo* pCstInfo = GetCstInfo(pszTempl);
	if (pCstInfo != NULL)
	{
		if ((dwAdd > AT_NEW) && (pCstInfo->pFrame == NULL))
		{
			pCstInfo->pFrame = new CChildFrame(ULV_CUSTOMIZEVIEW, pCstInfo, this);
			return pCstInfo;
		}
		return NULL;
	}
	
	pCstInfo = new CCstInfo;
	pCstInfo->strTempl = pszTempl;
	pCstInfo->strFile  = ACV_FILE(pszTempl);
	if (dwAdd > AT_NEW)
	{
		pCstInfo->pFrame = new CChildFrame(ULV_CUSTOMIZEVIEW, pCstInfo, this);
	}

	m_cstiList.AddTail(pCstInfo);
	return pCstInfo;
}

POSITION CULDoc::FindCell(CCell2000* pCell, CGraphHeaderView** ppFView)
{
	POSITION pos = m_celliList.GetHeadPosition();
	for ( ; pos != NULL; )
	{
		CCellInfo* pCellInfo = m_celliList.GetNext(pos);
		if (pCellInfo == NULL)
			continue;

		if (pCellInfo->pFrame == NULL)
			continue;

		CGraphHeaderView* pFView = pCellInfo->pFrame->m_pGHView;
		if (pFView == NULL)
			continue;

		POSITION pos1 = pFView->m_cellGroup.Find(pCell);
		if (pos1 != NULL)
		{
			*ppFView = pFView;
			return pos1;
		}
	}

	return NULL;
}

void CULDoc::GetCelliList(CCellInfoList* pCells)
{
	CStringList strList;
	POSITION pos = m_celliList.GetHeadPosition();
	for ( ; pos != NULL; )
	{
		CCellInfo* pCell0 = m_celliList.GetNext(pos);
		if (pCell0 == NULL)
			continue;
		
		if (strList.Find(pCell0->strTempl))
			continue;
		
		strList.AddTail(pCell0->strTempl);
		CCellInfo* pCellInfo = new CCellInfo;
		pCellInfo->strTempl = pCell0->strTempl;
		if (pCell0->strFile.GetLength())
			pCellInfo->strFile = pCell0->strFile;
		else
			pCellInfo->strFile = HTS_FILE(pCell0->strTempl);
		
		pCellInfo->pFrame = pCell0->pFrame;
		pCells->AddTail(pCellInfo);
	}
	
	for (/*POSITION*/ pos = m_pofiList.GetHeadPosition(); pos != NULL;)
	{
		CPofInfo* pPofInfo = m_pofiList.GetNext(pos);
		if (pPofInfo == NULL)
			continue ;
		
		if (pPofInfo->pPof != NULL)
		{
			pPofInfo->pPof->AddHead(&strList, pCells);
			continue ;
		}
		
		if (pPofInfo->strFile.IsEmpty() && pPofInfo->strTempl.GetLength())
		{
			pPofInfo->strFile = PRN_FILE(pPofInfo->strTempl);
		}
		
		if (pPofInfo->strFile.GetLength())
		{
			CPof* pPof = new CPof;
			if (pPof->LoadOrder(pPof->m_strFile))
			{
				pPof->AddHead(&strList, pCells);
				pPofInfo->pPof = pPof;
			}
			else
				delete pPof;
		}
	}
}

// 刷新井道中的标签信息
void CULDoc::InvalidateCell(CCell2000* pCell, long lCol, long lRow, long lSheet)
{
	CString strNote = pCell->GetCellNote(lCol, lRow, 0);
	if (strNote.IsEmpty())
		return;
	int nCase = 0;
	if (strNote.Right(1) == "$") // 曲线越界边界
		nCase = 1;
	if (strNote.Right(1) == "#") // 曲线越界次数
		nCase = 2;
	if (nCase > 0)
	{
		CString strTrack;
		CString strCurve;
		CString strSide;	
		int nPos = strNote.Find("_");
		if (nPos != -1)
		{	
			strTrack = strNote.Left(nPos);
			CString str = strNote.Right(strNote.GetLength() - nPos - 1);
			nPos = str.Find("_");
			if (nPos != -1)
			{
				strCurve = str.Left(nPos);
				strSide = str.Mid(nPos + 1, 1);
			}
		}
		if (strTrack.GetLength() > 0 && strCurve.GetLength() > 0 && strSide.GetLength() > 0)
		{
			CCurve* pCellCurve = NULL;
			for (POSITION pos = m_sheetiList.GetHeadPosition(); pos != NULL; )
			{
				CSheetInfo* pSheetInfo = m_sheetiList.GetNext(pos);
				if (pSheetInfo->pSheet == NULL)
					continue;
				CSheet* pSheet = pSheetInfo->pSheet;
				for (int i = 0; i < pSheet->m_TrackList.GetSize(); i++)
				{
					CTrack* pTrack = (CTrack*)pSheet->m_TrackList.GetAt(i);
					for (int j = 0; j < pTrack->m_vecCurve.size(); j++)
					{
						CCurve* pCurve = pTrack->m_vecCurve.at(j);
						if (pTrack->m_strName == strTrack && pCurve->m_strName == strCurve)
						{
							pCellCurve = pCurve;
							break;
						}
					}
				}
			}
			if (pCellCurve == NULL)
				return;

			if (pCellCurve->m_pData == NULL)
				return;

			switch (nCase)  // 曲线越界边界
			{
			case 1:
				{
					CString strValue;
					if (strSide == "L")
					{
						strValue.Format("%.3lf", pCellCurve->m_pData->m_LeftBound);
					}
					else if (strSide == "R")
					{
						strValue.Format("%.3lf", pCellCurve->m_pData->m_RightBound);
					}
					pCell->SetCellString(lCol, lRow, lSheet, strValue);
				}
				break;
			case 2:    // 曲线越界次数
				{
					CString strValue;
					if (strSide == "L")
					{
						strValue.Format("%d", pCellCurve->m_pData->m_nLeftOutNum);
					}
					else if (strSide == "R")
					{
						strValue.Format("%d", pCellCurve->m_pData->m_nRightOutNum);
					}
					pCell->SetCellString(lCol, lRow, lSheet, strValue);
				}
				break;
			}
		}
	}
}

void CULDoc::CellfromPof()
{
	// Get celli has been added
	CStringList strList;
	POSITION pos = m_celliList.GetHeadPosition();
	for ( ; pos != NULL; )
	{
		CCellInfo* pCell0 = m_celliList.GetNext(pos);
		if (pCell0 == NULL)
			continue;
		
		if (strList.Find(pCell0->strTempl))
			continue;
		
		strList.AddTail(pCell0->strTempl);
	}
	
	// Get new celli from pof
	for (/*POSITION*/ pos = m_pofiList.GetHeadPosition(); pos != NULL; )
	{
		POSITION pos1 = pos;
		CPofInfo* pPofInfo = m_pofiList.GetNext(pos);
		if (pPofInfo == NULL)
			continue ;
		
		if (pPofInfo->pPof != NULL)
		{
			pPofInfo->pPof->AddHead(&strList, &m_celliList);
			continue ;
		}
		
		if (pPofInfo->strFile.IsEmpty() && pPofInfo->strTempl.GetLength())
		{
			pPofInfo->strFile = PRN_FILE(pPofInfo->strTempl);
		}
		
		if (pPofInfo->strFile.IsEmpty())
		{
			m_pofiList.RemoveAt(pos1);
			continue;
		}
		
		CPof* pPof = new CPof;
		if (pPof->LoadOrder(pPofInfo->strFile))
		{
			pPof->AddHead(&strList, &m_celliList);
			pPofInfo->pPof = pPof;
		}
		else
		{
			delete pPof;
			m_pofiList.RemoveAt(pos1);
			delete pPofInfo;
		}
	}
}

BOOL CULDoc::AddPofTempl(LPCTSTR pszTempl)
{
	CPofInfo* pPofInfo = GetPofInfo(pszTempl);
	if (pPofInfo != NULL)
		return FALSE;
		
	pPofInfo = new CPofInfo;
	pPofInfo->strTempl = pszTempl;
	pPofInfo->strFile = PRN_FILE(pPofInfo->strTempl);
	m_pofiList.AddTail(pPofInfo);
	return TRUE;
}

CChildFrame* CULDoc::GetFirstFrame(DWORD dwType, LPCTSTR pszTempl /* = NULL */)
{
	if (dwType & ULV_GRAPHS)
	{
		if (pszTempl != NULL)
		{
			POSITION pos = m_sheetiList.GetHeadPosition();
			for ( ; pos != NULL; )
			{
				CSheetInfo* pSheetInfo = m_sheetiList.GetNext(pos);
				if (pSheetInfo == NULL)
					continue ;

				if (pSheetInfo->strTempl == pszTempl)
					return pSheetInfo->pFrame;
			}
		}

		dwType = ULV_GRAPHS;
	}
	else if (dwType & ULV_GRAPHHEAD)
	{
		if (pszTempl != NULL)
		{
			POSITION pos = m_celliList.GetHeadPosition();
			for ( ; pos != NULL; )
			{
				CCellInfo* pCellInfo = m_celliList.GetNext(pos);
				if (pCellInfo == NULL)
					continue ;
				
				if (pCellInfo->strTempl == pszTempl)
					return pCellInfo->pFrame;
			}

			return NULL;
		}
	}
		
	for (POSITION pos = m_frmList.GetHeadPosition(); pos != NULL; )
	{
		CChildFrame* pChild = m_frmList.GetNext(pos);
		if (pChild == NULL)
			continue ;
		
		if (pChild->m_nViewType & dwType)
			return pChild;
	}
	
	return NULL;
}

void CULDoc::OpenFrames(DWORD dwTypes)
{
	if (m_sheetiList.GetCount() < 1)
	{
		CString strFind = SHT_FILE("*");
		WIN32_FIND_DATA fd;
		HANDLE hFind = ::FindFirstFile(strFind, &fd);
		BOOL bFindNext = (hFind != INVALID_HANDLE_VALUE);
		
		while (bFindNext)
		{
			strFind = fd.cFileName;
			int nPos = strFind.Find('.');
			if (nPos != -1)
				strFind = strFind.Left(nPos);

			if (AddSheetTempl(strFind, AT_NEW))
				break;

			bFindNext = ::FindNextFile(hFind, &fd);
		}
		::FindClose(hFind);
	}
	
	POSITION pos = m_sheetiList.GetHeadPosition();
	theApp.SetStatusPrompt(IDS_CREATEGRAPHS);
	for ( ; pos != NULL; )
	{
		CSheetInfo* pSheetInfo = m_sheetiList.GetNext(pos);
		if (pSheetInfo->pFrame->GetSafeHwnd())
			continue ;

		CChildFrame* pChild = pSheetInfo->pFrame;
		if (pChild == NULL)
			pChild = new CChildFrame(ULV_GRAPHS, pSheetInfo->pSheet, this);
		
		if (!pChild->LoadFrame(IDR_MAINFRAME, WS_VISIBLE|WS_CHILD, theApp.m_pMainWnd))
		{
			TRACE0("Warning: Couldn't create a frame of presentation.\n");
			continue ;
		}
		
		pSheetInfo->pFrame = pChild;
		pChild->SetWindowText(pSheetInfo->strTempl);
	}
	
	theApp.SetStatusPrompt(IDS_CREATEGRAPHHEADER);
	for (pos = m_celliList.GetHeadPosition(); pos != NULL; )
	{
		POSITION pos1 = pos;
		CCellInfo* pCellInfo = m_celliList.GetNext(pos);
		if (pCellInfo == NULL)
		{
			m_celliList.RemoveAt(pos1);
			continue;
		}
		
		if (pCellInfo->strFile.IsEmpty())
		{
			if (pCellInfo->strTempl.IsEmpty())
				continue ;
			
			pCellInfo->strFile = HTS_FILE(pCellInfo->strTempl);
		}

		CFileFind ff;
		if (!ff.FindFile(pCellInfo->strFile))
		{
			m_celliList.RemoveAt(pos1);
			delete pCellInfo;
			continue;
		}

		if (pCellInfo->pFrame)
			continue;
		
		CChildFrame* pChild = new CChildFrame(ULV_GRAPHHEAD, pCellInfo, this);
		pChild->m_dwState &= ~ST_SHOW;
		if (!pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD, theApp.m_pMainWnd))
		{
			TRACE0("Warning: Couldn't create a frame of header.\n");
			continue ;
		}
		
		pCellInfo->pFrame = pChild;
		pCellInfo->pCell = &pChild->m_pGHView->m_Cell;
		pChild->SetWindowText(pCellInfo->strTempl);
		pChild->m_bDestroy = FALSE;
		CULView::InvalidateCell(pCellInfo->pCell);
	}
	
	for (pos = m_cstiList.GetHeadPosition(); pos != NULL; )
	{
		POSITION pos1 = pos;
		CCstInfo* pCstInfo = m_cstiList.GetNext(pos);
		if (pCstInfo == NULL)
		{
			m_cstiList.RemoveAt(pos1);
			continue;
		}

		if (pCstInfo->strFile.IsEmpty())
		{
			if (pCstInfo->strTempl.IsEmpty())
				continue;
			
			pCstInfo->strFile = ACV_FILE(pCstInfo->strTempl);
		}

		CFileFind ff;
		if (!ff.FindFile(pCstInfo->strFile))
		{
			m_cstiList.RemoveAt(pos1);
			delete pCstInfo;
			continue;
		}

		if (pCstInfo->pFrame)
			continue;
		
		CChildFrame* pChild = new CChildFrame(ULV_CUSTOMIZEVIEW, pCstInfo, this);
		pChild->m_dwState &= ~ST_SHOW;
		if (!pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD, theApp.m_pMainWnd))
		{
			TRACE0("Warning: Couldn't create a frame of header.\n");
			continue ;
		}
		
		pCstInfo->pFrame = pChild;
		pChild->SetWindowText(pCstInfo->strTempl);
		pChild->m_bDestroy = FALSE;
	}
	
	if (GetFirstFrame(ULV_TOOLS) == NULL)
	{   
		theApp.SetStatusPrompt(IDS_CREATETOOLGROUP);
		CChildFrame* pChild = new CChildFrame(ULV_TOOLS, m_pProject, this);
		pChild->m_dwState &= ~ST_SHOW;
		if (!pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD, theApp.m_pMainWnd))
		{
			TRACE0("Warning: Couldn't create a frame of tools.\n");
		}
		else
		{
			pChild->SetTitle(IDS_TEXT_TOOLGROUP);
		}
	}
	
	if (GetFirstFrame(ULV_CONSTRUCT) == NULL)
	{
		CChildFrame* pChild = new CChildFrame(ULV_CONSTRUCT, m_pProject, this);
		pChild->m_dwState &= ~ST_SHOW;
		if (!pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD, theApp.m_pMainWnd))
		{
			TRACE0("Warning: Couldn't create a frame of well schematic.\n");
		}
		else
		{
			pChild->SetTitle(IDS_WELL_SCHEMATIC);
		}
	}
	
	// 2008.11.29发现程序Bug:AXP启动后，打开任意一个数据文件，无法加载TabInfoView
	// 等视图。程序跟踪到OpenFrames函数时，发现这些视图没有加载成功。后来查明原因
	// TabInforView,FileInforView,TooInforView等几个视图的实现使用了Cell组件，而本
	// 机的Cell组件已经被卸载掉。
	
	// 如果程序加载TabInforView等视图失败，可以先查看一下是否是没有注册Cell组件

	CPtrArray* pTools = &m_pProject->m_ULKernel.m_arrTools;
	if (GetFirstFrame(ULV_TABLEINFOVIEW) == NULL)
	{
		CChildFrame* pChild = new CChildFrame(ULV_TABLEINFOVIEW, pTools, this);
		pChild->m_dwState &= ~ST_SHOW;
		if (!pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD, theApp.m_pMainWnd))
		{
			TRACE0("Warning : Couldn't create a frame of table informations.\n");
		}
		else
		{
			pChild->SetTitle(IDS_TABLEINFOVIEW);
		}
	}
	
	if (GetFirstFrame(ULV_CALSUMMARY) == NULL)
	{
		CChildFrame* pChild = new CChildFrame(ULV_CALSUMMARY, pTools, this);
		pChild->m_dwState &= ~ST_SHOW;
		if (!pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD, theApp.m_pMainWnd))
		{
			TRACE0("Warning : Couldn't create a frame of calibarte summary.\n");
		}
		else
		{
			pChild->SetTitle(IDS_CALIBRATE_SUMMARY);
		}
	}
	
	if (GetFirstFrame(ULV_TOOLINFOVIEW) == NULL)
	{
		CChildFrame* pChild = new CChildFrame(ULV_TOOLINFOVIEW, pTools, this);
		pChild->m_dwState &= ~ST_SHOW;
		if (!pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD, theApp.m_pMainWnd))
		{
			TRACE0("Warning : Couldn't create a frame of tool informations.\n");
		}
		else
		{
			pChild->SetTitle(IDS_TOOLINFOVIEW);
		}
	}
	
	if (GetFirstFrame(ULV_OUTPUTSTABLE) == NULL)
	{
		CChildFrame* pChild = new CChildFrame(ULV_OUTPUTSTABLE, &m_vecCurve, this);
		pChild->m_dwState &= ~ST_SHOW;
		if (!pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD, theApp.m_pMainWnd))
		{
			TRACE0("Warning : Couldn't create a frame of outputs table.\n");
		}
		else
		{
			pChild->SetTitle(IDS_OUTPUTSTABLE);
		}
	}

#ifndef _LOGIC
	
	if (GetFirstFrame(ULV_FACTOR) == NULL)
	{
		CChildFrame* pChild = NULL;
		CULFile* pFile = DYNAMIC_DOWNCAST(CULFile, this);
		if (pFile == NULL)
		{
			pChild = new CChildFrame(ULV_FACTOR, pTools, this);
		}
		else
			pChild = new CChildFrame(ULV_FACTOR, &m_vecCurve, this);

		pChild->m_dwState &= ~ST_SHOW;
		if (!pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD, theApp.m_pMainWnd))
		{
			TRACE0("Warning : Couldn't create a frame of tool informations.\n");
		}
		else
		{
			pChild->SetTitle(IDS_CURVE_FACTOR_TABLE);
		}
	}
	
#endif
	
	if (GetFirstFrame(ULV_CALCHART) == NULL)
	{
		CChildFrame* pChild = new CChildFrame(ULV_CALCHART, pTools, this);
		pChild->m_dwState &= ~ST_SHOW;
		if (!pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD, theApp.m_pMainWnd))
		{
			TRACE0("Warning : Couldn't create a frame of calibarte report.\n");
		}
		else
		{
			pChild->SetTitle(IDS_CALIBRATE_CHART);
		}
	}
	
	if (GetFirstFrame(ULV_CALUSER) == NULL)
	{
		CChildFrame* pChild = new CChildFrame(ULV_CALUSER, pTools, this);
		pChild->m_dwState &= ~ST_SHOW;
		if (!pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD, theApp.m_pMainWnd))
		{
			TRACE0("Warning : Couldn't create a frame of calibarte report.\n");
		}
		else
		{
			pChild->SetTitle(IDS_CALIBRATE_USER);
		}
	}	
	
	if (GetFirstFrame(ULV_SCHEDULE) == NULL)
	{
		CChildFrame* pChild = new CChildFrame(ULV_SCHEDULE, NULL, this);
		pChild->m_dwState &= ~ST_SHOW;
		if (!pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD,theApp.m_pMainWnd))
		{
			TRACE0("Warning : Count't create frame of perforation schedule view.\n");
		}
		else
		{
			pChild->SetTitle(IDS_PERFORATION_SCHEDULE);
		}
	}
	
	if (GetFirstFrame(ULV_CALCOEF) == NULL)
	{
		CChildFrame* pChild = new CChildFrame(ULV_CALCOEF, pTools, this);
		pChild->m_dwState &= ~ST_SHOW;
		if (!pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD, theApp.m_pMainWnd))
		{
			TRACE0("Warning : Count't create frame of calibration coefview.\n");
		}
		else
		{
			pChild->SetTitle(IDS_CALCOEF_REPORT);
		}
	}
	
	if (GetFirstFrame(ULV_OGRESULT) == NULL)
	{
		CChildFrame* pChild = new CChildFrame(ULV_OGRESULT, NULL, this);
		pChild->m_dwState &= ~ST_SHOW;
		if (!pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD, theApp.m_pMainWnd))
		{
			TRACE0("Warning : Count't create frame of calibration coefview.\n");
		}
		else
		{
			pChild->SetTitle(IDS_OGRESULT);
		}
	}
	
	if (GetFirstFrame(ULV_UHISOLID) == NULL)
	{
		CChildFrame* pChild = new CChildFrame(ULV_UHISOLID, NULL, this);
		pChild->m_dwState &= ~ST_SHOW;
		if (!pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD, theApp.m_pMainWnd))
		{
			TRACE0("Warning : Count't create frame of calibration coefview.\n");
		}
		else
		{
			pChild->SetTitle(IDS_UHISOLID);
		}
	}
	
	if (GetFirstFrame(ULV_LOGINFO) == NULL)
	{
		CCellInfo ci;
		LANGID langid = MAKELANGID(LANG_CHINESE, SUBLANG_CHINESE_SIMPLIFIED);
		if (Gbl_ConfigInfo.m_System.Language == langid)
		{
			if(m_nDriveMode==1)
			{
				ci.strTempl = _T("LogInformationCN.ulh");
			}
			else
			{
				ci.strTempl = _T("LogInformationTimeCN.ulh");//ADD BY XWH 时间模版
			}
			
		}
		else
		{
			if(m_nDriveMode==1)
			{
				ci.strTempl = _T("LogInformationEN.ulh");
			}
			else
			{
				ci.strTempl = _T("LogInformationTimeEN.ulh");
			}
		}

		ci.strFile.Empty();
		CChildFrame* pChild = new CChildFrame(ULV_LOGINFO, &ci, this);
		pChild->m_dwState &= ~ST_SHOW;
		if (!pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD, theApp.m_pMainWnd))
		{
			TRACE0("Warning : Count't create frame of LogInfo!\n");
		}
		else
		{
			pChild->SetWindowText("LogInfo");
			((CLogInformationManager*)pChild->m_pULView)->SetLogInfoHeight();
		}
	}
		
	// Add 2018-12-20
	if (GetFirstFrame(ULV_RUNDATA) == NULL)
	{
		theApp.SetStatusPrompt("run data");
		CChildFrame* pChild = new CChildFrame(ULV_RUNDATA, pTools, this);
		pChild->m_dwState &= ~ST_SHOW;
		if (!pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD, theApp.m_pMainWnd))
		{
			TRACE0("Warning : Couldn't create a frame of run data.\n");
		}
		else
		{
			pChild->SetTitle(IDS_RUNDATAVIEW);
		}
	}

}

void CULDoc::CloseFrames()
{
	POSITION pos = m_sheetiList.GetHeadPosition();
	for (; pos != NULL; )
	{
		CSheetInfo* pSheetInfo = m_sheetiList.GetNext(pos);
		if (pSheetInfo)
		{
			if (m_frmList.Find(pSheetInfo->pFrame))
				pSheetInfo->pFrame = NULL;

// 			if (pSheetInfo->pSheet)
// 				pSheetInfo->pSheet->ClearBackCurves();
		}
	}

	for (/*POSITION*/ pos = m_celliList.GetHeadPosition(); pos != NULL; )
	{
		CCellInfo* pCellInfo = m_celliList.GetNext(pos);
		if (pCellInfo)
		{
			if (m_frmList.Find(pCellInfo->pFrame))
				pCellInfo->pFrame = NULL;
		}
	}

	for (/*POSITION*/ pos = m_pofiList.GetHeadPosition(); pos != NULL; )
	{
		CPofInfo* pPofInfo = m_pofiList.GetNext(pos);
		if (pPofInfo)
		{
			if (m_frmList.Find(pPofInfo->pFrame))
				pPofInfo->pFrame = NULL;
		}
	}
	while (m_frmList.GetCount())
	{
		CChildFrame* pChild = m_frmList.RemoveTail();
		if (pChild->GetSafeHwnd())
		{
			pChild->m_pDoc = NULL;
			pChild->m_bDestroy = TRUE;
			pChild->SendMessage(WM_CLOSE);
			pChild = NULL;
		}
		else if (pChild != NULL)
		{
			delete pChild;
		}
	}
}

CView* CULDoc::GetFirstView(DWORD dwType, LPCTSTR pszTempl /* = NULL */)
{
	CChildFrame* pChild = GetFirstFrame(dwType, pszTempl);
	if (pChild != NULL)
	{		
		if (dwType == ULV_GRAPHCURVE)
		{
			return pChild->m_pGraphHeader;
		}

		return pChild->m_pULView;
	}

	return NULL;
}

CGraphWnd* CULDoc::GetGraphWnd(UINT nIndex /* = 0 */)
{
	if (nIndex >= m_frmList.GetCount())
		return NULL;
		
	int nCount = -1;
	for (POSITION pos = m_frmList.GetHeadPosition(); pos != NULL; )
	{
		CChildFrame* pChild = m_frmList.GetNext(pos);
		if (pChild != NULL && pChild->m_nViewType == ULV_GRAPHS)
			nCount++;
			
		if (nIndex == nCount)
			return pChild->m_pGraphWnd;
	}
	
	return NULL;
}

CULView* CULDoc::GetCustomizeView(UINT nIndex /* = 0 */)
{
	if (nIndex >= m_frmList.GetCount())
		return NULL;
		
	int nCount = -1;
	for (POSITION pos = m_frmList.GetHeadPosition(); pos != NULL; )
	{
		CChildFrame* pChild = m_frmList.GetNext(pos);
		if (pChild != NULL && pChild->m_nViewType == ULV_CUSTOMIZEVIEW)
			nCount++;
			
		if (nIndex == nCount)
			return pChild->m_pULView;
	}
	
	return NULL;
}


CGraphWnd* CULDoc::GetPrintGraph()
{
	for (POSITION pos = m_frmList.GetHeadPosition(); pos != NULL; )
	{
		CChildFrame* pChild = m_frmList.GetNext(pos);
		if (pChild != NULL && pChild->m_nViewType == ULV_GRAPHS)
		{
			if (pChild->m_pGraphWnd->m_bLoggingPrint)
				return pChild->m_pGraphWnd;
		}
	}
	
	return NULL;
}

void CULDoc::GetDepthRange(int nDirection, long& lDepth0, long& lDepth1, int nDriveMode/* = UL_DRIVE_DEPT*/)
{
	int nCurve = m_vecCurve.size();
	if (nDriveMode == UL_DRIVE_TIME)
	{
		for (int i = 0; i < nCurve; i++)
		{
			CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
			// 开始深度最小的
			long lDepth = pCurve->GetTime(0);
			if (lDepth != INVALID_DEPTH)
			{
				if (lDepth < lDepth0) 
					lDepth0 = lDepth;
				else if (0 == i)
					lDepth0 = lDepth;
			}
			
			// 结束深度最大的
			lDepth = pCurve->GetTimeED();
			if (lDepth != INVALID_DEPTH)
			{
				if (lDepth > lDepth1)	// 在上提时取得所有曲线数据中最浅的点	
					lDepth1 = lDepth;	// 在下放时取得最知曲线的结束值
				else if (0 == i)
					lDepth1 = lDepth;
			}
		}

		return ;
	}
	for (int i = 0; i < nCurve; i++)
	{
		CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
		if (nDirection == SCROLL_UP ) //上提
		{
			// 开始深度
			long lDepth = pCurve->GetDepth(0);
			if (lDepth != INVALID_DEPTH)
			{
				// 在上提时取得所有曲线数据中最大的点
				if (lDepth > lDepth0)	// 开始深度中最大的
					lDepth0 = lDepth;
				else if (0 == i)
					lDepth0 = lDepth;
			}
			
			// 结束深度
			lDepth = pCurve->GetDepthED();
			if (lDepth != INVALID_DEPTH)
			{
				if (lDepth < lDepth1)	// 结束深度中最小的	
					lDepth1 = lDepth;
				else if (0 == i)
					lDepth1 = lDepth;
			}
		}
		else // 下放
		{
			// 开始深度最小的
			long lDepth = pCurve->GetDepth(0);
			if (lDepth != INVALID_DEPTH)
			{
				if (lDepth < lDepth0) 
					lDepth0 = lDepth;
				else if (0 == i)
					lDepth0 = lDepth;
			}
			
			// 结束深度最大的
			lDepth = pCurve->GetDepthED();
			if (lDepth != INVALID_DEPTH)
			{
				if (lDepth > lDepth1)	// 在上提时取得所有曲线数据中最浅的点	
					lDepth1 = lDepth;	// 在下放时取得最知曲线的结束值
				else if (0 == i)
					lDepth1 = lDepth;
			}
		}
	}
}

void CULDoc::AdjustAllGraphs(BOOL bRemap /* = FALSE */, BOOL bInit /* = FALSE */)
{
	CULFile* pFile = DYNAMIC_DOWNCAST(CULFile, this);
	if (pFile == NULL)
		return;

	long lDepth0 = 0, lDepth1 = 0;

	if (pFile->m_pDCurve == NULL)
		return;
	
	CCurve* pCurve = pFile->m_pDCurve;
	int nData = pCurve->GetDataSize();

	if (bInit)
	{
		CChildFrame* pActive = DYNAMIC_DOWNCAST(CChildFrame, g_pMainWnd->GetActiveFrame());
		POSITION pos = m_frmList.GetHeadPosition();
		for (; pos != NULL; )
		{
			CChildFrame* pChild = m_frmList.GetNext(pos);
			if (pChild != NULL && pChild->m_nViewType == ULV_GRAPHS)
			{
				CGraphWnd* pGraph = pChild->m_pGraphWnd;
				if (nData > 0)
				{
					lDepth0 = pGraph->m_nDriveMode == UL_DRIVE_DEPT? pCurve->GetDepth(0) : pCurve->GetTime(0);
					lDepth1 = pGraph->m_nDriveMode == UL_DRIVE_DEPT? pCurve->GetDepth(nData-1) : pCurve->GetTime(nData-1);
				}
				if (::IsWindow(pGraph->GetSafeHwnd())/* && pGraph->m_bReset*/)
				{
					pGraph->m_pSheet->SetLogMode(WORKMODE_IDLE, pFile->m_nDriveMode);
			//		pGraph->AdjustViewDepth(lDepth0, lDepth1);
					pGraph->AdjustViewDepth();
					if (pChild == pActive)
						pGraph->Invalidate();
				}
			}
		}

		return;
	}

	// 文件内同时含有时间驱动以及深度驱动模版时，调整的显示范围不正确
	// add by bao 2013/7/24
//	GetDepthRange(pFile->m_nDirection, lDepth0, lDepth1);

	for (POSITION pos = m_frmList.GetHeadPosition(); pos != NULL; )
	{
		CChildFrame* pChild = m_frmList.GetNext(pos);
		if (pChild != NULL && pChild->m_nViewType == ULV_GRAPHS)
		{
			CGraphWnd* pGraph = pChild->m_pGraphWnd;
			if (::IsWindow(pGraph->GetSafeHwnd()))
			{
				GetDepthRange(pFile->m_nDirection, lDepth0, lDepth1, pGraph->m_nDriveMode);
				pGraph->AdjustViewDepth(lDepth0, lDepth1);
			}
		}
	}

	if (bRemap)
	{
		for (POSITION pos = m_sheetiList.GetHeadPosition(); pos != NULL; )
		{
			CSheetInfo* pSheetInfo = m_sheetiList.GetNext(pos);
			if (pSheetInfo->pSheet == NULL)
				continue;
			
			pSheetInfo->pSheet->RemapTrackCurve(m_vecCurve);
			pSheetInfo->pSheet->SetLogMode(WORKMODE_IDLE, pFile->m_nDriveMode);
		}
	}
}

CULView* CULDoc::GetPPView(CPrintParam* pParam, BOOL bInit /* = FALSE */)
{
	if (pParam->m_dwType & ULV_GRAPHS)
	{
		return GetGraphWnd(pParam->m_nParam);
	}

	if (pParam->m_dwType & ULV_CUSTOMIZEVIEW)
	{
		return GetCustomizeView(pParam->m_nParam);
	}

	CULView* pULView = (CULView*)GetFirstView(pParam->m_dwType, pParam->m_strParam);
	if (!bInit)
		return pULView;

	if (pParam->m_dwType & ULV_GRAPHHEAD)
	{
		if (pULView != NULL)
		{
			if (pULView->m_pEdit->GetSafeHwnd())
			{
				pULView->SetCellFitSize(pULView->m_pEdit);
			}
			
			return pULView;
		}

		// If we find the file in templates, auto add it
		CFileFind ff;
		if (ff.FindFile(HTS_FILE(pParam->m_strParam)))
		{
			CCellInfo* pCellInfo = AddCellTempl(pParam->m_strParam, AT_FRM);
			if (pCellInfo)
			{
				if (g_pMainWnd->m_wndWorkspace.m_wndTree.GetSafeHwnd())
				{
					g_pMainWnd->m_wndWorkspace.m_wndTree.RefreshDocHeaders(this);
				}
				return pCellInfo->pFrame->m_pULView;
			}
		}
	}
// 	else if (pParam->m_dwType & ULV_CUSTOMIZEVIEW)
// 	{
// 		if (pULView != NULL)
// 			return pULView;
// 
// 		// If we find the file in templates, auto add it
// 		CFileFind ff;
// 		if (ff.FindFile(ACV_FILE(pParam->m_strParam)))
// 		{
// 			CCstInfo* pCstInfo = AddCstTempl(pParam->m_strParam, AT_FRM);
// 			if (pCstInfo)
// 			{
// 				if (g_pMainWnd->m_wndWorkspace.m_wndTree.GetSafeHwnd())
// 				{
// 					g_pMainWnd->m_wndWorkspace.m_wndTree.RefreshDocHeaders(this);
// 				}
// 				return pCstInfo->pFrame->m_pULView;
// 			}
// 		}
// 	}

	return pULView;
}

void CULDoc::UpdateViews(DWORD dwType)
{
	for (POSITION pos = m_frmList.GetHeadPosition(); pos != NULL; )
	{
		CChildFrame* pChild = m_frmList.GetNext(pos);
		if (pChild->m_pULView->GetSafeHwnd() && (pChild->m_nViewType & dwType))
		{
			pChild->m_pULView->OnUpdate();
		}
	}
}

void CULDoc::UpdateCells(DWORD dwType)
{
	for (POSITION pos = m_frmList.GetHeadPosition(); pos != NULL; )
	{
		CChildFrame* pChild = m_frmList.GetNext(pos);
		if (pChild->m_pULView->GetSafeHwnd() && (pChild->m_nViewType & dwType))
		{
			pChild->m_pULView->OnUpdateCell();
		}
	}
}

CPtrArray* CULDoc::GetTools()
{
	if (m_pProject != NULL)
	{
		return &m_pProject->m_ULKernel.m_arrTools;
	}

	return NULL;
}

BOOL CULDoc::OnAdjustView(CGraphWnd* pGraph)
{
	if ((pGraph == NULL) || pGraph->m_bIsLogging)
		return FALSE;

	CSheet* pSheet = pGraph->m_pSheet;
	
	// add by bao 2013-7-23 时间驱动下调整显示范围
	if (pSheet->m_nDriveMode == UL_DRIVE_TIME)
	{
		CDlgRangeTime dlg;
		dlg.m_lTime0 = pSheet->m_lStartTime;
		dlg.m_lTime1 = pSheet->m_lEndTime;
		if (pGraph->m_pDoc && pGraph->m_pDoc->m_pDCurve)
		{
			CCurve* pTCurve = pGraph->m_pDoc->m_pTCurve == NULL? pGraph->m_pDoc->m_pDCurve : pGraph->m_pDoc->m_pTCurve;
			if (pTCurve == NULL)
			{
				return FALSE;
			}
			int nData = pTCurve->GetDataSize();
			if (nData > 0)
			{
				long lTime0 = pTCurve->GetTime(0);
				long lTime1 = pTCurve->GetTime(nData - 1);
				if (lTime0 < lTime1)
				{
					dlg.m_lTimeA0 = lTime0;
					dlg.m_lTimeA1 = lTime1;
				}
				else if (lTime1 < lTime0)
				{
					dlg.m_lTimeA0 = lTime1;
					dlg.m_lTimeA1 = lTime0;
				}
			}
		}
		
		if (dlg.DoModal() == IDOK)
		{
			if ((dlg.m_lTime0 == pSheet->m_lStartTime)
				&& (dlg.m_lTime1 == pSheet->m_lEndTime))
			{
				return FALSE;
			}
			
			pSheet->m_lStartTime = dlg.m_lTime0;
			pSheet->m_lEndTime = dlg.m_lTime1;
			pSheet->SetDepthRange(dlg.m_lTime0, dlg.m_lTime1);
			pGraph->SetScrollInformation();
			if (::IsWindow(pGraph->GetSafeHwnd()))
				pGraph->Invalidate();
			
			return TRUE;
		}
	}
	else
	{
		CDlgRange dlg;
		dlg.m_lDepth0 = pSheet->m_lStartDepth;
		dlg.m_lDepth1 = pSheet->m_lEndDepth;
		if (pGraph->m_pDoc && pGraph->m_pDoc->m_pDCurve)
		{
			CCurve* pDCurve = pGraph->m_pDoc->m_pDCurve;
			int nData = pDCurve->GetDataSize();
			if (nData > 0)
			{
				long lDepth0 = pDCurve->GetDepth(0);
				long lDepth1 = pDCurve->GetDepth(nData - 1);
				if (lDepth0 < lDepth1)
				{
					dlg.m_lDepthA0 = lDepth0;
					dlg.m_lDepthA1 = lDepth1;
				}
				else if (lDepth1 < lDepth0)
				{
					dlg.m_lDepthA0 = lDepth1;
					dlg.m_lDepthA1 = lDepth0;
				}
			}

			if(dlg.m_lDepthA0 > dlg.m_lDepth0)
				dlg.m_lDepthA0 = dlg.m_lDepth0;

			if(dlg.m_lDepthA1 < dlg.m_lDepth1)
				dlg.m_lDepthA1 = dlg.m_lDepth1;
		}
		
		if (dlg.DoModal() == IDOK)
		{
			if ((dlg.m_lDepth0 == pSheet->m_lStartDepth)
				&& (dlg.m_lDepth1 == pSheet->m_lEndDepth))
			{
				return FALSE;
			}
			
			pSheet->m_lStartDepth = dlg.m_lDepth0;
			pSheet->m_lEndDepth = dlg.m_lDepth1;
			pSheet->SetDepthRange(dlg.m_lDepth0, dlg.m_lDepth1);
			pGraph->SetScrollInformation();
			if (::IsWindow(pGraph->GetSafeHwnd()))
				pGraph->Invalidate();
			
			return TRUE;
		}
		
	}

	return FALSE;
}

CPof* CULDoc::GetPOF(int nIndex)
{
	POSITION pos = m_pofiList.FindIndex(nIndex);
	if (pos != NULL)
	{
		CPofInfo* pPofInfo = m_pofiList.GetAt(pos);
		return pPofInfo->pPof;
	}
	return NULL;
}

void CULDoc::PreparePrint(CSheet* pSheet, BOOL bRecalcGaps /* = FALSE */)
{
	CCurve* pDCurve = m_pDCurve;

	if(Gbl_ConfigInfo.m_Plot.bGapUseTimeCurve)
	{
		if(m_pTCurve != NULL)
		{
			pDCurve = m_pTCurve;
		}
		else
		{
			for(int i = 0 ; i < m_vecCurve.size() ; i++)
			{
				CCurve *pTempCurve = (CCurve *)m_vecCurve.at(i);
				CString strCurveName = pTempCurve->Name();
				strCurveName.MakeUpper();
				if(strCurveName == "TIME")
				{
					pDCurve = pTempCurve;
					break;
				}
			}
		}
	}

	CTrack::m_lTD = INVALID_DEPTH;
	
	if (m_pProject != NULL)
	{
		int x = m_pProject->m_nCasingPrc - 1;
		double d = 0;
		CString string;
		m_pProject->GetInformation("DEPTH_DRILLER", &string);
		if (string.GetLength())
			d = g_units->LMValueParse(string);
		if (d < m_pProject->m_fCasingDepth[x])
			d = m_pProject->m_fCasingDepth[x];
		CTrack::m_lTD = floor(d + .5);
	}

	long lMinDepth = pSheet->m_lStartDepth;
	long lMaxDepth = pSheet->m_lEndDepth;

	// 得出打印深度范围内的缺口的深度by zjp 05,04,13。
	if (bRecalcGaps)
	{
		m_GapDepthArray.RemoveAll();
		int nData = pDCurve ? pDCurve->GetDataSize() : 0;
		if (nData > 0)
		{
			int nGapCount = 0;
			long lTimeST = 0;
			
			if(Gbl_ConfigInfo.m_Plot.bGapUseTimeCurve)
				lTimeST = pDCurve->GetValue(0);
			else
				lTimeST = pDCurve->GetTime(0);
						
			long lTimeIntervals = Gbl_ConfigInfo.m_Plot.PrintGapInterVal * 1000;
			if (lTimeIntervals == 0)
			{
				lTimeIntervals = 1.0;
			}
			for (int i = 0; i < nData; i++)
			{
				long lDepth = pDCurve->GetDepth(i);

				if (lDepth < lMinDepth)
					continue;
				
				if (lDepth > lMaxDepth)
					continue;
				
				long lTime = 0;
				if(Gbl_ConfigInfo.m_Plot.bGapUseTimeCurve)
					lTime = pDCurve->GetValue(i);
				else
					lTime = pDCurve->GetTime(i);

				if (abs(lTime - lTimeST)/lTimeIntervals > nGapCount)
				{
					nGapCount++;
					m_GapDepthArray.Add(lDepth);
					// TRACE("Depth: %f\n", DTM(lDepth));
				}
			}
		}
	}
	
	int nTrack = pSheet->m_TrackList.GetSize();
	if (nTrack < 1)
		return;
	
	CTrack* pTrack = NULL;
	CCurve* pCurve = NULL;
	for (int i = 0; i < nTrack; i++)
	{
		pTrack = (CTrack *)pSheet->m_TrackList.GetAt (i);
		pTrack->m_bRTPrint = FALSE;
		pTrack->m_bPrintGap = Gbl_ConfigInfo.m_Plot.PrintGap;
		pTrack->m_nGapSize = Gbl_ConfigInfo.m_Plot.PrintGapSize;	
		pTrack->m_nTrackIndex = i ? 0 : TI_FIRST;
		pTrack->m_pGapDepthArray = &m_GapDepthArray;
		int nCurve = pTrack->m_vecCurve.size();
		for (int j = 0; j < nCurve; j++)
		{
			CCurve* pCurve = (CCurve*)pTrack->m_vecCurve.at(j);
			pCurve->m_nCurPrintPos = 0;	// 打印索引
		}
	}
	pTrack->m_nTrackIndex |= TI_LAST;
}

int CULDoc::Trace(UINT nID, DWORD dwType, UINT nEvent, UINT nResult, LPCTSTR pszRemark /* = NULL */)
{
	return AEI(nID, theApp.m_pszAppName, dwType, nEvent, nResult, pszRemark);
}

int CULDoc::Trace(UINT nID, DWORD dwType, UINT nEvent, LPCTSTR pszResult, LPCTSTR pszRemark /* = NULL */)
{
	return AEI(nID, theApp.m_pszAppName, dwType, nEvent, pszResult, pszRemark);
}

BOOL CULDoc::IsNoData()
{
	if (m_pDCurve)
	{
		if (m_pDCurve->GetDataSize())
		{
			return FALSE;
		}

		return TRUE;
	}
	return FALSE; 
}
void CULDoc::SaveDatas()
{
	for (int i = 1; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
		pCurve->m_pData->MSave();
	}
}

void CULDoc::ReleaseCurvesData()
{
	CULFile* pFile = DYNAMIC_DOWNCAST(CULFile, this);
	if (pFile != NULL)
	{
		for (int i = 1; i < m_vecCurve.size(); i++)
		{
			CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
			if ((pCurve != NULL) && (!pCurve->IsDepthCurve()))
			{
				pCurve->m_pData->MRelease();
			}
		}
	}
	else
	{
		if (m_pProject && m_pProject->m_ULKernel.m_nArrivePoint > 512)
		{
			for (int i = 0; i < m_pProject->m_ULKernel.m_vecData.size(); i++)
			{
				CCurveData* pData = m_pProject->m_ULKernel.m_vecData.at(i);
				if (pData != m_pProject->m_ULKernel.m_pDepthData)
					pData->MRelease();
			}
		}
	}

}

CString CULDoc::GetToolSNByCurve(CString strCurve)
{
    int nCurve = m_vecCurve.size();
	for (int j = 0; j < nCurve; j++)
	{
		CCurve* pCurve = (CCurve*)m_vecCurve.at(j);
		CString str = pCurve->m_strName;
		if (str.CompareNoCase(strCurve) == 0)
		{
			CULTool* pTool = (CULTool*)m_pProject->m_ULKernel.GetTool(pCurve->m_strSource);
			return pTool->strSN;
		}
	}
	return "";
}

CString CULDoc::GetToolANByCurve(CString strCurve)
{
  int nCurve = m_vecCurve.size();
	for (int j = 0; j < nCurve; j++)
	{
		CCurve* pCurve = (CCurve*)m_vecCurve.at(j);
		CString str = pCurve->m_strName;
		if (str.CompareNoCase(strCurve) == 0)
		{
			CULTool* pTool = (CULTool*)m_pProject->m_ULKernel.GetTool(pCurve->m_strSource);
			return pTool->strAssetNo;
		}
	}
	return "";
}

CPofInfo* CULDoc::GetPofInfo(LPCTSTR pszTempl)
{
	for (POSITION pos = m_pofiList.GetHeadPosition(); pos != NULL; )
	{
		CPofInfo* pPofInfo = m_pofiList.GetNext(pos);
		if (pPofInfo->strTempl == pszTempl)
			return pPofInfo;
	}
	return NULL;
}

void CULDoc::GetPofTempls(CStringArray* pTempls)
{
		for (POSITION pos = m_pofiList.GetHeadPosition(); pos != NULL; )
		{
			CPofInfo* pPofInfo = m_pofiList.GetNext(pos);
			if (pPofInfo != NULL)
				pTempls->Add(pPofInfo->strTempl);
		}
	}

CPofInfo* CULDoc::RemovePofInfo(LPCTSTR pszTempl)
{
		for (POSITION pos = m_pofiList.GetHeadPosition(); pos != NULL; )
		{
			POSITION pos1 = pos;
			CPofInfo* pPofInfo = m_pofiList.GetNext(pos);
			if (pPofInfo->strTempl == pszTempl)
			{
				m_pofiList.RemoveAt(pos1);
				return pPofInfo;
			}
		}
		return NULL;
	}

void CULDoc::ClearPofInfo()
{
	while (m_pofiList.GetCount())
	{
		delete m_pofiList.RemoveHead();
	}
}

CString CULDoc::GetUniqueName(CString strName)
{
    UINT nIndex = 1; 
    CString strNewName = strName;
	if (!IsCurveNameExist(strNewName))
		return strNewName;

    strNewName.Format("%s%d", strName, nIndex++); 
    while (IsCurveNameExist(strNewName))
        strNewName.Format("%s%d", strName, nIndex++);
	
    return strNewName;
}

BOOL CULDoc::IsCurveNameExist(CString strName)
{
    for (int i = 0; i < m_vecCurve.size(); i++)
    {
        if (m_vecCurve.at(i)->Name() == strName)
            return TRUE;
    }

    return FALSE; 
}

CCurve* CULDoc::GetCurve(CString strName)
{
    CCurve *pCurve = NULL;
	for (int i = 0; i < m_vecCurve.size(); i++)
    {
        if (m_vecCurve.at(i)->Name() == strName)
		{
			pCurve = (CCurve *)m_vecCurve.at(i);
		}
    }

    return pCurve; 
}

void CULDoc::SolidCompute()
{
	int nID = 0;
	double fStep = PI * 2.0 / FRAME_SIZE;
	m_nDisplayRange = MTD(5.0);
	m_fRatio = 1;

	//获得幅度曲线
	CString strName = "RAMP";
	CCurve* pAmpCurve = NULL;
	
	if(!IsCurveNameExist(strName))
	{	
		//AfxMessageBox("文件中不存在名为 RAMP 的曲线");
		CString str;
		str.LoadString(IDS_STRING60068);
		AfxMessageBox(str);
		return;
	}
	else
	{
		pAmpCurve = GetCurve(strName);
	}

	//获得到时曲线
	strName = "RTIM";
	CCurve* pTimeCurve = NULL;
	if(!IsCurveNameExist(strName))
	{
		//AfxMessageBox("文件中不存在名为 RTIM 的曲线");
		CString str;
		str.LoadString(IDS_STRING60069);
		AfxMessageBox(str);
		return;
	}
	else
	{
		pTimeCurve = GetCurve(strName);
	}

	//计算各个顶点
	long lRange = MTD(500.0);
	long lDepth = 0.0f;
	float *pTime = NULL;
	float *pColor = NULL;
	BYTE bColorRank = 0;
	int nDataLen = pAmpCurve->GetDataSize();

	for(int i = 0 ; i < nDataLen ; i++)
	{
		DRAW_INFO *pDrawInfo = new DRAW_INFO;
		m_VertexList.Add (pDrawInfo);
		lDepth = pAmpCurve->GetDepth(i);
		pDrawInfo->nDepth = lDepth;
		pDrawInfo->nCoordinate = lDepth / 5;//* DISPLAY_MAP;
		
		pColor = (float *)pAmpCurve->GetValueBuffer(i);
		pTime = (float *)pTimeCurve->GetValueBuffer(i);

		for(int j = 0 ; j < FRAME_SIZE ; j++)
		{
			pTime[j] = 130;
			
			if(pTime[j] < 150)
			{
				pDrawInfo->VertexArray[j].x = (float) sin(j * fStep) * pTime[j] / 5 * 5;	
				pDrawInfo->VertexArray[j].z = (float) cos(j * fStep) * pTime[j] / 5 * 5;
			}
			else
			{
				pTime[j] = 130;
				pDrawInfo->VertexArray[j].x = 0;	
				pDrawInfo->VertexArray[j].z = 0;
			}

			pDrawInfo->VertexArray[j].bColorRank = (5000.0f - pColor[j]) * 256 / 5000;
			pDrawInfo->VertexArray[j].bTime = (BYTE)(pTime[j] / 4);
			
		}
	}

	
	//设置显示深度
	DRAW_INFO* pDrawInfo = (DRAW_INFO *)m_VertexList.GetAt(0);
	long nStartDepth = pDrawInfo->nDepth ;
	pDrawInfo = (DRAW_INFO *)m_VertexList.GetAt(m_VertexList.GetSize() - 1);
	long nEndDepth = pDrawInfo->nDepth ;
	
	SetDisplayDepth((nStartDepth + nEndDepth) / 2.0f);
	SetDisplayPosition();
}

void CULDoc::SetDisplayPosition ()
{
	if(m_VertexList.GetSize() <= 0)
		return ;

	DRAW_INFO* pDrawInfo = (DRAW_INFO *)m_VertexList.GetAt(0);
	float fStartDepth = pDrawInfo->nDepth ;
	pDrawInfo = (DRAW_INFO *)m_VertexList.GetAt(m_VertexList.GetSize() - 1);
	float fEndDepth = pDrawInfo->nDepth ;
	BOOL bIsLogUp;
	if(fStartDepth > fEndDepth)
		bIsLogUp = TRUE;
	else
		bIsLogUp = FALSE;
	if(bIsLogUp)
	{
		m_nStartPos = 0;
		for(int i = 0 ; i < m_VertexList.GetSize () ; i++)
		{
			pDrawInfo = (DRAW_INFO *)m_VertexList.GetAt(i);
			m_nStartPos = i;
			if(m_nDisplayDepth + m_nDisplayRange / 2.0f >= pDrawInfo->nDepth )
			{
				break;
			}
		}
		m_nEndPos = m_nStartPos;
		for(i = m_nStartPos ; i < m_VertexList.GetSize () ;i++)
		{
			pDrawInfo = (DRAW_INFO *)m_VertexList.GetAt(i);
			m_nEndPos = i;
			if(m_nDisplayDepth - m_nDisplayRange / 2.0f >= pDrawInfo->nDepth )
			{
				break;
			}
		}
	}
	else
	{
		m_nStartPos = 0;
		for(int i = 0 ; i < m_VertexList.GetSize () ; i++)
		{
			pDrawInfo = (DRAW_INFO *)m_VertexList.GetAt(i);
			if(m_nDisplayDepth - m_nDisplayRange / 2.0f <= pDrawInfo->nDepth )
			{
				m_nStartPos = i;
				break;
			}
		}
		m_nEndPos = m_nStartPos;
		for(i = m_nStartPos ; i < m_VertexList.GetSize () ;i++)
		{
			pDrawInfo = (DRAW_INFO *)m_VertexList.GetAt(i);
			if(m_nDisplayDepth + m_nDisplayRange / 2.0f <= pDrawInfo->nDepth )
			{
				m_nEndPos = i;
				break;
			}
		}
	}
}

void CULDoc::SetDisplayDepth (long nDepth)
{
	m_nDisplayDepth = nDepth;	
}

void CULDoc::ClearSolidInfo()
{
	for(int i = 0 ; i < m_VertexList.GetSize() ; i ++)
	{
		DRAW_INFO *pDrawInfo = (DRAW_INFO *)m_VertexList.GetAt(i);
		delete pDrawInfo;
	}

	m_VertexList.RemoveAll();
}

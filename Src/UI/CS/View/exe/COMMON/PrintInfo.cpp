// PrintInfo.cpp: implementation of the CULPrintInfo class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PrintInfo.h"
#include "resource.h"

#include <math.h>
#include <winspool.h>
#include <windows.h>
#include <atlconv.h>
#include <afxdlgs.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CULPrintInfo::CULPrintInfo()
{
	m_pMemDC = NULL;
	m_nPageNO = 0;					// 当前页号
	m_nDirection = SCROLL_UP;		// (测井)打印方向
	m_nStartPrintPos = 0;
	m_fPrintReviseX = 1;			// 打印机X方向刻度系数
	m_fPrintReviseY = 1;			// 打印机Y方向刻度系数
	m_nRatio = 200;

	m_bCalibrated = FALSE;
	m_dwMode = PM_FILE;				// 文件打印
	m_nPageRH = 125;
	m_bStartDoc = FALSE;
	m_strDevice.Empty();
	m_strDoc.Empty();
	m_hDCDef = NULL;
	m_strDef.Empty();
	m_bColorDef = FALSE;
	m_hDCPdf = NULL;
	m_strPdf = "Adobe PDF";
	m_bColorPdf = TRUE;
	m_bColor = FALSE;
	m_fUnit = 1;

	CWindowDC dc(NULL);
	m_nDDC = GetDeviceCaps(dc.GetSafeHdc(), LOGPIXELSY);

	m_bTimeTop = TRUE;
}

CULPrintInfo::~CULPrintInfo()
{
	ReleasePrintDC();
}

void CULPrintInfo::Init()
{
	m_nPageNO = 0	;				 // 当前页号
	m_nDirection = SCROLL_UP; 		 //(测井)打印方向
	m_fPrintReviseX = 1;			 //打印机X方向刻度系数
	m_fPrintReviseY = 1;			 //打印机Y方向刻度系数
}

BOOL CULPrintInfo::PrinterCalibrate(int  nMillimeter)
{
	if (!m_printDC.GetSafeHdc())
	{
		return	FALSE;
	}

	m_bCalibrated = TRUE;
	nMillimeter *= 10; //转化为0.1MM

	CString strDoc = AfxGetAppName();
	CString str;
	str.LoadString(IDS_PRINTER_CAILIBRATION);// Printer Calibration
	strDoc += " " + str;

	DOCINFO di;
	::ZeroMemory(&di, sizeof(DOCINFO));
	di.cbSize = sizeof(DOCINFO);
	di.lpszDocName = strDoc;
	if (m_printDC.StartDoc(&di) < 0)
	{
		AfxMessageBox(AFX_IDP_FAILED_TO_START_PRINT);
		m_printDC.Detach();
		m_strDevice.Empty();
		return FALSE;
	}

	m_printDC.SaveDC();
	m_printDC.StartPage();
	m_printDC.SetMapMode(MM_LOMETRIC);		
	m_printDC.SetWindowOrg(0, 0);			

	PrintText(strDoc,
		CRect(m_rcPage.left, m_rcPage.top, m_rcPage.right, m_rcPage.top - 100),
		CSize(0, 50));

	int nHeight = -200;
	CString strText;
	///////////////////////////////////////////////
	// 竖向坐标轴
	m_printDC.MoveTo(m_rcPage.Width() / 2, nHeight);
	m_printDC.LineTo(m_rcPage.Width() / 2, nHeight - nMillimeter); 

	int nNum = nMillimeter / 100 / 2;
	int i = nHeight;
	for (; i >= nHeight - nMillimeter; i -= 100)
	{
		//开始
		if (i == nHeight)
		{
			strText.Format(_T("%dcm"), nNum);
			m_printDC.MoveTo(m_rcPage.Width() / 2 - 50, i);
			m_printDC.LineTo(m_rcPage.Width() / 2 + 50, i);
			PrintText(CSize(0, 32), m_rcPage.Width() / 2 - 10, i - 10, strText);
		}
		//结束
		else if (i == nHeight - nMillimeter)
		{
			strText.Format(_T("%dcm"), nNum);
			m_printDC.MoveTo(m_rcPage.Width() / 2 - 50, i);
			m_printDC.LineTo(m_rcPage.Width() / 2 + 50, i);
			PrintText(CSize(0, 32), m_rcPage.Width() / 2 - 10, i + 40, strText);
		}
		else
		{
			m_printDC.MoveTo(m_rcPage.Width() / 2 - 25, i);
			m_printDC.LineTo(m_rcPage.Width() / 2 + 25, i);
		}

		nNum --;
	}
	///////////
	//横向坐标
	int nHorzPos = nHeight - nMillimeter / 2;
	m_printDC.MoveTo(m_rcPage.Width() / 2 - 500, nHorzPos);
	m_printDC.LineTo(m_rcPage.Width() / 2 + 500, nHorzPos);
	nNum = -5;
	for (i = m_rcPage.Width() / 2 - 500;
		i <= m_rcPage.Width() / 2 + 500;
		i += 100)
	{
		if (i == m_rcPage.Width() / 2 - 500)
		{
			strText.Format(_T("%dcm"), nNum);
			m_printDC.MoveTo(i, nHorzPos + 50);
			m_printDC.LineTo(i, nHorzPos - 50);
			PrintText(CSize(0, 32), i + 70, nHorzPos + 30, strText);
		}
		else if (i == m_rcPage.Width() / 2 + 500)
		{
			strText.Format(_T("%dcm"), nNum);
			m_printDC.MoveTo(i, nHorzPos + 50);
			m_printDC.LineTo(i, nHorzPos - 50);
			PrintText(CSize(0, 32), i - 10, nHorzPos + 30, strText);
		}
		else if (i == m_rcPage.Width() / 2)
		{
			strText.Format(_T("%dcm"), nNum);			
			PrintText(CSize(0, 32), i + 60, nHorzPos + 30, strText);
		}
		else
		{
			m_printDC.MoveTo(i, nHorzPos + 25);
			m_printDC.LineTo(i, nHorzPos - 25);
		}
		nNum++;
	}

	int nTextPos = nHeight - nMillimeter - 100;
	str.LoadString(IDS_PRINTER_CORRECTION);//Horizontal and Vertical Correction Plans.
	PrintText(str, CRect(0, nTextPos, m_rcPage.Width(), nTextPos - 100), CSize(0, 50));

	int nRGBPos = nTextPos - 200;
	//灰度校正图	
	for (i = 2; i <= 256; i *= 2)
	{
		str.LoadString(IDS_LEVEL);//%d Level
		strText.Format(str, i);
		PrintText(CSize(0, 32), m_rcPage.Width() / 2 - 600, nRGBPos, strText);
		for (int j = 0; j < i; j++)
		{
			int nColorVal = 255 / (i - 1) * j;
			COLORREF color = RGB(nColorVal, nColorVal, nColorVal);
			int nRectLeft = m_rcPage.Width() / 2 - 500 + 1000 / i* j;
			CRect rect(nRectLeft, nRGBPos, nRectLeft + 1000 / i, nRGBPos - 50);
			m_printDC.FillSolidRect(rect, color);
		}

		////////////////////////////////////
		nRGBPos -= 50;
	}


	nRGBPos -= 100;
	str.LoadString(IDS_PRINTER_GRAY_CORRECTION);//Gray Correction Plan
	PrintText(str, CRect(0, nRGBPos, m_rcPage.Width(), nRGBPos - 100), CSize(0, 50));

	PrintText("Printer Calibration End",
		CRect(0, nRGBPos, m_rcPage.Width(), nRGBPos - 200), CSize(0, 50));

	m_printDC.EndPage();
	m_printDC.RestoreDC(-1);

	m_printDC.EndDoc();

	m_bCalibrated = FALSE;

	return TRUE;
}

// 打印机刻度，打印打印机刻度前后的事例图
BOOL CULPrintInfo::PrinterCalibration(const int& nHLength,
	const int& nVLength, const int& nGrade, const BOOL bCal)
{
	CString strTitle;
	strTitle.LoadString(AFX_IDS_APP_TITLE);
	strTitle += " Printer Calibration";
	if (!StartPrintDoc(strTitle))
	{
		return	FALSE;
	}

	m_bCalibrated = TRUE;
	m_printDC.SaveDC();
	m_printDC.StartPage();
	if (m_fUnit == 1.0)
	{
		m_printDC.SetMapMode(MM_LOMETRIC);//	MM_LOMETRIC);	
	}
	else
	{
    	m_printDC.SetMapMode(MM_LOENGLISH);//	MM_LOMETRIC);
		m_rcPage.right = m_rcPage.right / 2.54;
		m_rcPage.bottom = m_rcPage.bottom / 2.54;
		
	}
	m_printDC.SetWindowOrg(0, 0);			

	PrintText(strTitle,
		CRect(m_rcPage.left, m_rcPage.top, m_rcPage.right, m_rcPage.top - 100 * m_fUnit),
		CSize(0, 50 * m_fUnit));

	int nHeight = 0;
	float fNum;
	CString strText;
	nHeight -= 200 * m_fUnit;

	/* // 画边线(为了走纸),  使用标准打印机驱动，不必画线走纸,  shenkun remove, 2003 - 10 - 8 
	m_printDC.MoveTo(PAGE_MARGIN,0);
	m_printDC.LineTo(PAGE_MARGIN,-1 * 2 * nMillimeter); 
	m_printDC.MoveTo(m_rcPage.right - PAGE_MARGIN,nHeight);
	m_printDC.LineTo(m_rcPage.right - PAGE_MARGIN,-1 * 2 * nMillimeter); */

	// 竖向坐标轴
	int nVCalLength = nVLength * 10;	 // 转化为0.1MM/0.01in
	CPoint pt;
	pt.x = m_rcPage.Width() / 2;
	pt.y = nHeight; 

	if (bCal)
	{
		CoordinateCalibrate(pt);
	}
	// m_printDC.MoveTo( m_rcPage.Width() / 2, nHeight );
	// m_printDC.LineTo( m_rcPage.Width() / 2, nHeight - nVCalLength ); 
	m_printDC.MoveTo(pt);
	pt.x = m_rcPage.Width() / 2;
	pt.y = nHeight - nVCalLength; 
	if (bCal)
	{
		CoordinateCalibrate(pt);
	}
	m_printDC.LineTo(pt);

	fNum = nVCalLength / 100.0 / 2.0;
	int i = nHeight;
	for (; i >= nHeight - nVCalLength - 10 * m_fUnit; i -= 100)
	{
		if (i <= nHeight - nVCalLength)
		{
            i = nHeight - nVCalLength;
		}

		//开始
		if (i == nHeight)
		{
			strText.Format(_T("%.1f%s"), fNum,m_strUnit);

			pt.x = m_rcPage.Width() / 2 - 50 * m_fUnit;
			pt.y = i; 
			if (bCal)
			{
				CoordinateCalibrate(pt);
			}
			m_printDC.MoveTo(pt);
			pt.x = m_rcPage.Width() / 2 + 50 * m_fUnit;
			pt.y = i; 
			if (bCal)
			{
				CoordinateCalibrate(pt);
			}
			m_printDC.LineTo(pt);

			// m_printDC.MoveTo(m_rcPage.Width()/2 -50,i);
			// m_printDC.LineTo(m_rcPage.Width()/2 +50,i);
			pt.x = m_rcPage.Width() / 2 - 10  * m_fUnit;
			pt.y = i - 10  * m_fUnit; 
			if (bCal)
			{
				CoordinateCalibrate(pt);
			}

			PrintText(CSize(0, 32  * m_fUnit), pt.x, pt.y, strText);
		}
		//结束
		else if (fabs(i - nHeight + nVCalLength) < 10  * m_fUnit)
		{
			strText.Format(_T("%.1f%s"), fNum,m_strUnit);


			pt.x = m_rcPage.Width() / 2 - 50  * m_fUnit;
			pt.y = i; 
			if (bCal)
			{
				CoordinateCalibrate(pt);
			}
			m_printDC.MoveTo(pt);
			pt.x = m_rcPage.Width() / 2 + 50  * m_fUnit;
			pt.y = i; 
			if (bCal)
			{
				CoordinateCalibrate(pt);
			}
			m_printDC.LineTo(pt);

			// m_printDC.MoveTo(m_rcPage.Width()/2 -50,i);
			// m_printDC.LineTo(m_rcPage.Width()/2 +50,i);
			pt.x = m_rcPage.Width() / 2 - 10  * m_fUnit;
			pt.y = i + 40  * m_fUnit; 
			if (bCal)
			{
				CoordinateCalibrate(pt);
			}

			PrintText(CSize(0, 32  * m_fUnit), pt.x, pt.y, strText);
			/*
							m_printDC.MoveTo(m_rcPage.Width()/2 -50,i);
							m_printDC.LineTo(m_rcPage.Width()/2 +50,i);
							PrintText(CSize(0,32),m_rcPage.Width()/2 - 10,  i ,  strText );
							*/
		}
		else
		{
			pt.x = m_rcPage.Width() / 2 - 25  * m_fUnit;
			pt.y = i; 
			if (bCal)
			{
				CoordinateCalibrate(pt);
			}
			m_printDC.MoveTo(pt);
			pt.x = m_rcPage.Width() / 2 + 25  * m_fUnit;
			pt.y = i; 
			if (bCal)
			{
				CoordinateCalibrate(pt);
			}
			m_printDC.LineTo(pt);
			/*
					m_printDC.MoveTo(m_rcPage.Width()/2 - 25,i);
					m_printDC.LineTo(m_rcPage.Width()/2 + 25,i);
					*/
		}	  
		fNum = fNum - 1.0;
	}

	// 横向坐标
	int nHCalLength = nHLength * 10;

	int nHorzPos = nHeight - nVCalLength / 2;

	pt.x = m_rcPage.Width() / 2 - nHCalLength / 2;
	pt.y = nHorzPos; 
	if (bCal)
	{
		CoordinateCalibrate(pt);
	}
	m_printDC.MoveTo(pt);
	pt.x = m_rcPage.Width() / 2 + nHCalLength / 2;
	pt.y = nHorzPos; 
	if (bCal)
	{
		CoordinateCalibrate(pt);
	}
	m_printDC.LineTo(pt);


	// m_printDC.MoveTo(m_rcPage.Width()/2 -500, nHorzPos);
	// m_printDC.LineTo(m_rcPage.Width()/2 +500, nHorzPos);
	fNum = -nHCalLength / 100.0 / 2.0;    //	nNum = -5;
	for (i = m_rcPage.Width() / 2 - nHCalLength / 2;
		i <= m_rcPage.Width() / 2 + nHCalLength / 2 + 10  * m_fUnit;
		i += 100)
	{
	    if (i >= m_rcPage.Width() / 2 + nHCalLength / 2)
	    {
            i = m_rcPage.Width() / 2 + nHCalLength / 2;
	    }
		if (i == m_rcPage.Width() / 2 - nHCalLength / 2)
		{
			strText.Format(_T("%.1f%s"), fNum,m_strUnit);

			pt.x = i;
			pt.y = nHorzPos + 50  * m_fUnit; 
			if (bCal)
			{
				CoordinateCalibrate(pt);
			}
			m_printDC.MoveTo(pt);
			pt.x = i;
			pt.y = nHorzPos - 50  * m_fUnit; 
			if (bCal)
			{
				CoordinateCalibrate(pt);
			}
			m_printDC.LineTo(pt);

			pt.x = i + 70  * m_fUnit;
			pt.y = nHorzPos + 30  * m_fUnit; 
			if (bCal)
			{
				CoordinateCalibrate(pt);
			}

			PrintText(CSize(0, 32  * m_fUnit), pt.x + 10  * m_fUnit, pt.y + 10  * m_fUnit, strText);

			//PrintText(CSize(0,32), ,  ,  strText);
		}
		else if (fabs(i - m_rcPage.Width() / 2 - nHCalLength / 2) < 10  * m_fUnit)
		{
			strText.Format(_T("%.1f%s"), fNum,m_strUnit);
			/*m_printDC.MoveTo(i,nHorzPos +50);
					m_printDC.LineTo(i,nHorzPos -50);
					PrintText(CSize(0,32),  i-10, nHorzPos + 30,  strText);*/
			pt.x = i;
			pt.y = nHorzPos + 50  * m_fUnit; 
			if (bCal)
			{
				CoordinateCalibrate(pt);
			}
			m_printDC.MoveTo(pt);
			pt.x = i;
			pt.y = nHorzPos - 50  * m_fUnit; 
			if (bCal)
			{
				CoordinateCalibrate(pt);
			}
			m_printDC.LineTo(pt);

			pt.x = i - 10  * m_fUnit;
			pt.y = nHorzPos + 30  * m_fUnit; 
			if (bCal)
			{
				CoordinateCalibrate(pt);
			}

			PrintText(CSize(0, 32  * m_fUnit), pt.x + 10  * m_fUnit, pt.y + 10  * m_fUnit, strText);
		}
		else if (i == m_rcPage.Width() / 2)
		{
			strText.Format(_T("%.1f%s"), fNum,m_strUnit);			
			// PrintText(CSize(0,32),  i + 60,  nHorzPos + 30, strText);

			pt.x = i + 60  * m_fUnit;
			pt.y = nHorzPos + 30  * m_fUnit; 
			if (bCal)
			{
				CoordinateCalibrate(pt);
			}

			PrintText(CSize(0, 32  * m_fUnit), pt.x + 10  * m_fUnit, pt.y + 10  * m_fUnit, strText);
		}
		else
		{
			/*
					m_printDC.MoveTo(i,nHorzPos +25);
					m_printDC.LineTo(i,nHorzPos -25);
					*/					 
			pt.x = i;
			pt.y = nHorzPos + 25  * m_fUnit; 
			if (bCal)
			{
				CoordinateCalibrate(pt);
			}
			m_printDC.MoveTo(pt);
			pt.x = i;
			pt.y = nHorzPos - 25  * m_fUnit; 
			if (bCal)
			{
				CoordinateCalibrate(pt);
			}
			m_printDC.LineTo(pt);
		}
		fNum = fNum + 1.0;
	}

	int nTextPos = nHeight - nVCalLength - 100  * m_fUnit;
	PrintText("Horizontal and Vertical Correction Plans",
		CRect(0, nTextPos, m_rcPage.Width(), nTextPos - 100  * m_fUnit), CSize(0, 50  * m_fUnit));

	//灰度校正图
	int nRGBPos = nHeight - nVCalLength - 200  * m_fUnit;	  
	int nCalGrade = nGrade;
	for (i = 2; i <= nCalGrade; i *= 2)
	{
		strText.Format(_T("%d Level"), i);
		PrintText(CSize(0, 32  * m_fUnit), m_rcPage.Width() / 2 - 600  * m_fUnit, nRGBPos, strText);

		int nRectRight = m_rcPage.Width() / 2 - 500  * m_fUnit;
		for (int j = 0; j < i; j++)
		{
			int nColorVal = 255 / (i - 1) * j;
			COLORREF color = RGB(nColorVal, nColorVal, nColorVal);
			//int nRectLeft = m_rcPage.Width() / 2 - 500  * m_fUnit + 1000 / i* j   * m_fUnit;
			CRect rect(nRectRight, nRGBPos, nRectRight + 1000 / i   * m_fUnit, nRGBPos - 50  * m_fUnit);
            nRectRight += 1000 / i   * m_fUnit;
			m_printDC.FillSolidRect(rect, color);
		}

		nRGBPos -= 50  * m_fUnit;
	}

	PrintText("Gray Correction Plan",
		CRect(0, nRGBPos, m_rcPage.Width(), nRGBPos - 150  * m_fUnit), CSize(0, 50  * m_fUnit));

	m_printDC.EndPage();
	m_printDC.RestoreDC(-1);

	EndPrintDoc();

	m_bCalibrated = FALSE;

	if (m_fUnit != 1.0)
	{
		m_rcPage.right = m_rcPage.right * 2.54;
		m_rcPage.bottom = m_rcPage.bottom * 2.54;	
	}

	return TRUE;
}

BOOL CULPrintInfo::SetPrintDC()
{
	if (m_printDC.GetSafeHdc())
		return TRUE;

	CPrintDialog dlg(FALSE);
	if (!dlg.GetDefaults()) 	//没有安装打印机或打印机错误
	{
	#ifdef _LWD
		CString strLoadExt;
		strLoadExt.LoadString(IDS_LANGUAGE_EXT);
		if (strLoadExt == _T("_CN"))
		{
			AfxMessageBox(_T("打印机出错！")); //打印机出错
		}
		else
		{
			AfxMessageBox(_T("Print Error!")); //打印机出错
		}
	#else
		AfxMessageBox(AFX_IDP_E_PRINTERERROR); //打印机出错
	#endif
		
		
		CRect rcPage;
		rcPage.left = 0;		 
		rcPage.top = 0;
		rcPage.right = 2160;		  //设置默认的页的大小  		  
		rcPage.bottom = 2790;

		SetPageInfo(rcPage);

		return FALSE;
	}

	m_printDC.Attach(dlg.GetPrinterDC());
	m_nPDC = GetDeviceCaps(m_printDC.GetSafeHdc(), LOGPIXELSY);
	m_strDevice = dlg.GetDeviceName();
	LPDEVMODE pDevMode = dlg.GetDevMode();
	m_bColor = (pDevMode->dmColor == DMCOLOR_COLOR);
	m_printDC.m_bPrinting = TRUE;

	CSize szPage = GetPrinterPageSize();
	CRect rcPage;
	rcPage.left = 0;
	rcPage.top = 0;
	rcPage.right = szPage.cx;
	rcPage.bottom = szPage.cy;

	SetPageInfo(rcPage);

	return TRUE;
}

/* ---------------------------------------------- */
/* 设置 PDF 打印机                                */
/* ---------------------------------------------- */

BOOL CULPrintInfo::SetPdfPrinter()
{
	/* The current printer is pdf already. */
	if (m_strDevice == m_strPdf)
		return TRUE;

	/* Detect pdf printer if hasn't been detected. */
	if (m_hDCPdf == NULL)
	{
		m_hDCPdf = CreatePrinter((LPTSTR)(LPCTSTR)m_strPdf);

		/* Set pdf printer is the default printer first.
		if (SetDefPrinter((LPTSTR)(LPCTSTR)m_strPdf))
		{
			CPrintDialog dlg(FALSE);
			if (dlg.GetDefaults())
			{
				m_hDCPdf = dlg.GetPrinterDC();
			}

			/* Restore the default printer. 
			if (m_strDevice.GetLength())
				SetDefPrinter((LPTSTR)(LPCTSTR)m_strDevice);
		}*/
	}

	/* Pdf printer has been detected. */
	if (m_hDCPdf != NULL)
	{
		/* Save current printer first. */
		if (m_printDC.GetSafeHdc())
		{
			m_hDCDef = m_printDC.Detach();
			m_strDef = m_strDevice;
			m_bColorDef = m_bColor;
		}

		/* Set pdf printer is the current printer. */
		m_printDC.Attach(m_hDCPdf);
		m_nPDC = GetDeviceCaps(m_printDC.GetSafeHdc(), LOGPIXELSY);
		m_strDevice = m_strPdf;
		m_bColor = m_bColorPdf;

		m_printDC.m_bPrinting = TRUE;
		CSize szPage = GetPrinterPageSize();
		CRect rcPage;
		rcPage.left = 0;
		rcPage.top = 0;
		rcPage.right = szPage.cx;
		rcPage.bottom = szPage.cy;
		SetPageInfo(rcPage);
		return TRUE;
	}

	return FALSE;
}

/* ---------------------------------------------- */
/* 恢复 PDF 之前的打印机设置                      */
/* ---------------------------------------------- */

BOOL CULPrintInfo::RestorePrinter()
{
	if ((m_hDCDef == NULL) || m_strDef.IsEmpty())
		return TRUE;

	if (m_strDevice == m_strDef)
		return TRUE;

	if (m_printDC.GetSafeHdc())
		m_printDC.Detach();

	m_printDC.Attach(m_hDCDef);
	m_nPDC = GetDeviceCaps(m_printDC.GetSafeHdc(), LOGPIXELSY);
	m_strDevice = m_strDef;
	m_bColor = m_bColorDef;
	m_printDC.m_bPrinting = TRUE;
	CSize szPage = GetPrinterPageSize();
	CRect rcPage;
	rcPage.left = 0;
	rcPage.top = 0;
	rcPage.right = szPage.cx;
	rcPage.bottom = szPage.cy;
	SetPageInfo(rcPage);

	m_hDCPdf = NULL;
	m_strDef.Empty();
	return TRUE;
}

HDC CULPrintInfo::CreatePrinter(LPTSTR pPrinterName)
{
	HANDLE hPrinter = NULL;
	BOOL bFlag = OpenPrinter(pPrinterName, &hPrinter, NULL);   
	if (!bFlag || !hPrinter)
		return NULL;   
	
	DWORD dwNeeded = 0; 
	GetPrinter(hPrinter, 2, 0, 0, &dwNeeded);   
	if (dwNeeded == 0)
	{
		ClosePrinter(hPrinter);   
		return NULL;
	}
	
	PRINTER_INFO_2* ppi2 = (PRINTER_INFO_2*) GlobalAlloc(GPTR, dwNeeded);   
	if (!ppi2)
	{
		ClosePrinter(hPrinter);   
		return NULL;
	}   
		
	bFlag = GetPrinter(hPrinter, 2, (LPBYTE) ppi2, dwNeeded, &dwNeeded);   
	if ((!bFlag) || (!ppi2->pDriverName) || (!ppi2->pPortName))
	{
		ClosePrinter(hPrinter);   
		GlobalFree(ppi2);   
		return NULL;
	}
		
	HDC hDCPrinter = ::CreateDC((LPCTSTR)ppi2->pDriverName, pPrinterName, NULL, ppi2->pDevMode);
	if (m_strPdf.Compare(pPrinterName) == 0)
	{
		m_bColorPdf = (ppi2->pDevMode->dmColor == DMCOLOR_COLOR);
	}

	if (hPrinter)
		ClosePrinter(hPrinter);   
	if (ppi2)
		GlobalFree(ppi2);

	return hDCPrinter;
}

BOOL CULPrintInfo::SetDefPrinter(LPTSTR pPrinterName)
{
	BOOL bFlag;
	DWORD dwNeeded = 0;   
	HANDLE hPrinter = NULL;   
	PRINTER_INFO_2* ppi2 = NULL;   
	LPTSTR pBuffer = NULL;   
	LONG lResult;
	OSVERSIONINFO osv;
    osv.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	::GetVersionEx (&osv);
	if (osv.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS)
	{
		bFlag = OpenPrinter(pPrinterName, &hPrinter, NULL);   
		if (!bFlag || !hPrinter)
			return FALSE;
		
		GetPrinter(hPrinter, 2, 0, 0, &dwNeeded);   
		if (dwNeeded == 0)
		{
			ClosePrinter(hPrinter);   
			return FALSE;
		}   

		ppi2 = (PRINTER_INFO_2 *) GlobalAlloc(GPTR, dwNeeded);   
		if (!ppi2)
		{
			ClosePrinter(hPrinter);   
			return FALSE;
		}   

		bFlag = GetPrinter(hPrinter, 2, (LPBYTE) ppi2, dwNeeded, &dwNeeded);   
		if (!bFlag)
		{
			ClosePrinter(hPrinter);   
			GlobalFree(ppi2);   
			return   FALSE;
		}   

		ppi2->Attributes |= PRINTER_ATTRIBUTE_DEFAULT;   
		bFlag = SetPrinter(hPrinter, 2, (LPBYTE) ppi2, 0);   
		if (!bFlag)
		{
			ClosePrinter(hPrinter);   
			GlobalFree(ppi2);   
			return   FALSE;
		}   

		lResult = SendMessageTimeout(HWND_BROADCAST, WM_SETTINGCHANGE, 0L,
					(LPARAM) (LPCTSTR) "windows", SMTO_NORMAL, 1000, NULL);
	}
	else if (osv.dwPlatformId == VER_PLATFORM_WIN32_NT)
	{
		bFlag = OpenPrinter(pPrinterName, &hPrinter, NULL);   
		if (!bFlag || !hPrinter)
			return   FALSE;   
		
		GetPrinter(hPrinter, 2, 0, 0, &dwNeeded);   
		if (dwNeeded == 0)
		{
			ClosePrinter(hPrinter);   
			return   FALSE;
		}   
		ppi2 = (PRINTER_INFO_2 *) GlobalAlloc(GPTR, dwNeeded);   
		if (!ppi2)
		{
			ClosePrinter(hPrinter);   
			return   FALSE;
		}   
		bFlag = GetPrinter(hPrinter, 2, (LPBYTE) ppi2, dwNeeded, &dwNeeded);   
		if ((!bFlag) || (!ppi2->pDriverName) || (!ppi2->pPortName))
		{
			ClosePrinter(hPrinter);   
			GlobalFree(ppi2);   
			return   FALSE;
		}   
		
		pBuffer = (LPTSTR) GlobalAlloc(GPTR,
			lstrlen(pPrinterName) +
			lstrlen(ppi2->pDriverName) +
			lstrlen(ppi2->pPortName) +
			3);   
		if (!pBuffer)
		{
			ClosePrinter(hPrinter);   
			GlobalFree(ppi2);   
			return   FALSE;
		}   
		
		lstrcpy(pBuffer, pPrinterName); 	lstrcat(pBuffer, _T(","));   
		lstrcat(pBuffer, ppi2->pDriverName);	 lstrcat(pBuffer, _T(","));   
		lstrcat(pBuffer, ppi2->pPortName);   
		
		bFlag = WriteProfileString(_T("windows"), _T("device"), pBuffer);   
		if (!bFlag)
		{
			ClosePrinter(hPrinter);   
			GlobalFree(ppi2);   
			GlobalFree(pBuffer);   
			return   FALSE;
		} 
	
		lResult = SendMessageTimeout(HWND_BROADCAST, WM_SETTINGCHANGE, 0L, 0L,
			SMTO_NORMAL, 1000, NULL);
	}   

	if (hPrinter)
		ClosePrinter(hPrinter);   
	if (ppi2)
		GlobalFree(ppi2);   
	if (pBuffer)
		GlobalFree(pBuffer);

	return TRUE;
}

void CULPrintInfo::PrinterSetUp()
{
	CPrintDialog dlg(FALSE);
	if (AfxGetApp()->DoPrintDialog(&dlg) == IDOK)
	{
		if (m_printDC.GetSafeHdc())
		{
			m_printDC.Detach();
			m_strDevice.Empty();
		}

		m_printDC.Attach(dlg.GetPrinterDC());
		m_strDevice = dlg.GetDeviceName();
		LPDEVMODE pDevMode = dlg.GetDevMode();
		m_bColor = (pDevMode->dmColor == DMCOLOR_COLOR);

		CSize szPage = GetPrinterPageSize();
		CRect rcPage;
		rcPage.left = 0;
		rcPage.top = 0;
		rcPage.right = szPage.cx;
		rcPage.bottom = 1000;

		SetPageInfo(rcPage);
	}
}

void CULPrintInfo::ReleasePrintDC()
{
	if (m_printDC.GetSafeHdc())
	{
		m_printDC.Detach();
		m_strDevice.Empty();
	}
}

CDC* CULPrintInfo::GetPrintDC()
{
	if (m_pMemDC->GetSafeHdc() != NULL)
		return m_pMemDC;
	
	if (m_printDC.GetSafeHdc())
		return &m_printDC;

	return NULL;
}

CRect CULPrintInfo::SetPageInfo()
{
	CSize szPage = GetPrinterPageSize();
	m_rcPage.left = 0;
	m_rcPage.top = 0;
	m_rcPage.right = szPage.cx;
	m_rcPage.bottom = szPage.cy;
	return m_rcPage;
}

void CULPrintInfo::SetPageInfo(const CRect& rcPage)
{
	m_rcPage = rcPage;
}

CRect CULPrintInfo::GetPageInfo()
{
	return m_rcPage;
}

CSize CULPrintInfo::GetPrinterPageSize()
{
	CSize size;
	//size.cx = m_printDC.GetDeviceCaps(HORZSIZE)*10;	// 0.1mm为单位
	size.cx = (double)GetDeviceCaps(m_printDC.GetSafeHdc(), PHYSICALWIDTH) /
		GetDeviceCaps(m_printDC.GetSafeHdc(), LOGPIXELSX) * 254;
	//size.cy = m_printDC.GetDeviceCaps(VERTSIZE)*10;	// 0.1mm为单位
	size.cy = (double)GetDeviceCaps(m_printDC.GetSafeHdc(), PHYSICALHEIGHT) / 
		GetDeviceCaps(m_printDC.GetSafeHdc(), LOGPIXELSY) * 254; 

	return size;
}															   

double CULPrintInfo::SetPrintStartDepth(double fStartDepth)
{
	if (m_nDirection == SCROLL_UP)
	{
		m_fStartPrintDepth = ceil(fStartDepth);
	}
	else
	{
		m_fStartPrintDepth = floor(fStartDepth);
	}
	return m_fStartPrintDepth;
}

double CULPrintInfo::GetPrintStartDepth()
{
	return m_fStartPrintDepth;
}

void CULPrintInfo::SetPrintEndDepth(double fEndDepth)
{
	double nUnit = GetMeterPerPage();

	if (m_nDirection == SCROLL_UP)
	{
		double nPage = ceil((m_fStartPrintDepth - fEndDepth) / nUnit);
		m_fEndPrintDepth = m_fStartPrintDepth - nPage * nUnit;
	}
	else
	{
		// double nPage = floor( (fEndDepth - m_fStartPrintDepth) / nUnit );
		m_fEndPrintDepth = ceil(fEndDepth);
	}
}

double CULPrintInfo::GetPrintEndDepth()
{
	return m_fEndPrintDepth;
}


void CULPrintInfo::SetPrintNum(int nPrintNum)
{
	m_nPageNO = nPrintNum;
}


int CULPrintInfo::GetPrintNum()
{
	return m_nPageNO;
}

int CULPrintInfo::AddPrintNum()
{
	m_nPageNO ++;
	return m_nPageNO;
}

void CULPrintInfo::SetPrintDirection(int nDirection)
{
	m_nDirection = nDirection;
}

int CULPrintInfo::GetPrintDirection()
{
	return m_nDirection;
}

void CULPrintInfo::SetStartPrintPos(int nStartPos)
{
	m_nStartPrintPos = nStartPos;
}

void CULPrintInfo::SetCurPrintDepth(double fCurDepth)
{
	m_fCurPrintDepth = fCurDepth;
}

void CULPrintInfo::SetPrintReviseX(const double& fReviseX)
{
	m_fPrintReviseX = fReviseX;
}

void CULPrintInfo::SetPrintReviseY(const double& fReviseY)
{
	m_fPrintReviseY = fReviseY;
}

double CULPrintInfo::GetPrintReviseX()
{
	return m_fPrintReviseX;
}

double CULPrintInfo::GetPrintReviseY()
{
	return m_fPrintReviseY;
}

CFont* CULPrintInfo::SetFont(CSize szFont, int nEscapement /* = 0 */,
	BYTE bUnderline /* = FALSE */, long lfWeight /* = FW_SEMIBOLD */)
{
	LOGFONT lf;
	ZeroMemory(&lf, sizeof(LOGFONT));
	lf.lfWidth = szFont.cx;
	lf.lfHeight = szFont.cy;
	lf.lfClipPrecision = CLIP_LH_ANGLES;
	lf.lfCharSet = ANSI_CHARSET;
	lf.lfWeight = lfWeight;
	lf.lfOrientation = 0;
	lf.lfEscapement = nEscapement;
	lf.lfUnderline = bUnderline;

	CFont fontPrint;
	fontPrint.CreateFontIndirect(&lf);
	CDC* pDC = GetPrintDC();
	return pDC->SelectObject(&fontPrint);
}

CFont* CULPrintInfo::SetFont(CFont* pNewFont, int nEscapement /* = 0 */, BYTE bUnderline /* = FALSE */)
{
	LOGFONT lf;
	pNewFont->GetLogFont(&lf);
	lf.lfClipPrecision = CLIP_LH_ANGLES;
	lf.lfEscapement = nEscapement;
	lf.lfUnderline = bUnderline;
	
	CFont fontPrint;
	fontPrint.CreateFontIndirect(&lf);
	CDC* pDC = GetPrintDC();
	return pDC->SelectObject(&fontPrint);	
}

void CULPrintInfo::PrintText(const CString& str, int x, int y)
{
	CDC* pDC = GetPrintDC();
	pDC->DrawText(str, CRect(x, y, 0, 0), DT_LEFT | DT_NOCLIP);
}

void CULPrintInfo::PrintText(const CString& str, int x, int y, int nEscapement, CPen* pPen/* = new CPen(PS_SOLID, 1, RGB(0, 0, 0))*/)
{
	CDC* pDC = GetPrintDC();
	pDC->SetBkMode(TRANSPARENT);

	TEXTMETRIC tm;
	pDC->GetTextMetrics(&tm);
	CSize szText = pDC->GetTextExtent(str);
	switch (nEscapement)
	{
	case 0:
		{
			UINT nFlags = pDC->SetTextAlign(TA_LEFT);
// 			x -= szText.cx / 2;
// 			y += tm.tmHeight / 2;
			if(m_dwMode == PM_FILE) // 打开文件时的静态打印
			{
				if (m_nDirection == SCROLL_UP)
				{
					x -= szText.cx / 2;
					y += (tm.tmHeight - 4);
				}
				else
				{
					x -= szText.cx / 2;
					y += 2/*tm.tmHeight / 2*/;
				}
			}
			else // 实时打印
			{
				if (m_nDirection == SCROLL_UP)
				{
					x -= szText.cx / 2;
					y += tm.tmHeight / 2;
				}
				else
				{
					x -= szText.cx / 2;
					y -= 5/*tm.tmHeight / 2*/;
				}
			}
			LOGPEN logpen;
			pPen->GetLogPen(&logpen);
			COLORREF pOldColor = pDC->SetTextColor(logpen.lopnColor);
			pDC->DrawText(str, CRect(x, y, 0, 0), DT_LEFT | DT_NOCLIP);
			pDC->SetTextColor(pOldColor);
			pDC->SetTextAlign(nFlags);
		}
		break;
	case 900:
		{
			UINT nFlags = pDC->SetTextAlign(TA_LEFT);
			x += tm.tmHeight / 2;
			y += szText.cx / 2;
			pDC->DrawText(str, CRect(x, y, 0, 0), DT_LEFT | DT_NOCLIP);
			pDC->SetTextAlign(nFlags);
		}
		break;
	case 1800:
		{
			UINT nFlags = pDC->SetTextAlign(TA_RIGHT);
			x -= szText.cx / 2;
			y -= tm.tmHeight / 2;
			pDC->DrawText(str, CRect(x, y, 0, 0), DT_LEFT | DT_NOCLIP);
			pDC->SetTextAlign(nFlags);
		}
		break;
	case 2700:
		{
			UINT nFlags = pDC->SetTextAlign(TA_RIGHT);
			x -= tm.tmHeight / 2;
			y += szText.cx / 2;
			pDC->DrawText(str, CRect(x, y, 0, 0), DT_LEFT | DT_NOCLIP);
			pDC->SetTextAlign(nFlags);
		}
		break;
	default:
		break;
	}
}

void CULPrintInfo::PrintText(CSize szfont, int x, int y, CString strText,
	int nEscapement /* = 0 */, int nBkMode /* = TRANSPARENT */,
	BYTE bUnderline /* = FALSE */, long lfWeight/*=600*/, CPen* pPen/* = new CPen(PS_SOLID, 1, RGB(0, 0, 0))*/)
{
	CDC* pDC = GetPrintDC();

	if ((m_dwMode == PM_SHOOT) && (m_nDirection == SCROLL_UP))
	{
		nEscapement = 0;
		//	y += 20;
	}
	CFont fontPrint;
	LOGFONT font;
	memset(&font, 0, sizeof(LOGFONT));

	font.lfWidth = szfont.cx;
	font.lfHeight = szfont.cy;
	//	strcpy(font.lfFaceName, "Arial");
	font.lfClipPrecision = CLIP_LH_ANGLES;
	font.lfCharSet = ANSI_CHARSET;
	font.lfWeight = lfWeight;
	font.lfOrientation = 0;
	font.lfEscapement = nEscapement;
	font.lfUnderline = bUnderline;

	fontPrint.CreateFontIndirect(&font);

	CFont* pOldFont = pDC->SelectObject(&fontPrint);

	UINT nMode;
	if (m_nDirection == SCROLL_UP && (m_dwMode != PM_SHOOT))
		nMode = pDC->SetTextAlign(TA_RIGHT);
	else
		nMode = pDC->SetTextAlign(TA_LEFT);

	if (m_bCalibrated)
	{
		pDC->SetTextAlign(TA_RIGHT);
	}

	//m_pPrintInfo->printDC.DrawText(strText,rcText,m_nAlignMode);
	//m_printDC.TextOut( x, y, strText);

	//需要颠倒时 不要
	if ((m_dwMode == PM_SHOOT) && (m_nDirection == SCROLL_UP))
	{
		//如果含有回车，考虑只有一个回车
		int nEnterIndex = strText.Find('\n');
		if (nEnterIndex != -1)
		{
			CString str1 = strText.Left(nEnterIndex);
			CString str2 = strText.Right(strText.GetLength() - nEnterIndex - 1);
			strText = str2 + "\n" + str1;
			//再加上一个字高
			//y += szfont.cy;
		}
	}

	int nBkMode0 = pDC->SetBkMode(nBkMode);
	if ((nBkMode == OPAQUE) && (bUnderline == FALSE))
	{
		TEXTMETRIC tm;
		m_printDC.GetTextMetrics(&tm);
		CSize sizeF = m_printDC.GetTextExtent(strText);
		CPen pen(PS_SOLID, 1, RGB(0, 0, 0));
		CBrush brush(RGB(255, 255, 255));
		CPen* pPen0 = pDC->SelectObject(/*&pen*/pPen);
		CBrush* pBrush0 = pDC->SelectObject(&brush);
		if (pDC->GetTextAlign() == TA_LEFT)
			pDC->Rectangle(x, y, x + sizeF.cx, y - tm.tmHeight);
		else
			pDC->Rectangle(x, y, x + sizeF.cx, y + tm.tmHeight);
		pDC->SelectObject(pBrush0);
		pDC->SelectObject(pPen0);
	}
	// 将标签放置在矩形内
// 	CBrush whiteBrush(RGB(255, 255, 255));
// 	CPen rectPen(PS_SOLID, 3, RGB(0, 0, 0));
// 	CBrush* pOldBrush = pDC->SelectObject(&whiteBrush);
// 	CPen* pOldPen1 = pDC->SelectObject(/*&rectPen*/pPen);
// 	CSize csText = pDC->GetTextExtent(strText);
// 	if (m_nDirection == SCROLL_UP)
// 	{
// 		y += 5;
// 		pDC->Rectangle(x-3, y + 3, x + csText.cx + 3, y + csText.cy - 2);
// 	}
// 	else if (m_nDirection == SCROLL_DOWN)
// 	{
// 		y -= 5;
// 		pDC->Rectangle(x-3, y + 3, x + csText.cx + 3, y - csText.cy - 2);
// 	}
// 	pDC->SelectObject(pOldBrush);
// 	pDC->SelectObject(pOldPen1);

	LOGPEN logpen;
	pPen->GetLogPen(&logpen);
	COLORREF pOldColor = pDC->SetTextColor(logpen.lopnColor);
	pDC->DrawText(strText, CRect(x, y, 0, 0), DT_LEFT | DT_NOCLIP);
	pDC->SetTextColor(pOldColor);

	pDC->SetTextAlign(nMode);
	pDC->SetBkMode(nBkMode0);
	pDC->SelectObject(pOldFont);
}

void CULPrintInfo::PrintText(CString strText, CRect rcText, CSize szFont,
	int nEscapement, UINT nFormat)
{
	LOGFONT font;
	CFont fontPrint;
	memset(&font, 0, sizeof(LOGFONT));

	font.lfWidth = szFont.cx;
	font.lfHeight = szFont.cy;
	//	strcpy(font.lfFaceName, "Arial");
	font.lfClipPrecision = CLIP_STROKE_PRECIS;
	font.lfCharSet = ANSI_CHARSET;
	font.lfWeight = FW_NORMAL;
	font.lfOrientation = 0;
	font.lfEscapement = nEscapement;

	fontPrint.CreateFontIndirect(&font);
	CPen WritePen(PS_SOLID, 3, RGB(255, 255, 255));
    CDC* pDC = GetPrintDC();
	CPen* pOldPen = pDC->SelectObject(&WritePen);
	CFont* pOldFont = (CFont*) pDC->SelectObject(&fontPrint);
	UINT nMode = pDC->SetBkMode(TRANSPARENT);
	pDC->SetBkColor(RGB(255,255,255));
	pDC->DrawText(strText, rcText, nFormat);

	pDC->SetBkMode(nMode);
	pDC->SelectObject(pOldFont);
	pDC->SelectObject(pOldPen);
}

CRect CULPrintInfo::CoordinateConvert(CRect rect)
{
	// 由于绘图时左右有两个PAGE_MARGIN,在打印时无，因此坐标出现差
	if (m_nDirection == SCROLL_UP && (m_dwMode != PM_SHOOT))
	{
		CRect rcOld = rect;
		rect.left = m_rcPage.right - rcOld.right + PAGE_MARGIN;
		rect.right = m_rcPage.right - rcOld.left + PAGE_MARGIN;
	}
	else
	{
		rect.left -= PAGE_MARGIN;
		rect.right -= PAGE_MARGIN;
	}

	return rect;
}

int CULPrintInfo::CoordinateConvertX(int x)
{
	if (m_nDirection == SCROLL_UP && (m_dwMode != PM_SHOOT))
		x = m_rcPage.right - x + PAGE_MARGIN;
	else
		x -= PAGE_MARGIN;
	return x;
}

int CULPrintInfo::GetPrintDepthCoordinate(double fDepth)
{
	if (PM_FILE == m_dwMode)
	{
		double dPos = (fDepth - m_fStartPrintDepth) * 10000.0 / m_nRatio;
		int nPos = floor(dPos + 0.5);
		nPos *= -1;
		return (m_nStartPrintPos / m_fPrintReviseY + nPos);
	}

	if (m_nDirection == SCROLL_UP)
	{
		double fCurPrintDepth = m_fStartPrintDepth - m_nPageNO * m_nUnitPerPage;
		double dPos = (fDepth - fCurPrintDepth) * 10000.0 / m_nRatio;
		int nPos = floor(dPos + 0.5);
		return nPos;
	}

	// if (m_nDirection == SCROLL_DOWN)
	{
        double fCurPrintDepth = m_fStartPrintDepth + m_nPageNO * m_nUnitPerPage;
		double dPos = (fDepth - fCurPrintDepth) * 10000.0 / m_nRatio;
		int nPos = floor(dPos + 0.5);
		nPos *= -1;
		return nPos;	
	}

	//return 0;
}

void CULPrintInfo::CoordinateCalibrate(CRect& rect)
{
	rect.left = (int) (rect.left * m_fPrintReviseX);
	rect.right = (int) (rect.right * m_fPrintReviseX);
	rect.top = (int) (rect.top * m_fPrintReviseY);
	rect.bottom = (int) (rect.bottom * m_fPrintReviseY);
}

void CULPrintInfo::CoordinateCalibrate(CPoint& point)
{
	point.x = (int) (point.x * m_fPrintReviseX);
	point.y = (int) (point.y * m_fPrintReviseY);
}

CPoint CULPrintInfo::CoordCalibrate(CPoint point)
{
	CPoint pt;
	pt.x = (int) (point.x * m_fPrintReviseX);
	pt.y = (int) (point.y * m_fPrintReviseY);
	return pt;
}

void CULPrintInfo::SetPrintRatio(float nRatio)
{
	m_nRatio = nRatio;
	m_nUnitPerPage = GetMeterPerPage();
	m_nTimePerPage = GetMilliSecondPerPage();
}

float CULPrintInfo::GetPrintRatio()
{
	return m_nRatio;
}

double CULPrintInfo::GetMeterPerPage()
{
	double step = 10000.0 / m_nRatio;
	step *= m_fPrintReviseY;

	// 每页换算的米数	 
	return abs(m_rcPage.Height()) / step;
}

int CULPrintInfo::GetMaxPrintPageNum(int& nY)
{
	double fDepthRange = 10000.0 * (m_fStartPrintDepth  -  m_fEndPrintDepth);
	int nPagesHeight = abs(fDepthRange) * m_fPrintReviseY / (double)m_nRatio + abs(nY);
	int nPageCount = (nPagesHeight - 1) / abs(m_rcPage.Height()) + 1;
	nY = abs(m_rcPage.Height()) - (nPageCount*abs(m_rcPage.Height()) - nPagesHeight);
	return abs(nPageCount);
}

void CULPrintInfo::SetPageSize(BOOL bLogging)
{
	CSize szPage = GetPrinterPageSize();
	CRect rcPage;
	rcPage.left = 0;
	rcPage.top = 0;
	rcPage.right = szPage.cx;
	rcPage.bottom = bLogging ? m_nPageRH : szPage.cy;
	SetPageInfo(rcPage);
}

/* 
	打印空白页
*/
void CULPrintInfo::PrintHollowPage(long lHeight)
{
	m_printDC.StartPage();		
	m_printDC.SaveDC(); 
	m_printDC.SetMapMode(MM_LOMETRIC);
	m_printDC.SetWindowOrg(0, 0);		
	//	m_printDC.SetPixel( ( nLeft + nRight ) / 2, 0, RGB( 0, 0, 0 ) );
	//	m_printDC.SetPixel( ( nLeft + nRight ) / 2, space, RGB( 0, 0, 0 ) );

	CRect rect(0, -lHeight, m_rcPage.Width(), 0);
	rect.DeflateRect(PAGE_MARGIN, 0, PAGE_MARGIN, 0);
	// 	rect.left = m_rcPage.left;
	// 	rect.right = m_rcPage.right;
	// 	rect.top = -lHeight;
	// 	rect.bottom = 0;
	// m_printDC.FillSolidRect(rect, RGB(255,255,255));
	m_printDC.Rectangle(&rect);

	m_printDC.RestoreDC(-1); 			
	m_printDC.EndPage();
	Sleep(1000);
}

BOOL CULPrintInfo::StartPrintDoc(UINT nIDDocName)
{
	CString str;
	str.LoadString(nIDDocName);
	return StartPrintDoc(str);
}

BOOL CULPrintInfo::StartPrintDoc(LPCTSTR lpszDocName)
{
	if (m_bStartDoc)
		return FALSE;

	DOCINFO di; 
	ZeroMemory(&di, sizeof(DOCINFO));
	di.cbSize = sizeof(DOCINFO);
	di.lpszDocName = lpszDocName;

	if (m_printDC.GetSafeHdc())
	{
		if (m_printDC.StartDoc(&di) < 0)
		{
			AfxMessageBox(AFX_IDP_FAILED_TO_START_PRINT);
			return FALSE;
		}

		m_bStartDoc = TRUE;
		m_strDoc = lpszDocName;
		m_strLastDoc = lpszDocName;
		return TRUE;
	}

	return FALSE;
}

CString CULPrintInfo::EndPrintDoc()
{
	CString strDoc;
	strDoc.Empty();

	if (m_bStartDoc)
	{
		m_printDC.EndDoc();

		m_bStartDoc = FALSE;
		strDoc = m_strDoc;
		m_strDoc.Empty();
	}

	return strDoc;
}

// 时间驱动
double  CULPrintInfo::SetPrintStartTime(double  fStartTime)
{
	if (m_nDirection == SCROLL_UP)
	{
		m_fStartPrintTime = ceil(fStartTime);
	}
	else
	{
		m_fStartPrintTime = floor(fStartTime);
	}
	return m_fStartPrintTime;
}
	
double   CULPrintInfo::GetPrintStartTime()
{
	return m_fStartPrintTime;
}

void  CULPrintInfo::SetPrintEndTime(double fEndTime)
{
	double nUnit = GetMilliSecondPerPage();
	
	if (m_nDirection == SCROLL_UP)
	{
		double nPage = ceil((m_fStartPrintTime - fEndTime) / nUnit);
		m_fEndPrintTime = m_fStartPrintTime - nPage * nUnit;
	}
	else
	{
		// double nPage = floor( (fEndTime - m_fStartPrintTime) / nUnit );
		m_fEndPrintTime = ceil(fEndTime);
	}
}
	
double   CULPrintInfo::GetPrintEndTime()
{
	return m_fEndPrintTime;
}

void CULPrintInfo::SetCurPrintTime(double fCurTime)
{
	m_fCurPrintTime = fCurTime;
}

int CULPrintInfo::GetPrintTimeCoordinate(double fTime)
{
	if (PM_FILE == m_dwMode)
	{
		if (m_bTimeTop)
		{
			double dPos = (fTime - m_fStartPrintTime) / m_nRatio;
			int nPos = floor(dPos + 0.5);
			nPos *= -1;
			return (m_nStartPrintPos / m_fPrintReviseY + nPos);
		}
		else
		{
			double dPos = (m_fEndPrintTime - fTime) / m_nRatio;
			int nPos = floor(dPos + 0.5);
			nPos *= -1;
			return (m_nStartPrintPos / m_fPrintReviseY + nPos);
		}
	}
	
	if (m_nDirection == SCROLL_UP) // 时间驱动时 方向向上只能是时间原点在下方
	{
		double fCurPrintTime = m_fStartPrintTime + m_nPageNO * m_nTimePerPage;
		double dPos = (fTime - fCurPrintTime) / m_nRatio;
		int nPos = floor(dPos + 0.5);
		nPos *= -1;
		return nPos;
	}
	
	// if (m_nDirection == SCROLL_DOWN)
	{
        double fCurPrintTime = m_fStartPrintTime + m_nPageNO * m_nTimePerPage;
		double dPos = (fTime - fCurPrintTime) / m_nRatio;
		int nPos = floor(dPos + 0.5);
		nPos *= -1;
		return nPos;	
	}
}

double CULPrintInfo::GetMilliSecondPerPage()
{
	double step = 1.0 / m_nRatio;
	step *= m_fPrintReviseY;
	
	// 每页换算的米数	 
	return abs(m_rcPage.Height()) / step;
}
	
int CULPrintInfo::GetMaxPrintPageNumTime(int& nY)
{
	double fTimeRange = 1.0 * (m_fStartPrintTime  -  m_fEndPrintTime);
	int nPagesHeight = abs(fTimeRange) * m_fPrintReviseY / (double)m_nRatio + abs(nY);
	int nPageCount = (nPagesHeight - 1) / abs(m_rcPage.Height()) + 1;
	nY = abs(m_rcPage.Height()) - (nPageCount*abs(m_rcPage.Height()) - nPagesHeight);
	return abs(nPageCount);
}

BOOL CULPrintInfo::SetTimeTop(BOOL bTimeTop)
{
	m_bTimeTop = bTimeTop;
	return m_bTimeTop;
}
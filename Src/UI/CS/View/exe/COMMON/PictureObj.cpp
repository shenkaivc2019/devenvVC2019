// PictureObj.cpp: implementation of the CPictureObj class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PictureObj.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPictureObj::CPictureObj(LPCRECT lpRect, LPCTSTR pszText, LPCSTR pszFile)
{
	m_rectPosition = lpRect;
	m_strText = pszText;
	m_strFile = pszFile;
	if (LoadPicture(m_strFile))
	{
		m_rectPosition.left = (TOOL_WIDTH - _GetWidth())/2;
		m_rectPosition.right = m_rectPosition.left + _GetWidth();
		m_rectPosition.bottom = m_rectPosition.top + _GetHeight();
	}
}

CPictureObj::~CPictureObj()
{

}

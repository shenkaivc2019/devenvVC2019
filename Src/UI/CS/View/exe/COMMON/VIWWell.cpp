// VIWWell.cpp : implementation file
//

#include "stdafx.h"
#include "VIWWell.h"
#include "ComConfig.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CVIWWell, CWnd)

/////////////////////////////////////////////////////////////////////////////
// CVIWWell
BOOL CVIWWell::Create(LPCTSTR lpszWindowName, DWORD dwStyle,
					  const RECT& rect, CWnd* pParentWnd, UINT nID,
					  CFile* pPersist /*= NULL*/, BOOL bStorage /*= FALSE*/,
					  BSTR bstrLicKey /*= NULL*/)
{
	if (CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID))
	{
		SetConfig(_variant_t(AfxGetComConfig()));
		return TRUE;
	}
	
	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// CVIWWell message handlers

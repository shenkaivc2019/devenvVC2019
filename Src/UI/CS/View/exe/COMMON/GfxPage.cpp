// GfxPage.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "GfxPage.h"
#include "Sheet.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CGraphWnd*	CGfxPage::m_pGraph = NULL;
CTrack*		CGfxPage::m_pTrack = NULL;
CCurve*		CGfxPage::m_pCurve = NULL;
CSheet*		CGfxPage::m_pSheet = NULL;

IMPLEMENT_DYNAMIC(CGfxPage, CDialog)

/////////////////////////////////////////////////////////////////////////////
// CGfxPage dialog

CGfxPage::CGfxPage(UINT nIDTemplate, CWnd* pParentWnd /* = NULL */)
	: CDialog(nIDTemplate, pParentWnd)
{
}

CGfxPage::~CGfxPage()
{
	
}

BEGIN_MESSAGE_MAP(CGfxPage, CDialog)
	//{{AFX_MSG_MAP(CGfxPage)
	ON_COMMAND(ID_GFX_ENTER, OnGfxEnter)
	ON_COMMAND(ID_GFX_ESC, OnGfxEsc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGfxPage message handlers

BOOL CGfxPage::OnInitDialog()
{
	BOOL bRet = CDialog::OnInitDialog();
	
	// Load dialog's accelerators
	m_hAccel = ::LoadAccelerators(AfxGetResourceHandle(), MAKEINTRESOURCE(IDD_GFX_PAGE));
	
	return bRet;
}

void CGfxPage::InitCurve()
{

}
void CGfxPage::OnOK()
{

}

void CGfxPage::OnCancel()
{
	
}

BOOL CGfxPage::PreTranslateMessage(MSG* pMsg)
{	
	if (WM_KEYFIRST <= pMsg->message && pMsg->message <= WM_KEYLAST) 
	{
		HACCEL hAccel = m_hAccel;
		if (hAccel && ::TranslateAccelerator(m_hWnd, hAccel, pMsg))
			return TRUE;
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CGfxPage::OnGfxEnter()
{
	// Move to next control
	CWnd* pWndNext = GetNextDlgTabItem(GetFocus());
	if (pWndNext) {
		pWndNext->SetFocus();
	}
}

void CGfxPage::OnGfxEsc()
{
	CWnd* pWnd = GetParent();
	if (pWnd->GetSafeHwnd())
	{
		pWnd = pWnd->GetParent();
		if (pWnd->GetSafeHwnd())
		{
			pWnd->SendMessage(WM_COMMAND, IDCANCEL);
		}
	}
}

//////////////////
// This function is not used, since its message map entry is commented out.
// If all you want to do is ignore the Enter key (not map it to a command),
// then all you have to do is return zero here. Note that you MUST return
// the special code DC_HASDEFID in the high-order word!!
//
LRESULT CGfxPage::OnGetDefID(WPARAM wp, LPARAM lp)
{
	return MAKELONG(0, DC_HASDEFID);
}

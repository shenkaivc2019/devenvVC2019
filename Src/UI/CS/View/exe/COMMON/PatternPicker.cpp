// PatternPicker.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "PatternPicker.h"
#include "DlgPattern.h"
#include "RemarkObject.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CString g_strPatPath;
extern void InitPatPath();
extern CBrush* SelectPatternBrush(CDC* pDC, CBrush* pBrush, LPCTSTR pszPattern, COLORREF clrFore, COLORREF clrBkg);

/////////////////////////////////////////////////////////////////////////////
// CPatternPicker

CPatternPicker::CPatternPicker()
{
	m_clrFore = RGB(0, 0, 0);
	m_clrBk = RGB(255,255,255);
	m_dwType = 0x11;
}

CPatternPicker::~CPatternPicker()
{
}


BEGIN_MESSAGE_MAP(CPatternPicker, CButton)
	//{{AFX_MSG_MAP(CPatternPicker)
	ON_CONTROL_REFLECT(BN_CLICKED, OnClicked)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPatternPicker message handlers

void CPatternPicker::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	ASSERT(lpDrawItemStruct);
	
	if (g_strPatPath.IsEmpty())
		InitPatPath();

    CDC*    pDC     = CDC::FromHandle(lpDrawItemStruct->hDC);
    CRect   rect    = lpDrawItemStruct->rcItem;
    UINT    state   = lpDrawItemStruct->itemState;
    CString m_strText;

    CSize Margins(::GetSystemMetrics(SM_CXEDGE), ::GetSystemMetrics(SM_CYEDGE));

    // Must reduce the size of the "client" area of the button due to edge thickness.
    rect.DeflateRect(Margins.cx, Margins.cy);

	CPen pen(PS_SOLID, 1, ::GetSysColor(COLOR_3DDKSHADOW));
	CPen* pOldPen = pDC->SelectObject(&pen);

	CBrush brush;
	CString strPat = g_strPatPath + m_strPattern;
	HBITMAP hBitmap = (HBITMAP) LoadImage(NULL, strPat, IMAGE_BITMAP, NULL, NULL, LR_LOADFROMFILE);

	CString strText = _T("FillSymbol\\None.bmp");
	if ((hBitmap != NULL) && m_strPattern.CompareNoCase(strText))
	{
		CBrush* pOldBr = SelectPatternBrush(pDC, &brush, strPat, m_clrFore, m_clrBk);
		pDC->Rectangle(rect);
		pDC->SelectObject(pOldBr);
		strText.Empty();
	}
	else
	{
		brush.CreateSolidBrush(RGB(255,255,255));
		CBrush* pOldBr = pDC->SelectObject(&brush);
		pDC->Rectangle(rect);
		pDC->SelectObject(pOldBr);

// 		strPat = m_strPatPath.Left(m_strPatPath.ReverseFind(_T('\\')));
// 		strPat = strPat.Left(strPat.ReverseFind(_T('\\'))) + _T("\\RemarkSymbol\\") + m_strPattern;
		CRemarkObject obj;
		if (obj.LoadFromFile(strPat, FALSE))
		{
			obj.Draw(pDC, rect);
			strText.Empty();
		}
		else
		{
			m_strPattern.Empty();
			strText = _T("None");
		}
	}

	pDC->SelectObject(pOldPen);
	
    if (strText.GetLength())
    {
        pDC->SetBkMode(TRANSPARENT);
        if (IsWindowEnabled())
        {
			pDC->SetTextColor(0);
            pDC->DrawText(strText, rect, DT_CENTER|DT_SINGLELINE|DT_VCENTER);
        }
        else
        {
            rect.OffsetRect(1,1);
            pDC->SetTextColor(::GetSysColor(COLOR_3DHILIGHT));
            pDC->DrawText(strText, rect, DT_CENTER|DT_SINGLELINE|DT_VCENTER);
            rect.OffsetRect(-1,-1);
            pDC->SetTextColor(::GetSysColor(COLOR_3DSHADOW));
            pDC->DrawText(strText, rect, DT_CENTER|DT_SINGLELINE|DT_VCENTER);
        }
    }
}

void CPatternPicker::OnClicked() 
{
	CDlgPattern dlg(m_dwType);
	if (dlg.DoModal() == IDOK)
	{
		m_strPattern = dlg.m_strSelect;
		Invalidate();
		CWnd* pWnd = GetParent();
		if (pWnd->GetSafeHwnd())
		{
			pWnd->SendMessage(PPN_SELENDOK, GetDlgCtrlID());
		}
	}
}

void CPatternPicker::PreSubclassWindow() 
{
	ModifyStyle(0, BS_OWNERDRAW);        // Make it owner drawn
    CButton::PreSubclassWindow();
}

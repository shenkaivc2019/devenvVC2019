
#include "stdafx.h"

#define STGM_OR2R   (STGM_SHARE_DENY_WRITE|STGM_READ)
#define STGM_OR2RW	(STGM_SHARE_EXCLUSIVE|STGM_READWRITE)
#define STGM_CR2RW	(STGM_SHARE_EXCLUSIVE|STGM_CREATE|STGM_READWRITE)

#define STGM_OC2R	(STGM_SHARE_EXCLUSIVE|STGM_READ)
#define STGM_OC2RW	(STGM_SHARE_EXCLUSIVE|STGM_READWRITE)
#define STGM_CC2RW	(STGM_SHARE_EXCLUSIVE|STGM_CREATE|STGM_READWRITE)

#define STGM_OS2R   (STGM_SHARE_EXCLUSIVE|STGM_READ)
#define STGM_OS2RW  (STGM_SHARE_EXCLUSIVE|STGM_READWRITE)
#define STGM_CS2RW  (STGM_SHARE_EXCLUSIVE|STGM_CREATE|STGM_READWRITE)

BOOL GetRootStorage(CString& strStgFile, IStoragePtr& stg, BOOL bWrite/*= TRUE*/)
{
	if (strStgFile.IsEmpty())
		return FALSE;
	USES_CONVERSION;
	
	DWORD dwMode = bWrite ? STGM_OR2RW : STGM_OR2R;
	HRESULT hr = ::StgOpenStorage(T2COLE(strStgFile), NULL, dwMode, NULL, 0, &stg);

	if(SUCCEEDED(hr))
		return TRUE;

	switch(hr)
	{
	case STG_E_FILENOTFOUND:		// 文件不存在
	case STG_E_FILEALREADYEXISTS:	// 非存储文件
	case STG_E_PATHNOTFOUND:
		{
			if(bWrite)
			{
				hr = ::StgCreateDocfile(T2COLE(strStgFile), STGM_CR2RW, 0, &stg);
				if(SUCCEEDED(hr))	return TRUE;
				TRACE(_T("Error : Failed to created file " + strStgFile + "!\n"));
				return FALSE;
			}
		}
		break;
	case STG_E_INVALIDNAME:		// 无效文件名
	case STG_E_INVALIDPOINTER:
	case STG_E_INVALIDFLAG:
	case STG_E_INVALIDFUNCTION:
		TRACE(_T("Error : Invalid param or flag to get root storage!\n"));
		break;
	case STG_E_ACCESSDENIED:	// 拒绝访问
	case STG_E_LOCKVIOLATION:
	case STG_E_SHAREVIOLATION:
	case STG_E_TOOMANYOPENFILES:
	case STG_E_INSUFFICIENTMEMORY:
		TRACE(_T("Error : Access denied to get root storage!\n"));
	    break;
	default:
		TRACE(_T("Error : Unknown error to get root storage!\n"));
	    break;
	}

	return FALSE;
}

BOOL GetChildStorage(LPCTSTR pszName, IStoragePtr stg, IStoragePtr& stgChild, BOOL bWrite /*= TRUE*/)
{
	DWORD dwMode = bWrite ? STGM_OC2RW : STGM_OC2R;

#ifdef _UNICODE
	LPCWSTR pwcsName = pszName;
#else
	USES_CONVERSION;
	LPCWSTR pwcsName = T2OLE(pszName);
#endif

	HRESULT hr = stg->OpenStorage(pwcsName, NULL, dwMode, NULL, 0, &stgChild);
	if (SUCCEEDED(hr))
		return TRUE;
	
	switch(hr)
	{
	case STG_E_FILENOTFOUND:
		{
			if(bWrite)
			{
				hr = stg->CreateStorage(pwcsName, STGM_CC2RW, 0, 0, &stgChild);
				if(SUCCEEDED(hr)) return TRUE;
				TRACE(_T("Error : Failed to created storage " + CString(pwcsName) + "!\n"));
				return FALSE;
			}
		
		}
		break;
	case STG_E_INVALIDFLAG:
	case STG_E_INVALIDFUNCTION:
	case STG_E_INVALIDNAME:
	case STG_E_INVALIDPOINTER:
	case STG_E_INVALIDPARAMETER:
		TRACE(_T("Error : Invalid param or flag to get child storage!\n"));
		break;
	case STG_E_ACCESSDENIED:
	case STG_E_TOOMANYOPENFILES:
	case STG_E_INSUFFICIENTMEMORY:
		TRACE(_T("Error : Access denied to get child storage!\n"));
	    break;
	default:
		TRACE(_T("Error : Unknown error to get child storage!\n"));
	    break;
	}
	return FALSE;
}

BOOL GetChildStream(LPCTSTR pszName, IStoragePtr stg, IStreamPtr& stream, BOOL bWrite /*= TRUE*/)
{
	DWORD dwMode = bWrite ? STGM_OS2RW : STGM_OS2R;

#ifdef _UNICODE
	LPCWSTR pwcsName = pszName;
#else
	USES_CONVERSION;
	LPCWSTR pwcsName = T2OLE(pszName);
#endif
	
	HRESULT hr = stg->OpenStream(pwcsName, NULL, dwMode, 0, &stream);
	if(SUCCEEDED(hr))
		return TRUE;

	switch(hr)
	{
	case STG_E_FILENOTFOUND:
		{
			// 创建流对象
			if(bWrite != 2)
			{
				hr = stg->CreateStream(pwcsName, STGM_CS2RW, 0, 0, &stream);
				if(SUCCEEDED(hr)) return TRUE;
				TRACE(_T("Error : Failed to created stream " + CString(pszName) + "!\n"));
				return FALSE;
			}
		}
		break;
	case STG_E_INVALIDFLAG:
	case STG_E_INVALIDFUNCTION:
	case STG_E_INVALIDNAME:
	case STG_E_INVALIDPOINTER:
	case STG_E_INVALIDPARAMETER:
		TRACE(_T("Error : Invalid param or flag to get child stream!\n"));
		break;
	case STG_E_ACCESSDENIED:
	case STG_E_INSUFFICIENTMEMORY:
	case STG_E_TOOMANYOPENFILES:
		TRACE(_T("Error : Access denied to get child stream!\n"));
	    break;
	default:
		TRACE(_T("Error : Unknown error to get child stream!\n"));
	    break;
	}
	return FALSE;
}

BOOL DeleteElement(LPCTSTR lpszPath, IStoragePtr& stg)
{
	CString strPath = lpszPath;
	
	int iPathLen = strPath.GetLength ();
	if (iPathLen < 1)
		return FALSE;
	
	if (strPath [iPathLen - 1] != _T('\\'))
	{
		strPath += _T('\\');
	}
	else
		iPathLen--;
	
	int iEnd = strPath.Find (_T('\\'));
	if (iEnd < 0)
	{
		// ASSERT (FALSE);
		return FALSE;
	}
	
	CString strSubKey = strPath.Left(iEnd);
	CString strSubPath = strPath.Right(iPathLen - iEnd);

	// strSubKey.Remove (_T(' '));

	if (strSubPath.GetLength() < 2)
	{
#ifdef _UNICODE
		HRESULT hr = stg->DestroyElement(strSubKey);
#else
		USES_CONVERSION;
		HRESULT hr = stg->DestroyElement(T2OLE(strSubKey));
#endif
		if (SUCCEEDED(hr))
			return TRUE;
		
		switch (hr)
		{
		case STG_E_FILENOTFOUND:
			TRACE(_T("Error : Element not found!\n"));
			break;
		case STG_E_INVALIDNAME:
		case STG_E_INVALIDPOINTER: 
		case STG_E_INVALIDPARAMETER:
			TRACE(_T("Error : Invalid param or flag to get child stream!\n"));
			break;
		case STG_E_ACCESSDENIED:
		case STG_E_INSUFFICIENTMEMORY:
		case STG_E_TOOMANYOPENFILES:
			TRACE(_T("Error : Access denied to get child stream!\n"));
			break; 
		default:
			TRACE(_T("Error : Unknown error to get child stream!\n"));
			break;
		}

		return FALSE;
	}

	IStoragePtr stgChild;
	if (GetChildStorage(strSubKey, stg, stgChild, TRUE))
		return DeleteElement(strSubPath, stgChild);
	
	return FALSE;
}
// TableInfoView.cpp : implementation file
//////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "resource.h"
#include "TableInfoView.h"
#include "ulcommdef.h"
#include "Project.h"
#include "ULTool.h"
#include "PrintInfo.h"
#include "MainFrm.h"
#include "ComConfig.h"
#include "ULDoc.h"
#include "ULFile.h"
#include "SaveAsBmp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
/////////////////////////////////////////////////////////////////////////////
// CTableInfoView

IMPLEMENT_DYNCREATE(CTableInfoView, CULView)

CTableInfoView::CTableInfoView(LPVOID pTools/*=NULL*/)
{
	m_pArrTools = (CPtrArray *) pTools;
	m_pEdit = &m_Cell;
}

CTableInfoView::~CTableInfoView()
{
	Clear();
}


BEGIN_MESSAGE_MAP(CTableInfoView, CULView)
//{{AFX_MSG_MAP(CTableInfoView)
// NOTE - the ClassWizard will add and remove mapping macros here.
ON_WM_CREATE()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTableInfoView drawing
void CTableInfoView::OnDraw(CDC* pDC)//LL这个函数未作变动
{
	CRect Rect;
	GetClientRect(Rect);
	pDC->DPtoLP(Rect);
	if (m_nRefresh)
	{
		PrepareCell(&m_Cell, pDC, Rect);
		SetScrollInformation();
		m_nRefresh = 0;
	}
	CMyMemDC memDC(pDC, &Rect);
	DrawPage(&memDC, Rect);
}

void CTableInfoView::Clear()   //LL这个函数未做变动
{
	for (int i = 0; i < m_ToolInfoList.GetSize(); i++)
	{
		TOOL* pInfo = (TOOL*) m_ToolInfoList.GetAt(i);
		delete pInfo;
	}
	m_ToolInfoList.RemoveAll();
}

void CTableInfoView::InitToolInfo()  //这个函数需要变动
{
	Clear();
	for (int i = 0; i < m_pArrTools->GetSize(); i++)
	{
		CULTool* pULTool = (CULTool*) m_pArrTools->GetAt(i);
		TOOL* pInfo = new TOOL;
		pULTool->GetToolProp(pInfo);
		m_ToolInfoList.Add(pInfo);
	}
}

int CTableInfoView::CalcPageHeight()
{
	if (NULL == m_Cell.m_hWnd)
		return CULView::CalcPageHeight();
	return (int) GetCellSize(&m_Cell).cy + 2 * PAGE_MARGIN;
}

int CTableInfoView::CalcPrintHeight()
{
	return CalcPageHeight();
}

void CTableInfoView::OnUpdate()
{
	InitToolInfo();
	InitCallData();	
	m_nRefresh = 1;
	Invalidate();
}

void CTableInfoView::SaveAsBitmap(CDC* pDC, LPCTSTR pszFile)
{
	SaveCellAsBitmap(pDC, pszFile, &m_Cell);
}
/////////////////////////////////////////////////////////////////////////////
// CTableInfoView diagnostics

#ifdef _DEBUG
void CTableInfoView::AssertValid() const		  //这个函数不变动
{
	CULView::AssertValid();
}

void CTableInfoView::Dump(CDumpContext& dc) const //这个函数不变动
{
	CULView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTableInfoView message handlers
int CTableInfoView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!CreateCell(&m_Cell, this))
		return -1;

	OpenCellFile();
	WriteVolumeBmp();

	return 0;
}

void CTableInfoView::OpenCellFile()
{
	InitToolInfo();
	SetScrollInformation();
	InitCallData();
}

void CTableInfoView::OpenCellFile(CCell2000* pCell)
{
	InitToolInfo();
	InitCallData(pCell);
	if (pCell != &m_Cell)
		InitCallData(&m_Cell);

	//////////////////////////////////////////////////////////////////////////
}
void CTableInfoView::InitCallData(CCell2000* pCell)
{
	// InitToolInfo();

	CCell2000* pWorkCell = NULL;
	if (pCell != NULL)
		pWorkCell = pCell;
	else
		pWorkCell = &m_Cell;

	LOGFONT lf;
	afxGlobalData.fontRegular.GetLogFont(&lf);
	long para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftR].lfFaceName,
							1);
	pWorkCell->SetDefaultFont(para, AfxGetComConfig()->m_Watch.FFont[ftR].cy+1);
	//////////////////////////////////////////////////////////////////////////

	//计算需分配的行数
	int nLineCount = 0;
	int i = 0;
	for (i = 0; i < m_ToolInfoList.GetSize(); i++)
	{
		TOOL* pInfo = (TOOL*) m_ToolInfoList.GetAt(i);
		if (pInfo->strENName == "DEPTH")
			continue;

		nLineCount ++;
	}

	//////////////////////////////////////////////////////////////////////////
	//增加分配结构体行数条件语句

	CString strFormat[] =
	{
		_T("Integrated Hole Volume Major Pip Every %s"),
		_T("Integrated Hole Volume Minor Pip Every %s"),
		_T("Integrated Cement Volume Major Pip Every %s"),
		_T("Integrated Cement Volume Minor Pip Every %s"),
		_T("Integrated Transmit Time Major Pip Every %s"),
		_T("Integrated Transmit Time Minor Pip Every %s")
	};

	CString strNames[] =
	{
		_T("IHV:Major"), _T("IHV:Minor"), _T("ICV:Major"), _T("ICV:Minor"),
		_T("ITT:Major"), _T("ITT:Minor")
	};

	CString strValues[6];
	int iImage[3] = { -1, -1, -1 };

	int iInfors = 0;
	CProject* pProject = m_pDoc->m_pProject;
	if (pProject != NULL)
	{
		for (i = 0; i < 6; i++)
		{
			pProject->GetInformation(strNames[i], &strValues[i]);
			if (strValues[i].GetLength())
				iInfors++;
		}

		CString strImage;
		if (pProject->GetInformation(_T("IHV:Show"), &strImage))
			iImage[0] = _ttoi(strImage);
		if (pProject->GetInformation(_T("ICV:Show"), &strImage))
			iImage[1] = _ttoi(strImage);
		if (pProject->GetInformation(_T("ITT:Show"), &strImage))
			iImage[2] = _ttoi(strImage);
	}

	nLineCount += iInfors;
	nLineCount += 11;
	pWorkCell->SetRows(nLineCount, 0);  						   //分配显示行数
	long endCol = pWorkCell->GetCols(0) - 1;
	long endRow = nLineCount - 1;

	// 设置表格整体效果
	pWorkCell->ShowHScroll(0, 0);   							   // 不显示水平滚动条
	pWorkCell->ShowVScroll(0, 0);   							   // 不显示竖直滚动条
	pWorkCell->ShowTopLabel(0, 0);  							   // 不显示列标
	pWorkCell->ShowSideLabel(0, 0); 							   // 不显示行标
	pWorkCell->ShowSheetLabel(0, 0);							   // 隐藏页签
	pWorkCell->SetWorkbookReadonly(TRUE);   					   // 设为只读
	pWorkCell->SetAllowSizeColInGrid(FALSE);						// 不允许调整列宽
	pWorkCell->SetAllowSizeRowInGrid(FALSE);					  // 不允许调整行宽
	pWorkCell->ShowPageBreak(0);								   // 不显示打印分页线
	pWorkCell->ShowGridLine(0, 0);  							   // 不显示格线
	pWorkCell->DrawGridLine(1, 1, endCol, endRow, 0, 0, 0);			// 抹原有网格线
	pWorkCell->DrawGridLine(1, 4, 11, 4, 5, 2, 0);  				// 画网格线

	// 显示表头标题
	pWorkCell->SetRowHeight(1, 30, 1, 0);   					   //设置行高
	pWorkCell->MergeCells(1, 1, 10, 1); 							   //合并
	pWorkCell->SetCellString(1, 1, 0, "Input File");//输出文件头
	pWorkCell->SetCellAlign(1, 1, 0, 32 + 4);

	// 设置输入文件
	CString strValue;
	CDC* pDC = GetDC();
	CSize szText = pDC->GetTextExtent(m_pDoc->GetPathName());
	CRect rt;
	GetClientRect(rt);
	if (szText.cx > rt.Width()) 									   //如果路径太长，显示短路径；否则显?
	{
		TCHAR path[_MAX_PATH];
		GetShortPathName(m_pDoc->GetPathName(), path, _MAX_PATH);
		strValue = "DEFAULT: ";
		pWorkCell->SetCellString(1, 2, 0, strValue + path);
		pWorkCell->SetCellAlign(1, 2, 0, 32 + 4);
	}
	else
	{
		strValue = "DEFAULT: " + m_pDoc->GetPathName();
		pWorkCell->SetCellString(1, 2, 0, strValue);
		pWorkCell->SetCellAlign(1, 2, 0, 32 + 4);
	}

	//设计字体格式
	para = pWorkCell->FindFontIndex(AfxGetComConfig()->m_Watch.FFont[ftT1].lfFaceName,
						1);	
	pWorkCell->SetCellFont(1, 1, 0, para);
	pWorkCell->SetCellFontSize(1, 1, 0, AfxGetComConfig()->m_Watch.FFont[ftT1].cy);
//	pWorkCell->SetCellFontStyle(1, 1, 0, para);
	pWorkCell->MergeCells(1, 2, 10, 2); 									  //合并第二行
	pWorkCell->MergeCells(1, 4, 10, 4); 									  //合并第四行
	//设置行高，字体，大小，类型
	pWorkCell->SetRowHeight(1, 30, 3, 0);
	pWorkCell->MergeCells(1, 3, 10, 3);
	pWorkCell->SetCellString(1, 3, 0, "Output File");
	pWorkCell->SetCellAlign(1, 3, 0, 32 + 4);
	pWorkCell->SetCellFont(1, 3, 0, para);
	pWorkCell->SetCellFontSize(1, 3, 0, AfxGetComConfig()->m_Watch.FFont[ftT1].cy);
//	pWorkCell->SetCellFontStyle(1, 3, 0, para);

	//设置输出文件
	if (szText.cx > rt.Width()) 											 //如果路径太长，显示短路径；否则显?	
	{
		TCHAR path[_MAX_PATH];
		GetShortPathName(m_pDoc->GetPathName(), path, _MAX_PATH);
		strValue = "DEFAULT: ";
		pWorkCell->SetCellString(1, 4, 0, strValue + path);
		pWorkCell->SetCellAlign(1, 4, 0, 32 + 4);
	}
	else
	{
		strValue = "DEFAULT: " + m_pDoc->GetPathName();
		pWorkCell->SetCellString(1, 4, 0, strValue);
		pWorkCell->SetCellAlign(1, 4, 0, 32 + 4);
	}
	ReleaseDC(pDC);

	CString strVersion = AfxGetComConfig()->m_Plot.PlotVersion;

	long row = 5;   														  //标记行号	
	pWorkCell->MergeCells(1, row, 10, row);
	pWorkCell->SetRowHeight(1, 30, row, 0);   								 //设置行高
	pWorkCell->SetCellString(1, row, 0, "Operation System Version:" + strVersion);	   //显示标题
	pWorkCell->SetCellAlign(1, row, 0, 32 + 4);   								   //显示方式	
	pWorkCell->SetCellFont(1, row, 0, para);  								  //字体
	pWorkCell->SetCellFontSize(1, row, 0, AfxGetComConfig()->m_Watch.FFont[ftT1].cy);								  //字体大小
	//pWorkCell->SetCellFontStyle(1, row, 0, para);								  //字体风格
	row++;

	//显示仪器信息
	for (i = 0; i < m_ToolInfoList.GetSize(); i++)
	{
		TOOL* pInfo = (TOOL*) m_ToolInfoList.GetAt(i);
		if (pInfo->strENName == "DEPTH")										  //将DEPTH仪器去掉，不显?		
			continue;

		pWorkCell->MergeCells(2, row, 4, row);
		strValue = pInfo->strENName;	
		pWorkCell->SetCellString(2, row, 0, strValue); 						 //填写仪器名称
		pWorkCell->MergeCells(5, row, 6, row);
		pWorkCell->MergeCells(7, row, 8, row);
		pWorkCell->MergeCells(9, row, 10, row);
		pWorkCell->SetCellString(7, row, 0, strVersion);   						 //填写备注
		row++;
	}
	
	//行号加1
	pWorkCell->DrawGridLine(1, row, 11, row, 4, 2, 0);					   //画表格上线
	
	//合并单元格
	iInfors += 5;
	int k = 0;
	for (k = 0; k < iInfors; k++)
	{
		if (k == 0)
			pWorkCell->MergeCells(1, row, 11, row);
		else if ((k > 0) && (k < 5))
		{
			pWorkCell->MergeCells(1, row, 2, row);
			pWorkCell->MergeCells(3, row, 11, row);
		}
		else
		{
			pWorkCell->MergeCells(1, row, 2, row);
			pWorkCell->MergeCells(4, row, 11, row);
		}
		row++;
	}
	row -= iInfors;

	//设置行高，内容，字体样式
	pWorkCell->SetRowHeight(1, 30, row, 0);
	pWorkCell->SetCellString(1, row, 0, "Integrated Hole / Cement Volume Summary");
	pWorkCell->SetCellAlign(1, row, 0, 32 + 4);
	pWorkCell->SetCellFont(1, row, 0, para);
	pWorkCell->SetCellFontSize(1, row, 0,AfxGetComConfig()->m_Watch.FFont[ftT1].cy );
//	pWorkCell->SetCellFontStyle(1, row, 0, para);
	row++;

	//////////////////////////////////////////////////////////////////////////

	// 格式化输出水泥体积， 环空体积， 时间等。
	CString strIHV;
	CString strICV;
	CString strITT;
	if (m_pDoc->m_pProject)
	{
		m_pDoc->m_pProject->GetInformation(_T("IHV"), &strIHV);
		m_pDoc->m_pProject->GetInformation(_T("ICV"), &strICV);
		m_pDoc->m_pProject->GetInformation(_T("ITT"), &strITT);
	}

	if (strIHV.IsEmpty())
		strIHV = _T("(null)");

	strValue.Format(_T("Hole Volume = %s"), strIHV);
	pWorkCell->SetCellString(3, row++, 0, strValue);	//显示水泥体积

	if (strICV.IsEmpty())
		strICV = _T("(null)");

	strValue.Format(_T("Cement Volume = %s"), strICV);
	pWorkCell->SetCellString(3, row++, 0, strValue);	//显示环孔体积
	
	if (strITT.IsEmpty())
		strITT = _T("(null)");

	strValue.Format(_T("Time = %s"), strITT);
	pWorkCell->SetCellString(3, row++, 0, strValue);	//显示时间

	// 有的数据文件，不一定有深度曲线，譬如wis
	if ((m_pDoc == NULL) || (m_pDoc->m_pDCurve == NULL))
	{
		return;
	}

	long lDepth0 = m_pDoc->m_pDCurve->GetDepth(0);
	int nSize = m_pDoc->m_pDCurve->GetDataSize();
	long lDepthE = m_pDoc->m_pDCurve->GetDepth(nSize - 1);
	CString str;
	CULFile* pFile = NULL;
	pFile = m_pDoc->GetFile();
	if ((pFile != NULL) && (pFile->m_pCAL != NULL))
	{
		str = pFile->m_pCAL->m_strSource + "_" + pFile->m_pCAL->m_strName;
		if (pFile->m_nDirection == SCROLL_DOWN)    //如果下放测井
			strValue.Format("Computed from %.2f m to %.2f m using data channel(s) %s",
						DTM(lDepth0), DTM(lDepthE), str);
		else							  //如果上提测井
			strValue.Format("Computed from %.2f m to %.2f m using data channel(s) %s",
						DTM(lDepthE), DTM(lDepth0), str);
	}
	else
	{
		strValue.Format("Computed from %.2f m to %.2f m using data channel(s) %s",
					DTM(lDepth0), DTM(lDepthE), "");
	}

	pWorkCell->SetCellString(3, row++, 0, strValue);

	
	CString strText;

	// 显示井眼体积，环空体积
	for (i = 0; i < 6; i++)
	{
		if (strValues[i].IsEmpty())
			continue;

		if (pWorkCell->GetCellNote(0, 0, 0).IsEmpty())
		{
			CString strFiles[] = { _T("sField_LL.bmp"), _T("sField_LS.bmp"),
				_T("sField_RL.bmp"), _T("sField_RS.bmp") };
			CString strImage = Gbl_AppPath + _T("\\Images\\");

			for (int j = 0; j < 4; j++)
			{
				m_lImages[j] = pWorkCell->AddImage(strImage + strFiles[j]);
			}
			pWorkCell->SetCellNote(0, 0, 0, _T("AddImage"));
		}

		strText.Format(strFormat[i], strValues[i]);
		if ((iImage[i/2] > -1) && (iImage[i/2] < 2))
			pWorkCell->SetCellImage(3, row, 0, m_lImages[2*iImage[i/2] + i % 2], 3, 0, 0);
		
		pWorkCell->SetCellString(4, row, 0, strText);
		row++;
	}
		
	//////////////////////////////////////////////////////////////////////////
	pWorkCell->DrawGridLine(1, 1, endCol, endRow, 1, 3, 0);
}

void CTableInfoView::WriteVolumeBmp()
{
	CString strPath;
	CString strFile;
	CString strFilePath;
	CRect rc(0, 0, 30, 15);
	CDC* pDC = GetDC();
	CDC dc;
	dc.CreateCompatibleDC(pDC);
	CBitmap bmp;
	bmp.CreateCompatibleBitmap(pDC, 30, 15);
	dc.SelectObject(&bmp);

	CPen pen(PS_SOLID, 1, RGB(0, 0, 0));
	CPen* pOldPen = dc.SelectObject(&pen);

	CFileFind find;
	{
		dc.FillSolidRect(rc, RGB(255, 255, 255));
		dc.MoveTo(5, 3);
		dc.LineTo(5, 15);
		dc.MoveTo(5, 9);
		dc.LineTo(15, 9);
		strPath = Gbl_AppPath + "\\Images\\";
		strFile = "sField_LS.bmp";
		strFilePath = strPath + strFile;
		if (!find.FindFile(strFilePath))
			WriteDCToDIB(strFilePath, &dc);
		dc.MoveTo(15, 9);
		dc.LineTo(25, 9);
		strFile = "sField_LL.bmp";
		strFilePath = strPath + strFile;
		if (!find.FindFile(strFilePath))
			WriteDCToDIB(strFilePath, &dc);
	}
	{
		dc.FillSolidRect(rc, RGB(255, 255, 255));
		dc.MoveTo(25, 3);
		dc.LineTo(25, 15);
		dc.MoveTo(25, 9);
		dc.LineTo(15, 9);
		strPath = Gbl_AppPath + "\\Images\\";
		strFile = "sField_RS.bmp";
		strFilePath = strPath + strFile;
		if (!find.FindFile(strFilePath))
			WriteDCToDIB(strFilePath, &dc);
		dc.MoveTo(25, 9);
		dc.LineTo(5, 9);

		strFile = "sField_RL.bmp";
		strFilePath = strPath + strFile;
		if (!find.FindFile(strFilePath))
			WriteDCToDIB(strFilePath, &dc);	
		dc.SelectObject(pOldPen);
	}
}

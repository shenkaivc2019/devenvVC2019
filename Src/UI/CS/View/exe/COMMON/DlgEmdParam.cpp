// DlgEmdParam.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "DlgEmdParam.h"
#include "ULEmd.h"
#include "MyFileDialog.h"
#include "DlgPartRange.h"
#include "Units.h"
#include "GraphWnd.h"
#include "Utility.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgEmdParam dialog


CDlgEmdParam::CDlgEmdParam(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_EMENDATION_PARAM, pParent)
{
	//{{AFX_DATA_INIT(CDlgEmdParam)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pULEmd = NULL;
	m_pGraph = NULL;
	m_iSel = -1;
}


void CDlgEmdParam::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgEmdParam)
	DDX_Control(pDX, IDC_LIST2, m_wndDepth);
	DDX_Control(pDX, IDC_LIST1, m_wndList);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDlgEmdParam, CDialog)
	//{{AFX_MSG_MAP(CDlgEmdParam)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_BN_CLICKED(IDOK, OnOk)
	ON_BN_CLICKED(IDC_BUTTON4, OnButton4)
	ON_BN_CLICKED(IDC_BUTTON5, OnButton5)
	ON_BN_CLICKED(IDC_BUTTON6, OnButton6)
	ON_BN_CLICKED(IDC_BUTTON7, OnButton7)
	ON_BN_CLICKED(IDC_BUTTON8, OnButton8)
	ON_LBN_DBLCLK(IDC_LIST2, OnDblclkList2)
	ON_LBN_SELCHANGE(IDC_LIST2, OnSelchangeList2)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgEmdParam message handlers

BOOL CDlgEmdParam::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_iSel = -1;
	int nID[] = {0, IDS_PARA, IDS_PREVALUE, IDS_VALUE, IDS_UNIT, IDS_DESCRIPTION};
	int iWidth[] =
		{
			30, 80, 70, 70, 60, 100
		};
	
	LV_COLUMN lvColumn;
		lvColumn.mask = LVCF_FMT |
			LVCF_SUBITEM |
			LVCF_TEXT |
			LVCF_WIDTH |
			LVCF_ORDER;
		lvColumn.fmt = LVCFMT_LEFT;

	CString strText = _T("#");
	for (int i = 0; i < cols; i++)
	{
		if (i)
		{
			strText.LoadString(nID[i]);
		}
		lvColumn.pszText = (LPTSTR)(LPCTSTR)strText;
		lvColumn.iSubItem = i;
		lvColumn.iOrder = i;
		lvColumn.cx = iWidth[i];
		m_wndList.InsertColumn(i, &lvColumn);
		m_wndList.EnableEditColumn(i, i);
	}
	m_wndList.EnableEditColumn(ppvalue, FALSE);

	DWORD dwStyle = m_wndList.GetExtendedStyle() & (~LVS_EX_FULLROWSELECT);
	dwStyle |= /*LVS_EX_CHECKBOXES |*/ LVS_EX_GRIDLINES;
	// m_wndList.SetExtendedStyle(dwStyle);

	if (m_pULEmd != NULL)
	{
		m_pULEmd->InitList(&m_wndList, _T("Params"));
		if (m_pULEmd->m_bLock && m_wndList.GetItemCount())
		{
			for (int i = 0; i < 3; i++)
			{
				GetDlgItem(IDC_BUTTON1+i)->ShowWindow(SW_HIDE);
			}
		}
		else
		{
			int nIDInv[] = { IDC_BUTTON4, IDC_BUTTON5, IDC_BUTTON7, IDC_BUTTON8};
			for (int i = 0; i < 4; i++)
			{
				GetDlgItem(nIDInv[i])->ShowWindow(SW_HIDE);
			}
		}
		
		m_pULEmd->GetDepthRange(m_lDepthF0, m_lDepthF1);
		if (m_pULEmd->m_depths.GetSize() < 4)
		{
			m_pULEmd->m_depths.RemoveAll();
			m_pULEmd->m_depths.Add(m_lDepthF0);
			m_pULEmd->m_depths.Add(m_lDepthF1);
			m_pULEmd->m_depths.InsertAt(0, m_lDepthF0);
			m_pULEmd->m_depths.Add(m_lDepthF1);
		}		
		m_pULEmd->InitDepths(&m_wndDepth);
		if (m_wndDepth.GetCount())
		{
			m_wndDepth.SetCurSel(0);
			m_iSel = 0;
			m_pULEmd->InitParam(&m_wndList, 0);
			if (m_pGraph != NULL)
			{
				m_pGraph->InitStops(m_pULEmd->m_depths, m_iSel);
			}
		}
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgEmdParam::OnButton1() 
{
	CMFCPropertyGridCtrl* pList = &m_wndList;
	if (pList != NULL)
	{
		int iItem = pList->GetItemCount();
		CString strText;
		strText.Format(_T("%d"), iItem);
		pList->InsertItem(iItem, strText);
		pList->SetItemText(iItem, pname, _T(""));
		pList->SetItemText(iItem, pvalue, _T(""));
		pList->SetItemText(iItem, punit, _T(""));
		pList->SetItemText(iItem, pcomment, _T(""));
	}
}

void CDlgEmdParam::OnButton2() 
{
	m_wndList.DeleteSelectedItems();	
}

void CDlgEmdParam::OnOk() 
{
	if (m_pULEmd != NULL)
	{
		if (m_pULEmd->m_bLock)
		{
			m_pULEmd->SaveList(&m_wndList, _T("Params"));
			if (m_iSel != -1)
				m_pULEmd->SaveParam(&m_wndList, m_iSel);
			m_pULEmd->SaveParams(m_pULEmd->m_strDef);
			if (m_pULEmd->ProcessEmd())
			{
				if (m_pGraph)
				{
					m_pGraph->ClearStops();
					m_pGraph->Invalidate();
				}
				GetParent()->PostMessage(UM_PROP_UPDATE, (WPARAM)m_pULEmd->m_pULFile);
				EndDialog(IDOK);
			}
		}
		else
		{
			OnCancel();
		}
	}
}

void CDlgEmdParam::OnButton4() 
{
	if (m_pULEmd == NULL)
		return;

	CMyFileDialog fd(FALSE, _T("Emd.xml"), _T("EMD_PARAM"), OFN_OVERWRITEPROMPT, _T("XML File(*.xml)|*.xml||"), this);
	if (fd.DoModal() == IDOK)
	{
		if (m_iSel != -1)
			m_pULEmd->SaveParam(&m_wndList, m_iSel);
		m_pULEmd->SaveParams(fd.GetPathName());
	}
}

void CDlgEmdParam::OnButton5() 
{
	if (m_pULEmd == NULL)
		return;

	CMyFileDialog fd(TRUE, _T("Emd.xml"), _T("EMD_PARAM"), OFN_FILEMUSTEXIST, _T("XML File(*.xml)|*.xml|All Files(*.*)|*.*||"), this);
	if (fd.DoModal() == IDOK)
	{
		CString strFile = fd.GetPathName();
		if (strFile.Right(4).CompareNoCase(_T(".txt")) == 0)
		{
			CArray<long,long> depths;
			try
			{
				CStdioFile file;
				if (!file.Open (strFile, CFile::modeRead))
				{
					TRACE(_T("File not found: %s"), strFile);
					return;
				}
				
				CString strText, strName, strValue;
				
				CStringArray arrTexts;
				int i;
				while (file.ReadString (strText))
				{
					strText.TrimLeft();
					strText.TrimRight();
					
					i = 0;
					while (strText.Replace(_T(" ="), _T("=")))
					{
						i++;
						if (i > 8)
						{
							TRACE("Left space large than 8.\n");
							return;
						}
					}
					
					i = 0;
					while (strText.Replace(_T("= "), _T("=")))
					{
						i++;
						if (i > 8)
						{
							TRACE("Right space large than 8.\n");
							return;
						}
					}
					
					SplitString(strText, ' ', arrTexts);
				}
				
				double fVal;
				LPTSTR stop;
				
				int nCount = m_wndList.GetItemCount();
				for (i = 0; i < arrTexts.GetSize(); i++)
				{
					strText = arrTexts[i];
					strText.TrimLeft();
					strText.TrimRight();
					if (strText.IsEmpty())
						continue;
					
					int iFind = strText.Find(_T("="));
					if (iFind < 1)
					{
						TRACE(_T("Text error : %s\n"), strText);
						return;
					}
					
					strName = strText.Left(iFind);
					strValue = strText.Right(strText.GetLength() - iFind - 1);
					fVal = _tcstod(strValue, &stop);
					if (strName == _T("SDEP"))
					{
						depths.Add(MTD(fVal));
					}
					else if (strName == _T("EDEP"))
					{
						depths.Add(MTD(fVal));
					}
					else
					{
						for (int i = 0; i < nCount; i++)
						{
							CString strPN = m_wndList.GetItemText(i, pname);
							if (strPN == strName)
							{
								m_wndList.SetItemText(i, pvalue, strValue);
								break;
							}
						}
						
						if (i == nCount)
							AfxMessageBox(strName);
					}
				}
				
			}
			catch (CFileException* pEx)
			{
				pEx->ReportError ();
				pEx->Delete ();
				
				return;
			}
		}
		else
		{
			if (m_pULEmd->LoadParams(strFile))
			{
				m_iSel = -1;
				m_pULEmd->InitDepths(&m_wndDepth);
				m_pULEmd->InitList(&m_wndList, _T("Params"));
				if (m_wndDepth.GetCount())
				{
					m_wndDepth.SetCurSel(0);
					m_pULEmd->InitParam(&m_wndList, 0);
				}
			}
		}
	}
}

void CDlgEmdParam::OnButton6() 
{
	if (m_pULEmd != NULL)
	{
		m_pULEmd->SaveList(&m_wndList, _T("Params"));
		m_pULEmd->SaveDefault();
	}
}

void CDlgEmdParam::OnButton7() 
{
	int iItem = m_pULEmd->InsertDepth(&m_wndDepth, m_wndDepth.GetCurSel());
	m_wndDepth.SetCurSel(iItem);
	if (m_pGraph != NULL)
		m_pGraph->InitStops(m_pULEmd->m_depths, iItem);
}

void CDlgEmdParam::OnButton8() 
{
	int iItem = m_wndDepth.GetCurSel();
	if (iItem != LB_ERR)
	{
		iItem = m_pULEmd->DeleteDepth(&m_wndDepth, iItem);
		m_wndDepth.SetCurSel(iItem);
		if (m_pGraph != NULL)
			m_pGraph->InitStops(m_pULEmd->m_depths, iItem);
	}
}

void CDlgEmdParam::OnDblclkList2() 
{
	CDlgPartRange dlg;
	int iItem = m_wndDepth.GetCurSel();
	CString strText;
	if (iItem == LB_ERR)
		return;

	m_pULEmd->GetDepthPart(iItem, dlg.m_lDepth0, dlg.m_lDepth1, dlg.m_lDepthA0, dlg.m_lDepthA1);

	if (dlg.DoModal() == IDOK)
	{
		iItem = m_pULEmd->SetDepth(&m_wndDepth, iItem, dlg.m_lDepth0, dlg.m_lDepth1, 0x03);
		m_wndDepth.SetCurSel(iItem);
		if (m_pGraph != NULL)
			m_pGraph->InitStops(m_pULEmd->m_depths, m_iSel);
	}
	
}

void CDlgEmdParam::OnSelchangeList2() 
{
	// Save values first
	if (m_iSel != -1)
	{
		m_pULEmd->SaveParam(&m_wndList, m_iSel);
	}

	m_iSel = m_wndDepth.GetCurSel();
	m_pULEmd->InitParam(&m_wndList, m_iSel);
	if (m_pGraph != NULL)
	{
		m_pGraph->InitStops(m_pULEmd->m_depths, m_iSel);
	}
}

void CDlgEmdParam::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	if (m_pULEmd)
	{
		int i = m_pULEmd->m_depths.GetSize();
		if (i > 3)
		{
			m_pULEmd->m_depths.RemoveAt(i-1);
			m_pULEmd->m_depths.RemoveAt(0);
		}
	}
}

void CDlgEmdParam::SetDepth(long lDepth, BOOL bStart /* = TRUE */)
{
	if ((m_iSel != -1) && (m_pULEmd != NULL))
	{
		if (bStart)
			m_pULEmd->SetDepth(&m_wndDepth, m_iSel, lDepth, 0, 0x01);
		else
			m_pULEmd->SetDepth(&m_wndDepth, m_iSel, 0, lDepth, 0x02);
		m_wndDepth.SetCurSel(m_iSel);
	}
}

void CDlgEmdParam::OnCancel() 
{
	// TODO: Add extra cleanup here
	if (m_pGraph)
	{
		m_pGraph->ClearStops();
		m_pGraph->Invalidate();
	}

	if (m_pULEmd)
	{
		int i = m_pULEmd->m_depths.GetSize();
		if (i > 3)
		{
			m_pULEmd->m_depths.RemoveAt(i-1);
			m_pULEmd->m_depths.RemoveAt(0);
		}
	}
	
	GetParent()->PostMessage(UM_PROP_UPDATE);

	CDialog::OnCancel();
}

void CDlgEmdParam::UpdateDepths(long* pDepths, int nCount)
{
	if (m_pULEmd == NULL)
		return;

	while (m_pULEmd->m_depths.GetSize() > 2)
	{
		m_pULEmd->m_depths.RemoveAt(1);
	}

	int nSize = m_pULEmd->m_depths.GetSize();
	if (nSize < 2)
		return;

	long lS = m_pULEmd->m_depths[0];
	long lE = m_pULEmd->m_depths[nSize-1];
	m_pULEmd->m_depths.InsertAt(1, lE);
	m_pULEmd->m_depths.InsertAt(0, lS);
	int i = 0;
	for (i = 0; i < nCount; i++)
	{
		if (pDepths[i] < lS)
			continue;

		if (pDepths[i] < lE)
		{
			nSize = m_pULEmd->m_depths.GetSize();
			if (nSize % 2)
			{
				m_pULEmd->m_depths.InsertAt(nSize - 2, pDepths[i-1]);
				nSize++;
			}
			m_pULEmd->m_depths.InsertAt(nSize - 2, pDepths[i]);
		}
		else
			break;
	}

	nSize = m_pULEmd->m_depths.GetSize();
	if (nSize % 2)
	{
		m_pULEmd->m_depths.InsertAt(nSize - 2, pDepths[i-1]);
	}

	m_pULEmd->InitDepths(&m_wndDepth);
	if (m_wndDepth.GetCount())
	{
		m_wndDepth.SetCurSel(0);
		m_iSel = 0;
		m_pULEmd->InitParam(&m_wndList, 0);
		if (m_pGraph != NULL)
		{
			m_pGraph->InitStops(m_pULEmd->m_depths, m_iSel);
		}
	}
}

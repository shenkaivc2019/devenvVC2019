// DlgCurveMerge.cpp : implementation file
//

#include "stdafx.h"
//#include "resource.h"
#include "DlgCurveMerge.h"
#include "ULFile.h"
#include "Curve.h"
#include "ComConfig.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgCurveMerge dialog


CDlgCurveMerge::CDlgCurveMerge(CDocument* pFile, CWnd* pParent /*=NULL*/)
	: CDialog(IDD_CURVE_MERGE, pParent)
{
	//{{AFX_DATA_INIT(CDlgCurveMerge)
	m_strASuffix = _T("");
	//}}AFX_DATA_INIT
	m_pAFile = NULL;
	m_pFile = pFile;
}


void CDlgCurveMerge::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgCurveMerge)
	DDX_Control(pDX, IDC_LIST2, m_lbXCurves);
	DDX_Control(pDX, IDC_LIST1, m_lbACurves);
	DDX_Text(pDX, IDC_EDIT3, m_strASuffix);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgCurveMerge, CDialog)
	//{{AFX_MSG_MAP(CDlgCurveMerge)
	ON_BN_CLICKED(IDC_BUTTON9, OnButton9)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_BN_CLICKED(IDC_BUTTON3, OnButton3)
	ON_BN_CLICKED(IDC_BUTTON4, OnButton4)
	ON_BN_CLICKED(IDC_BUTTON5, OnButton5)
	ON_BN_CLICKED(IDC_BUTTON6, OnButton6)
	ON_BN_CLICKED(IDC_BUTTON7, OnButton7)
	ON_BN_CLICKED(IDC_BUTTON8, OnButton8)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgCurveMerge message handlers

BOOL CDlgCurveMerge::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	InitCurves(m_pFile, &m_lbXCurves);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgCurveMerge::InitCurves(CDocument* pFile, CCheckListBox* pList)
{
	CULFile* pFileDoc = (CULFile*)pFile;
	pList->ResetContent();
	if (pFile == NULL)
		return;
	
	int nCount = pFileDoc->m_vecCurve.size();
	for (int i = 0; i < nCount; i++)
	{
		CCurve* pCurve = (CCurve*)pFileDoc->m_vecCurve.at(i);

		CString string;
		if (pCurve->m_strSource.GetLength())
		{
			string.Format("%-8s[%s]", pCurve->m_strName, pCurve->m_strSource);
		}
		else
			string = pCurve->m_strName;

		int nItem = pList->AddString(string);
		pList->SetCheck(nItem, TRUE);
		pList->SetItemData(nItem, (DWORD_PTR)pCurve);
	}
}

void CDlgCurveMerge::OnButton9() 
{
	CStringArray aryFileFtName;
	int iCount = AfxGetComConfig()->GetAllFileFormatName(aryFileFtName);
	if (iCount < 2)
	{
		AfxMessageBox(IDS_FORMAT_NOT_SUPPORT, MB_OK|MB_ICONINFORMATION);
		return;
	}

	CString  strFilters;
	CString  strExts;
	for (int i = 0; i < iCount; i++)
	{
        CString strFilter = aryFileFtName.GetAt(i);
		if (strExts.GetLength())
			strExts += ";";
		strExts += "*." + strFilter;
		strFilters += strFilter + " Files" + " (*." + strFilter + ")" + "|" + "*." + strFilter + "|";
	}
	
	strFilters += "All Supported Files|" + strExts;
	strFilters += "|";

	CString strFileName;
	GetDlgItemText(IDC_EDIT1, strFileName);
	CFileDialog fd(TRUE, NULL, strFileName, OFN_FILEMUSTEXIST, strFilters, this);
	fd.m_ofn.lStructSize = 88;
	if (fd.DoModal() != IDOK)
		return;
	
	strFileName = fd.GetPathName();
	CULFile* pAFile = new CULFile;
	if (!pAFile->ReadAllCurves(strFileName))
	{
		delete pAFile;
		return;
	}

	if (m_pAFile != NULL)
	{
		delete m_pAFile;
		m_pAFile = NULL;
	}

	m_pAFile = pAFile;
	InitCurves(m_pAFile, &m_lbACurves);
	SetDlgItemText(IDC_EDIT1, strFileName);
	OnButton4();
}

void CDlgCurveMerge::OnOK() 
{
	if (!UpdateData())
		return;

	int nCount = m_lbXCurves.GetCount();
	for (int i = 0; i < nCount; i++)
	{
		if (m_lbXCurves.GetCheck(i))
			continue;
		
		CCurve* pCurve = (CCurve*)m_lbXCurves.GetItemData(i);
		if (pCurve->IsDepthCurve())
			continue;

		((CULFile*)m_pFile)->DeleteCurve(pCurve);
		pCurve->Release();
	}

	if (m_pAFile != NULL)
	{
		nCount = m_lbACurves.GetCount();
		for (int i = 0; i < nCount; i++)
		{
			if (m_lbACurves.GetCheck(i))
			{
				CCurve* pCurve = (CCurve*)m_lbACurves.GetItemData(i);
				if (pCurve->IsDepthCurve())
					continue;

				((CULFile*)m_pAFile)->DeleteCurve(pCurve);
				CString strName = pCurve->m_strName;
				strName += m_strASuffix;
				pCurve->SetName(strName);
				((CULFile*)m_pFile)->AddCurve(pCurve);
			}
		}
		
		delete m_pAFile;
		m_pAFile = NULL;
	}
	
	EndDialog(IDOK);
}

void CDlgCurveMerge::OnCancel() 
{
	if (m_pAFile)
	{
		delete m_pAFile;
		m_pAFile = NULL;
	}
	
	CDialog::OnCancel();
}

void CDlgCurveMerge::CheckAll(CCheckListBox* pList, BOOL bCheck /* = TRUE */)
{
	int nCount = pList->GetCount();
	for (int i = 0; i < nCount; i++)
	{
		pList->SetCheck(i, bCheck);
	}
}

void CDlgCurveMerge::CheckInverse(CCheckListBox* pList)
{
	int nCount = pList->GetCount();
	for (int i = 0; i < nCount; i++)
	{
		int nCheck = pList->GetCheck(i);
		pList->SetCheck(i, !nCheck);
	}
}

void CDlgCurveMerge::CheckDif(CCheckListBox* pList, CCheckListBox* pDList)
{
	CStringList strings;
	int nCount = pDList->GetCount();
	int i = 0;
	for ( ; i < nCount; i++)
	{
		if (pDList->GetCheck(i))
		{
			CCurve* pCurve = (CCurve*)pDList->GetItemData(i);
			strings.AddTail(pCurve->m_strName);
		}
	}

	nCount = pList->GetCount();
	for (i = 0; i < nCount; i++)
	{
		CCurve* pCurve = (CCurve*)pList->GetItemData(i);
		if (strings.Find(pCurve->m_strName))
			pList->SetCheck(i, FALSE);
		else
			pList->SetCheck(i, TRUE);
	}
}

void CDlgCurveMerge::OnButton1() 
{
	CheckAll(&m_lbACurves);
}

void CDlgCurveMerge::OnButton2() 
{
	CheckAll(&m_lbACurves, FALSE);
}

void CDlgCurveMerge::OnButton3() 
{
	CheckInverse(&m_lbACurves);
}

void CDlgCurveMerge::OnButton4() 
{
	CheckDif(&m_lbACurves, &m_lbXCurves);
}

void CDlgCurveMerge::OnButton5() 
{
	CheckAll(&m_lbXCurves);
}

void CDlgCurveMerge::OnButton6() 
{
	CheckAll(&m_lbXCurves, FALSE);
}

void CDlgCurveMerge::OnButton7() 
{
	CheckInverse(&m_lbXCurves);
}

void CDlgCurveMerge::OnButton8() 
{
	CheckDif(&m_lbXCurves, &m_lbACurves);
}

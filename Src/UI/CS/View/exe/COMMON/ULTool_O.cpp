// ULTool.cpp: implementation of the CULTool class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ULTool.h"
#include "ULCOMMDEF.h"
#include "Project.h"
#include "MainFrm.h"
#include "ChildFrm.h"
#include "ServiceTableItem.h"
#include "Storage.h"
#include "StreamArchive.h"
#include "..\\UL2000\\CalChartView.h"

#ifdef _PERFORATION
#include "UL2000.h"
#include "ParamsView.h"
#include "CalFileDialog.h"
#ifdef _OLD_SCOPES
#include "OscilloGraphWnd.h"
#else
#include "SCPCommon.h"
#endif

#include "CalChartObj.h"
#include "CalChildFrm.h"
#include "IniFile.h"
#else
#include "Logic.h"
#endif

#include "ULFile.h"
#include "sys/timeb.h"
#include "IToolParam.h"
#include "Utility.h"
#include "ComConfig.h"
#include "ScoutFunctionDef.h"
#include "Unitx.h"
#include "DSF.h"
#include "GraphHeaderView.h"
#include "MultiLang.h"
#include "VParam.h"
#include "LocalRes.h"
#include "ScoutFunctionDef.h"
#include <imagehlp.h>


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]     = __FILE__;
#define new DEBUG_NEW
#endif

typedef		DWORD(FAR PASCAL* _ULGETVERSION)();
typedef		IUnknown*(FAR PASCAL* _NEWTOOL)();

UINT		ToolThreadProc(LPVOID pParam);

ULMIMP CULTool::GetIUnits(IUnits** pIUnits)
{
	*pIUnits = g_units;
	return UL_NO_ERROR;
}

#define	colx	6
#define cth		75
#define cgh		75
// #define _SINGLE_COEF

typedef struct tagCALCELL 
{
	DWORD dwPhases;
	UINT  nPhase;
	CStringList ls;
} CALCELL;

//////////////////////////////////////////////////////////////////////
// Static Member Variant
//////////////////////////////////////////////////////////////////////

vec_ts		CULTool::m_vecTools;			// 当前所有打开的仪器
DWORD		CULTool::m_dwLoad = 0;
BOOL		CULTool::m_bSaveRawData	= FALSE;// 原始数据存盘
int			CULTool::m_nFileID = 0;

long        CULTool::m_lCurDepth	= 0 ;	// 当前仪器深度
long        CULTool::m_lCurTime		= 0 ;	// 当前仪器时间

double      CULTool::m_fCurSpeed	= 0 ;   // 当前仪器速度
double      CULTool::m_fCurTemp		= 0 ;	// 当前仪器温度
double      CULTool::m_fCurTension	= 0 ;	// 当前仪器张力
double      CULTool::m_fCurVol		= 0 ;	// 当前仪器电压

double		CULTool::m_dCurCountDown= -5000 ;   // 当前倒计数
IProject*	CULTool::m_pIProject = NULL;

long		CULTool::m_lCurDepthIndex = 0; // 存取当前原始数据时的深度索引

CString strCalPhase[4] = { "Master", "Before", "After", "Primary"};
UINT nPhaseTime[4] = { 0, 3, 1, 2 };

COLORREF clrTm[5] = { /*RGB(0, 255, 0), RGB(128, 255, 128),*/RGB(204, 255, 204), 
					RGB(204, 255, 204), RGB(204, 255, 204), 
					RGB(255, 0, 0), RGB(255, 255, 255) };

extern IScout* g_pScout;
extern CProject* g_pActPrj;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CULTool::CULTool()
{
	m_strToolDllName.Empty();
	m_hTool			 = NULL;
	m_nToolCount	 = 0;
	m_nSameToolIndex = -1;
	m_pEvent		 = NULL;
	m_hExchangeFile  = NULL;

	m_bOpened		 = FALSE;
	m_pChannelEvent  = NULL;
	m_pWaitEvent	 = NULL;

	m_nWorkMode  	 = WORKMODE_IDLE;
	m_nToolState	 = TS_IDLE;
	m_nUseCount  	 = 0;

	m_pITool		 = NULL;
	m_pIParam		 = NULL;
	m_pIBoard		 = NULL;
	m_pITool2		 = NULL;
	m_pCalWnd		 = NULL;	// 用户自定义刻度对话框

	m_dwDisabled  = 0;
	m_dwCalibrate = 0;

	m_bCaled[0]	  = FALSE;		//	车间刻度									    
	m_bCaled[1]	  = FALSE;		//	测前刻度									    
	m_bCaled[2]	  = FALSE;		//	测后刻度
	m_bCaled[3]   = FALSE;

	m_tmCaled[0]  = tmNew;
	m_tmCaled[1]  = tmNew;
	m_tmCaled[2]  = tmNew;
	m_tmCaled[3]  = tmNew;

	m_dwReport	  = CR_GRAPH;
	
	m_CalWnds[0] = NULL;
	m_CalWnds[1] = NULL;
	m_CalWnds[2] = NULL;
	m_CalWnds[3] = NULL;

	m_bCalDefined[0] = FALSE;
	m_bCalDefined[1] = FALSE;
	m_bCalDefined[2] = FALSE;
	m_bCalDefined[3] = FALSE;

	m_bCalUserPrint[0] = FALSE;
	m_bCalUserPrint[1] = FALSE;
    m_bCalUserPrint[2] = FALSE;
	m_bCalUserPrint[3] = FALSE;

	m_pArrCalCoefs= NULL;		//	store the callibration coefficient for every    
								//	curve to be calibrated					    
	m_nUserCalCurves = 0;		//	At first the cal curve of the user equate 0									    
	m_nCalUserTime	 = 0;
								    
	
	//	The demo calibration type of the tool is sytem asst type
	m_nCalType[0] = UL_CAL_ASST;
    m_nCalType[1] = UL_CAL_ASST;
	m_nCalType[2] = UL_CAL_ASST;
	m_nCalType[3] = UL_CAL_ASST;

	m_hMetaReport[0] = NULL;
	m_hMetaReport[1] = NULL;
	m_hMetaReport[2] = NULL;
	m_hMetaReport[3] = NULL;
	m_szMetaReport[0] = 0;
	m_szMetaReport[1] = 0;
	m_szMetaReport[2] = 0;
	m_szMetaReport[3] = 0;
	m_dwCalMetaReports = CP_MASTER|CP_BEFROR|CP_AFTER|CP_PRIMARY;

	m_strCalFilePath.Empty();

	m_uCalPhase		= 0;
	m_nCalDelayTime = 0;
	m_nUseCount     = 0;
	m_nReadUseCount = 0;  //add by gj20130605
	
	m_CalFilePathArray.SetSize(4);
	m_CalFilePathArray[0].Empty();
	m_CalFilePathArray[1].Empty();
	m_CalFilePathArray[2].Empty();
	m_CalFilePathArray[3].Empty();

	m_dwCalReports = 0;
	m_CalReportArray.SetSize(4);
	m_CalReportArray[0].Empty();
	m_CalReportArray[1].Empty();
	m_CalReportArray[2].Empty();
	m_CalReportArray[3].Empty();

	m_bRunThread1 = FALSE;
	//m_fTimeStartTime = 0;
	m_dwStartTickCount = 0;
	m_pToolInfoPack = NULL;
	
	m_strGroupTo.Empty();
	m_strGroupFrom.Empty();
	m_nCalDays = 30;
	m_dwDisabled = 0;

#ifdef _OLD_SCOPES
	ZeroMemory(&m_ScopeSettings, sizeof(ScopeSettings));
	m_ScopeSettings.nDataPerSec = 10;
	m_ScopeSettings.nCycle = 20;
	m_ScopeSettings.nCollTimes = 10;
#endif

	m_nCurGroup = -1;

	m_lNoDataCount = 0;
}

CULTool::~CULTool()
{
    ClearCurves();
	ClearStatusCurves();
	ClearCalCurves();
	ClearChannels();
	ClearCoefficient();

	// Clear Buffer Array
	for (int i = 0; i < m_BufferArray.GetSize() ; i++)
	{
		IOBuffer buf = m_BufferArray.GetAt(i);
		delete buf.pBuffer;
		if (buf.pChannelLength)
			delete buf.pChannelLength;
		delete buf.pLength;
	}
	m_BufferArray.RemoveAll();

#ifdef _PERFORATION
	
#ifdef _OLD_SCOPES
	for (i = 0; i < m_Scopes.GetSize(); i++)
	{
		delete (CULOscilliscope*)m_Scopes.GetAt(i);
	}

	int nSize = m_arDepthScopeItems.GetSize();
	for (i = nSize - 1; i >= 0; i--)
	{
		DepthScopeItem* pItem = (DepthScopeItem*)m_arDepthScopeItems.GetAt(i);
		delete pItem;
		m_arDepthScopeItems.RemoveAt(i);
	}

#else
	for (i = 0; i < m_Scopes.GetSize(); i++)
	{
		delete (CSCPCommon*)m_Scopes.GetAt(i);
	}
#endif
	
#endif

	m_Scopes.RemoveAll();
	ClearCtrlWnds();
	
	if (m_pChannelEvent != NULL)
	{
		delete m_pChannelEvent;
		m_pChannelEvent = NULL;
	}
	
	if (m_pWaitEvent != NULL)
	{
		delete m_pWaitEvent;
		m_pWaitEvent = NULL;
	}
	
	InterlockedExchange(&m_bRunThread1, 0);	//	停止线程函数
	
	if (m_pEvent != NULL)
	{
		m_pEvent->SetEvent();	// 释放所有等待的线程
		Sleep(10);
		delete m_pEvent;
		m_pEvent = NULL;
	}
	
	ReleaseTool();
	
	ClearDotsMap(&m_mapDots);

	for (i = 0; i < 4; i++)
		ClearCalDescs(i);

	if (m_pToolInfoPack != NULL)
		m_pToolInfoPack->m_pULTool = NULL;

	for (UINT uCalPhase=0; uCalPhase<4; uCalPhase++)
	{
		if(m_hMetaReport[uCalPhase]!=NULL)
			DeleteMetaFile(m_hMetaReport[uCalPhase]);
	}

	for (i = 0; i < 4; i++)
	{
		if(!m_mapCalGroup[i].IsEmpty())
			m_mapCalGroup[i].RemoveAll();
	}
}

BEGIN_ULI_MAP(CULTool, IULTool)
	ULI_PART(IID_IULTool, IULTool)
	ULI_PART(IID_IULToolEx, IULToolEx)
	ULI_PART(IID_IULScout, IULScout)
END_ULI_MAP()

//////////////////////////////////////////////////////////////////////
// Tool Thread Process
//////////////////////////////////////////////////////////////////////
UINT ToolThreadProc(LPVOID pParam)
{
    CULTool*pULTool = (CULTool*) pParam;
	
    if (pULTool == NULL)
    {
        AfxEndThread(0);
        return 0;
    }
	
    int nCount = pULTool->m_Channels.GetSize();
    if (nCount <= 0)
    {
        AfxEndThread(0);
        return 0;
    }
#ifndef _LOGIC	
    HANDLE hWait = NULL;
    for (int i = 0; i < nCount; i++)
    {
        CChannel* pChannel = (CChannel*)pULTool->m_Channels.GetAt(i);
        if (pChannel->m_pEvent && pChannel->m_pEvent->m_hObject)
		{
			hWait = pChannel->m_pEvent->m_hObject;
			break;
		}
    }

    if (hWait == NULL)
	{
		AfxEndThread(0);
        return 0;
	}
	
    while (pULTool->m_bRunThread1)
    {
        // 等待所有的通道将数据准备好
        // 当其中一个通道出问题时，会出现无限等待的错误。
        // int nRetCode = ::WaitForMultipleObjects(nCount, pHandle, TRUE, INFINITE);
        int nRetCode = ::WaitForSingleObject(hWait, INFINITE); 
		
        if (nRetCode == WAIT_OBJECT_0)  //开始处理数据
        {
            if (!pULTool->m_bRunThread1)
                break;
			
            // 是否仪器可以接收数据 
            int nToolState = pULTool->GetToolState();
			
            if (nToolState != TS_IDLE)
            {
                //判断工作模式
                switch (pULTool->m_nWorkMode)
                {
				case WORKMODE_LOG:
					{
						int nRet = ::WaitForSingleObject(pULTool->m_DepthToolEvent, INFINITE);
						if (nRet == WAIT_OBJECT_0)
						{
							if (!pULTool->m_bRunThread1)
								break;
							pULTool->LogIO();
						}
					}

					// 加入与之共享通道的仪器的LOGIO
					break;
				case WORKMODE_CAL:
					pULTool->CalibrateIO();        
					if (pULTool->m_pCalWnd != NULL &&
						pULTool->m_uCalMsgID != 0)
						::SendMessage(pULTool->m_pCalWnd->m_hWnd,
						pULTool->m_uCalMsgID, 0, 0);
					break;
				case WORKMODE_VERIFY:
					pULTool->VerifyIO();        
					if (pULTool->m_pCalWnd != NULL &&
						pULTool->m_uCalMsgID != 0)
						::SendMessage(pULTool->m_pCalWnd->m_hWnd,
						pULTool->m_uCalMsgID, 0, 0);
					break;
				default:
					break;
                }
            }
			pULTool->UnlockChannel();
            if (pULTool->m_pEvent != NULL)
                pULTool->m_pEvent->SetEvent();
        }
        else if (nRetCode == WAIT_FAILED)
            pULTool->m_bRunThread1 = FALSE;
    }
#endif	
	
    //AfxEndThread(0);
    return 0;
}

UINT DepthToolThreadProc(LPVOID pParam)
{
    CULTool*pULTool = (CULTool*) pParam;
	
    if (pULTool == NULL)
    {
        AfxEndThread(0);
        return 0;
    }
	
    int nCount = pULTool->m_Channels.GetSize();
    if (nCount <= 0)
    {
        AfxEndThread(0);
        return 0;
    }
	
    HANDLE hWait = NULL;
    for (int i = 0; i < nCount; i++)
    {
        CChannel* pChannel = (CChannel*)pULTool->m_Channels.GetAt(i);
        if (pChannel->m_pEvent && pChannel->m_pEvent->m_hObject)
		{
			hWait = pChannel->m_pEvent->m_hObject;
			break;
		}
    }

    if (hWait == NULL)
	{
		AfxEndThread(0);
        return 0;
	}
	
    while (pULTool->m_bRunThread1)
    {
        // 等待所有的通道将数据准备好
        // 当其中一个通道出问题时，会出现无限等待的错误。
        // int nRetCode = ::WaitForMultipleObjects(nCount, pHandle, TRUE, INFINITE);
        int nRetCode = ::WaitForSingleObject(hWait, INFINITE); 
		
        if (nRetCode == WAIT_OBJECT_0)  //开始处理数据
        {
            if (!pULTool->m_bRunThread1)
                break;
			
            // 是否仪器可以接收数据 
            int nToolState = pULTool->GetToolState();
			
            if (nToolState != TS_IDLE)
            {
                //判断工作模式
                switch (pULTool->m_nWorkMode)
                {
				case WORKMODE_LOG:
					{
						pULTool->LogIO();
						for (int i = 0; i < pULTool->m_vecTools.size(); i++)
						{
							CULTool* pTool = (CULTool*)pULTool->m_vecTools[i].pIULTool;
							pTool->m_DepthToolEvent.SetEvent();
						}
					}
				
					// 加入与之共享通道的仪器的LOGIO
					break;
				case WORKMODE_CAL:
					pULTool->CalibrateIO();        
					if (pULTool->m_pCalWnd != NULL &&
						pULTool->m_uCalMsgID != 0)
						::SendMessage(pULTool->m_pCalWnd->m_hWnd,
						pULTool->m_uCalMsgID, 0, 0);
					break;
				case WORKMODE_VERIFY:
					pULTool->VerifyIO();        
					if (pULTool->m_pCalWnd != NULL &&
						pULTool->m_uCalMsgID != 0)
						::SendMessage(pULTool->m_pCalWnd->m_hWnd,
						pULTool->m_uCalMsgID, 0, 0);
					break;
				default:
					break;
                }
            }
			
			pULTool->UnlockChannel();
            if (pULTool->m_pEvent != NULL)
                pULTool->m_pEvent->SetEvent();
        }
        else if (nRetCode == WAIT_FAILED)
            pULTool->m_bRunThread1 = FALSE;
    }
	
    AfxEndThread(0);
    return 0;
}

int OurReportingFunction( int reportType, char *userMessage, int *retVal )
{
/*
* Tell the user our reporting function is being called.
* In other words - verify that the hook routine worked.
    */
// 	fprintf(stdout, "Inside the client-defined reporting function.\n");
// 	fflush(stdout);
	
	/*
    * When the report type is for an ASSERT,
    * we'll report some information, but we also
    * want _CrtDbgReport to get called - 
    * so we'll return TRUE.
    *
    * When the report type is a WARNing or ERROR,
    * we'll take care of all of the reporting. We don't
    * want _CrtDbgReport to get called - 
    * so we'll return FALSE.
    */
	if (reportType == _CRT_ASSERT)
	{
//		gl_num_asserts++;
//		fprintf(stdout, "This is the number of Assertion failures that have occurred: %d \n", gl_num_asserts);
// 		fflush(stdout);
// 		fprintf(stdout, "Returning TRUE from the client-defined reporting function.\n");
//		fflush(stdout);
		return(TRUE);
	} else {
// 		fprintf(stdout, "This is the debug user message: %s \n", userMessage);
// 		fflush(stdout);
// 		fprintf(stdout, "Returning FALSE from the client-defined reporting function.\n");
// 		fflush(stdout);
		return(TRUE);
	}
	
	/*
    * By setting retVal to zero, we are instructing _CrtDbgReport
    * to continue with normal execution after generating the report.
    * If we wanted _CrtDbgReport to start the debugger, we would set
    * retVal to one.
    */
	retVal = 0;
}


//////////////////////////////////////////////////////////////////////
// Load/Release the User's Tool Library
//////////////////////////////////////////////////////////////////////
BOOL CULTool::LoadTool(CString strToolName, BOOL bQuiet /* = FALSE */)
{
	int nPos = strToolName.ReverseFind('\\');
	CString strToolPath; 
	if (nPos > 0)
	{
		strToolPath = strToolName.Left(nPos);
	}

	char szCurPath[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, szCurPath); 
	SetCurrentDirectory(strToolPath);
    m_hTool = LoadLibrary(strToolName);	
	SetCurrentDirectory(szCurPath);

	CString str;
    if (m_hTool == NULL)
	{
		DWORD dwErr = GetLastError();

		if (bQuiet)
			return FALSE;

		// 统一显示错误提示
//		AfxFormatString1(str, IDS_FAIL_LOADTOOL, strToolName);
		//AfxMessageBox(str);
//		g_Message += str;
		return FALSE;
	}

	// 查询仪器动态库版本号
	_ULGETVERSION ULGetVersion = (_ULGETVERSION)GetProcAddress(m_hTool, "ULGetVersion");
	if (ULGetVersion == NULL)
	{
		FreeLibrary(m_hTool);
		m_hTool = NULL;
		if (bQuiet)
			return FALSE;

		// 统一显示错误提示
		AfxFormatString1(str, IDS_FAIL_TOOLDAMAGE, strToolName);
		//AfxMessageBox(str);
		g_Message += str;
		return FALSE;
	}
	
	DWORD dwVersion = ULGetVersion();
	if (dwVersion < MAKEWORD(_UL2000_VERSION_MINOR, _UL2000_VERSION_MAJOR))
	{
		FreeLibrary(m_hTool);
		m_hTool = NULL;
		if (bQuiet)
			return FALSE;

		// 统一显示错误提示
		AfxFormatString1(str, IDS_FAIL_TOOLVERSION, strToolName);
		//AfxMessageBox(str);
		g_Message += str;
		return FALSE;
	}	
	
	// 查询仪器库接口
	_NEWTOOL CreateTool = (_NEWTOOL) GetProcAddress(m_hTool, "CreateTool");
	if (CreateTool == NULL)
	{
		FreeLibrary(m_hTool);
		m_hTool = NULL;
		if (bQuiet)
			return FALSE;

		// 统一显示错误提示
		AfxFormatString1(str, IDS_FAIL_TOOLDAMAGE, strToolName);
		//AfxMessageBox(str);
		g_Message += str;
		return FALSE;
	}
	
	m_pITool = (ITool*)CreateTool();
	if (m_pITool == NULL)
	{
		FreeLibrary(m_hTool);
		m_hTool = NULL;
		if (bQuiet)
			return FALSE;

		// 统一显示错误提示
		AfxFormatString1(str, IDS_FAIL_CREATETOOL, strToolName);
		//AfxMessageBox(str);
		g_Message += str;
		return FALSE;
	}
	
	HRESULT hr;
	hr = m_pITool->QueryInterface(IID_ITool, (void**)&m_pITool);
	if (FAILED(hr))
	{
		FreeLibrary(m_hTool);
		m_hTool = NULL;
		m_pITool = NULL;
		return FALSE;    
	}

	m_strToolDllName = strToolName;
	if (m_dwLoad == 0)
		strInfoPath = Gbl_ConfigInfo.FindInfoPath(strToolName);
	
	CLocalRes lr(m_hTool);

#ifdef _DEBUG
	_CRT_REPORT_HOOK crt_rep = _CrtSetReportHook(OurReportingFunction);
#endif
	try
	{
#ifdef _DEBUG
		// When we load it from a file, disable debug error messages
		if (m_dwLoad & DL_ERROR)
		{
			
			try
			{
				m_pITool->SetULTool(this);
				m_pITool->InitTool();
				m_pITool->DeclareDataExchange();
			}
			catch (CException* e)
			{
				e->ReportError();
				e->Delete();
			}
			catch (...) 
			{
				TRACE("!!!Load tool unknown error\n");
			}
			
		}
		else
#endif
		{
			m_pITool->SetULTool(this);
			m_pITool->InitTool();
			m_pITool->DeclareDataExchange();
		}
	}
	catch (CException* e)
	{
		e->ReportError();
		e->Delete();
	}
	catch (...) 
	{

	}

	// Query interfaces of this tool supported once
	hr = m_pITool->QueryInterface(IID_IToolParam, (void**)&m_pIParam);
	hr = m_pITool->QueryInterface(IID_IToolBoard, (void**)&m_pIBoard);
	hr = m_pITool->QueryInterface(IID_ITool2, (void**)&m_pITool2);
#ifdef _DEBUG
	_CrtSetReportHook(crt_rep);
#endif
	return TRUE;
}

ULMIMP CULTool::ReloadTool(LPCTSTR lpszToolFile)
{
	ReleaseTool();
	if (LoadTool(lpszToolFile))
		return UL_NO_ERROR;
	else
		return UL_ERROR;
}

void CULTool::ReleaseTool(BOOL bEmptyPath /* = TRUE */)
{
	if (m_pITool)
	{
		ASSERT(m_hTool);
		m_pITool->Release();
		::FreeLibrary(m_hTool);
		m_pITool = NULL;
		m_pIParam = NULL;
		m_pIBoard = NULL;
		m_pITool2 = NULL;
		m_hTool = NULL;
	}

	if (bEmptyPath)
		m_strToolDllName.Empty();
}

//////////////////////////////////////////////////////////////////////
// Channel Operations
//////////////////////////////////////////////////////////////////////
void CULTool::AddChannel(CChannel* pChannel)
{
#ifndef _LOGIC
    m_Channels.Add(pChannel);
#endif
}

BOOL CULTool::AllocChannel(CChannel* pChannel, int nIndex)
{
    BOOL       bReturn = FALSE;
#ifndef _LOGIC
    // 设置命令参数
    AskCommand	cmd;
	cmd.ex_context.ch_param.dDataRate = pChannel->dblRate;
	cmd.ex_context.ch_param.lPrecision = pChannel->lPrecision;
	cmd.ex_context.ch_param.dwCHBufferSize = pChannel->dwBufferSize;
	cmd.ex_context.ch_param.result = CH_FAIL;
	_tcscpy( cmd.ex_context.ch_param.chUsedToolName, strToolName);
	_tcscpy( cmd.ex_context.ch_param.chBoardType, pChannel->strType);
	_tcscpy( cmd.ex_context.ch_param.chEventName, pChannel->m_strEventName);
	_tcscpy( cmd.ex_context.ch_param.chMapFileName, pChannel->m_strMapFileName);
// 	if (pChannel->m_pDataItem != NULL)
// 		_tcscpy( cmd.ex_context.ch_param.chCurveName, pChannel->m_pDataItem->m_strDataName);
	cmd.ex_context.ch_param.dwToolIndex = m_vecTools.size() + nIndex;
	cmd.nCommandType = UL_ASKCHANNEL;


    if (m_hExchangeFile == NULL)
        return FALSE;   // 建立数据交换映射文件是否成功 
    memcpy(m_lpCodeBuffer, &cmd, sizeof(AskCommand)); // 发送命令
    m_pChannelEvent->SetEvent();      // 设置通道信号 

    int nRetCode = ::WaitForSingleObject(m_pWaitEvent->m_hObject, 1000); // INFINITE

    // 通道申请成功
    if (nRetCode == WAIT_OBJECT_0)
    {
        AskCommand* pReturnCmd = (AskCommand*) m_lpCodeBuffer;
        // 设置通道属性
        pChannel->dwID = pReturnCmd->ex_context.ch_param.dwCHID;
        pChannel->m_strEventName = pReturnCmd->ex_context.ch_param.chEventName;
        pChannel->m_strMapFileName = pReturnCmd->ex_context.ch_param.chMapFileName;
//         if (pChannel->m_pDataItem != NULL)
//             pChannel->m_pDataItem->m_dwCHID = pChannel->dwID;
        bReturn = TRUE;  
        // 分配失败
        if (pReturnCmd->ex_context.ch_param.result == CH_FAIL)
            bReturn = FALSE; 
    }
    else
        bReturn = FALSE; 
#endif
    return bReturn;
}

ULMPTRIMP CULTool::AllocNewChannel(DWORD nBufferSize, LPCTSTR lpszType, double dblRate, long lPrecision, void* pDataItem)
{	
	if (m_dwLoad & DL_CHANNEL)
		return NULL;
#ifdef _LOGIC
	return NULL;
#else
	CChannel* pChannel = new CChannel;
	pChannel->SetBufferSize(nBufferSize);
	pChannel->strType = lpszType;
	pChannel->dblRate = dblRate;
	pChannel->lPrecision= lPrecision;
	pChannel->m_pDataItem = (CDataItem*)pDataItem;
	m_Channels.Add(pChannel);
	m_arAllocChannelSize.Add(nBufferSize);
	GetAllChannelSize();
    return pChannel;
#endif
}

int CULTool::GetChannelOffset(int nChannel)
{
    int nOffset = 0; 
#ifndef _LOGIC
    for (int i = 0; i < m_Channels.GetSize(); i++)
    {
        if (i == nChannel)
            break;

        CChannel* pChannel = (CChannel*) m_Channels.GetAt(i);
        nOffset += pChannel->dwBufferSize;
    }
#endif
    return nOffset;
}

int CULTool::GetChannelLength(int nChannel)
{
    int nLength = 0;
#ifndef _LOGIC
	if (nChannel >= 0 && nChannel < m_Channels.GetSize())
    {
        CChannel* pChannel = (CChannel*) m_Channels.GetAt(nChannel);
        nLength = pChannel->dwBufferSize;
    }
#endif
    return nLength;
}

ULMPTRIMP CULTool::GetChannelBuffer(int nIndex)
{
    if (m_nWorkMode == WORKMODE_COMPUTEPLAYBACK)
    {
        if (m_BufferArray.GetSize() > 0)
        {
            IOBuffer buf  = m_BufferArray.GetAt(0);
			int nOffset = 0;
			for (int i = 0; i < buf.nChannelCount; i++)
			{
				if (i == nIndex) 
				{
					return &buf.pBuffer[nOffset];
				}
				nOffset += buf.pChannelLength[i];
			}
           return NULL;
        }
        else
            return NULL;
    }
#ifndef _LOGIC
    if (nIndex < m_Channels.GetSize())
        return ((CChannel *) m_Channels.GetAt(nIndex))->GetBuffer();
#endif
    return NULL;
}

ULMPTRIMP CULTool::GetChannelBuffer(LPCTSTR lpszChannel)
{
#ifndef _LOGIC
	for (int i = 0; i < m_Channels.GetSize(); i++)
    {
        if (_tcscmp(lpszChannel, ((CChannel *)m_Channels.GetAt(i))->strType) == 0)
            return GetChannelBuffer(i);
    }
#endif
    return NULL;
}

// 如果多个仪器共享一个通道得到的是所有仪器占用通道的总大小
ULMINTIMP CULTool::GetAllChannelSize()
{
    nBufferSize = 0;
#ifndef _LOGIC
    for (int i = 0; i < m_Channels.GetSize(); i++)
    {
        CChannel* pChannel = (CChannel*) m_Channels.GetAt(i);
        nBufferSize += pChannel->dwBufferSize;
    }
#endif
    return nBufferSize;
}

// 得到的仅是自身申请的通道大小
ULMINTIMP CULTool::GetAllocChannelSize()
{
	int nSize = 0;
	for (int i = 0; i < m_arAllocChannelSize.GetSize(); i++)
	{
		nSize += m_arAllocChannelSize.GetAt(i);
	}
	return nSize;
}

void CULTool::ClearChannels()
{
#ifndef _LOGIC
    for (int i = 0; i < m_Channels.GetSize(); i++)
    {
        CChannel* pChannel = (CChannel*)m_Channels.GetAt(i);
        delete pChannel;
    }
#endif
    m_Channels.RemoveAll();
}

//////////////////////////////////////////////////////////////////////
// Curve Operations
//////////////////////////////////////////////////////////////////////
void CULTool::AddCurve(CCurve* pCurve)
{
    pCurve->m_strSource = strToolName;
	pCurve->m_pData->MInit();
    m_Curves.Add(pCurve);
}

ULMPTRIMP CULTool::AddCurve(LPCTSTR lpszName, LPCTSTR lpszUnit, 
						double fLeftValue, double fRightValue, 
						long lDepthOffset, int nOffMode /* = 0 */,	
						int nFilterPiont /* = 0 */, int nFilterType /* = 2 */, 
						int nDimension /* = 1 */, int nPointFrame /* = 1 */, 
						int nCurveMode /* = CURVE_NORMAL */, int nValueType /* = VT_R4 */, 
						LPCTSTR lpszIDName /* = NULL */)
{
    CCurve* pCurve = new CCurve(NB_TOOL, nValueType);

    pCurve->m_strName = lpszName;
    pCurve->m_LeftValue = fLeftValue;
    pCurve->m_RightValue = fRightValue;
    pCurve->m_nStyle = PS_SOLID;
    pCurve->m_crColor = RGB(0, 0, 0);
	pCurve->m_strIDName	= lpszIDName;
	pCurve->m_nMode = nCurveMode;

    // 替换并兼容以前的平滑滤波
    if (nFilterType == 2)
    {
        CULFilter* pFilter = new CULFilter;
		pFilter->m_pCurve = pCurve;
        pFilter->LoadFilter("Smooth");
        if (pFilter != NULL)
        {
            // 先清空参数列表
            pFilter->RemoveAllParams();
            // 依次加入各个参数
            for (int n = 0; n < nFilterPiont; n++)
            {
                CString str;
                str.Format("%d", n);
                pFilter->AddParam("Param" + str, 1.0 / (double) nFilterPiont,
                                  "Param" + str);
            }
            pCurve->SetFilter(pFilter);
        }
    }

#ifdef _RUNIT
	pCurve->SetUnits(lpszUnit);
	pCurve->m_strLRUnit = lpszUnit;
	if (m_dwLoad & EL_UNIT)
		g_units->AddCurveUnit(lpszName, lpszUnit, TRUE);
#else
	pCurve->m_pData->m_strUnit = lpszUnit;
#endif	// _RUNIT

	if (nCurveMode & CURVE_DEPTH)
		pCurve->m_bDepth = TRUE;
	pCurve->m_pData->m_nMode = nCurveMode;
	pCurve->m_pData->m_nDimension = nDimension;
    pCurve->m_pData->SetPointFrame(nPointFrame);
	pCurve->m_pData->m_lDepthOffset = lDepthOffset;
	pCurve->m_pData->m_nOffsetMode = nOffMode;
    AddCurve(pCurve);
    return pCurve;
}

ULMPTRIMP CULTool::AddStatusCurve(LPCTSTR lpszName, LPCTSTR lpszUnit, double fLeftValue, double fRightValue, int nDimension /* = 1 */, int nPointFrame /* = 1 */, int nCurveMode /* = CURVE_STATUS */, int nValueType /* = VT_R4 */, LPCTSTR lpszIDName /* = NULL */)
{
	CCurve* pCurve = new CCurve(NB_TOOL, nValueType);
	pCurve->m_strSource = strToolName;

	pCurve->m_pData->m_strUnit = lpszUnit;
	pCurve->m_strName = lpszName;
	pCurve->m_LeftValue = fLeftValue;
	pCurve->m_RightValue = fRightValue;
	pCurve->m_nStyle = PS_SOLID;
	pCurve->m_crColor = RGB( 0, 0, 0);

	pCurve->m_pData->m_nDimension = nDimension;
    pCurve->m_pData->SetPointFrame(nPointFrame);
	pCurve->m_pData->m_nMode = CURVE_STATUS;
	pCurve->m_strIDName = lpszIDName;
	pCurve->m_pData->MInit(1);
	pCurve->AddNewData();

	m_StatusCurves.Add(pCurve);	

	return pCurve;
}

void CULTool::ClearCurves()
{
    for (int i = 0; i < m_Curves.GetSize(); i++)
    {
        CCurve* pCurve = (CCurve*) m_Curves.GetAt(i);
        if (pCurve != NULL)
			pCurve->Release();
    }
    m_Curves.RemoveAll();
}

void CULTool::ClearStatusCurves()
{
    for (int i = 0; i < m_StatusCurves.GetSize(); i++)
    {
        CCurve* pCurve = (CCurve*) m_StatusCurves.GetAt(i);
        if (pCurve != NULL)
			pCurve->Release();
    }
    m_StatusCurves.RemoveAll();
}

ULMIMP CULTool::AddCurveData(int nCurve, long lDepth, void* pValue, long lTime /* = 0 */)
{
    if (nCurve < m_Curves.GetSize())
    {
        CCurve* pCurve = (CCurve*) m_Curves.GetAt(nCurve);
        pCurve->AddData(lDepth, pValue, lTime);
        return UL_NO_ERROR;
    }
    else
        return UL_ERROR;
}

ULMIMP CULTool::AddCurveData(LPCTSTR lpszName, long lDepth, void* pValue, long lTime /* = 0 */, LPCTSTR lpszIDName /* = NULL */)
{
    for (int i = 0; i < m_Curves.GetSize(); i++)
    {
        CCurve*pCurve = (CCurve*) m_Curves.GetAt(i);
        if (_tcscmp(pCurve->m_strName, lpszName) == 0)
        {
			if (lpszIDName == NULL)
				pCurve->AddData(lDepth, pValue, lTime);
			else
			{
				if(_tcscmp(pCurve->m_strIDName, lpszIDName)==0)
					pCurve->AddData(lDepth, pValue, lTime);
			}
            return UL_NO_ERROR;
        }
    }
    return UL_ERROR;
}

ULMIMP CULTool::AddStatusCurveData(int nCurve, void* pValue, int nShowPos)
{
	if (nCurve < m_StatusCurves.GetSize())
    {
        CCurve*pCurve = (CCurve*) m_StatusCurves.GetAt(nCurve);
		pCurve->AddStatusData(0, pValue);
		pCurve->m_dStartPos = nShowPos;
        return UL_NO_ERROR;
    }
    else
        return UL_ERROR;
}

ULMIMP CULTool::AddStatusCurveData(LPCTSTR lpszName, void* pValue, int nShowPos /* =0 */, LPCTSTR lpszIDName /* = NULL */)
{
	for (int i = 0; i < m_StatusCurves.GetSize(); i++)
    {
        CCurve *pCurve = (CCurve*) m_StatusCurves.GetAt(i);
        if (_tcscmp(pCurve->m_strName, lpszName) == 0)
        {
			if(lpszIDName == NULL)
			{
				pCurve->AddStatusData(0, pValue);
				pCurve->m_dStartPos = nShowPos;
			}
			else
			{
				if(_tcscmp(pCurve->m_strIDName, lpszIDName)==0)
				{
					pCurve->AddStatusData(0, pValue);
					pCurve->m_dStartPos = nShowPos;
				}
			}
            return UL_NO_ERROR;
        }
    }
    return UL_ERROR;
}

ULMIMP CULTool::GetULCurves(CURVES& vecCurve, UINT nFile /* = 0 */)
{
	vecCurve.clear();	
	if (nFile != 0)	// 当前工程中的背景曲线
	{
		// 查找打开文件中的曲线
		nFile--;
	}
	
	POSITION pos = CULDoc::m_DocList.FindIndex(nFile);
	if (pos != NULL)
	{
		CULDoc* pDoc = CULDoc::m_DocList.GetAt(pos);
		if (pDoc != NULL)
		{
			vecCurve.assign(pDoc->m_vecCurve.begin(), pDoc->m_vecCurve.end());
			return UL_NO_ERROR;
		}
	}

	return UL_ERROR;
}

ULMPTRIMP CULTool::GetULCurve(LPCTSTR lpszCurveName, UINT nFile /* = 0 */, LPCTSTR lpszIDName /* = NULL */)
{
	if (nFile != 0)	// 当前工程中的系统曲线
	{
		nFile--;
	}
	
	POSITION pos = CULDoc::m_DocList.FindIndex(nFile);
	if (pos != NULL)
	{
		CULDoc* pDoc = CULDoc::m_DocList.GetAt(pos);
		if (pDoc != NULL)
		{
			for (int i = 0; i < pDoc->m_vecCurve.size(); i++)
			{
				CCurve* pCurve = (CCurve*)pDoc->m_vecCurve.at(i);
				if (pCurve == NULL)
					continue ;

				if (pCurve->m_strName == lpszCurveName)
				{
					if (lpszIDName == NULL)
						return pCurve;
					
					if (pCurve->m_strIDName == lpszIDName)
						return pCurve;
				}
			}
		}
	}
	return NULL;
}

//////////////////////////////////////////////////////////////////////
// Map/Unmap Exchange File Operation
//////////////////////////////////////////////////////////////////////

BOOL CULTool::MapExchangeFile()
{
    //建立数据交换映射文件 
    if (m_hExchangeFile == NULL)
    {
        //创建一个物理文件无关的内存映射
        m_hExchangeFile = CreateFileMapping((HANDLE) 0xFFFFFFFF, 0,
                                            PAGE_READWRITE, 0, 4096,
                                            "AskChannelData.Map");

        /*
        HANDLE CreateFileMapping(
         HANDLE hFile,                       //物理文件句柄
         LPSECURITY_ATTRIBUTES lpAttributes, //安全设置
         DWORD flProtect,                    //保护设置
         DWORD dwMaximumSizeHigh,            //高位文件大小
         DWORD dwMaximumSizeLow,             //低位文件大小
         LPCTSTR lpName                      //共享内存名称
         ); 
        */

        //把文件数据映射到进程的地址空间
        m_lpCodeBuffer = MapViewOfFile(m_hExchangeFile, FILE_MAP_ALL_ACCESS,
                                       0, 0, 4096);

        /*
        LPVOID MapViewOfFile(HANDLE hFileMappingObject, //文件映像对象句柄
         DWORD dwDesiredAccess,							//保护设置
         DWORD dwFileOffsetHigh,						//数据文件的高位偏移地址
         DWORD dwFileOffsetLow,							//数据文件的低位偏移地址
         DWORD dwNumberOfBytesToMap);					//数据文件的映射长度
        */

        if (m_lpCodeBuffer == NULL)
        {
            CloseHandle(m_hExchangeFile);
            return FALSE;
        }
    }
    return TRUE;
}

void CULTool::UnMapExchangeFile()
{
    if (m_hExchangeFile != NULL)
    {
        UnmapViewOfFile(m_lpCodeBuffer);
        CloseHandle(m_hExchangeFile);
        m_hExchangeFile = NULL;
    }
}

//////////////////////////////////////////////////////////////////////
// Tool Property Operations
//////////////////////////////////////////////////////////////////////

/*
ULMIMP CULTool::SetToolProperty(int nCount,   // 仪器个数
            CString strName,  // 仪器名称
            CString strEnName, // 英文标识
            CString strChName, // 中文标识
            CString strType,  // 仪器类型
            float fToolLen,  // 仪器长度
            float fBasePoint)  // 基本测量点值
{
	 strName.MakeUpper();
	 m_nToolCount = nCount;
	 strToolName = strName;
	 strcpy(strENName,strEnName);
	 strcpy(m_ToolInfo.chChName,strChName);
	 strcpy(m_ToolInfo.chType,strType);
	 m_ToolInfo.fLength = fToolLen;
	 m_ToolInfo.fBasePoint = fBasePoint;

	 // heap err@ ~CString() 
	 // _stdcall function  can't use CString as param

	 return UL_NO_ERROR;
}*/

ULMIMP CULTool::SetToolProperty(int nCount, LPCTSTR lpszName, LPCTSTR lpszEnName, LPCTSTR lpszCnName, LPCTSTR lpszType, float fToolLen, float fBasePoint)
{
    m_nToolCount = nCount;
	strToolName = lpszName;
	strToolName.MakeUpper();
	strENName = lpszEnName;
	strCNName = lpszCnName;
    strType = lpszType;
    lLength = MTD(fToolLen);
 /* m_ToolInfo.fBasePoint = fBasePoint;*/
    return UL_NO_ERROR;
}

//////////////////////////////////////////////////////////////////////
// Tool Windows Operations
//////////////////////////////////////////////////////////////////////

ULMIMP CULTool::AddToolScope(int nType, LPCTSTR lpszName)
{
	if ((m_dwDisabled) || (nType < 1))
		return UL_NO_ERROR;

#ifdef _PERFORATION

#ifdef _OLD_SCOPES
	ULScopePara scopeData;
    scopeData.nType = nType; // UL_OSCILLISCOPE_DEPTH 深度....
    memcpy(scopeData.szName, lpszName, sizeof(scopeData.szName));
	CULOscilliscope* p = new CULOscilliscope;
	if (p->OpenOscilliscope(&scopeData))
	{
		m_Scopes.Add(p);
		return UL_NO_ERROR;
	}

	delete p;	

#else
	CString strSCPType[] = { _T(""), _T("SCPStd.SCPStdCtrl.1"), _T("SCPDepth.SCPDepthCtrl.1"),
		_T("SCPAcoustic.SCPAcousticCtrl.1"), _T("SCPAcoustic.SCPAcousticCtrl.1"),
		_T("SCPAzim.SCPAzimCtrl.1"), _T("SCPPolar.SCPPolarCtrl.1") };

	CSCPCommon* pSCPCom = new CSCPCommon;
	pSCPCom->m_strName = lpszName;
    pSCPCom->m_nType = nType; 
	if (nType < 7)
	{
		pSCPCom->SetProgID(strSCPType[nType]);
	}
	else
	{
		CString strType;
		strType.Format(_T("%d"), nType);
		CIniFile file;
		strType = file.GetProp(_T("SCPCommon"), strType, strType);
		pSCPCom->SetProgID(strType);
	}

	m_Scopes.Add(pSCPCom);
#endif

#endif

    return UL_NO_ERROR;
}

/*  仪器控制刻度窗口(对话框)添加 */
ULMIMP CULTool::AddUserDlg(void* pDlg, UINT nID, LPCTSTR lpszTitle /* = _T("") */)
{
	CBInfo* pCtrlBar = new CBInfo;
	pCtrlBar->pWnd = (CWnd*) pDlg;
	pCtrlBar->nID = nID;
	
	if (lpszTitle == NULL)
	{
		int nWnds = m_arrWnd.GetSize();
		if (nWnds > 0)
			pCtrlBar->strTitle.Format("%s[%d]", strToolName, nWnds);
		else
			pCtrlBar->strTitle = strToolName;
	}
	else
		pCtrlBar->strTitle = lpszTitle;
	m_arrWnd.Add(pCtrlBar);
    return UL_NO_ERROR;
}

void CULTool::ClearCtrlWnds()
{
	for (int i = 0; i < m_arrWnd.GetSize(); i++)
	{
		CBInfo* pCtrlBar = m_arrWnd.GetAt(i);
		if (pCtrlBar != NULL)
			delete pCtrlBar;
	}
	m_arrWnd.RemoveAll();
}

ULMIMP CULTool::AddUserCalDlg(void* pDlg, UINT nID)
{
	if (m_dwLoad & DL_CALIBRATE)
		return UL_ERROR;

    if (m_pCalWnd == NULL)
    {
        m_pCalWnd = (CDialog*)pDlg;
		m_nCalWndID = nID;
		if (AfxIsValidAddress(pDlg, sizeof(CObject)))
		{
			if (m_pCalWnd->IsKindOf(RUNTIME_CLASS(CDialog)))
			{
				((CDialog*)pDlg)->Create(nID);
			}
		}

		m_CalWnds[0] = (CDialog*)pDlg;
		m_CalWndsID[0] = nID;
    }

    return UL_NO_ERROR;
}

//////////////////////////////////////////////////////////////////////
// Tool Open/Close Operations
//////////////////////////////////////////////////////////////////////
ULMIMP CULTool::OpenTool()
{
#ifndef _LOGIC
    if (m_bOpened)
        return UL_NO_ERROR;

    if (m_pEvent != NULL)
    {
        delete m_pEvent;
        m_pEvent = NULL;
    }

    // 仪器事件
    m_pEvent = new CEvent(FALSE, FALSE, GetEventName(), NULL);

    if (m_pEvent == NULL)
        return UL_ERROR;

    // 建立数据交换事件
    MapExchangeFile();

    // 等待响应事件，通道事件初始化
    if (m_pWaitEvent == NULL)
        m_pWaitEvent = new CEvent(FALSE, FALSE, "RespondAskChannel", NULL);
    if (m_pChannelEvent == NULL)
        m_pChannelEvent = new CEvent(FALSE, FALSE, "AskChannel", NULL);

    int nChCount = m_Channels.GetSize();
    for (int i = 0; i < nChCount; i++)
    {
        CChannel* pChannel = (CChannel*) m_Channels.GetAt(i);
        if (AllocChannel(pChannel, i) == FALSE)
        {
            CString strPrompt;
			AfxFormatString2(strPrompt, IDS_FAIL_APPLYCH, strToolName, pChannel->strType);
			AEI(IDS_PROJECT, theApp.m_pszAppName, MAKELONG(ET_OTHER, ET_ERROR), strPrompt, strPrompt);
            g_Message += strPrompt;
        }

        pChannel->OpenChannel();
    }
	
	GetAllChannelSize(); // 保证共享通道的数据空间申请正确
    m_bRunThread1 = TRUE;

	if (IsDepthTool())
	{
		if (AfxBeginThread(DepthToolThreadProc, this, THREAD_PRIORITY_TIME_CRITICAL) == NULL)
		{
			return UL_ERROR;
		}
	}
	else
	{
		if (AfxBeginThread(ToolThreadProc, this, THREAD_PRIORITY_TIME_CRITICAL) == NULL)
		{
			return UL_ERROR;
		}
	}
	
    if (m_pITool)
	{
        m_pITool->OpenTool();
	}

    m_bOpened = TRUE;
#endif
	
    return UL_NO_ERROR;
}

ULMIMP CULTool::ReplayOpenTool()
{
	if (m_pITool)
	{
        m_pITool->OpenTool();
	}

	if (m_bOpened)
		return UL_NO_ERROR;

	m_bOpened = TRUE;
	GetAllChannelSize();
    /*
     * add the opened tool to the group
     */
//by xwh 2013-6-27 多次添加 导致nCount仪器数目为仪器数目2倍，在SetToolInetface中已经完成如下操作，无需重复执行导致BUG
//     ULTOOL oTool;
//     oTool.pIULTool = static_cast <IULTool*>(this);
//     oTool.pITool = m_pITool;
//     m_vecTools.push_back(oTool);
	
    return UL_NO_ERROR;
}

ULMIMP CULTool::ReplayCloseTool()
{
	if (m_pITool)
        m_pITool->CloseTool();

    if (!m_bOpened)
        return UL_NO_ERROR;

	m_bOpened = FALSE;

	
	/*
     * remove the opened tool from the group of active service item
     */
    int nIndex     = -1;
    int nSizeTools = m_vecTools.size();
    if (nSizeTools == 1)
    {
        m_vecTools.clear();
    }
    else
    {
        for (int i = 0; i < nSizeTools; i++)
        {
            CULTool* pULTool = (CULTool*) m_vecTools.at(i).pIULTool;
            if (pULTool->strENName == strENName)
            {
                nIndex = i;
                // break;
            }
        }

        if (nIndex >= 0)
        {
            m_vecTools.erase(m_vecTools.begin() + nIndex);
        }
    }

    return UL_NO_ERROR;
}

ULMIMP CULTool::CloseTool()
{
    if (m_pITool)
        m_pITool->CloseTool();

    if (!m_bOpened)
        return UL_NO_ERROR;

    m_bRunThread1 = FALSE;
	m_DepthToolEvent.SetEvent();
    if (m_pEvent != NULL)
    {
        m_pEvent->SetEvent();
        delete m_pEvent;
        m_pEvent = NULL;
    }

    if (m_pWaitEvent != NULL)
    {
        delete m_pWaitEvent;
        m_pWaitEvent = NULL;
    }

    if (m_pChannelEvent != NULL)
    {
        delete m_pChannelEvent; 
        m_pChannelEvent = NULL;
    }
#ifndef _LOGIC
    for (int i = 0; i < m_Channels.GetSize(); i++)
    {
        CChannel*pChannel = (CChannel*) m_Channels.GetAt(i);
        pChannel->CloseChannel();
    }
#endif
    UnMapExchangeFile();
    m_bOpened = FALSE;

	/*
     * remove the opened tool from the group of active service item
     */
    int nIndex     = -1;
    int nSizeTools = m_vecTools.size();
    if (nSizeTools == 1)
    {
        m_vecTools.clear();
    }
    else
    {
        for (int i = 0; i < nSizeTools; i++)
        {
            CULTool* pULTool = (CULTool*) m_vecTools.at(i).pIULTool;
            if (strcmp(pULTool->strENName, strENName) == 0)
            {
                nIndex = i;
                // break;
            }
        }

        if (nIndex >= 0)
        {
            m_vecTools.erase(m_vecTools.begin() + nIndex);
        }
    }

    return UL_NO_ERROR;
}

/* -------------------------- *
 *  Log Operation
 * -------------------------- */
ULMIMP CULTool::BeforeLog(ULLogInfo* pLogInfo)
{

	InitBoundInfo();

	if (m_pITool)
		m_pITool->BeforeLog(pLogInfo);

	m_DepthToolEvent.ResetEvent();
    SetToolState(TS_LOG);

	// before logging, remove all data from the curvelist
    // ClearCurvesData();
	if (m_nWorkMode != WORKMODE_COMPUTEPLAYBACK)
		StartChannel();

    return UL_NO_ERROR;
}

ULMIMP CULTool::LogIO()
{
#ifndef _LOGIC
	if (m_nWorkMode != WORKMODE_COMPUTEPLAYBACK)
	{
		if (m_bSaveRawData)
		{
			m_CS.Lock();

			// 申请空间 
			BYTE* pBuffer;
			int* pLength;
			int nChannels = m_Channels.GetSize();
			int* pChannelLength;
			if (m_nUseCount >= m_BufferArray.GetSize()) // 暂存数大于已申请的缓冲大小
			{
				IOBuffer buf;
				buf.pBuffer = (BYTE *) new BYTE[nBufferSize + sizeof(long/*深度*/) + sizeof(long/*时间*/) + sizeof(long/*索引*/)];
				buf.pLength = new int(nBufferSize + sizeof(long) + sizeof(long) + sizeof(long)); // 包括插入的深度、时间、索引的长度
				buf.nChannelCount = nChannels;
				buf.pChannelLength = NULL;
				if (nChannels > 0)
					buf.pChannelLength = new int[nChannels];
				pBuffer = buf.pBuffer;
				pLength = buf.pLength;
				pChannelLength = buf.pChannelLength;
				m_BufferArray.Add(buf);
			}
			else   // 使用已申请的缓冲区
			{
				pBuffer = (BYTE*)(m_BufferArray.GetAt(m_nUseCount)).pBuffer;  // 一帧数据的缓冲区
				pLength = (int*)(m_BufferArray.GetAt(m_nUseCount)).pLength; // 一帧数据的缓冲区大小
				pChannelLength = (int*)(m_BufferArray.GetAt(m_nUseCount)).pChannelLength; // 通道长度
			}
			m_nUseCount++;
			
			// 填充数据
			*pLength = 0; // 当前通道相对于本仪器所有通道的偏移
			for (int i = 0; i < nChannels; i++)
			{
				CChannel* pChannel = (CChannel*) m_Channels.GetAt(i);
				if (pChannel->m_lpDataBuffer != NULL)
				{
					pChannelLength[i] = pChannel->GetData(&pBuffer[*pLength], pChannel->dwBufferSize);
					*pLength += pChannelLength[i];
					//memcpy(&pBuffer[nOffset], pChannel->m_lpDataBuffer, pChannel->dwBufferSize);
				}
			}

			 // add the depth specification to the data block for compute replay of the raw data
		//	memcpy(&pBuffer[*pLength], &m_lRDSDepth, sizeof(long /*深度*/));
			memcpy(&pBuffer[*pLength], &m_lCurDepth, sizeof(long /*深度*/));
			*pLength += sizeof(long);
			//  memcpy(&pBuffer[nBufferSize + sizeof(long)], &m_lRawDataSaveTime, sizeof(long /*时间*/));
			// add the time and depth index to the data block for compute replay of the raw data
			memcpy(&pBuffer[*pLength], &m_lCurTime, sizeof(long /*时间*/));
			*pLength += sizeof(long);
			memcpy(&pBuffer[*pLength], &m_lCurDepthIndex, sizeof(long /*索引*/));
			*pLength += sizeof(long);

			m_CS.Unlock();
		}
		else
			m_nUseCount = 0;
	}
	//int nCount = GetTickCount() - nTick;
	//TRACE("Tool LogIO Time %d\n", nCount);
	if (m_pITool)
	{
		m_pITool->LogIO();
	}

#endif

    return UL_NO_ERROR;
}

ULMIMP CULTool::AfterLog()
{
    if (m_pITool)
        m_pITool->AfterLog();

    SetToolState(TS_IDLE);
    return UL_NO_ERROR;
}


//////////////////////////////////////////////////////////////////////
// ULTool Operations
//////////////////////////////////////////////////////////////////////

CString CULTool::GetEventName()
{
	CString str;
    str.Format("%s%d", strToolName, m_nToolCount);
    return (str + "Event");
}


//////////////////////////////////////////////////////////////////////
// Calibrate Operation
//////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////
// 向当前刻度类型增加刻度曲线
///////////////////////////////////////////////////////////////////////
void CULTool::AddCalCurve(ULCalCurveData* pCurve, UINT nCalGroup/* = 0*/)
{
    while (pCurve[0].szName[0] != 0)
    {
        AddCalCurve(pCurve[0].szName, pCurve[0].szUnit, 
					pCurve[0].szTip, pCurve[0].iCalMethod, nCalGroup);
        pCurve++;
    }
}

void CULTool::AddCalCurve(CString strName, CString strUnit, CString strTip,
                          int nPrecis, UINT nCalGroup/* = 0*/)
{
    CULCalCurve*pCalCurve = new CULCalCurve();

    pCalCurve->m_strName = strName;
    pCalCurve->m_strUnit = strUnit;
    pCalCurve->m_strTip = strTip;
    /*if (nMethod == 1)
        pCalCurve->m_bCalMethod = FALSE;
    else
        pCalCurve->m_bCalMethod = TRUE;*/
	pCalCurve->m_nPrecis = 2;//默认初始化精度为2 以后从xml中读取 XWH 2013--5-29

	pCalCurve->m_nCalGroup = nCalGroup; // add by bao 2013/0603 刻度分组

    AddCalCurve(pCalCurve);
}

void CULTool::AddCalCurve(CULCalCurve* pCalCurve)
{
    for (int i = 0; i < m_CalCurves[m_uCalPhase].GetSize(); i++)
    {
        CULCalCurve* pCurve = (CULCalCurve*)m_CalCurves[m_uCalPhase].GetAt(i);
        if (pCurve->m_strName == pCalCurve->m_strName)
		{
			delete pCalCurve;
            return;
		}
    }
    m_CalCurves[m_uCalPhase].Add(pCalCurve);
}
///////////////////////////////////////////////////////////////////////
// 在线段链表上添加一条刻度线段
///////////////////////////////////////////////////////////////////////
void CULTool::AddCalCurveSeg(ULCalSegData* pSeg)
{
    while (pSeg[0].szName[0] != 0)
    {
        CULCalCurveSeg* p = new CULCalCurveSeg();
        p->m_strName = pSeg[0].szName;
        p->m_strUnit = pSeg[0].szUnit;
        p->m_fValue = pSeg[0].fValue;
        AddCalCurveSeg(pSeg[0].szCurveName, p);
        pSeg++;
    }
}

void CULTool::AddCalCurveSeg(CString strCurveName, CULCalCurveSeg* pSeg)
{
    for (int i = 0; i < m_CalCurves[m_uCalPhase].GetSize(); i++)
    {
        CULCalCurve* pCurve = (CULCalCurve*)
			m_CalCurves[m_uCalPhase].GetAt(i);
        if (pCurve->m_strName == strCurveName)
        {
            AddCalCurveSeg(pCurve, pSeg);
            return;
        }
    }
    AfxMessageBox(IDS_NOEXSIT_NOCUR);
}

void CULTool::AddCalCurveSeg(CULCalCurve* pCurve, CULCalCurveSeg* pCS)
{
	if (pCurve->GetCurveSeg(pCS->m_fValue))
	{
		AfxMessageBox(IDS_EXSIT_CALSEG, MB_OK);
		return ;
	}

    pCurve->m_CalCSs.Add(pCS);
}

void CULTool::AddCalDot(ULCalDotData* pDot)
{
    while (pDot[0].szName[0] != 0)
    {
        AddCalDot(pDot[0].szCurveName, pDot[0].szName, pDot[0].szAliax,
                  pDot[0].nPriority, pDot[0].nDestime, pDot[0].bFixedPoint,
                  pDot[0].szTip);

        pDot++;
    }
}

void CULTool::AddCalDot(CString strCurveName, CString strDotName,
                        CString strAlias, UINT uPri, UINT uDes,
                        BOOL bFixedPoint, CString szTip)
{
    for (int i = 0; i < m_CalCurves[m_uCalPhase].GetSize(); i++)
    {
        CULCalCurve* pCurve = (CULCalCurve*)
                              m_CalCurves[m_uCalPhase].GetAt(i);
        if (pCurve->m_strName == strCurveName)
        {
            if (pCurve->m_CalCSs.GetSize() == 0)
            {
                CULCalCurveSeg* pSeg = new CULCalCurveSeg();
                pSeg->m_fValue = 0.0;
                pCurve->m_CalCSs.Add(pSeg);
            }
            for (int j = 0; j < pCurve->m_CalCSs.GetSize(); j++)
            {
                CULCalCurveSeg* pSeg = (CULCalCurveSeg*)
                                       pCurve->m_CalCSs.GetAt(j);
                for (int k = 0; k < pSeg->m_CalDotList.GetSize(); k++)
                {
                    CULCalDot* p = (CULCalDot*) pSeg->m_CalDotList.GetAt(k);
                    if (strDotName == p->m_strName)
                    {
                        CString str;
						str.LoadString(IDS_EXSIT_CALDOT);
                        //AfxMessageBox(strDotName + str);提示次数太多没必要提示
                        return;
                    }
                }
                CULCalDot* pDot = new CULCalDot(strDotName, strAlias, uPri,
                                                uDes, bFixedPoint, szTip);
                if (bFixedPoint)
                    pDot->m_bCal = bFixedPoint;
                int nIndexDot = pSeg->m_CalDotList.GetSize();
                pDot->m_dwIndex = (i << 16) + (j << 8) + nIndexDot;    //To Set the dot's index
                pSeg->m_CalDotList.Add(pDot);
            }
        }
    }
}

// 设置刻度点当前刻度阶段的采集时间
ULMIMP CULTool::SetCalDotTime(LPCTSTR lpszDotName, DWORD dwTime)
{
    for (int i = 0; i < m_CalCurves[m_uCalPhase].GetSize(); i++)
    {
        CULCalCurve* pCurve = (CULCalCurve*)
                              m_CalCurves[m_uCalPhase].GetAt(i);

        if (pCurve->m_CalCSs.GetSize() <= 0)
            continue;
        for (int j = 0; j < pCurve->m_CalCSs.GetSize(); j++)
        {
            CULCalCurveSeg* pSeg = (CULCalCurveSeg*)
                                   pCurve->m_CalCSs.GetAt(j);
            for (int k = 0; k < pSeg->m_CalDotList.GetSize(); k++)
            {
                CULCalDot* p = (CULCalDot*)pSeg->m_CalDotList.GetAt(k);
                if (lpszDotName == p->m_strName)
                {
                    p->m_iDestTm = dwTime;
                }
            }
        }
    }
	return UL_NO_ERROR;
}

ULMIMP CULTool::SetCalDotVarMin(LPCTSTR lpszCurve, LPCTSTR lpszDot, double fMinValue)
{
	for (int i = 0; i < 3; i++)
	{
		CULCalCurve* pCur = GetCalCurve(i, lpszCurve);
		if (pCur == NULL)
			continue ;
		
		for (int j = 0; j < pCur->m_CalCSs.GetSize(); j++)
		{
			CULCalCurveSeg* pCS = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(j);
			CULCalDot* pDot = pCS->GetCalDot(lpszDot);
			if (pDot)
			{
				pDot->m_fUserMin = fMinValue;
				pDot->Exceed();
				// return UL_NO_ERROR;
			}
		}
	}
	
	return UL_NO_ERROR;
}

ULMIMP CULTool::SetCalDotVarMax(LPCTSTR lpszCurve, LPCTSTR lpszDot, double fMaxValue)
{
	for (int i = 0; i < 3; i++)
	{
		CULCalCurve* pCur = GetCalCurve(i, lpszCurve);
		if (pCur == NULL)
			continue ;
		
		for (int j = 0; j < pCur->m_CalCSs.GetSize(); j++)
		{
			CULCalCurveSeg* pCS = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(j);
			CULCalDot* pDot = pCS->GetCalDot(lpszDot);
			if (pDot)
			{
				pDot->m_fUserMax = fMaxValue;
				pDot->Exceed();
				// return UL_NO_ERROR;
			}
		}
	}
	
	return UL_NO_ERROR;
}

ULMIMP CULTool::SetCalDotVarNormal(LPCTSTR lpszCurve, LPCTSTR lpszDot, double fNormalValue)
{
	for (int i = 0; i < 3; i++)
	{
		CULCalCurve* pCur = GetCalCurve(i, lpszCurve);
		if (pCur == NULL)
			continue ;
		
		for (int j = 0; j < pCur->m_CalCSs.GetSize(); j++)
		{
			CULCalCurveSeg* pCS = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(j);
			CULCalDot* pDot = pCS->GetCalDot(lpszDot);
			if (pDot)
			{
				pDot->m_fUserNormal = fNormalValue;
				// return UL_NO_ERROR;
			}
		}
	}
	
	return UL_NO_ERROR;
}

ULMIMP CULTool::SetCalDotVarRange(LPCTSTR lpszCurve, LPCTSTR lpszDot, double fMinVal, double fMaxVal, double fNorVal)
{
	for (int i = 0; i < 4; i++)
	{
		CULCalCurve* pCur = GetCalCurve(i, lpszCurve);
		if (pCur == NULL)
			continue ;
		
		for (int j = 0; j < pCur->m_CalCSs.GetSize(); j++)
		{
			CULCalCurveSeg* pCS = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(j);
			CULCalDot* pDot = pCS->GetCalDot(lpszDot);
			if (pDot)
			{
				pDot->m_fUserMin = fMinVal;
				pDot->m_fUserMax = fMaxVal;
				pDot->m_fUserNormal = fNorVal;
				pDot->Exceed();
			}
		}
	}
	
	return UL_NO_ERROR;
}

ULMIMP CULTool::SetCalDotVarRange(UINT nPhase , LPCTSTR lpszCurve, LPCTSTR lpszDot, double fMinVal, double fMaxVal, double fNorVal)
{
	CULCalCurve* pCur = GetCalCurve(nPhase, lpszCurve);
	if (pCur == NULL)
		UL_ERROR ;
	
	for (int j = 0; j < pCur->m_CalCSs.GetSize(); j++)
	{
		CULCalCurveSeg* pCS = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(j);
		CULCalDot* pDot = pCS->GetCalDot(lpszDot);
		if (pDot)
		{
			pDot->m_fUserMin = fMinVal;
			pDot->m_fUserMax = fMaxVal;
			pDot->m_fUserNormal = fNorVal;
			pDot->Exceed();
		}
	}
	
	
	return UL_NO_ERROR;
}

ULMIMP CULTool::SetCalDotStandardRange(LPCTSTR lpszCurve, LPCTSTR lpszDot, 
								  double fMinVal, double fMaxVal, double fNorVal)
{
	for (int i = 0; i < 4; i++)
	{
		CULCalCurve* pCur = GetCalCurve(i, lpszCurve);
		if (pCur == NULL)
			continue ;
		
		for (int j = 0; j < pCur->m_CalCSs.GetSize(); j++)
		{
			CULCalCurveSeg* pCS = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(j);
			CULCalDot* pDot = pCS->GetCalDot(lpszDot);
			if (pDot)
			{
				pDot->m_fMinStand = fMinVal;
				pDot->m_fMaxStand = fMaxVal;
				pDot->m_fFineStand = fNorVal;
				//pDot->Exceed();
			}
		}
	}
	
	return UL_NO_ERROR;
}


ULMIMP CULTool::SetCalDotStandardRange(UINT nPhase , LPCTSTR lpszCurve, LPCTSTR lpszDot , double fMinVal, double fMaxVal, double fNorVal)
{
	CULCalCurve* pCur = GetCalCurve(nPhase, lpszCurve);
	if (pCur == NULL)
		return UL_ERROR;
		
	for (int j = 0; j < pCur->m_CalCSs.GetSize(); j++)
	{
		CULCalCurveSeg* pCS = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(j);
		CULCalDot* pDot = pCS->GetCalDot(lpszDot);
		if (pDot)
		{
			pDot->m_fMinStand = fMinVal;
			pDot->m_fMaxStand = fMaxVal;
			pDot->m_fFineStand = fNorVal;
			//pDot->Exceed();
		}
	}
	
	return UL_NO_ERROR;
}

void CULTool::SetUserDotVar()
{
/////////////加载工程值/////////////////////////////////////////////
	POSITION	pos = m_mapEngInit.GetStartPosition();
	DWORD		dKey;
	double		va;
	for (; pos != NULL;)
	{
		m_mapEngInit.GetNextAssoc(pos, dKey, va);
		int nType = dKey >> 24;
		int nCurve = /*(dKey - (nType <<24)) >> 16*/(dKey & 0x00ff0000)>>16;
		int nSeg = /*(dKey - (nType<<24) - (nCurve<<16))>>8*/(dKey & 0x0000ff00)>>8;
		int nDot = dKey & 0x000000ff;

		int nCalCurveNum = GetCalCurveNum();
		if (nCurve >= nCalCurveNum)
		    return;
		CULCalCurve*pCurve = (CULCalCurve*)m_CalCurves[nType].GetAt(nCurve);
		VERIFY(pCurve);
		int nCalSeg = pCurve->m_CalCSs.GetSize();
		if (nSeg >= nCalSeg)
		    return;
		CULCalCurveSeg*pSeg = (CULCalCurveSeg*) pCurve->m_CalCSs.GetAt(nSeg);
		VERIFY(pSeg);
		int nCalDot = pSeg->m_CalDotList.GetSize();
		if (nDot >= nCalDot)
		    return;
		CULCalDot*pCalDot = (CULCalDot*) pSeg->m_CalDotList.GetAt(nDot);
		VERIFY(pCalDot);
		pCalDot->m_fEng = va;
	}
/////////////加载标称值/////////////////////////////////////////////
	pos = m_mapStdInit.GetStartPosition();
	for (; pos != NULL;)
	{
		m_mapStdInit.GetNextAssoc(pos, dKey, va);
		int nType = dKey >> 24;
		int nCurve = /*(dKey - (nType <<24)) >> 16*/(dKey & 0x00ff0000)>>16;
		int nSeg = /*(dKey - (nType<<24) - (nCurve<<16))>>8*/(dKey & 0x0000ff00)>>8;
		int nDot = dKey & 0x000000ff;

		int nCalCurveNum = GetCalCurveNum();
		if (nCurve >= nCalCurveNum)
		    return;
		CULCalCurve*pCurve = (CULCalCurve*)
		                     m_CalCurves[nType].GetAt(nCurve);
		VERIFY(pCurve);
		int nCalSeg = pCurve->m_CalCSs.GetSize();
		if (nSeg >= nCalSeg)
		    return;
		CULCalCurveSeg*pSeg = (CULCalCurveSeg*) pCurve->m_CalCSs.GetAt(nSeg);
		VERIFY(pSeg);
		int nCalDot = pSeg->m_CalDotList.GetSize();
		if (nDot >= nCalDot)
		    return;
		CULCalDot*pCalDot = (CULCalDot*) pSeg->m_CalDotList.GetAt(nDot);
		VERIFY(pCalDot);
		pCalDot->m_fFineStand = va;
	}
}

ULMIMP CULTool::AddCalDotValue(DWORD dwDot, double dDotValue)
{
    CULCalDot* pDot = GetCalDot(m_uCalPhase, dwDot);
	if (pDot)
		pDot->m_Buffer.push_back(dDotValue);
    return UL_NO_ERROR;
}

ULMIMP CULTool::ComputeCalCoefficient()
{
	if (m_pITool != NULL)
		m_pITool->ComputeCalCoefficient();

	return UL_NO_ERROR;
}

///////////////////////////////////////////////////////////////////////
// 得到当前刻度类型待刻曲线数量
///////////////////////////////////////////////////////////////////////
ULMINTIMP CULTool::GetCalSegNum(int nCurve)
{
    int nCurveNum = GetCalCurveNum();
    if (nCurve >= nCurveNum)
        return 0;

    CULCalCurve* pCur = (CULCalCurve*)m_CalCurves[m_uCalPhase].GetAt(nCurve);
    return pCur->m_CalCSs.GetSize();
}

ULMINTIMP CULTool::GetCalDotNum(int nCurve, int nSeg)
{
    int nCalCurveNum = GetCalCurveNum();
    if (nCurve >= nCalCurveNum)
        return 0;
    CULCalCurve*pCurve = (CULCalCurve*)
                         m_CalCurves[m_uCalPhase].GetAt(nCurve);
    VERIFY(pCurve);
    int nCalSeg = pCurve->m_CalCSs.GetSize();
    if (nSeg >= nCalSeg)
        return 0;
    CULCalCurveSeg*pSeg = (CULCalCurveSeg*) pCurve->m_CalCSs.GetAt(nSeg);
    VERIFY(pSeg);
    return pSeg->m_CalDotList.GetSize();
}   

int CULTool::GetCoefNum(CULCalCurveSeg* pSeg)
{
    VERIFY(pSeg);
    return pSeg->m_CalCoefs.GetSize();
}

///////////////////////////////////////////////////////////////////////
// 刻度接口调用函数
///////////////////////////////////////////////////////////////////////
ULMIMP CULTool::SetCalDelayTime(int nDelay)
{
	m_nCalDelayTime = nDelay;
	return UL_NO_ERROR;
}

ULMINTIMP CULTool::GetCalDelayTime()
{
	return m_nCalDelayTime;
}

ULMIMP CULTool::AddCalParam(void* pCurve, void* pSeg, void* pDot)
{
	if (m_dwLoad & DL_CALIBRATE)
		return UL_ERROR;

    for (int n = 0; n < 4; n++)
    {
        SetCalPhase(UL_CAL_MASTER + n);			// 设置刻度类型
        AddCalCurve((ULCalCurveData*) pCurve);	// 追加刻度曲线 
        AddCalCurveSeg((ULCalSegData*) pSeg);	// 追加曲线段  
        AddCalDot((ULCalDotData*) pDot);		// 追加刻度点
    }

    m_dwCalibrate = CP_MASTER|CP_PRIMARY|CP_BEFROR|CP_AFTER;
	m_dwCalReports = CP_MASTER|CP_PRIMARY|CP_BEFROR|CP_AFTER;

    return UL_NO_ERROR;
}

ULMIMP CULTool::AddCalParam(UINT uPhase, void* pCurveData, void* pSegData, void* pDotData)
{
	if (m_dwLoad & DL_CALIBRATE)
		return UL_ERROR;

	if (uPhase > 3)
		return UL_ERROR;

	SetCalPhase(uPhase);
	AddCalCurve((ULCalCurveData*) pCurveData);	// 追加刻度曲线 
	AddCalCurveSeg((ULCalSegData*) pSegData);	// 追加曲线段  
    AddCalDot((ULCalDotData*) pDotData);		// 追加刻度点

	m_dwCalibrate |= dwCalPhase[uPhase];
	m_dwCalReports |= dwCalPhase[uPhase];

	return UL_NO_ERROR;
}

ULMIMP CULTool::GetCalValue(int nCurve, int nSeg, int nDot, double& dMeasure, double& dEngineer)
{
    CULCalDot* pCalDot = GetCalDot(m_uCalPhase, nCurve, nSeg, nDot);
    if (pCalDot)
	{
		dMeasure = pCalDot->m_fMeas;
		dEngineer = pCalDot->m_fEng;
	    return UL_NO_ERROR;
	}

	return UL_ERROR;
}

ULMIMP CULTool::SetCalDotEng(int nCurve, int nSeg, int nDot, double dEng)
{
	CULCalDot* pCalDot = GetCalDot(m_uCalPhase, nCurve, nSeg, nDot);
	if (pCalDot)
	{
		pCalDot->m_fEng = dEng;
		return UL_NO_ERROR;
	}

	return UL_ERROR;
}

ULMIMP CULTool::SetCalDotEng(int nPhase, int nCurve, int nSeg, int nDot, double dEng)
{
    CULCalDot* pCalDot = GetCalDot(nPhase, nCurve, nSeg, nDot);
	if (pCalDot)
	{
		pCalDot->m_fEng = dEng;
		return UL_NO_ERROR;
	}

	return UL_ERROR;
}

ULMIMP CULTool::SetCalDotMea(int nCurve, int nSeg, int nDot, double dMea)
{
	CULCalDot* pCalDot = GetCalDot(m_uCalPhase, nCurve, nSeg, nDot);
	if (pCalDot)
	{
		pCalDot->m_fMeas = dMea;
		return UL_NO_ERROR;
	}
	
	return UL_ERROR;
}

ULMIMP CULTool::SetCalDotMea(int nPhase, int nCurve, int nSeg, int nDot, double dMea)
{
    CULCalDot* pCalDot = GetCalDot(nPhase, nCurve, nSeg, nDot);
	if (pCalDot)
	{
		pCalDot->m_fMeas = dMea;
		return UL_NO_ERROR;
	}
	
	return UL_ERROR;
}

ULMIMP	CULTool::SetCalDotInitEng(int nCurve, int nSeg, int nDot,double dEngineer)
{
	WORD low = MAKEWORD(nDot,nSeg);
	WORD high = MAKEWORD(nCurve,m_uCalPhase);
	DWORD index = ((WORD)low | ((WORD)high<<16));

	m_mapEngInit[index] = dEngineer;
	return UL_NO_ERROR;
}

ULMIMP CULTool::SetCalDotInitEng(int nCalType,int nCurve, int nSeg, int nDot,double dEngineer)
{
	WORD low = MAKEWORD(nDot,nSeg);
	WORD high = MAKEWORD(nCurve,nCalType);
	DWORD index = ((WORD)low | ((WORD)high<<16));

	m_mapEngInit[index] = dEngineer;
	return UL_NO_ERROR;
}

ULMIMP	CULTool::SetCalDotStd(int nCurve, int nSeg, int nDot, double dStand)
{
    CULCalDot* pCalDot = GetCalDot(m_uCalPhase, nCurve, nSeg, nDot);
	if (pCalDot)
	{
    	pCalDot->m_fFineStand = dStand;
		return UL_NO_ERROR;
	}

	return UL_ERROR;
}

ULMIMP CULTool::SetCalDotStd(int nCalPhase, int nCurve, int nSeg, int nDot, double dStand)
{
    CULCalDot* pCalDot = GetCalDot(nCalPhase, nCurve, nSeg, nDot);
	if (pCalDot)
    {
		pCalDot->m_fFineStand = dStand;
		return UL_NO_ERROR;
	}

	return UL_ERROR;
}

ULMIMP	CULTool::SetCalDotInitStd(int nCurve, int nSeg, int nDot, double dStand)
{
	WORD low = MAKEWORD(nDot,nSeg);
	WORD high = MAKEWORD(nCurve,m_uCalPhase);
	DWORD index = ((WORD)low | ((WORD)high<<16));

	m_mapStdInit[index] = dStand;
	return UL_NO_ERROR;
}

ULMIMP CULTool::SetCalDotInitStd(int nCalType, int nCurve, int nSeg, int nDot, double dStand)
{
	WORD low = MAKEWORD(nDot,nSeg);
	WORD high = MAKEWORD(nCurve,nCalType);
	DWORD index = ((WORD)low | ((WORD)high<<16));

	m_mapStdInit[index] = dStand;
	return UL_NO_ERROR;
}

ULMIMP CULTool::GetCalDotMaxValue(int nCurve, int nSeg, int nDot, double& fMax)
{
	CULCalDot* pCalDot = GetCalDot(m_uCalPhase, nCurve, nSeg, nDot);
	if (pCalDot && pCalDot->m_Buffer.size())
	{
		fMax = pCalDot->m_Buffer.at(0);
		for (int i = 1; i < pCalDot->m_Buffer.size(); i++)
		{
			double fTemp = pCalDot->m_Buffer.at(i);
			if( fTemp > fMax )
				fMax = fTemp;
		}	
		pCalDot->m_fMaxMeas = fMax;
		return UL_NO_ERROR;
	}

	return UL_ERROR;
}

ULMIMP CULTool::GetCalDotMinValue(int nCurve, int nSeg, int nDot,double& fMin)
{
	CULCalDot* pCalDot = GetCalDot(m_uCalPhase, nCurve, nSeg, nDot);
	if (pCalDot && pCalDot->m_Buffer.size())
	{
		fMin = pCalDot->m_Buffer.at(0);
		for (int i = 1; i < pCalDot->m_Buffer.size();i++)
		{
			double fTemp = pCalDot->m_Buffer.at(i);
			if( fTemp < fMin )
				fMin = fTemp;
		}
		pCalDot->m_fMinMeas = fMin;
		return UL_NO_ERROR;
	}
	return UL_ERROR;
}

ULMIMP CULTool::GetCalDotLastValue(int nCurve, int nSeg, int nDot,double& fLast)
{
	int nCalCurveNum = GetCalCurveNum();
    if (nCurve >= nCalCurveNum)
        return UL_ERROR;
    CULCalCurve*pCurve = (CULCalCurve*)
                         m_CalCurves[m_uCalPhase].GetAt(nCurve);
    VERIFY(pCurve);
    int nCalSeg = pCurve->m_CalCSs.GetSize();
    if (nSeg >= nCalSeg)
        return UL_ERROR;
    CULCalCurveSeg*pSeg = (CULCalCurveSeg*) pCurve->m_CalCSs.GetAt(nSeg);
    VERIFY(pSeg);
    int nCalDot = pSeg->m_CalDotList.GetSize();
    if (nDot >= nCalDot)
        return UL_ERROR;
    CULCalDot*pCalDot = (CULCalDot*) pSeg->m_CalDotList.GetAt(nDot);
    VERIFY(pCalDot);

	if(pCalDot->m_Buffer.size() <= 0)
		return UL_ERROR;
	fLast = pCalDot->m_Buffer.at(pCalDot->m_Buffer.size()-1);
	return UL_NO_ERROR;
}

ULMDBLIMP CULTool::GetCalDotValue(int nCur, int nSeg, int nDot, double dErr /* = DBL_MAX */, int nValue /* = VM_DEF */)
{
	if(nCur < 0 || nSeg < 0 || nDot < 0)
		return dErr;
	
	if(nValue == 0)
		nValue = m_nCalVM;
	
	int nCurs = m_CalCurves[m_uCalPhase].GetSize();
	if(nCur < nCurs)
	{
		CULCalCurve* pCurve = (CULCalCurve*)m_CalCurves[m_uCalPhase].GetAt(nCur);
		int nSegs = pCurve->m_CalCSs.GetSize();
		if (nSeg < nSegs)
		{
			CULCalCurveSeg* pSeg = (CULCalCurveSeg*)pCurve->m_CalCSs.GetAt(nSeg);
			int nDots = pSeg->m_CalDotList.GetSize();
			if (nDot < nDots)
			{
				CULCalDot* pDot = (CULCalDot*)pSeg->m_CalDotList.GetAt(nDot);
				int nVals = pDot->m_Buffer.size();
				if (nVals)
				{
					switch(nValue)
					{
					case VM_MIN:
						{
							double dRes = pDot->m_Buffer.at(0);
							for (int i=1; i<nVals; i++)
							{
								if(pDot->m_Buffer.at(i) < dRes)
									dRes = pDot->m_Buffer.at(i);
							}
							return dRes;
						}
					case VM_AVE:
						{
							double dRes = 0;
							for (int i=0; i<nVals; i++)
								dRes += pDot->m_Buffer.at(i);
							return (dRes/nVals);
						}
					case VM_MAX:
						{
							double dRes = pDot->m_Buffer.at(0);
							for (int i=1; i<nVals; i++)
							{
								if(dRes < pDot->m_Buffer.at(i))
									dRes = pDot->m_Buffer.at(i);
							}
							return dRes;
						}
					case VM_SUM:
						{
							double dRes = 0;
							for (int i=0; i<nVals; i++)
							{
								dRes += pDot->m_Buffer.at(i);
							}
							return dRes;
						}
					case VM_END:
					default:
						return pDot->m_Buffer.at(nVals-1);
					}
				}
			}
		}
	}
	return dErr;
}

ULMIMP CULTool::SetCalDotCoefRange(int nPhase, int nCur, int nSeg, LPCTSTR lpszCoef, double fMinValue, double fMaxValue)
{
	CULCalCurveSeg* pCS = GetCalCS(nPhase, nCur, nSeg);
	if (pCS != NULL)
	{
		ULCalCoef cc;
		int nCoef = pCS->GetCoef(lpszCoef, cc);
		if (nCoef >= 0)//lxc
		{
			cc.bSetRange = TRUE;
			cc.dRMin = min(fMinValue, fMaxValue);
			cc.dRMax = max(fMinValue, fMaxValue);
			cc.dRNor = (fMinValue + fMaxValue)/2;
			pCS->m_CalCoefs.SetAt(nCoef, cc);
			return UL_NO_ERROR;
		}
	}
	
	return UL_ERROR;
}

ULMDBLIMP CULTool::GetCalCoef(int nCur, int nSeg, int nCoefId)
{
	CULCalCurveSeg* pCS = GetCalCS(m_uCalPhase, nCur, nSeg);
	if (pCS)
	{
		int nCoefCount = pCS->m_CalCoefs.GetSize();
		if (nCoefId > -1 && nCoefId < nCoefCount)
		{
			ULCalCoef Coef = pCS->m_CalCoefs.GetAt(nCoefId);
			return Coef.dValue;
		}		
	}

	return 1;
}

ULMDBLIMP CULTool::GetCalCoef(int nPhase, int nCur, int nSeg, int nCoefId)
{
	/*
	 *	to get the specified coeffiecient of the segment with the specified calibration type
	 *  nPhase:0, Master; 1,Before; 2,After; 
	 */	
	CULCalCurveSeg* pCS = GetCalCS(nPhase, nCur, nSeg);
	if (pCS)
	{
		int nCoefCount = pCS->m_CalCoefs.GetSize();
		if (nCoefId < nCoefCount)
		{
			ULCalCoef Coef = pCS->m_CalCoefs.GetAt(nCoefId);
			return Coef.dValue;
		}
	}

	return 1;
}

ULMIMP CULTool::AddCoef(int nCurve, int nSeg, CString strName, double fCoef)
{
    CULCalCurveSeg* pCS = GetCalCS(m_uCalPhase, nCurve, nSeg);
    if (pCS != NULL)
	{
		ULCalCoef cc;
		if (pCS->GetCoef(strName, cc) > -1)
			return UL_ERROR;

		cc.dValue = fCoef;
		cc.szName = strName;
		pCS->m_CalCoefs.Add(cc);
		return UL_NO_ERROR;
	}

	return UL_ERROR;
}

ULMIMP CULTool::AddDefaultCoef(int nPhase, int nCurve, int nSeg, LPCTSTR lpszName, double fCoef)
{
    CULCalCurveSeg* pCS = GetCalCS(nPhase, nCurve, nSeg);
	if (pCS != NULL)
	{
		ULCalCoef cc;
		if (pCS->GetCoef(lpszName, cc) > -1)
			return UL_ERROR;

		cc.dValue = fCoef;
		cc.szName = lpszName;
		pCS->m_CalCoefs.Add(cc);
		return UL_NO_ERROR;
	}

    return UL_ERROR;
}

ULMIMP CULTool::AddDefCoef(UINT nPhase, UINT nCurve, UINT nSeg, LPCTSTR lpszName, double fCoef, double fRMin, double fRMax, double fRNor)
{
	CULCalCurveSeg* pCS = GetCalCS(nPhase, nCurve, nSeg);
	if (pCS != NULL)
	{
		ULCalCoef cc;
		if (pCS->GetCoef(lpszName, cc) > -1)
			return UL_ERROR;
		
		cc.dValue = fCoef;
		cc.szName = lpszName;
		cc.szAlias = lpszName;
		cc.bSetRange = TRUE;
//		if ((fRMin <= fRNor) && (fRNor <= fRMax))
//		{
			cc.dRMin = fRMin;
			cc.dRMax = fRMax;
			cc.dRNor = fRNor;
//		}
//		else
//		{
//			double dR[3] = {fRMin, fRNor, fRMax};
//			QuickSort(dR, 3);
//			cc.dRMin = dR[0];
//			cc.dRNor = dR[1];
//			cc.dRMax = dR[2];
//		}

		pCS->m_CalCoefs.Add(cc);
		return UL_NO_ERROR;
	}

	return UL_ERROR;
}

ULMIMP CULTool::AddDefCoef(UINT nPhase, UINT nCurve, UINT nSeg, LPCTSTR lpszName, LPCTSTR lpszAlias, double fCoef, double fRMin, double fRMax, double fRNor)
{
	CULCalCurveSeg* pCS = GetCalCS(nPhase, nCurve, nSeg);
	if (pCS != NULL)
	{
		ULCalCoef cc;
		if (pCS->GetCoef(lpszName, cc) > -1)
			return UL_ERROR;
		
		cc.dValue = fCoef;
		cc.szName = lpszName;
		cc.szAlias = lpszAlias;
		cc.bSetRange = TRUE;
//		if ((fRMin <= fRNor) && (fRNor <= fRMax))
//		{
			cc.dRMin = fRMin;
			cc.dRMax = fRMax;
			cc.dRNor = fRNor;
//		}
//		else
//		{
//			double dR[3] = {fRMin, fRNor, fRMax};
//			QuickSort(dR, 3);
//			cc.dRMin = dR[0];
//			cc.dRNor = dR[1];
//			cc.dRMax = dR[2];
//		}

		pCS->m_CalCoefs.Add(cc);
		return UL_NO_ERROR;
	}

	return UL_ERROR;
}


ULMIMP CULTool::RemoveCoef(int nCurve, int nSeg)
{
    int nCalCurveNum = GetCalCurveNum();
    if (nCurve >= nCalCurveNum)
        return UL_ERROR;
    CULCalCurve*pCurve = (CULCalCurve*)
                         m_CalCurves[m_uCalPhase].GetAt(nCurve);
    VERIFY(pCurve);
    int nCalSeg = pCurve->m_CalCSs.GetSize();
    if (nSeg >= nCalSeg)
        return UL_ERROR;
    CULCalCurveSeg*pSeg = (CULCalCurveSeg*) pCurve->m_CalCSs.GetAt(nSeg);
    VERIFY(pSeg);
    pSeg->m_CalCoefs.RemoveAll();
    return UL_NO_ERROR;
}

ULMINTIMP CULTool::AddCoefficient(double fcoefficient, int nCurve)
{
    // 向标注为第nCurIndex条刻度曲线的，存放该曲线的刻度系数的数据加入数据fcoefficient
    // 返回值为该刻度系数在数组中的索引号
    return (m_pArrCalCoefs + nCurve)->Add(fcoefficient);
}

ULMINTIMP CULTool::AddUserDefualtCoef(double fCoef, int nCurIndex)
{
    // 向标注为第nCurIndex条刻度曲线的，存放该曲线的刻度系数的数据加入数据fcoefficient
    // 返回值为该刻度系数在数组中的索引号
    return (m_pArrCalCoefs + nCurIndex)->Add(fCoef);
}

ULMINTIMP CULTool::AddUserDefualtCoef(double fCoef, int nNum, int nCurIndex)
{
    ASSERT(nNum > 0);
    for (int i = 0; i < nNum; i++)
        (m_pArrCalCoefs + nCurIndex)->Add(fCoef); 
    return nNum;
}

/* --------------------------------- *
 *	修改刻度系数
 *	nIndexCur : 刻度曲线索引号
 *	nIndexSeg : 刻度点索引号
 *	fCoef  	  : 刻度系数值
 * --------------------------------- */

ULMIMP CULTool::SetCoef(int iCur, int iSeg, int iCoef, LPCTSTR lpszName, double fCoef)
{
	CULCalCurveSeg* pCS = GetCalCS(m_uCalPhase, iCur, iSeg);
	if (pCS)
	{
		if (iCoef < 0)
			return UL_ERROR;

		if (iCoef < pCS->m_CalCoefs.GetSize())
		{
			ULCalCoef cc = pCS->m_CalCoefs.GetAt(iCoef);
			cc.szName = lpszName;
			cc.dValue = fCoef;
			pCS->m_CalCoefs.SetAt(iCoef, cc);
			return UL_NO_ERROR;
		}
	}

	return UL_ERROR;
}

ULMIMP CULTool::SetCoef(int nPhase, int iCur, int iSeg, int iCoef, LPCTSTR lpszName, double fCoef)
{
	CULCalCurveSeg* pCS = GetCalCS(nPhase, iCur, iSeg);
	if (pCS)
	{
		if (iCoef < 0)
			return UL_ERROR;
		
		if (iCoef < pCS->m_CalCoefs.GetSize())
		{
			ULCalCoef Coef = pCS->m_CalCoefs.GetAt(iCoef);
			Coef.dValue = fCoef;
			Coef.szName = lpszName;
			pCS->m_CalCoefs.SetAt(iCoef, Coef);
			return UL_NO_ERROR;
		}
	}	
	
	return UL_ERROR;
}

ULMIMP CULTool::SetCoefficient(double fCoef, int nIndex, int nCurve)
{
    (m_pArrCalCoefs + nCurve)->SetAt(nIndex, fCoef);
	return UL_NO_ERROR;
}

// nIndex: 所在曲线的刻度系数的索引号
// nCurve: 刻度曲线的索引号
ULMDBLIMP CULTool::GetCoefficient(int nIndex, int nCurve)
{
    if (nIndex < 0 || nIndex >= (m_pArrCalCoefs + nCurve)->GetSize())
    {
        TRACE("所索引的刻度系数不存在!可能索引号不正确或刻度文件没有加载!");
        return 1.0;
    }
    return (m_pArrCalCoefs + nCurve)->GetAt(nIndex);
}

ULMINTIMP CULTool::GetCoefNum(int nCurve)
{	
	if (m_pArrCalCoefs==NULL)
		return -1;
	
	return (m_pArrCalCoefs+nCurve)->GetSize();
}

ULMIMP CULTool::SetCalUserCurveNum(int nNumb)
{
    // 将以前的刻度系数数组清空
    ClearCoefficient();

    // 设置刻度曲线的数目，并生成刻度系数数组
	if (nNumb > 0)
	{
		m_nUserCalCurves = nNumb;
		m_pArrCalCoefs = new CDblArray[nNumb];
	}

	return UL_NO_ERROR;
}

ULMIMP CULTool::EmptyCoefficient()
{
    // 将刻度系数数组中的的所有刻度系数清空
    int NumCalCurves = GetCalUserCurvesNum();
    // 将以前的刻度系数数组清空
    if (m_pArrCalCoefs != NULL)
    {
        for (int i = 0; i < NumCalCurves; i++)
            EmptyCoefficient(i);
    }

	return UL_NO_ERROR;
}

// nCurve:  The indicator of the calibration curve
ULMIMP CULTool::EmptyCoefficient(int nCurve)
{    
    // 清空某一个数组中的刻度系数，相当于重新对某一个曲线进行刻度处理
    if (nCurve < 0)
        return UL_ERROR;

	if (nCurve < m_nUserCalCurves)
	{
		(m_pArrCalCoefs + nCurve)->RemoveAll();
		return UL_NO_ERROR;
	}

	return UL_ERROR;
}

void CULTool::ClearCoefficient()
{
    if (m_pArrCalCoefs != NULL)
    {
		delete[] m_pArrCalCoefs;
		m_pArrCalCoefs = NULL;
    }

	m_nUserCalCurves = 0;
}

///////////////////////////////////////////////////////////////////////
// Tool Calibrating Operations
///////////////////////////////////////////////////////////////////////
void CULTool::InitCalMsg(CWnd* pCalWnd, UINT uCalMsgeID)
{
    m_pCalWnd = pCalWnd;
    m_uCalMsgID = uCalMsgeID;
}

ULMIMP CULTool::BeforeCalibrate(LPCTSTR lpszItem)
{
	for (int i = 0; i < m_CalDotList.GetSize(); i++)
	{
		CULCalDot* pDot = (CULCalDot*)m_CalDotList.GetAt(i);
		pDot->m_Buffer.clear();
		pDot->m_bEditMeasure = FALSE;	// 初始化为未编辑状态
	}

    SetToolState(TS_CAL);

    // Remove all data from the curvelist
    ClearCurvesData();
	
    if (m_pITool)
        m_pITool->BeforeCalibrate(lpszItem);
    if (m_nWorkMode != WORKMODE_COMPUTEPLAYBACK)
				StartChannel();

    return UL_NO_ERROR;
} 

ULMIMP CULTool::CalibrateIO()
{
	if (m_pITool)
		m_pITool->CalibrateIO();
	
	return UL_NO_ERROR;
}


ULMIMP CULTool::AfterCalibrate()
{
	short sRes = UL_NO_ERROR;
    if (m_pITool)
        sRes = m_pITool->AfterCalibrate();

    SetToolState(TS_IDLE);

    // Remove all data from the curvelist
    ClearCurvesData();
    return sRes;
}

///////////////////////////////////////////////////////////////////////
// Calibrate File Load/Save Operations
///////////////////////////////////////////////////////////////////////
ULMIMP CULTool::SaveCalFile(CFile* pFile)
{
    CTime tm = CTime::GetCurrentTime();
	CString strTime = tm.Format("%Y%m%d%H%M");

    // Get the calibration time
    ULCALFILEHEAD cfh;
	ZeroMemory(&cfh, sizeof(ULCALFILEHEAD));
    _tcscpy(cfh.szFile, strCalPhase[m_uCalPhase] + ".udc");
    _tcscpy(cfh.szTool, strToolName);
    _tcscpy(cfh.szToolSN, strSN);
    _tcscpy(cfh.szTime, strTime);
    _tcscpy(cfh.szPhase, strCalPhase[m_uCalPhase]);
	cfh.lTime = m_tmCaled[m_uCalPhase].GetTime();
    pFile->Write(&cfh, sizeof(ULCALFILEHEAD));    

    int nCalCurveNum = GetCalUserCurvesNum();
    pFile->Write(&nCalCurveNum, sizeof(int));

    // 将每一条刻度曲线上的刻度系数加入到文件中去
    for (int i = 0; i < nCalCurveNum; i++)
    {
        int nCoefs = (m_pArrCalCoefs + i)->GetSize();
        pFile->Write(&nCoefs, sizeof(int));

        for (int j = 0; j < nCoefs; j++)
        {
            double fCoef = (m_pArrCalCoefs + i)->GetAt(j);
            pFile->Write(&fCoef, sizeof(double));
        }
    }

    return UL_NO_ERROR;
}


ULMIMP CULTool::ReadCalFile(CFile* pFile)
{
    ULCALFILEHEAD cfh;
    pFile->SeekToBegin();

    pFile->Read(&cfh, sizeof(ULCALFILEHEAD));

    // for determing whether the tool is coordination with the cal data file 
    if (_tcscmp(cfh.szTool, strToolName))
        return UL_ERROR;
    
	if (_tcscmp(cfh.szPhase, "Master") == 0)
		m_uCalPhase = 0;
	else if (_tcscmp(cfh.szPhase, "Before") == 0)
		m_uCalPhase = 1;
	else if (_tcscmp(cfh.szPhase, "After") == 0)
		m_uCalPhase = 2;
	else if (_tcscmp(cfh.szPhase, "Primary") == 0)
		m_uCalPhase = 3;
	else
		return UL_ERROR;

	m_tmCaled[m_uCalPhase] = CTime((time_t)cfh.lTime);
//	if (bTime)
//	{
//		m_bCaled[m_uCalPhase] = (GetCalTimeColor(m_uCalPhase) == RGB(255,255,255)) ? FALSE : TRUE;
// 		if (!m_bCaled[m_uCalPhase])
// 			return UL_ERROR;
//	}
// 	else
 		m_bCaled[m_uCalPhase] = TRUE;


    pFile->Read(&m_nUserCalCurves, sizeof(int));		// 刻度曲线的数目
	SetCalUserCurveNum(m_nUserCalCurves);

    double fCoef;

    // 将每一条刻度曲线上的刻度系数读入到文件中去
    for (int i = 0; i < m_nUserCalCurves; i++)
    {
        int nCoefs = 0;
        pFile->Read(&nCoefs, sizeof(int));     // 读取刻度系数的数目
        for (int j = 0; j < nCoefs; j++)
        {
            pFile->Read(&fCoef, sizeof(double));    // 读取刻度系数
            (m_pArrCalCoefs + i)->Add(fCoef);
        }
    }

	return UL_NO_ERROR;
}

void CULTool::ReloadCalFile()
{
	if(m_bCaled[m_uCalPhase])
		ReadUserCalFile(m_CalFilePathArray[m_uCalPhase],strCalPhase[m_uCalPhase]);
}

int CULTool::GetMaxNoF(LPCTSTR pszFind, TCHAR szL /* = _T('-') */)
{
	int nMax = 0;
	WIN32_FIND_DATA fd;
	HANDLE hFind = ::FindFirstFile(pszFind, &fd);
	
	CString strFind;
	BOOL bFindNext = (hFind != INVALID_HANDLE_VALUE);
	while (bFindNext)
	{
		strFind = fd.cFileName;
		int nPos = strFind.ReverseFind(_T('\\'));
		if (nPos != -1)
			strFind = strFind.Right(strFind.GetLength() - nPos);
		
		nPos = strFind.ReverseFind(_T('.'));
		if (nPos != -1)
			strFind = strFind.Left(nPos);
		
		nPos = strFind.ReverseFind(szL);
		if (nPos == -1)
		{
			bFindNext = ::FindNextFile(hFind, &fd);
			continue ;
		}
		
		strFind = strFind.Right(strFind.GetLength() - nPos - 1);
		int nLen = strFind.GetLength();
		if (nLen < 1)
		{
			bFindNext = ::FindNextFile(hFind, &fd);
			continue ;
		}
		
		BOOL bZero = FALSE;
		if (_ttoi(strFind) == 0)
			bZero = TRUE;
		
		if (bZero && strFind[0] != _T('0'))
		{
			bFindNext = ::FindNextFile(hFind, &fd);
			continue ;
		}
		
		for (int i = 0; i < nLen; i++)
		{
			if (strFind[i] < _T('0') || strFind[i] > _T('9'))
			{
				if (i < (nLen - 1))
					break;
			}
		}
		
		if (i < nLen)
		{
			bFindNext = ::FindNextFile(hFind, &fd);
			continue ;
		}
		
		int n = _ttoi(strFind);
		if (nMax < n)
			nMax = n;
		
		bFindNext = ::FindNextFile(hFind, &fd);
	}
	
	::FindClose(hFind);
	return nMax;
}

void CULTool::ReadCalFileEx(CFile* pFile, UINT uPhase)
{
	ULCALFILEHEAD cfh;	
	CString strCalPhase;
	strCalPhase = _T("~");
	pFile->Read(&cfh, sizeof(ULCALFILEHEAD));
	
// 	if (_tcscmp(cfh.szTool, strToolName))
// 		return ;
	
	if (_tcscmp(cfh.szPhase, "Master") == 0)
	{
		strCalPhase = strCalPhase + "Master";
		m_uCalPhase = 0;
	}
	else if (_tcscmp(cfh.szPhase, "Before") == 0)
	{
		strCalPhase = strCalPhase + "Before";
		m_uCalPhase = 1;
	}
	else if (_tcscmp(cfh.szPhase, "After") == 0)
	{
		strCalPhase = strCalPhase + "After";
		m_uCalPhase = 2;
	}
	else if (_tcscmp(cfh.szPhase, "Primary") == 0)
	{
		strCalPhase = strCalPhase + "Primary";
		m_uCalPhase = 3;
	}
 //	else	//Add by zy 2012 4 20 当原始数据文件中刻度文件与仪器库不匹配时，会产生崩溃
 //		return ;
	
	m_tmCaled[m_uCalPhase] = CTime((time_t)cfh.lTime);	
	
	pFile->Read(&m_nUserCalCurves, sizeof(int));
	SetCalUserCurveNum(m_nUserCalCurves);
	
	double fCoef;
	for (int i = 0; i < m_nUserCalCurves; i++)
	{
		int nCoefs = 0;
		pFile->Read(&nCoefs, sizeof(int));     // 读取刻度系数的数目
		for (int j = 0; j < nCoefs; j++)
		{
			pFile->Read(&fCoef, sizeof(double));    // 读取刻度系数
			(m_pArrCalCoefs + i)->Add(fCoef);
		}
	}

	// 读取自定义报告
	pFile->Read(&m_bCaled[m_uCalPhase], sizeof(BOOL));
	pFile->Read(&m_szMetaReport[m_uCalPhase], sizeof(short));
	
	CFile file;
	DWORD dwMetaLen = 0;
	pFile->Read(&dwMetaLen, sizeof(DWORD));
	if (dwMetaLen > 0)
	{
		BYTE* pBuf = new BYTE[dwMetaLen];
		if (TRUE) // 为后面生成的wmf文件生成不同的名字
		{
			CString strFind = Gbl_AppPath + "\\Temp\\" + strCalPhase + _T("*.*");
			int nMax = GetMaxNoF(strFind);
			
			CString str;
			str.Format(_T("%d"), ++nMax);
			strCalPhase += _T("-") + str;	
		}
		
		CString strTemp = Gbl_AppPath + "\\Temp\\" + strCalPhase + ".wmf";
		pFile->Read(pBuf, dwMetaLen);
		if (file.Open(strTemp, CFile::modeCreate | CFile::modeWrite))
		{
			file.Write(pBuf, dwMetaLen);
			file.Close();
		}
		
		m_hMetaReport[m_uCalPhase] = GetMetaFile(strTemp);		
		delete[] pBuf;
	}
}

void CULTool::SaveCalFileEx(CFile* pFile, UINT uPhase)
{	
	CTime tm = m_tmCaled[uPhase];// add by bao 保存raw文件时时间不采用最新刻度 //CTime::GetCurrentTime();
	CString strTime = tm.Format("%Y%m%d%H%M");

    ULCALFILEHEAD cfh;
	ZeroMemory(&cfh, sizeof(ULCALFILEHEAD));
    _tcscpy(cfh.szFile, strCalPhase[uPhase] + ".udc");
    _tcscpy(cfh.szTool, strToolName);
    _tcscpy(cfh.szToolSN, strSN);
    _tcscpy(cfh.szTime, strTime);
    _tcscpy(cfh.szPhase, strCalPhase[uPhase]);
	cfh.lTime = m_tmCaled[uPhase].GetTime();
    pFile->Write(&cfh, sizeof(ULCALFILEHEAD));    
	
    int nCalCurveNum = GetCalUserCurvesNum();
    pFile->Write(&nCalCurveNum, sizeof(int));
	
    for (int i = 0; i < nCalCurveNum; i++)
    {
        int nCoefs = (m_pArrCalCoefs + i)->GetSize();
        pFile->Write(&nCoefs, sizeof(int));
		
        for (int j = 0; j < nCoefs; j++)
        {
            double fCoef = (m_pArrCalCoefs + i)->GetAt(j);
            pFile->Write(&fCoef, sizeof(double));
        }
    }
	
	// 保存自定义报告
	pFile->Write(&m_bCaled[uPhase], sizeof(BOOL)); 
	pFile->Write(&m_szMetaReport[uPhase], sizeof(short)); 
	
	CFile metafile;
	DWORD dwMetaLen = 0;
	if (m_hMetaReport[uPhase] != NULL)
	{
		CString strTemp = Gbl_AppPath + "\\Temp\\" +  "MetaReport.wmf";
		HMETAFILE hMetafile = CopyMetaFile(m_hMetaReport[uPhase], strTemp);
		DeleteMetaFile(hMetafile);
		if (metafile.Open(strTemp, CFile::modeRead))
		{
			dwMetaLen = metafile.GetLength();
			pFile->Write(&dwMetaLen,sizeof(DWORD));
			if (dwMetaLen>0)
			{
				BYTE* pBuf = new BYTE[dwMetaLen];
				UINT nBytesRead = metafile.Read(pBuf, dwMetaLen);
				//pFile->Write(&nBytesRead, sizeof(UINT));
				pFile->Write(pBuf, dwMetaLen);
				delete[] pBuf;
			}
			metafile.Close();
			DeleteFile(strTemp);
		}
		else
		{
			//TRACE("Load CalMetafile error.\n");
			dwMetaLen = 0;
			pFile->Write(&dwMetaLen,sizeof(DWORD));
		}
	}
	else
		pFile->Write(&dwMetaLen,sizeof(DWORD));
}

/* ------------------------------
 *	写刻度文本文件
 *  包括版本、测量值、标称值、乘因子、加因子
/* ---------------------------------------- */

BOOL CULTool::WriteCalTextFile(CString strFile)
{
    CString str = strFile;
    CFile   file;
    if (file.Open(strFile, CFile::modeCreate | CFile::modeWrite) == FALSE)
        return FALSE;
	CString strLoad;
    switch (m_uCalPhase)
    {
       case 0:
          strLoad.LoadString (IDS_MASTER_CAL_FILE);
		  str += strLoad;
          break;
       case 1:
		  strLoad.LoadString (IDS_BEFORE_CAL_FILE);
          str += strLoad;
          break;
       case 2:
          strLoad.LoadString (IDS_AFTER_CAL_FILE);
		  str += strLoad;
          break;
       default:
          break;
    }

    file.Write(str, str.GetLength());

    DWORD dwLen = m_CalCurves[m_uCalPhase].GetSize();

    for (DWORD i = 0; i < dwLen; i++)
    {
        CULCalCurve*pCalCurve = (CULCalCurve*)
                                m_CalCurves[m_uCalPhase].GetAt(i);

        strLoad.LoadString (IDS_CAL_FILE_VERSION);
		str = strLoad + pCalCurve->m_strVersion + "\r\n\r\n";
        file.Write(str, str.GetLength());

        str = pCalCurve->m_strName +
              "\t" +
              pCalCurve->m_strUnit +
              "\t" +
              pCalCurve->m_strTip +
              "\r\n\r\n";
        file.Write(str, str.GetLength());

        for (int j = 0; j < pCalCurve->m_CalCSs.GetSize(); j++)
        {
            CULCalCurveSeg*pSeg = (CULCalCurveSeg*)
                                  pCalCurve->m_CalCSs.GetAt(j);
            strLoad.LoadString (IDS_CAL_FORMAT9);
			str.Format(strLoad, j + 1,
                       pSeg->m_fValue);
            file.Write(str, str.GetLength());

            str.LoadString (IDS_CAL_FORMAT10);
            file.Write(str, str.GetLength());

            for (int k = 0; k < pSeg->m_CalDotList.GetSize(); k++)
            {
                CULCalDot*pDot = (CULCalDot*) pSeg->m_CalDotList.GetAt(k);
                str.Format("%6.2f\t%6.2f\t%6.2f\r\n", pDot->m_fMeas,
                           pDot->m_fEng, pDot->m_fFineStand);
                file.Write(str, str.GetLength());
            }

            str.LoadString (IDS_CAL_FORMAT11);
            file.Write(str, str.GetLength());

            for (int l = 0; l < pSeg->m_CalCoefs.GetSize(); l++)
            {
                ULCalCoef Gene = pSeg->m_CalCoefs.GetAt(l);
                str.Format("%s\t%s\t%9.5f\r\n", Gene.szName, Gene.szUnit,
                           Gene.dValue);
                file.Write(str, str.GetLength());
            }
        }
    }       
    file.Close();    
    return TRUE;
}

void CULTool::WriteCalUserTextFile(CString strFile)
{
    CString str  = strFile;

    CString strLoad;
	CTime   time = CTime::GetCurrentTime(); 
    CString strTime = time.Format("%Y%m%d%H%M");

    CStdioFile file;
    if (!file.Open(strFile, CFile::modeCreate | CFile::modeWrite))
    {
        return ;
    }
	strLoad.LoadString (IDS_FILE_PATH);
    file.WriteString(strLoad + str + "\r\n"); 

    // 文件头
    str = strCalPhase[m_uCalPhase] + ".udc";  //m_ToolInfo.strEngName+
    strLoad.LoadString (IDS_CAL_FORMAT12);
	file.WriteString(strLoad + str + "\n"); 
    strLoad.LoadString (IDS_CAL_FORMAT13);
	file.WriteString(strLoad + strToolName + "\n"); 
    strLoad.LoadString (IDS_CAL_FORMAT14);
	file.WriteString(strLoad + strSN + "\n");
    strLoad.LoadString (IDS_CAL_TIME);
	file.WriteString(strLoad + ":\t" + strTime + "\n");      
    strLoad.LoadString (IDS_CAL_FORMAT16);
	file.WriteString(strLoad + strCalPhase[m_uCalPhase] + "\n");

    int nCalCurveNum = GetCalUserCurvesNum();
    strLoad.LoadString (IDS_CAL_FORMAT17);
	str.Format(strLoad, nCalCurveNum);
    file.WriteString(str);

    double fcoefficient;

    // 将每一条刻度曲线上的刻度系数加入到文件中去
    for (int i = 0; i < nCalCurveNum; i++)
    {
        strLoad.LoadString (IDS_CAL_FORMAT18);
		str.Format(strLoad , i);
        file.WriteString(str);

        int num = (m_pArrCalCoefs + i)->GetSize();
        for (int j = 0; j < num; j++)
        {
            fcoefficient = (m_pArrCalCoefs + i)->GetAt(j);
            str.Format("[%d]\t%7.4f\r\n", j, fcoefficient);
            file.WriteString(str);
        }
    }
    file.Close();
}
// ---------------------------------
//	读系统默认刻度文件:
// ---------------------------------
ULMIMP CULTool::ReadCalInfo(LPCTSTR pszFile, UINT uPhase /* =  */)
{
	CFile file;
	if (!file.Open(pszFile, CFile::modeRead))
		return UL_ERROR;
	
	if (ReadCalInfo(&file, uPhase))
	{
		file.Close();
		return UL_ERROR;
	}
	
	file.Close();
	return UL_NO_ERROR;
}

CString CULTool::GetCalDescFrom(LPCTSTR pszFile, int nCount /* = 256 */)
{
	CFile file;
	if (!file.Open(pszFile, CFile::modeRead))
		return _T("");
	
	CXMLSettings xml;
	if (!xml.ReadXMLFromFile(&file))
		return _T("");

	file.Close();
			
	int nDescs = 0;
	xml.Read("CalDescCount", nDescs);
	if (nCount < nDescs)
	{
		nDescs = nCount;
	}

	CString strText = " ";
	for (int i = 0; i < nDescs; i++)
	{
		if (xml.Open("CalDesc%02d", i))
		{
			CString strName = _T("");
			CString strValue= _T("");

			xml.Read("DescN", strName);
			xml.Read("DescV", strValue);
			
			strValue.Replace("\\n", "\r\n");
			strValue.Replace("\\t", "\t");

			if (strName.GetLength() && strValue.GetLength())
			{
				strText += strName + " : ";
				
				CString strR = strValue.Right(1);
				if (strR == "\n" /*|| strR == "\t"*/)
				{
					strText += strValue + " ";
					xml.Back();
					continue ;
				}

				strText += strValue + "\r\n";
			}
			
			xml.Back();
		}
	}

	strText.TrimRight("\r\n");
	return strText;
}

CString CULTool::GetCalTimeFrom(LPCTSTR pszFile, LPCTSTR pFormat /* = _T */)
{
	CFile file;
	if (!file.Open(pszFile, CFile::modeRead))
		return _T("");

	TCHAR    buf[512];
	CArchive ar(&file, CArchive::load, 512, buf);
	
	TRY {
		CString  strName;
		ar >> strName;
		
		if (strName != strToolName)
		{
			file.SeekToBegin();
			CXMLSettings xml;
			if (!xml.ReadXMLFromFile(&file, "ToolName", "CalTime"))
				return _T("");

			xml.Read("ToolName", strName);
			if (strName != strToolName)
				return _T("");
				
			long ltm;
			xml.Read("CalTime", ltm);
			CTime tmCaled = CTime((time_t)ltm);
			if (GetRValue(GetCalTimeColor(tmCaled)) == 255)
				return _T("");

			return tmCaled.Format(pFormat);
		}
		
		UINT uPhase;
		ar >> uPhase;
		
		CTime tmCaled = tmNew;
		ar >> tmCaled;
		if (GetRValue(GetCalTimeColor(tmCaled)) == 255)
			return _T("");

		return tmCaled.Format(pFormat);
	}
	CATCH (CException, e)
	{
		e->Delete();
		return _T("");
	}
	END_CATCH
	
	file.Close();
	return _T("");
}

ULMIMP CULTool::ReadCalInfo(CFile* pFile, UINT uPhase)
{
	CString strStatus;
	strStatus.Format(IDS_LOAD_CALIBRATE_INFO, strToolName);
	theApp.SetStatusPrompt(strStatus);
	BOOL bTime = TRUE;
	if (uPhase == ((UINT)-1))
	{
		uPhase = m_uCalPhase;
	}
	else
	{
		if (HIWORD(uPhase))
			bTime = FALSE;

		uPhase &= 0x03;
	}

	DWORD dwPos = pFile->GetPosition();
	DWORD cbLen = 0;
	DWORD nLen = pFile->GetLength();
	if (dwPos >= nLen)
		return UL_ERROR;
	pFile->Read(&cbLen, sizeof(DWORD));
	pFile->Seek(dwPos, CFile::begin);
	if (dwPos + cbLen > nLen)
		return UL_ERROR;

	CXMLSettings xml;
	if (xml.ReadXMLFromFile(pFile))
	{
		return SerializeCalInfo(xml, uPhase, bTime);
	}

	pFile->Seek(dwPos, CFile::begin);
	TCHAR    buf[512];
	CArchive ar(pFile, CArchive::load, 512, buf);
	TRY {
		CString  strName;
		ar >> strName;
		
		if (strName != strToolName)
			return UL_ERROR;
		
		UINT uCalPhase;
		ar >> uCalPhase;
		if (uCalPhase != uPhase)
		{
			if (uPhase != 3)
				return UL_ERROR;
			uPhase = uCalPhase;
			if (uPhase > 3)
				return UL_ERROR;
		}
		
		ar >> m_tmCaled[uPhase];
		m_bCaled[uPhase] = (GetRValue(GetCalTimeColor(uPhase)) == 255) ? FALSE : TRUE;
		
		if (!m_bCaled[uPhase])
			return UL_NO_ERROR;

		ClearCalCurves(uPhase);
		DWORD dwCurves = 0;
		ar >> dwCurves;
		for (DWORD i = 0; i < dwCurves; i++)
		{
			CULCalCurve* pCalCurve = NULL;
			ar >> pCalCurve;
			m_CalCurves[uPhase].Add(pCalCurve);
		}
		TCHAR buffer[512];
		ar.Read(buffer, 512);
		ar.Close();
	}
	CATCH (CException, e)
	{
		e->ReportError();
		e->Delete();
		return UL_ERROR;
	}
	END_CATCH
	
	return UL_NO_ERROR;
}

ULMIMP CULTool::ReadCalInfoEx(CFile* pFile, UINT uPhase)
{
	CString strStatus;
	strStatus.Format(IDS_LOAD_CALIBRATE_INFO, strToolName);
	theApp.SetStatusPrompt(strStatus);
	BOOL bTime = TRUE;
	if (uPhase == ((UINT)-1))
	{
		uPhase = m_uCalPhase;
	}
	else
	{
		if (HIWORD(uPhase))
			bTime = FALSE;

		uPhase &= 0x07;
	}

	DWORD dwPos = pFile->GetPosition();
	DWORD cbLen = 0;
	DWORD nLen = pFile->GetLength();
	if (dwPos >= nLen)
		return UL_ERROR;
	pFile->Read(&cbLen, sizeof(DWORD));
	pFile->Seek(dwPos, CFile::begin);
	if (dwPos + cbLen > nLen)
		return UL_ERROR;

	CXMLSettings xml;
	if (xml.ReadXMLFromFile(pFile))
	{
		// SerializeCalInfo(xml, uPhase, bTime);
		CString strName;
		xml.Read(_T("ToolName"), strName);
		if (strName != strToolName)
			strToolName = strName;

		UINT uCalPhase;
		xml.Read(_T("CalPhase"), uCalPhase);
		if (uCalPhase != uPhase)
		{
			uPhase = uCalPhase;
			if (uPhase > 3)
			{
				return UL_ERROR;
			}
		}

		m_uCalPhase = uPhase;
				
		long ltm;
		xml.Read(_T("CalTime"), ltm);
		m_tmCaled[uPhase] = CTime((time_t)ltm);
		
		m_bCaled[uPhase] = TRUE;
		
		CXMLNode* pCurrNode = xml.m_pCurrNode;
		POSITION pos = pCurrNode->m_lstChildren.GetHeadPosition();
		for (; pos != NULL;)
		{
			CXMLNode* pNode = pCurrNode->m_lstChildren.GetNext(pos);
			if (pNode->m_lstChildren.GetCount())
			{
				xml.SetCurrNode(pNode);
				CULCalCurve* pCur = GetCalCurve(uPhase, pNode->m_strName);
				if (pCur != NULL)
					pCur->SerializeEx(xml);
				else
				{
					pCur = new CULCalCurve;
					pCur->SerializeEx(xml);
					m_CalCurves[m_uCalPhase].Add(pCur);
				}	
			}
		}
		xml.m_pCurrNode = pCurrNode;

		ClearCalDescs(uPhase);
		int nDescs = 0;
		xml.Read("CalDescCount", nDescs);
		for (int i = 0; i < nDescs; i++)
		{
			if (xml.Open("CalDesc%02d", i))
			{
				CALDESC* pCalDesc = new CALDESC;
				xml.Read("DescN", pCalDesc->strName);
				xml.Read("DescV", pCalDesc->strValue);
				m_CalDescs[uPhase].AddTail(pCalDesc);
				xml.Back();
			}
		}

		if (m_CalCurves[uPhase].GetSize())
		{
			m_dwCalibrate |= dwCalPhase[uPhase];
			m_dwCalReports |= dwCalPhase[uPhase];
		}

		//MetaReport
		m_szMetaReport[uPhase] = 0;
		if(m_hMetaReport[uPhase] != NULL)
		{
			DeleteMetaFile(m_hMetaReport[uPhase]);
			m_hMetaReport[uPhase] = NULL;
		}
		
// 		if(xml.Open("MetaReport"))
		{
			xml.Read("MetaHeight",m_szMetaReport[uPhase]);
			
			CFile file;
			UINT dwMetaLen = 0;
			xml.Read("MetaSize",dwMetaLen);
			if (dwMetaLen > 0)
			{
				CString strTPh = "~";
				strTPh += strCalPhase[uPhase];
				BYTE* pBuf = new BYTE[dwMetaLen];
				xml.Read("MetaFile", pBuf, dwMetaLen);
				if (TRUE) // 为后面生成的wmf文件生成不同的名字
				{
					CString strFind = Gbl_AppPath + "\\Temp\\" + strTPh + _T("*.*");
					int nMax = GetMaxNoF(strFind);
					
					CString str;
					str.Format(_T("%d"), ++nMax);
					strTPh += _T("-") + str;	
				}
				
				CString strTemp = Gbl_AppPath + "\\Temp\\" + strTPh + ".wmf";
				if (file.Open(strTemp, CFile::modeCreate | CFile::modeWrite))
				{
					file.Write(pBuf, dwMetaLen);
					file.Close();
				}
				
				m_hMetaReport[m_uCalPhase] = GetMetaFile(strTemp);			
				delete[] pBuf;
			}
		}

		return UL_NO_ERROR;
	}

	pFile->Seek(dwPos, CFile::begin);
	TCHAR    buf[512];
	CArchive ar(pFile, CArchive::load, 512, buf);
	TRY {
		CString  strName;
		ar >> strName;
		
		if (strName != strToolName)
			return UL_ERROR;
		
		UINT uCalPhase;
		ar >> uCalPhase;
		if (uCalPhase != uPhase)
		{
			if (uPhase != 3)
				return UL_ERROR;
			uPhase = uCalPhase;
			if (uPhase > 3)
				return UL_ERROR;
		}
		
		ar >> m_tmCaled[uPhase];
		m_bCaled[uPhase] = (GetRValue(GetCalTimeColor(uPhase)) == 255) ? FALSE : TRUE;
		
		if (!m_bCaled[uPhase])
			return UL_NO_ERROR;

		ClearCalCurves(uPhase);
		DWORD dwCurves = 0;
		ar >> dwCurves;
		for (DWORD i = 0; i < dwCurves; i++)
		{
			CULCalCurve* pCalCurve = NULL;
			ar >> pCalCurve;
			m_CalCurves[uPhase].Add(pCalCurve);
		}
		TCHAR buffer[512];
		ar.Read(buffer, 512);
		ar.Close();
	}
	CATCH (CException, e)
	{
		e->ReportError();
		e->Delete();
		return UL_ERROR;
	}
	END_CATCH
	
	return UL_NO_ERROR;
}


// ---------------------------------
//	写系统默认刻度文件:
// ---------------------------------
ULMIMP CULTool::WriteCalInfo(LPCTSTR pszFile, UINT uPhase /* = (UINT)-1 */)
{
    CFile file;
    if (!file.Open(pszFile, CFile::modeCreate | CFile::modeWrite))
        return UL_ERROR;

	if (uPhase < 0 || uPhase > 3)
		uPhase = m_uCalPhase;

	if (SaveCalInfo(&file, uPhase))
	{
		file.Close();
		return UL_ERROR;
	}

    file.Close();
	return UL_NO_ERROR;
}

ULMIMP CULTool::SaveCalInfo(CFile* pFile, UINT uPhase)
{
    CXMLSettings xml(FALSE, "CalInfo");
	SerializeCalInfo(xml, uPhase);
	if (xml.WriteXMLToFile(pFile))
		return UL_NO_ERROR;

	return UL_ERROR;
}

BOOL CULTool::ReadUserCalFile(CString strFileName, CString strCalType)
{
    // read the user's own cal file by the file path name
    CFile file ;
    if (file.Open(strFileName, CFile::modeRead))
    {
        ReadCalFile(&file);
        CalUserLoad(&file, strCalType);
        file.Close();
    }
    return TRUE;
}

void CULTool::SetToolCalStatus()
{
    for (int i = 0; i < m_CalCurves[m_uCalPhase].GetSize(); i++)
    {
        CULCalCurve* pCurve = (CULCalCurve*)
                              m_CalCurves[m_uCalPhase].GetAt(i);
        if (pCurve->m_bCaled == FALSE)
        {
            m_bCaled[m_uCalPhase] = FALSE;
            return;
        }
    }
    m_bCaled[m_uCalPhase] = TRUE;
}

BOOL CULTool::GetToolCalStatus()
{
    return m_bCaled[m_uCalPhase];
}

/* -----------------
 *	用户刻度重载函数
 * ----------------- */
ULMIMP CULTool::CalUserCancel()
{
    // 用户重载此函数，将本次刻度所获取的刻度数据放弃，其它的数据保持不变
 #ifdef _PERFORATION
	if (m_pITool)
        m_pITool->CalUserCancel();
	if (GetCalType() == UL_CAL_INDEPENDENT)
	{
		if (g_pMainWnd->m_pCalFrm->GetSafeHwnd() != NULL)
		{
			g_pMainWnd->m_pCalFrm->CalUserCancel();
		}
	}
#endif
    SetToolState(TS_IDLE);
    return UL_NO_ERROR;
}

short CULTool::CalUserCalculate()
{
    // 用户根据自己的刻度公式，利用所获得的刻度数据，求解刻度系数
    // 并将刻度系数加入到与各条刻度曲线相对应的刻度系数存储数组中
    if (m_pITool)
        return m_pITool->CalUserCalculate();

    return UL_ERROR;
}

ULMIMP CULTool::CalUserLoad(CFile* pFile, LPCTSTR strCalPhaseName)
{
// 	if (m_pITool)
//         return m_pITool->CalUserLoad(pFile, (LPSTR)strCalPhase);
//     return UL_ERROR;

	if(pFile == NULL)
		return UL_ERROR;

	DWORD dwLen = 0; 
	pFile->Read(&dwLen, sizeof(DWORD));
	if (dwLen > 0)
	{
		BYTE* buf = new BYTE[dwLen];
		pFile->Read(buf, dwLen);
		if(m_pITool)
		{
			CMemFile file(buf, dwLen);
			m_pITool->CalUserLoad(&file, (LPSTR)strCalPhaseName);

			// 如果为自定义刻度报告，此时应该重新生成刻度报告
			for (int i = 0; i < 4; i++)
			{
				if (strCalPhaseName == strCalPhase[i])
				{
					break;
				}
			}
			if (i > -1 && i < 4 && m_bCalUserPrint[i])
			{
				CalcMetaReport(i);
			}
		}
		delete[] buf;
	}
    return UL_NO_ERROR;
}

ULMIMP CULTool::CalUserSave(CFile* pFile, LPCTSTR strCalPhaseName)
{
// 	if (m_pITool)
//         return m_pITool->CalUserSave(pFile, (LPSTR)strCalPhase);
//     return UL_ERROR;

	BOOL bReturn = UL_ERROR;

	if (pFile==NULL)
		return bReturn;
	
	CMemFile file;
	if (m_pITool)
	{
		bReturn = m_pITool->CalUserSave(&file, (LPSTR)strCalPhaseName);

		// 如果为自定义刻度报告，此时应该重新生成刻度报告
		for (int i = 0; i < 4; i++)
		{
			if (strCalPhaseName == strCalPhase[i])
			{
				break;
			}
		}
		if (i > -1 && i < 4 && m_bCalUserPrint[i])
		{
			CalcMetaReport(i);
		}
	}

	DWORD dwLen = file.GetLength();
	if (dwLen > 0)
	{
		pFile->Write(&dwLen, sizeof(DWORD));
		if (dwLen > 0)
		{
			BYTE* buf = file.Detach();
			pFile->Write(buf, dwLen);
			delete[] buf;
		}
	}
	else
	{
		dwLen = 0;
		pFile->Write(&dwLen, sizeof(DWORD));
	}

    return bReturn;
}

ULMIMP CULTool::CalUserLoad(CFile* pFile, UINT uCalPhase)
{
    return CalUserLoad(pFile, strCalPhase[uCalPhase]);
}

ULMIMP CULTool::CalUserSave(CFile* pFile, UINT uCalPhase)
{
    return CalUserSave(pFile, strCalPhase[uCalPhase]);
}

short CULTool::CalUserPrint(CDC* pDC, CSize szPage, CString strCalPhase)
{
    // 提供给用户一些默认的刻度数据输出，可以自己编写来实现

    return UL_ERROR;
}

short CULTool::CalSysPrint(CDC* pDC, CSize szPage, CString strCalPhase)
{
    return UL_ERROR;
}

short CULTool::CalUserPrint(CDC* pDC, CSize szPage, UINT uCalPhase)
{
    // 提供给用户一些默认的刻度数据输出，可以自己编写来实现
    if (m_pITool)
        return m_pITool->CalUserPrint(pDC, szPage, uCalPhase);
    return UL_ERROR;
}

// 增加系统打印功能
short CULTool::CalSysPrint(CDC* pDC, CSize szPage, UINT uCalPhase)
{
    return UL_ERROR;
}

/* -----------------
 *	设置仪器中刻度曲线的状态与其中的刻度点的状态
 *	bCaled : FALSE	曲线与刻度点未刻度
 *			TRUE	曲线与刻度点已经刻度完成
 *  此函数只对系统刻度有效
 * ----------------------------------------- */

void CULTool::SetSysCalCurveState(BOOL bCaled /* = FALSE */)
{
	for (int n = 0; n < 3; n++)
	{
		if (!(m_nCalType[n] & UL_CAL_DEFAULT))
			continue ;
		
		int nCur = m_CalCurves[n].GetSize();
		for (int i = 0; i < nCur; i++)
		{
			CULCalCurve* pCur = (CULCalCurve*)m_CalCurves[n].GetAt(i);
			if (pCur == NULL)
				continue ;
			
			pCur->m_bCaled = bCaled;
			for (int j = 0; j < pCur->m_CalCSs.GetSize(); j++)
			{
				CULCalCurveSeg* pSeg = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(j);
				if (pSeg == NULL)
					continue ;

				for (int k = 0; k < pSeg->m_CalDotList.GetSize(); k++)
				{
					CULCalDot* pDot = (CULCalDot*)pSeg->m_CalDotList.GetAt(k);
					if (pDot == NULL)
						continue ;

					pDot->m_bCal = bCaled;
				}
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////
// Tool Verifying Operations
///////////////////////////////////////////////////////////////////////
ULMIMP CULTool::BeforeVerify()
{
	//if(m_pITool)
	//	m_pITool->BeforeVerify();

    SetToolState(TS_VERIFY);
    return UL_NO_ERROR;
}

ULMIMP CULTool::VerifyIO()
{
	//if(m_pITool)
	//	m_pITool->VerifyIO();

    return UL_NO_ERROR;
} 

ULMIMP CULTool::AfterVerify()
{
	//if(m_pITool)
	//	m_pITool->AfterVerify();

    SetToolState(TS_IDLE);
    return UL_NO_ERROR;
}
 
///////////////////////////////////////////////////////////////////////
// Replay Calibrate File
///////////////////////////////////////////////////////////////////////

// 设置时间驱动
long _stdcall CULTool::SetTimeStartInfo(float fStartTime, int nDirection )  
{
	m_dwStartTickCount = GetTickCount();
   return m_dwStartTickCount ;

}

ULMLNGIMP CULTool::SetRelativeTime() 
{
	DWORD dwElapseCount = GetTickCount() - m_dwStartTickCount;
	SetCurTime(dwElapseCount);
	return dwElapseCount;
}

ULMDWDIMP CULTool::SendCommand(int nChannel, PTBYTE pCmdBuf, UINT nSize)
{
	if (m_nWorkMode == WORKMODE_COMPUTEPLAYBACK)
		return FALSE;

	if (nChannel >= m_Channels.GetSize())	
		return FALSE;

	CChannel* pChannel = (CChannel *)m_Channels.GetAt(nChannel);
	if(pChannel == NULL) return FALSE;
	
	AskCommand cmd;
	cmd.nCommandType = UL_SENDCOMMAND;
	cmd.ex_context.cmd_param.dwCHID = pChannel->dwID;
	memcpy(cmd.ex_context.cmd_param.cmd_buf, pCmdBuf, nSize);
	cmd.ex_context.cmd_param.cmd_size = nSize;
	
	return SendAskCmd(&cmd);
}

DWORD CULTool::SendAskCmd(AskCommand * pCmd)
{
	if (m_pChannelEvent == NULL)
		m_pChannelEvent = new CEvent(FALSE, FALSE, "AskChannel", NULL);
	if (m_pWaitEvent == NULL)
		m_pWaitEvent = new CEvent(FALSE, FALSE, "RespondAskChannel", NULL);
	
	if (m_hExchangeFile == NULL) 
		MapExchangeFile();	
	
	memcpy(m_lpCodeBuffer, pCmd, sizeof(AskCommand));
	m_pChannelEvent->SetEvent();
	
	int nRetCode = ::WaitForSingleObject(m_pWaitEvent->m_hObject, 1000);
	
	if (nRetCode == WAIT_OBJECT_0)
	{
		AskCommand * pReturnCmd = (	AskCommand *)m_lpCodeBuffer;
		return pReturnCmd->ex_context.cmd_param.dwReturn;		
	}
	else		
		return UL_ERROR;
}

ULMIMP CULTool::UpdateData(LPCTSTR lpszCell, BOOL bSaveAndValidate /* = TRUE */)
{
#ifdef _PERFORATION
	CChildFrame* pChild = NULL;
	if (m_mapCell.Lookup(lpszCell, (void*&)pChild))
	{
		if (bSaveAndValidate)
			Gbl_DataEx.UpdateVar(&pChild->m_pGHView->m_Cell);
		else
			Gbl_DataEx.UpdateCell(&pChild->m_pGHView->m_Cell, pChild->m_pDoc->m_pProject);
		return UL_NO_ERROR;
	}
#endif	
	return UL_ERROR;
}

ULMIMP CULTool::UpdateData(BOOL bSaveAndValidate /* = TRUE */)
{
#ifdef _PERFORATION
	for(int i=0; i<m_arCellName.GetSize(); i++)
	{
		CChildFrame* pChild = NULL;
		if(m_mapCell.Lookup(m_arCellName[i], (void*&)pChild))
		{
			ASSERT(pChild->m_pGHView != NULL);
			if(bSaveAndValidate)
				Gbl_DataEx.UpdateVar(&pChild->m_pGHView->m_Cell);
			else
				Gbl_DataEx.UpdateCell(&pChild->m_pGHView->m_Cell, pChild->m_pDoc->m_pProject);
		}
	}
#endif	
	return UL_NO_ERROR;
}

ULMIMP CULTool::SaveParams()
{
#ifdef _PERFORATION
	if (m_pIParam)
	{
		m_pIParam->OnSaveParam();
		m_Params.Save();
		CMainFrame* pMainFrame = (CMainFrame*)theApp.m_pMainWnd;
		if (pMainFrame->m_pChildParam->GetSafeHwnd())
			((CParamsView*)pMainFrame->m_pChildParam->m_pULView)->OnParamsChange();
	}
#endif
	return UL_NO_ERROR;
}

ULMIMP CULTool::SaveParam(LPCTSTR pszKey)
{
#ifdef _PERFORATION
	CToolParam* pTP = m_Params.FindParam(pszKey);
	if (pTP)
	{
		if (m_pIParam)
		{
			m_pIParam->OnSaveParam();
			InterlockedIncrement(&pTP->m_lSave);
			CMainFrame* pMainFrame = (CMainFrame*)theApp.m_pMainWnd;
			if (pMainFrame->m_pChildParam->GetSafeHwnd())
				((CParamsView*)pMainFrame->m_pChildParam->m_pULView)->OnParamsChange();
		}

		return UL_NO_ERROR;
	}
#endif
	return UL_ERROR;
}

void CULTool::ClearSensors()
{
	for (int i = 0; i < m_Sensors.GetSize(); i++)
	{
		CSensor* pMP = (CSensor*)m_Sensors.GetAt(i);
		if (pMP != NULL)
			delete pMP;
	}
	m_Sensors.RemoveAll();
}

void CULTool::SerializeProps(CXMLSettings& xml, DWORD dwSN /* = 0x03 */)
{
	if (xml.IsLoading())
	{
		CString str;
		if (dwSN & 0x01)
		{
			xml.Read("SN", str);
			if (strSN == _T("Default") && str.GetLength() > 0)
			{
				strSN = str;
			}
			xml.Read("Length", lLength);
			xml.Read("Weight", fWeight);
			xml.Read("I.D.", iDiameter);
			xml.Read("O.D.", oDiameter);
			xml.Read(_T("O.D.Max"), oDiameterMax);
			xml.Read(_T("O.D.Min"), oDiameterMin);
			xml.Read("MaxPressure", fMaxPressure);
			xml.Read("MaxTemp", fMaxTemperature);
			xml.Read("MaxSpeed", fMaxSpeed);
			xml.Read(_T("CalReports"), m_dwCalReports);
			m_dwCalReports &= m_dwCalibrate;
		}

		if (dwSN & 0x02)
		{
			xml.Read("Sensors", bSensors);
			int nSensors = 0;
			xml.Read("SensorCount", nSensors);
			ClearSensors();
			for (int i = 0; i<nSensors; i++)
			{
				if (xml.Open("Sensor%02d", i))
				{
					CSensor* pMP = new CSensor;
					xml.Read(_T("Name"), pMP->strName);
					xml.Read(_T("Visible"), pMP->bShow);
					xml.Read(_T("Offset"), pMP->lPoint);
					m_Sensors.Add(pMP);
					xml.Back();
				}
			}

			if (m_dwCalibrate != 0)
			{
				if (xml.Open("CalCurves"))
				{
					int nPreFlag=0;
					xml.Read("PreFlag",nPreFlag);
					if (nPreFlag == 1)
					{
						for (int i = 0; i < 4; i++)
						{
							int nCurves = m_CalCurves[i].GetSize();
							for (int j = 0; j < nCurves; j++)
							{
								CULCalCurve* pCur = (CULCalCurve*)m_CalCurves[i].GetAt(j);
								xml.Read(pCur->m_strName, pCur->m_nPrecis);
							}
						}
					}		
				}
			}
		}
		
		if (dwSN & 0x01)
		{
			if (xml.Open("Curves"))
			{
				int nCurves = m_Curves.GetSize();
				for (int j = 0; j < nCurves; j++)
				{
					CCurve* pCurve = (CCurve*)m_Curves.GetAt(j);
					if (xml.Open(pCurve->m_strName))
					{
						xml.Read("OffMode", pCurve->m_pData->m_nOffsetMode);
						xml.Read("DepthOff", pCurve->m_pData->m_lDepthOffset);
						xml.Back();
					}
				}
				xml.Back();
			}
			
			if (xml.Open("Calibration"))
			{
				if (xml.Open("Master"))
				{
					xml.Read("Define",m_bCalDefined[0]);
					xml.Read("Type",m_nCalType[0]);
					xml.Read("Cal",m_bCaled[0]);
					xml.Back();
				} 
				if (xml.Open("Before"))
				{
					xml.Read("Define",m_bCalDefined[1]);
					xml.Read("Type",m_nCalType[1]);
					xml.Read("Cal",m_bCaled[1]);
					xml.Back();
				} 
				if (xml.Open("After"))
				{
					xml.Read("Define",m_bCalDefined[2]);
					xml.Read("Type",m_nCalType[2]);
					xml.Read("Cal",m_bCaled[2]);
					xml.Back();
				} 
				if (xml.Open("Primary"))
				{
					xml.Read("Define",m_bCalDefined[3]);
					xml.Read("Type",m_nCalType[3]);
					xml.Read("Cal",m_bCaled[3]);
					xml.Back();
				} 
			}
		}
	}
	else
	{
		xml.Write("SN", strSN);
		xml.Write("Length", lLength);
		xml.Write("Weight", fWeight);
		xml.Write("I.D.", iDiameter);
		xml.Write("O.D.", oDiameter);
		xml.Write("MaxPressure", fMaxPressure);
		xml.Write("MaxTemp", fMaxTemperature);
		xml.Write("MaxSpeed", fMaxSpeed);
		xml.Write("CalReports", m_dwCalReports);
		xml.Write("Sensors", bSensors);
		int nSensors = m_Sensors.GetSize();
		xml.Write("SensorCount", nSensors); 
		for (int i = 0; i<nSensors; i++)
		{
			if (xml.CreateKey("Sensor%02d", i))
			{
				CSensor* pMP = (CSensor*)m_Sensors.GetAt(i);
				xml.Write(_T("Name"), pMP->strName);
				xml.Write(_T("Visible"), pMP->bShow);
				xml.Write(_T("Offset"), pMP->lPoint);
				xml.Back();
			}
		}

		if (xml.CreateKey("Curves"))
		{
			int nCurves = m_Curves.GetSize();
			for (int j = 0; j < nCurves; j++)
			{
				CCurve* pCurve = (CCurve*)m_Curves.GetAt(j);
				if (xml.CreateKey(pCurve->m_strName))
				{
					xml.Write("OffMode", pCurve->m_pData->m_nOffsetMode);
					xml.Write("DepthOff", pCurve->m_pData->m_lDepthOffset);
					xml.Back();
				}
			}
			xml.Back();
		}

		if (xml.CreateKey("Calibration"))
		{
			if (xml.CreateKey("Master"))
			{
				xml.Write("Define",m_bCalDefined[0]);
				xml.Write("Type",m_nCalType[0]);
				xml.Write("Cal",m_bCaled[0]);
				xml.Back();
			} 
			if (xml.CreateKey("Before"))
			{
				xml.Write("Define",m_bCalDefined[1]);
				xml.Write("Type",m_nCalType[1]);
				xml.Write("Cal",m_bCaled[1]);
				xml.Back();
			} 
			if (xml.CreateKey("After"))
			{
				xml.Write("Define",m_bCalDefined[2]);
				xml.Write("Type",m_nCalType[2]);
				xml.Write("Cal",m_bCaled[2]);
				xml.Back();
			} 
			if (xml.CreateKey("Primary"))
			{
				xml.Write("Define",m_bCalDefined[3]);
				xml.Write("Type",m_nCalType[3]);
				xml.Write("Cal",m_bCaled[3]);
				xml.Back();
			} 
			xml.Back();
		}

		if (m_dwCalibrate == 0)
			return;
		
		if (xml.CreateKey("CalCurves"))
		{
			xml.Write("PreFlag",1);
			for (int i = 0; i < 4; i++)
			{
				int nCurves = m_CalCurves[i].GetSize();
				for (int j = 0; j < nCurves; j++)
				{
					CULCalCurve* pCur = (CULCalCurve*)m_CalCurves[i].GetAt(j);
					if (pCur->m_nPrecis >=0)
						xml.Write(pCur->m_strName, pCur->m_nPrecis);
				}
			}
		}
	}
}

void CULTool::SerializeCurves(CXMLSettings& xml)
{
	if (xml.IsLoading())
	{
		for (int i = m_Curves.GetSize() - 1; i > -1; i--)
		{
			CCurve* pCurve = (CCurve*)m_Curves.GetAt(i);
			if (xml.Open(pCurve->m_strName))
			{
				pCurve->Serialize(xml);
				xml.Back();
			}
		}
	}
	else
	{
		for (int i = m_Curves.GetSize() - 1; i > -1; i--)
		{
			CCurve* pCurve = (CCurve*)m_Curves.GetAt(i);
			if (xml.CreateKey(pCurve->m_strName))
			{
				pCurve->Serialize(xml);
				xml.Back();
			}
		}
	}
}

ULMIMP CULTool::GroupWithTool(LPCTSTR strToolName)
{
	m_strGroupTo = strToolName;
	return UL_NO_ERROR;
}

ULMPTRIMP CULTool::OpenTable(LPCTSTR lpszTableName, LPCTSTR lpszTableCHName /* = NULL */)
{
	return NULL;
}

ULMPTRIMP CULTool::CreateTable(LPCTSTR lpszTableName, LPCTSTR lpszTableCHName, int nField)
{
	return NULL;
}

ULMIMP CULTool::CloseTable(LPCTSTR lpszTableName, LPCTSTR lpszTableCHName /* = NULL */)
{
	return UL_NO_ERROR;
}

void CULTool::CalMeasMaxMin()
{
	for (int n = 0; n < 3; n++)
    {
		int nCurs = m_CalCurves[n].GetSize();
        for (int i = 0; i < nCurs; i++)
        {
            CULCalCurve* pCur = (CULCalCurve*)m_CalCurves[n].GetAt(i);
			if (pCur == NULL)
				continue ;

            for (int j = 0; j < pCur->m_CalCSs.GetSize(); j++)
            {
                CULCalCurveSeg* pCS = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(j);
				if (pCS == NULL)
					continue ;

				for (int k = 0; k < pCS->m_CalDotList.GetSize(); k++)
				{
					CULCalDot* pDot = (CULCalDot*)pCS->m_CalDotList.GetAt(k);
					pDot->CalcMinMax();
				}
			}
		}
	}
}

ULMIMP CULTool::ClearCalDotBuffer(int nCurve, int nSeg, int nDot)
{
    CULCalDot* pCalDot = GetCalDot(m_uCalPhase, nCurve, nSeg, nDot);
    if (pCalDot != NULL)
	{
		pCalDot->m_Buffer.clear();
		return UL_NO_ERROR;
	}

	return UL_ERROR;
}

ULMIMP CULTool::GetCurves(CURVES& vecCurve)
{
	vecCurve.clear();
	for (int i = 0; i<m_Curves.GetSize(); i++)
	{
		ICurve* pCurve = (ICurve*)m_Curves.GetAt(i);
		vecCurve.push_back(pCurve);
	}
	return vecCurve.size();
}

ULMPTRIMP CULTool::GetCurve(LPCTSTR lpszName, LPCTSTR lpszIDName /* = NULL */)
{
	CString strCurve = lpszName;
	for (int i = 0; i < m_Curves.GetSize(); i++)
	{
		CCurve* pCurve = (CCurve*)m_Curves.GetAt(i);
		if(pCurve->m_strName == lpszName)
		{
			if (lpszIDName != NULL)
			{
				if(_tcscmp(pCurve->m_strIDName, lpszIDName)==0)
					return pCurve;
			}
			else
				return pCurve;
		}
	}
	return NULL;
}

ULMPTRIMP CULTool::GetStatusCurve(LPCTSTR lpszName, LPCTSTR lpszIDName /* = NULL */)
{
	CString strCurve = lpszName;
	for (int i = 0; i<m_StatusCurves.GetSize(); i++)
	{
		CCurve* pCurve = (CCurve*)m_StatusCurves.GetAt(i);
		if(pCurve->m_strName == strCurve)
		{
			if (lpszIDName != NULL)
			{
				if(_tcscmp(pCurve->m_strIDName, lpszIDName)==0)
					return pCurve;
			}
			else
				return pCurve;
		}
	}
	return NULL;
}

ULMIMP CULTool::SetCalValues(int nValue /* = VM_AVE */)
{
	if(nValue < 1)
		return UL_NO_ERROR;
	
	if(nValue < VM_END + 1)
	{
		m_nCalVM = nValue;
		return UL_NO_ERROR;
	}
	
	return UL_ERROR;
}

void CULTool::SNChanged(LPCTSTR pszSN, BOOL bReplay /* = FALSE */, 
						int iOption /* = 0 */, BOOL* pCheck /* = NULL */)
{
	strSN = pszSN;

	if (m_pToolInfoPack == NULL)
		return ;

	m_pToolInfoPack->m_Tool.strSN = pszSN;
	CString strPath = GetToolDllPath();
	strPath += pszSN;

	if ((pCheck != NULL) && (iOption > 0))
	{
		if (_tchdir(strPath))
		{
			if (_tmkdir(strPath))
				return ;
		}

		CFileFind ff;
		CStringArray strArray;
		CString strPrompt;
		CString strE;
		for (int i = 0; i < 4; i++)
		{
			if (pCheck[i] == 0)
				continue ;

			CString strFile = m_CalFilePathArray[i];
			if (strFile.IsEmpty())
				continue ;

			if (!ff.FindFile(strFile))
				continue ;

			SplitPath(strFile, strArray);
			int nEnd = strArray.GetSize() - 2;
			if (nEnd < 0)
				continue ;

			// 查找Txt刻度文件是否存在
			CString strTxt = strFile.Left(strFile.GetLength() - 3);
			strTxt += "txt";
			if (!ff.FindFile(strTxt))
				strTxt.Empty();

			CString strTime = m_tmCaled[i].Format("%Y-%m-%d-%H-%M");
			CString strNewFile = strPath + "\\" + strTime;
			
			// 目标文件夹下创建时间文件夹
			if (_tchdir(strNewFile))
			{
				if (_tmkdir(strNewFile))
					continue ;
			}

			// 源刻度文件在时间文件夹时，如果是移动，则移动完毕时移除此目录(如果为空)
			CString strRemove;
			CString strDeletes[2];
			int nFind = strFile.ReverseFind('\\');
			strRemove = strFile.Left(nFind);
			if (strArray.GetAt(nEnd) == strTime) 
			{
				
			}
			else
			{
				// 查找目录下是否存在此时间文件
				// 存在且移动时，需要移除此目录下同名的刻度文件及目录
				CString strDel = strRemove + "\\" + strTime + "\\" + strArray.GetAt(nEnd + 1);
				if (ff.FindFile(strDel))
					strDeletes[0] = strDel;

				strDel = strDel.Left(strDel.GetLength() - 3);
				strDel += "txt";
				if (ff.FindFile(strDel))
				{
					if (strTxt.IsEmpty())
						strTxt = strDel;

					strDeletes[1] = strDel;
				}

				if (strDeletes[0].GetLength() || strDeletes[1].GetLength())
					strRemove += "\\" + strTime;
				else
					strRemove.Empty();
			}
			
			strNewFile += "\\" + strArray.GetAt(++nEnd);
			CString strNewSN = strPath + "\\" + strArray.GetAt(nEnd);
			CString strTimeSN = GetCalTimeFrom(strNewSN, "%Y-%m-%d-%H-%M");
			if (strTimeSN.IsEmpty() || (strTimeSN.GetLength() && (strTimeSN < strTime)))
			{
				if (CopyFile(strFile, strNewSN, FALSE))
				{
					m_CalFilePathArray.SetAt(i, strNewSN);
				}
				else
				{
					strE.Format("Failed to copy %s to %s!", strFile, strNewSN);
					if (strPrompt.GetLength())
						strPrompt += "\r\n";
					strPrompt += strE;
				}
			}
			
			// 复制刻度文件到时间目录下
			if (CopyFile(strFile, strNewFile, FALSE))
			{
				m_CalFilePathArray.SetAt(i, strNewFile);
				if (iOption == 2)
				{
					DeleteFile(strFile);
					if (strDeletes[0].GetLength())
						DeleteFile(strDeletes[0]);
				}
			}
			else
			{					
				strE.Format("Failed to copy %s to %s!", strFile, strNewFile);
				if (strPrompt.GetLength())
					strPrompt += "\r\n";
				strPrompt += strE;
			}

			if (strTxt.GetLength())
			{
				CString strNewTxt = strNewFile.Left(strNewFile.GetLength() - 3);
				strNewTxt += "txt";
				if (CopyFile(strTxt, strNewTxt, FALSE))
				{
					if (iOption == 2)
					{
						DeleteFile(strTxt);
						if (strDeletes[1].GetLength())
							DeleteFile(strDeletes[1]);
					}
				}
				else
				{
					strE.Format("Failed to copy %s to %s!", strTxt, strNewTxt);
					if (strPrompt.GetLength())
						strPrompt += "\r\n";
					strPrompt += strE;
				}
			}

			if (strRemove.GetLength() && (iOption == 2))
			{
				ff.Close();
				if (RemoveDirectory(strRemove) == 0)
				{
					strE.Format("Failed to delete %s!", strRemove);
					if (strPrompt.GetLength())
						strPrompt += "\r\n";
					strPrompt += strE;
				}
			}
		}

		if (strPrompt.GetLength())
			AfxMessageBox(strPrompt);
	}
	else
		pCheck = NULL;

	if (bReplay)
		LoadCalInfo(strPath, LS_SAVE|LS_TIME, pCheck);
	else
		LoadCalInfo(strPath, 0, pCheck);
	
	CalUserLoad(NULL, "");
}

// 加载刻度文件
//	
BOOL CULTool::LoadCalInfo()
{
// 	TOOL_INFO* pToolInfo = Gbl_ConfigInfo.GetToolInfo(strENName);
// 	if (pToolInfo == NULL)
// 		return FALSE;
// 	
//	strInfoPath = Gbl_ConfigInfo.GetToolDllPath(*pToolInfo);
	CString str = Gbl_AppPath + "\\Tools\\" + strInfoPath;

	if (m_pToolInfoPack)
		strSN = m_pToolInfoPack->m_Tool.strSN;
// 	else
// 		strSN = pToolInfo->S_NO;
	

	// 用户自定义刻度：加载默认文件夹Defualt中最新的刻度文件
	// 系统刻度：      加载默认文件夹Defualt中的三种类型：Master,Before,After名称刻度文件
	// 在搜索刻度文件时，存在错误，现在只搜索default目录，没有对仪器号进行处理，在 default目录中的刻度文件为该仪器号时可以得到正确结果，否则仪器刻度文件会加载错误
	strSN.TrimRight(); strSN.TrimLeft();
	if(!strSN.IsEmpty()) // add by bao 2013/7/22 序列号为空时路径加载不正确
	{
		str += strSN;
		str += "\\";
	}

	LoadCalInfo(str, LS_SAVE);
	return TRUE;
}

void CULTool::LoadCalInfo(CString str, DWORD dwLS /* = 0 */, BOOL* pCheck /* = NULL */)
{
	int nLen = str.GetLength();
	if (nLen < 1)
		return ;

	if (str[nLen-1] != '\\')
		str += "\\";

	CFileFind ff;
	CFile file;
	BOOL bCheck[4] = { 0, 0, 0, 0};
	if (pCheck != NULL)
	{
		for (int i = 0; i < 4; i++)
		{
			bCheck[i] = pCheck[i];
		}
	}
	//modify by gj 2012/11/29
	//修改：SetUserDotVar的放置位置改变，先设置初始值，再加载刻度文件
	SetUserDotVar();
	for (int i = 0; i < 4; i++)
	{
		if (bCheck[i])
			continue ;

		// User's own define calibration type
		if (m_nCalType[i] & UL_CAL_USER)
		{
			CString strFile = str + strCalPhase[i] + ".udc";
			if (ff.FindFile(strFile))
			{
				SetCalPhase(i);
				if (file.Open(strFile, CFile::modeRead))
				{
					ReadCalFile(&file);
					CalUserLoad(&file, strCalPhase[i]);
					file.Close();

					m_strCalFilePath = strFile;
					m_CalFilePathArray.SetAt(i, strFile);

					if (m_bCalDefined[i])
					{
						CopyCalToPrj(NULL, i); // add by gj 2013-10-23 工程目录下拷贝刻度文件
					}
				}			
			}
		}
		else
		{

			CString strNewFile = GetToolDllPath();
			if (strNewFile.GetLength())
				strNewFile += (strCalPhase[i] + "New.ulc");
			
			if ((dwLS & LS_SAVE) && strNewFile.GetLength())
			{
				CTime tmCaled = m_tmCaled[i];
				m_tmCaled[i] = CTime::GetCurrentTime();
				WriteCalInfo(strNewFile, i);
				m_tmCaled[i] = tmCaled;
			}

			CString strFile = str + strCalPhase[i] + ".ulc";
			if (!ff.FindFile(strFile))
			{
				CTime tmCaled = m_tmCaled[i];
				ReadCalInfo(strNewFile, i);
				if (m_bCalDefined[i])
				{
					CopyCalToPrj(NULL, i); // add by gj 2013-10-23 工程目录下拷贝刻度文件
				}
				m_tmCaled[i] = tmNew; // tmCaled;
				continue ;
			}
			

			if (ReadCalInfo(strFile, MAKELONG(i, (dwLS & LS_TIME))))
				continue ;
			
			if (m_bCalDefined[i])
			{
				CopyCalToPrj(NULL, i); // add by gj 2013-10-23 工程目录下拷贝刻度文件
			}

			m_strCalFilePath = strFile;
			m_CalFilePathArray.SetAt(i, strFile);
		}
	}
	
	CalcAllMetaReport();
// 	SetUserDotVar();


}

// --------------------------------------------------
//	Get time span off the calibrate time : 
// --------------------------------------------------
ULMIMP CULTool::GetCalTime(CTime& tm)
{
	UINT uPhase = m_uCalPhase % 4;
	if (m_tmCaled[uPhase] != tmNew)
	{
		tm = m_tmCaled[uPhase];
		return UL_NO_ERROR;
	}

	return UL_ERROR;
}

COLORREF CULTool::GetCalTimeColor(CTime& tmCaled)
{
	if (tmCaled.GetTime() == tmNew)
		return RGB(255,255,255);

	int nHs = m_nCalDays * 8;
	long lDays = nHs / 24;
	int nHours = nHs % 24;
	CTimeSpan tms(lDays, nHours, 0, 0);

	CTime tmC = CTime::GetCurrentTime();
	CTime tm = tmCaled + tms;
	
	for (int i = 0; i < 3; i++)
	{
		if (tmC < tm)
			break;
		
		tm += tms;
	}

	return clrTm[i];
}
//***************************************************************************
int CULTool::GetToolSNs(CStringList* pList)
{
/*	if (strInfoPath.IsEmpty())
	{
		int nLen = strToolDllDir.GetLength();
		if (nLen > 0)
		{
			CString strLeft = strToolDllDir.Left(7);
			if (strLeft.CompareNoCase("\\Tools\\") == 0)
			{
				int nFind = strToolDllDir.ReverseFind('\\');
				if ((nFind > 0) && (nFind < nLen))
					strInfoPath = strToolDllDir.Mid(7, nFind - 6);
			}
		}

		if (strInfoPath.IsEmpty())
		{
			TOOL_INFO* pToolInfo = Gbl_ConfigInfo.GetToolInfo(strENName);
			if (pToolInfo != NULL)
				strInfoPath = Gbl_ConfigInfo.GetToolDllPath(*pToolInfo);
			else
				return 0;
		}
	}
*/
	WIN32_FIND_DATA fd;
	CString strToolDir = Gbl_AppPath + "\\Tools\\" + strInfoPath;
	if (strToolDir.Right(1) != "\\")
		strToolDir += '\\';
	
	CString strToolInstallDir = strToolDir + "*.*";
	
	HANDLE hFind = ::FindFirstFile (strToolInstallDir, &fd);
	BOOL bFindNext = (hFind != INVALID_HANDLE_VALUE);
	
	while (bFindNext)
	{
		if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		{
			CString strDirName = fd.cFileName;
			if (strDirName != "." && strDirName != ".." && strDirName != "Default")
			{
				pList->AddTail(strDirName);				
			}
		}
		bFindNext = ::FindNextFile (hFind, &fd);
	}
	
	::FindClose(hFind);

	if ((strSN != _T("Default")) && (!pList->Find(strSN)))
		pList->AddTail(strSN);

	return pList->GetCount();
}

//初始化刻度辅助界面的一个刻度阶段的cell表, 点击"放弃"按钮时也会调用
//***************************************************************************
void CULTool::InitCalCell(CCell2000* pCell, UINT uPhase, UINT nGroup/* = 0*/)
{
	ASSERT(uPhase < 4);

	// ---------------------------------
	//	Init the calibrate cell :
	// ---------------------------------

	CStringList strList0,	// Calibrate curve
				strList1,	// Calibrate dot
				strList2,	// Calibrate coef
				strList3,	// Calibrate items (dot & coef)
				strList4,   // Calibrate dot Alias
				strList5,   // Calbirate coef Alias
				strList6;   // Calibrate items Alias 

	CString strMea, strRef, strEng;
	strMea.LoadString(IDS_READ_VALUE);
	strRef.LoadString(IDS_ENG_VALUE);
	strEng.LoadString(IDS_ENG_VALUE);

	strMea = _T("Read");
	strRef = _T("Ref");
	strEng = uPhase ? _T("Eng") : _T("Ref");

	// Top left grid's coordinate of the cell 
	int nX = 2;
	int nY = 3;

	CMapStringToPtr	mapDots;
	for (int i = 0; i < m_CalCurves[uPhase].GetSize(); i++)
	{
		CULCalCurve* pCur = (CULCalCurve*)m_CalCurves[uPhase].GetAt(i);
		if (pCur == NULL)
			continue ;

		if (pCur->m_nCalGroup != nGroup/* && (uPhase == m_uCalPhase)*/) // 2014 2 11  分组时不显示不属于该分组内的刻度曲线
		{
			continue;
		}

		if (strList0.Find(pCur->m_strName))
			continue ;

		strList0.AddTail(pCur->m_strName);
		
		if (pCur->m_CalCSs.GetSize() < 1)
			continue ;

		CULCalCurveSeg* pSeg = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(0);
		for (int j = 0; j < pSeg->m_CalDotList.GetSize(); j++)
		{
			CULCalDot* pDot = (CULCalDot*)pSeg->m_CalDotList.GetAt(j);
			if (pDot == NULL)
				continue ;

			if (strList1.Find(pDot->m_strName))
			{
				CPtrList* pDotList = NULL;
				mapDots.Lookup(pDot->m_strName, (void*&)pDotList);
				if (pDotList)
				{
					CULCalDot* pHD = (CULCalDot*)pDotList->GetHead();
					pDot->m_nX = pHD->m_nX;
					pDot->m_nY = nY;
					pDotList->AddTail(pDot);
				}

				continue ;
			}

			strList1.AddTail(pDot->m_strName);
			strList1.AddTail(_T(""));
			strList4.AddTail(pDot->m_strAlias);
			strList4.AddTail(_T(""));

			pDot->m_nX = nX;
			pDot->m_nY = nY;

			CPtrList* pDotList = new CPtrList;
			pDotList->AddTail(pDot);
			mapDots[pDot->m_strName] = pDotList;

			strList3.AddTail(/*pDot->m_strName + "\r\n" +*/ strMea);
			strList3.AddTail(/*pDot->m_strName + "\r\n" +*/ strEng);

			nX += 2;
		}

		for (int k = 0; k < pSeg->m_CalCoefs.GetSize(); k++)
		{
			ULCalCoef coef = pSeg->m_CalCoefs.GetAt(k);
			
 			if (strList2.Find(coef.szName))
 				continue ;
 
 			strList2.AddTail(coef.szName);
 			strList5.AddTail(coef.szAlias);
		}

		nY++;
	}

	if (m_uCalPhase == uPhase)
	{
		m_DotsList.RemoveAll();
		POSITION pos = NULL;
		for (pos = strList1.GetHeadPosition(); pos != NULL; )
		{
			CString str = strList1.GetNext(pos);
			if (str.GetLength())
			{
				m_DotsList.AddTail(str);
		}
		}
		m_DotsAliasList.RemoveAll();
		for (pos = strList4.GetHeadPosition(); pos != NULL; )
		{
			CString str = strList4.GetNext(pos);
			if (str.GetLength())
			{
				m_DotsAliasList.AddTail(str);
			}
		}
		CopyDotsMap(&mapDots);
	}
	else
		ClearDotsMap(&mapDots);
	
//	strList0.AddHead("Items");
	
	strList3.AddHead(_T(" "));

	int nItems = strList1.GetCount() + 2;
	strList1.AddHead(_T(" "));
	if (g_multiLang.m_nLang == MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US))
	{
		strList6.AddHead(_T(" "));
		strList6.AddTail(&strList4);
		strList6.AddTail(&strList5);
	}
	else
	{
		strList6.AddTail(&strList1);
		strList6.AddTail(&strList2);
	}
	strList1.AddTail(&strList2);

	int  nCols = strList6.GetCount() + 1;
	int  nRows = strList0.GetCount() + 3;

	pCell->SetCols(nCols, 0);
	pCell->SetRows(nRows, 0);

	for (i = 2; i < nItems; i += 2)
		pCell->MergeCells(i, 1, i+1, 1);
	for (i = nItems; i < nCols; i++)
		pCell->MergeCells(i, 1, i, 2);

	COLORREF clrBk = globalData.clrBtnFace;
	clrBk = RGB(234,234,234);

	POSITION pos1 = strList6.GetHeadPosition();
	POSITION pos3 = strList3.GetHeadPosition();
	for (i = 1; i < nCols; i++)
	{
		pCell->SetColWidth(0, 200, i, 0);
		// pCell->SetCellBackColor(i, 1, 0, pCell->FindColorIndex(clrBk, 1));
		pCell->SetCellString(i, 1, 0, strList6.GetNext(pos1));
		POSITION pos = strList1.FindIndex(i - 1);
		pCell->SetCellNote(i, 1, 0, strList1.GetAt(pos));
		if (pos3 != NULL)
			pCell->SetCellString(i, 2, 0, strList3.GetNext(pos3));
	}

	POSITION pos = strList0.GetHeadPosition();
	pCell->MergeCells(1, 1, 1, 2);
	pCell->SetCellString(1, 1, 0, _T("Items"));
	pCell->SetRowHeight(0, 50, 1, 0);
	pCell->SetRowHeight(0, 50, 2, 0);
	for (i = 3; i < nRows; i++)
	{
		pCell->SetRowHeight(0, 50, i, 0);
		// pCell->SetCellBackColor(1, i, 0, pCell->FindColorIndex(clrBk, 1));
		if (pos != NULL)
			pCell->SetCellString(1, i, 0, strList0.GetNext(pos));
	}

	pCell->SelectRange(-1, -1, -1, -1);
	
	LOGFONT lf;
	globalData.fontBold.GetLogFont(&lf);
	long lPara = pCell->FindFontIndex(lf.lfFaceName, 1);
	for (i = 1; i < nCols; i++)
	{
		for (int j = 1; j < nRows; j++)
		{
			pCell->SetCellAlign(i, j, 0, 4 + 32);
			pCell->SetCellInput(i, j, 0, 5);
			pCell->SetCellFont(i, j, 0, lPara);
			pCell->SetCellFontSize(i, j, 0, 12);
		}
	}

	COLORREF clrLine = RGB(128,128,128);
	pCell->DrawGridLine(1, 1, nCols-1, nRows-1, 3, 2, pCell->FindColorIndex(clrLine, 1));
	pCell->DrawGridLine(1, 1, nCols-1, nRows-1, 5, 2, pCell->FindColorIndex(clrLine, 1));

	if (m_uCalPhase == uPhase)
	{
		COLORREF clrCP = RGB(187,224,227); // RGB(238,255,238);
		long lColor = pCell->FindColorIndex(clrCP, 1);
		for (i = 1; i < nCols; i++)
		{
			for (int j = 1; j < nRows; j++)
			{
				pCell->SetCellBackColor(i, j, 0, lColor);
			}
		}
	}

	UpdateCalCell(pCell, uPhase, TRUE, nGroup);
}
//***************************************************************************
void CULTool::EnableCalCell(CCell2000* pCell, UINT uPhase, long para, UINT nGroup/* = 0*/)
{
	int nX = 0;
	for (int i = 0; i < m_CalCurves[uPhase].GetSize(); i++)
	{
		CULCalCurve* pCur = (CULCalCurve*)m_CalCurves[uPhase].GetAt(i);
		if (pCur == NULL)
			continue;

		if (pCur->m_nCalGroup != nGroup && (uPhase == m_uCalPhase))
			continue;
		
		if (pCur->m_CalCSs.GetSize() < 1)
			continue;
		
		CULCalCurveSeg* pCS = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(0);
		for (int j = 0; j < pCS->m_CalDotList.GetSize(); j++)
		{
			CULCalDot* pDot = (CULCalDot*)pCS->m_CalDotList.GetAt(j);
			if (pDot == NULL)
				continue ;
			
			//if (uPhase == UL_CAL_MASTER)
				pCell->SetCellInput(pDot->m_nX, pDot->m_nY, 0, para);
			pCell->SetCellInput(pDot->m_nX + 1, pDot->m_nY, 0, para);
			if (pDot->m_nX > nX)
				nX = pDot->m_nX;
		}
	}

	// if (uPhase == UL_CAL_MASTER)
	{
		long nCols = pCell->GetCols(0);
		long nRows = pCell->GetRows(0);
		for (long col = nX + 2; col < nCols; col++)
		{
			for (long row = 2; row < nRows; row++)
			{
				CString strVal = pCell->GetCellString(col, row, 0);
				if (strVal == _T(" -- "))
					pCell->SetCellInput(col, row, 0, 5);
				else
					pCell->SetCellInput(col, row, 0, para);
			}
		}
	}
}

// ------------------------------------------
//	Set the calibrate coefficients to cell :
// ------------------------------------------

void CULTool::FillCalResult(CCell2000* pCell, UINT uPhase, long col, long row, UINT nGroup/* = 0*/)
{
	int nCols = pCell->GetCols(0);
	int nRow = row + 2;

	for (int i = 0; i < m_CalCurves[uPhase].GetSize(); i++)
	{
		CULCalCurve* pCur = (CULCalCurve*)m_CalCurves[uPhase].GetAt(i);
		if (pCur == NULL)
			continue ;
		
		if (pCur->m_nCalGroup != nGroup && (uPhase == m_uCalPhase))
			continue;
		
		if (pCur->m_CalCSs.GetSize() < 1)
			continue ;
		
		CULCalCurveSeg* pSeg = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(0);
		for (int j = col; j < nCols; j++)
		{
			CString strCoef = pCell->GetCellNote(j, row, 0);

			ULCalCoef cc;
			CString strVal = _T(" -- ");
			if (pSeg->GetCoef(strCoef, cc) > -1)
			{
				pCell->SetCellNumType(j, nRow, 0, 1);
				pCell->SetCellDigital(j, nRow, 0, 4);
				// add by bao 刻度系数添加单位
				if (!cc.szUnit.IsEmpty())
				{
					CString strCoefUnit = "";
					strCoefUnit.Format("%.2f%s%s%s", cc.dValue, "(", cc.szUnit, ")");
					pCell->SetCellString(j, nRow, 0, strCoefUnit);
				} 
				else
				{
					pCell->SetCellDouble(j, nRow, 0, cc.dValue);
				}
				// End
//				pCell->SetCellDouble(j, nRow, 0, cc.dValue);
				
				if (cc.dRMin == cc.dRMax)
				{
					long para = pCell->FindColorIndex(RGB(0, 0, 0), 1);
					pCell->SetCellTextColor(j, nRow, 0, para);
				}
				else
				{
					if (cc.dValue < cc.dRMin || cc.dRMax < cc.dValue)
					{
						long para = pCell->FindColorIndex(RGB(255, 0, 0), 1);
						pCell->SetCellTextColor(j, nRow, 0, para);
					}
					else//lxc
					{
						long para = pCell->FindColorIndex(RGB(0, 0, 0), 1);
						pCell->SetCellTextColor(j, nRow, 0, para);
					}
				}
			}
			else
				pCell->SetCellString(j, nRow, 0, strVal);
		}

		nRow++;
	}
}

// ---------------------------------------------
//	Get the calibrate coefficients from cell :
// ---------------------------------------------

void CULTool::SuckCalResult(CCell2000* pCell, UINT uPhase, long col, long row, UINT nGroup/* = 0*/)
{
	int nCols = pCell->GetCols(0);
	int nRow = row + 2;
	
	for (int i = 0; i < m_CalCurves[uPhase].GetSize(); i++)
	{
		CULCalCurve* pCur = (CULCalCurve*)m_CalCurves[uPhase].GetAt(i);
		if (pCur == NULL)
			continue ;

		if (pCur->m_nCalGroup != nGroup && (uPhase == m_uCalPhase))
			continue;
		
		if (pCur->m_CalCSs.GetSize() < 1)
			continue ;
		
		CULCalCurveSeg* pSeg = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(0);
		for (int j = col; j < nCols; j++)
		{
			CString strCoef = pCell->GetCellNote(j, row, 0);
			ULCalCoef cc;
			CString strVal = _T(" -- ");
			int nCoef = pSeg->GetCoef(strCoef, cc);
			if (nCoef < 0)
				continue ;
			
			// add by bao 为刻度系数添加单位
			CString strCoefUnit = "";
			strCoefUnit = pCell->GetCellString2(j, nRow, 0);
			if (strCoefUnit.Find('(') != -1)
			{
				strCoefUnit = strCoefUnit.Left(strCoefUnit.Find('('));
				cc.dValue = atof(strCoefUnit);
			} 
			else
			{
				cc.dValue = pCell->GetCellDouble2(j, nRow, 0);
			}
			if (!cc.szUnit.IsEmpty())
			{
				CString strCoefUnit = "";
				strCoefUnit.Format("%.2f%s%s%s", cc.dValue, "(", cc.szUnit, ")");
				pCell->SetCellString(j, nRow, 0, strCoefUnit);
			} 
			else
			{
				pCell->SetCellDouble(j, nRow, 0, cc.dValue);
			}
			// End
		//	cc.dValue = pCell->GetCellDouble2(j, nRow, 0);
			if (cc.dRMin == cc.dRMax)
			{
				long para = pCell->FindColorIndex(RGB(0, 0, 0), 1);
				pCell->SetCellTextColor(j, nRow, 0, para);
			}
			else
			{
				if (cc.dValue < cc.dRMin || cc.dRMax < cc.dValue)
				{
					long para = pCell->FindColorIndex(RGB(255, 0, 0), 1);
					pCell->SetCellTextColor(j, nRow, 0, para);
				}
				else//lxc
				{
					long para = pCell->FindColorIndex(RGB(0, 0, 0), 1);
					pCell->SetCellTextColor(j, nRow, 0, para);
				}
			}
			pSeg->m_CalCoefs.SetAt(nCoef, cc);
		}
		
		nRow++;
	}
}

// ---------------------------------
//	Update all data of the cell :
// ---------------------------------
//  刻度结束时 (CalibrateIO停止时), 以及点计算按钮、放弃按钮时，刷新测量值、工程值、刻度系数
//  
void CULTool::UpdateCalCell(CCell2000* pCell, UINT uPhase, BOOL bSaveAndValidate /* = TRUE */, UINT nGroup/* = 0*/)
{
	if (bSaveAndValidate)
	{
		CStringList strList1;
		for (int i = 0; i < m_CalCurves[uPhase].GetSize(); i++)
		{
			CULCalCurve* pCur = (CULCalCurve*)m_CalCurves[uPhase].GetAt(i);
			if (pCur == NULL)
				continue ;

			if (pCur->m_nCalGroup != nGroup && (uPhase == m_uCalPhase))
				continue;
			
			if (pCur->m_CalCSs.GetSize() < 1)
				continue ;
			
			CULCalCurveSeg* pSeg = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(0);
			for (int j = 0; j < pSeg->m_CalDotList.GetSize(); j++)
			{
				CULCalDot* pDot = (CULCalDot*)pSeg->m_CalDotList.GetAt(j);
				if (pDot == NULL)
					continue ;

				if (!strList1.Find(pDot->m_strName))
					strList1.AddTail(pDot->m_strName);					
				
				pCell->SetCellNumType(pDot->m_nX, pDot->m_nY, 0, 1);
				pCell->SetCellDigital(pDot->m_nX, pDot->m_nY, 0, 2);
				pCell->SetCellDouble(pDot->m_nX, pDot->m_nY, 0, pDot->m_fMeas);

				COLORREF clrText = pDot->IsExceed() ? RGB(255, 0, 0) : 0;
				long lPara = pCell->FindColorIndex(clrText, 1);
				pCell->SetCellTextColor(pDot->m_nX, pDot->m_nY, 0, lPara);

				pCell->SetCellNumType(pDot->m_nX + 1, pDot->m_nY, 0, 1);
				pCell->SetCellDigital(pDot->m_nX + 1, pDot->m_nY, 0, 2);
				// add by bao 显示刻度单位 
				if (!pDot->m_strUnit.IsEmpty())
				{
					CString strDotUnit = "";
					strDotUnit.Format("%.2f%s%s%s", pDot->m_fEng, "(", pDot->m_strUnit, ")");
					pCell->SetCellString(pDot->m_nX+1, pDot->m_nY, 0, strDotUnit);
				}
				else
				{
					pCell->SetCellDouble(pDot->m_nX+1, pDot->m_nY, 0, pDot->m_fEng);
				}
				// End
			//	pCell->SetCellDouble(pDot->m_nX+1, pDot->m_nY, 0, pDot->m_fEng);
			}
		}
		
		FillCalResult(pCell, uPhase, 2*(strList1.GetCount() + 1), 1, nGroup);
	}
	else
	{
		pCell->SelectRange(-1, -1, -1, -1);
		
		CStringList strList1;
		for (int i = 0; i < m_CalCurves[uPhase].GetSize(); i++)
		{
			CULCalCurve* pCur = (CULCalCurve*)m_CalCurves[uPhase].GetAt(i);
			if (pCur == NULL)
				continue ;

			if (pCur->m_nCalGroup != nGroup && (uPhase == m_uCalPhase))
				continue;
			
			if (pCur->m_CalCSs.GetSize() < 1)
				continue ;
			
			CULCalCurveSeg* pSeg = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(0);
			for (int j = 0; j < pSeg->m_CalDotList.GetSize(); j++)
			{
				CULCalDot* pDot = (CULCalDot*)pSeg->m_CalDotList.GetAt(j);
				if (pDot == NULL)
					continue ;
				
				if (!strList1.Find(pDot->m_strName))
					strList1.AddTail(pDot->m_strName);					
				
				CString strVal = pCell->GetCellString2(pDot->m_nX, pDot->m_nY, 0);
				if (strVal.GetLength())
				{
					pDot->m_fMeas = pCell->GetCellDouble2(pDot->m_nX, pDot->m_nY, 0);
				}
				else
				{
					pCell->SetCellString(pDot->m_nX, pDot->m_nY, 0, "0.00");
					pDot->m_fMeas = 0;
				}

				COLORREF clrText = pDot->IsExceed() ? RGB(255, 0, 0) : 0;
				long lPara = pCell->FindColorIndex(clrText, 1);
				pCell->SetCellTextColor(pDot->m_nX, pDot->m_nY, 0, lPara);

				strVal = pCell->GetCellString2(pDot->m_nX+1, pDot->m_nY, 0);
				if (strVal.GetLength())
				{
					// add by bao 显示刻度单位
					if (strVal.Find('(') != -1)
					{
						strVal = strVal.Left(strVal.Find('('));
						pDot->m_fEng = atof(strVal);
					} 
					else
					{
						pDot->m_fEng = pCell->GetCellDouble2(pDot->m_nX+1, pDot->m_nY, 0);
					} 
					if (!pDot->m_strUnit.IsEmpty())
					{
						CString strDotUnit = "";
						strDotUnit.Format("%.2f%s%s%s", pDot->m_fEng, "(", pDot->m_strUnit, ")");
						pCell->SetCellString(pDot->m_nX+1, pDot->m_nY, 0, strDotUnit);
					}
					// End
//					pDot->m_fEng = pCell->GetCellDouble2(pDot->m_nX+1, pDot->m_nY, 0);
				}
				else
				{
					pCell->SetCellString(pDot->m_nX+1, pDot->m_nY, 0, "0.00");
					pDot->m_fEng = 0;
				}				
			}
		}
		
		SuckCalResult(pCell, uPhase, 2*(strList1.GetCount() + 1), 1, nGroup);
	}
}

BOOL CULTool::OnCalNew()
{
	CString strNewFile = GetToolDllPath();
	if (strNewFile.IsEmpty())
		return FALSE;
	
	if (ReadCalInfo(strNewFile + strCalPhase[m_uCalPhase] + "New.ulc"))
		return FALSE;

	return TRUE;
}

// 刻度界面上， 按"加载"按钮
BOOL CULTool::OnCalLoad()
{
#ifdef _PERFORATION
	switch (GetCalType())
	{
	case UL_CAL_DEFAULT:
	case UL_CAL_ASST:
	default:
		{
			CString strFileName = _T("");

			CString strDllPath = GetToolDllPath();
			if (m_strCalFilePath.GetLength())
			{
				int iFind = m_strCalFilePath.ReverseFind('\\');
				if (iFind > 0)
					strFileName = m_strCalFilePath.Left(iFind + 1);
			}
			else
			{
				if (strDllPath.GetLength())
					strFileName = strDllPath;
			}
			
			CString strFile = strCalPhase[m_uCalPhase];
			strFileName += strFile;

			CString strSN = SN();
			if (strDllPath.GetLength() && strSN.GetLength())
				strDllPath += ("\\" + strSN);
			
			strFile += ".ulc";
			CCalFileDialog fd(this, "ulc", strFileName, OFN_HIDEREADONLY|OFN_EXPLORER, ULCFilter);
			if (fd.DoModal() != IDOK)
				return FALSE;
			
			CString strPath;
			if (fd.m_nPage == CCalFileDialog::CalFileOpen)
				strPath = fd.m_strCalPathName;
			else
				strPath = fd.GetPathName();
			if (ReadCalInfo(strPath))
			{
				//AfxMessageBox(IDS_ERR_NOMATCH);
				AEI(IDS_TOOL_CAL, theApp.m_pszAppName, ET_CAL|ES_ERROR, IDS_CAL_LOAD,
					IDS_CAL_LOAD_FAILED, strPath, m_lCurDepth, strToolName);

				return FALSE;
			}
			
			m_strCalFilePath = strPath;
			m_CalFilePathArray[m_uCalPhase] = strPath;
			AEI(IDS_TOOL_CAL, theApp.m_pszAppName, ET_CAL, IDS_CAL_LOAD,
				IDS_CAL_LOAD_RESULT, strPath, m_lCurDepth, strToolName);
		}
		break;
	case UL_CAL_USER:
		{
			CString strFile = m_CalFilePathArray[m_uCalPhase];

			// add by bao 2013/9/6 路径中含有空格路径时加载对话框不弹出问题
			CString strTemp = strFile;
			while ((strTemp.ReverseFind('\\') != -1))
			{
				CString strTemp2 = strTemp.Right(strTemp.GetLength() - strTemp.ReverseFind('\\') - 1);
				strTemp2.TrimRight();strTemp2.TrimLeft();
				if (strTemp2.IsEmpty())
				{
					strFile = strCalPhase[m_uCalPhase];
					break;
				}
				strTemp = strTemp.Left(strTemp.ReverseFind('\\'));
			}

			// add by bao 2013/7/22 自带原本就已经包含有后缀名 4带表.udc
			if ((strFile.ReverseFind('.') == -1) || (strFile.ReverseFind('.') != (strFile.GetLength() - 4)))
			{
				strFile += _T(".udc");
			}
			
			//strFile += _T(".udc");
			TCHAR ULCFilter[] = _T("Calibreate User File (.udc)|*.udc||");
			CFileDialog fd(TRUE, _T("udc"), strFile , OFN_HIDEREADONLY|OFN_EXPLORER, ULCFilter);
			if (fd.DoModal() != IDOK)
			{
				return FALSE;
			}
			
			CString strPath = fd.GetPathName();

			CFile file;
			if (file.Open(strPath, CFile::modeRead))
			{
				if (!ReadCalFile(&file))
				{
					DWORD dwLen = 0; 
					file.Read(&dwLen, sizeof(DWORD));
					m_pITool->CalUserLoad(&file, strCalPhase[m_uCalPhase].GetBuffer(0));
					strCalPhase[m_uCalPhase].ReleaseBuffer();
				}
				file.Close();

				m_CalFilePathArray[m_uCalPhase] = strPath;
			}
		}
		break;
	case UL_CAL_INDEPENDENT:
		break;
	}

	if (m_pIParam)
		m_pIParam->OnSaveParam();
#endif	
	return TRUE;
}
//***************************************************************************
UINT CULTool::GetCalDotsTime(LPCTSTR pszItem)
{
	CPtrList* pDotList = NULL;
	m_mapDots.Lookup(pszItem, (void*&)pDotList);

	m_CalDotList.RemoveAll();
	UINT nTime = 0;
	if (pDotList != NULL)
	{
		POSITION pos = pDotList->GetHeadPosition();
		for ( ; pos != NULL; )
		{
			CULCalDot* pDot = (CULCalDot*)pDotList->GetNext(pos);
			m_CalDotList.Add(pDot);
			if (nTime < pDot->m_iDestTm)
				nTime = pDot->m_iDestTm;
		}
	}

	return nTime;
}
//***************************************************************************
int CULTool::GetCharts(LPCTSTR pszText, CStringList* pList, CStringList* pT)
{
	pT->RemoveAll();
	CString strText = "_0.00_";
	strText += pszText;
	int nLen = strText.GetLength();
	for (POSITION pos = pList->GetHeadPosition(); pos != NULL;)
	{
		CString strChart = pList->GetNext(pos);
		if (strChart.Right(nLen) == strText)
		{
			pT->AddTail(strChart);
		}
	}
	
	return pT->GetCount();
}
//***************************************************************************
BOOL CULTool::GetChart(CString& strChart, LPCTSTR pszText, CStringList* pList, POSITION& pos)
{
	CString strText = "_0.00_";
	strText += pszText;
	int nLen = strText.GetLength();
	for (; pos != NULL;)
	{
		strChart = pList->GetNext(pos);
		if (strChart.Right(nLen) == strText)
		{
			return TRUE;
		}
	}

	return FALSE;
}
//***************************************************************************
void CULTool::FillChart(CCell2000* pCell, LPCTSTR pszText, CStringList* pMap, long& col, long& row,
						 long colE, LPCTSTR pszFormat, int nIndex, long para, DWORD dwPhases, int nPhase)
{
	pCell->ShowGridLine(0, 0);
	CString strPhase[] = { _T(""), _T("M_"), _T("B_"), _T("M.B_"), _T("A_"), _T("M.A_"), _T("B.A_"), _T("M.B.A_"),
	_T("P_"), _T("M.P_"), _T("P.B_"), _T("M.P.B_"), _T("P.A_"), _T("M.P.A_"), _T("P.B.A_"), _T("M.P.B.A_") };

	CString strValue;
	int nCols = colE + 1;
	
	CString strCell = pszText;
	if (pMap->GetCount() == 0)
		return;

	if (row > 1)
	{
		pCell->SetRowHeight(0, cth, row, 0);
		
		long colme = 3*colx + 2;
		pCell->MergeCells(1, row, colme, row);
		pCell->DrawGridLine(1, row, colme, row, 2, 1, 0);
		pCell->DrawGridLine(1, row, colme, row, 3, 1, 0);
		pCell->DrawGridLine(1, row, colme, row, 5, 1, 0);
		row ++;
	}
	
	CString strT = _T("Wellsite");
	switch (dwPhases)
	{
	case CP_MASTER:
		strT = _T("Master");
		break;
	case CP_BEFROR:
		strT = _T("Before");
		break;
	case CP_AFTER:
		strT = _T("After");
		break;
	case CP_PRIMARY:
		strT = _T("Primary");
		break;
	}
	
	colE = pMap->GetCount();
	if (colE > 3)
		colE = 3;
	
	colE = colE * colx + 1;
	if (colE > 3 * colx)
		colE++;    // 合并最末尾一列

	long row0 = row;
	//strValue.Format("%s %s Calibration", strENName, strT);
	strValue.Format("%s Calibration", strT);
	pCell->SetRowHeight(0, cth, row, 0);
	pCell->SetCellAlign(1, row, 0, 4);
	pCell->SetCellFont(1, row, 0, para);
	pCell->SetCellFontSize(1, row, 0, 11);
	pCell->SetCellFontStyle(1, row, 0, 2);
	pCell->SetCellString(1, row, 0, strValue);
	if (col < colE)
		pCell->MergeCells(1, row, colE, row);
	strValue.Format(pszFormat, strCell);
	row++;
	pCell->SetRowHeight(0, cth, row, 0);
	pCell->SetCellAlign(1, row, 0, 4);
	pCell->SetCellFont(1, row, 0, para);
	pCell->SetCellFontSize(1, row, 0, 11);
	pCell->SetCellFontStyle(1, row, 0, 2);
	pCell->SetCellString(1, row, 0, strValue);
	if (col < colE)
		pCell->MergeCells(1, row, colE, row);
	
	col = nCols - 1;
	int nLines = nPhase + 2;
	POSITION pos = pMap->GetHeadPosition();
	for ( ; pos != NULL; col += colx)
	{
		if (col >= colE)
		{
			col = 2;
			row ++;
			
			if (colx == 6)
			{
				long rowx0 = row + 1;
				pCell->InsertRow(rowx0, nLines, 0);
				long rowxE = rowx0 + nLines;
				for (int x = rowx0; x < rowxE; x++)
					pCell->SetRowHeight(0, 0, x, 0);
				
				if (row != (row0 + 2))
					row += nLines;
			}
			
			pCell->SetRowHeight(0, cgh* nLines, row, 0);
		}
		
		if (colx > 1)
			pCell->MergeCells(col, row, col + colx - 1, row);
		strValue = pMap->GetNext(pos);
		strValue.Insert(nIndex, strPhase[dwPhases]);
		CString strText = "<%";
		strText += strValue;
		strText += "%>";
		pCell->SetCellString(col, row, 0, strText);
		// strText += "@1";
		pCell->SetCellNote(col, row, 0, strText);
	}
	
	if (col < colE)
	{
		long rowme = row;
		if (colx == 6)
			rowme += nLines;
		pCell->MergeCells(col, row, colE, rowme);
	}
	
	row ++;
	if (colx == 6)
		row += nLines;
	pCell->SetRowHeight(0, cth, row, 0);
	int iPhase = 0;
	long row1 = row;
	for (col = 2; iPhase < 4; iPhase ++)
	{	
		nPhase = nPhaseTime[iPhase];
		if (dwPhases & dwCalPhase[nPhase])
		{
			if (col >= colE)
			{
				col = 2;
				row ++;
				pCell->SetRowHeight(0, cth, row, 0);
			}
			
			if (colx > 1)
			{
				int n = col + colx - 1;
				if (n > colx * 3)
					n++;
				pCell->MergeCells(col, row, n, row);
			}
			strValue = strCalPhase[nPhase];
			strValue += ": ";
			COLORREF clr = GetCalTimeColor(m_tmCaled[nPhase]);
			if (clr == clrTm[4])
				strValue += "Calibration not done";
//			else if (clr == clrTm[3])
// 				strValue += "Calibration out of date";
			else
				strValue += m_tmCaled[nPhase].Format(TM_FORMAT);
			
			pCell->SetCellFont(col, row, 0, para);
			pCell->SetCellFontSize(col, row, 0, 11);
			pCell->SetCellFontStyle(col, row, 0, 2);
			pCell->SetCellString(col, row, 0, strValue);
			col += colx;
		}
	}
	
	// Merge times
	if (col < colE)
		pCell->MergeCells(col, row, colE, row);
	
	// Merge right
	if (row0 != row)
	{
		if (colE < (3*colx - 1))
			pCell->MergeCells(colE + 1, row0, nCols - 1, row);
	}
	
	// Merge title
	if (colE != 1)
	{
		pCell->MergeCells(1, row0, colE, row0);
		pCell->MergeCells(1, (row0 + 1), colE, (row0 + 1));
	}
	
	// Erase lines
	pCell->DrawGridLine(1, row0, nCols - 1, row, 0, 1, 0);
	
	// Draw lines
	pCell->DrawGridLine(1, row0, colE, row, 0, 3, 0);
	
	// Erase lines between times
	if (colE != colx)
		pCell->DrawGridLine(1, row1, colE - colx, row, 3, 1, 0);
	
	col = 2;
	row ++;
}

void CULTool::FillChart(CCell2000* pCell, CStringList* pList, CStringList* pText, long& col, long& row,
						long colE, LPCTSTR pszFormat, int nIndex, long para, DWORD dwPhases)
{
	if (pText->GetCount() < 1)
		return ;

	pCell->ShowGridLine(0, 0);
	CString strPhase[] = { _T(""), _T("M_"), _T("B_"), _T("M.B_"), _T("A_"), _T("M.A_"), _T("B.A_"), _T("M.B.A_"),
	_T("P_"), _T("M.P_"), _T("B.P_"), _T("M.B.P_"), _T("A.P_"), _T("M.A.P_"), _T("B.A.P_"), _T("M.B.A.P_") };

	CString strValue;
	int nCols = colE + 1;

	// 空行
	if (row > 1)
	{
		pCell->SetRowHeight(0, cth, row, 0);
// 		if (col < colE)
// 			pCell->MergeCells(col, row, colE, row);
		
		long colme = 3*colx + 2;
		pCell->MergeCells(1, row, colme, row);
		pCell->DrawGridLine(1, row, colme, row, 2, 1, 0);
		pCell->DrawGridLine(1, row, colme, row, 3, 1, 0);
		pCell->DrawGridLine(1, row, colme, row, 5, 1, 0);
		row ++;
	}

	// 标题
	CString strT = "Wellsite";
	int nLines = 1;
	switch (dwPhases)
	{
	case 0:
		nLines = 0;
		break;
	case CP_MASTER:
		strT = _T("Master");
		break;
	case CP_BEFROR:
		strT = _T("Before");
		break;
	case CP_AFTER:
		strT = _T("After");
		break;
	case CP_PRIMARY:
		strT = _T("Primary");
		break;
	case (CP_MASTER|CP_BEFROR|CP_AFTER):
	case (CP_MASTER|CP_BEFROR|CP_PRIMARY):
	case (CP_MASTER|CP_AFTER|CP_PRIMARY):
	case (CP_BEFROR|CP_AFTER|CP_PRIMARY):
		nLines = 3;
		break;
	case (CP_MASTER|CP_BEFROR|CP_AFTER|CP_PRIMARY):
		nLines = 4;
		break;
	default:
		nLines = 2;
	}
	nLines += 3;

	colE = pText->GetCount();
	if (colE > 3)
		colE = 3;
	
	colE = colE * colx + 1;
	if (colE > 3 * colx)
		colE++;    // 合并最末尾一列
	
	long row0 = row;
	//strValue.Format("%s %s Calibration", strENName, strT);
	strValue.Format("%s Calibration", strT);
	pCell->SetRowHeight(0, cth, row, 0);
	pCell->SetCellAlign(1, row, 0, 4);
	pCell->SetCellFont(1, row, 0, para);
	pCell->SetCellFontSize(1, row, 0, 11);
	pCell->SetCellFontStyle(1, row, 0, 2);
	pCell->SetCellString(1, row, 0, strValue);
	if (col < colE)
		pCell->MergeCells(1, row, colE, row);

	// 正文
	int nCount = 0;
	long colme = nCols - 1;		// 上一系数结束列
	long row1 = row++;		// 上一系数标题行
	long rowS = row;
	CString strTitle;
	for (POSITION pos = pList->GetHeadPosition(); pos != NULL; )
	{
		CString strCell = pList->GetNext(pos);
		POSITION posT = pText->GetHeadPosition();
		if (posT == NULL)
			continue;
		
		strTitle.Format(pszFormat, strCell);
		col = colme; // 从上一系数结束列开始
		for ( ; GetChart(strValue, strCell, pText, posT); col += colx)
		{
			// 换行
			if (col >= colE)
			{
				col = 2;
				if (row != (row0 + 1))
				{
					row += nLines;	
				}
				
				row ++;
				row1 = row - 1;
				pCell->SetRowHeight(0, cgh* nLines, row, 0);
			}

			// 设置系数小标题
			pCell->SetCellString(1, row1, 0, strTitle);
			pCell->SetCellAlign(1, row1, 0, 4);
			pCell->SetCellFont(1, row1, 0, para);
			pCell->SetCellFontSize(1, row1, 0, 11);
			pCell->SetCellFontStyle(1, row1, 0, 2);
			
			if (colx > 1)
				pCell->MergeCells(col, row, col + colx - 1, row);
			strValue.Insert(nIndex, strPhase[dwPhases]);
			CString strText = "<%";
			strText += strValue;
			strText += "%>";
			pCell->SetCellString(col, row, 0, strText);
			// strText += "@1";
			pCell->SetCellNote(col, row, 0, strText);
		}

		colme = col;
	}

	// 标题
	int i = 0;
	for (long r = rowS; r <= row1; r+= (nLines + 1))
	{
		strTitle.Empty();
		pCell->SetRowHeight(0, cth, r, 0);
		long colS = 1;
		CString string;
		for (long c = 2; c < colE; c+= colx)
		{
			pCell->SetCellAlign(c, r, 0, 4);
			pCell->SetCellFont(c, r, 0, para);
			pCell->SetCellFontSize(c, r, 0, 11);
			pCell->SetCellFontStyle(c, r, 0, 2);
			string = pCell->GetCellString(c, r, 0);
			if (strTitle.GetLength())
			{
				if (string != strTitle)
				{
					int n = c - 1;
					if (n > 3 * colx)
						n++;
					pCell->MergeCells(colS, r, n, r);
					colS = c;
				}
			}
			else
			{
				strTitle = string;
			}
		}
		int n = c - 1;
		if (n > 3 * colx)
			n++;
		pCell->MergeCells(colS, r, n, r);
	}

	if (col < colE)
	{
		long rowme = row1;
		if (colx == 6)
			rowme += nLines;

		pCell->MergeCells(col, row1, colE, rowme);
	}

	// 时间
	if (colx == 6)
		row += nLines;
	pCell->SetRowHeight(0, cth, row, 0);

	
	long row2 = row;
	int iPhase = 0;
	for (col = 2; iPhase < 4; iPhase ++)
	{
		UINT nPhase = nPhaseTime[iPhase];
		if (dwPhases & dwCalPhase[nPhase])
		{
			if (col >= colE)
			{
				col = 2;
				row ++;
				pCell->SetRowHeight(0, cth, row, 0);
			}
			
			if (colx > 1)
			{
				int n = col + colx - 1;
				if (n > colx * 3)
					n++;
				pCell->MergeCells(col, row, n, row);
			}
			strValue = strCalPhase[nPhase];
			strValue += ": ";
			COLORREF clr = GetCalTimeColor(m_tmCaled[nPhase]);
			if (clr == clrTm[4])
				strValue += "Calibration not done";
// 			else if (clr == clrTm[3])
// 				strValue += "Calibration out of date";
			else
				strValue += m_tmCaled[nPhase].Format(TM_FORMAT);
			
			pCell->SetCellFont(col, row, 0, para);
			pCell->SetCellFontSize(col, row, 0, 11);
			pCell->SetCellFontStyle(col, row, 0, 2);
			pCell->SetCellString(col, row, 0, strValue);
			col += colx;
		}
	}
	
	// Merge times
	if (col < colE)
		pCell->MergeCells(col, row, colE, row);
	
	// Merge right
	if (row0 != row)
	{
		if (colE < (3*colx - 1))
			pCell->MergeCells(colE + 1, row0, nCols - 1, row);
	}
	
	
	// Erase lines
	pCell->DrawGridLine(1, row0, nCols - 1, row, 0, 1, 0);
	
	// Draw lines
	pCell->DrawGridLine(1, row0, colE, row, 0, 3, 0);
	
	// Erase lines between times
	if (colE != colx)
		pCell->DrawGridLine(1, row2, colE - colx, row, 3, 1, 0);
	
	col = 2;
	row ++;
}
//***************************************************************************
// 生成刻度图表报告
void CULTool::AddCalChart(CCell2000* pCell, DWORD dwPhases /* = 15 */)
{
	long nCols = 3 + 3*colx; // 总列数 0列是图头 1列 空白 2-20列 图表  共21列

	// Set cols count
	if (nCols != pCell->GetCols(0))
		pCell->SetCols(nCols, 0);

	pCell->SetColWidth(0, 10, 1, 0);
	pCell->SetColWidth(0, 10, nCols - 1, 0);
	
	CString strSybs[] = { _T("CAL[E]::"), _T("CAL[R]::"), _T("CAL[F]::") };
	int nInsert[] = { 0, 0, 0 };
	
	CStringList  lstDot[2];
	CStringList  lstCoef;
	CStringArray strDot[2];
	CStringArray strCoef;
	CUIntArray dwDot[2];
	CUIntArray dwCoef;
	
	for (int m = 0; m < 3; m++)
	{
		strSybs[m] += strToolName + "_";
		nInsert[m] = strSybs[m].GetLength();
		for (int n = 0; n < 4; n++)
		{
			if ((dwPhases & dwCalPhase[n]) == 0)
				continue;

			if (m_tmCaled[n].GetTime() == tmNew)
				continue;

			for (int c = 0; c < m_CalCurves[n].GetSize(); c++)
			{
				CULCalCurve* pCur = (CULCalCurve*)m_CalCurves[n].GetAt(c);
				if (pCur == NULL)
					continue;
				
				if (pCur->m_CalCSs.GetSize() == 0)
					continue;

				int s = 0;
				CULCalCurveSeg* pCS = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(s);
				if (pCS == NULL)
					continue;
					
				CString strCS = "0.00";
				// strCS.Format("%.2lf", pCS->m_fValue);
					
				CString strPre = strSybs[m] + pCur->m_strName + "_" + strCS + "_";
				if (m == 2)
				{
					for (int f = 0; f < pCS->m_CalCoefs.GetSize(); f++)
					{
						ULCalCoef cc = pCS->m_CalCoefs.GetAt(f);
						if (cc.dRMin == cc.dRMax)
							continue;

						if (!lstCoef.Find(cc.szAlias))
							lstCoef.AddTail(cc.szAlias);

						CString str = strPre + cc.szAlias;
						int nCount = strCoef.GetSize();
						for (int x = 0; x < nCount; x++)
						{
							if (strCoef.GetAt(x) == str)
								break;
						}

						if (x < nCount)
						{
							dwCoef[x] |= dwCalPhase[n];
						}
						else
						{
							strCoef.Add(str);
							dwCoef.Add(dwCalPhase[n]);
						}
					}						
					continue;
				}

				for (int d = 0; d < pCS->m_CalDotList.GetSize(); d++)
				{
					CULCalDot* pDot = (CULCalDot*)pCS->m_CalDotList.GetAt(d);

					if (m == 1)
					{
						if (pDot->m_fMinStand == pDot->m_fMaxStand)
							continue;
					}
					else
					{
						if (pDot->m_fUserMin == pDot->m_fUserMax)
							continue;
					}

					if (!lstDot[m].Find(pDot->m_strAlias))
						lstDot[m].AddTail(pDot->m_strAlias);

					CString str = strPre + pDot->m_strAlias;
					int nCount = strDot[m].GetSize();
					for (int x = 0; x < nCount; x++)
					{
						if (strDot[m].GetAt(x) == str)
							break;
					}

					if (x < nCount)
					{
						dwDot[m][x] |= dwCalPhase[n];
					}
					else
					{
						strDot[m].Add(str);
						dwDot[m].Add(dwCalPhase[n]);
					}
				}
			}
		}
	}

	int nPhasex[] = { 0, 1, 1, 2, 1, 2, 2, 3,
					  1, 2, 2, 3, 2, 3, 3, 4};

	CStringList strDots[2][16];
	int i;
	for (m = 0; m < 2; m++)
	{
		for (i = 0; i < dwDot[m].GetSize(); i++)
		{
			strDots[m][dwDot[m][i]].AddTail(strDot[m][i]);
		}
	}

	int nRows = 0;
	POSITION pos = NULL;
	
	for (m = 0; m < 2; m++)
	{
		for (pos = lstDot[m].GetHeadPosition(); pos != NULL; )
		{
			CString strDotx = lstDot[m].GetNext(pos);
			CStringList strDotsx;
			for (i = 1; i < 16; i++)
			{
				int nCount = GetCharts(strDotx, &strDots[m][i], &strDotsx);
				if (nCount == 0)
					continue;
				
				nRows += ((nCount - 1)/3);
				if (nCount == 1)
					nRows += (nPhasex[i] - 1);
				else if (nCount == 2)
				{
					if (nPhasex[i] > 2)
						nRows++;
				}
				else
				{
					if (nPhasex[i] > 3)
						nRows++;
				}
				nRows += 5;
			}
		}
	}
	
	CStringList strCoefs[16];
	for (i = 0; i < dwCoef.GetSize(); i++)
	{
		strCoefs[dwCoef[i]].AddTail(strCoef[i]);
	}

	for (i = 1; i < 16; i++)
	{
		int nCount = strCoefs[i].GetCount();
		if (nCount == 0)
			continue;

		int nAR = (nCount + 2)/3;
		nRows += (nPhasex[i] + 4)*nAR;
		if (nCount == 1)
			nRows += (nPhasex[i] - 1);
		else if (nCount == 2)
		{
			if (nPhasex[i] > 2)
				nRows++;
		}
		else
		{
			if (nPhasex[i] > 3)
				nRows++;
		}
		nRows += 3;
	}

	// ******Calculate lines of title*******
	if (nRows > 0)
		nRows += 4;
	
	BOOL bDescs = FALSE;
	for (i = 0; i < 4; i++)
	{
		if (dwPhases & dwCalPhase[i])
		{
			int nDescs = m_CalDescs[i].GetCount();
			nRows += nDescs;
			if (nDescs)
			{
				nRows++;
				bDescs = TRUE;
			}
		}
	}
	if (bDescs)
		nRows++;
	//******************************
	if (nRows <= 1)
		return;
	long col = 2;
	long row = pCell->GetRows(0);
	long colE = nCols - 1;
	if (row < 2)
	{
		nRows --;
		pCell->SetRows(row + nRows, 0);
	}
	else
	{
		pCell->SetRows(row + nRows, 0);
	}
	
	long para = pCell->FindFontIndex(Gbl_ConfigInfo.m_Watch.FFont[ftT2].lfFaceName, 1);
	long row1 = row;
	DWORD dwPhasex[] = {0, 15, 7, 14, 11, 13, 6, 3, 5, 10, 12, 9, 2, 4, 1, 8};
	int iPhasex[] = {0, 4, 3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1};

	// ***************Fill title*****************
	long colme = 3*colx + 1;
	// 空行
	if (row > 1)
	{
		pCell->SetRowHeight(0, cth, row, 0);
		pCell->MergeCells(1, row, nCols - 1, row);
		pCell->DrawGridLine(1, row, nCols - 1, row, 2, 1, 0);
		pCell->DrawGridLine(1, row, nCols - 1, row, 3, 1, 0);
		pCell->DrawGridLine(1, row, nCols - 1, row, 5, 1, 0);
		row ++;
	}

	pCell->MergeCells(1, row, nCols - 1, row); 
	pCell->MergeCells(col, row + 1, colme, row + 1);
	pCell->MergeCells(1, row + 2, nCols - 1, row + 2);

	pCell->SetRowHeight(0, 10, row, 0);
	pCell->SetRowHeight(0, 10, row + 2, 0);

	CString strTitle = Label();
	strTitle += _T(" Calibration and Check");
	int nHeadStart = row + 4;
	pCell->SetRowHeight(0, 100, row + 1, 0);
	pCell->SetCellAlign(col, row + 1, 0, 36);	// 垂直居中 | 水平居中
	pCell->SetCellFont(col, row + 1, 0, para);
	pCell->SetCellFontSize(col, row + 1, 0, 13);
	pCell->SetCellFontStyle(col, row + 1, 0, 2);
	pCell->SetCellString(col, row + 1, 0, strTitle);
	pCell->DrawGridLine(1, row, nCols - 1, row + 2, 1, 3, 0);
	pCell->DrawGridLine(1, row + 1, 1, row + 1, 4, 0, 0);
	pCell->DrawGridLine(1, row + 1, 1, row + 1, 5, 0, 0);
	pCell->DrawGridLine(nCols - 1, row + 1, nCols - 1, row + 1, 4, 0, 0);
	pCell->DrawGridLine(nCols - 1, row + 1, nCols - 1, row + 1, 5, 0, 0);
	pCell->DrawGridLine(col, row + 1, colme, row + 1, 1, 3, 0);
	row += 3;

	if (bDescs) // 图头与设备之间的空行
	{
		pCell->MergeCells(1, row, nCols - 1, row);
		pCell->DrawGridLine(1, row, nCols - 1, row, 2, 0, 0);
		pCell->DrawGridLine(1, row, nCols - 1, row, 3, 0, 0);
		row++;
	}
	CString strHead[4] = {_T("Master Equipment"), _T("Before Equipment"), 
		_T("After Equipment"), _T("Primary Equipment")};

	for (i = 0; i < 4; i++)
	{
		UINT nPhase = nPhaseTime[i];
		if (dwPhases & dwCalPhase[nPhase])
		{
			pos = m_CalDescs[nPhase].GetHeadPosition();
			for (int j = 0; pos != NULL; j++)
			{
				CALDESC* pDesc = m_CalDescs[nPhase].GetNext(pos);
				if (j == 0)
				{
					pCell->MergeCells(col, row, nCols - 1, row);
					pCell->SetCellFont(col, row, 0, para);
					pCell->SetCellFontSize(col, row, 0, 11);
					pCell->SetCellFontStyle(col, row, 0, 2);
					pCell->SetCellString(col, row, 0, strHead[nPhase]);
					row++;
				}
				
				pCell->MergeCells(col, row, colx, row);
				pCell->SetCellFont(col, row, 0, para);
				pCell->SetCellFontSize(col, row, 0, 11);
				pCell->SetCellFontStyle(col, row, 0, 2);
				pCell->SetCellString(col, row, 0, pDesc->strName);

				pCell->MergeCells(colx + 1, row, nCols - 1, row);
				pCell->SetCellFont(colx + 1, row, 0, para);
				pCell->SetCellFontSize(colx + 1, row, 0, 11);
				pCell->SetCellFontStyle(colx + 1, row, 0, 2);
				pCell->SetCellString(colx + 1, row, 0, pDesc->strValue);
				row++;
			}
		}
	}

	//pCell->DrawGridLine(col, nHeadStart, colme, row - 1, 0, 0, 0);
	if (bDescs)
		pCell->DrawGridLine(1, nHeadStart, nCols, row - 1, 1, 3, 0);

	//***********************************************
	CString strTFormat[] = { _T("%s Measurement"), _T("%s Engineering") };
	for (m = 0; m < 2; m++)
	{
		for (pos = lstDot[m].GetHeadPosition(); pos != NULL; )
		{
			CString strDotx = lstDot[m].GetNext(pos);
			CStringList strDotsx;
			for (i = 1; i < 16; i++)
			{
				if (GetCharts(strDotx, &strDots[m][dwPhasex[i]], &strDotsx) == 0)
					continue;
				
				FillChart(pCell, strDotx, &strDotsx, col, row, colE, strTFormat[m], nInsert[m], para, dwPhasex[i], iPhasex[i]);
			}
		}
	}
	
	for (i = 1; i < 16; i++)
	{
		FillChart(pCell, &lstCoef, &strCoefs[dwPhasex[i]], col, row, colE, _T("%s Coefficient"), nInsert[2], para, dwPhasex[i]);
	}

	pCell->SetBorder(0);
	CULView* pView = DYNAMIC_DOWNCAST(CULView, pCell->GetParent());
	if (pView)
		pView->SetCellFitColWidth(pCell, CSize(42, 42));
}

void CULTool::AddCalChart(CPtrList* pList, DWORD dwPhases /* = 15 */)
{
//#ifndef _LOGIC 

	CMapStringToPtr mapDot[2];
	CMapStringToPtr mapCoef;
	
	CPtrList lstItem;
	
	CString strTFormat[] = { _T("%s Measurement"), _T("%s Engineering"), _T("%s Coefficient") };
	for (int m = 0; m < 3; m++)
	{
		for (int n = 0; n < 4; n++)
		{
			if ((dwPhases & dwCalPhase[n]) == 0)
				continue;

			if (m_tmCaled[n].GetTime() == tmNew)
				continue;

			for (int c = 0; c < m_CalCurves[n].GetSize(); c++)
			{
				CULCalCurve* pCur = (CULCalCurve*)m_CalCurves[n].GetAt(c);
				if (pCur == NULL)
					continue;
				
				if (pCur->m_CalCSs.GetSize() == 0)
					continue;

				int s = 0;
				CULCalCurveSeg* pCS = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(s);
				if (pCS == NULL)
					continue;

				CCalChartItem* pItem = NULL;
				CString strItem;

				if (m == 2)
				{
					for (int f = 0; f < pCS->m_CalCoefs.GetSize(); f++)
					{
						ULCalCoef cc = pCS->m_CalCoefs.GetAt(f);
						if (cc.dRMin == cc.dRMax)
							continue;

						pItem = NULL;
						CCalBodyObj* pBody = NULL;
						strItem.Format(_T("%s %s %s"), pCur->m_strName, cc.szAlias, cc.szUnit);

						if (mapCoef.Lookup(cc.szAlias, (void*&)pBody))
						{
							pItem = pBody->FindItem(strItem);
						}
						else
						{
							pBody = new CCalBodyObj;
							pBody->m_strText.Format(strTFormat[m], cc.szAlias);
							lstItem.AddTail(pBody);
							mapCoef.SetAt(cc.szAlias, pBody);
						}

						if (NULL == pItem)
						{
							pItem = new CCalChartItem;
							pItem->strItem = strItem;
							pBody->m_lstItems.AddTail(pItem);

							pItem->fMinValue = cc.dRMin;
							pItem->fNorValue = cc.dRNor;
							pItem->fMaxValue = cc.dRMax;
						}

						pItem->fValue[n] = cc.dValue;
						COLORREF clr = GetCalTimeColor(n);
						if (clr == clrTm[4])
							pItem->nState[n] = NOT_DONE;
// 						else if (clr == clrTm[3])
// 							pItem->nState[n] = EXCEEDS_TIME;
						else
							pItem->nState[n] = 0;

						pBody->m_dwPhases |= dwCalPhase[n];
					}						
					continue;
				}

				for (int d = 0; d < pCS->m_CalDotList.GetSize(); d++)
				{
					CULCalDot* pDot = (CULCalDot*)pCS->m_CalDotList.GetAt(d);
					
					if (m == 1)
					{
						if (pDot->m_fMinStand == pDot->m_fMaxStand)
							continue;

						if (fabs(pDot->m_fEng) < FLT_EPSILON)
							continue;
					}
					else
					{
						if (pDot->m_fUserMin == pDot->m_fUserMax)
							continue;

						if (fabs(pDot->m_fMeas) < FLT_EPSILON)
							continue;
					}

					pItem = NULL;
					CCalBodyObj* pBody = NULL;
					strItem.Format(_T("%s %s %s"), pCur->m_strName, pDot->m_strAlias, pCur->m_strUnit);
					if (mapDot[m].Lookup(pDot->m_strAlias, (void*&)pBody))
					{
						pItem = pBody->FindItem(strItem);
					}
					else
					{
						pBody = new CCalBodyObj;
						pBody->m_strText.Format(strTFormat[m], pDot->m_strAlias);
						lstItem.AddTail(pBody);
						mapDot[m].SetAt(pDot->m_strAlias, pBody);
					}

					if (NULL == pItem)
					{
						pItem = new CCalChartItem;
						pItem->strItem = strItem;
						pBody->m_lstItems.AddTail(pItem);
						
						if (m == 1)
						{
							pItem->fMinValue = pDot->m_fMinStand;
							pItem->fNorValue = pDot->m_fFineStand;
							pItem->fMaxValue = pDot->m_fMaxStand;
						}
						else
						{
							pItem->fMinValue = pDot->m_fUserMin;
							pItem->fNorValue = pDot->m_fUserNormal;
							pItem->fMaxValue = pDot->m_fUserMax;
						}
					}

					pItem->fValue[n] = m ? pDot->m_fEng : pDot->m_fMeas;
					COLORREF clr = GetCalTimeColor(n);
					if (clr == clrTm[4])
						pItem->nState[n] = NOT_DONE;
// 					else if (clr == clrTm[3])
// 						pItem->nState[n] = EXCEEDS_TIME;
					else
						pItem->nState[n] = 0;
					pBody->m_dwPhases |= dwCalPhase[n];
				}
			}
		}
	}

	if (lstItem.GetCount() < 1)
		return;

	// Add Title
	CString strTitle = Label();
	strTitle += _T(" Calibration and Check");

	CCalTitleObj* pTitle = new CCalTitleObj;
	pTitle->m_strText = strTitle;
	
	CString strHead[4] = {_T("Master Equipment"), _T("Before Equipment"), 
		_T("After Equipment"), _T("Primary Equipment")};

	for (int i = 0; i < 4; i++)
	{
		UINT nPhase = nPhaseTime[i];
		if (dwPhases & dwCalPhase[nPhase])
		{
			POSITION pos = m_CalDescs[nPhase].GetHeadPosition();
			for (int j = 0; pos != NULL; j++)
			{
				CALDESC* pDesc = m_CalDescs[nPhase].GetNext(pos);
				if (j == 0)
				{
					pTitle->m_names.Add(strHead[nPhase]);
				}
				
				CString strName = _T("    ");
				strName += pDesc->strName;
				pTitle->m_names.Add(strName);
				pTitle->m_values.Add(pDesc->strValue);
			}
		}
	}

	pTitle->m_iLineWidth = Gbl_ConfigInfo.SetLWLevel(1);
	pList->AddTail(pTitle);

	CStringArray times;
	for (int n = 0; n < 4; n++)
	{
		CString strTime = strCalPhase[n];
		strTime += _T(": ");
		COLORREF clr = GetCalTimeColor(m_tmCaled[n]);
		if (clr == clrTm[4])
			strTime += _T("Calibration not done");
// 		else if (clr == clrTm[3])
// 			strTime += _T("Calibration out of date");
		else
			strTime += m_tmCaled[n].Format(TM_FORMAT);
		times.Add(strTime);
	}

	for (POSITION pos = lstItem.GetHeadPosition(); pos != NULL; )
	{
		CCalBodyObj* pCalBody = (CCalBodyObj*)lstItem.GetNext(pos);
		pCalBody->m_iLineWidth = pTitle->m_iLineWidth;
		pCalBody->m_times.Copy(times);
	}

	pList->AddTail(&lstItem);
//#endif
}

//////////////////////////////////////////////////////////////////////////
//生成自定义刻度报告
short CULTool::CalcAllMetaReport()
{
	for (UINT i=0; i<4; i++)
	{
		if(m_hMetaReport[i]!=NULL&&m_pITool!=NULL)
		{
			DeleteMetaFile(m_hMetaReport[i]);
			m_hMetaReport[i] = NULL;
			m_szMetaReport[i] = 0;
		}
		CalcMetaReport(i);
	}
	return UL_NO_ERROR;
}

short CULTool::CalcMetaReport(UINT uCalPhase)
{
	if (m_pITool != NULL)
	{
		if(m_hMetaReport[uCalPhase]!=NULL)
			DeleteMetaFile(m_hMetaReport[uCalPhase]);
		m_hMetaReport[uCalPhase] = NULL;
		m_szMetaReport[uCalPhase] = 0;

		CMetaFileDC metaDC;
		metaDC.Create();
		metaDC.SetMapMode(MM_LOMETRIC);
		CSize sz(0,0);
		m_szMetaReport[uCalPhase] = m_pITool->CalUserPrint(&metaDC,sz,uCalPhase);
		if(m_szMetaReport[uCalPhase] < 0)
			m_szMetaReport[uCalPhase] = 0;
		m_hMetaReport[uCalPhase] = metaDC.Close();
	}

	return UL_NO_ERROR;	
}

long CULTool::GetMetaReportHeight()
{
	long lToolMetaReportHeight = 0;
	for (UINT i=0; i<4; i++)
	{
		DWORD dwIndex = 0x01<<i;
		if((m_dwCalMetaReports & dwIndex) == dwIndex)
			lToolMetaReportHeight+=m_szMetaReport[i];
	}
	return lToolMetaReportHeight;
}

long CULTool::GetMetaReportHeight(UINT uCalPhase)
{
	long lToolMetaReportHeight = 0;

		DWORD dwIndex = 0x01<<uCalPhase;
		if((m_dwCalMetaReports & dwIndex) == dwIndex)
			lToolMetaReportHeight+=m_szMetaReport[uCalPhase];
	return lToolMetaReportHeight;
}

ULMDWDIMP CULTool::WriteToFile(CFile* pFile, DWORD dwType, IULTool* pToolUpdate /*=NULL*/)
{
	DWORD dwRes = dwType;
	pFile->Write(&dwType, sizeof(DWORD));
	if (dwType & 1) // 将仪器动态库文件写入数据文件
	{
		CFile file1;
		if (file1.Open(m_strToolDllName, CFile::modeRead))
		{
			DWORD dwBytes = file1.GetLength();
			pFile->Write(&dwBytes, sizeof(DWORD));
			if (dwBytes > 0)
			{
				BYTE* pBuf = new BYTE[dwBytes];
				UINT nBytesRead = file1.Read(pBuf, dwBytes);
				pFile->Write(pBuf, nBytesRead);
				delete[] pBuf;
			}

			file1.Close();
		}
		else
		{
			DWORD dwBytes = 0;
			pFile->Write(&dwBytes, sizeof(DWORD));
			dwRes &= ~1;
			TRACE1("Warning : Failed to save %s library!", strToolName);
		}
	}

	if (dwType & 2)  // 属性信息
	{
		CXMLSettings xml(FALSE, strToolName);
		Serialize(xml);
		SerializeProps(xml);
		if (xml.WriteXMLToFile(pFile))
		{
		}
		else
		{
			DWORD dwBytes = 0;
			pFile->Write(&dwBytes, sizeof(DWORD));
			dwRes &= ~2;
			TRACE1("Warning : Failed to save %s information.", strToolName);
		}
	}

	if (dwType & 4)  // 刻度信息
	{
		CalcAllMetaReport();
		CMemFile file;
        IULTool* pTool; 

		for (UINT uPhase = 0; uPhase < 4; uPhase++)
        {
            if(pToolUpdate != NULL &&  // update calibration info ?
                (pToolUpdate->NeedUpdateCalibrate() & dwCalPhase[uPhase]))
                pTool = pToolUpdate; 
            else 
                pTool = this; 

            pTool->SetCalPhase(uPhase); 
            switch (m_nCalType[uPhase])
            {
            case UL_CAL_USER:
                ((CULTool*)pTool)->SaveCalFileEx(&file,uPhase);
				CopyCalFile(&file,uPhase);	
                //pTool->CalUserSave(&file, strCalPhase[uPhase]); 
				((CULTool*)pTool)->CalUserSaveEX(&file, uPhase);
                break;
			case UL_CAL_INDEPENDENT:
            case UL_CAL_DEFAULT:
            case UL_CAL_ASST:
            default:
                pTool->SaveCalInfo(&file, uPhase); 
                break;
            }
        }

		DWORD dwLen = file.GetLength();
		if (dwLen > 0)
		{
			pFile->Write(&dwLen, sizeof(DWORD));
			if (dwLen > 0)
			{
				BYTE* buf = file.Detach();
				pFile->Write(buf, dwLen);
				delete[] buf;
			}
		}
		else
		{
			dwLen = 0;
			pFile->Write(&dwLen, sizeof(DWORD));
		}
	}

	return dwRes;
}

short _stdcall CULTool::CopyCalFile(CFile* pFile, UINT uPhase) 
{
	// 打开需要保存的文件，把前面系统添加的头部去掉，只保留用户存储的部分写入文件中
	// TODO: Add your control notification handler code here
/*
	if (g_pActPrj == NULL && g_pActPrj->m_ULKernel.m_arrTools.GetSize() <= 0) // 未打开工程
	{
		return UL_ERROR;
	}
	
	if (g_pActPrj->m_pService == NULL)
	{
		return UL_ERROR;
	}
	if(g_pActPrj->m_bComputReplay == TRUE)
	{
		return UL_ERROR;
	}*/
	
	CString strPathx = Gbl_AppPath + strToolDllDir; //源文件目录
	strPathx = strPathx.Left(strPathx.ReverseFind('\\'));
    strPathx += "\\";
	if (_tchdir(strPathx))
		_tmkdir(strPathx);
	
	CString strSN = SN();
	if (strSN.GetLength() > 0)
	{
		strPathx += strSN;
	}
	else
	{
		strPathx +=_T("Default");
	}

	strPathx += "\\";
	
	if (_tchdir(strPathx))
		_tmkdir(strPathx);
	
	
	CTime tm = m_tmCaled[uPhase];
	CString strTime = tm.Format("%Y-%m-%d-%H-%M");
	
	strPathx += strTime;
	strPathx += "\\";
	if (_tchdir(strPathx))
		_tmkdir(strPathx);

	CString StrFileName = strCalPhase[uPhase];
	StrFileName += ".udc";

	strPathx += StrFileName;

	CFile fSource;	
	if(!fSource.Open(strPathx,CFile::modeRead))
	{
		DWORD dwLen = 0;
		pFile->Write(&dwLen, sizeof(DWORD));
		return UL_ERROR;
	}
	
	// 将ReadCalFile读取的字节偏移过去
	if (!ReadCalFile(&fSource))
	{
		DWORD dwLen = 0; 
		fSource.Read(&dwLen, sizeof(DWORD));
		//m_pITool->CalUserLoad(&file, strCalPhase[m_uCalPhase].GetBuffer(0));
		//strCalPhase[m_uCalPhase].ReleaseBuffer();
		BYTE* pBuff = new BYTE[dwLen];
		fSource.Read(pBuff, dwLen);

		pFile->Write(&dwLen, sizeof(DWORD));
		pFile->Write(pBuff, dwLen);

		delete [] pBuff;
	}
	else
	{
		DWORD dwLen = 0;
		pFile->Write(&dwLen, sizeof(DWORD));
	}
				
	fSource.Close();
				
	return UL_NO_ERROR;
}

ULMDWDIMP CULTool::ReadFromFile(CFile* pFile, DWORD dwType)
{
	DWORD dwRes;
	pFile->Read(&dwRes, sizeof(DWORD));
	if (dwRes > 1024)
	{
		pFile->Seek(dwRes, CFile::current);
		/*		CString strCount;
		strCount.Format("%d", m_nFileID ++);
		CString strFile = Gbl_AppPath + "\\Temp\\" + strCount + "TToolDll.dll";		
		CFile file1;
		if (file1.Open(strFile, CFile::modeCreate|CFile::modeWrite))
		{
		BYTE* pBuf = new BYTE[dwRes];
		UINT nBytesRead = pFile->Read(pBuf, dwRes);
		file1.Write(pBuf, nBytesRead);
		file1.Close();
		delete []pBuf;
		}

		m_dwLoad = DL_CHANNEL|DL_ERROR;
		ReloadTool(strFile);
		ReleaseTool(FALSE);
		m_dwLoad = 0;*/
		return 1;
	}

	if (dwRes & 1)
	{
		DWORD dwBytes = 0;
		pFile->Read(&dwBytes, sizeof(DWORD));
		if (dwBytes > 0)
		{
			pFile->Seek(dwBytes, CFile::current);
			/*
			CString strCount;
			strCount.Format("%d", m_nFileID ++);
			CString strFile = Gbl_AppPath + "\\Temp\\" + strCount + "TToolDll.dll";	
			CFile file1;
			if (file1.Open(strFile, CFile::modeCreate|CFile::modeWrite))
			{
				BYTE* pBuf = new BYTE[dwBytes];
				UINT nBytesRead = pFile->Read(pBuf, dwBytes);
				file1.Write(pBuf, nBytesRead);
				file1.Close();
				delete []pBuf;
			}

			m_dwLoad = DL_CHANNEL|DL_ERROR;
			ReloadTool(strFile);
			ReleaseTool(FALSE);
			m_dwLoad = 0;	*/
		}
	}

	if (dwRes & 2)
	{
		CXMLSettings xml;
		if (xml.ReadXMLFromFile(pFile))
		{
			Serialize(xml);
			SerializeProps(xml);
		}
	}

	if (dwRes & 4)
	{
		DWORD dwLen = 0; 
		pFile->Read(&dwLen, sizeof(DWORD));
		if (dwLen > 0)
		{
			BYTE* buf = new BYTE[dwLen];
			pFile->Read(buf, dwLen);
			CMemFile file(buf, dwLen);
			for (int i = 0; i < 4; i++)
			{
				switch (m_nCalType[i])
				{
				case UL_CAL_USER:
					ReadCalFileEx(&file,i);
					CalUserLoad(&file, strCalPhase[i]);
					CalUserLoadEX(&file, i);
					m_CalFilePathArray[i] = "\\" + strCalPhase[i] + ".udc";
					break;
				case UL_CAL_INDEPENDENT:
				case UL_CAL_DEFAULT:
				case UL_CAL_ASST:
				default:
					if (m_pITool)
						ReadCalInfo(&file, i);
					else
						ReadCalInfoEx(&file, i);
					m_CalFilePathArray[i] = "\\" + strCalPhase[i] + ".ulc";
					break;
				}
			}

			delete []buf;
		}
	}

	return dwRes;
}

ULMBOOLIMP CULTool::AddParam2(LPCTSTR pszKey, int iVal, LPCTSTR pszList, 
		LPCTSTR pszType /* = NULL */, LPCTSTR pszDesc /* = NULL */, 
		LPCTSTR pszUnit /* = NULL */, LPCTSTR pszComment /* = NULL */, 
		DWORD dwPrint /* = 3 */)
{
	CToolParam* pTP = m_Params.GetParam(pszKey);
	if (pTP)
	{
		CString strList = pszList;
		pTP->m_strArray.RemoveAll();
		SplitString(strList, '\n', pTP->m_strArray);
		
		pTP->SetVal(iVal);
		pTP->m_strType = pszType;
		pTP->m_strDesc = pszDesc;
		pTP->m_strUnit = pszUnit;
		pTP->m_strComment = pszComment;
		pTP->m_bDisplayPrint = dwPrint;
		return TRUE;
	}
	
	return FALSE;
}

ULMBOOLIMP CULTool::AddParam2(LPCTSTR pszKey, LPCTSTR pszVal, LPCTSTR pszList, 
			LPCTSTR pszType /* = NULL */, LPCTSTR pszDesc /* = NULL */, 
			LPCTSTR pszUnit /* = NULL */, LPCTSTR pszComment /* = NULL */, 
			DWORD dwPrint /* = 3 */)
{
	CToolParam* pTP = m_Params.GetParam(pszKey);
	if (pTP)
	{
		CString strList = pszList;
		pTP->m_strArray.RemoveAll();
		SplitString(strList, '\n', pTP->m_strArray);
		pTP->m_nValType = VT_INT|VT_ARRAY;		
		pTP->SetVal(pszVal);
		pTP->m_strType = pszType;
		pTP->m_strDesc = pszDesc;
		pTP->m_strUnit = pszUnit;
		pTP->m_strComment = pszComment;            
		pTP->m_bDisplayPrint = dwPrint;
		return TRUE;
	}
	
	return FALSE;
}

void CULTool::UpdateDlgData(BOOL bSaveAndValidate /* = TRUE */)
{
	for (int i = 0; i < m_arrWnd.GetSize(); i++)
	{
		CBInfo* pCBInfo = (CBInfo*)m_arrWnd.GetAt(i);
		if (pCBInfo && ::IsWindow(pCBInfo->pWnd->GetSafeHwnd()))
			pCBInfo->pWnd->UpdateData(bSaveAndValidate);
	}
}

ULMIMP CULTool::SerializeCalInfo(CXMLSettings& xml, UINT uPhase, BOOL bTime /* = TRUE */)
{
	if (xml.IsStoring())
	{
		xml.Write("ToolName", strToolName);
		xml.Write("CalPhase", uPhase);
		
		long lTime = m_tmCaled[uPhase].GetTime();
		xml.Write("CalTime", lTime);
		
		int nCurves = m_CalCurves[uPhase].GetSize();
		// xml.Write("CalCurveCount", nCurves);
		for (int i = 0; i < nCurves; i++)
		{
			CULCalCurve* pCur = (CULCalCurve*)m_CalCurves[uPhase].GetAt(i);
			if (xml.CreateKey(pCur->m_strName))
			{
				pCur->Serialize(xml);
				xml.Back();
			}
		}
		
		int nDescs = m_CalDescs[uPhase].GetCount();
		xml.Write("CalDescCount", nDescs);
		POSITION pos = m_CalDescs[uPhase].GetHeadPosition();
		for (i = 0; pos != NULL; i++)
		{
			CALDESC* pCalDesc = m_CalDescs[uPhase].GetNext(pos);
			if (xml.CreateKey("CalDesc%02d", i))
			{
				xml.Write("DescN", pCalDesc->strName);
				xml.Write("DescV", pCalDesc->strValue);
				xml.Back();
			}
		}

		//MetaReport
// 		if(xml.CreateKey("MetaReport"))
		{
			xml.Write("MetaHeight",m_szMetaReport[uPhase]);

			CFile metafile;
			UINT dwMetaLen = 0;
			if (m_hMetaReport[uPhase] != NULL)
			{
				CString strTemp = Gbl_AppPath + "\\Temp\\" +  "MetaReport.wmf";
				HMETAFILE hMetafile = CopyMetaFile(m_hMetaReport[uPhase], strTemp);
				DeleteMetaFile(hMetafile);
				if (metafile.Open(strTemp, CFile::modeRead))
				{
					dwMetaLen = metafile.GetLength();
					xml.Write("MetaSize",dwMetaLen);
					if (dwMetaLen>0)
					{
						BYTE* pBuf = new BYTE[dwMetaLen];
						UINT nBytesRead = metafile.Read(pBuf, dwMetaLen);
						//pFile->Write(&nBytesRead, sizeof(UINT));
						xml.Write("MetaFile",pBuf, dwMetaLen);
						delete[] pBuf;
					}
					metafile.Close();
					DeleteFile(strTemp);
				}
				else
				{
					//TRACE("Load CalMetafile error.\n");
					dwMetaLen = 0;
					xml.Write("MetaSize",dwMetaLen);
				}
			}
			else
				xml.Write("MetaSize",dwMetaLen);
		}
	}
	else
	{
		CString strName;
		xml.Read("ToolName", strName);
		if (strName != strToolName)
		{
			TRACE0("Warning : Couldn't read the calibration file of another tool!\n");
			if (strToolName.GetLength())
				AfxMessageBox(IDS_ERR_NOMATCH, MB_ICONERROR);
			return UL_ERROR;
		}

		UINT uCalPhase;
		xml.Read("CalPhase", uCalPhase);
		if (uCalPhase != uPhase)
		{
			//if (uPhase != 3)
			{
				AfxMessageBox(IDS_CAL_MISMATCH, MB_ICONERROR);
				return UL_ERROR;
			}

			uPhase = uCalPhase;
			if (uPhase > 3)
			{
				AfxMessageBox(IDS_CAL_MISMATCH, MB_ICONERROR);
				return UL_ERROR;
			}
		}
				
		long ltm;
		xml.Read("CalTime", ltm);
		m_tmCaled[uPhase] = CTime((time_t)ltm);

		if (bTime)
		{
			m_bCaled[uPhase] = (GetCalTimeColor(uPhase) == RGB(255,255,255)) ? FALSE : TRUE;
			if (!m_bCaled[uPhase])
				return UL_ERROR;
		}
		else
			m_bCaled[uPhase] = TRUE;
				
		int nCurves = 0;
		if (xml.Read("CalCurveCount", nCurves))
		{
			for (int i = 0; i < nCurves; i++)
			{
				if (!xml.Open("CalCurve%02d", i))
					continue ;
				
				CString strCalCurve;
				xml.Read("Name", strCalCurve);
				CULCalCurve* pCur = GetCalCurve(uPhase, strCalCurve);
				if (m_nCurGroup ==-1 || pCur->m_nCalGroup == m_nCurGroup) // add by bao 2013/0603 刻度分组
				{
					if (pCur != NULL)
						pCur->Serialize(xml);
					else
					{
						TRACE("Warning : Cal curve %s is ignore by tool %s!\n", 
							strCalCurve, strToolName);
					}
				}
				
				xml.Back();
			}
		}
		else
		{
			for (int i = m_CalCurves[uPhase].GetSize() - 1; i > -1; i--)
			{
				CULCalCurve* pCur = (CULCalCurve*)m_CalCurves[uPhase].GetAt(i);
				if (m_nCurGroup >=0 && pCur->m_nCalGroup != m_nCurGroup) // add by bao 2013/0603 刻度分组
				{
					continue;
				}
				if (xml.Open(pCur->m_strName))
				{
					pCur->Serialize(xml);
					xml.Back();
				}
			}
		}
		
		
		ClearCalDescs(uPhase);
		int nDescs = 0;
		xml.Read("CalDescCount", nDescs);
		for (int i = 0; i < nDescs; i++)
		{
			if (xml.Open("CalDesc%02d", i))
			{
				CALDESC* pCalDesc = new CALDESC;
				xml.Read("DescN", pCalDesc->strName);
				xml.Read("DescV", pCalDesc->strValue);
				m_CalDescs[uPhase].AddTail(pCalDesc);
				xml.Back();
			}
		}

/*		//MetaReport
		m_szMetaReport[uPhase] = 0;
		if(m_hMetaReport[uPhase] != NULL)
		{
			DeleteMetaFile(m_hMetaReport[uPhase]);
			m_hMetaReport[uPhase] = NULL;
		}

		//if(xml.Open("MetaReport"))
		{
			xml.Read("MetaHeight",m_szMetaReport[uPhase]);
			
			CFile file;
			UINT dwMetaLen = 0;
			xml.Read("MetaSize",dwMetaLen);
			if (dwMetaLen > 0)
			{
				CString strTPh = "~";
				strTPh += strCalPhase[uPhase];
				BYTE* pBuf = new BYTE[dwMetaLen];
				xml.Read("MetaFile", pBuf, dwMetaLen);
				if (TRUE) // 为后面生成的wmf文件生成不同的名字
				{
					CString strFind = Gbl_AppPath + "\\Temp\\" + strTPh + _T("*.*");
					int nMax = GetMaxNoF(strFind);
					
					CString str;
					str.Format(_T("-%d"), ++nMax);
					strTPh += str;	
				}
				
				CString strTemp = Gbl_AppPath + "\\Temp\\" + strTPh + ".wmf";
				if (file.Open(strTemp, CFile::modeCreate | CFile::modeWrite))
				{
					file.Write(pBuf, dwMetaLen);
					file.Close();
				}
				
				m_hMetaReport[m_uCalPhase] = GetMetaFile(strTemp);				
				delete[] pBuf;
			}
		}*/
	}
	return UL_NO_ERROR;
}

ULMIMP CULTool::SetCoefUnit(LPCTSTR lpszCurve , LPCTSTR lpszName,	LPCTSTR lpszUnit)
{
	for (int i = 0; i < 4; i++)
	{
		CULCalCurve* pCur = GetCalCurve(i, lpszCurve);
		if (pCur == NULL)
			continue ;
			
		for (int j = 0; j < pCur->m_CalCSs.GetSize(); j++)
		{
			CULCalCurveSeg* pCS = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(j);
			
			if (pCS != NULL)
			{
				CString strName = lpszName;
				for(int i = 0 ; i < pCS->m_CalCoefs .GetSize () ; i++)
				{
					ULCalCoef coef = pCS->m_CalCoefs [i];
					CString strTemp = coef.szName;
					if(strName == strTemp)
					{	
						pCS->m_CalCoefs [i].szUnit = lpszUnit;
						break;
					}
				}
			}
		}
	}

	return UL_NO_ERROR;
}

ULMIMP CULTool::SetDotUnit(LPCTSTR lpszCurve , LPCTSTR lpszName , LPCTSTR lpszUnit)
{
	for (int i = 0; i < 4; i++)
	{
		CULCalCurve* pCur = GetCalCurve(i, lpszCurve);
		if (pCur == NULL)
			continue ;
			
		for (int j = 0; j < pCur->m_CalCSs.GetSize(); j++)
		{
			CULCalCurveSeg* pCS = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(j);
			
			if (pCS != NULL)
			{
				CString strName = lpszName;
				for(int i = 0 ; i < pCS->m_CalDotList .GetSize () ; i++)
				{
					CULCalDot* p = (CULCalDot *)pCS->m_CalDotList .GetAt (i);
					CString strTemp = p->m_strName ;
					if(strName == strTemp)
					{	
						p->m_strUnit = lpszUnit;
						break;
					}
				}
			}
		}
	}

	return UL_NO_ERROR;
}

BOOL CULTool::ExceedsLimit(UINT nPhase)
{
	if (nPhase > 3)
		return TRUE;

	int nCur = m_CalCurves[nPhase].GetSize();
	for (int i = 0; i < nCur; i++)
	{
		CULCalCurve* pCur = (CULCalCurve*)m_CalCurves[nPhase].GetAt(i);
		if (pCur == NULL)
			continue;

		int nCS = pCur->m_CalCSs.GetSize();
		for (int j = 0; j < nCS; j++)
		{
			CULCalCurveSeg* pCS = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(j);
			if (pCS->ExceedsLimit())
				return TRUE;
		}
	}

	return FALSE;
}

//#include "FileTrace.h"

ULMLNGIMP CULTool::GetCurDepth()
{
#ifndef _LOGIC
	if (Gbl_ConfigInfo.m_System.RelogDepth && (m_nWorkMode == WORKMODE_COMPUTEPLAYBACK))
	{	
		// Compute replaying
		if (m_BufferArray.GetSize() <= 0)
			return 0;

		ASSERT( m_BufferArray.GetSize() > 0 );
		
		BYTE* pBuf = (BYTE*) m_BufferArray.GetAt(0).pBuffer;
		if (g_pActPrj != NULL && (g_pActPrj->m_ULKernel.m_pRawFile != NULL)
			&& (g_pActPrj->m_ULKernel.m_pRawFile->m_fVersion > 2.300f))
		{
			int nSize = *m_BufferArray.GetAt(0).pLength - 12;
			memcpy(&m_lRDSDepth, (pBuf + nSize), sizeof(long));
		}
		else
		{
			int nSize = *m_BufferArray.GetAt(0).pLength - 4;
			memcpy(&m_lRDSDepth, (pBuf + nSize), sizeof(long)); 
		}		
	}
	else	// Logging*/
	{
		m_lRDSDepth = CULTool::m_lCurDepth;
	}
#endif

/*
	//Debug strat
	if (m_BufferArray.GetSize() <= 0)
			return 0;

	ASSERT( m_BufferArray.GetSize() > 0 );
	
	BYTE* pBuf = (BYTE*) m_BufferArray.GetAt(0).pBuffer;
	int nSize = *m_BufferArray.GetAt(0).pLength - 4;
	long lRawDepth = 0;
	memcpy(&lRawDepth, (pBuf + nSize), sizeof(long)); 

	TRACE("%s m_lRDSDepth = %.3f , lRawDepth = %.3f ; \n" , strToolName , DTM(m_lRDSDepth) , DTM(lRawDepth));
	//Debug end
*/
#ifdef _DATA_TRACE
	TRACE("%s----- %ld\n", strToolName, m_lRDSDepth);
#endif

	return m_lRDSDepth;
}


ULMIMP CULTool::SetImagePath(LPCTSTR strPath)
{
	m_strImagePath = strPath;
	return UL_NO_ERROR;
	/*
	if (!Gbl_ConfigInfo.m_System.bSetImagePath)
	{
		m_strImagePath = strPath;
		return UL_NO_ERROR;
	}
	//自定义路径
	CString strName;
	int nPostion = strPath.ReverseFind('/');
	if (nPostion == -1)
	{
		nPostion = strPath.ReverseFind('\\');
	}
	if (nPostion != -1)
	{
		strName = strPath.Right(strPath.GetLength() - nPostion - 1);	
	}
	else
		strName = strToolName + ".bmp";
	m_strImagePath = Gbl_ConfigInfo.m_System.ImagePath;	
	if (m_strImagePath.IsEmpty())
		m_strImagePath = Gbl_AppPath + "\\Template\\Shapes\\" + strName;
	else
	{
		if (m_strImagePath.GetAt(m_strImagePath.GetLength() - 1) != '\\')
			m_strImagePath += '\\';
		m_strImagePath += strName;
	}*/
	return UL_NO_ERROR;
}


ULMIMP CULTool::SetToolInfo(LPCTSTR SN, LPCTSTR AssetNo, double Diameter, double Weight, double MaxSpeed)
{
	strSN = SN;
	strAssetNo = AssetNo;
	oDiameter = Diameter;
	iDiameter = Diameter;
	oDiameterMax = Diameter;
	oDiameterMin = Diameter;
	fWeight = Weight;
	fMaxSpeed = MaxSpeed;
	return UL_NO_ERROR;
}

ULMIMP CULTool::SetToolInfoEx(LPCTSTR pszInfo, double* pValue)
{
	CVParam p(pszInfo);
	UINT nCount = p.m_params.GetSize();
	if (!AfxIsValidAddress(pValue, nCount*sizeof(double), FALSE))
		return UL_ERROR;
	
	double* pProp = &iDiameter;
	CString strProps[] = {_T("ID"), _T("OD"), _T("ODMax"), _T("ODMin"), _T("PressMax"), _T("TempMax"), _T("SpeedMax") };

	for (int i = 0; i < nCount; i++)
	{
		CString strProp = p.m_params.GetAt(i);
		for (int j = 0; j < 7; j++)
		{
			if (strProp == strProps[j])
			{
				pProp[j] = pValue[i];
				break;
			}
		}		
	}

	return UL_NO_ERROR;
}

ULMIMP CULTool::GetToolInfoEx(LPCTSTR pszInfo, double* pValue)
{
	CVParam p(pszInfo);
	UINT nCount = p.m_params.GetSize();
	if (!AfxIsValidAddress(pValue, nCount*sizeof(double)))
		return UL_ERROR;
	
	double* pProp = &iDiameter;
	CString strProps[] = {_T("ID"), _T("OD"), _T("ODMax"), _T("ODMin"), _T("PressMax"), _T("TempMax"), _T("SpeedMax") };

	for (int i = 0; i < nCount; i++)
	{
		CString strProp = p.m_params.GetAt(i);
		for (int j = 0; j < 7; j++)
		{
			if (strProp == strProps[j])
			{
				pValue[i] = pProp[j];
				break;
			}
		}		
	}

	return UL_NO_ERROR;
}

ULMIMP CULTool::GetToolInfo(LPTSTR SN, LPTSTR AssetNo, double& Diameter, double& Weight, double& MaxSpeed)
{
	strcpy(SN, strSN);
	strcpy(AssetNo, strAssetNo);
	Diameter = oDiameter;
	Weight = fWeight;
	MaxSpeed = fMaxSpeed;
	return UL_NO_ERROR;
}

/* 函数名称：SetCurCountDown
 * 函数描述：给绞车面板发送当前倒计数值
 * 返回类型：short
 * 函数参数：fCurCountDown当前倒计数值
 */
short CULTool::SetCurCountDown(float fCurCountDown, BOOL bFalg)
{
#ifdef _PERFORATION
	if (m_pITool2 != NULL)
		return m_pITool2->SetCurCountDown(fCurCountDown, bFalg);
#endif
	return UL_ERROR;
}

ULMBOOLIMP CULTool::ScopeSetting(int nOperate, int nParam, LPVOID lpParam, int nParam2)
{
	int nSet = -1;

#ifdef _PERFORATION
#ifdef _OLD_SCOPES
	m_ScopeSettings.nScopeIndex = -1;

	switch (nOperate)
	{
	case SS_SETXMAX:
		m_ScopeSettings.nXMax = nParam;
		nSet = 1;
		break;
	case SS_SETYMAX:
		m_ScopeSettings.nYMax = nParam;
		nSet = 1;
		break;
	case SS_SETDATANUMPERSEC:
		m_ScopeSettings.nDataPerSec = nParam;
		nSet = 1;
		break;
	case SS_SETCYCLE:
		m_ScopeSettings.nCycle = nParam;
		nSet = 1;
		break;
	case SS_SETCOLLTIMES:
		m_ScopeSettings.nCollTimes = nParam;
		nSet = 1;
		break;
	case SS_SHOW:
		m_ScopeSettings.nShowCmd = nParam;
		nSet = 1;
		break;
	case SS_CUSTOMIZEDEPTHSCOPE:
		m_ScopeSettings.nDepthScopeCustomized = nParam;
		break;
	case SS_ADDDEPTHSCOPEITEM:
		{
			DepthScopeItem* pItem = (DepthScopeItem*)lpParam;
			int bH = -1;
			for(int i = 0 ; i < m_arDepthScopeItems.GetSize() ; i++)
			{
				DepthScopeItem* pTempItem = (DepthScopeItem *)m_arDepthScopeItems.GetAt(i);
				if(stricmp(pTempItem->cENName , pItem->cENName) == 0 && pTempItem->nDataSource == pItem->nDataSource)
				{
					bH = i;
					break;
				}
			}
			DepthScopeItem* pNewItem = NULL;
			if(bH >= 0)
				pNewItem = (DepthScopeItem *)m_arDepthScopeItems.GetAt(bH);
			else
			{
				pNewItem = new DepthScopeItem;
				m_arDepthScopeItems.Add(pNewItem);
			}
			
			strcpy(pNewItem->cCNName, pItem->cCNName);
		    strcpy(pNewItem->cENName, pItem->cENName);
		    strcpy(pNewItem->cUnit, pItem->cUnit);
		    pNewItem->fMaxValue = pItem->fMaxValue;
		    pNewItem->fMinValue = pItem->fMinValue;
			pNewItem->fMidValue = pItem->fMidValue;
		    pNewItem->nItemType = pItem->nItemType;
			pNewItem->nDataSource = pItem->nDataSource;
			pNewItem->nPrecision = pItem->nPrecision;

			for(i = 0 ; i < m_Scopes.GetSize() ; i++)
			{
				CULOscilliscope* p = (CULOscilliscope*)m_Scopes.GetAt(i);
				if(p->m_ScopePara.nType == UL_OSCILLISCOPE_DEPTH && p->m_ScopePara.pDlg != NULL && bH >= 0)
				{
					CCommonScope* pScope = (CCommonScope *)p->m_ScopePara.pDlg;
					pScope->ReinitScope();
				}
			}
		}
		break;
	case SS_SETDEPTHITEMVALUE:
		nSet = 1;
		break;
	case SS_ADDCURVETOSCOPE:
		{
			int nScopeIndex = nParam;
			LPCTSTR lpszCurve = (LPCTSTR)(LPTSTR)lpParam;
			SCInfo info;
           	info.nIndex = nScopeIndex;
	        info.strCurve = lpszCurve;
	        m_arrScopeCurves.Add(info);
		}
		break;
    case SS_CUSTOMIZEDCURVE:
		m_ScopeSettings.nCurveCustomized = nParam;	
		m_ScopeSettings.nScopeIndex = nParam2;
		break;
	case SS_WINDOWVISIBLE:
		{
			CMainFrame* pFrm = (CMainFrame*)theApp.m_pMainWnd;
			if (pFrm->GetSafeHwnd() != NULL)
			{
				pFrm->ShowScopeWnd(this, nParam, nParam2);
			}
		}
		break;
	case SS_ADDUSERDIALOG:
		{
			 ScopeUserDlg dlg;
			 dlg.nScope = nParam;
			 dlg.pWnd = (CWnd*)lpParam;
			 dlg.nID = nParam2;
			 m_arScopeUserDlg.Add(dlg);
		}
		break;
	case SS_ADDMARK:
		{
			int nScopeIndex = nParam;
		}
		break;
	case SS_SETUSERSCOPEDLG:
		{
			if (nParam >= 0 && nParam < m_Scopes.GetSize())
			{
				CULOscilliscope* p = (CULOscilliscope*)m_Scopes.GetAt(nParam);
				p->m_ScopePara.pDlg = lpParam;
				p->m_ScopePara.nID = nParam2;
			}
		}
		break;
	case SS_SENDMESSAGE:
		{
			CMainFrame* pFrm = (CMainFrame*)theApp.m_pMainWnd;
			if (pFrm->GetSafeHwnd() != NULL)
			{
				int n = pFrm->m_Scopes.GetSize();
				for (int i  = 0; i < n; i++)
				{
					CCommonScope* pWnd = 
						DYNAMIC_DOWNCAST(CCommonScope, (CWnd*)pFrm->m_Scopes.GetAt(i));
					if (pWnd != NULL)
						pWnd->SendMessage(nParam, 0, (long)lpParam);
				}
			}			
		}
		break;

	case SS_SETXUNIT:
		{
			nSet = 1;
			m_ScopeSettings.nScopeIndex = nParam2;
		}
		break;

	case SS_SETXLABLE:
		{
			nSet = 1;
			m_ScopeSettings.nScopeIndex = nParam2;
		}
		break;

	case SS_SETCURVEPROP:
		{
			nSet = 1;
			m_ScopeSettings.nScopeIndex = nParam2;
		}
		break;
//add by gj 2012/12/03
//record:增加添加标记以及设置标记属性功能
	case SS_ADDMARKTOSCOPE:
		{
			nSet = 1;
			m_ScopeSettings.nScopeIndex = nParam2;
		}
		break;
	case SS_SETMARKPROP:
		{
			nSet = 1;
			m_ScopeSettings.nScopeIndex = nParam2;
		}
		break;

	default:
		break;
	}
	int nCount = 0;
	if (nSet != -1)
	{
		CMainFrame* pFrm = (CMainFrame*)theApp.m_pMainWnd;
		if (pFrm->GetSafeHwnd() != NULL)
		{
			int n = pFrm->m_Scopes.GetSize();
			for (int i  = 0; i < n; i++)
			{
				CCommonScope* pWnd = 
					DYNAMIC_DOWNCAST(CCommonScope, (CWnd*)pFrm->m_Scopes.GetAt(i));
				//add by xia 2012/11/21
				//record:调用Scopesetting 时，对所有的示波器都进行了设置
				//pWnd->m_pTool== this和nCount == m_ScopeSettings.nScopeIndex保证了对应正确的仪器示波器
				//	m_ScopeSettings.nScopeIndex 对所有示波器修改
				if (pWnd != NULL)
				{
					if (pWnd->m_pTool == this)
					{
						if ( m_ScopeSettings.nScopeIndex == -1)
						{
							pWnd->ScopeSettingInfo(nOperate, lpParam);
						}
						else if (m_ScopeSettings.nScopeIndex != -1)
						{
							if (nCount == m_ScopeSettings.nScopeIndex )
							{
								pWnd->ScopeSettingInfo(nOperate, lpParam);
							}
							nCount++;
						}
					}
				}
			}
		}
	}
#endif
#endif
	return nSet;
}

ULMIMP CULTool::CurvesView(void* pStringArray)
{
	CStringArray* pArray = (CStringArray*)pStringArray;
	if (pArray != NULL)
	{
		int n = m_Curves.GetSize();
		int i;
		for (i = 0; i < n; i++)
		{
			CCurve* pCurve = m_Curves.GetAt(i);
			pCurve->View(FALSE);
		}
		n = m_StatusCurves.GetSize();
		for (i = 0; i < n; i++)
		{
			CCurve* pCurve = m_StatusCurves.GetAt(i);
			pCurve->View(FALSE);
		}
		n = pArray->GetSize();
		for (i = 0; i < n; i++)
		{
			CString strCurve = pArray->GetAt(i);
			CCurve* pCurve = (CCurve*)GetCurve(strCurve);
		    // 只有打开的曲线才能显示出来
			if (pCurve != NULL && pCurve->IsOpen())
			{
				pCurve->View(TRUE);
			}
			pCurve = (CCurve*)GetStatusCurve(strCurve);
			if (pCurve != NULL && pCurve->IsOpen())
			{
				pCurve->View(TRUE);
			}
		}
	}

#ifndef _LOGIC
	CMainFrame* pFrm = (CMainFrame*)theApp.m_pMainWnd;
	if (pFrm->GetSafeHwnd() != NULL)
	{
		pFrm->RefreshDataView();
	}

#endif
	return UL_NO_ERROR;
}

ULMIMP CULTool::CurvesOpen(void* pStringArray)
{
	CStringArray* pArray = (CStringArray*)pStringArray;
	if (pArray != NULL)
	{
		int n = m_Curves.GetSize();
		int i;
		for (i = 0; i < n; i++)
		{
			CCurve* pCurve = m_Curves.GetAt(i);
			pCurve->Open(FALSE);
		}
		n = m_StatusCurves.GetSize();
		for (i = 0; i < n; i++)
		{
			CCurve* pCurve = m_StatusCurves.GetAt(i);
			pCurve->Open(FALSE);
		}
		n = pArray->GetSize();
		for (i = 0; i < n; i++)
		{
			CString strCurve = pArray->GetAt(i);
			CCurve* pCurve = (CCurve*)GetCurve(strCurve);
			if (pCurve != NULL)
			{
				pCurve->Open(TRUE);
			}
			pCurve = (CCurve*)GetStatusCurve(strCurve);
			if (pCurve != NULL)
			{
				pCurve->Open(TRUE);
			}
		}
	}

#ifndef _LOGIC

	CMainFrame* pFrm = (CMainFrame*)theApp.m_pMainWnd;
	if (pFrm->GetSafeHwnd() != NULL)
	{
		pFrm->RefreshAfterCurveOpened(this);
	}

#endif

	return UL_NO_ERROR;
}

ULMIMP CULTool::SetCurveBounds(int nCurve, double fLeftBound, double fRightBound)
{
#ifdef _PERFORATION
	int n = m_Curves.GetSize();
	if (nCurve >= 0 && nCurve < n)
	{
		CCurve* pCurve = (CCurve*)m_Curves.GetAt(nCurve);
		if (fLeftBound < fRightBound)
		{
			pCurve->m_pData->m_LeftBound = fLeftBound;
			pCurve->m_pData->m_RightBound = fRightBound;
		}
		else
		{
			pCurve->m_pData->m_LeftBound = fRightBound;
			pCurve->m_pData->m_RightBound = fLeftBound;
		}
		
		return UL_NO_ERROR;
	}
#endif
	return UL_ERROR;
}

ULMIMP CULTool::AddMark(int nCurve, LPCTSTR lpszName, long lDepth, DWORD dwColor, int nType)
{
#ifdef _PERFORATION
	int n = m_Curves.GetSize();
	if (nCurve >= 0 && nCurve < n)
	{
		CCurve* pCurve = (CCurve*)m_Curves.GetAt(nCurve);
	    g_pMainWnd->AddMark(pCurve, lpszName, lDepth, dwColor, nType);
		return UL_NO_ERROR;
	}
	else
#endif
	    return UL_ERROR;
}

ULMIMP CULTool::SetCurveOutOfBoundNum(int nCurve, int nLeftNum, int nRightNum)
{
#ifdef _PERFORATION
	int n = m_Curves.GetSize();
	if (nCurve >= 0 && nCurve < n)
	{
		CCurve* pCurve = (CCurve*)m_Curves.GetAt(nCurve);
		pCurve->m_pData->m_nLeftOutNum = nLeftNum;
		pCurve->m_pData->m_nRightOutNum = nRightNum;
		return UL_NO_ERROR;
	}

#endif
	return UL_ERROR;
}

void CULTool::InitBoundInfo()
{
#ifdef _PERFORATION
	int n = m_Curves.GetSize();
	for (int i = 0; i < n; i++)
	{
		CCurve* pCurve = (CCurve*)m_Curves.GetAt(i);
		pCurve->m_pData->m_nLeftOutNum = 0;
		pCurve->m_pData->m_nRightOutNum = 0;
		pCurve->m_pData->m_bLeftOut = FALSE;
		pCurve->m_pData->m_bRightOut = FALSE;
	}
#endif
}

ULMINTIMP CULTool::TestCurveBound(int nCurve)
{
	int n = m_Curves.GetSize();
	if (nCurve >= 0 && nCurve < n)
	{
	    CCurve* pCurve = (CCurve*)m_Curves.GetAt(nCurve);
		return pCurve->TestCurveBound();
	}
	return 0;
}

void CULTool::UnlockChannel()
{
#ifndef _LOGIC
   for (int i = 0; i < m_Channels.GetSize(); i++)
   {
        CChannel* pChannel = (CChannel*)m_Channels.GetAt(i);
		if (!pChannel->Unlock())
			TRACE("Failed to unlock channel");
   }
   //TRACE("Unlock channel Time: %d\n", GetTickCount());
#endif
}

void CULTool::StartChannel()
{
#ifndef _LOGIC
	for (int i = 0; i < m_Channels.GetSize(); i++)
	{
        CChannel* pChannel = (CChannel*)m_Channels.GetAt(i);
		pChannel->Start();
	}
#endif
}

void CULTool::StopChannel()
{
#ifndef _LOGIC
	for (int i = 0; i < m_Channels.GetSize(); i++)
	{
        CChannel* pChannel = (CChannel*)m_Channels.GetAt(i);
		pChannel->Stop();
	}
#endif
}

ULMIMP CULTool::GetToolPath(LPTSTR pszPath)
{
	CString strPath = GetToolDllPath();
	_tcscpy(pszPath, strPath);
	return UL_NO_ERROR;
}

ULMIMP CULTool::AddCalDesc(LPCTSTR pszName, LPCTSTR pszValue)
{
	CALDESC* pCalDesc = new CALDESC;
	pCalDesc->strName = pszName;
	pCalDesc->strValue= pszValue;
	m_CalDescs[m_uCalPhase].AddTail(pCalDesc);
	return UL_NO_ERROR;
}

ULMIMP CULTool::UpdateCalDesc(LPCTSTR pszName, LPCTSTR pszValue)
{
	POSITION pos = m_CalDescs[m_uCalPhase].GetHeadPosition(); 
	while (pos != NULL)
	{
		CALDESC* pCalDesc = m_CalDescs[m_uCalPhase].GetNext(pos);
		if (pCalDesc->strName == pszName)
			pCalDesc->strValue = pszValue;
	}
	return UL_NO_ERROR;
}

ULMIMP CULTool::ClearCalDescs()
{
	while (m_CalDescs[m_uCalPhase].GetCount())
		delete m_CalDescs[m_uCalPhase].RemoveHead();
	return UL_NO_ERROR;
}

ULMIMP CULTool::DeleteCalDescs(LPCTSTR pszName)
{
	POSITION pos = m_CalDescs[m_uCalPhase].GetHeadPosition(); 
	while (pos != NULL)
	{
		CALDESC* pCalDesc = m_CalDescs[m_uCalPhase].GetAt(pos);
		if (pCalDesc->strName == pszName)
		{
			delete pCalDesc;
			m_CalDescs[m_uCalPhase].RemoveAt(pos);
			return UL_NO_ERROR;
		}	
		m_CalDescs[m_uCalPhase].GetNext(pos);
	}
	return UL_ERROR;
}

ULMTSTRIMP CULTool::GetCalDesc(LPCTSTR pszName)
{
	CString strValue; strValue.Empty();
	POSITION pos = m_CalDescs[m_uCalPhase].GetHeadPosition(); 
	while (pos != NULL)
	{
		CALDESC* pCalDesc = m_CalDescs[m_uCalPhase].GetAt(pos);
		if (pCalDesc->strName == pszName)
		{
			return (LPSTR)(LPCTSTR)pCalDesc->strValue;
		}	
		m_CalDescs[m_uCalPhase].GetNext(pos);
	}
	return (LPSTR)(LPCTSTR)strValue;
}

ULMIMP CULTool::AddCalDesc(UINT uPhase, LPCTSTR pszName, LPCTSTR pszValue)
{
	if (uPhase > 3)
		return UL_ERROR;

	CALDESC* pCalDesc = new CALDESC;
	pCalDesc->strName = pszName;
	pCalDesc->strValue= pszValue;
	m_CalDescs[uPhase].AddTail(pCalDesc);
	return UL_NO_ERROR;
}

ULMIMP CULTool::UpdateCalDesc(UINT uPhase, LPCTSTR pszName, LPCTSTR pszValue)
{
	if (uPhase > 3)
		return UL_ERROR;
	POSITION pos = m_CalDescs[uPhase].GetHeadPosition(); 
	while (pos != NULL)
	{
		CALDESC* pCalDesc = m_CalDescs[uPhase].GetNext(pos);
		if (pCalDesc->strName == pszName)
			pCalDesc->strValue = pszValue;
	}
	return UL_NO_ERROR;
}

ULMIMP CULTool::ClearCalDescs(UINT uPhase)
{
	if (uPhase > 3)
		return UL_ERROR;
	while (m_CalDescs[uPhase].GetCount())
		delete m_CalDescs[uPhase].RemoveHead();
	return UL_NO_ERROR;
}

ULMIMP CULTool::DeleteCalDescs(UINT uPhase, LPCTSTR pszName)
{
	if (uPhase > 3)
		return UL_ERROR;
	POSITION pos = m_CalDescs[uPhase].GetHeadPosition(); 
	while (pos != NULL)
	{
		CALDESC* pCalDesc = m_CalDescs[uPhase].GetAt(pos);
		if (pCalDesc->strName == pszName)
		{
			delete pCalDesc;
			m_CalDescs[uPhase].RemoveAt(pos);
			return UL_NO_ERROR;
		}	
		m_CalDescs[uPhase].GetNext(pos);
	}
	return UL_ERROR;
}

ULMTSTRIMP CULTool::GetCalDesc(UINT uPhase, LPCTSTR pszName)
{
	CString strValue; strValue.Empty();
	if (uPhase > 3)
		return (LPSTR)(LPCTSTR)strValue;
	POSITION pos = m_CalDescs[uPhase].GetHeadPosition(); 
	while (pos != NULL)
	{
		CALDESC* pCalDesc = m_CalDescs[uPhase].GetAt(pos);
		if (pCalDesc->strName == pszName)
		{
			return (LPSTR)(LPCTSTR)pCalDesc->strValue;
		}	
		m_CalDescs[uPhase].GetNext(pos);
	}
	return (LPSTR)(LPCTSTR)strValue;
}

ULMBOOLIMP CULTool::GetULParam(UINT nID, void* pValue)
{
	if (m_vecTools.size() == 0)
		return FALSE;
	
	CULTool* pULTool = (CULTool*)m_vecTools[0].pIULTool;
	CString strID;
	strID.Format("%d", nID);
	void* p = NULL;
	pULTool->m_mapULParam.Lookup(strID, p);
	*(int*)pValue = (int)p;
	return TRUE;
}

ULMBOOLIMP CULTool::SetULParam(UINT nID, void* pValue)
{
	if (m_vecTools.size() == 0)
		return FALSE;
	
	CULTool* pULTool = (CULTool*)m_vecTools[0].pIULTool;
	CString strID;
	strID.Format("%d", nID);
	pULTool->m_mapULParam[strID] = pValue;
	return TRUE;
}

ULMIMP CULTool::SaveCalFile()
{
	CString strToolSN = SN();

	UINT uPhase = m_uCalPhase;
	CTime tm = CTime::GetCurrentTime();

	// Tools\厂商\系列\名称\序列号\日期+时间\类型.刻度文件扩展名
	CString strTime = tm.Format("%Y-%m-%d-%H-%M");

	// Get the system calibrate file save path
	CString strPathx = Gbl_AppPath + strToolDllDir;
	strPathx = strPathx.Left(strPathx.ReverseFind('\\'));
	CString strDefPath = strPathx + "\\Default\\";
	CString strSysPath = strPathx + "\\" + strSN;
	CString strLastPath = strSysPath + "\\";

	if (_tchdir(strSysPath))
		_tmkdir(strSysPath);
	
	strSysPath += "\\" + strTime;
	if (_tchdir(strSysPath))
	   	_tmkdir(strSysPath);

	// Get the user calibrate file save path
	CString strUsrPath = _T("");
	BOOL bUserSave = FALSE;
	if (Gbl_ConfigInfo.m_DataSavePath.nSaveStyle)  
	{
		CString strUsrPathTemp = Gbl_ConfigInfo.m_DataSavePath.szToolCalSavePath;

		if (strUsrPathTemp.GetLength())
		{
			if(_tchdir(strUsrPathTemp))
				_tmkdir(strUsrPathTemp);
			
			strUsrPath = strUsrPathTemp;
			CString str = strENName;
			strUsrPath += "\\" + str;
			if (_tchdir(strUsrPath))
				_tmkdir(strUsrPath);

			strUsrPath += "\\" + strSN;
			if (_tchdir(strUsrPath))
				_tmkdir(strUsrPath);

			strUsrPath += "\\" + strTime;
			if (_tchdir(strUsrPath))
				_tmkdir(strUsrPath);

			bUserSave = TRUE;
		}
	}

	CString strCalFileName = strCalPhase[uPhase];

	// Set The calibration time 
	m_tmCaled[uPhase] = tm;
	m_bCaled[uPhase] = TRUE;

	for (int i=0; i < m_CalCurves[uPhase].GetSize(); i++)
	{
		CULCalCurve* pCur = (CULCalCurve*)m_CalCurves[uPhase].GetAt(i);
		if (pCur->m_CalCSs.GetSize() == 0)
			break ;
		
		for (int j = 0; j < pCur->m_CalCSs.GetSize(); j++)
		{
			CULCalCurveSeg* pCS = (CULCalCurveSeg*)pCur->m_CalCSs.GetAt(j);
			pCS->m_strCalTm[uPhase] = tm.Format("%Y.%m.%d-%H:%M");
		}		
	}
	
	CString strFile = strSysPath + "\\" + strCalFileName;
	WriteCalInfo(strFile + _T(".ulc"));
	WriteCalTextFile(strFile + _T(".txt"));

	if (bUserSave && strUsrPath.GetLength())  
	{
		strFile = strUsrPath + "\\" + strCalFileName;
		WriteCalInfo(strFile + _T(".ulc"));
		WriteCalTextFile(strFile + _T(".txt"));
	}
	

	// Write the calibrate file to the default directory
	strFile = strDefPath + strCalFileName + _T(".ulc");
	WriteCalInfo(strFile);
	
	// 在仪器序列号目录保存最新刻度文件
	strFile = strLastPath + strCalFileName + _T(".ulc");
	WriteCalInfo(strFile);
	
	m_CalFilePathArray[uPhase] = strFile;
	
	return UL_NO_ERROR;
}

ULMIMP CULTool::ReadCalFile(CString strCalFileName)
{
#ifndef _LOGIC
	CString strFilePathName = strCalFileName;
	if(strFilePathName.IsEmpty())
	{
		CString strFileName = _T("");
		CString strDllPath = GetToolDllPath();
		if (m_strCalFilePath.GetLength())
		{
			int iFind = m_strCalFilePath.ReverseFind('\\');
			if (iFind > 0)
				strFileName = m_strCalFilePath.Left(iFind + 1);
		}
		else
		{
			if (strDllPath.GetLength())
				strFileName = strDllPath;
		}
		
		CString strFile = strCalPhase[m_uCalPhase];
		strFileName += strFile;

		CString strSN = SN();
		if (strDllPath.GetLength() && strSN.GetLength())
			strDllPath += ("\\" + strSN);
		
		strFile += ".ulc";
		CCalFileDialog fd(this, "ulc", strFileName, OFN_HIDEREADONLY|OFN_EXPLORER, ULCFilter);
		if (fd.DoModal() != IDOK)
			return FALSE;
		
		CString strPath;
		if (fd.m_nPage == CCalFileDialog::CalFileOpen)
			strFilePathName = fd.m_strCalPathName;
		else
			strFilePathName = fd.GetPathName();
	}

	if (ReadCalInfo(strFilePathName))
	{
		AEI(IDS_TOOL_CAL, theApp.m_pszAppName, ET_CAL|ES_ERROR, IDS_CAL_LOAD,
			IDS_CAL_LOAD_FAILED, strCalFileName, m_lCurDepth, strToolName);

		return FALSE;
	}
	
	m_strCalFilePath = strFilePathName;
	m_CalFilePathArray[m_uCalPhase] = strFilePathName;
	AEI(IDS_TOOL_CAL, theApp.m_pszAppName, ET_CAL, IDS_CAL_LOAD,
		IDS_CAL_LOAD_RESULT, strFilePathName, m_lCurDepth, strToolName);
#endif
	return UL_NO_ERROR;
}

ULMIMP CULTool::ShowCalReport(CWnd *pParent)
{
#ifndef _LOGIC
	if(pParent == NULL)
		return UL_ERROR;
	if(!pParent->IsKindOf(RUNTIME_CLASS( CCalChildFrame )))
		return UL_ERROR;
	
	DWORD dwPhases = 15;
	CString strText = "CMPBA";
	switch (m_uCalPhase)
	{
	case UL_CAL_MASTER:
		dwPhases = CP_MASTER;
		strText = "CM";
		break;
	case UL_CAL_BEFORE:
		dwPhases = CP_BEFROR;
		strText = "CB";
		break;
	case UL_CAL_AFTER:
		dwPhases = CP_AFTER;
		strText = "CA";
		break;	
	case UL_VERIFY_MASTER:
		dwPhases = CP_PRIMARY;
		strText = "CP";
		break;
	default:
		break;
	}

	UINT uPhase = m_uCalPhase;
	CCalChildFrame* pCal = (CCalChildFrame*)pParent;
	CChildFrame* pChild = (CChildFrame*)pCal->m_frmArray.GetAt(uPhase);
	if (pChild != NULL)
	{
		pChild->m_bDestroy = TRUE;
		pChild->SendMessage(WM_CLOSE);
		pCal->m_frmArray.SetAt(uPhase, NULL);
	}
	
	CMainFrame* pMainFrame = (CMainFrame*)theApp.m_pMainWnd;
	pChild = pMainFrame->CreateChild(ULV_CALCHART);
	if (pChild != NULL)
	{
		CString strTempl;
		strTempl.Format("%s_%s", strToolName, strText);
		pChild->SetWindowText(strTempl);
		CCalChartView* pCalChart = (CCalChartView*)pChild->m_pULView;
		AddCalChart(&pCalChart->m_lstCharts, dwPhases);
		pCalChart->m_nRefresh = 2;
		m_CalReportArray[uPhase] = strTempl;
		pCal->m_frmArray.SetAt(uPhase, pChild);
	}
#endif	
	return UL_NO_ERROR;
}

ULMIMP CULTool::ShowCalSummary(CWnd *pParent)
{
#ifndef _LOGIC
	if(pParent == NULL)
		return UL_ERROR;
	if(!pParent->IsKindOf(RUNTIME_CLASS( CCalChildFrame )))
		return UL_ERROR;
	
	DWORD dwPhases = 15;
	CString strText = "CMPBA";
	switch (m_uCalPhase)
	{
	case UL_CAL_MASTER:
		dwPhases = CP_MASTER;
		strText = "CM";
		break;
	case UL_CAL_BEFORE:
		dwPhases = CP_BEFROR;
		strText = "CB";
		break;
	case UL_CAL_AFTER:
		dwPhases = CP_AFTER;
		strText = "CA";
		break;	
	case UL_VERIFY_MASTER:
		dwPhases = CP_PRIMARY;
		strText = "CP";
		break;
	default:
		break;
	}


	UINT uPhase = m_uCalPhase;
	CCalChildFrame* pCal = (CCalChildFrame*)pParent;
	CChildFrame* pChild = (CChildFrame*)pCal->m_frmArray.GetAt(uPhase);
	if (pChild != NULL)
	{
		pChild->m_bDestroy = TRUE;
		pChild->SendMessage(WM_CLOSE);
		pCal->m_frmArray.SetAt(uPhase, NULL);
		pChild = NULL;
	}
	CPtrArray arrTools;
	arrTools.Add(this);
	pChild = new CChildFrame(ULV_CALSUMMARY, &arrTools , NULL);
	pChild->m_dwState &= ST_SHOW;
	if (!pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD, theApp.m_pMainWnd))
	{
		TRACE0("Warning : Couldn't create a frame of calibarte summary.\n");
	}
	else
	{
		pChild->SetTitle(IDS_CALIBRATE_SUMMARY);
	}

	if (pChild != NULL)
	{
		CString strTempl;
		strTempl.Format("%s_%s", strToolName, strText);
		pChild->SetWindowText(strTempl);
		pCal->m_frmArray.SetAt(uPhase, pChild);
	}

#endif	
	return UL_NO_ERROR;
}

short _stdcall CULTool::AddEventItem(LPCTSTR lpstrName, LPCTSTR lpstrOrigination, BYTE byEventType, 
									 LPCTSTR lpstrEventContent, LPCTSTR lpstrEventResult, 
									 long lDepth /* = 0 */, LPCTSTR lpToolName /* = NULL */, 
									 LPCTSTR lpParaName /* = NULL */, double fParaValue /* = 0 */, 
									 LPCTSTR lpstrMark /* = NULL */)
{
	if (g_pScout)
	{
		return g_pScout->AddEventItem(lpstrName, lpstrOrigination, lDepth, lpToolName, 
			lpParaName, fParaValue, 6/* 默认为仪器参数事件 */, byEventType,
			lpstrEventContent, lpstrEventResult, lpstrMark);
	}
}

short CULTool::CalUserSaveEX(CFile* pFile, UINT uPhase)
{
	DWORD dwMark = 0xFFFFFFF0;
	pFile->Write(&dwMark, sizeof(DWORD));
	pFile->Write(&dwMark, sizeof(DWORD));
	CXMLSettings xml(FALSE, "CalInfo");
	SerializeCalInfo(xml, uPhase);
	if (xml.WriteXMLToFile(pFile))
		return UL_NO_ERROR;
	
	return UL_ERROR;
}

short CULTool::CalUserLoadEX(CFile* pFile, UINT uPhase)
{
	DWORD dwMark = 0xFFFFFFF0, dwMarkRead1 = 0, dwMarkRead2 = 0;
	DWORD dwPos = pFile->GetPosition();
	pFile->Read(&dwMarkRead1, sizeof(DWORD));
	pFile->Read(&dwMarkRead2, sizeof(DWORD));
	if ((dwMarkRead1 != dwMark) || (dwMarkRead2 != dwMark))
	{
		pFile->Seek(dwPos, CFile::begin);
		return UL_ERROR;
	}
	
	if (m_pITool)
		ReadCalInfo(pFile, uPhase);
	else
		ReadCalInfoEx(pFile, uPhase);
	
	return UL_NO_ERROR;
}

short _stdcall CULTool::AddCalParamEx(void* pCurve, void* pSeg, void* pDot, LPCTSTR pszGroupName/* = _T(" ")*/)
{
	if (m_dwLoad & DL_CALIBRATE)
		return UL_ERROR;

	
    for (int n = 0; n < 4; n++)
    {
        SetCalPhase(UL_CAL_MASTER + n);			// 设置刻度类型

		CString strNewGroupName = pszGroupName;
		for (int i = 0; i < m_mapCalGroup[n].GetCount(); i++)
		{
			CString strGroupName;
			if (m_mapCalGroup[n].Lookup(i, strGroupName))
			{
				if (strNewGroupName == strGroupName)
				{
					break;
				}
			}
		}
		if (i == m_mapCalGroup[n].GetCount())
		{
			m_mapCalGroup[n].SetAt(i, strNewGroupName);
		}
        AddCalCurve((ULCalCurveData*) pCurve, i);	// 追加刻度曲线 
        AddCalCurveSeg((ULCalSegData*) pSeg);	// 追加曲线段  
        AddCalDot((ULCalDotData*) pDot);		// 追加刻度点
    }
	
    m_dwCalibrate = CP_MASTER|CP_PRIMARY|CP_BEFROR|CP_AFTER;
	m_dwCalReports = CP_MASTER|CP_PRIMARY|CP_BEFROR|CP_AFTER;
	
    return UL_NO_ERROR;
}

short _stdcall CULTool::AddCalParamEx(UINT uPhase, void* pCurveData, void* pSegData, void* pDotData, LPCTSTR pszGroupName/* = _T(" ")*/)
{
	if (m_dwLoad & DL_CALIBRATE)
		return UL_ERROR;
	
	if (uPhase > 3)
		return UL_ERROR;

	CString strNewGroupName = pszGroupName;
	for (int i = 0; i < m_mapCalGroup[uPhase].GetCount(); i++)
	{
		CString strGroupName;
		if (m_mapCalGroup[uPhase].Lookup(i, strGroupName))
		{
			if (strNewGroupName == strGroupName)
			{
				break;
			}
		}
	}
	if (i == m_mapCalGroup[uPhase].GetCount())
	{
		m_mapCalGroup[uPhase].SetAt(i, strNewGroupName);
		}
	
	SetCalPhase(uPhase);
	AddCalCurve((ULCalCurveData*) pCurveData, i);	// 追加刻度曲线 
	AddCalCurveSeg((ULCalSegData*) pSegData);	// 追加曲线段  
    AddCalDot((ULCalDotData*) pDotData);		// 追加刻度点
	
	m_dwCalibrate |= dwCalPhase[uPhase];
	m_dwCalReports |= dwCalPhase[uPhase];
	
	return UL_NO_ERROR;
}

int CULTool::GetCalGroupCount(UINT nPhase)
{
	int nGroup = 0;
	if (nPhase > 3)
	{
		return nGroup;
	}

	nGroup = m_mapCalGroup[nPhase].GetCount();
	return nGroup;
}

short _stdcall CULTool::SaveCalToPrj(LPCTSTR lpszPath, int nPhase /* = 0 */)
{
	if (g_pActPrj == NULL && g_pActPrj->m_ULKernel.m_arrTools.GetSize() <= 0) // 未打开工程
	{
		return UL_ERROR;
	}

	if (g_pActPrj->m_pService == NULL)
	{
		return UL_ERROR;
	}
	if(g_pActPrj->m_bComputReplay == TRUE)
	{
		return UL_ERROR;
	}
	CString strFile = g_pActPrj->m_strPPath + "\\" + g_pActPrj->m_pService->m_strItem;
	if (_tchdir(strFile))
		_tmkdir(strFile);

	strFile += "\\";
	strFile += "Calibrate\\";
	if (_tchdir(strFile))
		_tmkdir(strFile);

	strFile += strToolName;
	strFile += "\\";
	if (_tchdir(strFile))
		_tmkdir(strFile);

	if (m_pToolInfoPack)
	{
		if (m_pToolInfoPack->m_Tool.strSN.GetLength()>0)
		{
			strFile += m_pToolInfoPack->m_Tool.strSN;
		}
		else
		{
			strFile +=_T("Default");
		}
	}
	strFile += "\\";
	if (_tchdir(strFile))
		_tmkdir(strFile);

	CTime tm = m_tmCaled[nPhase];
	CString strTime = tm.Format("%Y-%m-%d-%H-%M");

	strFile += strTime;
	strFile += "\\";
	if (_tchdir(strFile))
		_tmkdir(strFile);
	
	strFile += strCalPhase[nPhase];
	switch (m_nCalType[nPhase])
	{
	case UL_CAL_USER:
		{
			strFile += ".udc";
			CFile file;;
			if (file.Open((strFile), CFile::modeCreate|CFile::modeWrite))
			{
				SaveCalFile(&file);
				
				CalUserSave(&file, strCalPhase[nPhase]);
				file.Close();
			}
			else
			{
				CString strPrompt;
				AfxFormatString1(strPrompt, IDS_FAIL_OPENFILE, strFile);
				AfxMessageBox(strPrompt);
			}
		}
		break;
	default:
	case UL_CAL_ASST:
		{
			strFile += ".ulc";
			WriteCalInfo(strFile);
		}
		break;
	}


	return UL_NO_ERROR;
}
//add by gj 20131023
short _stdcall CULTool::CopyCalToPrj(LPCTSTR lpszPath, int nPhase /* = 0 */)
{
	if (g_pActPrj == NULL && g_pActPrj->m_ULKernel.m_arrTools.GetSize() <= 0) // 未打开工程
	{
		return UL_ERROR;
	}
	
	if (g_pActPrj->m_pService == NULL)
	{
		return UL_ERROR;
	}
	if(g_pActPrj->m_bComputReplay == TRUE)
	{
		return UL_ERROR;
	}
	CString strFile = g_pActPrj->m_strPPath + "\\" + g_pActPrj->m_pService->m_strItem;
	if (_tchdir(strFile))
		_tmkdir(strFile);
	
	strFile += "\\";
	strFile += "Calibrate\\";
	if (_tchdir(strFile))
		_tmkdir(strFile);
	
	strFile += strToolName;
	strFile += "\\";
	if (_tchdir(strFile))
		_tmkdir(strFile);

	CString strPathx = Gbl_AppPath + strToolDllDir; //源文件目录
	strPathx = strPathx.Left(strPathx.ReverseFind('\\'));
    strPathx += "\\";
	if (_tchdir(strPathx))
		_tmkdir(strPathx);

	
	if (m_pToolInfoPack)
	{
		if (m_pToolInfoPack->m_Tool.strSN.GetLength()>0)
		{
			strFile += m_pToolInfoPack->m_Tool.strSN;
			strPathx += m_pToolInfoPack->m_Tool.strSN;
		}
		else
		{
			strFile +=_T("Default");
			strPathx +=_T("Default");
		}
	}
	strFile += "\\";
	strPathx += "\\";

	if (_tchdir(strPathx))
		_tmkdir(strPathx);

	if (_tchdir(strFile))
		_tmkdir(strFile);
	
	CTime tm = m_tmCaled[nPhase];
	CString strTime = tm.Format("%Y-%m-%d-%H-%M");
	
	strFile += strTime;
	if (_tchdir(strFile))
		_tmkdir(strFile);

	strPathx += strTime;
	if (_tchdir(strPathx))
		_tmkdir(strPathx);


	
	CString StrFileName = strCalPhase[nPhase];

	switch (m_nCalType[nPhase])
	{
	case UL_CAL_USER:
		{
			StrFileName += ".udc";
			FileCopyTo(strPathx,strFile,StrFileName,TRUE);
		}
		break;
	default:
	case UL_CAL_ASST:
		{
			StrFileName += ".ulc";
			FileCopyTo(strPathx,strFile,StrFileName,TRUE);
		}
		break;
	}
	
	
	return UL_NO_ERROR;
}
//add by gj 20131023
void _stdcall CULTool::FileCopyTo(CString source, CString destination, CString searchStr, BOOL cover)
{
	CString strSourcePath = source;
	CString strDesPath = destination;
	CString strFileName = searchStr;
	CFileFind filefinder;
	CString strSearchPath = strSourcePath + "\\" + strFileName;
	CString filename;
	BOOL bfind = filefinder.FindFile(strSearchPath);
	CString SourcePath, DisPath;
	while (bfind)
	{
		bfind = filefinder.FindNextFile();
		filename = filefinder.GetFileName();
		SourcePath = strSourcePath + "\\" + filename;
		DisPath = strDesPath + "\\" + filename;
		CopyFile((LPCSTR)SourcePath, (LPCSTR)DisPath, cover);	
	}
	filefinder.Close();
}


short _stdcall CULTool::StartChannels()
{
	StartChannel();
	return UL_NO_ERROR;
}
short _stdcall CULTool::GetCurCalGroupInt()
{
	return m_nGroup;
}

BOOL _stdcall CULTool::SetCurveNoOffset(LPCTSTR lpszName, BOOL bCalcOffset)
{
	for (int i = 0; i < m_Curves.GetSize(); i++)
    {
        CCurve*pCurve = (CCurve*) m_Curves.GetAt(i);
        if (_tcscmp(pCurve->m_strName, lpszName) == 0)
        {
			if (pCurve->m_pData)
			{
				BOOL bOldCalc = pCurve->m_pData->m_bDrawFirst;
				pCurve->m_pData->m_bDrawFirst = bCalcOffset;
				return bOldCalc;
			}
        }
    }
    return 0;
}

BOOL _stdcall CULTool::SetCurveNoOffset(int nCurve, BOOL bCalcOffset)
{
	if (nCurve < m_Curves.GetSize())
    {
        CCurve* pCurve = (CCurve*) m_Curves.GetAt(nCurve);
        if (pCurve->m_pData)
        {
			BOOL bOldCalc = pCurve->m_pData->m_bDrawFirst;
			pCurve->m_pData->m_bDrawFirst = bCalcOffset;
			return bOldCalc;
        }
    }
	
	return 0;
}
// RemarkObject.cpp: implementation of the CRemarkObject class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RemarkObject.h"
#include <math.h>
#include "MyMEMDC.h"
#include "SaveAsBmp.h"
#include "Picture.h"

#define EDIT_MODE_FILL_SYMBOL       0       // 填充符号
#define EDIT_MODE_REMARK_SYMBOL     1       // 解释符号

#define WM_FILE_MODIFIED    (WM_USER + 100)
#define WM_PAINT_PREVIEW    (WM_USER + 101)
#define WM_UPDATE_PREVIEW   (WM_USER + 102)
#define WM_FILE_ITEM_OPEN   (WM_USER + 103)
#define WM_REFRESH_FILE_LIST (WM_USER + 104)
#define WM_SELECTION_CHANGE (WM_USER + 105)
#define WM_PROPERTY_CHANGE (WM_USER + 106)

// 解释符号图元对象属性改变事件
#define PT_IS_FILL  0
#define PT_FILL_COLOR   1
#define PT_LINE_COLOR   2
#define PT_IMAGE_FILE   3
#define PT_IMAGE_MODE   4

// 创建图片的方式
#define CREATE_IMAGE_BY_MOVE_CENTER 0   // 拖动图片中心点
#define CREATE_IMAGE_BY_DRAG_RECT   1   // 拖动一个矩形框

#define HT_DISTANCE 3  // 点击测试的距离

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CRemarkObject, CObject, 0)

CSize g_sizeRemark = CSize(200, 400);
CString CBaseRemark::m_strFPath;

CRemarkObject::CRemarkObject() 
{
    m_size = g_sizeRemark;
    m_rcPosition.left = 10; 
    m_rcPosition.top = 10; 
    m_rcPosition.right = m_rcPosition.left + m_size.cx; 
    m_rcPosition.bottom = m_rcPosition.top + m_size.cy; 
}

CRemarkObject::~CRemarkObject()
{
    Clear(); 
}

void CRemarkObject::Serialize(CArchive& ar)
{
    if(ar.IsStoring())
    {
        ar << m_size; 
        ar << m_rcPosition; 
    }
    else 
    {
        ar >> m_size; 
        ar >> m_rcPosition; 
    }

    m_listRemark.Serialize(ar); 
}

void CRemarkObject::Draw(CDC* pDC, LPRECT lpRect)
{
	m_rcPosition = lpRect;
	Draw(pDC);
}

void CRemarkObject::Draw(CDC* pDC)
{
	pDC->IntersectClipRect(m_rcPosition);
	
    UINT nMode = pDC->SetBkMode(TRANSPARENT); 

    double dRatioX = (double)m_rcPosition.Width() / m_size.cx; 
    double dRatioY = (double)m_rcPosition.Height() / m_size.cy; 
    POSITION pos = m_listRemark.GetTailPosition(); 
    while(pos != NULL)
    {
        CBaseRemark* pRemark = (CBaseRemark*)m_listRemark.GetPrev(pos); 
        pRemark->Draw(pDC, m_rcPosition.TopLeft(), dRatioX, dRatioY); 
    }

    CPen* pOldPen = (CPen*)pDC->SelectStockObject(BLACK_PEN); 
    CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(NULL_BRUSH); 
    pDC->Rectangle(m_rcPosition);
    pDC->SelectObject(pOldBrush); 
    pDC->SelectObject(pOldPen); 
    pDC->SetBkMode(nMode); 
    pDC->SelectClipRgn(NULL); 
}

CRect CRemarkObject::GetRect()
{
    return m_rcPosition; 
}

void CRemarkObject::SetRect(CRect rect)
{
    m_rcPosition = rect; 
}

void CRemarkObject::SetOriginalSize(CSize size)
{
    m_size = size; 
}

void CRemarkObject::AddRemark(CBaseRemark* pRemark)
{
    if(pRemark != NULL)
    {
        m_listRemark.AddHead(pRemark);
        CBaseRemark* pNew = pRemark->Clone(); 
        pNew->m_bAdd = FALSE; 
        m_listUndo.AddHead(pNew); 
        while(m_listRedo.GetCount() > 0)
            delete m_listRedo.RemoveHead(); 
    }
}

void CRemarkObject::AddPoint(CPoint point)
{
    if(m_listRemark.GetHeadPosition() != NULL)
    {
        CBaseRemark* pBase = (CBaseRemark*)m_listRemark.GetHead(); 
        if(pBase->IsKindOf(RUNTIME_CLASS(CPolygonRemark)))
        {
            CPolygonRemark* pPolygon = (CPolygonRemark*)pBase; 
            pPolygon->AddPoint(point); 
        }
    }
}

void CRemarkObject::SetLastPoint(CPoint point)
{
    if(m_listRemark.GetHeadPosition() != NULL)
    {
        CBaseRemark* pBase = (CBaseRemark*)m_listRemark.GetHead(); 
        pBase->SetEndPoint(point); 
    }
}

void CRemarkObject::SelectObject(CPoint point)
{
    ClearSelection(); 
    POSITION pos = m_listRemark.GetHeadPosition(); 
    while(pos != NULL)
    {
        CBaseRemark* pBase = (CBaseRemark*)m_listRemark.GetNext(pos); 
        if(point == CPoint(-1, -1) || pBase->HitTest(point))
        {
            pBase->m_bSelected = TRUE; 
            return; 
        }
    }
}

void CRemarkObject::ClearSelection()
{
    POSITION pos = m_listRemark.GetHeadPosition(); 
    while(pos != NULL)
    {
        CBaseRemark* pBase = (CBaseRemark*)m_listRemark.GetNext(pos); 
        pBase->m_bSelected = FALSE; 
    }
}

CBaseRemark* CRemarkObject::GetSelectObject()
{
    POSITION pos = m_listRemark.GetHeadPosition(); 
    while(pos != NULL)
    {
        CBaseRemark* pBase = (CBaseRemark*)m_listRemark.GetNext(pos); 
        if(pBase->m_bSelected)
            return pBase; 
    }

    return NULL; 
}

void CRemarkObject::RemoveObject(CBaseRemark* pRemark)
{
    if(pRemark == NULL)
        return; 

    POSITION pos = m_listRemark.Find(pRemark); 
    if(pos != NULL)
    {
        m_listRemark.RemoveAt(pos); 
        pRemark->m_bAdd = TRUE; 
        pRemark->m_bSelected = FALSE; 
        m_listUndo.AddHead(pRemark); 
        while(m_listRedo.GetCount() > 0)
            delete m_listRedo.RemoveHead(); 
    }
}

BOOL CRemarkObject::CanUndo()
{
    return !m_listUndo.IsEmpty(); 
}

BOOL CRemarkObject::CanRedo()
{
    return !m_listRedo.IsEmpty(); 
}

void CRemarkObject::Undo()
{
    if(m_listUndo.IsEmpty())
        return; 

    CBaseRemark* pRemark = (CBaseRemark*)m_listUndo.RemoveHead();
    if(pRemark->m_bAdd)
    {
        m_listRemark.AddHead(pRemark); 
        CBaseRemark* pNew = pRemark->Clone(); 
        pNew->m_bAdd = FALSE; 
        m_listRedo.AddHead(pNew); 
    }
    else 
    {
        delete pRemark; 
        if(m_listRemark.GetCount() > 0)
        {
            pRemark = (CBaseRemark*)m_listRemark.RemoveHead(); 
            pRemark->m_bAdd = TRUE; 
            pRemark->m_bSelected = FALSE; 
            m_listRedo.AddHead(pRemark); 
        }
    }
}

void CRemarkObject::Redo()
{
    if(m_listRedo.IsEmpty())
        return; 

    CBaseRemark* pRemark = (CBaseRemark*)m_listRedo.RemoveHead(); 
    if(pRemark->m_bAdd)
    {
        m_listRemark.AddHead(pRemark); 
        CBaseRemark* pNew = pRemark->Clone(); 
        pNew->m_bAdd = FALSE; 
        m_listUndo.AddHead(pNew); 
    }
    else 
    {
        delete pRemark; 
        if(m_listRemark.GetCount() > 0)
        {
            pRemark = (CBaseRemark*)m_listRemark.RemoveHead(); 
            pRemark->m_bAdd = TRUE; 
            pRemark->m_bSelected = FALSE; 
            m_listUndo.AddHead(pRemark); 
        }
    }
}

void CRemarkObject::Clear()
{
    while(m_listRemark.GetCount() > 0)
        delete m_listRemark.RemoveHead(); 

    while(m_listUndo.GetCount() > 0)
        delete m_listUndo.RemoveHead(); 

    while(m_listRedo.GetCount() > 0)
        delete m_listRedo.RemoveHead(); 
}

BOOL CRemarkObject::LoadFromFile(CString strFileName, BOOL bReportErr /* = TRUE */)
{
    CStdioFile file; 
    CFileException e; 
    if (!file.Open(strFileName, CFile::modeRead | CFile::shareDenyWrite | CFile::typeText, &e))
    {
		if (bReportErr)
			e.ReportError();  
        return FALSE; 
    }

	int iFind  = strFileName.ReverseFind(_T('\\'));
	if (iFind > 0)
		CBaseRemark::m_strFPath = strFileName.Left(iFind);
	
    CXmlArchive xml(&file, FALSE); 
    //xml.AddClass(RUNTIME_CLASS(CRectRemark)); 
    //xml.AddClass(RUNTIME_CLASS(CLineRemark)); 
    //xml.AddClass(RUNTIME_CLASS(CPictureRemark)); 
    //xml.AddClass(RUNTIME_CLASS(CPolygonRemark)); 
    CObList listAppend; 

    TRY
    {
        xml.ReadLine();             // warning.
        xml.ReadLine();             // xml version
        CString strVersion; 
        xml.ReadKey("RemarkSymbol"); 
        xml.ReadValue("Version", strVersion); 

        //SetFileVersion(strVersion); 
        //if(strVersion != CROSS_PLOT_VERSION)
        //{
        //    CString strWarning; //_T("创建文件的库版本[%s]与当前[%s]不同,仍然尝试打开吗?")
        //    strWarning.LoadString(IDS_CROSSPLOT_VERSION_DIFFERENT_PROMPT); 
        //    CString strPrompt; 
        //    strPrompt.Format(strWarning, strVersion, CROSS_PLOT_VERSION); 
        //    if(AfxMessageBox(strPrompt, MB_YESNO, 0) == IDNO)
        //        return FALSE; 
        //}

        xml.SerializeObject(&m_size, _T("OriginalSize")); 
        xml.SerializeObject(&m_rcPosition, _T("Position")); 
        SerializeObjectList(xml, &m_listRemark); 
        xml.ReadKey("RemarkSymbol", FALSE); 
    }
	CATCH_ALL(e)
	{
		if (bReportErr)
			e->ReportError(); 
        e->Delete(); 
        file.Close(); 
        return FALSE; 
	}
	END_CATCH_ALL
    
    file.Close(); 
    return TRUE; 
}

BOOL CRemarkObject::SaveToFile(CString strFileName)
{
    CStdioFile file; 
    CFileException e; 
    if(!file.Open(strFileName, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite | CFile::typeText, &e))
    {
        e.ReportError(); 
        return FALSE; 
    }
   
    CXmlArchive xml(&file, TRUE); 
    xml.WriteLine(_T("<!-- WARNING: DO NOT EDIT THIS FILE MANUALLY, PROGRAM CAN NOT READ IT AGAIN IF BROKEN. -->\n"));
    xml.WriteLine(_T("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n")); 
    xml.WriteKey(_T("RemarkSymbol")); 
    xml.WriteValue(_T("Version"), _T("01.02.00.0500")); 
    xml.SerializeObject(&m_size, _T("OriginalSize")); 
    xml.SerializeObject(&m_rcPosition, _T("Position")); 
    SerializeObjectList(xml, &m_listRemark); 
    xml.WriteKey(_T("RemarkSymbol"), FALSE);    
    file.Close(); 
    return TRUE; 
}

void CRemarkObject::SerializeObjectList(CXmlArchive& xml, CObList* pList)
{
    if(xml.IsStoring())
    {
        xml.WriteKey(_T("FigureCollection")); 
        xml.WriteValue(_T("Count"), (DWORD)pList->GetCount()); 

        POSITION pos = pList->GetHeadPosition(); 
        while(pos != NULL)
        {
            CBaseRemark* pRemark = (CBaseRemark*)pList->GetNext(pos); 
            CRuntimeClass* pClass = pRemark->GetRuntimeClass(); 
            xml.WriteKey(CString(pClass->m_lpszClassName)); 
            pRemark->Serialize(xml); 
            xml.WriteKey(CString(pClass->m_lpszClassName), FALSE); 
        }

        xml.WriteKey(_T("FigureCollection"), FALSE); 
    }
    else 
    {
        int nCount; 
        xml.ReadKey(_T("FigureCollection")); 
        xml.ReadValue(_T("Count"), (DWORD*)&nCount); 

        CString strClassName; 
        CBaseRemark* pRemark = NULL;
        for(int i=0; i<nCount; i++)
        {
            pRemark = (CBaseRemark*)xml.ReadClass(strClassName); 
            if(pRemark != NULL)
            {
                pList->AddTail(pRemark);       // 必须先加入列表，防止产生异常后内存泄漏
                pRemark->Serialize(xml); 
                xml.ReadKey(strClassName, FALSE); 
            }
        }

        xml.ReadKey(_T("FigureCollection"), FALSE); 
    }
}

CRemarkObject* PASCAL CRemarkObject::LoadSymbol(CString strFileName)
{
    CRemarkObject* pRemark = new CRemarkObject(); 
    if(pRemark->LoadFromFile(strFileName))
        return pRemark; 
    else 
        delete pRemark; 
    return NULL; 
}

//////////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CBaseRemark, CObject, 0)

CBaseRemark::CBaseRemark()
{
    m_bSelected = FALSE;
    m_bAdd = FALSE;
    memset(&m_logPen, 0, sizeof(LOGPEN)); 
    m_logPen.lopnStyle = PS_SOLID; 
    m_logPen.lopnWidth.x = 1; 
    m_logPen.lopnColor = RGB(0, 0, 0); 
    memset(&m_logBrush, 0, sizeof(LOGBRUSH)); 
    m_logBrush.lbStyle = BS_NULL; 
    m_logBrush.lbColor = RGB(255, 255, 255); 
    m_logBrush.lbHatch = HS_HORIZONTAL; 
}

CBaseRemark::~CBaseRemark()
{
}

void CBaseRemark::Serialize(CArchive& ar)
{
    if(ar.IsStoring())
    {
        ar << m_logPen.lopnStyle; 
        ar << m_logPen.lopnColor; 
        ar << m_logPen.lopnWidth.x; 

        ar << m_logBrush.lbStyle; 
        ar << m_logBrush.lbColor; 
        ar << m_logBrush.lbHatch; 
    }
    else 
    {
        ar >> m_logPen.lopnStyle; 
        ar >> m_logPen.lopnColor; 
        ar >> m_logPen.lopnWidth.x; 

        ar >> m_logBrush.lbStyle; 
        ar >> m_logBrush.lbColor; 
        ar >> m_logBrush.lbHatch; 
    }
}

void CBaseRemark::Serialize(CXmlArchive& xml)
{
    //if(xml.IsStoring())
    //{
    //}
    //else 
    //{
    //}
    xml.SerializeObject(&m_logPen, _T("Pen")); 
    xml.SerializeObject(&m_logBrush, _T("Brush")); 
}

void CBaseRemark::Draw(CDC* pDC, CPoint ptTopLeft, double dRatioX, double dRatioY)
{
}

HPEN CBaseRemark::GetPen()
{
    LOGPEN logPen; 
    memcpy(&logPen, &m_logPen, sizeof(LOGPEN)); 
    if(m_bSelected)
        logPen.lopnWidth.x += 3; 
    return ::CreatePenIndirect(&logPen);
}

HBRUSH CBaseRemark::GetBrush()
{
    return ::CreateBrushIndirect(&m_logBrush); 
}

void CBaseRemark::SetEndPoint(CPoint point)
{
}

BOOL CBaseRemark::HitTest(CPoint point)
{
    return FALSE;
}

BOOL CBaseRemark::IsHitLine(int x1, int y1, int x2, int y2, CPoint point)
{
    CRect rect = CRect(x1, y1, x2, y2); 
    rect.NormalizeRect(); 
    rect.InflateRect(HT_DISTANCE, HT_DISTANCE); 
    if(rect.Width() == 0)
        rect.right ++; 
    if(rect.Height() == 0)
        rect.bottom ++; 
    if(!rect.PtInRect(point))
        return FALSE; 

    double A, B, C; 
    if(x1 == x2)
    {
        A = -1; 
        B = 0; 
        C = x1; 
    }
    else 
    {
        A = (double)(y2 - y1) / (x2 - x1); 
        B = -1; 
        C = (double)(x2 * y1 - x1 * y2) / (x2 - x1); 
    }

    double dis = fabs((A * point.x + B * point.y + C) / sqrt(A*A + B*B)); 
    return dis <= HT_DISTANCE; 
}

CBaseRemark* CBaseRemark::Clone()
{
    CRuntimeClass* pClass = GetRuntimeClass(); 
    CBaseRemark* pNew = (CBaseRemark*)pClass->CreateObject(); 
    pNew->m_logPen = m_logPen; 
    pNew->m_logBrush = m_logBrush; 
    pNew->m_bSelected = FALSE; 
    return pNew; 
}

//////////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CRectRemark, CBaseRemark, 0)

CRectRemark::CRectRemark()
{
    m_rect.SetRectEmpty(); 
}

CRectRemark::~CRectRemark()
{
}

void CRectRemark::Serialize(CArchive& ar)
{
    CBaseRemark::Serialize(ar); 
    if(ar.IsStoring())
        ar << m_rect; 
    else 
        ar >> m_rect; 
}

void CRectRemark::Serialize(CXmlArchive& xml)
{
    CBaseRemark::Serialize(xml); 
    xml.SerializeObject(&m_rect, _T("Rectangle")); 
}

void CRectRemark::Draw(CDC* pDC, CPoint ptTopLeft, double dRatioX, double dRatioY)
{
    CRect rect = m_rect; 
    rect.left = ptTopLeft.x + (int)(rect.left * dRatioX + 0.5); 
    rect.top = ptTopLeft.y + (int)(rect.top * dRatioY + 0.5); 
    rect.right = ptTopLeft.x + (int)(rect.right * dRatioX + 0.5); 
    rect.bottom = ptTopLeft.y + (int)(rect.bottom * dRatioY + 0.5); 

    CPen pen; 
    pen.Attach(GetPen()); 
    CPen* pOldPen = (CPen*)pDC->SelectObject(&pen); 

    CBrush brush; 
    brush.Attach(GetBrush()); 
    CBrush* pOldBrush = (CBrush*)pDC->SelectObject(&brush); 

    pDC->Rectangle(rect); 
    pDC->SelectObject(pOldBrush); 
    pDC->SelectObject(pOldPen); 
}

void CRectRemark::SetEndPoint(CPoint point)
{
    m_rect.right = point.x; 
    m_rect.bottom = point.y; 
}

BOOL CRectRemark::HitTest(CPoint point)
{
    CRect rect, rect2; 
    rect = rect2 = m_rect; 
    rect.NormalizeRect(); 
    rect2.NormalizeRect(); 
    rect.InflateRect(HT_DISTANCE, HT_DISTANCE); 
    rect2.DeflateRect(HT_DISTANCE, HT_DISTANCE); 
    CRgn rgn, rgn2; 
    rgn.CreateRectRgnIndirect(rect); 
    rgn2.CreateRectRgnIndirect(rect2); 
    rgn.CombineRgn(&rgn, &rgn2, RGN_DIFF); 
    return rgn.PtInRegion(point); 
}

CBaseRemark* CRectRemark::Clone()
{
    CRectRemark* pNew = (CRectRemark*)CBaseRemark::Clone(); 
    pNew->m_rect = m_rect; 
    return pNew; 
}

//////////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CLineRemark, CRectRemark, 0)

CLineRemark::CLineRemark()
{
}

CLineRemark::~CLineRemark()
{
}

void CLineRemark::Serialize(CArchive& ar)
{
    CRectRemark::Serialize(ar); 
}

void CLineRemark::Draw(CDC* pDC, CPoint ptTopLeft, double dRatioX, double dRatioY)
{
    CPoint ptBegin = m_rect.TopLeft(); 
    CPoint ptEnd = m_rect.BottomRight(); 
    ptBegin.x = ptTopLeft.x + (int)(ptBegin.x * dRatioX + 0.5); 
    ptBegin.y = ptTopLeft.y + (int)(ptBegin.y * dRatioY + 0.5); 
    ptEnd.x = ptTopLeft.x + (int)(ptEnd.x * dRatioX + 0.5);
    ptEnd.y = ptTopLeft.y + (int)(ptEnd.y * dRatioY + 0.5); 

    CPen pen; 
    pen.Attach(GetPen()); 
    CPen* pOldPen = (CPen*)pDC->SelectObject(&pen); 

    pDC->MoveTo(ptBegin); 
    pDC->LineTo(ptEnd);
    pDC->SelectObject(pOldPen); 
}

BOOL CLineRemark::HitTest(CPoint point)
{
    return IsHitLine(m_rect.left, m_rect.top, m_rect.right, m_rect.bottom, point); 
}

//////////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CPictureRemark, CRectRemark, 0)

CPictureRemark::CPictureRemark()
{
    m_strFileName = ""; 
    m_bRepeat = FALSE; 
    m_bNeedLoad = TRUE; 
}

CPictureRemark::~CPictureRemark()
{
}

void CPictureRemark::Serialize(CArchive& ar)
{
    CRectRemark::Serialize(ar); 
    if(ar.IsStoring())
    {
        ar << m_strFileName; 
        ar << m_bRepeat; 
    }
    else 
    {
        ar >> m_strFileName; 
        ar >> m_bRepeat; 
        m_bNeedLoad = TRUE; 
    }
}

void CPictureRemark::Serialize(CXmlArchive& xml)
{
    CRectRemark::Serialize(xml); 
    if(xml.IsStoring())
    {
        xml.WriteValue(_T("ImageFileName"), m_strFileName); 
        xml.WriteValue(_T("IsRepeat"), (DWORD)m_bRepeat); 
    }
    else 
    {
        xml.ReadValue(_T("ImageFileName"), m_strFileName); 
        xml.ReadValue(_T("IsRepeat"), (DWORD*)&m_bRepeat);
    }
}

void CPictureRemark::Draw(CDC* pDC, CPoint ptTopLeft, double dRatioX, double dRatioY)
{
    if (m_bNeedLoad)
        LoadBitmap(m_strFileName); 
        
    CRect rect; 
    if (m_bRepeat)
    {
        rect.left = ptTopLeft.x + (int)(m_rect.left * dRatioX + 0.5); 
        rect.top = ptTopLeft.y + (int)(m_rect.top * dRatioY + 0.5); 
        rect.right = ptTopLeft.x + (int)(m_rect.right * dRatioX + 0.5); 
        rect.bottom = ptTopLeft.y + (int)(m_rect.bottom * dRatioY + 0.5); 
    }
    else 
    {
        CPoint ptCenter = m_rect.CenterPoint(); 
        ptCenter.x = ptTopLeft.x + (int)(ptCenter.x * dRatioX + 0.5); 
        ptCenter.y = ptTopLeft.y + (int)(ptCenter.y * dRatioY + 0.5); 
        rect = CRect(ptCenter, CSize(0, 0)); 
        
        CSize sizeDefault;
        if (m_bitmap.GetSafeHandle() == NULL)
        {
            sizeDefault = CSize(16, 16); 
            pDC->DPtoLP(&sizeDefault); 
            rect.InflateRect(sizeDefault); 
            //rect.InflateRect(16, 16); 
        }
        else 
        {
            BITMAP bm; 
            m_bitmap.GetBitmap(&bm);
            sizeDefault = CSize((int)(bm.bmWidth/2.0+0.5), (int)(bm.bmHeight/2.0+0.5));
			if (pDC->IsPrinting())
			{
				CWindowDC dc(NULL);
				int nMapMode = dc.SetMapMode(MM_LOMETRIC);
				CSize szText(sizeDefault.cx, sizeDefault.cy);
				dc.DPtoLP(&szText);
				dc.SetMapMode(nMapMode);
				sizeDefault.cx = szText.cx;
				sizeDefault.cy = szText.cy;
			}
			else
				pDC->DPtoLP(&sizeDefault);
            rect.InflateRect(sizeDefault);
            //rect.InflateRect((int)(bm.bmWidth/2.0+0.5), (int)(bm.bmHeight/2.0+0.5));             
        }
    }

    if (pDC->GetMapMode() != MM_TEXT)
    {
        int temp = rect.top; 
        rect.top = rect.bottom; 
        rect.bottom = temp; 
    }

    if (m_bitmap.GetSafeHandle() == NULL)
    {
        CPen pen; 
        pen.Attach(GetPen()); 
        CPen* pOldPen = (CPen*)pDC->SelectObject(&pen); 

        CBrush brush; 
        brush.Attach(GetBrush()); 
        CBrush* pOldBrush = (CBrush*)pDC->SelectObject(&brush); 

        pDC->Rectangle(rect); 
        pDC->SelectObject(pOldPen); 
        pDC->SelectObject(pOldBrush); 
    }
    else 
    {
        BITMAP bm; 
        m_bitmap.GetBitmap(&bm);       
        CPen pen; 
        pen.Attach(GetPen()); 
    
        if (m_bRepeat)
        {
            CBrush brush(&m_bitmap); 
            CPen* pOldPen = (CPen*)pDC->SelectObject(&pen); 
            CBrush* pOldBrush = pDC->SelectObject(&brush); 
            pDC->SetBrushOrg(rect.TopLeft()); 
            pDC->Rectangle(rect); 
            pDC->SelectObject(pOldBrush); 
            pDC->SelectObject(pOldPen); 
        }
        else
        {
			DrawPicture(pDC->GetSafeHdc(), rect.left, rect.top, rect.Width(), rect.Height());
			
//             CDC dc;
//             dc.CreateCompatibleDC(pDC); 
//             CBitmap* pOldBitmap = (CBitmap*)dc.SelectObject(&m_bitmap); 
//             pDC->StretchBlt(rect.left, rect.top, rect.Width(), rect.Height(), 
//                 &dc, 0, 0, bm.bmWidth, bm.bmHeight, SRCAND); 
//             dc.SelectObject(pOldBitmap);

            if (m_bSelected)
            {
                CPen* pOldPen = (CPen*)pDC->SelectObject(&pen); 
                CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(NULL_BRUSH); 
                pDC->Rectangle(rect); 
                pDC->SelectObject(pOldBrush); 
                pDC->SelectObject(pOldPen); 
            }
        }
    }
}

BOOL CPictureRemark::LoadBitmap(CString strFileName)
{
    m_bNeedLoad = FALSE; 
    m_bitmap.DeleteObject(); 
    m_strFileName = strFileName; 
    HBITMAP hBitmap = (HBITMAP)::LoadImage(NULL, strFileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	if (NULL == hBitmap)
	{
		int iFind = strFileName.ReverseFind(_T('\\'));
		if (iFind > 0)
		{
			strFileName = strFileName.Right(strFileName.GetLength() - iFind);
			strFileName = m_strFPath + strFileName; 
			hBitmap = (HBITMAP)::LoadImage(NULL, strFileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			m_strFileName = strFileName;
		}
	}

	LoadPicture(m_strFileName);

    BOOL bRes = m_bitmap.Attach(hBitmap);
    if (bRes && !m_bRepeat)
    {
        BITMAP bm; 
        m_bitmap.GetBitmap(&bm); 
        CRect rect(m_rect.CenterPoint(), CSize(0, 0)); 
        rect.InflateRect((int)(bm.bmWidth/2.0+0.5), (int)(bm.bmHeight/2.0+0.5)); 
        m_rect = rect; 
    }
    return bRes; 
}

void CPictureRemark::SetEndPoint(CPoint point)
{
    if(m_bRepeat)
        CRectRemark::SetEndPoint(point); 
    else 
    {
        m_rect = CRect(point, CSize(0,0)); 
        m_rect.InflateRect(16, 16); 
    }
}

CBaseRemark* CPictureRemark::Clone()
{
    CPictureRemark* pNew = (CPictureRemark*)CRectRemark::Clone(); 
    pNew->m_strFileName = m_strFileName; 
    pNew->m_bNeedLoad = TRUE; 
    return pNew; 
}

//////////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CPolygonRemark, CBaseRemark, 0)

CPolygonRemark::CPolygonRemark()
{
    m_nCount = 0; 
    m_pPoints = NULL; 
}

CPolygonRemark::~CPolygonRemark()
{
    if(m_pPoints != NULL)
        delete[] m_pPoints; 
}

void CPolygonRemark::Serialize(CArchive& ar)
{
    CBaseRemark::Serialize(ar); 
    if(ar.IsStoring())
    {
        ar << m_nCount; 
        if(m_nCount > 0 && m_pPoints != NULL)
        {
            for(int i=0; i<m_nCount; i++)
                ar << m_pPoints[i]; 
        }
    }
    else 
    {
        ar >> m_nCount; 
        if(m_pPoints != NULL)
            delete[] m_pPoints; 

        if(m_nCount > 0)
        {
            m_pPoints = new POINT[m_nCount]; 
            for(int i=0; i<m_nCount; i++)
                ar >> m_pPoints[i]; 
        }
    }
}

void CPolygonRemark::Serialize(CXmlArchive& xml)
{
    CBaseRemark::Serialize(xml); 
    if(xml.IsStoring())
    {
        xml.WriteKey(_T("PointCollection")); 
        xml.WriteValue(_T("PointCount"), (DWORD)m_nCount); 
        for(int i=0; i<m_nCount; i++)
            xml.SerializeObject(&m_pPoints[i], _T("Point")); 
        xml.WriteKey(_T("PointCollection"), FALSE); 
    }
    else 
    {
        xml.ReadKey(_T("PointCollection")); 
        xml.ReadValue(_T("PointCount"), (DWORD*)&m_nCount); 
        if(m_nCount > 0)
        {
            if(m_pPoints != NULL)
                delete[] m_pPoints; 
            m_pPoints = new POINT[m_nCount]; 
            memset(m_pPoints, 0, m_nCount * sizeof(POINT)); 
        }

        for(int i=0; i<m_nCount; i++)
            xml.SerializeObject(&m_pPoints[i], _T("Point")); 
        xml.ReadKey(_T("PointCollection"), FALSE); 
    }
}

void CPolygonRemark::Draw(CDC* pDC, CPoint ptTopLeft, double dRatioX, double dRatioY)
{
    if(m_nCount < 2 || m_pPoints == NULL)
        return; 

    LPPOINT pPoints = new POINT[m_nCount]; 
    memcpy(pPoints, m_pPoints, m_nCount * sizeof(POINT)); 
    for(int i=0; i<m_nCount; i++)
    {
        pPoints[i].x = ptTopLeft.x + (int)(pPoints[i].x * dRatioX + 0.5); 
        pPoints[i].y = ptTopLeft.y + (int)(pPoints[i].y * dRatioY + 0.5); 
    }

    CPen pen; 
    pen.Attach(GetPen()); 
    CPen* pOldPen = (CPen*)pDC->SelectObject(&pen); 

    CBrush brush; 
    brush.Attach(GetBrush()); 
    CBrush* pOldBrush = (CBrush*)pDC->SelectObject(&brush); 

    pDC->Polygon(pPoints, m_nCount); 
    pDC->SelectObject(pOldBrush); 
    pDC->SelectObject(pOldPen); 
    delete[] pPoints; 
}

void CPolygonRemark::SetEndPoint(CPoint point)
{
    if(m_nCount == 0 || m_pPoints == NULL)
        return; 

    m_pPoints[m_nCount-1] = point; 
}

BOOL CPolygonRemark::HitTest(CPoint point)
{
    if(m_nCount == 0 || m_pPoints == NULL)
        return FALSE; 

    for(int i=0; i<m_nCount; i++)
    {
        POINT pt1 = m_pPoints[i]; 
        POINT pt2 = m_pPoints[(i+1)%m_nCount]; 
        if(IsHitLine(pt1.x, pt1.y, pt2.x, pt2.y, point))
            return TRUE; 
    }

    return FALSE; 
}

void CPolygonRemark::AddPoint(CPoint point)
{
    m_nCount ++;     
    LPPOINT pPoints = m_pPoints; 
    m_pPoints = new POINT[m_nCount]; 
    if(m_nCount - 1 > 0)
        memcpy(m_pPoints, pPoints, (m_nCount - 1) * sizeof(POINT)); 

    m_pPoints[m_nCount-1] = point; 
    if(pPoints != NULL)
        delete[] pPoints; 
}

CBaseRemark* CPolygonRemark::Clone()
{
    CPolygonRemark* pNew = (CPolygonRemark*)CBaseRemark::Clone(); 
    if(m_nCount > 0 && m_pPoints != NULL)
    {
        pNew->m_nCount = m_nCount; 
        pNew->m_pPoints = new POINT[m_nCount]; 
        memcpy(pNew->m_pPoints, m_pPoints, m_nCount * sizeof(POINT)); 
    }
    return pNew; 
}


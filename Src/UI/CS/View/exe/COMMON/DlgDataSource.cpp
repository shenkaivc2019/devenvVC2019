// DlgDataSource.cpp : implementation file
//

#include "stdafx.h"
//#include "resource.h"
#include "DlgDataSource.h"
#include "Curve.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgDataSource dialog


CDlgDataSource::CDlgDataSource(vec_ic* pDatas, CCurve* pCurve, CWnd* pParent /* = NULL */)
	: CDialog(IDD_DATA_SOURCE, pParent)
{
	//{{AFX_DATA_INIT(CDlgDataSource)
	m_nIndex1 = 0;
	m_nIndex2 = 0;
	//}}AFX_DATA_INIT
	m_pDatas = pDatas;
	m_pCurve = pCurve;
	if (m_pCurve)
	{
		m_nIndex1 = m_pCurve->m_wIndex1;
		m_nIndex2 = m_pCurve->m_wIndex2;
	}
}

CDlgDataSource::~CDlgDataSource()
{

}


void CDlgDataSource::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgDataSource)
	DDX_Control(pDX, IDC_COMBO1, m_cbDataSrc);
	DDX_Text(pDX, IDC_EDIT1, m_nIndex1);
	DDV_MinMaxInt(pDX, m_nIndex1, 0, 32767);
	DDX_Text(pDX, IDC_EDIT2, m_nIndex2);
	DDV_MinMaxInt(pDX, m_nIndex2, 0, 32767);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgDataSource, CDialog)
	//{{AFX_MSG_MAP(CDlgDataSource)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnSelchangeCombo1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgDataSource message handlers

BOOL CDlgDataSource::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CString strText;
	GetWindowText(strText);
	CString strTextNew;
	strTextNew.Format(strText, m_pCurve->m_strName);
	SetWindowText(strTextNew);

	m_cbDataSrc.ResetContent();
	int nCount = m_pDatas->size();
	CString strSel;
	for (int i = 0; i < nCount; i++)
	{
		CCurve* pCurve = (CCurve*)m_pDatas->at(i);
		if (!pCurve->IsOpen())
			continue;
		CString string = pCurve->m_strName;
		if (pCurve->m_strSource.GetLength())
			string += "[" + pCurve->m_strSource + "]";

		int nItem = m_cbDataSrc.AddString(string);
		m_cbDataSrc.SetItemData(nItem, (DWORD_PTR)pCurve->m_pData);
		if (pCurve->m_pData == m_pCurve->m_pData)
			strSel = string;
	}

	if (strSel.GetLength())
	{
		m_cbDataSrc.SelectString(-1, strSel);
		OnSelchangeCombo1();
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CDlgDataSource::OnOK() 
{
	if (!UpdateData())
		return;

	int nSel = m_cbDataSrc.GetCurSel();
	if (nSel != CB_ERR)
	{
		CCurveData* pData = (CCurveData*)m_cbDataSrc.GetItemData(nSel);
		if ((pData != m_pCurve->m_pData) && (pData != NULL))
			m_pCurve->AttachData(pData);
	}

	m_pCurve->m_wIndex1 = m_nIndex1;
	m_pCurve->m_wIndex2 = m_nIndex2;

	EndDialog(IDOK);
}

void CDlgDataSource::OnSelchangeCombo1() 
{
	int nSel = m_cbDataSrc.GetCurSel();
	if (nSel != CB_ERR)
	{
		CCurveData* pData = (CCurveData*)m_cbDataSrc.GetItemData(nSel);
		GetDlgItem(IDC_EDIT1)->EnableWindow(pData->m_nDimension > 1);
		GetDlgItem(IDC_EDIT2)->EnableWindow(pData->m_nDimension > 2);
		CString strText;
		strText.Format(_T("< %hd"), pData->GetPointFrame());
		SetDlgItemText(IDC_STATIC1, strText);
		strText.Format(_T("< %hd"), pData->GetArrayCount());
		SetDlgItemText(IDC_STATIC2, strText);
	}
}

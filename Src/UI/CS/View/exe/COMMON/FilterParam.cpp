// FilterParam.cpp: implementation of the CFilterParam class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FilterParam.h"
#include "XMLSettings.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void CFilterParams::Serialize(CXMLSettings& xml)
{
	if (xml.IsStoring())
	{
		xml.Write(_T("FilterName"), m_strFilter);
		xml.Write(_T("FilterParamCount"), m_fpList.GetCount());
		POSITION pos = m_fpList.GetHeadPosition();
		for (int i = 0; pos != NULL; i++)
		{
			CFilterParam* pFP = m_fpList.GetNext(pos);
			if (xml.CreateKey(_T("FilterParam%02d"), i))
			{
				xml.Write(_T("PName"), pFP->m_strName);
				xml.Write(_T("PValue"), pFP->m_dValue);
				xml.Write(_T("PDesc"), pFP->m_strDesc);
				xml.Back();
			}
		}
	}
	else
	{
		xml.Read(_T("FilterName"), m_strFilter);
		
		int nCount = 0;
		xml.Read(_T("FilterParamCount"), nCount);
		ClearParams();
		for (int i = 0; i < nCount; i++)
		{
			CFilterParam* pFP = new CFilterParam;
			if (xml.Open(_T("FilterParam%02d"), i))
			{
				xml.Read(_T("PName"), pFP->m_strName);
				xml.Read(_T("PValue"), pFP->m_dValue);
				xml.Read(_T("PDesc"), pFP->m_strDesc);
				xml.Back();
			}
			m_fpList.AddTail(pFP);
		}
	}
}

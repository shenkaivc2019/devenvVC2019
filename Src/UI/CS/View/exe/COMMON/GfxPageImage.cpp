// ProoPageImageDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "GfxPageImage.h"
#include "Curve.h"
#include "GraphWnd.h"
#include "MainFrm.h"
#include "Utility.h"
#include "ULFilter.h"
#include "ComConfig.h"
#include "Units.h"
#include "DlgGradient.h"
#include "Sheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CUnits* g_units;

/////////////////////////////////////////////////////////////////////////////
// CGfxPageImage dialog


CGfxPageImage::CGfxPageImage(CWnd* pParent /*=NULL*/)
	: CGfxPage(IDD_GFX_PAGE_IMAGE, pParent)
{
	//{{AFX_DATA_INIT(CGfxPageImage)
	m_bDisplay = FALSE;
	m_bPrint = FALSE;
	m_bSave = FALSE;
	m_fLeftValue = 0.0;
	m_fRightValue = 0.0;
	m_fEndPosition = 0.0;
	m_fStartPosition = 0.0;
	m_lDepthOffset = 0;
	m_strName = _T("");
	m_strName2 = _T("");
	m_strUnit = _T("");
	m_lTimeDelay = 0;
	m_iTimeInter = 1;
	//}}AFX_DATA_INIT	
}


void CGfxPageImage::DoDataExchange(CDataExchange* pDX)
{
	CGfxPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGfxPageImage)
	DDX_Control(pDX, IDC_CB_COLORRANK, m_cbColorRank);
	DDX_Check(pDX, IDC_CHECK_DISPLAY, m_bDisplay);
	DDX_Check(pDX, IDC_CHECK_PRINT, m_bPrint);
	DDX_Check(pDX, IDC_CHECK_SAVE, m_bSave);
	DDX_Text(pDX, IDC_EDIT_LEFTVALUE, m_fLeftValue);
	DDX_Text(pDX, IDC_EDIT_RIGHTVALUE, m_fRightValue);
	DDX_Text(pDX, IDC_EDIT_ENDPOS, m_fEndPosition);
	DDX_Text(pDX, IDC_EDIT_STARTPOS, m_fStartPosition);
	DDX_Text(pDX, IDC_EDIT_NAME, m_strName);
	DDX_Text(pDX, IDC_EDIT_NAME2, m_strName2);
	DDX_Text(pDX, IDC_EDIT_UNIT, m_strUnit);
	DDX_Control(pDX, IDC_LIGHTCOLOR, m_btnLightColor);
	DDX_Control(pDX, IDC_DARKCOLOR, m_btnDarkColor);
	DDX_Text(pDX, IDC_EDIT_TIMEINTER, m_iTimeInter);
	DDX_Text(pDX, IDC_EDIT_TIMEDELAY, m_lTimeDelay);
	//}}AFX_DATA_MAP
	g_units->DDX_Depth(pDX, IDC_DEPTHOFFSET, m_lDepthOffset);
}


BEGIN_MESSAGE_MAP(CGfxPageImage, CGfxPage)
	//{{AFX_MSG_MAP(CGfxPageImage)
	ON_EN_CHANGE(IDC_EDIT_ENDPOS, OnChangeEditEndpos)
	ON_EN_CHANGE(IDC_EDIT_STARTPOS, OnChangeEditStartpos)
	ON_EN_CHANGE(IDC_DEPTHOFFSET, OnChangeDepthoffset)
	ON_EN_KILLFOCUS(IDC_EDIT_LEFTVALUE, OnKillFocusLeftValue)
	ON_EN_CHANGE(IDC_EDIT_NAME, OnChangeEditName)
	ON_EN_KILLFOCUS(IDC_EDIT_RIGHTVALUE, OnKillFocusRightValue)
	ON_EN_CHANGE(IDC_EDIT_UNIT, OnChangeEditUnit)
	ON_EN_KILLFOCUS(IDC_EDIT_ENDPOS, OnKillfocusEditEndpos)
	ON_EN_KILLFOCUS(IDC_EDIT_STARTPOS, OnKillfocusEditStartpos)
	ON_BN_CLICKED(IDC_DARKCOLOR, OnDarkcolor)
	ON_BN_CLICKED(IDC_LIGHTCOLOR, OnLightcolor)
	ON_WM_DESTROY()
	ON_EN_CHANGE(IDC_EDIT_TIMEINTER, OnChangeEditTimeinter)
	ON_EN_CHANGE(IDC_EDIT_TIMEDELAY, OnChangeEditTimedelay)
	ON_EN_KILLFOCUS(IDC_EDIT_TIMEINTER, OnKillfocusEditTimeinter)
	ON_EN_KILLFOCUS(IDC_EDIT_TIMEDELAY, OnKillfocusEditTimedelay)
	ON_BN_CLICKED(IDC_COLOR, OnColor)
	ON_CBN_SELCHANGE(IDC_CB_COLORRANK, RefreshData)
	ON_BN_CLICKED(IDC_CHECK_DISPLAY, RefreshData)
	ON_BN_CLICKED(IDC_CHECK_PRINT, RefreshData)
	ON_BN_CLICKED(IDC_CHECK_SAVE, RefreshData)
	ON_EN_KILLFOCUS(IDC_DEPTHOFFSET, RefreshData)
	ON_BN_CLICKED(IDC_COLORR, OnColorr)
	//}}AFX_MSG_MAP
	
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGfxPageImage message handlers


void CGfxPageImage::InitCurve()
{
	CString str;
	if (m_pCurve != NULL)
	{
		m_bDisplay = m_pCurve->IsVisible();
		m_bPrint = m_pCurve->IsPrint();
		m_bSave = m_pCurve->IsSave();
		m_strName = m_pCurve->m_strName;
		m_strName2 = m_pCurve->m_strIDName;
		m_strUnit = m_pCurve->Unit();
		m_lDepthOffset = m_pCurve->DepthOffset();
		m_fLeftValue = m_pCurve->m_LeftValue;
		m_fRightValue= m_pCurve->m_RightValue;
		
		if (m_pCurve->m_PlotInfo.PlotENName.CompareNoCase(_T("WDS")))
		{
			m_iTimeInter = m_pCurve->TimeInter();
			m_lTimeDelay = m_pCurve->TimeDelay();
			GetDlgItem(IDC_STATIC1)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STATIC2)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STATIC3)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC4)->ShowWindow(SW_HIDE);
		}
		else
		{
			m_iTimeInter = m_pCurve->m_nDivision;
			m_lTimeDelay = m_pCurve->m_nPointDivision;
			GetDlgItem(IDC_STATIC1)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC2)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC3)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STATIC4)->ShowWindow(SW_SHOW);
		}

		if (m_pGraph->m_pSheet->m_nDriveMode)
		{
			GetDlgItem(IDC_STATIC_DEPTHOFFSET)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_DEPTHOFFSET)->ShowWindow(SW_SHOW);
		}
		else
		{
			GetDlgItem(IDC_STATIC_DEPTHOFFSET)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_DEPTHOFFSET)->ShowWindow(SW_HIDE);
		}
		
		if (m_pCurve->m_PlotInfo.PlotENName == "VDL")
		{
			m_fStartPosition = m_pCurve->m_dStartPos * m_iTimeInter;
			int n = m_pCurve->m_dEndPos + 1;
			if (n < 0)
				n = 0;
			m_fEndPosition = n * m_iTimeInter;
		}
		else
		{
			m_fStartPosition = m_pCurve->m_dStartPos;
			m_fEndPosition = m_pCurve->m_dEndPos;
		}

		//str.Format("%d", m_pCurve->m_nColorRank);
		//m_cbColorRank.SetWindowText(str);
		float n = log10((float)m_pCurve->m_nColorRank) / log10(2);	
		if ((int)n - 1 >= 0)
		    m_cbColorRank.SetCurSel((int)n - 1);
		m_btnDarkColor.SetColor(m_pCurve->m_crDarkColor);		
		m_btnLightColor.SetColor(m_pCurve->m_crLightColor);
		
		UpdateData(FALSE);
	}
}

void CGfxPageImage::RefreshData()
{
	CString strTemp="";
	UpdateData(TRUE);
	if(m_pCurve!=NULL)
	{
		m_pCurve->m_strName = m_strName;
		m_pCurve->m_strIDName = m_strName2;
		m_pCurve->SetUnit((LPSTR)(LPCTSTR)m_strUnit);
		m_pCurve->m_LeftValue = m_fLeftValue;
		m_pCurve->m_RightValue = m_fRightValue;
		m_pCurve->SetDepthOffset(m_lDepthOffset);
		m_pCurve->Visible(m_bDisplay);
		m_pCurve->Print(m_bPrint);
		m_pCurve->Save(m_bSave);

// 		int nIndex = m_cbFilter.GetCurSel();
// 		if(nIndex!=CB_ERR)
// 			m_cbFilter.GetLBText(nIndex,strTemp);
// 		else
// 			m_cbFilter.GetWindowText(strTemp);
// 		int nItem = (int) atoi(strTemp);
// 		if(nItem>0)
//  			m_pCurve->m_Filter.SetNum(nItem);

		// 根据选择来设定滤波方法
/*		CString strFilter = "";
		int n = m_cbFilter.GetCurSel();
		if ( (CB_ERR<n) && (n<m_cbFilter.GetCount()-1) )		
			m_cbFilter.GetLBText(n, strFilter);
		
		m_pCurve->SetFilter(strFilter);
*/		m_pCurve->m_crDarkColor = m_btnDarkColor.GetColor();
		m_pCurve->m_crLightColor = m_btnLightColor.GetColor();

		// 获得用户输入的色阶
		CString strTemp ;
		int n = m_cbColorRank.GetCurSel();
		if( n != CB_ERR)		
			m_cbColorRank.GetLBText(n,strTemp);
		else
			m_cbColorRank.GetWindowText(strTemp);
		
		m_pCurve->m_nColorRank  = (int)atoi(strTemp); // 临时变量存储
		
		if (m_pCurve->m_PlotInfo.PlotENName == "VDL")
		{
			if (m_iTimeInter != 0)
			{
				m_pCurve->m_dStartPos = m_fStartPosition / m_iTimeInter;
				int n = m_fEndPosition / m_iTimeInter - 1;
				if (n < 0)
					n = 0;
				m_pCurve->m_dEndPos = n;
			}
		}
		else
		{
			m_pCurve->m_dEndPos = m_fEndPosition;		
			m_pCurve->m_dStartPos = m_fStartPosition;
		}

		if (m_pCurve->m_PlotInfo.PlotENName.CompareNoCase(_T("WDS")))
		{
			m_pCurve->TimeInter(m_iTimeInter);
			m_pCurve->TimeDelay(m_lTimeDelay);
		}
		else
		{
			m_pCurve->m_nDivision = m_iTimeInter;
			m_pCurve->m_nPointDivision = m_lTimeDelay;
		}		

		if (m_pGraph != NULL)
			m_pGraph->InvalidateAll();		
	}

}

void CGfxPageImage::OnChangeEditEndpos() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CGfxPage::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	CString str="";
	GetDlgItem(IDC_EDIT_ENDPOS)->GetWindowText(str);
	
	if(str.IsEmpty())
		return;	

	RefreshData();
	
}

void CGfxPageImage::OnChangeEditStartpos() 
{	
	CString str="";
	GetDlgItem(IDC_EDIT_STARTPOS)->GetWindowText(str);
	if(str.IsEmpty())	
		return;
	
	RefreshData();
	
}

void CGfxPageImage::OnChangeDepthoffset() 
{
// shenkun add, 2003-9-4
	CString strInput;
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_DEPTHOFFSET);
	pEdit->GetWindowText(strInput);
	if (IsDecimal(strInput))
	{
		RefreshData();
	}
}

// shenkun add, 2003-9-4
void CGfxPageImage::OnKillFocusLeftValue()
{
	CString strInput;
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_LEFTVALUE);
	pEdit->GetWindowText(strInput);
	
	double d;
	if (SimpleFloatParse(strInput, d))
	{
        int pos = 0, nPosition;
		pos = strInput.Find('.');
		if(pos == -1)
			nPosition = 0;
		else
			nPosition = strInput.GetLength() - pos -1;
		if(m_pCurve != NULL)
		{
			m_pCurve->m_nLeftDotNum = nPosition;
		}
		
		RefreshData();
	}
	else
	{
		pEdit->SetWindowText(DoubleToString(m_fLeftValue));
	}
}

void CGfxPageImage::OnChangeEditName() 
{
	CString str="";
	GetDlgItem(IDC_EDIT_NAME)->GetWindowText(str);
	if(str.IsEmpty())
		return;	
	
	RefreshData();
}

// shenkun add, 2003-9-4
void CGfxPageImage::OnKillFocusRightValue()
{
	CString strInput;
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_RIGHTVALUE);
	pEdit->GetWindowText(strInput);
	double d;
	if (SimpleFloatParse(strInput, d))
	{
		int pos = 0,nPosition;
		pos = strInput.Find('.');
		if(pos == -1)
			nPosition = 0;
		else
			nPosition = strInput.GetLength() - pos -1;
		if(m_pCurve != NULL)
		{
			m_pCurve->m_nRightDotNum = nPosition;
		}
		
		RefreshData();
	}
	else
	{
		pEdit->SetWindowText(DoubleToString(m_fRightValue));
	}
}

void CGfxPageImage::OnChangeEditUnit() 
{
	CString str="";
	GetDlgItem(IDC_DEPTHOFFSET)->GetWindowText(str);
	if(str.IsEmpty())
		return;	
	
	RefreshData();
}

CString CGfxPageImage::DoubleToString(double f)
{
	CString strInput;
	strInput.Format("%f", f);
	int iDotPos = strInput.Find('.');
	if (iDotPos == -1)
		return strInput;
	int iZeroCount = 0;
	for (int i = strInput.GetLength() - 1; i > iDotPos; i--)
	{
		if (strInput.GetAt(i) == '0')
			iZeroCount++;
		else
			break;
	}
	CString strOutput = strInput.Left(strInput.GetLength() - iZeroCount);
	if (strOutput.Right(1) == ".")
		strOutput = strOutput.Left(strOutput.GetLength() - 1);
	return strOutput;
}

void CGfxPageImage::OnKillfocusEditEndpos()
{
// shenkun add, 2003-10-27
	CString strInput;
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_ENDPOS);
	pEdit->GetWindowText(strInput);
	if (!IsDecimal(strInput))
	{
		pEdit->SetWindowText(DoubleToString(m_fEndPosition));
	}
}

void CGfxPageImage::OnKillfocusEditStartpos()
{
// shenkun add, 2003-10-27
	CString strInput;
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_STARTPOS);
	pEdit->GetWindowText(strInput);
	if (!IsDecimal(strInput))
	{
		pEdit->SetWindowText(DoubleToString(m_fStartPosition));
	}
}

BOOL CGfxPageImage::OnInitDialog() 
{
	CGfxPage::OnInitDialog();
	
	m_btnDarkColor.EnableOtherButton ("");
	m_btnDarkColor.SetColor ((COLORREF)-1);
	m_btnDarkColor.SetColumnsNumber (8);

	m_btnLightColor.EnableOtherButton ("");
	m_btnLightColor.SetColor ((COLORREF)-1);
	m_btnLightColor.SetColumnsNumber (8);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CGfxPageImage::OnDarkcolor() 
{
	// TODO: Add your control notification handler code here
	if (m_pCurve != NULL)
	{	
		m_pCurve->m_crDarkColor = m_btnDarkColor.GetColor();
		if (m_pGraph != NULL)
			m_pGraph->InvalidateAll();
	}
}

void CGfxPageImage::OnLightcolor() 
{
	// TODO: Add your control notification handler code here
	if (m_pCurve != NULL)
	{	
		m_pCurve->m_crLightColor=m_btnLightColor.GetColor();
		if (m_pGraph != NULL)
			m_pGraph->InvalidateAll();
	}
}

void CGfxPageImage::OnDestroy() 
{
	CGfxPage::OnDestroy();
	
	m_pGraph = NULL;
	m_pCurve = NULL;	
}

void CGfxPageImage::OnChangeEditTimeinter() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CGfxPage::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	CString strInput;
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_TIMEINTER);
	pEdit->GetWindowText(strInput);
	if (IsDecimal(strInput))
		RefreshData();
	
}

void CGfxPageImage::OnChangeEditTimedelay() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CGfxPage::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	CString strInput;
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_TIMEDELAY);
	pEdit->GetWindowText(strInput);
	if (IsDecimal(strInput))
		RefreshData();
}

void CGfxPageImage::OnKillfocusEditTimeinter() 
{
	CString strInput;
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_TIMEINTER);
	pEdit->GetWindowText(strInput);
	if (!IsDecimal(strInput))
	{
		pEdit->SetWindowText(DoubleToString(m_iTimeInter));
	}	
}

void CGfxPageImage::OnKillfocusEditTimedelay() 
{
	CString strInput;
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_TIMEDELAY);
	pEdit->GetWindowText(strInput);
	if (!IsDecimal(strInput))
	{
		pEdit->SetWindowText(DoubleToString(m_lTimeDelay));
	}
}

void CGfxPageImage::OnColor() 
{
	if (m_pCurve != NULL)
	{
		CCurve* pCurve = m_pCurve;
		CDlgGradient dlg;
		dlg.m_stcGradient.m_tblLocat.Copy(m_pCurve->m_tblLocat);
		dlg.m_stcGradient.m_tblColor.Copy(m_pCurve->m_tblColor);
		bool bLocked = g_GfxProp.LockingProperty(TRUE);
		if (dlg.DoModal() == IDOK)
		{
			pCurve->m_tblLocat.Copy(dlg.m_stcGradient.m_tblLocat);
			pCurve->m_tblColor.Copy(dlg.m_stcGradient.m_tblColor);
			if (m_pGraph != NULL)
				m_pGraph->InvalidateAll();
		}
		g_GfxProp.LockingProperty(bLocked);
	}
}

void CGfxPageImage::OnColorr() 
{
	if (m_pCurve != NULL)
	{
		m_pCurve->m_tblLocat.RemoveAll();
		m_pCurve->m_tblColor.RemoveAll();
		if (m_pGraph != NULL)
			m_pGraph->InvalidateAll();
	}
}

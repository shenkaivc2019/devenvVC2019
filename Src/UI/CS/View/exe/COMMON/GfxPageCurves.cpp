// GfxPageCurves.cpp: implementation of the CGfxPageCurves class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "graphwnd.h"
#include "GfxPageCurves.h"
#include "Handler.h"
#include "Track.h"
#include "MainFrm.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGfxPageCommon	CGfxPageCurves::m_pageCommon;		// 常规属性控制
CGfxPageImage	CGfxPageCurves::m_pageImage;		// 图象属性控制
CGfxPageFill	CGfxPageCurves::m_pageFill;		// 填充属性控制
CGfxPageWave	CGfxPageCurves::m_pageWave;		// 波列属性控制
CGfxPageVolume  CGfxPageCurves::m_pageVolume;   // 体积属性控制


CGfxPageCurves::CGfxPageCurves(CWnd* pParent /*=NULL*/)
		:CGfxPage(IDD_GFX_PAGE_CURVES, pParent)
{
	m_pActivePage = NULL;
}

CGfxPageCurves::~CGfxPageCurves()
{	
}

void CGfxPageCurves::DoDataExchange(CDataExchange* pDX)
{
	CGfxPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGfxPageCurves)
	DDX_Control(pDX, IDC_CURVE_TYPE, m_cbCurveType);
	DDX_Control(pDX, IDC_LIST_CURVE, m_lbCurveList);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CGfxPageCurves, CGfxPage)
//{{AFX_MSG_MAP(CGfxPageCurves)
	ON_LBN_SELCHANGE(IDC_LIST_CURVE, OnSelchangeListCurve)
	ON_CBN_SELCHANGE(IDC_CURVE_TYPE, OnSelchangeCurveType)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/*++

  函数名称：

	OnInitDialog

  函数描述：

	对话框创建成功后初始化相应页签

  函数参数：

	void

  返回类型：

	void
--*/
BOOL CGfxPageCurves::OnInitDialog() 
{
	CGfxPage::OnInitDialog();
	
	InitCurveType();
	
	if (!m_pageCommon.GetSafeHwnd())
	{
		m_pageCommon.Create(IDD_GFX_PAGE_COMMON, this);
	}
	
	if (!m_pageImage.GetSafeHwnd())
	{
		m_pageImage.Create(IDD_GFX_PAGE_IMAGE, this);
	}
	
	if (!m_pageWave.GetSafeHwnd())
	{
		m_pageWave.Create(IDD_GFX_PAGE_WAVE, this);
	}
	if(!m_pageFill.GetSafeHwnd())
	{
		m_pageFill.Create(IDD_GFX_PAGE_FILL,this);
	}
	if (!m_pageVolume.GetSafeHwnd())
	{
		m_pageVolume.Create(IDD_GFX_PAGE_VOLUME, this);
	}

	return TRUE;  
}

/*++

  函数名称：
  
	InitCurve

  函数描述：

	填充曲线列表，并默认选中第一条曲线

  函数参数：
	//modify by yh 2009.2.6
	//解决了bug:井道中同名曲线混淆
	pCurve-要添加的曲线，若为空(NULL)，添加井道中所有曲线

  返回类型：

	void
--*/
void CGfxPageCurves::InitCurve(CCurve* pCurveOut)
{
	m_lbCurveList.ResetContent();
	if (m_pTrack == NULL)
	{
		m_pTrack = pCurveOut->m_pTrack;
		if (m_pTrack == NULL)
			return;
	}

	CString str;
	if(pCurveOut == NULL)//全部曲线
	{
		for(int i=0; i < m_pTrack->m_vecCurve.size(); i++)
		{
			CCurve* pCurve = (CCurve*)m_pTrack->m_vecCurve.at(i);
			if (pCurve == NULL)
				continue;

			str = pCurve->m_strName + " . " + pCurve->m_strSource;
			if (pCurve->m_nTLIndex > 0)
			{
				CString strIndex;
				strIndex.Format("%d", pCurve->m_nTLIndex);
				str = str + "(" + strIndex + ")";
			}

			int nItem = m_lbCurveList.AddString(str);
			m_lbCurveList.SetItemData(nItem, (DWORD)pCurve);
		}
	}
	else 
	{
		str = pCurveOut->m_strName + " . " + pCurveOut->m_strSource;
		if (pCurveOut->m_nTLIndex > 0)
		{
			CString strIndex;
			strIndex.Format("%d", pCurveOut->m_nTLIndex);
			str = str + "(" + strIndex + ")";
		}

		int nItem = m_lbCurveList.AddString(str);
		m_lbCurveList.SetItemData(nItem, (DWORD)pCurveOut);
	}

	m_lbCurveList.SetCurSel(0);
	OnSelchangeListCurve();
}

/*++

  函数名称：
  
	InitCurveType

  函数描述：

	填充曲线PlotStyle列表

  函数参数：

	void

  返回类型：

	void
--*/
void CGfxPageCurves::InitCurveType()
{	
	m_cbCurveType.ResetContent();

	for (int i = 0; i < Handler::Gbl_PlotInfo.GetSize(); i++)
	{
		PLOTINFO PlotInfo = Handler::Gbl_PlotInfo.GetAt(i);
		m_cbCurveType.AddString(PlotInfo.PlotENName);
	}
}

/*++

  函数名称：

	OnSelchangeCurveType

  函数描述：

	更改当前选中曲线的PlotStyle

  函数参数：

	void

  返回类型：

	void
--*/
void CGfxPageCurves::OnSelchangeCurveType()
{
	int nSel =m_lbCurveList.GetCurSel();
	if (nSel == CB_ERR)
		return;

	CCurve* pCurve = (CCurve*)m_lbCurveList.GetItemData(nSel);
	if (pCurve == NULL)
		return;

	nSel = m_cbCurveType.GetCurSel();
	if (nSel == CB_ERR)
		return;

	PLOTINFO PlotInfo = Handler::Gbl_PlotInfo.GetAt(nSel);
	pCurve->m_Plot.Release();
	pCurve->m_PlotInfo = PlotInfo;
	if (pCurve->m_Plot.CoCreateInstance(PlotInfo.PlotID) == S_OK)
	{
		pCurve->m_Plot->Advise(m_pTrack);

		AdjustLayout(pCurve);

		// 修改曲线画法，刷新绘图页面
		if (m_pGraph != NULL)
			m_pGraph->InvalidateAll();
	}

}

/*++

  函数名称：

	OnSelchangeListCurve

  函数描述：

	处理曲线列表选择消息，当选中某条曲线时，
	选择曲线相应的绘制方法并根据绘制方法显示不同的参数信息

  函数参数：

	void

  返回类型：

	void
--*/
void CGfxPageCurves::OnSelchangeListCurve()
{
	int nItemSel = m_lbCurveList.GetCurSel();
	if (nItemSel == LB_ERR)
		return;

    CCurve* pCurve = (CCurve*)m_lbCurveList.GetItemData(nItemSel);
	if (pCurve == NULL)
		return;

	for (int i = 0; i < Handler::Gbl_PlotInfo.GetSize(); i++)
	{
		PLOTINFO PlotInfo = Handler::Gbl_PlotInfo.GetAt(i);
		if (PlotInfo.PlotENName == pCurve->m_PlotInfo.PlotENName)
		{
			m_cbCurveType.SetCurSel(i);
			break;
		}	
	}

	AdjustLayout(pCurve);
}

/*++

  函数名称：

	AdjustLayout

  函数描述：

	在改变了曲线绘制方法或改变了选择的曲线后，重新调整窗口布局

  函数参数：

	pCurve-当前选中的曲线，或当前要改变属性窗口的曲线

  返回类型：

	void
--*/
void CGfxPageCurves::AdjustLayout(CCurve* pCurve)
{
	if (pCurve->IsInpre())
	{
		if ((m_pActivePage->GetSafeHwnd()!=NULL) && (m_pActivePage != &m_pageFill))
			m_pActivePage->ShowWindow(SW_HIDE);

		m_pActivePage = &m_pageFill;
	} 
	else if ((pCurve->m_PlotInfo.PlotENName.CompareNoCase("Wave") == 0)
		|| (pCurve->m_PlotInfo.PlotENName.CompareNoCase(_T("Vert")) == 0))
	{
		if ((m_pActivePage->GetSafeHwnd()!=NULL) && (m_pActivePage != &m_pageWave))
			m_pActivePage->ShowWindow(SW_HIDE);

		m_pActivePage = &m_pageWave;
	}
	else if ((pCurve->m_PlotInfo.PlotENName.CompareNoCase("Image") == 0)
		|| (pCurve->m_PlotInfo.PlotENName.CompareNoCase("VDL") == 0)
		|| (pCurve->m_PlotInfo.PlotENName.CompareNoCase("WDS") == 0))
	{
		if ((m_pActivePage->GetSafeHwnd()!=NULL) && (m_pActivePage != &m_pageImage))
			m_pActivePage->ShowWindow(SW_HIDE);

		m_pActivePage = &m_pageImage;
	}
	else if (pCurve->m_PlotInfo.PlotENName.CompareNoCase("Volume") == 0)
	{
		if ((m_pActivePage->GetSafeHwnd()!=NULL) && (m_pActivePage != &m_pageVolume))
			m_pActivePage->ShowWindow(SW_HIDE);
			m_pActivePage = &m_pageVolume;
	}
	else if (pCurve->m_PlotInfo.PlotENName.CompareNoCase("Time") == 0)
	{
		if ((m_pActivePage->GetSafeHwnd()!=NULL) && (m_pActivePage != &m_pageCommon))
			m_pActivePage->ShowWindow(SW_HIDE);

		m_pActivePage = &m_pageCommon;
	}
	else
	{
		if ((m_pActivePage->GetSafeHwnd()!=NULL) && (m_pActivePage != &m_pageCommon))
			m_pActivePage->ShowWindow(SW_HIDE);

		m_pActivePage = &m_pageCommon;
	}


	if(m_pActivePage->GetSafeHwnd() != NULL)
	{
		// 设置当前选定的曲线
		m_pActivePage->m_pCurve = pCurve;
		m_pActivePage->InitCurve();
		
		// 显示ActivePage页签
		CRect rcActivePage;
		CWnd* pStatic = GetDlgItem(IDC_STATIC_ACTIVEPAGE);
		if (pStatic == NULL)
			return;
		
		pStatic->GetClientRect(&rcActivePage);
		pStatic->MapWindowPoints(this, rcActivePage);
		
		m_pActivePage->SetWindowPos(NULL,rcActivePage.left,rcActivePage.top,
			rcActivePage.Width(),rcActivePage.Height(),SWP_NOACTIVATE | SWP_NOZORDER);	
		m_pActivePage->ShowWindow(SW_SHOW);
	}
}


// TemplActiveDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "TemplActiveDlg.h"
#include "ULDoc.h"
#include "ULView.h"
#include "MainFrm.h"
#include "PrintOrderFile.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTemplActiveDlg dialog

extern CString Gbl_AppPath;

CTemplActiveDlg::CTemplActiveDlg(int nType, CDocument* pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(IDD_TEMPL_ACTIVE_DLG, pParent)
{
	m_nType = nType;
	m_pDoc = pDoc;
}


void CTemplActiveDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTemplActiveDlg)
	DDX_Control(pDX, IDC_LIST_SUPPORTED, m_ListSupported);
	DDX_Control(pDX, IDC_LIST_NOTSUPPORTED, m_ListNotSupported);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTemplActiveDlg, CDialog)
	//{{AFX_MSG_MAP(CTemplActiveDlg)
	ON_CONTROL_RANGE(LBN_DBLCLK, IDC_LIST_NOTSUPPORTED, IDC_LIST_SUPPORTED, OnDblclkList)
	ON_COMMAND_RANGE(IDC_ADDONE, IDC_REMOVEALL, OnSelect)
	ON_COMMAND_RANGE(IDOK, IDCANCEL, OnCommand)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTemplActiveDlg message handlers

BOOL CTemplActiveDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CFileFind ff;
	CString strFind;
	BOOL bFind = FALSE;
	switch(m_nType)
	{
	case ID_PRESENTATION_ACTIVE:
		{
			strFind = Gbl_AppPath + "\\Template\\Tracks\\*.xml";
			if (m_pDoc != NULL)
			{
				POSITION pos = ((CULDoc*)m_pDoc)->m_sheetiList.GetHeadPosition();
				for (int i = 0; pos != NULL; i++)
				{
					CSheetInfo* sheet = ((CULDoc*)m_pDoc)->m_sheetiList.GetNext(pos);
					m_ListSupported.AddString(sheet->strTempl);
				}
			}
		}
		break;
	case ID_HEADER_ACTIVE:
		{
			strFind = Gbl_AppPath + "\\Template\\Headers\\*.ul*";

			if (m_pDoc != NULL)
			{
				POSITION pos = ((CULDoc*)m_pDoc)->m_celliList.GetHeadPosition();
				for (int i = 0; pos != NULL; i++)
				{
					CCellInfo* cell = ((CULDoc*)m_pDoc)->m_celliList.GetNext(pos);
					m_ListSupported.AddString(cell->strTempl);
				}
			}
		}
		break;
	case ID_CVS_ACTIVE:
		{
			strFind = Gbl_AppPath + "\\Template\\CstViews\\*.acv";

			if (m_pDoc != NULL)
			{
				POSITION pos = ((CULDoc*)m_pDoc)->m_cstiList.GetHeadPosition();
				for (int i = 0; pos != NULL; i++)
				{
					CCstInfo* cst = ((CULDoc*)m_pDoc)->m_cstiList.GetNext(pos);
					m_ListSupported.AddString(cst->strTempl);
				}
			}
		}
		break;
	case ID_PLOTTEMPLATE_ACTIVE:
		{
			strFind = Gbl_AppPath + "\\Template\\Prints\\*.ulo";

			if (m_pDoc != NULL)
			{
				POSITION pos = ((CULDoc*)m_pDoc)->m_pofiList.GetHeadPosition();
				for (int i = 0; pos != NULL; i++)
				{
					CPofInfo* pof = ((CULDoc*)m_pDoc)->m_pofiList.GetNext(pos);
					m_ListSupported.AddString(pof->strTempl);
				}
			}
		}
		break;
	default:
		break;
	}
	bFind = ff.FindFile(strFind);

	while (bFind)
	{
		bFind = ff.FindNextFile();
		CString strName = ff.GetFileName();
		if (strName == "." || strName == "..")
			continue;

		if (ff.IsDirectory())
			continue;

		// 图表文件分.ulh,.ult,.ulr，如果使用GetFileTitle的话，CULDoc添加图表会找不到指定的文件
		// 虽然这在添加时没有什么问题，但是，在文件树中双击显示图表时会无法显示所选择的图表。
		// 原因是：CGraphHeaderView中OpenCellFile时找不到相应的图表文件(缺少相应的后缀名)。

		CString strTitle;
		if (m_nType == ID_HEADER_ACTIVE)
			strTitle = ff.GetFileName();
		else
			strTitle = ff.GetFileTitle();
		if (LB_ERR == m_ListSupported.FindStringExact(-1, strTitle))
		{
			m_ListNotSupported.AddString(strTitle);
		}
	}
	ff.Close();

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CTemplActiveDlg::OnSelect(UINT id)
{	
	switch(id)
	{
	case IDC_ADDONE:
		{
			CString strTempl;
			int nSel = m_ListNotSupported.GetCurSel();
			if (LB_ERR != nSel)
			{
				m_ListNotSupported.GetText(nSel, strTempl);
				m_ListNotSupported.DeleteString(nSel);
				m_ListSupported.AddString(strTempl);
				
				DoSelect(strTempl, ADDTEMPL);
			}
		}
		break;
	case IDC_REMOVEONE:
		{
			CString strTempl;
			int nSel = m_ListSupported.GetCurSel();
			if (LB_ERR != nSel)
			{
				m_ListSupported.GetText(nSel, strTempl);
				m_ListSupported.DeleteString(nSel);
				m_ListNotSupported.AddString(strTempl);
				
				DoSelect(strTempl, REMOVETEMPL);
			}
		}
		break;
	case IDC_ADDALL:
		{
			BeginWaitCursor();

			for (int i = 0; i < m_ListNotSupported.GetCount(); i++)
			{
				CString strTempl;
				m_ListNotSupported.GetText(i, strTempl);
				m_ListSupported.AddString(strTempl);

				DoSelect(strTempl, ADDTEMPL);
			}
			m_ListNotSupported.ResetContent();
			
			EndWaitCursor();
		}
		break;
	case IDC_REMOVEALL:
		{
			BeginWaitCursor();

			for (int i = 0; i < m_ListSupported.GetCount(); i++)
			{
				CString strTempl;
				m_ListSupported.GetText(i, strTempl);
				m_ListNotSupported.AddString(strTempl);
				
				DoSelect(strTempl, REMOVETEMPL);
			}
			m_ListSupported.ResetContent();

			EndWaitCursor();
		}
		break;
	}
}

void CTemplActiveDlg::DoSelect(CString strTempl, UINT id)
{
	switch(id)
	{
	case ADDTEMPL:
		ASSERT(m_pDoc != NULL);
		switch(m_nType)
		{
		case ID_PRESENTATION_ACTIVE:
			((CULDoc*)m_pDoc)->AddSheetTempl(strTempl);
			break;
		case ID_HEADER_ACTIVE:
			((CULDoc*)m_pDoc)->AddCellTempl(strTempl);
			break;
		case ID_CVS_ACTIVE:
			((CULDoc*)m_pDoc)->AddCstTempl(strTempl);
			break;
		case ID_PLOTTEMPLATE_ACTIVE:
			((CULDoc*)m_pDoc)->AddPofTempl(strTempl);
			break;
		default:
			break;
		}
		break;
	
	case REMOVETEMPL:
		ASSERT(m_pDoc != NULL);
		switch(m_nType)
		{
		case ID_PRESENTATION_ACTIVE:
			delete ((CULDoc*)m_pDoc)->RemoveSheetInfo(strTempl);
			break;
		case ID_HEADER_ACTIVE:
			delete ((CULDoc*)m_pDoc)->RemoveCellInfo(strTempl);
			break;
		case ID_CVS_ACTIVE:
			delete ((CULDoc*)m_pDoc)->RemoveCstInfo(strTempl);
			break;
		case ID_PLOTTEMPLATE_ACTIVE:
			delete ((CULDoc*)m_pDoc)->RemovePofInfo(strTempl);
			break;
		default:
			break;
		}
		break;
	default:
		break;		
	}
}

void CTemplActiveDlg::OnDblclkList(UINT id)
{
	(id == IDC_LIST_NOTSUPPORTED) ? OnSelect(IDC_ADDONE):OnSelect(IDC_REMOVEONE);
}

void CTemplActiveDlg::OnCommand(UINT id) 
{
	//g_pMainWnd->m_wndWorkspace.m_wndTree.UpdateDocItems(m_pDoc, ULV_GRAPHS);
	EndDialog(id);
}


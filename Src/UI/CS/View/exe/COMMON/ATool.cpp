// ATool.cpp: implementation of the CATool class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ATool.h"
#include "ULCOMMDEF.H"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void tagTOOL::GetInfoPath()
{
	if (strToolDllDir.Left(7).CompareNoCase(_T("\\Tools\\")))
		return;
	
	int nRight = strToolDllDir.ReverseFind('\\');
	if (nRight < 0)
		return;
	
	strInfoPath = strToolDllDir.Mid(7, nRight - 6);
}

void tagTOOL::Serialize(CXMLSettings& xml)
{
	if (xml.IsStoring())
	{
		xml.Write(_T("Tool"), strToolName);
		xml.Write(_T("SN"), strSN);
		xml.Write(_T("ToolDllPath"), strToolDllDir);
		xml.Write(_T("ENName"), strENName);
		xml.Write(_T("CNName"), strCNName);
		xml.Write(_T("Type"), strType);
		xml.Write(_T("Length"), lLength);
		xml.Write(_T("Weight"), fWeight);
		xml.Write(_T("I.D."), iDiameter);
		xml.Write(_T("O.D."), oDiameter);
		xml.Write(_T("O.D.Max"), oDiameterMax);
		xml.Write(_T("O.D.Min"), oDiameterMin);
		xml.Write(_T("MaxPressure"), fMaxPressure);
		xml.Write(_T("MaxTemp"), fMaxTemperature);
		xml.Write(_T("MaxSpeed"), fMaxSpeed);
		xml.Write(_T("BufferSize"), nBufferSize);
		xml.Write(_T("GroupIndex"), nGroupIndex);
	}
	else
	{
		if (!xml.Read(_T("Tool"), strToolName))
			xml.Read(_T("ToolName"), strToolName);
		if (!xml.Read(_T("SN"), strSN))
			xml.Read(_T("ToolSeries"), strSN);
		
		xml.Read(_T("ToolDllPath"), strToolDllDir);
		GetInfoPath();
		ToolInfo ti;
		ZeroMemory(&ti, sizeof(ToolInfo));
		UINT cbRead = 0;
		if (xml.Read(_T("ToolInfo"), (LPBYTE)&ti, cbRead))
		{
			strENName = ti.chEnName;
			strCNName = ti.chChName;
			strType = ti.chType;
			lLength = MTD(ti.fLength);
			fMaxPressure = ti.fMaxPressure;
			fMaxTemperature = ti.fMaxTemperature;
			fMaxSpeed = ti.fMaxSpeed;
			return ;
		}

		xml.Read(_T("ENName"), strENName);
		xml.Read(_T("CNName"), strCNName);
		xml.Read(_T("Type"), strType);
		xml.Read(_T("Length"), lLength);
		xml.Read(_T("Weight"), fWeight);
		xml.Read(_T("I.D."), iDiameter);
		xml.Read(_T("O.D."), oDiameter);
		xml.Read(_T("O.D.Max"), oDiameterMax);
		xml.Read(_T("O.D.Min"), oDiameterMin);
		xml.Read(_T("MaxPressure"), fMaxPressure);
		xml.Read(_T("MaxTemp"), fMaxTemperature);
		xml.Read(_T("MaxSpeed"), fMaxSpeed);
		xml.Read(_T("BufferSize"), nBufferSize);
		xml.Read(_T("GroupIndex"), nGroupIndex);
	}
}

CATool::CATool()
{
	m_strGroupFrom.Empty();
	m_dwCalibrate = 0;
}

CATool::~CATool()
{
	ClearSensors();
}

void CATool::Serialize(CXMLSettings& xml)
{
	if (xml.IsLoading())
	{
		if (!xml.Read(_T("Tool"), strToolName))
			if (!xml.Read(_T("ToolName"), strToolName))
				strToolName = xml.m_pCurrNode->m_strName;

		if (!xml.Read(_T("SN"), strSN))
			xml.Read(_T("ToolSeries"), strSN);

//		xml.Read(_T("Picture"), strImageFile);
		xml.Read(_T("ToolDllPath"), strToolDllDir);
		GetInfoPath();
		ToolInfo ti;
		ZeroMemory(&ti, sizeof(ToolInfo));
		UINT cbRead = 0;
		if (xml.Read(_T("ToolInfo"), (LPBYTE)&ti, cbRead))
		{
			strENName = ti.chEnName;
			strCNName = ti.chChName;
			strType = ti.chType;
			lLength = MTD(ti.fLength);
			fMaxPressure = ti.fMaxPressure;
			fMaxTemperature = ti.fMaxTemperature;
			fMaxSpeed = ti.fMaxSpeed;
			return ;
		}

		xml.Read(_T("ENName"), strENName);
		xml.Read(_T("CNName"), strCNName);
		xml.Read(_T("Type"), strType);

		xml.Read(_T("Length"), lLength);
		xml.Read(_T("Weight"), fWeight);
		xml.Read(_T("I.D."), iDiameter);
		xml.Read(_T("O.D."), oDiameter);
		xml.Read(_T("O.D.Max"), oDiameterMax);
		xml.Read(_T("O.D.Min"), oDiameterMin);
		xml.Read(_T("MaxPressure"), fMaxPressure);
		xml.Read(_T("MaxTemp"), fMaxTemperature);
		xml.Read(_T("MaxSpeed"), fMaxSpeed);
		xml.Read(_T("Sensors"), bSensors);
		int nSensors = 0;
		xml.Read(_T("SensorCount"), nSensors);
		ClearSensors();
		for (int i = 0; i<nSensors; i++)
		{
			if (xml.Open(_T("Sensor%02d"), i))
			{
				CSensor* pMP = new CSensor;
				xml.Read(_T("Name"), pMP->strName);
				xml.Read(_T("Visible"), pMP->bShow);
				xml.Read(_T("Offset"), pMP->lPoint);
				m_Sensors.Add(pMP);
				xml.Back();
			}
		}
	}
	else
	{
		xml.Write(_T("Tool"), strToolName);
		xml.Write(_T("SN"), strSN);
//		xml.Write(_T("Picture"), strImageFile);
		xml.Write(_T("ToolDllPath"), strToolDllDir);
		xml.Write(_T("ENName"), strENName);
		xml.Write(_T("CNName"), strCNName);
		xml.Write(_T("Type"), strType);
		xml.Write(_T("Length"), lLength);
		xml.Write(_T("Weight"), fWeight);
		xml.Write(_T("I.D."), iDiameter);
		xml.Write(_T("O.D."), oDiameter);
		xml.Write(_T("O.D.Max"), oDiameterMax);
		xml.Write(_T("O.D.Min"), oDiameterMin);
		xml.Write(_T("MaxPressure"), fMaxPressure);
		xml.Write(_T("MaxTemp"), fMaxTemperature);
		xml.Write(_T("MaxSpeed"), fMaxSpeed);
		xml.Write(_T("Sensors"), bSensors);
		int nSensors = m_Sensors.GetSize();
		xml.Write(_T("SensorCount"), nSensors); 
		for (int i = 0; i< nSensors; i++)
		{
			if (xml.CreateKey(_T("Sensor%02d"), i))
			{
				CSensor* pMP = (CSensor*)m_Sensors.GetAt(i);
				xml.Write(_T("Name"), pMP->strName);
				xml.Write(_T("Visible"), pMP->bShow);
				xml.Write(_T("Offset"), pMP->lPoint);
				xml.Back();
			}
		}
		xml.Write(_T("Calibrate"), m_dwCalibrate);
	}
}

BOOL CATool::FindImage(CString& strImage, BOOL bBmp /* = TRUE */)
{
	TCHAR szPath[_MAX_PATH];
	GetModuleFileName(NULL, szPath, _MAX_PATH);
    CString strPath = szPath;
	strPath = strPath.Left(strPath.ReverseFind('\\'));
	strPath = strPath.Left(strPath.ReverseFind('\\'));

	int nLen = strToolDllDir.GetLength();
	if (nLen > 0)
	{
		if (strToolDllDir.Left(strPath.GetLength()).CompareNoCase(strPath))
			strImage = strPath + strToolDllDir.Left(nLen - 4);
		else
			strImage = strToolDllDir.Left(nLen - 4);
	}
	else
	{
		strImage = strPath + _T("\\Tools\\") + strInfoPath + _T("TL") + strToolName;
	}

	if (m_strGroupFrom.GetLength())
	{
		int nLen = strImage.GetLength();
		CString strLeft = strImage.Left(strImage.ReverseFind('\\'));
		strImage = strLeft + "\\" + strToolName + "-" + m_strGroupFrom;
	}

	strImage += _T(".");
	CString strExt[2] = { _T("bmp"), _T("jpg") }; //可以再扩展后缀名
	int i = 0;
	for (; i < 2; i++)
	{
		strImage += strExt[i];
		CFileFind ff;
		if (ff.FindFile(strImage))
			return TRUE;
		int nPos = strImage.ReverseFind('.');
		strImage = strImage.Left(nPos + 1);
	}
	
	strImage = strPath + _T("\\Template\\Shapes\\") + strToolName;
	strImage += _T(".");
	for (/*int*/ i = 0; i < 2; i++)
	{
		strImage += strExt[i];
		CFileFind ff;
		if (ff.FindFile(strImage))
			return TRUE;
		int nPos = strImage.ReverseFind('.');
		strImage = strImage.Left(nPos + 1);
	}

	return FALSE;
}

CString CATool::Label()
{
	CString strLabel;
	
	if (PRIMARYLANGID(LANGIDFROMLCID(GetThreadLocale())) == LANG_CHINESE)
		strLabel = strCNName;
	else
		strLabel = strENName;

	if (strLabel.GetLength())
		return strLabel;

	return strToolName;
}
// ULFile.cpp: implementation of the CULFile class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#ifdef _LOGIC
#include "Logic.h"
#else
#include "ul2000.h"
#include "TView.h"
#include "ParamsView.h"
#include "SketchView.h"
#include "PrintOrderDlg.h"
#include "CalSummaryView.h"
#include "CalCoefView.h"
#include "GroupView.h"
#include "Perforation.h"
#endif
#include "ULFile.h"
#include "Curve.h"
#include "servicetableitem.h"
#include "GraphWnd.h"
#include "MainFrm.h"
#include "ChildFrm.h"
#include "PrintOrderFile.h"
#include "Sheet.h"
#include "GraphView.h"
#include "ULTool.h"
#include "XMLSettings.h"
#include "Project.h"
#include "DlgCurves.h"
#include "Unitx.h"
#include "Utility.h"
#include "ULDataEditClass.h"
#include "ComConfig.h"
#include "GraphHeaderView.h"
#include "CloseAllFileDlg.h"
#include "CurveNameSpace.h"
#include "EILog.h"
#include "FileVersion.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

#define ID_CELLT  122

// extern "C" void DataProcess(void* a,int na,void* b,int *nb);

typedef IUnknown*(FAR PASCAL* _NEWFILE)();

// ---------------------------------
//	Save thread procedure:
// ---------------------------------

UINT SaveThreadProc(LPVOID pParam)
{
	CULFile* pULFile = (CULFile*) pParam;

	if (pULFile)
	{
		pULFile->SaveAs();
		if (pULFile->m_pProject)
			pULFile->m_pProject->Release();

		if (pULFile->m_bAutoDelete)
			delete pULFile;
	}

	return 0;
}

UINT LoadThreadProc(LPVOID pParam)
{
	CULFile* pULFile = (CULFile*) pParam;
	int nFrame = pULFile->m_lFrameCount;
	for (int i = 0; i < nFrame; i++)
	{
		if (pULFile->ReadDataFrame(i))
			break;
	}

	if(pULFile->m_nDriveMode == UL_DRIVE_DEPT)
		pULFile->AdjustDirection();
	
	pULFile->ReadDataEnd();
	pULFile->Close();
	if (g_pMainWnd->GetSafeHwnd())
		g_pMainWnd->SendMessage(UM_DATA_SAVE, 8, (LPARAM) pULFile);

	return 0;
}

IMPLEMENT_DYNAMIC(CULFile, CULDoc)
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CULFile::CULFile()
{
	m_hFFmt = NULL;			// File format driver
	m_pIFile = NULL;		// File interface	
	m_bOpened = FALSE;		// Is open
	m_bReadCurves = FALSE;	// Read curve only
	m_bAutoDelete = FALSE;	// When end saving
	m_bAutoMap = FALSE;		// Auto search presentation

	m_lFrameCount = 0;		// Read frame count
	m_nDriveMode = UL_DRIVE_DEPT;	// Driver mode
	m_nDirection = SCROLL_DOWN;		// Depth curve direction
	m_pSaveOP = NULL;		// Save operation

	m_pGraph = NULL;		// First view
	m_bSTarget = FALSE;

	m_pCAL = NULL;
	m_pDEV = NULL;

	m_pEILog = NULL;
#ifndef _LOGIC
	ZeroMemory(&m_RftProperty, sizeof(RFT_PROPERTY));
#endif
}

CULFile::~CULFile()
{
	if (m_pIFile)
	{
		m_pIFile->Release();
		m_pIFile = NULL;
	}

	if (m_hFFmt)
	{
		::FreeLibrary(m_hFFmt);
		m_hFFmt = NULL;
	}

	if (m_nRead)
	{
		if (m_pProject != NULL)
		{
			delete m_pProject;
			m_pProject = NULL;
		}
	}

	for (int i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
		if ((pCurve != NULL) && (pCurve->m_nbType == NB_FILE))
			pCurve->Release();
	}
	m_vecCurve.clear();

	while (m_tpTree.GetCount())
		delete m_tpTree.RemoveHead();

	if (m_pSaveOP)
	{
		delete m_pSaveOP;
		m_pSaveOP = NULL;
	}
}
//***************************************************************************
ULMPTRIMP CULFile::GetProjectInfo()
{
	return m_pProject;
}
//***************************************************************************
ULMINTIMP CULFile::GetSheetCount()
{
	CULDoc* pDoc = this;
	if (m_pProject && m_pProject->m_pService)
		pDoc = m_pProject->m_pService;

	return pDoc->m_sheetiList.GetCount();
}
//***************************************************************************
BOOL CULFile::LoadFileFormat(CString strFileFormat)
{
	if (m_pIFile)
	{
		m_pIFile->Release();
		m_pIFile = NULL;
	}

	if (m_hFFmt)
	{
		::FreeLibrary(m_hFFmt);
		m_hFFmt = NULL;
	}

	// 如果扩展名为空，尝试使用双GR文件格式打开
	if (strFileFormat.IsEmpty())
	{
		strFileFormat = "FFDGR.dll";
		m_hFFmt = LoadLibrary(strFileFormat);
		if (m_hFFmt == NULL)
			return FALSE;
	}
	else
	{
		strFileFormat = "FF" + strFileFormat + ".dll";
		m_hFFmt = LoadLibrary(strFileFormat);
		if (m_hFFmt == NULL)
			return FALSE;
	}

	_NEWFILE CreateXFile = (_NEWFILE) GetProcAddress(m_hFFmt, "CreateXFile");
	if (CreateXFile == NULL)
	{
		::FreeLibrary(m_hFFmt);
		m_hFFmt = NULL;
		return FALSE;
	}

	m_pIFile = (IFile *) CreateXFile();
	HRESULT hr = m_pIFile->QueryInterface(IID_IFile, (void**) &m_pIFile);
	if (SUCCEEDED(hr))
	{
		m_pIFile->AdviseFile(this);
		m_pIFile->InitFile();
		return TRUE;
	}

	::FreeLibrary(m_hFFmt);
	m_hFFmt = NULL;
	return FALSE;
}
//**************************************************************************************
ULMIMP CULFile::GetTools(ULTOOLS& vecTools)
{
	vecTools.clear();
	if (m_pProject != NULL)
	{
		CULKernel* pKernel = &m_pProject->m_ULKernel;
		for (int i = 0; i < pKernel->m_arrTools.GetSize(); i++)
		{
			CULTool* pULTool = (CULTool*) pKernel->m_arrTools.GetAt(i);
			ULTOOL sTool;
			sTool.pIULTool = static_cast<IULTool*>(pULTool);
			sTool.pITool = pULTool->m_pITool;
			vecTools.push_back(sTool);
		}
		return UL_NO_ERROR;
	}
	return UL_ERROR;
}
//**************************************************************************************
ULMIMP CULFile::CreateTools(ULTOOLS& vecTools, UINT nTools)
{
	if (m_bReadCurves)
		return UL_ERROR;

	vecTools.clear();
	if (m_pProject != NULL)
	{
		for (int i = 0; i < nTools; i++)
		{
			CULTool* pULTool = new CULTool;
			m_arrTools.Add(pULTool);
			ULTOOL sTool;
			sTool.pIULTool = static_cast<IULTool*>(pULTool);
			sTool.pITool = NULL;
			vecTools.push_back(sTool);
		}
		return UL_NO_ERROR;
	}
	return UL_ERROR;
}
// ---------------------------------
//	Get cells in the project/file:
// ---------------------------------
ULMIMP CULFile::GetCells(CStringArray* pArrCell, CStringArray* pArrTempl)
{
	pArrCell->RemoveAll();
	if (m_pProject != NULL)
	{
		if (m_pProject->m_pService != NULL)	// 工程中保存
		{
			while (m_celliList.GetCount())
				delete m_celliList.RemoveHead();

			m_pProject->m_pService->GetCelliList(&m_celliList);
		}

		for (POSITION pos = m_celliList.GetHeadPosition(); pos != NULL;)
		{
			CCellInfo* pCellInfo = m_celliList.GetNext(pos);
			pArrCell->Add(pCellInfo->strFile);
			pArrTempl->Add(pCellInfo->strTempl);
		}

		return UL_NO_ERROR;
	}

	return UL_ERROR;
}
// ---------------------------------
//	Save current sheet i to file:
// ---------------------------------
ULMIMP CULFile::SaveSheet(int i, LPCTSTR lpszSheet)
{
	CULDoc* pDoc = this;
	if (m_pProject && m_pProject->m_pService)
		pDoc = m_pProject->m_pService;

	POSITION pos = pDoc->m_sheetiList.FindIndex(i);
	if (pos != NULL)
	{
		// 添加到文件中的绘图模板可分为两种情况
		// 1.添加到文件并被打开，这是pSheetInfo->pSheet不为空的情况
		// 2.添加到文件却未被打开，这是pSheetInfo->pSheet为空的情况
		// 出现第二种情况时，如果不创建一个模板并调用其SaveSheet成员，则该模板不能被保存到文件中
		// 第二次打开该文件，在加载到这个事实上并未被保存的绘图模板时会提示“加载模板失败”
		// 为了解决这种问题，需要处理pSheetInfo->pSheet==NULL的情况
		CSheetInfo* pSheetInfo = pDoc->m_sheetiList.GetAt(pos);
		if (pSheetInfo && pSheetInfo->pSheet)
		{
			pSheetInfo->pSheet->SaveSheet(lpszSheet, FALSE, FALSE);
			return UL_NO_ERROR;
		}
		if (pSheetInfo && (pSheetInfo->pSheet == NULL))
		{
			pSheetInfo->pSheet = new CSheet(pSheetInfo->strFile);
			pSheetInfo->pSheet->AddRef();
			//pSheetInfo->pFrame = new CChildFrame(ULV_GRAPHS, pSheetInfo->pSheet, pDoc);
			//pSheetInfo->pSheet->RemapTrackCurve(pDoc->m_vecCurve);

			pSheetInfo->pSheet->SaveSheet(lpszSheet, FALSE, FALSE);

			// 使用之后删除
			delete pSheetInfo->pSheet;
			pSheetInfo->pSheet = NULL;

			return UL_NO_ERROR;
		}
	}

	return UL_ERROR;
}
//**************************************************************************************
ULMIMP CULFile::SetSheet(int i, LPCTSTR lpszSheet)
{
	CULDoc* pDoc = this;
	if (m_pProject && m_pProject->m_pService)
		pDoc = m_pProject->m_pService;

	POSITION pos = pDoc->m_sheetiList.FindIndex(i);
	if (pos != NULL)
	{
		CSheetInfo* pSheetInfo = pDoc->m_sheetiList.GetAt(pos);
		if (pSheetInfo && pSheetInfo->pSheet)
		{
			pSheetInfo->pSheet->LoadSheet(lpszSheet);
			return UL_NO_ERROR;
		}
	}

	return UL_ERROR;
}
//**************************************************************************************
ULMIMP CULFile::GetSheets(CStringArray* pArrSheet)
{
	pArrSheet->RemoveAll();
	CULDoc* pDoc = this;
	if (m_pProject && m_pProject->m_pService)
		pDoc = m_pProject->m_pService;

	pDoc->GetSheetFiles(pArrSheet);
	return UL_NO_ERROR;
}
//**************************************************************************************
ULMIMP CULFile::SetSheets(CStringArray* pArrSheet)
{
	ClearSheetInfo();
	int iAdd = 0;
	for (int i = 0; i < pArrSheet->GetSize(); i++)
	{
		CSheetInfo* pSheetInfo = new CSheetInfo;
		pSheetInfo->strTempl.Empty();
		pSheetInfo->strFile = pArrSheet->GetAt(i);
		pSheetInfo->pSheet = new CSheet;
		pSheetInfo->pSheet->AddRef();
		if (pSheetInfo->pSheet->LoadSheet(pSheetInfo->strFile))
		{
			pSheetInfo->strTempl = pSheetInfo->pSheet->m_strName;
			m_sheetiList.AddTail(pSheetInfo);
			iAdd ++;
		}
		else
			delete pSheetInfo;
	}

	if (iAdd == 0)
	{
		m_bAutoMap = TRUE;
	}

	return UL_NO_ERROR;
}
//**************************************************************************************
ULMIMP CULFile::SetCells(CStringArray* pArrCell, CStringArray* pArrTempl)
{
	int nSize = pArrCell->GetSize();
	if (pArrTempl->GetSize() != nSize)
		return UL_ERROR;

	while (m_celliList.GetCount())
		delete m_celliList.RemoveHead();

	for (int i = 0; i < nSize; i++)
	{
		CCellInfo* pCellInfo = new CCellInfo;
		pCellInfo->strFile = pArrCell->GetAt(i);
		pCellInfo->strTempl = pArrTempl->GetAt(i);
		if (pCellInfo->strTempl.Right(4).Left(3).CompareNoCase(".ul"))
		{
			pCellInfo->strTempl += ".ulh";
		}

		m_celliList.AddTail(pCellInfo);
	}

	return UL_NO_ERROR;
}

// ---------------------------------
//	Create a new file for saving:
// ---------------------------------

BOOL CULFile::Create(LPCTSTR pszFileFormat, CString& strFileName)
{
	//	if (m_strExtent.CompareNoCase(pszFileFormat))
	{
		m_strExtent = pszFileFormat;
		if (!LoadFileFormat(m_strExtent))
		{
			AfxMessageBox(IDS_FAIL_LOADFORMAT);
			return FALSE;
		}
	}

	strFileName += ("." + m_strExtent);
	if (m_pIFile->Create(strFileName))
	{
		AfxMessageBox(IDS_FAIL_CREATEFILE);
		return FALSE;
	}

	m_bOpened = TRUE;
	return TRUE;
}
//**************************************************************************************
BOOL CULFile::Save(BOOL bAsny /* = TRUE */)
{
	if (m_strPathName.IsEmpty())
		return FALSE;

	if (m_bOpened)
		Close();

	// _trename(m_strPathName, m_strPathName + _T(".bak"));
	::DeleteFile(m_strPathName);
	CString strName = m_strPathName.Left(m_strPathName.ReverseFind('.'));
	if (m_pSaveOP != NULL)
		delete m_pSaveOP;

	m_pSaveOP = new SAVEOPT;
	ZeroMemory(m_pSaveOP, sizeof(SAVEOPT));
	m_pSaveOP->dwOption = SO_WRAP;
	m_pSaveOP->bRecordDown = m_nDirection;
	m_pSaveOP->bMetric = TRUE;
	m_pSaveOP->fVersion = m_fVersion;//add by gj130903 保存LAS版本号	

	BOOL bRes = SaveAs(m_strExtent, strName, bAsny);
	return bRes;
}
//**************************************************************************************
BOOL CULFile::SaveAs(CString strFileFormat, CString strFileName,
	BOOL bAsny /* = TRUE */)
{
	if (Create(strFileFormat, strFileName))
	{
		for (POSITION pos = m_frmList.GetHeadPosition(); pos != NULL;)
		{
			CChildFrame* pFrame = m_frmList.GetNext(pos);
			pFrame->m_dwLocked |= LT_SAVING;
		}

		if (strFileFormat.CompareNoCase(_T("AXP")) == 0)
		{
			// Save well sketch, tool string file
			for (pos = m_frmList.GetHeadPosition(); pos != NULL;)
			{
				CChildFrame* pFrame = m_frmList.GetNext(pos);
				if (pFrame->m_pULView)
					pFrame->m_pULView->SaveService();
			}
		}
		
		if (AfxMessageBox(IDP_SAVE_TEMPLATES, MB_YESNO|MB_ICONQUESTION) == IDYES)
		{
			// Save well sketch, tool string file
			for (pos = m_frmList.GetHeadPosition(); pos != NULL;)
			{
				CChildFrame* pFrame = m_frmList.GetNext(pos);
				if (pFrame->m_pULView)
					pFrame->m_pULView->SaveAsTempl(strFileName);
			}
		}

		if (m_pProject)
			m_pProject->AddRef();

		if (bAsny)
		{
			AfxBeginThread(SaveThreadProc, this);
		}
		else
		{
			SaveThreadProc(this);
		}

		return TRUE;
	}

	return FALSE;
}
//**************************************************************************************
BOOL CULFile::SaveAs()
{
	CString strExt = m_strExtent;
	strExt.MakeUpper();
	if (!strExt.CompareNoCase(_T("ALD")))
	{
		return SaveAsALD();
	}

	BOOL bNotXTF = m_strExtent.CompareNoCase(_T("xtf"));
	// Put the depth curve in the beginning of the vector
	if (bNotXTF)
	{
		for (int i = 0; i < m_vecCurve.size(); i++)
		{
			CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
			if (pCurve->IsDepthCurve())
			{
				if (i > 0)
				{
					m_vecCurve.erase(m_vecCurve.begin() + i);
					m_vecCurve.insert(m_vecCurve.begin(), pCurve);
				}

				break;
			}
		}
	}

// 	// add by bao 2013/7/30 时间驱动保存
	if (m_nDriveMode == UL_DRIVE_TIME && bNotXTF)
	{
		return SaveAsByTime();
	}
// 	// end

	int nStartIndex = 0;
	int nEndIndex = 0;  //起始终止深度索引
	long lStartDepth = 0;
	long lEndDepth = 0;   

	long lStep = 10;
	g_pMainWnd->SendMessage(UM_DATA_SAVE);
	CTime tmModify = CTime::GetCurrentTime();
	SetTime(tmModify.GetTime(), FT_MODIFY);
	SaveFileHead(m_pProject, NULL);

	int nOff = 1;

	if (m_pDCurve == NULL)
		m_pDCurve = (CCurve *) m_vecCurve.at(0);

	if (!m_pDCurve)
		return FALSE;

	CCurveData ddata(m_pDCurve->m_pData);
	if (bNotXTF)
		ddata.Copy(m_pDCurve->m_pData);
	else
	{
		if (m_pSaveOP->dwOption & SO_DEPT)
		{
			long lFinalDepth0 = 0;
			long lFinalDepthE = 0;	// 经过深度填充后的深度范围(所有曲线最大的范围)
			
			int nFrameCount = m_pDCurve->GetDataSize();
			lFinalDepth0 = m_pDCurve->GetDepth(0);
			lFinalDepthE = m_pDCurve->GetDepth(nFrameCount - 1);
			
			if (lFinalDepth0 > lFinalDepthE)
			{
				long lTemp = lFinalDepth0;
				lFinalDepth0 = lFinalDepthE;
				lFinalDepthE = lTemp;
			}

			m_pSaveOP->lMaxDepth = min(m_pSaveOP->lMaxDepth, lFinalDepthE);
			m_pSaveOP->lMinDepth = max(lFinalDepth0, m_pSaveOP->lMinDepth);            
            nStartIndex = m_pDCurve->GetFirstIndexByDepth(m_pSaveOP->lMinDepth);
            nEndIndex = m_pDCurve->GetFirstIndexByDepth(m_pSaveOP->lMaxDepth);
            if (nStartIndex > nEndIndex)
            {
                int nTemp = nStartIndex;
                nStartIndex = nEndIndex;
                nEndIndex = nTemp;
            }			
		}
		else
		{
			m_lFrameCount = m_pDCurve->GetDataSize();
			long lFinalDepth0 = m_pDCurve->GetDepth(0);
			long lFinalDepthE = m_pDCurve->GetDepth(m_lFrameCount - 1);
			m_pSaveOP->lMaxDepth = max(lFinalDepthE, lFinalDepth0);
			m_pSaveOP->lMinDepth = min(lFinalDepth0, lFinalDepthE);
			nStartIndex = 0;
			nEndIndex = m_lFrameCount;
		}
		
		// Get the start Depth and the end Depth
		if (m_nDirection == SCROLL_UP)
		{
			lStartDepth = max(m_pSaveOP->lMinDepth,
				m_pSaveOP->lMaxDepth);
			lEndDepth = min(m_pSaveOP->lMaxDepth, m_pSaveOP->lMinDepth);
		}
		else
		{
			lStartDepth = min(m_pSaveOP->lMaxDepth,
				m_pSaveOP->lMinDepth);
			lEndDepth = max(m_pSaveOP->lMinDepth, m_pSaveOP->lMaxDepth);
		}
        
        int nSize = m_lFrameCount - 1;
        if (nEndIndex > nSize)
            nEndIndex = nSize;
        
        lStartDepth = m_pDCurve->GetDepth(nStartIndex);
        lEndDepth = m_pDCurve->GetDepth(nEndIndex);
        //m_pSaveOP->lMinDepth = min(lStartDepth, lEndDepth);
        //m_pSaveOP->lMaxDepth = max(lStartDepth, lEndDepth);
	}

	if (bNotXTF && (m_nDriveMode == UL_DRIVE_DEPT))
	{
		m_lFrameCount = m_pDCurve->GetDataSize();
		if (m_lFrameCount > 0)
		{
			long lDepth0 = m_pDCurve->GetDepth(0);
			long lDepth1 = m_pDCurve->GetDepth(m_lFrameCount - 1);

			// 使用采样间隔来对深度曲线进行填充，使深度曲线成为最长曲线
			double fFeedStep = 0.1f;
			if (m_lFrameCount > 1)
				fFeedStep = abs(lDepth0 - lDepth1) / (m_lFrameCount - 1);

			int nDir0 = m_pSaveOP ? m_pSaveOP->bRecordDown : m_nDirection;
			m_nDirection = lDepth0 > lDepth1 ? SCROLL_UP : SCROLL_DOWN;
			nOff = (nDir0 == m_nDirection) ? 1 : -1;
			GetDepthRange(m_nDirection, lDepth0, lDepth1);
			if (m_nDirection == SCROLL_UP) //上提
			{
				// 填充开始部分：深度+步长， 取最开始一个点的值
				// 采样步长
				long lDepth = m_pDCurve->GetDepth(0);
				float fValue = m_pDCurve->GetfltValue(0);
				long lTime = m_pDCurve->GetTime(0);
				double dDepth = lDepth;
				while (dDepth < lDepth0)
				{
					dDepth += fFeedStep;
					m_pDCurve->m_pData->Insert(0, (long) (dDepth + .5),
											fValue, lTime);
				}

				// 填充结束部分
				int nEnd = m_pDCurve->GetDataSize() - 1;
				lDepth = m_pDCurve->GetDepth(nEnd);
				fValue = m_pDCurve->GetfltValue(nEnd);
				lTime = m_pDCurve->GetTime(nEnd);
				dDepth = lDepth;
				while (dDepth > lDepth1)
				{
					dDepth -= fFeedStep;
					m_pDCurve->m_pData->Add((long) (dDepth + .5), fValue,
											lTime);
				}
			}
			else   //下
			{
				// 填充开始部分：深度 - 步长， 取最开始一个点的值
				// 采样步长
				long lDepth = m_pDCurve->GetDepth(0);
				float fValue = m_pDCurve->GetfltValue(0);
				long lTime = m_pDCurve->GetTime(0);
				double dDepth = lDepth;
				while (dDepth > lDepth0)
				{
					dDepth -= fFeedStep;
					m_pDCurve->m_pData->Insert(0, (long) (dDepth + .5),
											fValue, lTime);
				}

				// 填充结束部分
				int nEnd = m_pDCurve->GetDataSize() - 1;
				lDepth = m_pDCurve->GetDepth(nEnd);
				fValue = m_pDCurve->GetfltValue(nEnd);
				lTime = m_pDCurve->GetTime(nEnd);
				dDepth = lDepth;
				while (dDepth < lDepth1)
				{
					dDepth += fFeedStep;
					m_pDCurve->m_pData->Add((long) (dDepth + .5), fValue,
											lTime);
				}
			}//到此深度曲线填充完毕
			
			if (m_pSaveOP->dwOption & SO_MIN)
			{
				for (int i = 0; i < m_vecCurve.size(); i++)
				{
					CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
					if (!pCurve->IsSave())
						continue;

					long lStep = pCurve->CalcInterval();
					if (lStep < 10)
						continue;

					if (m_pSaveOP->nStep < 10)
					{
						m_pSaveOP->nStep = lStep;
						continue;
					}

					if ((lStep > 10) && (lStep < m_pSaveOP->nStep))
						m_pSaveOP->nStep = lStep;
				}
			}

			if ((m_pSaveOP->dwOption & SO_INP) && (m_pDCurve->IsDepthCurve()))
			{
				CULDataEdit dataEditPerform(TRUE);// 构造曲线编辑对象
				// 调用插补算法
				DATA_EDIT_OPERATE opCode;
				opCode.nOperateCode = INTERPOLATEION;
				opCode.unionParam1.lParam = m_pSaveOP->nStep;
				opCode.pCurve = m_pDCurve;
				opCode.pCurveData = m_pDCurve->m_pData;
				dataEditPerform.DataEditManager(opCode);
				
				if (m_pSaveOP->nStep < 1)
					m_pSaveOP->nStep = opCode.unionParam1.lParam;

				m_lFrameCount = m_pDCurve->GetDataSize();
			}

			if (m_pSaveOP->dwOption & SO_DEPT)
			{
				long lFinalDepth0 = 0;
				long lFinalDepthE = 0;	// 经过深度填充后的深度范围(所有曲线最大的范围)

				int nFrameCount = m_pDCurve->GetDataSize();
				lFinalDepth0 = m_pDCurve->GetDepth(0);
				lFinalDepthE = m_pDCurve->GetDepth(nFrameCount - 1);

				if (lFinalDepth0 > lFinalDepthE)
				{
					long lTemp = lFinalDepth0;
					lFinalDepth0 = lFinalDepthE;
					lFinalDepthE = lTemp;
				}
				m_pSaveOP->lMaxDepth = min(m_pSaveOP->lMaxDepth, lFinalDepthE);
				m_pSaveOP->lMinDepth = max(lFinalDepth0, m_pSaveOP->lMinDepth);
				nStartIndex = m_pDCurve->GetFirstIndexByDepth(m_pSaveOP->lMinDepth);
				nEndIndex = m_pDCurve->GetFirstIndexByDepth(m_pSaveOP->lMaxDepth);
				if (nStartIndex > nEndIndex)
				{
					int nTemp = nStartIndex;
					nStartIndex = nEndIndex;
					nEndIndex = nTemp;
				}

			}
			else
			{
				m_lFrameCount = m_pDCurve->GetDataSize();
				long lFinalDepth0 = m_pDCurve->GetDepth(0);
				long lFinalDepthE = m_pDCurve->GetDepth(m_lFrameCount - 1);
				m_pSaveOP->lMaxDepth = max(lFinalDepthE, lFinalDepth0);
				m_pSaveOP->lMinDepth = min(lFinalDepth0, lFinalDepthE);
				nStartIndex = 0;
				nEndIndex = m_lFrameCount;
			}

			// Get the start Depth and the end Depth
			if (m_nDirection == SCROLL_UP)
			{
				lStartDepth = max(m_pSaveOP->lMinDepth,
					m_pSaveOP->lMaxDepth);
				lEndDepth = min(m_pSaveOP->lMaxDepth, m_pSaveOP->lMinDepth);
			}
			else
			{
				lStartDepth = min(m_pSaveOP->lMaxDepth,
					m_pSaveOP->lMinDepth);
				lEndDepth = max(m_pSaveOP->lMinDepth, m_pSaveOP->lMaxDepth);
			}

			int nSize = m_lFrameCount - 1;
			if (nEndIndex > nSize)
				nEndIndex = nSize;

			lStartDepth = m_pDCurve->GetDepth(nStartIndex);
			lEndDepth = m_pDCurve->GetDepth(nEndIndex);
			//m_pSaveOP->lMinDepth = min(lStartDepth, lEndDepth);
			//m_pSaveOP->lMaxDepth = max(lStartDepth, lEndDepth);
			nEndIndex++;

			long lFrom = max(lStartDepth, lEndDepth);
			if (m_pSaveOP->bRecordDown)
				lFrom = min(lStartDepth, lEndDepth);

			for (int i = 0; i < m_vecCurve.size(); i++)
			{
				CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
				pCurve->IniCurSavePos(lFrom, nOff);
			}
		}
	}

	if (SaveDataBegin(NULL, TRUE, lStartDepth, lEndDepth))
	{
		Close();

		for (POSITION pos = m_frmList.GetHeadPosition(); pos != NULL;)
		{
			CChildFrame* pFrame = m_frmList.GetNext(pos);
			pFrame->m_dwLocked &= ~LT_SAVING;
		}
		g_pMainWnd->SendMessage(UM_DATA_SAVE, 2, (LPARAM)this);
		return FALSE;
	}

	if (bNotXTF)
	{
		g_pMainWnd->SendMessage(UM_DATA_SAVE, 1, lStep);
		if (m_vecCurve.size() > 0)
		{
			long lDLevel = 0;
			if (m_pDCurve->GetDataSize() > 1)
			{
				long lFD = m_pDCurve->GetDepth(0);
				long lSD = m_pDCurve->GetDepth(1);
				lDLevel = abs(lFD - lSD);
			}

			float fDelta = m_lFrameCount / (lStep - 1);
			float j = fDelta;
			
			// 需要保存的纪录方向和此文件的纪录方向相同，则索引从小到大
			// int nOff = (m_bRecordDown == m_nDirection) ? 1 : -1;
			if (nOff == 1)
			{
				for (int i = nStartIndex; i < nEndIndex; i++)
				{
					SaveDataFrame(i, nOff, lDLevel);
					if (i > j)
					{
						j += fDelta;
						lStep += 10;
						g_pMainWnd->SendMessage(UM_DATA_SAVE, 1, lStep);
					}
				}
			}
			else
			{
				for (int i = nEndIndex - 1; i >= nStartIndex; i--)
				{
					SaveDataFrame(i, nOff, lDLevel);
					if (i > j)
					{
						j += fDelta;
						lStep += 10;
						g_pMainWnd->SendMessage(UM_DATA_SAVE, 1, lStep);
					}
				}
			}
		}
	}

	SaveDataEnd();
	Close();
	for (POSITION pos = m_frmList.GetHeadPosition(); pos != NULL;)
	{
		CChildFrame* pFrame = m_frmList.GetNext(pos);
		pFrame->m_dwLocked &= ~LT_SAVING;
	}
	g_pMainWnd->SendMessage(UM_DATA_SAVE, 2);

	if (bNotXTF)
	{
		m_pDCurve->m_pData->Clear();
		m_pDCurve->m_pData->Append(&ddata);
	}

	SetModifiedFlag(FALSE);
	return TRUE;
}

//**************************************************************************************
BOOL CULFile::SaveProject(CString strFileFormat, CString strFileName)
{
	if (m_pProject == NULL)
		return FALSE;

	m_pDCurve = m_pProject->m_ULKernel.GetDepthCurve(m_vecCurve); // 得到所有曲线列表
	if (m_pDCurve == NULL)
		return FALSE;

	if (m_pProject && m_pProject->m_pService)
	{
		CULDoc* pDoc = m_pProject->m_pService;
		POSITION pos = pDoc->m_sheetiList.GetHeadPosition();
		for (; pos != NULL;)
		{
			CSheetInfo* pSheetInfo = pDoc->m_sheetiList.GetNext(pos);
			if (pSheetInfo && pSheetInfo->pFrame)
				m_frmList.AddTail(pSheetInfo->pFrame);
		}
	}

	m_nDriveMode = m_pProject->m_ULKernel.m_logInfo.nMode;
	m_nDirection = m_pProject->m_ULKernel.m_logInfo.nDirection;

	return SaveAs(strFileFormat, strFileName);
}
//**************************************************************************************
BOOL CULFile::Read(LPCTSTR lpszFile, BOOL bShowMsg /* = FALSE */)
{
	CString strFile = lpszFile;

	if (bShowMsg)
	{
		if (!Open(strFile))
		{
			AfxMessageBox(IDS_NOEXSIT_NOFILE);
			return FALSE;
		}

		theApp.SetStatusPrompt(IDS_READ_FILE_HEAD);
		if (ReadFileHead())
		{
			AfxMessageBox(IDS_ERR_READFILE);
			return FALSE;
		}

		// 读数据头信息失败
		theApp.SetStatusPrompt(IDS_READ_DATA_HEAD);
		if (ReadDataBegin())
		{
			AfxMessageBox(IDS_ERR_READDATA);
			return FALSE;
		}

		theApp.SetStatusPrompt(IDS_READ_DATA);
		for (int i = 0; i < m_lFrameCount; i++)
		{
			if (ReadDataFrame(i))
				break;
		}

		if (m_pDCurve != NULL)
			m_lFrameCount = m_pDCurve->GetDataSize();

		ReadDataEnd();
		Close();

		// 加载绘图设置
		if (m_sheetiList.GetCount() == 0)
			TRACE("Warning : There is no sheet to create frame!\n");

		return TRUE;
	}

	if (!Open(strFile))
		return FALSE;

	theApp.SetStatusPrompt(IDS_READ_FILE_HEAD);
	if (ReadFileHead() == UL_ERROR)
		return FALSE;

	theApp.SetStatusPrompt(IDS_READ_DATA_HEAD);
	if (ReadDataBegin() == UL_ERROR)
	{
		AfxMessageBox(IDS_UNKNOWN_FILEFORMAT);
		return FALSE;
	}

	theApp.SetStatusPrompt(IDS_READ_DATA);
	AssignSameTools();

#ifdef _FileThread
	AfxBeginThread(LoadThreadProc, this);
	Sleep(100);
#else
	LoadThreadProc(this);
#endif

	if (m_pDCurve != NULL)
	{
		m_lFrameCount = m_pDCurve->GetDataSize();
		if (m_lFrameCount > 1)
		{
			long lDepth0 = m_pDCurve->GetDepth(0);
			long lDepthE = m_pDCurve->GetDepth(m_lFrameCount - 1);
			m_nDirection = (BOOL) (lDepth0 < lDepthE);
		}
	}

	return TRUE;
}
//**************************************************************************************
BOOL CULFile::ReadAllCurves(LPCTSTR pszFile)
{
	if (!Open(pszFile))
		return FALSE;

	m_bReadCurves = TRUE;
	theApp.SetStatusPrompt(IDS_READ_FILE_HEAD);
	ReadFileHead();	
	theApp.SetStatusPrompt(IDS_READ_DATA_HEAD);
	ReadDataBegin();
	theApp.SetStatusPrompt(IDS_READ_DATA);
	for (int i = 0; i < m_lFrameCount; i++)
	{
		if (ReadDataFrame(i))
			break;
	}

	if (m_pDCurve != NULL)
	{
		m_lFrameCount = m_pDCurve->GetDataSize();
		if (m_lFrameCount > 1)
		{
			long lDepth0 = m_pDCurve->GetDepth(0);
			long lDepthE = m_pDCurve->GetDepth(m_lFrameCount - 1);
			m_nDirection = (BOOL) (lDepth0 < lDepthE);
		}
	}

	theApp.SetStatusPrompt(IDS_CULFILE_READ_ERR);//Read file tail...
	ReadDataEnd();
	g_pMainWnd->SendMessage(WM_SETMESSAGESTRING, AFX_IDS_IDLEMESSAGE);
	Close();
	m_bReadCurves = FALSE;

	return TRUE;
}
//**************************************************************************************
BOOL CULFile::Open(CString strFile)
{
	m_strPathName = strFile;

	// 如果打开一个不存在扩展名的文件，会误把路径中的.作为分隔符
	// 如: “c:\axp.v110\data\双GR-俄罗斯格式文件”，程序在解析该文件
	// 时，会误把“.v110\data\双GR-俄罗斯格式文件”当作扩展名
	// 注.“双GR-俄罗斯格式文件”为俄罗斯某解释软件所产生的文件

	TCHAR ext[_MAX_EXT];
	_tsplitpath(strFile, 0, 0, 0, ext);

	m_strExtent = ext;
	m_strExtOpened = m_strExtent;
	m_strExtent.Delete(0); //去除其中的“.”
	if (LoadFileFormat(m_strExtent))
	{
		if (m_pIFile->Open(strFile))
		{
			CString strPrompt;
			AfxFormatString1(strPrompt, IDS_FAIL_OPENFILE, strFile);
			AfxMessageBox(strPrompt);
			return FALSE;
		}

		m_bOpened = TRUE;
		return TRUE;
	}

	AfxMessageBox(IDS_FAIL_LOADFORMAT);
	return FALSE;
}
//**************************************************************************************
void CULFile::SaveFileHead(CProject* pProject, CServiceTableItem* pSi){
	if (pProject == NULL)
		m_pProject = g_pActPrj;
	else
		m_pProject = pProject;	// 所属工程

	CULKernel* pKernel = m_pProject != NULL? &m_pProject->m_ULKernel : NULL;
	if (pKernel != NULL)
	{
		for (int i=0; i < pKernel->m_arrTools.GetSize(); i++)
		{
			CULTool* pULTool = (CULTool*)pKernel->m_arrTools.GetAt(i);
			for (int j=0; j<pULTool->m_Curves.GetSize(); j++)
			{
				CCurve* pCurve = (CCurve*)pULTool->m_Curves.GetAt(j);
				if( !pCurve->m_bDelete || (FALSE && pCurve->m_bDelete) ) // FALSE 为pKernel->GetAllCurveProps(vecCurve);的第二个参数
				{
					m_arrToolCurves.Add(pCurve);
				}
			}
		}
	}

	if (m_pIFile)
	{
#ifndef _LOGIC
		if ((pSi != NULL) && (pSi->m_toolstrings.vt != VT_EMPTY))
		{
			m_pProject->m_xmlInfo.m_bReadOnly = FALSE;
			m_pProject->m_xmlInfo.Back(-1);
			m_pProject->m_xmlInfo.Write(_T("ToolStrings"), pSi->m_toolstrings);
		}
#endif
		m_pIFile->SaveFileHead(m_pProject);
	}
}
//**************************************************************************************
void CULFile::GetAllCurveProps(CURVEPROPS& vecCurve,
	BOOL bDelAdd /* = FALSE */)
{
	vecCurve.clear();
	for (int i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
		if (pCurve->IsSave())
		{
			CURVEPROPERTY* curve = new CURVEPROPERTY;
			pCurve->GetCurveProp(curve);
			vecCurve.push_back(curve);
		}
	}
}
//**************************************************************************************
BOOL CULFile::DeleteCurve(CCurve* pCurve)
{
	for (int i = 0; i < m_vecCurve.size(); i++)
	{
		if (pCurve == m_vecCurve[i])
		{
			m_vecCurve.erase(m_vecCurve.begin() + i);
			//	pCurve->m_pData->DetachCurves(TRUE);
			pCurve->m_pData->DetachCurves(FALSE);
			return TRUE;
		}
	}
	return FALSE;
}
//**************************************************************************************
void CULFile::AddCurve(CCurve* pCurve)
{
	pCurve->SetDirection(m_nDirection);
	m_vecCurve.push_back(pCurve);
}
//**************************************************************************************
void CULFile::AdjustDirection()
{
	if (m_pDCurve != NULL)
	{
		m_lFrameCount = m_pDCurve->GetDataSize();
		if (m_lFrameCount > 1)
		{
			long lDepth0 = m_pDCurve->GetDepth(0);
			long lDepthE = m_pDCurve->GetDepth(m_lFrameCount - 1);
			m_nDirection = (BOOL) (lDepth0 < lDepthE);
			for (int i = m_vecCurve.size() - 1; i > -1; i--)
			{
				CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
				pCurve->SetDirection(m_nDirection);
			}
		}
	}
}
//**************************************************************************************
ULMIMP CULFile::SaveDataBegin(CULKernel* pKernel, BOOL bSaveAs /* = FALSE */,
	long lStartDepth, long lEndDepth)
{
	if (bSaveAs)
	{
		for (POSITION pos = m_frmList.GetHeadPosition(); pos != NULL;)
		{
			CChildFrame* pFrame = m_frmList.GetNext(pos);
			if (pFrame->m_nViewType == ULV_GRAPHS)
			{
				m_pGraph = pFrame->m_pGraphWnd;
				break;
			}
		}
	}
	else
	{
		if (pKernel == NULL)
			return UL_ERROR;

		m_pDCurve = pKernel->GetDepthCurve(m_vecCurve);
		if (m_pDCurve == NULL)
			return UL_ERROR;
	}

	PreSaveData(lStartDepth,lEndDepth);
	if (m_pIFile)
	{
		ULFBInfo fbi;
		ZeroMemory(&fbi, sizeof(fbi));

		fbi.bScaleUnit = 255;	// 米	

		CURVEPROPS vecCurve;
		if (bSaveAs)
		{
			fbi.bScaleUnit = 255;	// 米		
			fbi.lFrameStep = GetDepthInterval();
			if (fbi.lFrameStep <= 0)
				fbi.lFrameStep = 1000;
			fbi.nDriveMode = m_nDriveMode;
			if (m_nDriveMode == UL_DRIVE_TIME)
			{
				m_pSaveOP->bRecordDown = SCROLL_DOWN;
			}
			fbi.nDirection = m_pSaveOP->bRecordDown;
			fbi.bSetDepth = (m_pSaveOP->dwOption & SO_DEPT) ? TRUE : FALSE;
			fbi.bMetric = m_pSaveOP->bMetric;
			fbi.fVersion = m_pSaveOP->fVersion;
			fbi.bWrapText = (m_pSaveOP->dwOption & SO_WRAP) ? TRUE : FALSE;
            fbi.bDepthOncePerRecord = (m_pSaveOP->dwOption & SO_DEPTH_ONCE_PER_RECORD) ? TRUE : FALSE; 
			fbi.lStep = m_pSaveOP->nStep ? m_pSaveOP->nStep : fbi.lFrameStep;
			fbi.bSetStep = (m_pSaveOP->dwOption & SO_STEP) ?  TRUE : FALSE;

			if (fbi.nDirection)
			{
				fbi.lStartDepth = min(lStartDepth, lEndDepth);
				fbi.lEndDepth = max(lStartDepth, lEndDepth);
			}
			else
			{
				fbi.lStartDepth = max(lStartDepth, lEndDepth);
				fbi.lEndDepth = min(lStartDepth, lEndDepth);
			}

			fbi.bRecordDown = fbi.nDirection;
			GetAllCurveProps(vecCurve);
			if(m_pSaveOP->bCurveRename )
			{
				CCurveNameSpace CNS;
				CNS.Init();
				CNS.Select(m_pSaveOP->dwNameSpaceIndex);
				
				for (int i = 0; i < vecCurve.size(); i++)
				{
					CURVEPROPERTY* pCurve = vecCurve.at(i);
					CString strRawName = pCurve->lpszName ;
					CString strNewName = CNS.GetCurveName(strRawName);
					ZeroMemory(pCurve->lpszName , 64);
					strcpy(pCurve->lpszName , strNewName);
				}
			}

			m_pIFile->SaveBegin(&fbi, vecCurve);

			std::vector<CURVEPROPERTY*>::iterator itPos = vecCurve.begin();
			for (; itPos < vecCurve.end(); itPos++)
				delete * itPos;
			vecCurve.clear();
			return UL_NO_ERROR;
		}

		fbi.lFrameStep = GetDepthInterval();
		fbi.nDriveMode = pKernel->m_logInfo.nMode;
		fbi.nDirection = pKernel->m_logInfo.nDirection;
		m_nDirection = pKernel->m_logInfo.nDirection;

		pKernel->GetAllCurveProps(vecCurve);
		m_pIFile->SaveBegin(&fbi, vecCurve);
		std::vector<CURVEPROPERTY*>::iterator itPos = vecCurve.begin();
		for (; itPos < vecCurve.end(); itPos++)
			delete * itPos;
		vecCurve.clear();
	}

	CString strMessage = g_units->FormatLM(IDS_FILE_FORMAT1, INVALID_DEPTH);
	CClientDC dc(g_pMainWnd);
	CSize sz = dc.GetTextExtent(strMessage);
	g_pMainWnd->m_wndStatusBar.SetPaneWidth(nStatusSave, sz.cx);

	return UL_NO_ERROR;
}

ULMIMP CULFile::PreSaveData(long lStartDepth, long lEndDepth)
{
	int nSamplesCount = m_pDCurve->GetSamplesCount();

	for (int i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
		if (pCurve->IsSave())
		{
			SDATA* pSData = new SDATA;
			pSData->nRSample = pCurve->GetSamplesCount();
			if (nSamplesCount > 1)
			{
				int nSaveSamples = pSData->nRSample;
				if (nSaveSamples >= nSamplesCount)
					nSaveSamples /= nSamplesCount;
				else if (nSaveSamples > 1)
					nSaveSamples = 1;

				pCurve->SetSamplesCount(nSaveSamples);
			}

			pSData->pData = pCurve->m_pData;
			size_t sz = pCurve->m_pData->GetValueSize();
			if (pCurve->m_pData->m_nSamplesCount > 1)
				sz *= pCurve->m_pData->m_nSamplesCount;
			pSData->sz = sz;
			pSData->pValue = new BYTE[sz];
			if (pCurve->GetDataSize())
			{
				pSData->lDepth0 = lStartDepth/*pCurve->GetDepth(0)*/;
				pSData->lDepthE = lEndDepth/*pCurve->GetDepthED()*/;
			}
			else
			{
				pSData->lDepth0 = pSData->lDepthE = 0;
			}
			
			if (pSData->lDepthE < pSData->lDepth0)
			{
				long l = pSData->lDepth0;
				pSData->lDepth0 = pSData->lDepthE;
				pSData->lDepthE = l;
			}
			m_lstSave.AddTail(pSData);
		}
	}

	return UL_NO_ERROR;
}
//**************************************************************************************
#include "FileTrace.h"
void CULFile::SaveDataFrame()
{
	if (m_pIFile == NULL)
	{
		TRACE("return in CULFile::SaveDataFrame() m_pIFile == NULL");
		return;
	}

	// 存储曲线数据
	if (m_pDCurve == NULL)
	{
		TRACE("return in CULFile::SaveDataFrame() m_pDCurve == NULL");
		return;
	}
	int nDepPos = m_pDCurve->GetCurSavePos();	// 当前的存盘位置
	if (nDepPos <= 0)
		return;

	// 对深度数据是否有效进行判断
	// 	if (m_pDCurve->GetDataMode(nDepPos) == DATA_INVALID)
	// 		return;

	long lDepth = m_pDCurve->GetDepth(nDepPos);	// 0.1mm		
	if (lDepth == INVALID_DEPTH)
		return;

	long lTime = m_pDCurve->GetTime(nDepPos);	// 0.1ms
	vec_cv values;

	POSITION pos = m_lstSave.GetHeadPosition();
	for (; pos != NULL;)
	{
		SDATA* pSData = (SDATA*) m_lstSave.GetNext(pos);

		CurveValue cv;
		cv.vt = pSData->pData->GetValueType();
		cv.sz = pSData->sz;
		cv.pValue = pSData->pValue;
		
	//  Add by zy 2011 10 10 , 修改多点采样实时记录无数据问题
	//	pSData->pData->GetCurSaveValues(lDepth, (LPBYTE)cv.pValue, cv.sz, 0);
		long lDLevel = MTD(1.0) / pSData->pData->m_fFeedPoint;
		pSData->pData->GetCurSaveValues(lDepth, (LPBYTE)cv.pValue, cv.sz, lDLevel);
		values.push_back(cv);
	}
	
#ifdef _SAVE_TRACE
	TRACE("<< %d : %ld, %ld\n", nDepPos, lDepth, lTime);
#endif

	if (m_pIFile)
	{
		//	TRACE("lDepthSjh = %f" , DTM(lDepth));
		m_pIFile->SaveFrame(lDepth, lTime, &values, (void *) TRUE);
	}

	values.clear();
}
//**************************************************************************************
void CULFile::SaveDataFrame(int nFrame, int nOff, long lDLevel)
{
	long lDepth, lTime;
	if (m_pDCurve != NULL)
	{
		if (m_pDCurve->GetDataMode(nFrame) == DATA_INVALID)
			return;

		lDepth = m_pDCurve->GetDepth(nFrame);	// 0.1mm		
		lTime = m_pDCurve->GetTime(nFrame);		// 0.1ms
	}
	else
	{
		lDepth = m_vecCurve.at(0)->GetDepth(nFrame);
		lTime = m_vecCurve.at(0)->GetTime(nFrame);
	}

	vec_cv values;
	POSITION pos = m_lstSave.GetHeadPosition();

	int i;

	for (; pos != NULL;)
	{
		SDATA* pSData = (SDATA*) m_lstSave.GetNext(pos);

		CurveValue cv;
		cv.vt = pSData->pData->GetValueType();
		cv.sz = pSData->sz;
		cv.pValue = pSData->pValue;

		pSData->pData->GetCurSaveValues(lDepth, (LPBYTE)cv.pValue, cv.sz, lDLevel, nOff);
		if (cv.pValue != NULL)
		{
			if ((lDepth < pSData->lDepth0) || (lDepth > pSData->lDepthE))
			{
				if (cv.vt == VT_R4)
				{
					for (i = 0; i < cv.sz / sizeof(float); i++)
					{
						((float *) cv.pValue)[i] = -999.25;
					}
				}
				else if (cv.vt == VT_I4)
				{
					for (i = 0; i < cv.sz / sizeof(long); i++)
					{
						((long *) cv.pValue)[i] = -9999;
					}
				}
				else if (cv.vt == VT_R8)
				{
					for (i = 0; i < cv.sz / sizeof(double); i++)
					{
						((double *) cv.pValue)[i] = -999.25;
					}
				}
			}
		}

		values.push_back(cv);
	}

	if (m_pIFile)
		m_pIFile->SaveFrame(lDepth, lTime, &values);

	values.clear();
}
//**************************************************************************************
void CULFile::SaveDataEnd(LPCTSTR pszMarks /* = NULL */)
{
	if (pszMarks != NULL)
		m_strMarks = pszMarks;

	while (m_lstSave.GetCount())
	{
		SDATA* pSData = (SDATA *) m_lstSave.RemoveHead();
		pSData->pData->m_nSamplesCount = pSData->nRSample;
		delete[] pSData->pValue;
		delete pSData;
	}

	if (m_pIFile)
	{
		m_pIFile->SaveEnd(NULL);
	}
}
//**************************************************************************************
short CULFile::ReadFileHead()
{
	CMainFrame* pFrm = (CMainFrame*) theApp.m_pMainWnd;
	
	CStringArray strSheets;
	CStringArray strCells;

	if (m_pProject == NULL)
	{
		m_pProject = new CProject(FALSE, TRUE);
		m_nRead = 1;
	}

	if (m_pIFile)
	{
		//读取文件头失败
		if (m_pIFile->ReadFileHead(m_pProject, &strSheets, &m_celliList,
			&m_pofiList))
			return UL_ERROR;
	}
	else
		return UL_ERROR;
	
	if (m_bReadCurves)
		return UL_NO_ERROR;	

	theApp.SetStatusPrompt(IDS_LOAD_PRESENTATIONS);
	SetTools();
	if (strSheets.GetSize() == 0)
	{
		CFileFind ff;
		
		CString strFind = m_strPathName + _T("*.xml");

		// start working for files
		BOOL bWorking = ff.FindFile(strFind);
		
		while (bWorking)
		{
			bWorking = ff.FindNextFile();
			
			// skip . and .. files; otherwise, we'd
			// recur infinitely!
			
			if (ff.IsDots())
				continue;
			
			// if it's a directory, recursively search it
			if (ff.IsDirectory())
				continue;
			
			strSheets.Add(ff.GetFilePath());
		}
	}

	SetSheets(&strSheets);
	return UL_NO_ERROR;
}
//**************************************************************************************
void CULFile::SetTools()
{
	if (m_arrTools.GetSize() < 1)				// 文件没有加载仪器则返回
		return;

	if (m_pProject != NULL)
	{
		CULKernel* pKernel = &m_pProject->m_ULKernel;
		pKernel->m_arrTools.RemoveAll();
		for (int i = 0; i < m_arrTools.GetSize(); i++)
		{
			CULTool* pULTool = (CULTool*) m_arrTools.GetAt(i);
			pKernel->AddTool(pULTool);
		}
	}
}
//**************************************************************************************

void CULFile::AssignSameTools()
{
	int nSize = m_arrTools.GetSize();
	CPtrArray arrSameTools;
	for (int i = 0; i < nSize; i++)
	{
		CULTool* pTool = (CULTool*) m_arrTools.GetAt(i);
		arrSameTools.Add(pTool);
		for (int j = 0; j < nSize; j++)
		{
			if (j == i)
				continue;
			CULTool* pOTool = (CULTool*) m_arrTools.GetAt(j);
			if (pOTool->strToolName == pTool->strToolName)
			{
				if (j < i)
					break;
				arrSameTools.Add(pOTool);
			}
		}
		int nSameTools = arrSameTools.GetSize();
		if (nSameTools > 1)
		{
			CString strToolName;
			for (int k = 0; k < nSameTools; k++)
			{
				CULTool* pSameTool = (CULTool*) arrSameTools.GetAt(k);
				strToolName = pSameTool->strToolName;
				pSameTool->m_nSameToolIndex = k + 1;
				int nCurves = pSameTool->m_Curves.GetSize();
				for (int l = 0; l < nCurves; l++)
				{
					CCurve* pCurve = pSameTool->m_Curves.GetAt(l);
					pCurve->m_nTLIndex = k + 1;
				}	

				// 映射曲线的时候用的是文件里的曲线，而非仪器中的曲线
				int nIndex = 1;
				CString strCurves;
				int nCurvesCount = m_vecCurve.size();	
				for (int m = 0; m < m_vecCurve.size(); m++)
				{
					CCurve* pCurve = (CCurve*) m_vecCurve.at(m);
					CString strName = pCurve->m_strName;
					
					// 本只仪器里已有这个曲线
					if (strCurves.Find(strName) != -1)
						continue;

					if (pCurve->m_strSource == strToolName &&
						pCurve->m_nTLIndex == 0)
					{
						pCurve->m_nTLIndex = k + 1;
						strCurves = strCurves + ";" + strName;
					}
				}
			}
		}
		arrSameTools.RemoveAll();
	}
}

short CULFile::ReadDataBegin()
{
	if (m_pIFile == NULL)
		return UL_ERROR;

	ULFBInfo fbi;
	ZeroMemory(&fbi, sizeof(ULFBInfo));
	CURVEPROPS vecCurve;
	vecCurve.clear();

	if (m_pIFile->ReadBegin(&fbi, vecCurve))
	{
		return UL_ERROR;
	}

	m_fVersion = fbi.fVersion;//add by gj130903保存LAS版本号
	m_nDirection = fbi.nDirection;

	// 重新设置绘图
	CMapStringToPtr mapCurves;

	m_pDCurve = NULL;
	m_vecCurve.clear();
	for (int i = 0; i < vecCurve.size(); i++)
	{
		CCurve* pCurve = NULL;
		if (m_vecCurve.size() < (i + 1))
		{
			pCurve = new CCurve(NB_FILE);
			m_vecCurve.push_back(pCurve);
		}
		else
			pCurve = (CCurve *) m_vecCurve.at(i);

		CURVEPROPERTY* curve = vecCurve.at(i);	
		pCurve->SetValueType(curve->nValueType);
		pCurve->m_strName = curve->lpszName;
		pCurve->m_strName.TrimRight();
		pCurve->m_strIDName = curve->lpszIDName;
		pCurve->m_strDescrip = curve->lpszDescrip;


		// 记录深度曲线
		if (pCurve->IsDepthCurve())
			m_pDCurve = pCurve;

		pCurve->m_strSource = curve->lpszSrc;

#ifdef _RUNIT
		pCurve->SetUnits(curve->lpszUnit);
		CString strInfoPath;
		int nTools = m_arrTools.GetSize();
		for (int i = 0; i < nTools; i++)
		{
			CULTool* pULTool = (CULTool*) m_arrTools.GetAt(i);
			if (pULTool->strToolName == pCurve->m_strSource)
			{
				strInfoPath = Gbl_ConfigInfo.FindInfoPath(pULTool->strToolDllDir);
				pULTool->strInfoPath = strInfoPath;
				break;
			}
		}

		if (strInfoPath.GetLength())
		{
			pCurve->m_pData->CalcUnit(g_units, strInfoPath);
			CPtrList* pT = NULL;
			if (mapCurves.Lookup(strInfoPath, (void * &) pT))
			{
				pT->AddTail(pCurve);
			}
			else
			{
				pT = new CPtrList;
				pT->AddTail(pCurve);
				mapCurves.SetAt(strInfoPath, pT);
			}
		}
#else
		pCurve->SetUnit(curve->lpszUnit);
#endif // _RUNIT


		pCurve->SetPointFrame(curve->nPointFrame);
		if (curve->nDimension > 1)
			pCurve->Dimension(curve->nDimension);
		if (curve->nArrayCount > 1)
		{
			pCurve->SetArrayCount(curve->nArrayCount);
			pCurve->SetReceiver(curve->nRecver, &curve->fRRDistance);
			pCurve->SetSender(curve->nSender, &curve->fSRDistance);
		}

		pCurve->TimeInter(curve->fTimeInter);
		pCurve->TimeDelay(curve->fTimeDelay);

		pCurve->SetSamplesCount(curve->nSamplesCount);
		pCurve->SetDirection (m_nDirection);
		pCurve->m_pData->m_nDirection = m_nDirection;

		if (curve->cbInitSize < 4096)
		{
			pCurve->m_pData->MInit(curve->cbInitSize);
		}
		else
			pCurve->m_pData->MInit();

		delete curve;
		/*	TRACE("curve %d sizepoint*pointframe : %d*%d\n", i, pCurve->m_nSizePoint, pCurve->m_nPointFrame);*/
	}

	CString strInfoPath;
	POSITION pos = mapCurves.GetStartPosition();
	for (; pos != NULL;)
	{
		CPtrList* pT = NULL;
		mapCurves.GetNextAssoc(pos, strInfoPath, (void * &) pT);
		CXMLSettings xml;
		CString strIni = Gbl_AppPath +
			"\\Tools\\" +
			strInfoPath +
			"Curves.ini";
		if (xml.ReadXMLFromFile(strIni))
		{
			POSITION posc = pT->GetHeadPosition();
			for (; posc != NULL;)
			{
				CCurve* pCurve = (CCurve*) pT->GetNext(posc);
				if (xml.Open(pCurve->m_strName))
				{
					pCurve->Serialize(xml);
					xml.Back();
				}
			}
		}

		delete pT;
	}

	m_lFrameCount = fbi.lFrameCount;
	if (fbi.nDriveMode == UL_DRIVE_TIME)
		m_nDriveMode = UL_DRIVE_TIME;
	else
		m_nDriveMode = UL_DRIVE_DEPT;

	return UL_NO_ERROR;
}
//**************************************************************************************
short CULFile::ReadDataFrame(int nPos)
{
	if (m_pIFile == NULL)
		return UL_ERROR;

	// 读取曲线数据
	long lDepth = 0;
	long lTime = 0;
	vec_cv values;
	return m_pIFile->ReadFrame(nPos, &lDepth, &lTime, &values);
}
//**************************************************************************************
void CULFile::ReadDataEnd()
{
	if (m_pIFile)
		m_pIFile->ReadEnd(NULL);

	// 检查深度数据是否回滚
	if (m_vecCurve.size() > 0)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(0);
		int nCount = pCurve->GetDataSize();
		if (nCount > 0)
		{
			long lDepth0 = pCurve->GetDepth(0);
			long lDepth1 = pCurve->GetDepth(nCount - 1);
			if (lDepth0 < lDepth1)
				m_nDirection = SCROLL_DOWN;
			else
				m_nDirection = SCROLL_UP;
		}

		if (m_pDCurve == NULL)
		{
			m_pDCurve = pCurve;
		}

		if (m_pDCurve)
			m_lFrameCount = m_pDCurve->GetDataSize();
	}

	for (int i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
		pCurve->m_pData->CheckDepth(m_nDirection);
	}

	// add by bao 打开文件时加载曲线描述
	CString strConfig = Gbl_AppPath + "\\Config\\" + "CurveDescription.xml";
	BOOL bExit = FALSE;
	for ( i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
		CXMLSettings xml;
		if (xml.ReadXMLFromFile(strConfig))
		{
			CStringList strNameList;
			xml.ReadSubKeys(strNameList);
			CString strName = pCurve->m_strName;
			if (strNameList.Find(strName))
			{
				xml.Open(strName);
				xml.Read("CurveDescrip", pCurve->m_strDescrip);
				xml.Back(-1);
				bExit = TRUE;
			}
		}
		xml.Close();
		if (!bExit)
		{
			CXMLSettings xmlSave(FALSE, "CurveDescription");
			xmlSave.ReadXMLFromFile(strConfig);
			xmlSave.CreateKey(pCurve->m_strName); 
			xmlSave.Write("CurveDescrip", pCurve->m_strDescrip); 
			xmlSave.Back(1);
			xmlSave.WriteXMLToFile(strConfig);
			bExit = FALSE;
			xmlSave.Close();
		}
	}
	// end

	InterlockedExchange(&m_nRead, 2);
}
//**************************************************************************************
ULMPTRIMP CULFile::GetCurve(LPCTSTR pszName)
{
	for (int i = m_vecCurve.size() - 1; i > -1; i--)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
		if (pCurve->m_strName == pszName)
			return pCurve;
	}

	return NULL;
}

ULMPTRIMP CULFile::GetCurve(LPCTSTR pszName, LPCTSTR lpszSrc)
{
	for (int i = m_vecCurve.size() - 1; i > -1; i--)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
		if (pCurve->m_strName == pszName && pCurve->m_strSource == lpszSrc)
			return pCurve;
	}
	
	return NULL;
}
//**************************************************************************************
ULMPTRIMP CULFile::GetCurveNoCase(LPCTSTR pszName)
{
	for (int i = m_vecCurve.size() - 1; i > -1; i--)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
		if (pCurve->m_strName.CompareNoCase(pszName) == 0)
			return pCurve;
	}

	return NULL;
}
//**************************************************************************************
ULMIMP CULFile::AddCurveData(int nCurve, long lDepth, void* pValue,
	long lTime /* = 0 */)
{
	if (nCurve < 0)
		return UL_ERROR;

	if (nCurve < m_vecCurve.size())
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(nCurve);
		pCurve->m_pData->AddB(lDepth, pValue, lTime);

		return UL_NO_ERROR;
	}

	return UL_ERROR;
}
//**************************************************************************************
ULMIMP CULFile::AddCurveDatas(int nCurve, long lDepth, long lNextDepth,
	void* pValue, long lTime /* = 0 */)
{
	if (nCurve < 0)
		return UL_ERROR;

	if (nCurve < m_vecCurve.size())
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(nCurve);
		if (pCurve->m_pData == NULL)
			return UL_ERROR;

		CCurveData* pData = pCurve->m_pData;
		if (pData->m_nSamplesCount > 0)
		{
			size_t sz = pData->GetValueSize();
			for (int j = 0; j < pData->m_nSamplesCount; j++)
			{
				long lDepthj = lDepth +
					floor((double)
						(j * (lNextDepth - lDepth) /
					(double) pData->m_nSamplesCount +
					.5));

				pData->AddB(lDepthj, (void *) &(((LPBYTE) pValue)[j * sz]),
						lTime);
				

			}

			return UL_NO_ERROR;
		}

		if (pData->GetSize() % (abs(pData->m_nSamplesCount) + 1) == 0)
		{
			pData->AddB(lDepth, pValue, lTime);
	
		}

		return UL_NO_ERROR;
	}

	return UL_ERROR;
}
//**************************************************************************************
ULMLNGIMP CULFile::GetTime(DWORD dwTime /* = FT_CREATE */)
{
	if (dwTime & FT_CREATE)
	{
		return m_tmCreate.GetTime();
	}

	return m_tmModify.GetTime();
}
//**************************************************************************************
ULMIMP CULFile::SetTime(time_t time, DWORD dwTime /* = FT_CREATE */)
{
	if (dwTime & FT_CREATE)
	{
		m_tmCreate = time;
	}

	if (dwTime & FT_MODIFY)
	{
		m_tmModify = time;
	}

	return UL_NO_ERROR;
}
//**************************************************************************************
BOOL CULFile::CanCloseFrame(CLOSE_ALL_FILE* pInfo/* = NULL*/)
{
	for (POSITION pos = m_frmList.GetHeadPosition(); pos != NULL;)
	{
		CChildFrame* pChild = m_frmList.GetNext(pos);
		if (pChild->m_dwLocked)
			return FALSE;
	}

	if (!IsModified())
		return TRUE;		// ok to continue
	
	CString strPrompt;
	AfxFormatString1(strPrompt, AFX_IDP_ASK_TO_SAVE, m_strPathName);

	if (pInfo != NULL)
	{
		CCloseAllFileDlg dlg;
		//dlg.m_strPrompt = strPrompt;
		dlg.m_strPrompt = m_strPathName;
		if (!pInfo->bNoAll && !pInfo->bYesAll)
		{
			dlg.m_pInfo = pInfo;
			dlg.DoModal();
		}

		if (pInfo->bCancel)
			return FALSE;
		if (pInfo->bYesAll)
		{
			if (!DoFileSave())
				return FALSE; 
			return TRUE;
		}
		if (pInfo->bNoAll)
		{
			return TRUE;
		}
		if (dlg.m_bYesNo)
		{
			if (!DoFileSave())
				return FALSE; 
			return TRUE;
		}
		if (!dlg.m_bYesNo)
		{
			return TRUE;
		}
	}

	switch (AfxMessageBox(strPrompt, MB_YESNOCANCEL|MB_ICONQUESTION))
	{
	case IDCANCEL:
		return FALSE;	// don't continue

	case IDYES:
		// If so, either Save or Update, as appropriate
		if (!DoFileSave())
			return FALSE;   	// don't continue
		break;

	case IDNO:
		// If not saving changes, revert the document
		break;

	default:
		ASSERT(FALSE);
		break;
	}
	return TRUE;	// keep going
	/*	for (POSITION pos = m_frmList.GetHeadPosition(); pos != NULL; )
		{
			CChildFrame* pChild = m_frmList.GetNext(pos);
			if (pChild->m_dwLocked)
				return FALSE;
		}

		if (!IsModified())
			return TRUE;		// ok to continue
		
		CString strPrompt;
		AfxFormatString1(strPrompt, AFX_IDP_ASK_TO_SAVE, m_strPathName);
		switch (AfxMessageBox(strPrompt, MB_YESNOCANCEL, AFX_IDP_ASK_TO_SAVE))
		{
		case IDCANCEL:
			return FALSE;	// don't continue

		case IDYES:
			// If so, either Save or Update, as appropriate
			if (!DoFileSave())
				return FALSE;   	// don't continue
			break;
			
		case IDNO:
			// If not saving changes, revert the document
			break;
			
		default:
			ASSERT(FALSE);
			break;
		}
		return TRUE;	// keep going*/
}
//**************************************************************************************
BOOL CULFile::DoFileSave()
{
	CWaitCursor wait;
	return Save(FALSE);
}
//**************************************************************************************
void CULFile::CloseFrames()
{
	// 关闭测井窗口
	CULDoc::CloseFrames();

	while (m_pofiList.GetCount())
		delete m_pofiList.RemoveHead();

	while (m_celliList.GetCount())
		delete m_celliList.RemoveHead();
}
//**************************************************************************************
long CULFile::GetDepthInterval()
{
	if (m_pDCurve == NULL)
		return -1;

	int nLen = m_pDCurve->GetDataSize();
	if (nLen < 2)
		return -1;

	long lDepth0 = m_pDCurve->GetDepth(0);
	long lDepth1 = m_pDCurve->GetDepth(nLen - 1);
	long lStep = floor(fabs(lDepth0 - lDepth1) / (double)(nLen - 1) + .5);
	return lStep;
}
//**************************************************************************************
ULMIMP CULFile::SetParam(LPCTSTR lpszParam)
{
	if (m_bReadCurves)
		return UL_NO_ERROR;

	m_strParam = lpszParam;
	while (m_tpTree.GetCount())
		delete m_tpTree.RemoveHead();

	CFile file;
	if (!file.Open(lpszParam, CFile::shareDenyNone))
		return UL_ERROR;

	// Read the tools' params from file
	CXMLSettings xml;
	while (xml.ReadXMLFromFile(&file))
	{
		CToolParams* pTPs = new CToolParams;
		pTPs->Serialize(xml, TRUE);
		m_tpTree.AddTail(pTPs);
	}
	file.Close();

	return UL_NO_ERROR;
}

ULMTSTRIMP  CULFile::GetMarks() 
{
	CXMLSettings xml(FALSE);

	for (int i = m_vecCurve.size() - 1; i > -1; i--)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
		if (xml.Open(pCurve->m_strSource))
		{
			if (xml.Open(pCurve->m_strName))
			{
				long lFRDepth = INVALID_DEPTH;
				xml.Read("FRDepth", lFRDepth);
				pCurve->FRDepth(lFRDepth);
				xml.Back();
			}
			xml.Back();
		}
	}

	if (xml.CreateKey("CurveBoundsMarks"))
	{
		POSITION pos = m_sheetiList.GetHeadPosition();
		int nSheet = 0;
		for (; pos != NULL;)
		{
			CSheetInfo* pInfo = (CSheetInfo*) m_sheetiList.GetNext(pos);	
			CString strSheet;
			strSheet.Format("Sheet%d", nSheet);
			if (xml.CreateKey(strSheet))
			{
				int nTrackCount = pInfo->pSheet->m_TrackList.GetSize();
				for (int j = 0; j < nTrackCount; j++)
				{
					CTrack* pTrack = (CTrack*)
						pInfo->pSheet->m_TrackList.GetAt(j);
					CString strTrack;
					strTrack.Format("Track%d", j);
					if (xml.CreateKey(strTrack))
					{
						pTrack->SerializeMark(xml);
						xml.Back();
					}
				}
				xml.Back();
			}
			nSheet++;
		}
		xml.Back();
	}

	CFile file;
	if (file.Open(m_strMarks, CFile::modeCreate|CFile::modeWrite))
	{
		BOOL b = xml.WriteXMLToFile(&file);
	}	

	return (LPTSTR) (LPCTSTR)m_strMarks; 
}

//***********************读取标签****************************************************
ULMIMP CULFile::SetMarks(LPCTSTR lpszMarks)
{
	m_strMarks = lpszMarks;
	CFile file;
	if (!file.Open(lpszMarks, CFile::modeRead))
		return UL_ERROR;

	CXMLSettings xml;
	if (xml.ReadXMLFromFile(&file))
	{
		for (int i = m_vecCurve.size() - 1; i > -1; i--)
		{
			CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
			if (xml.Open(pCurve->m_strSource))
			{
				if (xml.Open(pCurve->m_strName))
				{
					long lFRDepth = INVALID_DEPTH;
					xml.Read("FRDepth", lFRDepth);
					pCurve->FRDepth(lFRDepth);
					xml.Back();
				}
				xml.Back();
			}
		}
#ifdef _PERFORATION
		//读取该枪信息
		ShootItem shootItem;
		if (xml.Open(_T("PeforationInfos")))
		{
			if (xml.Open(_T("ShootInfo")))
			{
				xml.Read(_T("Index"), shootItem.index);
				xml.Read(_T("Index0"), shootItem.index0);
				xml.Read(_T("IndexE"), shootItem.indexE);
				xml.Read(_T("Stdcollarindex"), shootItem.stdcollarindex);
				xml.Read(_T("Mark"), shootItem.mark);
				xml.Read(_T("Mode"), shootItem.mode);
				xml.Read(_T("Finished)"), shootItem.finished);
				xml.Read(_T("Depth"), shootItem.depth);
				xml.Read(_T("Value"), shootItem.value);
				xml.Back();
			}

			xml.Back();
		}

		//读取输入节箍（深度矫正后）
		if (xml.Open(_T("PeforationInfos")))
		{
			if (xml.Open(_T("RefCollarsInfo")))
			{
				int iCount = 0;
				long lDepthAdjust = g_pTask->m_lDepthAdjust;
				if (TRUE ==
					xml.Read(_T("Count"), iCount) &&
					TRUE ==
					xml.Read("DepthAdjust",
							lDepthAdjust))
				{
					CString str;
					for (int j = 0; j < iCount; j++)
					{
						str.Format(_T("RefCollars%02d"), j);
						xml.Open(str);

						long depth;
						if (FALSE == xml.Read(_T("depth"), depth))
							continue;

						CTrack* pTrack = NULL;
						CSheetInfo* pSheetInfo = NULL;
						POSITION pos = m_sheetiList.FindIndex(0);
						if (pos != NULL)
						{
							pSheetInfo = m_sheetiList.GetAt(pos);
							if (NULL == pSheetInfo)
								continue;

							if (pSheetInfo && pSheetInfo->pSheet)
							{
								if (0 <=
									pSheetInfo->pSheet->m_TrackList.GetSize())
								{
									pTrack = (CTrack *)
										pSheetInfo->pSheet->m_TrackList.GetAt(0);
									if (NULL == pTrack)
										continue;
								}
								else
									break;
							}
						}

						if (0 != depth)
						{
							if (shootItem.stdcollarindex == j + 1)
								pTrack->m_Marker.AddMark(pTrack, depth, j + 1,
													shootItem.index + 1,
													MARKS_PERFORATION_COLLOR,
													lDepthAdjust);
							else
								pTrack->m_Marker.AddMark(pTrack, depth, j + 1,
													-1,
													MARKS_PERFORATION_COLLOR,
													lDepthAdjust);
						}	
						xml.Back();
					}
					xml.Back();
				}
			}
			xml.Back();
		}

		//读取实测节箍
		if (xml.Open(_T("PeforationInfos")))
		{
			if (xml.Open(_T("RTCollarsInfo")))
			{
				CString str(_T("RTCollarsRun01"));
				for (int i = 0; xml.Open(str); i++)
				{
					int iCount = 0;
					if (FALSE == xml.Read(_T("Count"), iCount))
						continue;

					for (int j = 0; j < iCount; j++)
					{
						str.Format(_T("RTCollars%02d"), j);
						xml.Open(str);

						long depth;
						if (FALSE == xml.Read(_T("depth"), depth))
							continue;

						CTrack* pTrack = NULL;
						CSheetInfo* pSheetInfo = NULL;
						POSITION pos = m_sheetiList.FindIndex(0);
						if (pos != NULL)
						{
							pSheetInfo = m_sheetiList.GetAt(pos);
							if (NULL == pSheetInfo)
								continue;

							if (pSheetInfo && pSheetInfo->pSheet)
							{
								if (3 +
									2 * i <=
									pSheetInfo->pSheet->m_TrackList.GetSize())
								{
									pTrack = (CTrack *)
										pSheetInfo->pSheet->m_TrackList.GetAt(3 +
																			2 * i);
									if (NULL == pTrack)
										continue;
								}
								else
									break;
							}
						}

						if (0 != depth)
						{
							/*if(1 == i%2) 
							{
								if(shootItem.stdcollarindex == iCount-j)
									pTrack->AddMarker(depth, iCount-j, i+1);
								else
									pTrack->AddMarker(depth, iCount-j);
							}
							else */
							{
								if (shootItem.stdcollarindex == j + 1)
									pTrack->m_Marker.AddMark(pTrack, depth,
														j + 1, i + 1);
								else
									pTrack->m_Marker.AddMark(pTrack, depth,
														j + 1);
							}
						}	
						xml.Back();
					}

					if (TRUE == xml.Open("FireLine"))//Start:
					{
						long depth;
						if (TRUE == xml.Read(_T("depth"), depth))
						{
							CTrack* pTrack = NULL;
							CSheetInfo* pSheetInfo = NULL;
							POSITION pos = m_sheetiList.FindIndex(0);
							if (pos != NULL)
							{
								pSheetInfo = m_sheetiList.GetAt(pos);
								if (NULL == pSheetInfo)
									continue;

								if (pSheetInfo && pSheetInfo->pSheet)
								{
									if (3 +
										2 * i <=
										pSheetInfo->pSheet->m_TrackList.GetSize())
									{
										pTrack = (CTrack *)
											pSheetInfo->pSheet->m_TrackList.GetAt(3 +
																				2 * i);
										if (NULL == pTrack)
											continue;
									}
									else
										break;
								}
							}

							if (NULL != pTrack)
							{
								pTrack->m_Marker.AddMark(pTrack, depth, -1,
													-1, MARKS_FIRE_LINE);
								pTrack = (CTrack *)
									pSheetInfo->pSheet->m_TrackList.GetAt(2 +
																		2 * i);
								if (NULL != pTrack)
								{
									pTrack->m_Marker.AddMark(pTrack, depth,
														-1, -1,
														MARKS_FIRE_LINE1);
								}
							}
						}
						xml.Back();
					}//End: if(TRUE == xml.Open("FireLine"))

					if (TRUE == xml.Open("MarkLine"))//Start:
					{
						long depth;
						if (TRUE == xml.Read(_T("depth"), depth))
						{
							CTrack* pTrack = NULL;
							CSheetInfo* pSheetInfo = NULL;
							POSITION pos = m_sheetiList.FindIndex(0);
							if (pos != NULL)
							{
								pSheetInfo = m_sheetiList.GetAt(pos);
								if (NULL == pSheetInfo)
									continue;

								if (pSheetInfo && pSheetInfo->pSheet)
								{
									if (3 +
										2 * i <=
										pSheetInfo->pSheet->m_TrackList.GetSize())
									{
										pTrack = (CTrack *)
											pSheetInfo->pSheet->m_TrackList.GetAt(3 +
																				2 * i);
										if (NULL == pTrack)
											continue;
									}
									else
										break;
								}
							}

							pTrack->m_Marker.AddMark(pTrack, depth, -1, -1,
												MARKS_MARK_LINE);
							pTrack = (CTrack *)
								pSheetInfo->pSheet->m_TrackList.GetAt(2 +
																	2 * i);
							if (NULL != pTrack)
							{
								pTrack->m_Marker.AddMark(pTrack, depth, -1,
													-1, MARKS_MARK_LINE1);
							}
						}
						xml.Back();
					} //End:if(TRUE == xml.Open("MarkLine"))

					if (TRUE == xml.Open("BlockCurve")) //Start:
					{
						long depth;
						if (TRUE == xml.Read(_T("depth"), depth))
						{
							CTrack* pTrack = NULL;
							CSheetInfo* pSheetInfo = NULL;
							POSITION pos = m_sheetiList.FindIndex(0);
							if (pos != NULL)
							{
								pSheetInfo = m_sheetiList.GetAt(pos);
								if (NULL == pSheetInfo)
									continue;

								if (pSheetInfo && pSheetInfo->pSheet)
								{
									if (3 +
										2 * i <=
										pSheetInfo->pSheet->m_TrackList.GetSize())
									{
										pTrack = (CTrack *)
											pSheetInfo->pSheet->m_TrackList.GetAt(3 +
																				2 * i);
										if (NULL == pTrack)
											continue;
									}
									else
										break;
								}
							}

							pTrack->m_Marker.AddMark(pTrack, depth, -1, -1,
												MARKS_BLOCK_CURVE);
						}
						xml.Back();
					}//End:if(TRUE == xml.Open("BlockCurve"))

					str.Format(_T("RTCollarsRun%02d"), i + 2);
					xml.Back();
				}
				xml.Back();
			}	
			xml.Back();
		}
		#endif

		////////////////////////////////////

		//		CPerforation PerforationInfo(PERFORATIONFILE);
		//		PerforationInfo.Serialize(xml);


		/*		POSITION pos = m_sheetiList.FindIndex(0);
					CSheetInfo* pSheetInfo = NULL;
					if (pos != NULL)
					{
						pSheetInfo = m_sheetiList.GetAt(pos);
						if (pSheetInfo && pSheetInfo->pSheet)
						{
							CTrack* Ref = (CTrack*)pSheetInfo->pSheet->m_TrackList.GetAt(0);
							CTrack* RT	= (CTrack*)pSheetInfo->pSheet->m_TrackList.GetAt(2);
							PerforationInfo.SetRefTrack(Ref);
							PerforationInfo.SetRTTrack(RT);
							PerforationInfo.Sync(SYNC_MARKER);
						}
					}*/

		if (xml.Open("CurveBoundsMarks"))
		{
			POSITION pos = m_sheetiList.GetHeadPosition();
			int nSheet = 0;
			for (; pos != NULL;)
			{
				CSheetInfo* pInfo = (CSheetInfo*) m_sheetiList.GetNext(pos);	
				CString strSheet;
				strSheet.Format("Sheet%d", nSheet);
				if (xml.Open(strSheet))
				{
					int nTrackCount = pInfo->pSheet->m_TrackList.GetSize();
					for (int j = 0; j < nTrackCount; j++)
					{
						CTrack* pTrack = (CTrack*)
							pInfo->pSheet->m_TrackList.GetAt(j);
						CString strTrack;
						strTrack.Format("Track%d", j);
						if (xml.Open(strTrack))
						{
							pTrack->SerializeMark(xml);
							xml.Back();
						}
					}
					xml.Back();
				}
				nSheet++;
			}
			xml.Back();
		}
	}
	return UL_NO_ERROR;
}
//**************************************************************************************
void CULFile::OpenFrames(DWORD dwTypes /* = ULV_GRAPHS */)
{
	if (m_vecCurve.size() > 0)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(0);
		int nCount = pCurve->GetDataSize();
		if (nCount > 0)
		{
			long lDepth0 = pCurve->GetDepth(0);
			long lDepth1 = pCurve->GetDepth(nCount - 1);
			if (lDepth0 < lDepth1)
				m_nDirection = SCROLL_DOWN;
			else
				m_nDirection = SCROLL_UP;
		}
	}

	if (m_sheetiList.GetCount() < 1)
	{
		m_bAutoMap = TRUE;
		CSheetInfo* pSheetInfo = new CSheetInfo;
		pSheetInfo->pSheet = new CSheet;
		pSheetInfo->pSheet->AddRef();
		pSheetInfo->pSheet->MatchSheet(m_vecCurve, 256);
		pSheetInfo->strTempl = pSheetInfo->pSheet->m_strName;
		pSheetInfo->pSheet->SetLogMode(WORKMODE_IDLE, m_nDriveMode);
		m_sheetiList.AddTail(pSheetInfo);
	}
	else
	{
		for (POSITION pos = m_sheetiList.GetHeadPosition(); pos != NULL;)
		{
			CSheetInfo* pSheetInfo = m_sheetiList.GetNext(pos);
			CSheet* pSheet = pSheetInfo->pSheet;
			if (m_bAutoMap)
				pSheet->AutoMap(m_vecCurve);
			else if (0 == m_strExtent.CompareNoCase(_T("PEF")))
				pSheet->ReMapPerforationCurve(m_vecCurve);
			else
				pSheet->RemapTrackCurve(m_vecCurve);

			pSheet->SetLogMode(WORKMODE_IDLE, m_nDriveMode);
		}
	}

	CULDoc::OpenFrames(dwTypes);

	// 打开仪器参数表
	DWORD dwStyle = WS_CHILD;
	BOOL bShow = FALSE;
	if (dwTypes & ULV_PARAM)
	{
		dwStyle |= WS_VISIBLE;
		bShow = TRUE;
	}

	CChildFrame* pChild = new CChildFrame(ULV_PARAM, &m_tpTree, this);
	pChild->m_dwState &= ~ST_SHOW;
	if (!pChild->LoadFrame(IDR_MAINFRAME, dwStyle, theApp.m_pMainWnd))
		TRACE0("Warning: Couldn't create a frame of tool param.\n");
	else
	{
		pChild->SetTitle(IDS_TOOL_PARA_TABLE);
	}

	g_pMainWnd->MDITabActivate(m_frmList.GetHead());
}
//**************************************************************************************
void CULFile::GetRecord(CString& strDoc, CString& strTime, CString& strRecord,
	CString& strDepthST, CString& strDepthED, CSheet* pSheet)
{
	strDoc = GetSimpleName();
	
	if (m_pIFile == NULL)
	{
		strRecord.LoadString(IDS_FILE_FORMAT5);
		CFileStatus status;
		if (CFile::GetStatus(m_strPathName, status))
		{
			strTime = status.m_ctime.Format("%b %d %H:%M %Y");
		}
		else
		{
			strTime.LoadString(IDS_FILE_FORMAT9);
		}
	}
	else
	{
		if (GetTime(FT_CREATE) == GetTime(FT_MODIFY))
		{
			strRecord.LoadString(IDS_FILE_FORMAT11);
			strTime = m_tmCreate.Format("%b %d %H:%M %Y");
		}
		else
		{
			strRecord.LoadString(IDS_FILE_FORMAT12);
			strTime = m_tmModify.Format("%b %d %H:%M %Y");
		}
	}
	
	long lDepth0 = 0, lDepth1 = 0;
	
	if (m_pDCurve)
	{
		if (m_nDriveMode == UL_DRIVE_DEPT)
		{
			lDepth0 = m_pDCurve->GetDepth(0);
			lDepth1 = m_pDCurve->GetDepthED();
			if (lDepth0 < lDepth1)
			{
				if (pSheet->m_lStartDepth > lDepth0)
					lDepth0 = pSheet->m_lStartDepth;
				
				if (pSheet->m_lEndDepth < lDepth1)
					lDepth1 = pSheet->m_lEndDepth;
			}
			else
			{
				if (pSheet->m_lStartDepth > lDepth1)
					lDepth1 = pSheet->m_lStartDepth;
				
				if (pSheet->m_lEndDepth < lDepth0)
					lDepth0 = pSheet->m_lEndDepth;
			}
		} 
		else
		{
			lDepth0 = m_pDCurve->GetTime(0);
			lDepth1 = m_pDCurve->GetTimeED();
		}
	}
	
	strDepthST = g_units->FormatLM("%.3lf %s", lDepth0);
	strDepthED = g_units->FormatLM("%.3lf %s", lDepth1);
}
//**************************************************************************************
void CULFile::GetRecord2(CString& strComp, CString& strTeam,
	CString& strWell, CString& strField)
{

	// 	if (m_pProject)
	// 	{
	// 		strComp = m_pProject->m_NewProjectInfo.infoDrilling.strCompany;
	// 		strTeam = m_pProject->m_NewProjectInfo.infoDrilling.strTeam;
	// 		strCapt = m_pProject->m_NewProjectInfo.infoTeam.strCaptain;
	// 		strOprt = m_pProject->m_NewProjectInfo.infoTeam.strOperator;
	//	}
	
	CXMLSettings* pXml = &m_pProject->m_xmlInfo;	
	pXml->Back(-1);
	pXml->Open("Information");
	pXml->Open("System");
	CXMLNodes* pNodes = &(pXml->m_pCurrNode->m_lstChildren);
	int nCount = pNodes->GetCount();
	if (nCount < 4)
		return;
	
	POSITION pos1 = pNodes->FindIndex(0);
	CStringArray strATmp;	
	CString strTmp,strCmt;
	pXml->SetCurrNode(pNodes->GetAt(pos1));
	CXMLNode* pNode = pXml->m_pCurrNode;
	if (pNode == NULL)
		return;
	CXMLNodes* pChildNodes = &pNode->m_lstChildren;
	nCount = pChildNodes->GetCount();
	if (nCount < 1)
		return;
	POSITION pos = pChildNodes->GetHeadPosition();
	strATmp.RemoveAll();
	for (int i = 0; i < nCount; i++)
	{
		if (pos == NULL)
			break;
		pXml->SetCurrNode(pChildNodes->GetNext(pos));
		pXml->Read("Value", strTmp);
		pXml->Read("Remark", strCmt);
		strATmp.Add(strTmp);
	}
	if (nCount > 5)
		strComp = strATmp.GetAt(5);
	
	POSITION pos2 = pNodes->FindIndex(3); 
	pXml->SetCurrNode(pNodes->GetAt(pos2));
	pNode = pXml->m_pCurrNode;
	if (pNode == NULL)
		return;
	pChildNodes = &pNode->m_lstChildren;
	nCount = pChildNodes->GetCount();
	if (nCount < 1)
		return;
	pos = pChildNodes->GetHeadPosition();
	strATmp.RemoveAll();
	for (i = 0; i < nCount; i++)
	{
		if (pos == NULL)
			break;
		pXml->SetCurrNode(pChildNodes->GetNext(pos));
		pXml->Read("Value", strTmp);
		pXml->Read("Remark", strCmt);
		strATmp.Add(strTmp);
	}
	if (nCount > 4)
	{
		strTeam = strATmp.GetAt(4);
		// 		strDevice = strATmp.GetAt(5);
		// 		strTLSN = strATmp.GetAt(6);
	}
	
	POSITION pos3 = pNodes->FindIndex(1); 
	pXml->SetCurrNode(pNodes->GetAt(pos3));
	pNode = pXml->m_pCurrNode;
	if (pNode == NULL)
		return;
	pChildNodes = &pNode->m_lstChildren;
	nCount = pChildNodes->GetCount();
	if (nCount < 1)
		return;
	pos = pChildNodes->GetHeadPosition();
	strATmp.RemoveAll();
	for (i = 0; i < nCount; i++)
	{
		if (pos == NULL)
			break;
		pXml->SetCurrNode(pChildNodes->GetNext(pos));
		pXml->Read("Value", strTmp);
		pXml->Read("Remark", strCmt);
		strATmp.Add(strTmp);
	}
	if (nCount > 9)
	{
		strField = strATmp.GetAt(0);
		strWell = strATmp.GetAt(9);
	}
	
	pXml->Back(-1);
}
//**************************************************************************************
ULMIMP CULFile::SaveCell(int i, LPCTSTR lpszCell)
{
	POSITION pos = m_celliList.FindIndex(i);
	if (pos == NULL)
		return UL_ERROR;

	CCellInfo* pCellInfo = m_celliList.GetAt(pos);
	CChildFrame* pChild = pCellInfo->pFrame;
	if (pChild->GetSafeHwnd() != NULL)
	{
		if (pChild->m_pGHView->SaveFileAs(lpszCell))
			return UL_NO_ERROR;
	}

	if (pCellInfo->strFile.GetLength())
	{
		if (CopyFile(pCellInfo->strFile, lpszCell, FALSE))
			return UL_NO_ERROR;
	}
	else
	{
		if (CopyFile(HTS_FILE(pCellInfo->strTempl), lpszCell, FALSE))
			return UL_NO_ERROR;
	}

	return UL_ERROR;
}
//**************************************************************************************
ULMIMP CULFile::GetPrints(CStringArray* pArrPrint, CStringArray* pArrTempl)
{
	pArrPrint->RemoveAll();
	if (m_pProject != NULL)
	{
		if (m_pProject->m_pService != NULL)	// 工程中保存
		{
			while (m_pofiList.GetCount())
				delete m_pofiList.RemoveHead();

			m_pProject->m_pService->GetPrints(pArrPrint, pArrTempl);
			return UL_NO_ERROR;
		}

		for (POSITION pos = m_pofiList.GetHeadPosition(); pos != NULL;)
		{
			CPofInfo* pPofInfo = m_pofiList.GetNext(pos);
			pArrPrint->Add(pPofInfo->strFile);
			pArrTempl->Add(pPofInfo->strTempl);
		}

		return UL_NO_ERROR;
	}

	return UL_ERROR;
}
//**************************************************************************************
ULMIMP CULFile::SetPrints(CStringArray* pArrPrint, CStringArray* pArrTempl)
{
	int nSize = pArrPrint->GetSize();
	if (pArrTempl->GetSize() != nSize)
		return UL_ERROR;

	while (m_pofiList.GetCount())
		delete m_pofiList.RemoveHead();

	for (int i = 0; i < nSize; i++)
	{
		CPofInfo* pPofInfo = new CPofInfo;
		pPofInfo->strFile = pArrPrint->GetAt(i);
		pPofInfo->strTempl = pArrTempl->GetAt(i);
		m_pofiList.AddTail(pPofInfo);
	}

	return UL_NO_ERROR;
}
//**************************************************************************************
ULMTSTRIMP CULFile::GetWellName()
{
	if (m_pProject)
		return (LPTSTR) (LPCTSTR) m_pProject->m_strPName;

	return _T("");
}
//**************************************************************************************
void CULFile::GetValueInfo(UINT nIndex, UINT nTargetIndex, ValueInfo& vi)
{
	/*	if (nIndex >= m_arData.GetSize())
			return;
		if (nTargetIndex >= m_vecTarget.size())
			return;
		vi.lDepth = m_vecData[nIndex].lDepth;
		vi.lVertDepth = m_vecData[nIndex].lVertDepth;
		vi.lOffset = m_vecData[nIndex].lTotalOffset;
		vi.fAzim = m_vecData[nIndex].fTotalAzim;
		TARGET tar = m_vecTarget[nTargetIndex];
		double lEPos = tar.lOffset * sin(tar.fAzim * PI / 180.0);
		double lNPos = tar.lOffset * cos(tar.fAzim * PI / 180.0);
		vi.lOffsetFromTarget = sqrt((lEPos - m_vecData[nIndex].lEPos) *  (lEPos - m_vecData[nIndex].lEPos) + (lNPos - m_vecData[nIndex].lNPos) * (lNPos - m_vecData[nIndex].lNPos));
		vi.fAzimFromTarget = asin((m_vecData[nIndex].lEPos - lEPos) / vi.lOffsetFromTarget) / PI * 180.0;
		
		if (m_vecData[nIndex].lNPos - lNPos < 0)
			vi.fAzimFromTarget = 180.0 - vi.fAzimFromTarget;
		if (m_vecData[nIndex].lEPos - lEPos < 0 && m_vecData[nIndex].lNPos - lNPos > 0)
			vi.fAzimFromTarget = 360.0 + vi.fAzimFromTarget;*/
}
//**************************************************************************************
ULMIMP CULFile::Compute(double fDegree)
{
	int nID = 0;
	double fStep = PI / 180.0f;

	CCurve* pDEVCurve = (CCurve*) GetCurveNoCase("DEV");
	if (pDEVCurve == NULL)
	{
		pDEVCurve = (CCurve *) GetCurveNoCase(_T("DEVI"));
		if (pDEVCurve == NULL)
		{
			CDlgCurves dlg(IDS_NOTFOUND_DEV, IDS_DEV_SET);
			dlg.SetCurves(&m_vecCurve);
			if (dlg.DoModal() != IDOK)
				return UL_ERROR;

			pDEVCurve = (CCurve *) m_vecCurve.at(dlg.m_nCurve);
		}
	}

	m_pDEV = pDEVCurve;
	CCurve* pAZIMCurve = (CCurve*) GetCurveNoCase("AZIM");
	if (pAZIMCurve == NULL)
	{
		CDlgCurves dlg(IDS_NOTFOUND_AZIM, IDS_AZIM_SET);
		dlg.SetCurves(&m_vecCurve);
		if (dlg.DoModal() != IDOK)
			return UL_ERROR;

		pAZIMCurve = (CCurve *) m_vecCurve.at(dlg.m_nCurve);
	}

	DATAVALUE dv, pdv;
	int nIndex = pDEVCurve->GetDataSize();
	if (nIndex < 0)
		return UL_ERROR;


	ZeroMemory(&dv, sizeof(DATAVALUE));
	long lDepth0 = pDEVCurve->GetDepth(0);
	long lDepthE = pDEVCurve->GetDepthED();	

	// 保证无论上提下放，m_vecData数据 深度都是从小到大
	BOOL bLoggingUp; 
	if (lDepth0 > lDepthE)
	{
		bLoggingUp = TRUE;
		dv.fDepth = pDEVCurve->GetDepth(nIndex - 1);
	}
	else
	{
		bLoggingUp = FALSE;	
		dv.fVertDepth = dv.fDepth;
	}
	// 	dv.fDev = pDEVCurve->GetdblValue(nIndex-1);
	// 	dv.fAzim = pAZIMCurve->GetdblValue(nIndex-1);
	BOOL bFirst = TRUE;
	for (int j = nIndex - 1; j > -1; j--)
	{
		int n = 0;
		if (bLoggingUp)
			n = j;
		else
			n = nIndex - j - 1;
		memcpy(&pdv, &dv, sizeof(DATAVALUE));
		double lPreDepth = pdv.fDepth;
		double lCurDepth = pDEVCurve->GetDepth(n);

		// 默认计算深度范围是0 到 30000
		if (lPreDepth < 0 || bFirst)
		{
			dv.fDepth = lCurDepth;
			dv.fVertDepth = lCurDepth;
			dv.fDev = pDEVCurve->GetdblValue(n);
			dv.fAzim = pAZIMCurve->GetdblValue(n);
			bFirst = FALSE;
			m_arData.Add(dv);
			continue;
		}
		if (lPreDepth > MTD(30000))
			break;

		dv.fDev = pDEVCurve->GetdblValue(n);
		// dv.fAzim = pAZIMCurve->GetdblValue(n);
		dv.fDepth = pDEVCurve->GetDepth(n);
		dv.fAzim = pAZIMCurve->GetValueAtDepth(dv.fDepth);

		dv.fVertDepth = pdv.fVertDepth +
			(dv.fDepth - pdv.fDepth) * cos(fStep * pdv.fDev);
		dv.fTrueAzim = dv.fAzim + fDegree;
		dv.fEPos = pdv.fEPos +
			DTM(dv.fDepth - pdv.fDepth) * sin(fStep * pdv.fDev) * sin(fStep * pdv.fTrueAzim);
		dv.fNPos = pdv.fNPos +
			DTM(dv.fDepth - pdv.fDepth) * sin(fStep * pdv.fDev) * cos(fStep * pdv.fTrueAzim);
		dv.fTotalOffset = sqrt(dv.fEPos * dv.fEPos + dv.fNPos * dv.fNPos);
		dv.fTotalAzim = asin(dv.fEPos / dv.fTotalOffset) / fStep;

		if (dv.fNPos < 0)
			dv.fTotalAzim = 180.0 - dv.fTotalAzim;

		if (dv.fEPos <0 && dv.fNPos> 0)
			dv.fTotalAzim = 360.0 + dv.fTotalAzim;

		int nIndex0 = m_pDEV->GetFirstIndexByDepth(dv.fDepth - MTD(25));   // 前25米一帧数据的曲线索引
		if (nIndex0 < nIndex)  // 上提
			nIndex0 = m_pDEV->GetDataSize() - nIndex0 - 1; // 将曲线索引转换成m_vecData的索引
		if (nIndex0 >= 0 && nIndex0 < m_arData.GetSize())
		{
			double fDiffAzim = dv.fAzim - pdv.fAzim;
			pdv = m_arData.GetAt(nIndex0);
			if (fabs(fDiffAzim) > 180)
				fDiffAzim = 180 - pdv.fAzim;
			//dv.fAlpha = ((dv.lDepth - pdv.lDepth) / (dv.lVertDepth - pdv.lVertDepth)) * sqrt(pow(dv.fDev - pdv.fDev, 2) + pow(fDiffAzim, 2) * pow(sin((dv.fDev - pdv.fDev) * 3.1415926 / 2 * 180),2));
			dv.fAlpha = acos(cosa(dv.fDev) * cosa(pdv.fDev) +
							sina(dv.fDev) * sina(pdv.fDev) * cosa(fDiffAzim)) * 180.f /
				PI;
		}
		else
			dv.fAlpha = -1;

		if (dv.fTrueAzim < 0)
			dv.fTrueAzim += (((int)abs(dv.fTrueAzim))/360 + 1)*360;
		if (dv.fTrueAzim >= 360)
			dv.fTrueAzim -= (((int)abs(dv.fTrueAzim))/360)*360;

		m_arData.Add(dv);
	}

	if (m_arData.GetSize() > 0)
		return UL_NO_ERROR;
	else
		return UL_ERROR;
}

ULMIMP CULFile::RecalcTVDData(double fDegree, double fInterval, double fStart,
	double fEnd)
{
	double fStep = PI / 180.0;
	DATAVALUE Value;
	DATAVALUE PreValue;
	int nSize = m_arData.GetSize();
	int nIndex;
	BOOL bFirst = TRUE;
	for (int i = 1; i < nSize; i++)
	{
		Value = m_arData[i];
		PreValue = m_arData[i - 1];

		// 计算范围是从fStart到fEnd
		if (Value.fDepth < MTD(fStart))
		{
			m_arData[i].fVertDepth = Value.fDepth;
			m_arData[i].fTrueAzim = Value.fAzim;
			m_arData[i].fEPos = 0.0;
			m_arData[i].fNPos = 0.0;
			m_arData[i].fTotalAzim = 0.0;
			m_arData[i].fTotalOffset = 0.0;
			continue;
		}

		if (Value.fDepth > MTD(fEnd))
			break;

		if (bFirst)
		{
			Value.fVertDepth = Value.fDepth;
			Value.fTrueAzim = Value.fAzim;
			Value.fEPos = 0.0;
			Value.fNPos = 0.0;
			Value.fTotalAzim = 0.0;
			Value.fTotalOffset = 0.0;
			bFirst = FALSE;
		}
		else
		{
			Value.fVertDepth = PreValue.fVertDepth +
				(Value.fDepth - PreValue.fDepth) * cos(fStep * PreValue.fDev);
			Value.fTrueAzim = Value.fAzim + fDegree;
			Value.fEPos = PreValue.fEPos +
				DTM(Value.fDepth - PreValue.fDepth) * sin(fStep * (Value.fDev +
				PreValue.fDev) /2) * sin(fStep * (Value.fTrueAzim +
				PreValue.fTrueAzim) /2);
			Value.fNPos = PreValue.fNPos +
				DTM(Value.fDepth - PreValue.fDepth) * sin(fStep * (Value.fDev +
				PreValue.fDev) /2) * cos(fStep * (Value.fTrueAzim +
				PreValue.fTrueAzim) /2);
			Value.fTotalOffset = sqrt(Value.fEPos * Value.fEPos +
									Value.fNPos * Value.fNPos);
			Value.fTotalAzim = asin(Value.fEPos / Value.fTotalOffset) / fStep;
		}	

		if (Value.fNPos < 0)
			Value.fTotalAzim = 180.0 - Value.fTotalAzim;
		if (Value.fEPos <0 && Value.fNPos> 0)
			Value.fTotalAzim = 360.0 + Value.fTotalAzim;

		nIndex = m_pDEV->GetFirstIndexByDepth(Value.fDepth - MTD(fInterval));   // 前25米一帧数据的曲线索引
		int nIndex0 = m_pDEV->GetFirstIndexByDepth(Value.fDepth);  // 当前帧曲线索引
		if (nIndex0 < nIndex)  // 上提
			nIndex = m_pDEV->GetDataSize() - nIndex - 1; // 将曲线索引转换成m_vecData的索引
		if (nIndex >= 0 &&
			nIndex <m_arData.GetSize() &&
			(Value.fDepth - MTD(fInterval)> MTD(fStart)))
		{
			double fDiffAzim = Value.fAzim - PreValue.fAzim;
			PreValue = m_arData.GetAt(nIndex);
			/*
			if (fabs(fDiffAzim) > 180)
				fDiffAzim = 180 - PreValue.fAzim;
			Value.fAlpha = acos(cosa(Value.fDev) * cosa(PreValue.fDev) +
							sina(Value.fDev) * sina(PreValue.fDev) * cosa(fDiffAzim)) * 180.f /
				PI;*/

			// 根据中华人民共和国石油天然气行业标准SY/T 5088-2008的公式修改
			if (fabs(fDiffAzim) > 180)
			{
				fDiffAzim = 360 - fabs(fDiffAzim);
			}
			if (fabs(Value.fDepth - PreValue.fDepth) < 0.001)
				Value.fAlpha = PreValue.fAlpha;
			else
				Value.fAlpha = fInterval * 1.0 / 25 * sqrt(pow(Value.fDev - PreValue.fDev, 2) + 
					pow(fDiffAzim, 2) * pow(sin((Value.fDev + PreValue.fDev) / 2 * fStep), 2));
		}
		else
			Value.fAlpha = -1.0;


		if (Value.fTrueAzim < 0)
			Value.fTrueAzim += (((int)abs(Value.fTrueAzim))/360 + 1)*360;
		if (Value.fTrueAzim >= 360)
			Value.fTrueAzim -= (((int)abs(Value.fTrueAzim))/360)*360;
		m_arData[i] = Value;
	}

	return UL_NO_ERROR;
}

//**************************************************************************************
void CULFile::ShowView(DWORD dwViewType)
{
	CChildFrame* pChild = GetFirstFrame(dwViewType);
	if (pChild != NULL)
	{
		CMainFrame* pMainFrame = (CMainFrame*) theApp.m_pMainWnd;
		pMainFrame->MDITabActivate(pChild);
	}
}
//**************************************************************************************
ULMIMP CULFile::SetStatusPrompt(LPCTSTR pszText)
{
	theApp.SetStatusPrompt(pszText);
	return UL_NO_ERROR;
}

ULMIMP CULFile::SetStatusProcess(int nPrecent)
{
	g_pMainWnd->SendMessage(UM_DATA_SAVE, 1, nPrecent);
	
	return UL_NO_ERROR;
}

ULMIMP CULFile::GetTools(CPtrArray* arrTools)
{
	arrTools->RemoveAll();
	for (int i = 0; i < m_arrTools.GetSize(); i++)
	{
		CULTool* pULTool = (CULTool*) m_arrTools.GetAt(i);
		arrTools->Add(pULTool);
	}
	return UL_NO_ERROR;
}


ULMIMP CULFile::ApplyParam(CString strTools)
{
	// 	CString strPath = Gbl_AppPath + "\\PARAMETER\\";//参数表文件的存放位置
	// 	CString strTools = _T("DEPTH-CNL-CDL"); //仪器串，组成文件名
	// 	CString strFile = strPath + strTools + _T(".xml"); 
	// 	
	// 	CXMLSettings xml(TRUE);
	// 	
	// 	xml.ReadXMLFromFile(strFile);
	// 	CXMLNode *pTree = xml.GetTree();
	// 	
	// 	POSITION posTool = pTree->m_lstChildren.GetHeadPosition();
	// 	
	// 	for(; posTool != NULL;)
	// 	{
	// 		
	// 		CXMLNode *pNodeTool = pTree->m_lstChildren.GetNext(posTool);
	// 		
	// 		CString strTool;
	// 		strTool = pNodeTool->m_strName;   //仪器名
	// 		//   	if (!strTool.CompareNoCase(_T("General")))  //深度仪器
	// 		// 		{
	// 		// 			strTool = _T("Depth");
	// 		// 		}	
	// 		
	// 		CToolParams* pTPs = new CToolParams;
	// 		pTPs->m_strTool = strTool;  
	// 		POSITION posCurve = pNodeTool->m_lstChildren.GetHeadPosition();
	// 		for (; posCurve != NULL;)
	// 		{
	// 			CXMLNode* pNodeCurve = pNodeTool->m_lstChildren.GetNext(posCurve);
	// 			CString strCurve = pNodeCurve->m_strName;   //曲线名
	// 			CToolParam* pTP = new CToolParam;
	// 			pTP->m_strName = strCurve;
	// 			//CString strValue, strUnit, strDescription;
	// 			POSITION posProperties = pNodeCurve->m_lstChildren.GetHeadPosition();
	// 			for (; posProperties != NULL;)
	// 			{
	// 				CXMLNode* pNodeProp = pNodeCurve->m_lstChildren.GetNext(posProperties);
	// 				//Value节点
	// 				if (!pNodeProp->m_strName.CompareNoCase(_T("Value")))
	// 				{
	// 					// 					char buf[256];
	// 					// 					memcpy(buf, pNodeProp->m_strValue, sizeof(pNodeProp->m_strValue));
	// 					//					pTP->m_pValue = (void*)buf;
	// 					pTP->m_nValType = VT_BSTR;
	// 					pTP->SetString(pNodeProp->m_strValue);
	// 			
	// 				}
	// 				//Unit节点
	// 				else if (!pNodeProp->m_strName.CompareNoCase(_T("Unit")))
	// 					pTP->m_strUnit = pNodeProp->m_strValue;
	// 				//Description 节点
	// 				else if (!pNodeProp->m_strName.CompareNoCase(_T("Description")))
	// 					pTP->m_strDesc = pNodeProp->m_strValue;
	// 			}
	// 			pTPs->m_tpList.AddTail(pTP);
	// 		}
	// 		m_tpTree.AddTail(pTPs);
	// 		
	// 	} 
	// 	
	// 	
//#ifndef _LOGIC
	m_pEILog->ApplyParameter(&m_tpTree, strTools);
//#endif
	return UL_NO_ERROR;
}


ULMIMP CULFile::AutoMapSheet(CString strTools)
{
	CStringArray arySheet;
//#ifndef _LOGIC
	m_pEILog->GetMappedSheet(strTools, arySheet);
//#endif
	CStringArray aryFile;
	aryFile.RemoveAll();
	for (int i = 0; i < arySheet.GetSize(); i++)
	{
		CString strSheet = arySheet.GetAt(i);
		CString strFile = SHT_FILE(strSheet);
		aryFile.Add(strFile);
	}
	SetSheets(&aryFile);
	return UL_NO_ERROR;
}

void CULFile::RemapSheets()
{
	POSITION pos = m_sheetiList.GetHeadPosition();
	for (; pos != NULL;)
	{
		CSheetInfo* pSheetInfo = m_sheetiList.GetNext(pos);
		if (pSheetInfo && pSheetInfo->pSheet)
		{
			pSheetInfo->pSheet->RemapTrackCurve(m_vecCurve);
		}
	}
}

ULMIMP CULFile::GetInterface(const IID& ID, void** p)
{
	*p = NULL;
	if (IsEqualIID(ID, IID_IULFile))
	{
		*p = static_cast <CULFile*>(this);
	}
	else if (IsEqualIID(ID, IID_IUnknown))
	{
		*p = static_cast <CULFile*>(this);
	}
//#ifndef _LOGIC
	else if (IsEqualIID(ID, IID_IEILog))
	{
		if (m_pEILog == NULL)
        {
			m_pEILog = new CEILog;
            m_pEILog->m_lRef = 0; 
            m_pEILog->AddRef(); 
        }
		*p = static_cast <CEILog*>(m_pEILog);
        return *p == NULL ? UL_ERROR : UL_NO_ERROR; 
	}
//#endif	
	if (NULL != *p)
	{
		AddRef();
		return UL_NO_ERROR;
	}
	return UL_ERROR;
}

ULMPTRIMP CULFile::CreateCurve(LPCTSTR pszName, VARTYPE vt /* = VT_R4 */)
{
	CCurve* pCurve = new CCurve(NB_FILE, vt, NULL, pszName);
	pCurve->m_pData->m_nDirection = m_nDirection;
	pCurve->m_pData->MInit();
	m_vecCurve.push_back(pCurve);
	return pCurve;
}

ULMPTRIMP CULFile::CreateCurve(LPCTSTR pszName, VARTYPE vt /* = VT_R4 */ , int nDimension /* = 1 */)
{
	CCurve* pCurve = new CCurve(NB_FILE, vt, NULL, pszName);
	pCurve->m_pData->m_nDirection = m_nDirection;
	pCurve->SetPointFrame(nDimension);
	pCurve->SetArrayCount(1);
	pCurve->m_pData->MInit();
	m_vecCurve.push_back(pCurve);
	return pCurve;
}

void CULFile::ApplyCurve(CCurve* pCurve)
{
	for (int i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve1 = (CCurve*) m_vecCurve.at(i);
		if (pCurve1->m_strName == pCurve->m_strName)
		{
			pCurve->AttachData(pCurve1->m_pData);
			break;
		}
	}
}

ULMLNGIMP CULFile::GetUniqueNameSuffix(CStringArray* pArrNames)
{
	UINT nIndex = 1;

	CString strName;
	CString strNewName;
	int nCurve = pArrNames->GetSize();
	while (nIndex < 100)
	{
		for (int i = 0; i < nCurve; i++)
		{
			strName = pArrNames->GetAt(i);
			strNewName.Format("%s%d", strName, nIndex);
			if (IsCurveNameExist(strNewName))
			{
				nIndex++;
				break;
			}
		}

		if (i == nCurve)
			return nIndex;
	}

	return nIndex;
}

// 更新水泥体积及井眼体积
void CULFile::UpdateInfors()
{
	if (NULL == m_pProject)
		return;
	
	BOOL bUpdate = FALSE;
	CString strValue;
	CString strNames[] = { _T("IHV"), _T("ICV"), _T("ITT") };
	for (int i = 0; i < 3; i++)
	{
		CCurve* pCurve = (CCurve*)GetCurveNoCase(strNames[i]);
		if (NULL == pCurve)
			continue;

		CCurveData* pData = pCurve->m_pData;

		CString strUnit = pData->m_strUnit;

		for (int j = 0; j < pData->m_vecCurve.size(); j++)
		{
			CCurve* pVC = (CCurve*)pData->m_vecCurve.at(j);
			if (pVC->m_PlotInfo.PlotENName.CompareNoCase(_T("Volume")) == 0)
			{
				strValue.Format(_T("%.2f %s"), pVC->m_fMajorPip, strUnit);
				m_pProject->SetInformation(strNames[i] + _T(":Major"), strValue);

				strValue.Format(_T("%.2f %s"), pVC->m_fMinorPip, strUnit);
				m_pProject->SetInformation(strNames[i] + _T(":Minor"), strValue);

				if (pVC->m_nPipShow)
				{
					strValue.Format(_T("%d"), pVC->m_nPipShow - 1);
					m_pProject->SetInformation(strNames[i] + _T(":Show"), strValue);
				}
				bUpdate = TRUE;
				break;
			}
		}
		
		int iEnd = pData->GetSize();
		if (0 == iEnd)
			continue;

		double fVal0 = pData->GetdblValue(0);
		double fValE = pData->GetdblValue(iEnd-1);
		strValue.Format(_T("%.2f %s"), max(fVal0, fValE), strUnit);
		m_pProject->SetInformation(strNames[i], strValue);
		bUpdate = TRUE;
	}

	if (bUpdate)
		UpdateViews(ULV_TABLEINFOVIEW);
}

#ifndef _LOGIC
CRftProfileInfo::CRftProfileInfo()
{
	fStartDepth = 0;
	fEndDepth = 0;
	nProfileType = RFT_PROFILE_TYPE_MVD;
	nMarkType = RFT_MARK_TYPE_DTRI;
	crMarkColor = RGB(255, 0, 0);

	pWnd = NULL;
	pParent = NULL;
	nHorzGrid = 8;
	nVertGrid = 8;
	fHorzMin = 0;
	fHorzMax = 0;
	fVertMin = 0;
	fVertMax = 0;

	bShowSelectLine = FALSE;
	fSelectLineValue = 0;
	bSelectPos = 0;
	nSelectIndex = 0;
	bOmit = FALSE;
	nOmitIndex = -1;
}

CRftProfileInfo::~CRftProfileInfo()
{
	for (int i = 0; i < ProfileLine.GetSize(); i++)
	{
		CRftProfileLine* pLine = (CRftProfileLine*) ProfileLine.GetAt(i);
		delete pLine;
	}
	ProfileLine.RemoveAll();
}

CRftProfileLine::CRftProfileLine()
{
}

CRftProfileLine::~CRftProfileLine()
{
}

BOOL CRftProfileLine::Compute()
{
	int nCount = Depth .GetSize();
	double fSumX = 0;
	double fSumY = 0;
	double fSumXY = 0;
	double fSumXX = 0;
	double fX = 0;
	double fY = 0;
	for (int i = 0; i < nCount; i++)
	{
		fX = Depth[i];
		fY = Value[i];
		fSumX = fSumX + fX;
		fSumY = fSumY + fY;
		fSumXY = fSumXY + fX * fY;
		fSumXX = fSumXX + fX * fX;
	}
	k = (fSumXY - fSumX * fSumY / nCount) / (fSumXX - fSumX * fSumX / nCount);
	b = fSumY / nCount - k * fSumX / nCount;

	fStartValue = k * fStartDepth + b;
	fEndValue = k * fEndDepth + b;

	return TRUE;
}

BOOL CRftProfileLine::Clear()
{
	Depth.RemoveAll();
	Value.RemoveAll();

	return TRUE;
}

#endif

ULMIMP CULFile::SaveFileter(int i, LPCTSTR lpszFilter)
{
	CCurve* pCurve = NULL;
	if (g_pActPrj != NULL)
	{
		pCurve = (CCurve*)m_arrToolCurves.GetAt(i);
	}
	else
	{
		pCurve = (CCurve*)m_vecCurve.at(i);
	}
	if (pCurve != NULL)
	{
		CFile fileFilter;
		if (fileFilter.Open(lpszFilter, CFile::modeCreate | CFile::modeWrite))
		{
			fileFilter.Write(&i, sizeof(int));
			int nLen = 0;
			CULFilter* pFilter = pCurve->GetFilter();
			if (pFilter != NULL)
			{
// 				CString strFilter = pFilter->GetFilterName();
// 				nLen = strFilter.GetLength();
				nLen += sizeof(int); // 本身长度
				nLen += sizeof(int); // nTemp的长度，滤波模版长度
				nLen += sizeof(char)*1024; // 扩展空间长度
				fileFilter.Write(&nLen, sizeof(int));
				int nTemp = pCurve->GetFilterTemplate();
				fileFilter.Write(&nTemp, sizeof(int));
				char cHar[1024];
				fileFilter.Write(cHar, sizeof(char)*1024);
				pFilter->SaveFilter(&fileFilter);
			}
			else
			{
				fileFilter.Write(&nLen, sizeof(int));
			}

			fileFilter.Close();
			return UL_NO_ERROR;
		}
	}

	return UL_ERROR;
}

ULMIMP CULFile::ReadFileter(int i, LPCTSTR lpszFilter)
{
	CCurve* pCurve = (CCurve*)m_vecCurve.at(i);
	if (pCurve != NULL)
	{
		CFile fileFilter;
		if (fileFilter.Open(lpszFilter, CFile::modeRead))
		{
			fileFilter.Read(&i, sizeof(int));
			int nLen = 0;
			fileFilter.Read(&nLen, sizeof(int));
			if (nLen != 0)
			{
				int nTemp = 0;
				fileFilter.Read(&nTemp, sizeof(int));
				char cHar[1024];
				fileFilter.Read(cHar, sizeof(char)*1024);
				pCurve->SetFilterTemplate(nTemp);
				CULFilter* pFilter = new CULFilter;
				pFilter->ReadFilter(&fileFilter);
				pCurve->SetFilter(pFilter);
			}
			else
			{
			}
			
			fileFilter.Close();
			return UL_NO_ERROR;
		}
	}
	
	return UL_ERROR;
}

void CULFile::SaveDataFrameTime()
{
	if (m_pIFile == NULL)
	{
		TRACE("return in CULFile::SaveDataFrame() m_pIFile == NULL");
		return;
	}
	
	// 存储曲线数据
	if (m_pDCurve == NULL)
	{
		TRACE("return in CULFile::SaveDataFrame() m_pDCurve == NULL");
		return;
	}
	int nDepPos = m_pDCurve->GetDataSize() - 1;	// 当前的存盘位置

	
	long lDepth = m_pDCurve->GetDepth(nDepPos);	// 0.1mm		
	if (lDepth == INVALID_DEPTH)
		return;
	
	long lTime = m_pDCurve->GetTime(nDepPos);	// 0.1ms
	vec_cv values;

	POSITION pos = m_lstSave.GetHeadPosition();
	for (; pos != NULL;)
	{
		SDATA* pSData = (SDATA*) m_lstSave.GetNext(pos);
		
		CurveValue cv;
		cv.vt = pSData->pData->GetValueType();
		cv.sz = pSData->sz;
		cv.pValue = pSData->pValue;
		ZeroMemory(pSData->pValue, pSData->sz);
		values.push_back(cv);
	}
	
	if (m_pIFile)
	{
		m_pIFile->SaveFrame(lDepth, lTime, &values, (void *) TRUE);
	}
	
	values.clear();
}

BOOL CULFile::SaveAsALD()
{
	
	int nStartIndex = 0;
	int nEndIndex = 0;  //起始终止索引
	long lStart = 0;
	long lEnd = 0;

	for (int i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
		if (pCurve->IsDepthCurve())
		{
			if (i > 0)
			{
				m_vecCurve.erase(m_vecCurve.begin() + i);
				m_vecCurve.insert(m_vecCurve.begin(), pCurve);
			}
		}
		nStartIndex = pCurve->GetDataSize();;
		if (nEndIndex < nStartIndex)
		{
			nEndIndex = nStartIndex;
		}
	}
	nStartIndex = 0;
	
	long lStep = 10;
	g_pMainWnd->SendMessage(UM_DATA_SAVE);
	CTime tmModify = CTime::GetCurrentTime();
	SetTime(tmModify.GetTime(), FT_MODIFY);
	SaveFileHead(m_pProject, NULL);

	int nOff = 1;

	if (m_pDCurve == NULL)
		m_pDCurve = (CCurve *) m_vecCurve.at(0);
	
	if (!m_pDCurve)
		return FALSE;
	
	CCurveData ddata(m_pDCurve->m_pData);
	ddata.Copy(m_pDCurve->m_pData);

	{
		for (POSITION pos = m_frmList.GetHeadPosition(); pos != NULL;)
		{
			CChildFrame* pFrame = m_frmList.GetNext(pos);
			if (pFrame->m_nViewType == ULV_GRAPHS)
			{
				m_pGraph = pFrame->m_pGraphWnd;
				break;
			}
		}
	}

	lStart = m_pDCurve->GetTime(nStartIndex);
	lEnd = m_pDCurve->GetTime(m_pDCurve->GetDataSize() - 1);

	if (SaveDataBegin(NULL, TRUE, lStart, lEnd))
	{
		Close();

		for (POSITION pos = m_frmList.GetHeadPosition(); pos != NULL;)
		{
			CChildFrame* pFrame = m_frmList.GetNext(pos);
			pFrame->m_dwLocked &= ~LT_SAVING;
		}
		g_pMainWnd->SendMessage(UM_DATA_SAVE, 2, (LPARAM)this);
		return FALSE;
	}

	g_pMainWnd->SendMessage(UM_DATA_SAVE, 1, 0);//开始保存前进度条置零
	if (m_vecCurve.size() > 0)
	{
		vec_cv values;
		if (m_pIFile)
			m_pIFile->SaveFrame(-1, 0, &values); // -1是ALD程序内判断用
	/*	m_lFrameCount = m_pDCurve->GetDataSize();
		float fDelta = (nEndIndex-nStartIndex) / (lStep);
		float j = fDelta;
		lStep = 0;
		// 需要保存的纪录方向和此文件的纪录方向相同，则索引从小到大
		// int nOff = (m_bRecordDown == m_nDirection) ? 1 : -1;
		//xwh 2013-05-22 修改时间另存 进度条 和 顶部向上的时间格式
		if (nStartIndex<=nEndIndex)
		{
			vec_cv values;
			for (int i = nStartIndex; i < nEndIndex; i++)
			{
				if (m_pIFile)
						m_pIFile->SaveFrame(-1, i, &values); // -1是ALD程序内判断用
				if (i > j)
				{
					j += fDelta;
					lStep =100*(i-nStartIndex)/(nEndIndex-nStartIndex);
					g_pMainWnd->SendMessage(UM_DATA_SAVE, 1, lStep);
				}
				else if (i == (nEndIndex - 1))
				{
				//	if (m_pIFile)
				//		m_pIFile->SaveFrame(-1, i, &values); // -1是ALD程序内判断用
					
					j += fDelta;
					lStep = 100;
					g_pMainWnd->SendMessage(UM_DATA_SAVE, 1, lStep);
				}
			}
		}
		else if (nStartIndex>nEndIndex)
		{
			vec_cv values;
			for (int i = nEndIndex; i < nStartIndex; i++)
			{
				if (m_pIFile)
						m_pIFile->SaveFrame(-1, i, &values); // -1是ALD程序内判断用
				if (i > j)
				{	
					j += fDelta;
					lStep = 100*(i-nEndIndex)/(nStartIndex -nEndIndex);
					g_pMainWnd->SendMessage(UM_DATA_SAVE, 1, lStep);
				}
				else if (i == (nStartIndex - 1))
				{
				//	if (m_pIFile)
				//		m_pIFile->SaveFrame(-1, i, &values); // -1是ALD程序内判断用
					
					j += fDelta;
					lStep = 100;
					g_pMainWnd->SendMessage(UM_DATA_SAVE, 1, lStep);
				}
			}
		}*/
	}

	SaveDataEnd();
	Close();
	for (POSITION pos = m_frmList.GetHeadPosition(); pos != NULL;)
	{
		CChildFrame* pFrame = m_frmList.GetNext(pos);
		pFrame->m_dwLocked &= ~LT_SAVING;
	}
	g_pMainWnd->SendMessage(UM_DATA_SAVE, 2);
	
	m_pDCurve->m_pData->Clear();
	m_pDCurve->m_pData->Append(&ddata);


	SetModifiedFlag(FALSE);
	return TRUE;
}

BOOL CULFile::SaveAsByTime()
{
	int nStartIndex = 0;
	int nEndIndex = 0;  //起始终止深度索引
	long lStartDepth = 0;
	long lEndDepth = 0;   

	long lStep = 10;
	g_pMainWnd->SendMessage(UM_DATA_SAVE);
	CTime tmModify = CTime::GetCurrentTime();
	SetTime(tmModify.GetTime(), FT_MODIFY);
	SaveFileHead(m_pProject, NULL);

	for (int i = 0; i < m_vecCurve.size(); i++)
	{
		CCurve* pCurve = (CCurve*) m_vecCurve.at(i);
		if (pCurve->IsTimeCurve())
		{
			m_pTCurve = pCurve;
			if (i > 0)
			{
				m_vecCurve.erase(m_vecCurve.begin() + i);
				m_vecCurve.insert(m_vecCurve.begin(), pCurve);
			}
			
			break;
		}
	}
	int nOff = 1;

	if (m_pTCurve == NULL)
	{
		if (m_pDCurve == NULL)
		{
			m_pDCurve = (CCurve *) m_vecCurve.at(0);
		}
		m_pTCurve = m_pDCurve;
	}

	if (!m_pDCurve)
		return FALSE;

	CCurveData ddata(m_pTCurve->m_pData);
	ddata.Copy(m_pTCurve->m_pData);

	lStartDepth = m_pTCurve->GetTime(0);
	lEndDepth = m_pTCurve->GetTimeED();

	if (SaveDataBegin(NULL, TRUE, lStartDepth, lEndDepth))
	{
		Close();

		for (POSITION pos = m_frmList.GetHeadPosition(); pos != NULL;)
		{
			CChildFrame* pFrame = m_frmList.GetNext(pos);
			pFrame->m_dwLocked &= ~LT_SAVING;
		}
		g_pMainWnd->SendMessage(UM_DATA_SAVE, 2, (LPARAM)this);
		return FALSE;
	}

	nStartIndex = 0;
	nEndIndex = m_pTCurve->GetDataSize();

	g_pMainWnd->SendMessage(UM_DATA_SAVE, 1, lStep);
	if (m_vecCurve.size() > 0)
	{
		long lDLevel = 0;
		if (m_pTCurve->GetDataSize() > 1)
		{
			long lFD = m_pTCurve->GetTime(0);
			long lSD = m_pTCurve->GetTime(1);
			lDLevel = abs(lFD - lSD);
		}
		
		float fDelta = m_lFrameCount / (lStep - 1);
		float j = fDelta;
		
		// 需要保存的纪录方向和此文件的纪录方向相同，则索引从小到大
		// int nOff = (m_bRecordDown == m_nDirection) ? 1 : -1;
		for (int i = nStartIndex; i < nEndIndex; i++)
		{
			SaveDataFrameByTime(i, nOff, lDLevel);
			if (i > j)
			{
				j += fDelta;
				lStep += 10;
				g_pMainWnd->SendMessage(UM_DATA_SAVE, 1, lStep);
			}
		}
	}
	
	SaveDataEnd();
	Close();
	for (POSITION pos = m_frmList.GetHeadPosition(); pos != NULL;)
	{
		CChildFrame* pFrame = m_frmList.GetNext(pos);
		pFrame->m_dwLocked &= ~LT_SAVING;
	}
	g_pMainWnd->SendMessage(UM_DATA_SAVE, 2);

	m_pTCurve->m_pData->Clear();
	m_pTCurve->m_pData->Append(&ddata);

	SetModifiedFlag(FALSE);
	return TRUE;
}

void CULFile::SaveDataFrameByTime(int nFrame, int nOff, long lDLevel)
{
	long lDepth, lTime;
	if (m_pTCurve != NULL)
	{
		if (m_pTCurve->GetDataMode(nFrame) == DATA_INVALID)
			return;
		
		lDepth = m_pTCurve->GetDepth(nFrame);	// 0.1mm		
		lTime = m_pTCurve->GetTime(nFrame);		// 0.1ms
	}
	else
	{
		lDepth = m_vecCurve.at(0)->GetDepth(nFrame);
		lTime = m_vecCurve.at(0)->GetTime(nFrame);
	}
	
	vec_cv values;
	POSITION pos = m_lstSave.GetHeadPosition();
	
	int i;
	
	for (; pos != NULL;)
	{
		SDATA* pSData = (SDATA*) m_lstSave.GetNext(pos);
		
		CurveValue cv;
		cv.vt = pSData->pData->GetValueType();
		cv.sz = pSData->sz;
		cv.pValue = pSData->pValue;		
	//	pSData->pData->GetCurSaveValues(lDepth, (LPBYTE)cv.pValue, cv.sz, lDLevel, nOff);

		int nIndex = pSData->pData->GetFirstIndexByTime(lTime, 1); //add by gj 131008 由于各曲线采样率不同导致保存LAS文件时出错


		void* pValue = pSData->pData->GetCurSaveValuePtr(nIndex); //统一按照时间索引取值
		if(pValue != NULL && AfxIsValidAddress(pValue, cv.sz))
		{
			memcpy(cv.pValue, pValue, cv.sz);
		}


		if (cv.pValue != NULL)
		{
			if ((lTime < pSData->lDepth0) || (lTime > pSData->lDepthE))
			{
				if (cv.vt == VT_R4)
				{
					for (i = 0; i < cv.sz / sizeof(float); i++)
					{
						((float *) cv.pValue)[i] = -999.25;
					}
				}
				else if (cv.vt == VT_I4)
				{
					for (i = 0; i < cv.sz / sizeof(long); i++)
					{
						((long *) cv.pValue)[i] = -9999;
					}
				}
				else if (cv.vt == VT_R8)
				{
					for (i = 0; i < cv.sz / sizeof(double); i++)
					{
						((double *) cv.pValue)[i] = -999.25;
					}
				}
			}
		} 
		
		values.push_back(cv);
	}
	
	if (m_pIFile)
		m_pIFile->SaveFrame(lDepth, lTime, &values);
	
	values.clear();
}

ULMTSTRIMP CULFile::GetAXPVersion()
{
	CString strVersion;
	TCHAR szPath[_MAX_PATH];
	GetModuleFileName(GetModuleHandle(AfxGetAppName()), szPath, _MAX_PATH);
	CString strPath = szPath;
	
	CFileVersion fv;
	if (fv.Open(strPath))
		strVersion = fv.GetFileVersion();
	strVersion.Replace("," , ".");
	m_strVersion.Format("AXP%s",strVersion);

	return (LPTSTR)(LPCTSTR)m_strVersion;
}
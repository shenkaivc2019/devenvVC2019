#include "StdAfx.h"
#include "ScoutFunctionDef.h"
#include "..\UL2000\resource.h"

HMODULE	g_hScout = NULL;
IScout*	g_pScout = NULL;
typedef	DWORD(FAR PASCAL* _ULGETVERSION)();
typedef	IUnknown*(FAR PASCAL* _NEWSCOUT)();

BOOL LoadScout()
{
	g_hScout = ::LoadLibrary(ULSCOUTCONNTROL);
    if (g_hScout == NULL)
	{
		::AfxMessageBox(IDS_FAIL_LOADSCOUT);
		return FALSE;
	}
	
	// 查询仪器动态库版本号
	_ULGETVERSION ULGetVersion = (_ULGETVERSION)GetProcAddress(g_hScout, "ULGetVersion");
	if (ULGetVersion == NULL)
	{
		::FreeLibrary(g_hScout);
		g_hScout = NULL;
		::AfxMessageBox(IDS_FAIL_SCOUTDAMAGE);
		return FALSE;
	}
	
	DWORD dwVersion = ULGetVersion();
	if (dwVersion < MAKEWORD(_UL2000_VERSION_MINOR, _UL2000_VERSION_MAJOR))
	{
		::FreeLibrary(g_hScout);
		g_hScout = NULL;
		AfxMessageBox(IDS_FAIL_SCOUTVERSION);
		return FALSE;
	}	
	
	// 查询仪器库接口
	_NEWSCOUT CreateScout = (_NEWSCOUT) GetProcAddress(g_hScout, "CreateScout");
	if (CreateScout == NULL)
	{
		::FreeLibrary(g_hScout);
		g_hScout = NULL;
		::AfxMessageBox(IDS_FAIL_SCOUTDAMAGE);
		return FALSE;
	}
	
	g_pScout = (IScout*)CreateScout();
	if (g_pScout == NULL)
	{
		::FreeLibrary(g_hScout);
		g_hScout = NULL;
		::AfxMessageBox(IDS_FAIL_CREATESCOUT);
		return FALSE;
	}
	
	HRESULT hr;
	hr = g_pScout->QueryInterface(IID_IScout, (void**) &g_pScout);
	if (FAILED(hr) || g_pScout == NULL)
	{
		::FreeLibrary(g_hScout);
		g_hScout = NULL;
		return FALSE;
	}
	
	return TRUE;
}

void UnloadScout()
{
	if(g_pScout)
	{
		g_pScout->Release();
		g_pScout = NULL;
	}

	if(g_hScout)
	{
		FreeLibrary(g_hScout);
		g_hScout = NULL;
	}
}

// Create the scoutcontrol object according the project and segment
int CreateScout(CString strFileName,	//监视文件路径及名称
				CString strProjName)	//工程名称
{
	if(g_pScout == NULL)
		LoadScout();

	if(g_pScout)
		return g_pScout->Create(strFileName, strProjName);
	return UL_ERROR;
}

// Destroy the file, Used in UL2000
void DestroyScout()
{
	if(g_pScout)
		g_pScout->Destroy();
}

int AEI(UINT nID, LPCTSTR pszSrc, DWORD dwType, LPCTSTR pszEvent,
		LPCTSTR pszResult, LPCTSTR pszRemark /* = NULL */,
		long lDepth /* = 0 */, LPCTSTR pszTool /* = NULL */,
		LPCTSTR pszPara /* = NULL */, double lfValue /* = 0  */)
{
	int nResult = UL_ERROR;
	if (g_pScout)
	{
		CString string;
		string.LoadString(nID);
		return g_pScout->AddEventItem(string, pszSrc, lDepth, pszTool, 
			pszPara, lfValue, (WORD)dwType, GetBValue(dwType),
			pszEvent, pszResult, pszRemark);
	}
	
	return UL_ERROR;
}

int AEI(UINT nID, LPCTSTR pszSrc, DWORD dwType, UINT nEvent,
		LPCTSTR pszResult, LPCTSTR pszRemark /* = NULL */,
		long lDepth /* = 0 */, LPCTSTR pszTool /* = NULL */,
		LPCTSTR pszPara /* = NULL */, double lfValue /* = 0  */)
{
	CString strEvent;
	strEvent.LoadString(nEvent);
	return AEI(nID, pszSrc, dwType, strEvent, pszResult, pszRemark, lDepth, pszTool, pszPara, lfValue);
}

int AEI(UINT nID, LPCTSTR pszSrc, DWORD dwType, UINT nEvent, UINT nResult,
		 LPCTSTR pszRemark /* = NULL */, long lDepth /* = 0 */,
		 LPCTSTR pszTool /* = NULL */, LPCTSTR pszPara /* = NULL */,
		 double lfValue /* = 0  */)
{
	CString strEvent, strResult;
	strEvent.LoadString(nEvent);
	strResult.LoadString(nResult);
	return AEI(nID, pszSrc, dwType, strEvent, strResult, pszRemark, lDepth, pszTool, pszPara, lfValue);
}

int AEI(UINT nID, LPCTSTR pszSrc, DWORD dwType, UINT nEvent, UINT nResult,
		 UINT nRemark, long lDepth /* = 0 */, LPCTSTR pszTool /* = NULL */,
		 LPCTSTR pszPara /* = NULL */, double lfValue /* = 0  */)
{
	CString strEvent, strResult, strRemark;
	strEvent.LoadString(nEvent);
	strResult.LoadString(nResult);
	strRemark.LoadString(nRemark);
	return AEI(nID, pszSrc, dwType, strEvent, strResult, strRemark, lDepth, pszTool, pszPara, lfValue);
}

// Whether the object validate, Validate:TRUE, Invalidate:FALSE; Only used to open and close Scout files
BOOL IsValidateScout()
{
	return FALSE;
}

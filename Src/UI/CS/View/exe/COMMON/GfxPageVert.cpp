// GfxPageVert.cpp : implementation file
// 纵向波列属性对话框

#include "stdafx.h"
#include "ul2000.h"
#include "GfxPageVert.h"
#include "Curve.h"
#include "GraphWnd.h"
#include "Unitx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGfxPageVert dialog

CGfxPageVert::CGfxPageVert(CWnd* pParent /*=NULL*/)
	: CGfxPage(CGfxPageVert::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGfxPageVert)
	m_bDisplay = FALSE;
	m_bPrint = FALSE;
	m_bSave = FALSE;
	m_lDepthDelay = 0;
	m_lDepthDistance = 0;
	m_lDepthOffset = 0;
	m_lCalcDelay = 0;
	m_dEndPosition = 0;
	m_dStartPosition = 0;
	m_dStartTime = 0;
	m_dEndTime = 0;
	m_fLowerValue = 0.0;
	m_strName = _T("");
	m_strAlias = _T("");
	m_strUnit = _T("");
	m_fUpperValue = 0.0;
	m_iTimeInter = 0;
	m_nPlotStyle = 0;
	m_nFillType = 0;
	//}}AFX_DATA_INIT
}

void CGfxPageVert::DoDataExchange(CDataExchange* pDX)
{
	CGfxPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGfxPageVert)
	DDX_Control(pDX, IDC_LIST_CURVE, m_lbCurveList);
//	DDX_Control(pDX, IDC_COMBO_FILTER, m_cbFilter);
	DDX_Control(pDX, IDC_LINEWIDTH, m_cbLineWidth);
	DDX_Control(pDX, IDC_BTN_COLOR, m_btnCurveColor);
	DDX_Check(pDX, IDC_CHECK_DISPLAY, m_bDisplay);
	DDX_Check(pDX, IDC_CHECK_PRINT, m_bPrint);
	DDX_Check(pDX, IDC_CHECK_SAVE, m_bSave);
	DDX_Text(pDX, IDC_EDIT_ENDPOS, m_dEndPosition);
	DDX_Text(pDX, IDC_EDIT_STARTPOS, m_dStartPosition);
	DDX_Text(pDX, IDC_EDIT_LOWERVALUE, m_fLowerValue);
	DDX_Text(pDX, IDC_EDIT_NAME, m_strName);
	DDX_Text(pDX, IDC_EDIT_NAME2, m_strAlias);
	DDX_Text(pDX, IDC_EDIT_UNIT, m_strUnit);
	DDX_Text(pDX, IDC_EDIT_UPPERVALUE, m_fUpperValue);
	DDX_Text(pDX, IDC_EDIT_TIMEINTER, m_iTimeInter);
	DDX_Text(pDX, IDC_EDIT_TIMEDELAY, m_lTimeDelay);

	//}}AFX_DATA_MAP
	g_units->DDX_Depth(pDX, IDC_DEPTHDELAY, m_lDepthDelay);
	g_units->DDX_Depth(pDX, IDC_CALCDELAY, m_lCalcDelay);
	DDX_Text(pDX, IDC_DEPTHDISTANCE, m_lDepthDistance);
}

BEGIN_MESSAGE_MAP(CGfxPageVert, CGfxPage)
	//{{AFX_MSG_MAP(CGfxPageVert)
	ON_EN_CHANGE(IDC_EDIT_ENDPOS, OnChangeEditEndpos)
	ON_EN_CHANGE(IDC_EDIT_STARTPOS, OnChangeEditStartpos)
	ON_EN_CHANGE(IDC_EDIT_STARTTIME, OnChangeEditStartTime)
	ON_EN_CHANGE(IDC_EDIT_ENDTIME, OnChangeEditEndTime)
	ON_LBN_SELCHANGE(IDC_LIST_CURVE, OnSelchangeListCurve)
	ON_BN_CLICKED(IDC_CHECK_DISPLAY, OnCheckDisplay)
	ON_BN_CLICKED(IDC_CHECK_PRINT, OnCheckPrint)
	ON_BN_CLICKED(IDC_CHECK_SAVE, OnCheckSave)
	ON_CBN_SELCHANGE(IDC_COMBO_FILTER, OnSelchangeComboFilter)
	ON_EN_CHANGE(IDC_EDIT_UPPERVALUE, OnChangeEditUppervalue)
	ON_EN_CHANGE(IDC_EDIT_NAME2, OnChangeEditName)
	ON_EN_CHANGE(IDC_EDIT_LOWERVALUE, OnChangeEditLowervalue)
	ON_EN_CHANGE(IDC_EDIT_UNIT, OnChangeEditUnit)
	ON_EN_KILLFOCUS(IDC_EDIT_UPPERVALUE, OnKillfocusEditUppervalue)
	ON_EN_KILLFOCUS(IDC_EDIT_LOWERVALUE, OnKillfocusEditLowervalue)
	ON_EN_KILLFOCUS(IDC_DEPTHOFFSET, OnKillfocusDepthOffset)
	ON_EN_KILLFOCUS(IDC_EDIT_STARTPOS, OnKillfocusEditStartpos)
	ON_EN_KILLFOCUS(IDC_EDIT_ENDPOS, OnKillfocusEditEndpos)
	ON_EN_KILLFOCUS(IDC_DEPTHDISTANCE, OnKillfocusDepthDistance)
	ON_EN_KILLFOCUS(IDC_DEPTHDELAY, OnKillfocusDepthDelay)
	ON_EN_KILLFOCUS(IDC_EDIT_TIMEINTER, OnKillfocusEditTimeinter)
	ON_BN_CLICKED(IDC_BTN_COLOR, OnBtnColor)
	ON_BN_CLICKED(IDC_BTN_COLOR2, OnBtnFillColor)
	ON_CBN_SELCHANGE(IDC_LINEWIDTH, OnSelChangeLineWidth)
	ON_WM_DESTROY()
	ON_EN_KILLFOCUS(IDC_EDIT_TIMEDELAY, OnKillfocusEditTimedelay)
	ON_BN_CLICKED(IDC_RADIO1, OnPlotStyle)
	ON_BN_CLICKED(IDC_RADIO2, OnPlotStyle)
	ON_BN_CLICKED(IDC_RADIO3, OnPlotStyle)
	ON_BN_CLICKED(IDC_RADIO4, OnFillType)
	ON_BN_CLICKED(IDC_RADIO5, OnFillType)
	ON_BN_CLICKED(IDC_RADIO6, OnFillType)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGfxPageVert message handlers

void CGfxPageVert::InitCurve()
{
	if (m_pCurve == NULL)
	{
		return ;
	}

	m_bDisplay = m_pCurve->m_bDisplay;
	m_bPrint = m_pCurve->m_bPrint;
	m_bSave = m_pCurve->IsSave();
	m_strName = m_pCurve->m_strName;
	m_strAlias = m_pCurve->m_strIDName;
	m_strUnit = m_pCurve->Unit();
	m_lDepthOffset = m_pCurve->DepthOffset();
	m_lDepthDistance = m_pCurve->m_nPointDivision;
	m_iTimeInter = m_pCurve->m_iTimeInter;
	m_lTimeDelay = m_pCurve->m_lTimeDelay;

	m_lDepthDelay = m_pCurve->m_lDepthDelay;
	m_lCalcDelay = m_pCurve->m_lCalcDelay;
	m_fLowerValue = m_pCurve->m_LeftValue;
	m_fUpperValue = m_pCurve->m_RightValue;
	m_nPlotStyle = m_pCurve->m_nPlotStyle;
	m_nFillType = m_pCurve->m_nWaveFillType;

	m_dStartPosition = m_pCurve->m_dStartPos;
	m_dEndPosition = m_pCurve->m_dEndPos;

	if (m_pCurve->StartPos2() != 0 || m_pCurve->EndPos2() != 360)
	{
		m_dStartTime = m_pCurve->StartPos2();
		m_dEndTime = m_pCurve->EndPos2();
	}
	else
	{
		m_dStartTime = 0;
		m_dEndTime = m_pCurve->GetFrameSize();
	}

	m_cbLineWidth.SetCurSel(LWLevel(m_pCurve->m_nWidth));
	m_btnCurveColor.SetColor(m_pCurve->m_crColor);
	m_btnFillColor.SetColor(m_pCurve->m_dwWaveFillColor);
	UpdateData(FALSE);
}

void CGfxPageVert::ResetCurve()
{
	if (m_lbCurveList.GetCount()>0)
	{
		m_lbCurveList.ResetContent();
	}
}

void CGfxPageVert::AddCurve(CCurve* pCurve)
{
	if (pCurve != NULL)
	{
		int nItem = m_lbCurveList.AddString(pCurve->m_strName + "." + pCurve->m_strSource);
		m_lbCurveList.SetItemData(nItem, (DWORD)pCurve);
	}
}

void CGfxPageVert::SelectCurve(int nIndex)
{
	if (m_lbCurveList.GetCount() > nIndex)
	{
		m_pCurve = NULL;
		m_pCurve = (CCurve*)m_lbCurveList.GetItemData(nIndex);
		InitCurve();
	}
}

void CGfxPageVert::RefreshData()
{
	UpdateData(TRUE);
	if (m_pCurve == NULL)
		return;

	m_pCurve->m_strName = m_strName;
	m_pCurve->m_strIDName = m_strAlias;
	m_pCurve->SetUnit(m_strUnit.GetBuffer(m_strUnit.GetLength()));
	m_strUnit.ReleaseBuffer();
	m_pCurve->m_LeftValue = m_fLowerValue;
	if (m_fLowerValue != floor(m_fLowerValue))
		m_pCurve->m_nLeftDotNum = 2;
	else
		m_pCurve->m_nLeftDotNum = 0;
	m_pCurve->m_RightValue = m_fUpperValue;
	if (m_fUpperValue != floor(m_fUpperValue))
		m_pCurve->m_nRightDotNum = 2;
	else
		m_pCurve->m_nRightDotNum = 0;
// 	if (m_pCurve->m_pData)
//		m_pCurve->m_pData->m_lDepthOffset = m_lDepthOffset;
	m_pCurve->m_nPointDivision = m_lDepthDistance;
	m_pCurve->m_lDepthDelay = m_lDepthDelay;
	m_pCurve->m_bDisplay = m_bDisplay;
	m_pCurve->m_bPrint = m_bPrint;
	m_pCurve->Save(m_bSave);
	m_pCurve->m_iTimeInter = m_iTimeInter;
	m_pCurve->m_lTimeDelay = m_lTimeDelay;
	
/*	int nIndex = m_cbFilter.GetCurSel();
	CString str;
	if (nIndex != CB_ERR)
	{
		m_cbFilter.GetLBText(nIndex, str);
	}
	else
	{
		m_cbFilter.GetWindowText(str);
	}

	int nItem = (int)atoi(str);*/

	int nLWLevel = m_cbLineWidth.GetCurSel();
	if (nLWLevel != CB_ERR)
		m_pCurve->m_nWidth = Gbl_ConfigInfo.SetLWLevel(nLWLevel);

	m_pCurve->m_nPlotStyle = m_nPlotStyle;
	m_pCurve->m_nWaveFillType = m_nFillType;
	m_pCurve->m_crColor = m_btnCurveColor.GetColor();
	m_pCurve->m_dwWaveFillColor = m_btnFillColor.GetColor();

	if (m_dEndPosition >= m_pCurve->GetPointFrame() && m_nPlotStyle == 0)
	{
		m_pCurve->m_dEndPos = m_pCurve->GetPointFrame() - 1;
	}
	else
	{
		m_pCurve->m_dEndPos = m_dEndPosition;
	}
	if (m_dStartPosition > m_dEndPosition)
	{
		m_pCurve->m_dStartPos = m_dEndPosition;
	}
	else
	{
		m_pCurve->m_dStartPos = m_dStartPosition;
	}

	m_pCurve->m_dStartTime = m_dStartTime;
	m_pCurve->m_dEndTime = m_dEndTime;

	if (m_pGraph != NULL)
		m_pGraph->InvalidateAll();
}

void CGfxPageVert::OnChangeEditEndpos()
{
	CString str="";
	GetDlgItem(IDC_EDIT_ENDPOS)->GetWindowText(str);
	if (str.IsEmpty())
	{
		return;
	}
	RefreshData();
}

void CGfxPageVert::OnChangeEditStartpos()
{
	CString str="";
	GetDlgItem(IDC_EDIT_STARTPOS)->GetWindowText(str);
	if (str.IsEmpty())
	{
		return;
	}
	RefreshData();
}

void CGfxPageVert::OnChangeEditStartTime()
{
	CString str="";
	GetDlgItem(IDC_EDIT_STARTTIME)->GetWindowText(str);
	if (str.IsEmpty())
	{
		return;
	}
	RefreshData();	
}

void CGfxPageVert::OnChangeEditEndTime()
{
	CString str="";
	GetDlgItem(IDC_EDIT_ENDTIME)->GetWindowText(str);
	if (str.IsEmpty())
	{
		return;
	}
	RefreshData();
}

void CGfxPageVert::OnSelchangeListCurve()
{
	if (m_lbCurveList.GetCount() <= 0)
	{
		return;
	}
	int nItemSel = m_lbCurveList.GetCurSel();
	if (nItemSel<0)
	{
		return;
	}
	m_pCurve = NULL;
	m_pCurve = (CCurve*)m_lbCurveList.GetItemData(nItemSel);

	InitCurve();
}

void CGfxPageVert::OnCheckDisplay()
{
	RefreshData();
}

void CGfxPageVert::OnCheckPrint()
{
	RefreshData();
}

void CGfxPageVert::OnCheckSave()
{
	RefreshData();
}

void CGfxPageVert::OnSelchangeComboFilter() 
{
	if (m_cbFilter.GetCount()<=0)
	{
		return;
	}
	RefreshData();
}

void CGfxPageVert::OnChangeEditLowervalue()
{
	CString strText;
	GetDlgItem(IDC_EDIT_LOWERVALUE)->GetWindowText(strText);
	if (!IsNumber(strText))
		return;
	RefreshData();
}

void CGfxPageVert::OnChangeEditName()
{
	CString str="";
	GetDlgItem(IDC_EDIT_NAME2)->GetWindowText(str);
	if (str.IsEmpty())
	{
		return;
	}
	RefreshData();
}

void CGfxPageVert::OnChangeEditUppervalue()
{
	CString strText;
	GetDlgItem(IDC_EDIT_UPPERVALUE)->GetWindowText(strText);
	if (!IsNumber(strText))
		return;
	RefreshData();
}

void CGfxPageVert::OnChangeEditUnit()
{
	CString str="";
	//GetDlgItem(IDC_DEPTHOFFSET)->GetWindowText(str);
	//if (str.IsEmpty())
	//	return;
	RefreshData();
}

BOOL CGfxPageVert::IsNumber(CString strNum)
{
	if (strNum.IsEmpty())
		return FALSE;

	if (strNum == "-" || strNum == ".")
		return FALSE;

	double fTest = 5.0;
	fTest = atof(strNum);
	if (fTest == 0.0)
	{
		fTest = atof(strNum + "5");
		if (fTest == 0.0)
			return FALSE;
		else
		{
			if (strNum[0] == '-')
				return FALSE;
		}
	}

	CString strNumChar = "-.0123456789";
	int iDot=0, iMinus=0;
	for (int i=0; i < strNum.GetLength(); i++)
	{
		//含有无效字符
		if (strNumChar.Find(strNum[i]) == -1)
			return FALSE;
		if (strNum[i] == '.')
			iDot++;
		if (strNum[i] == '-')
		{
			if (i!=0)
				return FALSE;
			iMinus++;
		}
	}
	if (iDot>1 || iMinus>1)
		return FALSE;

	return TRUE;
}

void CGfxPageVert::OnKillfocusEditUppervalue()
{
	//防止输入无效参数
	UpdateData(FALSE);
}

void CGfxPageVert::OnKillfocusEditLowervalue()
{
	//防止输入无效参数
	UpdateData(FALSE);
}

void CGfxPageVert::OnKillfocusDepthOffset()
{
	RefreshData();
}

void CGfxPageVert::OnKillfocusEditStartpos() 
{
	//防止输入无效参数
	UpdateData(FALSE);
}

void CGfxPageVert::OnKillfocusEditEndpos() 
{
	//防止输入无效参数
	UpdateData(FALSE);
}

void CGfxPageVert::OnKillfocusDepthDistance() 
{
	RefreshData();
}

void CGfxPageVert::OnKillfocusDepthDelay() 
{
	RefreshData();
}

BOOL CALLBACK ChildWndProc(HWND hwndChild, LPARAM lParam) 
{
	EnableWindow(hwndChild, FALSE);
	return TRUE;
}

BOOL CGfxPageVert::OnInitDialog() 
{
	CGfxPage::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_cbLineWidth.Initialize();
	m_btnCurveColor.EnableOtherButton(_T("More"));
	m_btnFillColor.EnableOtherButton(_T("More"));
	BOOL bPrint = (BOOL)(m_pGraph && m_pGraph->m_bLoggingPrint);
	if (bPrint)
	{
		EnumChildWindows(GetSafeHwnd(), ChildWndProc, 0);	
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CGfxPageVert::OnKillfocusEditTimeinter() 
{
	RefreshData();
}

void CGfxPageVert::OnBtnColor() 
{
	RefreshData();
}

void CGfxPageVert::OnBtnFillColor()
{
	RefreshData();
}

void CGfxPageVert::OnDestroy() 
{
	CGfxPage::OnDestroy();
	
	// TODO: Add your message handler code here
	m_pCurve = NULL;
	m_pGraph = NULL;
}

void CGfxPageVert::OnKillfocusEditTimedelay() 
{
	RefreshData();	
}

void CGfxPageVert::OnPlotStyle()
{
	RefreshData();
}

void CGfxPageVert::OnFillType()
{
	RefreshData();
}

void CGfxPageVert::OnSelChangeLineWidth() 
{	
	RefreshData();
}
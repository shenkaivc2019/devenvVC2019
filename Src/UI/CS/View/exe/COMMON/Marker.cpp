// Marker.cpp: implementation of the CMarker class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "Marker.h"
#include "Mark.h"
#include "Sheet.h"
#include "Utility.h"
#include "Project.h"
#include "GraphWnd.h"
#ifdef _PERFORATION
#include "PerforationDef.h"
#endif

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define DRAW_COLLAR_LEN  MTD(1.8)

extern CProject* g_pActPrj;
CMarker::CMarker()
{
	m_pSheet = NULL;
	m_pGraph = NULL;
	m_pTrack = NULL;
	m_MarkList.RemoveAll();
	m_lMinDepth = INVALID_DEPTH;
	m_lMaxDepth = INVALID_DEPTH; 
}

CMarker::CMarker(CSheet* pSheet)
{
	m_pSheet = pSheet;
	m_MarkList.RemoveAll();
	m_lMinDepth = INVALID_DEPTH;
	m_lMaxDepth = INVALID_DEPTH; 
}

CMarker::~CMarker()
{
	for(int i = 0; i < m_MarkList.GetSize(); i++)
	{
		CMark *pMark = (CMark*)m_MarkList.GetAt(i);
		delete pMark;
	}
	m_MarkList.RemoveAll();
}
int  CMarker::AddNullMark(long lDepth)
{
	BOOL bMin = FALSE;
	BOOL bMax = FALSE;
	if( INVALID_DEPTH == m_lMinDepth || lDepth < m_lMinDepth)
	{
		m_lMinDepth = lDepth;
		bMin = TRUE;
	}
	if( INVALID_DEPTH == m_lMaxDepth || lDepth > m_lMaxDepth)
	{
		m_lMaxDepth = lDepth;
		bMax = TRUE;
	}
	if( FALSE == bMin && FALSE == bMax )
	{
		return m_MarkList.GetSize();
	}

	if(TRUE == bMin)
	{	
		int nSize = m_MarkList.GetSize();
		CMark* pMark = NULL;
		int i = 0;
		for(i=0; i<nSize; i++)
		{
			pMark = (CMark*)m_MarkList.GetAt(i);	
			if( pMark != NULL && MARKS_MIN_NULL == pMark->m_nShape )
			{	
				pMark->m_lDepth = lDepth;
				if(0 != i)
				{	
					m_MarkList.RemoveAt(i);
					AddMark(pMark);
				}	
				break;
			}			
		}
		if(nSize == i)
		{
			pMark = new CMark();
	        pMark->m_lDepth = m_lMinDepth;
			pMark->m_nShape = MARKS_MIN_NULL;
			pMark->m_pTrack = m_pTrack;
			AddMark(pMark);
		}
	}

	if(TRUE == bMax)
	{
		int nSize = m_MarkList.GetSize();
		CMark* pMark = NULL;
		int i = 0;
		for(i=nSize-1; i>=0; i--)
		{	
			pMark = (CMark*)m_MarkList.GetAt(i);	
			if( pMark != NULL && AfxIsValidAddress(pMark, sizeof(CMark)) && MARKS_MAX_NULL == pMark->m_nShape )
			{
				pMark->m_lDepth = lDepth;
				if(i != nSize-1)
				{
					m_MarkList.RemoveAt(i);
					AddMark(pMark);
				}		
				break;
			}			
		}
		if(-1 == i)
		{
			pMark = new CMark();
	        pMark->m_lDepth = m_lMaxDepth;
			pMark->m_nShape = MARKS_MAX_NULL;
			pMark->m_pTrack = m_pTrack;
			AddMark(pMark);
		}
	}
	
	return m_MarkList.GetSize();
/*	BOOL bChange = FALSE;
	if( INVALID_DEPTH == m_lMinDepth || lDepth < m_lMinDepth)
	{
		m_lMinDepth = lDepth;
		bChange = TRUE;
	}
	if( INVALID_DEPTH == m_lMaxDepth || lDepth > m_lMaxDepth)
	{
		m_lMaxDepth = lDepth;
		bChange = TRUE;
	}
	if( FALSE == bChange )
	{
		return m_MarkList.GetSize();
	}

	
	//添加的节箍按深度升序排序
	int nSize = m_MarkList.GetSize();
	if(nSize>0)
	{
		CMark* pMark;
		for(int i=nSize-1; i>=0; i--)
		{
			CMark* pMark = (CMark*)m_MarkList.GetAt(i);
			int nShape = pMark->m_nShape;
			if(MARKS_NULL == nShape)
			{
				
				if( ( pMark = (CMark*)m_MarkList.GetAt(i) ) != NULL)
				{
					m_MarkList.RemoveAt(i);
					delete pMark;
					pMark = NULL;
				}
			}			
		}
	}
	CMark *pMark = new CMark();
	pMark->m_lDepth = m_lMinDepth;
	pMark->m_nShape = MARKS_NULL;
	pMark->m_pTrack = m_pTrack;
	AddMark(pMark);
    pMark = NULL;

	pMark = new CMark();
	pMark->m_lDepth = m_lMaxDepth;
	pMark->m_nShape = MARKS_NULL;
	pMark->m_pTrack = m_pTrack;
	AddMark(pMark);

	return m_MarkList.GetSize();*/
}

int CMarker::AddMark(CMark* pMark)
{
	switch(pMark->m_nShape)
	{
	case MARKS_VERTICAL_LINE:
		pMark->m_rect.left = 0;
		pMark->m_rect.right = 30;
		pMark->m_rect.bottom = 30;
		pMark->m_rect.top = 0;
		break;
	case MARKS_TRIANGLE:
		pMark->m_rect.left = 0;
		pMark->m_rect.right = 30;
		pMark->m_rect.bottom = 50;
		pMark->m_rect.top = 0;
		break;
	case MARKS_ARROW:
		pMark->m_rect.left = 0;
		pMark->m_rect.right = 60;
		pMark->m_rect.bottom = 60;
		pMark->m_rect.top = 0;
		break;
	case MARKS_SPLICE:
		{
			CRect rc;
			m_pGraph->GetClientRect(&rc);
			pMark->m_rect.left = 0;
			pMark->m_rect.right = rc.right;
			pMark->m_rect.top = 0;
			pMark->m_rect.bottom = 30;
		}
		break;
	default:
		pMark->m_rect.left = 0;
		pMark->m_rect.right = 30;
		pMark->m_rect.bottom = 30;
		pMark->m_rect.top = 0;
		break;
	}

    //添加的节箍按深度升序排序
	int nSize = m_MarkList.GetSize();
	if(0 == nSize)
	{
		m_MarkList.Add(pMark);
	}
	else if(nSize>0)
	{
		int i = 0;
		for(i=0; i<nSize; i++)
		{
			long lDepth = ((CMark*)m_MarkList[i])->m_lDepth;
			if(pMark->m_lDepth < lDepth)
			{
				m_MarkList.InsertAt(i,pMark);
				break;
			}			
		}
		if(i == nSize)
		{
			m_MarkList.Add(pMark);
		}
	}

	
	return m_MarkList.GetSize();
}

void CMarker::AddMark(CTrack* pTrack, long lDepth,  int nCollarIndex/* = -1*/, int nStdCollarIndex/* = -1*/, 
					   int nLineStyle /*= MARKS_PERFORATION_COLLOR*/, long lDepthAdjust/* = 0*/)
{
	CMark* pMark = new CMark;	
	pMark->m_strName.Format("%.2lf", DTM(lDepth-lDepthAdjust));
	pMark->m_lDepth = lDepth;
	pMark->m_pTrack = pTrack;
	m_pTrack = pTrack;
	pMark->m_nCollarIndex = nCollarIndex;
	pMark->m_nStdCollarIndex = nStdCollarIndex;		
	pMark->m_nShape = nLineStyle;
	pMark->m_lDepthAdjust = lDepthAdjust;
	
	AddMark(pMark);
}

/* 函数名称：DrawMark
 * 函数描述：绘制标签
 * 返回类型：void
 * 函数参数：
 */
void CMarker::DrawMark(CDC* pDC, long lStartDepth, long lEndDepth)
{
	int i = 0;
	for(i=0; i<m_MarkList.GetSize(); i++)
	{
		CMark* pMark=(CMark*)m_MarkList.GetAt(i);
		pMark->Draw(pDC, lStartDepth, lEndDepth);
	}

	if (m_MarkList.GetSize() > 1)
	{
		CRect rcTrack;
		m_pTrack->GetRegionRect(&rcTrack, FALSE);
		int nLeft= rcTrack.left;
		int nRight=rcTrack.right;

		int size = m_MarkList.GetSize();
		for (i = 1; i < size; i++)
		{
			CMark* pMark0 = (CMark*)m_MarkList.GetAt(i-1);
			CMark* pMark1 = (CMark*)m_MarkList.GetAt(i);
			//如果若所画为垂直标签，即射孔所用的标尺
			if( MARKS_PERFORATION_COLLOR != pMark0->m_nShape &&
				MARKS_MIN_NULL != pMark0->m_nShape &&
				MARKS_MAX_NULL != pMark0->m_nShape &&
				MARKS_MARK_LINE != pMark0->m_nShape &&
				MARKS_FIRE_LINE != pMark0->m_nShape &&
				MARKS_BLOCK_CURVE != pMark0->m_nShape
			  )
			{
				return;
			}
            

			// 如果标签的深度超过了当前要显示的深度，就不再显示标签
			if( (pMark1->m_lDepth > lEndDepth + MTD(15.0)) || (pMark0->m_lDepth > lEndDepth + MTD(15.0)) )
				return;

			long lLength = pMark1->m_lDepth - pMark0->m_lDepth;
			//if ( lLength > DRAW_COLLAR_LEN )
			{
				long lDepth  = (pMark0->m_lDepth + pMark1->m_lDepth) / 2;

				int nPos = m_pTrack->GetCoordinateFromDepth(lDepth);
				

				if ( lLength > DRAW_COLLAR_LEN )
				{
					CFont myFont,*pOldFont;
					myFont.CreatePointFont(100,"Arial",pDC);
					pOldFont = pDC->SelectObject(&myFont);
                    CString str;
					if( ( pMark1->m_nStdCollarIndex != SC_ERR && pMark0->m_nShape == MARKS_FIRE_LINE ) ||
						( pMark0->m_nStdCollarIndex != SC_ERR && pMark1->m_nShape == MARKS_FIRE_LINE ) )
					{
#ifdef _PERFORATION						
						if (NULL != g_pActPrj)
						{
							TAILOR_INFO ti;
							g_pActPrj->GetPerforationInfo(ti);
							str.Format("%.2lf(%.2lf)", DTM(labs(lLength)), DTM(ti.lRefDownCount) );
						}
#endif
					}
					else
					{
						str.Format("%.2lf", DTM(labs(lLength)));
					}

					pDC->TextOut((nLeft+nRight)/2, nPos, str);

					pDC->SelectObject(pOldFont);
				}
				
				long lDepth0 = m_pTrack->GetCoordinateFromDepth(pMark0->m_lDepth);
				long lDepth1 = m_pTrack->GetCoordinateFromDepth(pMark1->m_lDepth);
				CPen pen( PS_SOLID, 1, RGB(0,0,0) );
				CPen* pOldPen = pDC->SelectObject(&pen);
				
				pDC->MoveTo((nLeft+nRight)/2, lDepth0);
				pDC->LineTo((nLeft+nRight)/2, lDepth1);

				pDC->SelectObject(pOldPen);
			}
		}
	}	
}

/* 函数名称：PrintMark
 * 函数描述：打印节箍及节箍长度
 * 返回类型：void
 * 函数参数：CULPrintInfo
 */
void CMarker::PrintMark(CULPrintInfo* pInfo)
{
	int i = 0;
	for (i = 0; i < m_MarkList.GetSize(); i++)
	{
		CMark* pMark = (CMark*)m_MarkList.GetAt(i);
		pMark->Print(pInfo);
	}

	if (m_MarkList.GetSize() > 1)
	{
		CRect rcTrack;
		m_pTrack->GetRegionRect(&rcTrack, TRUE);
		
		CRect rt = pInfo->CoordinateConvert(rcTrack);

		for (i = 1; i < m_MarkList.GetSize(); i++)
		{
			CMark* pMark0 = (CMark*)m_MarkList.GetAt(i-1);
			CMark* pMark1 = (CMark*)m_MarkList.GetAt(i);
			//如果若所画为垂直标签，即射孔所用的标尺
			if( MARKS_PERFORATION_COLLOR != pMark0->m_nShape &&
				MARKS_MIN_NULL != pMark0->m_nShape &&
				MARKS_MAX_NULL != pMark0->m_nShape &&
				MARKS_MARK_LINE != pMark0->m_nShape &&
				MARKS_FIRE_LINE != pMark0->m_nShape &&
				MARKS_BLOCK_CURVE != pMark0->m_nShape
			  )
			{
				return;
			}

			long lLength = pMark1->m_lDepth - pMark0->m_lDepth;
			//if (lLength > 0)
			{
				long lDepth  = (pMark0->m_lDepth + pMark1->m_lDepth) / 2;
				
				int nPos = pInfo->GetPrintDepthCoordinate(DTM(lDepth));
				
				if ( lLength > DRAW_COLLAR_LEN )
				{
					CFont myFont,*pOldFont;
					myFont.CreatePointFont(100,"Arial",&pInfo->m_printDC);
					pOldFont = pInfo->m_printDC.SelectObject(&myFont);

					CString str;
					if( ( pMark1->m_nStdCollarIndex != SC_ERR && pMark0->m_nShape == MARKS_FIRE_LINE ) ||
						( pMark0->m_nStdCollarIndex != SC_ERR && pMark1->m_nShape == MARKS_FIRE_LINE ) )
					{
#ifdef _PERFORATION
						if (NULL != g_pActPrj)
						{
							TAILOR_INFO ti;
							g_pActPrj->GetPerforationInfo(ti);
							str.Format("%.2lf(%.2lf)", DTM(labs(lLength)), DTM(ti.lRefDownCount) );
						}
#endif
					}
					else
					{
						str.Format("%.2lf", DTM(labs(lLength)));
					}

					pInfo->PrintText(str, (rt.left+rt.right)/2, nPos);
					pInfo->m_printDC.SelectObject(pOldFont);
				}
				
				long lDepth0 = pInfo->GetPrintDepthCoordinate(DTM(pMark0->m_lDepth));
				long lDepth1 = pInfo->GetPrintDepthCoordinate(DTM(pMark1->m_lDepth));
				pInfo->m_printDC.MoveTo((rt.left+rt.right)/2, lDepth0);
				pInfo->m_printDC.LineTo((rt.left+rt.right)/2, lDepth1);
			}
		}
	}

}

/* 函数名称：HitTest
 * 函数描述：点击测试
 * 返回类型：CMark* 返回所击中的Mark
 * 函数参数：pt：当前点击点的坐标；pCurve：无用
 */
CMark* CMarker::HitTest(CPoint pt, CCurve* pCurve)
{
	for(int i = 0 ; i < m_MarkList.GetSize() ; i ++)
	{
		CMark *pMark = (CMark*)m_MarkList.GetAt(i);
		CRect rcMark = MarkInRect(pMark);
		long temp = pt.x - rcMark.left;
		rcMark.left += temp;
		rcMark.right += temp;
		rcMark.NormalizeRect();
		if(rcMark.PtInRect(pt))
			return pMark ;
	}
	return NULL;
}

/* 函数名称：MarkInRect
 * 函数描述：返回Mark所在的矩形区
 * 返回类型：CRect：Mark所在矩形区对象
 * 函数参数：pMark：用以返回矩形区的Mark
 */
CRect CMarker::MarkInRect(CMark * pMark)
{
	CRect rcMark;
	
	CTrack * pTrack = m_pTrack;//(CTrack * ) m_pSheet->m_TrackList.GetAt(pMark->nTrackNO);

	pTrack->GetRegionRect(&rcMark, FALSE);
	int nLeftMargin = rcMark.left;
	int nRightMargin = rcMark.right;
	
	int nPos =  pTrack->GetCoordinateFromDepth(pMark->m_lDepth);
	rcMark.left = nLeftMargin;
	rcMark.right = rcMark.left + abs(pMark->m_rect.right - pMark->m_rect.left);
	rcMark.bottom = nPos - abs((pMark->m_rect.bottom - pMark->m_rect.top)/2);
	rcMark.top  = nPos + abs((pMark->m_rect.bottom - pMark->m_rect.top)/2);
	
	return rcMark;	
}

/* 函数名称：DeleteMark
 * 函数描述：删除指定范围的Mark
 * 返回类型：void
 * 函数参数：strName：要删除的Mark的名称
 *			 long：	  要删除的Mark的深度
 *			 nTrackNO：要删除的Mark所在的井道号
 */
/*
void CMarker::DeleteMark(CString strName , long fDepth , int nTrackNO)
{
	int nSize = m_MarkList.GetSize();
	for( int i = nSize - 1 ; i >= 0; i--)
	{
		CMark *pMark = (CMark*)m_MarkList.GetAt(i);
		CString strMarkName = pMark->m_strName;
		long fMarkDepth = pMark->m_lDepth;
		if((strName == strMarkName)	&& (nTrackNO == pMark->m_nTrackNO) && (abs(fDepth - fMarkDepth) < 1))
		{
			m_MarkList.RemoveAt(i);
			delete pMark;
			pMark = NULL;
		}
	}
}
*/
void CMarker::DeleteMark(CMark* pMark)
{
	int nSize = m_MarkList.GetSize();
	for (int i = nSize - 1; i >= 0; i--)
	{
		CMark* p = (CMark*)m_MarkList.GetAt(i);
		if (p == pMark)
		{
			m_MarkList.RemoveAt(i);
			delete pMark;
			pMark = NULL;
		}
	}
}

/* 函数名称：GetMarkSize
 * 函数描述：返回MarkList中元素的个数
 * 返回类型：void
 * 函数参数：void
 */
int CMarker::GetMarkSize()
{
	return m_MarkList.GetSize();
}

/* 函数名称：ResetMark
 * 函数描述：重置m_MarkList，重置将删除MarkList中所有元素，包括它们申请的内存
 * 返回类型：void
 * 函数参数：void
 */
void CMarker::ResetMark()
{
	for (int i = 0; i < this->m_MarkList.GetSize(); i++)
	{
		CMark* pMark = (CMark*)m_MarkList.GetAt(i);
		delete pMark;
		pMark = NULL;
	}
	m_MarkList.RemoveAll();
}
/* 函数名称：ModifyMark
 * 函数描述：若查找到，则修改Mark信息
 * 返回类型：BOOL:   FALSE--修改失败  TRUE--查找修改成功
 * 函数参数：
 *      lDepth:用它来修改CMark::m_lDepth
 *      nCollarIndex: 查找条件
 *      nStdCollarIndex: 查找条件
 *      查找条件:  nCollarIndex == CMark::m_nCollarIndex && nStdCollarIndex == CMark::m_nStdCollarIndex
 */
BOOL CMarker::ModifyMark(long lDepth,int nCollarIndex, int nStdCollarIndex)
{
	UINT nSize = m_MarkList.GetSize();
	for(int i=0; i<nSize; i++)
	{
		if(((CMark*)m_MarkList[i])->m_nCollarIndex == nCollarIndex &&
			((CMark*)m_MarkList[i])->m_nStdCollarIndex == nStdCollarIndex)
		{
			((CMark*)m_MarkList[i])->m_lDepth = lDepth;
			((CMark*)m_MarkList[i])->m_strName.Format("%.2lf", DTM(lDepth));
			return TRUE;
		}
	}
	return FALSE;
}

/* 函数名称：AdjustMarksDepth
 * 函数描述：校正所有Mark深度值，即加上参数lAdjustDepth
 * 返回类型：无
 * 函数参数：
 *      lAdjustDepth:用来校正Mark的深度值
 */
void CMarker::AdjustMarksDepth(long lAdjustDepth)
{
	int nSize = m_MarkList.GetSize();
	for(int i=0; i<nSize; i++)
	{
		((CMark*)m_MarkList[i])->m_lDepth += lAdjustDepth;
	}
}

void CMarker::Serialize(CXMLSettings& xml)
{
	if (xml.IsStoring())
	{
		int nCount = m_MarkList.GetSize();
		xml.Write(_T("TrackMarkCount"), nCount);
		for (int i = 0; i < nCount; i++)
		{
			CString strMark;
			strMark.Format("TrackMark%d", i);
			if (xml.CreateKey(strMark))
			{
				CMark* pMark = (CMark*)m_MarkList.GetAt(i);
				pMark->Serialize(xml);
				xml.Back();
			}

		}
	}
	else
	{
		int nCount = 0;
		xml.Read(_T("TrackMarkCount"), nCount);
		for (int i = 0; i < nCount; i++)
		{
			CString strMark;
			strMark.Format("TrackMark%d", i);
			if (xml.Open(strMark))
			{
				CMark* pMark = new CMark;
				pMark->m_pTrack = m_pTrack;
				pMark->Serialize(xml);
				m_MarkList.Add(pMark);	
				xml.Back();
			}
		}
	}
}

// 将当前的节箍拷贝道pTrack所指的井道中
void CMarker::CopyTo(CTrack* pTrack)
{
	// 如果pTrack为空或pTrack与当前节箍所在井道相同，不进行操作
	if (NULL == pTrack )
		return;
	if (m_pTrack == pTrack)
		return;

	pTrack->m_Marker.ResetMark();
	UINT nSize = GetMarkSize();
	for(int i=0; i<nSize; i++)
	{
		CMark* pMark = (CMark*)m_MarkList.GetAt(i);
		if(NULL != pMark)
		{
			CMark* pNewMark = new CMark(pMark,pTrack);
			if(NULL != pNewMark)
			{
				pTrack->m_Marker.m_MarkList.Add(pNewMark);
			}
		}	
	}//End for
}

void CMarker::InitMarkerDepth()
{
	m_lMinDepth = INVALID_DEPTH;
	m_lMaxDepth = INVALID_DEPTH; 
}

BOOL CMarker::DeleteMark(CString& strName)
{
	for(int i = m_MarkList.GetSize() - 1; i >= 0; i--)
	{
		CMark* pMark = (CMark*)m_MarkList.GetAt(i);
		if (strName == pMark->m_strName)
		{
			delete pMark;
			m_MarkList.RemoveAt(i);
			return TRUE;
		}
	}
	return FALSE;
}
// SCurve.cpp: implementation of the CSCurve class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SCurve.h"
#include "ICurve.h" 

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSCurve::CSCurve(LPCTSTR pszName, LPCTSTR pszUnit)
{
	m_strName = pszName;
	m_strUnit = pszUnit;
	int iFind = m_strUnit.Find(_T(","));
	if (iFind > 0)
	{
		m_strUType = m_strUnit.Left(iFind);
		m_strUName = m_strUnit.Right(m_strUnit.GetLength() - iFind - 1);
	}

	m_pValue = NULL;
    m_pICurve = NULL; 
}

CSCurve::~CSCurve()
{
}


int CSCurve::Dimension()
{
    if (m_nPointFrame > 1)
    {
        return 2;
    }
    
    return 1;
}

double CSCurve::GetValueOfBuffer(int nIndex)
{
    double dValue = 0;
    if(m_pICurve)
    {
        int n = m_pICurve->GetDataSize(); 
        if(n > 0)
            dValue = m_pICurve->GetdblValue(n-1, nIndex); 
    }
    else if(m_pValue)
    {
        switch (m_nValueType)
        {
        case VT_I1:
            dValue = ((char *) m_pValue)[nIndex];
            break;
        case VT_I2:
            dValue = ((short *) m_pValue)[nIndex];
            break;
        case VT_I4:
            dValue = ((long *) m_pValue)[nIndex];
            break;
        case VT_R4:
            dValue = ((float *) m_pValue)[nIndex];
            break;
        case VT_R8:
            dValue = ((double *) m_pValue)[nIndex];
            break;
        case VT_UI1:
            dValue = ((BYTE *) m_pValue)[nIndex];
            break;
        case VT_UI2:
            dValue = ((WORD *) m_pValue)[nIndex];
            break;
        case VT_UI4:
            dValue = ((DWORD *) m_pValue)[nIndex];
            break;
        default:
            TRACE(_T("Error : Get value of unsupported type!\n"));
        }
    }
    return dValue;
}

int CSCurve::GetValueBufferSize()
{
    return m_nSizePoint * m_nPointFrame;
}

void CSCurve::SetValType(VARTYPE lValType)
{
    switch(lValType)
    {
    case VT_I4:
    case VT_UI4:
    case VT_R4:
        m_nSizePoint = 4;
        break;
    case VT_R8:
        m_nSizePoint = 8;
        break;
    case VT_I2:
    case VT_UI2:
        m_nSizePoint = 2;
        break;
    case VT_I1:
    case VT_UI1:
        m_nSizePoint = 1;
        break;
    default:
        return;
    }
    
    m_nValueType = lValType;
}
// GfxPageFill.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "GfxPageFill.h"
#include "GraphWnd.h"
#include "MainFrm.h" 
#include "Track.h"
#include "Curve.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGfxPageFill dialog

CGfxPageFill::CGfxPageFill(CWnd* pParent /*=NULL*/) : CGfxPage(IDD_GFX_PAGE_FILL, pParent)
{
	//{{AFX_DATA_INIT(CGfxPageFill)
	//}}AFX_DATA_INIT
	m_nCurSelect = -1;
	m_nFillNumber = 0;
	m_fLeftValue = 0.0f;
	m_fRightValue = 0.0f;
	m_nLeftFillType = 0;
	m_nRightFillType = 0;
}


void CGfxPageFill::DoDataExchange(CDataExchange* pDX)
{
	CGfxPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGfxPageFill)
	DDX_Control(pDX, IDC_COMBO_RIGHT_CURVE, m_cmbRight);
	DDX_Control(pDX, IDC_COMBO_LEFT_CURVE, m_cmbLeft);
	DDX_Control(pDX, IDC_BTN_COLOR, m_btnColor);
	DDX_Control(pDX, IDC_BTN_BKCOLOR,m_btnBKColor);
	DDX_Control(pDX, IDC_BUTTON1, m_btnPattern);
	DDX_Text(pDX, IDC_EDIT_LEFT_VALUE, m_fLeftValue);
	DDX_Text(pDX, IDC_EDIT_RIGHT_VALUE, m_fRightValue);
	DDX_Radio(pDX, IDC_RADIO_LEFT_VALUE, m_nLeftFillType);
	DDX_Radio(pDX, IDC_RADIO_RIGHT_VALUE, m_nRightFillType);
	DDX_Text(pDX, IDC_EDIT_CURVENAME, m_strName);
	//}}AFX_DATA_MAP

}


BEGIN_MESSAGE_MAP(CGfxPageFill, CGfxPage)
	//{{AFX_MSG_MAP(CGfxPageFill)
	ON_EN_KILLFOCUS(IDC_EDIT_LEFT_VALUE, FlushData)
	ON_EN_KILLFOCUS(IDC_EDIT_RIGHT_VALUE, FlushData)
	ON_CBN_SELCHANGE(IDC_COMBO_LEFT_CURVE, FlushData)
	ON_CBN_SELCHANGE(IDC_COMBO_RIGHT_CURVE, FlushData)
	ON_BN_CLICKED(IDC_RADIO_LEFT_VALUE, OnRadioLeftValue)
	ON_BN_CLICKED(IDC_RADIO_LEFT_CURVE, OnRadioLeftCurve)
	ON_BN_CLICKED(IDC_RADIO_RIGHT_VALUE, OnRadioRightValue)
	ON_BN_CLICKED(IDC_RADIO_RIGHT_CURVE, OnRadioRightCurve)
	ON_BN_CLICKED(IDC_BTN_COLOR, FlushData)
	ON_BN_CLICKED(IDC_BTN_BKCOLOR,FlushData)
	ON_EN_CHANGE(IDC_EDIT_CURVENAME,OnChangeCurveName)
	//}}AFX_MSG_MAP
	ON_MESSAGE(PPN_SELENDOK, FlushDataEx)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGfxPageFill message handlers
LONG CGfxPageFill::OnColorSelOk(UINT lParam, LONG wParam)
{
	FlushData();
	return 0;
}

LRESULT CGfxPageFill::FlushDataEx(WPARAM wp, LPARAM lp)
{
	FlushData();
	return 0;
}
void CGfxPageFill::FlushData()
{
	if (m_pCurve == NULL)
		return;

	int nIndex = -1;
	FILLPROP* pFillProp = &m_pCurve->m_FillProp;
	UpdateData(TRUE);

	//add by yh 2009.2.5
	//解决了bug:左值小于0或右值大于100填充道显示在井道外侧
// 	if (m_fLeftValue < 0)
// 		pFillProp->fLValue = 0;
// 	else
		pFillProp->fLValue = m_fLeftValue;

// 	if (m_fRightValue > 100)
// 		pFillProp->fRValue = 100;
// 	else
		pFillProp->fRValue = m_fRightValue;

	pFillProp->nLType = m_nLeftFillType;
	pFillProp->nRType = m_nRightFillType;
	pFillProp->crBkColor = m_btnBKColor.GetColor();
	pFillProp->crFgColor = m_btnColor.GetColor();
	pFillProp->nFillType = m_nFillType;
	_tcscpy(pFillProp->szFillFileName, m_btnPattern.m_strPattern);
	nIndex = m_cmbLeft.GetCurSel();
	if (nIndex >= 0)
	{
		m_cmbLeft.GetLBText(nIndex, pFillProp->szLCurveName);
	}

	nIndex = m_cmbRight.GetCurSel();
	if (nIndex >= 0)
	{
		m_cmbRight.GetLBText(nIndex, pFillProp->szRCurveName);
	}

	// 更新绘图页面
	if (m_pGraph != NULL)
		m_pGraph->InvalidateAll();
}

void CGfxPageFill::OnRadioLeftValue()
{
	// TODO: Add your control notification handler code here
	m_nLeftFillType = VALUE_TYPE;
	FlushData();
}

void CGfxPageFill::OnRadioLeftCurve()
{
	// TODO: Add your control notification handler code here
	m_nLeftFillType = CURVE_TYPE;
	FlushData();
}

void CGfxPageFill::OnRadioRightValue()
{
	// TODO: Add your control notification handler code here
	m_nRightFillType = VALUE_TYPE;
	FlushData();
}

void CGfxPageFill::OnRadioRightCurve()
{
	// TODO: Add your control notification handler code here
	m_nRightFillType = CURVE_TYPE;
	FlushData();
}

void CGfxPageFill::InitCurve()
{
	if (m_pCurve == NULL)
		return;

	m_cmbLeft.ResetContent();
	m_cmbRight.ResetContent();
	int nIndex = -1;
	for (int i = 0; i < m_pTrack->m_vecCurve.size(); i++)
	{
		CCurve* pCurve = (CCurve*) m_pTrack->m_vecCurve.at(i);
		CString strTemp = pCurve->m_strName;
		m_cmbLeft.AddString(strTemp);
		m_cmbRight.AddString(strTemp);
	}

	m_strName = m_pCurve->m_strName;
	m_fLeftValue = m_pCurve->m_FillProp.fLValue;
	m_fRightValue = m_pCurve->m_FillProp.fRValue;
	m_nLeftFillType = m_pCurve->m_FillProp.nLType;
	m_nRightFillType = m_pCurve->m_FillProp.nRType;
	m_btnColor.SetColor(m_pCurve->m_FillProp.crFgColor);
	m_btnBKColor.SetColor(m_pCurve->m_FillProp.crBkColor);
	m_nFillType = m_pCurve->m_FillProp.nFillType;
	m_btnPattern.m_strPattern = m_pCurve->m_FillProp.szFillFileName;

	UINT nFlag = 0;

	CString strTemp = m_pCurve->m_FillProp.szLCurveName;
	nFlag = m_cmbLeft.FindString(-1, strTemp);
	if (nFlag != CB_ERR)
	{
		m_cmbLeft.SetCurSel(nFlag);
	}

	strTemp = m_pCurve->m_FillProp.szRCurveName;
	nFlag = m_cmbRight.FindString(-1, strTemp);
	if (nFlag != CB_ERR)
	{
		m_cmbRight.SetCurSel(nFlag);
	}

	UpdateData(FALSE);
	Invalidate(FALSE);
}

BOOL CGfxPageFill::OnInitDialog()
{
	CGfxPage::OnInitDialog();
	

	m_btnPattern.m_dwType = 0x01;
	m_btnColor.EnableOtherButton("");
	m_btnBKColor.EnableOtherButton("");

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CGfxPageFill::OnChangeCurveName()
{
	UpdateData(TRUE);
	m_pCurve->SetName(m_strName);
	if (m_pGraph != NULL)
		m_pGraph->InvalidateAll();
}

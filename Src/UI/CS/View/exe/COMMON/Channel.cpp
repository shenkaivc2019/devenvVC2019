// Channel.cpp: implementation of the CChannel class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Channel.h"
#include "ULCOMMDEF.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


CChannel::CChannel()
{
	m_pEvent = NULL;
	m_pDataItem = NULL;
	m_lpDataBuffer = NULL;
	m_strEventName = _T("ChannelEvent");
	m_strMapFileName = _T("ChannelMap");
	m_hExchangeFile = NULL;

	m_pDataHeader = NULL;

}

CChannel::CChannel(CHANNEL initChn)
{
	*(CHANNEL*)this = initChn;
	m_pEvent = NULL;
	m_pDataItem = NULL;
	m_lpDataBuffer = NULL;
	m_strEventName = _T("ChannelEvent");
	m_strMapFileName = _T("ChannelMap");
	m_hExchangeFile = NULL;
	
	m_pDataHeader = NULL;
}

CChannel::~CChannel()
{
	if (m_hExchangeFile != NULL)
	{
		UnmapViewOfFile(m_lpDataBuffer);
		CloseHandle(m_hExchangeFile);
	}

	if (m_pEvent != NULL)
	{
		delete m_pEvent;
		m_pEvent = NULL;
	}

}

short CChannel::OpenChannel()
{
	if (m_pEvent == NULL)
	{
		m_pEvent = new CEvent(FALSE, TRUE, m_strEventName, NULL);
	}	

	//建立数据交换映射文件 
	if (m_hExchangeFile == NULL)
	{
		m_hExchangeFile = CreateFileMapping((HANDLE)0xFFFFFFFF,
			0,
			PAGE_READWRITE,
			0,
			dwBufferSize + sizeof(ChannelHeader),
			m_strMapFileName);

		m_lpDataBuffer = MapViewOfFile(
			m_hExchangeFile, 
			FILE_MAP_ALL_ACCESS,// FILE_MAP_READ,
			0,
			0,
			dwBufferSize + sizeof(ChannelHeader));

		if (m_lpDataBuffer == NULL)
		{
			CloseHandle(m_hExchangeFile);
			m_hExchangeFile = NULL;
			return UL_ERROR;
		}

		m_pDataHeader = (ChannelHeader*)m_lpDataBuffer;
		m_pDataHeader->bStart = 0;
		m_pDataHeader->lLock = 0;
	}
	
	return UL_NO_ERROR;

}

short CChannel::CloseChannel()
{
	if (m_pEvent != NULL)
	{
		//m_pEvent->SetEvent();
		m_pEvent->PulseEvent();
		/*设置为有信号，释放所有等待的线程，重置为非信号状态*/

		delete m_pEvent;
		m_pEvent = NULL;
	}
	return UL_NO_ERROR;
}

CString	CChannel::GetEventName()
{	
	return m_strEventName;
}

CString CChannel::GetMapFileName()
{	
	return m_strMapFileName;
}

void CChannel::SetBufferSize(DWORD dwSize)
{
	dwBufferSize = dwSize;
}

DWORD CChannel::GetBufferSize()
{
	return	dwBufferSize;
}

LPVOID CChannel::GetBuffer()
{
	CMutex mutex;
	CSingleLock sLock(&(mutex)); 
	sLock.Lock ();				//数据加锁
	if (sLock.IsLocked())
	{
		LPVOID pDataBuffer = NULL;
		if (m_lpDataBuffer != NULL)
			pDataBuffer = (BYTE*)m_lpDataBuffer+sizeof(ChannelHeader)+nOffset;
		sLock.Unlock ();	    //解锁
		return pDataBuffer;
	}
	else
		return NULL;
}

int CChannel::GetData(LPVOID pData, int nNum)
{
	if (m_lpDataBuffer != NULL)
	{
		//	CSingleLock sLock(&(m_Mutex)); 
		//	sLock.Lock ();          // 数据加锁
		//////////////////////////////
		int nDynLen = 0;
		//memcpy(&nDynLen, m_lpDataBuffer, sizeof(int));
		nDynLen = m_pDataHeader->nLength;
		nNum = min(nDynLen, nNum);
		memcpy(pData, (BYTE*)m_lpDataBuffer + sizeof(ChannelHeader), nNum);
		//////////////////////////////
		//	sLock.Unlock ();	    // 解锁
		return nNum;
	}
	
	return 0;
}

int CChannel::SetData(LPVOID pData, int nNum)
{
	CMutex  mutex;
	if (m_lpDataBuffer != NULL)
	{
		CSingleLock sLock(&(mutex));
		sLock.Lock ();          // 数据加锁
		/////////////////////////////////
		//memcpy(m_lpDataBuffer, &nNum, sizeof(int));
		m_pDataHeader->nLength = nNum;
		memcpy((BYTE*)m_lpDataBuffer + sizeof(ChannelHeader), pData, nNum);
		/////////////////////////////////
		sLock.Unlock ();	    // 解锁
		return (short)nNum;
	}
	
	return UL_ERROR;
}

BOOL CChannel::Unlock()
{
	/*CMutex  mutex;
	CSingleLock sLock(&(mutex)); 
	sLock.Lock ();				//数据加锁
	if (sLock.IsLocked())
	{
		m_pDataHeader->bLock = FALSE;
		sLock.Unlock();	    // 解锁
		return TRUE;
		//TRACE("UnLock Channel Flag : %d\n", m_pDataHeader->bLock);
	}
	return FALSE;*/
	//InterlockedDecrement(&m_pDataHeader->lLock);
	if (m_pDataHeader != NULL)
	{
		m_pDataHeader->lLock -= 1;
		if (m_pDataHeader->lLock < 0)
		{
			m_pDataHeader->lLock = 0;
		}
	}
	return TRUE;
}

void CChannel::Start()
{
	//CMutex  mutex;
	//CSingleLock sLock(&(mutex)); 
	//sLock.Lock ();				//数据加锁
	//if (sLock.IsLocked())
	if (m_pDataHeader != NULL)
	{
		m_pDataHeader->lLock = 0;
		m_pDataHeader->bStart = TRUE;	
	//	sLock.Unlock();	    // 解锁
	}
}

void CChannel::Stop()
{
	if (m_pDataHeader != NULL)
		m_pDataHeader->bStart = FALSE;	
}
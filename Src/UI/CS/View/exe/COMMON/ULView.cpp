// ULView.cpp : implementation file
//

#include "stdafx.h"
#ifdef _LOGIC
#include "Logic.h"
#else
#include "ul2000.h"
#include "CalSelectDlg.h"
#include "CalReport.h"
#include "CalCoefView.h"
#include "CalSummaryView.h"
#include "ParamsView.h"
#include "RftDataGroupView.h"
#include "RftProfileGroupView.h"
#endif
#include "ULView.h"
#include "ULFile.h"
#include "PrintInfo.h"
#include "Cell2000.h"
#include "Project.h"
#include "GraphWnd.h"
#include "Sheet.h"
#include "Curve.h"
#include "MainFrm.h"
#include "SaveAsBmp.h"
#include "PrintOrderFile.h"
#include "GraphHeaderView.h"
#include "ServiceTableItem.h"
#include "ReportSetDlg.h"
#include "ULTool.h"
#include "ChildFrm.h"
#include "ComConfig.h"
#include "SymbolManagerDlg.h"
#include "GridPropertySheet.h"
#include "Utility.h"
#include "SymbolCustomizedDlg.h"
#include "Picture.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CULPrintInfo* CULView::m_pPrintInfo = NULL;
CMainFrame* CULView::m_pMainWnd = NULL;
CFont g_font[ftCount];
CArray<PDFPRINTINFO , PDFPRINTINFO> CULView::m_pdfPrintInfo;
CSymbolManagerDlg* CULView::m_pSymDlg = NULL;

HCURSOR CULView::m_hcurLine = AfxGetApp()->LoadCursor(IDC_AFXBARRES_LINE);
HCURSOR CULView::m_hcurRect = AfxGetApp()->LoadCursor(IDC_AFXBARRES_RECT);

BOOL CULView::m_bAutoFitPaper = TRUE;
BOOL CULView::m_bPrinter820 = TRUE;
BOOL CULView::m_bDefineSymbol = FALSE;
long CULView::m_nLineStyle = 2;
int CULView::m_nFileID = 0;
int CULView::m_nTA = 0;

#define ID_EDIT_FIRST	ID_EDIT_COL_INSERT_LEFT
#define ID_EDIT_LAST	ID_EDIT_FILL_COLOR

/////////////////////////////////////////////////////////////////////////////
// CULView

IMPLEMENT_DYNCREATE(CULView, CScrollView)

CULView::CULView()
{
	m_rcPage.SetRectEmpty();
	m_dwEdit = 0;
	m_nRefresh = 1;
	m_pEdit = NULL;
	m_pDoc = NULL;
}

CULView::~CULView()
{
}

BEGIN_MESSAGE_MAP(CULView, CScrollView)
	//{{AFX_MSG_MAP(CULView)
	ON_WM_CREATE()
	ON_WM_ERASEBKGND()
	ON_COMMAND(ID_EDIT_CLEAR, OnEditClear)
	ON_COMMAND(ID_EDIT_CLEAR_ALL, OnEditClearAll)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_COMMAND(ID_EDIT_FIND, OnEditFind)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_COMMAND(ID_EDIT_PASTE_SPECIAL, OnEditPasteSpecial)
	ON_COMMAND(ID_EDIT_REPLACE, OnEditReplace)
	ON_COMMAND(ID_EDIT_SELECT_ALL, OnEditSelectAll)
	ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
	ON_COMMAND(ID_EDIT_REDO, OnEditRedo)	
	ON_COMMAND(ID_EDIT_COL_INSERT_LEFT, OnEditColInsertLeft)
	ON_COMMAND(ID_EDIT_COL_INSERT_RIGHT, OnEditColInsertRight)
	ON_COMMAND(ID_EDIT_ROW_INSERT_TOP, OnEditRowInsertTop)
	ON_COMMAND(ID_EDIT_ROW_INSERT_BOTTOM, OnEditRowInsertBottom)
	ON_COMMAND(ID_EDIT_COL_REMOVE, OnEditColRemove)
	ON_COMMAND(ID_EDIT_ROW_REMOVE, OnEditRowRemove)
	ON_COMMAND(ID_EDIT_COL_WIDTH_BEST, OnEditColWidthBest)
	ON_COMMAND(ID_EDIT_ROW_HEIGHT_BEST, OnEditRowHeightBest)
	ON_COMMAND(ID_EDIT_JOIN_CELL_RANGE, OnEditJoinCellRange)
	ON_COMMAND(ID_EDIT_UNJOIN_CELL_RANGE, OnEditUnjoinCellRange)
	ON_COMMAND(ID_EDIT_JOIN_RANGE_COL, OnEditJoinRangeCol)
	ON_COMMAND(ID_EDIT_JOIN_RANGE_ROW, OnEditJoinRangeRow)
	ON_COMMAND(ID_EDIT_ALIGN_TOP, OnEditAlignTop)
	ON_COMMAND(ID_EDIT_ALIGN_MIDDLE, OnEditAlignMiddle)
	ON_COMMAND(ID_EDIT_ALIGN_BOTTOM, OnEditAlignBottom)
	ON_COMMAND(ID_EDIT_ALIGN_LEFT, OnEditAlignLeft)
	ON_COMMAND(ID_EDIT_ALIGN_CENTER, OnEditAlignCenter)
	ON_COMMAND(ID_EDIT_ALIGN_RIGHT, OnEditAlignRight)
	ON_COMMAND(ID_EDIT_FILL_COLOR, OnEditFillColor)
	ON_COMMAND(ID_EDIT_LINE_COMBO, OnEditLineCombo)
	ON_COMMAND(ID_SYMBOL_INSERT, OnSymbolInsert)
	ON_COMMAND(ID_SYMBOL_DEFINE, OnSymbolDefine)
	ON_COMMAND(ID_SYMBOL_REFRESH, OnSymbolRefresh)
	ON_UPDATE_COMMAND_UI(ID_SYMBOL_INSERT, OnUpdateSymbolInsert)
	ON_UPDATE_COMMAND_UI(ID_SYMBOL_DEFINE, OnUpdateSymbolDefine)
	ON_UPDATE_COMMAND_UI(ID_SYMBOL_REFRESH, OnUpdateSymbolRefresh)
	//}}AFX_MSG_MAP
	ON_CBN_SELENDOK(ID_EDIT_LINE_COMBO, OnEditLineCombo)
	ON_UPDATE_COMMAND_UI_RANGE(ID_EDIT_FIRST, ID_EDIT_LAST, OnUpdateEdit)
	ON_COMMAND_RANGE(ID_BORDER_1, ID_BORDER_8, OnBorderType)
	ON_UPDATE_COMMAND_UI_RANGE(ID_BORDER_1, ID_BORDER_8, OnUpdateBorderType)
	ON_UPDATE_COMMAND_UI(ID_CUSTOMIZE_SYMBOL, OnUpdateCustomizeSymbol)
	ON_COMMAND(ID_CUSTOMIZE_SYMBOL, OnCustomizeSymbol)
	ON_COMMAND(ID_SYMBOL_MANAGER, OnSymbolManager)

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CULView drawing

void CULView::OnUpdate()
{

}

void CULView::OnUpdateCell()
{

}

void CULView::OnDraw(CDC* pDC)
{
	CRect rect;
	GetClientRect(&rect);
	pDC->DPtoLP(&rect);
	DrawPage(pDC, rect);
	Draw(pDC, m_rcPage);
}

BOOL CULView::SetScrollPos32(int nBar, int nPos, BOOL bRedraw /* = TRUE */)
{
	SCROLLINFO si;
	si.cbSize = sizeof(SCROLLINFO);
	si.fMask = SIF_POS;
	si.nPos = nPos;
	return SetScrollInfo(nBar, &si, bRedraw);
}

int CULView::GetScrollPos32(int nBar, BOOL bGetTrackPos)
{
	SCROLLINFO si;
	si.cbSize = sizeof(SCROLLINFO);

	if (bGetTrackPos)
	{
		if (GetScrollInfo(nBar, &si, SIF_TRACKPOS))
			return si.nTrackPos;
	}
	else
	{
		if (GetScrollInfo(nBar, &si, SIF_POS))
			return si.nPos;
	}

	return 0;
}

void CULView::OnDrawList(CDC* pDC, CBCGPListCtrl* pList)
{
	CRect rect;
	GetClientRect(rect);
	pDC->DPtoLP(rect);

	DrawPage(pDC, rect);

	CRect rcList = m_rcPage;
	rcList.DeflateRect(PAGE_MARGIN, -PAGE_MARGIN, PAGE_MARGIN, -PAGE_MARGIN);
	rcList.top -= DEFAULT_TITLE_HEIGHT;
	pDC->LPtoDP(rcList);
	pList->MoveWindow(rcList);
	
	CRect rcDraw = m_rcPage;
	rcDraw.DeflateRect(PAGE_MARGIN, -PAGE_MARGIN, PAGE_MARGIN, -PAGE_MARGIN);
	Draw(pDC, rcDraw);
}

void CULView::Draw(CDC* pDC, LPRECT lpRect)
{
}

// lpRect在此函数外进行去除PageMargin的操作
void CULView::DrawTitle(CDC* pDC, LPRECT lpRect, LPCTSTR lpszTitle,
	int nTH /* = DEFAULT_TITLE_HEIGHT */)
{
	CRect rcTitle = lpRect;
//	rcTitle.top -= PAGE_MARGIN;
	rcTitle.bottom = rcTitle.top - nTH;
	//rcTitle.DeflateRect(PAGE_MARGIN, 0, PAGE_MARGIN, 0);
	pDC->MoveTo(rcTitle.left, rcTitle.bottom - DEFAULT_TITLE_HEIGHT / 2);
	pDC->LineTo(rcTitle.left, rcTitle.top);
	pDC->LineTo(rcTitle.right, rcTitle.top);
	pDC->LineTo(rcTitle.right, rcTitle.bottom - DEFAULT_TITLE_HEIGHT / 2);

	if (lpszTitle != NULL)
	{
		CFont* pOldFont = pDC->SelectObject(&g_font[ftT1]);
		pDC->Rectangle(rcTitle);
		pDC->DrawText(lpszTitle, rcTitle,
				DT_CENTER | DT_SINGLELINE | DT_VCENTER | DT_NOCLIP);
		pDC->SelectObject(pOldFont);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CULView diagnostics

#ifdef _DEBUG
void CULView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CULView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CULView message handlers

void CULView::OnChangeVisualStyle()
{
}

int CULView::CalcPageWidth()
{
	int nWidth = DEFAULT_PAGE_WIDTH;
	return nWidth;

	// 根据打印机设置纸宽
	if (!m_bPrinter820)
	{
	//	if (Gbl_ConfigInfo.m_Record.bAutoFit)
		{
			if (m_pPrintInfo && m_pPrintInfo->GetPrintDC())
				nWidth = m_pPrintInfo->GetPrinterPageSize().cx;
		}
	}
	
	return nWidth;
}

void CULView::DrawPage(CDC* pDC, CRect rect, COLORREF crPage /* = 0xFFFFFF */,
	COLORREF crBorder /* = 0 */)
{
	CBrush brBack(::GetSysColor(COLOR_APPWORKSPACE));
	pDC->FillRect(&rect, &brBack);

	CBrush brPage(crPage);
	m_rcPage = rect;

	m_rcPage.top = -PAGE_MARGIN;
	m_rcPage.bottom = m_rcPage.top - CalcPageHeight();
	m_rcPage.left = PAGE_MARGIN;
	m_rcPage.right = m_rcPage.left + CalcPageWidth();

	CRect rcShade = m_rcPage;	
	rcShade.InflateRect(-8, 8, 8, -8);
	CBrush brShade(RGB(0, 0, 0));
	pDC->FillRect(&rcShade, &brShade);

	CPen pen(PS_SOLID, 1, crBorder);
	CPen* pOldPen = pDC->SelectObject(&pen);
	CBrush* pOldBrush = pDC->SelectObject(&brPage);
	pDC->Rectangle(m_rcPage);
	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
}

int CULView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetScrollInformation();
	return 0;
}

void CULView::SetScrollInformation()
{
	CSize sizeTotal;
	sizeTotal.cx = CalcPageWidth() + 2 * PAGE_MARGIN;
	sizeTotal.cy = CalcPageHeight() + 2 * PAGE_MARGIN;
	if ((m_totalLog.cx == sizeTotal.cx) && (m_totalLog.cy == sizeTotal.cy))
		return;

	CSize sizePage;
	sizePage.cx = 5 * PAGE_MARGIN;
	sizePage.cy = 5 * PAGE_MARGIN;

	CSize sizeLine;
	sizeLine.cx = PAGE_MARGIN;
	sizeLine.cy = PAGE_MARGIN;

	try
	{
		SetScrollSizes(MM_LOMETRIC, sizeTotal, sizePage, sizeLine);
	}
	catch (...)
	{
	}
}

BOOL CULView::OnEraseBkgnd(CDC* pDC)
{
	// TODO: Add your message handler code here and/or call default

	return TRUE;
}

void CULView::OnEditClear()
{
}

void CULView::OnEditClearAll()
{
	if (m_pEdit->GetSafeHwnd())
	{
		m_dwEdit |= EC_UNDO;
		long lStartRow, lEndRow, lStartCol, lEndCol;
		m_pEdit->GetSelectRange(&lStartCol, &lStartRow, &lEndCol, &lEndRow);
		m_pEdit->ClearArea(lStartCol, lStartRow, lEndCol, lEndRow, 0, 32);
	}
}

void CULView::OnEditCopy()
{
	if (m_pEdit->GetSafeHwnd())
	{
		m_dwEdit |= EC_UNDO;
		long lStartRow, lEndRow, lStartCol, lEndCol;
		m_pEdit->GetSelectRange(&lStartCol, &lStartRow, &lEndCol, &lEndRow);
		m_pEdit->CopyRange(lStartCol, lStartRow, lEndCol, lEndRow);
	}
}

void CULView::OnEditCut()
{
	if (m_pEdit->GetSafeHwnd())
	{
		m_dwEdit |= EC_UNDO;
		long lStartRow, lEndRow, lStartCol, lEndCol;
		m_pEdit->GetSelectRange(&lStartCol, &lStartRow, &lEndCol, &lEndRow);
		m_pEdit->CutRange(lStartCol, lStartRow, lEndCol, lEndRow);
	}
}

void CULView::OnEditFind()
{
	if (m_pEdit->GetSafeHwnd())
	{
		long col = m_pEdit->GetCurrentCol();
		long row = m_pEdit->GetCurrentRow();
		CString strFind = m_pEdit->GetCellString(col, row, 0);
		m_pEdit->FindDialogEx(0, strFind);
	}
}

void CULView::OnEditPaste()
{
	if (m_pEdit->GetSafeHwnd())
	{
		m_dwEdit |= EC_UNDO;
		long lStartRow, lEndRow, lStartCol, lEndCol;
		m_pEdit->GetSelectRange(&lStartCol, &lStartRow, &lEndCol, &lEndRow);
		m_pEdit->Paste(lStartCol, lStartRow, 0, 1, 1);
	}
}

void CULView::OnEditPasteSpecial()
{
}

void CULView::OnEditReplace()
{
	if (m_pEdit->GetSafeHwnd())
	{
		long col = m_pEdit->GetCurrentCol();
		long row = m_pEdit->GetCurrentRow();
		CString strFind = m_pEdit->GetCellString(col, row, 0);
		m_pEdit->ReplaceDialog(strFind, "");
	}
}

void CULView::OnEditSelectAll()
{
	if (m_pEdit->GetSafeHwnd())
	{
		if (m_dwEdit & EC_SALL)
			m_dwEdit &= ~EC_SALL;
		else
			m_dwEdit |= EC_SALL;

		long lCols = m_pEdit->GetCols(0);
		long lRows = m_pEdit->GetRows(0); 
		if (m_dwEdit & EC_SALL)
			m_pEdit->SelectRange(1, 1, lCols - 1, lRows - 1);
		else
			m_pEdit->SelectRange(1, 1, 0, 0);

		m_pEdit->Invalidate();
	}
}

void CULView::OnEditUndo()
{
	if (m_pEdit->GetSafeHwnd())
	{
		m_dwEdit |= EC_REDO;
		m_dwEdit &= ~EC_UNDO;
		m_pEdit->Undo();
	}
}

void CULView::OnEditRedo()
{
	if (m_pEdit->GetSafeHwnd())
	{
		m_dwEdit |= EC_UNDO;
		m_dwEdit &= ~EC_REDO;
		m_pEdit->Redo();
	}
}

void CULView::OnBorderType(UINT nID)
{
	ASSERT(m_pEdit->GetSafeHwnd() != NULL);
	long lStartRow, lEndRow, lStartCol, lEndCol;
	m_pEdit->GetSelectRange(&lStartCol, &lStartRow, &lEndCol, &lEndRow);
	m_pEdit->DrawGridLine(lStartCol, lStartRow, lEndCol, lEndRow,
				nID - ID_BORDER_1, m_nLineStyle, -1);
}

void CULView::OnUpdateBorderType(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_pEdit->GetSafeHwnd() ? TRUE : FALSE);
}

/////////////////////////////////////////////////////////////////////////////
// CULView printing

void CULView::Print()
{
	CDC* pDC = &m_pPrintInfo->m_printDC;
	CRect rect = m_pPrintInfo->SetPageInfo();
	rect.top = -rect.top;
	rect.bottom = -rect.bottom;
	rect.DeflateRect(PAGE_MARGIN, 0, PAGE_MARGIN, 0); 

	CRect rcPage0 = rect;	
	int nPagesHeight = abs(m_rcPage.Height());

	nPagesHeight = CalcPageHeight();

	int nPageHeight = abs(rcPage0.Height());
	int nPageCount = (nPagesHeight - 1) / nPageHeight + 1;
	CRect rcPrint = rect;
	rcPrint.top -= PAGE_MARGIN;
	rcPrint.bottom = rcPrint.top - nPagesHeight;

// 	CRect rcMem = rcPrint;
// 	rcMem.bottom = -nPageHeight;
		
	for (int i = 0; i < nPageCount; i++)
	{			
		pDC->StartPage();
		pDC->SaveDC();
		pDC->SetMapMode(MM_LOMETRIC);
		pDC->SetWindowOrg(0, i * rcPage0.Height());
		
		/*{
			CMyMemDC dc(pDC, &rcMem, -1);
			m_pPrintInfo->m_pMemDC = &dc;
			Print(&dc, rcPrint);
			m_pPrintInfo->m_pMemDC = NULL;
		}*/
		
		Print(&m_pPrintInfo->m_printDC, rcPrint);
		pDC->RestoreDC(-1);
		pDC->EndPage();

// 		rcMem.top = rcMem.bottom;
// 		rcMem.bottom -= nPageHeight;
	}
}

void CULView::Print(CDC* pDC, LPRECT lpRect)
{
	if (m_pEdit->GetSafeHwnd())
	{
		CRect rc = lpRect;
		PrintCell(m_pEdit, pDC, rc);
	}
	else
		Draw(pDC, lpRect);
}

void CULView::PrintBlank(long lHeight)
{
	if (m_pPrintInfo != NULL)
		m_pPrintInfo->PrintHollowPage(lHeight);
}

void CULView::PrintLogInfo(CULDoc* pDoc, CGraphWnd* pGraph,
	DWORD dwStyle /* = 0 */)
{
	if (pDoc == NULL)
		return ;

	if (pGraph == NULL)
	{
		pGraph = pDoc->GetGraphWnd();
		if (pGraph == NULL)
			return ;
	}

	if (pDoc->m_pDCurve == NULL)
		return ;

	int nCount = pDoc->m_pDCurve->GetDataSize();
	long lDepth0 = 0;
	long lDepth1 = 0;
	if (nCount > 0)
	{
		lDepth0 = pDoc->m_pDCurve->GetDepth(0);
		lDepth1 = pDoc->m_pDCurve->GetDepth(nCount - 1);
	}

	m_pPrintInfo->m_printDC.StartPage();
	int nMode = m_pPrintInfo->m_printDC.SetMapMode(MM_LOMETRIC);
	m_pPrintInfo->m_printDC.SetWindowOrg(0, 0);

	CRect rcInfo;
	CPen pen(PS_SOLID, 2, RGB(0, 0, 0));
	CPen* pOldPen = m_pPrintInfo->m_printDC.SelectObject(&pen);
	m_pPrintInfo->SetPageSize(TRUE);

	CRect rcPage = m_pPrintInfo->GetPageInfo();
	if (!pGraph->m_bLoggingPrint)
		m_pPrintInfo->SetPrintDirection(SCROLL_DOWN);

	int nTrack = pGraph->m_pSheet->m_TrackList.GetSize();
	rcInfo.top = 0;
	rcInfo.bottom = -3 * rcPage.Height();
	rcInfo.left = pGraph->m_pSheet->GetTrackRect(0).left;
	rcInfo.right = pGraph->m_pSheet->GetTrackRect(nTrack - 1).right;
	rcInfo = m_pPrintInfo->CoordinateConvert(rcInfo);

	// 打印小图头
	pGraph->DrawLogInfo(m_pPrintInfo->GetPrintDC(), rcInfo, pDoc, dwStyle);

	m_pPrintInfo->m_printDC.SelectObject(pOldPen);
	m_pPrintInfo->m_printDC.SetMapMode(nMode);
	m_pPrintInfo->m_printDC.EndPage();
	m_pPrintInfo->SetPageSize(pGraph->m_bLoggingPrint);
}

void CULView::PrintCell(CCell2000* pCell)
{
	if (pCell->GetSafeHwnd() == NULL)
		return ;

	CString strFile = pCell->GetFileName();
	if (strFile.IsEmpty())
	{
		strFile.Format("Untitled-%d", m_nFileID++);
		pCell->SaveFile(TMP_FILE(strFile), 0);
	}

	CSize szPage = m_pPrintInfo->GetPrinterPageSize();
	pCell->SetPrinter(m_pPrintInfo->m_strDevice);
	
// 	if (m_bPrinter820)
// 		pCell->PrintSetCustomPaper(szPage.cx, szPage.cy, 0);
//	else
		pCell->PrintSetCustomPaper(szPage.cx, szPage.cy, 0);

	pCell->PrintSetAlign(1, 0);
	pCell->PrintPageBreak(0);
	pCell->PrintAdjustPageLen(0);
//	pCell->PrintPara(1, 0, 0, 0);

	InvalidateCell(pCell);

	CWnd* pParent = pCell->GetParent();
	if (pParent->GetSafeHwnd())
	{
		pParent->SetRedraw(FALSE);
	}
	CSize size = SetCellFitSize(pCell, TRUE);
	pCell->PrintSheet(FALSE, 0);
	if (pParent->GetSafeHwnd())
	{
		ResetCellSize(pCell);
		pParent->SetRedraw(TRUE);
	}

	AddPdfPrintInfo(size.cy, strFile);

	int nPos = strFile.ReverseFind('.');
	if (nPos >= 0)
		strFile = strFile.Left(nPos);
	nPos = strFile.ReverseFind('\\');
	if (nPos >= 0)
		strFile = strFile.Right(strFile.GetLength() - nPos - 1);
	m_pPrintInfo->m_strLastDoc = strFile;
}

void CULView::PrintCell(CString strCell)
{
	CString strFile = HTS_FILE(strCell);

	CCell2000 cell;
	cell.Create(NULL, SB_CTL, CRect(0, 0, 0, 0), theApp.m_pMainWnd,
			AFX_IDC_CELL);
	cell.EnableUndo(1);

	BYTE chName[10] =
	{
		0xBD, 0xFA, 0xD2, 0xB5, 0xC8, 0xED, 0xBC, 0xFE, 0x00, 0x00
	};
	cell.Login(CString(chName), "1101010373", "1300-0616-0122-3005");
	if (cell.OpenFile(strFile, "") > 0)
		PrintCell(&cell);
}

void CULView::DrawTextInRect(CDC* pDC, CString& strText, CRect& rect, UINT nFormat)
{
	CFont* pFont = pDC->GetCurrentFont();
	LOGFONT lf;
	pFont->GetLogFont(&lf);
	CStringArray arrText;
	int nPos = strText.Find('\n');
	int nWidth = rect.Width();
	if (lf.lfEscapement == 900 || lf.lfEscapement == 2700)
		nWidth = abs(rect.Height());
	while (nPos != -1)
	{
		CString str = strText.Left(nPos);
		CSize size =  pDC->GetTextExtent(str);
		if (size.cx > nWidth)
		{
			CString strA;
			CString strB;
            for (int i = 1; i < str.GetLength(); i++)
            {
				strA = str.Left(i);
				if (pDC->GetTextExtent(strA).cx > nWidth)
					break;
				strB = strA;
			}
            arrText.Add(strB);
			str = str.Right(str.GetLength() - strB.GetLength());
			arrText.Add(str);
		}
		else
		    arrText.Add(str);
		strText = strText.Right(strText.GetLength() - nPos - 1);
		nPos = strText.Find('\n');
	}
	if (strText.GetLength())
		arrText.Add(strText);
	
	int xPos = 0;
	int yPos = 0;
	int nLineCount = arrText.GetSize();
	pDC->IntersectClipRect(rect);
	for (int i = 0; i < nLineCount; i++)
	{
		CString strLineText = arrText.GetAt(i);
		CSize size = pDC->GetTextExtent(strLineText);
		ASSERT(size.cy > 0);
		if (lf.lfEscapement == 0)
		{
			xPos = rect.left;
			if (nFormat & DT_RIGHT)
				xPos = rect.right - size.cx;
			else if (nFormat & DT_CENTER)
				xPos = rect.left + (rect.Width() - size.cx) / 2;
	
			yPos = (rect.Height() > 0) ? (rect.top + size.cy * i) : (rect.top - size.cy * i);
			if (nFormat & DT_VCENTER)
			{
			
				if (rect.Height() > 0)
					yPos = rect.top + (rect.Height() - (size.cy * nLineCount)) / 2 + size.cy * i;
				else
					yPos = rect.top + (rect.Height() + (size.cy * nLineCount)) / 2 - size.cy * i;
			}
			else if (nFormat & DT_BOTTOM)
			{
				yPos = (rect.Height() > 0) ? (rect.bottom - size.cy * (nLineCount - i)) :
			                                     (rect.bottom + size.cy * (nLineCount - i));
			}
		}
		else if (lf.lfEscapement == 900)
		{
		
			xPos = rect.left + (nLineCount - i) * size.cy;
			if (nFormat & DT_RIGHT)
				xPos = rect.right - i * size.cy;
			else if (nFormat & DT_CENTER)
				xPos = rect.left + (rect.Width() - size.cy * nLineCount) / 2 + 
				                         (nLineCount - i) * size.cy;
		
			yPos = rect.top;
			if (nFormat & DT_VCENTER)
			{
			
				if (rect.Height() > 0)
					yPos = rect.top + (rect.Height() - size.cx) / 2;
				else
					yPos = rect.top + (rect.Height() + size.cx) / 2;
			}
			else if (nFormat & DT_BOTTOM)
			{
				yPos = (rect.Height() > 0) ? (rect.bottom - size.cx) :
			                                     (rect.bottom + size.cx);
			}
		}
		pDC->TextOut(xPos, yPos, strLineText);
	}
	pDC->SelectClipRgn(NULL);
}

void CULView::PrintCellImage(CCell2000* pCell, CDC* pDC, long col,
	long row, long nSheet, LPRECT lpRect)
{
	long lImageIndex = -1, lStyle = 0, lHAlign = 0, lVAlign = 0;
	pCell->GetCellImage(col, row, nSheet, &lImageIndex, &lStyle, &lHAlign,
			&lVAlign);
	if (lImageIndex == -1)
		return;

	CString strImage;
	strImage.Format(_T("%s%d.jpg"), m_strTempPath, lImageIndex);
	CPicture pic;
	if (pic.LoadPicture(strImage) == NULL)
		return;

	int bmpWidth = pic._GetWidth();
	int bmpHeight = pic._GetHeight();

	CSize bmpSize(bmpWidth, bmpHeight);
		
	{
		CWindowDC dc(NULL);
		int nMM = dc.SetMapMode(MM_LOMETRIC);
		dc.DPtoLP(&bmpSize);
		dc.SetMapMode(nMM);
	}

	bmpWidth = bmpSize.cx;
	bmpHeight = bmpSize.cy;

	CRect rcCell = lpRect;
	CRect rcMergedCell = lpRect;

	int lMergeHeight = rcMergedCell.Height();
	int lMergeWidth = rcMergedCell.Width();

	if ((lStyle & 1) > 0) // Auto size
	{
		double dVS = (double) (lMergeHeight - 8) / bmpHeight;
		double dHS = (double) (lMergeWidth - 8) / bmpWidth;
		double dS = min(dVS, dHS);
		bmpWidth *= (int) dS;
		bmpHeight *= (int) dS;
		rcMergedCell.DeflateRect(2, -2, 2, -2);
		pic.DrawPicture(pDC->GetSafeHdc(), rcMergedCell.left,
				rcMergedCell.top, rcMergedCell.Width(), rcMergedCell.Height());
	}
	else if ((lStyle & 4) > 0) // Tile
	{
	}
	else
	{
		CRect rcImage(rcCell.left + 4, rcCell.bottom - 4,
			rcCell.left + bmpWidth, rcCell.bottom - bmpHeight);

		if (lHAlign == 2) // Center
		{
			rcImage.left = rcCell.left + (lMergeWidth - bmpWidth - 8) / 2;
			rcImage.right = rcImage.left + bmpWidth;
		}
		else if (lHAlign == 3) // Right
		{
			rcImage.right = rcCell.left + lMergeWidth - 4;
			rcImage.left = rcImage.right - bmpWidth;
		}

		if (lVAlign == 2)
		{
			rcImage.top = rcCell.bottom -
				(lMergeHeight - bmpHeight - 8) /
				2 -
				4;
			rcImage.bottom = rcImage.top - bmpHeight;
		}
		else if (lVAlign == 3)
		{
			rcImage.bottom = rcCell.bottom - lMergeHeight + 8;
			rcImage.top = rcImage.bottom + bmpHeight;
		}

		pic.DrawPicture(pDC->GetSafeHdc(), rcImage.left, rcImage.top, rcImage.Width(),
				rcImage.Height());
	}
}

//  cell表自主打印
void CULView::PrintCell(CCell2000* pCell, CDC* pDC, CRect& rect)
{
	if (pCell->GetSafeHwnd() == NULL)
		return;

	double fCellWidth = rect.Width();
	double fCellHeight = abs(rect.Height());

	int nSheet = pCell->GetCurSheet();
	int nCols = pCell->GetCols(nSheet);
	int nRows = pCell->GetRows(nSheet);
	
	// Calculate column width and row hight
	double* fWidth = new double[nCols];
	double fWidths = 0;
	int i = 1;
	for ( ; i < nCols; i++)
	{
		fWidth[i] = (short)pCell->GetColWidth(0, i, nSheet);
		if (fWidth[i] < 0)
			fWidth[i] = 0;
		fWidths += fWidth[i];
	}
	if (fWidths < 1)
	{
		delete fWidth;
		return;
	}
	
	int* nWidth = new int[nCols];
	double fD = 0.0;	//  increase
	int nD = 1;			// range
	
	for (/*int*/ i = 1; i < nCols; i++)
	{
		fWidth[i] = fWidth[i]/fWidths * fCellWidth;
		nWidth[i] = (int)floor(fWidth[i]);
		fD += (fWidth[i] - (double)nWidth[i]);
		if (fD >= nD)
		{
			nWidth[i]++;
			nD += 1;
		}
	}
	
	double* fHeight = new double[nRows];
	double fHeights = 0;
	for (/*int*/ i = 1; i < nRows; i++)
	{
		fHeight[i] = (short)pCell->GetRowHeight(0, i, nSheet);
		if (fHeight[i] < 0)
			fHeight[i] = 0;
		fHeights += fHeight[i];
	}
	if (fHeights < 1)
	{
		delete fWidth;
		delete nWidth;
		delete fHeight;
		return;
	}
	
	int* nHeight = new int[nRows];
	fD = 0.0;	// increase
	nD = 1;			// range
	
	for (/*int*/ i = 1; i < nRows; i++)
	{
		fHeight[i] = fHeight[i] / fHeights * fCellHeight;
		nHeight[i] = (int)floor(fHeight[i]);
		fD += (fHeight[i] - (double)nHeight[i]);
		if (fD >= nD)
		{
			nHeight[i]++;
			nD += 1;
		}
     }

	// Save images first
	m_strTempPath = GetTempPathSK();

	CList<long,long> imageIndex;
	for ( i = 1; i < nRows; i++)
	{
		for (int j = 1; j < nCols; j++)
		{
			long lIndex = pCell->GetCellImageIndex(j, i, nSheet);
			if (lIndex < 0)
				continue;

			if (!imageIndex.Find(lIndex))
				imageIndex.AddTail(lIndex);
		}
	}

	long iImage = 0;
	CString strImage;
	for (POSITION pos = imageIndex.GetHeadPosition(); pos != NULL;)
	{
		iImage = imageIndex.GetNext(pos);
		strImage.Format(_T("%s%d.jpg"), m_strTempPath, iImage);
		try
		{
			pCell->SaveImageFile(iImage, strImage);
		}
		catch (CException &e)
		{
			e.Delete();
		}
		catch (...) 
		{
		}
	}

	CFont* pOldFont = pDC->GetCurrentFont();
	CPen* pOldPen = pDC->GetCurrentPen();
	COLORREF clrText = pDC->GetTextColor();
	COLORREF clrBK = pDC->GetBkColor();

	CRect rcCell(rect.left, 0, 0, rect.top);
	for (/*int*/ i = 1; i < nRows; i++)
	{
		rcCell.top = rcCell.bottom - nHeight[i];
		rcCell.left = rect.left;
		for (int j = 1; j < nCols; j++)
		{
			int nBackColorIndex = pCell->GetCellBackColor(j, i, nSheet);
			COLORREF crBack = pCell->GetColor(nBackColorIndex);

			rcCell.right = rcCell.left + nWidth[j];

			long lStartRow, lEndRow, lStartCol, lEndCol;
			pCell->GetMergeRange(j, i, &lStartCol, &lStartRow, &lEndCol,
					&lEndRow);

			if (lEndCol >= nCols)
				lEndCol = nCols-1;
			if (lEndRow >= nRows)
				lEndRow = nRows-1;

			// 画格子线
			CPoint pt[5] =
			{
				CPoint(rcCell.left, rcCell.top),
				CPoint(rcCell.left, rcCell.bottom),
				CPoint(rcCell.right, rcCell.bottom),
				CPoint(rcCell.right, rcCell.top),
				CPoint(rcCell.left, rcCell.top)
			};

			long lMergeWidth = 0; // 合并区域的打印宽度
			long lMergeHeight = 0;

			for (int x = lStartRow; x <= lEndRow; x++)
			{
				lMergeHeight += nHeight[x];
			}
			for (int y = lStartCol; y <= lEndCol; y++)
			{
				lMergeWidth += nWidth[y];
			}
			CRect rcMergedCell(rcCell.left, rcCell.bottom,
				rcCell.left + lMergeWidth, rcCell.bottom - lMergeHeight);
			rcMergedCell.DeflateRect(1, -1, 1, -1);
			if (j == lStartCol && i == lStartRow)
			{
				int nIndex = pCell->GetCellBackColor(j, i, nSheet);  //背景颜色
				if (nIndex != -1)
				{
					COLORREF bClr = pCell->GetColor(nIndex);   
					if (bClr != RGB(255, 255, 255))
					{
						pDC->FillSolidRect(rcMergedCell, bClr);
					}
				}

				// 单元格图片
				PrintCellImage(pCell, pDC, j, i, nSheet, rcMergedCell);
			}

			for (int n = 0; n < 4; n++)
			{
				long lBorder = pCell->GetCellBorder(j, i, nSheet, n);		
				int nClrIndex = pCell->GetCellBorderClr(j, i, nSheet, n);
				COLORREF bClr = pCell->GetColor(nClrIndex);
				int nPenSize = lBorder + 1; //格线大小
#ifdef DrawCellBorder			
				if ((n == 0 && j == 1) ||
					(n == 1 && i == 1) ||
					(n == 2 && j == nCols - 1) ||
					(n == 3 && i == nRows - 1))
					nPenSize = PEN_BORDER_SIZE;	// 边界
				else 			
#endif
					if (lBorder == 0 || lBorder == 1) //没格子线
						continue;
					else if ((n == 0 && j != lStartCol) ||   // 合并区域的中间位置
						(n == 1 && i != lStartRow) ||
						(n == 2 && j != lEndCol) ||
						(n == 3 && i != lEndRow))
						continue;
					
					CPen pen(PS_SOLID, nPenSize, bClr);
					pDC->SelectObject(pen);
					pDC->MoveTo(pt[n]);
					pDC->LineTo(pt[n + 1]);
			}
			
			if (j == lStartCol && i == lStartRow)
			{
				CRect rcText = rcMergedCell;
				int nDataType = pCell->GetCellDataType(j, i, nSheet);
				if (nDataType > 0) // 字符串或数字
				{
					CString strText = pCell->GetCellString(j, i, nSheet);
					if (strText.GetLength())
					{
						//1: 居左 2: 居右 4: 水平居中 8: 居上 16: 居下 32: 垂直居中
						long lAlign = pCell->GetCellAlign(lStartCol, lStartRow,
												nSheet);
						UINT nFormat = DT_LEFT | DT_BOTTOM;  //cell默认为居左和居下
						if (lAlign & 2)
							nFormat |= DT_RIGHT;
						if (lAlign & 4)
							nFormat |= DT_CENTER;
						if (lAlign & 8)
							nFormat &= ~DT_BOTTOM;
						if (lAlign & 16)
							nFormat |= DT_BOTTOM;
						if (lAlign & 32)
							nFormat |= DT_VCENTER;

						//0 默认（水平方向） 1 水平方向 2 垂直向上 3 垂直向下
						long lOrient = pCell->GetCellTextOrientation(lStartCol,
												lStartRow, nSheet);
						LOGFONT logFont;
						g_font[ftR].GetLogFont(&logFont);
						long lFontIndex = pCell->GetCellFont(lStartCol, lStartRow,
													nSheet);

						CString strFontName;
						if (lFontIndex == 0)
							strFontName = pCell->GetDefaultFontName(); 
						else
							strFontName = pCell->GetFontName(lFontIndex);
						strcpy(logFont.lfFaceName, strFontName);

						if (lOrient == 2)
							logFont.lfEscapement = 2700;
						else if (lOrient == 3)
							logFont.lfEscapement = 900;

						long lSize = pCell->GetCellFontSize(lStartCol, lStartRow,
												nSheet);
						if (lSize == 0)
							lSize = pCell->GetDefaultFontSize();
						lSize -= 2;
						CSize szText;
						szText.cx = 0;
						szText.cy = -MulDiv(lSize,
										GetDeviceCaps(pDC->GetSafeHdc(),
											LOGPIXELSY),
										72);
						pDC->DPtoLP(&szText);
						logFont.lfHeight = szText.cy;
						long lStyle = pCell->GetCellTextStyle(lStartCol,
												lStartRow, nSheet);
						if (lStyle & 2) //粗体
							logFont.lfWeight = FW_BLACK;

						CFont font;
						font.CreateFontIndirect(&logFont);
						pDC->SelectObject(&font);
						pDC->SetBkMode(TRANSPARENT);
						long lCI = pCell->GetCellTextColor(lStartCol, lStartRow,
											nSheet);
						if (lCI != -1)
							pDC->SetTextColor(pCell->GetColor(lCI));
						else
							pDC->SetTextColor(RGB(0, 0, 0));
						DrawTextInRect(pDC, strText, rcText, nFormat);
					}
				}
			}

			rcCell.left = rcCell.right;
		} // 列循环结束	
		rcCell.bottom = rcCell.top;
	}// 行循环结束
	rect.bottom = rcCell.bottom;
	/*	CPen pen(PS_SOLID, PEN_BORDER_SIZE, RGB(0, 0, 0));  //画边框
		CPen* pOldPen = pDC->SelectObject(&pen);
		pDC->SelectStockObject(NULL_BRUSH);
		pDC->Rectangle(rect);*/
	pDC->SelectObject(pOldFont);
	pDC->SelectObject(pOldPen);
	pDC->SetTextColor(clrText);
	pDC->SetBkColor(clrBK);

	delete fWidth;
	delete nWidth;
	delete fHeight;
	delete nHeight;
}

void CULView::PrintList(CDC* pDC, LPRECT lpRect, CBCGPListCtrl* pList,
	UINT nID, int nTitleHeight /* = 300 */)
{
	// Print title
	if (nTitleHeight > 0)
	{
		pDC->StartPage();
		pDC->SaveDC();
		pDC->SetMapMode(MM_LOMETRIC);
		pDC->SetWindowOrg(0, 0);
		if (nID == 0)
		{
		}
		else
		{
			CString strTitle;
			strTitle.LoadString(nID);
			DrawTitle(pDC, lpRect, strTitle, nTitleHeight);
		}
		pDC->RestoreDC(-1);
		pDC->EndPage();
	}

	CHeaderCtrl& header = pList->GetHeaderCtrl();
	int nCols = header.GetItemCount();
	if (nCols < 1)
		return ;

	CArray<int, int> wArray;
	int iWidth = 0;

	CRect rectItem;
	int i;
	for (i = 0; i < nCols; i++)
	{
		header.GetItemRect(i, rectItem);
		wArray.Add(rectItem.Width());
		iWidth += rectItem.Width();
	}

	int iHeight = 46;

	// Print first page
	CFont* pOldFont = pDC->SelectObject(&g_font[ftR]);
	pDC->SetBkMode(TRANSPARENT);

	pDC->StartPage();
	pDC->SaveDC();
	pDC->SetMapMode(MM_LOMETRIC);
	pDC->SetWindowOrg(0, 0);

	CRect rcPage = lpRect;
	rcPage.DeflateRect(PAGE_MARGIN, 0, PAGE_MARGIN, 0);

	double dK = (double) rcPage.Width() / (double) iWidth;

	CRect* rcItem = new CRect[nCols];
	rcItem[0] = rcPage;
	rcItem[0].right = rcItem[0].left + floor(wArray[0] * dK + .5);
	rcItem[0].bottom = rcItem[0].top - iHeight;

	UINT nFormat = DT_CENTER | DT_NOCLIP | DT_VCENTER | DT_SINGLELINE;
	HD_ITEM hdItem;
	memset(&hdItem, 0, sizeof(hdItem));
	hdItem.mask = HDI_FORMAT | HDI_BITMAP | HDI_TEXT | HDI_IMAGE;

	TCHAR szText[256];
	hdItem.pszText = szText;
	hdItem.cchTextMax = 255;

	CFont* normalFont, blackFont;
	normalFont = pDC->GetCurrentFont();
	LOGFONT logFont;
	normalFont->GetLogFont(&logFont);
	logFont.lfWeight = FW_BLACK;
	blackFont.CreateFontIndirect(&logFont);
	pDC->SelectObject(&blackFont);
	if (header.GetItem(0, &hdItem))
	{
		CString strText = hdItem.pszText;
		if (rcItem[0].Width() > 0)
			pDC->DrawText(strText, rcItem[0], nFormat);
	}

	for (i = 1; i < nCols; i++)
	{
		rcItem[i] = rcItem[i - 1];
		rcItem[i].left = rcItem[i - 1].right;
		rcItem[i].right = rcItem[i].left + floor(wArray[i] * dK + .5);

		memset(&hdItem, 0, sizeof(hdItem));
		hdItem.mask = HDI_FORMAT | HDI_BITMAP | HDI_TEXT | HDI_IMAGE;
		hdItem.pszText = szText;
		hdItem.cchTextMax = 255;

		if (!header.GetItem(i, &hdItem))
			continue ;

		CString strText = hdItem.pszText;
		pDC->DrawText(strText, rcItem[i], nFormat);
	}
	pDC->SelectObject(normalFont);
	pDC->MoveTo(rcItem[0].left, rcItem[0].top);
	pDC->LineTo(rcItem[nCols - 1].right, rcItem[0].top);
	pDC->MoveTo(rcItem[0].left, rcItem[0].bottom);
	pDC->LineTo(rcItem[nCols - 1].right, rcItem[0].bottom);

	int nRows = pList->GetItemCount();
	if (nRows == 0)
	{
		int nX = rcItem[0].left;
		for (i = 0; i <= nCols; i++)
		{
			pDC->MoveTo(nX, rcItem[0].top);
			pDC->LineTo(nX, rcItem[0].bottom);
			nX = rcItem[i].right;
		}

		pDC->RestoreDC(-1);
		pDC->EndPage();

		pDC->SelectObject(pOldFont);
		return ;
	}

	pList->GetItemRect(0, rectItem, LVIR_BOUNDS);
	pDC->DPtoLP(rectItem);
	int nRowHeight = iHeight;
	int nItems = (abs(rcPage.Height()) + 1) / nRowHeight - 1;
	if (nItems == 0)
		return ;

	int iItem;
	for (iItem = 0; iItem < nRows; iItem++)
	{
		for (i = 0; i < nCols; i++)
		{
			rcItem[i].top = rcItem[i].bottom;
			rcItem[i].bottom -= nRowHeight;
			CString str = pList->GetItemText(iItem, i);
			if (str.IsEmpty())
				continue ;

			pDC->DrawText(str, rcItem[i], nFormat);
		}

		pDC->MoveTo(rcItem[0].left, rcItem[0].bottom);
		pDC->LineTo(rcItem[nCols - 1].right, rcItem[0].bottom);

		if ((iItem + 1) % nItems == 0)
		{
			int nX = rcPage.left;
			for (i = 0; i <= nCols; i++)
			{
				pDC->MoveTo(nX, rcPage.top);
				pDC->LineTo(nX, rcItem[0].bottom);
				nX = rcItem[i].right;
			}

			pDC->RestoreDC(-1);
			pDC->EndPage();

			if (iItem < nRows)
			{
				rcPage.top = 0;
				pDC->StartPage();
				pDC->SaveDC();
				pDC->SetMapMode(MM_LOMETRIC);
				pDC->SetWindowOrg(0, 0);
				for (i = 0; i < nCols; i++)
					rcItem[i].bottom = 0;
			}
		}
	}

	if (iItem % nItems != 0)
	{
		int nX = rcPage.left;
		for (i = 0; i <= nCols; i++)
		{
			pDC->MoveTo(nX, rcPage.top);
			pDC->LineTo(nX, rcItem[0].bottom);
			nX = rcItem[i].right;
		}

		pDC->RestoreDC(-1);
		pDC->EndPage();
	}

	pDC->SelectObject(pOldFont);
}

void CULView::PrintCalReport(CProject* pProject)
{
	ASSERT(pProject != NULL);
#ifndef _LOGIC
	CCalSelectDlg dlgCalSel(&pProject->m_ULKernel.m_arrTools);
	if (dlgCalSel.DoModal() == IDOK)
	{
		int nTool = dlgCalSel.m_ToolArray.GetSize();
		if (nTool < 1)
			return ;

		for (int i = 0; i < nTool; i++)
		{
			CULTool* pTool = dlgCalSel.m_ToolArray.GetAt(i);
			if (pTool == NULL)
				continue ;

			CString strCalPhase = dlgCalSel.m_TypeArray.GetAt(i);
			CString str(MAKEINTRESOURCE(IDS_COMPONENT_TOOL));
			CString strToolName = pTool->strToolName;

			CReportSetDlg dlgRS;
			dlgRS.m_strTool = str + strToolName + ":";

			if (dlgRS.DoModal() == IDOK)
			{
				UINT uPhase;
				if (strCalPhase == "Master")
					uPhase = 0;
				if (strCalPhase == "Before")
					uPhase = 1;
				if (strCalPhase == "After")
					uPhase = 2;
				if (strCalPhase == "Primary")
					uPhase = 3;

				if (uPhase == pTool->m_uCalPhase)
				{
					pTool->m_dwReport = dlgRS.m_bGraph +
						dlgRS.m_bCurve * 2 +
						dlgRS.m_bData * 4;

					PrintCalReport(pTool, uPhase);
				}
			}
		}
	}
#endif
}

void CULView::PrintCalReport(CULTool* pTool, UINT uPhase)
{
#ifndef _LOGIC
	BOOL bDirection = TRUE;

	CSize szPage;
	szPage.cx = m_pPrintInfo->m_printDC.GetDeviceCaps(HORZSIZE) * 10;
	szPage.cy = m_pPrintInfo->m_printDC.GetDeviceCaps(VERTSIZE) * 10;

	switch (pTool->m_nCalType[uPhase])
	{
	case UL_CAL_USER:
		{
			// User calibrate print
			pTool->CalUserPrint(&m_pPrintInfo->m_printDC, szPage, uPhase);
		}
		break;
	case UL_CAL_DEFAULT:
		{
			if (pTool->CalSysPrint(&m_pPrintInfo->m_printDC, szPage, uPhase))
			{
				CString str = m_pPrintInfo->EndPrintDoc();
				CCalReport Report(pTool, bDirection);
				Report.PrintReport(uPhase);
				if (str.GetLength())
					m_pPrintInfo->StartPrintDoc(IDS_LOGDATAGRAPH);
			}
		}
		break;
	default:
		break;
	}
#endif
}

/////////////////////////////////////////////////////////////////////////////
// CULView saving to bitmap

BOOL CULView::BeginCapture()
{
	if (m_pMainWnd->IsFullScreen())
	{
		//m_pMainWnd-ShowFullScreenBar(FALSE);
		m_pMainWnd->ShowFullScreen();
		// m_pMainWnd->UpdateCapture();
		return TRUE;
	}

	//m_pMainWnd->ShowFullScreen(FALSE);
	m_pMainWnd->ShowFullScreen();
	return FALSE;
}

void CULView::CaptureWnd(CMyMemDC* pDC, CWnd* pWnd, CRect& rect, int nPos /* = 0 */)
{
	ASSERT(m_pMainWnd != NULL);
	ASSERT(pWnd != NULL);

	CRect rcWnd;
	if (nPos == 0)
	{	
		pWnd->GetWindowRect(&rcWnd);
		ScreenToClient(&rcWnd);
		pDC->m_pDC->SetWindowOrg(0, 0);
		OnPrepareDC(pDC->m_pDC);
		pDC->m_pDC->DPtoLP(&rcWnd);
		//rcWnd.top++;
		//rcWnd.bottom++;
	}
	else
	{
		rcWnd.top = nPos;
	}

	ScrollToPositionEx(CPoint(0, rcWnd.top));
	m_pMainWnd->RedrawAll();
	
	CClientDC dc(this);
	OnPrepareDC(&dc);

	CRect rc;
	GetClientRect(rc);
	int nDPOffset = abs(rc.Height());
	dc.DPtoLP(&rc);
	int nLPOffset = abs(rc.Height());
	
	pWnd->GetWindowRect(rc);
	CRect rcCapture(0, 0, rc.Width(), nDPOffset);
	int nBottom = rect.bottom;
	rect.bottom = rect.top - nLPOffset;
	rcWnd.bottom = rcWnd.top - nLPOffset;

	while (nBottom < rect.bottom)
	{
		WriteWindowToDC(pDC, rect, pWnd, rcCapture);
		rect.top = rect.bottom;
		rect.bottom = rect.top - nLPOffset;
		
		rcWnd.top = rcWnd.bottom;
		rcWnd.bottom = rcWnd.top - nLPOffset;

		ScrollToPositionEx(CPoint(0, rcWnd.top));
		m_pMainWnd->RedrawAll();
		
		rcCapture.OffsetRect(0, nDPOffset);
	}
	
	rect.bottom = nBottom;
	rcCapture.bottom = abs(rc.Height());
	WriteWindowToDC(pDC, rect, pWnd, rcCapture);
}

void CULView::EndCapture(BOOL bFullScreen /* = FALSE */)
{
	//if (bFullScreen)
	//	m_pMainWnd->>ShowFullScreenBar();
	//else
		m_pMainWnd->ShowFullScreen();
}

void CULView::SaveAsBitmap(CString strFile)
{
	FindWindow("Shell_TrayWnd", NULL)->ShowWindow(SW_HIDE);
	CDC* pDC = GetDC();
	int nMode = pDC->SetMapMode(MM_LOMETRIC);
	SaveAsBitmap(pDC, strFile);
	pDC->SetMapMode(nMode);
	ReleaseDC(pDC);
	FindWindow("Shell_TrayWnd", NULL)->ShowWindow(SW_SHOW);
}

void CULView::SaveAsBitmap(CDC* pDC, LPCTSTR pszFile)
{
	if (m_pEdit->GetSafeHwnd())
	{
		SetCellFitSize(m_pEdit);
		ResizeCell();
		SaveCellAsBitmap(pDC, pszFile, m_pEdit);	
	}
	else
	{
		CRect rect = m_rcPage;
		CMyMemDC dc(pDC, &rect);
		Draw(&dc, rect);
		WriteDCToDIB(pszFile, &dc);
	}
}

void CULView::SaveCellAsBitmap(CDC* pDC, LPCTSTR pszFile, CCell2000* pCell)
{
	pCell->SelectRange(-1, -1, -1, -1);
	pCell->SetLeftCol(0);
	pCell->SetTopRow(0);
	long  gridLines = pCell->GetGridLineState(0);
	if (gridLines)
		pCell->ShowGridLine(0, 0);

	CRect rect = m_rcPage;
	CMyMemDC dc(pDC, &rect);
	rect.DeflateRect(PAGE_MARGIN, -PAGE_MARGIN, PAGE_MARGIN, -PAGE_MARGIN);
	BOOL bFS = BeginCapture();
	pCell->ReDraw();
	CaptureWnd(&dc, pCell, rect);
	EndCapture(bFS);
	WriteDCToDIB(pszFile, &dc);

	if (gridLines)
		pCell->ShowGridLine(1, 0);
}

void CULView::SaveListAsBitmap(CDC* pDC, LPCTSTR pszFile, CBCGPListCtrl* pList)
{
	CRect rect = m_rcPage;
	CMyMemDC dc(pDC, &rect);
	Draw(&dc, rect);

	rect.DeflateRect(PAGE_MARGIN, -PAGE_MARGIN, PAGE_MARGIN, -PAGE_MARGIN);
	rect.top -= DEFAULT_TITLE_HEIGHT;

	BOOL bFS = BeginCapture();
	CaptureWnd(&dc, pList, rect);
	EndCapture(bFS);
	WriteDCToDIB(pszFile, &dc);
}

long CULView::GetCellWidth(CCell2000* pCell, long type /* = 0 */, long border /* = 0 */)
{
	long lWidth = 0;
	for (long i = 1; i < pCell->GetCols(0); i++)
		lWidth += pCell->GetColWidth(type, i, 0);

	if (border)
		lWidth += 2;
	return (int) abs(lWidth);
}

long CULView::GetCellHeight(CCell2000* pCell, long type /* = 0 */)
{
	long lHeight = 0;
	for (long i = 1; i < pCell->GetRows(0); i++)
		lHeight += pCell->GetRowHeight(type, i, 0);

	return (int) abs(lHeight);
}

CPoint CULView::GetCellPos(CCell2000* pCell, long col, long row,
	long type /* = 0 */, CPoint ptTopLeft /* = CPoint */)
{
	row = min(row, pCell->GetRows(0));
	col = min(col, pCell->GetCols(0));

	CPoint point = ptTopLeft;
	long i;
	for (i = 1; i < col; i++)
		point.x += pCell->GetColWidth(1, i, 0);

	for (i = 1; i < row; i++)
		point.y += pCell->GetRowHeight(1, i, 0);

	if (type == 1)
		return point;

	CWnd* pWnd = pCell->GetParent();
	if (pWnd == NULL)
		return point;

	CDC* pDC = pWnd->GetDC();
	int nMode = pDC->SetMapMode(MM_LOMETRIC);
	pDC->DPtoLP(&point);
	pDC->SetMapMode(nMode);
	pWnd->ReleaseDC(pDC);
	return point;
}

CSize CULView::GetCellSize(CCell2000* pCell, long type /* = 0 */,
	CSize szBorder /* = CSize */)
{
	// 	szBorder.cx = pCell->GetBorder()*2;
	// 	szBorder.cy = pCell->GetBorder()*2;

	CSize size;
	size.cx = GetCellWidth(pCell, 1) + szBorder.cx;
	size.cy = GetCellHeight(pCell, 1) + szBorder.cy;
	if (type == 1)
		return size;

	CWnd* pWnd = pCell->GetParent();
	if (pWnd == NULL)
		return size;

	CDC* pDC = pWnd->GetDC();
	int nMode = pDC->SetMapMode(MM_LOMETRIC);
	pDC->DPtoLP(&size);
	pDC->SetMapMode(nMode);
	pWnd->ReleaseDC(pDC);

	return size;
}

int GetMax(CArray<int, int>& n)
{
	int nSize = n.GetSize();
	if (nSize < 1)
		return -1;

	int nMax = 0;
	for (int i = 1; i < nSize; i++)
	{
		if (n[i] > n[nMax])
			nMax = i;
	}

	return nMax;
}

CSize CULView::SetCellFitSize(CCell2000* pCell, BOOL bPrint /* = FALSE */)
{
	pCell->SelectRange(-1, -1, -1, -1);
	double dScale = pCell->GetScreenScale(0);
	if (dScale != 1.0)
		pCell->SetScreenScale(0, 1.0);

	if (bPrint)
	{
		CSize size;
		size.cx = GetCellWidth(pCell, 0);
		size.cy = GetCellHeight(pCell, 0);
		
		double dCellWidth = size.cx; 
		double dPrintWidth = m_pPrintInfo->GetPrinterPageSize().cx;
		if (dPrintWidth > DEFAULT_PAGE_WIDTH)
			dPrintWidth = DEFAULT_PAGE_WIDTH;
		double dPageWidth = dPrintWidth - 2*PAGE_MARGIN;
		if (m_bPrinter820)
		{
			dPageWidth -= 4;
			if (pCell->GetBorder())
				dPageWidth += 2;
		}
		else
			dPageWidth += 4;
		
		pCell->PrintSetMargin(0, 0, 0, 0);
		pCell->PrintSetScale(1.0);
		double d = dPageWidth/dCellWidth;
		if (d == 1.0)
			return size;

		// Restore the widths for reset
		int nCols = pCell->GetCols(0);
		if (nCols < 1)
			return size;

		int i;
		for (i = 0; i < nCols; i++)
		{
			int w = pCell->GetColWidth(0, i, 0);
			CString string;
			string.Format("%d", w);
			pCell->SetCellNote(i, 0, 0, string);
		}

		// Adjust cell width to fit print
		double w = 0;
		double fD = 0.0;	// diff increasement
		int nD = 1;			// diff range
		CArray<int, int> nWidth;
		int nCols1 = 0;
		for (/*int*/ i = 0; i < nCols; i++)
		{
			nWidth.Add(pCell->GetColWidth(0, i, 0));
			if (nWidth[i] > 0)
				nCols1++;

			w = nWidth[i]*d;
			fD += (w - floor(w));
			if (fD > nD)
			{
				pCell->SetColWidth(0, floor(w) + 1, i, 0);
				nD += 1;
			}
			else
				pCell->SetColWidth(0, floor(w), i, 0);
		}

		size.cx = GetCellWidth(pCell);
		nD = dPageWidth - size.cx;

		// Divided by zero
		if (nCols1 == 0)
			nCols1 = nCols;
		int nA = (abs(nD) - 1)/nCols1 + 2;
		int nL = m_bPrinter820 ? 6 : 0;
		while (nD > nL)
		{
			int nMax = GetMax(nWidth);
			if (nMax < 0)
				break;
			
			w = pCell->GetColWidth(0, nMax, 0) + nA;
			if (w > nA)
			{
				pCell->SetColWidth(0, w, nMax, 0);
				if (m_bPrinter820)
					nD -= nA;
				else
					nD = dPageWidth - GetCellWidth(pCell);
			}
			nWidth.RemoveAt(nMax);
		}
		
		nL = m_bPrinter820 ? 2 : 0;
		while (nD < nL)
		{
			int nMax = GetMax(nWidth);
			if (nMax < 0)
				break;
			
			w = pCell->GetColWidth(0, nMax, 0) - nA;
			if (w > 1)
			{
				pCell->SetColWidth(0, w, nMax, 0);
				nD += nA;
			}
			nWidth.RemoveAt(nMax);
		}

		size.cx = dPageWidth - nD;
		return size;
	}

	CSize size = GetCellSize(pCell);
	if (size.cx == 0)
		return size;
	
	double dCellWidth = size.cx/* - 4*/;
	double dPageWidth = CalcPageWidth() - 2*PAGE_MARGIN/* - 4*/;
	if (pCell->GetBorder() == 0)
		dPageWidth += 4;
	double d = dPageWidth / dCellWidth;

	double w = 0;
	double fD = 0.0;
	int nD = 2;
	int nCols = pCell->GetCols(0);
	CArray<int, int> nWidth;
	int nCols1 = 0;
	for (int i = 0; i < nCols; i++)
	{
		nWidth.Add(pCell->GetColWidth(0, i, 0));
		if (nWidth[i] > 0)
			nCols1++;
		w = nWidth[i]*d;
		fD += (w - floor(w));
		if (fD > nD)
		{
			pCell->SetColWidth(0, floor(w) + 1, i, 0);
			nD += 2;
		}
		else
			pCell->SetColWidth(0, floor(w), i, 0);
	}
	
	nD = dPageWidth - GetCellSize(pCell).cx;
	int nA = (abs(nD) - 1)/nCols1 + 1;
	while (nD > 0)
	{
		int nMax = GetMax(nWidth);
		if (nMax < 0)
			break;

		w = pCell->GetColWidth(0, nMax, 0) + nA;
		if (w > nA)
			pCell->SetColWidth(0, w, nMax, 0);
		nWidth.RemoveAt(nMax);
		nD = dPageWidth - GetCellSize(pCell).cx;
	}

	/*while (nD < nCols)
	{
		int nMax = GetMax(nWidth);
		if (nMax < 0)
			break;
		
		w = pCell->GetColWidth(0, nMax, 0) - nA;
		if (w > 1)
			pCell->SetColWidth(0, w, nMax, 0);
		nWidth.RemoveAt(nMax);
		nD = dPageWidth - GetCellSize(pCell).cx;
	}*/
	
	size.cx = dPageWidth - nD;
	return size;
}

void CULView::ResizeCell()
{
	m_nRefresh = 1;
	RedrawWindow(NULL, NULL, RDW_INVALIDATE|RDW_ERASE|RDW_ALLCHILDREN|RDW_UPDATENOW);
	Invalidate();
}

void CULView::ResetCellSize(CCell2000* pCell)
{
	int nCols = pCell->GetCols(0);
	for (int i = 0; i < nCols; i++)
	{
		CString string = pCell->GetCellNote(i, 0, 0);
		int w = _ttoi(string);
		pCell->SetColWidth(0, w, i, 0);
	}
	pCell->ReDraw();
}

void CULView::SetCellFitColWidth(CCell2000* pCell,
	CSize szBorder /* = CSize */)
{
	pCell->SetScreenScale(0, 1.0);
	CSize size(0, 0);
	size.cx = CalcPageWidth() - 2 * PAGE_MARGIN;

	int nCols = pCell->GetCols(0);
	CWnd* pWnd = pCell->GetParent();
	if (pWnd == NULL)
		return ;

	size.cx -= szBorder.cx;
	pCell->SetColWidth(0, 0, 0, 0);
	long w = size.cx / (nCols - 3);
	long w1 = w;
	long w2 = 7*w/8;
	long w3 = 3*w/2;
	double d = size.cx - 3*(w1 + 4*w2 + w3);
	if (nCols > 1)
		d = d / (double) (nCols - 1);

	int nD = 1;
	long ws[] = { w1, w2, w2, w2, w2, w3};
	for (int i = 2; i < nCols - 1; i++)
	{
		int j = (i - 1) % 6 - 1;
		if (j < 0)
			j = 5;
		w = ws[j];
/*		if (i * d > nD)
		{
			pCell->SetColWidth(0, w + 1, i, 0);
			nD ++;
		}
		else
*/			pCell->SetColWidth(0, w, i, 0);
	}
}

void CULView::AddPdfPrintInfo(UINT nHeight, CString strFileName)
{
	CString strName = strFileName;
	int nPos = strName.ReverseFind('\\');
	if (nPos > 0)
	{
		strName = strName.Right(strName.GetLength() - nPos - 1);
	}
	nPos = strName.Find('.');
	if (nPos > 0)
	{
		strName = strName.Left(nPos);
	}
	strName += ".pdf";

	PDFPRINTINFO pdfPrintInfo;
	ZeroMemory(&pdfPrintInfo, sizeof(PDFPRINTINFO));
	memcpy(pdfPrintInfo.strFileName, strName, strName.GetLength());
	pdfPrintInfo.nHeight = nHeight;
	m_pdfPrintInfo.Add(pdfPrintInfo);
}

void CULView::CreatePdfLinkFile(CString strFileName)
{
	CStdioFile file;
	file.Open(strFileName, CFile::modeCreate | CFile::modeWrite);
	CString strValue;
	CSize szPrintSize = m_pPrintInfo->GetPrinterPageSize();
	strValue.Format("%d\n", szPrintSize.cy);
	file.WriteString(strValue);
	for (int i = 0 ; i < m_pdfPrintInfo.GetSize() ; i++)
	{
		CString strName = m_pdfPrintInfo[i].strFileName ;
		strName += "\n";
		file.WriteString(strName);
		strValue.Format("%d\n", m_pdfPrintInfo[i].nHeight);
		file.WriteString(strValue);
	}
	file.Close();
}

BOOL CULView::CreateCell(CCell2000* pCell, CWnd* pParent,
	BOOL bAllowSize /* = TRUE */, UINT nID)
{
	if (!pCell->Create(NULL, WS_CHILD|WS_VISIBLE|SB_CTL,
			CRect(0, 0, 0, 0), pParent, nID))
	{
		return FALSE;
	}

	BYTE chName[10] =
	{
		0xBD, 0xFA, 0xD2, 0xB5, 0xC8, 0xED, 0xBC, 0xFE, 0x00, 0x00
	};

// 	if (GetThreadLocale() == MAKELCID(MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), SORT_DEFAULT))
// 		pCell->LocalizeControl(0x409);
// 	else
// 		pCell->LocalizeControl(0x804);

	pCell->Login(CString(chName), "1101010373", "1300-0616-0122-3005");

	pCell->SetCols(10, 0);
	pCell->SetRows(31, 0);
	pCell->ShowPageBreak(0); 
	pCell->SetShowCellTip(FALSE);
	pCell->SetAllowSizeColInGrid(bAllowSize);
	pCell->SetAllowSizeRowInGrid(bAllowSize);
	pCell->EnableUndo(TRUE);
	pCell->SetBorder(1);
	pCell->SetAllowDragdrop(FALSE);
	pCell->SetNumCellColor(RGB(0, 0, 0));
	// 设置数值、货币、日期、时间、百分比、科学计数、货币大写类型单元格的文字颜色

	int nCols = pCell->GetCols(0) - 1;
	int nRows = pCell->GetRows(0) - 1;
	int i;
	for (i = 1; i < nCols; i++)
	{
		for (int j = 1; j < nRows; j++)
		{
			pCell->SetCellTextStyle(i, j, 0, 1);
			pCell->SetCellAlign(i, j, 0, 1);
		}
	}	

	int nWidth = CalcPageWidth();
	nWidth -= (2 * PAGE_MARGIN);
	nWidth = nWidth / nCols;
	int nHeight = CELL_HEIGHT / nRows;
	for (i = 1; i < nRows; i ++)
		pCell->SetRowHeight(0, nHeight, i, 0);
	for (i = 1; i < nCols; i ++)
		pCell->SetColWidth(0, nWidth, i, 0);


	//	pCell->DrawGridLine(1, 1, nCols - 1, nRows - 1, 0, 2, 0);
	pCell->ShowHScroll(0, 0);   // 不显示水平滚动条
	pCell->ShowVScroll(0, 0);   // 不显示竖直滚动条
	pCell->ShowTopLabel(0, 0);  // 不显示列标
	pCell->ShowSideLabel(0, 0); // 不显示行标
	pCell->ShowSheetLabel(0, 0);// 隐藏页签

	return TRUE;
}

void CULView::PrepareCell(CCell2000* pCell, CDC* pDC, LPRECT lpRect)
{
	SetCellFitSize(pCell);

	CRect rect;
	rect.left = PAGE_MARGIN;
	rect.top = -PAGE_MARGIN;
	int nWidth = CalcPageWidth();
	rect.right = rect.left + nWidth;
	rect.bottom = rect.top - CalcPageHeight();
	rect.DeflateRect(PAGE_MARGIN, -PAGE_MARGIN, PAGE_MARGIN, -PAGE_MARGIN);
	pDC->LPtoDP(rect);

	if (pCell->GetBorder() == 0)
		rect.left += 2;

	pCell->SetWindowPos(NULL, rect.left, rect.top, rect.Width(),
			rect.Height(), SWP_NOZORDER);
	pCell->SetLeftCol(0);
	pCell->SetTopRow(0);
	pCell->SelectRange(-1, -1, -1, -1);
}

void CULView::DefineCell(CCell2000* pCell, BOOL bAuto /* = FALSE */)
{
	if (pCell->GetSafeHwnd() == NULL)
		return ;

	CGraphHeaderView* pGHView = DYNAMIC_DOWNCAST(CGraphHeaderView,
									pCell->GetParent());
	if (pGHView == NULL)
		return ;

	long col1 = pCell->GetCurrentCol();
	long row1 = pCell->GetCurrentRow();
	pGHView->SaveCell(pCell, col1, row1, 0);

	if (!bAuto)
	{
		if (m_bDefineSymbol)
			return ;

		m_bDefineSymbol = TRUE;
	}

	CString strNote;
	int nCols = pCell->GetCols(0);
	int nRows = pCell->GetRows(0);
	for (int i = 1; i < nCols; i++)
	{
		for (int j = 1; j < nRows; j++)
		{
			DefineCell(pCell, i, j, 0);
		}
	}
}

// 由符号值变成符号名
void CULView::DefineCell(CCell2000* pCell, long col, long row, long sheet)
{
	CString note = pCell->GetCellNote(col, row, sheet);
	int iFind = note.ReverseFind('@');
	if (iFind > -1)
		note = note.Left(iFind);

	if (note.GetLength() < 5)
		return;

	int iTagStart = note.Find("<");
	if (iTagStart < 0)
		return;

	int iTagEnd = note.Find(">", iTagStart);
	if (iTagEnd < 0)
		return;

	pCell->SetCellString(col, row, sheet, note);
}

void CULView::InvalidateCell(CCell2000* pCell, BOOL bAuto /* = FALSE */, CProject* pPrj /* = NULL */)
{
	if (pCell->GetSafeHwnd() == NULL)
		return;

	CChildFrame* pChild = DYNAMIC_DOWNCAST(CChildFrame,
							pCell->GetParentFrame());
	if (pChild == NULL || (pChild->m_nViewType & ULV_CELL) == 0)
		return;

	if (pChild->m_pGHView != NULL)
	{
		long col = pCell->GetCurrentCol();
		long row = pCell->GetCurrentRow();
		pChild->m_pGHView->SaveCell(pCell, col, row, 0);
	}

	if (pPrj == NULL)
	{
		pPrj = g_pActPrj;
		if (pChild->m_pULFile != NULL)
		{
			pPrj = pChild->m_pULFile->m_pProject;
		}
	}

	if (pPrj == NULL)
		return;

	int nCols = pCell->GetCols(0);
	int nRows = pCell->GetRows(0);
	for (int i = 1; i < nCols; i++)
	{
		for (int j = 1; j < nRows; j++)
		{
			pPrj->InvalidateCell(pCell, i, j, 0);
			if (pChild->m_pDoc != NULL)// 刷新标签信息
			    pChild->m_pDoc->InvalidateCell(pCell, i, j, 0);
		}
	}
	
	if (!bAuto)
		m_bDefineSymbol = FALSE;
}

void CULView::CloseSymDlg()
{
	if (m_pSymDlg->GetSafeHwnd() != NULL)
	{
		m_pSymDlg->DestroyWindow();
		delete m_pSymDlg;
		m_pSymDlg = NULL;
	}
}

// Save cell edit
BOOL CULView::SaveCell(CCell2000* pCell, long col, long row, long sheet)
{
	if (pCell->GetSafeHwnd() == NULL || col < 0 || row < 0 || sheet < 0)
		return FALSE;

	CString note = pCell->GetCellNote(col, row, sheet);
	int iFind = note.ReverseFind('@');
	if (iFind > -1)
		note = note.Left(iFind);

	CString string2 = pCell->GetCellString2(col, row, sheet);

	// Define symbol mode
	if (m_bDefineSymbol)
	{
		// No changing
		if (note == string2)
			return FALSE;

		int iLen = string2.GetLength();
		if (iLen < 5)
		{
			pCell->SetCellNote(col, row, sheet, "");
			return FALSE;
		}

		// Check the symbol is valid if there is
		CString test = string2;
		int iTagStart = test.Find('<');
		if (iTagStart < 0)
			return FALSE;

		if (++iTagStart == iLen)
			return FALSE;

		switch (test[iTagStart])
		{
		case '$':
		case '#':
		case '%':
			break;
		default:
			return FALSE;
		}

		CString sub = test.Mid(iTagStart, 1) + ">";
		int iTagEnd = test.Find(sub, iTagStart);
		if (iTagEnd < 0)
			return FALSE;

		// int count = 1;
		// note.Format("%s@%d", string2, count);
		// We find a substring with symbol format
		pCell->SetCellNote(col, row, sheet, string2);
		return TRUE;
	}

	// The note is invalid
	int iLen = note.GetLength();
	if (iLen < 5)
		return FALSE;

	// The text isn't change
	CString string = pCell->GetCellString(col, row, sheet);
	if (string == string2)
		return FALSE;

	// A symbol
	if ((m_pDoc != NULL) && (m_pDoc->m_pProject != NULL))
	{
		// Save value to project information
		return m_pDoc->m_pProject->SaveInformation(note, string2);
	}

	return FALSE;
}

void CULView::OnColsSize(CSize szBorder /* = CSize */)
{
}

void CULView::OnRowsSize()
{
}

// ---------------------------------
//	Table edit:
// ---------------------------------

void CULView::OnEditColInsertLeft()
{
	long col = m_pEdit->GetCurrentCol();
	m_pEdit->InsertCol(col, 1, 0);
	OnColsSize();
	m_pEdit->ReDraw();
}

void CULView::OnEditColInsertRight()
{
	long col = m_pEdit->GetCurrentCol();
	m_pEdit->InsertCol(col + 1, 1, 0);
	OnColsSize();
	m_pEdit->ReDraw();
}

void CULView::OnEditRowInsertTop()
{
	long row = m_pEdit->GetCurrentRow();
	m_pEdit->InsertRow(row, 1, 0);
	OnRowsSize();
	m_pEdit->ReDraw();
}

void CULView::OnEditRowInsertBottom()
{
	long row = m_pEdit->GetCurrentRow();
	m_pEdit->InsertRow(row + 1, 1, 0);
	OnRowsSize();
	m_pEdit->ReDraw();
}

void CULView::OnEditColRemove()
{
	long col = m_pEdit->GetCurrentCol();
	m_pEdit->DeleteCol(col, 1, 0);
	OnColsSize();
	m_pEdit->ReDraw();
}

void CULView::OnEditRowRemove()
{
	long row = m_pEdit->GetCurrentRow();
	m_pEdit->DeleteRow(row, 1, 0);
	OnRowsSize();
	m_pEdit->ReDraw();
}

void CULView::OnEditColWidthBest()
{
	long col, row;
	m_pEdit->GetFirstSelectedCell(&col, &row);

	long bestWidth = m_pEdit->GetColBestWidth(col);
	if (bestWidth)
	{
		m_pEdit->SetColWidth(1, bestWidth, col, 0);
	}
}

void CULView::OnEditRowHeightBest()
{
	long col, row;
	m_pEdit->GetFirstSelectedCell(&col, &row);
	long bestHeight = m_pEdit->GetRowBestHeight(row);
	if (bestHeight)
	{
		m_pEdit->SetRowHeight(1, bestHeight, row, 0);
	}
}

void CULView::OnEditJoinCellRange()
{
	long lStartRow, lEndRow, lStartCol, lEndCol;
	m_pEdit->GetSelectRange(&lStartCol, &lStartRow, &lEndCol, &lEndRow);
	m_pEdit->MergeCells(lStartCol, lStartRow, lEndCol, lEndRow);
}

void CULView::OnEditUnjoinCellRange()
{
	long lStartRow, lEndRow, lStartCol, lEndCol;
	long lStartRow1, lEndRow1, lStartCol1, lEndCol1;
	long lCurSheet = m_pEdit->GetCurSheet();
	m_pEdit->GetSelectRange(&lStartCol, &lStartRow, &lEndCol, &lEndRow);
	for (long col = lStartCol; col <= lEndCol; col++)
	{
		for (long row = lStartRow; row <= lEndRow; row++)
		{
			m_pEdit->GetMergeRange(col, row, &lStartCol1, &lStartRow1,
						&lEndCol1, &lEndRow1);
			m_pEdit->UnmergeCells(lStartCol1, lStartRow1, lEndCol1, lEndRow1);
		}
	}
}

void CULView::OnEditJoinRangeCol()
{
	long lStartRow, lEndRow, lStartCol, lEndCol;
	m_pEdit->GetSelectRange(&lStartCol, &lStartRow, &lEndCol, &lEndRow);
	for (long row = lStartRow; row <= lEndRow; row++)
		m_pEdit->MergeCells(lStartCol, row, lEndCol, row);
}

void CULView::OnEditJoinRangeRow()
{
	long lStartRow, lEndRow, lStartCol, lEndCol;
	m_pEdit->GetSelectRange(&lStartCol, &lStartRow, &lEndCol, &lEndRow);
	for (long col = lStartCol; col <= lEndCol; col++)
		m_pEdit->MergeCells(col, lStartRow, col, lEndRow);
}

void CULView::OnEditAlignTop()
{
	long lStartRow, lEndRow, lStartCol, lEndCol;
	long lHAlign = 0, lVAlign = 8, lAlign = 0;
	m_pEdit->GetSelectRange(&lStartCol, &lStartRow, &lEndCol, &lEndRow);
	long lCurSheet = m_pEdit->GetCurSheet();
	for (long row = lStartRow; row <= lEndRow; row++)
	{
		for (long col = lStartCol; col <= lEndCol; col++)
		{
			lHAlign = m_pEdit->GetCellAlign(col, row, lCurSheet) & (1 + 2 + 4);
			lAlign = lVAlign + lHAlign;
			m_pEdit->SetCellAlign(col, row, lCurSheet, lAlign);
		}
	}

	m_pEdit->Invalidate();
}

void CULView::OnEditAlignMiddle()
{
	long lStartRow, lEndRow, lStartCol, lEndCol;
	long row, col, lCurSheet;
	long lHAlign = 0, lVAlign = 32, lAlign = 0;
	m_pEdit->GetSelectRange(&lStartCol, &lStartRow, &lEndCol, &lEndRow);
	lCurSheet = m_pEdit->GetCurSheet();	
	for (row = lStartRow; row <= lEndRow; row++)
	{
		for (col = lStartCol; col <= lEndCol; col++)
		{
			lHAlign = m_pEdit->GetCellAlign(col, row, lCurSheet) & (1 + 2 + 4);
			lAlign = lVAlign + lHAlign;
			m_pEdit->SetCellAlign(col, row, lCurSheet, lAlign);
		}
	}
	m_pEdit->Invalidate();
}

void CULView::OnEditAlignBottom()
{
	long lStartRow, lEndRow, lStartCol, lEndCol;
	long row, col, lCurSheet;
	long lHAlign = 0, lVAlign = 16, lAlign = 0;
	m_pEdit->GetSelectRange(&lStartCol, &lStartRow, &lEndCol, &lEndRow);
	lCurSheet = m_pEdit->GetCurSheet();
	for (row = lStartRow; row <= lEndRow; row++)
	{
		for (col = lStartCol; col <= lEndCol; col++)
		{
			lHAlign = m_pEdit->GetCellAlign(col, row, lCurSheet) & (1 + 2 + 4);
			lAlign = lVAlign + lHAlign;
			m_pEdit->SetCellAlign(col, row, lCurSheet, lAlign);
		}
	}
	m_pEdit->Invalidate();
}

void CULView::OnEditAlignLeft()
{
	long lStartRow, lEndRow, lStartCol, lEndCol;
	long row, col, lCurSheet;
	long lHAlign = 1, lVAlign = 0, lAlign = 0;
	m_pEdit->GetSelectRange(&lStartCol, &lStartRow, &lEndCol, &lEndRow);
	lCurSheet = m_pEdit->GetCurSheet();	
	for (row = lStartRow; row <= lEndRow; row++)
	{
		for (col = lStartCol; col <= lEndCol; col++)
		{
			lVAlign = m_pEdit->GetCellAlign(col, row, lCurSheet) & (8 +
				16 +
				32);
			lAlign = lVAlign + lHAlign;
			m_pEdit->SetCellAlign(col, row, lCurSheet, lAlign);
		}
	}
	m_pEdit->ReDraw();
}

void CULView::OnEditAlignCenter()
{
	long lStartRow, lEndRow, lStartCol, lEndCol;
	long row, col, lCurSheet;
	long lHAlign = 4, lVAlign = 0, lAlign = 0;
	m_pEdit->GetSelectRange(&lStartCol, &lStartRow, &lEndCol, &lEndRow);
	lCurSheet = m_pEdit->GetCurSheet();
	for (row = lStartRow; row <= lEndRow; row++)
	{
		for (col = lStartCol; col <= lEndCol; col++)
		{
			lVAlign = m_pEdit->GetCellAlign(col, row, lCurSheet) & (8 +
				16 +
				32);
			lAlign = lVAlign + lHAlign;
			m_pEdit->SetCellAlign(col, row, lCurSheet, lAlign);
		}
	}
	m_pEdit->ReDraw();
}

void CULView::OnEditAlignRight()
{
	long lStartRow, lEndRow, lStartCol, lEndCol;
	long lHAlign = 2, lVAlign = 0, lAlign = 0;
	m_pEdit->GetSelectRange(&lStartCol, &lStartRow, &lEndCol, &lEndRow);
	long lCurSheet = m_pEdit->GetCurSheet();
	for (long row = lStartRow; row <= lEndRow; row++)
	{
		for (long col = lStartCol; col <= lEndCol; col++)
		{
			lVAlign = m_pEdit->GetCellAlign(col, row, lCurSheet) & (8 +
				16 +
				32);
			lAlign = lVAlign + lHAlign;
			m_pEdit->SetCellAlign(col, row, lCurSheet, lAlign);
		}
	}
	m_pEdit->ReDraw();
}

void CULView::OnEditFillColor() 
{
	CGridPropertySheet sheet(this);
	sheet.DoModal();
}

void CULView::OnEditLineCombo()
{
	CMFCToolBarComboBoxButton* pSrcCombo = NULL;
	CObList listButtons;
	if (CMFCToolBar::GetCommandButtons (ID_EDIT_LINE_COMBO, listButtons) > 0)
	{
		for (POSITION posCombo = listButtons.GetHeadPosition (); 
		pSrcCombo == NULL && posCombo != NULL;)
		{
			CMFCToolBarComboBoxButton* pCombo = 
				DYNAMIC_DOWNCAST (CMFCToolBarComboBoxButton, listButtons.GetNext (posCombo));
			
			if (pCombo != NULL && 
				CMFCToolBar::IsLastCommandFromButton (pCombo))
			{
				pSrcCombo = pCombo;
			}
		}
	}
	
	if (pSrcCombo != NULL)
	{
		m_nLineStyle = pSrcCombo->GetCurSel() + 1;
	}
}

void CULView::OnUpdateEdit(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_pEdit->GetSafeHwnd() ? TRUE : FALSE);
}

void CULView::OnSymbolInsert()
{
	if (m_pSymDlg == NULL)
	{
		OnSymbolDefine();
		//CProject* pProject = m_pDoc ? m_pDoc->m_pProject : NULL;

		//add by yh 2009.2.14
		//解决了bug:打开工程，插入符号与符号管理中符号不一致
		//如果打开工程，就优先显示工程符号库；否则显示文件的符号库
		CProject *pProject = NULL ;
		if(g_pActPrj == NULL || !AfxIsValidAddress(g_pActPrj, sizeof(CProject)))
			pProject = m_pDoc ? m_pDoc->m_pProject : NULL;
		else 
			pProject = g_pActPrj ;

		m_pSymDlg = new CSymbolManagerDlg(pProject, TRUE, m_pMainWnd);
		m_pSymDlg->Create(CSymbolManagerDlg::IDD, m_pMainWnd);
		m_pSymDlg->SetCell(m_pEdit);
	}
}

void CULView::OnSymbolDefine()
{
	DefineCell(m_pEdit);
}

void CULView::OnSymbolRefresh()
{
	InvalidateCell(m_pEdit);
	if (m_pDoc != NULL)
	{
		m_pDoc->UpdateViews(ULV_CONSTRUCT|ULV_GRAPHHEAD);
		m_pDoc->UpdateCells(ULV_GROUP);
	}
}

void CULView::OnUpdateSymbolInsert(CCmdUI* pCmdUI) 
{
	CChildFrame* pChild = DYNAMIC_DOWNCAST(CChildFrame, GetParentFrame());
	if (pChild->GetSafeHwnd())
		pCmdUI->Enable(pChild->m_nViewType & ULV_GRAPHHEAD);
	else
		pCmdUI->Enable(FALSE);
}

void CULView::OnUpdateSymbolDefine(CCmdUI* pCmdUI) 
{
	CChildFrame* pChild = DYNAMIC_DOWNCAST(CChildFrame, GetParentFrame());
	if (pChild->GetSafeHwnd())
		pCmdUI->Enable(pChild->m_nViewType & ULV_GRAPHHEAD);
	else
		pCmdUI->Enable(FALSE);

	pCmdUI->SetCheck(m_bDefineSymbol);	
}

void CULView::OnUpdateSymbolRefresh(CCmdUI* pCmdUI) 
{
	CChildFrame* pChild = DYNAMIC_DOWNCAST(CChildFrame, GetParentFrame());
	if (pChild->GetSafeHwnd())
		pCmdUI->Enable(pChild->m_nViewType & ULV_GRAPHHEAD);
	else
		pCmdUI->Enable(FALSE);	
}

void CULView::OnSymbolManager() 
{
	// TODO: Add your command handler code here
	//CChildFrame* pChild = DYNAMIC_DOWNCAST(CChildFrame, GetActiveFrame());

	//add by yh 2009.2.14
	//解决了bug:打开文件，插入符号与符号管理中符号不一致
	//如果没有打开工程，就显示文件的符号库；否则优先显示工程符号库
	CProject *pProject = NULL ;
	if(g_pActPrj == NULL || !AfxIsValidAddress(g_pActPrj, sizeof(CProject)))
		pProject = m_pDoc ? m_pDoc->m_pProject : NULL;
	else
		pProject = g_pActPrj ;

	CSymbolManagerDlg dlg(pProject, FALSE, this);
	dlg.DoModal();
	
}

void CULView::OnUpdateCustomizeSymbol(CCmdUI* pCmdUI)
{
	if ((m_pDoc==NULL) || (m_pDoc->m_pProject==NULL))
		pCmdUI->Enable(FALSE);
	else
		pCmdUI->Enable();

	/*CRuntimeClass* prt = GetRuntimeClass();
	if (prt != NULL)
	{
		if (strcmp( prt->m_lpszClassName, "CGraphHeaderView" )  != 0 )
			pCmdUI->Enable(FALSE);
	}*/
}
void CULView::OnCustomizeSymbol() 
{
	// TODO: Add your command handler code here
	CSymbolCustomizedDlg dlg(m_pDoc->m_pProject);
	if (dlg.DoModal() == IDOK)
	{
		//m_pDoc->m_pProject->SetInformation("NewSE", "7",1);
		InvalidateCell(m_pEdit);
		m_pDoc->UpdateViews(ULV_CONSTRUCT|ULV_GRAPHHEAD);
		m_pDoc->UpdateCells(ULV_GROUP);
	}
}

POINT CULView::ScrollToPositionEx(POINT pt)	// logical coordinates
{
	ASSERT(m_nMapMode > 0);     // not allowed for shrink to fit

	CWindowDC dc(NULL);
	dc.SetMapMode(m_nMapMode);
	dc.LPtoDP((LPPOINT)&pt);
	
	// now in device coordinates - limit if out of range
	int xMax = GetScrollLimit(SB_HORZ);
	int yMax = GetScrollLimit(SB_VERT);
	if (pt.x < 0)
		pt.x = 0;
	else if (pt.x > xMax)
		pt.x = xMax;
	if (pt.y < 0)
		pt.y = 0;
	else if (pt.y > yMax)
		pt.y = yMax;
	
	ScrollToDevicePosition(pt);
	dc.DPtoLP(&pt);
	return pt;
}

void CULView::ScrollToCellCurPos()
{
	long col = m_pEdit->GetCurrentCol();
	long row = m_pEdit->GetCurrentRow();
	CPoint pt = GetCellPos(m_pEdit, col, row, 1);
	CRect rect;
	GetClientRect(&rect);
	CPoint pt0(0, 0);
	pt0.x = GetScrollPos(SB_HORZ);
	pt0.y = GetScrollPos(SB_VERT);

	rect.OffsetRect(pt0);
	if (!rect.PtInRect(pt))
	{
		// now in device coordinates - limit if out of range
		int xMax = GetScrollLimit(SB_HORZ);
		int yMax = GetScrollLimit(SB_VERT);
		if (pt.x < 0)
			pt.x = 0;
		else if (pt.x > xMax)
			pt.x = xMax;
		if (pt.y < 0)
			pt.y = 0;
		else if (pt.y > yMax)
			pt.y = yMax;
		
		ScrollToDevicePosition(pt);
		SendMessage(WM_SIZE);
	}
}


DWORD CULView::Locked()
{
	CChildFrame* pChild = DYNAMIC_DOWNCAST(CChildFrame, GetParentFrame());
	if (pChild != NULL)
		return pChild->m_dwLocked;

	return LT_UNLOCK;
}

void CULView::InitFonts()
{
	g_font[ftT1].CreateFont(60, 45, 0, 0, FW_BLACK, FALSE, FALSE, 0, ANSI_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Arial");
	
	g_font[ftT2].CreateFont(40, 20, 0, 0, FW_BLACK, FALSE, FALSE, 0, ANSI_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Arial");
	
	LOGFONT lf;
	afxGlobalData.fontRegular.GetLogFont(&lf);
	lf.lfHeight = -30;
//	memcpy(lf.lfFaceName, Gbl_ConfigInfo.m_Watch.FFont[ftR].lfFaceName, 32); 
	g_font[ftR].CreateFontIndirect(&lf);
}

// TemplManageDlg.cpp : implementation file
//

#include "stdafx.h"
#ifndef _LOGIC
#include "ul2000.h"
#else
#include "Logic.h"
#endif

#include "TemplManageDlg.h"
#include "Sheet.h"
#include "ChildFrm.h"
#include "ULView.h"
#include "MainFrm.h"
#include "GraphHeaderView.h"
#include "PrintOrderDlg.h"
#include "IsValidFileName.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTemplManageDlg dialog


CTemplManageDlg::CTemplManageDlg(int nType, CWnd* pParent /*=NULL*/)
	: CDialog(CTemplManageDlg::IDD, pParent)
{
	m_nType = nType;
}


void CTemplManageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTemplManageDlg)
	DDX_Control(pDX, IDC_TEMPLATE_LIST, m_TemplsList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTemplManageDlg, CDialog)
	//{{AFX_MSG_MAP(CTemplManageDlg)
	ON_COMMAND_RANGE(IDC_BTN_NEWTEMPL, IDC_BTN_EXITMANAGE, OnCommand)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_TEMPLATE_LIST, OnItemChange)
	ON_NOTIFY(NM_DBLCLK, IDC_TEMPLATE_LIST, OnDblclkTemplateList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTemplManageDlg message handlers

BOOL CTemplManageDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
			
	CFileFind ff;
	CString strFind;
	BOOL bFind = FALSE;
	CString strTitle;
	switch(m_nType)
	{
	// 为绘图模板创建图标
	case ID_PRESENTATION_MANAGE:
		strFind = Gbl_AppPath + "\\Template\\Tracks\\*.xml";
		m_ImageList.Create(16, 16, ILC_MASK | ILC_COLOR32, 0, 1);
		m_ImageList.Add((HICON)AfxGetApp()->LoadIcon(IDI_DEFAULTICON));
		strTitle.LoadString(IDS_PRESENTATION_TEMPLATE);
		break;

	// 为图表模板创建图标
	case ID_HEADER_MANAGE:
		strFind = Gbl_AppPath + "\\Template\\Headers\\*.ul?";  //*.ulh, *.ulr, *.ult
		m_ImageList.Create(16, 16, ILC_MASK | ILC_COLOR16, 0, 1);
		m_ImageList.Add(theApp.m_wkspcImages.ExtractIcon(18));

		m_menu.LoadMenu (IDR_POPUP_HEADER);
		m_btnSave.m_hMenu = m_menu.GetSubMenu (0)->GetSafeHmenu ();

		//modify by yh 2009.2.5
		//解决了bug:模板窗体退出而按钮菜单仍然存在
		//解决了bug:保存为图尾不可用
		m_btnSave.m_bOSMenu = TRUE;

		m_btnSave.m_bDefaultClick = TRUE;
		m_btnSave.m_bRightArrow = TRUE;
		m_btnSave.m_bStayPressed = TRUE;

		m_btnSave.SubclassWindow(::GetDlgItem(m_hWnd, IDC_BTN_SAVETEMPL));
		strTitle.LoadString(IDS_HEADER_TEMPLATE);
		break;

	case ID_CVS_MANAGE:
		strFind = Gbl_AppPath + "\\Template\\CstViews\\*.acv";
		m_ImageList.Create(16, 16, ILC_MASK | ILC_COLOR32, 0, 1);
		m_ImageList.Add(theApp.m_wkspcImages.ExtractIcon(28));
		strTitle.LoadString(IDS_CUSTOM_TEMPLATE);
		break;

	// 为组合图模板创建图标
	case ID_PLOTTEMPLATE_MANAGE:
		strFind = Gbl_AppPath + "\\Template\\Prints\\*.ulo";
		m_ImageList.Create(16, 16, ILC_MASK | ILC_COLOR32, 0, 1); //ILC_MASK 设置背景色，可防止背景变黑。
		m_ImageList.Add((HICON)theApp.LoadIcon(IDI_PRINTICON));

		GetDlgItem(IDC_BTN_SAVETEMPL)->EnableWindow(FALSE);
		strTitle.LoadString(IDS_PRINT_TEMPLATE);
		break;
	default:
		break;
	}
	
	SetWindowText(strTitle);
	m_ImageList.SetBkColor(RGB(255, 255, 255));
//	m_ImageList.SetImageCount(0);
	m_TemplsList.SetImageList(&m_ImageList, LVSIL_SMALL);
	m_TemplsList.InsertColumn(0, "");
	m_TemplsList.InsertColumn(1, "");
	m_TemplsList.InsertColumn(2, "");

	bFind = ff.FindFile(strFind);

	// 加载模板文件
	while (bFind)
	{
		bFind = ff.FindNextFile();
//		CString strName = ff.GetFileName();
//		if (ff.IsDots()/*strName == "." || strName == ".."*/)
//			continue;

		if (ff.IsDirectory())
			continue;

		CString strTitle = ff.GetFileTitle();
		int nIndex = m_TemplsList.InsertItem(m_TemplsList.GetItemCount(), strTitle);
		m_TemplsList.SetItemText(nIndex, 2, ff.GetFilePath());
	}
	ff.Close();	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CTemplManageDlg::OnItemChange(NMHDR* pNMHDR, LRESULT* pResult)
{
	POSITION pos = m_TemplsList.GetFirstSelectedItemPosition();
	if (pos == NULL)
		return;

	CString strTemplsName;
	while (pos)
	{
		int nItem = m_TemplsList.GetNextSelectedItem(pos);
		
		if (strTemplsName.IsEmpty())
			strTemplsName = m_TemplsList.GetItemText(nItem, 0);
		else
			strTemplsName += "," + m_TemplsList.GetItemText(nItem, 0);
	}

	SetDlgItemText(IDC_EDIT_TEMPLNAME, strTemplsName);
}

// 命令消息
void CTemplManageDlg::OnCommand(UINT id)
{
	switch(id)
	{
	// 新建模板
	case IDC_BTN_NEWTEMPL:
		OnNewTempl();
		break;

	// 编辑模板
	case IDC_BTN_EDITTEMPL:
		OnEditTempl();
		break;

	// 保存模板
	case IDC_BTN_SAVETEMPL:
		OnSaveTempl();
		break;

	// 删除模板
	case IDC_BTN_DELETETEMPL:
		OnDeleteTempl();
		break;

	// 退出模板管理
	case IDC_BTN_EXITMANAGE:
	default:		
		EndDialog(IDCANCEL);
		break;
	}
}

// 新建模板
void CTemplManageDlg::OnNewTempl()
{
	static int iIndex = 1;	
	CString strTempl;

	switch (m_nType)
	{
	// 新建绘图模板
	case ID_PRESENTATION_MANAGE:
		{
			strTempl.Format("Templ%d", iIndex++);
			
			CSheet* pSheet = new CSheet;
			pSheet->Default();
			pSheet->m_strName = strTempl;
			
			CChildFrame* pChild = new CChildFrame(ULV_GRAPHS, pSheet);
			pChild->m_bAutoDes = TRUE;
			if (pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD|WS_VISIBLE, g_pMainWnd))
			{
				pChild->SetWindowText(_T("[P]") + strTempl);
				g_pMainWnd->m_preList.AddTail(pChild);
			}
			
			// 自动打开ClassViewBar
			g_pMainWnd->ShowClassBar();
			
		}
		break;

	// 新建图头模板
	case ID_HEADER_MANAGE:
		{
			strTempl.Format("Templ%d", iIndex++);
			CCellInfo cell;
			cell.strFile.Empty();
			cell.strTempl = strTempl + ".ulh";
			CChildFrame* pChild = new CChildFrame(ULV_GRAPHHEAD, &cell);
			pChild->m_bAutoDes = TRUE;
			if (pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD|WS_VISIBLE, g_pMainWnd))
			{
				pChild->SetWindowText(_T("[H]") + strTempl);
			}
			
		}
		break;

	case ID_CVS_MANAGE:
		{
			strTempl.Format("Templ%d", iIndex++);
			
			CCstInfo cst;
			cst.strFile.Empty();
			cst.strTempl = strTempl;

			CChildFrame* pChild = new CChildFrame(ULV_CUSTOMIZEVIEW, &cst);
			pChild->m_bAutoDes = TRUE;
			if (pChild->LoadFrame(IDR_MAINFRAME, WS_CHILD|WS_VISIBLE, g_pMainWnd))
			{
				pChild->SetWindowText(_T("[C]") + strTempl);
				// g_pMainWnd->m_preList.AddTail(pChild);
			}
			
			if (!g_pMainWnd->m_wndToolbarAsst.IsVisible())
				g_pMainWnd->SendMessage(WM_COMMAND, ID_VIEW_TOOLASST_TOOLBAR);

			
		}
		break;

	// 新建组合图模板
	case ID_PLOTTEMPLATE_MANAGE:
		{
			CString filename ;
			GetDlgItemText(IDC_EDIT_TEMPLNAME, filename);
			
			// 判断用户输入的用户名是否合法

			WCHAR wszDomain[256]; 
			MultiByteToWideChar(CP_ACP, 0, filename,
			strlen(filename)+1, wszDomain, sizeof(wszDomain)/sizeof(wszDomain[0]));

			if (IsValidFileName(wszDomain) != 0)
			{
				AfxMessageBox(IDS_INVALID_FILENAME);
				return;
			}

			CPrintOrderDlg dlg;
			if( filename.Find(",", 0)==-1 )
			{
				dlg.m_strPOF = filename ; 
			}

			if (dlg.DoModal() == IDOK)
			{
				int nItem = m_TemplsList.GetItemCount();
				int i = 0;
				for (i = 0; i < nItem; i++)
				{
					CString strText = m_TemplsList.GetItemText(i, 0);
					if (strText.CompareNoCase(dlg.m_strPOF) == 0)
						return;
				}
				
				m_TemplsList.InsertItem(i, dlg.m_strPOF);
				m_TemplsList.SetItemText(i, 2, Gbl_AppPath + "\\Template\\Prints\\" + filename +".ulo");
				m_TemplsList.Arrange(LVA_DEFAULT);
			}
		}

	default:
		break;
	}
	
	g_pMainWnd->BringWindowToTop();
	if (::IsWindow(GetSafeHwnd()))
		EndDialog(IDCANCEL);
}

// 编辑模板
void CTemplManageDlg::OnEditTempl()
{
	UINT nSelectedCount = m_TemplsList.GetSelectedCount();
	if (nSelectedCount < 1)
		return;	
	
	POSITION pos = m_TemplsList.GetFirstSelectedItemPosition();
	if (pos == NULL)
		return;
	
	int nIndex = m_TemplsList.GetNextSelectedItem(pos);
	CString strFileTitle = m_TemplsList.GetItemText(nIndex, 0);     // e.g.  noname
	CString strFilePathName = m_TemplsList.GetItemText(nIndex, 2);  // e.g.  c:\noname.txt
	//strFilePathName.Insert(strFilePathName.ReverseFind('\\')+1, strFileTitle);
	

	switch(m_nType)
	{
	// 编辑绘图模板
	case ID_PRESENTATION_MANAGE:
		{
			CChildFrame* pChild = g_pMainWnd->FindChild(ULV_GRAPHS, _T("[P]") + strFileTitle);
			if (pChild != NULL)
			{
				g_pMainWnd->MDITabActivate(pChild);
			}
			else
			{
				CSheet* pSheet = new CSheet;		
				pSheet->LoadSheet(strFilePathName);
				pChild = new CChildFrame(ULV_GRAPHS, pSheet);
				pChild->m_bAutoDes = TRUE;
				if (pChild->LoadFrame(IDR_MAINFRAME, WS_VISIBLE|WS_CHILD, g_pMainWnd))
				{	
					pChild->SetWindowText(_T("[P]") + strFileTitle);
					g_pMainWnd->m_preList.AddTail(pChild);
				}
			}
			
			// 自动打开ClassViewBar
			g_pMainWnd->ShowClassBar();
			EndDialog(IDCANCEL);
		}
		break;

	// 编辑图表模板
	case ID_HEADER_MANAGE:
		{
			CCellInfo cell;
			cell.strFile  = strFilePathName;
			cell.strTempl = strFileTitle;
			CChildFrame* pChild = new CChildFrame(ULV_GRAPHHEAD, &cell);
			pChild->m_bAutoDes = TRUE;
			if (pChild->LoadFrame(IDR_MAINFRAME, WS_VISIBLE|WS_CHILD, g_pMainWnd))
			{
				CString strText = cell.strFile.Right(4);
				if (strText.CompareNoCase(".ulh") == 0)
				{
					strText = _T("[H]") + m_TemplsList.GetItemText(nIndex, 0);
				}
				else if (strText.CompareNoCase(".ult") == 0)
				{
					strText = _T("[T]") + m_TemplsList.GetItemText(nIndex, 0);
				}
				else if(strText.CompareNoCase(".ulr") == 0)
				{
					strText = _T("[S]") + m_TemplsList.GetItemText(nIndex, 0);
				}
				else
					strText = cell.strTempl;
				pChild->SetWindowText(strText);	
			}
			EndDialog(IDCANCEL);
		}
		break;
	case ID_CVS_MANAGE:
		{
			CCstInfo cst;
			cst.strFile  = strFilePathName;
			cst.strTempl = strFileTitle;
			CChildFrame* pChild = new CChildFrame(ULV_CUSTOMIZEVIEW, &cst);
			pChild->m_bAutoDes = TRUE;
			if (pChild->LoadFrame(IDR_MAINFRAME, WS_VISIBLE|WS_CHILD, g_pMainWnd))
			{
				CString strText = _T("[U]") + m_TemplsList.GetItemText(nIndex, 0);
				pChild->SetWindowText(strText);
			}
			EndDialog(IDCANCEL);
		}
		break;

	// 编辑组合图模板
	case ID_PLOTTEMPLATE_MANAGE:
		{
			CPrintOrderDlg pofDlg;
			CPofInfo pi;
			pi.strFile = strFilePathName;
			pi.strTempl = strFileTitle;
			pofDlg.m_pPofInfo = &pi;
			if (pofDlg.DoModal() == IDOK)
			{
				if (pofDlg.m_strPOF != strFileTitle)
				{
					int nItem = m_TemplsList.GetItemCount();
					int i = 0;
					for (i = 0; i < nItem; i++)
					{
						CString strText = m_TemplsList.GetItemText(i, 0);
						if (strText.CompareNoCase(pofDlg.m_strPOF) == 0)
							return;
					}
					
					int nIndex = m_TemplsList.InsertItem(i, pofDlg.m_strPOF);
					m_TemplsList.SetItemText(nIndex, 2,  Gbl_AppPath + "\\Template\\Prints\\" + pofDlg.m_strPOF + ".ulo");
				}
			}
		}
		break;
	default:
		break;
	}

	g_pMainWnd->BringWindowToTop();
}

// 保存模板
void CTemplManageDlg::OnSaveTempl()
{
	CString strTemplName;
	GetDlgItemText(IDC_EDIT_TEMPLNAME, strTemplName);
	if (strTemplName.IsEmpty())
	{
		// 模板文件名不能为空
		AfxMessageBox(IDS_NOEXSIT_NOTEMPNAME);
		GetDlgItem(IDC_EDIT_TEMPLNAME)->SetFocus();
		return ;
	}

	WCHAR wszDomain[256]; 
	MultiByteToWideChar(CP_ACP, 0, strTemplName,
    strlen(strTemplName)+1, wszDomain, sizeof(wszDomain)/sizeof(wszDomain[0]));

	if (IsValidFileName(wszDomain) != 0)
	{
		AfxMessageBox(IDS_INVALID_FILENAME);
		return;
	}
	
	CChildFrame* pChild = DYNAMIC_DOWNCAST(CChildFrame, g_pMainWnd->GetActiveFrame());
	switch(m_nType)
	{
	// 保存绘图模板
	case ID_PRESENTATION_MANAGE:
		{
			if ((pChild == NULL) || (pChild->m_pSheet == NULL))
			{
				AfxMessageBox(IDS_NO_ACTIVE_VIEW_TO_SAVE);
				return;
			}
			
			BOOL bNew = TRUE;
			CString strFile = SHT_FILE(strTemplName);
			CFileFind ff;
			if (ff.FindFile(strFile))
			{
				CString strPrompt;
				AfxFormatString1(strPrompt, IDS_EXSIT_FILE, strFile);
				if (AfxMessageBox(strPrompt, MB_YESNO) != IDYES)
					return;
				
				bNew = FALSE;
			}
			
			CSheet* pSheet = pChild->m_pSheet;
			if (pSheet->SaveSheet(strFile, FALSE, FALSE))
			{
				if (bNew)
				{
					int nIndex = m_TemplsList.InsertItem(0, strTemplName, 0);
					m_TemplsList.SetItemText(nIndex, 2, strFile);
					m_TemplsList.Arrange(LVA_DEFAULT);
					if (pChild->m_bAutoDes)
						pChild->SetWindowText(_T("[P]") + strTemplName);
				}
			}
		}
		break;

	// 保存图表模板
	case ID_HEADER_MANAGE:
		{
			if ((pChild == NULL) || (pChild->m_pGHView == NULL))
			{
				AfxMessageBox(IDS_NO_ACTIVE_VIEW_TO_SAVE);
				return;
			}
			
			CString strTempl = strTemplName;
			switch(m_btnSave.m_nMenuResult)
			{
			case ID_SAVE_HEADER:
				strTempl += ".ulh";
				break;
			case ID_SAVE_TRAILER:
				strTempl += ".ult";
				break;
			case ID_SAVE_SEPARATOR:
				strTempl += ".ulr";
				break;
			default:
				strTempl += pChild->m_pGHView->GetTemplExt();
				break;
			}
			
			CString strFile = HTS_FILE(strTempl);
			BOOL bNew = FALSE;
			CFileFind ff;
			if (ff.FindFile(strFile))
			{
				CString strPrompt;
				AfxFormatString1(strPrompt, IDS_EXSIT_FILE, strFile);
				if (AfxMessageBox(strPrompt, MB_YESNO) != IDYES)
					return;
			}
			else
				bNew = TRUE;
			
			if (!pChild->m_pGHView->SaveHeaderFile(strFile))
			{
				AfxMessageBox(IDS_ERR_SAVEERR);
				return;
			}
			
			int i = strTempl.GetLength();
			CString strName = strTempl.Left(i - 4);
			CString strExt = strTempl.Right(4);
			i = 0;
			if (strExt.CompareNoCase(".ult") == 0)
				i = 1;
			else if (strExt.CompareNoCase(".ulr") == 0)
				i = 2;
			
			if (pChild->m_bAutoDes)
			{
				CString strPre[] = { _T("[H]"), _T("[T]"), _T("[S]")}; 
				pChild->SetWindowText(strPre[i] + strName);
			}
			
			if (bNew)
			{
				int nIndex = m_TemplsList.InsertItem(0, strTempl);
				m_TemplsList.SetItemText(nIndex, 0, strName);
				m_TemplsList.SetItemText(nIndex, 2, strFile);
			}
		}
		break;

	case ID_CVS_MANAGE:
		{
			if ((pChild == NULL) || ((pChild->m_nViewType & ULV_CUSTOMIZEVIEW) == NULL))
			{
				AfxMessageBox(IDS_NO_ACTIVE_VIEW_TO_SAVE);
				return;
			}
			
			BOOL bNew = TRUE;
			CString strFile = ACV_FILE(strTemplName);
			CFileFind ff;
			if (ff.FindFile(strFile))
			{
				CString strPrompt;
				AfxFormatString1(strPrompt, IDS_EXSIT_FILE, strFile);
				if (AfxMessageBox(strPrompt, MB_YESNO) != IDYES)
					return;
				
				bNew = FALSE;
			}
			
			pChild->m_pULView->SaveAsTempl(strFile);
			
			if (bNew)
			{
				int nIndex = m_TemplsList.InsertItem(0, strTemplName, 0);
				m_TemplsList.SetItemText(nIndex, 2, strFile);
				m_TemplsList.Arrange(LVA_DEFAULT);
				if (pChild->m_bAutoDes)
					pChild->SetWindowText(_T("[C]") + strTemplName);
			}
		}
		break;

	// 保存组合图模板
	case ID_PLOTTEMPLATE_MANAGE:
	default:
		break;
	}
}

// 删除模板
void CTemplManageDlg::OnDeleteTempl()
{
	UINT nSelectedCount = m_TemplsList.GetSelectedCount();
	if (nSelectedCount < 1)
		return;

	int nItem = -1;
	
	POSITION pos = m_TemplsList.GetFirstSelectedItemPosition();
	CArray<int, int> items;
	items.Add(-1);
	while(pos != NULL)
	{
		int nItem = m_TemplsList.GetNextSelectedItem(pos);		
		for(int i=0; i < items.GetSize(); i++)
		{
			if(nItem > items[i])
			{
				items.InsertAt(i, nItem);
				break;
			}
		}
	}
	
	int Ret;
	CString prompt;
	if (nSelectedCount == 1)
	{	
		AfxFormatString1(prompt, IDS_CTEMPLMANAGEDLG_DELETE_TEMPLATE, m_TemplsList.GetItemText(items[0], 2)); //确实要删除模板 %s ?
		Ret = AfxMessageBox(prompt, MB_OKCANCEL|MB_ICONQUESTION);
	} else {
		CString Msg, strFormat;
		strFormat.LoadString(IDS_CTEMPLMANAGEDLG_DELETE_TEMPLATE2);//确实要删除这 %d 项?
		Msg.Format(strFormat, nSelectedCount);
		Ret = AfxMessageBox(prompt, MB_OKCANCEL|MB_ICONQUESTION);
	}
	
	if (Ret != IDOK)
		return;

	for (int i = 0; i < items.GetSize()-1; i++)
	{
		try{
			CFile::Remove(m_TemplsList.GetItemText(items[i], 2));
		}catch(CFileException* e){
			e->ReportError();
			e->Delete();
		}
		
		m_TemplsList.DeleteItem(items[i]);
		m_TemplsList.Update(items[i]);
	}

	m_TemplsList.Arrange(LVA_DEFAULT);
	SetDlgItemText(IDC_EDIT_TEMPLNAME, _T(""));
}

// 双击列表，激活模板
void CTemplManageDlg::OnDblclkTemplateList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	OnEditTempl();
	*pResult = 0;
}

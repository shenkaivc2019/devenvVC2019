// DlgPartRange.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "DlgPartRange.h"
#include "Units.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//extern CUnits* g_units;

/////////////////////////////////////////////////////////////////////////////
// CDlgPartRange dialog


CDlgPartRange::CDlgPartRange(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_PART_RANGE, pParent)
{
	//{{AFX_DATA_INIT(CDlgPartRange)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_lDepth0 = m_lDepthA0 =
	m_lDepth1 = m_lDepthA1 = 0;
}


void CDlgPartRange::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPartRange)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
	g_units->DDX_Depth(pDX, IDC_EDIT1, IDC_STATIC1, m_lDepth0);
	g_units->DDX_Depth(pDX, IDC_EDIT2, IDC_STATIC2, m_lDepth1);
	g_units->DDX_Depth(pDX, IDC_EDIT3, IDC_STATIC3, m_lDepthA0);
	g_units->DDX_Depth(pDX, IDC_EDIT4, IDC_STATIC4, m_lDepthA1);
}


BEGIN_MESSAGE_MAP(CDlgPartRange, CDialog)
	//{{AFX_MSG_MAP(CDlgPartRange)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgPartRange message handlers

void CDlgPartRange::OnOK() 
{
	if (!UpdateData())
		return;

	if (m_lDepth1 < m_lDepth0)
	{
		long lDepth = m_lDepth0;
		m_lDepth0 = m_lDepth1;
		m_lDepth1 = lDepth;
	}
	else if (m_lDepth0 == m_lDepth1)
	{
		m_lDepth0 = m_lDepthA0;
		m_lDepth1 = m_lDepthA1;
	}

// 	if (m_lDepth0 < m_lDepthA0)
// 		m_lDepth0 = m_lDepthA0;
// 
// 	if (m_lDepth1 > m_lDepthA1)
// 		m_lDepth1 = m_lDepthA1;

	EndDialog(IDOK);
}

BOOL CDlgPartRange::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	if (m_lDepth0 < m_lDepthA0)
		m_lDepth0 = m_lDepthA0;

	if (m_lDepth1 > m_lDepthA1)
		m_lDepth1 = m_lDepthA1;
	
	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgPartRange::OnButton1() 
{
	m_lDepth0 = m_lDepthA0;
	m_lDepth1 = m_lDepthA1;
	
	UpdateData(FALSE);
}

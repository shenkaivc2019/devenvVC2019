// SketchView.cpp : implementation file
// 井深结构图

#include "stdafx.h"
#include "SketchView.h"
#include "viwwell1.h"
#include "ULDoc.H"
#include "ComConfig.h"

#include "resource.h"
#include "Project.h"
#include "Utility.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef TITLE_HEIGHT
	#undef TITLE_HEIGHT 
#endif

#define LOGDEPTH		1000
/////////////////////////////////////////////////////////////////////////////
// CSketchView

IMPLEMENT_DYNAMIC(CSketchView, CULFormView)

CSketchView::CSketchView(LPVOID pWellPI /* = NULL */) : CULFormView(IDD_FORMVIEW)
{
	m_pViwWell = NULL;
	m_pWellPI = (CProject*)pWellPI;
	m_bUpdate = TRUE;
}

CSketchView::~CSketchView()
{

}


BEGIN_MESSAGE_MAP(CSketchView, CULFormView)
	//{{AFX_MSG_MAP(CSketchView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CSketchView::OnUpdate()
{
	if (m_pWellPI && m_bUpdate)
	{
		CXMLSettings xml(FALSE, _T("WellSketch"));
		m_pWellPI->Serialize(xml);
		xml.m_bReadOnly = TRUE;
		xml.Back(-1);
		m_pViwWell->ReadXML(&xml);
	}

	if (m_pDoc)
		m_pDoc->UpdateViews(ULV_GROUP);
}

// 计算打印高度, 不包括上下两个PAGE_MARGIN, 方便组合图的计算
int CSketchView::CalcPrintHeight()
{
	return CalcPageHeight() - 2 * PAGE_MARGIN;
}

#ifdef _DEBUG
void CSketchView::AssertValid() const
{
	CULFormView::AssertValid();
}

void CSketchView::Dump(CDumpContext& dc) const
{
	CULFormView::Dump(dc);
}
#endif //_DEBUG

int CSketchView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CULFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_pDoc)
		return 0;

	CString strFile = m_pDoc->GetPathName();
	CString strExt = strFile.Right(4);
	
	BOOL bLoadFromFile = FALSE;
	CString strAWS = strFile + _T(".aws");
	CFileFind ff;
	if (ff.FindFile(strAWS))
	{
		bLoadFromFile = TRUE;
	}

	if (strFile.GetLength()  && strExt.CompareNoCase(_T(".RAW")) && bLoadFromFile)
	{
		m_pViwWell->Load(strAWS);
	}
	else if (m_pWellPI)
	{
		VARIANT data;
		
		m_pWellPI->m_xmlInfo.m_bReadOnly = TRUE;
		m_pWellPI->m_xmlInfo.Back(-1);
		if (m_pWellPI->m_xmlInfo.Read(_T("WellSketch"), data))
			m_pViwWell->ReadFromBuffer(&data);
		
		CXMLSettings xml(FALSE, _T("WellSketch"));
		m_pWellPI->Serialize(xml);
		xml.m_bReadOnly = TRUE;
		xml.Back(-1);
		m_pViwWell->ReadXML(&xml);
	}

	return 0;
}

BEGIN_EVENTSINK_MAP(CSketchView, CULFormView)
//{{AFX_EVENTSINK_MAP(CASCOPEDlg)
ON_EVENT(CSketchView, AFX_IDC_ACTXCTRL, 1 /* DataChanged */, OnDataChangedVIWWELL1CTRL1, VTS_NONE)
//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

void CSketchView::OnDataChangedVIWWELL1CTRL1()
{	
	m_bUpdate = FALSE;
	OnUpdate();
	m_bUpdate = TRUE;

	if (m_pWellPI)
	{
		VARIANT data;
		m_pViwWell->WriteToBuffer(&data);
		m_pWellPI->m_xmlInfo.m_bReadOnly = FALSE;
		m_pWellPI->m_xmlInfo.Back(-1);
		m_pWellPI->m_xmlInfo.Write(_T("WellSketch"), data);

		try
		{
			CString strTemp = GetTempFileNameSK();
			CFile file(strTemp, CFile::modeCreate | CFile::modeWrite);
			file.Write(data.parray->pvData, data.parray->cbElements);
			file.Close();
			CXMLSettings xml;
			if (xml.ReadXMLFromFile(strTemp))
			{
				m_pWellPI->SerializeWK(xml);
			}
		}
		catch (CException* e)
		{
			e->Delete();
		}
	}

	if (m_pDoc)
		m_pDoc->SetModifiedFlag();
}

void CSketchView::LoadTempl(LPCTSTR pszFile)
{
	if (m_pViwWell->GetSafeHwnd())
	{
		CString strTempl = pszFile;
		if (m_pViwWell->Load(strTempl))
		{
			strTempl = strTempl.Right(strTempl.GetLength() - strTempl.ReverseFind(_T('\\')) - 1);
			strTempl = _T("[W]") + strTempl.Left(strTempl.GetLength() - 4);
			GetParent()->SetWindowText(strTempl);
		}
	}
}

void CSketchView::SaveAsTempl(LPCTSTR pszFile)
{
	if (m_pViwWell->GetSafeHwnd())
	{
		CString strTempl = pszFile;
		if (strTempl.Right(4).CompareNoCase(_T(".aws")))
			strTempl += _T(".aws");
		m_pViwWell->Save(strTempl);
	}
}

void CSketchView::SaveService()
{
	if (m_pWellPI && ::IsWindow(m_pViwWell->GetSafeHwnd()))
	{
		VARIANT data;
		m_pViwWell->WriteToBuffer(&data);
		m_pWellPI->m_xmlInfo.m_bReadOnly = FALSE;
		m_pWellPI->m_xmlInfo.Back(-1);
		m_pWellPI->m_xmlInfo.Write(_T("WellSketch"), data);
	}
}
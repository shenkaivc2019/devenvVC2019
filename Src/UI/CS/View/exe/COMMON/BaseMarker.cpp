// BaseMarker.cpp: implementation of the CBaseMarker class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "BaseMarker.h"
#include "BaseMark.h"
#include "Track.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBaseMarker::CBaseMarker()
{
	
}

CBaseMarker::~CBaseMarker()
{
	for(int i = 0; i < m_MarkList.GetSize(); i++)
	{
		CMark *pMark = (CMark*)m_MarkList.GetAt(i);
		delete pMark;
	}
	m_MarkList.RemoveAll();
}

void CBaseMarker::AddMark(CBaseMark* pMark)
{
	switch(pMark->m_nShape)
	{
	case MARKS_LINE:
		pMark->m_rect.left = 0;
		pMark->m_rect.right = 30;
		pMark->m_rect.bottom = 30;
		pMark->m_rect.top = 0;
		break;
	case MARKS_TRIANGLE:
		pMark->m_rect.left = 0;
		pMark->m_rect.right = 30;
		pMark->m_rect.bottom = 50;
		pMark->m_rect.top = 0;
		break;
	case MARKS_ARROW:
		pMark->m_rect.left = 0;
		pMark->m_rect.right = 60;
		pMark->m_rect.bottom = 60;
		pMark->m_rect.top = 0;
		break;
	default:
		pMark->m_rect.left = 0;
		pMark->m_rect.right = 30;
		pMark->m_rect.bottom = 30;
		pMark->m_rect.top = 0;
		break;
	}
	m_MarkList.Add(pMark);
}

void CBaseMarker::DrawMark(CDC* pDC, long lStartDepth, long lEndDepth)
{
	for(int i=0; i<m_MarkList.GetSize(); i++)
	{
		CBaseMark* pMark=(CBaseMark*)m_MarkList.GetAt(i);
		pMark->Draw(pDC, lStartDepth, lEndDepth);
	}
}
/***函数：DrawCurveFlag
****功能：绘制曲线标识
****作者：xwh
****创建时间：2013-9-3
****修订记录：XWH2013-9-10注释 由曲线属性框触发绘制曲线标识，原方法暂时注释
****修订记录：无
****/
/*
void CBaseMarker::DrawCurveFlag(CDC* pDC, long lStartDepth, long lEndDepth)
{
	for(int i=0; i<m_pTrack->m_vecCurve.size(); i++)
	{
		CBaseMark* pMark=new CBaseMark;
		pMark->m_pTrack = m_pTrack;
		pMark->DrawCurveFlag(pDC, lStartDepth, lEndDepth,i);
		delete pMark;
	}
}*/
void CBaseMarker::PrintMark(CULPrintInfo* pInfo, float fMinDepth, float fMaxDepth)
{
	for(int i = 0; i < m_MarkList.GetSize(); i++)
	{
		CBaseMark* pMark=(CBaseMark*)m_MarkList.GetAt(i);
		pMark->Print(pInfo, fMinDepth, fMaxDepth);
	}
}

CBaseMark* CBaseMarker::HitTest(CPoint pt, CCurve* pCurve)
{
	for(int i = 0 ; i < m_MarkList.GetSize() ; i ++)
	{
		CBaseMark *pMark = (CBaseMark*)m_MarkList.GetAt(i);
		CRect rcMark = MarkInRect(pMark);
		long temp = pt.x - rcMark.left;
		rcMark.left += temp;
		rcMark.right += temp;
		rcMark.NormalizeRect();
		if(rcMark.PtInRect(pt))
			return pMark ;
	}
	return NULL;
}
/***函数：HitTest
****功能：判断箭头是否在点击区域
****作者：xwh
****创建时间：2013-9-4
****修订记录：无
****/
CBaseMark* CBaseMarker::HitTestArrow(CPoint pt, CCurve* pCurve)
{
	for(int i = 0 ; i < m_MarkList.GetSize() ; i ++)
	{
		CBaseMark *pMark = (CBaseMark*)m_MarkList.GetAt(i);
		CRect rcArrow = MarkInArrow(pMark);	
		rcArrow.NormalizeRect();
		if (rcArrow.PtInRect(pt))
			return pMark;
	}
	return NULL;
}
/***函数：HitTestCurveFlag
****功能：判断曲线标识箭头是否在点击区域
****作者：xwh
****创建时间：2013-9-4
****修订记录：无
****/

CBaseMark* CBaseMarker::HitTestCurveFlag(CPoint pt, CCurve* pCurve)
{
	long lCurDepth;
	CPoint lCurPt;
	double PI = 3.1415926;
	for(int i = 0 ; i < m_MarkList.GetSize() ; i ++)
	{
		CBaseMark *pMark = (CBaseMark*)m_MarkList.GetAt(i);
		if (pMark->m_Arr_lDepthFlag.GetSize()<1)
		{
			continue;
		}
		for (int k=0;k<pMark->m_Arr_lDepthFlag.GetSize();k++)
		{
			lCurDepth = pMark->m_Arr_lDepthFlag.GetAt(k);
			lCurPt.y = m_pTrack->GetCoordinateFromDepth(lCurDepth);
			if ((pt.y>lCurPt.y+20)||(pt.y<lCurPt.y-20))
			{
				continue;
			}
			for (int j=0;j<m_pTrack->m_vecCurve.size();j++)
			{
				if (!strcmp(pMark->m_strName,m_pTrack->m_vecCurve.at(j)->m_strName))
				{
					lCurPt.x = m_pTrack->m_vecCurve.at(j)->GetCurveXPos(lCurDepth);//预测有曲线名相同的BUG
					break;
				}
			}
			if ((pt.x>lCurPt.x+15)||(pt.x<lCurPt.x-15))
			{
				continue;
			}
// 			lCurDepth = m_pTrack->GetDepthFromCoordinate(pt.y);
			pMark->m_nIndex = k;
// 			pMark->m_Arr_lDepthFlag.RemoveAt(k);
// 			pMark->m_Arr_lDepthFlag.InsertAt(k,lCurDepth,1);
			return pMark;
		}
		
	}
	return NULL;
}
/***函数：HitTestCurveFlagText
****功能：判断曲线标识文字是否在点击区域
****作者：xwh
****创建时间：2013-9-4
****修订记录：无
****/

CBaseMark* CBaseMarker::HitTestCurveFlagText(CPoint pt, CCurve* pCurve)
{
	CPoint lCurPt;
	double PI = 3.1415926;
	CRect RectFlagText;
	for(int i = 0 ; i < m_MarkList.GetSize() ; i ++)
	{
		CBaseMark *pMark = (CBaseMark*)m_MarkList.GetAt(i);
		if (pMark->m_Array_point.GetSize()<1)
		{
			continue;
		}
		for (int k=0;k<pMark->m_Array_point.GetSize();k++)
		{
			RectFlagText.left = pMark->m_Array_point.GetAt(k).x*m_pTrack->m_fRatioX;
			RectFlagText.right = pMark->m_Array_point.GetAt(k).x*m_pTrack->m_fRatioX+40;
			long lTemp = pMark->m_Array_point.GetAt(k).y;
			RectFlagText.bottom = m_pTrack->GetCoordinateFromDepth(lTemp) - 80;
			RectFlagText.top = m_pTrack->GetCoordinateFromDepth(lTemp) + 80;
			RectFlagText.NormalizeRect();
			if (RectFlagText.PtInRect(pt))
			{
				pMark->m_nIndexText = k;
				return pMark;
			}
			
		}
		
	}
	return NULL;
}
/***函数：MarkInArrow
****功能：判断箭头是否在点击区域
****作者：xwh
****创建时间：2013-9-4
****修订记录：无
****/
CRect CBaseMarker::MarkInArrow(CBaseMark * pMark)
{
	CRect rcArrow;
	long lCalDepthOffSet,lDepthOffSet =0.0;
	lCalDepthOffSet = pMark->ComputeDepthOffSet(0,0);
	CTrack* pTrack = m_pTrack;
	
	int nXPos = pMark->m_fMarkXCoordinate * m_pTrack->m_fRatioX ;
	int nXRawPos = pMark->m_fRawXCoordinate * m_pTrack->m_fRatioX;
	
	double fCurLeftMargin = m_pTrack->RegionLeft();
	double fCurRightMargin = m_pTrack->RegionRight();
	if(fCurLeftMargin != pMark->m_fLeftMargin || fCurRightMargin != pMark->m_fRightMargin)
	{
		double f = (nXPos - pMark->m_fLeftMargin) / (pMark->m_fRightMargin - pMark->m_fLeftMargin);
		nXPos = f * (fCurRightMargin - fCurLeftMargin) + fCurLeftMargin;
		
		f = (nXRawPos - pMark->m_fLeftMargin) / (pMark->m_fRightMargin - pMark->m_fLeftMargin);
		nXRawPos = f * (fCurRightMargin - fCurLeftMargin) + fCurLeftMargin;
	}
	//	lDepthOffSet = m_lDepth - lCalDepthOffSet;
	lDepthOffSet = pMark->m_lRawDepth - lCalDepthOffSet;
	int nYRawPos = m_pTrack->GetCoordinateFromDepth(lDepthOffSet);
	rcArrow = CRect(nXRawPos - 20, nYRawPos + 15, nXRawPos + 20, nYRawPos - 15);
	return rcArrow;	
}
CRect CBaseMarker::MarkInRect(CBaseMark * pMark)
{
	CRect rcMark;
	
	CTrack* pTrack = m_pTrack;
	
	pTrack->GetRegionRect(rcMark, FALSE);
	int nLeftMargin = rcMark.left;
	int nRightMargin = rcMark.right;
	int nPos =  pTrack->GetCoordinateFromDepth(pMark->m_lDepth);
	rcMark.left = nLeftMargin;
	rcMark.right = rcMark.left + abs(pMark->m_rect.right - pMark->m_rect.left);
	rcMark.bottom = nPos - abs((pMark->m_rect.bottom - pMark->m_rect.top)/2);
	rcMark.top  = nPos + abs((pMark->m_rect.bottom - pMark->m_rect.top)/2);
	
	return rcMark;	
}

void CBaseMarker::DeleteMark(CBaseMark* pMark)
{
	int nSize = m_MarkList.GetSize();
	for (int i = nSize - 1; i >= 0; i--)
	{
		CBaseMark* p = (CBaseMark*)m_MarkList.GetAt(i);
		if (p == pMark)
		{
			m_MarkList.RemoveAt(i);
			delete pMark;
			pMark = NULL;
		}
	}
}	

void CBaseMarker::RemoveAll()
{
	int nSize = m_MarkList.GetSize();
	for (int i = nSize - 1; i >= 0; i--)
	{
		CBaseMark* p = (CBaseMark*)m_MarkList.GetAt(i);
		if (p->m_bRealTime)
		{
			m_MarkList.RemoveAt(i);
			delete p;
			p = NULL;
		}
	}
}

void CBaseMarker::Serialize(CXMLSettings& xml)
{
	if (xml.IsStoring())
	{
		int nCount = m_MarkList.GetSize();
		xml.Write(_T("TrackMarkCount"), nCount);
		for (int i = 0; i < nCount; i++)
		{
			CString strMark;
			strMark.Format("TrackMark%d", i);
			if (xml.CreateKey(strMark))
			{
				CBaseMark* pMark = (CBaseMark*)m_MarkList.GetAt(i);
				pMark->Serialize(xml);
				xml.Back();
			}

		}
	}
	else
	{
		int nCount = 0;
		xml.Read(_T("TrackMarkCount"), nCount);
		for (int i = 0; i < nCount; i++)
		{
			CString strMark;
			strMark.Format("TrackMark%d", i);
			if (xml.Open(strMark))
			{
				CBaseMark* pMark = new CBaseMark;
				pMark->m_pTrack = m_pTrack;
				pMark->Serialize(xml);
				m_MarkList.Add(pMark);	
				xml.Back();
			}
		}
	}
}


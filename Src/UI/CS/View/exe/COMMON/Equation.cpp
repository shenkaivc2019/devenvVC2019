// Equation.cpp: implementation of the CEquation class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Equation.h"
#include <math.h>
#include "resource.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// CParameter Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CParameter, CObject, 0)

CParameter::CParameter(BOOL bVariable /*=FALSE*/)
{
    m_bVariable = bVariable; 
    m_strName = "New Parameter"; 
    m_dValue = 0; 
}

CParameter::~CParameter()
{

}

void CParameter::Serialize(CArchive& ar)
{
    if(ar.IsStoring())
    {
        ar << m_bVariable; 
        ar << m_strName; 
        ar << m_dValue; 
    }
    else
    {
        ar >> m_bVariable; 
        ar >> m_strName; 
        ar >> m_dValue; 
    }
}

void CParameter::Serialize(CXmlArchive& xml, LPCTSTR lpszKeyName /*=NULL*/)
{
    CString strKey = (lpszKeyName == NULL) ? _T("Parameter") : lpszKeyName; 
    if(xml.IsStoring())
    {
        xml.WriteKey(strKey); 
        xml.WriteValue(_T("IsVariable"), (DWORD)m_bVariable); 
        xml.WriteValue(_T("Name"), m_strName); 
        xml.WriteValue(_T("Value"), m_dValue); 
        xml.WriteKey(strKey, FALSE); 
    }
    else 
    {
        xml.ReadKey(strKey); 
        xml.ReadValue(_T("IsVariable"), (DWORD*)&m_bVariable); 
        xml.ReadValue(_T("Name"), m_strName); 
        xml.ReadValue(_T("Value"), &m_dValue); 
        xml.ReadKey(strKey, FALSE); 
    }
}

CParameter* CParameter::Clone()
{
    CParameter* pParam = new CParameter(m_bVariable); 
    pParam->m_strName = m_strName; 
    pParam->m_dValue = m_dValue; 
    return pParam; 
}


//////////////////////////////////////////////////////////////////////
// COperator Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// 运算符表示
TCHAR _ulOperatorTextCollection[OT_MAX][10] = 
{
    _T("+"), 
    _T("-"),
    _T("*"), 
    _T("/"),
    _T("pow"),
    _T("lg"), 
    _T("ln"),
    _T("("), 
    _T(")"),
    _T("#"),
}; 

// 运算符优先级
int _ulOperatorPrecede[OT_MAX][2] = 
{
    {100,   100},     //'+'
    {100,   100},     //'-'
    {200,   200},     //'*'
    {200,   200},     //'/'
    {300,   300},     //'pow'
    {300,   300},     //'lg'
    {300,   300},     //'ln'
    {0,     400},     //'('
    {0,     0},       //')'
    {-1,    -1},      //'#'
}; 

IMPLEMENT_SERIAL(COperator, CObject, 0)

COperator::COperator(UINT nType /*=OT_PLUS*/)
{
    m_nOpType = nType; 
    m_strText = _ulOperatorTextCollection[nType]; 
    m_nLeftPrecede = _ulOperatorPrecede[nType][0]; 
    m_nRightPrecede = _ulOperatorPrecede[nType][1]; 
}

COperator::~COperator()
{

}

double COperator::Calculate(double a, double b /*=1*/)
{
    double dRes = 0; 
    switch(m_nOpType)
    {
        case OT_PLUS:
            dRes = a + b; 
            break; 
        case OT_MINUS:
            dRes = a - b; 
            break; 
        case OT_MULTIPLY:
            dRes = a * b; 
            break; 
        case OT_DIVIDE:
            if(b != 0)
                dRes = a / b; 
            break; 
        case OT_POWER:
            if(a != 0)
                dRes = ::pow(a, b); 
            break; 
        case OT_LOGARITHM_10:
            if(a > 0)
                dRes = ::log10(a); 
            break; 
        case OT_LOGARITHM_E:
            if(a > 0)
                dRes = ::log(a); 
            break; 
    }
    return dRes; 
}

int COperator::GetParamCount()
{
    int nRes = 2; 
    switch(m_nOpType)
    {
        case OT_PLUS:
        case OT_MINUS:
        case OT_MULTIPLY:
        case OT_DIVIDE:
        case OT_POWER:
            nRes = 2; 
            break; 
        case OT_LEFT_BRACKET:
        case OT_RIGHT_BRACKET:
        case OT_BOUNDARY:
            nRes = 0; 
            break; 
        case OT_LOGARITHM_10:
        case OT_LOGARITHM_E:
            nRes = 1; 
            break; 
    }
    return nRes; 
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CEquation, CObject, 0)

CEquation::CEquation(LPCTSTR lpszText /*=NULL*/)
{
    for(int i=0; i<OT_MAX - 1; i++)
    {
        COperator* pOperator = new COperator(i); 
        m_listOperator.AddTail(pOperator); 
    }

    m_strText = (lpszText == NULL) ? _T("") : lpszText; 
    SetText(m_strText); 
}

CEquation::~CEquation()
{
    while (m_listParam.GetCount())
        delete m_listParam.RemoveHead();

    while (m_listItem.GetCount())
        delete m_listItem.RemoveHead();

    while (m_listOperator.GetCount())
        delete m_listOperator.RemoveHead();
}

void CEquation::Serialize(CArchive& ar)
{
    if(ar.IsStoring())
    {
        ar << m_strText; 
        ar << m_listParam.GetCount(); 
        POSITION pos = m_listParam.GetHeadPosition(); 
        while(pos != NULL)
        {
            CParameter* pParam = (CParameter*)m_listParam.GetNext(pos); 
            pParam->Serialize(ar); 
        }
    }
    else 
    {
        ar >> m_strText; 
        UINT nCount = 0; 
        ar >> nCount; 
        
        POSITION pos = m_listParam.GetHeadPosition(); 
        while(pos != NULL)
            delete m_listParam.GetNext(pos); 
        m_listParam.RemoveAll(); 

        for(UINT i=0; i<nCount; i++)
        {
            CParameter* pParam = new CParameter(); 
            pParam->Serialize(ar); 
            m_listParam.AddTail(pParam); 
        }

        SetText(m_strText); 
    }
}

void CEquation::Serialize(CXmlArchive& xml, LPCTSTR lpszKeyName /*=NULL*/)
{
    CString strKey = (lpszKeyName == NULL) ? _T("Equation") : lpszKeyName; 
    if(xml.IsStoring())
    {
        xml.WriteKey(strKey); 
        xml.WriteValue(_T("Text"), m_strText); 
        xml.WriteValue(_T("ParameterCount"), (DWORD)m_listParam.GetCount()); 
        POSITION pos = m_listParam.GetHeadPosition(); 
        while(pos != NULL)
        {
            CParameter* pParam = (CParameter*)m_listParam.GetNext(pos); 
            pParam->Serialize(xml); 
        }
        
        xml.WriteKey(strKey, FALSE); 
    }
    else 
    {
        xml.ReadKey(strKey); 
        xml.ReadValue(_T("Text"), m_strText); 
        UINT nCount = 0; 
        xml.ReadValue(_T("ParameterCount"), (DWORD*)&nCount); 

        POSITION pos = m_listParam.GetHeadPosition(); 
        while(pos != NULL)
            delete m_listParam.GetNext(pos); 
        m_listParam.RemoveAll(); 

        for(UINT i=0; i<nCount; i++)
        {
            CParameter* pParam = new CParameter(); 
            m_listParam.AddTail(pParam); 
            pParam->Serialize(xml); 
        }
        xml.ReadKey(strKey, FALSE); 
        SetText(m_strText); 
    }
}

CParameter* CEquation::GetVariable()
{
    POSITION pos = m_listParam.GetHeadPosition(); 
    while(pos != NULL)
    {
        CParameter* pParam = (CParameter*)m_listParam.GetNext(pos);
        if(pParam->m_bVariable) // exists
            return pParam; 
    }
    return NULL; 
}

void CEquation::AddParam(CParameter* pParam)
{
    if(pParam->m_bVariable && GetVariable() != NULL)
        pParam->m_bVariable = FALSE; 

    m_listParam.AddTail(pParam); 
}

BOOL CEquation::SetText(LPCTSTR lpszText/*=NULL*/, BOOL bSilence /*=TRUE*/)
{
    if(lpszText != NULL)
        m_strText = lpszText; 

    TRY
    {
        m_bSuccess = Parse(); 
    }
    CATCH_ALL(e)
    {
        if(!bSilence)
            e->ReportError(); 
        e->Delete(); 
        m_bSuccess = FALSE; 
    }
    END_CATCH_ALL

    return m_bSuccess; 
}

BOOL CEquation::Parse()
{
    POSITION pos = m_listItem.GetHeadPosition(); 
    while(pos != NULL)
        delete m_listItem.GetNext(pos); 
    m_listItem.RemoveAll(); 
    m_listItem.AddTail(new COperator(OT_BOUNDARY)); 

    CString strText = m_strText; 
    int nPos = strText.Find('='); // 过滤等号
    if(nPos >= 0) 
        strText = strText.Right(strText.GetLength() - nPos - 1); 
    else 
        nPos = 0; 

    while(strText.GetLength() > 0)
    {
        CString strParam; 
        UINT nType = OT_PLUS; 
        COperator* pOperator = NULL; 
        int nStart = GetNextOperator(strText, &nType); 
        if(nStart == strText.GetLength())
        {
            strParam = strText; 
            strText = ""; 
            nPos += nStart; 
        }
        else 
        {
            int nLen = 1; // 运算符长度
            strParam = strText.Left(nStart); 
            pOperator = new COperator(nType); 
            nLen = pOperator->m_strText.GetLength(); 
            strText = strText.Right(strText.GetLength() - nStart - nLen); 
            nPos += nStart + nLen; 
        }

        strParam.TrimLeft(); 
        strParam.TrimRight(); 
        CParameter* pParam = NULL; 
        UINT nLen = strParam.GetLength(); 
        if(nLen > 0)
        {
            LPTSTR lpszValue = new TCHAR[nLen + 1]; 
            lstrcpyn(lpszValue, strParam, nLen + 1); 
            LPTSTR lpszOld = lpszValue; // lpszValue will change in 'strtod'
            double dValue = _tcstod(lpszValue, &lpszValue);

            if(*lpszValue != '\0') // conversion failure
            {
                // 非数字
                pParam = GetParameter(strParam); 
                if(pParam == NULL)
                {                    
                    if(pOperator != NULL)
                    {
                        delete pOperator;
                        pOperator = NULL; 
                    }
                    delete lpszOld; 
    
                    CString strWarning; // _T("位置[%d]: 无法识别的符号[%s].")
                    strWarning.LoadString(IDS_CROSSERROR_UNRECOGNISE_PARAMETER); 
                    CString strMessage; 
                    strMessage.Format(strWarning, nPos, strParam); 
                    THROW(new CCrossException(strMessage)); 
                }
            }
            else 
            {
                pParam = new CParameter(); 
                pParam->m_strName = strParam; 
                pParam->m_dValue = dValue; 
            }

            delete lpszOld; 
        }

        if(pParam != NULL)
            m_listItem.AddTail(pParam); 

        if(pOperator != NULL && pOperator->m_nOpType == OT_MINUS)
        {
            // 负号判断, 将 -xxx 转化为 -1 * xxx
            CObject* pObject = NULL; 
            if(m_listItem.GetCount() > 0)
                pObject = m_listItem.GetTail(); 

            BOOL bConvert = FALSE;
            if(pObject == NULL)
                bConvert = TRUE; 
            else if(pObject->IsKindOf(RUNTIME_CLASS(COperator)))
            {
                COperator* pOp = (COperator*)pObject; 
                if(pOp->m_nOpType != OT_RIGHT_BRACKET)
                    bConvert = TRUE; 
            }

            if(bConvert)
            {
                CParameter* pAdded = new CParameter(); 
                pAdded->m_dValue = -1; 
                pAdded->m_strName = "-1"; 
                m_listItem.AddTail(pAdded); 

                delete pOperator; 
                pOperator = new COperator(OT_MULTIPLY); 
            }
        }

        if(pOperator != NULL)
            m_listItem.AddTail(pOperator);
    }

    m_listItem.AddTail(new COperator(OT_BOUNDARY)); 
    if(m_listItem.GetCount() <= 2)
    {
        CString strError; // _T("方程中不包含任何有效元素.")
        strError.LoadString(IDS_CROSSERROR_EMPTY_EQUATION); 
        THROW(new CCrossException(strError)); 
    }

    return TRUE; 
}

int CEquation::GetNextOperator(CString strText, UINT* nType)
{
    int nStart = strText.GetLength(); 
    POSITION pos = m_listOperator.GetHeadPosition(); 
    while(pos != NULL)
    {
        COperator* pOperator = (COperator*)m_listOperator.GetNext(pos);
        int nPos = strText.Find(pOperator->m_strText); 
        if(nPos >= 0 && nPos < nStart)
        {
            nStart = nPos; 
            *nType = pOperator->m_nOpType; 
        }
    }
    return nStart; 
}

CParameter* CEquation::GetParameter(CString strName)
{
    POSITION pos = m_listParam.GetHeadPosition(); 
    while(pos != NULL)
    {
        // 构造一个克隆对象返回，防止析构时两次删除同一对象。
        CParameter* pParam = (CParameter*)m_listParam.GetNext(pos); 
        if(pParam->m_strName == strName)
            return pParam->Clone(); 
    }

    return NULL; 
}

CString CEquation::GetText()
{
    CString strRes = ""; 
    if(!m_bSuccess)
        return strRes; 

    POSITION pos = m_listItem.GetHeadPosition(); 
    while(pos != NULL)
    {
        CObject* pObject = m_listItem.GetNext(pos); 
        if(pObject->IsKindOf(RUNTIME_CLASS(CParameter)))
        {
            CParameter* pParam = (CParameter*)pObject; 
            strRes += pParam->m_strName + " "; 
        }
        else if(pObject->IsKindOf(RUNTIME_CLASS(COperator)))
        {
            COperator* pOperator = (COperator*)pObject; 
            strRes += pOperator->m_strText + " "; 
        }
    }

    return strRes; 
}

double CEquation::Calculate(double x)
{
    if(!m_bSuccess)
        return 0; 

    CObList m_stackParam;         // 参数栈
    CObList m_stackOp;            // 运算符栈

    BOOL bGetNext = TRUE; 
    CObject* pItem = NULL; 
    POSITION pos = m_listItem.GetHeadPosition(); 
    while(pos != NULL || m_stackOp.GetCount() > 0)
    {
        if(bGetNext && pos != NULL)
            pItem = m_listItem.GetNext(pos); 
        
        bGetNext = TRUE;
        if(pItem->IsKindOf(RUNTIME_CLASS(COperator)))
        {
            COperator* pOpNext = (COperator*)pItem;
            COperator* pOpNow = NULL; 
            if(m_stackOp.GetCount() > 0)
                pOpNow = (COperator*)m_stackOp.GetTail(); 
            
            if(pOpNow == NULL)
                m_stackOp.AddTail(pOpNext); 
            else 
            {
                if(pOpNext->m_nRightPrecede > pOpNow->m_nLeftPrecede)
                    m_stackOp.AddTail(pOpNext); 
                else 
                {
                    bGetNext = FALSE; 
                    int nNum = pOpNow->GetParamCount(); 
                    if(m_stackParam.GetCount() < nNum)
                        return 0; 

                    if(nNum == 0)
                        bGetNext = TRUE; 
                    if(nNum == 1)
                    {
                        CParameter* pParam = (CParameter*)m_stackParam.GetTail(); 
                        pParam->m_dValue = pOpNow->Calculate(pParam->m_dValue); 
                    }
                    else if(nNum == 2)
                    {
                        CParameter* pParam1 = (CParameter*)m_stackParam.RemoveTail(); 
                        CParameter* pParam2 = (CParameter*)m_stackParam.GetTail(); 
                        pParam2->m_dValue = pOpNow->Calculate(pParam2->m_dValue, pParam1->m_dValue); 
                        delete pParam1; 
                    }
                    m_stackOp.RemoveTail(); 
                }
            }
        }
        else if(pItem->IsKindOf(RUNTIME_CLASS(CParameter)))
        {
            CParameter* pParam = (CParameter*)pItem; 
            if(pParam->m_bVariable)
                pParam->m_dValue = x; 

            // 克隆对象，防止修改原参数值
            m_stackParam.AddTail(pParam->Clone()); 
        }
    }

    double dRes = 0; 
    if(m_stackParam.GetCount() > 0)
    {
        CParameter* pParam = (CParameter*)m_stackParam.GetTail(); 
        dRes = pParam->m_dValue; 
    }

    pos = m_stackParam.GetHeadPosition(); 
    while(pos != NULL)
        delete m_stackParam.GetNext(pos); 

    return dRes; 
}
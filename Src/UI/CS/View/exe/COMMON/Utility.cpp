#include "stdafx.h"
#include "Utility.h"

BOOL EanbleDlgItem(CWnd* pDlg, int nID, BOOL bEnable)
{
	if (pDlg->GetSafeHwnd() == NULL)
		return FALSE;
	CWnd* pWnd = pDlg->GetDlgItem(nID);
	if (pWnd != NULL)
	{
		pDlg->EnableWindow(bEnable);
		return TRUE;
	}
	return FALSE;
}

int GetDiskFreeM(CString& strPath)
{
	int nPos = strPath.Find(_T(":"));
	if (nPos == -1)
		return -1;
	CString strRootPath = strPath.Left(nPos + 2);

	__int64 i64FreeBytesToCaller, i64TotalBytes, i64FreeBytes;
	BOOL fResult;
	fResult = GetDiskFreeSpaceEx(strRootPath,
				(PULARGE_INTEGER) & i64FreeBytesToCaller,
				(PULARGE_INTEGER) & i64TotalBytes,
				(PULARGE_INTEGER) & i64FreeBytes);

	int iFreeM = i64FreeBytes / (1024 * 1024);
	return iFreeM;
}

float GetDiskFreeG(CString& strPath)
{
	int nPos = strPath.Find(_T(":"));
	if (nPos == -1)
		return -1;
	CString strRootPath = strPath.Left(nPos + 2);

	__int64 i64FreeBytesToCaller, i64TotalBytes, i64FreeBytes;
	BOOL fResult;
	fResult = GetDiskFreeSpaceEx(strRootPath,
				(PULARGE_INTEGER) & i64FreeBytesToCaller,
				(PULARGE_INTEGER) & i64TotalBytes,
				(PULARGE_INTEGER) & i64FreeBytes);

	int iFreeM = i64FreeBytes / (1024 * 1024); 
	float fFreeG = iFreeM / 1024.0;  
	return fFreeG;
}

BOOL SimpleScanf(LPCTSTR lpszText, LPCTSTR lpszFormat, void* pData)
{
	ASSERT(lpszText != NULL);
	ASSERT(lpszFormat != NULL);

	ASSERT(*lpszFormat == '%');
	lpszFormat++;   	 // skip '%'

	BOOL bLong = FALSE;
	BOOL bShort = FALSE;
	BOOL bWord = FALSE;
	if (*lpszFormat == 'l')
	{
		bLong = TRUE;
		lpszFormat++;
	}
	else if (*lpszFormat == 's')
	{
		bShort = TRUE;
		bWord = TRUE;
		lpszFormat++;
	}

	ASSERT(*lpszFormat == 'd' || *lpszFormat == 'u');
	ASSERT(lpszFormat[1] == '\0');

	while (*lpszText == ' ' || *lpszText == '\t')
		lpszText++;
	TCHAR chFirst = lpszText[0];
	long l, l2;
	if (*lpszFormat == 'd')
	{
		// signed
		l = _tcstol(lpszText, (LPTSTR *) &lpszText, 10);
		l2 = (int) l;
		bWord = FALSE;
	}
	else
	{
		// unsigned
		if (*lpszText == '-')
			return FALSE;
		l = (long) _tcstoul(lpszText, (LPTSTR *) &lpszText, 10);
		l2 = (unsigned int) l;
		bShort = FALSE;
	}
	if (l == 0 && chFirst != '0')
		return FALSE;   // could not convert

	while (*lpszText == ' ' || *lpszText == '\t')
		lpszText++;
	if (*lpszText != '\0')
		return FALSE;   // not terminated properly

	if (bShort)
	{
		if ((short) l != l)
			return FALSE;   // too big for short
		*(short *) pData = (short) l;
	}
	else if (bWord)
	{
		if ((WORD) l != l)
			return FALSE;
		*(WORD *) pData = (WORD) l;
	}
	else
	{
		ASSERT(sizeof(long) == sizeof(int));
		ASSERT(l == l2);
		*(long *) pData = l;
	}

	// all ok
	return TRUE;
}

BOOL SimpleFloatParse(LPCTSTR lpszText, double& d)
{
	ASSERT(lpszText != NULL);
	while (*lpszText == ' ' || *lpszText == '\t')
		lpszText++;

	TCHAR chFirst = lpszText[0];
	d = _tcstod(lpszText, (LPTSTR *) &lpszText);
	if (d == 0.0 && chFirst != '0')
		return FALSE;   // could not convert
	while (*lpszText == ' ' || *lpszText == '\t')
		lpszText++;

	if (*lpszText != '\0')
		return FALSE;   // not terminated properly

	return TRUE;
}

inline int roundleast(int n)
{
	int mod = n % 10;
	n -= mod;
	if (mod >= 5)
		n += 10;
	else if (mod <= -5)
		n -= 10;
	return n;
}

void RoundRect(LPRECT r1)
{
	r1->left = roundleast(r1->left);
	r1->right = roundleast(r1->right);
	r1->top = roundleast(r1->top);
	r1->bottom = roundleast(r1->bottom);
}

void RoundInt(int& n)
{
	n = roundleast(n);
}

void MulDivInt(int& n1, int& n2, int num, int div)
{
	n1 = MulDiv(n2, num, div);
}

void MulDivRect(LPRECT r1, LPRECT r2, int num, int div)
{
	r1->left = MulDiv(r2->left, num, div);
	r1->top = MulDiv(r2->top, num, div);
	r1->right = MulDiv(r2->right, num, div);
	r1->bottom = MulDiv(r2->bottom, num, div);
}

/////////////////////////////////////////
// 厘米与缇的转换
int TwipsToMetrics(int nT, int nLog)
{
	return MulDiv(nT, nLog, 1440);
}

int MetricsToTwips(int nR, int nLog)
{
	return MulDiv(nR, 1440, nLog);
}

BOOL IsDecimal(LPCTSTR strDecimal)
{
	CString strCheck = strDecimal;
	if (strCheck.IsEmpty())
		return FALSE;

	BOOL bZero = FALSE;
	TCHAR* stop;
	if (_tcstod(strDecimal, &stop) == 0.0)
		bZero = TRUE;

	if (bZero && strCheck[0] != '0')
		return FALSE;

	BOOL bDot = FALSE;
	for (int i = 0; i < strCheck.GetLength(); i++)
	{
		if ((strCheck[i] < '0' || strCheck[i] > '9') &&
			strCheck[i] != '.' &&
			strCheck[i] != '-')
			return FALSE;
		if (strCheck[i] == '-' && i > 0)
			return FALSE;
		if (strCheck[i] == '.')
		{
			if (bDot)
				return FALSE;
			else
				bDot = TRUE;
		}
	}

	return TRUE;
}

CString SplitString(CString& str, char ch)
{
	CString strResult;

	int nPos = -1;
	nPos = str.Find(ch);
	if (nPos == -1)
	{
		strResult = str;
		str.Empty();
	}
	else
	{
		strResult = str.Left(nPos);
		str = str.Right(str.GetLength() - nPos - 1);
	}

	return strResult;
}

void SplitString(CString str, char ch, CStringArray& arr)
{
	CString strResult;

	int nPos = -1;
	nPos = str.Find(ch);
	while (nPos != -1)
	{
		strResult = str.Left(nPos);
		str = str.Right(str.GetLength() - nPos - 1);
		arr.Add(strResult);
		nPos = str.Find(ch);
	}
	arr.Add(str);
}

BOOL SplitPath(LPCTSTR lpszPath, CStringArray& strArray)
{
	strArray.RemoveAll();

	CString strPath = lpszPath;
	int iPathLen = strPath.GetLength();
	if (iPathLen < 1)
		return TRUE;

	if (strPath[iPathLen - 1] != _T('\\'))
	{
		strPath += _T('\\');
	}

	for (int iFrom = 0; iFrom < strPath.GetLength();)
	{
		int iEnd = strPath.Find(_T('\\'), iFrom);
		if (iEnd < 0)
			break;

		CString strSubKey = strPath.Mid(iFrom, iEnd - iFrom);
		if (strSubKey.GetLength())
			strArray.Add(strSubKey);
		iFrom = iEnd + 1;
	}

	return TRUE;
}

BOOL PeekAndPump()
{
	static MSG msg;

	while (::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
	{
		if (!AfxGetApp()->PumpMessage())
		{
			::PostQuitMessage(0);
			return FALSE;
		}
	}

	return TRUE;
}

int SaveBitmapToFile(HBITMAP hBitmap, LPCTSTR lpFileName)
{
	HDC hDC;	   //设备描述表
	int iBits;     //当前显示分辨率下每个像素所占字节数
	WORD wBitCount; //位图中每个像素所占字节数

	//定义调色板大小,位图中像素字节大小,位图文件大小,写入文件字节数
	DWORD dwPaletteSize = 0, dwBmBitsSize, dwDIBSize, dwWritten;

	BITMAP Bitmap;  			 //位图属性结构
	BITMAPFILEHEADER bmfHdr;   //位图文件头结构
	BITMAPINFOHEADER bi;	   //位图信息头结构 
	LPBITMAPINFOHEADER lpbi;	 //指向位图信息头结构
	HANDLE fh, hDib, hPal, hOldPal = NULL; //定义文件，分配内存句柄，调色板句柄

	//计算位图文件每个像素所占字节数
	hDC = CreateDC(_T("DISPLAY"), NULL, NULL, NULL);
	iBits = GetDeviceCaps(hDC, BITSPIXEL) * GetDeviceCaps(hDC, PLANES);
	DeleteDC(hDC);
	if (iBits <= 1)
		wBitCount = 1;
	else if (iBits <= 4)
		wBitCount = 4;
	else if (iBits <= 8)
		wBitCount = 8;
	else if (iBits <= 24)
		wBitCount = 24;
	//计算调色板大小
	if (wBitCount <= 8)
		dwPaletteSize = (1 << wBitCount) * sizeof(RGBQUAD);

	//设置位图信息头结构
	GetObject(hBitmap, sizeof(BITMAP), (LPSTR) & Bitmap);

	bi.biSize = sizeof(BITMAPINFOHEADER);
	bi.biWidth = Bitmap.bmWidth;
	bi.biHeight = Bitmap.bmHeight;
	bi.biPlanes = 1;
	bi.biBitCount = wBitCount;
	bi.biCompression = BI_RGB;
	bi.biSizeImage = 0;
	bi.biXPelsPerMeter = 0;
	bi.biYPelsPerMeter = 0;
	bi.biClrUsed = 0;
	bi.biClrImportant = 0;

	dwBmBitsSize = ((Bitmap.bmWidth * wBitCount + 31) / 32) * 4 * Bitmap.bmHeight ;
	//为位图内容分配内存
	hDib = GlobalAlloc(GHND,
			dwBmBitsSize + dwPaletteSize + sizeof(BITMAPINFOHEADER));
	lpbi = (LPBITMAPINFOHEADER) GlobalLock(hDib);
	*lpbi = bi;
	// 处理调色板   
	hPal = GetStockObject(DEFAULT_PALETTE);
	if (hPal)
	{
		hDC = ::GetDC(NULL);
		hOldPal = SelectPalette(hDC, (HPALETTE) hPal, FALSE);
		RealizePalette(hDC);
	}
	// 获取该调色板下新的像素值
	GetDIBits(hDC, hBitmap, 0, (UINT) Bitmap.bmHeight,
		(LPSTR) lpbi + sizeof(BITMAPINFOHEADER) + dwPaletteSize,
		(BITMAPINFO *) lpbi, DIB_RGB_COLORS);
	//恢复调色板   
	if (hOldPal)
	{
		SelectPalette(hDC, (HPALETTE) hOldPal, TRUE);
		RealizePalette(hDC);
		::ReleaseDC(NULL, hDC);
	}
	//创建位图文件    
	fh = CreateFile(lpFileName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
			FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
	if (fh == INVALID_HANDLE_VALUE)
		return FALSE;
	// 设置位图文件头
	bmfHdr.bfType = 0x4D42;  // "BM"
	dwDIBSize = sizeof(BITMAPFILEHEADER) +
		sizeof(BITMAPINFOHEADER) +
		dwPaletteSize +
		dwBmBitsSize;  
	bmfHdr.bfSize = dwDIBSize;
	bmfHdr.bfReserved1 = 0;
	bmfHdr.bfReserved2 = 0;
	bmfHdr.bfOffBits = (DWORD)sizeof(BITMAPFILEHEADER) +
		(DWORD)sizeof(BITMAPINFOHEADER) +
		dwPaletteSize;
	// 写入位图文件头
	WriteFile(fh, (LPSTR) & bmfHdr, sizeof(BITMAPFILEHEADER), &dwWritten, NULL);
	// 写入位图文件其余内容
	WriteFile(fh, (LPSTR) lpbi, dwDIBSize, &dwWritten, NULL);
	//清除   
	GlobalUnlock(hDib);
	GlobalFree(hDib);
	CloseHandle(fh);
	return dwWritten;
}

void RecurseFind(CStringList& lstFiles, LPCTSTR pszPath, LPCTSTR pszFind, int nLevel /*= 0*/)
{
	CFileFind ff;

	// build a string with wildcard
	CString strPath = pszPath;
	strPath += _T("\\");

	// start working for files
	BOOL bWorking = ff.FindFile(strPath + pszFind);

	while (bWorking)
	{
		bWorking = ff.FindNextFile();

		// skip . and .. files; otherwise, we'd
		// recur infinitely!

		if (ff.IsDots())
			continue;

		// if it's a directory, recursively search it
		if (ff.IsDirectory())
			continue;

		lstFiles.AddTail(ff.GetFilePath());
	}

	if (nLevel == 1)
		return;
	
	nLevel--;
	// start working for directory
	bWorking = ff.FindFile(strPath + "*.*");

	while (bWorking)
	{
		bWorking = ff.FindNextFile();

		// skip . and .. files; otherwise, we'd
		// recur infinitely!

		if (ff.IsDots())
			continue;

		// if it's a directory, recursively search it
		if (ff.IsDirectory())
		{
			RecurseFind(lstFiles, ff.GetFilePath(), pszFind, nLevel);
		}
	}
}

CString GetFileName(LPCTSTR pszPathName)
{		
	TCHAR fname[_MAX_FNAME];   
	TCHAR ext[_MAX_EXT];
	_tsplitpath(pszPathName, NULL, NULL, fname, ext);
	return CString(fname);
}

CString GetTempFileName()
{
	//Get the temporary files directory.
	TCHAR szTempPath[MAX_PATH];
	DWORD dwResult = :: GetTempPath(MAX_PATH, szTempPath);
	ASSERT(dwResult);

// 	CStringList lstFiles;
// 	RecurseFind(lstFiles, szTempPath, _T("*.tmp"));
// 	while (lstFiles.GetCount())
// 	{
// 		::DeleteFile(lstFiles.RemoveHead());
// 	}

	//Create a unique temporary file.
	TCHAR szTempFile[MAX_PATH];
	UINT nResult = GetTempFileName(szTempPath, _T("~ex"), 0, szTempFile);

	TCHAR szFullPath[1024];
	ZeroMemory(szFullPath, 1024);
	if (GetLongPathName(szTempFile, szFullPath, 1024) > 0)
		return CString(szFullPath);
	
	return CString(szTempFile);
}

CString GetTempPath()
{
	//Get the temporary files directory.
	TCHAR szTempPath[MAX_PATH];
	DWORD dwResult = :: GetTempPath(MAX_PATH, szTempPath);
	ASSERT(dwResult);
	
	TCHAR szFullPath[1024];
	ZeroMemory(szFullPath, 1024);
	if (GetLongPathName(szTempPath, szFullPath, 1024) > 0)
		return CString(szFullPath);
	
	return CString(szTempPath);
}

//删除文件或者文件夹
bool FileDelete(LPCTSTR lpszPath)
{
	SHFILEOPSTRUCT FileOp = {0};
	FileOp.fFlags = /*FOF_ALLOWUNDO |*/   //允许放回回收站
		FOF_NOCONFIRMATION | FOF_NOERRORUI; //不出现确认对话框
	
	char cPath[1024];
	ZeroMemory(cPath, 1024);
#ifdef _UNICODE
	USES_CONVERSION;
	sprintf(cPath, "%s%s", W2T(lpszPath), "\0\0");
#else
	sprintf(cPath, "%s%s", lpszPath, "\0\0");
#endif	
	
	FileOp.pFrom = cPath;
	FileOp.pTo = NULL;      //一定要是NULL
	FileOp.wFunc = FO_DELETE;    //删除操作
	return SHFileOperation(&FileOp) == 0;
}

//复制文件或文件夹
bool FileCopy(LPCTSTR pFrom, LPCTSTR pTo)
{
	SHFILEOPSTRUCT FileOp = {0};
	FileOp.fFlags = FOF_NOCONFIRMATION|   //不出现确认对话框
		FOF_NOCONFIRMMKDIR | FOF_NOERRORUI; //需要时直接创建一个文件夹,不需用户确定

	char cFrom[1024];
	char cTo[1024];
	ZeroMemory(cFrom, 1024);
	ZeroMemory(cTo, 1024);

#ifdef _UNICODE
	USES_CONVERSION;
	sprintf(cFrom, "%s%s", W2T(pFrom), "\0\0");
	sprintf(cTo, "%s%s", W2T(pTo), "\0\0");
#else
	sprintf(cFrom, "%s%s", pFrom, "\0\0");
	sprintf(cTo, "%s%s", pTo, "\0\0");
#endif	

	FileOp.pFrom = cFrom;
	FileOp.pTo = cTo;
	FileOp.wFunc = FO_COPY;
	return SHFileOperation(&FileOp) == 0;
}

void CopyDir(LPCTSTR src, LPCTSTR dst, BOOL bRecursive /*= FALSE*/)
{ 
	WIN32_FIND_DATA fd;

	TCHAR tmpsrc[256];
	_tcscpy(tmpsrc, src); 
	_tcscat(tmpsrc, "\\*.*"); 
	
	HANDLE hFind = FindFirstFile(tmpsrc, &fd); 
	if (hFind == INVALID_HANDLE_VALUE) 
		return; 
	
	CreateDirectory(dst, 0);
	do
	{ 
		TCHAR newdst[256]; 
		_tcscpy(newdst,dst); 
		if (newdst[strlen(newdst)] != '\\') 
			_tcscat(newdst,"\\"); 
		_tcscat(newdst,fd.cFileName); 
		
		TCHAR newsrc[256]; 
		_tcscpy(newsrc, src); 
		if (newsrc[strlen(newsrc)] != '\\')
			_tcscat(newsrc, "\\");
		_tcscat(newsrc,fd.cFileName); 
		if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{ 
			if (bRecursive)
			{
				if (_tcscmp(fd.cFileName, _T(".")) 
				&& _tcscmp(fd.cFileName, _T(".."))) 
				{ 
					CopyDir(newsrc, newdst, bRecursive);
				}
			}
		}
		else 
		{ 
			CopyFile(newsrc, newdst, FALSE); 
		} 
	}
	while (FindNextFile(hFind, &fd));
	FindClose(hFind);
}
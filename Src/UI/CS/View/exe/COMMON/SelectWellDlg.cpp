// BaseLineCorrectDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "SelectWellDlg.h"
#include "Curve.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectWellDlg dialog


CSelectWellDlg::CSelectWellDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectWellDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectWellDlg)
	//}}AFX_DATA_INIT
    m_pDocSel = NULL; 
}


void CSelectWellDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectWellDlg)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectWellDlg, CDialog)
	//{{AFX_MSG_MAP(CSelectWellDlg)
	ON_NOTIFY(TVN_SELCHANGED, IDC_TREE_WELL, OnTreeSelectChange)
	ON_BN_CLICKED(ID_BUTTON_SELECT, OnBtnSelectWell)
    ON_BN_CLICKED(ID_BUTTON_EXIT, OnBtnExit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectWellDlg message handlers

BOOL CSelectWellDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
    CTreeCtrl* pTree = (CTreeCtrl*)GetDlgItem(IDC_TREE_WELL); 
    if(pTree != NULL)
    {
        CImageList imageList; 
        if(imageList.Create(IDB_CROSSPLOT_SELECT_WELL_IMAGELIST, 16, 1, RGB(255, 0, 255)))
        {
            pTree->SetImageList(&imageList, TVSIL_NORMAL);
            pTree->SetImageList(&imageList, TVSIL_STATE); 
        }
        imageList.Detach(); 

        CString strText; //_T("井集合")
        strText.LoadString(IDS_WELL_COLLECTION); 
        HTREEITEM hRoot = pTree->InsertItem(strText); 
        pTree->SetItemImage(hRoot, 0, 0); 
        POSITION pos = CULDoc::m_DocList.GetHeadPosition(); 
        while(pos != NULL)
        {
            CULDoc* pDoc = CULDoc::m_DocList.GetNext(pos); 
            if(pDoc != NULL)
            {
                CString strFileName = pDoc->GetSimpleName(); 
                if(strFileName.IsEmpty()) // ignore Projects.
                    continue;  

                HTREEITEM hFile = pTree->InsertItem(strFileName, hRoot); 
                pTree->SetItemImage(hFile, 1, 1); 
                pTree->SetItemData(hFile, (DWORD)pDoc); 
                for(int i=0; i<pDoc->m_vecCurve.size(); i++)
                {
                    CCurve* pCurve = (CCurve*)pDoc->m_vecCurve.at(i); 
                    HTREEITEM hCurve = pTree->InsertItem(pCurve->m_strName, hFile);
                    pTree->SetItemImage(hCurve, 2, 2); 
                    pTree->SetItemData(hCurve, (DWORD)pCurve); 
                }
            }
        }
        pTree->SelectItem(hRoot);
        pTree->Expand(hRoot, TVE_EXPAND);
    }

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CSelectWellDlg::OnTreeSelectChange(NMHDR* pNMHDR, LRESULT* pResult)
{
    CString strText; 
    CTreeCtrl* pTree = (CTreeCtrl*)GetDlgItem(IDC_TREE_WELL); 
    if(pTree != NULL)
    {
        HTREEITEM hSel = pTree->GetSelectedItem();
        if(hSel != NULL)
        {
            int nLevel; 
            pTree->GetItemImage(hSel, nLevel, nLevel); 
            BOOL bEnable = TRUE; 
            CString strInfo = "";
            switch(nLevel)
            {
                case 0:
                    bEnable = FALSE; 
                    strInfo = ""; 
                    break; 
                case 1:
                {
                    bEnable = TRUE; 
                    CULDoc* pDoc = (CULDoc*)pTree->GetItemData(hSel); 
                    if(pDoc != NULL)
                    {
                        strText.LoadString(IDS_SELECT_WELL_FILEPATH); //"文件路径:[%s]"
                        strInfo.Format(strText, pDoc->GetPathName()); 
                    }
                    break; 
                }
                case 2:
                    bEnable = FALSE; 
                    CCurve* pCurve = (CCurve*)pTree->GetItemData(hSel); 
                    if(pCurve != NULL)
                    {
                        CString strA, strB; 
                        strText.LoadString(IDS_SELECT_WELL_CURVE_NAME); //"曲线名:[%s]"
                        strA.Format(strText, pCurve->Name()); 
                        strText.LoadString(IDS_SELECT_WELL_CURVE_UNIT); //"曲线单位:[%s]"
                        strB.Format(strText, pCurve->Unit()); 
                        strInfo.Format("%-25s %-25s\r\n", strA, strB); 
                
                        if(pCurve->GetData() != NULL)
                        {
                            int nCount = pCurve->GetDataSize(); 
                            long lMin = pCurve->GetDepth(0);
                            long lMax = pCurve->GetDepth(nCount - 1);                             
                            if(lMin > lMax)
                            {
                                long tmp = lMin; 
                                lMin = lMax; 
                                lMax = tmp; 
                            }
                    
                            CString strMoreInfo; 
                            strText.LoadString(IDS_SELECT_WELL_BEGIN_DEPTH); //"起始深度:[%f]"
                            strA.Format(strText, DTM(lMin)); 
                            strText.LoadString(IDS_SELECT_WELL_END_DEPTH); //"终止深度:[%f]"
                            strB.Format(strText, DTM(lMax)); 
                            strMoreInfo.Format("%-25s %-25s\r\n", strA, strB);  
                            strInfo += strMoreInfo; 
                    
                            double dStep = (nCount > 1) ? DTM(lMax - lMin) / (nCount - 1) : 0; 
                            strText.LoadString(IDS_SELECT_WELL_STEP_DEPTH); //"深度间隔:[%f]"
                            strA.Format(strText, dStep); 
                            strText.LoadString(IDS_SELECT_WELL_SAMPLE_COUNT); //"采样点数:[%d]"
                            strB.Format(strText, nCount); 
                            strMoreInfo.Format("%-25s %-25s\r\n", strA, strB); 
                            strInfo += strMoreInfo; 
                    
                            double dMin = pCurve->GetdblValue(0); 
                            double dMax = dMin; 
                            for(int i=1; i<nCount; i++)
                            {
                                double dValue = pCurve->GetdblValue(i); 
                                if(IsInvalidValue(dValue))
                                    continue; 
                    
                                if(IsInvalidValue(dMin) || dMin > dValue)
                                    dMin = dValue; 
                                else if(IsInvalidValue(dMax) || dMax < dValue)
                                    dMax = dValue; 
                            }
                
                            strText.LoadString(IDS_SELECT_WELL_MIN_VALUE); //"最小值:[%f]"
                            strA.Format(strText, dMin); 
                            strText.LoadString(IDS_SELECT_WELL_MAX_VALUE); //"最大值:[%f]"
                            strB.Format(strText, dMax); 
                            strMoreInfo.Format("%-25s %-25s\r\n", strA, strB); 
                            strInfo += strMoreInfo; 
                        }
                    }
                    break;
            }

            HWND hwndBtnSelWell; 
            GetDlgItem(ID_BUTTON_SELECT, &hwndBtnSelWell); 
            ::EnableWindow(hwndBtnSelWell, bEnable);
            SetDlgItemText(IDC_EDIT_INFO, strInfo); 
        }
    }
}

inline BOOL CSelectWellDlg::IsInvalidValue(double value)
{
    return (value == -9999 || value == -999.25 || value == -32767); 
}

void CSelectWellDlg::OnBtnSelectWell()
{
    CTreeCtrl* pTree = (CTreeCtrl*)GetDlgItem(IDC_TREE_WELL); 
    if(pTree != NULL)
    {
        HTREEITEM hSel = pTree->GetSelectedItem();
        if(hSel != NULL)
        {
            int nLevel; 
            pTree->GetItemImage(hSel, nLevel, nLevel); 
            if(nLevel == 1)
                m_pDocSel = (CULDoc*)pTree->GetItemData(hSel); 
            else 
                m_pDocSel = NULL; 
        }
        else 
            m_pDocSel = NULL; 
    }
    else 
        m_pDocSel = NULL; 

    EndDialog(IDOK); 
}

void CSelectWellDlg::OnBtnExit()
{
    EndDialog(IDCANCEL); 
}

CULDoc* CSelectWellDlg::GetSelectedWell()
{
    return m_pDocSel; 
}
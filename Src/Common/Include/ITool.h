/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __ITOOL_H__
#define __ITOOL_H__

#include "ULInterface.h"
#include "ULCOMMDEF.H"

// Interfaces
interface ITool : IUnknown
{
	// 基本操作
	ULMETHOD	SetULTool(void* pULTool) = 0;
	ULMETHOD	InitTool() = 0;
	ULMETHOD	OpenTool() = 0;
	ULMETHOD	CloseTool() = 0;
	ULMETHOD	DeclareDataExchange() = 0;
	// 测井操作
	ULMETHOD	BeforeLog(ULLogInfo* pLogInfo) = 0;
	ULMETHOD	LogIO() = 0;
	ULMETHOD	AfterLog() = 0;

	// 刻度操作
	ULMETHOD	BeforeCalibrate(LPCTSTR lpszItem) = 0;
	ULMETHOD	CalibrateIO() = 0;
	ULMETHOD	AfterCalibrate() = 0;

	// 刻度计算
	ULMETHOD	ComputeCalCoefficient() = 0;

	// 自定义刻度操作
    ULMETHOD	CalUserCancel() = 0;
	ULMETHOD	CalUserCalculate() = 0;
	ULMETHOD	CalUserLoad(CFile* pFile, char strPhase[]) = 0;
	ULMETHOD	CalUserSave(CFile* pfile, char strPhase[]) = 0;

    // uReport 取CP_MASTER CP_PRIMARY CP_BEFORE CP_AFTER中的组合
	ULMETHOD	CalUserPrint(CDC* pDC, CSize szPage, UINT uReport) = 0;	
	
	// 回放
	ULMETHOD	BeforeReplay() = 0;
	ULMETHOD	AfterReplay() = 0;

	ULMETHOD	AddRcvDataWnd(HWND hWnd) = 0;

	ULMETHOD	RefreshParameters() = 0;

	//2020.11.2 ver1.6.0 bug[229] start
	ULMVOID		ReadCurveUnit() = 0;
	//2020.11.2 ver1.6.0 bug[229] end

	//2020.11.17 ver1.6.0 bug[253] start
	ULMVOID		SetReacquire(bool bReacquire, int nState, float fStart, float fEnd) = 0;
	//2020.11.17 ver1.6.0 bug[253] end
};

// 接口ID

// {B6EED056-F0BF-46fe-962B-93E44BF72D86}
static const IID IID_ITool = 
{ 0xb6eed056, 0xf0bf, 0x46fe, { 0x96, 0x2b, 0x93, 0xe4, 0x4b, 0xf7, 0x2d, 0x86 } };

extern "C" DWORD ULGetVersion();
extern "C" IUnknown* CreateTool();

#endif
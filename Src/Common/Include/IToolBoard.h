/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __ITOOLBOARD_H__
#define __ITOOLBOARD_H__

#include "ITool.h"

// Interfaces
interface IToolBoard : IUnknown
{
	ULMETHOD OpenBoards(CStringList* pBoards, DWORD* pParam) = 0;
};

// 接口ID
// {E9410D3F-4E08-4a87-914B-A54C2F5A0344}
static const IID IID_IToolBoard = 
{ 0xe9410d3f, 0x4e08, 0x4a87, { 0x91, 0x4b, 0xa5, 0x4c, 0x2f, 0x5a, 0x3, 0x44 } };


#endif
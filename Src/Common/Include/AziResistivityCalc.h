#pragma once

#ifndef __AZIRESISTIVITY_CALC_H_
#define __AZIRESISTIVITY_CALC_H_

#ifndef AZIRESISTIVITY_CALC_API
	#ifdef AZIRESISTIVITY_CALC_EXPORT
		#define AZIRESISTIVITY_CALC_API __declspec( dllexport )
	#else	
		#define AZIRESISTIVITY_CALC_API __declspec( dllimport )
	#endif
#endif
class AZIRESISTIVITY_CALC_API CAziResistivityCalc
{
public:
	CAziResistivityCalc();
	~CAziResistivityCalc();
public:
	long CalculateIndividual(/*[in]*/ short modelType,/*[in]*/ long dataIndex,/*[in]*/ double x,/*[out]*/ double* estimated,/*[out,retval]*/ bool* pRetVal);
	long CalculateApparentResistivity(/*[in]*/ float boreholeSize,/*[in]*/ float mudResistivity,/*[in]*/ float mudTemperatureRatio,/*[in]*/ long index,/*[in]*/ float x,/*[out]*/ float* estimate,/*[out,retval]*/ bool* pRetVal);
	void DetectBoundary(/*[in]*/ float* afSrc,/*[in]*/  float aziReal,/*[in]*/  float aziImaginary,/*[in]*/  byte aziSectorReal,/*[in]*/ byte aziSectorImaginary,/*[out,retval]*/ float* pResultData,/*[out,retval]*/ bool* pRetVal);
private:
	//电阻率计算，边界计算
	WRT_Calculator::IResistivityCalculatorPtr m_pResistivityCalculator;
	WRT_MeasurementAnalysis::IMeasurementCalculatorPtr m_pCalculator;
	WRT_MeasurementAnalysis::IMeasurementPtr m_pMeasurement;
	long _TestMeasurement(std::map<char, float> conventionalMap, float aziReal, float aziImaginary, byte aziSectorReal, byte aziSectorImaginary,/*[out,retval]*/ float* pResultData,/*[out,retval]*/ bool* pRetVal);
private:
	void InitialResistivity();
};

AZIRESISTIVITY_CALC_API CAziResistivityCalc* AfxGetAziResCalc();

#endif //__AZIRESISTIVITY_CALC_H_
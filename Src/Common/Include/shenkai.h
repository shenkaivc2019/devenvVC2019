#ifndef SHENKAI_H
#define SHENKAI_H

#include<string>
using namespace std;

#define skfilter_max_order 200
#define skfilter_max_bandcount 7

// 调试模式（在发行模式时需要将其注释掉）
#define DebugMode

#pragma pack(push, 1)

typedef	enum _skfilter_error_t
{
	skfilter_error_success				= 0,			//调用成功
	skfilter_error_config				= 1,			//设置失败
} skfilter_error_t;

typedef	enum _skfilter_code_t
{
	skfilter_code_continue				= 0x00,			//没检测到曼彻斯特码, 继续输入下一个采样点
	skfilter_code_code0					= 0x30,			//曼彻斯特'0'
	skfilter_code_code0_possible		= 0x2D,			//可能是曼彻斯特0'-'
	skfilter_code_code1_possible		= 0x2B,			//可能是曼彻斯特1'+'
	skfilter_code_code1					= 0x31,			//曼彻斯特'1'
} skfilter_code_t;

/*
频带结构. 
*/
typedef struct
{
	double								pass;			//带通系数. 1.0带通 0.0理想带阻, 一般带阻取0.001即可, 太小可能导致生成失败
	double								start;			//频带起始频率. 从0.0到0.5, 比end小. 注意期望频率要除以采样频率才能用于离散滤波器.
	double								end;			//频率结束频率. 从start到0.5. 注意期望频率要除以采样频率才能用于离散滤波器.
	double								weight;			//误差权重. 如对该频带的控制精度要求很高(相对其他频带), 则将该权重设低例如0.001.
} skfilter_group_t;

#pragma pack(pop)
skfilter_error_t WINAPI skfilter_config(int order,int bandcount,const skfilter_group_t* band,int detect_0_8_or_0_5,int samplerate);

/*
从滤波输出中检测曼彻斯特编码
----------------------------------------------------------------------------------------------------------
输入:
value						原始输入

返回:
skfilter_code_t				检测出的编码

输出:
*/
skfilter_code_t WINAPI skfilter_decode(double raw,double* filtered = NULL);

typedef 
 skfilter_error_t  (WINAPI * skfilter_config_p)(int						order, 
												int						bandcount, 
												const skfilter_group_t*	band,
												int						detect_0_8_or_0_5,
												int						samplerate);
typedef
skfilter_code_t (WINAPI * skfilter_decode_p)(double						raw,
											 double*					filtered);

void  logToFile(char buffer[]);
int binaryToDecimal(string strBits);

HMODULE GetCurrentModule();

#endif // !SHENKAI_H

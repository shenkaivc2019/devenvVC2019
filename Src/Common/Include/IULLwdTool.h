/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2014-1-20
 * 平台版本号：     AXP 1.75
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.75
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/

#ifndef __IULLWDTOOL__
#define __IULLWDTOOL__

#include "IDB.h"

#define LWD_PARAM_DOWN		0	// 参数下装
#define LWD_CHECK_TOOL		1	// 设备检测
#define LWD_READ_DATA		2	// 数据读取
#define LWD_TOOL_TEST       3   // 仪器检测
//Add by xx 2015-10-16
#define  LWD_DATABASE_SET	4	//数据库相关

#define TIMEMODE            13  //时间方式读取数据
#define ADDRESSMODE         14  //地址方式读取数据

#define SEND_SUCCESS		0x00000000	// 发送成功
#define OVER_TIME			0x00000001	// 发送超时
#define PORT_ERROR			0x00000002	// 端口错误，未能建立连接

#define FILE_DAT			0x00000001	// .dat文件格式
#define FILE_XLS			0x00000002	// .xls文件格式
#define FILE_TXT			0x00000004	// .txt文件格式

#define UM_PROGRESS		(WM_USER + 1000)

#define MSG_FLAG_NORMAL		0x00000000
#define MSG_FLAG_WARNING	0x00000001
#define MSG_FLAG_ERROR		0x00000002
#define MSG_FLAG_STATE		0x00000003

interface IULLwdTool : IUnknown
{
	// 向仪器下发命令 命令超时时长ms 命令内容  长度  返回命令内容， 返回最长字节数/实际返回的长度
	// 返回值为 
	virtual DWORD _stdcall SendCommand(UINT nOverTime, PTBYTE pCmdBuf, UINT nSize,
									   PTBYTE pRecvBuf, UINT& nRecvSize) = 0;

	// 获取仪器上传的数据, 获取到的缓冲区长度
	virtual void* _stdcall GetLWDBuffer(int& nBufLen) = 0;

	// 设置参数下装/设备检测/...等的对话框
	// 对话框类型，对话框对象地址，对话框ID，显示名字
	virtual short _stdcall AddLWDUserDlg(DWORD nType, void* pDlg, UINT nID, LPCTSTR lpszTitle = NULL) = 0;

	// 获取读数的起始时间，停止时间，起始搜索地址
	virtual short _stdcall GetReadTime(long& ldtStart, long& ldtStop, DWORD& dwStartBlock, DWORD& dwStartPage) = 0;

	// 获取读数的起始地址，停止地址
	virtual short _stdcall GetReadAddress(DWORD& dwStartBlock, DWORD& dwStartPage, DWORD& dwStopBlock, DWORD& dwStopPage) = 0;

	// 获得读取数据的方式
	virtual DWORD _stdcall GetReadMode() = 0;

	// 获取已设置的存储路径(D:\xxx\xx),  文件名称
	virtual short _stdcall GetFilePath(CString& strPath,CString& strName) = 0;

	// 创建要保存的文件
	// 当前启用的仪器功能类型、要创建的文件类型、创建的第几个文件、创建的文件名、
	// lpszName是否是全路径、预留备用接口（可约定代表的含义）
	virtual IUnknown* _stdcall OpenSaveFile(DWORD dwToolType, DWORD dwFileType, DWORD dwFileNum,
											LPCTSTR lpszName, BOOL bAllPath = FALSE, LPARAM lpParam = 0) = 0;
	// 关闭文件
	// 参数与OpenSaveFile中第三个参数指定的文件序号关联，-1时关闭所有已打开文件
	virtual short _stdcall CloseSaveFile(int nFileNum) = 0;

	//设置进度条的宽度
    virtual void _stdcall SetProgressWidth(int nWidth) = 0;

	//设置进度条的进度
	virtual void _stdcall SetProgressBar(int nWidth) = 0;

	//关闭进度条
	virtual void _stdcall CloseProgressBar() = 0;

	//输出检测结果
    virtual void _stdcall OutPutData() = 0;

	//获取连接状态
	virtual BOOL _stdcall GetConnectState() = 0;

	virtual void _stdcall TraceMessage(CString strMsg , DWORD dwFlag = MSG_FLAG_NORMAL) = 0;
	virtual BOOL _stdcall GetData(BYTE *pBuf , UINT &nLen , int nOverTime = INFINITE) = 0;
	virtual IDB* _stdcall GetIDB() = 0;

	virtual void _stdcall SetRunState(LPCTSTR pszState) = 0;
};

// {51DA3489-CFBF-4d69-B673-78EBDE074790}
static const GUID IID_IULLwdTool = 
{ 0x51da3489, 0xcfbf, 0x4d69, { 0xb6, 0x73, 0x78, 0xeb, 0xde, 0x7, 0x47, 0x90 } };

#endif
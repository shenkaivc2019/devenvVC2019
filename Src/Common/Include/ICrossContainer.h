/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
// ICrossContainer.h: interface for the ICrossContainer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ICROSSCONTAINER_H__3B39848F_1CD0_42BF_A718_714ECED31743__INCLUDED_)
#define AFX_ICROSSCONTAINER_H__3B39848F_1CD0_42BF_A718_714ECED31743__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//////////////////////////////////////////////////////////////////////
// ICrossContainer: 交会图窗口的容器窗口

interface ICurve; 
class CULDoc;
class ICrossContainer  
{
public:
    virtual HWND        GetHwnd() = 0; 
    virtual ICurve*     CreateCorrectedCurve(CString strName, CString strUnit, UINT nPointFrame) = 0; 
    virtual void        AddCorrectedCurve(CULDoc* pWell, ICurve* pCurve, BOOL bRefresh = TRUE) = 0; 
};

#endif // !defined(AFX_ICROSSCONTAINER_H__3B39848F_1CD0_42BF_A718_714ECED31743__INCLUDED_)

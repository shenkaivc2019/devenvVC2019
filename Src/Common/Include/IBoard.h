/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __IBOARD_H__
#define __IBOARD_H__

#include "ULInterface.h"

// Interface
interface IBoard : IUnknown
{
	ULMETHOD		SetULBoard(void* pv) = 0;
	ULMETHOD		InitBoard() = 0;
	ULMETHOD		ulBDOpen() = 0;
	ULMETHOD		ulBDStart() = 0;
	ULMETHOD        ulBDStop() = 0;
	ULMETHOD		ulBDClose() = 0;

	/* ----------------------------- *
	 *	板卡中断函数
	 *  return : UL_NO_ERROR 正常返回，继续执行系统操作
	 *  return : UL_ERROR 异常返回，跳出执行系统操作
	 * ------------------------------------------ */ 
	ULMETHOD		ulBDBeforeIO() = 0;
	ULMETHOD		ulBDSingleIO(LPVOID pDataBuf) = 0;
	ULMETHOD		ulBDSingleIO1(LPVOID pDataBuf) = 0;
	ULMETHOD		ulBDSingleIO2(LPVOID pDataBuf) = 0;
	ULMETHOD		ulBDSingleIO3(LPVOID pDataBuf) = 0;
	ULMETHOD		ulBDAfterIO() = 0;

	ULMETHOD		ulBDSendCommand(int nChannelNo, BYTE* pCmd, int nNum) = 0;
	ULMETHOD		ulBDSetMode(BYTE* pParam, int nNum) = 0;
	ULMETHODDWD		ulBDGetMode(BYTE* pParam, int nNum) = 0;
};

// {A945E7A9-EE03-4346-91D0-12276659D733}
static const IID IID_IBoard = 
{ 0xa945e7a9, 0xee03, 0x4346, { 0x91, 0xd0, 0x12, 0x27, 0x66, 0x59, 0xd7, 0x33 } };

extern "C"	DWORD GetULVersion();
extern "C"  IUnknown* CreateBoard();

#endif

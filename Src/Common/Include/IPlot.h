/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __IPLOT__H
#define __IPLOT__H

#include "ITrack.h"
#include "ICurve.h"

static const CATID CATID_ULPLOT = {0xA0021493,0x0000,0x0000, {0xC0, 0x00,0x00, 0x00, 0x00, 0x00, 0x00, 0x46}};
struct __declspec(uuid("100214E4-0000-0000-C000-000000000046")) IPlot:public IUnknown
{
	virtual HRESULT STDMETHODCALLTYPE 	Advise(ITrack* pHodler) = 0;
	virtual HRESULT STDMETHODCALLTYPE	GetNameOfPlot(VARIANT* name) = 0;
	virtual HRESULT STDMETHODCALLTYPE 	ChangeGridCount() = 0;
	virtual HRESULT STDMETHODCALLTYPE 	GetCurveXPos(ICurve* pCurve, long lDepth, BOOL bRolled = TRUE) = 0;

	virtual HRESULT STDMETHODCALLTYPE 	DrawHeadCurve(VARIANT* pDC,ICurve* pCurve, LPRECT lpRect, BOOL bDrawLine = TRUE) = 0;
	virtual HRESULT STDMETHODCALLTYPE 	DrawCurve(VARIANT* dc, ICurve* pCurve, long lStartDepth, long lEndDepth) = 0;
	virtual HRESULT STDMETHODCALLTYPE 	LogDrawCurve(VARIANT* dc,ICurve* pCurve, long lStartDepth, long lEndDepth) = 0;
	
	virtual HRESULT STDMETHODCALLTYPE 	PrintHeadCurve(VARIANT* pInfo, ICurve* pCurve, LPRECT lpRect) = 0;
	virtual HRESULT STDMETHODCALLTYPE 	StaticPrint(VARIANT* pInfo, ICurve* pCurve, double fMaxDepth, double fMinDepth) = 0;
	virtual HRESULT STDMETHODCALLTYPE 	Print(VARIANT* pInfo, ICurve* pCurve, double fMaxDepth, double fMinDepth) = 0;

	virtual HRESULT STDMETHODCALLTYPE   DrawThumb(VARIANT* dc, ICurve* pCurve, LPRECT lpRect, long lStart, double dDelta, int nPoint) = 0;
};

#endif
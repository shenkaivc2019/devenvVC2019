/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef	_UL_OSCILLISCOPE
#define _UL_OSCILLISCOPE

//示波器类型
#define UL_OSCILLISCOPE_NULL		0//无示波器
#define UL_OSCILLISCOPE_STANDARD	1//标准
#define UL_OSCILLISCOPE_DEPTH		2//深度
#define UL_OSCILLISCOPE_ACOUSTIC	3//声波
#define UL_OSCILLISCOPE_RADIOACTIVE 4//放射性
#define UL_OSCILLISCOPE_AZIM        5//方位
#define UL_OSCILLISCOPE_POLAR       6// 极坐标
#define UL_OSCILLISCOPE_TEXT        7//文本
#define UL_OSCILLISCOPE_LWD			8//随钻DDU示波器
#define UL_OSCILLISCOPE_USERDEFINED 255//用户自定义

#ifdef _OLD_SCOPES

#define UL_OSCILLISCOPE_AXIS_NONE		0 // 没有坐标轴
#define UL_OSCILLISCOPE_AXIS_LEFT		1 // 坐标轴放在左边
#define UL_OSCILLISCOPE_AXIS_CENTER		2 // 坐标轴放在中间
#define UL_OSCILLISCOPE_AXIS_RIGHT		3 // 坐标轴放在右边
#define UL_OSCILLISCOPE_AXIS_TOP		4 // 坐标轴放在上边
#define UL_OSCILLISCOPE_AXIS_BOTTOM		5 // 坐标轴放在下边

// 用于IULTool::ScopeSetting函数的第一个参数
#define SS_SETXMAX                      0 // 设置X轴最大值, 用于标准示波器的谱显示
#define SS_SETYMAX                      1 // 设置Y轴最大值, 用于标准示波器的谱显示
#define SS_SETDATANUMPERSEC             2 // 设置单位时间的数据量, 用于标准示波器的谱显示
#define SS_SETCYCLE                     3 // 设置谱周期, 用于标准示波器的谱显示
#define SS_SETCOLLTIMES                 4 // 设置谱采集次数(多次采集累加或平均), 用于标准示波器的谱显示
#define SS_SHOW                         5 // 谱显示 0: 开始谱显示 1: 结束谱显示, 用于标准示波器的谱显示
#define SS_SETYMAXRANGE                 6 // 设置Y轴最大值的范围, 用于标准示波器的谱显示
#define SS_SETVERTGRID                  7 // 设置垂直的格子数, 用于标准示波器的谱显示
#define SS_SETHORZGRID                  8 // 设置水平的格子数, 用于标准示波器的谱显示
#define SS_SETXAXISTICKSCOUNT           9 // 设置x轴坐标数目, 用于标准示波器的谱显示
#define SS_SETYAXISTICKSCOUNT          10 // 设置y轴坐标数目, 用于标准示波器的谱显示
#define SS_CUSTOMIZEDEPTHSCOPE         11 // 定制深度示波器，0 定制 1 不定制	
#define SS_ADDDEPTHSCOPEITEM           12 // 为深度示波器添加项目
#define SS_SETDEPTHITEMVALUE           13 // 定制示波器项目后设置数据
#define SS_CUSTOMIZEDCURVE             14 // 定制示波器曲线
#define SS_ADDCURVETOSCOPE             15 // 添加曲线到示波器
#define SS_WINDOWVISIBLE               16 // 设置示波器窗口是否可见
#define SS_ADDUSERDIALOG               17 // 添加示波器设置页
#define SS_ADDMARK                     18 // 添加标记
#define SS_SETUSERSCOPEDLG             19 // 为用户自定义示波器，设置对话框
#define SS_SENDMESSAGE                 20 // 给所有示波器发送自定义消息
#define SS_SETXUNIT					   21 // 设置声波示波器横坐标采样间隔
#define SS_SETXLABLE				   22 // 设置声波示波器横坐标标签间隔	
#define SS_SETCURVEPROP				   23 // 设置声波示波器曲线属性
#define SS_ADDMARKTOSCOPE              24 // 为声波示波器添加标记
#define SS_SETMARKPROP                 25 // 设置声波示波器标记属性
#define SS_SETSTARTPOS				   26 // 设置声波示波器横轴坐标起始位置
#define SS_SETTHREADHOLD			   27 // 设置声波示波器横轴坐标起始位置
#define SS_SETNUMBERTYPE			   28 // 设置工具面示波器外围表盘数据类型：0:0-90R-180-90L/1:0-180-360
#define SS_GETPARAM					   29 // 获取示波器属性
#define SS_SETDIALINFO				   30 //设置工具面示波器上方位探边信息（2个数值，1个int，1个double*）

#define SCOPE_PARAM_THRESHOLD			1		//声波示波器门槛线 

#define SC_TYPE_LEFT_VALUE				1
#define SC_TYPE_RIGHT_VALUE				2
#define SC_TYPE_X_POS					3
#define SC_TYPE_Y_POS					4
#define SC_TYPE_X_GAIN					5
#define SC_TYPE_Y_GAIN					6

//add by gj 2012/12/03
//record:设置示波器标记的形状及填充模式的宏
#define SM_SCOPE_MARK_X      0
#define SM_SCOPE_MARK_HLINE  1
#define SM_SCOPE_MARK_RECT   2
#define SM_SCOPE_MARK_TRIA   3
#define SM_SCOPE_MARK_CROSS  4
#define SM_SCOPE_MARK_VLINE  5
#define SM_SCOPE_MARK_CIRCLE 6

#define SM_SCOPE_MARK_BRUSH  0
#define SM_SCOPE_MARK_NULL   1
#define SM_SCOPE_MARK_GRID   2

//add by gj 2012/12/03
//record:设置示波器属性的宏

#define SCOPE_MARK_STYLE         0x0001
#define SCOPE_MARK_COLOR         0x0002
#define SCOPE_MARK_FILLSTYLE     0x0004
#define SCOPE_MARK_XPOS          0x0008
#define SCOPE_MARK_YPOS          0x0010
#define SCOPE_MARK_WIDTH         0x0020
#define SCOPE_MARK_HIGH          0x0040
#define SCOPE_MARK_SHOW          0x0080
#define SCOPE_MARK_JOINSTYLE     0x0100
#define SCOPE_MARK_LIVECURVE     0x0200
#define SCOPE_MARK_CTRLCURVE     0x0400
#define SCOPE_MARK_ACTIVE        0x0800
#define SCOPE_MARK_CHANNEL       0x1000
#define SCOPE_MARK_XPLUS         0x2000
#define SCOPE_MARK_YPLUS         0x4000
#define SCOPE_MARK_XCURPOS       0x8000


typedef struct tagSCOPECURVEPROP
{
	CString strCuveName;
	DWORD nType;
	double fValue;
}SCOPE_CURVE_PROP;

//add by gj 2012/12/03
//record:定义示波器标记的结构体
//示波器标记
typedef struct tagSCOPEMARK
{
	TCHAR strMarkName[10];//标记名称
	int		m_nStyle;//标记样式
	COLORREF	m_crLineColor;//标记颜色
	int		m_nFillStyle;//添充方式
	int		m_nXPos;//标记横坐标
	int		m_nYPos;//标记纵坐标
	int		m_nWidth;//标记宽
	int		m_nHigh;//标记高
	BOOL	m_bShow;//标记显示
	
	int		m_nJoinStyle;//连接方式 0: 无; 2:全关联；1：Y轴关联 3 :X轴关联
	int		m_nLiveCurve;//标记Y方向关联的曲线
	int		m_nCtrlCurve;//标记X方向关联的曲线
	BOOL	m_bActive;//标记是否被选中
	int		m_nChannel;//标记所在的通道
	int		m_nXPlus;//标记X方向增益
	int		m_nYPlus;//标记Y方向增益
	int		m_nXCurPos;//标记在曲线上的位置
	DWORD   m_mask;//掩码
	BYTE   Reserved[200];
}SCOPE_MARK;


typedef struct tagULSCOPEPARA	//示波器属性结构
{
	char	szName[32];			//示波器名称
	UINT	nType;				//示波器类型
	void*   pDlg;				//示波器对话窗口
	UINT    nID;				//示波器窗口ID
	UCHAR	Reserved[1016];		//示波器用户定义参数
}ULScopePara;

// 深度示波器项目类型
#define DSIT_LCD         0   // LCD项目
#define DSIT_METER       1   // 仪表
#define DSIT_THERMOMETER 2   // 温度计

// 深度示波器示波器数据来源
#define DSDS_CUSTOMIZED  0   // 自定义数据
#define DSDS_DEPTH       1   // 深度
#define DSDS_SPEED       2   // 速度
#define DSDS_TEMP        3   // 温度
#define DSDS_TENSION     4   // 张力
#define DSDS_VOLTAGE     5   // 电压

typedef struct tagDEPTHSCOPEITEM
{
	int   nItemType;           // 项目类型, 如DSIT_LCD
	char  cCNName[32];         // 中文名称
	char  cENName[32];         // 英文名称
	char  cUnit[32];           // 单位
    float fMinValue;           // 最小值
	float fMidValue;           // 中间值
	float fMaxValue;           // 最大值
	int   nDataSource;         // 数据来源
	UINT  nPrecision;          // 小数点几位
}DepthScopeItem;

typedef struct tagDEPTHSCOPEITEMVALUE
{
	int   nType;             // 项目类型
	int   nItem;             // 项目序号         
	float fValue;            // 项目值
}DepthScopeItemValue;

struct DepthConfig
{
	int nMode;  // 0 时间驱动 1 深度驱动
	int nSample; // 采样 深度驱动表示每米采多少个点 时间驱动表示多少毫秒采集一次
	int nTransRate; // 传输率 表示每采样一次 进行多少次传输
};

//Add by xx 探边方位信息	2016-10-28
typedef struct tagLWDDIALINFO
{	
	int nDialSec;			//Sec - 4Byte
	double dDialImg;		//Img - 8Byte
}LWDDialInfo;

#define UM_DEPTHCONFIG WM_USER + 704

class  CULOscilliscope
{
public:
	ULScopePara m_ScopePara;
	
public:
	BOOL    OpenOscilliscope(ULScopePara* pScopeData);
	BOOL	CloseOscilliscope(ULScopePara* pScopeData);
	ULScopePara*   OpenOscilliscope(CString strName, UINT nType);
	BOOL	RefreshOscilliscopePara(ULScopePara* pScopeData);
};

#endif

#endif
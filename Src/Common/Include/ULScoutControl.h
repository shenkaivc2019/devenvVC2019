/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
// ULScoutControl.h: interface for the CULScoutControl class.
//
////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_ULSCOUTCONTROL_H__17CEF444_8E26_4767_9908_327B8B0487D0__INCLUDED_)
#define AFX_ULSCOUTCONTROL_H__17CEF444_8E26_4767_9908_327B8B0487D0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxtempl.h>		// MFC Class Template	

//-------------------------------------------------------------
// 仪器状态及状态参数mod by zjp 06.01.10
//----------------------------------------------------------------

typedef struct tagTOOLPARAEVENT{
	char strTool[20];
	char strPara[20];
	double fParaValue;
}TOOLPARAEVENT;

//-------------------------------------------------------------
//监控事件结构的定义，该事件主要用于UL2000软件各种操作的日志纪录
//-------------------------------------------------2003.1.10---

typedef struct tagSCOUTEVENT{
	WORD wdSize;				//监控事件结构的大小
	char chName[64];			//产生的监控事件名称
	char chDate[11];			//日期
	char chTime[9];				//时间
	char chOrigination[64];		//事件来源
	double dDepth;				//监控事件产生的深度位置
	TOOLPARAEVENT ToolPara;		//对应仪器的参数信息  mod by zjp 06.01.10	
	WORD wdEventType;			//事件类型
	BYTE btEventType;			//事件发生的状态,正确，错误或警告
	char chEventContent[64];	//事件内容
	char chEventResult[64];		//事件结果
	char chMark[64];			//该监控事件备注
	BYTE nReserved[20];
}SCOUTEVENT; 	

//-------------------------------------------------------------
//监控文件头定义，监控文件中的各种信息定义
//-------------------------------------------------2003.1.11---
typedef struct tagScoutFileHead{
	char chIndicator[24];		//监视文件标识
	char chProjectName[32];		//工程名称
	char chJobName[32];			//作业名称	
	char chWorkSegName[32];		//工作段名称
//	char chReserve1[32];		//预留1
//	char chReserve2[32];		//预留2
//	char chReserve3[32];		//预留3
}SCOUTFILEHEAD;

/*-----------------------------------------------------
  该类实现对各种监控信息的处理：
				    1、监控事件信息的生成
					2、监控事件信息的加入
					3、将监控信息写入文件
					4、从监控文件中读取监控信息
					5、监控信息的分类处理，按类型进行分类
					6、实现对监控信息的打印处理
					7、对监控信息的提取处理
  -----------------------------------------2003.1.10-*/

class AFX_EXT_CLASS CULScoutControl
{
private:
	CFile m_fileScout;								//监控文件对象
	SCOUTFILEHEAD m_ScoutFileHead;					//The File Head of the Scout
	CString m_strFileName;							//The Name of the Scout file
	
	CArray<SCOUTEVENT,SCOUTEVENT> m_arrayTotal;		//存放所有的监控纪录
	CArray<int,int> m_arrayLogIndex;  				//测井监控记录到所有纪录的索引
	CArray<int,int> m_arrayCalIndex;  				//刻度监控记录到所有纪录的索引
	CArray<int,int> m_arrayDataEditIndex;  			//数据编辑监控记录到所有纪录的索引
	CArray<int,int> m_arrayToolOperateIndex;        //仪器操作记录到所有记录的索引 mod by zjp 2006.1.10
	CArray<int,int> m_arraySysErrorIndex;           //系统出错纪录到所有记录的索引 mod by zjp 2006.1.10
	CArray<int,int> m_arrayOtherIndex;				//其它监控记录的索引序号

	enum{encrpttype0=0,encrpttype1=1,encrpttype2=2,encrpttype3=3,encrpttype4=4,encrpttype5=5};//used for describle the type of encryption of the Scout Record 
private:
	int ReadEventTotal();							//读取监控文件,used in Scout Viewer	 
	//Print the Scout Event Item 
	void PrintScoutEvntItem(CDC *pDC,const SCOUTEVENT &ScoutEvent,int nXCoor,int nYCoor,int nCharWidth,int nCharHeight);

	//Set the file head of the Scout file,used in UL2000 System 
	void SetScoutInfo(CString strProjName,			//工程名称
					  CString strJobName,			//作业名称
					  CString strSegName			//段名称
					  );   

	/*对监控记录进行加密处理,
      pBuf:需要加密的地址,dwCharLen:需要加密的字节的长度,EncryType:所采用的加密算法*/
	int Encrypt(char*pBuf,DWORD dwCharLen,int EncrypType);	   //加密函数
	int UnEncrypt(char*pBuf,DWORD dwCharLen,int EncrypType);   //解密函数

public:
	enum{typeTotal=0,typeLog=1,typeCal=2,typeDataEdit=3,typeToolOp=4,typeSysError = 5,typeOther=6};	//用于枚举监控纪录的类型
	enum{typeCorrect=0,typeWarning=1,typeWrong=2};						//用于枚举监控纪录的类型

public:
	//-------------------Used in UL2000 -----------------------------//
	//Create one Scout file of the project,Job and Segment ,Used in UL2000
	int Create(CString strFileName,CString strProjName,CString strJobName,CString strSegName);
	//将监控事件加入,Used in UL2000
	int AddEventItem(const SCOUTEVENT &ScoutEvent);	

	//Destroy the file,Used in UL2000
	int Destroy();				   						
	
	//将监控事件加入
	int AddEventItem(													
			CString strName,						//产生的监控事件名称
			CString strOrigination,					//事件来源
			double dDepth,							//监控事件产生的深度位置
			CString strToolName,
			CString strParaName,
	        	double  fParaValue,
			WORD wdEventTpe,						//事件类型	  
			BYTE byEventType,						//事件发生的状态,正确，警告或错误
			CString strEventContent,				//事件内容
			CString strEventResult,					//事件结果
			CString strMark);						//事件背注
	//-------------------Used in UL2000-----------------------------//
	
	//-------------------Used in Scout Viewer------------------------//
	//打开监控文件			
	int OpenFile(CString strScoutFile);									

	//获取监控文件头
	SCOUTFILEHEAD GetScoutFileHead();

	//获取某类型监控所有记录的条目数
	int GetScoutSize(int nScoutType=typeTotal);                         

	//某类型某一个索引值处的监控信息
	SCOUTEVENT GetScoutEventItem(int nIndex,int nScoutType=typeTotal);	
	
	//打印某一类型的监控纪录
	void PrintScoutEvent(CDC* pDC,int nScoutType=typeTotal);            

	//关闭监控文件
	int CloseFile();													
    //-------------------Used in Scout Viewer------------------------//		 

	//Whether the object Validate,Validate:TRUE,Invalidate:FALSE;Only used to open and close Scout files
	BOOL IsValidate();   
	
public:											
	CULScoutControl();							
	virtual ~CULScoutControl();	  
};

#endif // !defined(AFX_ULSCOUTCONTROL_H__17CEF444_8E26_4767_9908_327B8B0487D0__INCLUDED_)

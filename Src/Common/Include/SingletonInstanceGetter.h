#pragma once
#if	!defined(__SINGLETON_INSTANCE_GETTER_H__)
#define	__SINGLETON_INSTANCE_GETTER_H__

////////////////////////////////////////////////////////////////////////////////////
template <typename T>
class CSingletonInstanceGetter
{
public:
    CSingletonInstanceGetter(void)
    {
        if (m_pInstance == NULL)
        {
            try
            {
                m_pInstance = new T();
            }
            catch (...)     // 防止new分配内存可能出错的问题，如果是内存分配错误异常为std::bad_alloc
            {
                m_pInstance = NULL;
            }
        }

        //这里不管SingletonExample创建成功与否都要加1
        m_uiReference++;
    }  

    ~CSingletonInstanceGetter(void)
    {  
        m_uiReference--;
        if (m_uiReference == 0)
        {  
            if (m_pInstance != NULL)
            {  
                delete m_pInstance;
                m_pInstance = NULL; //非常重要，不然下次再次建立单例的对象的时候错误
            }
        }
    }

public:
    T *GetInstance()
    {  
        return m_pInstance;
    }
	void SetInstance(T* pInstance)
	{
		if (m_pInstance != pInstance)
		{
			if (m_pInstance)
			{
				delete m_pInstance;
				m_uiReference = 0;
			}
			m_pInstance = pInstance;
			if (pInstance)
			{
				m_uiReference = 1;
			}
		}
	}

private:
    static T *m_pInstance;
    static unsigned int m_uiReference;
};
template <typename T>
T *CSingletonInstanceGetter<T>::m_pInstance = NULL;

template <typename T>
unsigned int CSingletonInstanceGetter<T>::m_uiReference = 0;
////////////////////////////////////////////////////////////////////////////////////

#endif // __SINGLETON_INSTANCE_GETTER_H__
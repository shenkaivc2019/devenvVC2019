/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __IFILE_H__
#define __IFILE_H__

#include "ULInterface.h"
#include "ULCOMMDEF.H"

interface IProject;

// Interfaces
interface IFile : IUnknown
{
	// 基本操作
	ULMETHOD	AdviseFile(void* pULFile) = 0;
	ULMETHOD	InitFile() = 0;

	ULMETHOD	Create(LPCTSTR lpszFile) = 0;
	ULMETHOD	Open(LPCTSTR lpszFileName, DWORD dwLength = 0) = 0;
	ULMETHOD    Close() = 0;

	// 存盘操作
	ULMETHOD    SaveFileHead(IProject* pProject) = 0;


	ULMETHOD	SaveBegin(void* pFBInfo, CURVEPROPS& vecCurve) = 0;
	ULMETHOD	SaveFrame(long lDepth, long lTime, vec_cv* pValues, void* pFInfo = NULL) = 0;
	ULMETHOD	SaveEnd(void* pFEInfo) = 0;

	// 读盘操作
	ULMETHOD    ReadFileHead(IProject* pFHInfo, CStringArray* pArrSheet, 
				CCellInfoList* pCellList, CPofInfoList* pPofList) = 0;
	
	ULMETHOD    ReadBegin(void* pFBInfo, CURVEPROPS& vecCurve) = 0;
	ULMETHOD    ReadFrame(int nPos, long* lDepth, long* lTime, vec_cv* pValues, void* pFInfo = NULL) = 0;
	ULMETHOD    ReadEnd(void* pFEInfo) = 0;

    // only supported by FFAXP.
    ULMBOOL     UpdateCalibration(LPCTSTR lpszFile) = 0;
};

// 接口ID

// {1438B51C-CA1A-441c-9FC9-DD81AAE3802A}
static const IID IID_IFile = 
{ 0x1438b51c, 0xca1a, 0x441c, { 0x9f, 0xc9, 0xdd, 0x81, 0xaa, 0xe3, 0x80, 0x2a } };

extern "C" DWORD ULGetVersion();
extern "C" IUnknown* CreateXFile();

#endif
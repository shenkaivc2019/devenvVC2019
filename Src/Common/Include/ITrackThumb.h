/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __ITRACKTHUMB_H__
#define __ITRACKTHUMB_H__



// Interface
interface ITrackThumb : IUnknown
{	
	/* ----------------------------- *
	 *  缩略图
	 *  return : UL_NO_ERROR 正常返回，执行系统缺省的绘图操作
	 *  return : UL_ERROR 异常返回，不执行系统缺省的绘图操作
	 *  to overwrite the drawing must return UL_ERROR
	 * ------------------------------------------ */
	ULMETHOD DrawThumb(CDC* pDC, LPRECT lpRect, long ls, double dDelta, int nPoint) = 0;
};

// {45B16D46-F58A-4bc8-9308-CBD9AB8A0746}
static const IID IID_ITrackThumb = 
{ 0x45b16d46, 0xf58a, 0x4bc8, { 0x93, 0x8, 0xcb, 0xd9, 0xab, 0x8a, 0x7, 0x46 } };

#endif
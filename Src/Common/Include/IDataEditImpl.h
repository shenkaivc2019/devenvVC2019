/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#pragma once

#define virtual HRESULT STDMETHODCALLTYPE  ULINTERFACE
struct __declspec(uuid("100214E4-0000-0000-ABCD-000000000046")) IDataEdit:public IUnknown
{	
	// 常规曲线操作
	//水平移动
	ULINTERFACE 	DataEdit_MoveHorz(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//垂直移动
	ULINTERFACE 	DataEdit_MoveVert(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//双向移动
	ULINTERFACE 	DataEdit_MoveHorzVert(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//水平缩放
	ULINTERFACE 	DataEdit_ExpendHorz(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//垂直缩放
	ULINTERFACE 	DataEdit_ExpendVert(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//基线校正
	ULINTERFACE 	DataEdit_BaselineRevise(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//滤波
	ULINTERFACE 	DataEdit_Filter(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//删除
	ULINTERFACE 	DataEdit_Delete(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//恢复
	ULINTERFACE 	DataEdit_Recover(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//剪切
	ULINTERFACE 	DataEdit_Cut(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//粘贴
	ULINTERFACE 	DataEdit_Paste(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//复制
	ULINTERFACE 	DataEdit_Copy(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//切齐
	ULINTERFACE 	DataEdit_Uniform(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//镜象
	ULINTERFACE 	DataEdit_Image(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//反转
	ULINTERFACE 	DataEdit_Turn(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//插补
	ULINTERFACE 	DataEdit_Interpolation(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//恢复
	ULINTERFACE 	DataEdit_Undo() = 0;
	//重做
	ULINTERFACE 	DataEdit_Redo() = 0;
	//单点修复
	ULINTERFACE 	DataEdit_OnePointCorrect(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//合并
	ULINTERFACE  DataEdit_Combine(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//替换
	ULINTERFACE 	DataEdit_Replace(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;

	// 二维曲线变换
	//二维曲线灰度线性变换
	ULINTERFACE 	DataEdit_LinerTrans(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//二维曲线灰度阈值变换
	ULINTERFACE 	DataEdit_ThresholdTrans(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//二维曲线灰度窗口变换
	ULINTERFACE 	DataEdit_WindowTrans(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//二维曲线灰度拉伸
	ULINTERFACE 	DataEdit_GrayStretch(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//二维曲线灰度均衡
	ULINTERFACE 	DataEdit_InteEqualize(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
	//二维曲线梯度锐化
	ULINTERFACE 	DataEdit_EnhaGradsharp(ICurve* pCurve, LPDATAEDITSTRUCT lpds) = 0;
};

template <class TBase, DWORD dwReserved = 0>
class ATL_NO_VTABLE IPlotImpl : public IDataEdit
{
};
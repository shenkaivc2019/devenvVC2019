/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __IULCHANNEL_H__
#define __IULCHANNEL_H__

#include "ULInterface.h"

// Interface
interface IChannel : IUnknown
{
	ULMETHOD	OpenChannel() = 0;	// initialize the channel
	ULMETHOD	CloseChannel() = 0;	// close the channel
	
	ULMTSTR		GetEventName() = 0;
	ULMTSTR		GetMapFileName() = 0;
	
	ULMETHOD	SetEvent() = 0;
    ULMETHOD	SetEventName(LPCTSTR lpszEventName) = 0;
	ULMETHOD	SetMapFileName(LPCTSTR lpszMapFileName) = 0;
	
	ULMPTR		GetDataBuffer() = 0;
	ULMETHOD	SetBufferSize(DWORD dwSize) = 0;
	
	ULMINT		GetData(LPVOID pData, int nNum) = 0;	// get the data from the channel
	ULMINT      AddData(LPVOID pData, int nNum) = 0;
	ULMINT		SetData() = 0;	// set the data into the channel
	ULMETHOD	SetZero() = 0;
	
	ULMETHOD	CheckChannel() = 0;	

	ULMETHOD	SetID(DWORD dwID) = 0;
	ULMETHOD	SetID(BYTE bytBoard, BYTE bytCh0, BYTE bytCh1) = 0;
	ULMETHOD	SetBoardID(BYTE bytBoard) = 0;
	ULMETHOD	SetChannelID(BYTE bytCh0, BYTE bytCh1 = 0) = 0;

	ULMDWD		ID() = 0;
	ULMBYT		ID(int nIndex) = 0;
	ULMBYT		BoardID() = 0;
	ULMBYT		ChannelID(int nIndex = 0) = 0;
	ULMBOOL		IsChannelID(short nTypeNum, short nNum) = 0;

	ULMTSTR		Type() = 0;
	ULMDBL		DataRate() = 0;
	ULMLNG		Precision() = 0;
	ULMETHOD    SetType(LPCTSTR lpszCHType)=0;

	ULMINT      Lock() = 0;
	ULMINT      IsLocked() = 0;
	ULMINT      IsBufferEmpty() = 0;

	virtual long _stdcall AddTool() = 0;		// 有仪器申请此通道时调用该函数一次，增加一支仪器 add by bao 2012/2/3
};

// {4EA2B295-1FB3-466c-87A9-65DDB0978085}
static const IID IID_IChannel = 
{ 0x4ea2b295, 0x1fb3, 0x466c, { 0x87, 0xa9, 0x65, 0xdd, 0xb0, 0x97, 0x80, 0x85 } };



#endif

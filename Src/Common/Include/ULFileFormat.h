/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef _ULFILE_FORMAT_H_
#define _ULFILE_FORMAT_H_

// 测井开始时数据信息
typedef struct  
{
	// 测井信息
	BYTE		 bScaleUnit;	// 数据传输单位
	long		 lFrameStep;	// 深度采样间隔
	int			 nDriveMode;	// 驱动模式
	int          nDirection;	// 测井方向
	long		 lFrameCount;	// 数据帧
	long         lStartDepth;   // 起始深度
	long         lEndDepth;     // 终止深度
	BOOL         bSetDepth;     // 是否设置了存盘深度
	BOOL         bRecordDown;   // 下放?
	BOOL         bMetric;       // 公制?
	BOOL         bWrapText;     // 数据换行
	long		 lStep;
	float		 fVersion;		// 保存LAS版本
	BOOL		 bSetStep;		// 是否设置了强制存盘间隔
    short        bDepthOncePerRecord; // LIS 格式深度存放方式.
	short        nAXPVersion;//AXP版本号
} ULFBInfo;


#endif











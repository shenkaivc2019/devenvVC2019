/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __IPROJECT_H__
#define __IPROJECT_H__

#include "ULInterface.h"
#include "ULCOMMDEF.H"

// Interface
interface IProject : IUnknown
{
	ULMCTSTR GetName() = 0;
	ULMDWD WriteToFile(CFile* pFile, DWORD dwType = 1) = 0;
	ULMDWD ReadFromFile(CFile* pFile, DWORD dwType = 1) = 0;
	ULMBOOL GetInformation(LPCTSTR pszName, CString* pValue, DWORD dwIndex = -1) = 0;
	ULMBOOL SetInformation(LPCTSTR pszName, LPCTSTR pszValue, DWORD dwIndex = 0) = 0;
	ULMBOOL SetInforBuffer(LPCTSTR pszName, LPBYTE pByte, DWORD dwSize, DWORD dwIndex = 1) = 0;
	ULMDWD GetInforBuffer(LPCTSTR pszName, LPBYTE pByte, DWORD dwSize, DWORD dwIndex = 1) = 0;
	ULMINT GetOGResults(CArray<long, long>* pDepths, CStringArray* pResults, int* pSelect = NULL) = 0;
	ULMINT SetOGResults(CArray<long, long>* pDepths, CStringArray* pResults) = 0;
	ULMINT GetCasingPro(double* pCasingSize, double* pCasingDepth , double* pCasingThickness) = 0;
	ULMINT SetCasingPro(double* pCasingSize, double* pCasingDepth, double* pCasingThickness , int nProCount) = 0;
	ULMDBL GetCasingSizeByDepth(double fCasingDepth) = 0;
	ULMINT GetBitPro(double* pBitSize, double* pBitDepth) = 0;

	ULMETHOD SetCurTime(SYSTEMTIME sysTime) = 0;	//设置当前测井时间
	ULMETHOD GetCurTime(SYSTEMTIME& sysTime) = 0;	//获取当前测井时间
	
	ULMETHOD SetSerialConfig(CStringArray* pArrSerialPath) = 0;			//设置序列配置路径集合
	ULMETHOD GetSerialPath(LPCTSTR pszSrcPath, CString* pDesPath) = 0;	//获取目标序列配置
};

// {64549961-8586-463E-83BC-F72A279E86EB}
static const IID IID_IPROJECT =
{ 0x64549961, 0x8586, 0x463e, { 0x83, 0xbc, 0xf7, 0x2a, 0x27, 0x9e, 0x86, 0xeb } };

////////////////////////////////////////////////////////////////////////////////////////////
// Interface
interface IProjectTest : IUnknown
{
	virtual LPCTSTR GetName() = 0;
	virtual DWORD WriteToFile(CFile* pFile, DWORD dwType = 1) = 0;
	virtual DWORD ReadFromFile(CFile* pFile, DWORD dwType = 1) = 0;
	virtual BOOL GetInformation(LPCTSTR pszName, CString* pValue, DWORD dwIndex = -1) = 0;
	virtual BOOL SetInformation(LPCTSTR pszName, LPCTSTR pszValue, DWORD dwIndex = 0) = 0;
	virtual BOOL SetInforBuffer(LPCTSTR pszName, LPBYTE pByte, DWORD dwSize, DWORD dwIndex = 1) = 0;
	virtual DWORD GetInforBuffer(LPCTSTR pszName, LPBYTE pByte, DWORD dwSize, DWORD dwIndex = 1) = 0;
	virtual int GetOGResults(CArray<long, long>* pDepths, CStringArray* pResults, int* pSelect = NULL) = 0;
	virtual int SetOGResults(CArray<long, long>* pDepths, CStringArray* pResults) = 0;
	virtual int GetCasingPro(double* pCasingSize, double* pCasingDepth, double* pCasingThickness) = 0;
	virtual int SetCasingPro(double* pCasingSize, double* pCasingDepth, double* pCasingThickness, int nProCount) = 0;
	virtual double GetCasingSizeByDepth(double fCasingDepth) = 0;
	virtual int GetBitPro(double* pBitSize, double* pBitDepth) = 0;

	virtual short SetCurTime(SYSTEMTIME sysTime) = 0;	//设置当前测井时间
	virtual short GetCurTime(SYSTEMTIME& sysTime) = 0;	//获取当前测井时间

	virtual short SetSerialConfig(CStringArray* pArrSerialPath) = 0;			//设置序列配置路径集合
	virtual short GetSerialPath(LPCTSTR pszSrcPath, CString* pDesPath) = 0;	//获取目标序列配置
};

#endif

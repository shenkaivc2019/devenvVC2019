/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
/*++ BUILD Version: 0001     Increment this if a change has global effects

Copyright (c) 2005-2006  Arena Corporation

Module Name:

    ulinterface.h

Abstract:

    This module defines the ul2000 platform types and constants that are
    defined by ul2000, but exposed through the interface of ul2000.

Revision History:

--*/

#ifndef _ULINTERFACE_
#define _ULINTERFACE_

#include <objbase.h>		// interface
#include <comdef.h>			// _variant_t, _bstr_t
#include <vector>			// vector

#define _UL2000_VERSION_MAJOR	1
#define _UL2000_VERSION_MINOR	20

#define _UL2000_VERSION_		(DWORD)MAKEWORD(_UL2000_VERSION_MINOR, _UL2000_VERSION_MAJOR)
#define _UL2000_VERSION_FLT		(_UL2000_VERSION_MAJOR + _UL2000_VERSION_MINOR*0.01)
#define _UL2000_VERSION_STR		_T("1.20") 

#define ULMVOID             virtual void        _stdcall
#define ULMETHOD			virtual	short		_stdcall
#define ULMETHODPTR			virtual void*		_stdcall
#define ULMPTR				virtual void*		_stdcall
#define ULMETHODSTR			virtual char*		_stdcall
#define ULMSTR				virtual char*		_stdcall
#define ULMETHODINT			virtual int			_stdcall
#define ULMINT				virtual int			_stdcall

#define ULMETHODCHR			virtual char		_stdcall
#define ULMETHODSHT			virtual short		_stdcall
#define ULMETHODLNG			virtual long		_stdcall
#define ULMLNG				virtual long		_stdcall
#define ULMETHODFLT			virtual float		_stdcall
#define ULMFLT				virtual float		_stdcall
#define ULMETHODDBL			virtual double		_stdcall
#define ULMDBL				virtual double		_stdcall

#define ULMETHODBOOL		virtual BOOL		_stdcall
#define ULMBOOL				virtual BOOL		_stdcall
#define ULMETHODBYT			virtual BYTE		_stdcall
#define ULMBYT				virtual BYTE		_stdcall
#define ULMETHODWRD			virtual WORD		_stdcall
#define ULMWRD				virtual WORD		_stdcall
#define ULMETHODDWD			virtual DWORD		_stdcall
#define ULMDWD				virtual DWORD		_stdcall
#define ULMETHODUNT			virtual UINT		_stdcall
#define ULMUNT				virtual UINT		_stdcall
#define ULMETHODTSTR		virtual LPTSTR		_stdcall
#define ULMTSTR				virtual LPTSTR		_stdcall
#define ULMCTSTR			virtual LPCTSTR		_stdcall

#define ULMETHODBSTR		virtual _bstr_t		_stdcall
#define ULMETHODVAR			virtual _variant_t	_stdcall

#define ULMVOIDIMP          void        _stdcall
#define ULMETHODIMP			short		_stdcall
#define ULMIMP				short		_stdcall
#define ULMETHODPTRIMP		void*		_stdcall
#define ULMPTRIMP			void*		_stdcall
#define ULMETHODSTRIMP		char*		_stdcall
#define ULMSTRIMP			char*		_stdcall
#define ULMETHODINTIMP		int			_stdcall
#define ULMINTIMP			int			_stdcall

#define ULMETHODCHRIMP		char		_stdcall
#define ULMETHODSHTIMP		short		_stdcall
#define ULMETHODLNGIMP		long		_stdcall
#define ULMLNGIMP			long		_stdcall
#define ULMETHODFLTIMP		float		_stdcall
#define ULMFLTIMP			float		_stdcall
#define ULMETHODDBLIMP		double		_stdcall
#define ULMDBLIMP			double		_stdcall

#define ULMETHODBOOLIMP		BOOL		_stdcall
#define ULMBOOLIMP			BOOL		_stdcall
#define ULMETHODBYTIMP		BYTE		_stdcall
#define ULMBYTIMP			BYTE		_stdcall
#define ULMETHODWRDIMP		WORD		_stdcall
#define ULMETHODDWDIMP		DWORD		_stdcall
#define ULMDWDIMP			DWORD		_stdcall
#define ULMETHODUNTIMP		UINT		_stdcall
#define ULMUNTIMP			UINT		_stdcall
#define ULMETHODTSTRIMP		LPTSTR		_stdcall
#define ULMTSTRIMP			LPTSTR		_stdcall

#define ULMETHODBSTRIMP		_bstr_t		_stdcall
#define ULMBSTRIMP			_bstr_t		_stdcall
#define ULMETHODVARIMP		_variant_t	_stdcall
#define ULMVARIMP			_variant_t	_stdcall

#define ULCALLBACK			LRESULT		WINAPI

#define	UL_NO_ERROR         0
#define	UL_ERROR            1

interface IULTool;
interface ITool;
interface ICurve;
interface IPrintInfo;
class CUnits;

typedef struct tagULTOOL
{
	IULTool*	pIULTool;
	ITool*		pITool;
} ULTOOL;

typedef struct  
{
	VARTYPE vt;		//2 bytes
	void*	pValue;	//4 bytes
	size_t  sz;		//4	bytes
} CurveValue;

#define ULTOOLS				std::vector<ULTOOL>
#define CURVES				std::vector<ICurve*>
#define CURVEPROPS			std::vector<CURVEPROPERTY*>

typedef std::vector<ULTOOL>		vec_ts;
typedef std::vector<ICurve*>	vec_ic;
typedef std::vector<CurveValue>	vec_cv;

// MFC Class Declaration
class CFile;
class CDC;
class CSize;
class CStringArray;

// General Class Declaration
class CDashLine;
class CULPrintInfo;

struct CTemplInfo
{
	CString strFile;
	CString strTempl;
};

#ifndef __AFXTEMPL_H__
	#include <afxtempl.h>
#endif

typedef CList<CTemplInfo*, CTemplInfo*> CTemplInfoList;

class CSheetInfo;
class CCellInfo;
class CCstInfo;
class CPofInfo;
typedef CSheetInfo* LPSHEETINFO;
typedef CList<CSheetInfo*, CSheetInfo*> CSheetInfoList;
typedef CCellInfo*	LPCELLINF0;
typedef CList<CCellInfo*, CCellInfo*> CCellInfoList;
typedef CCstInfo*	LPCSTNF0;
typedef CList<CCstInfo*, CCstInfo*> CCstInfoList;
typedef CPofInfo* LPPOFINFO;
typedef CList<CPofInfo*, CPofInfo*>	CPofInfoList;

class CDBFilterCurveInfo;

enum { ftT1, ftT2, ftR, ftCT, ftCU, ftDV, ftCount}; // 一级标题  二级标题  正文  曲线标题  曲线单位 深度标识

typedef BOOL (PASCAL *PAINTPROC)(CDC* pDC, LPRECT lpRect, ULTOOL* pTool);
typedef LRESULT (WINAPI *PPAINTFUNC)(LPVOID, CDC*, LPRECT);


#define DECLARE_ULI_MAP()	public:\
								virtual HRESULT _stdcall QueryInterface(const IID& iid, void** ppv);\
								virtual ULONG _stdcall AddRef()		{ return 0; }\
								virtual ULONG _stdcall Release()	{ delete this; return 1; }

#define DECLARE_ULI_MAPX()	public:\
								virtual HRESULT _stdcall QueryInterface(const IID& iid, void** ppv);\
								virtual ULONG _stdcall AddRef()		{ return 0; }\
								virtual ULONG _stdcall Release()	{ return 1; }

#define BEGIN_ULI_MAP(theClass, iface) HRESULT _stdcall theClass::QueryInterface(const IID& iid, void** ppv)\
								{\
									if(iid == IID_IUnknown)\
										*ppv = static_cast <iface *>(this);
#define ULI_PART(ifaceid, iface)	else if(iid == ifaceid)\
										*ppv = static_cast <iface *>(this);
#define ULI_PART_IMP(ifaceid, iface, ifaceimp) else if (iid == ifaceid)\
										{ *ppv = static_cast <iface *>(ifaceimp);\
										  if (NULL == *ppv) return E_NOINTERFACE; }
#define END_ULI_MAP()		else\
							{\
								*ppv = NULL;\
								return E_NOINTERFACE;\
							}\
							reinterpret_cast <IUnknown *>(*ppv)->AddRef();\
							return S_OK;\
						}

#define DECLARE_ULI(ifaceid, iface)	public:\
										HRESULT _stdcall QueryInterface(const IID& iid,void** ppv)\
										{\
											if(iid == IID_IUnknown)\
												*ppv = static_cast <iface *>(this);\
											else if(iid == ifaceid)\
												*ppv = static_cast <iface *>(this);\
											else\
											{\
												*ppv = NULL;\
												return E_NOINTERFACE;\
											}\
											reinterpret_cast <IUnknown *>(*ppv)->AddRef();\
											return S_OK;\
										}\
										ULONG _stdcall AddRef()\
										{ return InterlockedIncrement(&m_lRef); }\
										ULONG _stdcall Release()\
										{\
											if (InterlockedDecrement(&m_lRef) == 0)\
											{\
												delete this;\
												return 0;\
											}\
											return m_lRef;\
										}\
										long m_lRef;

#define DECLARE_DATA_EXCHANGE()	  ULMETHOD DeclareDataExchange();
		

#define BEGIN_DATA_EXCHANGE(theClass)	typedef LRESULT (WINAPI  theClass::* pPaintFunc)(CDC*, LPRECT);\
										ULMETHODIMP theClass::DeclareDataExchange(){ 
		
#define DATA_EXCHANGE(id, data) \
		m_pIULTool->DDX(id, &data); \

#define DATA_CELL_EXCHANGE(id, data) \
		CString strTemp = data; \
		m_pIULTool->AddCell(strTemp.GetBuffer(strTemp.GetLength())); \
		strTemp.ReleaseBuffer();

#define DATA_FUN_EXCHANGE(id, data) \
		m_pIULTool->DDX(id, (void*)data);

#define DDX_CELL(cid, cell)	\
m_pIULTool->AddCell(#cell);

#define DDX_VAR(vid, var) \
m_pIULTool->DDX(#vid, &var);

#define DDX_PAINT_FUNC(fid, func) \
{\
	pPaintFunc p = func;\
	DWORD dwAddress;\
	memcpy(&dwAddress, &p, sizeof(p));\
	m_pIULTool->DDXF(#fid, (void*)&dwAddress);\
}

#define END_DATA_EXCHANGE()	 return UL_NO_ERROR;}



#endif


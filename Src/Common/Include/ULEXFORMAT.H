/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __ULEXFORMAT_H
#define __ULEXFORMAT_H

//ULExFormat.h

/*************************** 数据交换结构定义 ****************************/
///////////////////////

//通道状态 


//命令类型
typedef enum tagCOMMANDTYPE         
{
	UL_ASKCHANNEL      = 0,             //请求一通道命令	
	UL_SENDCOMMAND     = 1,             //向一通道发送控制命令
	UL_MANUALADJUST    = 2,             //手工分配命令
	UL_SCALETOOL       = 3,             //刻度命令
	UL_FILEREBACK      = 4,             //文件回放命令
	UL_SETWORKMODE     = 5,             //工作模式设定
	UL_SETBOARDMODE    = 6,             //设置板卡模式
	UL_REPLAYPARAM     = 7,             //回放参数设定	
	UL_CLOSEALLCHANNEL = 8,             //关闭所有模拟通道命令	
	UL_STARTBOARD      = 9,             //start board 命令
	UL_STOPBOARD       = 10,            //stop board
	UL_BOARDBEFOREIO   =11,             //beforeIO board
	UL_BOARDAFTERIO   = 12,             //afterIO  board
	UL_CLOSEDATAMANNEGE = 13,
	UL_CLOSESINGLECHANNEL=14,           //关闭单一模拟通道命令 
}CommandType;

///////////////////////
//命令执行情况
typedef enum tagCHASKRESULT   
{
	CH_FAIL   = 0,                     //失败
	CH_ISOK   = 1,                     //一次分配成功(或手动调整完毕)    
	CH_SECONDOK = 2                    //二次分配成功(注：第一枚找到合适的通道，把一要求低的通道及到新位置)
}CHAskResult;

//////////////////////
//通道信息
typedef struct tagEXCOMPARAM
{
	short  nPort;
	char   chSetString;
	short  nOutBufSize;
	short  nInBufSize;
	short  nShakeHand;
}EXComParam;

/////////////////////////////////////////////
//申请通道交换参数
typedef struct tagEXCHANNELPARAM
{
	DWORD       dwCHID;               //通道ID号
	double      dDataRate;
	long        lPrecision;
	TCHAR		chBoardType[32];
	TCHAR       chEventName[64];      //通道事件名	
	TCHAR		chMapFileName[64];	  //通道映射文件名
	TCHAR       chUsedToolName[32];   //用此通道的仪器名
	TCHAR       chCurveName[32];
	DWORD       dwCHBufferSize;       //通道缓冲区大小
	DWORD       dwToolIndex;          //仪器序号
	CHAskResult result;		
}EXChannelParam;

/////////////////////////////////////////////
//命令交换参数
typedef struct tagEXCOMMANDPARAM
{
	DWORD     dwCHID;                 //通道ID号
	DWORD     dwReturn;               //命令返回值
	UINT      cmd_size;               //命令长度
	BYTE      cmd_buf[2048];          //命令内容（字符串）
	//char        CmdErrorInfo[128];  //出错信息提示
	//BOOL        CmdState;           //命令执行是否成功
}EXCommandParam;
/////////////////////////////////////////////
//模式改变参数
typedef struct tagEXCHMODEPARAM
{	
	DWORD     dwCHID;             //通道ID号
	int       nWorkMode;
	UINT      mode_size;               //命令长度
	BYTE      mode_buf[1024];		  //命令内容（字符串）
	
}EXCHModeParam;
/////////////////////////////////////////////
//模式改变参数
typedef struct tagEXWORKMODEPARAM
{
	DWORD     dwCHID;             //通道ID号
	int       nWorkMode;	
}EXWorkModeParam;
/////////////////////////////////////////////
//刻度仪器交换参数
typedef struct tagEXSCALETOOLPARAM
{
	DWORD     dwCHID;                 //通道ID号
	UINT      cmd_size;               //命令长度
	DWORD     dwReturn;               //命令返回值
	BYTE      cmd_buf[1024];          //命令内容（字符串）	
}EXScaleParam;

/////////////////////////////////////////////
//文件回放参数
typedef struct tagEXREPLAYPARAM
{
	DWORD dwCHID;             //通道ID号
	char  chFileName[128];
	DWORD dwPlayBackSpeed;
}EXReplayParam;
///////////////////////////////////////////
//交换参数联合体
typedef union tagEXCHANGECONTEXT
{
	EXChannelParam    ch_param;
	EXCommandParam    cmd_param;
	EXScaleParam      scale_param;
	EXCHModeParam     chmode_param;
	EXWorkModeParam   workmode_param;
	EXReplayParam     replay_param;
}ExchangeContext;

typedef struct tagASKCOMMAND
{		
	CommandType	nCommandType;       //命令类型	
	ExchangeContext ex_context;
}AskCommand;


////////////////////////////////////////////////////////////////////////////////////
#endif
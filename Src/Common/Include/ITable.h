/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __IULTABLE_H__
#define __IULTABLE_H__

#include "ULInterface.h"

typedef struct tagTABLE_FIELD
{
	char	Name[32];
	WORD	RepCode;
	WORD	Length;
	char	CHName[64];
	char	Unit[32];
	DWORD	Reserved;
}TABLE_FIELD;

// Interface
interface ITable : IUnknown
{
	ULMETHODDWD	GetRecordCount() = 0;
	ULMETHOD	SetRecordCount(DWORD dwRecord) = 0;
	ULMETHODDWD GetFieldCount() = 0;
	ULMETHOD	SetFieldCount(DWORD dwField) = 0;
	ULMETHODPTR	GetFieldInfo(DWORD dwIndex) = 0;
	ULMETHOD	GetFieldInfo(DWORD dwIndex, TABLE_FIELD* pFieldInfo) = 0;
	ULMETHODDWD GetRecordLength(DWORD dwIndex) = 0;
	ULMETHOD	ReadRecord() = 0;
	ULMETHOD	WriteRecord() = 0;
	ULMETHOD	AddRecord() = 0;
	ULMETHOD	InsertRecord() = 0;
	ULMETHOD	GetRecord() = 0;
	ULMETHOD	GetRecordSet() = 0;
	ULMETHOD	CombineRecordSet() = 0;
	ULMETHOD	ReadRecordSet() = 0;
};

// {DA687944-7BCC-48e7-9F64-BD3502210DC4}
static const IID IID_ITable = 
{ 0xda687944, 0x7bcc, 0x48e7, { 0x9f, 0x64, 0xbd, 0x35, 0x2, 0x21, 0xd, 0xc4 } };

#endif

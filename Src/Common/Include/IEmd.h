/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __IEMD_H__
#define __IEMD_H__

#include "ULInterface.h"
#include "ULCOMMDEF.H"

#define	UL_FINISH            2 // ProcessPer处理完成不需要主程序重复添加输出曲线数据
// Interfaces
interface IEmd : IUnknown
{
	// 基本操作
	ULMETHOD  AdviseEmd(void* pULEmd) = 0;
	ULMTSTR	  IDName() = 0;
	ULMETHOD  InitEmd() = 0;	// 初始化，检查
	ULMETHOD  ProcessPer(double* pInput, double* pOutput, double* pParams, long lDepth, long lTime) = 0;	// 曲线数据计算
	ULMETHOD  TermEmd() = 0;	// 校正结束
};

// 接口ID

// {4C61A8D5-1204-445b-9FB6-0A4AAB411C44}
static const IID IID_IEmd = 
{ 0x4c61a8d5, 0x1204, 0x445b, { 0x9f, 0xb6, 0xa, 0x4a, 0xab, 0x41, 0x1c, 0x44 } };

extern "C" DWORD ULGetVersion();
extern "C" IUnknown* CreateEmd();

#endif
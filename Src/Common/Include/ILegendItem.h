/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
// ILegendItem.h: interface for the ILegendItem class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ILEGENDITEM_H__28EA281E_FA10_4C6A_AB38_1ED979BBD54A__INCLUDED_)
#define AFX_ILEGENDITEM_H__28EA281E_FA10_4C6A_AB38_1ED979BBD54A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//////////////////////////////////////////////////////////////////////
// ILegendItem: 图例子项接口

class CXmlArchive; 
class ILegendItem  
{
public:
    virtual int GetItemWidth(CDC* pDC, LPLOGFONT pFont) = 0; 
    virtual void SetItemRect(CRect rect) = 0; 
    virtual CRect GetItemRect() = 0; 
    virtual void DrawItem(CDC* pDC, CRect rect, LPLOGFONT pFont) = 0; 
};

#endif // !defined(AFX_ILEGENDITEM_H__28EA281E_FA10_4C6A_AB38_1ED979BBD54A__INCLUDED_)

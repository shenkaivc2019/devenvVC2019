/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __IULEMD_H__
#define __IULEMD_H__

#include "ULInterface.h"
// Interface

interface IULEmd : IUnknown
{
	// ---------------------------------
	//	获取曲线:
	// ---------------------------------

	ULMPTR	GetICurve(LPCTSTR pszCName) = 0;	// 获得输入曲线，不存在时返回NULL
	ULMPTR  GetOCurve(LPCTSTR pszCName, int nValueType = VT_R4 , int nDimension = 1) = 0;
	// 获得输出曲线 (曲线不存在时，自动创建数据类型为nValueType的曲线，存在时返回已有曲线)

	// ---------------------------------
	//	获取参数:
	// ---------------------------------
	
	ULMINT	GetParamsCount() = 0;				 // 获得参数卡个数
	ULMTSTR	GetParamName(int nIndex) = 0;		 // 获得第n个参数名称
	ULMINT  GetParamIndex(LPCTSTR pszPName) = 0;	// 获得参数索引,-1表示该参数不存在
	ULMDBL	GetParamVal(int nIndex) = 0;		// 获得参数双精度浮点值(缺省),不存在时返回0
	ULMTSTR GetParamStr(int nIndex) = 0; 		// 获得参数字符值(缺省),不存在时返回空串
	ULMINT  GetParamInt(int nIndex) = 0;		// 获得参数整形值(布尔值/枚举值)(缺省),不存在时返回-1
	ULMDBL	GetParamVald(int nIndex, long lDepth) = 0;	// 获得指定深度参数双精度浮点值
	ULMTSTR GetParamStrd(int nIndex, long lDepth) = 0; 	// 获得指定深度参数字符值
	ULMINT  GetParamIntd(int nIndex, long lDepth) = 0;	// 获得指定深度参数整形值(布尔值/枚举值)
	ULMTSTR	GetParamUnit(int nIndex) = 0; // 获得参数单位
	ULMTSTR GetParamDesc(int nIndex) = 0; // 获得参数描述信息
	ULMDBL	GetParamVal(LPCTSTR pszPName) = 0;		// 获得参数双精度浮点值(缺省),不存在时返回0
	ULMTSTR GetParamStr(LPCTSTR pszPName) = 0; 		// 获得参数字符值(缺省),不存在时返回空串
	ULMINT  GetParamInt(LPCTSTR pszPName) = 0;		// 获得参数整形值(布尔值/枚举值)(缺省),不存在时返回-1
	ULMDBL	GetParamVald(LPCTSTR pszPName, long lDepth) = 0;	// 获得指定深度参数双精度浮点值
	ULMTSTR GetParamStrd(LPCTSTR pszPName, long lDepth) = 0; 	// 获得指定深度参数字符值
	ULMINT  GetParamIntd(LPCTSTR pszPName, long lDepth) = 0;	// 获得指定深度参数整形值(布尔值/枚举值)
	ULMTSTR	GetParamUnit(LPCTSTR pszPName) = 0; // 获得参数单位
	ULMTSTR GetParamDesc(LPCTSTR pszPName) = 0; // 获得参数描述信息
	
	// ---------------------------------
	// Get Depth Ranges, you need to initialize memory enough for pDepths
	// pDepths size must be GetDepthsCount()*sizeof(long) at least
	// the pDepths will be set : lStart0, lEnd0, lStart1, lEnd1 ... 
	// pDepths in ASC order : lStart0 <= lEnd0 <= lStart1 <= lEnd1 ...
	// so the ranges count is GetDepthsCount()/2
	// ---------------------------------
	ULMINT  GetDepthsCount() = 0;
	ULMINT  GetDepths(long* pDepths) = 0;

	// ---------------------------------
	//	Get params' value once, nCount = -1 is to the end,
	//  you need to initialize memory enough for pValues
	//  pValues size must be nCount*sizeof(double) at least
	//  result is you get values' count actually
	// ---------------------------------
	ULMINT  GetParamsVal(double* pValues, int iStart = 0, int nCount = -1) = 0;
	ULMINT  GetParamsVald(double* pValues, long lDepth, int iStart = 0, int nCount = -1) = 0;

	// ---------------------------------
	//	When first, no config file, uses defaults settings
	// ---------------------------------
	ULMINT DefICurves(int nCount, CString* pICurves, CString* pRemarks) = 0;
	ULMINT DefOCurves(int nCount, CString* pOCurves, CString* pRemarks) = 0;
	ULMINT DefParams(int nCount, CString* pParams, double* pValues, CString* pUnits, CString* pDescs) = 0;

	// User defined process, for Rw correction
	ULMETHOD UserProcess() = 0;
};

// {3AC5DB35-D2E5-43c4-86B2-FA73C88C9E2D}
static const IID IID_IULEmd = 
{ 0x3ac5db35, 0xd2e5, 0x43c4, { 0x86, 0xb2, 0xfa, 0x73, 0xc8, 0x8c, 0x9e, 0x2d } };

#endif

/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
// ICrossView.h: interface for the ICrossView class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ICROSSVIEW_H__A4FEF5EE_3EE3_4903_9B4F_A828928A160E__INCLUDED_)
#define AFX_ICROSSVIEW_H__A4FEF5EE_3EE3_4903_9B4F_A828928A160E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Definition.h"

//////////////////////////////////////////////////////////////////////
// IInplaceEditSupport: 即地文本编辑的支持

class IInplaceEditSupport
{
public:
    virtual HWND GetHwnd() = 0; 
    virtual void EndEdit() = 0; 
}; 

//////////////////////////////////////////////////////////////////////
// ICrossView: 交会图接口 

class CFigureObject; 
class CFigureObList; 
class CFigureTool; 
class CULDoc; 
class ICrossView : public IInplaceEditSupport
{
public:
    virtual HWND GetHwnd() = 0; 
    virtual void InvalidateView(LPRECT lpRect = NULL) = 0; 
    virtual void SetMouseCapture() = 0; 
    virtual void DrawTrackRect(CRect rect, CRect rtLast) = 0; 

    virtual void SetSelection(UINT nFlags = FF_CLEAR) = 0; 
    virtual void SelectFigure(CPoint point, UINT nFlags = FF_CLEAR_BEFORE_SET) = 0; 
    virtual void SelectWithinRect(CRect rect, UINT nFlags = FF_CLEAR_BEFORE_SET) = 0; 
    
    virtual UINT GetNewID() = 0; 
    virtual void AddFigure(CFigureObject* pFigure) = 0; 
    virtual void RemoveFigure(CFigureObject* pFigure = NULL) = 0; 
    virtual void MoveFigure(CSize offset, UINT nFlags = FF_MOVE_SELECTION) = 0; 
    virtual CFigureObject* GetFigureAtPoint(CPoint point, UINT nFlags = FF_NONE) = 0;
    virtual void GetChildFigureByID(int nID, CObList* pChildList) = 0; 
    virtual CFigureObject* GetFigureByID(int nID) = 0; 
    
    virtual CFigureTool* GetTool(UINT nIndex) = 0; 
    virtual CFigureTool* GetCurrentTool() = 0; 
    virtual void SetCurrentTool(UINT nIndex) = 0; 

    virtual void SetModifiedFlag() = 0; 
    virtual void BeginEdit(CFigureObject* pFigure) = 0; 
    virtual void EndEdit() = 0; 
    
    virtual CULDoc* GetWell() = 0; 

public:
    // interface for GroupView.
    virtual CSize GetViewSize() = 0; 
    virtual void DrawInGroupView(CDC* pDC, LPRECT lpRect) = 0; 
    virtual CString GetFileName() = 0; 
    virtual void Print(CDC* pDC, LPRECT pRect) = 0; 
};

CString GetFileVersion(); 
void SetFileVersion(LPCTSTR lpszFileVersion); 

#endif // !defined(AFX_ICROSSVIEW_H__A4FEF5EE_3EE3_4903_9B4F_A828928A160E__INCLUDED_)

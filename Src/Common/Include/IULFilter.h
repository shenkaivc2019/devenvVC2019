/***************************************************************************************** 
 * 修订记录
 * 
 * 作者：           软件组
 * 创建日期：       2013-7-18
 * 平台版本号：     AXP 1.70
 *
 * 修订说明：       SDK 修订记录以及新增函数说明
 * 版本号：			1.70
 *                  创建
 * 版本号：			下一版本号
 *                  增加函数说明
 *					以后每个发布版本修订记录以此格式依次增加
 ******************************************************************************************/
#ifndef __IULFILTER_H__
#define __IULFILTER_H__

#include "ULInterface.h"
// Interface

interface IULFilter : IUnknown
{
	// ---------------------------------
	//	滤波参数设置:
	// ---------------------------------

	ULMBOOL	AddParam(LPCTSTR strParamName, double dValue = 0.0, LPCTSTR strDescripiton = NULL) = 0;
	ULMINT	GetParamsCount() = 0;
	ULMTSTR	GetParamName(int nIndex) = 0;
	ULMDBL	GetParamValue(int nIndex) = 0;
	ULMTSTR	GetParamDescription(int nIndex) = 0;
	ULMETHOD	RemoveAllParams() = 0;
	
	// ---------------------------------
	//	曲线操作:
	// ---------------------------------

	ULMDBL		GetCurveValue(ICurve* pCurve, int nIndex) = 0;
	ULMETHOD	SetCurveValue(ICurve* pCurve, int nIndex, double fltVal) = 0;
	ULMINT		GetCurveValueUB(ICurve* pCurve) = 0;
	ULMETHOD	Filter(ICurve* pCurve, int nStartPos, int nEndPos) = 0;
	ULMETHOD	Filter(double& dValue, BOOL bClear) = 0;
	ULMETHOD	Filter(FILTER_VALUE *pInValue , FILTER_VALUES &vecOut , BOOL bClear) = 0;

	ULMTSTR		GetFilterName() = 0;
	ULMBOOL		LoadFilter(LPCTSTR strFilter) = 0;
	ULMTSTR     GetFilterPath() = 0;

	ULMETHOD    SetTemplate(int nTemplate) = 0;
	ULMINT      GetTemplate() = 0;

	// 获取指定曲线指针 2011/9/13 add by bao
	//ULMPTR		GetCurvePtr(LPCTSTR pszName) = 0;
};

extern "C" IULFilter* CreateULFilter();

// {418EB221-93F2-4416-B048-B2D4AA382FC9}
static const IID IID_IULFilter = 
{ 0x418eb221, 0x93f2, 0x4416, { 0xb0, 0x48, 0xb2, 0xd4, 0xaa, 0x38, 0x2f, 0xc9 } };

#endif

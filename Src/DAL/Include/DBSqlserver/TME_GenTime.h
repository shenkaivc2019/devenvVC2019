//---------------------------------------------------------------------------//
// 文件名: ME_GenTime.h
// 说明:	内存基本时间数据表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/6/8 8:53:43
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TME_GenTime.h : DME_GenTime

#ifndef	_TME_GENTIME_H
#define	_TME_GENTIME_H

#define	TID_ME_GENTIME								_T("ME_GenTime")
#define	OID_ME_GENTIME								_T("")

// Sort No
#define	SORT_ME_GENTIME_PK0				0							// PK:
//#define	SORT_ME_GENTIME_A1							%#%							// A1:

// Colum No
#define	COL_ME_GENTIME_DATAID					(short)0						// 
#define	COL_ME_GENTIME_RUNID					(short)1						// 趟钻编号
#define	COL_ME_GENTIME_TDATETIME					(short)2						// 时间（精确到秒）
#define	COL_ME_GENTIME_MILLITIME					(short)3						// 毫秒
#define	COL_ME_GENTIME_TOOLID					(short)4						// 工具编号
#define	COL_ME_GENTIME_MDEPTH					(short)5						// 电阻率测量井深
#define	COL_ME_GENTIME_VDEPTH					(short)6						// 电阻率测量垂深
#define	COL_ME_GENTIME_DATE					(short)7						// 日期
#define	COL_ME_GENTIME_TIME					(short)8						// 时间
#define	COL_ME_GENTIME_ACTC					(short)9						// 钻井状态参数
#define	COL_ME_GENTIME_DBTM					(short)10						// 钻头测量深度
#define	COL_ME_GENTIME_DBTV					(short)11						// 钻头垂直深度
#define	COL_ME_GENTIME_DMEA					(short)12						// 测量井深
#define	COL_ME_GENTIME_DVER					(short)13						// 垂直井深
#define	COL_ME_GENTIME_BPOS					(short)14						// 大钩高度
#define	COL_ME_GENTIME_ROPA					(short)15						// 钻时
#define	COL_ME_GENTIME_HKLA					(short)16						// 大钩负荷（平均值）
#define	COL_ME_GENTIME_HKLX					(short)17						// 大钩负荷（最大值）
#define	COL_ME_GENTIME_WOBA					(short)18						// 钻压（平均值）
#define	COL_ME_GENTIME_WOBX					(short)19						// 钻压（最大值）
#define	COL_ME_GENTIME_TQA					(short)20						// 转盘扭矩（平均值）
#define	COL_ME_GENTIME_TQX					(short)21						// 转盘扭矩 （最大值）
#define	COL_ME_GENTIME_RPMA					(short)22						// 转盘转速（平均值）
#define	COL_ME_GENTIME_SPPA					(short)23						// 立管压力（平均值）
#define	COL_ME_GENTIME_CHKP					(short)24						// 套管压力
#define	COL_ME_GENTIME_SPM1					(short)25						// 泵冲数1
#define	COL_ME_GENTIME_SPM2					(short)26						// 泵冲数2
#define	COL_ME_GENTIME_SPM3					(short)27						// 泵冲数3
#define	COL_ME_GENTIME_TVA					(short)28						// 活动池体积
#define	COL_ME_GENTIME_TVCA					(short)29						// 池体积变化量
#define	COL_ME_GENTIME_MFOP					(short)30						// 相对出口流量
#define	COL_ME_GENTIME_MFOA					(short)31						// 出口流量（平均值）
#define	COL_ME_GENTIME_MFIA					(short)32						// 入口流量（平均值）
#define	COL_ME_GENTIME_MDOA					(short)33						// 出口密度（平均值）
#define	COL_ME_GENTIME_MDIA					(short)34						// 入口密度（平均值）
#define	COL_ME_GENTIME_MTOA					(short)35						// 出口温度（平均值）
#define	COL_ME_GENTIME_MTIA					(short)36						// 入口温度（平均值）
#define	COL_ME_GENTIME_MCOA					(short)37						// 出口电导（平均值）
#define	COL_ME_GENTIME_MCIA					(short)38						// 入口电导（平均值）
#define	COL_ME_GENTIME_STKC					(short)39						// 泵冲总数
#define	COL_ME_GENTIME_LAGSTKS					(short)40						// 迟到泵冲数
#define	COL_ME_GENTIME_DEPTRETM					(short)41						// 迟到井深
#define	COL_ME_GENTIME_GASA					(short)42						// 全烃（平均）
#define	COL_ME_GENTIME_CONF					(short)43						// 旋转状态:No|0.否;Yes|1.是
#define	COL_ME_GENTIME_SLIDING					(short)44						// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	COL_ME_GENTIME_BAD					(short)45						// 是否坏点:No|0.否;Yes|1.是
#define	COL_ME_GENTIME_DRILLACTIV					(short)46						// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	COL_ME_GENTIME_RELOG					(short)47						// 是否复测 :Yes|0.否;No|1.是
#define	COL_ME_GENTIME_LAS					(short)48						// 是否Las :Yes|0.否;No|1.是
#define	COL_ME_GENTIME_CREATETIME					(short)49						// 创建时间戳
#define	COL_ME_GENTIME_STATUS					(short)50						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_ME_GENTIME_MEMO					(short)51						// 备注

// Colum(Field) Name
#define	FLD_ME_GENTIME_DATAID					_T("DataID")					// 
#define	FLD_ME_GENTIME_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_ME_GENTIME_TDATETIME					_T("TDateTime")					// 时间（精确到秒）
#define	FLD_ME_GENTIME_MILLITIME					_T("Millitime")					// 毫秒
#define	FLD_ME_GENTIME_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_ME_GENTIME_MDEPTH					_T("MDepth")					// 电阻率测量井深
#define	FLD_ME_GENTIME_VDEPTH					_T("VDepth")					// 电阻率测量垂深
#define	FLD_ME_GENTIME_DATE					_T("DATE")					// 日期
#define	FLD_ME_GENTIME_TIME					_T("TIME")					// 时间
#define	FLD_ME_GENTIME_ACTC					_T("ACTC")					// 钻井状态参数
#define	FLD_ME_GENTIME_DBTM					_T("DBTM")					// 钻头测量深度
#define	FLD_ME_GENTIME_DBTV					_T("DBTV")					// 钻头垂直深度
#define	FLD_ME_GENTIME_DMEA					_T("DMEA")					// 测量井深
#define	FLD_ME_GENTIME_DVER					_T("DVER")					// 垂直井深
#define	FLD_ME_GENTIME_BPOS					_T("BPOS")					// 大钩高度
#define	FLD_ME_GENTIME_ROPA					_T("ROPA")					// 钻时
#define	FLD_ME_GENTIME_HKLA					_T("HKLA")					// 大钩负荷（平均值）
#define	FLD_ME_GENTIME_HKLX					_T("HKLX")					// 大钩负荷（最大值）
#define	FLD_ME_GENTIME_WOBA					_T("WOBA")					// 钻压（平均值）
#define	FLD_ME_GENTIME_WOBX					_T("WOBX")					// 钻压（最大值）
#define	FLD_ME_GENTIME_TQA					_T("TQA")					// 转盘扭矩（平均值）
#define	FLD_ME_GENTIME_TQX					_T("TQX")					// 转盘扭矩 （最大值）
#define	FLD_ME_GENTIME_RPMA					_T("RPMA")					// 转盘转速（平均值）
#define	FLD_ME_GENTIME_SPPA					_T("SPPA")					// 立管压力（平均值）
#define	FLD_ME_GENTIME_CHKP					_T("CHKP")					// 套管压力
#define	FLD_ME_GENTIME_SPM1					_T("SPM1")					// 泵冲数1
#define	FLD_ME_GENTIME_SPM2					_T("SPM2")					// 泵冲数2
#define	FLD_ME_GENTIME_SPM3					_T("SPM3")					// 泵冲数3
#define	FLD_ME_GENTIME_TVA					_T("TVA")					// 活动池体积
#define	FLD_ME_GENTIME_TVCA					_T("TVCA")					// 池体积变化量
#define	FLD_ME_GENTIME_MFOP					_T("MFOP")					// 相对出口流量
#define	FLD_ME_GENTIME_MFOA					_T("MFOA")					// 出口流量（平均值）
#define	FLD_ME_GENTIME_MFIA					_T("MFIA")					// 入口流量（平均值）
#define	FLD_ME_GENTIME_MDOA					_T("MDOA")					// 出口密度（平均值）
#define	FLD_ME_GENTIME_MDIA					_T("MDIA")					// 入口密度（平均值）
#define	FLD_ME_GENTIME_MTOA					_T("MTOA")					// 出口温度（平均值）
#define	FLD_ME_GENTIME_MTIA					_T("MTIA")					// 入口温度（平均值）
#define	FLD_ME_GENTIME_MCOA					_T("MCOA")					// 出口电导（平均值）
#define	FLD_ME_GENTIME_MCIA					_T("MCIA")					// 入口电导（平均值）
#define	FLD_ME_GENTIME_STKC					_T("STKC")					// 泵冲总数
#define	FLD_ME_GENTIME_LAGSTKS					_T("LAGSTKS")					// 迟到泵冲数
#define	FLD_ME_GENTIME_DEPTRETM					_T("DEPTRETM")					// 迟到井深
#define	FLD_ME_GENTIME_GASA					_T("GASA")					// 全烃（平均）
#define	FLD_ME_GENTIME_CONF					_T("Conf")					// 旋转状态:No|0.否;Yes|1.是
#define	FLD_ME_GENTIME_SLIDING					_T("Sliding")					// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	FLD_ME_GENTIME_BAD					_T("Bad")					// 是否坏点:No|0.否;Yes|1.是
#define	FLD_ME_GENTIME_DRILLACTIV					_T("DrillActiv")					// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	FLD_ME_GENTIME_RELOG					_T("ReLog")					// 是否复测 :Yes|0.否;No|1.是
#define	FLD_ME_GENTIME_LAS					_T("Las")					// 是否Las :Yes|0.否;No|1.是
#define	FLD_ME_GENTIME_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_ME_GENTIME_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_ME_GENTIME_MEMO					_T("Memo")					// 备注

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_ME_GENTIME_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_ME_GENTIME_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_ME_GENTIME
{
 
  int	iDataID;							// 
  char	szRunID[TV_ME_GENTIME_RUNID_DIGITS + 1];							// 趟钻编号
  time_t	lTDateTime;							// 时间（精确到秒）
  int	iMillitime;							// 毫秒
  int	iToolID;							// 工具编号
  long	lMDepth;							// 电阻率测量井深
  long	lVDepth;							// 电阻率测量垂深
  float	fDATE;							// 日期
  float	fTIME;							// 时间
  float	fACTC;							// 钻井状态参数
  float	fDBTM;							// 钻头测量深度
  float	fDBTV;							// 钻头垂直深度
  float	fDMEA;							// 测量井深
  float	fDVER;							// 垂直井深
  float	fBPOS;							// 大钩高度
  float	fROPA;							// 钻时
  float	fHKLA;							// 大钩负荷（平均值）
  float	fHKLX;							// 大钩负荷（最大值）
  float	fWOBA;							// 钻压（平均值）
  float	fWOBX;							// 钻压（最大值）
  float	fTQA;							// 转盘扭矩（平均值）
  float	fTQX;							// 转盘扭矩 （最大值）
  float	fRPMA;							// 转盘转速（平均值）
  float	fSPPA;							// 立管压力（平均值）
  float	fCHKP;							// 套管压力
  float	fSPM1;							// 泵冲数1
  float	fSPM2;							// 泵冲数2
  float	fSPM3;							// 泵冲数3
  float	fTVA;							// 活动池体积
  float	fTVCA;							// 池体积变化量
  float	fMFOP;							// 相对出口流量
  float	fMFOA;							// 出口流量（平均值）
  float	fMFIA;							// 入口流量（平均值）
  float	fMDOA;							// 出口密度（平均值）
  float	fMDIA;							// 入口密度（平均值）
  float	fMTOA;							// 出口温度（平均值）
  float	fMTIA;							// 入口温度（平均值）
  float	fMCOA;							// 出口电导（平均值）
  float	fMCIA;							// 入口电导（平均值）
  float	fSTKC;							// 泵冲总数
  float	fLAGSTKS;							// 迟到泵冲数
  float	fDEPTRETM;							// 迟到井深
  float	fGASA;							// 全烃（平均）
  short	nConf;							// 旋转状态:No|0.否;Yes|1.是
  short	nSliding;							// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
  short	nBad;							// 是否坏点:No|0.否;Yes|1.是
  short	nDrillActiv;							// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
  short	nReLog;							// 是否复测 :Yes|0.否;No|1.是
  short	nLas;							// 是否Las :Yes|0.否;No|1.是
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_ME_GENTIME_MEMO_DIGITS + 1];							// 备注
} TS_ME_GENTIME;

typedef	TS_ME_GENTIME FAR*	LPTS_ME_GENTIME;

#pragma	pack(pop)

#endif // _TME_GENTIME_H

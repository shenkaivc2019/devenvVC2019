//---------------------------------------------------------------------------//
// 文件名: RT_UDRImage.h
// 说明:	上下电阻率4点成像数据表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/10/9 15:57:05
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TRT_UDRImage.h : DRT_UDRImage

#ifndef	_TRT_UDRIMAGE_H
#define	_TRT_UDRIMAGE_H

#define	TID_RT_UDRIMAGE								_T("RT_UDRImage")
#define	OID_RT_UDRIMAGE								_T("")

// Sort No
#define	SORT_RT_UDRIMAGE_PK0				0							// PK:数据编号
//#define	SORT_RT_UDRIMAGE_A1							%#%							// A1:

// Colum No
#define	COL_RT_UDRIMAGE_DATAID					(short)0						// 数据编号
#define	COL_RT_UDRIMAGE_RUNID					(short)1						// 趟钻编号
#define	COL_RT_UDRIMAGE_TDATETIME					(short)2						// 时间（精确到秒）
#define	COL_RT_UDRIMAGE_MILLITIME					(short)3						// 毫秒
#define	COL_RT_UDRIMAGE_TOOLID					(short)4						// 工具编号
#define	COL_RT_UDRIMAGE_MDEPTH					(short)5						// SIML测量点井深
#define	COL_RT_UDRIMAGE_VDEPTH					(short)6						// SIML测量点垂深
#define	COL_RT_UDRIMAGE_URDATAID					(short)7						// 上电阻率数据对应表中的ID
#define	COL_RT_UDRIMAGE_DRDDATAID					(short)8						// 下电阻率数据对应表中的ID
#define	COL_RT_UDRIMAGE_LRDATAID					(short)9						// 左电阻率数据对应表中的ID
#define	COL_RT_UDRIMAGE_RRDDATAID					(short)10						// 右电阻率数据对应表中的ID
#define	COL_RT_UDRIMAGE_UR					(short)11						// 上电阻率数据
#define	COL_RT_UDRIMAGE_DR					(short)12						// 下电阻率数据
#define	COL_RT_UDRIMAGE_LR					(short)13						// 左电阻率数据
#define	COL_RT_UDRIMAGE_RR					(short)14						// 右电阻率数据
#define	COL_RT_UDRIMAGE_CONF					(short)15						// 可信度
#define	COL_RT_UDRIMAGE_BAD					(short)16						// 是否坏点:No|0.否;Yes|1.是
#define	COL_RT_UDRIMAGE_DRILLACTIV					(short)17						// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	COL_RT_UDRIMAGE_SLIDING					(short)18						// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	COL_RT_UDRIMAGE_RELOG					(short)19						// 是否复测 :Yes|0.否;No|1.是
#define	COL_RT_UDRIMAGE_LAS					(short)20						// 是否Las :No|0.否;Yes|1.是
#define	COL_RT_UDRIMAGE_CREATETIME					(short)21						// 创建时间戳
#define	COL_RT_UDRIMAGE_STATUS					(short)22						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_RT_UDRIMAGE_MEMO					(short)23						// 备注

// Colum(Field) Name
#define	FLD_RT_UDRIMAGE_DATAID					_T("DataID")					// 数据编号
#define	FLD_RT_UDRIMAGE_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_RT_UDRIMAGE_TDATETIME					_T("TDateTime")					// 时间（精确到秒）
#define	FLD_RT_UDRIMAGE_MILLITIME					_T("Millitime")					// 毫秒
#define	FLD_RT_UDRIMAGE_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_RT_UDRIMAGE_MDEPTH					_T("MDepth")					// SIML测量点井深
#define	FLD_RT_UDRIMAGE_VDEPTH					_T("VDepth")					// SIML测量点垂深
#define	FLD_RT_UDRIMAGE_URDATAID					_T("URDataID")					// 上电阻率数据对应表中的ID
#define	FLD_RT_UDRIMAGE_DRDDATAID					_T("DRDDataID")					// 下电阻率数据对应表中的ID
#define	FLD_RT_UDRIMAGE_LRDATAID					_T("LRDataID")					// 左电阻率数据对应表中的ID
#define	FLD_RT_UDRIMAGE_RRDDATAID					_T("RRDDataID")					// 右电阻率数据对应表中的ID
#define	FLD_RT_UDRIMAGE_UR					_T("UR")					// 上电阻率数据
#define	FLD_RT_UDRIMAGE_DR					_T("DR")					// 下电阻率数据
#define	FLD_RT_UDRIMAGE_LR					_T("LR")					// 左电阻率数据
#define	FLD_RT_UDRIMAGE_RR					_T("RR")					// 右电阻率数据
#define	FLD_RT_UDRIMAGE_CONF					_T("Conf")					// 可信度
#define	FLD_RT_UDRIMAGE_BAD					_T("Bad")					// 是否坏点:No|0.否;Yes|1.是
#define	FLD_RT_UDRIMAGE_DRILLACTIV					_T("DrillActiv")					// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	FLD_RT_UDRIMAGE_SLIDING					_T("Sliding")					// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	FLD_RT_UDRIMAGE_RELOG					_T("ReLog")					// 是否复测 :Yes|0.否;No|1.是
#define	FLD_RT_UDRIMAGE_LAS					_T("Las")					// 是否Las :No|0.否;Yes|1.是
#define	FLD_RT_UDRIMAGE_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_RT_UDRIMAGE_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_RT_UDRIMAGE_MEMO					_T("Memo")					// 备注

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_RT_UDRIMAGE_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_RT_UDRIMAGE_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_RT_UDRIMAGE
{
 
  long	lDataID;							// 数据编号
  char	szRunID[TV_RT_UDRIMAGE_RUNID_DIGITS + 1];							// 趟钻编号
  time_t	lTDateTime;							// 时间（精确到秒）
  int	iMillitime;							// 毫秒
  int	iToolID;							// 工具编号
  long	lMDepth;							// SIML测量点井深
  long	lVDepth;							// SIML测量点垂深
  long	lURDataID;							// 上电阻率数据对应表中的ID
  long	lDRDDataID;							// 下电阻率数据对应表中的ID
  long	lLRDataID;							// 左电阻率数据对应表中的ID
  long	lRRDDataID;							// 右电阻率数据对应表中的ID
  float	fUR;							// 上电阻率数据
  float	fDR;							// 下电阻率数据
  float	fLR;							// 左电阻率数据
  float	fRR;							// 右电阻率数据
  short	nConf;							// 可信度
  short	nBad;							// 是否坏点:No|0.否;Yes|1.是
  short	nDrillActiv;							// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
  short	nSliding;							// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
  short	nReLog;							// 是否复测 :Yes|0.否;No|1.是
  short	nLas;							// 是否Las :No|0.否;Yes|1.是
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_RT_UDRIMAGE_MEMO_DIGITS + 1];							// 备注
} TS_RT_UDRIMAGE;

typedef	TS_RT_UDRIMAGE FAR*	LPTS_RT_UDRIMAGE;

#pragma	pack(pop)

#endif // _TRT_UDRIMAGE_H

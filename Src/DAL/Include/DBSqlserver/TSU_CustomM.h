//---------------------------------------------------------------------------//
// 文件名: SU_CustomM.h
// 说明:	内存地质导线数据表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/10/13 15:30:23
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TSU_CustomM.h : DSU_CustomM

#ifndef	_TSU_CUSTOMM_H
#define	_TSU_CUSTOMM_H

#define	TID_SU_CUSTOMM								_T("SU_CustomM")
#define	OID_SU_CUSTOMM								_T("")

// Sort No
#define	SORT_SU_CUSTOMM_PK0				0							// PK:自增字段
//#define	SORT_SU_CUSTOMM_A1							%#%							// A1:

// Colum No
#define	COL_SU_CUSTOMM_DATAID					(short)0						// 自增字段
#define	COL_SU_CUSTOMM_RUNID					(short)1						// 趟钻编号
#define	COL_SU_CUSTOMM_TDATETIME					(short)2						// 时间（精确到秒）
#define	COL_SU_CUSTOMM_MILLITIME					(short)3						// 毫秒
#define	COL_SU_CUSTOMM_TOOLID					(short)4						// 工具编号
#define	COL_SU_CUSTOMM_MDEPTH					(short)5						// 测量深度 
#define	COL_SU_CUSTOMM_VDEPTH					(short)6						// 测量深度 
#define	COL_SU_CUSTOMM_SU_INC					(short)7						// 导向头井斜
#define	COL_SU_CUSTOMM_SU_GTF					(short)8						// 导向头重力工具面
#define	COL_SU_CUSTOMM_SU_RP1					(short)9						// 导向推靠压力1
#define	COL_SU_CUSTOMM_SU_RP2					(short)10						// 导向推靠压力2
#define	COL_SU_CUSTOMM_SU_RP3					(short)11						// 导向推靠压力3
#define	COL_SU_CUSTOMM_SU_RM1					(short)12						// 导向马达转速1
#define	COL_SU_CUSTOMM_SU_RM2					(short)13						// 导向马达转速2
#define	COL_SU_CUSTOMM_SU_RM3					(short)14						// 导向马达转速3
#define	COL_SU_CUSTOMM_SU_TD					(short)15						// 导向头温度
#define	COL_SU_CUSTOMM_DLCMD					(short)16						// 导向下传命令
#define	COL_SU_CUSTOMM_SU_MODE					(short)17						// 导向方式
#define	COL_SU_CUSTOMM_SU_SF					(short)18						// 导向力
#define	COL_SU_CUSTOMM_SU_DIR					(short)19						// 导向方向
#define	COL_SU_CUSTOMM_SU_TINC					(short)20						// 导向头目标井斜
#define	COL_SU_CUSTOMM_SU_BF					(short)21						// 导向造斜力
#define	COL_SU_CUSTOMM_SU_WF					(short)22						// 导向方向力
#define	COL_SU_CUSTOMM_CONF					(short)23						// 旋转状态:No|0.否;Yes|1.是
#define	COL_SU_CUSTOMM_BAD					(short)24						// 是否坏点:No|0.否;Yes|1.是
#define	COL_SU_CUSTOMM_DRILLACTIV					(short)25						// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	COL_SU_CUSTOMM_SLIDING					(short)26						// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	COL_SU_CUSTOMM_RELOG					(short)27						// 是否复测 :Yes|0.否;No|1.是
#define	COL_SU_CUSTOMM_LAS					(short)28						// 是否Las :Yes|0.否;No|1.是
#define	COL_SU_CUSTOMM_CREATETIME					(short)29						// 创建时间戳
#define	COL_SU_CUSTOMM_UPDTIME					(short)30						// 最后一次修改时间戳
#define	COL_SU_CUSTOMM_STATUS					(short)31						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_SU_CUSTOMM_MEMO					(short)32						// 备注

// Colum(Field) Name
#define	FLD_SU_CUSTOMM_DATAID					_T("DataID")					// 自增字段
#define	FLD_SU_CUSTOMM_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_SU_CUSTOMM_TDATETIME					_T("TDateTime")					// 时间（精确到秒）
#define	FLD_SU_CUSTOMM_MILLITIME					_T("Millitime")					// 毫秒
#define	FLD_SU_CUSTOMM_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_SU_CUSTOMM_MDEPTH					_T("MDepth")					// 测量深度 
#define	FLD_SU_CUSTOMM_VDEPTH					_T("VDepth")					// 测量深度 
#define	FLD_SU_CUSTOMM_SU_INC					_T("SU_INC")					// 导向头井斜
#define	FLD_SU_CUSTOMM_SU_GTF					_T("SU_GTF")					// 导向头重力工具面
#define	FLD_SU_CUSTOMM_SU_RP1					_T("SU_RP1")					// 导向推靠压力1
#define	FLD_SU_CUSTOMM_SU_RP2					_T("SU_RP2")					// 导向推靠压力2
#define	FLD_SU_CUSTOMM_SU_RP3					_T("SU_RP3")					// 导向推靠压力3
#define	FLD_SU_CUSTOMM_SU_RM1					_T("SU_RM1")					// 导向马达转速1
#define	FLD_SU_CUSTOMM_SU_RM2					_T("SU_RM2")					// 导向马达转速2
#define	FLD_SU_CUSTOMM_SU_RM3					_T("SU_RM3")					// 导向马达转速3
#define	FLD_SU_CUSTOMM_SU_TD					_T("SU_TD")					// 导向头温度
#define	FLD_SU_CUSTOMM_DLCMD					_T("DLCMD")					// 导向下传命令
#define	FLD_SU_CUSTOMM_SU_MODE					_T("SU_MODE")					// 导向方式
#define	FLD_SU_CUSTOMM_SU_SF					_T("SU_SF")					// 导向力
#define	FLD_SU_CUSTOMM_SU_DIR					_T("SU_DIR")					// 导向方向
#define	FLD_SU_CUSTOMM_SU_TINC					_T("SU_TINC")					// 导向头目标井斜
#define	FLD_SU_CUSTOMM_SU_BF					_T("SU_BF")					// 导向造斜力
#define	FLD_SU_CUSTOMM_SU_WF					_T("SU_WF")					// 导向方向力
#define	FLD_SU_CUSTOMM_CONF					_T("Conf")					// 旋转状态:No|0.否;Yes|1.是
#define	FLD_SU_CUSTOMM_BAD					_T("Bad")					// 是否坏点:No|0.否;Yes|1.是
#define	FLD_SU_CUSTOMM_DRILLACTIV					_T("DrillActiv")					// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	FLD_SU_CUSTOMM_SLIDING					_T("Sliding")					// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	FLD_SU_CUSTOMM_RELOG					_T("ReLog")					// 是否复测 :Yes|0.否;No|1.是
#define	FLD_SU_CUSTOMM_LAS					_T("Las")					// 是否Las :Yes|0.否;No|1.是
#define	FLD_SU_CUSTOMM_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_SU_CUSTOMM_UPDTIME					_T("UpdTime")					// 最后一次修改时间戳
#define	FLD_SU_CUSTOMM_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_SU_CUSTOMM_MEMO					_T("Memo")					// 备注

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_SU_CUSTOMM_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_SU_CUSTOMM_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_SU_CUSTOMM
{
 
  int	iDataID;							// 自增字段
  char	szRunID[TV_SU_CUSTOMM_RUNID_DIGITS + 1];							// 趟钻编号
  time_t	lTDateTime;							// 时间（精确到秒）
  int	iMillitime;							// 毫秒
  int	iToolID;							// 工具编号
  long	lMDepth;							// 测量深度 
  long	lVDepth;							// 测量深度 
  float	fSU_INC;							// 导向头井斜
  float	fSU_GTF;							// 导向头重力工具面
  float	fSU_RP1;							// 导向推靠压力1
  float	fSU_RP2;							// 导向推靠压力2
  float	fSU_RP3;							// 导向推靠压力3
  float	fSU_RM1;							// 导向马达转速1
  float	fSU_RM2;							// 导向马达转速2
  float	fSU_RM3;							// 导向马达转速3
  float	fSU_TD;							// 导向头温度
  float	fDLCMD;							// 导向下传命令
  float	fSU_MODE;							// 导向方式
  float	fSU_SF;							// 导向力
  float	fSU_DIR;							// 导向方向
  float	fSU_TINC;							// 导向头目标井斜
  float	fSU_BF;							// 导向造斜力
  float	fSU_WF;							// 导向方向力
  short	nConf;							// 旋转状态:No|0.否;Yes|1.是
  short	nBad;							// 是否坏点:No|0.否;Yes|1.是
  short	nDrillActiv;							// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
  short	nSliding;							// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
  short	nReLog;							// 是否复测 :Yes|0.否;No|1.是
  short	nLas;							// 是否Las :Yes|0.否;No|1.是
  time_t	lCreateTime;							// 创建时间戳
  time_t	lUpdTime;							// 最后一次修改时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_SU_CUSTOMM_MEMO_DIGITS + 1];							// 备注
} TS_SU_CUSTOMM;

typedef	TS_SU_CUSTOMM FAR*	LPTS_SU_CUSTOMM;

#pragma	pack(pop)

#endif // _TSU_CUSTOMM_H

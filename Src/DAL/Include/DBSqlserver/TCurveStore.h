//---------------------------------------------------------------------------//
// 文件名: CurveStore.h
// 说明:	曲线存储数据表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/9/15 16:33:49
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TCurveStore.h : DCurveStore

#ifndef	_TCURVESTORE_H
#define	_TCURVESTORE_H

#define	TID_CURVESTORE								_T("CurveStore")
#define	OID_CURVESTORE								_T("")

// Sort No
#define	SORT_CURVESTORE_PK0				0							// PK:表ID
//#define	SORT_CURVESTORE_A1							%#%							// A1:

// Colum No
#define	COL_CURVESTORE_CTID					(short)0						// 表ID
#define	COL_CURVESTORE_CTNAME					(short)1						// 表中文名称
#define	COL_CURVESTORE_CTALIAS					(short)2						// 表别名（显示在界面用）
#define	COL_CURVESTORE_DESCRIPTION_CN					(short)3						// 中文描述
#define	COL_CURVESTORE_DESCRIPTION_EN					(short)4						// 英文描述
#define	COL_CURVESTORE_CTTYPE					(short)5						// 存储曲线的类型:RealTime|0.实时数据表;Memory|1.内存数据表;
#define	COL_CURVESTORE_SORDER					(short)6						// 排序
#define	COL_CURVESTORE_CREATETIME					(short)7						// 创建时间戳
#define	COL_CURVESTORE_STATUS					(short)8						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_CURVESTORE_MEMO					(short)9						// 备注
#define	COL_CURVESTORE_UPDCOUNT					(short)10						// 更新计数

// Colum(Field) Name
#define	FLD_CURVESTORE_CTID					_T("CTID")					// 表ID
#define	FLD_CURVESTORE_CTNAME					_T("CTName")					// 表中文名称
#define	FLD_CURVESTORE_CTALIAS					_T("CTAlias")					// 表别名（显示在界面用）
#define	FLD_CURVESTORE_DESCRIPTION_CN					_T("Description_cn")					// 中文描述
#define	FLD_CURVESTORE_DESCRIPTION_EN					_T("Description_en")					// 英文描述
#define	FLD_CURVESTORE_CTTYPE					_T("CTType")					// 存储曲线的类型:RealTime|0.实时数据表;Memory|1.内存数据表;
#define	FLD_CURVESTORE_SORDER					_T("SOrder")					// 排序
#define	FLD_CURVESTORE_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_CURVESTORE_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_CURVESTORE_MEMO					_T("Memo")					// 备注
#define	FLD_CURVESTORE_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_CURVESTORE_CTID_DIGITS				50					// 表ID位数
#define	TV_CURVESTORE_CTNAME_DIGITS				50					// 表中文名称位数
#define	TV_CURVESTORE_CTALIAS_DIGITS				50					// 表别名（显示在界面用）位数
#define	TV_CURVESTORE_DESCRIPTION_CN_DIGITS				50					// 中文描述位数
#define	TV_CURVESTORE_DESCRIPTION_EN_DIGITS				50					// 英文描述位数
#define	TV_CURVESTORE_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_CURVESTORE
{
 
  char	szCTID[TV_CURVESTORE_CTID_DIGITS + 1];							// 表ID
  char	szCTName[TV_CURVESTORE_CTNAME_DIGITS + 1];							// 表中文名称
  char	szCTAlias[TV_CURVESTORE_CTALIAS_DIGITS + 1];							// 表别名（显示在界面用）
  char	szDescription_cn[TV_CURVESTORE_DESCRIPTION_CN_DIGITS + 1];							// 中文描述
  char	szDescription_en[TV_CURVESTORE_DESCRIPTION_EN_DIGITS + 1];							// 英文描述
  int	iCTType;							// 存储曲线的类型:RealTime|0.实时数据表;Memory|1.内存数据表;
  int	iSOrder;							// 排序
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_CURVESTORE_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_CURVESTORE;

typedef	TS_CURVESTORE FAR*	LPTS_CURVESTORE;

#pragma	pack(pop)

#endif // _TCURVESTORE_H

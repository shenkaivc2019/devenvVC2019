//---------------------------------------------------------------------------//
// 文件名: RunInfoTB.h
// 说明:	趟钻数据表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020/10/19 22:11:35
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TRunInfoTB.h : DRunInfoTB

#ifndef	_TRUNINFOTB_H
#define	_TRUNINFOTB_H

#define	TID_RUNINFOTB								_T("RunInfoTB")
#define	OID_RUNINFOTB								_T("")

// Sort No
#define	SORT_RUNINFOTB_PK0				0							// PK:自增字段
//#define	SORT_RUNINFOTB_A1							%#%							// A1:

// Colum No
#define	COL_RUNINFOTB_RUNID					(short)0						// 自增字段
#define	COL_RUNINFOTB_HOLEID					(short)1						// 井眼编号
#define	COL_RUNINFOTB_RUNNAME					(short)2						// 趟钻名称
#define	COL_RUNINFOTB_RUNDESCRIPTION					(short)3						// 描述
#define	COL_RUNINFOTB_STARTTIME					(short)4						// 起始时间
#define	COL_RUNINFOTB_ENDTIME					(short)5						// 结束时间
#define	COL_RUNINFOTB_RUNHOURS					(short)6						// 持续时间
#define	COL_RUNINFOTB_STARTDEPTH					(short)7						// 起始井深精确到0.1mm
#define	COL_RUNINFOTB_ENDDEPTH					(short)8						// 结束井深精确到0.1mm
#define	COL_RUNINFOTB_TOOLSIZE					(short)9						// 工具尺寸（和井眼尺寸是两个概念）
#define	COL_RUNINFOTB_BHAID					(short)10						// 钻具组合编号
#define	COL_RUNINFOTB_MEASDIR					(short)11						// 测量方向:Down|0.向下测量;Up|1.向上测量;
#define	COL_RUNINFOTB_CREATETIME					(short)12						// 创建时间戳
#define	COL_RUNINFOTB_STATUS					(short)13						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结;Active|3.激活状态;
#define	COL_RUNINFOTB_MEMO					(short)14						// 备注
#define	COL_RUNINFOTB_UPDCOUNT					(short)15						// 更新计数

// Colum(Field) Name
#define	FLD_RUNINFOTB_RUNID					_T("RunID")					// 自增字段
#define	FLD_RUNINFOTB_HOLEID					_T("HoleID")					// 井眼编号
#define	FLD_RUNINFOTB_RUNNAME					_T("RunName")					// 趟钻名称
#define	FLD_RUNINFOTB_RUNDESCRIPTION					_T("RunDescription")					// 描述
#define	FLD_RUNINFOTB_STARTTIME					_T("StartTime")					// 起始时间
#define	FLD_RUNINFOTB_ENDTIME					_T("EndTime")					// 结束时间
#define	FLD_RUNINFOTB_RUNHOURS					_T("RunHours")					// 持续时间
#define	FLD_RUNINFOTB_STARTDEPTH					_T("StartDepth")					// 起始井深精确到0.1mm
#define	FLD_RUNINFOTB_ENDDEPTH					_T("EndDepth")					// 结束井深精确到0.1mm
#define	FLD_RUNINFOTB_TOOLSIZE					_T("ToolSize")					// 工具尺寸（和井眼尺寸是两个概念）
#define	FLD_RUNINFOTB_BHAID					_T("BHAID")					// 钻具组合编号
#define	FLD_RUNINFOTB_MEASDIR					_T("MeasDir")					// 测量方向:Down|0.向下测量;Up|1.向上测量;
#define	FLD_RUNINFOTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_RUNINFOTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结;Active|3.激活状态;
#define	FLD_RUNINFOTB_MEMO					_T("Memo")					// 备注
#define	FLD_RUNINFOTB_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_RUNINFOTB_RUNID_DIGITS				50					// 自增字段位数
#define	TV_RUNINFOTB_HOLEID_DIGITS				50					// 井眼编号位数
#define	TV_RUNINFOTB_RUNNAME_DIGITS				50					// 趟钻名称位数
#define	TV_RUNINFOTB_RUNDESCRIPTION_DIGITS				500					// 描述位数
#define	TV_RUNINFOTB_BHAID_DIGITS				50					// 钻具组合编号位数
#define	TV_RUNINFOTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_RUNINFOTB
{
 
  char	szRunID[TV_RUNINFOTB_RUNID_DIGITS + 1];							// 自增字段
  char	szHoleID[TV_RUNINFOTB_HOLEID_DIGITS + 1];							// 井眼编号
  char	szRunName[TV_RUNINFOTB_RUNNAME_DIGITS + 1];							// 趟钻名称
  char	szRunDescription[TV_RUNINFOTB_RUNDESCRIPTION_DIGITS + 1];							// 描述
  time_t	lStartTime;							// 起始时间
  time_t	lEndTime;							// 结束时间
  float	fRunHours;							// 持续时间
  long	lStartDepth;							// 起始井深精确到0.1mm
  long	lEndDepth;							// 结束井深精确到0.1mm
  float	fToolSize;							// 工具尺寸（和井眼尺寸是两个概念）
  char	szBHAID[TV_RUNINFOTB_BHAID_DIGITS + 1];							// 钻具组合编号
  short	nMeasDir;							// 测量方向:Down|0.向下测量;Up|1.向上测量;
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结;Active|3.激活状态;
  char	szMemo[TV_RUNINFOTB_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_RUNINFOTB;

typedef	TS_RUNINFOTB FAR*	LPTS_RUNINFOTB;

#pragma	pack(pop)

#endif // _TRUNINFOTB_H

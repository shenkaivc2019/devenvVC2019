// OrderInfo.h: interface for the COrderInfo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ORDERINFO_H__D90A590B_7EDB_46D2_963A_DC7E24CEED8A__INCLUDED_)
#define AFX_ORDERINFO_H__D90A590B_7EDB_46D2_963A_DC7E24CEED8A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <vector>
#include "TToolAizRes.h"
#include "TPressureCalibration.h"
#include "TReLog.h"
#include "TCurveInfoTB.h"
#include  "TUnitTB.h"

#define	CI_CNSIZE		64

#define DB_CURVE_MODE_RT	1
#define DB_CURVE_MODE_TOOL	2

#define DB_DATA_MASK_VALUE		0x00000001
#define DB_DATA_MASK_ONBOTTOM	0x00000002
#define DB_DATA_MASK_BAD		0x00000004

#define DB_INDEX_MASK_DEPTH		0x01000000
#define DB_INDEX_MASK_TIME		0x02000000
#define	DB_INDEX_MASK_NAME		0x04000000
#ifdef _DEBUG
	#define  DATABASEDLL		_T("DBSqlserverd.dll")
#else
	#define  DATABASEDLL		_T("DBSqlserver.dll")
#endif // _DEBUG
/*工程信息*/
typedef struct tagPROJECTINFO
{
	char ProParam[CI_CNSIZE];
	char ProValue[256];    //容量是否合适
}DBProjectInfo;
typedef std::vector<DBProjectInfo> DB_ProjectInfo;

/*Run信息*/
typedef struct tagRUNINFO
{
	char		Desc[CI_CNSIZE];
	SYSTEMTIME	StartTime;
	SYSTEMTIME	EndTime;
	float		ElapsedHours;
	float		StartDepth;
	float		EndDepth;
	float		HoleSize;
	float		GammaSerial;
	float		GammaOffset;
	float		ResistivitySerial;
	float		ResistivityOffset;
	float		SurveyOffset;
	float		ToolfaceToGamma;
	float		Declination;
	float		DepthTieIn;
	float		InclinationTieIn;
	float		AzimuthTieIn;
	float		NorthTieIn;
	float		EastTieIn;
	float		TVDTieIn;
	float		VertSectAzmTieIn;
	float		VertSectOriginNorth;
	float		VertSectOriginEast;
}DBRunInfo;
typedef std::vector<DBRunInfo> DB_RunInfo;

/*泥浆信息*/
typedef struct tagMUDINFO
{
	SYSTEMTIME	time;		//时间
	int			Type;		//类型
	float		Weight;	
	float		Rm;
	float		Km;
	float		MudPitTemp;
// 2019.07.29 add Temp.C Start
//	float		AdjustedRm;
	float		TempC;
// 2019.07.29 add Temp.C End
}DBMudInfo;
typedef std::vector<DBMudInfo> DB_MudInfo;

/*钻井信息*/
typedef struct tagDRILLINFO
{
	SYSTEMTIME	time;		//时间
	int			Type;		//钻井方式
}DBDrillInfo;
typedef std::vector<DBDrillInfo> DB_DrillInfo;

// 2020.03.03 Ver1.60 TASK【002】Start
//typedef struct tagSURVEY_DATA
//{
//	long lDepth;
//	float fInc;
//	float fAz;
//	long lTVD;
//	float fNS;
//	float fEW;
//	float fLeg;
//	bool bTieIn;
//	bool bChoose;
//}DBSurveyData;
// 2020.03.03 Ver1.60 TASK【002】End

        
/*序列参数信息*/
typedef struct tagORDERPARAM
{
	char CurveName[CI_CNSIZE];//曲线名
	int  Num; //所占波形数
}OrderParamInfo;
typedef std::vector<OrderParamInfo> DB_OrderParam;


/*序列*/
typedef struct tagORDERINFO  
{
	char OrderName[CI_CNSIZE];
	DB_OrderParam OrderParam;
}DBOrderInfo;

typedef	std::vector<DBOrderInfo> DB_Orders;

/*时间深度信息*/
typedef struct tagDEPTHDATA
{
	SYSTEMTIME	time;   //时间
	__int64 depth;		//深度	
	float SPP;			//力压
	int OnBottom;
	//float dgzh;			//大钩载荷
	//float jcjs;			//角测计数
}DBDepthData;

/*曲线信息*/
typedef struct tagDBCURVEINFO
{
	char	CurveName[CI_CNSIZE];	//曲线名
	char	CurveUnit[CI_CNSIZE];	//曲线单位
	float	DepthOffset;			//深度偏移
	WORD	CurveType;             //曲线数据类型
	int		Dimension;				//维数
	int		PointCount;			//点数
	
}DBCurveInfo;
//#define DB_CurveInfos			std::vector<DBCurveInfo*>   用typedef好一些
typedef std::vector<DBCurveInfo> DB_CurveInfos;

typedef struct tagDBCURVEVALUE
{
	char	CurveName[CI_CNSIZE];   //曲线名称
	float*	CurveValue;				//曲线值指针
	int		nData;					//数据个数	
	
}DBCurveValue;

typedef std::vector<DBCurveValue*> DB_CurveValues;

//一条仪器数据记录
typedef struct tagDBTOOLDATA
{
	__int64				depth;
	int					OnBottom;
	SYSTEMTIME			time;
	int					num;
	CArray<float,float> fArrValues;
	tagDBTOOLDATA()
	{
	}
	tagDBTOOLDATA(const tagDBTOOLDATA& td)
	{
		fArrValues.Copy(td.fArrValues);
	}
	tagDBTOOLDATA& operator=(const tagDBTOOLDATA& td)
	{
		if (&td == this)
		{
			return *this;
		}
		depth = td.depth;
		OnBottom = td.OnBottom;
		time = td.time;
		num = td.num;
		fArrValues.RemoveAll();
		fArrValues.Copy(td.fArrValues);
	}
}DBToolData;
typedef std::vector<DBToolData*> DB_ToolDatas;

/*实时数据*/
typedef struct tagRTDATAINFO 
{
	SYSTEMTIME	Time;
	__int64		Depth;
//2021.06.09 sys Start
	__int64		lTvd;
	int		nLas;
//2021.06.09 sys End	
	int         OnBottom;
	int			Bad;
	char		RunNo[51];
	char		CurveName[CI_CNSIZE];
	float		CurveValue;
	char		OrderName[CI_CNSIZE];
//2021.10.14 Start
	short		nStatus;
//2021.10.14 End
	
}RTDataInfo;
typedef std::vector<RTDataInfo> DB_RTDatas;

/*输出仪器曲线数据结构*/
typedef struct tagOUTMATCHDATA
{
	tagOUTMATCHDATA()
	{

	}
	tagOUTMATCHDATA(const tagOUTMATCHDATA& other)
	{
		memcpy(ToolName,other.ToolName,sizeof(ToolName));
		arrCurves.Copy(other.arrCurves);
	}
	tagOUTMATCHDATA & operator =(const tagOUTMATCHDATA &other)
	{
		memcpy(ToolName,other.ToolName,sizeof(ToolName));
		arrCurves.Copy(other.arrCurves);
		return *this;
	}
	char			ToolName[CI_CNSIZE];	//仪器名
	CStringArray	arrCurves;
	//CURVEPROPS		vecCurves;
}DBOutMatchData;
typedef std::vector<DBOutMatchData> DB_OutMatchDatas;

//导出仪器数据所需的一些参数
typedef struct tagOUTDATAPARAM
{
	long	lDepthSpace;	//深度间隔
	BOOL	bTitle;			//是否输出标题行（针对txt文件）
	TCHAR	cMark[16];		//间隔标记（针对txt文件）
	int		nDataType;		//数据类型：0-成像数据；1-非成像数据（针对txt文件）

}OutDataParam;

typedef struct _tagCTF
{
	float fToolSize;
	double fApparentResistivity;
	double fMudResistivity;
	double fHoleSize;
	double fCoefficient;
	int iCalculated;
}CTF;


typedef struct tagCorParam 
{
	long lDepth0;
	long lDepth1;
	double dHD;
	double dRM;
	double dDIE;
	double dPXJ;
	BOOL bHole;
	BOOL bDie;
	BOOL bEcc;
}CorParam;
typedef std::vector<CorParam*> CorParams;


// 2020.01.19 Ver1.60 TASK【007】Start

//------------------------------------//
//结构体名称: 解析PRD0017.raw文件的一条记录
//说明:  
//备注:
//------------------------------------// 
typedef struct tagMEResistivityTB
{
	char   acTime[128];		//时间
	long   nTime;
	int   iMillitime;
	double dCCP1;			//相差 
	double dCCP2;
	double dCCP3;
	double dCCP4;
	double dCCA1;			//幅差
	double dCCA2;
	double dCCA3;
	double dCCA4;

	double dRe1p;			//相差电阻率 
	double dRe2p;
	double dRe3p;
	double dRe4p;
	double dRe1a;			//幅差电阻率
	double dRe2a;
	double dRe3a;
	double dRe4a;
}MEResistivityTB;
typedef	tagMEResistivityTB FAR*		LPMEResistivityTB;

//------------------------------------//
//结构体名称: MEResAirHangTB表对应的字段记录
//说明:  
//备注:
//------------------------------------// 
typedef struct tagMEResAirHangTB
{
	char szRunID[50];
	int nToolID;
	int nBHAID;
	char acToolSN[64];
	char acDatetime[64];
	int nMilitime;
	double dAH_AFL;
	double dAH_ANL;
	double dAH_AFH;
	double dAH_ANH;
	double dAH_PFL;
	double dAH_PNL;
	double dAH_PFH;
	double dAH_PNH;
	double  dSTDV_AFL;
	double 	dSTDV_ANL;
	double 	dSTDV_AFH;
	double 	dSTDV_ANH;
	double 	dSTDV_PFL;
	double 	dSTDV_PNL;
	double 	dSTDV_PFH;
	double 	dSTDV_PNH;
}MEResAirHangTB;
typedef	tagMEResAirHangTB FAR* LPMEResAirHangTB;
//------------------------------------//
//结构体名称: tagToolInfoTB表对应的字段记录
//说明:  
//备注:
//------------------------------------// 
typedef struct tagToolInfoTB
{
	int nToolID;
	char acToolSN[128];
}ToolInfoTB;
typedef	tagToolInfoTB FAR* LPToolInfoTB;

// 2020.01.19 Ver1.60 TASK【007】End

// 2020.1.14 Ver1.6.0 井场信息定义 Start
//---------------------------------------------------------------------------//
// 结构体名称： 趟钻信息
// 说明：   用来描述趟钻相关信息
// 备注：   无。
//---------------------------------------------------------------------------//
//typedef struct tagRUN_INFO
//{
//	int		nRunID;
//	int		nHoleID;
//	int		nBHAID;
//	int		nStatus;
//	float	fRunHours;
//	float	fToolSize;
//	CString strRunName;
//	CString	strRunDescription;
//	CString strMemo;
//	INT64	nStartDepth;			
//	INT64	nEndDepth;
//	COleDateTime tStartTime;
//	COleDateTime tEndTime;
//	COleDateTime tCreateTime;
//	int	iGSID;
//	void Init()
//	{
//		nRunID = -1;
//		strRunName = _T("");
//		nHoleID = -1;
//		nBHAID = -1;
//		tCreateTime = COleDateTime::GetCurrentTime();
//		nStatus = 1;
//		fRunHours = 0.0f;
//		fToolSize = 0.0f;
//		tStartTime = COleDateTime::GetCurrentTime();
//		tEndTime = COleDateTime::GetCurrentTime();
//		nStartDepth = 0;			
//		nEndDepth = 0;
//		iGSID = -1;
//	}
//}RUN_INFO;
//
////---------------------------------------------------------------------------//
//// 结构体名称： 井眼信息
//// 说明：   用来描述井眼相关信息
//// 备注：   无。
////---------------------------------------------------------------------------//
//typedef struct tagHOLE_INFO
//{
//	int		nHoleID;
//	int		nWellID;
//	int		nParentHoleID;
//	CString	strWellHoleName;
//	CString	strOilCompany;
//	CString	strSvrCompany;
//	CString	strDrillCompany;
//	CString	strRig;
//	CString	strJogNumber;
//	CString	strAPISN;
//	CString	strOilCompanyRep1;
//	CString	strOilCompanyRep2;
//	CString	strSvrCompanyRep1;
//	CString	strSvrCompanyRep2;
//	CString	strGeologist1;
//	CString	strGeologist2;
//	CString	strDirectionalDriller1;
//	CString	strDirectionalDriller2;
//	CString	strMWDEngneer1;
//	CString	strMWDEngneer2;
//	CString	strMWDKit;
//	CString strMemo;
//	int		nDepthRef;
//	int		nGroundLevel;
//	int		nStatus;
//	float	fDrilFlrGrd;
//	float	fDrilFlrSea;
//	float	fKellyBushDrilFlr;
//	float	fKellyBushGrd;
//	float	fKellyBushsea;
//	float	fHoleSize;
//	COleDateTime tCreateTime;
//	void Init()
//	{
//		nHoleID = -1;
//		nWellID = -1;
//		nParentHoleID = -1;
//		nDepthRef = 0;
//		nGroundLevel = 0;
//		tCreateTime = COleDateTime::GetCurrentTime();
//		nStatus = 1;
//		fDrilFlrGrd = 0.0f;
//		fDrilFlrSea = 0.0f;
//		fKellyBushDrilFlr = 0.0f;
//		fKellyBushGrd = 0.0f;
//		fKellyBushsea = 0.0f;
//		fHoleSize = 0.0f;
//	}
//}HOLE_INFO;
//
////---------------------------------------------------------------------------//
//// 结构体名称： 井信息
//// 说明：   用来描述井相关信息
//// 备注：   无。
////---------------------------------------------------------------------------//
//typedef struct tagWELL_INFO
//{
//	int		nWellID;
//	int		nWellSiteID;
//	int		nStatus;
//	CString	strWellName;
//	float	fLatitude;
//	//2020.7.11 sys [BUG84] Start
//	float   fLatitudeDegree;
//	float   fLatitudeBranch;
//	float   fLatitudeSecond;
//	float   fLongitudeDegree;
//	float   fLongitudeBranch;
//	float   fLongitudeSecond;
//	//2020.7.11 sys [BUG84] End
//	float	fLongitude;
//	float	fLocationNorthing;
//	float	fLocationEasting;
//	int nDepthRef;
//	float	fDrilFlrGrd;
//	float	fDrilFlrSea;
//	float	fKellyBushDrilFlr;
//	float	fKellyBushGrd;
//	float	fKellyBushsea;
//	int	nGroundLevel;
//	CString	strMemo;
//	COleDateTime tCreateTime;
//	void Init()
//	{
//		nWellID = -1;
//		nWellSiteID = -1;
//		nStatus = 1;
//		tCreateTime = COleDateTime::GetCurrentTime();
//		fLatitude = 0.0f;
//		//2020.7.11 sys [BUG84] Start
//		fLatitudeDegree = 0.0f;
//		fLatitudeBranch = 0.0f;
//		fLatitudeSecond = 0.0f;
//		fLongitudeDegree = 0.0f;
//		fLongitudeBranch = 0.0f;
//		fLongitudeSecond = 0.0f;
//		//2020.7.11 sys [BUG84] End
//		fLongitude = 0.0f;
//		fLocationNorthing = 0.0f;
//		fLocationEasting = 0.0f;
//	}
//}WELL_INFO;

//---------------------------------------------------------------------------//
// 结构体名称： 井场信息
// 说明：   用来描述井场相关信息
// 备注：   无。
//---------------------------------------------------------------------------//
//typedef struct tagWELLSITE_INFO
//{
//	int		nWellSiteID;
//	int		nStatus;
//	CString strProID;
//	CString	strWellSiteName;
//	CString	strCountry;
//	CString strProvince;
//	CString strField;
//	CString	strRegion;
//	CString	strCounty;
//	CString	strWellLocation;
//	CString	strDrillCompany;
//	CString	strRig;
//	CString	strMemo;
//	COleDateTime tCreateTime;
//	void Init()
//	{
//		nWellSiteID = -1;
//		tCreateTime = COleDateTime::GetCurrentTime();
//		nStatus = 1;
//	}
//}WELLSITE_INFO;
//
////---------------------------------------------------------------------------//
//// 结构体名称： 工程信息
//// 说明：   用来描述工程相关信息
//// 备注：   无。
////---------------------------------------------------------------------------//
//typedef struct tagPROJECT_INFO
//{
//	int		nStatus;
//	CString strProID;
//	CString	strProName;
//	CString	strProClass;
//	CString strDepart;
//	CString	strMemo;
//	COleDateTime tCreateTime;
//	void Init()
//	{
//		tCreateTime = COleDateTime::GetCurrentTime();
//		nStatus = 1;
//	}
//}PROJECT_INFO;

//---------------------------------------------------------------------------//
// 结构体名称： BHA信息
// 说明：   用来描述BHA相关信息
// 备注：   无。
//---------------------------------------------------------------------------//
//typedef struct tagBHA_INFO
//{
//	int nBHAID;
//	int nStatus;
//	CString strBHAName;
//	CString strBHADescription;
//	CString strDrawTemp;
//	CString strChartFile;
//	CString strFeedGraph;
//	CString strMemo;
//	float fLenSum;
//	float fWeiSum;
//	COleDateTime tCreateTime;
//}BHA_INFO;
//
////---------------------------------------------------------------------------//
//// 结构体名称： BHA信息
//// 说明：   用来描述BHA相关信息
//// 备注：   无。
////---------------------------------------------------------------------------//
//typedef struct tagBHA_TOOL_INFO
//{
//	int nBHAID;
//	int nToolID;
//	int nToolNo;
//	int nStatus;
//	CString strToolSN;
//	CString strMemo;
//	float fToolBotToBit;
//	float fCAPI; // 传感器校准系数
//	COleDateTime oleCALTime; // 标定时间
//	COleDateTime tCreateTime;
//}BHA_TOOL_INFO;

//---------------------------------------------------------------------------//
// 结构体名称： Tool信息
// 说明：   用来描述Tool相关信息
// 备注：   无。
//---------------------------------------------------------------------------//
typedef struct _tagDB_TOOL_INFO
{
	int nToolID;
	int nStatus;
	CString strTCID;
	CString strToolName;
	CString strToolAlias;
	CString strToolPic;
	CString strToolDescription;
	CString strToolPN;
	CString strInDLL;
	CString strMemo;
	float fToolLength;
	float fToolWeight;
	float fToolIntDia;
	float fToolExtDia;
	float fToolODMax;
	float fToolODMin;
	float fToolPreMax;
	float fToolTempMax;
	COleDateTime tCreateTime;
}DB_TOOL_INFO;

//---------------------------------------------------------------------------//
// 结构体名称： 仪器类型信息
// 说明：   用来描述仪器类型相关信息
// 备注：   无。
//---------------------------------------------------------------------------//
typedef struct _tagDB_TOOL_TYPE_INFO
{
	int nTCOrder;
	int nGrade;
	int nStatus;
	CString strTCID;
	CString strFTCID;
	CString strTCNameCN;
	CString strTCNameEN;
	CString strImgPath;
	CString strMemo;
	COleDateTime tCreateTime;
}DB_TOOL_TYPE_INFO;
//---------------------------------------------------------------------------//
// 结构体名称： 泥浆信息
// 说明：   用来描述泥浆相关信息
// 备注：   无。
//---------------------------------------------------------------------------//
typedef struct _tagMUD_INFO
{
	int nMudID;
	int nHoleID;
	int nMudType;
	int nStatus;
	float fDensity;
	float fMudResistivity;
	float fPitTemperature;
	float fPercentageOfKm;
	float fPercentageOfK2CO3;
	float fDownHoleTemperature;
	CString strMudName;
	CString strMemo;
	COleDateTime tCreateTime;
	INT64 nSDateTime;
	INT64 nEDateTime;
}MUD_INFO;

//---------------------------------------------------------------------------//
// 结构体名称： Survey信息
// 说明：   用来描述Survey相关信息
// 备注：   无。
//---------------------------------------------------------------------------//
typedef struct _tagSURVEY_INFO
{
	int nSurveyID;
	CString strHoleID;
	CString strRunID;
	int nTieIn;
	int nChoose;
	int nStatus;
	int	nTFType;			//工具面类型，磁性或重力
	float fEast;
	float fNorth;
	float fVertSection;		//视平移
	float fDoglegSeverity;
	float fIncChngRate;
	float fAziChngRate;
	float fInclination;	//井斜
	float fAzimuth;		//网格方位
	float fTotalOffset;		//闭合范围
	float fTotalAzim;		//闭合位移
	float fVSAzim;			//设计方位
	float fSegLength;		//段长
	float fTF;				//工具面
	CString strMemo;
	COleDateTime tCreateTime;
	INT64 nSurveyDepth;//测深
	INT64 nTVDDepth;
	void Init()
	{
		nSurveyID = -1;
		strHoleID = _T("");
		strRunID =_T("");
		nTieIn = 0;
		nChoose = 1;
		nStatus = 0;
		fEast = 0.0f;
		fNorth = 0.0f;
		fVertSection = 0.0f;
		fTotalOffset = 0.0f;
		fTotalAzim = 0.0f;
		fDoglegSeverity = 0.0f;
		fIncChngRate = 0.0f;
		fAziChngRate = 0.0f;
		fInclination = 0.0f;
		fAzimuth = 0.0f;
		fTotalOffset = 0.0f;
		fTotalAzim = 0.0f;
		fVSAzim = 0.0f;
		fSegLength = 0.0f;
		fTF = 0.0f;
		nTFType = -1;
		strMemo = "";
		tCreateTime = COleDateTime::GetCurrentTime();
		nSurveyDepth = 0;
		nTVDDepth = 0;
	}

	void Assign(struct _tagSURVEY_INFO *pInfo)
	{
		nSurveyID = pInfo->nSurveyID;
		strHoleID = pInfo->strHoleID;
		strRunID = pInfo->strRunID;
		nTieIn = pInfo->nTieIn;
		nChoose = pInfo->nChoose;
		nStatus = pInfo->nStatus;
		fEast = pInfo->fEast;
		fNorth = pInfo->fNorth;
		fVertSection = pInfo->fVertSection;
		fTotalOffset = pInfo->fTotalOffset;
		fTotalAzim = pInfo->fTotalAzim;
		fDoglegSeverity = pInfo->fDoglegSeverity;
		fIncChngRate = pInfo->fIncChngRate;
		fAziChngRate = pInfo->fAziChngRate;
		fInclination = pInfo->fInclination;
		fAzimuth = pInfo->fAzimuth;
		fTotalOffset = pInfo->fTotalOffset;
		fTotalAzim = pInfo->fTotalAzim;
		fVSAzim = pInfo->fVSAzim;
		fSegLength = pInfo->fSegLength;
		fTF = pInfo->fTF;
		nTFType = pInfo->nTFType;
		strMemo = pInfo->strMemo;
		tCreateTime = pInfo->tCreateTime;
		nSurveyDepth = pInfo->nSurveyDepth;
		nTVDDepth = pInfo->nTVDDepth;	
	}
}SURVEY_INFO;
typedef std::vector<SURVEY_INFO> DB_SurveyData;
//---------------------------------------------------------------------------//
// 结构体名称： 靶点信息
// 说明：   用来描述靶点相关信息
// 备注：   无。
//---------------------------------------------------------------------------//
typedef struct _tagTARGET_INFO
{
	int nTargID;
	char szHoleID[50];
	char szRunID[50];
	int nTargetShape;
	int nStatus;
	float fTargetTVD;
	float fTargetCooridinateX;
	float fTargetCooridinateY;
	float fTargetWidth;	
	float fTargetHeight;
	float fTargeShapeRotate;
	float fTargeShapeDIP;
	float fAzimuth;
	CString strMemo;
	COleDateTime tCreateTime;
	INT64 nTargetNo;
}TARGET_INFO;
// 2020.1.14 Ver1.6.0 井场信息定义 End

//2020.2.18 Ver1.6.0 BUG【24】 Start
typedef struct _tagCURVE_INFO
{
	char cName[MAX_PATH];
	char cUnit[MAX_PATH];
	double fLeftValue;
	double fRightValue;
	long   lDepthOffset;
	
}CURVEINFOTB;
//2020.2.18 Ver1.6.0 BUG【24】 End
//2020.7.8 sys Start
typedef struct _tagCURVE_TOOL_INFO
{
	char cName[MAX_PATH];
	char cToolExtDia[MAX_PATH];
	char cToolIntDia[MAX_PATH];

}CURVETOOLINFOTB;
typedef struct _tagBHAINFODTB
{
	char cName[MAX_PATH];
	char cCAPI[MAX_PATH];
}BHAINFODTB;
typedef struct _tagPOSITION
{
	char cName[MAX_PATH];
	char cPosition[MAX_PATH];
}POSITIONS;
//2020.7.8 sys End
// 2020.05.19 增加新的实时数据曲线值结构体定义 Start
typedef struct tagMS_CURVEVALUE
{
	SYSTEMTIME		gTime;
	__int64			lDepth;
	__int64			lTVD;
	short			nOnBottom;
	short			nBad;

	short			nConf;
	short			nRelog;
//2021.6.25 sys Start
	short           nLas;
//2021.6.25 sys End
	char			szRunNo[50];
	int	iDimension;							// 维数(常规曲线1，阵列曲线2)
	int	iPointCount;							// 点数(常规曲线1)
	float**		ppfCurveValue;

	tagMS_CURVEVALUE& operator=(const tagMS_CURVEVALUE& value)
	{
		memcpy(&gTime, &value.gTime, sizeof(SYSTEMTIME));
		lDepth = value.lDepth;
		lTVD = value.lTVD;
		nOnBottom = value.nOnBottom;
		nBad = value.nBad;
		nConf = value.nConf;
		nRelog = value.nRelog;
//2021.6.25 sys Start
		nLas = value.nLas;
//2021.6.25 sys End
		_tcscpy_s(szRunNo, sizeof(szRunNo), value.szRunNo);
		iDimension = value.iDimension;
		iPointCount = value.iPointCount;
		ppfCurveValue = NULL;
		if (iDimension > 0)
		{
			//创建行指针。创建iDimension个元素的数组，每个元素指向一个float*型
			ppfCurveValue = new float*[iDimension];
			for (int i = 0; i < iDimension; i++)
			{
				if (iPointCount > 0)
				{
					// 开辟空间
					ppfCurveValue[i] = new float[iPointCount];
					memcpy(ppfCurveValue[i], value.ppfCurveValue[i], sizeof(float) * iPointCount);
				}
			}
		}
		return *this;
	}
	tagMS_CURVEVALUE()
	{
		iDimension = 0;
		iPointCount = 0;
		ppfCurveValue = NULL;
	}
	tagMS_CURVEVALUE(const tagMS_CURVEVALUE& s)
	{
		*this = s;
	}
	~tagMS_CURVEVALUE(){
		if (ppfCurveValue && iDimension > 0)
		{
			for (int i = 0;i < iDimension;i++) {
				delete ppfCurveValue[i];
			}
			delete[] ppfCurveValue;
			ppfCurveValue = NULL;
		}
	}
} MS_CURVEVALUE;

typedef	MS_CURVEVALUE FAR*	LPMS_CURVEVALUE;
// 2020.05.19 增加新的实时数据曲线值结构体定义 End

#include <TBHAInfoDTB.h>
#include <TRunInfoTB.h>
#include <TMeasPointInfoTB.h>
#include <TToolInfoTB.h>
typedef struct tagMS_GAMACORR_PARA
{
	char	szRunID[TV_RUNINFOTB_RUNID_DIGITS + 1];			// 趟钻编号
	int	iToolID;												// 工具编号
	int	iMPID;													// 测量点编号
	char	szMPName[TV_MEASPOINTINFOTB_MPNAME_DIGITS + 1];		// 测量点名称
	short	nPosition;											// 测点位置:Default|0.缺省无意义;Center|1.居中; Sidewall|2.侧壁
	float	fCAPI;												// 传感器校准系数
	time_t	lCALTime;											// 校准时间
	double	dToolIntDia;										// 内径 目前的单位是经过转换过的英寸
	double	dToolExtDia;										// 外径 目前的单位是经过转换过的英寸
	double	dToolBotToBit;										// 深度偏移(距本钻具组合底部距离),计算得出=BHAInfoMTB.BotToBit+ToolInfoTB.ToolLength(各工具的长度相加到该工具的序号BHAInfoDTB.ToolNo）
	double	dSensorToolToBot;									// 深度偏移(该测点距离所在工具底部距离)
	double	dToolACF;
} MS_GAMACORR_PARA;

typedef	MS_GAMACORR_PARA FAR* LPMS_GAMACORR_PARA;

#endif // !defined(AFX_ORDERINFO_H__D90A590B_7EDB_46D2_963A_DC7E24CEED8A__INCLUDED_)

//2020.7.11 ver1.6.0 bug[76] start
//保存从数据库获取的深度以及计算后的垂深
typedef struct _CAL_VDEPTH
{
	INT64 nSurveyDepth;//测深
	INT64 nTVDDepth;//垂深
	INT64 nDataID;
}CAL_VDEPTH;
//2020.7.11 very1.6.0 Bug[76] start

//2020.7.14 ver1.6.0 bug[102] start
typedef struct _CAL_ECD
{
	INT64 nPressureVDepth;
	float fECD;
	INT64 nDataID;
}CAL_ECD;
//2020.7.14 ver1.6.0 bug[102] end

typedef struct tagMS_RUNINFOTB
{
	TS_RUNINFOTB gTS_RUNINFOTB;			// 趟钻编信息
	double	dHoleSize;					// 井眼尺寸（裸眼直经）
} MS_RUNINFOTB;

//2020.9.17 ver1.6.0 bug[183] start
typedef struct _CALMEASUREMENT
{
	time_t dataTime;
	int millitime;
	int nDrillActiv;
	long lDepth;
	int nBad;
	int nConf;
	int nRelog;
	int nStatus;

	float fPFL_400;
	float fPFH_2M;
	float fAFL_400;
	float fANL_400;
	float fAFH_2M;
	float fPNL_400;
	float fPNH_2M;
	float fANH_2M;

	float fAmpRE400K;
	float fAmpIM400K;
	int iSectorRe400;
	int iSectorIm400;

	float fNBRes;
	float fRBRes;
	float fD2RB;
	float fRBAZI;
}CALMEASUREMENT;

typedef struct _CALAZIRES
{
	time_t dataTime;
	int millitime;
	long lDepth;
	int nDrillActiv;
	int nBad;
	int nConf;
	int nRelog;
	short nStatus;

	float fAmpIM400K;
	int iSectorIm400;
	float fRes_PFL;
	float fRes_PFH;
	float fRes_AFL;
	float fRes_ANL;
	float fRes_AFH;
	float fRes_PNL;
	float fRes_PNH;
	float fRes_ANH;


	float fIUpRes;
	float fIDownRes;
}CALAZIRES;

typedef struct _CALECDM
{
	int		nID;
	long	lAZIGRVDepth;	// 方位伽马测量垂深
	float	fAPM;			// 环空压力
	float	fECDM;			// 循环当量密度 = 环空压力 / 方位伽马测量点垂深
	bool	bIsCal;			//标记表中的方位伽马垂深是否被计算过。被计算为true，否则为false。为false时不进行ECDM的计算
	short	nStatus;
}CALECDM;
//2020.9.17 ver1.6.0 bug[183] end

//2020.11.2 bug[229] start
typedef struct _CHOOSEUNIT
{
	CString strCurveName;
	int nWitsID;
	CString strUnitType;
	CString strUnitName;
	int nDataFrom;
}CHOOSEUNIT;
//2020.11.2 bug[229] end

//2020.11.4 bug[236] start
typedef struct _TOOLAIZRES
{
	TS_TOOLAIZRES aizRes;
	char szToolName[TV_TOOLINFOTB_TOOLNAME_DIGITS + 1];
	int oldToolID;
	char oldToolSN[TV_TOOLAIZRES_TOOLSN_DIGITS + 1];
}TOOLAIZRES;
//2020.11.4 bug[236] end

typedef struct _PRESSURE_CALIBRATION
{
	TS_PRESSURECALIBRATION gPressureCalibration;
	char szToolName[TV_TOOLINFOTB_TOOLNAME_DIGITS + 1];
	int oldToolID;
	char oldToolSN[TV_PRESSURECALIBRATION_TOOLSN_DIGITS + 1];
}PRESSURE_CALIBRATION;

//2020.12.3 ver1.6.0 bug[265] start
typedef struct _RELOG_GRID
{
	TS_RELOG reLog;
	char szRunName[TV_RUNINFOTB_RUNNAME_DIGITS + 1];
}RELOG_GRID;

typedef struct _RELOG_TD
{
	int nStatus;// 0:开始  1:进行中  2:结束
	SYSTEMTIME time;
	long depth;
}RELOG_TD;

//2020.12.3 ver1.6.0 bug[265] end

typedef struct tagMS_CURVEINFOTB
{
	TS_CURVEINFOTB gCURVEINFOTB;
	char	szUName[TV_UNITTB_UNAME_DIGITS + 1];							// 单位名称
} MS_CURVEINFOTB;

// 2021.05.07 ltg Start
#include <TTimeDepthTB.h>
typedef struct tagMS_DEPTHSYSDATA
{
	char	szRunID[TV_TIMEDEPTHTB_RUNID_DIGITS + 1];				// 趟钻编号
	time_t	lTDateTime;												// 时间（精确到秒）
	int	iMillitime;													// 毫秒
	long	lBitDepth;												// 钻头深度 
	long	lWellDepth;												// 井深
	double	dROP;													// 钻进速度按30秒或井深增加0. 2米，先到就算
	short	nBad;													// 是否坏点:No|0.否;Yes|1.是
	short	nDrillActiv;											// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
	short	nSliding;												// 是否滑动:No|0.否;Yes|1.是
} MS_DEPTHSYSDATA;
typedef	MS_DEPTHSYSDATA FAR* LPMS_DEPTHSYSDATA;
// 202105.07 ltg End

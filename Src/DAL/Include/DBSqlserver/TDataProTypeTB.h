//---------------------------------------------------------------------------//
// 文件名: DataProTypeTB.h
// 说明:	数据处理类型表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/2/2 10:18:14
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TDataProTypeTB.h : DDataProTypeTB

#ifndef	_TDATAPROTYPETB_H
#define	_TDATAPROTYPETB_H

#define	TID_DATAPROTYPETB								_T("DataProTypeTB")
#define	OID_DATAPROTYPETB								_T("")

// Sort No
#define	SORT_DATAPROTYPETB_PK0				0							// PK:数据处理类型ID
//#define	SORT_DATAPROTYPETB_A1							%#%							// A1:

// Colum No
#define	COL_DATAPROTYPETB_DPTID					(short)0						// 数据处理类型ID
#define	COL_DATAPROTYPETB_TNAME					(short)1						// 数据处理类型:WellSiteData|1.井场数据;CurveData|2.曲线数据;DepthData|3.深度数据
#define	COL_DATAPROTYPETB_DESCRIPTION_CN					(short)2						// 中文描述
#define	COL_DATAPROTYPETB_DESCRIPTION_EN					(short)3						// 英文描述
#define	COL_DATAPROTYPETB_CREATETIME					(short)4						// 创建时间戳
#define	COL_DATAPROTYPETB_STATUS					(short)5						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_DATAPROTYPETB_MEMO					(short)6						// 备注
#define	COL_DATAPROTYPETB_UPDCOUNT					(short)7						// 更新计数

// Colum(Field) Name
#define	FLD_DATAPROTYPETB_DPTID					_T("DPTID")					// 数据处理类型ID
#define	FLD_DATAPROTYPETB_TNAME					_T("TName")					// 数据处理类型:WellSiteData|1.井场数据;CurveData|2.曲线数据;DepthData|3.深度数据
#define	FLD_DATAPROTYPETB_DESCRIPTION_CN					_T("Description_cn")					// 中文描述
#define	FLD_DATAPROTYPETB_DESCRIPTION_EN					_T("Description_en")					// 英文描述
#define	FLD_DATAPROTYPETB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_DATAPROTYPETB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_DATAPROTYPETB_MEMO					_T("Memo")					// 备注
#define	FLD_DATAPROTYPETB_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_DATAPROTYPETB_DESCRIPTION_CN_DIGITS				100					// 中文描述位数
#define	TV_DATAPROTYPETB_DESCRIPTION_EN_DIGITS				100					// 英文描述位数
#define	TV_DATAPROTYPETB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_DATAPROTYPETB
{
 
  int	iDPTID;							// 数据处理类型ID
  int	iTName;							// 数据处理类型:WellSiteData|1.井场数据;CurveData|2.曲线数据;DepthData|3.深度数据
  char	szDescription_cn[TV_DATAPROTYPETB_DESCRIPTION_CN_DIGITS + 1];							// 中文描述
  char	szDescription_en[TV_DATAPROTYPETB_DESCRIPTION_EN_DIGITS + 1];							// 英文描述
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_DATAPROTYPETB_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_DATAPROTYPETB;

typedef	TS_DATAPROTYPETB FAR*	LPTS_DATAPROTYPETB;

#pragma	pack(pop)

#endif // _TDATAPROTYPETB_H

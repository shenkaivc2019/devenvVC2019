//---------------------------------------------------------------------------//
// 文件名: RT_GRImage.h
// 说明:	实时伽马成像数据表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2022/12/14 15:35:32
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TRT_GRImage.h : DRT_GRImage

#ifndef	_TRT_GRIMAGE_H
#define	_TRT_GRIMAGE_H

#define	TID_RT_GRIMAGE								_T("RT_GRImage")
#define	OID_RT_GRIMAGE								_T("")

// Sort No
#define	SORT_RT_GRIMAGE_PK0				0							// PK:数据编号
//#define	SORT_RT_GRIMAGE_A1							%#%							// A1:

// Colum No
#define	COL_RT_GRIMAGE_DATAID					(short)0						// 数据编号
#define	COL_RT_GRIMAGE_RUNID					(short)1						// 趟钻编号
#define	COL_RT_GRIMAGE_TDATETIME					(short)2						// 时间（精确到秒）
#define	COL_RT_GRIMAGE_MILLITIME					(short)3						// 毫秒
#define	COL_RT_GRIMAGE_TOOLID					(short)4						// 工具编号
#define	COL_RT_GRIMAGE_MDEPTH					(short)5						// 方位伽马测量点井深
#define	COL_RT_GRIMAGE_VDEPTH					(short)6						// 方位伽马测量点垂深
#define	COL_RT_GRIMAGE_GRUDATAID					(short)7						// 4扇区扇区上伽马计数数据编号
#define	COL_RT_GRIMAGE_GRDDATAID					(short)8						// 4扇区扇区下伽马计数数据编号
#define	COL_RT_GRIMAGE_GRLDATAID					(short)9						// 4扇区扇区左伽马计数数据编号
#define	COL_RT_GRIMAGE_GRRDATAID					(short)10						// 4扇区扇区右伽马计数数据编号
#define	COL_RT_GRIMAGE_GRU					(short)11						// 4扇区扇区上伽马计数
#define	COL_RT_GRIMAGE_GRD					(short)12						// 4扇区扇区下伽马计数
#define	COL_RT_GRIMAGE_GRL					(short)13						// 4扇区扇区左伽马计数
#define	COL_RT_GRIMAGE_GRR					(short)14						// 4扇区扇区右伽马计数
#define	COL_RT_GRIMAGE_GRT					(short)15						// 4扇区扇区平均伽马计数
#define	COL_RT_GRIMAGE_CONF					(short)16						// 可信度
#define	COL_RT_GRIMAGE_BAD					(short)17						// 是否坏点:No|0.否;Yes|1.是
#define	COL_RT_GRIMAGE_DRILLACTIV					(short)18						// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	COL_RT_GRIMAGE_SLIDING					(short)19						// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	COL_RT_GRIMAGE_RELOG					(short)20						// 是否复测 :Yes|0.否;No|1.是
#define	COL_RT_GRIMAGE_LAS					(short)21						// 是否Las :No|0.否;Yes|1.是
#define	COL_RT_GRIMAGE_CREATETIME					(short)22						// 创建时间戳
#define	COL_RT_GRIMAGE_UPDTIME					(short)23						// 最后一次修改时间戳
#define	COL_RT_GRIMAGE_STATUS					(short)24						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_RT_GRIMAGE_MEMO					(short)25						// 备注

// Colum(Field) Name
#define	FLD_RT_GRIMAGE_DATAID					_T("DataID")					// 数据编号
#define	FLD_RT_GRIMAGE_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_RT_GRIMAGE_TDATETIME					_T("TDateTime")					// 时间（精确到秒）
#define	FLD_RT_GRIMAGE_MILLITIME					_T("Millitime")					// 毫秒
#define	FLD_RT_GRIMAGE_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_RT_GRIMAGE_MDEPTH					_T("MDepth")					// 方位伽马测量点井深
#define	FLD_RT_GRIMAGE_VDEPTH					_T("VDepth")					// 方位伽马测量点垂深
#define	FLD_RT_GRIMAGE_GRUDATAID					_T("GRUDataID")					// 4扇区扇区上伽马计数数据编号
#define	FLD_RT_GRIMAGE_GRDDATAID					_T("GRDDataID")					// 4扇区扇区下伽马计数数据编号
#define	FLD_RT_GRIMAGE_GRLDATAID					_T("GRLDataID")					// 4扇区扇区左伽马计数数据编号
#define	FLD_RT_GRIMAGE_GRRDATAID					_T("GRRDataID")					// 4扇区扇区右伽马计数数据编号
#define	FLD_RT_GRIMAGE_GRU					_T("GRU")					// 4扇区扇区上伽马计数
#define	FLD_RT_GRIMAGE_GRD					_T("GRD")					// 4扇区扇区下伽马计数
#define	FLD_RT_GRIMAGE_GRL					_T("GRL")					// 4扇区扇区左伽马计数
#define	FLD_RT_GRIMAGE_GRR					_T("GRR")					// 4扇区扇区右伽马计数
#define	FLD_RT_GRIMAGE_GRT					_T("GRT")					// 4扇区扇区平均伽马计数
#define	FLD_RT_GRIMAGE_CONF					_T("Conf")					// 可信度
#define	FLD_RT_GRIMAGE_BAD					_T("Bad")					// 是否坏点:No|0.否;Yes|1.是
#define	FLD_RT_GRIMAGE_DRILLACTIV					_T("DrillActiv")					// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	FLD_RT_GRIMAGE_SLIDING					_T("Sliding")					// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	FLD_RT_GRIMAGE_RELOG					_T("ReLog")					// 是否复测 :Yes|0.否;No|1.是
#define	FLD_RT_GRIMAGE_LAS					_T("Las")					// 是否Las :No|0.否;Yes|1.是
#define	FLD_RT_GRIMAGE_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_RT_GRIMAGE_UPDTIME					_T("UpdTime")					// 最后一次修改时间戳
#define	FLD_RT_GRIMAGE_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_RT_GRIMAGE_MEMO					_T("Memo")					// 备注

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_RT_GRIMAGE_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_RT_GRIMAGE_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_RT_GRIMAGE
{
 
  long	lDataID;							// 数据编号
  char	szRunID[TV_RT_GRIMAGE_RUNID_DIGITS + 1];							// 趟钻编号
  time_t	lTDateTime;							// 时间（精确到秒）
  int	iMillitime;							// 毫秒
  int	iToolID;							// 工具编号
  long	lMDepth;							// 方位伽马测量点井深
  long	lVDepth;							// 方位伽马测量点垂深
  long	lGRUDataID;							// 4扇区扇区上伽马计数数据编号
  long	lGRDDataID;							// 4扇区扇区下伽马计数数据编号
  long	lGRLDataID;							// 4扇区扇区左伽马计数数据编号
  long	lGRRDataID;							// 4扇区扇区右伽马计数数据编号
  float	fGRU;							// 4扇区扇区上伽马计数
  float	fGRD;							// 4扇区扇区下伽马计数
  float	fGRL;							// 4扇区扇区左伽马计数
  float	fGRR;							// 4扇区扇区右伽马计数
  float	fGRT;							// 4扇区扇区平均伽马计数
  short	nConf;							// 可信度
  short	nBad;							// 是否坏点:No|0.否;Yes|1.是
  short	nDrillActiv;							// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
  short	nSliding;							// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
  short	nReLog;							// 是否复测 :Yes|0.否;No|1.是
  short	nLas;							// 是否Las :No|0.否;Yes|1.是
  time_t	lCreateTime;							// 创建时间戳
  time_t	lUpdTime;							// 最后一次修改时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_RT_GRIMAGE_MEMO_DIGITS + 1];							// 备注
} TS_RT_GRIMAGE;

typedef	TS_RT_GRIMAGE FAR*	LPTS_RT_GRIMAGE;

#pragma	pack(pop)

#endif // _TRT_GRIMAGE_H

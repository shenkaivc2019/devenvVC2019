//---------------------------------------------------------------------------//
// 文件名: ME_AziGammaTB.h
// 说明:	仪器内存数据表(方位伽马）
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/10/13 14:53:33
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TME_AziGammaTB.h : DME_AziGammaTB

#ifndef	_TME_AZIGAMMATB_H
#define	_TME_AZIGAMMATB_H

#define	TID_ME_AZIGAMMATB								_T("ME_AziGammaTB")
#define	OID_ME_AZIGAMMATB								_T("")

// Sort No
#define	SORT_ME_AZIGAMMATB_PK0				0							// PK:自增字段
//#define	SORT_ME_AZIGAMMATB_A1							%#%							// A1:

// Colum No
#define	COL_ME_AZIGAMMATB_DATAID					(short)0						// 自增字段
#define	COL_ME_AZIGAMMATB_RUNID					(short)1						// 趟钻编号
#define	COL_ME_AZIGAMMATB_TDATETIME					(short)2						// 时间（精确到秒）
#define	COL_ME_AZIGAMMATB_MILLITIME					(short)3						// 毫秒
#define	COL_ME_AZIGAMMATB_TOOLID					(short)4						// 工具编号
#define	COL_ME_AZIGAMMATB_AZIGRMDEPTH					(short)5						// 方位伽马测量井深
#define	COL_ME_AZIGAMMATB_AZIGRVDEPTH					(short)6						// 方位伽马测量垂深
#define	COL_ME_AZIGAMMATB_AZIGAM_S0					(short)7						// 16扇0区扇区伽马值
#define	COL_ME_AZIGAMMATB_AZIGAM_S1					(short)8						// 16扇1区扇区伽马值
#define	COL_ME_AZIGAMMATB_AZIGAM_S2					(short)9						// 16扇2区扇区伽马值
#define	COL_ME_AZIGAMMATB_AZIGAM_S3					(short)10						// 16扇3区扇区伽马值
#define	COL_ME_AZIGAMMATB_AZIGAM_S4					(short)11						// 16扇4区扇区伽马值
#define	COL_ME_AZIGAMMATB_AZIGAM_S5					(short)12						// 16扇5区扇区伽马值
#define	COL_ME_AZIGAMMATB_AZIGAM_S6					(short)13						// 16扇6区扇区伽马值
#define	COL_ME_AZIGAMMATB_AZIGAM_S7					(short)14						// 16扇7区扇区伽马值
#define	COL_ME_AZIGAMMATB_AZIGAM_S8					(short)15						// 16扇8区扇区伽马值
#define	COL_ME_AZIGAMMATB_AZIGAM_S9					(short)16						// 16扇9区扇区伽马值
#define	COL_ME_AZIGAMMATB_AZIGAM_S10					(short)17						// 16扇10区扇区伽马值
#define	COL_ME_AZIGAMMATB_AZIGAM_S11					(short)18						// 16扇11区扇区伽马值
#define	COL_ME_AZIGAMMATB_AZIGAM_S12					(short)19						// 16扇12区扇区伽马值
#define	COL_ME_AZIGAMMATB_AZIGAM_S13					(short)20						// 16扇13区扇区伽马值
#define	COL_ME_AZIGAMMATB_AZIGAM_S14					(short)21						// 16扇14区扇区伽马值
#define	COL_ME_AZIGAMMATB_AZIGAM_S15					(short)22						// 16扇15区扇区伽马值
#define	COL_ME_AZIGAMMATB_AZIGAM_T					(short)23						// 总伽马RAW
#define	COL_ME_AZIGAMMATB_AZIGAM_S0_API					(short)24						// 16扇区0扇区伽马值（API计算系数校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S0_APIC					(short)25						// 16扇区0扇区伽马值（API计算环境校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S1_API					(short)26						// 16扇区1扇区伽马值（API计算系数校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S1_APIC					(short)27						// 16扇区1扇区伽马值（API计算环境校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S2_API					(short)28						// 16扇区2扇区伽马值（API计算系数校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S2_APIC					(short)29						// 16扇区2扇区伽马值（API计算环境校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S3_API					(short)30						// 16扇区3扇区伽马值（API计算系数校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S3_APIC					(short)31						// 16扇区3扇区伽马值（API计算环境校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S4_API					(short)32						// 16扇区4扇区伽马值（API计算系数校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S4_APIC					(short)33						// 16扇区4扇区伽马值（API计算环境校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S5_API					(short)34						// 16扇区5扇区伽马值（API计算系数校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S5_APIC					(short)35						// 16扇区5扇区伽马值（API计算环境校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S6_API					(short)36						// 16扇区6扇区伽马值（API计算系数校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S6_APIC					(short)37						// 16扇区6扇区伽马值（API计算环境校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S7_API					(short)38						// 16扇区7扇区伽马值（API计算系数校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S7_APIC					(short)39						// 16扇区7扇区伽马值（API计算环境校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S8_API					(short)40						// 16扇区8扇区伽马值（API计算系数校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S8_APIC					(short)41						// 16扇区8扇区伽马值（API计算环境校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S9_API					(short)42						// 16扇区9扇区伽马值（API计算系数校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S9_APIC					(short)43						// 16扇区9扇区伽马值（API计算环境校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S10_API					(short)44						// 16扇区10扇区伽马值（API计算系数校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S10_APIC					(short)45						// 16扇区10扇区伽马值（API计算环境校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S11_API					(short)46						// 16扇区11扇区伽马值（API计算系数校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S11_APIC					(short)47						// 16扇区11扇区伽马值（API计算环境校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S12_API					(short)48						// 16扇区12扇区伽马值（API计算系数校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S12_APIC					(short)49						// 16扇区12扇区伽马值（API计算环境校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S13_API					(short)50						// 16扇区13扇区伽马值（API计算系数校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S13_APIC					(short)51						// 16扇区13扇区伽马值（API计算环境校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S14_API					(short)52						// 16扇区14扇区伽马值（API计算系数校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S14_APIC					(short)53						// 16扇区14扇区伽马值（API计算环境校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S15_API					(short)54						// 16扇区15扇区伽马值（API计算系数校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S15_APIC					(short)55						// 16扇区15扇区伽马值（API计算环境校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_TAPI					(short)56						// 总伽马经过API计算系数校正后数据
#define	COL_ME_AZIGAMMATB_AZIGAM_TAPIC					(short)57						// 总伽马经过API计算环境校正后数据
#define	COL_ME_AZIGAMMATB_AZIGAM_S0_API_D					(short)58						// 16扇区0扇区伽马值（API暗电流校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S0_APIC_G					(short)59						// 16扇区0扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S1_API_D					(short)60						// 16扇区1扇区伽马值（API暗电流校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S1_APIC_G					(short)61						// 16扇区1扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S2_API_D					(short)62						// 16扇区2扇区伽马值（API暗电流校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S2_APIC_G					(short)63						// 16扇区2扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S3_API_D					(short)64						// 16扇区3扇区伽马值（API暗电流校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S3_APIC_G					(short)65						// 16扇区3扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S4_API_D					(short)66						// 16扇区4扇区伽马值（API暗电流校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S4_APIC_G					(short)67						// 16扇区4扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S5_API_D					(short)68						// 16扇区5扇区伽马值（API暗电流校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S5_APIC_G					(short)69						// 16扇区5扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S6_API_D					(short)70						// 16扇区6扇区伽马值（API暗电流校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S6_APIC_G					(short)71						// 16扇区6扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S7_API_D					(short)72						// 16扇区7扇区伽马值（API暗电流校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S7_APIC_G					(short)73						// 16扇区7扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S8_API_D					(short)74						// 16扇区8扇区伽马值（API暗电流校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S8_APIC_G					(short)75						// 16扇区8扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S9_API_D					(short)76						// 16扇区9扇区伽马值（API暗电流校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S9_APIC_G					(short)77						// 16扇区9扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S10_API_D					(short)78						// 16扇区10扇区伽马值（API暗电流校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S10_APIC_G					(short)79						// 16扇区10扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S11_API_D					(short)80						// 16扇区11扇区伽马值（API暗电流校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S11_APIC_G					(short)81						// 16扇区11扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S12_API_D					(short)82						// 16扇区12扇区伽马值（API暗电流校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S12_APIC_G					(short)83						// 16扇区12扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S13_API_D					(short)84						// 16扇区13扇区伽马值（API暗电流校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S13_APIC_G					(short)85						// 16扇区13扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S14_API_D					(short)86						// 16扇区14扇区伽马值（API暗电流校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S14_APIC_G					(short)87						// 16扇区14扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S15_API_D					(short)88						// 16扇区15扇区伽马值（API暗电流校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_S15_APIC_G					(short)89						// 16扇区15扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_AZIGAMMATB_AZIGAM_TAPI_D_T					(short)90						// 总伽马经过API计算系数校正后数据
#define	COL_ME_AZIGAMMATB_AZIGAM_TAPIC_G_T					(short)91						// 总伽马经过API计算环境校正后数据
#define	COL_ME_AZIGAMMATB_AZIGAMU_TAPIC					(short)92						// 上伽马经过API计算环境校正后数据
#define	COL_ME_AZIGAMMATB_AZIGAMD_TAPIC					(short)93						// 下伽马经过API计算环境校正后数据
#define	COL_ME_AZIGAMMATB_AZIGAML_TAPIC					(short)94						// 左伽马经过API计算环境校正后数据
#define	COL_ME_AZIGAMMATB_AZIGAMR_TAPIC					(short)95						// 右伽马经过API计算环境校正后数据
#define	COL_ME_AZIGAMMATB_TEMP_FX10					(short)96						// 钻铤温度
#define	COL_ME_AZIGAMMATB_APM					(short)97						// 环空压力
#define	COL_ME_AZIGAMMATB_ECDM					(short)98						// 循环当量密度=环空压力/方位伽马测量点垂深
#define	COL_ME_AZIGAMMATB_CONF					(short)99						// 旋转状态:No|0.否;Yes|1.是
#define	COL_ME_AZIGAMMATB_BAD					(short)100						// 是否坏点:No|0.否;Yes|1.是
#define	COL_ME_AZIGAMMATB_DRILLACTIV					(short)101						// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	COL_ME_AZIGAMMATB_SLIDING					(short)102						// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	COL_ME_AZIGAMMATB_RELOG					(short)103						// 是否复测 :Yes|0.否;No|1.是
#define	COL_ME_AZIGAMMATB_LAS					(short)104						// 是否Las :Yes|0.否;No|1.是
#define	COL_ME_AZIGAMMATB_CREATETIME					(short)105						// 创建时间戳
#define	COL_ME_AZIGAMMATB_UPDTIME					(short)106						// 最后一次修改时间戳
#define	COL_ME_AZIGAMMATB_STATUS					(short)107						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_ME_AZIGAMMATB_MEMO					(short)108						// 备注

// Colum(Field) Name
#define	FLD_ME_AZIGAMMATB_DATAID					_T("DataID")					// 自增字段
#define	FLD_ME_AZIGAMMATB_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_ME_AZIGAMMATB_TDATETIME					_T("TDateTime")					// 时间（精确到秒）
#define	FLD_ME_AZIGAMMATB_MILLITIME					_T("Millitime")					// 毫秒
#define	FLD_ME_AZIGAMMATB_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_ME_AZIGAMMATB_AZIGRMDEPTH					_T("AZIGRMDepth")					// 方位伽马测量井深
#define	FLD_ME_AZIGAMMATB_AZIGRVDEPTH					_T("AZIGRVDepth")					// 方位伽马测量垂深
#define	FLD_ME_AZIGAMMATB_AZIGAM_S0					_T("AZIGAM_S0")					// 16扇0区扇区伽马值
#define	FLD_ME_AZIGAMMATB_AZIGAM_S1					_T("AZIGAM_S1")					// 16扇1区扇区伽马值
#define	FLD_ME_AZIGAMMATB_AZIGAM_S2					_T("AZIGAM_S2")					// 16扇2区扇区伽马值
#define	FLD_ME_AZIGAMMATB_AZIGAM_S3					_T("AZIGAM_S3")					// 16扇3区扇区伽马值
#define	FLD_ME_AZIGAMMATB_AZIGAM_S4					_T("AZIGAM_S4")					// 16扇4区扇区伽马值
#define	FLD_ME_AZIGAMMATB_AZIGAM_S5					_T("AZIGAM_S5")					// 16扇5区扇区伽马值
#define	FLD_ME_AZIGAMMATB_AZIGAM_S6					_T("AZIGAM_S6")					// 16扇6区扇区伽马值
#define	FLD_ME_AZIGAMMATB_AZIGAM_S7					_T("AZIGAM_S7")					// 16扇7区扇区伽马值
#define	FLD_ME_AZIGAMMATB_AZIGAM_S8					_T("AZIGAM_S8")					// 16扇8区扇区伽马值
#define	FLD_ME_AZIGAMMATB_AZIGAM_S9					_T("AZIGAM_S9")					// 16扇9区扇区伽马值
#define	FLD_ME_AZIGAMMATB_AZIGAM_S10					_T("AZIGAM_S10")					// 16扇10区扇区伽马值
#define	FLD_ME_AZIGAMMATB_AZIGAM_S11					_T("AZIGAM_S11")					// 16扇11区扇区伽马值
#define	FLD_ME_AZIGAMMATB_AZIGAM_S12					_T("AZIGAM_S12")					// 16扇12区扇区伽马值
#define	FLD_ME_AZIGAMMATB_AZIGAM_S13					_T("AZIGAM_S13")					// 16扇13区扇区伽马值
#define	FLD_ME_AZIGAMMATB_AZIGAM_S14					_T("AZIGAM_S14")					// 16扇14区扇区伽马值
#define	FLD_ME_AZIGAMMATB_AZIGAM_S15					_T("AZIGAM_S15")					// 16扇15区扇区伽马值
#define	FLD_ME_AZIGAMMATB_AZIGAM_T					_T("AZIGAM_T")					// 总伽马RAW
#define	FLD_ME_AZIGAMMATB_AZIGAM_S0_API					_T("AZIGAM_S0_API")					// 16扇区0扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S0_APIC					_T("AZIGAM_S0_APIC")					// 16扇区0扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S1_API					_T("AZIGAM_S1_API")					// 16扇区1扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S1_APIC					_T("AZIGAM_S1_APIC")					// 16扇区1扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S2_API					_T("AZIGAM_S2_API")					// 16扇区2扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S2_APIC					_T("AZIGAM_S2_APIC")					// 16扇区2扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S3_API					_T("AZIGAM_S3_API")					// 16扇区3扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S3_APIC					_T("AZIGAM_S3_APIC")					// 16扇区3扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S4_API					_T("AZIGAM_S4_API")					// 16扇区4扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S4_APIC					_T("AZIGAM_S4_APIC")					// 16扇区4扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S5_API					_T("AZIGAM_S5_API")					// 16扇区5扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S5_APIC					_T("AZIGAM_S5_APIC")					// 16扇区5扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S6_API					_T("AZIGAM_S6_API")					// 16扇区6扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S6_APIC					_T("AZIGAM_S6_APIC")					// 16扇区6扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S7_API					_T("AZIGAM_S7_API")					// 16扇区7扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S7_APIC					_T("AZIGAM_S7_APIC")					// 16扇区7扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S8_API					_T("AZIGAM_S8_API")					// 16扇区8扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S8_APIC					_T("AZIGAM_S8_APIC")					// 16扇区8扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S9_API					_T("AZIGAM_S9_API")					// 16扇区9扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S9_APIC					_T("AZIGAM_S9_APIC")					// 16扇区9扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S10_API					_T("AZIGAM_S10_API")					// 16扇区10扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S10_APIC					_T("AZIGAM_S10_APIC")					// 16扇区10扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S11_API					_T("AZIGAM_S11_API")					// 16扇区11扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S11_APIC					_T("AZIGAM_S11_APIC")					// 16扇区11扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S12_API					_T("AZIGAM_S12_API")					// 16扇区12扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S12_APIC					_T("AZIGAM_S12_APIC")					// 16扇区12扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S13_API					_T("AZIGAM_S13_API")					// 16扇区13扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S13_APIC					_T("AZIGAM_S13_APIC")					// 16扇区13扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S14_API					_T("AZIGAM_S14_API")					// 16扇区14扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S14_APIC					_T("AZIGAM_S14_APIC")					// 16扇区14扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S15_API					_T("AZIGAM_S15_API")					// 16扇区15扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S15_APIC					_T("AZIGAM_S15_APIC")					// 16扇区15扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_TAPI					_T("AZIGAM_TAPI")					// 总伽马经过API计算系数校正后数据
#define	FLD_ME_AZIGAMMATB_AZIGAM_TAPIC					_T("AZIGAM_TAPIC")					// 总伽马经过API计算环境校正后数据
#define	FLD_ME_AZIGAMMATB_AZIGAM_S0_API_D					_T("AZIGAM_S0_API_D")					// 16扇区0扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S0_APIC_G					_T("AZIGAM_S0_APIC_G")					// 16扇区0扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S1_API_D					_T("AZIGAM_S1_API_D")					// 16扇区1扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S1_APIC_G					_T("AZIGAM_S1_APIC_G")					// 16扇区1扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S2_API_D					_T("AZIGAM_S2_API_D")					// 16扇区2扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S2_APIC_G					_T("AZIGAM_S2_APIC_G")					// 16扇区2扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S3_API_D					_T("AZIGAM_S3_API_D")					// 16扇区3扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S3_APIC_G					_T("AZIGAM_S3_APIC_G")					// 16扇区3扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S4_API_D					_T("AZIGAM_S4_API_D")					// 16扇区4扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S4_APIC_G					_T("AZIGAM_S4_APIC_G")					// 16扇区4扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S5_API_D					_T("AZIGAM_S5_API_D")					// 16扇区5扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S5_APIC_G					_T("AZIGAM_S5_APIC_G")					// 16扇区5扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S6_API_D					_T("AZIGAM_S6_API_D")					// 16扇区6扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S6_APIC_G					_T("AZIGAM_S6_APIC_G")					// 16扇区6扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S7_API_D					_T("AZIGAM_S7_API_D")					// 16扇区7扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S7_APIC_G					_T("AZIGAM_S7_APIC_G")					// 16扇区7扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S8_API_D					_T("AZIGAM_S8_API_D")					// 16扇区8扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S8_APIC_G					_T("AZIGAM_S8_APIC_G")					// 16扇区8扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S9_API_D					_T("AZIGAM_S9_API_D")					// 16扇区9扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S9_APIC_G					_T("AZIGAM_S9_APIC_G")					// 16扇区9扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S10_API_D					_T("AZIGAM_S10_API_D")					// 16扇区10扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S10_APIC_G					_T("AZIGAM_S10_APIC_G")					// 16扇区10扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S11_API_D					_T("AZIGAM_S11_API_D")					// 16扇区11扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S11_APIC_G					_T("AZIGAM_S11_APIC_G")					// 16扇区11扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S12_API_D					_T("AZIGAM_S12_API_D")					// 16扇区12扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S12_APIC_G					_T("AZIGAM_S12_APIC_G")					// 16扇区12扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S13_API_D					_T("AZIGAM_S13_API_D")					// 16扇区13扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S13_APIC_G					_T("AZIGAM_S13_APIC_G")					// 16扇区13扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S14_API_D					_T("AZIGAM_S14_API_D")					// 16扇区14扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S14_APIC_G					_T("AZIGAM_S14_APIC_G")					// 16扇区14扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S15_API_D					_T("AZIGAM_S15_API_D")					// 16扇区15扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_S15_APIC_G					_T("AZIGAM_S15_APIC_G")					// 16扇区15扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_AZIGAMMATB_AZIGAM_TAPI_D_T					_T("AZIGAM_TAPI_D_T")					// 总伽马经过API计算系数校正后数据
#define	FLD_ME_AZIGAMMATB_AZIGAM_TAPIC_G_T					_T("AZIGAM_TAPIC_G_T")					// 总伽马经过API计算环境校正后数据
#define	FLD_ME_AZIGAMMATB_AZIGAMU_TAPIC					_T("AZIGAMU_TAPIC")					// 上伽马经过API计算环境校正后数据
#define	FLD_ME_AZIGAMMATB_AZIGAMD_TAPIC					_T("AZIGAMD_TAPIC")					// 下伽马经过API计算环境校正后数据
#define	FLD_ME_AZIGAMMATB_AZIGAML_TAPIC					_T("AZIGAML_TAPIC")					// 左伽马经过API计算环境校正后数据
#define	FLD_ME_AZIGAMMATB_AZIGAMR_TAPIC					_T("AZIGAMR_TAPIC")					// 右伽马经过API计算环境校正后数据
#define	FLD_ME_AZIGAMMATB_TEMP_FX10					_T("TEMP_Fx10")					// 钻铤温度
#define	FLD_ME_AZIGAMMATB_APM					_T("APM")					// 环空压力
#define	FLD_ME_AZIGAMMATB_ECDM					_T("ECDM")					// 循环当量密度=环空压力/方位伽马测量点垂深
#define	FLD_ME_AZIGAMMATB_CONF					_T("Conf")					// 旋转状态:No|0.否;Yes|1.是
#define	FLD_ME_AZIGAMMATB_BAD					_T("Bad")					// 是否坏点:No|0.否;Yes|1.是
#define	FLD_ME_AZIGAMMATB_DRILLACTIV					_T("DrillActiv")					// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	FLD_ME_AZIGAMMATB_SLIDING					_T("Sliding")					// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	FLD_ME_AZIGAMMATB_RELOG					_T("ReLog")					// 是否复测 :Yes|0.否;No|1.是
#define	FLD_ME_AZIGAMMATB_LAS					_T("Las")					// 是否Las :Yes|0.否;No|1.是
#define	FLD_ME_AZIGAMMATB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_ME_AZIGAMMATB_UPDTIME					_T("UpdTime")					// 最后一次修改时间戳
#define	FLD_ME_AZIGAMMATB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_ME_AZIGAMMATB_MEMO					_T("Memo")					// 备注

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_ME_AZIGAMMATB_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_ME_AZIGAMMATB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_ME_AZIGAMMATB
{
 
  int	iDataID;							// 自增字段
  char	szRunID[TV_ME_AZIGAMMATB_RUNID_DIGITS + 1];							// 趟钻编号
  time_t	lTDateTime;							// 时间（精确到秒）
  int	iMillitime;							// 毫秒
  int	iToolID;							// 工具编号
  long	lAZIGRMDepth;							// 方位伽马测量井深
  long	lAZIGRVDepth;							// 方位伽马测量垂深
  float	fAZIGAM_S0;							// 16扇0区扇区伽马值
  float	fAZIGAM_S1;							// 16扇1区扇区伽马值
  float	fAZIGAM_S2;							// 16扇2区扇区伽马值
  float	fAZIGAM_S3;							// 16扇3区扇区伽马值
  float	fAZIGAM_S4;							// 16扇4区扇区伽马值
  float	fAZIGAM_S5;							// 16扇5区扇区伽马值
  float	fAZIGAM_S6;							// 16扇6区扇区伽马值
  float	fAZIGAM_S7;							// 16扇7区扇区伽马值
  float	fAZIGAM_S8;							// 16扇8区扇区伽马值
  float	fAZIGAM_S9;							// 16扇9区扇区伽马值
  float	fAZIGAM_S10;							// 16扇10区扇区伽马值
  float	fAZIGAM_S11;							// 16扇11区扇区伽马值
  float	fAZIGAM_S12;							// 16扇12区扇区伽马值
  float	fAZIGAM_S13;							// 16扇13区扇区伽马值
  float	fAZIGAM_S14;							// 16扇14区扇区伽马值
  float	fAZIGAM_S15;							// 16扇15区扇区伽马值
  float	fAZIGAM_T;							// 总伽马RAW
  float	fAZIGAM_S0_API;							// 16扇区0扇区伽马值（API计算系数校正后数据）
  float	fAZIGAM_S0_APIC;							// 16扇区0扇区伽马值（API计算环境校正后数据）
  float	fAZIGAM_S1_API;							// 16扇区1扇区伽马值（API计算系数校正后数据）
  float	fAZIGAM_S1_APIC;							// 16扇区1扇区伽马值（API计算环境校正后数据）
  float	fAZIGAM_S2_API;							// 16扇区2扇区伽马值（API计算系数校正后数据）
  float	fAZIGAM_S2_APIC;							// 16扇区2扇区伽马值（API计算环境校正后数据）
  float	fAZIGAM_S3_API;							// 16扇区3扇区伽马值（API计算系数校正后数据）
  float	fAZIGAM_S3_APIC;							// 16扇区3扇区伽马值（API计算环境校正后数据）
  float	fAZIGAM_S4_API;							// 16扇区4扇区伽马值（API计算系数校正后数据）
  float	fAZIGAM_S4_APIC;							// 16扇区4扇区伽马值（API计算环境校正后数据）
  float	fAZIGAM_S5_API;							// 16扇区5扇区伽马值（API计算系数校正后数据）
  float	fAZIGAM_S5_APIC;							// 16扇区5扇区伽马值（API计算环境校正后数据）
  float	fAZIGAM_S6_API;							// 16扇区6扇区伽马值（API计算系数校正后数据）
  float	fAZIGAM_S6_APIC;							// 16扇区6扇区伽马值（API计算环境校正后数据）
  float	fAZIGAM_S7_API;							// 16扇区7扇区伽马值（API计算系数校正后数据）
  float	fAZIGAM_S7_APIC;							// 16扇区7扇区伽马值（API计算环境校正后数据）
  float	fAZIGAM_S8_API;							// 16扇区8扇区伽马值（API计算系数校正后数据）
  float	fAZIGAM_S8_APIC;							// 16扇区8扇区伽马值（API计算环境校正后数据）
  float	fAZIGAM_S9_API;							// 16扇区9扇区伽马值（API计算系数校正后数据）
  float	fAZIGAM_S9_APIC;							// 16扇区9扇区伽马值（API计算环境校正后数据）
  float	fAZIGAM_S10_API;							// 16扇区10扇区伽马值（API计算系数校正后数据）
  float	fAZIGAM_S10_APIC;							// 16扇区10扇区伽马值（API计算环境校正后数据）
  float	fAZIGAM_S11_API;							// 16扇区11扇区伽马值（API计算系数校正后数据）
  float	fAZIGAM_S11_APIC;							// 16扇区11扇区伽马值（API计算环境校正后数据）
  float	fAZIGAM_S12_API;							// 16扇区12扇区伽马值（API计算系数校正后数据）
  float	fAZIGAM_S12_APIC;							// 16扇区12扇区伽马值（API计算环境校正后数据）
  float	fAZIGAM_S13_API;							// 16扇区13扇区伽马值（API计算系数校正后数据）
  float	fAZIGAM_S13_APIC;							// 16扇区13扇区伽马值（API计算环境校正后数据）
  float	fAZIGAM_S14_API;							// 16扇区14扇区伽马值（API计算系数校正后数据）
  float	fAZIGAM_S14_APIC;							// 16扇区14扇区伽马值（API计算环境校正后数据）
  float	fAZIGAM_S15_API;							// 16扇区15扇区伽马值（API计算系数校正后数据）
  float	fAZIGAM_S15_APIC;							// 16扇区15扇区伽马值（API计算环境校正后数据）
  float	fAZIGAM_TAPI;							// 总伽马经过API计算系数校正后数据
  float	fAZIGAM_TAPIC;							// 总伽马经过API计算环境校正后数据
  float	fAZIGAM_S0_API_D;							// 16扇区0扇区伽马值（API暗电流校正后数据）
  float	fAZIGAM_S0_APIC_G;							// 16扇区0扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fAZIGAM_S1_API_D;							// 16扇区1扇区伽马值（API暗电流校正后数据）
  float	fAZIGAM_S1_APIC_G;							// 16扇区1扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fAZIGAM_S2_API_D;							// 16扇区2扇区伽马值（API暗电流校正后数据）
  float	fAZIGAM_S2_APIC_G;							// 16扇区2扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fAZIGAM_S3_API_D;							// 16扇区3扇区伽马值（API暗电流校正后数据）
  float	fAZIGAM_S3_APIC_G;							// 16扇区3扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fAZIGAM_S4_API_D;							// 16扇区4扇区伽马值（API暗电流校正后数据）
  float	fAZIGAM_S4_APIC_G;							// 16扇区4扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fAZIGAM_S5_API_D;							// 16扇区5扇区伽马值（API暗电流校正后数据）
  float	fAZIGAM_S5_APIC_G;							// 16扇区5扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fAZIGAM_S6_API_D;							// 16扇区6扇区伽马值（API暗电流校正后数据）
  float	fAZIGAM_S6_APIC_G;							// 16扇区6扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fAZIGAM_S7_API_D;							// 16扇区7扇区伽马值（API暗电流校正后数据）
  float	fAZIGAM_S7_APIC_G;							// 16扇区7扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fAZIGAM_S8_API_D;							// 16扇区8扇区伽马值（API暗电流校正后数据）
  float	fAZIGAM_S8_APIC_G;							// 16扇区8扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fAZIGAM_S9_API_D;							// 16扇区9扇区伽马值（API暗电流校正后数据）
  float	fAZIGAM_S9_APIC_G;							// 16扇区9扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fAZIGAM_S10_API_D;							// 16扇区10扇区伽马值（API暗电流校正后数据）
  float	fAZIGAM_S10_APIC_G;							// 16扇区10扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fAZIGAM_S11_API_D;							// 16扇区11扇区伽马值（API暗电流校正后数据）
  float	fAZIGAM_S11_APIC_G;							// 16扇区11扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fAZIGAM_S12_API_D;							// 16扇区12扇区伽马值（API暗电流校正后数据）
  float	fAZIGAM_S12_APIC_G;							// 16扇区12扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fAZIGAM_S13_API_D;							// 16扇区13扇区伽马值（API暗电流校正后数据）
  float	fAZIGAM_S13_APIC_G;							// 16扇区13扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fAZIGAM_S14_API_D;							// 16扇区14扇区伽马值（API暗电流校正后数据）
  float	fAZIGAM_S14_APIC_G;							// 16扇区14扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fAZIGAM_S15_API_D;							// 16扇区15扇区伽马值（API暗电流校正后数据）
  float	fAZIGAM_S15_APIC_G;							// 16扇区15扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fAZIGAM_TAPI_D_T;							// 总伽马经过API计算系数校正后数据
  float	fAZIGAM_TAPIC_G_T;							// 总伽马经过API计算环境校正后数据
  float	fAZIGAMU_TAPIC;							// 上伽马经过API计算环境校正后数据
  float	fAZIGAMD_TAPIC;							// 下伽马经过API计算环境校正后数据
  float	fAZIGAML_TAPIC;							// 左伽马经过API计算环境校正后数据
  float	fAZIGAMR_TAPIC;							// 右伽马经过API计算环境校正后数据
  float	fTEMP_Fx10;							// 钻铤温度
  float	fAPM;							// 环空压力
  float	fECDM;							// 循环当量密度=环空压力/方位伽马测量点垂深
  short	nConf;							// 旋转状态:No|0.否;Yes|1.是
  short	nBad;							// 是否坏点:No|0.否;Yes|1.是
  short	nDrillActiv;							// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
  short	nSliding;							// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
  short	nReLog;							// 是否复测 :Yes|0.否;No|1.是
  short	nLas;							// 是否Las :Yes|0.否;No|1.是
  time_t	lCreateTime;							// 创建时间戳
  time_t	lUpdTime;							// 最后一次修改时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_ME_AZIGAMMATB_MEMO_DIGITS + 1];							// 备注
} TS_ME_AZIGAMMATB;

typedef	TS_ME_AZIGAMMATB FAR*	LPTS_ME_AZIGAMMATB;

#pragma	pack(pop)

#endif // _TME_AZIGAMMATB_H

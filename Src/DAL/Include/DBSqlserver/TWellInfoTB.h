//---------------------------------------------------------------------------//
// 文件名: WellInfoTB.h
// 说明:	井信息表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/2/2 16:56:34
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TWellInfoTB.h : DWellInfoTB

#ifndef	_TWELLINFOTB_H
#define	_TWELLINFOTB_H

#define	TID_WELLINFOTB								_T("WellInfoTB")
#define	OID_WELLINFOTB								_T("")

// Sort No
#define	SORT_WELLINFOTB_PK0				0							// PK:井编号
//#define	SORT_WELLINFOTB_A1							%#%							// A1:

// Colum No
#define	COL_WELLINFOTB_WELLID					(short)0						// 井编号
#define	COL_WELLINFOTB_WELLSITEID					(short)1						// 井场编号
#define	COL_WELLINFOTB_WELLNAME					(short)2						// 井名称
#define	COL_WELLINFOTB_LATITUDE					(short)3						// 井口的纬度
#define	COL_WELLINFOTB_LONGITUDE					(short)4						// 井口的经度
#define	COL_WELLINFOTB_LOCATIONNORTHING					(short)5						// 井口北坐标
#define	COL_WELLINFOTB_LOCATIONEASTING					(short)6						// 井口东坐标
#define	COL_WELLINFOTB_DEPTHREF					(short)7						// 深度参考相对基准:DrillFloor|0.钻台面高度;KellyBushing|1.方补心高度;GroundLevel|2.地面;SeaLevel|3.海平面;
#define	COL_WELLINFOTB_DRILFLRGRD					(short)8						// 钻台面高度相对于地面
#define	COL_WELLINFOTB_DRILFLRSEA					(short)9						// 钻台面高度相对于海平面
#define	COL_WELLINFOTB_KELLYBUSHDRILFLR					(short)10						// 方补心高度相对于钻台面
#define	COL_WELLINFOTB_KELLYBUSHGRD					(short)11						// 方补心高度相对于地面
#define	COL_WELLINFOTB_KELLYBUSHSEA					(short)12						// 方补心高度相对于海平面
#define	COL_WELLINFOTB_GROUNDLEVEL					(short)13						// 地面海拔高度
#define	COL_WELLINFOTB_CREATETIME					(short)14						// 创建时间戳
#define	COL_WELLINFOTB_STATUS					(short)15						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_WELLINFOTB_MEMO					(short)16						// 备注
#define	COL_WELLINFOTB_UPDCOUNT					(short)17						// 更新计数

// Colum(Field) Name
#define	FLD_WELLINFOTB_WELLID					_T("WellID")					// 井编号
#define	FLD_WELLINFOTB_WELLSITEID					_T("WellSiteID")					// 井场编号
#define	FLD_WELLINFOTB_WELLNAME					_T("WellName")					// 井名称
#define	FLD_WELLINFOTB_LATITUDE					_T("Latitude")					// 井口的纬度
#define	FLD_WELLINFOTB_LONGITUDE					_T("Longitude")					// 井口的经度
#define	FLD_WELLINFOTB_LOCATIONNORTHING					_T("LocationNorthing")					// 井口北坐标
#define	FLD_WELLINFOTB_LOCATIONEASTING					_T("LocationEasting")					// 井口东坐标
#define	FLD_WELLINFOTB_DEPTHREF					_T("DepthRef")					// 深度参考相对基准:DrillFloor|0.钻台面高度;KellyBushing|1.方补心高度;GroundLevel|2.地面;SeaLevel|3.海平面;
#define	FLD_WELLINFOTB_DRILFLRGRD					_T("DrilFlrGrd")					// 钻台面高度相对于地面
#define	FLD_WELLINFOTB_DRILFLRSEA					_T("DrilFlrSea")					// 钻台面高度相对于海平面
#define	FLD_WELLINFOTB_KELLYBUSHDRILFLR					_T("KellyBushDrilFlr")					// 方补心高度相对于钻台面
#define	FLD_WELLINFOTB_KELLYBUSHGRD					_T("KellyBushGrd")					// 方补心高度相对于地面
#define	FLD_WELLINFOTB_KELLYBUSHSEA					_T("KellyBushsea")					// 方补心高度相对于海平面
#define	FLD_WELLINFOTB_GROUNDLEVEL					_T("GroundLevel")					// 地面海拔高度
#define	FLD_WELLINFOTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_WELLINFOTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_WELLINFOTB_MEMO					_T("Memo")					// 备注
#define	FLD_WELLINFOTB_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_WELLINFOTB_WELLID_DIGITS				50					// 井编号位数
#define	TV_WELLINFOTB_WELLSITEID_DIGITS				50					// 井场编号位数
#define	TV_WELLINFOTB_WELLNAME_DIGITS				50					// 井名称位数
#define	TV_WELLINFOTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_WELLINFOTB
{
 
  char	szWellID[TV_WELLINFOTB_WELLID_DIGITS + 1];							// 井编号
  char	szWellSiteID[TV_WELLINFOTB_WELLSITEID_DIGITS + 1];							// 井场编号
  char	szWellName[TV_WELLINFOTB_WELLNAME_DIGITS + 1];							// 井名称
  double	dLatitude;							// 井口的纬度
  double	dLongitude;							// 井口的经度
  double	dLocationNorthing;							// 井口北坐标
  double	dLocationEasting;							// 井口东坐标
  short	nDepthRef;							// 深度参考相对基准:DrillFloor|0.钻台面高度;KellyBushing|1.方补心高度;GroundLevel|2.地面;SeaLevel|3.海平面;
  float	fDrilFlrGrd;							// 钻台面高度相对于地面
  float	fDrilFlrSea;							// 钻台面高度相对于海平面
  float	fKellyBushDrilFlr;							// 方补心高度相对于钻台面
  float	fKellyBushGrd;							// 方补心高度相对于地面
  float	fKellyBushsea;							// 方补心高度相对于海平面
  float	fGroundLevel;							// 地面海拔高度
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_WELLINFOTB_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_WELLINFOTB;

typedef	TS_WELLINFOTB FAR*	LPTS_WELLINFOTB;

#pragma	pack(pop)

#endif // _TWELLINFOTB_H

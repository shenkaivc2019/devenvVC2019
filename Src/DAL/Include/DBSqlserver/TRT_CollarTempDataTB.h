//---------------------------------------------------------------------------//
// 文件名: RT_CollarTempDataTB.h
// 说明:	实时钻铤温度数据表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/10/9 15:57:05
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TRT_CollarTempDataTB.h : DRT_CollarTempDataTB

#ifndef	_TRT_COLLARTEMPDATATB_H
#define	_TRT_COLLARTEMPDATATB_H

#define	TID_RT_COLLARTEMPDATATB								_T("RT_CollarTempDataTB")
#define	OID_RT_COLLARTEMPDATATB								_T("")

// Sort No
#define	SORT_RT_COLLARTEMPDATATB_PK0				0							// PK:数据编号
//#define	SORT_RT_COLLARTEMPDATATB_A1							%#%							// A1:

// Colum No
#define	COL_RT_COLLARTEMPDATATB_DATAID					(short)0						// 数据编号
#define	COL_RT_COLLARTEMPDATATB_RUNID					(short)1						// 趟钻编号
#define	COL_RT_COLLARTEMPDATATB_TDATETIME					(short)2						// 时间（精确到秒）
#define	COL_RT_COLLARTEMPDATATB_MILLITIME					(short)3						// 毫秒
#define	COL_RT_COLLARTEMPDATATB_TOOLID					(short)4						// 工具编号
#define	COL_RT_COLLARTEMPDATATB_DCTEMPMDEPTH					(short)5						// 钻铤温度测量井深
#define	COL_RT_COLLARTEMPDATATB_DCTEMPVDEPTH					(short)6						// 钻铤温度测量垂深
#define	COL_RT_COLLARTEMPDATATB_PREVDEPTH					(short)7						// 测量点预测垂深
#define	COL_RT_COLLARTEMPDATATB_COLLARTEMP					(short)8						// 钻铤温度原始数据值
#define	COL_RT_COLLARTEMPDATATB_VALUEM					(short)9						// 钻铤温度编辑数据值
#define	COL_RT_COLLARTEMPDATATB_CONF					(short)10						// 可信度
#define	COL_RT_COLLARTEMPDATATB_BAD					(short)11						// 是否坏点:No|0.否;Yes|1.是
#define	COL_RT_COLLARTEMPDATATB_DRILLACTIV					(short)12						// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	COL_RT_COLLARTEMPDATATB_SLIDING					(short)13						// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	COL_RT_COLLARTEMPDATATB_RELOG					(short)14						// 是否复测 :Yes|0.否;No|1.是
#define	COL_RT_COLLARTEMPDATATB_LAS					(short)15						// 是否Las :No|0.否;Yes|1.是
#define	COL_RT_COLLARTEMPDATATB_CREATETIME					(short)16						// 创建时间戳
#define	COL_RT_COLLARTEMPDATATB_UPDTIME					(short)17						// 最后一次修改时间戳
#define	COL_RT_COLLARTEMPDATATB_STATUS					(short)18						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_RT_COLLARTEMPDATATB_MEMO					(short)19						// 备注

// Colum(Field) Name
#define	FLD_RT_COLLARTEMPDATATB_DATAID					_T("DataID")					// 数据编号
#define	FLD_RT_COLLARTEMPDATATB_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_RT_COLLARTEMPDATATB_TDATETIME					_T("TDateTime")					// 时间（精确到秒）
#define	FLD_RT_COLLARTEMPDATATB_MILLITIME					_T("Millitime")					// 毫秒
#define	FLD_RT_COLLARTEMPDATATB_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_RT_COLLARTEMPDATATB_DCTEMPMDEPTH					_T("DCTempMDepth")					// 钻铤温度测量井深
#define	FLD_RT_COLLARTEMPDATATB_DCTEMPVDEPTH					_T("DCTempVDepth")					// 钻铤温度测量垂深
#define	FLD_RT_COLLARTEMPDATATB_PREVDEPTH					_T("PreVDepth")					// 测量点预测垂深
#define	FLD_RT_COLLARTEMPDATATB_COLLARTEMP					_T("CollarTemp")					// 钻铤温度原始数据值
#define	FLD_RT_COLLARTEMPDATATB_VALUEM					_T("ValueM")					// 钻铤温度编辑数据值
#define	FLD_RT_COLLARTEMPDATATB_CONF					_T("Conf")					// 可信度
#define	FLD_RT_COLLARTEMPDATATB_BAD					_T("Bad")					// 是否坏点:No|0.否;Yes|1.是
#define	FLD_RT_COLLARTEMPDATATB_DRILLACTIV					_T("DrillActiv")					// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	FLD_RT_COLLARTEMPDATATB_SLIDING					_T("Sliding")					// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	FLD_RT_COLLARTEMPDATATB_RELOG					_T("ReLog")					// 是否复测 :Yes|0.否;No|1.是
#define	FLD_RT_COLLARTEMPDATATB_LAS					_T("Las")					// 是否Las :No|0.否;Yes|1.是
#define	FLD_RT_COLLARTEMPDATATB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_RT_COLLARTEMPDATATB_UPDTIME					_T("UpdTime")					// 最后一次修改时间戳
#define	FLD_RT_COLLARTEMPDATATB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_RT_COLLARTEMPDATATB_MEMO					_T("Memo")					// 备注

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_RT_COLLARTEMPDATATB_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_RT_COLLARTEMPDATATB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_RT_COLLARTEMPDATATB
{
 
  long	lDataID;							// 数据编号
  char	szRunID[TV_RT_COLLARTEMPDATATB_RUNID_DIGITS + 1];							// 趟钻编号
  time_t	lTDateTime;							// 时间（精确到秒）
  int	iMillitime;							// 毫秒
  int	iToolID;							// 工具编号
  long	lDCTempMDepth;							// 钻铤温度测量井深
  long	lDCTempVDepth;							// 钻铤温度测量垂深
  long	lPreVDepth;							// 测量点预测垂深
  double	dCollarTemp;							// 钻铤温度原始数据值
  double	dValueM;							// 钻铤温度编辑数据值
  short	nConf;							// 可信度
  short	nBad;							// 是否坏点:No|0.否;Yes|1.是
  short	nDrillActiv;							// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
  short	nSliding;							// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
  short	nReLog;							// 是否复测 :Yes|0.否;No|1.是
  short	nLas;							// 是否Las :No|0.否;Yes|1.是
  time_t	lCreateTime;							// 创建时间戳
  time_t	lUpdTime;							// 最后一次修改时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_RT_COLLARTEMPDATATB_MEMO_DIGITS + 1];							// 备注
} TS_RT_COLLARTEMPDATATB;

typedef	TS_RT_COLLARTEMPDATATB FAR*	LPTS_RT_COLLARTEMPDATATB;

#pragma	pack(pop)

#endif // _TRT_COLLARTEMPDATATB_H

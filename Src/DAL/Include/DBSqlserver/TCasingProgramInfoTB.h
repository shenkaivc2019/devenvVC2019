//---------------------------------------------------------------------------//
// 文件名: CasingProgramInfoTB.h
// 说明:	井深结构信息表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020/10/19 22:11:35
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TCasingProgramInfoTB.h : DCasingProgramInfoTB

#ifndef	_TCASINGPROGRAMINFOTB_H
#define	_TCASINGPROGRAMINFOTB_H

#define	TID_CASINGPROGRAMINFOTB								_T("CasingProgramInfoTB")
#define	OID_CASINGPROGRAMINFOTB								_T("")

// Sort No
#define	SORT_CASINGPROGRAMINFOTB_PK0				0							// PK:井深结构编号
//#define	SORT_CASINGPROGRAMINFOTB_A1							%#%							// A1:

// Colum No
#define	COL_CASINGPROGRAMINFOTB_CPID					(short)0						// 井深结构编号
#define	COL_CASINGPROGRAMINFOTB_CPNAME					(short)1						// 井深结构名称
#define	COL_CASINGPROGRAMINFOTB_WELLID					(short)2						// 井编号
#define	COL_CASINGPROGRAMINFOTB_SURFACECASING					(short)3						// 表层套管
#define	COL_CASINGPROGRAMINFOTB_SCDEPTH					(short)4						// 表层套管下入井深
#define	COL_CASINGPROGRAMINFOTB_SCSIZE					(short)5						// 表层套管尺寸
#define	COL_CASINGPROGRAMINFOTB_INTERMEDIATECASING					(short)6						// 中间套管
#define	COL_CASINGPROGRAMINFOTB_ICDEPTH					(short)7						// 中间套管下入井深
#define	COL_CASINGPROGRAMINFOTB_ICSIZE					(short)8						// 中间套管尺寸
#define	COL_CASINGPROGRAMINFOTB_OPENHOLE					(short)9						// 裸眼
#define	COL_CASINGPROGRAMINFOTB_OHDEPTH					(short)10						// 裸眼井深
#define	COL_CASINGPROGRAMINFOTB_OHSIZE					(short)11						// 裸眼尺寸
#define	COL_CASINGPROGRAMINFOTB_CREATETIME					(short)12						// 创建时间戳
#define	COL_CASINGPROGRAMINFOTB_STATUS					(short)13						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_CASINGPROGRAMINFOTB_MEMO					(short)14						// 备注
#define	COL_CASINGPROGRAMINFOTB_UPDCOUNT					(short)15						// 更新计数

// Colum(Field) Name
#define	FLD_CASINGPROGRAMINFOTB_CPID					_T("CPID")					// 井深结构编号
#define	FLD_CASINGPROGRAMINFOTB_CPNAME					_T("CPName")					// 井深结构名称
#define	FLD_CASINGPROGRAMINFOTB_WELLID					_T("WellID")					// 井编号
#define	FLD_CASINGPROGRAMINFOTB_SURFACECASING					_T("SurfaceCasing")					// 表层套管
#define	FLD_CASINGPROGRAMINFOTB_SCDEPTH					_T("SCDepth")					// 表层套管下入井深
#define	FLD_CASINGPROGRAMINFOTB_SCSIZE					_T("SCSize")					// 表层套管尺寸
#define	FLD_CASINGPROGRAMINFOTB_INTERMEDIATECASING					_T("IntermediateCasing")					// 中间套管
#define	FLD_CASINGPROGRAMINFOTB_ICDEPTH					_T("ICDepth")					// 中间套管下入井深
#define	FLD_CASINGPROGRAMINFOTB_ICSIZE					_T("ICSize")					// 中间套管尺寸
#define	FLD_CASINGPROGRAMINFOTB_OPENHOLE					_T("OpenHole")					// 裸眼
#define	FLD_CASINGPROGRAMINFOTB_OHDEPTH					_T("OHDepth")					// 裸眼井深
#define	FLD_CASINGPROGRAMINFOTB_OHSIZE					_T("OHSize")					// 裸眼尺寸
#define	FLD_CASINGPROGRAMINFOTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_CASINGPROGRAMINFOTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_CASINGPROGRAMINFOTB_MEMO					_T("Memo")					// 备注
#define	FLD_CASINGPROGRAMINFOTB_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_CASINGPROGRAMINFOTB_CPID_DIGITS				50					// 井深结构编号位数
#define	TV_CASINGPROGRAMINFOTB_CPNAME_DIGITS				50					// 井深结构名称位数
#define	TV_CASINGPROGRAMINFOTB_WELLID_DIGITS				50					// 井编号位数
#define	TV_CASINGPROGRAMINFOTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_CASINGPROGRAMINFOTB
{
 
  char	szCPID[TV_CASINGPROGRAMINFOTB_CPID_DIGITS + 1];							// 井深结构编号
  char	szCPName[TV_CASINGPROGRAMINFOTB_CPNAME_DIGITS + 1];							// 井深结构名称
  char	szWellID[TV_CASINGPROGRAMINFOTB_WELLID_DIGITS + 1];							// 井编号
  int	iSurfaceCasing;							// 表层套管
  int	iSCDepth;							// 表层套管下入井深
  int	iSCSize;							// 表层套管尺寸
  int	iIntermediateCasing;							// 中间套管
  int	iICDepth;							// 中间套管下入井深
  int	iICSize;							// 中间套管尺寸
  int	iOpenHole;							// 裸眼
  int	iOHDepth;							// 裸眼井深
  double	dOHSize;							// 裸眼尺寸
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_CASINGPROGRAMINFOTB_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_CASINGPROGRAMINFOTB;

typedef	TS_CASINGPROGRAMINFOTB FAR*	LPTS_CASINGPROGRAMINFOTB;

#pragma	pack(pop)

#endif // _TCASINGPROGRAMINFOTB_H

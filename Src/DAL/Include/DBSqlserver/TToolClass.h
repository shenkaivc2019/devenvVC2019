//---------------------------------------------------------------------------//
// 文件名: ToolClass.h
// 说明:	工具分类表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020/10/19 22:11:35
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TToolClass.h : DToolClass

#ifndef	_TTOOLCLASS_H
#define	_TTOOLCLASS_H

#define	TID_TOOLCLASS								_T("ToolClass")
#define	OID_TOOLCLASS								_T("")

// Sort No
#define	SORT_TOOLCLASS_PK0				0							// PK:工具分类编号
//#define	SORT_TOOLCLASS_A1							%#%							// A1:

// Colum No
#define	COL_TOOLCLASS_TCID					(short)0						// 工具分类编号
#define	COL_TOOLCLASS_TCNAMECN					(short)1						// 工具分类中文名称
#define	COL_TOOLCLASS_TCNAMEEN					(short)2						// 工具分类英文名称
#define	COL_TOOLCLASS_FTCID					(short)3						// 工具分类父亲编号
#define	COL_TOOLCLASS_TCORDER					(short)4						// 排序
#define	COL_TOOLCLASS_GRADE					(short)5						// 级次
#define	COL_TOOLCLASS_IMGPATH					(short)6						// 工具分类图片
#define	COL_TOOLCLASS_CREATETIME					(short)7						// 创建时间戳
#define	COL_TOOLCLASS_STATUS					(short)8						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_TOOLCLASS_MEMO					(short)9						// 备注
#define	COL_TOOLCLASS_UPDCOUNT					(short)10						// 更新计数

// Colum(Field) Name
#define	FLD_TOOLCLASS_TCID					_T("TCID")					// 工具分类编号
#define	FLD_TOOLCLASS_TCNAMECN					_T("TCNameCN")					// 工具分类中文名称
#define	FLD_TOOLCLASS_TCNAMEEN					_T("TCNameEN")					// 工具分类英文名称
#define	FLD_TOOLCLASS_FTCID					_T("FTCID")					// 工具分类父亲编号
#define	FLD_TOOLCLASS_TCORDER					_T("TCOrder")					// 排序
#define	FLD_TOOLCLASS_GRADE					_T("Grade")					// 级次
#define	FLD_TOOLCLASS_IMGPATH					_T("ImgPath")					// 工具分类图片
#define	FLD_TOOLCLASS_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_TOOLCLASS_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_TOOLCLASS_MEMO					_T("Memo")					// 备注
#define	FLD_TOOLCLASS_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_TOOLCLASS_TCID_DIGITS				10					// 工具分类编号位数
#define	TV_TOOLCLASS_TCNAMECN_DIGITS				50					// 工具分类中文名称位数
#define	TV_TOOLCLASS_TCNAMEEN_DIGITS				50					// 工具分类英文名称位数
#define	TV_TOOLCLASS_FTCID_DIGITS				10					// 工具分类父亲编号位数
#define	TV_TOOLCLASS_IMGPATH_DIGITS				255					// 工具分类图片位数
#define	TV_TOOLCLASS_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_TOOLCLASS
{
 
  char	szTCID[TV_TOOLCLASS_TCID_DIGITS + 1];							// 工具分类编号
  char	szTCNameCN[TV_TOOLCLASS_TCNAMECN_DIGITS + 1];							// 工具分类中文名称
  char	szTCNameEN[TV_TOOLCLASS_TCNAMEEN_DIGITS + 1];							// 工具分类英文名称
  char	szFTCID[TV_TOOLCLASS_FTCID_DIGITS + 1];							// 工具分类父亲编号
  int	iTCOrder;							// 排序
  int	iGrade;							// 级次
  char	szImgPath[TV_TOOLCLASS_IMGPATH_DIGITS + 1];							// 工具分类图片
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_TOOLCLASS_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_TOOLCLASS;

typedef	TS_TOOLCLASS FAR*	LPTS_TOOLCLASS;

#pragma	pack(pop)

#endif // _TTOOLCLASS_H

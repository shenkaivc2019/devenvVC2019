//---------------------------------------------------------------------------//
// 文件名: BHACurveD.h
// 说明:	钻具组合工具的曲线信息子表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020/10/19 22:11:35
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TBHACurveD.h : DBHACurveD

#ifndef	_TBHACURVED_H
#define	_TBHACURVED_H

#define	TID_BHACURVED								_T("BHACurveD")
#define	OID_BHACURVED								_T("")

// Sort No
#define	SORT_BHACURVED_PK0				0							// PK:钻具组合编号
#define	SORT_BHACURVED_PK1				1							// PK:曲线编号(WITS表对应字段)
//#define	SORT_BHACURVED_A1							%#%							// A1:

// Colum No
#define	COL_BHACURVED_BHAID					(short)0						// 钻具组合编号
#define	COL_BHACURVED_CURVID					(short)1						// 曲线编号(WITS表对应字段)
#define	COL_BHACURVED_TOOLID					(short)2						// 工具编号
#define	COL_BHACURVED_TOOLNO					(short)3						// 连接序号（该工具在钻具组合中的位置）方向从钻头开始
#define	COL_BHACURVED_WITSID					(short)4						// 系统对应的WITS号
#define	COL_BHACURVED_RECVWITSID					(short)5						// 从其他系统接收WITS号，如果为空，则和系统对应的WITS号相同，接收后程序要转化成WITSID字段对应的WITS号
#define	COL_BHACURVED_SENDWITSID					(short)6						// 向其他系统发送的WITS号，发送时使用这个WITS号发送，如果为空，则和系统对应的WITS号相同
#define	COL_BHACURVED_MPID					(short)7						// 测量点编号
#define	COL_BHACURVED_UTYPE					(short)8						// 曲线单位类型 对应UnitTB表的UType字段 创建时选择输入
#define	COL_BHACURVED_UID					(short)9						// 数据库中保存的单位ID，如果界面没有设置显示单位，则作为缺省界面的显示单位
#define	COL_BHACURVED_RECVUID					(short)10						// 接收其他程序发送的单位ID，和UID数据结合，用单位表中的系数，换算成接收到的数据保存到DB中
#define	COL_BHACURVED_SENDUID					(short)11						// 对外发送的单位ID，和UID结合，用单位表中的系数，换算成发送的数据
#define	COL_BHACURVED_SENSORTOBIT					(short)12						// 深度偏移(该曲线测点距离钻头的距离) 该值自动计算=MeasPointInfoTB.SensorToolToBot + BHAInfoDTB.ToolBotToBit
#define	COL_BHACURVED_FILTER					(short)13						// 滤波
#define	COL_BHACURVED_SORDER					(short)14						// 排序
#define	COL_BHACURVED_CREATETIME					(short)15						// 创建时间戳
#define	COL_BHACURVED_STATUS					(short)16						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_BHACURVED_MEMO					(short)17						// 备注
#define	COL_BHACURVED_UPDCOUNT					(short)18						// 更新计数

// Colum(Field) Name
#define	FLD_BHACURVED_BHAID					_T("BHAID")					// 钻具组合编号
#define	FLD_BHACURVED_CURVID					_T("CurvID")					// 曲线编号(WITS表对应字段)
#define	FLD_BHACURVED_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_BHACURVED_TOOLNO					_T("ToolNo")					// 连接序号（该工具在钻具组合中的位置）方向从钻头开始
#define	FLD_BHACURVED_WITSID					_T("WITSID")					// 系统对应的WITS号
#define	FLD_BHACURVED_RECVWITSID					_T("RecvWITSID")					// 从其他系统接收WITS号，如果为空，则和系统对应的WITS号相同，接收后程序要转化成WITSID字段对应的WITS号
#define	FLD_BHACURVED_SENDWITSID					_T("SendWITSID")					// 向其他系统发送的WITS号，发送时使用这个WITS号发送，如果为空，则和系统对应的WITS号相同
#define	FLD_BHACURVED_MPID					_T("MPID")					// 测量点编号
#define	FLD_BHACURVED_UTYPE					_T("UType")					// 曲线单位类型 对应UnitTB表的UType字段 创建时选择输入
#define	FLD_BHACURVED_UID					_T("UID")					// 数据库中保存的单位ID，如果界面没有设置显示单位，则作为缺省界面的显示单位
#define	FLD_BHACURVED_RECVUID					_T("RecvUID")					// 接收其他程序发送的单位ID，和UID数据结合，用单位表中的系数，换算成接收到的数据保存到DB中
#define	FLD_BHACURVED_SENDUID					_T("SendUID")					// 对外发送的单位ID，和UID结合，用单位表中的系数，换算成发送的数据
#define	FLD_BHACURVED_SENSORTOBIT					_T("SensorToBit")					// 深度偏移(该曲线测点距离钻头的距离) 该值自动计算=MeasPointInfoTB.SensorToolToBot + BHAInfoDTB.ToolBotToBit
#define	FLD_BHACURVED_FILTER					_T("Filter")					// 滤波
#define	FLD_BHACURVED_SORDER					_T("SOrder")					// 排序
#define	FLD_BHACURVED_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_BHACURVED_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_BHACURVED_MEMO					_T("Memo")					// 备注
#define	FLD_BHACURVED_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_BHACURVED_BHAID_DIGITS				50					// 钻具组合编号位数
#define	TV_BHACURVED_RECVWITSID_DIGITS				10					// 从其他系统接收WITS号，如果为空，则和系统对应的WITS号相同，接收后程序要转化成WITSID字段对应的WITS号位数
#define	TV_BHACURVED_SENDWITSID_DIGITS				10					// 向其他系统发送的WITS号，发送时使用这个WITS号发送，如果为空，则和系统对应的WITS号相同位数
#define	TV_BHACURVED_UTYPE_DIGITS				50					// 曲线单位类型 对应UnitTB表的UType字段 创建时选择输入位数
#define	TV_BHACURVED_FILTER_DIGITS				50					// 滤波位数
#define	TV_BHACURVED_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_BHACURVED
{
 
  char	szBHAID[TV_BHACURVED_BHAID_DIGITS + 1];							// 钻具组合编号
  int	iCurvID;							// 曲线编号(WITS表对应字段)
  int	iToolID;							// 工具编号
  int	iToolNo;							// 连接序号（该工具在钻具组合中的位置）方向从钻头开始
  int	iWITSID;							// 系统对应的WITS号
  char	szRecvWITSID[TV_BHACURVED_RECVWITSID_DIGITS + 1];							// 从其他系统接收WITS号，如果为空，则和系统对应的WITS号相同，接收后程序要转化成WITSID字段对应的WITS号
  char	szSendWITSID[TV_BHACURVED_SENDWITSID_DIGITS + 1];							// 向其他系统发送的WITS号，发送时使用这个WITS号发送，如果为空，则和系统对应的WITS号相同
  int	iMPID;							// 测量点编号
  char	szUType[TV_BHACURVED_UTYPE_DIGITS + 1];							// 曲线单位类型 对应UnitTB表的UType字段 创建时选择输入
  int	iUID;							// 数据库中保存的单位ID，如果界面没有设置显示单位，则作为缺省界面的显示单位
  int	iRecvUID;							// 接收其他程序发送的单位ID，和UID数据结合，用单位表中的系数，换算成接收到的数据保存到DB中
  int	iSendUID;							// 对外发送的单位ID，和UID结合，用单位表中的系数，换算成发送的数据
  double	dSensorToBit;							// 深度偏移(该曲线测点距离钻头的距离) 该值自动计算=MeasPointInfoTB.SensorToolToBot + BHAInfoDTB.ToolBotToBit
  char	szFilter[TV_BHACURVED_FILTER_DIGITS + 1];							// 滤波
  int	iSOrder;							// 排序
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_BHACURVED_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_BHACURVED;

typedef	TS_BHACURVED FAR*	LPTS_BHACURVED;

#pragma	pack(pop)

#endif // _TBHACURVED_H

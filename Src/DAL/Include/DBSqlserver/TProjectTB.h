//---------------------------------------------------------------------------//
// 文件名: ProjectTB.h
// 说明:	工程信息表(用于存储整个测井工程的相关属性信息。)
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020/10/19 22:11:35
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TProjectTB.h : DProjectTB

#ifndef	_TPROJECTTB_H
#define	_TPROJECTTB_H

#define	TID_PROJECTTB								_T("ProjectTB")
#define	OID_PROJECTTB								_T("")

// Sort No
#define	SORT_PROJECTTB_PK0				0							// PK:工程ID 使用全球唯一字符串UUID
//#define	SORT_PROJECTTB_A1							%#%							// A1:

// Colum No
#define	COL_PROJECTTB_PROID					(short)0						// 工程ID 使用全球唯一字符串UUID
#define	COL_PROJECTTB_PRONAME					(short)1						// 工程名
#define	COL_PROJECTTB_PROCLASS					(short)2						// 工程分类
#define	COL_PROJECTTB_DEPART					(short)3						// 所在事业部
#define	COL_PROJECTTB_NORTHREF					(short)4						// 北参考
#define	COL_PROJECTTB_DEEPREF					(short)5						// 系统深度参考
#define	COL_PROJECTTB_OILCOMPANY					(short)6						// 油公司
#define	COL_PROJECTTB_SVRCOMPANY					(short)7						// 服务公司
#define	COL_PROJECTTB_OILCOMPANYREP1					(short)8						// 油公司代表1
#define	COL_PROJECTTB_OILCOMPANYREP2					(short)9						// 油公司代表2
#define	COL_PROJECTTB_SVRCOMPANYREP1					(short)10						// 服务公司代表1
#define	COL_PROJECTTB_SVRCOMPANYREP2					(short)11						// 服务公司代表2
#define	COL_PROJECTTB_CREATETIME					(short)12						// 创建时间戳
#define	COL_PROJECTTB_STATUS					(short)13						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_PROJECTTB_MEMO					(short)14						// 备注
#define	COL_PROJECTTB_UPDCOUNT					(short)15						// 更新计数

// Colum(Field) Name
#define	FLD_PROJECTTB_PROID					_T("ProID")					// 工程ID 使用全球唯一字符串UUID
#define	FLD_PROJECTTB_PRONAME					_T("ProName")					// 工程名
#define	FLD_PROJECTTB_PROCLASS					_T("ProClass")					// 工程分类
#define	FLD_PROJECTTB_DEPART					_T("Depart")					// 所在事业部
#define	FLD_PROJECTTB_NORTHREF					_T("NorthRef")					// 北参考
#define	FLD_PROJECTTB_DEEPREF					_T("DeepRef")					// 系统深度参考
#define	FLD_PROJECTTB_OILCOMPANY					_T("OilCompany")					// 油公司
#define	FLD_PROJECTTB_SVRCOMPANY					_T("SvrCompany")					// 服务公司
#define	FLD_PROJECTTB_OILCOMPANYREP1					_T("OilCompanyRep1")					// 油公司代表1
#define	FLD_PROJECTTB_OILCOMPANYREP2					_T("OilCompanyRep2")					// 油公司代表2
#define	FLD_PROJECTTB_SVRCOMPANYREP1					_T("SvrCompanyRep1")					// 服务公司代表1
#define	FLD_PROJECTTB_SVRCOMPANYREP2					_T("SvrCompanyRep2")					// 服务公司代表2
#define	FLD_PROJECTTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_PROJECTTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_PROJECTTB_MEMO					_T("Memo")					// 备注
#define	FLD_PROJECTTB_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_PROJECTTB_PROID_DIGITS				50					// 工程ID 使用全球唯一字符串UUID位数
#define	TV_PROJECTTB_PRONAME_DIGITS				50					// 工程名位数
#define	TV_PROJECTTB_PROCLASS_DIGITS				50					// 工程分类位数
#define	TV_PROJECTTB_DEPART_DIGITS				200					// 所在事业部位数
#define	TV_PROJECTTB_NORTHREF_DIGITS				200					// 北参考位数
#define	TV_PROJECTTB_DEEPREF_DIGITS				200					// 系统深度参考位数
#define	TV_PROJECTTB_OILCOMPANY_DIGITS				50					// 油公司位数
#define	TV_PROJECTTB_SVRCOMPANY_DIGITS				50					// 服务公司位数
#define	TV_PROJECTTB_OILCOMPANYREP1_DIGITS				50					// 油公司代表1位数
#define	TV_PROJECTTB_OILCOMPANYREP2_DIGITS				50					// 油公司代表2位数
#define	TV_PROJECTTB_SVRCOMPANYREP1_DIGITS				50					// 服务公司代表1位数
#define	TV_PROJECTTB_SVRCOMPANYREP2_DIGITS				50					// 服务公司代表2位数
#define	TV_PROJECTTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_PROJECTTB
{
 
  char	szProID[TV_PROJECTTB_PROID_DIGITS + 1];							// 工程ID 使用全球唯一字符串UUID
  char	szProName[TV_PROJECTTB_PRONAME_DIGITS + 1];							// 工程名
  char	szProClass[TV_PROJECTTB_PROCLASS_DIGITS + 1];							// 工程分类
  char	szDepart[TV_PROJECTTB_DEPART_DIGITS + 1];							// 所在事业部
  char	szNorthRef[TV_PROJECTTB_NORTHREF_DIGITS + 1];							// 北参考
  char	szDeepRef[TV_PROJECTTB_DEEPREF_DIGITS + 1];							// 系统深度参考
  char	szOilCompany[TV_PROJECTTB_OILCOMPANY_DIGITS + 1];							// 油公司
  char	szSvrCompany[TV_PROJECTTB_SVRCOMPANY_DIGITS + 1];							// 服务公司
  char	szOilCompanyRep1[TV_PROJECTTB_OILCOMPANYREP1_DIGITS + 1];							// 油公司代表1
  char	szOilCompanyRep2[TV_PROJECTTB_OILCOMPANYREP2_DIGITS + 1];							// 油公司代表2
  char	szSvrCompanyRep1[TV_PROJECTTB_SVRCOMPANYREP1_DIGITS + 1];							// 服务公司代表1
  char	szSvrCompanyRep2[TV_PROJECTTB_SVRCOMPANYREP2_DIGITS + 1];							// 服务公司代表2
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_PROJECTTB_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_PROJECTTB;

typedef	TS_PROJECTTB FAR*	LPTS_PROJECTTB;

#pragma	pack(pop)

#endif // _TPROJECTTB_H

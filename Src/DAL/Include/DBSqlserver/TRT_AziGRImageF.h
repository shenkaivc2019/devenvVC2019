//---------------------------------------------------------------------------//
// 文件名: RT_AziGRImageF.h
// 说明:	实时方位伽马滤波成像数据表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/10/9 15:57:06
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TRT_AziGRImageF.h : DRT_AziGRImageF

#ifndef	_TRT_AZIGRIMAGEF_H
#define	_TRT_AZIGRIMAGEF_H

#define	TID_RT_AZIGRIMAGEF								_T("RT_AziGRImageF")
#define	OID_RT_AZIGRIMAGEF								_T("")

// Sort No
#define	SORT_RT_AZIGRIMAGEF_PK0				0							// PK:数据编号
//#define	SORT_RT_AZIGRIMAGEF_A1							%#%							// A1:

// Colum No
#define	COL_RT_AZIGRIMAGEF_DATAID					(short)0						// 数据编号
#define	COL_RT_AZIGRIMAGEF_RUNID					(short)1						// 趟钻编号
#define	COL_RT_AZIGRIMAGEF_TDATETIME					(short)2						// 时间（精确到秒）
#define	COL_RT_AZIGRIMAGEF_MILLITIME					(short)3						// 毫秒
#define	COL_RT_AZIGRIMAGEF_TOOLID					(short)4						// 工具编号
#define	COL_RT_AZIGRIMAGEF_MDEPTH					(short)5						// 方位伽马测量点井深
#define	COL_RT_AZIGRIMAGEF_VDEPTH					(short)6						// 方位伽马测量点垂深
#define	COL_RT_AZIGRIMAGEF_AGRUDATAID					(short)7						// 4扇区扇区上伽马计数数据编号
#define	COL_RT_AZIGRIMAGEF_AGRDDATAID					(short)8						// 4扇区扇区下伽马计数数据编号
#define	COL_RT_AZIGRIMAGEF_AGRLDATAID					(short)9						// 4扇区扇区左伽马计数数据编号
#define	COL_RT_AZIGRIMAGEF_AGRRDATAID					(short)10						// 4扇区扇区右伽马计数数据编号
#define	COL_RT_AZIGRIMAGEF_AGRU					(short)11						// 4扇区扇区上伽马计数
#define	COL_RT_AZIGRIMAGEF_AGRD					(short)12						// 4扇区扇区下伽马计数
#define	COL_RT_AZIGRIMAGEF_AGRL					(short)13						// 4扇区扇区左伽马计数
#define	COL_RT_AZIGRIMAGEF_AGRR					(short)14						// 4扇区扇区右伽马计数
#define	COL_RT_AZIGRIMAGEF_CONF					(short)15						// 可信度
#define	COL_RT_AZIGRIMAGEF_BAD					(short)16						// 是否坏点:No|0.否;Yes|1.是
#define	COL_RT_AZIGRIMAGEF_DRILLACTIV					(short)17						// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	COL_RT_AZIGRIMAGEF_SLIDING					(short)18						// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	COL_RT_AZIGRIMAGEF_RELOG					(short)19						// 是否复测 :Yes|0.否;No|1.是
#define	COL_RT_AZIGRIMAGEF_LAS					(short)20						// 是否Las :No|0.否;Yes|1.是
#define	COL_RT_AZIGRIMAGEF_CREATETIME					(short)21						// 创建时间戳
#define	COL_RT_AZIGRIMAGEF_UPDTIME					(short)22						// 最后一次修改时间戳
#define	COL_RT_AZIGRIMAGEF_STATUS					(short)23						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_RT_AZIGRIMAGEF_MEMO					(short)24						// 备注

// Colum(Field) Name
#define	FLD_RT_AZIGRIMAGEF_DATAID					_T("DataID")					// 数据编号
#define	FLD_RT_AZIGRIMAGEF_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_RT_AZIGRIMAGEF_TDATETIME					_T("TDateTime")					// 时间（精确到秒）
#define	FLD_RT_AZIGRIMAGEF_MILLITIME					_T("Millitime")					// 毫秒
#define	FLD_RT_AZIGRIMAGEF_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_RT_AZIGRIMAGEF_MDEPTH					_T("MDepth")					// 方位伽马测量点井深
#define	FLD_RT_AZIGRIMAGEF_VDEPTH					_T("VDepth")					// 方位伽马测量点垂深
#define	FLD_RT_AZIGRIMAGEF_AGRUDATAID					_T("AGRUDataID")					// 4扇区扇区上伽马计数数据编号
#define	FLD_RT_AZIGRIMAGEF_AGRDDATAID					_T("AGRDDataID")					// 4扇区扇区下伽马计数数据编号
#define	FLD_RT_AZIGRIMAGEF_AGRLDATAID					_T("AGRLDataID")					// 4扇区扇区左伽马计数数据编号
#define	FLD_RT_AZIGRIMAGEF_AGRRDATAID					_T("AGRRDataID")					// 4扇区扇区右伽马计数数据编号
#define	FLD_RT_AZIGRIMAGEF_AGRU					_T("AGRU")					// 4扇区扇区上伽马计数
#define	FLD_RT_AZIGRIMAGEF_AGRD					_T("AGRD")					// 4扇区扇区下伽马计数
#define	FLD_RT_AZIGRIMAGEF_AGRL					_T("AGRL")					// 4扇区扇区左伽马计数
#define	FLD_RT_AZIGRIMAGEF_AGRR					_T("AGRR")					// 4扇区扇区右伽马计数
#define	FLD_RT_AZIGRIMAGEF_CONF					_T("Conf")					// 可信度
#define	FLD_RT_AZIGRIMAGEF_BAD					_T("Bad")					// 是否坏点:No|0.否;Yes|1.是
#define	FLD_RT_AZIGRIMAGEF_DRILLACTIV					_T("DrillActiv")					// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	FLD_RT_AZIGRIMAGEF_SLIDING					_T("Sliding")					// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	FLD_RT_AZIGRIMAGEF_RELOG					_T("ReLog")					// 是否复测 :Yes|0.否;No|1.是
#define	FLD_RT_AZIGRIMAGEF_LAS					_T("Las")					// 是否Las :No|0.否;Yes|1.是
#define	FLD_RT_AZIGRIMAGEF_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_RT_AZIGRIMAGEF_UPDTIME					_T("UpdTime")					// 最后一次修改时间戳
#define	FLD_RT_AZIGRIMAGEF_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_RT_AZIGRIMAGEF_MEMO					_T("Memo")					// 备注

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_RT_AZIGRIMAGEF_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_RT_AZIGRIMAGEF_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_RT_AZIGRIMAGEF
{
 
  long	lDataID;							// 数据编号
  char	szRunID[TV_RT_AZIGRIMAGEF_RUNID_DIGITS + 1];							// 趟钻编号
  time_t	lTDateTime;							// 时间（精确到秒）
  int	iMillitime;							// 毫秒
  int	iToolID;							// 工具编号
  long	lMDepth;							// 方位伽马测量点井深
  long	lVDepth;							// 方位伽马测量点垂深
  long	lAGRUDataID;							// 4扇区扇区上伽马计数数据编号
  long	lAGRDDataID;							// 4扇区扇区下伽马计数数据编号
  long	lAGRLDataID;							// 4扇区扇区左伽马计数数据编号
  long	lAGRRDataID;							// 4扇区扇区右伽马计数数据编号
  float	fAGRU;							// 4扇区扇区上伽马计数
  float	fAGRD;							// 4扇区扇区下伽马计数
  float	fAGRL;							// 4扇区扇区左伽马计数
  float	fAGRR;							// 4扇区扇区右伽马计数
  short	nConf;							// 可信度
  short	nBad;							// 是否坏点:No|0.否;Yes|1.是
  short	nDrillActiv;							// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
  short	nSliding;							// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
  short	nReLog;							// 是否复测 :Yes|0.否;No|1.是
  short	nLas;							// 是否Las :No|0.否;Yes|1.是
  time_t	lCreateTime;							// 创建时间戳
  time_t	lUpdTime;							// 最后一次修改时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_RT_AZIGRIMAGEF_MEMO_DIGITS + 1];							// 备注
} TS_RT_AZIGRIMAGEF;

typedef	TS_RT_AZIGRIMAGEF FAR*	LPTS_RT_AZIGRIMAGEF;

#pragma	pack(pop)

#endif // _TRT_AZIGRIMAGEF_H

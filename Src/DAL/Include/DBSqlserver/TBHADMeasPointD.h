//---------------------------------------------------------------------------//
// 文件名: BHADMeasPointD.h
// 说明:	钻具组合工具的测量点信息子表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020/10/19 22:11:35
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TBHADMeasPointD.h : DBHADMeasPointD

#ifndef	_TBHADMEASPOINTD_H
#define	_TBHADMEASPOINTD_H

#define	TID_BHADMEASPOINTD								_T("BHADMeasPointD")
#define	OID_BHADMEASPOINTD								_T("")

// Sort No
#define	SORT_BHADMEASPOINTD_PK0				0							// PK:钻具组合编号
#define	SORT_BHADMEASPOINTD_PK1				1							// PK:工具编号
#define	SORT_BHADMEASPOINTD_PK2				2							// PK:连接序号（该工具在钻具组合中的位置）方向从钻头开始
#define	SORT_BHADMEASPOINTD_PK3				3							// PK:测量点编号
//#define	SORT_BHADMEASPOINTD_A1							%#%							// A1:

// Colum No
#define	COL_BHADMEASPOINTD_BHAID					(short)0						// 钻具组合编号
#define	COL_BHADMEASPOINTD_TOOLID					(short)1						// 工具编号
#define	COL_BHADMEASPOINTD_TOOLNO					(short)2						// 连接序号（该工具在钻具组合中的位置）方向从钻头开始
#define	COL_BHADMEASPOINTD_MPID					(short)3						// 测量点编号
#define	COL_BHADMEASPOINTD_SENSORTOOLTOBOT					(short)4						// 深度偏移(该测点距离所在工具底部距离)
#define	COL_BHADMEASPOINTD_POSITION					(short)5						// 测点位置:Default|0.缺省无意义;Center|1.居中伽马; Sidewall|2.侧壁伽马
#define	COL_BHADMEASPOINTD_SORDER					(short)6						// 排序
#define	COL_BHADMEASPOINTD_CREATETIME					(short)7						// 创建时间戳
#define	COL_BHADMEASPOINTD_STATUS					(short)8						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_BHADMEASPOINTD_MEMO					(short)9						// 备注
#define	COL_BHADMEASPOINTD_UPDCOUNT					(short)10						// 更新计数

// Colum(Field) Name
#define	FLD_BHADMEASPOINTD_BHAID					_T("BHAID")					// 钻具组合编号
#define	FLD_BHADMEASPOINTD_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_BHADMEASPOINTD_TOOLNO					_T("ToolNo")					// 连接序号（该工具在钻具组合中的位置）方向从钻头开始
#define	FLD_BHADMEASPOINTD_MPID					_T("MPID")					// 测量点编号
#define	FLD_BHADMEASPOINTD_SENSORTOOLTOBOT					_T("SensorToolToBot")					// 深度偏移(该测点距离所在工具底部距离)
#define	FLD_BHADMEASPOINTD_POSITION					_T("Position")					// 测点位置:Default|0.缺省无意义;Center|1.居中伽马; Sidewall|2.侧壁伽马
#define	FLD_BHADMEASPOINTD_SORDER					_T("SOrder")					// 排序
#define	FLD_BHADMEASPOINTD_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_BHADMEASPOINTD_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_BHADMEASPOINTD_MEMO					_T("Memo")					// 备注
#define	FLD_BHADMEASPOINTD_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_BHADMEASPOINTD_BHAID_DIGITS				50					// 钻具组合编号位数
#define	TV_BHADMEASPOINTD_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_BHADMEASPOINTD
{
 
  char	szBHAID[TV_BHADMEASPOINTD_BHAID_DIGITS + 1];							// 钻具组合编号
  int	iToolID;							// 工具编号
  int	iToolNo;							// 连接序号（该工具在钻具组合中的位置）方向从钻头开始
  int	iMPID;							// 测量点编号
  double	dSensorToolToBot;							// 深度偏移(该测点距离所在工具底部距离)
  short	nPosition;							// 测点位置:Default|0.缺省无意义;Center|1.居中伽马; Sidewall|2.侧壁伽马
  int	iSOrder;							// 排序
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_BHADMEASPOINTD_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_BHADMEASPOINTD;

typedef	TS_BHADMEASPOINTD FAR*	LPTS_BHADMEASPOINTD;

#pragma	pack(pop)

#endif // _TBHADMEASPOINTD_H

//---------------------------------------------------------------------------//
// 文件名: RestoreInfo.h
// 说明:	恢复数据库实时信息表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2023/12/7 15:00:09
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TRestoreInfo.h : DRestoreInfo

#ifndef	_TRESTOREINFO_H
#define	_TRESTOREINFO_H

#define	TID_RESTOREINFO								_T("RestoreInfo")
#define	OID_RESTOREINFO								_T("")

// Sort No
#define	SORT_RESTOREINFO_PK0				0							// PK:
//#define	SORT_RESTOREINFO_A1							%#%							// A1:

// Colum No
#define	COL_RESTOREINFO_SESSIONID					(short)0						// 
#define	COL_RESTOREINFO_COMMAND					(short)1						// 
#define	COL_RESTOREINFO_PERCENTAGECOMPLETE					(short)2						// 
#define	COL_RESTOREINFO_ETACOMPLETIONTIME					(short)3						// 
#define	COL_RESTOREINFO_ELAPSEDMINUTES					(short)4						// 
#define	COL_RESTOREINFO_DATABASE					(short)5						// 
#define	COL_RESTOREINFO_COMMANDTYPE					(short)6						// 
#define	COL_RESTOREINFO_STARTTIME					(short)7						// 
#define	COL_RESTOREINFO_CREATETIME					(short)8						// 
#define	COL_RESTOREINFO_UPDTIME					(short)9						// 
#define	COL_RESTOREINFO_STATUS					(short)10						// 
#define	COL_RESTOREINFO_MEMO					(short)11						// 
#define	COL_RESTOREINFO_UPDCOUNT					(short)12						// 

// Colum(Field) Name
#define	FLD_RESTOREINFO_SESSIONID					_T("SessionID")					// 
#define	FLD_RESTOREINFO_COMMAND					_T("Command")					// 
#define	FLD_RESTOREINFO_PERCENTAGECOMPLETE					_T("PercentageComplete")					// 
#define	FLD_RESTOREINFO_ETACOMPLETIONTIME					_T("ETACompletionTime")					// 
#define	FLD_RESTOREINFO_ELAPSEDMINUTES					_T("ElapsedMinutes")					// 
#define	FLD_RESTOREINFO_DATABASE					_T("Database")					// 
#define	FLD_RESTOREINFO_COMMANDTYPE					_T("CommandType")					// 
#define	FLD_RESTOREINFO_STARTTIME					_T("StartTime")					// 
#define	FLD_RESTOREINFO_CREATETIME					_T("CreateTime")					// 
#define	FLD_RESTOREINFO_UPDTIME					_T("UpdTime")					// 
#define	FLD_RESTOREINFO_STATUS					_T("Status")					// 
#define	FLD_RESTOREINFO_MEMO					_T("Memo")					// 
#define	FLD_RESTOREINFO_UPDCOUNT					_T("UpdCount")					// 

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_RESTOREINFO_COMMAND_DIGITS				50					// 位数
#define	TV_RESTOREINFO_DATABASE_DIGITS				50					// 位数
#define	TV_RESTOREINFO_COMMANDTYPE_DIGITS				50					// 位数
#define	TV_RESTOREINFO_MEMO_DIGITS				100					// 位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_RESTOREINFO
{
 
  int	iSessionID;							// 
  char	szCommand[TV_RESTOREINFO_COMMAND_DIGITS + 1];							// 
  float	fPercentageComplete;							// 
  time_t	lETACompletionTime;							// 
  float	fElapsedMinutes;							// 
  char	szDatabase[TV_RESTOREINFO_DATABASE_DIGITS + 1];							// 
  char	szCommandType[TV_RESTOREINFO_COMMANDTYPE_DIGITS + 1];							// 
  time_t	lStartTime;							// 
  time_t	lCreateTime;							// 
  time_t	lUpdTime;							// 
  short	nStatus;							// 
  char	szMemo[TV_RESTOREINFO_MEMO_DIGITS + 1];							// 
  int	iUpdCount;							// 
} TS_RESTOREINFO;

typedef	TS_RESTOREINFO FAR*	LPTS_RESTOREINFO;

#pragma	pack(pop)

#endif // _TRESTOREINFO_H

//---------------------------------------------------------------------------//
// 文件名: ME_NBSub.h
// 说明:	仪器内存数据表(NBGRSub伽马）
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2023/9/18 14:06:24
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TME_NBSub.h : DME_NBSub

#ifndef	_TME_NBSUB_H
#define	_TME_NBSUB_H

#define	TID_ME_NBSUB								_T("ME_NBSub")
#define	OID_ME_NBSUB								_T("")

// Sort No
#define	SORT_ME_NBSUB_PK0				0							// PK:自增字段
//#define	SORT_ME_NBSUB_A1							%#%							// A1:

// Colum No
#define	COL_ME_NBSUB_DATAID					(short)0						// 自增字段
#define	COL_ME_NBSUB_RUNID					(short)1						// 趟钻编号
#define	COL_ME_NBSUB_TDATETIME					(short)2						// 时间（精确到秒）
#define	COL_ME_NBSUB_MILLITIME					(short)3						// 毫秒
#define	COL_ME_NBSUB_TOOLID					(short)4						// 工具编号
#define	COL_ME_NBSUB_MDEPTH					(short)5						// 自然伽马测量深度 
#define	COL_ME_NBSUB_VDEPTH					(short)6						// 自然伽马测量深度 
#define	COL_ME_NBSUB_GRS0					(short)7						// 16扇0区扇区伽马值
#define	COL_ME_NBSUB_GRS1					(short)8						// 16扇1区扇区伽马值
#define	COL_ME_NBSUB_GRS2					(short)9						// 16扇2区扇区伽马值
#define	COL_ME_NBSUB_GRS3					(short)10						// 16扇3区扇区伽马值
#define	COL_ME_NBSUB_GRS4					(short)11						// 16扇4区扇区伽马值
#define	COL_ME_NBSUB_GRS5					(short)12						// 16扇5区扇区伽马值
#define	COL_ME_NBSUB_GRS6					(short)13						// 16扇6区扇区伽马值
#define	COL_ME_NBSUB_GRS7					(short)14						// 16扇7区扇区伽马值
#define	COL_ME_NBSUB_GRS8					(short)15						// 16扇8区扇区伽马值
#define	COL_ME_NBSUB_GRS9					(short)16						// 16扇9区扇区伽马值
#define	COL_ME_NBSUB_GRS10					(short)17						// 16扇10区扇区伽马值
#define	COL_ME_NBSUB_GRS11					(short)18						// 16扇11区扇区伽马值
#define	COL_ME_NBSUB_GRS12					(short)19						// 16扇12区扇区伽马值
#define	COL_ME_NBSUB_GRS13					(short)20						// 16扇13区扇区伽马值
#define	COL_ME_NBSUB_GRS14					(short)21						// 16扇14区扇区伽马值
#define	COL_ME_NBSUB_GRS15					(short)22						// 16扇15区扇区伽马值
#define	COL_ME_NBSUB_GRS0A					(short)23						// 16扇区0扇区伽马值（API计算系数校正后数据）
#define	COL_ME_NBSUB_GRS0C					(short)24						// 16扇区0扇区伽马值（API计算环境校正后数据）
#define	COL_ME_NBSUB_GRS1A					(short)25						// 16扇区1扇区伽马值（API计算系数校正后数据）
#define	COL_ME_NBSUB_GRS1C					(short)26						// 16扇区1扇区伽马值（API计算环境校正后数据）
#define	COL_ME_NBSUB_GRS2A					(short)27						// 16扇区2扇区伽马值（API计算系数校正后数据）
#define	COL_ME_NBSUB_GRS2C					(short)28						// 16扇区2扇区伽马值（API计算环境校正后数据）
#define	COL_ME_NBSUB_GRS3A					(short)29						// 16扇区3扇区伽马值（API计算系数校正后数据）
#define	COL_ME_NBSUB_GRS3C					(short)30						// 16扇区3扇区伽马值（API计算环境校正后数据）
#define	COL_ME_NBSUB_GRS4A					(short)31						// 16扇区4扇区伽马值（API计算系数校正后数据）
#define	COL_ME_NBSUB_GRS4C					(short)32						// 16扇区4扇区伽马值（API计算环境校正后数据）
#define	COL_ME_NBSUB_GRS5A					(short)33						// 16扇区5扇区伽马值（API计算系数校正后数据）
#define	COL_ME_NBSUB_GRS5C					(short)34						// 16扇区5扇区伽马值（API计算环境校正后数据）
#define	COL_ME_NBSUB_GRS6A					(short)35						// 16扇区6扇区伽马值（API计算系数校正后数据）
#define	COL_ME_NBSUB_GRS6C					(short)36						// 16扇区6扇区伽马值（API计算环境校正后数据）
#define	COL_ME_NBSUB_GRS7A					(short)37						// 16扇区7扇区伽马值（API计算系数校正后数据）
#define	COL_ME_NBSUB_GRS7C					(short)38						// 16扇区7扇区伽马值（API计算环境校正后数据）
#define	COL_ME_NBSUB_GRS8A					(short)39						// 16扇区8扇区伽马值（API计算系数校正后数据）
#define	COL_ME_NBSUB_GRS8C					(short)40						// 16扇区8扇区伽马值（API计算环境校正后数据）
#define	COL_ME_NBSUB_GRS9A					(short)41						// 16扇区9扇区伽马值（API计算系数校正后数据）
#define	COL_ME_NBSUB_GRS9C					(short)42						// 16扇区9扇区伽马值（API计算环境校正后数据）
#define	COL_ME_NBSUB_GRS10A					(short)43						// 16扇区10扇区伽马值（API计算系数校正后数据）
#define	COL_ME_NBSUB_GRS10C					(short)44						// 16扇区10扇区伽马值（API计算环境校正后数据）
#define	COL_ME_NBSUB_GRS11A					(short)45						// 16扇区11扇区伽马值（API计算系数校正后数据）
#define	COL_ME_NBSUB_GRS11C					(short)46						// 16扇区11扇区伽马值（API计算环境校正后数据）
#define	COL_ME_NBSUB_GRS12A					(short)47						// 16扇区12扇区伽马值（API计算系数校正后数据）
#define	COL_ME_NBSUB_GRS12C					(short)48						// 16扇区12扇区伽马值（API计算环境校正后数据）
#define	COL_ME_NBSUB_GRS13A					(short)49						// 16扇区13扇区伽马值（API计算系数校正后数据）
#define	COL_ME_NBSUB_GRS13C					(short)50						// 16扇区13扇区伽马值（API计算环境校正后数据）
#define	COL_ME_NBSUB_GRS14A					(short)51						// 16扇区14扇区伽马值（API计算系数校正后数据）
#define	COL_ME_NBSUB_GRS14C					(short)52						// 16扇区14扇区伽马值（API计算环境校正后数据）
#define	COL_ME_NBSUB_GRS15A					(short)53						// 16扇区15扇区伽马值（API计算系数校正后数据）
#define	COL_ME_NBSUB_GRS15C					(short)54						// 16扇区15扇区伽马值（API计算环境校正后数据）
#define	COL_ME_NBSUB_GRS0D					(short)55						// 16扇区0扇区伽马值（API暗电流校正后数据）
#define	COL_ME_NBSUB_GRS0CG					(short)56						// 16扇区0扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_NBSUB_GRS1D					(short)57						// 16扇区1扇区伽马值（API暗电流校正后数据）
#define	COL_ME_NBSUB_GRS1CG					(short)58						// 16扇区1扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_NBSUB_GRS2D					(short)59						// 16扇区2扇区伽马值（API暗电流校正后数据）
#define	COL_ME_NBSUB_GRS2CG					(short)60						// 16扇区2扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_NBSUB_GRS3D					(short)61						// 16扇区3扇区伽马值（API暗电流校正后数据）
#define	COL_ME_NBSUB_GRS3CG					(short)62						// 16扇区3扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_NBSUB_GRS4D					(short)63						// 16扇区4扇区伽马值（API暗电流校正后数据）
#define	COL_ME_NBSUB_GRS4CG					(short)64						// 16扇区4扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_NBSUB_GRS5D					(short)65						// 16扇区5扇区伽马值（API暗电流校正后数据）
#define	COL_ME_NBSUB_GRS5CG					(short)66						// 16扇区5扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_NBSUB_GRS6D					(short)67						// 16扇区6扇区伽马值（API暗电流校正后数据）
#define	COL_ME_NBSUB_GRS6CG					(short)68						// 16扇区6扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_NBSUB_GRS7D					(short)69						// 16扇区7扇区伽马值（API暗电流校正后数据）
#define	COL_ME_NBSUB_GRS7CG					(short)70						// 16扇区7扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_NBSUB_GRS8D					(short)71						// 16扇区8扇区伽马值（API暗电流校正后数据）
#define	COL_ME_NBSUB_GRS8CG					(short)72						// 16扇区8扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_NBSUB_GRS9D					(short)73						// 16扇区9扇区伽马值（API暗电流校正后数据）
#define	COL_ME_NBSUB_GRS9CG					(short)74						// 16扇区9扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_NBSUB_GRS10D					(short)75						// 16扇区10扇区伽马值（API暗电流校正后数据）
#define	COL_ME_NBSUB_GRS10CG					(short)76						// 16扇区10扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_NBSUB_GRS11D					(short)77						// 16扇区11扇区伽马值（API暗电流校正后数据）
#define	COL_ME_NBSUB_GRS11CG					(short)78						// 16扇区11扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_NBSUB_GRS12D					(short)79						// 16扇区12扇区伽马值（API暗电流校正后数据）
#define	COL_ME_NBSUB_GRS12CG					(short)80						// 16扇区12扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_NBSUB_GRS13D					(short)81						// 16扇区13扇区伽马值（API暗电流校正后数据）
#define	COL_ME_NBSUB_GRS13CG					(short)82						// 16扇区13扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_NBSUB_GRS14D					(short)83						// 16扇区14扇区伽马值（API暗电流校正后数据）
#define	COL_ME_NBSUB_GRS14CG					(short)84						// 16扇区14扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_NBSUB_GRS15D					(short)85						// 16扇区15扇区伽马值（API暗电流校正后数据）
#define	COL_ME_NBSUB_GRS15CG					(short)86						// 16扇区15扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_NBSUB_GR					(short)87						// 总伽马RAW
#define	COL_ME_NBSUB_GRA					(short)88						// 总伽马经过API计算系数校正后数据
#define	COL_ME_NBSUB_GRC					(short)89						// 总伽马经过API计算环境校正后数据
#define	COL_ME_NBSUB_GRU					(short)90						// 16扇上伽马值
#define	COL_ME_NBSUB_GRUA					(short)91						// 16扇上伽马值（API计算系数校正后数据）
#define	COL_ME_NBSUB_GRUC					(short)92						// 上伽马经过API计算环境校正后数据
#define	COL_ME_NBSUB_GRD					(short)93						// 16扇下伽马值
#define	COL_ME_NBSUB_GRDA					(short)94						// 16扇下伽马值（API计算系数校正后数据）
#define	COL_ME_NBSUB_GRDC					(short)95						// 下伽马经过API计算环境校正后数据
#define	COL_ME_NBSUB_GRL					(short)96						// 16扇左伽马值
#define	COL_ME_NBSUB_GRLA					(short)97						// 16扇左伽马值（API计算系数校正后数据）
#define	COL_ME_NBSUB_GRLC					(short)98						// 左伽马经过API计算环境校正后数据
#define	COL_ME_NBSUB_GRR					(short)99						// 16扇右伽马值
#define	COL_ME_NBSUB_GRRA					(short)100						// 16扇右伽马值（API计算系数校正后数据）
#define	COL_ME_NBSUB_GRRC					(short)101						// 右伽马经过API计算环境校正后数据
#define	COL_ME_NBSUB_INC					(short)102						// 井斜
#define	COL_ME_NBSUB_CONF					(short)103						// 旋转状态:No|0.否;Yes|1.是
#define	COL_ME_NBSUB_BAD					(short)104						// 是否坏点:No|0.否;Yes|1.是
#define	COL_ME_NBSUB_DRILLACTIV					(short)105						// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	COL_ME_NBSUB_SLIDING					(short)106						// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	COL_ME_NBSUB_RELOG					(short)107						// 是否复测 :Yes|0.否;No|1.是
#define	COL_ME_NBSUB_LAS					(short)108						// 是否Las :Yes|0.否;No|1.是
#define	COL_ME_NBSUB_CREATETIME					(short)109						// 创建时间戳
#define	COL_ME_NBSUB_UPDTIME					(short)110						// 最后一次修改时间戳
#define	COL_ME_NBSUB_STATUS					(short)111						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_ME_NBSUB_MEMO					(short)112						// 备注

// Colum(Field) Name
#define	FLD_ME_NBSUB_DATAID					_T("DataID")					// 自增字段
#define	FLD_ME_NBSUB_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_ME_NBSUB_TDATETIME					_T("TDateTime")					// 时间（精确到秒）
#define	FLD_ME_NBSUB_MILLITIME					_T("Millitime")					// 毫秒
#define	FLD_ME_NBSUB_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_ME_NBSUB_MDEPTH					_T("MDepth")					// 自然伽马测量深度 
#define	FLD_ME_NBSUB_VDEPTH					_T("VDepth")					// 自然伽马测量深度 
#define	FLD_ME_NBSUB_GRS0					_T("GRS0")					// 16扇0区扇区伽马值
#define	FLD_ME_NBSUB_GRS1					_T("GRS1")					// 16扇1区扇区伽马值
#define	FLD_ME_NBSUB_GRS2					_T("GRS2")					// 16扇2区扇区伽马值
#define	FLD_ME_NBSUB_GRS3					_T("GRS3")					// 16扇3区扇区伽马值
#define	FLD_ME_NBSUB_GRS4					_T("GRS4")					// 16扇4区扇区伽马值
#define	FLD_ME_NBSUB_GRS5					_T("GRS5")					// 16扇5区扇区伽马值
#define	FLD_ME_NBSUB_GRS6					_T("GRS6")					// 16扇6区扇区伽马值
#define	FLD_ME_NBSUB_GRS7					_T("GRS7")					// 16扇7区扇区伽马值
#define	FLD_ME_NBSUB_GRS8					_T("GRS8")					// 16扇8区扇区伽马值
#define	FLD_ME_NBSUB_GRS9					_T("GRS9")					// 16扇9区扇区伽马值
#define	FLD_ME_NBSUB_GRS10					_T("GRS10")					// 16扇10区扇区伽马值
#define	FLD_ME_NBSUB_GRS11					_T("GRS11")					// 16扇11区扇区伽马值
#define	FLD_ME_NBSUB_GRS12					_T("GRS12")					// 16扇12区扇区伽马值
#define	FLD_ME_NBSUB_GRS13					_T("GRS13")					// 16扇13区扇区伽马值
#define	FLD_ME_NBSUB_GRS14					_T("GRS14")					// 16扇14区扇区伽马值
#define	FLD_ME_NBSUB_GRS15					_T("GRS15")					// 16扇15区扇区伽马值
#define	FLD_ME_NBSUB_GRS0A					_T("GRS0A")					// 16扇区0扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_NBSUB_GRS0C					_T("GRS0C")					// 16扇区0扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_NBSUB_GRS1A					_T("GRS1A")					// 16扇区1扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_NBSUB_GRS1C					_T("GRS1C")					// 16扇区1扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_NBSUB_GRS2A					_T("GRS2A")					// 16扇区2扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_NBSUB_GRS2C					_T("GRS2C")					// 16扇区2扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_NBSUB_GRS3A					_T("GRS3A")					// 16扇区3扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_NBSUB_GRS3C					_T("GRS3C")					// 16扇区3扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_NBSUB_GRS4A					_T("GRS4A")					// 16扇区4扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_NBSUB_GRS4C					_T("GRS4C")					// 16扇区4扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_NBSUB_GRS5A					_T("GRS5A")					// 16扇区5扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_NBSUB_GRS5C					_T("GRS5C")					// 16扇区5扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_NBSUB_GRS6A					_T("GRS6A")					// 16扇区6扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_NBSUB_GRS6C					_T("GRS6C")					// 16扇区6扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_NBSUB_GRS7A					_T("GRS7A")					// 16扇区7扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_NBSUB_GRS7C					_T("GRS7C")					// 16扇区7扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_NBSUB_GRS8A					_T("GRS8A")					// 16扇区8扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_NBSUB_GRS8C					_T("GRS8C")					// 16扇区8扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_NBSUB_GRS9A					_T("GRS9A")					// 16扇区9扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_NBSUB_GRS9C					_T("GRS9C")					// 16扇区9扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_NBSUB_GRS10A					_T("GRS10A")					// 16扇区10扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_NBSUB_GRS10C					_T("GRS10C")					// 16扇区10扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_NBSUB_GRS11A					_T("GRS11A")					// 16扇区11扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_NBSUB_GRS11C					_T("GRS11C")					// 16扇区11扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_NBSUB_GRS12A					_T("GRS12A")					// 16扇区12扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_NBSUB_GRS12C					_T("GRS12C")					// 16扇区12扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_NBSUB_GRS13A					_T("GRS13A")					// 16扇区13扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_NBSUB_GRS13C					_T("GRS13C")					// 16扇区13扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_NBSUB_GRS14A					_T("GRS14A")					// 16扇区14扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_NBSUB_GRS14C					_T("GRS14C")					// 16扇区14扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_NBSUB_GRS15A					_T("GRS15A")					// 16扇区15扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_NBSUB_GRS15C					_T("GRS15C")					// 16扇区15扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_NBSUB_GRS0D					_T("GRS0D")					// 16扇区0扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_NBSUB_GRS0CG					_T("GRS0CG")					// 16扇区0扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_NBSUB_GRS1D					_T("GRS1D")					// 16扇区1扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_NBSUB_GRS1CG					_T("GRS1CG")					// 16扇区1扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_NBSUB_GRS2D					_T("GRS2D")					// 16扇区2扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_NBSUB_GRS2CG					_T("GRS2CG")					// 16扇区2扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_NBSUB_GRS3D					_T("GRS3D")					// 16扇区3扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_NBSUB_GRS3CG					_T("GRS3CG")					// 16扇区3扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_NBSUB_GRS4D					_T("GRS4D")					// 16扇区4扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_NBSUB_GRS4CG					_T("GRS4CG")					// 16扇区4扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_NBSUB_GRS5D					_T("GRS5D")					// 16扇区5扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_NBSUB_GRS5CG					_T("GRS5CG")					// 16扇区5扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_NBSUB_GRS6D					_T("GRS6D")					// 16扇区6扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_NBSUB_GRS6CG					_T("GRS6CG")					// 16扇区6扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_NBSUB_GRS7D					_T("GRS7D")					// 16扇区7扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_NBSUB_GRS7CG					_T("GRS7CG")					// 16扇区7扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_NBSUB_GRS8D					_T("GRS8D")					// 16扇区8扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_NBSUB_GRS8CG					_T("GRS8CG")					// 16扇区8扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_NBSUB_GRS9D					_T("GRS9D")					// 16扇区9扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_NBSUB_GRS9CG					_T("GRS9CG")					// 16扇区9扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_NBSUB_GRS10D					_T("GRS10D")					// 16扇区10扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_NBSUB_GRS10CG					_T("GRS10CG")					// 16扇区10扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_NBSUB_GRS11D					_T("GRS11D")					// 16扇区11扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_NBSUB_GRS11CG					_T("GRS11CG")					// 16扇区11扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_NBSUB_GRS12D					_T("GRS12D")					// 16扇区12扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_NBSUB_GRS12CG					_T("GRS12CG")					// 16扇区12扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_NBSUB_GRS13D					_T("GRS13D")					// 16扇区13扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_NBSUB_GRS13CG					_T("GRS13CG")					// 16扇区13扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_NBSUB_GRS14D					_T("GRS14D")					// 16扇区14扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_NBSUB_GRS14CG					_T("GRS14CG")					// 16扇区14扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_NBSUB_GRS15D					_T("GRS15D")					// 16扇区15扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_NBSUB_GRS15CG					_T("GRS15CG")					// 16扇区15扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_NBSUB_GR					_T("GR")					// 总伽马RAW
#define	FLD_ME_NBSUB_GRA					_T("GRA")					// 总伽马经过API计算系数校正后数据
#define	FLD_ME_NBSUB_GRC					_T("GRC")					// 总伽马经过API计算环境校正后数据
#define	FLD_ME_NBSUB_GRU					_T("GRU")					// 16扇上伽马值
#define	FLD_ME_NBSUB_GRUA					_T("GRUA")					// 16扇上伽马值（API计算系数校正后数据）
#define	FLD_ME_NBSUB_GRUC					_T("GRUC")					// 上伽马经过API计算环境校正后数据
#define	FLD_ME_NBSUB_GRD					_T("GRD")					// 16扇下伽马值
#define	FLD_ME_NBSUB_GRDA					_T("GRDA")					// 16扇下伽马值（API计算系数校正后数据）
#define	FLD_ME_NBSUB_GRDC					_T("GRDC")					// 下伽马经过API计算环境校正后数据
#define	FLD_ME_NBSUB_GRL					_T("GRL")					// 16扇左伽马值
#define	FLD_ME_NBSUB_GRLA					_T("GRLA")					// 16扇左伽马值（API计算系数校正后数据）
#define	FLD_ME_NBSUB_GRLC					_T("GRLC")					// 左伽马经过API计算环境校正后数据
#define	FLD_ME_NBSUB_GRR					_T("GRR")					// 16扇右伽马值
#define	FLD_ME_NBSUB_GRRA					_T("GRRA")					// 16扇右伽马值（API计算系数校正后数据）
#define	FLD_ME_NBSUB_GRRC					_T("GRRC")					// 右伽马经过API计算环境校正后数据
#define	FLD_ME_NBSUB_INC					_T("Inc")					// 井斜
#define	FLD_ME_NBSUB_CONF					_T("Conf")					// 旋转状态:No|0.否;Yes|1.是
#define	FLD_ME_NBSUB_BAD					_T("Bad")					// 是否坏点:No|0.否;Yes|1.是
#define	FLD_ME_NBSUB_DRILLACTIV					_T("DrillActiv")					// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	FLD_ME_NBSUB_SLIDING					_T("Sliding")					// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	FLD_ME_NBSUB_RELOG					_T("ReLog")					// 是否复测 :Yes|0.否;No|1.是
#define	FLD_ME_NBSUB_LAS					_T("Las")					// 是否Las :Yes|0.否;No|1.是
#define	FLD_ME_NBSUB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_ME_NBSUB_UPDTIME					_T("UpdTime")					// 最后一次修改时间戳
#define	FLD_ME_NBSUB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_ME_NBSUB_MEMO					_T("Memo")					// 备注

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_ME_NBSUB_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_ME_NBSUB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_ME_NBSUB
{
 
  int	iDataID;							// 自增字段
  char	szRunID[TV_ME_NBSUB_RUNID_DIGITS + 1];							// 趟钻编号
  time_t	lTDateTime;							// 时间（精确到秒）
  int	iMillitime;							// 毫秒
  int	iToolID;							// 工具编号
  long	lMDepth;							// 自然伽马测量深度 
  long	lVDepth;							// 自然伽马测量深度 
  float	fGRS0;							// 16扇0区扇区伽马值
  float	fGRS1;							// 16扇1区扇区伽马值
  float	fGRS2;							// 16扇2区扇区伽马值
  float	fGRS3;							// 16扇3区扇区伽马值
  float	fGRS4;							// 16扇4区扇区伽马值
  float	fGRS5;							// 16扇5区扇区伽马值
  float	fGRS6;							// 16扇6区扇区伽马值
  float	fGRS7;							// 16扇7区扇区伽马值
  float	fGRS8;							// 16扇8区扇区伽马值
  float	fGRS9;							// 16扇9区扇区伽马值
  float	fGRS10;							// 16扇10区扇区伽马值
  float	fGRS11;							// 16扇11区扇区伽马值
  float	fGRS12;							// 16扇12区扇区伽马值
  float	fGRS13;							// 16扇13区扇区伽马值
  float	fGRS14;							// 16扇14区扇区伽马值
  float	fGRS15;							// 16扇15区扇区伽马值
  float	fGRS0A;							// 16扇区0扇区伽马值（API计算系数校正后数据）
  float	fGRS0C;							// 16扇区0扇区伽马值（API计算环境校正后数据）
  float	fGRS1A;							// 16扇区1扇区伽马值（API计算系数校正后数据）
  float	fGRS1C;							// 16扇区1扇区伽马值（API计算环境校正后数据）
  float	fGRS2A;							// 16扇区2扇区伽马值（API计算系数校正后数据）
  float	fGRS2C;							// 16扇区2扇区伽马值（API计算环境校正后数据）
  float	fGRS3A;							// 16扇区3扇区伽马值（API计算系数校正后数据）
  float	fGRS3C;							// 16扇区3扇区伽马值（API计算环境校正后数据）
  float	fGRS4A;							// 16扇区4扇区伽马值（API计算系数校正后数据）
  float	fGRS4C;							// 16扇区4扇区伽马值（API计算环境校正后数据）
  float	fGRS5A;							// 16扇区5扇区伽马值（API计算系数校正后数据）
  float	fGRS5C;							// 16扇区5扇区伽马值（API计算环境校正后数据）
  float	fGRS6A;							// 16扇区6扇区伽马值（API计算系数校正后数据）
  float	fGRS6C;							// 16扇区6扇区伽马值（API计算环境校正后数据）
  float	fGRS7A;							// 16扇区7扇区伽马值（API计算系数校正后数据）
  float	fGRS7C;							// 16扇区7扇区伽马值（API计算环境校正后数据）
  float	fGRS8A;							// 16扇区8扇区伽马值（API计算系数校正后数据）
  float	fGRS8C;							// 16扇区8扇区伽马值（API计算环境校正后数据）
  float	fGRS9A;							// 16扇区9扇区伽马值（API计算系数校正后数据）
  float	fGRS9C;							// 16扇区9扇区伽马值（API计算环境校正后数据）
  float	fGRS10A;							// 16扇区10扇区伽马值（API计算系数校正后数据）
  float	fGRS10C;							// 16扇区10扇区伽马值（API计算环境校正后数据）
  float	fGRS11A;							// 16扇区11扇区伽马值（API计算系数校正后数据）
  float	fGRS11C;							// 16扇区11扇区伽马值（API计算环境校正后数据）
  float	fGRS12A;							// 16扇区12扇区伽马值（API计算系数校正后数据）
  float	fGRS12C;							// 16扇区12扇区伽马值（API计算环境校正后数据）
  float	fGRS13A;							// 16扇区13扇区伽马值（API计算系数校正后数据）
  float	fGRS13C;							// 16扇区13扇区伽马值（API计算环境校正后数据）
  float	fGRS14A;							// 16扇区14扇区伽马值（API计算系数校正后数据）
  float	fGRS14C;							// 16扇区14扇区伽马值（API计算环境校正后数据）
  float	fGRS15A;							// 16扇区15扇区伽马值（API计算系数校正后数据）
  float	fGRS15C;							// 16扇区15扇区伽马值（API计算环境校正后数据）
  float	fGRS0D;							// 16扇区0扇区伽马值（API暗电流校正后数据）
  float	fGRS0CG;							// 16扇区0扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS1D;							// 16扇区1扇区伽马值（API暗电流校正后数据）
  float	fGRS1CG;							// 16扇区1扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS2D;							// 16扇区2扇区伽马值（API暗电流校正后数据）
  float	fGRS2CG;							// 16扇区2扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS3D;							// 16扇区3扇区伽马值（API暗电流校正后数据）
  float	fGRS3CG;							// 16扇区3扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS4D;							// 16扇区4扇区伽马值（API暗电流校正后数据）
  float	fGRS4CG;							// 16扇区4扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS5D;							// 16扇区5扇区伽马值（API暗电流校正后数据）
  float	fGRS5CG;							// 16扇区5扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS6D;							// 16扇区6扇区伽马值（API暗电流校正后数据）
  float	fGRS6CG;							// 16扇区6扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS7D;							// 16扇区7扇区伽马值（API暗电流校正后数据）
  float	fGRS7CG;							// 16扇区7扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS8D;							// 16扇区8扇区伽马值（API暗电流校正后数据）
  float	fGRS8CG;							// 16扇区8扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS9D;							// 16扇区9扇区伽马值（API暗电流校正后数据）
  float	fGRS9CG;							// 16扇区9扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS10D;							// 16扇区10扇区伽马值（API暗电流校正后数据）
  float	fGRS10CG;							// 16扇区10扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS11D;							// 16扇区11扇区伽马值（API暗电流校正后数据）
  float	fGRS11CG;							// 16扇区11扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS12D;							// 16扇区12扇区伽马值（API暗电流校正后数据）
  float	fGRS12CG;							// 16扇区12扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS13D;							// 16扇区13扇区伽马值（API暗电流校正后数据）
  float	fGRS13CG;							// 16扇区13扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS14D;							// 16扇区14扇区伽马值（API暗电流校正后数据）
  float	fGRS14CG;							// 16扇区14扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS15D;							// 16扇区15扇区伽马值（API暗电流校正后数据）
  float	fGRS15CG;							// 16扇区15扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGR;							// 总伽马RAW
  float	fGRA;							// 总伽马经过API计算系数校正后数据
  float	fGRC;							// 总伽马经过API计算环境校正后数据
  float	fGRU;							// 16扇上伽马值
  float	fGRUA;							// 16扇上伽马值（API计算系数校正后数据）
  float	fGRUC;							// 上伽马经过API计算环境校正后数据
  float	fGRD;							// 16扇下伽马值
  float	fGRDA;							// 16扇下伽马值（API计算系数校正后数据）
  float	fGRDC;							// 下伽马经过API计算环境校正后数据
  float	fGRL;							// 16扇左伽马值
  float	fGRLA;							// 16扇左伽马值（API计算系数校正后数据）
  float	fGRLC;							// 左伽马经过API计算环境校正后数据
  float	fGRR;							// 16扇右伽马值
  float	fGRRA;							// 16扇右伽马值（API计算系数校正后数据）
  float	fGRRC;							// 右伽马经过API计算环境校正后数据
  float	fInc;							// 井斜
  short	nConf;							// 旋转状态:No|0.否;Yes|1.是
  short	nBad;							// 是否坏点:No|0.否;Yes|1.是
  short	nDrillActiv;							// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
  short	nSliding;							// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
  short	nReLog;							// 是否复测 :Yes|0.否;No|1.是
  short	nLas;							// 是否Las :Yes|0.否;No|1.是
  time_t	lCreateTime;							// 创建时间戳
  time_t	lUpdTime;							// 最后一次修改时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_ME_NBSUB_MEMO_DIGITS + 1];							// 备注
} TS_ME_NBSUB;

typedef	TS_ME_NBSUB FAR*	LPTS_ME_NBSUB;

#pragma	pack(pop)

#endif // _TME_NBSUB_H

//---------------------------------------------------------------------------//
// 文件名: ReLog.h
// 说明:	复测数据表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020/12/2 18:11:14
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TReLog.h : DReLog

#ifndef	_TRELOG_H
#define	_TRELOG_H

#define	TID_RELOG								_T("ReLog")
#define	OID_RELOG								_T("")

// Sort No
#define	SORT_RELOG_PK0				0							// PK:数据编号
#define	SORT_RELOG_PK1				1							// PK:趟钻编号
//#define	SORT_RELOG_A1							%#%							// A1:

// Colum No
#define	COL_RELOG_DATAID					(short)0						// 数据编号
#define	COL_RELOG_RUNID					(short)1						// 趟钻编号
#define	COL_RELOG_RELOGNO					(short)2						// 第几次复测
#define	COL_RELOG_STARTTIME					(short)3						// 结束时间
#define	COL_RELOG_ENDTIME					(short)4						// 开始深度
#define	COL_RELOG_STARTDEPTH					(short)5						// 结束深度
#define	COL_RELOG_ENDDEPTH					(short)6						// 结束深度
#define	COL_RELOG_ACTIV					(short)7						// 上提还是下放:Up|0.上提;Down|1.下放
#define	COL_RELOG_RELOGSTATUS					(short)8						// 复测状态:Start|0.复测开始;Logging|1.复测中;End|2.复测结束
#define	COL_RELOG_CREATETIME					(short)9						// 创建时间戳
#define	COL_RELOG_STATUS					(short)10						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_RELOG_MEMO					(short)11						// 备注
#define	COL_RELOG_UPDCOUNT					(short)12						// 更新计数

// Colum(Field) Name
#define	FLD_RELOG_DATAID					_T("DataID")					// 数据编号
#define	FLD_RELOG_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_RELOG_RELOGNO					_T("RelogNo")					// 第几次复测
#define	FLD_RELOG_STARTTIME					_T("StartTime")					// 结束时间
#define	FLD_RELOG_ENDTIME					_T("EndTime")					// 开始深度
#define	FLD_RELOG_STARTDEPTH					_T("StartDepth")					// 结束深度
#define	FLD_RELOG_ENDDEPTH					_T("EndDepth")					// 结束深度
#define	FLD_RELOG_ACTIV					_T("Activ")					// 上提还是下放:Up|0.上提;Down|1.下放
#define	FLD_RELOG_RELOGSTATUS					_T("ReLogStatus")					// 复测状态:Start|0.复测开始;Logging|1.复测中;End|2.复测结束
#define	FLD_RELOG_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_RELOG_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_RELOG_MEMO					_T("Memo")					// 备注
#define	FLD_RELOG_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_RELOG_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_RELOG_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_RELOG
{
 
  long	lDataID;							// 数据编号
  char	szRunID[TV_RELOG_RUNID_DIGITS + 1];							// 趟钻编号
  int	iRelogNo;							// 第几次复测
  time_t	lStartTime;							// 结束时间
  time_t	lEndTime;							// 开始深度
  long	lStartDepth;							// 结束深度
  long	lEndDepth;							// 结束深度
  short	nActiv;							// 上提还是下放:Up|0.上提;Down|1.下放
  short	nReLogStatus;							// 复测状态:Start|0.复测开始;Logging|1.复测中;End|2.复测结束
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_RELOG_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_RELOG;

typedef	TS_RELOG FAR*	LPTS_RELOG;

#pragma	pack(pop)

#endif // _TRELOG_H

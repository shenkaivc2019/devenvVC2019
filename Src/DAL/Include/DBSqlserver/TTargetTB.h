//---------------------------------------------------------------------------//
// 文件名: TargetTB.h
// 说明:	靶点信息数据表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020/10/19 22:11:35
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TTargetTB.h : DTargetTB

#ifndef	_TTARGETTB_H
#define	_TTARGETTB_H

#define	TID_TARGETTB								_T("TargetTB")
#define	OID_TARGETTB								_T("")

// Sort No
#define	SORT_TARGETTB_PK0				0							// PK:自增字段
//#define	SORT_TARGETTB_A1							%#%							// A1:

// Colum No
#define	COL_TARGETTB_TARGID					(short)0						// 自增字段
#define	COL_TARGETTB_HOLEID					(short)1						// 井眼编号
#define	COL_TARGETTB_RUNID					(short)2						// 趟钻编号（根据趟钻关联到井眼)
#define	COL_TARGETTB_TARGETNO					(short)3						// 排序
#define	COL_TARGETTB_TARGETTVD					(short)4						// 目标垂深
#define	COL_TARGETTB_TARGETCOORIDINATEX					(short)5						// 坐标点X
#define	COL_TARGETTB_TARGETCOORIDINATEY					(short)6						// 坐标点Y
#define	COL_TARGETTB_TARGETSHAPE					(short)7						// 靶点类型（点靶，矩形靶，圆靶，多边形靶，椭圆靶）
#define	COL_TARGETTB_TARGETWIDTH					(short)8						// 靶心距X
#define	COL_TARGETTB_TARGETHEIGHT					(short)9						// 靶心距Y
#define	COL_TARGETTB_TARGESHAPEROTATE					(short)10						// 靶框角度
#define	COL_TARGETTB_TARGESHAPEDIP					(short)11						// 靶框倾角
#define	COL_TARGETTB_AZIMUTH					(short)12						// 投影方位
#define	COL_TARGETTB_CREATETIME					(short)13						// 创建时间戳
#define	COL_TARGETTB_STATUS					(short)14						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_TARGETTB_MEMO					(short)15						// 备注
#define	COL_TARGETTB_UPDCOUNT					(short)16						// 更新计数

// Colum(Field) Name
#define	FLD_TARGETTB_TARGID					_T("TargID")					// 自增字段
#define	FLD_TARGETTB_HOLEID					_T("HoleID")					// 井眼编号
#define	FLD_TARGETTB_RUNID					_T("RunID")					// 趟钻编号（根据趟钻关联到井眼)
#define	FLD_TARGETTB_TARGETNO					_T("TargetNo")					// 排序
#define	FLD_TARGETTB_TARGETTVD					_T("TargetTVD")					// 目标垂深
#define	FLD_TARGETTB_TARGETCOORIDINATEX					_T("TargetCooridinateX")					// 坐标点X
#define	FLD_TARGETTB_TARGETCOORIDINATEY					_T("TargetCooridinateY")					// 坐标点Y
#define	FLD_TARGETTB_TARGETSHAPE					_T("TargetShape")					// 靶点类型（点靶，矩形靶，圆靶，多边形靶，椭圆靶）
#define	FLD_TARGETTB_TARGETWIDTH					_T("TargetWidth")					// 靶心距X
#define	FLD_TARGETTB_TARGETHEIGHT					_T("TargetHeight")					// 靶心距Y
#define	FLD_TARGETTB_TARGESHAPEROTATE					_T("TargeShapeRotate")					// 靶框角度
#define	FLD_TARGETTB_TARGESHAPEDIP					_T("TargeShapeDIP")					// 靶框倾角
#define	FLD_TARGETTB_AZIMUTH					_T("Azimuth")					// 投影方位
#define	FLD_TARGETTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_TARGETTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_TARGETTB_MEMO					_T("Memo")					// 备注
#define	FLD_TARGETTB_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_TARGETTB_TARGID_DIGITS				50					// 自增字段位数
#define	TV_TARGETTB_HOLEID_DIGITS				50					// 井眼编号位数
#define	TV_TARGETTB_RUNID_DIGITS				50					// 趟钻编号（根据趟钻关联到井眼)位数
#define	TV_TARGETTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_TARGETTB
{
 
  char	szTargID[TV_TARGETTB_TARGID_DIGITS + 1];							// 自增字段
  char	szHoleID[TV_TARGETTB_HOLEID_DIGITS + 1];							// 井眼编号
  char	szRunID[TV_TARGETTB_RUNID_DIGITS + 1];							// 趟钻编号（根据趟钻关联到井眼)
  long	lTargetNo;							// 排序
  double	dTargetTVD;							// 目标垂深
  double	dTargetCooridinateX;							// 坐标点X
  double	dTargetCooridinateY;							// 坐标点Y
  int	iTargetShape;							// 靶点类型（点靶，矩形靶，圆靶，多边形靶，椭圆靶）
  double	dTargetWidth;							// 靶心距X
  double	dTargetHeight;							// 靶心距Y
  double	dTargeShapeRotate;							// 靶框角度
  double	dTargeShapeDIP;							// 靶框倾角
  double	dAzimuth;							// 投影方位
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_TARGETTB_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_TARGETTB;

typedef	TS_TARGETTB FAR*	LPTS_TARGETTB;

#pragma	pack(pop)

#endif // _TTARGETTB_H

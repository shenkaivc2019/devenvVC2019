//---------------------------------------------------------------------------//
// 文件名: ME_APR.h
// 说明:	仪器内存数据表(环空压力）
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2023/4/18 14:05:00
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TME_APR.h : DME_APR

#ifndef	_TME_APR_H
#define	_TME_APR_H

#define	TID_ME_APR								_T("ME_APR")
#define	OID_ME_APR								_T("")

// Sort No
#define	SORT_ME_APR_PK0				0							// PK:自增字段
//#define	SORT_ME_APR_A1							%#%							// A1:

// Colum No
#define	COL_ME_APR_DATAID					(short)0						// 自增字段
#define	COL_ME_APR_RUNID					(short)1						// 趟钻编号
#define	COL_ME_APR_TDATETIME					(short)2						// 时间（精确到秒）
#define	COL_ME_APR_MILLITIME					(short)3						// 毫秒
#define	COL_ME_APR_TOOLID					(short)4						// 工具编号
#define	COL_ME_APR_MDEPTH					(short)5						// 方位伽马测量井深
#define	COL_ME_APR_VDEPTH					(short)6						// 方位伽马测量垂深
#define	COL_ME_APR_CURVENAME					(short)7						// 曲线名称
#define	COL_ME_APR_WITSID					(short)8						// WITSID
#define	COL_ME_APR_VALUER					(short)9						// 环空压力原始数据值
#define	COL_ME_APR_VALUEM					(short)10						// 环空压力修改数据值
#define	COL_ME_APR_ECD					(short)11						// 循环当量密度=环空压力/方位伽马测量点垂深
#define	COL_ME_APR_ECDM					(short)12						// 循环当量密度=环空压力/方位伽马测量点垂深
#define	COL_ME_APR_CONF					(short)13						// 旋转状态:No|0.否;Yes|1.是
#define	COL_ME_APR_BAD					(short)14						// 是否坏点:No|0.否;Yes|1.是
#define	COL_ME_APR_DRILLACTIV					(short)15						// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	COL_ME_APR_SLIDING					(short)16						// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	COL_ME_APR_RELOG					(short)17						// 是否复测 :Yes|0.否;No|1.是
#define	COL_ME_APR_LAS					(short)18						// 是否Las :Yes|0.否;No|1.是
#define	COL_ME_APR_CREATETIME					(short)19						// 创建时间戳
#define	COL_ME_APR_UPDTIME					(short)20						// 最后一次修改时间戳
#define	COL_ME_APR_STATUS					(short)21						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_ME_APR_MEMO					(short)22						// 备注

// Colum(Field) Name
#define	FLD_ME_APR_DATAID					_T("DataID")					// 自增字段
#define	FLD_ME_APR_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_ME_APR_TDATETIME					_T("TDateTime")					// 时间（精确到秒）
#define	FLD_ME_APR_MILLITIME					_T("Millitime")					// 毫秒
#define	FLD_ME_APR_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_ME_APR_MDEPTH					_T("MDepth")					// 方位伽马测量井深
#define	FLD_ME_APR_VDEPTH					_T("VDepth")					// 方位伽马测量垂深
#define	FLD_ME_APR_CURVENAME					_T("CurveName")					// 曲线名称
#define	FLD_ME_APR_WITSID					_T("WITSID")					// WITSID
#define	FLD_ME_APR_VALUER					_T("ValueR")					// 环空压力原始数据值
#define	FLD_ME_APR_VALUEM					_T("ValueM")					// 环空压力修改数据值
#define	FLD_ME_APR_ECD					_T("ECD")					// 循环当量密度=环空压力/方位伽马测量点垂深
#define	FLD_ME_APR_ECDM					_T("ECDM")					// 循环当量密度=环空压力/方位伽马测量点垂深
#define	FLD_ME_APR_CONF					_T("Conf")					// 旋转状态:No|0.否;Yes|1.是
#define	FLD_ME_APR_BAD					_T("Bad")					// 是否坏点:No|0.否;Yes|1.是
#define	FLD_ME_APR_DRILLACTIV					_T("DrillActiv")					// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	FLD_ME_APR_SLIDING					_T("Sliding")					// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	FLD_ME_APR_RELOG					_T("ReLog")					// 是否复测 :Yes|0.否;No|1.是
#define	FLD_ME_APR_LAS					_T("Las")					// 是否Las :Yes|0.否;No|1.是
#define	FLD_ME_APR_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_ME_APR_UPDTIME					_T("UpdTime")					// 最后一次修改时间戳
#define	FLD_ME_APR_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_ME_APR_MEMO					_T("Memo")					// 备注

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_ME_APR_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_ME_APR_CURVENAME_DIGITS				50					// 曲线名称位数
#define	TV_ME_APR_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_ME_APR
{
 
  int	iDataID;							// 自增字段
  char	szRunID[TV_ME_APR_RUNID_DIGITS + 1];							// 趟钻编号
  time_t	lTDateTime;							// 时间（精确到秒）
  int	iMillitime;							// 毫秒
  int	iToolID;							// 工具编号
  long	lMDepth;							// 方位伽马测量井深
  long	lVDepth;							// 方位伽马测量垂深
  char	szCurveName[TV_ME_APR_CURVENAME_DIGITS + 1];							// 曲线名称
  int	iWITSID;							// WITSID
  float	fValueR;							// 环空压力原始数据值
  float	fValueM;							// 环空压力修改数据值
  float	fECD;							// 循环当量密度=环空压力/方位伽马测量点垂深
  float	fECDM;							// 循环当量密度=环空压力/方位伽马测量点垂深
  short	nConf;							// 旋转状态:No|0.否;Yes|1.是
  short	nBad;							// 是否坏点:No|0.否;Yes|1.是
  short	nDrillActiv;							// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
  short	nSliding;							// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
  short	nReLog;							// 是否复测 :Yes|0.否;No|1.是
  short	nLas;							// 是否Las :Yes|0.否;No|1.是
  time_t	lCreateTime;							// 创建时间戳
  time_t	lUpdTime;							// 最后一次修改时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_ME_APR_MEMO_DIGITS + 1];							// 备注
} TS_ME_APR;

typedef	TS_ME_APR FAR*	LPTS_ME_APR;

#pragma	pack(pop)

#endif // _TME_APR_H

//---------------------------------------------------------------------------//
// 文件名: GammaSub.h
// 说明:	伽马校正参数表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020/10/19 22:11:35
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TGammaSub.h : DGammaSub

#ifndef	_TGAMMASUB_H
#define	_TGAMMASUB_H

#define	TID_GAMMASUB								_T("GammaSub")
#define	OID_GAMMASUB								_T("")

// Sort No
#define	SORT_GAMMASUB_PK0				0							// PK:编号
//#define	SORT_GAMMASUB_A1							%#%							// A1:

// Colum No
#define	COL_GAMMASUB_GSID					(short)0						// 编号
#define	COL_GAMMASUB_GSNAME					(short)1						// 伽马校正名称
#define	COL_GAMMASUB_CALIBRATIONTIME					(short)2						// 校准时间
#define	COL_GAMMASUB_TOOLTYPE					(short)3						// 孔径类型（Dark Current factor）:Standard1|1.4.75 in;Standard2|2.6.75in;Standard3|3.7.125in;8.25|4.8.25in;Custom|5.自定义钻铤系数;
#define	COL_GAMMASUB_TOOLDCF					(short)4						// 暗电流修正系数
#define	COL_GAMMASUB_TOOLACF					(short)5						// 钻铤壁修正系数
#define	COL_GAMMASUB_APICF					(short)6						// 解析中间值
#define	COL_GAMMASUB_CENTERING					(short)7						// 暂时放这里 现在不知道怎么使用
#define	COL_GAMMASUB_TOOLINTDIA					(short)8						// 内径
#define	COL_GAMMASUB_TOOLEXTDIA					(short)9						// 外径
#define	COL_GAMMASUB_DESCRIPTION_CN					(short)10						// 中文描述
#define	COL_GAMMASUB_DESCRIPTION_EN					(short)11						// 英文描述
#define	COL_GAMMASUB_BITSIZE					(short)12						// 钻头直径
#define	COL_GAMMASUB_BARRELOD					(short)13						// 钻头直径
#define	COL_GAMMASUB_MWDCAPI					(short)14						// MWD传感器校准系数
#define	COL_GAMMASUB_LWDCC					(short)15						// LWD钻铤校正系数
#define	COL_GAMMASUB_LWDCAPI					(short)16						// LWD传感器校准系数
#define	COL_GAMMASUB_CREATETIME					(short)17						// 创建时间戳
#define	COL_GAMMASUB_STATUS					(short)18						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_GAMMASUB_MEMO					(short)19						// 备注
#define	COL_GAMMASUB_UPDCOUNT					(short)20						// 更新计数

// Colum(Field) Name
#define	FLD_GAMMASUB_GSID					_T("GSID")					// 编号
#define	FLD_GAMMASUB_GSNAME					_T("GSName")					// 伽马校正名称
#define	FLD_GAMMASUB_CALIBRATIONTIME					_T("CalibrationTime")					// 校准时间
#define	FLD_GAMMASUB_TOOLTYPE					_T("ToolType")					// 孔径类型（Dark Current factor）:Standard1|1.4.75 in;Standard2|2.6.75in;Standard3|3.7.125in;8.25|4.8.25in;Custom|5.自定义钻铤系数;
#define	FLD_GAMMASUB_TOOLDCF					_T("ToolDCF")					// 暗电流修正系数
#define	FLD_GAMMASUB_TOOLACF					_T("ToolACF")					// 钻铤壁修正系数
#define	FLD_GAMMASUB_APICF					_T("APICF")					// 解析中间值
#define	FLD_GAMMASUB_CENTERING					_T("Centering")					// 暂时放这里 现在不知道怎么使用
#define	FLD_GAMMASUB_TOOLINTDIA					_T("ToolIntDia")					// 内径
#define	FLD_GAMMASUB_TOOLEXTDIA					_T("ToolExtDia")					// 外径
#define	FLD_GAMMASUB_DESCRIPTION_CN					_T("Description_cn")					// 中文描述
#define	FLD_GAMMASUB_DESCRIPTION_EN					_T("Description_en")					// 英文描述
#define	FLD_GAMMASUB_BITSIZE					_T("BitSize")					// 钻头直径
#define	FLD_GAMMASUB_BARRELOD					_T("BarrelOD")					// 钻头直径
#define	FLD_GAMMASUB_MWDCAPI					_T("MWDCAPI")					// MWD传感器校准系数
#define	FLD_GAMMASUB_LWDCC					_T("LWDCc")					// LWD钻铤校正系数
#define	FLD_GAMMASUB_LWDCAPI					_T("LWDCAPI")					// LWD传感器校准系数
#define	FLD_GAMMASUB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_GAMMASUB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_GAMMASUB_MEMO					_T("Memo")					// 备注
#define	FLD_GAMMASUB_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_GAMMASUB_GSNAME_DIGITS				50					// 伽马校正名称位数
#define	TV_GAMMASUB_DESCRIPTION_CN_DIGITS				50					// 中文描述位数
#define	TV_GAMMASUB_DESCRIPTION_EN_DIGITS				50					// 英文描述位数
#define	TV_GAMMASUB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_GAMMASUB
{
 
  int	iGSID;							// 编号
  char	szGSName[TV_GAMMASUB_GSNAME_DIGITS + 1];							// 伽马校正名称
  time_t	lCalibrationTime;							// 校准时间
  short	nToolType;							// 孔径类型（Dark Current factor）:Standard1|1.4.75 in;Standard2|2.6.75in;Standard3|3.7.125in;8.25|4.8.25in;Custom|5.自定义钻铤系数;
  float	fToolDCF;							// 暗电流修正系数
  float	fToolACF;							// 钻铤壁修正系数
  float	fAPICF;							// 解析中间值
  float	fCentering;							// 暂时放这里 现在不知道怎么使用
  double	dToolIntDia;							// 内径
  double	dToolExtDia;							// 外径
  char	szDescription_cn[TV_GAMMASUB_DESCRIPTION_CN_DIGITS + 1];							// 中文描述
  char	szDescription_en[TV_GAMMASUB_DESCRIPTION_EN_DIGITS + 1];							// 英文描述
  float	fBitSize;							// 钻头直径
  float	fBarrelOD;							// 钻头直径
  float	fMWDCAPI;							// MWD传感器校准系数
  float	fLWDCc;							// LWD钻铤校正系数
  float	fLWDCAPI;							// LWD传感器校准系数
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_GAMMASUB_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_GAMMASUB;

typedef	TS_GAMMASUB FAR*	LPTS_GAMMASUB;

#pragma	pack(pop)

#endif // _TGAMMASUB_H

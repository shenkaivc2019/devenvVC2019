//---------------------------------------------------------------------------//
// 文件名: PressureCalibration.h
// 说明:	环空压力标定
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2023/4/23 9:12:40
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TPressureCalibration.h : DPressureCalibration

#ifndef	_TPRESSURECALIBRATION_H
#define	_TPRESSURECALIBRATION_H

#define	TID_PRESSURECALIBRATION								_T("PressureCalibration")
#define	OID_PRESSURECALIBRATION								_T("")

// Sort No
#define	SORT_PRESSURECALIBRATION_PK0				0							// PK:数据编号
//#define	SORT_PRESSURECALIBRATION_A1							%#%							// A1:

// Colum No
#define	COL_PRESSURECALIBRATION_DATAID					(short)0						// 数据编号
#define	COL_PRESSURECALIBRATION_TOOLID					(short)1						// 工具编号
#define	COL_PRESSURECALIBRATION_TOOLSN					(short)2						// 工具编号SN（类似于产品条码）
#define	COL_PRESSURECALIBRATION_RUNID					(short)3						// 趟钻编号
#define	COL_PRESSURECALIBRATION_TEMPERATURE					(short)4						// 温度
#define	COL_PRESSURECALIBRATION_ACTUAL					(short)5						// 手动输入的数据 即工装实测报告的数据
#define	COL_PRESSURECALIBRATION_ORIGIN					(short)6						// 原始数据 接收到的数据
#define	COL_PRESSURECALIBRATION_SCALEFANNU					(short)7						// 环空数据
#define	COL_PRESSURECALIBRATION_OFFSETANNU					(short)8						// 环空补偿
#define	COL_PRESSURECALIBRATION_SCALEFBORE					(short)9						// 钻孔数据
#define	COL_PRESSURECALIBRATION_OFFSETBORE					(short)10						// 钻孔补偿
#define	COL_PRESSURECALIBRATION_CREATETIME					(short)11						// 创建时间戳
#define	COL_PRESSURECALIBRATION_STATUS					(short)12						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_PRESSURECALIBRATION_MEMO					(short)13						// 备注
#define	COL_PRESSURECALIBRATION_UPDCOUNT					(short)14						// 更新计数

// Colum(Field) Name
#define	FLD_PRESSURECALIBRATION_DATAID					_T("DataID")					// 数据编号
#define	FLD_PRESSURECALIBRATION_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_PRESSURECALIBRATION_TOOLSN					_T("ToolSN")					// 工具编号SN（类似于产品条码）
#define	FLD_PRESSURECALIBRATION_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_PRESSURECALIBRATION_TEMPERATURE					_T("Temperature")					// 温度
#define	FLD_PRESSURECALIBRATION_ACTUAL					_T("Actual")					// 手动输入的数据 即工装实测报告的数据
#define	FLD_PRESSURECALIBRATION_ORIGIN					_T("Origin")					// 原始数据 接收到的数据
#define	FLD_PRESSURECALIBRATION_SCALEFANNU					_T("ScaleFAnnu")					// 环空数据
#define	FLD_PRESSURECALIBRATION_OFFSETANNU					_T("OffsetAnnu")					// 环空补偿
#define	FLD_PRESSURECALIBRATION_SCALEFBORE					_T("ScaleFBore")					// 钻孔数据
#define	FLD_PRESSURECALIBRATION_OFFSETBORE					_T("OffsetBore")					// 钻孔补偿
#define	FLD_PRESSURECALIBRATION_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_PRESSURECALIBRATION_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_PRESSURECALIBRATION_MEMO					_T("Memo")					// 备注
#define	FLD_PRESSURECALIBRATION_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_PRESSURECALIBRATION_TOOLSN_DIGITS				50					// 工具编号SN（类似于产品条码）位数
#define	TV_PRESSURECALIBRATION_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_PRESSURECALIBRATION_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_PRESSURECALIBRATION
{
 
  long	lDataID;							// 数据编号
  int	iToolID;							// 工具编号
  char	szToolSN[TV_PRESSURECALIBRATION_TOOLSN_DIGITS + 1];							// 工具编号SN（类似于产品条码）
  char	szRunID[TV_PRESSURECALIBRATION_RUNID_DIGITS + 1];							// 趟钻编号
  float	fTemperature;							// 温度
  float	fActual;							// 手动输入的数据 即工装实测报告的数据
  float	fOrigin;							// 原始数据 接收到的数据
  float	fScaleFAnnu;							// 环空数据
  float	fOffsetAnnu;							// 环空补偿
  float	fScaleFBore;							// 钻孔数据
  float	fOffsetBore;							// 钻孔补偿
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_PRESSURECALIBRATION_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_PRESSURECALIBRATION;

typedef	TS_PRESSURECALIBRATION FAR*	LPTS_PRESSURECALIBRATION;

#pragma	pack(pop)

#endif // _TPRESSURECALIBRATION_H

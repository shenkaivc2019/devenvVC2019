//---------------------------------------------------------------------------//
// 文件名: RT_NBSubImage.h
// 说明:	实时伽马成像数据表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2023/9/18 14:06:24
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TRT_NBSubImage.h : DRT_NBSubImage

#ifndef	_TRT_NBSUBIMAGE_H
#define	_TRT_NBSUBIMAGE_H

#define	TID_RT_NBSUBIMAGE								_T("RT_NBSubImage")
#define	OID_RT_NBSUBIMAGE								_T("")

// Sort No
#define	SORT_RT_NBSUBIMAGE_PK0				0							// PK:数据编号
//#define	SORT_RT_NBSUBIMAGE_A1							%#%							// A1:

// Colum No
#define	COL_RT_NBSUBIMAGE_DATAID					(short)0						// 数据编号
#define	COL_RT_NBSUBIMAGE_RUNID					(short)1						// 趟钻编号
#define	COL_RT_NBSUBIMAGE_TDATETIME					(short)2						// 时间（精确到秒）
#define	COL_RT_NBSUBIMAGE_MILLITIME					(short)3						// 毫秒
#define	COL_RT_NBSUBIMAGE_TOOLID					(short)4						// 工具编号
#define	COL_RT_NBSUBIMAGE_MDEPTH					(short)5						// 方位伽马测量点井深
#define	COL_RT_NBSUBIMAGE_VDEPTH					(short)6						// 方位伽马测量点垂深
#define	COL_RT_NBSUBIMAGE_GRUDATAID					(short)7						// 4扇区扇区上伽马计数数据编号
#define	COL_RT_NBSUBIMAGE_GRDDATAID					(short)8						// 4扇区扇区下伽马计数数据编号
#define	COL_RT_NBSUBIMAGE_GRLDATAID					(short)9						// 4扇区扇区左伽马计数数据编号
#define	COL_RT_NBSUBIMAGE_GRRDATAID					(short)10						// 4扇区扇区右伽马计数数据编号
#define	COL_RT_NBSUBIMAGE_GRU					(short)11						// 4扇区扇区上伽马计数
#define	COL_RT_NBSUBIMAGE_GRD					(short)12						// 4扇区扇区下伽马计数
#define	COL_RT_NBSUBIMAGE_GRL					(short)13						// 4扇区扇区左伽马计数
#define	COL_RT_NBSUBIMAGE_GRR					(short)14						// 4扇区扇区右伽马计数
#define	COL_RT_NBSUBIMAGE_GRT					(short)15						// 4扇区扇区平均伽马计数
#define	COL_RT_NBSUBIMAGE_CONF					(short)16						// 可信度
#define	COL_RT_NBSUBIMAGE_BAD					(short)17						// 是否坏点:No|0.否;Yes|1.是
#define	COL_RT_NBSUBIMAGE_DRILLACTIV					(short)18						// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	COL_RT_NBSUBIMAGE_SLIDING					(short)19						// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	COL_RT_NBSUBIMAGE_RELOG					(short)20						// 是否复测 :Yes|0.否;No|1.是
#define	COL_RT_NBSUBIMAGE_LAS					(short)21						// 是否Las :No|0.否;Yes|1.是
#define	COL_RT_NBSUBIMAGE_CREATETIME					(short)22						// 创建时间戳
#define	COL_RT_NBSUBIMAGE_UPDTIME					(short)23						// 最后一次修改时间戳
#define	COL_RT_NBSUBIMAGE_STATUS					(short)24						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_RT_NBSUBIMAGE_MEMO					(short)25						// 备注

// Colum(Field) Name
#define	FLD_RT_NBSUBIMAGE_DATAID					_T("DataID")					// 数据编号
#define	FLD_RT_NBSUBIMAGE_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_RT_NBSUBIMAGE_TDATETIME					_T("TDateTime")					// 时间（精确到秒）
#define	FLD_RT_NBSUBIMAGE_MILLITIME					_T("Millitime")					// 毫秒
#define	FLD_RT_NBSUBIMAGE_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_RT_NBSUBIMAGE_MDEPTH					_T("MDepth")					// 方位伽马测量点井深
#define	FLD_RT_NBSUBIMAGE_VDEPTH					_T("VDepth")					// 方位伽马测量点垂深
#define	FLD_RT_NBSUBIMAGE_GRUDATAID					_T("GRUDataID")					// 4扇区扇区上伽马计数数据编号
#define	FLD_RT_NBSUBIMAGE_GRDDATAID					_T("GRDDataID")					// 4扇区扇区下伽马计数数据编号
#define	FLD_RT_NBSUBIMAGE_GRLDATAID					_T("GRLDataID")					// 4扇区扇区左伽马计数数据编号
#define	FLD_RT_NBSUBIMAGE_GRRDATAID					_T("GRRDataID")					// 4扇区扇区右伽马计数数据编号
#define	FLD_RT_NBSUBIMAGE_GRU					_T("GRU")					// 4扇区扇区上伽马计数
#define	FLD_RT_NBSUBIMAGE_GRD					_T("GRD")					// 4扇区扇区下伽马计数
#define	FLD_RT_NBSUBIMAGE_GRL					_T("GRL")					// 4扇区扇区左伽马计数
#define	FLD_RT_NBSUBIMAGE_GRR					_T("GRR")					// 4扇区扇区右伽马计数
#define	FLD_RT_NBSUBIMAGE_GRT					_T("GRT")					// 4扇区扇区平均伽马计数
#define	FLD_RT_NBSUBIMAGE_CONF					_T("Conf")					// 可信度
#define	FLD_RT_NBSUBIMAGE_BAD					_T("Bad")					// 是否坏点:No|0.否;Yes|1.是
#define	FLD_RT_NBSUBIMAGE_DRILLACTIV					_T("DrillActiv")					// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	FLD_RT_NBSUBIMAGE_SLIDING					_T("Sliding")					// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	FLD_RT_NBSUBIMAGE_RELOG					_T("ReLog")					// 是否复测 :Yes|0.否;No|1.是
#define	FLD_RT_NBSUBIMAGE_LAS					_T("Las")					// 是否Las :No|0.否;Yes|1.是
#define	FLD_RT_NBSUBIMAGE_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_RT_NBSUBIMAGE_UPDTIME					_T("UpdTime")					// 最后一次修改时间戳
#define	FLD_RT_NBSUBIMAGE_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_RT_NBSUBIMAGE_MEMO					_T("Memo")					// 备注

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_RT_NBSUBIMAGE_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_RT_NBSUBIMAGE_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_RT_NBSUBIMAGE
{
 
  long	lDataID;							// 数据编号
  char	szRunID[TV_RT_NBSUBIMAGE_RUNID_DIGITS + 1];							// 趟钻编号
  time_t	lTDateTime;							// 时间（精确到秒）
  int	iMillitime;							// 毫秒
  int	iToolID;							// 工具编号
  long	lMDepth;							// 方位伽马测量点井深
  long	lVDepth;							// 方位伽马测量点垂深
  long	lGRUDataID;							// 4扇区扇区上伽马计数数据编号
  long	lGRDDataID;							// 4扇区扇区下伽马计数数据编号
  long	lGRLDataID;							// 4扇区扇区左伽马计数数据编号
  long	lGRRDataID;							// 4扇区扇区右伽马计数数据编号
  float	fGRU;							// 4扇区扇区上伽马计数
  float	fGRD;							// 4扇区扇区下伽马计数
  float	fGRL;							// 4扇区扇区左伽马计数
  float	fGRR;							// 4扇区扇区右伽马计数
  float	fGRT;							// 4扇区扇区平均伽马计数
  short	nConf;							// 可信度
  short	nBad;							// 是否坏点:No|0.否;Yes|1.是
  short	nDrillActiv;							// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
  short	nSliding;							// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
  short	nReLog;							// 是否复测 :Yes|0.否;No|1.是
  short	nLas;							// 是否Las :No|0.否;Yes|1.是
  time_t	lCreateTime;							// 创建时间戳
  time_t	lUpdTime;							// 最后一次修改时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_RT_NBSUBIMAGE_MEMO_DIGITS + 1];							// 备注
} TS_RT_NBSUBIMAGE;

typedef	TS_RT_NBSUBIMAGE FAR*	LPTS_RT_NBSUBIMAGE;

#pragma	pack(pop)

#endif // _TRT_NBSUBIMAGE_H

//---------------------------------------------------------------------------//
// 文件名: SurveyCheckPara.h
// 说明:	测斜评价参数设置表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/3/18 23:46:03
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TSurveyCheckPara.h : DSurveyCheckPara

#ifndef	_TSURVEYCHECKPARA_H
#define	_TSURVEYCHECKPARA_H

#define	TID_SURVEYCHECKPARA								_T("SurveyCheckPara")
#define	OID_SURVEYCHECKPARA								_T("")

// Sort No
#define	SORT_SURVEYCHECKPARA_PK0				0							// PK:编号
#define	SORT_SURVEYCHECKPARA_PK1				1							// PK:趟钻编号
//#define	SORT_SURVEYCHECKPARA_A1							%#%							// A1:

// Colum No
#define	COL_SURVEYCHECKPARA_SURID					(short)0						// 编号
#define	COL_SURVEYCHECKPARA_RUNID					(short)1						// 趟钻编号
#define	COL_SURVEYCHECKPARA_MAGFTHEORETICAL					(short)2						// 磁场和逻辑值
#define	COL_SURVEYCHECKPARA_GRAVTHEORETICAL					(short)3						// 重力和逻辑值
#define	COL_SURVEYCHECKPARA_DIPATHEORETICAL					(short)4						// 磁倾角逻辑值
#define	COL_SURVEYCHECKPARA_MAGFFAC					(short)5						// 磁场和
#define	COL_SURVEYCHECKPARA_GRAVFAC					(short)6						// 重力和
#define	COL_SURVEYCHECKPARA_DIPAFAC					(short)7						// 磁倾角
#define	COL_SURVEYCHECKPARA_CREATETIME					(short)8						// 创建时间戳
#define	COL_SURVEYCHECKPARA_STATUS					(short)9						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_SURVEYCHECKPARA_MEMO					(short)10						// 备注
#define	COL_SURVEYCHECKPARA_UPDCOUNT					(short)11						// 更新计数

// Colum(Field) Name
#define	FLD_SURVEYCHECKPARA_SURID					_T("SurID")					// 编号
#define	FLD_SURVEYCHECKPARA_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_SURVEYCHECKPARA_MAGFTHEORETICAL					_T("MagFTheoretical")					// 磁场和逻辑值
#define	FLD_SURVEYCHECKPARA_GRAVTHEORETICAL					_T("GravTheoretical")					// 重力和逻辑值
#define	FLD_SURVEYCHECKPARA_DIPATHEORETICAL					_T("DipATheoretical")					// 磁倾角逻辑值
#define	FLD_SURVEYCHECKPARA_MAGFFAC					_T("MagFFAC")					// 磁场和
#define	FLD_SURVEYCHECKPARA_GRAVFAC					_T("GravFAC")					// 重力和
#define	FLD_SURVEYCHECKPARA_DIPAFAC					_T("DipAFAC")					// 磁倾角
#define	FLD_SURVEYCHECKPARA_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_SURVEYCHECKPARA_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_SURVEYCHECKPARA_MEMO					_T("Memo")					// 备注
#define	FLD_SURVEYCHECKPARA_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_SURVEYCHECKPARA_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_SURVEYCHECKPARA_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_SURVEYCHECKPARA
{
 
  int	iSurID;							// 编号
  char	szRunID[TV_SURVEYCHECKPARA_RUNID_DIGITS + 1];							// 趟钻编号
  float	fMagFTheoretical;							// 磁场和逻辑值
  float	fGravTheoretical;							// 重力和逻辑值
  float	fDipATheoretical;							// 磁倾角逻辑值
  float	fMagFFAC;							// 磁场和
  float	fGravFAC;							// 重力和
  float	fDipAFAC;							// 磁倾角
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_SURVEYCHECKPARA_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_SURVEYCHECKPARA;

typedef	TS_SURVEYCHECKPARA FAR*	LPTS_SURVEYCHECKPARA;

#pragma	pack(pop)

#endif // _TSURVEYCHECKPARA_H

//---------------------------------------------------------------------------//
// 文件名: ResCalcPara.h
// 说明:	电阻率计算相关参数设置表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2022/7/4 11:03:14
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TResCalcPara.h : DResCalcPara

#ifndef	_TRESCALCPARA_H
#define	_TRESCALCPARA_H

#define	TID_RESCALCPARA								_T("ResCalcPara")
#define	OID_RESCALCPARA								_T("")

// Sort No
#define	SORT_RESCALCPARA_PK0				0							// PK:编号
#define	SORT_RESCALCPARA_PK1				1							// PK:趟钻编号
//#define	SORT_RESCALCPARA_A1							%#%							// A1:

// Colum No
#define	COL_RESCALCPARA_RESID					(short)0						// 编号
#define	COL_RESCALCPARA_RUNID					(short)1						// 趟钻编号
#define	COL_RESCALCPARA_STARTDATETIME					(short)2						// 使用开始时间（精确到秒）
#define	COL_RESCALCPARA_FUNC					(short)3						// 功能函数
#define	COL_RESCALCPARA_PARAMETER					(short)4						// 参数
#define	COL_RESCALCPARA_CREATETIME					(short)5						// 创建时间戳
#define	COL_RESCALCPARA_STATUS					(short)6						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_RESCALCPARA_MEMO					(short)7						// 备注
#define	COL_RESCALCPARA_UPDCOUNT					(short)8						// 更新计数

// Colum(Field) Name
#define	FLD_RESCALCPARA_RESID					_T("ResID")					// 编号
#define	FLD_RESCALCPARA_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_RESCALCPARA_STARTDATETIME					_T("StartDateTime")					// 使用开始时间（精确到秒）
#define	FLD_RESCALCPARA_FUNC					_T("Func")					// 功能函数
#define	FLD_RESCALCPARA_PARAMETER					_T("Parameter")					// 参数
#define	FLD_RESCALCPARA_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_RESCALCPARA_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_RESCALCPARA_MEMO					_T("Memo")					// 备注
#define	FLD_RESCALCPARA_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_RESCALCPARA_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_RESCALCPARA_FUNC_DIGITS				50					// 功能函数位数
#define	TV_RESCALCPARA_PARAMETER_DIGITS				200					// 参数位数
#define	TV_RESCALCPARA_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_RESCALCPARA
{
 
  int	iResID;							// 编号
  char	szRunID[TV_RESCALCPARA_RUNID_DIGITS + 1];							// 趟钻编号
  time_t	lStartDateTime;							// 使用开始时间（精确到秒）
  char	szFunc[TV_RESCALCPARA_FUNC_DIGITS + 1];							// 功能函数
  char	szParameter[TV_RESCALCPARA_PARAMETER_DIGITS + 1];							// 参数
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_RESCALCPARA_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_RESCALCPARA;

typedef	TS_RESCALCPARA FAR*	LPTS_RESCALCPARA;

#pragma	pack(pop)

#endif // _TRESCALCPARA_H

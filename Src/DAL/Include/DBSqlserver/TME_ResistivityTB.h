//---------------------------------------------------------------------------//
// 文件名: ME_ResistivityTB.h
// 说明:	仪器内存数据表(电阻率）
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/10/13 14:53:33
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TME_ResistivityTB.h : DME_ResistivityTB

#ifndef	_TME_RESISTIVITYTB_H
#define	_TME_RESISTIVITYTB_H

#define	TID_ME_RESISTIVITYTB								_T("ME_ResistivityTB")
#define	OID_ME_RESISTIVITYTB								_T("")

// Sort No
#define	SORT_ME_RESISTIVITYTB_PK0				0							// PK:
//#define	SORT_ME_RESISTIVITYTB_A1							%#%							// A1:

// Colum No
#define	COL_ME_RESISTIVITYTB_DATAID					(short)0						// 
#define	COL_ME_RESISTIVITYTB_RUNID					(short)1						// 趟钻编号
#define	COL_ME_RESISTIVITYTB_TDATETIME					(short)2						// 时间（精确到秒）
#define	COL_ME_RESISTIVITYTB_MILLITIME					(short)3						// 毫秒
#define	COL_ME_RESISTIVITYTB_TOOLID					(short)4						// 工具编号
#define	COL_ME_RESISTIVITYTB_RESMDEPTH					(short)5						// 电阻率测量井深
#define	COL_ME_RESISTIVITYTB_RESVDEPTH					(short)6						// 电阻率测量垂深
#define	COL_ME_RESISTIVITYTB_T1R1A_400K					(short)7						// T1以400K发射, R1接收的幅度值
#define	COL_ME_RESISTIVITYTB_T1R2A_400K					(short)8						// T1以400K发射, R2接收的幅度值
#define	COL_ME_RESISTIVITYTB_T2R1A_400K					(short)9						// T2以400K发射, R1接收的幅度值
#define	COL_ME_RESISTIVITYTB_T2R2A_400K					(short)10						// T2以400K发射, R2接收的幅度值
#define	COL_ME_RESISTIVITYTB_T3R1A_400K					(short)11						// T3以400K发射, R1接收的幅度值
#define	COL_ME_RESISTIVITYTB_T3R2A_400K					(short)12						// T3以400K发射, R2接收的幅度值
#define	COL_ME_RESISTIVITYTB_T4R1A_400K					(short)13						// T4以400K发射, R1接收的幅度值
#define	COL_ME_RESISTIVITYTB_T4R2A_400K					(short)14						// T4以400K发射, R1接收的幅度值
#define	COL_ME_RESISTIVITYTB_T1R1A_2M					(short)15						// T1以2M发射, R1接收的幅度值
#define	COL_ME_RESISTIVITYTB_T1R2A_2M					(short)16						// T1以2M发射, R2接收的幅度值
#define	COL_ME_RESISTIVITYTB_T2R1A_2M					(short)17						// T2以2M发射, R1接收的幅度值
#define	COL_ME_RESISTIVITYTB_T2R2A_2M					(short)18						// T2以2M发射, R2接收的幅度值
#define	COL_ME_RESISTIVITYTB_T3R1A_2M					(short)19						// T3以2M发射, R1接收的幅度值
#define	COL_ME_RESISTIVITYTB_T3R2A_2M					(short)20						// T3以2M发射, R2接收的幅度值
#define	COL_ME_RESISTIVITYTB_T4R1A_2M					(short)21						// T4以2M发射, R1接收的幅度值
#define	COL_ME_RESISTIVITYTB_T4R2A_2M					(short)22						// T4以2M发射, R2接收的幅度值
#define	COL_ME_RESISTIVITYTB_T1R1P_400K					(short)23						// T1以400K发射, R1接收的相位
#define	COL_ME_RESISTIVITYTB_T1R2P_400K					(short)24						// T1以400K发射, R2接收的相位
#define	COL_ME_RESISTIVITYTB_T2R1P_400K					(short)25						// T2以400K发射, R1接收的相位
#define	COL_ME_RESISTIVITYTB_T2R2P_400K					(short)26						// T2以400K发射, R2接收的相位
#define	COL_ME_RESISTIVITYTB_T3R1P_400K					(short)27						// T3以400K发射, R1接收的相位
#define	COL_ME_RESISTIVITYTB_T3R2P_400K					(short)28						// T3以400K发射, R3接收的相位
#define	COL_ME_RESISTIVITYTB_T4R1P_400K					(short)29						// T4以400K发射, R1接收的相位
#define	COL_ME_RESISTIVITYTB_T4R2P_400K					(short)30						// T4以400K发射, R2接收的相位
#define	COL_ME_RESISTIVITYTB_T1R1P_2M					(short)31						// T1以2M发射, R1收的相位
#define	COL_ME_RESISTIVITYTB_T1R2P_2M					(short)32						// T1以2M发射, R2接收的相位
#define	COL_ME_RESISTIVITYTB_T2R1P_2M					(short)33						// T2以2M发射, R1接收的相位
#define	COL_ME_RESISTIVITYTB_T2R2P_2M					(short)34						// T2以2M发射, R1接收的相位
#define	COL_ME_RESISTIVITYTB_T3R1P_2M					(short)35						// T3以2M发射, R1接收的相位
#define	COL_ME_RESISTIVITYTB_T3R2P_2M					(short)36						// T3以2M发射, R2接收的相位
#define	COL_ME_RESISTIVITYTB_T4R1P_2M					(short)37						// T4以2M发射, R1接收的相位
#define	COL_ME_RESISTIVITYTB_T4R2P_2M					(short)38						// T4以2M发射, R2接收的相位
#define	COL_ME_RESISTIVITYTB_RX_TEMP					(short)39						// 接收板温度
#define	COL_ME_RESISTIVITYTB_UCRT1_400K					(short)40						// T1,400K,幅度比
#define	COL_ME_RESISTIVITYTB_UCRT2_400K					(short)41						// T2,400K,幅度比
#define	COL_ME_RESISTIVITYTB_UCRT3_400K					(short)42						// T3,400K,幅度比
#define	COL_ME_RESISTIVITYTB_UCRT4_400K					(short)43						// T4,400K,幅度比
#define	COL_ME_RESISTIVITYTB_UCRT1_2M					(short)44						// T1,2M,幅度比
#define	COL_ME_RESISTIVITYTB_UCRT2_2M					(short)45						// T2,2M,幅度比
#define	COL_ME_RESISTIVITYTB_UCRT3_2M					(short)46						// T3,2M,幅度比
#define	COL_ME_RESISTIVITYTB_UCRT4_2M					(short)47						// T4,2M,幅度比
#define	COL_ME_RESISTIVITYTB_UCPT1_400K					(short)48						// T1,400K,相位差
#define	COL_ME_RESISTIVITYTB_UCPT2_400K					(short)49						// T2,400K,相位差
#define	COL_ME_RESISTIVITYTB_UCPT3_400K					(short)50						// T3,400K,相位差
#define	COL_ME_RESISTIVITYTB_UCPT4_400K					(short)51						// T4,400K,相位差
#define	COL_ME_RESISTIVITYTB_UCPT1_2M					(short)52						// T1,2M,相位差
#define	COL_ME_RESISTIVITYTB_UCPT2_2M					(short)53						// T2,2M,相位差
#define	COL_ME_RESISTIVITYTB_UCPT3_2M					(short)54						// T3,2M,相位差
#define	COL_ME_RESISTIVITYTB_UCPT4_2M					(short)55						// T4,2M,相位差
#define	COL_ME_RESISTIVITYTB_AFL_400					(short)56						// 校正幅度比(长源/低频)
#define	COL_ME_RESISTIVITYTB_ANL_400					(short)57						// 校正幅度比(短源/低频)
#define	COL_ME_RESISTIVITYTB_AFH_2M					(short)58						// 校正幅度比(长源/高频)
#define	COL_ME_RESISTIVITYTB_ANH_2M					(short)59						// 校正幅度比(短源/高频)
#define	COL_ME_RESISTIVITYTB_PFL_400					(short)60						// 校正相位差(长源/低频)
#define	COL_ME_RESISTIVITYTB_PNL_400					(short)61						// 校正相位差(短源/低频)
#define	COL_ME_RESISTIVITYTB_PFH_2M					(short)62						// 校正相位差(长源/高频)
#define	COL_ME_RESISTIVITYTB_PNH_2M					(short)63						// 校正相位差(短源/高频)
#define	COL_ME_RESISTIVITYTB_RES_AFL					(short)64						// 视电阻率,校正幅度比(长源/低频)
#define	COL_ME_RESISTIVITYTB_RES_ANL					(short)65						// 视电阻率,校正幅度比(短源/低频)
#define	COL_ME_RESISTIVITYTB_RES_AFH					(short)66						// 视电阻率,校正幅度比(长源/高频)
#define	COL_ME_RESISTIVITYTB_RES_ANH					(short)67						// 视电阻率,校正幅度比(短源/高频)
#define	COL_ME_RESISTIVITYTB_RES_PFL					(short)68						// 视电阻率,校正相位差(长源/低频)
#define	COL_ME_RESISTIVITYTB_RES_PNL					(short)69						// 视电阻率,校正相位差(短源/低频)
#define	COL_ME_RESISTIVITYTB_RES_PFH					(short)70						// 视电阻率,校正相位差(长源/高频)
#define	COL_ME_RESISTIVITYTB_RES_PNH					(short)71						// 视电阻率,校正相位差(短源/高频)
#define	COL_ME_RESISTIVITYTB_RES_AFL_C					(short)72						// 视电阻率,校正幅度比(长源/低频)校正环境
#define	COL_ME_RESISTIVITYTB_RES_ANL_C					(short)73						// 视电阻率,校正幅度比(短源/低频)校正环境
#define	COL_ME_RESISTIVITYTB_RES_AFH_C					(short)74						// 视电阻率,校正幅度比(长源/高频)校正环境
#define	COL_ME_RESISTIVITYTB_RES_ANH_C					(short)75						// 视电阻率,校正幅度比(短源/高频)校正环境
#define	COL_ME_RESISTIVITYTB_RES_PFL_C					(short)76						// 视电阻率,校正相位差(长源/低频)校正环境
#define	COL_ME_RESISTIVITYTB_RES_PNL_C					(short)77						// 视电阻率,校正相位差(短源/低频)校正环境
#define	COL_ME_RESISTIVITYTB_RES_PFH_C					(short)78						// 视电阻率,校正相位差(长源/高频)校正环境
#define	COL_ME_RESISTIVITYTB_RES_PNH_C					(short)79						// 视电阻率,校正相位差(短源/高频)校正环境
#define	COL_ME_RESISTIVITYTB_AMPRE400K					(short)80						// 400K发射时方位天线接收实部最大值
#define	COL_ME_RESISTIVITYTB_AMPIM400K					(short)81						// 400K发射时方位天线接收虚部最大值
#define	COL_ME_RESISTIVITYTB_SECTORRE400					(short)82						// 400K发射时方位天线接收实部最大值所对应扇区0~15
#define	COL_ME_RESISTIVITYTB_SECTORIM400					(short)83						// 400K发射时方位天线接收虚部最大值所对应扇区0~15
#define	COL_ME_RESISTIVITYTB_AMPRE2M					(short)84						// 2M发射时方位天线接收实部最大值
#define	COL_ME_RESISTIVITYTB_AMPIM2M					(short)85						// 2M发射时方位天线接收虚部最大值
#define	COL_ME_RESISTIVITYTB_SECTORRE2M					(short)86						// 2M发射时方位天线接收实部最大值所对应扇区0~15
#define	COL_ME_RESISTIVITYTB_SECTORIM2M					(short)87						// 2M发射时方位天线接收虚部最大值所对应扇区0~15
#define	COL_ME_RESISTIVITYTB_NBRES					(short)88						// 近地层电阻率
#define	COL_ME_RESISTIVITYTB_RBRES					(short)89						// 远地层电阻率
#define	COL_ME_RESISTIVITYTB_D2RB					(short)90						// 界面距离
#define	COL_ME_RESISTIVITYTB_RBAZI					(short)91						// 界面方位
#define	COL_ME_RESISTIVITYTB_RUPRES					(short)92						// 实部上电阻率
#define	COL_ME_RESISTIVITYTB_RDOWNRES					(short)93						// 实部下电阻率
#define	COL_ME_RESISTIVITYTB_IUPRES					(short)94						// 虚部上电阻率
#define	COL_ME_RESISTIVITYTB_IDOWNRES					(short)95						// 虚部下电阻率
#define	COL_ME_RESISTIVITYTB_VOLTAGE5					(short)96						// 5V电压采样
#define	COL_ME_RESISTIVITYTB_VOLTAGE12					(short)97						// 12V电压采样
#define	COL_ME_RESISTIVITYTB_CONF					(short)98						// 旋转状态:No|0.否;Yes|1.是
#define	COL_ME_RESISTIVITYTB_BAD					(short)99						// 是否坏点:No|0.否;Yes|1.是
#define	COL_ME_RESISTIVITYTB_DRILLACTIV					(short)100						// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	COL_ME_RESISTIVITYTB_SLIDING					(short)101						// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	COL_ME_RESISTIVITYTB_RELOG					(short)102						// 是否复测 :Yes|0.否;No|1.是
#define	COL_ME_RESISTIVITYTB_LAS					(short)103						// 是否Las :Yes|0.否;No|1.是
#define	COL_ME_RESISTIVITYTB_CREATETIME					(short)104						// 创建时间戳
#define	COL_ME_RESISTIVITYTB_UPDTIME					(short)105						// 最后一次修改时间戳
#define	COL_ME_RESISTIVITYTB_STATUS					(short)106						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_ME_RESISTIVITYTB_MEMO					(short)107						// 备注

// Colum(Field) Name
#define	FLD_ME_RESISTIVITYTB_DATAID					_T("DataID")					// 
#define	FLD_ME_RESISTIVITYTB_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_ME_RESISTIVITYTB_TDATETIME					_T("TDateTime")					// 时间（精确到秒）
#define	FLD_ME_RESISTIVITYTB_MILLITIME					_T("Millitime")					// 毫秒
#define	FLD_ME_RESISTIVITYTB_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_ME_RESISTIVITYTB_RESMDEPTH					_T("ResMDepth")					// 电阻率测量井深
#define	FLD_ME_RESISTIVITYTB_RESVDEPTH					_T("ResVDepth")					// 电阻率测量垂深
#define	FLD_ME_RESISTIVITYTB_T1R1A_400K					_T("T1R1A_400K")					// T1以400K发射, R1接收的幅度值
#define	FLD_ME_RESISTIVITYTB_T1R2A_400K					_T("T1R2A_400K")					// T1以400K发射, R2接收的幅度值
#define	FLD_ME_RESISTIVITYTB_T2R1A_400K					_T("T2R1A_400K")					// T2以400K发射, R1接收的幅度值
#define	FLD_ME_RESISTIVITYTB_T2R2A_400K					_T("T2R2A_400K")					// T2以400K发射, R2接收的幅度值
#define	FLD_ME_RESISTIVITYTB_T3R1A_400K					_T("T3R1A_400K")					// T3以400K发射, R1接收的幅度值
#define	FLD_ME_RESISTIVITYTB_T3R2A_400K					_T("T3R2A_400K")					// T3以400K发射, R2接收的幅度值
#define	FLD_ME_RESISTIVITYTB_T4R1A_400K					_T("T4R1A_400K")					// T4以400K发射, R1接收的幅度值
#define	FLD_ME_RESISTIVITYTB_T4R2A_400K					_T("T4R2A_400K")					// T4以400K发射, R1接收的幅度值
#define	FLD_ME_RESISTIVITYTB_T1R1A_2M					_T("T1R1A_2M")					// T1以2M发射, R1接收的幅度值
#define	FLD_ME_RESISTIVITYTB_T1R2A_2M					_T("T1R2A_2M")					// T1以2M发射, R2接收的幅度值
#define	FLD_ME_RESISTIVITYTB_T2R1A_2M					_T("T2R1A_2M")					// T2以2M发射, R1接收的幅度值
#define	FLD_ME_RESISTIVITYTB_T2R2A_2M					_T("T2R2A_2M")					// T2以2M发射, R2接收的幅度值
#define	FLD_ME_RESISTIVITYTB_T3R1A_2M					_T("T3R1A_2M")					// T3以2M发射, R1接收的幅度值
#define	FLD_ME_RESISTIVITYTB_T3R2A_2M					_T("T3R2A_2M")					// T3以2M发射, R2接收的幅度值
#define	FLD_ME_RESISTIVITYTB_T4R1A_2M					_T("T4R1A_2M")					// T4以2M发射, R1接收的幅度值
#define	FLD_ME_RESISTIVITYTB_T4R2A_2M					_T("T4R2A_2M")					// T4以2M发射, R2接收的幅度值
#define	FLD_ME_RESISTIVITYTB_T1R1P_400K					_T("T1R1P_400K")					// T1以400K发射, R1接收的相位
#define	FLD_ME_RESISTIVITYTB_T1R2P_400K					_T("T1R2P_400K")					// T1以400K发射, R2接收的相位
#define	FLD_ME_RESISTIVITYTB_T2R1P_400K					_T("T2R1P_400K")					// T2以400K发射, R1接收的相位
#define	FLD_ME_RESISTIVITYTB_T2R2P_400K					_T("T2R2P_400K")					// T2以400K发射, R2接收的相位
#define	FLD_ME_RESISTIVITYTB_T3R1P_400K					_T("T3R1P_400K")					// T3以400K发射, R1接收的相位
#define	FLD_ME_RESISTIVITYTB_T3R2P_400K					_T("T3R2P_400K")					// T3以400K发射, R3接收的相位
#define	FLD_ME_RESISTIVITYTB_T4R1P_400K					_T("T4R1P_400K")					// T4以400K发射, R1接收的相位
#define	FLD_ME_RESISTIVITYTB_T4R2P_400K					_T("T4R2P_400K")					// T4以400K发射, R2接收的相位
#define	FLD_ME_RESISTIVITYTB_T1R1P_2M					_T("T1R1P_2M")					// T1以2M发射, R1收的相位
#define	FLD_ME_RESISTIVITYTB_T1R2P_2M					_T("T1R2P_2M")					// T1以2M发射, R2接收的相位
#define	FLD_ME_RESISTIVITYTB_T2R1P_2M					_T("T2R1P_2M")					// T2以2M发射, R1接收的相位
#define	FLD_ME_RESISTIVITYTB_T2R2P_2M					_T("T2R2P_2M")					// T2以2M发射, R1接收的相位
#define	FLD_ME_RESISTIVITYTB_T3R1P_2M					_T("T3R1P_2M")					// T3以2M发射, R1接收的相位
#define	FLD_ME_RESISTIVITYTB_T3R2P_2M					_T("T3R2P_2M")					// T3以2M发射, R2接收的相位
#define	FLD_ME_RESISTIVITYTB_T4R1P_2M					_T("T4R1P_2M")					// T4以2M发射, R1接收的相位
#define	FLD_ME_RESISTIVITYTB_T4R2P_2M					_T("T4R2P_2M")					// T4以2M发射, R2接收的相位
#define	FLD_ME_RESISTIVITYTB_RX_TEMP					_T("RX_Temp")					// 接收板温度
#define	FLD_ME_RESISTIVITYTB_UCRT1_400K					_T("UCRT1_400K")					// T1,400K,幅度比
#define	FLD_ME_RESISTIVITYTB_UCRT2_400K					_T("UCRT2_400K")					// T2,400K,幅度比
#define	FLD_ME_RESISTIVITYTB_UCRT3_400K					_T("UCRT3_400K")					// T3,400K,幅度比
#define	FLD_ME_RESISTIVITYTB_UCRT4_400K					_T("UCRT4_400K")					// T4,400K,幅度比
#define	FLD_ME_RESISTIVITYTB_UCRT1_2M					_T("UCRT1_2M")					// T1,2M,幅度比
#define	FLD_ME_RESISTIVITYTB_UCRT2_2M					_T("UCRT2_2M")					// T2,2M,幅度比
#define	FLD_ME_RESISTIVITYTB_UCRT3_2M					_T("UCRT3_2M")					// T3,2M,幅度比
#define	FLD_ME_RESISTIVITYTB_UCRT4_2M					_T("UCRT4_2M")					// T4,2M,幅度比
#define	FLD_ME_RESISTIVITYTB_UCPT1_400K					_T("UCPT1_400K")					// T1,400K,相位差
#define	FLD_ME_RESISTIVITYTB_UCPT2_400K					_T("UCPT2_400K")					// T2,400K,相位差
#define	FLD_ME_RESISTIVITYTB_UCPT3_400K					_T("UCPT3_400K")					// T3,400K,相位差
#define	FLD_ME_RESISTIVITYTB_UCPT4_400K					_T("UCPT4_400K")					// T4,400K,相位差
#define	FLD_ME_RESISTIVITYTB_UCPT1_2M					_T("UCPT1_2M")					// T1,2M,相位差
#define	FLD_ME_RESISTIVITYTB_UCPT2_2M					_T("UCPT2_2M")					// T2,2M,相位差
#define	FLD_ME_RESISTIVITYTB_UCPT3_2M					_T("UCPT3_2M")					// T3,2M,相位差
#define	FLD_ME_RESISTIVITYTB_UCPT4_2M					_T("UCPT4_2M")					// T4,2M,相位差
#define	FLD_ME_RESISTIVITYTB_AFL_400					_T("AFL_400")					// 校正幅度比(长源/低频)
#define	FLD_ME_RESISTIVITYTB_ANL_400					_T("ANL_400")					// 校正幅度比(短源/低频)
#define	FLD_ME_RESISTIVITYTB_AFH_2M					_T("AFH_2M")					// 校正幅度比(长源/高频)
#define	FLD_ME_RESISTIVITYTB_ANH_2M					_T("ANH_2M")					// 校正幅度比(短源/高频)
#define	FLD_ME_RESISTIVITYTB_PFL_400					_T("PFL_400")					// 校正相位差(长源/低频)
#define	FLD_ME_RESISTIVITYTB_PNL_400					_T("PNL_400")					// 校正相位差(短源/低频)
#define	FLD_ME_RESISTIVITYTB_PFH_2M					_T("PFH_2M")					// 校正相位差(长源/高频)
#define	FLD_ME_RESISTIVITYTB_PNH_2M					_T("PNH_2M")					// 校正相位差(短源/高频)
#define	FLD_ME_RESISTIVITYTB_RES_AFL					_T("Res_AFL")					// 视电阻率,校正幅度比(长源/低频)
#define	FLD_ME_RESISTIVITYTB_RES_ANL					_T("Res_ANL")					// 视电阻率,校正幅度比(短源/低频)
#define	FLD_ME_RESISTIVITYTB_RES_AFH					_T("Res_AFH")					// 视电阻率,校正幅度比(长源/高频)
#define	FLD_ME_RESISTIVITYTB_RES_ANH					_T("Res_ANH")					// 视电阻率,校正幅度比(短源/高频)
#define	FLD_ME_RESISTIVITYTB_RES_PFL					_T("Res_PFL")					// 视电阻率,校正相位差(长源/低频)
#define	FLD_ME_RESISTIVITYTB_RES_PNL					_T("Res_PNL")					// 视电阻率,校正相位差(短源/低频)
#define	FLD_ME_RESISTIVITYTB_RES_PFH					_T("Res_PFH")					// 视电阻率,校正相位差(长源/高频)
#define	FLD_ME_RESISTIVITYTB_RES_PNH					_T("Res_PNH")					// 视电阻率,校正相位差(短源/高频)
#define	FLD_ME_RESISTIVITYTB_RES_AFL_C					_T("Res_AFL_C")					// 视电阻率,校正幅度比(长源/低频)校正环境
#define	FLD_ME_RESISTIVITYTB_RES_ANL_C					_T("Res_ANL_C")					// 视电阻率,校正幅度比(短源/低频)校正环境
#define	FLD_ME_RESISTIVITYTB_RES_AFH_C					_T("Res_AFH_C")					// 视电阻率,校正幅度比(长源/高频)校正环境
#define	FLD_ME_RESISTIVITYTB_RES_ANH_C					_T("Res_ANH_C")					// 视电阻率,校正幅度比(短源/高频)校正环境
#define	FLD_ME_RESISTIVITYTB_RES_PFL_C					_T("Res_PFL_C")					// 视电阻率,校正相位差(长源/低频)校正环境
#define	FLD_ME_RESISTIVITYTB_RES_PNL_C					_T("Res_PNL_C")					// 视电阻率,校正相位差(短源/低频)校正环境
#define	FLD_ME_RESISTIVITYTB_RES_PFH_C					_T("Res_PFH_C")					// 视电阻率,校正相位差(长源/高频)校正环境
#define	FLD_ME_RESISTIVITYTB_RES_PNH_C					_T("Res_PNH_C")					// 视电阻率,校正相位差(短源/高频)校正环境
#define	FLD_ME_RESISTIVITYTB_AMPRE400K					_T("AmpRE400K")					// 400K发射时方位天线接收实部最大值
#define	FLD_ME_RESISTIVITYTB_AMPIM400K					_T("AmpIM400K")					// 400K发射时方位天线接收虚部最大值
#define	FLD_ME_RESISTIVITYTB_SECTORRE400					_T("SectorRe400")					// 400K发射时方位天线接收实部最大值所对应扇区0~15
#define	FLD_ME_RESISTIVITYTB_SECTORIM400					_T("SectorIm400")					// 400K发射时方位天线接收虚部最大值所对应扇区0~15
#define	FLD_ME_RESISTIVITYTB_AMPRE2M					_T("AmpRE2M")					// 2M发射时方位天线接收实部最大值
#define	FLD_ME_RESISTIVITYTB_AMPIM2M					_T("AmpIM2M")					// 2M发射时方位天线接收虚部最大值
#define	FLD_ME_RESISTIVITYTB_SECTORRE2M					_T("SectorRe2M")					// 2M发射时方位天线接收实部最大值所对应扇区0~15
#define	FLD_ME_RESISTIVITYTB_SECTORIM2M					_T("SectorIm2M")					// 2M发射时方位天线接收虚部最大值所对应扇区0~15
#define	FLD_ME_RESISTIVITYTB_NBRES					_T("NBRes")					// 近地层电阻率
#define	FLD_ME_RESISTIVITYTB_RBRES					_T("RBRes")					// 远地层电阻率
#define	FLD_ME_RESISTIVITYTB_D2RB					_T("D2RB")					// 界面距离
#define	FLD_ME_RESISTIVITYTB_RBAZI					_T("RBAZI")					// 界面方位
#define	FLD_ME_RESISTIVITYTB_RUPRES					_T("RUpRes")					// 实部上电阻率
#define	FLD_ME_RESISTIVITYTB_RDOWNRES					_T("RDownRes")					// 实部下电阻率
#define	FLD_ME_RESISTIVITYTB_IUPRES					_T("IUpRes")					// 虚部上电阻率
#define	FLD_ME_RESISTIVITYTB_IDOWNRES					_T("IDownRes")					// 虚部下电阻率
#define	FLD_ME_RESISTIVITYTB_VOLTAGE5					_T("Voltage5")					// 5V电压采样
#define	FLD_ME_RESISTIVITYTB_VOLTAGE12					_T("Voltage12")					// 12V电压采样
#define	FLD_ME_RESISTIVITYTB_CONF					_T("Conf")					// 旋转状态:No|0.否;Yes|1.是
#define	FLD_ME_RESISTIVITYTB_BAD					_T("Bad")					// 是否坏点:No|0.否;Yes|1.是
#define	FLD_ME_RESISTIVITYTB_DRILLACTIV					_T("DrillActiv")					// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	FLD_ME_RESISTIVITYTB_SLIDING					_T("Sliding")					// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	FLD_ME_RESISTIVITYTB_RELOG					_T("ReLog")					// 是否复测 :Yes|0.否;No|1.是
#define	FLD_ME_RESISTIVITYTB_LAS					_T("Las")					// 是否Las :Yes|0.否;No|1.是
#define	FLD_ME_RESISTIVITYTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_ME_RESISTIVITYTB_UPDTIME					_T("UpdTime")					// 最后一次修改时间戳
#define	FLD_ME_RESISTIVITYTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_ME_RESISTIVITYTB_MEMO					_T("Memo")					// 备注

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_ME_RESISTIVITYTB_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_ME_RESISTIVITYTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_ME_RESISTIVITYTB
{
 
  int	iDataID;							// 
  char	szRunID[TV_ME_RESISTIVITYTB_RUNID_DIGITS + 1];							// 趟钻编号
  time_t	lTDateTime;							// 时间（精确到秒）
  int	iMillitime;							// 毫秒
  int	iToolID;							// 工具编号
  long	lResMDepth;							// 电阻率测量井深
  long	lResVDepth;							// 电阻率测量垂深
  float	fT1R1A_400K;							// T1以400K发射, R1接收的幅度值
  float	fT1R2A_400K;							// T1以400K发射, R2接收的幅度值
  float	fT2R1A_400K;							// T2以400K发射, R1接收的幅度值
  float	fT2R2A_400K;							// T2以400K发射, R2接收的幅度值
  float	fT3R1A_400K;							// T3以400K发射, R1接收的幅度值
  float	fT3R2A_400K;							// T3以400K发射, R2接收的幅度值
  float	fT4R1A_400K;							// T4以400K发射, R1接收的幅度值
  float	fT4R2A_400K;							// T4以400K发射, R1接收的幅度值
  float	fT1R1A_2M;							// T1以2M发射, R1接收的幅度值
  float	fT1R2A_2M;							// T1以2M发射, R2接收的幅度值
  float	fT2R1A_2M;							// T2以2M发射, R1接收的幅度值
  float	fT2R2A_2M;							// T2以2M发射, R2接收的幅度值
  float	fT3R1A_2M;							// T3以2M发射, R1接收的幅度值
  float	fT3R2A_2M;							// T3以2M发射, R2接收的幅度值
  float	fT4R1A_2M;							// T4以2M发射, R1接收的幅度值
  float	fT4R2A_2M;							// T4以2M发射, R2接收的幅度值
  float	fT1R1P_400K;							// T1以400K发射, R1接收的相位
  float	fT1R2P_400K;							// T1以400K发射, R2接收的相位
  float	fT2R1P_400K;							// T2以400K发射, R1接收的相位
  float	fT2R2P_400K;							// T2以400K发射, R2接收的相位
  float	fT3R1P_400K;							// T3以400K发射, R1接收的相位
  float	fT3R2P_400K;							// T3以400K发射, R3接收的相位
  float	fT4R1P_400K;							// T4以400K发射, R1接收的相位
  float	fT4R2P_400K;							// T4以400K发射, R2接收的相位
  float	fT1R1P_2M;							// T1以2M发射, R1收的相位
  float	fT1R2P_2M;							// T1以2M发射, R2接收的相位
  float	fT2R1P_2M;							// T2以2M发射, R1接收的相位
  float	fT2R2P_2M;							// T2以2M发射, R1接收的相位
  float	fT3R1P_2M;							// T3以2M发射, R1接收的相位
  float	fT3R2P_2M;							// T3以2M发射, R2接收的相位
  float	fT4R1P_2M;							// T4以2M发射, R1接收的相位
  float	fT4R2P_2M;							// T4以2M发射, R2接收的相位
  float	fRX_Temp;							// 接收板温度
  float	fUCRT1_400K;							// T1,400K,幅度比
  float	fUCRT2_400K;							// T2,400K,幅度比
  float	fUCRT3_400K;							// T3,400K,幅度比
  float	fUCRT4_400K;							// T4,400K,幅度比
  float	fUCRT1_2M;							// T1,2M,幅度比
  float	fUCRT2_2M;							// T2,2M,幅度比
  float	fUCRT3_2M;							// T3,2M,幅度比
  float	fUCRT4_2M;							// T4,2M,幅度比
  float	fUCPT1_400K;							// T1,400K,相位差
  float	fUCPT2_400K;							// T2,400K,相位差
  float	fUCPT3_400K;							// T3,400K,相位差
  float	fUCPT4_400K;							// T4,400K,相位差
  float	fUCPT1_2M;							// T1,2M,相位差
  float	fUCPT2_2M;							// T2,2M,相位差
  float	fUCPT3_2M;							// T3,2M,相位差
  float	fUCPT4_2M;							// T4,2M,相位差
  float	fAFL_400;							// 校正幅度比(长源/低频)
  float	fANL_400;							// 校正幅度比(短源/低频)
  float	fAFH_2M;							// 校正幅度比(长源/高频)
  float	fANH_2M;							// 校正幅度比(短源/高频)
  float	fPFL_400;							// 校正相位差(长源/低频)
  float	fPNL_400;							// 校正相位差(短源/低频)
  float	fPFH_2M;							// 校正相位差(长源/高频)
  float	fPNH_2M;							// 校正相位差(短源/高频)
  float	fRes_AFL;							// 视电阻率,校正幅度比(长源/低频)
  float	fRes_ANL;							// 视电阻率,校正幅度比(短源/低频)
  float	fRes_AFH;							// 视电阻率,校正幅度比(长源/高频)
  float	fRes_ANH;							// 视电阻率,校正幅度比(短源/高频)
  float	fRes_PFL;							// 视电阻率,校正相位差(长源/低频)
  float	fRes_PNL;							// 视电阻率,校正相位差(短源/低频)
  float	fRes_PFH;							// 视电阻率,校正相位差(长源/高频)
  float	fRes_PNH;							// 视电阻率,校正相位差(短源/高频)
  float	fRes_AFL_C;							// 视电阻率,校正幅度比(长源/低频)校正环境
  float	fRes_ANL_C;							// 视电阻率,校正幅度比(短源/低频)校正环境
  float	fRes_AFH_C;							// 视电阻率,校正幅度比(长源/高频)校正环境
  float	fRes_ANH_C;							// 视电阻率,校正幅度比(短源/高频)校正环境
  float	fRes_PFL_C;							// 视电阻率,校正相位差(长源/低频)校正环境
  float	fRes_PNL_C;							// 视电阻率,校正相位差(短源/低频)校正环境
  float	fRes_PFH_C;							// 视电阻率,校正相位差(长源/高频)校正环境
  float	fRes_PNH_C;							// 视电阻率,校正相位差(短源/高频)校正环境
  float	fAmpRE400K;							// 400K发射时方位天线接收实部最大值
  float	fAmpIM400K;							// 400K发射时方位天线接收虚部最大值
  int	iSectorRe400;							// 400K发射时方位天线接收实部最大值所对应扇区0~15
  int	iSectorIm400;							// 400K发射时方位天线接收虚部最大值所对应扇区0~15
  float	fAmpRE2M;							// 2M发射时方位天线接收实部最大值
  float	fAmpIM2M;							// 2M发射时方位天线接收虚部最大值
  int	iSectorRe2M;							// 2M发射时方位天线接收实部最大值所对应扇区0~15
  int	iSectorIm2M;							// 2M发射时方位天线接收虚部最大值所对应扇区0~15
  float	fNBRes;							// 近地层电阻率
  float	fRBRes;							// 远地层电阻率
  float	fD2RB;							// 界面距离
  float	fRBAZI;							// 界面方位
  float	fRUpRes;							// 实部上电阻率
  float	fRDownRes;							// 实部下电阻率
  float	fIUpRes;							// 虚部上电阻率
  float	fIDownRes;							// 虚部下电阻率
  int	iVoltage5;							// 5V电压采样
  int	iVoltage12;							// 12V电压采样
  short	nConf;							// 旋转状态:No|0.否;Yes|1.是
  short	nBad;							// 是否坏点:No|0.否;Yes|1.是
  short	nDrillActiv;							// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
  short	nSliding;							// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
  short	nReLog;							// 是否复测 :Yes|0.否;No|1.是
  short	nLas;							// 是否Las :Yes|0.否;No|1.是
  time_t	lCreateTime;							// 创建时间戳
  time_t	lUpdTime;							// 最后一次修改时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_ME_RESISTIVITYTB_MEMO_DIGITS + 1];							// 备注
} TS_ME_RESISTIVITYTB;

typedef	TS_ME_RESISTIVITYTB FAR*	LPTS_ME_RESISTIVITYTB;

#pragma	pack(pop)

#endif // _TME_RESISTIVITYTB_H

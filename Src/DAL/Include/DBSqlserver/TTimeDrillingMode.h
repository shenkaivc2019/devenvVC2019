//---------------------------------------------------------------------------//
// 文件名: TimeDrillingMode.h
// 说明:	趟钻工作模式时间段存储数据表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020/10/19 22:11:35
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TTimeDrillingMode.h : DTimeDrillingMode

#ifndef	_TTIMEDRILLINGMODE_H
#define	_TTIMEDRILLINGMODE_H

#define	TID_TIMEDRILLINGMODE								_T("TimeDrillingMode")
#define	OID_TIMEDRILLINGMODE								_T("")

// Sort No
#define	SORT_TIMEDRILLINGMODE_PK0				0							// PK:编号
//#define	SORT_TIMEDRILLINGMODE_A1							%#%							// A1:

// Colum No
#define	COL_TIMEDRILLINGMODE_TDMID					(short)0						// 编号
#define	COL_TIMEDRILLINGMODE_PROID					(short)1						// 工程ID 使用全球唯一字符串UUID
#define	COL_TIMEDRILLINGMODE_STARTTIME					(short)2						// 创建时间戳
#define	COL_TIMEDRILLINGMODE_SMILLITIME					(short)3						// 毫秒
#define	COL_TIMEDRILLINGMODE_ENDTIME					(short)4						// 创建时间戳
#define	COL_TIMEDRILLINGMODE_EMILLITIME					(short)5						// 毫秒
#define	COL_TIMEDRILLINGMODE_DRILLINGMODE					(short)6						// 趟钻工作模式:Rotate Centered|1.中心旋转;Rotate Off Centered|2.偏心旋转;Slide Off Centered|3.偏心滑行;Pull Out of Hole|4.抽出井眼;Slide Centered|5.中心滑行;
#define	COL_TIMEDRILLINGMODE_CREATETIME					(short)7						// 创建时间戳
#define	COL_TIMEDRILLINGMODE_STATUS					(short)8						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_TIMEDRILLINGMODE_MEMO					(short)9						// 备注
#define	COL_TIMEDRILLINGMODE_UPDCOUNT					(short)10						// 更新计数

// Colum(Field) Name
#define	FLD_TIMEDRILLINGMODE_TDMID					_T("TDMID")					// 编号
#define	FLD_TIMEDRILLINGMODE_PROID					_T("ProID")					// 工程ID 使用全球唯一字符串UUID
#define	FLD_TIMEDRILLINGMODE_STARTTIME					_T("StartTime")					// 创建时间戳
#define	FLD_TIMEDRILLINGMODE_SMILLITIME					_T("SMillitime")					// 毫秒
#define	FLD_TIMEDRILLINGMODE_ENDTIME					_T("EndTime")					// 创建时间戳
#define	FLD_TIMEDRILLINGMODE_EMILLITIME					_T("EMillitime")					// 毫秒
#define	FLD_TIMEDRILLINGMODE_DRILLINGMODE					_T("DrillingMode")					// 趟钻工作模式:Rotate Centered|1.中心旋转;Rotate Off Centered|2.偏心旋转;Slide Off Centered|3.偏心滑行;Pull Out of Hole|4.抽出井眼;Slide Centered|5.中心滑行;
#define	FLD_TIMEDRILLINGMODE_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_TIMEDRILLINGMODE_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_TIMEDRILLINGMODE_MEMO					_T("Memo")					// 备注
#define	FLD_TIMEDRILLINGMODE_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_TIMEDRILLINGMODE_PROID_DIGITS				50					// 工程ID 使用全球唯一字符串UUID位数
#define	TV_TIMEDRILLINGMODE_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_TIMEDRILLINGMODE
{
 
  int	iTDMID;							// 编号
  char	szProID[TV_TIMEDRILLINGMODE_PROID_DIGITS + 1];							// 工程ID 使用全球唯一字符串UUID
  time_t	lStartTime;							// 创建时间戳
  int	iSMillitime;							// 毫秒
  time_t	lEndTime;							// 创建时间戳
  int	iEMillitime;							// 毫秒
  short	nDrillingMode;							// 趟钻工作模式:Rotate Centered|1.中心旋转;Rotate Off Centered|2.偏心旋转;Slide Off Centered|3.偏心滑行;Pull Out of Hole|4.抽出井眼;Slide Centered|5.中心滑行;
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_TIMEDRILLINGMODE_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_TIMEDRILLINGMODE;

typedef	TS_TIMEDRILLINGMODE FAR*	LPTS_TIMEDRILLINGMODE;

#pragma	pack(pop)

#endif // _TTIMEDRILLINGMODE_H

//---------------------------------------------------------------------------//
// 文件名: CTF2R41P.h
// 说明:	CTF电阻率2R41P对应校正电阻率参照表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/9/9 15:10:26
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TCTF2R41P.h : DCTF2R41P

#ifndef	_TCTF2R41P_H
#define	_TCTF2R41P_H

#define	TID_CTF2R41P								_T("CTF2R41P")
#define	OID_CTF2R41P								_T("")

// Sort No
#define	SORT_CTF2R41P_PK0				0							// PK:自增字段
//#define	SORT_CTF2R41P_A1							%#%							// A1:

// Colum No
#define	COL_CTF2R41P_RESID					(short)0						// 自增字段
#define	COL_CTF2R41P_TOOLSIZE					(short)1						// 工具尺寸
#define	COL_CTF2R41P_APPARENTRESISTIVITY					(short)2						// 视电阻率
#define	COL_CTF2R41P_MUDRESISTIVITY					(short)3						// 泥浆电阻率
#define	COL_CTF2R41P_HOLESIZE					(short)4						// 井眼尺寸
#define	COL_CTF2R41P_COEFFICIENT					(short)5						// 系数
#define	COL_CTF2R41P_CALCULATED					(short)6						// 计算
#define	COL_CTF2R41P_CREATETIME					(short)7						// 创建时间戳
#define	COL_CTF2R41P_STATUS					(short)8						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_CTF2R41P_MEMO					(short)9						// 备注
#define	COL_CTF2R41P_UPDCOUNT					(short)10						// 更新计数

// Colum(Field) Name
#define	FLD_CTF2R41P_RESID					_T("ResID")					// 自增字段
#define	FLD_CTF2R41P_TOOLSIZE					_T("ToolSize")					// 工具尺寸
#define	FLD_CTF2R41P_APPARENTRESISTIVITY					_T("ApparentResistivity")					// 视电阻率
#define	FLD_CTF2R41P_MUDRESISTIVITY					_T("MudResistivity")					// 泥浆电阻率
#define	FLD_CTF2R41P_HOLESIZE					_T("HoleSize")					// 井眼尺寸
#define	FLD_CTF2R41P_COEFFICIENT					_T("Coefficient")					// 系数
#define	FLD_CTF2R41P_CALCULATED					_T("Calculated")					// 计算
#define	FLD_CTF2R41P_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_CTF2R41P_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_CTF2R41P_MEMO					_T("Memo")					// 备注
#define	FLD_CTF2R41P_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_CTF2R41P_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_CTF2R41P
{
 
  int	iResID;							// 自增字段
  float	fToolSize;							// 工具尺寸
  float	fApparentResistivity;							// 视电阻率
  float	fMudResistivity;							// 泥浆电阻率
  float	fHoleSize;							// 井眼尺寸
  float	fCoefficient;							// 系数
  float	fCalculated;							// 计算
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_CTF2R41P_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_CTF2R41P;

typedef	TS_CTF2R41P FAR*	LPTS_CTF2R41P;

#pragma	pack(pop)

#endif // _TCTF2R41P_H

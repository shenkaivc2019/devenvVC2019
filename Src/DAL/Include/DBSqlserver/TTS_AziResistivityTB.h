//---------------------------------------------------------------------------//
// 文件名: TS_AziResistivityTB.h
// 说明:	TS_AziResistivityTB
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020-05-30 17:47:33
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TTS_AziResistivityTB.h : DTS_AziResistivityTB

#ifndef	_TTS_AZIRESISTIVITYTB_H
#define	_TTS_AZIRESISTIVITYTB_H

#define	TID_TS_AZIRESISTIVITYTB								_T("TS_AziResistivityTB")
#define	OID_TS_AZIRESISTIVITYTB								_T("")

// Sort No

//#define	SORT_TS_AZIRESISTIVITYTB_A1							%#%							// A1:

// Colum No
#define	COL_TS_AZIRESISTIVITYTB_AZIRST_ID					(short)0						// 自增ID
#define	COL_TS_AZIRESISTIVITYTB_AZIDATATIME					(short)1						// 时间
#define	COL_TS_AZIRESISTIVITYTB_AMRE400K					(short)2						// AmRe400K
#define	COL_TS_AZIRESISTIVITYTB_AMIM400K					(short)3						// AmIm400K
#define	COL_TS_AZIRESISTIVITYTB_AMPRE2M					(short)4						// AmpRe2M
#define	COL_TS_AZIRESISTIVITYTB_AMPIM2M					(short)5						// AmpIm2M
#define	COL_TS_AZIRESISTIVITYTB_SECTORRE400K					(short)6						// SectorRe400K
#define	COL_TS_AZIRESISTIVITYTB_SECTORIM400K					(short)7						// SectorIm400K
#define	COL_TS_AZIRESISTIVITYTB_SECTORRE2M					(short)8						// SectorRe2M
#define	COL_TS_AZIRESISTIVITYTB_SECTORIM2M					(short)9						// SectorIm2M

// Colum(Field) Name
#define	FLD_TS_AZIRESISTIVITYTB_AZIRST_ID					_T("AziRst_Id")					// 自增ID
#define	FLD_TS_AZIRESISTIVITYTB_AZIDATATIME					_T("AziDataTime")					// 时间
#define	FLD_TS_AZIRESISTIVITYTB_AMRE400K					_T("AmRe400K")					// AmRe400K
#define	FLD_TS_AZIRESISTIVITYTB_AMIM400K					_T("AmIm400K")					// AmIm400K
#define	FLD_TS_AZIRESISTIVITYTB_AMPRE2M					_T("AmpRe2M")					// AmpRe2M
#define	FLD_TS_AZIRESISTIVITYTB_AMPIM2M					_T("AmpIm2M")					// AmpIm2M
#define	FLD_TS_AZIRESISTIVITYTB_SECTORRE400K					_T("SectorRe400K")					// SectorRe400K
#define	FLD_TS_AZIRESISTIVITYTB_SECTORIM400K					_T("SectorIm400K")					// SectorIm400K
#define	FLD_TS_AZIRESISTIVITYTB_SECTORRE2M					_T("SectorRe2M")					// SectorRe2M
#define	FLD_TS_AZIRESISTIVITYTB_SECTORIM2M					_T("SectorIm2M")					// SectorIm2M

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_TS_AZIRESISTIVITYTB_AMRE400K_DIGITS				64					// AmRe400K位数
#define	TV_TS_AZIRESISTIVITYTB_AMIM400K_DIGITS				64					// AmIm400K位数
#define	TV_TS_AZIRESISTIVITYTB_AMPRE2M_DIGITS				64					// AmpRe2M位数
#define	TV_TS_AZIRESISTIVITYTB_AMPIM2M_DIGITS				64					// AmpIm2M位数
#define	TV_TS_AZIRESISTIVITYTB_SECTORRE400K_DIGITS				64					// SectorRe400K位数
#define	TV_TS_AZIRESISTIVITYTB_SECTORIM400K_DIGITS				64					// SectorIm400K位数
#define	TV_TS_AZIRESISTIVITYTB_SECTORRE2M_DIGITS				64					// SectorRe2M位数
#define	TV_TS_AZIRESISTIVITYTB_SECTORIM2M_DIGITS				64					// SectorIm2M位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_TS_AZIRESISTIVITYTB
{
 
  int	iAziRst_Id;							// 自增ID
  time_t	lAziDataTime;							// 时间
  char	szAmRe400K[TV_TS_AZIRESISTIVITYTB_AMRE400K_DIGITS + 1];							// AmRe400K
  char	szAmIm400K[TV_TS_AZIRESISTIVITYTB_AMIM400K_DIGITS + 1];							// AmIm400K
  char	szAmpRe2M[TV_TS_AZIRESISTIVITYTB_AMPRE2M_DIGITS + 1];							// AmpRe2M
  char	szAmpIm2M[TV_TS_AZIRESISTIVITYTB_AMPIM2M_DIGITS + 1];							// AmpIm2M
  char	szSectorRe400K[TV_TS_AZIRESISTIVITYTB_SECTORRE400K_DIGITS + 1];							// SectorRe400K
  char	szSectorIm400K[TV_TS_AZIRESISTIVITYTB_SECTORIM400K_DIGITS + 1];							// SectorIm400K
  char	szSectorRe2M[TV_TS_AZIRESISTIVITYTB_SECTORRE2M_DIGITS + 1];							// SectorRe2M
  char	szSectorIm2M[TV_TS_AZIRESISTIVITYTB_SECTORIM2M_DIGITS + 1];							// SectorIm2M
} TS_TS_AZIRESISTIVITYTB;

typedef	TS_TS_AZIRESISTIVITYTB FAR*	LPTS_TS_AZIRESISTIVITYTB;

#pragma	pack(pop)

#endif // _TTS_AZIRESISTIVITYTB_H

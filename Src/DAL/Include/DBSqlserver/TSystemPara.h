//---------------------------------------------------------------------------//
// 文件名: SystemPara.h
// 说明:	系统参数设置表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/2/2 16:56:34
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TSystemPara.h : DSystemPara

#ifndef	_TSYSTEMPARA_H
#define	_TSYSTEMPARA_H

#define	TID_SYSTEMPARA								_T("SystemPara")
#define	OID_SYSTEMPARA								_T("")

// Sort No
#define	SORT_SYSTEMPARA_PK0				0							// PK:编号
//#define	SORT_SYSTEMPARA_A1							%#%							// A1:

// Colum No
#define	COL_SYSTEMPARA_SYSID					(short)0						// 编号
#define	COL_SYSTEMPARA_DBV					(short)1						// 趟钻编号
#define	COL_SYSTEMPARA_GRTCALCMODE					(short)2						// 伽马和计算方式:FromUp|1.从上伽马开始计算;Down|2.从下伽马开始计算
#define	COL_SYSTEMPARA_CREATETIME					(short)3						// 创建时间戳
#define	COL_SYSTEMPARA_STATUS					(short)4						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_SYSTEMPARA_MEMO					(short)5						// 备注
#define	COL_SYSTEMPARA_UPDCOUNT					(short)6						// 更新计数

// Colum(Field) Name
#define	FLD_SYSTEMPARA_SYSID					_T("SysID")					// 编号
#define	FLD_SYSTEMPARA_DBV					_T("DBV")					// 趟钻编号
#define	FLD_SYSTEMPARA_GRTCALCMODE					_T("GRTCalcMode")					// 伽马和计算方式:FromUp|1.从上伽马开始计算;Down|2.从下伽马开始计算
#define	FLD_SYSTEMPARA_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_SYSTEMPARA_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_SYSTEMPARA_MEMO					_T("Memo")					// 备注
#define	FLD_SYSTEMPARA_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_SYSTEMPARA_DBV_DIGITS				50					// 趟钻编号位数
#define	TV_SYSTEMPARA_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_SYSTEMPARA
{
 
  int	iSysID;							// 编号
  char	szDBV[TV_SYSTEMPARA_DBV_DIGITS + 1];							// 趟钻编号
  short	nGRTCalcMode;							// 伽马和计算方式:FromUp|1.从上伽马开始计算;Down|2.从下伽马开始计算
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_SYSTEMPARA_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_SYSTEMPARA;

typedef	TS_SYSTEMPARA FAR*	LPTS_SYSTEMPARA;

#pragma	pack(pop)

#endif // _TSYSTEMPARA_H

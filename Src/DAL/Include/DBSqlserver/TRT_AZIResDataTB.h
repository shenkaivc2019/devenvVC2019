//---------------------------------------------------------------------------//
// 文件名: RT_AZIResDataTB.h
// 说明:	实时方位电阻率数据表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/10/9 15:57:05
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TRT_AZIResDataTB.h : DRT_AZIResDataTB

#ifndef	_TRT_AZIRESDATATB_H
#define	_TRT_AZIRESDATATB_H

#define	TID_RT_AZIRESDATATB								_T("RT_AZIResDataTB")
#define	OID_RT_AZIRESDATATB								_T("")

// Sort No
#define	SORT_RT_AZIRESDATATB_PK0				0							// PK:数据编号
//#define	SORT_RT_AZIRESDATATB_A1							%#%							// A1:

// Colum No
#define	COL_RT_AZIRESDATATB_DATAID					(short)0						// 数据编号
#define	COL_RT_AZIRESDATATB_RUNID					(short)1						// 趟钻编号
#define	COL_RT_AZIRESDATATB_TDATETIME					(short)2						// 时间（精确到秒）
#define	COL_RT_AZIRESDATATB_MILLITIME					(short)3						// 毫秒
#define	COL_RT_AZIRESDATATB_TOOLID					(short)4						// 工具编号
#define	COL_RT_AZIRESDATATB_AZIRESMDEPTH					(short)5						// 方位电阻率测量点井深
#define	COL_RT_AZIRESDATATB_AZIRESVDEPTH					(short)6						// 方位电阻率测量点垂深
#define	COL_RT_AZIRESDATATB_PREVDEPTH					(short)7						// 测量点预测垂深
#define	COL_RT_AZIRESDATATB_CURVENAME					(short)8						// 曲线名称
#define	COL_RT_AZIRESDATATB_WITSID					(short)9						// WITSID
#define	COL_RT_AZIRESDATATB_VALUER					(short)10						// 原始数据值
#define	COL_RT_AZIRESDATATB_VALUEM					(short)11						// 编辑数据值
#define	COL_RT_AZIRESDATATB_CONF					(short)12						// 可信度
#define	COL_RT_AZIRESDATATB_BAD					(short)13						// 是否坏点:No|0.否;Yes|1.是
#define	COL_RT_AZIRESDATATB_DRILLACTIV					(short)14						// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	COL_RT_AZIRESDATATB_SLIDING					(short)15						// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	COL_RT_AZIRESDATATB_RELOG					(short)16						// 是否复测 :Yes|0.否;No|1.是
#define	COL_RT_AZIRESDATATB_LAS					(short)17						// 是否Las :No|0.否;Yes|1.是
#define	COL_RT_AZIRESDATATB_CREATETIME					(short)18						// 创建时间戳
#define	COL_RT_AZIRESDATATB_UPDTIME					(short)19						// 最后一次修改时间戳
#define	COL_RT_AZIRESDATATB_STATUS					(short)20						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_RT_AZIRESDATATB_MEMO					(short)21						// 备注

// Colum(Field) Name
#define	FLD_RT_AZIRESDATATB_DATAID					_T("DataID")					// 数据编号
#define	FLD_RT_AZIRESDATATB_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_RT_AZIRESDATATB_TDATETIME					_T("TDateTime")					// 时间（精确到秒）
#define	FLD_RT_AZIRESDATATB_MILLITIME					_T("Millitime")					// 毫秒
#define	FLD_RT_AZIRESDATATB_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_RT_AZIRESDATATB_AZIRESMDEPTH					_T("AZIResMDepth")					// 方位电阻率测量点井深
#define	FLD_RT_AZIRESDATATB_AZIRESVDEPTH					_T("AZIResVDepth")					// 方位电阻率测量点垂深
#define	FLD_RT_AZIRESDATATB_PREVDEPTH					_T("PreVDepth")					// 测量点预测垂深
#define	FLD_RT_AZIRESDATATB_CURVENAME					_T("CurveName")					// 曲线名称
#define	FLD_RT_AZIRESDATATB_WITSID					_T("WITSID")					// WITSID
#define	FLD_RT_AZIRESDATATB_VALUER					_T("ValueR")					// 原始数据值
#define	FLD_RT_AZIRESDATATB_VALUEM					_T("ValueM")					// 编辑数据值
#define	FLD_RT_AZIRESDATATB_CONF					_T("Conf")					// 可信度
#define	FLD_RT_AZIRESDATATB_BAD					_T("Bad")					// 是否坏点:No|0.否;Yes|1.是
#define	FLD_RT_AZIRESDATATB_DRILLACTIV					_T("DrillActiv")					// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	FLD_RT_AZIRESDATATB_SLIDING					_T("Sliding")					// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	FLD_RT_AZIRESDATATB_RELOG					_T("ReLog")					// 是否复测 :Yes|0.否;No|1.是
#define	FLD_RT_AZIRESDATATB_LAS					_T("Las")					// 是否Las :No|0.否;Yes|1.是
#define	FLD_RT_AZIRESDATATB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_RT_AZIRESDATATB_UPDTIME					_T("UpdTime")					// 最后一次修改时间戳
#define	FLD_RT_AZIRESDATATB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_RT_AZIRESDATATB_MEMO					_T("Memo")					// 备注

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_RT_AZIRESDATATB_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_RT_AZIRESDATATB_CURVENAME_DIGITS				50					// 曲线名称位数
#define	TV_RT_AZIRESDATATB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_RT_AZIRESDATATB
{
 
  long	lDataID;							// 数据编号
  char	szRunID[TV_RT_AZIRESDATATB_RUNID_DIGITS + 1];							// 趟钻编号
  time_t	lTDateTime;							// 时间（精确到秒）
  int	iMillitime;							// 毫秒
  int	iToolID;							// 工具编号
  long	lAZIResMDepth;							// 方位电阻率测量点井深
  long	lAZIResVDepth;							// 方位电阻率测量点垂深
  long	lPreVDepth;							// 测量点预测垂深
  char	szCurveName[TV_RT_AZIRESDATATB_CURVENAME_DIGITS + 1];							// 曲线名称
  int	iWITSID;							// WITSID
  double	dValueR;							// 原始数据值
  double	dValueM;							// 编辑数据值
  short	nConf;							// 可信度
  short	nBad;							// 是否坏点:No|0.否;Yes|1.是
  short	nDrillActiv;							// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
  short	nSliding;							// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
  short	nReLog;							// 是否复测 :Yes|0.否;No|1.是
  short	nLas;							// 是否Las :No|0.否;Yes|1.是
  time_t	lCreateTime;							// 创建时间戳
  time_t	lUpdTime;							// 最后一次修改时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_RT_AZIRESDATATB_MEMO_DIGITS + 1];							// 备注
} TS_RT_AZIRESDATATB;

typedef	TS_RT_AZIRESDATATB FAR*	LPTS_RT_AZIRESDATATB;

#pragma	pack(pop)

#endif // _TRT_AZIRESDATATB_H

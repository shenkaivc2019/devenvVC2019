//---------------------------------------------------------------------------//
// 文件名: EX_CurveInfo.h
// 说明:	扩展曲线信息表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/10/9 15:57:06
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TEX_CurveInfo.h : DEX_CurveInfo

#ifndef	_TEX_CURVEINFO_H
#define	_TEX_CURVEINFO_H

#define	TID_EX_CURVEINFO								_T("EX_CurveInfo")
#define	OID_EX_CURVEINFO								_T("")

// Sort No
#define	SORT_EX_CURVEINFO_PK1				1							// PK:曲线名称
//#define	SORT_EX_CURVEINFO_A1							%#%							// A1:

// Colum No
#define	COL_EX_CURVEINFO_CURVID					(short)0						// 曲线编号(WITS表对应字段)
#define	COL_EX_CURVEINFO_CURVENAME					(short)1						// 曲线名称
#define	COL_EX_CURVEINFO_CCID					(short)2						// 曲线分类编号
#define	COL_EX_CURVEINFO_WITSID					(short)3						// WITS表对应字段
#define	COL_EX_CURVEINFO_TOOLID					(short)4						// 工具编号
#define	COL_EX_CURVEINFO_MPID					(short)5						// 测量点编号
#define	COL_EX_CURVEINFO_UTYPE					(short)6						// 曲线单位类型 对应UnitTB表的UType字段 创建时选择输入
#define	COL_EX_CURVEINFO_UID					(short)7						// 数据库中保存的单位ID，如果界面没有设置显示单位，则作为缺省界面的显示单位
#define	COL_EX_CURVEINFO_SENSORTOBIT					(short)8						// 深度偏移(该曲线测点距离钻头的距离) 该值自动计算=MeasPointInfoTB.SensorToolToBot + BHAInfoDTB.ToolBotToBit
#define	COL_EX_CURVEINFO_CURVEMIN					(short)9						// 最小值
#define	COL_EX_CURVEINFO_CURVEMAX					(short)10						// 最大值
#define	COL_EX_CURVEINFO_WARNMIN					(short)11						// 预警最小值 显示共色线
#define	COL_EX_CURVEINFO_WARNMAX					(short)12						// 预警最大值 显示共色线
#define	COL_EX_CURVEINFO_ERRORMIN					(short)13						// 错误最小值 显示红色线
#define	COL_EX_CURVEINFO_ERRORMAX					(short)14						// 错误最大值 显示红色线
#define	COL_EX_CURVEINFO_INVPOINTVAL					(short)15						// 无效点值
#define	COL_EX_CURVEINFO_FILTER					(short)16						// 滤波
#define	COL_EX_CURVEINFO_TABLENAME					(short)17						// 对应存储表名
#define	COL_EX_CURVEINFO_FIELDNAME					(short)18						// 对应存储字段名
#define	COL_EX_CURVEINFO_DATATYPE					(short)19						// WITS数据类型:A|65.字符型;L|73.LONG;S|79.S;F|128.浮点型
#define	COL_EX_CURVEINFO_DIMENSION					(short)20						// 维数(常规曲线1，阵列曲线2)
#define	COL_EX_CURVEINFO_POINTCOUNT					(short)21						// 点数(常规曲线1)
#define	COL_EX_CURVEINFO_DESCRIPTION_EN					(short)22						// 参数英文描述
#define	COL_EX_CURVEINFO_DESCRIPTION_CN					(short)23						// 参数中文描述
#define	COL_EX_CURVEINFO_CURVENAMEM					(short)24						// 当前对应内存曲线名称
#define	COL_EX_CURVEINFO_SORDER					(short)25						// 排序
#define	COL_EX_CURVEINFO_ISCALC					(short)26						// 是否计算字段:NO|0.否;Yes|1.是
#define	COL_EX_CURVEINFO_CREATETIME					(short)27						// 创建时间戳
#define	COL_EX_CURVEINFO_UPDTIME					(short)28						// 最后一次修改时间戳
#define	COL_EX_CURVEINFO_STATUS					(short)29						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_EX_CURVEINFO_MEMO					(short)30						// 备注
#define	COL_EX_CURVEINFO_UPDCOUNT					(short)31						// 更新计数

// Colum(Field) Name
#define	FLD_EX_CURVEINFO_CURVID					_T("CurvID")					// 曲线编号(WITS表对应字段)
#define	FLD_EX_CURVEINFO_CURVENAME					_T("CurveName")					// 曲线名称
#define	FLD_EX_CURVEINFO_CCID					_T("CCID")					// 曲线分类编号
#define	FLD_EX_CURVEINFO_WITSID					_T("WITSID")					// WITS表对应字段
#define	FLD_EX_CURVEINFO_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_EX_CURVEINFO_MPID					_T("MPID")					// 测量点编号
#define	FLD_EX_CURVEINFO_UTYPE					_T("UType")					// 曲线单位类型 对应UnitTB表的UType字段 创建时选择输入
#define	FLD_EX_CURVEINFO_UID					_T("UID")					// 数据库中保存的单位ID，如果界面没有设置显示单位，则作为缺省界面的显示单位
#define	FLD_EX_CURVEINFO_SENSORTOBIT					_T("SensorToBit")					// 深度偏移(该曲线测点距离钻头的距离) 该值自动计算=MeasPointInfoTB.SensorToolToBot + BHAInfoDTB.ToolBotToBit
#define	FLD_EX_CURVEINFO_CURVEMIN					_T("CurveMin")					// 最小值
#define	FLD_EX_CURVEINFO_CURVEMAX					_T("CurveMax")					// 最大值
#define	FLD_EX_CURVEINFO_WARNMIN					_T("WarnMin")					// 预警最小值 显示共色线
#define	FLD_EX_CURVEINFO_WARNMAX					_T("WarnMax")					// 预警最大值 显示共色线
#define	FLD_EX_CURVEINFO_ERRORMIN					_T("ErrorMin")					// 错误最小值 显示红色线
#define	FLD_EX_CURVEINFO_ERRORMAX					_T("ErrorMax")					// 错误最大值 显示红色线
#define	FLD_EX_CURVEINFO_INVPOINTVAL					_T("InvPointVal")					// 无效点值
#define	FLD_EX_CURVEINFO_FILTER					_T("Filter")					// 滤波
#define	FLD_EX_CURVEINFO_TABLENAME					_T("TableName")					// 对应存储表名
#define	FLD_EX_CURVEINFO_FIELDNAME					_T("FieldName")					// 对应存储字段名
#define	FLD_EX_CURVEINFO_DATATYPE					_T("DataType")					// WITS数据类型:A|65.字符型;L|73.LONG;S|79.S;F|128.浮点型
#define	FLD_EX_CURVEINFO_DIMENSION					_T("Dimension")					// 维数(常规曲线1，阵列曲线2)
#define	FLD_EX_CURVEINFO_POINTCOUNT					_T("PointCount")					// 点数(常规曲线1)
#define	FLD_EX_CURVEINFO_DESCRIPTION_EN					_T("Description_en")					// 参数英文描述
#define	FLD_EX_CURVEINFO_DESCRIPTION_CN					_T("Description_cn")					// 参数中文描述
#define	FLD_EX_CURVEINFO_CURVENAMEM					_T("CurveNameM")					// 当前对应内存曲线名称
#define	FLD_EX_CURVEINFO_SORDER					_T("SOrder")					// 排序
#define	FLD_EX_CURVEINFO_ISCALC					_T("IsCalc")					// 是否计算字段:NO|0.否;Yes|1.是
#define	FLD_EX_CURVEINFO_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_EX_CURVEINFO_UPDTIME					_T("UpdTime")					// 最后一次修改时间戳
#define	FLD_EX_CURVEINFO_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_EX_CURVEINFO_MEMO					_T("Memo")					// 备注
#define	FLD_EX_CURVEINFO_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_EX_CURVEINFO_CURVENAME_DIGITS				50					// 曲线名称位数
#define	TV_EX_CURVEINFO_CCID_DIGITS				10					// 曲线分类编号位数
#define	TV_EX_CURVEINFO_UTYPE_DIGITS				50					// 曲线单位类型 对应UnitTB表的UType字段 创建时选择输入位数
#define	TV_EX_CURVEINFO_FILTER_DIGITS				50					// 滤波位数
#define	TV_EX_CURVEINFO_TABLENAME_DIGITS				50					// 对应存储表名位数
#define	TV_EX_CURVEINFO_FIELDNAME_DIGITS				2000					// 对应存储字段名位数
#define	TV_EX_CURVEINFO_DESCRIPTION_EN_DIGITS				200					// 参数英文描述位数
#define	TV_EX_CURVEINFO_DESCRIPTION_CN_DIGITS				200					// 参数中文描述位数
#define	TV_EX_CURVEINFO_CURVENAMEM_DIGITS				50					// 当前对应内存曲线名称位数
#define	TV_EX_CURVEINFO_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_EX_CURVEINFO
{
 
  int	iCurvID;							// 曲线编号(WITS表对应字段)
  char	szCurveName[TV_EX_CURVEINFO_CURVENAME_DIGITS + 1];							// 曲线名称
  char	szCCID[TV_EX_CURVEINFO_CCID_DIGITS + 1];							// 曲线分类编号
  int	iWITSID;							// WITS表对应字段
  int	iToolID;							// 工具编号
  int	iMPID;							// 测量点编号
  char	szUType[TV_EX_CURVEINFO_UTYPE_DIGITS + 1];							// 曲线单位类型 对应UnitTB表的UType字段 创建时选择输入
  int	iUID;							// 数据库中保存的单位ID，如果界面没有设置显示单位，则作为缺省界面的显示单位
  double	dSensorToBit;							// 深度偏移(该曲线测点距离钻头的距离) 该值自动计算=MeasPointInfoTB.SensorToolToBot + BHAInfoDTB.ToolBotToBit
  double	dCurveMin;							// 最小值
  double	dCurveMax;							// 最大值
  float	fWarnMin;							// 预警最小值 显示共色线
  float	fWarnMax;							// 预警最大值 显示共色线
  float	fErrorMin;							// 错误最小值 显示红色线
  float	fErrorMax;							// 错误最大值 显示红色线
  double	dInvPointVal;							// 无效点值
  char	szFilter[TV_EX_CURVEINFO_FILTER_DIGITS + 1];							// 滤波
  char	szTableName[TV_EX_CURVEINFO_TABLENAME_DIGITS + 1];							// 对应存储表名
  char	szFieldName[TV_EX_CURVEINFO_FIELDNAME_DIGITS + 1];							// 对应存储字段名
  int	iDataType;							// WITS数据类型:A|65.字符型;L|73.LONG;S|79.S;F|128.浮点型
  int	iDimension;							// 维数(常规曲线1，阵列曲线2)
  int	iPointCount;							// 点数(常规曲线1)
  char	szDescription_en[TV_EX_CURVEINFO_DESCRIPTION_EN_DIGITS + 1];							// 参数英文描述
  char	szDescription_cn[TV_EX_CURVEINFO_DESCRIPTION_CN_DIGITS + 1];							// 参数中文描述
  char	szCurveNameM[TV_EX_CURVEINFO_CURVENAMEM_DIGITS + 1];							// 当前对应内存曲线名称
  int	iSOrder;							// 排序
  short	nIsCalc;							// 是否计算字段:NO|0.否;Yes|1.是
  time_t	lCreateTime;							// 创建时间戳
  time_t	lUpdTime;							// 最后一次修改时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_EX_CURVEINFO_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_EX_CURVEINFO;

typedef	TS_EX_CURVEINFO FAR*	LPTS_EX_CURVEINFO;

#pragma	pack(pop)

#endif // _TEX_CURVEINFO_H

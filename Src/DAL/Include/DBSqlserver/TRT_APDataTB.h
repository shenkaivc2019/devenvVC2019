//---------------------------------------------------------------------------//
// 文件名: RT_APDataTB.h
// 说明:	实时环空压力数据表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2023/4/18 12:42:59
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TRT_APDataTB.h : DRT_APDataTB

#ifndef	_TRT_APDATATB_H
#define	_TRT_APDATATB_H

#define	TID_RT_APDATATB								_T("RT_APDataTB")
#define	OID_RT_APDATATB								_T("")

// Sort No
#define	SORT_RT_APDATATB_PK0				0							// PK:数据编号
//#define	SORT_RT_APDATATB_A1							%#%							// A1:

// Colum No
#define	COL_RT_APDATATB_DATAID					(short)0						// 数据编号
#define	COL_RT_APDATATB_RUNID					(short)1						// 趟钻编号
#define	COL_RT_APDATATB_TDATETIME					(short)2						// 时间（精确到秒）
#define	COL_RT_APDATATB_MILLITIME					(short)3						// 毫秒
#define	COL_RT_APDATATB_TOOLID					(short)4						// 工具编号
#define	COL_RT_APDATATB_PRESSUREMDEPTH					(short)5						// 环空压力测量点井深
#define	COL_RT_APDATATB_PRESSUREVDEPTH					(short)6						// 环空压力测量点垂深
#define	COL_RT_APDATATB_PREVDEPTH					(short)7						// 测量点预测垂深
#define	COL_RT_APDATATB_CURVENAME					(short)8						// 曲线名称
#define	COL_RT_APDATATB_WITSID					(short)9						// WITSID
#define	COL_RT_APDATATB_VALUER					(short)10						// 环空压力原始数据值
#define	COL_RT_APDATATB_VALUEM					(short)11						// 环空压力修改数据值
#define	COL_RT_APDATATB_ECD					(short)12						// 循环当量密度原始数据值=环空压力/环空压力测量点垂深（测量点预测垂深）
#define	COL_RT_APDATATB_ECDM					(short)13						// 循环当量密度修改数据值=环空压力/环空压力测量点垂深（测量点预测垂深）
#define	COL_RT_APDATATB_CONF					(short)14						// 可信度
#define	COL_RT_APDATATB_BAD					(short)15						// 是否坏点:No|0.否;Yes|1.是
#define	COL_RT_APDATATB_DRILLACTIV					(short)16						// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	COL_RT_APDATATB_SLIDING					(short)17						// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	COL_RT_APDATATB_RELOG					(short)18						// 是否复测 :Yes|0.否;No|1.是
#define	COL_RT_APDATATB_LAS					(short)19						// 是否Las :No|0.否;Yes|1.是
#define	COL_RT_APDATATB_CREATETIME					(short)20						// 创建时间戳
#define	COL_RT_APDATATB_UPDTIME					(short)21						// 最后一次修改时间戳
#define	COL_RT_APDATATB_STATUS					(short)22						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_RT_APDATATB_MEMO					(short)23						// 备注

// Colum(Field) Name
#define	FLD_RT_APDATATB_DATAID					_T("DataID")					// 数据编号
#define	FLD_RT_APDATATB_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_RT_APDATATB_TDATETIME					_T("TDateTime")					// 时间（精确到秒）
#define	FLD_RT_APDATATB_MILLITIME					_T("Millitime")					// 毫秒
#define	FLD_RT_APDATATB_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_RT_APDATATB_PRESSUREMDEPTH					_T("PressureMDepth")					// 环空压力测量点井深
#define	FLD_RT_APDATATB_PRESSUREVDEPTH					_T("PressureVDepth")					// 环空压力测量点垂深
#define	FLD_RT_APDATATB_PREVDEPTH					_T("PreVDepth")					// 测量点预测垂深
#define	FLD_RT_APDATATB_CURVENAME					_T("CurveName")					// 曲线名称
#define	FLD_RT_APDATATB_WITSID					_T("WITSID")					// WITSID
#define	FLD_RT_APDATATB_VALUER					_T("ValueR")					// 环空压力原始数据值
#define	FLD_RT_APDATATB_VALUEM					_T("ValueM")					// 环空压力修改数据值
#define	FLD_RT_APDATATB_ECD					_T("ECD")					// 循环当量密度原始数据值=环空压力/环空压力测量点垂深（测量点预测垂深）
#define	FLD_RT_APDATATB_ECDM					_T("ECDM")					// 循环当量密度修改数据值=环空压力/环空压力测量点垂深（测量点预测垂深）
#define	FLD_RT_APDATATB_CONF					_T("Conf")					// 可信度
#define	FLD_RT_APDATATB_BAD					_T("Bad")					// 是否坏点:No|0.否;Yes|1.是
#define	FLD_RT_APDATATB_DRILLACTIV					_T("DrillActiv")					// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	FLD_RT_APDATATB_SLIDING					_T("Sliding")					// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	FLD_RT_APDATATB_RELOG					_T("ReLog")					// 是否复测 :Yes|0.否;No|1.是
#define	FLD_RT_APDATATB_LAS					_T("Las")					// 是否Las :No|0.否;Yes|1.是
#define	FLD_RT_APDATATB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_RT_APDATATB_UPDTIME					_T("UpdTime")					// 最后一次修改时间戳
#define	FLD_RT_APDATATB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_RT_APDATATB_MEMO					_T("Memo")					// 备注

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_RT_APDATATB_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_RT_APDATATB_CURVENAME_DIGITS				50					// 曲线名称位数
#define	TV_RT_APDATATB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_RT_APDATATB
{
 
  long	lDataID;							// 数据编号
  char	szRunID[TV_RT_APDATATB_RUNID_DIGITS + 1];							// 趟钻编号
  time_t	lTDateTime;							// 时间（精确到秒）
  int	iMillitime;							// 毫秒
  int	iToolID;							// 工具编号
  long	lPressureMDepth;							// 环空压力测量点井深
  long	lPressureVDepth;							// 环空压力测量点垂深
  long	lPreVDepth;							// 测量点预测垂深
  char	szCurveName[TV_RT_APDATATB_CURVENAME_DIGITS + 1];							// 曲线名称
  int	iWITSID;							// WITSID
  double	dValueR;							// 环空压力原始数据值
  double	dValueM;							// 环空压力修改数据值
  double	dECD;							// 循环当量密度原始数据值=环空压力/环空压力测量点垂深（测量点预测垂深）
  double	dECDM;							// 循环当量密度修改数据值=环空压力/环空压力测量点垂深（测量点预测垂深）
  short	nConf;							// 可信度
  short	nBad;							// 是否坏点:No|0.否;Yes|1.是
  short	nDrillActiv;							// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
  short	nSliding;							// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
  short	nReLog;							// 是否复测 :Yes|0.否;No|1.是
  short	nLas;							// 是否Las :No|0.否;Yes|1.是
  time_t	lCreateTime;							// 创建时间戳
  time_t	lUpdTime;							// 最后一次修改时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_RT_APDATATB_MEMO_DIGITS + 1];							// 备注
} TS_RT_APDATATB;

typedef	TS_RT_APDATATB FAR*	LPTS_RT_APDATATB;

#pragma	pack(pop)

#endif // _TRT_APDATATB_H

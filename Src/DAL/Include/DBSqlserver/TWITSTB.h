//---------------------------------------------------------------------------//
// 文件名: WITSTB.h
// 说明:	WITS数据表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/2/2 10:18:14
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TWITSTB.h : DWITSTB

#ifndef	_TWITSTB_H
#define	_TWITSTB_H

#define	TID_WITSTB								_T("WITSTB")
#define	OID_WITSTB								_T("")

// Sort No
#define	SORT_WITSTB_PK0				0							// PK:WITSID
//#define	SORT_WITSTB_A1							%#%							// A1:

// Colum No
#define	COL_WITSTB_WITSID					(short)0						// WITSID
#define	COL_WITSTB_RECID					(short)1						// 记录ID
#define	COL_WITSTB_LRECTYPE					(short)2						// 逻辑记录类型
#define	COL_WITSTB_RECITEM					(short)3						// 参数序号
#define	COL_WITSTB_DESCRIPTION_EN					(short)4						// 参数英文描述
#define	COL_WITSTB_DESCRIPTION_CN					(short)5						// 参数中文描述
#define	COL_WITSTB_LONGMNEMONIC					(short)6						// 长字段名
#define	COL_WITSTB_SHORTMNEMONIC					(short)7						// 是否坏点:No|0.否;Yes|1.是
#define	COL_WITSTB_VALTYPE					(short)8						// WITS数据类型:A|65.字符型;L|73.LONG;S|79.S;F|128.浮点型
#define	COL_WITSTB_VALLENGTH					(short)9						// 字段长
#define	COL_WITSTB_VALIDUNIT					(short)10						// 使用公制单位:No|0.英制;Yes|1.公制
#define	COL_WITSTB_METRICUNITS					(short)11						// 公制单位
#define	COL_WITSTB_FPSUNITS					(short)12						// 英制单位
#define	COL_WITSTB_UID					(short)13						// 单位ID，存储数据的缺省单位
#define	COL_WITSTB_DPTID					(short)14						// 数据处理类型ID
#define	COL_WITSTB_UPDCOUNT					(short)15						// 更新计数

// Colum(Field) Name
#define	FLD_WITSTB_WITSID					_T("WITSID")					// WITSID
#define	FLD_WITSTB_RECID					_T("RecID")					// 记录ID
#define	FLD_WITSTB_LRECTYPE					_T("LRecType")					// 逻辑记录类型
#define	FLD_WITSTB_RECITEM					_T("RecItem")					// 参数序号
#define	FLD_WITSTB_DESCRIPTION_EN					_T("Description_en")					// 参数英文描述
#define	FLD_WITSTB_DESCRIPTION_CN					_T("Description_cn")					// 参数中文描述
#define	FLD_WITSTB_LONGMNEMONIC					_T("LongMnemonic")					// 长字段名
#define	FLD_WITSTB_SHORTMNEMONIC					_T("ShortMnemonic")					// 是否坏点:No|0.否;Yes|1.是
#define	FLD_WITSTB_VALTYPE					_T("ValType")					// WITS数据类型:A|65.字符型;L|73.LONG;S|79.S;F|128.浮点型
#define	FLD_WITSTB_VALLENGTH					_T("ValLength")					// 字段长
#define	FLD_WITSTB_VALIDUNIT					_T("ValidUnit")					// 使用公制单位:No|0.英制;Yes|1.公制
#define	FLD_WITSTB_METRICUNITS					_T("MetricUnits")					// 公制单位
#define	FLD_WITSTB_FPSUNITS					_T("FPSUnits")					// 英制单位
#define	FLD_WITSTB_UID					_T("UID")					// 单位ID，存储数据的缺省单位
#define	FLD_WITSTB_DPTID					_T("DPTID")					// 数据处理类型ID
#define	FLD_WITSTB_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_WITSTB_DESCRIPTION_EN_DIGITS				100					// 参数英文描述位数
#define	TV_WITSTB_DESCRIPTION_CN_DIGITS				100					// 参数中文描述位数
#define	TV_WITSTB_LONGMNEMONIC_DIGITS				50					// 长字段名位数
#define	TV_WITSTB_SHORTMNEMONIC_DIGITS				50					// 是否坏点:No|0.否;Yes|1.是位数
#define	TV_WITSTB_METRICUNITS_DIGITS				50					// 公制单位位数
#define	TV_WITSTB_FPSUNITS_DIGITS				50					// 英制单位位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_WITSTB
{
 
  int	iWITSID;							// WITSID
  int	iRecID;							// 记录ID
  int	iLRecType;							// 逻辑记录类型
  int	iRecItem;							// 参数序号
  char	szDescription_en[TV_WITSTB_DESCRIPTION_EN_DIGITS + 1];							// 参数英文描述
  char	szDescription_cn[TV_WITSTB_DESCRIPTION_CN_DIGITS + 1];							// 参数中文描述
  char	szLongMnemonic[TV_WITSTB_LONGMNEMONIC_DIGITS + 1];							// 长字段名
  char	szShortMnemonic[TV_WITSTB_SHORTMNEMONIC_DIGITS + 1];							// 是否坏点:No|0.否;Yes|1.是
  int	iValType;							// WITS数据类型:A|65.字符型;L|73.LONG;S|79.S;F|128.浮点型
  long	lValLength;							// 字段长
  short	nValidUnit;							// 使用公制单位:No|0.英制;Yes|1.公制
  char	szMetricUnits[TV_WITSTB_METRICUNITS_DIGITS + 1];							// 公制单位
  char	szFPSUnits[TV_WITSTB_FPSUNITS_DIGITS + 1];							// 英制单位
  int	iUID;							// 单位ID，存储数据的缺省单位
  int	iDPTID;							// 数据处理类型ID
  int	iUpdCount;							// 更新计数
} TS_WITSTB;

typedef	TS_WITSTB FAR*	LPTS_WITSTB;

#pragma	pack(pop)

#endif // _TWITSTB_H

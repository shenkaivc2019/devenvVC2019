//---------------------------------------------------------------------------//
// 文件名: ToolAizRes.h
// 说明:	方位电阻率工具基本信息表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2022/10/9 9:36:12
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TToolAizRes.h : DToolAizRes

#ifndef	_TTOOLAIZRES_H
#define	_TTOOLAIZRES_H

#define	TID_TOOLAIZRES								_T("ToolAizRes")
#define	OID_TOOLAIZRES								_T("")

// Sort No
#define	SORT_TOOLAIZRES_PK0				0							// PK:数据编号
//#define	SORT_TOOLAIZRES_A1							%#%							// A1:

// Colum No
#define	COL_TOOLAIZRES_DATAID					(short)0						// 数据编号
#define	COL_TOOLAIZRES_TOOLID					(short)1						// 工具编号
#define	COL_TOOLAIZRES_TOOLSN					(short)2						// 工具编号SN（类似于产品条码）
#define	COL_TOOLAIZRES_RUNID					(short)3						// 趟钻编号
#define	COL_TOOLAIZRES_TX4_400KAS					(short)4						// TX4_400KAS幅度校正系数
#define	COL_TOOLAIZRES_TX2_400KAS					(short)5						// TX2_400KS幅度校正系数
#define	COL_TOOLAIZRES_TX4_400KPO					(short)6						// TX4_400KPO相位校正系数
#define	COL_TOOLAIZRES_TX2_400KPO					(short)7						// TX2_400KPO相位校正系数
#define	COL_TOOLAIZRES_CREATETIME					(short)8						// 创建时间戳
#define	COL_TOOLAIZRES_STATUS					(short)9						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_TOOLAIZRES_MEMO					(short)10						// 备注
#define	COL_TOOLAIZRES_UPDCOUNT					(short)11						// 更新计数

// Colum(Field) Name
#define	FLD_TOOLAIZRES_DATAID					_T("DataID")					// 数据编号
#define	FLD_TOOLAIZRES_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_TOOLAIZRES_TOOLSN					_T("ToolSN")					// 工具编号SN（类似于产品条码）
#define	FLD_TOOLAIZRES_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_TOOLAIZRES_TX4_400KAS					_T("TX4_400KAS")					// TX4_400KAS幅度校正系数
#define	FLD_TOOLAIZRES_TX2_400KAS					_T("TX2_400KAS")					// TX2_400KS幅度校正系数
#define	FLD_TOOLAIZRES_TX4_400KPO					_T("TX4_400KPO")					// TX4_400KPO相位校正系数
#define	FLD_TOOLAIZRES_TX2_400KPO					_T("TX2_400KPO")					// TX2_400KPO相位校正系数
#define	FLD_TOOLAIZRES_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_TOOLAIZRES_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_TOOLAIZRES_MEMO					_T("Memo")					// 备注
#define	FLD_TOOLAIZRES_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_TOOLAIZRES_TOOLSN_DIGITS				50					// 工具编号SN（类似于产品条码）位数
#define	TV_TOOLAIZRES_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_TOOLAIZRES_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_TOOLAIZRES
{
 
  long	lDataID;							// 数据编号
  int	iToolID;							// 工具编号
  char	szToolSN[TV_TOOLAIZRES_TOOLSN_DIGITS + 1];							// 工具编号SN（类似于产品条码）
  char	szRunID[TV_TOOLAIZRES_RUNID_DIGITS + 1];							// 趟钻编号
  float	fTX4_400KAS;							// TX4_400KAS幅度校正系数
  float	fTX2_400KAS;							// TX2_400KS幅度校正系数
  float	fTX4_400KPO;							// TX4_400KPO相位校正系数
  float	fTX2_400KPO;							// TX2_400KPO相位校正系数
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_TOOLAIZRES_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_TOOLAIZRES;

typedef	TS_TOOLAIZRES FAR*	LPTS_TOOLAIZRES;

#pragma	pack(pop)

#endif // _TTOOLAIZRES_H

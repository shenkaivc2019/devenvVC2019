//---------------------------------------------------------------------------//
// 文件名: SurveyTB.h
// 说明:	测斜数据表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020/10/19 22:11:35
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TSurveyTB.h : DSurveyTB

#ifndef	_TSURVEYTB_H
#define	_TSURVEYTB_H

#define	TID_SURVEYTB								_T("SurveyTB")
#define	OID_SURVEYTB								_T("")

// Sort No
#define	SORT_SURVEYTB_PK0				0							// PK:自增字段
//#define	SORT_SURVEYTB_A1							%#%							// A1:

// Colum No
#define	COL_SURVEYTB_SURVEYID					(short)0						// 自增字段
#define	COL_SURVEYTB_HOLEID					(short)1						// 井眼编号
#define	COL_SURVEYTB_RUNID					(short)2						// 趟钻编号
#define	COL_SURVEYTB_SURVEYDEPTH					(short)3						// 测深（取数据时按此字段排序）
#define	COL_SURVEYTB_INCLINATION					(short)4						// 井斜
#define	COL_SURVEYTB_AZIMUTH					(short)5						// 方位
#define	COL_SURVEYTB_TVDDEPTH					(short)6						// 垂深
#define	COL_SURVEYTB_EAST					(short)7						// 东投
#define	COL_SURVEYTB_NORTH					(short)8						// 北投
#define	COL_SURVEYTB_VERTSECTION					(short)9						// =开根号（东投平方 + L134北投平方）
#define	COL_SURVEYTB_DOGLEGSEVERITY					(short)10						// 狗腿度
#define	COL_SURVEYTB_INCCHNGRATE					(short)11						// 井斜变化率
#define	COL_SURVEYTB_AZICHNGRATE					(short)12						// 方位变化率
#define	COL_SURVEYTB_TIEIN					(short)13						// 是否联入点:No|0.否;Yes|1.是
#define	COL_SURVEYTB_TOTALOFFSET					(short)14						// 闭合范围
#define	COL_SURVEYTB_TOTALAZIM					(short)15						// 闭合位移
#define	COL_SURVEYTB_VSAZIM					(short)16						// 设计方位，计算视位移用的
#define	COL_SURVEYTB_SEGLENGTH					(short)17						// 段长
#define	COL_SURVEYTB_TFTYPE					(short)18						// 工具面类型:No|0.磁性;Yes|1.重力
#define	COL_SURVEYTB_TF					(short)19						// 工具面
#define	COL_SURVEYTB_CHOOSE					(short)20						// 是否选中:No|0.否;Yes|1.是
#define	COL_SURVEYTB_CREATETIME					(short)21						// 创建时间戳
#define	COL_SURVEYTB_STATUS					(short)22						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_SURVEYTB_MEMO					(short)23						// 备注
#define	COL_SURVEYTB_UPDCOUNT					(short)24						// 更新计数

// Colum(Field) Name
#define	FLD_SURVEYTB_SURVEYID					_T("SurveyID")					// 自增字段
#define	FLD_SURVEYTB_HOLEID					_T("HoleID")					// 井眼编号
#define	FLD_SURVEYTB_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_SURVEYTB_SURVEYDEPTH					_T("SurveyDepth")					// 测深（取数据时按此字段排序）
#define	FLD_SURVEYTB_INCLINATION					_T("Inclination")					// 井斜
#define	FLD_SURVEYTB_AZIMUTH					_T("Azimuth")					// 方位
#define	FLD_SURVEYTB_TVDDEPTH					_T("TVDDepth")					// 垂深
#define	FLD_SURVEYTB_EAST					_T("East")					// 东投
#define	FLD_SURVEYTB_NORTH					_T("North")					// 北投
#define	FLD_SURVEYTB_VERTSECTION					_T("VertSection")					// =开根号（东投平方 + L134北投平方）
#define	FLD_SURVEYTB_DOGLEGSEVERITY					_T("DoglegSeverity")					// 狗腿度
#define	FLD_SURVEYTB_INCCHNGRATE					_T("IncChngRate")					// 井斜变化率
#define	FLD_SURVEYTB_AZICHNGRATE					_T("AziChngRate")					// 方位变化率
#define	FLD_SURVEYTB_TIEIN					_T("TieIn")					// 是否联入点:No|0.否;Yes|1.是
#define	FLD_SURVEYTB_TOTALOFFSET					_T("TotalOffset")					// 闭合范围
#define	FLD_SURVEYTB_TOTALAZIM					_T("TotalAzim")					// 闭合位移
#define	FLD_SURVEYTB_VSAZIM					_T("VSAzim")					// 设计方位，计算视位移用的
#define	FLD_SURVEYTB_SEGLENGTH					_T("SegLength")					// 段长
#define	FLD_SURVEYTB_TFTYPE					_T("TFType")					// 工具面类型:No|0.磁性;Yes|1.重力
#define	FLD_SURVEYTB_TF					_T("TF")					// 工具面
#define	FLD_SURVEYTB_CHOOSE					_T("Choose")					// 是否选中:No|0.否;Yes|1.是
#define	FLD_SURVEYTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_SURVEYTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_SURVEYTB_MEMO					_T("Memo")					// 备注
#define	FLD_SURVEYTB_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_SURVEYTB_HOLEID_DIGITS				50					// 井眼编号位数
#define	TV_SURVEYTB_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_SURVEYTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_SURVEYTB
{
 
  int	iSurveyID;							// 自增字段
  char	szHoleID[TV_SURVEYTB_HOLEID_DIGITS + 1];							// 井眼编号
  char	szRunID[TV_SURVEYTB_RUNID_DIGITS + 1];							// 趟钻编号
  long	lSurveyDepth;							// 测深（取数据时按此字段排序）
  float	fInclination;							// 井斜
  float	fAzimuth;							// 方位
  long	lTVDDepth;							// 垂深
  double	dEast;							// 东投
  double	dNorth;							// 北投
  double	dVertSection;							// =开根号（东投平方 + L134北投平方）
  double	dDoglegSeverity;							// 狗腿度
  double	dIncChngRate;							// 井斜变化率
  double	dAziChngRate;							// 方位变化率
  short	nTieIn;							// 是否联入点:No|0.否;Yes|1.是
  float	fTotalOffset;							// 闭合范围
  float	fTotalAzim;							// 闭合位移
  float	fVSAzim;							// 设计方位，计算视位移用的
  float	fSegLength;							// 段长
  short	nTFType;							// 工具面类型:No|0.磁性;Yes|1.重力
  float	fTF;							// 工具面
  short	nChoose;							// 是否选中:No|0.否;Yes|1.是
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_SURVEYTB_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_SURVEYTB;

typedef	TS_SURVEYTB FAR*	LPTS_SURVEYTB;

#pragma	pack(pop)

#endif // _TSURVEYTB_H

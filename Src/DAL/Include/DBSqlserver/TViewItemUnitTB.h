//---------------------------------------------------------------------------//
// 文件名: ViewItemUnitTB.h
// 说明:	界面项目单位信息表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/2/2 10:18:14
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TViewItemUnitTB.h : DViewItemUnitTB

#ifndef	_TVIEWITEMUNITTB_H
#define	_TVIEWITEMUNITTB_H

#define	TID_VIEWITEMUNITTB								_T("ViewItemUnitTB")
#define	OID_VIEWITEMUNITTB								_T("")

// Sort No
#define	SORT_VIEWITEMUNITTB_PK0				0							// PK:界面编号
#define	SORT_VIEWITEMUNITTB_PK1				1							// PK:界面中的项目名称
//#define	SORT_VIEWITEMUNITTB_A1							%#%							// A1:

// Colum No
#define	COL_VIEWITEMUNITTB_VIEWID					(short)0						// 界面编号
#define	COL_VIEWITEMUNITTB_ITEMID					(short)1						// 界面中的项目名称
#define	COL_VIEWITEMUNITTB_UTYPE					(short)2						// 单位类型
#define	COL_VIEWITEMUNITTB_DBUID					(short)3						// 数据库存储单位ID
#define	COL_VIEWITEMUNITTB_SHOWUID					(short)4						// 界面显示单位ID
#define	COL_VIEWITEMUNITTB_DESCRIPTION_CN					(short)5						// 中文描述
#define	COL_VIEWITEMUNITTB_DESCRIPTION_EN					(short)6						// 英文描述
#define	COL_VIEWITEMUNITTB_ITEMORDER					(short)7						// 排序
#define	COL_VIEWITEMUNITTB_CREATETIME					(short)8						// 创建时间戳
#define	COL_VIEWITEMUNITTB_STATUS					(short)9						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_VIEWITEMUNITTB_MEMO					(short)10						// 备注
#define	COL_VIEWITEMUNITTB_UPDCOUNT					(short)11						// 更新计数

// Colum(Field) Name
#define	FLD_VIEWITEMUNITTB_VIEWID					_T("ViewID")					// 界面编号
#define	FLD_VIEWITEMUNITTB_ITEMID					_T("ItemID")					// 界面中的项目名称
#define	FLD_VIEWITEMUNITTB_UTYPE					_T("UType")					// 单位类型
#define	FLD_VIEWITEMUNITTB_DBUID					_T("DBUID")					// 数据库存储单位ID
#define	FLD_VIEWITEMUNITTB_SHOWUID					_T("ShowUID")					// 界面显示单位ID
#define	FLD_VIEWITEMUNITTB_DESCRIPTION_CN					_T("Description_cn")					// 中文描述
#define	FLD_VIEWITEMUNITTB_DESCRIPTION_EN					_T("Description_en")					// 英文描述
#define	FLD_VIEWITEMUNITTB_ITEMORDER					_T("ItemOrder")					// 排序
#define	FLD_VIEWITEMUNITTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_VIEWITEMUNITTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_VIEWITEMUNITTB_MEMO					_T("Memo")					// 备注
#define	FLD_VIEWITEMUNITTB_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_VIEWITEMUNITTB_VIEWID_DIGITS				20					// 界面编号位数
#define	TV_VIEWITEMUNITTB_ITEMID_DIGITS				20					// 界面中的项目名称位数
#define	TV_VIEWITEMUNITTB_UTYPE_DIGITS				50					// 单位类型位数
#define	TV_VIEWITEMUNITTB_DESCRIPTION_CN_DIGITS				100					// 中文描述位数
#define	TV_VIEWITEMUNITTB_DESCRIPTION_EN_DIGITS				100					// 英文描述位数
#define	TV_VIEWITEMUNITTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_VIEWITEMUNITTB
{
 
  char	szViewID[TV_VIEWITEMUNITTB_VIEWID_DIGITS + 1];							// 界面编号
  char	szItemID[TV_VIEWITEMUNITTB_ITEMID_DIGITS + 1];							// 界面中的项目名称
  char	szUType[TV_VIEWITEMUNITTB_UTYPE_DIGITS + 1];							// 单位类型
  int	iDBUID;							// 数据库存储单位ID
  int	iShowUID;							// 界面显示单位ID
  char	szDescription_cn[TV_VIEWITEMUNITTB_DESCRIPTION_CN_DIGITS + 1];							// 中文描述
  char	szDescription_en[TV_VIEWITEMUNITTB_DESCRIPTION_EN_DIGITS + 1];							// 英文描述
  int	iItemOrder;							// 排序
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_VIEWITEMUNITTB_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_VIEWITEMUNITTB;

typedef	TS_VIEWITEMUNITTB FAR*	LPTS_VIEWITEMUNITTB;

#pragma	pack(pop)

#endif // _TVIEWITEMUNITTB_H

//---------------------------------------------------------------------------//
// 文件名: BHAInfoDTB.h
// 说明:	钻具组合信息子表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020/10/19 22:11:35
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TBHAInfoDTB.h : DBHAInfoDTB

#ifndef	_TBHAINFODTB_H
#define	_TBHAINFODTB_H

#define	TID_BHAINFODTB								_T("BHAInfoDTB")
#define	OID_BHAINFODTB								_T("")

// Sort No
#define	SORT_BHAINFODTB_PK0				0							// PK:钻具组合编号
#define	SORT_BHAINFODTB_PK1				1							// PK:工具编号
#define	SORT_BHAINFODTB_PK2				2							// PK:连接序号（该工具在钻具组合中的位置）方向从钻头开始
//#define	SORT_BHAINFODTB_A1							%#%							// A1:

// Colum No
#define	COL_BHAINFODTB_BHAID					(short)0						// 钻具组合编号
#define	COL_BHAINFODTB_TOOLID					(short)1						// 工具编号
#define	COL_BHAINFODTB_TOOLNO					(short)2						// 连接序号（该工具在钻具组合中的位置）方向从钻头开始
#define	COL_BHAINFODTB_TOOLSN					(short)3						// 工具编号（类似于产品条码）
#define	COL_BHAINFODTB_TOOLBOTTOBIT					(short)4						// 深度偏移(距本钻具组合底部距离),计算得出=BHAInfoMTB.BotToBit+ToolInfoTB.ToolLength(各工具的长度相加到该工具的序号BHAInfoDTB.ToolNo）
#define	COL_BHAINFODTB_CAPI					(short)5						// 传感器校准系数
#define	COL_BHAINFODTB_CALTIME					(short)6						// 校准时间
#define	COL_BHAINFODTB_TOOLLENGTH					(short)7						// 长度
#define	COL_BHAINFODTB_TOOLWEIGHT					(short)8						// 重量
#define	COL_BHAINFODTB_TOOLINTDIA					(short)9						// 内径
#define	COL_BHAINFODTB_TOOLEXTDIA					(short)10						// 外径
#define	COL_BHAINFODTB_TOOLODMAX					(short)11						// 最大外径
#define	COL_BHAINFODTB_TOOLODMIN					(short)12						// 最小外径
#define	COL_BHAINFODTB_TOOLPREMAX					(short)13						// 最大压强
#define	COL_BHAINFODTB_TOOLTEMPMAX					(short)14						// 最高温度
#define	COL_BHAINFODTB_CREATETIME					(short)15						// 创建时间戳
#define	COL_BHAINFODTB_STATUS					(short)16						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_BHAINFODTB_MEMO					(short)17						// 备注
#define	COL_BHAINFODTB_UPDCOUNT					(short)18						// 更新计数

// Colum(Field) Name
#define	FLD_BHAINFODTB_BHAID					_T("BHAID")					// 钻具组合编号
#define	FLD_BHAINFODTB_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_BHAINFODTB_TOOLNO					_T("ToolNo")					// 连接序号（该工具在钻具组合中的位置）方向从钻头开始
#define	FLD_BHAINFODTB_TOOLSN					_T("ToolSN")					// 工具编号（类似于产品条码）
#define	FLD_BHAINFODTB_TOOLBOTTOBIT					_T("ToolBotToBit")					// 深度偏移(距本钻具组合底部距离),计算得出=BHAInfoMTB.BotToBit+ToolInfoTB.ToolLength(各工具的长度相加到该工具的序号BHAInfoDTB.ToolNo）
#define	FLD_BHAINFODTB_CAPI					_T("CAPI")					// 传感器校准系数
#define	FLD_BHAINFODTB_CALTIME					_T("CALTime")					// 校准时间
#define	FLD_BHAINFODTB_TOOLLENGTH					_T("ToolLength")					// 长度
#define	FLD_BHAINFODTB_TOOLWEIGHT					_T("ToolWeight")					// 重量
#define	FLD_BHAINFODTB_TOOLINTDIA					_T("ToolIntDia")					// 内径
#define	FLD_BHAINFODTB_TOOLEXTDIA					_T("ToolExtDia")					// 外径
#define	FLD_BHAINFODTB_TOOLODMAX					_T("ToolODMax")					// 最大外径
#define	FLD_BHAINFODTB_TOOLODMIN					_T("ToolODMin")					// 最小外径
#define	FLD_BHAINFODTB_TOOLPREMAX					_T("ToolPreMax")					// 最大压强
#define	FLD_BHAINFODTB_TOOLTEMPMAX					_T("ToolTempMax")					// 最高温度
#define	FLD_BHAINFODTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_BHAINFODTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_BHAINFODTB_MEMO					_T("Memo")					// 备注
#define	FLD_BHAINFODTB_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_BHAINFODTB_BHAID_DIGITS				50					// 钻具组合编号位数
#define	TV_BHAINFODTB_TOOLSN_DIGITS				50					// 工具编号（类似于产品条码）位数
#define	TV_BHAINFODTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_BHAINFODTB
{
 
  char	szBHAID[TV_BHAINFODTB_BHAID_DIGITS + 1];							// 钻具组合编号
  int	iToolID;							// 工具编号
  int	iToolNo;							// 连接序号（该工具在钻具组合中的位置）方向从钻头开始
  char	szToolSN[TV_BHAINFODTB_TOOLSN_DIGITS + 1];							// 工具编号（类似于产品条码）
  double	dToolBotToBit;							// 深度偏移(距本钻具组合底部距离),计算得出=BHAInfoMTB.BotToBit+ToolInfoTB.ToolLength(各工具的长度相加到该工具的序号BHAInfoDTB.ToolNo）
  float	fCAPI;							// 传感器校准系数
  time_t	lCALTime;							// 校准时间
  double	dToolLength;							// 长度
  double	dToolWeight;							// 重量
  double	dToolIntDia;							// 内径
  double	dToolExtDia;							// 外径
  double	dToolODMax;							// 最大外径
  double	dToolODMin;							// 最小外径
  double	dToolPreMax;							// 最大压强
  double	dToolTempMax;							// 最高温度
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_BHAINFODTB_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_BHAINFODTB;

typedef	TS_BHAINFODTB FAR*	LPTS_BHAINFODTB;

#pragma	pack(pop)

#endif // _TBHAINFODTB_H

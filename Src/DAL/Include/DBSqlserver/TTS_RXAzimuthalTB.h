//---------------------------------------------------------------------------//
// 文件名: TS_RXAzimuthalTB.h
// 说明:	TS_RXAzimuthalTB
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020-05-30 17:47:33
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TTS_RXAzimuthalTB.h : DTS_RXAzimuthalTB

#ifndef	_TTS_RXAZIMUTHALTB_H
#define	_TTS_RXAZIMUTHALTB_H

#define	TID_TS_RXAZIMUTHALTB								_T("TS_RXAzimuthalTB")
#define	OID_TS_RXAZIMUTHALTB								_T("")

// Sort No

//#define	SORT_TS_RXAZIMUTHALTB_A1							%#%							// A1:

// Colum No
#define	COL_TS_RXAZIMUTHALTB_RXAZI_ID					(short)0						// 自增ID
#define	COL_TS_RXAZIMUTHALTB_RDATATIME					(short)1						// 时间
#define	COL_TS_RXAZIMUTHALTB_FREQUERYTYPE					(short)2						// 类别
#define	COL_TS_RXAZIMUTHALTB_S0					(short)3						// 数据0
#define	COL_TS_RXAZIMUTHALTB_S1					(short)4						// 数据1
#define	COL_TS_RXAZIMUTHALTB_S2					(short)5						// 数据2
#define	COL_TS_RXAZIMUTHALTB_S3					(short)6						// 数据3
#define	COL_TS_RXAZIMUTHALTB_S4					(short)7						// 数据4
#define	COL_TS_RXAZIMUTHALTB_S5					(short)8						// 数据5
#define	COL_TS_RXAZIMUTHALTB_S6					(short)9						// 数据6
#define	COL_TS_RXAZIMUTHALTB_S7					(short)10						// 数据7
#define	COL_TS_RXAZIMUTHALTB_S8					(short)11						// 数据8
#define	COL_TS_RXAZIMUTHALTB_S9					(short)12						// 数据9
#define	COL_TS_RXAZIMUTHALTB_S10					(short)13						// 数据10
#define	COL_TS_RXAZIMUTHALTB_S11					(short)14						// 数据11
#define	COL_TS_RXAZIMUTHALTB_S12					(short)15						// 数据12
#define	COL_TS_RXAZIMUTHALTB_S13					(short)16						// 数据13
#define	COL_TS_RXAZIMUTHALTB_S14					(short)17						// 数据14
#define	COL_TS_RXAZIMUTHALTB_S15					(short)18						// 数据15

// Colum(Field) Name
#define	FLD_TS_RXAZIMUTHALTB_RXAZI_ID					_T("RxAzi_Id")					// 自增ID
#define	FLD_TS_RXAZIMUTHALTB_RDATATIME					_T("RDataTime")					// 时间
#define	FLD_TS_RXAZIMUTHALTB_FREQUERYTYPE					_T("FrequeryType")					// 类别
#define	FLD_TS_RXAZIMUTHALTB_S0					_T("S0")					// 数据0
#define	FLD_TS_RXAZIMUTHALTB_S1					_T("S1")					// 数据1
#define	FLD_TS_RXAZIMUTHALTB_S2					_T("S2")					// 数据2
#define	FLD_TS_RXAZIMUTHALTB_S3					_T("S3")					// 数据3
#define	FLD_TS_RXAZIMUTHALTB_S4					_T("S4")					// 数据4
#define	FLD_TS_RXAZIMUTHALTB_S5					_T("S5")					// 数据5
#define	FLD_TS_RXAZIMUTHALTB_S6					_T("S6")					// 数据6
#define	FLD_TS_RXAZIMUTHALTB_S7					_T("S7")					// 数据7
#define	FLD_TS_RXAZIMUTHALTB_S8					_T("S8")					// 数据8
#define	FLD_TS_RXAZIMUTHALTB_S9					_T("S9")					// 数据9
#define	FLD_TS_RXAZIMUTHALTB_S10					_T("S10")					// 数据10
#define	FLD_TS_RXAZIMUTHALTB_S11					_T("S11")					// 数据11
#define	FLD_TS_RXAZIMUTHALTB_S12					_T("S12")					// 数据12
#define	FLD_TS_RXAZIMUTHALTB_S13					_T("S13")					// 数据13
#define	FLD_TS_RXAZIMUTHALTB_S14					_T("S14")					// 数据14
#define	FLD_TS_RXAZIMUTHALTB_S15					_T("S15")					// 数据15

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_TS_RXAZIMUTHALTB_FREQUERYTYPE_DIGITS				64					// 类别位数
#define	TV_TS_RXAZIMUTHALTB_S0_DIGITS				64					// 数据0位数
#define	TV_TS_RXAZIMUTHALTB_S1_DIGITS				64					// 数据1位数
#define	TV_TS_RXAZIMUTHALTB_S2_DIGITS				64					// 数据2位数
#define	TV_TS_RXAZIMUTHALTB_S3_DIGITS				64					// 数据3位数
#define	TV_TS_RXAZIMUTHALTB_S4_DIGITS				64					// 数据4位数
#define	TV_TS_RXAZIMUTHALTB_S5_DIGITS				64					// 数据5位数
#define	TV_TS_RXAZIMUTHALTB_S6_DIGITS				64					// 数据6位数
#define	TV_TS_RXAZIMUTHALTB_S7_DIGITS				64					// 数据7位数
#define	TV_TS_RXAZIMUTHALTB_S8_DIGITS				64					// 数据8位数
#define	TV_TS_RXAZIMUTHALTB_S9_DIGITS				64					// 数据9位数
#define	TV_TS_RXAZIMUTHALTB_S10_DIGITS				64					// 数据10位数
#define	TV_TS_RXAZIMUTHALTB_S11_DIGITS				64					// 数据11位数
#define	TV_TS_RXAZIMUTHALTB_S12_DIGITS				64					// 数据12位数
#define	TV_TS_RXAZIMUTHALTB_S13_DIGITS				64					// 数据13位数
#define	TV_TS_RXAZIMUTHALTB_S14_DIGITS				64					// 数据14位数
#define	TV_TS_RXAZIMUTHALTB_S15_DIGITS				64					// 数据15位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_TS_RXAZIMUTHALTB
{
 
  int	iRxAzi_Id;							// 自增ID
  time_t	lRDataTime;							// 时间
  char	szFrequeryType[TV_TS_RXAZIMUTHALTB_FREQUERYTYPE_DIGITS + 1];							// 类别
  char	szS0[TV_TS_RXAZIMUTHALTB_S0_DIGITS + 1];							// 数据0
  char	szS1[TV_TS_RXAZIMUTHALTB_S1_DIGITS + 1];							// 数据1
  char	szS2[TV_TS_RXAZIMUTHALTB_S2_DIGITS + 1];							// 数据2
  char	szS3[TV_TS_RXAZIMUTHALTB_S3_DIGITS + 1];							// 数据3
  char	szS4[TV_TS_RXAZIMUTHALTB_S4_DIGITS + 1];							// 数据4
  char	szS5[TV_TS_RXAZIMUTHALTB_S5_DIGITS + 1];							// 数据5
  char	szS6[TV_TS_RXAZIMUTHALTB_S6_DIGITS + 1];							// 数据6
  char	szS7[TV_TS_RXAZIMUTHALTB_S7_DIGITS + 1];							// 数据7
  char	szS8[TV_TS_RXAZIMUTHALTB_S8_DIGITS + 1];							// 数据8
  char	szS9[TV_TS_RXAZIMUTHALTB_S9_DIGITS + 1];							// 数据9
  char	szS10[TV_TS_RXAZIMUTHALTB_S10_DIGITS + 1];							// 数据10
  char	szS11[TV_TS_RXAZIMUTHALTB_S11_DIGITS + 1];							// 数据11
  char	szS12[TV_TS_RXAZIMUTHALTB_S12_DIGITS + 1];							// 数据12
  char	szS13[TV_TS_RXAZIMUTHALTB_S13_DIGITS + 1];							// 数据13
  char	szS14[TV_TS_RXAZIMUTHALTB_S14_DIGITS + 1];							// 数据14
  char	szS15[TV_TS_RXAZIMUTHALTB_S15_DIGITS + 1];							// 数据15
} TS_TS_RXAZIMUTHALTB;

typedef	TS_TS_RXAZIMUTHALTB FAR*	LPTS_TS_RXAZIMUTHALTB;

#pragma	pack(pop)

#endif // _TTS_RXAZIMUTHALTB_H

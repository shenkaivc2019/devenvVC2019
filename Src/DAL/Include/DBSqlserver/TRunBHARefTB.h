//---------------------------------------------------------------------------//
// 文件名: RunBHARefTB.h
// 说明:	趟钻仪器BHA对照表（指定Run用到的所有仪器）
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020-05-30 17:27:40
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TRunBHARefTB.h : DRunBHARefTB

#ifndef	_TRUNBHAREFTB_H
#define	_TRUNBHAREFTB_H

#define	TID_RUNBHAREFTB								_T("RunBHARefTB")
#define	OID_RUNBHAREFTB								_T("")

// Sort No
#define	SORT_RUNBHAREFTB_PK0				0							// PK:趟钻编号
#define	SORT_RUNBHAREFTB_PK1				1							// PK:钻具组合编号
//#define	SORT_RUNBHAREFTB_A1							%#%							// A1:

// Colum No
#define	COL_RUNBHAREFTB_RUNID					(short)0						// 趟钻编号
#define	COL_RUNBHAREFTB_BHAID					(short)1						// 钻具组合编号
#define	COL_RUNBHAREFTB_CREATETIME					(short)2						// 创建时间戳
#define	COL_RUNBHAREFTB_STATUS					(short)3						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_RUNBHAREFTB_MEMO					(short)4						// 备注

// Colum(Field) Name
#define	FLD_RUNBHAREFTB_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_RUNBHAREFTB_BHAID					_T("BHAID")					// 钻具组合编号
#define	FLD_RUNBHAREFTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_RUNBHAREFTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_RUNBHAREFTB_MEMO					_T("Memo")					// 备注

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_RUNBHAREFTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_RUNBHAREFTB
{
 
  int	iRunID;							// 趟钻编号
  int	iBHAID;							// 钻具组合编号
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_RUNBHAREFTB_MEMO_DIGITS + 1];							// 备注
} TS_RUNBHAREFTB;

typedef	TS_RUNBHAREFTB FAR*	LPTS_RUNBHAREFTB;

#pragma	pack(pop)

#endif // _TRUNBHAREFTB_H

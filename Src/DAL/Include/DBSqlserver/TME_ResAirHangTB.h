//---------------------------------------------------------------------------//
// 文件名: ME_ResAirHangTB.h
// 说明:	仪器内存数据表(空标值）
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/10/9 15:57:04
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TME_ResAirHangTB.h : DME_ResAirHangTB

#ifndef	_TME_RESAIRHANGTB_H
#define	_TME_RESAIRHANGTB_H

#define	TID_ME_RESAIRHANGTB								_T("ME_ResAirHangTB")
#define	OID_ME_RESAIRHANGTB								_T("")

// Sort No
#define	SORT_ME_RESAIRHANGTB_PK0				0							// PK:自增字段
//#define	SORT_ME_RESAIRHANGTB_A1							%#%							// A1:

// Colum No
#define	COL_ME_RESAIRHANGTB_DATAID					(short)0						// 自增字段
#define	COL_ME_RESAIRHANGTB_RUNID					(short)1						// 趟钻编号
#define	COL_ME_RESAIRHANGTB_TOOLID					(short)2						// 工具编号
#define	COL_ME_RESAIRHANGTB_BHAID					(short)3						// 钻具组合编号
#define	COL_ME_RESAIRHANGTB_TOOLSN					(short)4						// 工具编号SN（类似于产品条码）
#define	COL_ME_RESAIRHANGTB_TDATETIME					(short)5						// 空标时间（精确到秒）
#define	COL_ME_RESAIRHANGTB_MILLITIME					(short)6						// 毫秒
#define	COL_ME_RESAIRHANGTB_AH_AFL					(short)7						// 空标值(长源/低频)
#define	COL_ME_RESAIRHANGTB_AH_ANL					(short)8						// 空标值(短源/低频)
#define	COL_ME_RESAIRHANGTB_AH_AFH					(short)9						// 空标值(长源/高频)
#define	COL_ME_RESAIRHANGTB_AH_ANH					(short)10						// 空标值(短源/高频)
#define	COL_ME_RESAIRHANGTB_AH_PFL					(short)11						// 空标值(长源/低频)
#define	COL_ME_RESAIRHANGTB_AH_PNL					(short)12						// 空标值(短源/低频)
#define	COL_ME_RESAIRHANGTB_AH_PFH					(short)13						// 空标值(长源/高频)
#define	COL_ME_RESAIRHANGTB_AH_PNH					(short)14						// 空标值(短源/高频)
#define	COL_ME_RESAIRHANGTB_STDV_AFL					(short)15						// 标准偏差(长源/低频)
#define	COL_ME_RESAIRHANGTB_STDV_ANL					(short)16						// 标准偏差(短源/低频)
#define	COL_ME_RESAIRHANGTB_STDV_AFH					(short)17						// 标准偏差(长源/高频)
#define	COL_ME_RESAIRHANGTB_STDV_ANH					(short)18						// 标准偏差(短源/高频)
#define	COL_ME_RESAIRHANGTB_STDV_PFL					(short)19						// 标准偏差(长源/低频)
#define	COL_ME_RESAIRHANGTB_STDV_PNL					(short)20						// 标准偏差(短源/低频)
#define	COL_ME_RESAIRHANGTB_STDV_PFH					(short)21						// 标准偏差(长源/高频)
#define	COL_ME_RESAIRHANGTB_STDV_PNH					(short)22						// 标准偏差(短源/高频)
#define	COL_ME_RESAIRHANGTB_WITETOTOOL					(short)23						// 是否已经写入到仪器:No|0.否;Yes|1.是
#define	COL_ME_RESAIRHANGTB_CREATETIME					(short)24						// 创建时间戳
#define	COL_ME_RESAIRHANGTB_UPDTIME					(short)25						// 最后一次修改时间戳
#define	COL_ME_RESAIRHANGTB_STATUS					(short)26						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_ME_RESAIRHANGTB_MEMO					(short)27						// 备注

// Colum(Field) Name
#define	FLD_ME_RESAIRHANGTB_DATAID					_T("DataID")					// 自增字段
#define	FLD_ME_RESAIRHANGTB_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_ME_RESAIRHANGTB_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_ME_RESAIRHANGTB_BHAID					_T("BHAID")					// 钻具组合编号
#define	FLD_ME_RESAIRHANGTB_TOOLSN					_T("ToolSN")					// 工具编号SN（类似于产品条码）
#define	FLD_ME_RESAIRHANGTB_TDATETIME					_T("TDateTime")					// 空标时间（精确到秒）
#define	FLD_ME_RESAIRHANGTB_MILLITIME					_T("Millitime")					// 毫秒
#define	FLD_ME_RESAIRHANGTB_AH_AFL					_T("AH_AFL")					// 空标值(长源/低频)
#define	FLD_ME_RESAIRHANGTB_AH_ANL					_T("AH_ANL")					// 空标值(短源/低频)
#define	FLD_ME_RESAIRHANGTB_AH_AFH					_T("AH_AFH")					// 空标值(长源/高频)
#define	FLD_ME_RESAIRHANGTB_AH_ANH					_T("AH_ANH")					// 空标值(短源/高频)
#define	FLD_ME_RESAIRHANGTB_AH_PFL					_T("AH_PFL")					// 空标值(长源/低频)
#define	FLD_ME_RESAIRHANGTB_AH_PNL					_T("AH_PNL")					// 空标值(短源/低频)
#define	FLD_ME_RESAIRHANGTB_AH_PFH					_T("AH_PFH")					// 空标值(长源/高频)
#define	FLD_ME_RESAIRHANGTB_AH_PNH					_T("AH_PNH")					// 空标值(短源/高频)
#define	FLD_ME_RESAIRHANGTB_STDV_AFL					_T("STDV_AFL")					// 标准偏差(长源/低频)
#define	FLD_ME_RESAIRHANGTB_STDV_ANL					_T("STDV_ANL")					// 标准偏差(短源/低频)
#define	FLD_ME_RESAIRHANGTB_STDV_AFH					_T("STDV_AFH")					// 标准偏差(长源/高频)
#define	FLD_ME_RESAIRHANGTB_STDV_ANH					_T("STDV_ANH")					// 标准偏差(短源/高频)
#define	FLD_ME_RESAIRHANGTB_STDV_PFL					_T("STDV_PFL")					// 标准偏差(长源/低频)
#define	FLD_ME_RESAIRHANGTB_STDV_PNL					_T("STDV_PNL")					// 标准偏差(短源/低频)
#define	FLD_ME_RESAIRHANGTB_STDV_PFH					_T("STDV_PFH")					// 标准偏差(长源/高频)
#define	FLD_ME_RESAIRHANGTB_STDV_PNH					_T("STDV_PNH")					// 标准偏差(短源/高频)
#define	FLD_ME_RESAIRHANGTB_WITETOTOOL					_T("WiteToTool")					// 是否已经写入到仪器:No|0.否;Yes|1.是
#define	FLD_ME_RESAIRHANGTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_ME_RESAIRHANGTB_UPDTIME					_T("UpdTime")					// 最后一次修改时间戳
#define	FLD_ME_RESAIRHANGTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_ME_RESAIRHANGTB_MEMO					_T("Memo")					// 备注

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_ME_RESAIRHANGTB_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_ME_RESAIRHANGTB_BHAID_DIGITS				50					// 钻具组合编号位数
#define	TV_ME_RESAIRHANGTB_TOOLSN_DIGITS				50					// 工具编号SN（类似于产品条码）位数
#define	TV_ME_RESAIRHANGTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_ME_RESAIRHANGTB
{
 
  int	iDataID;							// 自增字段
  char	szRunID[TV_ME_RESAIRHANGTB_RUNID_DIGITS + 1];							// 趟钻编号
  int	iToolID;							// 工具编号
  char	szBHAID[TV_ME_RESAIRHANGTB_BHAID_DIGITS + 1];							// 钻具组合编号
  char	szToolSN[TV_ME_RESAIRHANGTB_TOOLSN_DIGITS + 1];							// 工具编号SN（类似于产品条码）
  time_t	lTDateTime;							// 空标时间（精确到秒）
  int	iMillitime;							// 毫秒
  float	fAH_AFL;							// 空标值(长源/低频)
  float	fAH_ANL;							// 空标值(短源/低频)
  float	fAH_AFH;							// 空标值(长源/高频)
  float	fAH_ANH;							// 空标值(短源/高频)
  float	fAH_PFL;							// 空标值(长源/低频)
  float	fAH_PNL;							// 空标值(短源/低频)
  float	fAH_PFH;							// 空标值(长源/高频)
  float	fAH_PNH;							// 空标值(短源/高频)
  float	fSTDV_AFL;							// 标准偏差(长源/低频)
  float	fSTDV_ANL;							// 标准偏差(短源/低频)
  float	fSTDV_AFH;							// 标准偏差(长源/高频)
  float	fSTDV_ANH;							// 标准偏差(短源/高频)
  float	fSTDV_PFL;							// 标准偏差(长源/低频)
  float	fSTDV_PNL;							// 标准偏差(短源/低频)
  float	fSTDV_PFH;							// 标准偏差(长源/高频)
  float	fSTDV_PNH;							// 标准偏差(短源/高频)
  short	nWiteToTool;							// 是否已经写入到仪器:No|0.否;Yes|1.是
  time_t	lCreateTime;							// 创建时间戳
  time_t	lUpdTime;							// 最后一次修改时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_ME_RESAIRHANGTB_MEMO_DIGITS + 1];							// 备注
} TS_ME_RESAIRHANGTB;

typedef	TS_ME_RESAIRHANGTB FAR*	LPTS_ME_RESAIRHANGTB;

#pragma	pack(pop)

#endif // _TME_RESAIRHANGTB_H

//---------------------------------------------------------------------------//
// 文件名: ParaCalc.h
// 说明:	伽马校正参数表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020/12/31 16:51:04
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TParaCalc.h : DParaCalc

#ifndef	_TPARACALC_H
#define	_TPARACALC_H

#define	TID_PARACALC								_T("ParaCalc")
#define	OID_PARACALC								_T("")

// Sort No
#define	SORT_PARACALC_PK0				0							// PK:编号
//#define	SORT_PARACALC_A1							%#%							// A1:

// Colum No
#define	COL_PARACALC_PARID					(short)0						// 编号
#define	COL_PARACALC_RUNID					(short)1						// 趟钻编号
#define	COL_PARACALC_GRTCALCMODE					(short)2						// 伽马和计算方式:FromUp|1.从上伽马开始计算;Down|2.从下伽马开始计算
#define	COL_PARACALC_CREATETIME					(short)3						// 创建时间戳
#define	COL_PARACALC_STATUS					(short)4						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_PARACALC_MEMO					(short)5						// 备注
#define	COL_PARACALC_UPDCOUNT					(short)6						// 更新计数

// Colum(Field) Name
#define	FLD_PARACALC_PARID					_T("ParID")					// 编号
#define	FLD_PARACALC_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_PARACALC_GRTCALCMODE					_T("GRTCalcMode")					// 伽马和计算方式:FromUp|1.从上伽马开始计算;Down|2.从下伽马开始计算
#define	FLD_PARACALC_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_PARACALC_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_PARACALC_MEMO					_T("Memo")					// 备注
#define	FLD_PARACALC_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_PARACALC_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_PARACALC_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_PARACALC
{
 
  int	iParID;							// 编号
  char	szRunID[TV_PARACALC_RUNID_DIGITS + 1];							// 趟钻编号
  short	nGRTCalcMode;							// 伽马和计算方式:FromUp|1.从上伽马开始计算;Down|2.从下伽马开始计算
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_PARACALC_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_PARACALC;

typedef	TS_PARACALC FAR*	LPTS_PARACALC;

#pragma	pack(pop)

#endif // _TPARACALC_H

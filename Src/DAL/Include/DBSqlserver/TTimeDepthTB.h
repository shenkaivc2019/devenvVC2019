//---------------------------------------------------------------------------//
// 文件名: TimeDepthTB.h
// 说明:	时深数据表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/10/9 15:57:05
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TTimeDepthTB.h : DTimeDepthTB

#ifndef	_TTIMEDEPTHTB_H
#define	_TTIMEDEPTHTB_H

#define	TID_TIMEDEPTHTB								_T("TimeDepthTB")
#define	OID_TIMEDEPTHTB								_T("")

// Sort No
#define	SORT_TIMEDEPTHTB_PK0				0							// PK:数据编号
//#define	SORT_TIMEDEPTHTB_A1							%#%							// A1:

// Colum No
#define	COL_TIMEDEPTHTB_DATAID					(short)0						// 数据编号
#define	COL_TIMEDEPTHTB_RUNID					(short)1						// 趟钻编号
#define	COL_TIMEDEPTHTB_TDATETIME					(short)2						// 时间（精确到秒）
#define	COL_TIMEDEPTHTB_MILLITIME					(short)3						// 毫秒
#define	COL_TIMEDEPTHTB_CURVENAME					(short)4						// 曲线名称
#define	COL_TIMEDEPTHTB_WITSID					(short)5						// WITSID
#define	COL_TIMEDEPTHTB_BITDEPTH					(short)6						// 钻头深度 
#define	COL_TIMEDEPTHTB_EDTBITDEPTH					(short)7						// 编辑钻头深度（有可能进行手工修改）
#define	COL_TIMEDEPTHTB_WELLDEPTH					(short)8						// 井深
#define	COL_TIMEDEPTHTB_EDTWELLDEPTH					(short)9						// 编辑井深 
#define	COL_TIMEDEPTHTB_BITTVD					(short)10						// 钻头垂深
#define	COL_TIMEDEPTHTB_WELLTVD					(short)11						// 井深垂深
#define	COL_TIMEDEPTHTB_DEPTHSRC					(short)12						// 深度来源:Salting|0:传感器;WITS|1:WITS;
#define	COL_TIMEDEPTHTB_BAD					(short)13						// 是否坏点:No|0.否;Yes|1.是
#define	COL_TIMEDEPTHTB_DRILLACTIV					(short)14						// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是;Relog|2.复测
#define	COL_TIMEDEPTHTB_SLIDING					(short)15						// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	COL_TIMEDEPTHTB_PUMPON					(short)16						// 是否开泵:No|0.否;Yes|1.是
#define	COL_TIMEDEPTHTB_RELOG					(short)17						// 是否复测 :Yes|0.否;No|1.是
#define	COL_TIMEDEPTHTB_LAS					(short)18						// 是否Las :No|0.否;Yes|1.是
#define	COL_TIMEDEPTHTB_CREATETIME					(short)19						// 创建时间戳
#define	COL_TIMEDEPTHTB_UPDTIME					(short)20						// 最后一次修改时间戳
#define	COL_TIMEDEPTHTB_STATUS					(short)21						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_TIMEDEPTHTB_MEMO					(short)22						// 备注

// Colum(Field) Name
#define	FLD_TIMEDEPTHTB_DATAID					_T("DataID")					// 数据编号
#define	FLD_TIMEDEPTHTB_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_TIMEDEPTHTB_TDATETIME					_T("TDateTime")					// 时间（精确到秒）
#define	FLD_TIMEDEPTHTB_MILLITIME					_T("Millitime")					// 毫秒
#define	FLD_TIMEDEPTHTB_CURVENAME					_T("CurveName")					// 曲线名称
#define	FLD_TIMEDEPTHTB_WITSID					_T("WITSID")					// WITSID
#define	FLD_TIMEDEPTHTB_BITDEPTH					_T("BitDepth")					// 钻头深度 
#define	FLD_TIMEDEPTHTB_EDTBITDEPTH					_T("EdtBitDepth")					// 编辑钻头深度（有可能进行手工修改）
#define	FLD_TIMEDEPTHTB_WELLDEPTH					_T("WellDepth")					// 井深
#define	FLD_TIMEDEPTHTB_EDTWELLDEPTH					_T("EdtWellDepth")					// 编辑井深 
#define	FLD_TIMEDEPTHTB_BITTVD					_T("BitTVD")					// 钻头垂深
#define	FLD_TIMEDEPTHTB_WELLTVD					_T("WellTVD")					// 井深垂深
#define	FLD_TIMEDEPTHTB_DEPTHSRC					_T("DepthSrc")					// 深度来源:Salting|0:传感器;WITS|1:WITS;
#define	FLD_TIMEDEPTHTB_BAD					_T("Bad")					// 是否坏点:No|0.否;Yes|1.是
#define	FLD_TIMEDEPTHTB_DRILLACTIV					_T("DrillActiv")					// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是;Relog|2.复测
#define	FLD_TIMEDEPTHTB_SLIDING					_T("Sliding")					// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	FLD_TIMEDEPTHTB_PUMPON					_T("PumpOn")					// 是否开泵:No|0.否;Yes|1.是
#define	FLD_TIMEDEPTHTB_RELOG					_T("ReLog")					// 是否复测 :Yes|0.否;No|1.是
#define	FLD_TIMEDEPTHTB_LAS					_T("Las")					// 是否Las :No|0.否;Yes|1.是
#define	FLD_TIMEDEPTHTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_TIMEDEPTHTB_UPDTIME					_T("UpdTime")					// 最后一次修改时间戳
#define	FLD_TIMEDEPTHTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_TIMEDEPTHTB_MEMO					_T("Memo")					// 备注

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_TIMEDEPTHTB_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_TIMEDEPTHTB_CURVENAME_DIGITS				50					// 曲线名称位数
#define	TV_TIMEDEPTHTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_TIMEDEPTHTB
{
 
  long	lDataID;							// 数据编号
  char	szRunID[TV_TIMEDEPTHTB_RUNID_DIGITS + 1];							// 趟钻编号
  time_t	lTDateTime;							// 时间（精确到秒）
  int	iMillitime;							// 毫秒
  char	szCurveName[TV_TIMEDEPTHTB_CURVENAME_DIGITS + 1];							// 曲线名称
  int	iWITSID;							// WITSID
  long	lBitDepth;							// 钻头深度 
  long	lEdtBitDepth;							// 编辑钻头深度（有可能进行手工修改）
  long	lWellDepth;							// 井深
  long	lEdtWellDepth;							// 编辑井深 
  long	lBitTVD;							// 钻头垂深
  long	lWellTVD;							// 井深垂深
  short	nDepthSrc;							// 深度来源:Salting|0:传感器;WITS|1:WITS;
  short	nBad;							// 是否坏点:No|0.否;Yes|1.是
  short	nDrillActiv;							// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是;Relog|2.复测
  short	nSliding;							// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
  short	nPumpOn;							// 是否开泵:No|0.否;Yes|1.是
  short	nReLog;							// 是否复测 :Yes|0.否;No|1.是
  short	nLas;							// 是否Las :No|0.否;Yes|1.是
  time_t	lCreateTime;							// 创建时间戳
  time_t	lUpdTime;							// 最后一次修改时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_TIMEDEPTHTB_MEMO_DIGITS + 1];							// 备注
} TS_TIMEDEPTHTB;

typedef	TS_TIMEDEPTHTB FAR*	LPTS_TIMEDEPTHTB;

#pragma	pack(pop)

#endif // _TTIMEDEPTHTB_H

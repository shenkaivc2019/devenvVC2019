//---------------------------------------------------------------------------//
// 文件名: ME_AziResGeoSig.h
// 说明:	方位电阻率地质信号数据表（计算结果）
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/10/13 14:53:33
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TME_AziResGeoSig.h : DME_AziResGeoSig

#ifndef	_TME_AZIRESGEOSIG_H
#define	_TME_AZIRESGEOSIG_H

#define	TID_ME_AZIRESGEOSIG								_T("ME_AziResGeoSig")
#define	OID_ME_AZIRESGEOSIG								_T("")

// Sort No
#define	SORT_ME_AZIRESGEOSIG_PK0				0							// PK:自增字段
//#define	SORT_ME_AZIRESGEOSIG_A1							%#%							// A1:

// Colum No
#define	COL_ME_AZIRESGEOSIG_DATAID					(short)0						// 自增字段
#define	COL_ME_AZIRESGEOSIG_RUNID					(short)1						// 趟钻编号
#define	COL_ME_AZIRESGEOSIG_TDATETIME					(short)2						// 时间（精确到秒）
#define	COL_ME_AZIRESGEOSIG_MILLITIME					(short)3						// 毫秒
#define	COL_ME_AZIRESGEOSIG_TOOLID					(short)4						// 工具编号
#define	COL_ME_AZIRESGEOSIG_AZIRESMDEPTH					(short)5						// 方位电阻率测量井深
#define	COL_ME_AZIRESGEOSIG_AZIRESVDEPTH					(short)6						// 方位电阻率测量深度 
#define	COL_ME_AZIRESGEOSIG_SRC1DATAID					(short)7						// 计算源数据编号1
#define	COL_ME_AZIRESGEOSIG_SRC2DATAID					(short)8						// 计算源数据编号2
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS0					(short)9						// 方位电阻率地质信号S0
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS1					(short)10						// 方位电阻率地质信号S0
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS2					(short)11						// 方位电阻率地质信号S1
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS3					(short)12						// 方位电阻率地质信号S2
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS4					(short)13						// 方位电阻率地质信号S3
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS5					(short)14						// 方位电阻率地质信号S4
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS6					(short)15						// 方位电阻率地质信号S5
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS7					(short)16						// 方位电阻率地质信号S6
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS8					(short)17						// 方位电阻率地质信号S7
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS9					(short)18						// 方位电阻率地质信号S8
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS10					(short)19						// 方位电阻率地质信号S9
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS11					(short)20						// 方位电阻率地质信号S10
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS12					(short)21						// 方位电阻率地质信号S11
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS13					(short)22						// 方位电阻率地质信号S12
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS14					(short)23						// 方位电阻率地质信号S13
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS15					(short)24						// 方位电阻率地质信号S14
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS0M					(short)25						// 方位电阻率地质信号S0修正后值
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS1M					(short)26						// 方位电阻率地质信号S1修正后值
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS2M					(short)27						// 方位电阻率地质信号S2修正后值
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS3M					(short)28						// 方位电阻率地质信号S3修正后值
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS4M					(short)29						// 方位电阻率地质信号S4修正后值
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS5M					(short)30						// 方位电阻率地质信号S5修正后值
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS6M					(short)31						// 方位电阻率地质信号S6修正后值
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS7M					(short)32						// 方位电阻率地质信号S7修正后值
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS8M					(short)33						// 方位电阻率地质信号S8修正后值
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS9M					(short)34						// 方位电阻率地质信号S9修正后值
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS10M					(short)35						// 方位电阻率地质信号S10修正后值
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS11M					(short)36						// 方位电阻率地质信号S11修正后值
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS12M					(short)37						// 方位电阻率地质信号S12修正后值
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS13M					(short)38						// 方位电阻率地质信号S13修正后值
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS14M					(short)39						// 方位电阻率地质信号S14修正后值
#define	COL_ME_AZIRESGEOSIG_ARSIGNALS15M					(short)40						// 方位电阻率地质信号S15修正后值
#define	COL_ME_AZIRESGEOSIG_ARS241S0					(short)41						// 方位电阻率地质信号241S0扇区值
#define	COL_ME_AZIRESGEOSIG_ARS241S1					(short)42						// 方位电阻率地质信号241S1扇区值
#define	COL_ME_AZIRESGEOSIG_ARS241S2					(short)43						// 方位电阻率地质信号241S2扇区值
#define	COL_ME_AZIRESGEOSIG_ARS241S3					(short)44						// 方位电阻率地质信号241S3扇区值
#define	COL_ME_AZIRESGEOSIG_ARS241S4					(short)45						// 方位电阻率地质信号241S4扇区值
#define	COL_ME_AZIRESGEOSIG_ARS241S5					(short)46						// 方位电阻率地质信号241S5扇区值
#define	COL_ME_AZIRESGEOSIG_ARS241S6					(short)47						// 方位电阻率地质信号241S6扇区值
#define	COL_ME_AZIRESGEOSIG_ARS241S7					(short)48						// 方位电阻率地质信号241S7扇区值
#define	COL_ME_AZIRESGEOSIG_ARS241S8					(short)49						// 方位电阻率地质信号241S8扇区值
#define	COL_ME_AZIRESGEOSIG_ARS241S9					(short)50						// 方位电阻率地质信号241S9扇区值
#define	COL_ME_AZIRESGEOSIG_ARS241S10					(short)51						// 方位电阻率地质信号241S10扇区值
#define	COL_ME_AZIRESGEOSIG_ARS241S11					(short)52						// 方位电阻率地质信号241S11扇区值
#define	COL_ME_AZIRESGEOSIG_ARS241S12					(short)53						// 方位电阻率地质信号241S12扇区值
#define	COL_ME_AZIRESGEOSIG_ARS241S13					(short)54						// 方位电阻率地质信号241S13扇区值
#define	COL_ME_AZIRESGEOSIG_ARS241S14					(short)55						// 方位电阻率地质信号241S14扇区值
#define	COL_ME_AZIRESGEOSIG_ARS241S15					(short)56						// 方位电阻率地质信号241S15扇区值
#define	COL_ME_AZIRESGEOSIG_ARS243S0					(short)57						// 方位电阻率地质信号243S0扇区值
#define	COL_ME_AZIRESGEOSIG_ARS243S1					(short)58						// 方位电阻率地质信号243S1扇区值
#define	COL_ME_AZIRESGEOSIG_ARS243S2					(short)59						// 方位电阻率地质信号243S2扇区值
#define	COL_ME_AZIRESGEOSIG_ARS243S3					(short)60						// 方位电阻率地质信号243S3扇区值
#define	COL_ME_AZIRESGEOSIG_ARS243S4					(short)61						// 方位电阻率地质信号243S4扇区值
#define	COL_ME_AZIRESGEOSIG_ARS243S5					(short)62						// 方位电阻率地质信号243S5扇区值
#define	COL_ME_AZIRESGEOSIG_ARS243S6					(short)63						// 方位电阻率地质信号243S6扇区值
#define	COL_ME_AZIRESGEOSIG_ARS243S7					(short)64						// 方位电阻率地质信号243S7扇区值
#define	COL_ME_AZIRESGEOSIG_ARS243S8					(short)65						// 方位电阻率地质信号243S8扇区值
#define	COL_ME_AZIRESGEOSIG_ARS243S9					(short)66						// 方位电阻率地质信号243S9扇区值
#define	COL_ME_AZIRESGEOSIG_ARS243S10					(short)67						// 方位电阻率地质信号243S10扇区值
#define	COL_ME_AZIRESGEOSIG_ARS243S11					(short)68						// 方位电阻率地质信号243S11扇区值
#define	COL_ME_AZIRESGEOSIG_ARS243S12					(short)69						// 方位电阻率地质信号243S12扇区值
#define	COL_ME_AZIRESGEOSIG_ARS243S13					(short)70						// 方位电阻率地质信号243S13扇区值
#define	COL_ME_AZIRESGEOSIG_ARS243S14					(short)71						// 方位电阻率地质信号243S14扇区值
#define	COL_ME_AZIRESGEOSIG_ARS243S15					(short)72						// 方位电阻率地质信号243S15扇区值
#define	COL_ME_AZIRESGEOSIG_ARSRELS0					(short)73						// 方位电阻率地质信号S0实部
#define	COL_ME_AZIRESGEOSIG_ARSRELS1					(short)74						// 方位电阻率地质信号S1实部
#define	COL_ME_AZIRESGEOSIG_ARSRELS2					(short)75						// 方位电阻率地质信号S2实部
#define	COL_ME_AZIRESGEOSIG_ARSRELS3					(short)76						// 方位电阻率地质信号S3实部
#define	COL_ME_AZIRESGEOSIG_ARSRELS4					(short)77						// 方位电阻率地质信号S4实部
#define	COL_ME_AZIRESGEOSIG_ARSRELS5					(short)78						// 方位电阻率地质信号S5实部
#define	COL_ME_AZIRESGEOSIG_ARSRELS6					(short)79						// 方位电阻率地质信号S6实部
#define	COL_ME_AZIRESGEOSIG_ARSRELS7					(short)80						// 方位电阻率地质信号S7实部
#define	COL_ME_AZIRESGEOSIG_ARSRELS8					(short)81						// 方位电阻率地质信号S8实部
#define	COL_ME_AZIRESGEOSIG_ARSRELS9					(short)82						// 方位电阻率地质信号S9实部
#define	COL_ME_AZIRESGEOSIG_ARSRELS10					(short)83						// 方位电阻率地质信号S10实部
#define	COL_ME_AZIRESGEOSIG_ARSRELS11					(short)84						// 方位电阻率地质信号S11实部
#define	COL_ME_AZIRESGEOSIG_ARSRELS12					(short)85						// 方位电阻率地质信号S12实部
#define	COL_ME_AZIRESGEOSIG_ARSRELS13					(short)86						// 方位电阻率地质信号S13实部
#define	COL_ME_AZIRESGEOSIG_ARSRELS14					(short)87						// 方位电阻率地质信号S14实部
#define	COL_ME_AZIRESGEOSIG_ARSRELS15					(short)88						// 方位电阻率地质信号S15实部
#define	COL_ME_AZIRESGEOSIG_AMPRE400K					(short)89						// 400K发射时方位天线接收实部最大值
#define	COL_ME_AZIRESGEOSIG_AMPIM400K					(short)90						// 400K发射时方位天线接收虚部最大值
#define	COL_ME_AZIRESGEOSIG_SECRE400P					(short)91						// 400K发射时方位天线接收实部最大值所对应相位
#define	COL_ME_AZIRESGEOSIG_SECIM400P					(short)92						// 400K发射时方位天线接收虚部最大值所对应相位
#define	COL_ME_AZIRESGEOSIG_SECTORRE400					(short)93						// 400K发射时方位天线接收实部最大值所对应扇区0~15
#define	COL_ME_AZIRESGEOSIG_SECTORIM400					(short)94						// 400K发射时方位天线接收虚部最大值所对应扇区0~15
#define	COL_ME_AZIRESGEOSIG_NBRES					(short)95						// 近地层电阻率
#define	COL_ME_AZIRESGEOSIG_RBRES					(short)96						// 远地层电阻率
#define	COL_ME_AZIRESGEOSIG_D2RB					(short)97						// 界面距离
#define	COL_ME_AZIRESGEOSIG_RBAZI					(short)98						// 界面方位
#define	COL_ME_AZIRESGEOSIG_RUPRES					(short)99						// 实部上电阻率
#define	COL_ME_AZIRESGEOSIG_RDOWNRES					(short)100						// 实部下电阻率
#define	COL_ME_AZIRESGEOSIG_IUPRES					(short)101						// 虚部上电阻率
#define	COL_ME_AZIRESGEOSIG_IDOWNRES					(short)102						// 虚部下电阻率
#define	COL_ME_AZIRESGEOSIG_CONF					(short)103						// 旋转状态:No|0.否;Yes|1.是
#define	COL_ME_AZIRESGEOSIG_BAD					(short)104						// 是否坏点:No|0.否;Yes|1.是
#define	COL_ME_AZIRESGEOSIG_DRILLACTIV					(short)105						// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	COL_ME_AZIRESGEOSIG_SLIDING					(short)106						// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	COL_ME_AZIRESGEOSIG_RELOG					(short)107						// 是否复测 :Yes|0.否;No|1.是
#define	COL_ME_AZIRESGEOSIG_LAS					(short)108						// 是否Las :Yes|0.否;No|1.是
#define	COL_ME_AZIRESGEOSIG_CREATETIME					(short)109						// 创建时间戳
#define	COL_ME_AZIRESGEOSIG_UPDTIME					(short)110						// 最后一次修改时间戳
#define	COL_ME_AZIRESGEOSIG_STATUS					(short)111						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_ME_AZIRESGEOSIG_MEMO					(short)112						// 备注

// Colum(Field) Name
#define	FLD_ME_AZIRESGEOSIG_DATAID					_T("DataID")					// 自增字段
#define	FLD_ME_AZIRESGEOSIG_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_ME_AZIRESGEOSIG_TDATETIME					_T("TDateTime")					// 时间（精确到秒）
#define	FLD_ME_AZIRESGEOSIG_MILLITIME					_T("Millitime")					// 毫秒
#define	FLD_ME_AZIRESGEOSIG_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_ME_AZIRESGEOSIG_AZIRESMDEPTH					_T("AZIResMDepth")					// 方位电阻率测量井深
#define	FLD_ME_AZIRESGEOSIG_AZIRESVDEPTH					_T("AZIResVDepth")					// 方位电阻率测量深度 
#define	FLD_ME_AZIRESGEOSIG_SRC1DATAID					_T("Src1DataID")					// 计算源数据编号1
#define	FLD_ME_AZIRESGEOSIG_SRC2DATAID					_T("Src2DataID")					// 计算源数据编号2
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS0					_T("ARSignalS0")					// 方位电阻率地质信号S0
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS1					_T("ARSignalS1")					// 方位电阻率地质信号S0
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS2					_T("ARSignalS2")					// 方位电阻率地质信号S1
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS3					_T("ARSignalS3")					// 方位电阻率地质信号S2
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS4					_T("ARSignalS4")					// 方位电阻率地质信号S3
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS5					_T("ARSignalS5")					// 方位电阻率地质信号S4
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS6					_T("ARSignalS6")					// 方位电阻率地质信号S5
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS7					_T("ARSignalS7")					// 方位电阻率地质信号S6
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS8					_T("ARSignalS8")					// 方位电阻率地质信号S7
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS9					_T("ARSignalS9")					// 方位电阻率地质信号S8
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS10					_T("ARSignalS10")					// 方位电阻率地质信号S9
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS11					_T("ARSignalS11")					// 方位电阻率地质信号S10
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS12					_T("ARSignalS12")					// 方位电阻率地质信号S11
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS13					_T("ARSignalS13")					// 方位电阻率地质信号S12
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS14					_T("ARSignalS14")					// 方位电阻率地质信号S13
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS15					_T("ARSignalS15")					// 方位电阻率地质信号S14
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS0M					_T("ARSignalS0M")					// 方位电阻率地质信号S0修正后值
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS1M					_T("ARSignalS1M")					// 方位电阻率地质信号S1修正后值
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS2M					_T("ARSignalS2M")					// 方位电阻率地质信号S2修正后值
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS3M					_T("ARSignalS3M")					// 方位电阻率地质信号S3修正后值
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS4M					_T("ARSignalS4M")					// 方位电阻率地质信号S4修正后值
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS5M					_T("ARSignalS5M")					// 方位电阻率地质信号S5修正后值
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS6M					_T("ARSignalS6M")					// 方位电阻率地质信号S6修正后值
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS7M					_T("ARSignalS7M")					// 方位电阻率地质信号S7修正后值
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS8M					_T("ARSignalS8M")					// 方位电阻率地质信号S8修正后值
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS9M					_T("ARSignalS9M")					// 方位电阻率地质信号S9修正后值
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS10M					_T("ARSignalS10M")					// 方位电阻率地质信号S10修正后值
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS11M					_T("ARSignalS11M")					// 方位电阻率地质信号S11修正后值
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS12M					_T("ARSignalS12M")					// 方位电阻率地质信号S12修正后值
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS13M					_T("ARSignalS13M")					// 方位电阻率地质信号S13修正后值
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS14M					_T("ARSignalS14M")					// 方位电阻率地质信号S14修正后值
#define	FLD_ME_AZIRESGEOSIG_ARSIGNALS15M					_T("ARSignalS15M")					// 方位电阻率地质信号S15修正后值
#define	FLD_ME_AZIRESGEOSIG_ARS241S0					_T("ARS241S0")					// 方位电阻率地质信号241S0扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS241S1					_T("ARS241S1")					// 方位电阻率地质信号241S1扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS241S2					_T("ARS241S2")					// 方位电阻率地质信号241S2扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS241S3					_T("ARS241S3")					// 方位电阻率地质信号241S3扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS241S4					_T("ARS241S4")					// 方位电阻率地质信号241S4扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS241S5					_T("ARS241S5")					// 方位电阻率地质信号241S5扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS241S6					_T("ARS241S6")					// 方位电阻率地质信号241S6扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS241S7					_T("ARS241S7")					// 方位电阻率地质信号241S7扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS241S8					_T("ARS241S8")					// 方位电阻率地质信号241S8扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS241S9					_T("ARS241S9")					// 方位电阻率地质信号241S9扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS241S10					_T("ARS241S10")					// 方位电阻率地质信号241S10扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS241S11					_T("ARS241S11")					// 方位电阻率地质信号241S11扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS241S12					_T("ARS241S12")					// 方位电阻率地质信号241S12扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS241S13					_T("ARS241S13")					// 方位电阻率地质信号241S13扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS241S14					_T("ARS241S14")					// 方位电阻率地质信号241S14扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS241S15					_T("ARS241S15")					// 方位电阻率地质信号241S15扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS243S0					_T("ARS243S0")					// 方位电阻率地质信号243S0扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS243S1					_T("ARS243S1")					// 方位电阻率地质信号243S1扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS243S2					_T("ARS243S2")					// 方位电阻率地质信号243S2扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS243S3					_T("ARS243S3")					// 方位电阻率地质信号243S3扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS243S4					_T("ARS243S4")					// 方位电阻率地质信号243S4扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS243S5					_T("ARS243S5")					// 方位电阻率地质信号243S5扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS243S6					_T("ARS243S6")					// 方位电阻率地质信号243S6扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS243S7					_T("ARS243S7")					// 方位电阻率地质信号243S7扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS243S8					_T("ARS243S8")					// 方位电阻率地质信号243S8扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS243S9					_T("ARS243S9")					// 方位电阻率地质信号243S9扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS243S10					_T("ARS243S10")					// 方位电阻率地质信号243S10扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS243S11					_T("ARS243S11")					// 方位电阻率地质信号243S11扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS243S12					_T("ARS243S12")					// 方位电阻率地质信号243S12扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS243S13					_T("ARS243S13")					// 方位电阻率地质信号243S13扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS243S14					_T("ARS243S14")					// 方位电阻率地质信号243S14扇区值
#define	FLD_ME_AZIRESGEOSIG_ARS243S15					_T("ARS243S15")					// 方位电阻率地质信号243S15扇区值
#define	FLD_ME_AZIRESGEOSIG_ARSRELS0					_T("ARSRelS0")					// 方位电阻率地质信号S0实部
#define	FLD_ME_AZIRESGEOSIG_ARSRELS1					_T("ARSRelS1")					// 方位电阻率地质信号S1实部
#define	FLD_ME_AZIRESGEOSIG_ARSRELS2					_T("ARSRelS2")					// 方位电阻率地质信号S2实部
#define	FLD_ME_AZIRESGEOSIG_ARSRELS3					_T("ARSRelS3")					// 方位电阻率地质信号S3实部
#define	FLD_ME_AZIRESGEOSIG_ARSRELS4					_T("ARSRelS4")					// 方位电阻率地质信号S4实部
#define	FLD_ME_AZIRESGEOSIG_ARSRELS5					_T("ARSRelS5")					// 方位电阻率地质信号S5实部
#define	FLD_ME_AZIRESGEOSIG_ARSRELS6					_T("ARSRelS6")					// 方位电阻率地质信号S6实部
#define	FLD_ME_AZIRESGEOSIG_ARSRELS7					_T("ARSRelS7")					// 方位电阻率地质信号S7实部
#define	FLD_ME_AZIRESGEOSIG_ARSRELS8					_T("ARSRelS8")					// 方位电阻率地质信号S8实部
#define	FLD_ME_AZIRESGEOSIG_ARSRELS9					_T("ARSRelS9")					// 方位电阻率地质信号S9实部
#define	FLD_ME_AZIRESGEOSIG_ARSRELS10					_T("ARSRelS10")					// 方位电阻率地质信号S10实部
#define	FLD_ME_AZIRESGEOSIG_ARSRELS11					_T("ARSRelS11")					// 方位电阻率地质信号S11实部
#define	FLD_ME_AZIRESGEOSIG_ARSRELS12					_T("ARSRelS12")					// 方位电阻率地质信号S12实部
#define	FLD_ME_AZIRESGEOSIG_ARSRELS13					_T("ARSRelS13")					// 方位电阻率地质信号S13实部
#define	FLD_ME_AZIRESGEOSIG_ARSRELS14					_T("ARSRelS14")					// 方位电阻率地质信号S14实部
#define	FLD_ME_AZIRESGEOSIG_ARSRELS15					_T("ARSRelS15")					// 方位电阻率地质信号S15实部
#define	FLD_ME_AZIRESGEOSIG_AMPRE400K					_T("AmpRE400K")					// 400K发射时方位天线接收实部最大值
#define	FLD_ME_AZIRESGEOSIG_AMPIM400K					_T("AmpIM400K")					// 400K发射时方位天线接收虚部最大值
#define	FLD_ME_AZIRESGEOSIG_SECRE400P					_T("SecRe400P")					// 400K发射时方位天线接收实部最大值所对应相位
#define	FLD_ME_AZIRESGEOSIG_SECIM400P					_T("SecIm400P")					// 400K发射时方位天线接收虚部最大值所对应相位
#define	FLD_ME_AZIRESGEOSIG_SECTORRE400					_T("SectorRe400")					// 400K发射时方位天线接收实部最大值所对应扇区0~15
#define	FLD_ME_AZIRESGEOSIG_SECTORIM400					_T("SectorIm400")					// 400K发射时方位天线接收虚部最大值所对应扇区0~15
#define	FLD_ME_AZIRESGEOSIG_NBRES					_T("NBRes")					// 近地层电阻率
#define	FLD_ME_AZIRESGEOSIG_RBRES					_T("RBRes")					// 远地层电阻率
#define	FLD_ME_AZIRESGEOSIG_D2RB					_T("D2RB")					// 界面距离
#define	FLD_ME_AZIRESGEOSIG_RBAZI					_T("RBAZI")					// 界面方位
#define	FLD_ME_AZIRESGEOSIG_RUPRES					_T("RUpRes")					// 实部上电阻率
#define	FLD_ME_AZIRESGEOSIG_RDOWNRES					_T("RDownRes")					// 实部下电阻率
#define	FLD_ME_AZIRESGEOSIG_IUPRES					_T("IUpRes")					// 虚部上电阻率
#define	FLD_ME_AZIRESGEOSIG_IDOWNRES					_T("IDownRes")					// 虚部下电阻率
#define	FLD_ME_AZIRESGEOSIG_CONF					_T("Conf")					// 旋转状态:No|0.否;Yes|1.是
#define	FLD_ME_AZIRESGEOSIG_BAD					_T("Bad")					// 是否坏点:No|0.否;Yes|1.是
#define	FLD_ME_AZIRESGEOSIG_DRILLACTIV					_T("DrillActiv")					// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	FLD_ME_AZIRESGEOSIG_SLIDING					_T("Sliding")					// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	FLD_ME_AZIRESGEOSIG_RELOG					_T("ReLog")					// 是否复测 :Yes|0.否;No|1.是
#define	FLD_ME_AZIRESGEOSIG_LAS					_T("Las")					// 是否Las :Yes|0.否;No|1.是
#define	FLD_ME_AZIRESGEOSIG_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_ME_AZIRESGEOSIG_UPDTIME					_T("UpdTime")					// 最后一次修改时间戳
#define	FLD_ME_AZIRESGEOSIG_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_ME_AZIRESGEOSIG_MEMO					_T("Memo")					// 备注

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_ME_AZIRESGEOSIG_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_ME_AZIRESGEOSIG_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_ME_AZIRESGEOSIG
{
 
  int	iDataID;							// 自增字段
  char	szRunID[TV_ME_AZIRESGEOSIG_RUNID_DIGITS + 1];							// 趟钻编号
  time_t	lTDateTime;							// 时间（精确到秒）
  int	iMillitime;							// 毫秒
  int	iToolID;							// 工具编号
  long	lAZIResMDepth;							// 方位电阻率测量井深
  long	lAZIResVDepth;							// 方位电阻率测量深度 
  int	iSrc1DataID;							// 计算源数据编号1
  int	iSrc2DataID;							// 计算源数据编号2
  float	fARSignalS0;							// 方位电阻率地质信号S0
  float	fARSignalS1;							// 方位电阻率地质信号S0
  float	fARSignalS2;							// 方位电阻率地质信号S1
  float	fARSignalS3;							// 方位电阻率地质信号S2
  float	fARSignalS4;							// 方位电阻率地质信号S3
  float	fARSignalS5;							// 方位电阻率地质信号S4
  float	fARSignalS6;							// 方位电阻率地质信号S5
  float	fARSignalS7;							// 方位电阻率地质信号S6
  float	fARSignalS8;							// 方位电阻率地质信号S7
  float	fARSignalS9;							// 方位电阻率地质信号S8
  float	fARSignalS10;							// 方位电阻率地质信号S9
  float	fARSignalS11;							// 方位电阻率地质信号S10
  float	fARSignalS12;							// 方位电阻率地质信号S11
  float	fARSignalS13;							// 方位电阻率地质信号S12
  float	fARSignalS14;							// 方位电阻率地质信号S13
  float	fARSignalS15;							// 方位电阻率地质信号S14
  float	fARSignalS0M;							// 方位电阻率地质信号S0修正后值
  float	fARSignalS1M;							// 方位电阻率地质信号S1修正后值
  float	fARSignalS2M;							// 方位电阻率地质信号S2修正后值
  float	fARSignalS3M;							// 方位电阻率地质信号S3修正后值
  float	fARSignalS4M;							// 方位电阻率地质信号S4修正后值
  float	fARSignalS5M;							// 方位电阻率地质信号S5修正后值
  float	fARSignalS6M;							// 方位电阻率地质信号S6修正后值
  float	fARSignalS7M;							// 方位电阻率地质信号S7修正后值
  float	fARSignalS8M;							// 方位电阻率地质信号S8修正后值
  float	fARSignalS9M;							// 方位电阻率地质信号S9修正后值
  float	fARSignalS10M;							// 方位电阻率地质信号S10修正后值
  float	fARSignalS11M;							// 方位电阻率地质信号S11修正后值
  float	fARSignalS12M;							// 方位电阻率地质信号S12修正后值
  float	fARSignalS13M;							// 方位电阻率地质信号S13修正后值
  float	fARSignalS14M;							// 方位电阻率地质信号S14修正后值
  float	fARSignalS15M;							// 方位电阻率地质信号S15修正后值
  float	fARS241S0;							// 方位电阻率地质信号241S0扇区值
  float	fARS241S1;							// 方位电阻率地质信号241S1扇区值
  float	fARS241S2;							// 方位电阻率地质信号241S2扇区值
  float	fARS241S3;							// 方位电阻率地质信号241S3扇区值
  float	fARS241S4;							// 方位电阻率地质信号241S4扇区值
  float	fARS241S5;							// 方位电阻率地质信号241S5扇区值
  float	fARS241S6;							// 方位电阻率地质信号241S6扇区值
  float	fARS241S7;							// 方位电阻率地质信号241S7扇区值
  float	fARS241S8;							// 方位电阻率地质信号241S8扇区值
  float	fARS241S9;							// 方位电阻率地质信号241S9扇区值
  float	fARS241S10;							// 方位电阻率地质信号241S10扇区值
  float	fARS241S11;							// 方位电阻率地质信号241S11扇区值
  float	fARS241S12;							// 方位电阻率地质信号241S12扇区值
  float	fARS241S13;							// 方位电阻率地质信号241S13扇区值
  float	fARS241S14;							// 方位电阻率地质信号241S14扇区值
  float	fARS241S15;							// 方位电阻率地质信号241S15扇区值
  float	fARS243S0;							// 方位电阻率地质信号243S0扇区值
  float	fARS243S1;							// 方位电阻率地质信号243S1扇区值
  float	fARS243S2;							// 方位电阻率地质信号243S2扇区值
  float	fARS243S3;							// 方位电阻率地质信号243S3扇区值
  float	fARS243S4;							// 方位电阻率地质信号243S4扇区值
  float	fARS243S5;							// 方位电阻率地质信号243S5扇区值
  float	fARS243S6;							// 方位电阻率地质信号243S6扇区值
  float	fARS243S7;							// 方位电阻率地质信号243S7扇区值
  float	fARS243S8;							// 方位电阻率地质信号243S8扇区值
  float	fARS243S9;							// 方位电阻率地质信号243S9扇区值
  float	fARS243S10;							// 方位电阻率地质信号243S10扇区值
  float	fARS243S11;							// 方位电阻率地质信号243S11扇区值
  float	fARS243S12;							// 方位电阻率地质信号243S12扇区值
  float	fARS243S13;							// 方位电阻率地质信号243S13扇区值
  float	fARS243S14;							// 方位电阻率地质信号243S14扇区值
  float	fARS243S15;							// 方位电阻率地质信号243S15扇区值
  float	fARSRelS0;							// 方位电阻率地质信号S0实部
  float	fARSRelS1;							// 方位电阻率地质信号S1实部
  float	fARSRelS2;							// 方位电阻率地质信号S2实部
  float	fARSRelS3;							// 方位电阻率地质信号S3实部
  float	fARSRelS4;							// 方位电阻率地质信号S4实部
  float	fARSRelS5;							// 方位电阻率地质信号S5实部
  float	fARSRelS6;							// 方位电阻率地质信号S6实部
  float	fARSRelS7;							// 方位电阻率地质信号S7实部
  float	fARSRelS8;							// 方位电阻率地质信号S8实部
  float	fARSRelS9;							// 方位电阻率地质信号S9实部
  float	fARSRelS10;							// 方位电阻率地质信号S10实部
  float	fARSRelS11;							// 方位电阻率地质信号S11实部
  float	fARSRelS12;							// 方位电阻率地质信号S12实部
  float	fARSRelS13;							// 方位电阻率地质信号S13实部
  float	fARSRelS14;							// 方位电阻率地质信号S14实部
  float	fARSRelS15;							// 方位电阻率地质信号S15实部
  float	fAmpRE400K;							// 400K发射时方位天线接收实部最大值
  float	fAmpIM400K;							// 400K发射时方位天线接收虚部最大值
  float	fSecRe400P;							// 400K发射时方位天线接收实部最大值所对应相位
  float	fSecIm400P;							// 400K发射时方位天线接收虚部最大值所对应相位
  int	iSectorRe400;							// 400K发射时方位天线接收实部最大值所对应扇区0~15
  int	iSectorIm400;							// 400K发射时方位天线接收虚部最大值所对应扇区0~15
  float	fNBRes;							// 近地层电阻率
  float	fRBRes;							// 远地层电阻率
  float	fD2RB;							// 界面距离
  float	fRBAZI;							// 界面方位
  float	fRUpRes;							// 实部上电阻率
  float	fRDownRes;							// 实部下电阻率
  float	fIUpRes;							// 虚部上电阻率
  float	fIDownRes;							// 虚部下电阻率
  short	nConf;							// 旋转状态:No|0.否;Yes|1.是
  short	nBad;							// 是否坏点:No|0.否;Yes|1.是
  short	nDrillActiv;							// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
  short	nSliding;							// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
  short	nReLog;							// 是否复测 :Yes|0.否;No|1.是
  short	nLas;							// 是否Las :Yes|0.否;No|1.是
  time_t	lCreateTime;							// 创建时间戳
  time_t	lUpdTime;							// 最后一次修改时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_ME_AZIRESGEOSIG_MEMO_DIGITS + 1];							// 备注
} TS_ME_AZIRESGEOSIG;

typedef	TS_ME_AZIRESGEOSIG FAR*	LPTS_ME_AZIRESGEOSIG;

#pragma	pack(pop)

#endif // _TME_AZIRESGEOSIG_H

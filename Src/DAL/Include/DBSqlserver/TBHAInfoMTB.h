//---------------------------------------------------------------------------//
// 文件名: BHAInfoMTB.h
// 说明:	钻具组合信息主表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020/10/19 22:11:35
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TBHAInfoMTB.h : DBHAInfoMTB

#ifndef	_TBHAINFOMTB_H
#define	_TBHAINFOMTB_H

#define	TID_BHAINFOMTB								_T("BHAInfoMTB")
#define	OID_BHAINFOMTB								_T("")

// Sort No
#define	SORT_BHAINFOMTB_PK0				0							// PK:钻具组合编号
//#define	SORT_BHAINFOMTB_A1							%#%							// A1:

// Colum No
#define	COL_BHAINFOMTB_BHAID					(short)0						// 钻具组合编号
#define	COL_BHAINFOMTB_BHANAME					(short)1						// 钻具组合名称
#define	COL_BHAINFOMTB_BHADESCRIPTION					(short)2						// 钻具组合描述
#define	COL_BHAINFOMTB_LENSUM					(short)3						// 长度总和
#define	COL_BHAINFOMTB_WEISUM					(short)4						// 重量总和
#define	COL_BHAINFOMTB_DRAWTEMP					(short)5						// 绘图模版
#define	COL_BHAINFOMTB_CHARTFILE					(short)6						// 图表文件
#define	COL_BHAINFOMTB_FEEDGRAPH					(short)7						// 给合图
#define	COL_BHAINFOMTB_BOTTOBIT					(short)8						// 深度偏移(本钻具组合到钻头的距离)
#define	COL_BHAINFOMTB_CREATETIME					(short)9						// 创建时间戳
#define	COL_BHAINFOMTB_STATUS					(short)10						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_BHAINFOMTB_MEMO					(short)11						// 备注
#define	COL_BHAINFOMTB_UPDCOUNT					(short)12						// 更新计数

// Colum(Field) Name
#define	FLD_BHAINFOMTB_BHAID					_T("BHAID")					// 钻具组合编号
#define	FLD_BHAINFOMTB_BHANAME					_T("BHAName")					// 钻具组合名称
#define	FLD_BHAINFOMTB_BHADESCRIPTION					_T("BHADescription")					// 钻具组合描述
#define	FLD_BHAINFOMTB_LENSUM					_T("LenSum")					// 长度总和
#define	FLD_BHAINFOMTB_WEISUM					_T("WeiSum")					// 重量总和
#define	FLD_BHAINFOMTB_DRAWTEMP					_T("DrawTemp")					// 绘图模版
#define	FLD_BHAINFOMTB_CHARTFILE					_T("ChartFile")					// 图表文件
#define	FLD_BHAINFOMTB_FEEDGRAPH					_T("FeedGraph")					// 给合图
#define	FLD_BHAINFOMTB_BOTTOBIT					_T("BotToBit")					// 深度偏移(本钻具组合到钻头的距离)
#define	FLD_BHAINFOMTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_BHAINFOMTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_BHAINFOMTB_MEMO					_T("Memo")					// 备注
#define	FLD_BHAINFOMTB_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_BHAINFOMTB_BHAID_DIGITS				50					// 钻具组合编号位数
#define	TV_BHAINFOMTB_BHANAME_DIGITS				50					// 钻具组合名称位数
#define	TV_BHAINFOMTB_BHADESCRIPTION_DIGITS				500					// 钻具组合描述位数
#define	TV_BHAINFOMTB_DRAWTEMP_DIGITS				255					// 绘图模版位数
#define	TV_BHAINFOMTB_CHARTFILE_DIGITS				255					// 图表文件位数
#define	TV_BHAINFOMTB_FEEDGRAPH_DIGITS				255					// 给合图位数
#define	TV_BHAINFOMTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_BHAINFOMTB
{
 
  char	szBHAID[TV_BHAINFOMTB_BHAID_DIGITS + 1];							// 钻具组合编号
  char	szBHAName[TV_BHAINFOMTB_BHANAME_DIGITS + 1];							// 钻具组合名称
  char	szBHADescription[TV_BHAINFOMTB_BHADESCRIPTION_DIGITS + 1];							// 钻具组合描述
  double	dLenSum;							// 长度总和
  double	dWeiSum;							// 重量总和
  char	szDrawTemp[TV_BHAINFOMTB_DRAWTEMP_DIGITS + 1];							// 绘图模版
  char	szChartFile[TV_BHAINFOMTB_CHARTFILE_DIGITS + 1];							// 图表文件
  char	szFeedGraph[TV_BHAINFOMTB_FEEDGRAPH_DIGITS + 1];							// 给合图
  double	dBotToBit;							// 深度偏移(本钻具组合到钻头的距离)
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_BHAINFOMTB_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_BHAINFOMTB;

typedef	TS_BHAINFOMTB FAR*	LPTS_BHAINFOMTB;

#pragma	pack(pop)

#endif // _TBHAINFOMTB_H

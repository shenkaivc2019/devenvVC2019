//---------------------------------------------------------------------------//
// 文件名: ME_AziResTB.h
// 说明:	仪器内存数据表(方位电阻率）
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/10/13 14:53:33
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TME_AziResTB.h : DME_AziResTB

#ifndef	_TME_AZIRESTB_H
#define	_TME_AZIRESTB_H

#define	TID_ME_AZIRESTB								_T("ME_AziResTB")
#define	OID_ME_AZIRESTB								_T("")

// Sort No
#define	SORT_ME_AZIRESTB_PK0				0							// PK:自增字段
//#define	SORT_ME_AZIRESTB_A1							%#%							// A1:

// Colum No
#define	COL_ME_AZIRESTB_DATAID					(short)0						// 自增字段
#define	COL_ME_AZIRESTB_RUNID					(short)1						// 趟钻编号
#define	COL_ME_AZIRESTB_TDATETIME					(short)2						// 时间（精确到秒）
#define	COL_ME_AZIRESTB_MILLITIME					(short)3						// 毫秒
#define	COL_ME_AZIRESTB_TOOLID					(short)4						// 工具编号
#define	COL_ME_AZIRESTB_AZIRESMDEPTH					(short)5						// 方位电阻率测量井深
#define	COL_ME_AZIRESTB_AZIRESVDEPTH					(short)6						// 方位电阻率测量深度 
#define	COL_ME_AZIRESTB_DATATYPE					(short)7						// 天线类型:2M1|240.2M天线数据;400K1|241.400K天线数据;2M2|242.2M数据天线2; 400K2|243.400K天线2
#define	COL_ME_AZIRESTB_R3A_S0					(short)8						// 16扇区0扇区方位天线接收幅度值
#define	COL_ME_AZIRESTB_R3A_S1					(short)9						// 16扇区1扇区方位天线接收幅度值
#define	COL_ME_AZIRESTB_R3A_S2					(short)10						// 16扇区2扇区方位天线接收幅度值
#define	COL_ME_AZIRESTB_R3A_S3					(short)11						// 16扇区3扇区方位天线接收幅度值
#define	COL_ME_AZIRESTB_R3A_S4					(short)12						// 16扇区4扇区方位天线接收幅度值
#define	COL_ME_AZIRESTB_R3A_S5					(short)13						// 16扇区5扇区方位天线接收幅度值
#define	COL_ME_AZIRESTB_R3A_S6					(short)14						// 16扇区6扇区方位天线接收幅度值
#define	COL_ME_AZIRESTB_R3A_S7					(short)15						// 16扇区7扇区方位天线接收幅度值
#define	COL_ME_AZIRESTB_R3A_S8					(short)16						// 16扇区8扇区方位天线接收幅度值
#define	COL_ME_AZIRESTB_R3A_S9					(short)17						// 16扇区9扇区方位天线接收幅度值
#define	COL_ME_AZIRESTB_R3A_S10					(short)18						// 16扇区10扇区方位天线接收幅度值
#define	COL_ME_AZIRESTB_R3A_S11					(short)19						// 16扇区11扇区方位天线接收幅度值
#define	COL_ME_AZIRESTB_R3A_S12					(short)20						// 16扇区12扇区方位天线接收幅度值
#define	COL_ME_AZIRESTB_R3A_S13					(short)21						// 16扇区13扇区方位天线接收幅度值
#define	COL_ME_AZIRESTB_R3A_S14					(short)22						// 16扇区14扇区方位天线接收幅度值
#define	COL_ME_AZIRESTB_R3A_S15					(short)23						// 16扇区15扇区方位天线接收幅度值
#define	COL_ME_AZIRESTB_R3P_S0					(short)24						// 16扇区0扇区方位天线接收相位
#define	COL_ME_AZIRESTB_R3P_S1					(short)25						// 16扇区1扇区方位天线接收相位
#define	COL_ME_AZIRESTB_R3P_S2					(short)26						// 16扇区2扇区方位天线接收相位
#define	COL_ME_AZIRESTB_R3P_S3					(short)27						// 16扇区3扇区方位天线接收相位
#define	COL_ME_AZIRESTB_R3P_S4					(short)28						// 16扇区4扇区方位天线接收相位
#define	COL_ME_AZIRESTB_R3P_S5					(short)29						// 16扇区5扇区方位天线接收相位
#define	COL_ME_AZIRESTB_R3P_S6					(short)30						// 16扇区6扇区方位天线接收相位
#define	COL_ME_AZIRESTB_R3P_S7					(short)31						// 16扇区7扇区方位天线接收相位
#define	COL_ME_AZIRESTB_R3P_S8					(short)32						// 16扇区8扇区方位天线接收相位
#define	COL_ME_AZIRESTB_R3P_S9					(short)33						// 16扇区9扇区方位天线接收相位
#define	COL_ME_AZIRESTB_R3P_S10					(short)34						// 16扇区10扇区方位天线接收相位
#define	COL_ME_AZIRESTB_R3P_S11					(short)35						// 16扇区11扇区方位天线接收相位
#define	COL_ME_AZIRESTB_R3P_S12					(short)36						// 16扇区12扇区方位天线接收相位
#define	COL_ME_AZIRESTB_R3P_S13					(short)37						// 16扇区13扇区方位天线接收相位
#define	COL_ME_AZIRESTB_R3P_S14					(short)38						// 16扇区14扇区方位天线接收相位
#define	COL_ME_AZIRESTB_R3P_S15					(short)39						// 16扇区15扇区方位天线接收相位
#define	COL_ME_AZIRESTB_CONF					(short)40						// 旋转状态:No|0.否;Yes|1.是
#define	COL_ME_AZIRESTB_BAD					(short)41						// 是否坏点:No|0.否;Yes|1.是
#define	COL_ME_AZIRESTB_DRILLACTIV					(short)42						// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	COL_ME_AZIRESTB_SLIDING					(short)43						// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	COL_ME_AZIRESTB_RELOG					(short)44						// 是否复测 :Yes|0.否;No|1.是
#define	COL_ME_AZIRESTB_LAS					(short)45						// 是否Las :Yes|0.否;No|1.是
#define	COL_ME_AZIRESTB_CREATETIME					(short)46						// 创建时间戳
#define	COL_ME_AZIRESTB_UPDTIME					(short)47						// 最后一次修改时间戳
#define	COL_ME_AZIRESTB_STATUS					(short)48						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_ME_AZIRESTB_MEMO					(short)49						// 备注

// Colum(Field) Name
#define	FLD_ME_AZIRESTB_DATAID					_T("DataID")					// 自增字段
#define	FLD_ME_AZIRESTB_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_ME_AZIRESTB_TDATETIME					_T("TDateTime")					// 时间（精确到秒）
#define	FLD_ME_AZIRESTB_MILLITIME					_T("Millitime")					// 毫秒
#define	FLD_ME_AZIRESTB_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_ME_AZIRESTB_AZIRESMDEPTH					_T("AZIResMDepth")					// 方位电阻率测量井深
#define	FLD_ME_AZIRESTB_AZIRESVDEPTH					_T("AZIResVDepth")					// 方位电阻率测量深度 
#define	FLD_ME_AZIRESTB_DATATYPE					_T("DataType")					// 天线类型:2M1|240.2M天线数据;400K1|241.400K天线数据;2M2|242.2M数据天线2; 400K2|243.400K天线2
#define	FLD_ME_AZIRESTB_R3A_S0					_T("R3A_S0")					// 16扇区0扇区方位天线接收幅度值
#define	FLD_ME_AZIRESTB_R3A_S1					_T("R3A_S1")					// 16扇区1扇区方位天线接收幅度值
#define	FLD_ME_AZIRESTB_R3A_S2					_T("R3A_S2")					// 16扇区2扇区方位天线接收幅度值
#define	FLD_ME_AZIRESTB_R3A_S3					_T("R3A_S3")					// 16扇区3扇区方位天线接收幅度值
#define	FLD_ME_AZIRESTB_R3A_S4					_T("R3A_S4")					// 16扇区4扇区方位天线接收幅度值
#define	FLD_ME_AZIRESTB_R3A_S5					_T("R3A_S5")					// 16扇区5扇区方位天线接收幅度值
#define	FLD_ME_AZIRESTB_R3A_S6					_T("R3A_S6")					// 16扇区6扇区方位天线接收幅度值
#define	FLD_ME_AZIRESTB_R3A_S7					_T("R3A_S7")					// 16扇区7扇区方位天线接收幅度值
#define	FLD_ME_AZIRESTB_R3A_S8					_T("R3A_S8")					// 16扇区8扇区方位天线接收幅度值
#define	FLD_ME_AZIRESTB_R3A_S9					_T("R3A_S9")					// 16扇区9扇区方位天线接收幅度值
#define	FLD_ME_AZIRESTB_R3A_S10					_T("R3A_S10")					// 16扇区10扇区方位天线接收幅度值
#define	FLD_ME_AZIRESTB_R3A_S11					_T("R3A_S11")					// 16扇区11扇区方位天线接收幅度值
#define	FLD_ME_AZIRESTB_R3A_S12					_T("R3A_S12")					// 16扇区12扇区方位天线接收幅度值
#define	FLD_ME_AZIRESTB_R3A_S13					_T("R3A_S13")					// 16扇区13扇区方位天线接收幅度值
#define	FLD_ME_AZIRESTB_R3A_S14					_T("R3A_S14")					// 16扇区14扇区方位天线接收幅度值
#define	FLD_ME_AZIRESTB_R3A_S15					_T("R3A_S15")					// 16扇区15扇区方位天线接收幅度值
#define	FLD_ME_AZIRESTB_R3P_S0					_T("R3P_S0")					// 16扇区0扇区方位天线接收相位
#define	FLD_ME_AZIRESTB_R3P_S1					_T("R3P_S1")					// 16扇区1扇区方位天线接收相位
#define	FLD_ME_AZIRESTB_R3P_S2					_T("R3P_S2")					// 16扇区2扇区方位天线接收相位
#define	FLD_ME_AZIRESTB_R3P_S3					_T("R3P_S3")					// 16扇区3扇区方位天线接收相位
#define	FLD_ME_AZIRESTB_R3P_S4					_T("R3P_S4")					// 16扇区4扇区方位天线接收相位
#define	FLD_ME_AZIRESTB_R3P_S5					_T("R3P_S5")					// 16扇区5扇区方位天线接收相位
#define	FLD_ME_AZIRESTB_R3P_S6					_T("R3P_S6")					// 16扇区6扇区方位天线接收相位
#define	FLD_ME_AZIRESTB_R3P_S7					_T("R3P_S7")					// 16扇区7扇区方位天线接收相位
#define	FLD_ME_AZIRESTB_R3P_S8					_T("R3P_S8")					// 16扇区8扇区方位天线接收相位
#define	FLD_ME_AZIRESTB_R3P_S9					_T("R3P_S9")					// 16扇区9扇区方位天线接收相位
#define	FLD_ME_AZIRESTB_R3P_S10					_T("R3P_S10")					// 16扇区10扇区方位天线接收相位
#define	FLD_ME_AZIRESTB_R3P_S11					_T("R3P_S11")					// 16扇区11扇区方位天线接收相位
#define	FLD_ME_AZIRESTB_R3P_S12					_T("R3P_S12")					// 16扇区12扇区方位天线接收相位
#define	FLD_ME_AZIRESTB_R3P_S13					_T("R3P_S13")					// 16扇区13扇区方位天线接收相位
#define	FLD_ME_AZIRESTB_R3P_S14					_T("R3P_S14")					// 16扇区14扇区方位天线接收相位
#define	FLD_ME_AZIRESTB_R3P_S15					_T("R3P_S15")					// 16扇区15扇区方位天线接收相位
#define	FLD_ME_AZIRESTB_CONF					_T("Conf")					// 旋转状态:No|0.否;Yes|1.是
#define	FLD_ME_AZIRESTB_BAD					_T("Bad")					// 是否坏点:No|0.否;Yes|1.是
#define	FLD_ME_AZIRESTB_DRILLACTIV					_T("DrillActiv")					// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	FLD_ME_AZIRESTB_SLIDING					_T("Sliding")					// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	FLD_ME_AZIRESTB_RELOG					_T("ReLog")					// 是否复测 :Yes|0.否;No|1.是
#define	FLD_ME_AZIRESTB_LAS					_T("Las")					// 是否Las :Yes|0.否;No|1.是
#define	FLD_ME_AZIRESTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_ME_AZIRESTB_UPDTIME					_T("UpdTime")					// 最后一次修改时间戳
#define	FLD_ME_AZIRESTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_ME_AZIRESTB_MEMO					_T("Memo")					// 备注

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_ME_AZIRESTB_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_ME_AZIRESTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_ME_AZIRESTB
{
 
  int	iDataID;							// 自增字段
  char	szRunID[TV_ME_AZIRESTB_RUNID_DIGITS + 1];							// 趟钻编号
  time_t	lTDateTime;							// 时间（精确到秒）
  int	iMillitime;							// 毫秒
  int	iToolID;							// 工具编号
  long	lAZIResMDepth;							// 方位电阻率测量井深
  long	lAZIResVDepth;							// 方位电阻率测量深度 
  int	iDataType;							// 天线类型:2M1|240.2M天线数据;400K1|241.400K天线数据;2M2|242.2M数据天线2; 400K2|243.400K天线2
  float	fR3A_S0;							// 16扇区0扇区方位天线接收幅度值
  float	fR3A_S1;							// 16扇区1扇区方位天线接收幅度值
  float	fR3A_S2;							// 16扇区2扇区方位天线接收幅度值
  float	fR3A_S3;							// 16扇区3扇区方位天线接收幅度值
  float	fR3A_S4;							// 16扇区4扇区方位天线接收幅度值
  float	fR3A_S5;							// 16扇区5扇区方位天线接收幅度值
  float	fR3A_S6;							// 16扇区6扇区方位天线接收幅度值
  float	fR3A_S7;							// 16扇区7扇区方位天线接收幅度值
  float	fR3A_S8;							// 16扇区8扇区方位天线接收幅度值
  float	fR3A_S9;							// 16扇区9扇区方位天线接收幅度值
  float	fR3A_S10;							// 16扇区10扇区方位天线接收幅度值
  float	fR3A_S11;							// 16扇区11扇区方位天线接收幅度值
  float	fR3A_S12;							// 16扇区12扇区方位天线接收幅度值
  float	fR3A_S13;							// 16扇区13扇区方位天线接收幅度值
  float	fR3A_S14;							// 16扇区14扇区方位天线接收幅度值
  float	fR3A_S15;							// 16扇区15扇区方位天线接收幅度值
  float	fR3P_S0;							// 16扇区0扇区方位天线接收相位
  float	fR3P_S1;							// 16扇区1扇区方位天线接收相位
  float	fR3P_S2;							// 16扇区2扇区方位天线接收相位
  float	fR3P_S3;							// 16扇区3扇区方位天线接收相位
  float	fR3P_S4;							// 16扇区4扇区方位天线接收相位
  float	fR3P_S5;							// 16扇区5扇区方位天线接收相位
  float	fR3P_S6;							// 16扇区6扇区方位天线接收相位
  float	fR3P_S7;							// 16扇区7扇区方位天线接收相位
  float	fR3P_S8;							// 16扇区8扇区方位天线接收相位
  float	fR3P_S9;							// 16扇区9扇区方位天线接收相位
  float	fR3P_S10;							// 16扇区10扇区方位天线接收相位
  float	fR3P_S11;							// 16扇区11扇区方位天线接收相位
  float	fR3P_S12;							// 16扇区12扇区方位天线接收相位
  float	fR3P_S13;							// 16扇区13扇区方位天线接收相位
  float	fR3P_S14;							// 16扇区14扇区方位天线接收相位
  float	fR3P_S15;							// 16扇区15扇区方位天线接收相位
  short	nConf;							// 旋转状态:No|0.否;Yes|1.是
  short	nBad;							// 是否坏点:No|0.否;Yes|1.是
  short	nDrillActiv;							// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
  short	nSliding;							// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
  short	nReLog;							// 是否复测 :Yes|0.否;No|1.是
  short	nLas;							// 是否Las :Yes|0.否;No|1.是
  time_t	lCreateTime;							// 创建时间戳
  time_t	lUpdTime;							// 最后一次修改时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_ME_AZIRESTB_MEMO_DIGITS + 1];							// 备注
} TS_ME_AZIRESTB;

typedef	TS_ME_AZIRESTB FAR*	LPTS_ME_AZIRESTB;

#pragma	pack(pop)

#endif // _TME_AZIRESTB_H

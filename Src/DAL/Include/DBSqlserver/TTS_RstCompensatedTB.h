//---------------------------------------------------------------------------//
// 文件名: TS_RstCompensatedTB.h
// 说明:	TS_RstCompensatedTB
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020-05-30 17:47:33
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TTS_RstCompensatedTB.h : DTS_RstCompensatedTB

#ifndef	_TTS_RSTCOMPENSATEDTB_H
#define	_TTS_RSTCOMPENSATEDTB_H

#define	TID_TS_RSTCOMPENSATEDTB								_T("TS_RstCompensatedTB")
#define	OID_TS_RSTCOMPENSATEDTB								_T("")

// Sort No

//#define	SORT_TS_RSTCOMPENSATEDTB_A1							%#%							// A1:

// Colum No
#define	COL_TS_RSTCOMPENSATEDTB_RSTCOM_ID					(short)0						// 自增ID
#define	COL_TS_RSTCOMPENSATEDTB_RSTDATETIME					(short)1						// 时间
#define	COL_TS_RSTCOMPENSATEDTB_LS_400K_RX_RATIO					(short)2						// LS-400K-RX-Ratio
#define	COL_TS_RSTCOMPENSATEDTB_SS_400K_RX_RATIO					(short)3						// SS-400K-RX-Ratio
#define	COL_TS_RSTCOMPENSATEDTB_LS_2M_RX_RATIO					(short)4						// LS-2M-RX-Ratio
#define	COL_TS_RSTCOMPENSATEDTB_SS_2M_RX_RATIO					(short)5						// SS-2M-RX-Ratio
#define	COL_TS_RSTCOMPENSATEDTB_LS_400K_RX_PHA					(short)6						// LS-400K-RX-PHA
#define	COL_TS_RSTCOMPENSATEDTB_SS_400K_RX_PHA					(short)7						// SS-400K-RX-PHA
#define	COL_TS_RSTCOMPENSATEDTB_LS_2M_RX_PHA					(short)8						// LS-2M-RX-PHA
#define	COL_TS_RSTCOMPENSATEDTB_SS_2M_RX_PHA					(short)9						// SS-2M-RX-PHA

// Colum(Field) Name
#define	FLD_TS_RSTCOMPENSATEDTB_RSTCOM_ID					_T("RstCom_Id")					// 自增ID
#define	FLD_TS_RSTCOMPENSATEDTB_RSTDATETIME					_T("RstDatetime")					// 时间
#define	FLD_TS_RSTCOMPENSATEDTB_LS_400K_RX_RATIO					_T("LS_400K_RX_Ratio")					// LS-400K-RX-Ratio
#define	FLD_TS_RSTCOMPENSATEDTB_SS_400K_RX_RATIO					_T("SS_400K_RX_Ratio")					// SS-400K-RX-Ratio
#define	FLD_TS_RSTCOMPENSATEDTB_LS_2M_RX_RATIO					_T("LS_2M_RX_Ratio")					// LS-2M-RX-Ratio
#define	FLD_TS_RSTCOMPENSATEDTB_SS_2M_RX_RATIO					_T("SS_2M_RX_Ratio")					// SS-2M-RX-Ratio
#define	FLD_TS_RSTCOMPENSATEDTB_LS_400K_RX_PHA					_T("LS_400K_RX_PHA")					// LS-400K-RX-PHA
#define	FLD_TS_RSTCOMPENSATEDTB_SS_400K_RX_PHA					_T("SS_400K_RX_PHA")					// SS-400K-RX-PHA
#define	FLD_TS_RSTCOMPENSATEDTB_LS_2M_RX_PHA					_T("LS_2M_RX_PHA")					// LS-2M-RX-PHA
#define	FLD_TS_RSTCOMPENSATEDTB_SS_2M_RX_PHA					_T("SS_2M_RX_PHA")					// SS-2M-RX-PHA

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_TS_RSTCOMPENSATEDTB_LS_400K_RX_RATIO_DIGITS				64					// LS-400K-RX-Ratio位数
#define	TV_TS_RSTCOMPENSATEDTB_SS_400K_RX_RATIO_DIGITS				64					// SS-400K-RX-Ratio位数
#define	TV_TS_RSTCOMPENSATEDTB_LS_2M_RX_RATIO_DIGITS				64					// LS-2M-RX-Ratio位数
#define	TV_TS_RSTCOMPENSATEDTB_SS_2M_RX_RATIO_DIGITS				64					// SS-2M-RX-Ratio位数
#define	TV_TS_RSTCOMPENSATEDTB_LS_400K_RX_PHA_DIGITS				64					// LS-400K-RX-PHA位数
#define	TV_TS_RSTCOMPENSATEDTB_SS_400K_RX_PHA_DIGITS				64					// SS-400K-RX-PHA位数
#define	TV_TS_RSTCOMPENSATEDTB_LS_2M_RX_PHA_DIGITS				64					// LS-2M-RX-PHA位数
#define	TV_TS_RSTCOMPENSATEDTB_SS_2M_RX_PHA_DIGITS				64					// SS-2M-RX-PHA位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_TS_RSTCOMPENSATEDTB
{
 
  int	iRstCom_Id;							// 自增ID
  time_t	lRstDatetime;							// 时间
  char	szLS_400K_RX_Ratio[TV_TS_RSTCOMPENSATEDTB_LS_400K_RX_RATIO_DIGITS + 1];							// LS-400K-RX-Ratio
  char	szSS_400K_RX_Ratio[TV_TS_RSTCOMPENSATEDTB_SS_400K_RX_RATIO_DIGITS + 1];							// SS-400K-RX-Ratio
  char	szLS_2M_RX_Ratio[TV_TS_RSTCOMPENSATEDTB_LS_2M_RX_RATIO_DIGITS + 1];							// LS-2M-RX-Ratio
  char	szSS_2M_RX_Ratio[TV_TS_RSTCOMPENSATEDTB_SS_2M_RX_RATIO_DIGITS + 1];							// SS-2M-RX-Ratio
  char	szLS_400K_RX_PHA[TV_TS_RSTCOMPENSATEDTB_LS_400K_RX_PHA_DIGITS + 1];							// LS-400K-RX-PHA
  char	szSS_400K_RX_PHA[TV_TS_RSTCOMPENSATEDTB_SS_400K_RX_PHA_DIGITS + 1];							// SS-400K-RX-PHA
  char	szLS_2M_RX_PHA[TV_TS_RSTCOMPENSATEDTB_LS_2M_RX_PHA_DIGITS + 1];							// LS-2M-RX-PHA
  char	szSS_2M_RX_PHA[TV_TS_RSTCOMPENSATEDTB_SS_2M_RX_PHA_DIGITS + 1];							// SS-2M-RX-PHA
} TS_TS_RSTCOMPENSATEDTB;

typedef	TS_TS_RSTCOMPENSATEDTB FAR*	LPTS_TS_RSTCOMPENSATEDTB;

#pragma	pack(pop)

#endif // _TTS_RSTCOMPENSATEDTB_H

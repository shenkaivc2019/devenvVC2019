//---------------------------------------------------------------------------//
// 文件名: CA_SurveyData.h
// 说明:	实时MWD数据表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/10/9 15:57:06
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TCA_SurveyData.h : DCA_SurveyData

#ifndef	_TCA_SURVEYDATA_H
#define	_TCA_SURVEYDATA_H

#define	TID_CA_SURVEYDATA								_T("CA_SurveyData")
#define	OID_CA_SURVEYDATA								_T("")

// Sort No
#define	SORT_CA_SURVEYDATA_PK0				0							// PK:数据编号
//#define	SORT_CA_SURVEYDATA_A1							%#%							// A1:

// Colum No
#define	COL_CA_SURVEYDATA_DATAID					(short)0						// 数据编号
#define	COL_CA_SURVEYDATA_RUNID					(short)1						// 趟钻编号
#define	COL_CA_SURVEYDATA_TDATETIME					(short)2						// 时间（精确到秒）
#define	COL_CA_SURVEYDATA_MILLITIME					(short)3						// 毫秒
#define	COL_CA_SURVEYDATA_TOOLID					(short)4						// 工具编号
#define	COL_CA_SURVEYDATA_MDEPTH					(short)5						// 测量点井深
#define	COL_CA_SURVEYDATA_VDEPTH					(short)6						// 测量点垂深
#define	COL_CA_SURVEYDATA_PREVDEPTH					(short)7						// 测量点预测垂深
#define	COL_CA_SURVEYDATA_INC					(short)8						// 井斜
#define	COL_CA_SURVEYDATA_AZM					(short)9						// 方位
#define	COL_CA_SURVEYDATA_MAGF					(short)10						// 磁场和
#define	COL_CA_SURVEYDATA_GRAV					(short)11						// 重力和
#define	COL_CA_SURVEYDATA_DIPA					(short)12						// 磁倾角
#define	COL_CA_SURVEYDATA_MAGFDIFF					(short)13						// 磁场和差值
#define	COL_CA_SURVEYDATA_GRAVDIFF					(short)14						// 重力和差值
#define	COL_CA_SURVEYDATA_DIPADIFF					(short)15						// 磁倾角差值
#define	COL_CA_SURVEYDATA_AX					(short)16						// 加速度X
#define	COL_CA_SURVEYDATA_AY					(short)17						// 加速度Y
#define	COL_CA_SURVEYDATA_AZ					(short)18						// 加速度Z
#define	COL_CA_SURVEYDATA_MX					(short)19						// 磁分量X
#define	COL_CA_SURVEYDATA_MY					(short)20						// 磁分量Y
#define	COL_CA_SURVEYDATA_MZ					(short)21						// 磁分量Z
#define	COL_CA_SURVEYDATA_CONF					(short)22						// 可信度
#define	COL_CA_SURVEYDATA_BAD					(short)23						// 是否坏点:No|0.否;Yes|1.是
#define	COL_CA_SURVEYDATA_DRILLACTIV					(short)24						// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	COL_CA_SURVEYDATA_SLIDING					(short)25						// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	COL_CA_SURVEYDATA_RELOG					(short)26						// 是否复测 :Yes|0.否;No|1.是
#define	COL_CA_SURVEYDATA_LAS					(short)27						// 是否Las :Yes|0.否;No|1.是
#define	COL_CA_SURVEYDATA_CREATETIME					(short)28						// 创建时间戳
#define	COL_CA_SURVEYDATA_UPDTIME					(short)29						// 最后一次修改时间戳
#define	COL_CA_SURVEYDATA_STATUS					(short)30						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_CA_SURVEYDATA_MEMO					(short)31						// 备注

// Colum(Field) Name
#define	FLD_CA_SURVEYDATA_DATAID					_T("DataID")					// 数据编号
#define	FLD_CA_SURVEYDATA_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_CA_SURVEYDATA_TDATETIME					_T("TDateTime")					// 时间（精确到秒）
#define	FLD_CA_SURVEYDATA_MILLITIME					_T("Millitime")					// 毫秒
#define	FLD_CA_SURVEYDATA_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_CA_SURVEYDATA_MDEPTH					_T("MDepth")					// 测量点井深
#define	FLD_CA_SURVEYDATA_VDEPTH					_T("VDepth")					// 测量点垂深
#define	FLD_CA_SURVEYDATA_PREVDEPTH					_T("PreVDepth")					// 测量点预测垂深
#define	FLD_CA_SURVEYDATA_INC					_T("Inc")					// 井斜
#define	FLD_CA_SURVEYDATA_AZM					_T("Azm")					// 方位
#define	FLD_CA_SURVEYDATA_MAGF					_T("MagF")					// 磁场和
#define	FLD_CA_SURVEYDATA_GRAV					_T("Grav")					// 重力和
#define	FLD_CA_SURVEYDATA_DIPA					_T("DipA")					// 磁倾角
#define	FLD_CA_SURVEYDATA_MAGFDIFF					_T("MagFDiff")					// 磁场和差值
#define	FLD_CA_SURVEYDATA_GRAVDIFF					_T("GravDiff")					// 重力和差值
#define	FLD_CA_SURVEYDATA_DIPADIFF					_T("DipADiff")					// 磁倾角差值
#define	FLD_CA_SURVEYDATA_AX					_T("Ax")					// 加速度X
#define	FLD_CA_SURVEYDATA_AY					_T("Ay")					// 加速度Y
#define	FLD_CA_SURVEYDATA_AZ					_T("Az")					// 加速度Z
#define	FLD_CA_SURVEYDATA_MX					_T("Mx")					// 磁分量X
#define	FLD_CA_SURVEYDATA_MY					_T("My")					// 磁分量Y
#define	FLD_CA_SURVEYDATA_MZ					_T("Mz")					// 磁分量Z
#define	FLD_CA_SURVEYDATA_CONF					_T("Conf")					// 可信度
#define	FLD_CA_SURVEYDATA_BAD					_T("Bad")					// 是否坏点:No|0.否;Yes|1.是
#define	FLD_CA_SURVEYDATA_DRILLACTIV					_T("DrillActiv")					// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	FLD_CA_SURVEYDATA_SLIDING					_T("Sliding")					// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	FLD_CA_SURVEYDATA_RELOG					_T("ReLog")					// 是否复测 :Yes|0.否;No|1.是
#define	FLD_CA_SURVEYDATA_LAS					_T("Las")					// 是否Las :Yes|0.否;No|1.是
#define	FLD_CA_SURVEYDATA_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_CA_SURVEYDATA_UPDTIME					_T("UpdTime")					// 最后一次修改时间戳
#define	FLD_CA_SURVEYDATA_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_CA_SURVEYDATA_MEMO					_T("Memo")					// 备注

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_CA_SURVEYDATA_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_CA_SURVEYDATA_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_CA_SURVEYDATA
{
 
  long	lDataID;							// 数据编号
  char	szRunID[TV_CA_SURVEYDATA_RUNID_DIGITS + 1];							// 趟钻编号
  time_t	lTDateTime;							// 时间（精确到秒）
  int	iMillitime;							// 毫秒
  int	iToolID;							// 工具编号
  long	lMDepth;							// 测量点井深
  long	lVDepth;							// 测量点垂深
  long	lPreVDepth;							// 测量点预测垂深
  float	fInc;							// 井斜
  float	fAzm;							// 方位
  float	fMagF;							// 磁场和
  float	fGrav;							// 重力和
  float	fDipA;							// 磁倾角
  float	fMagFDiff;							// 磁场和差值
  float	fGravDiff;							// 重力和差值
  float	fDipADiff;							// 磁倾角差值
  float	fAx;							// 加速度X
  float	fAy;							// 加速度Y
  float	fAz;							// 加速度Z
  float	fMx;							// 磁分量X
  float	fMy;							// 磁分量Y
  float	fMz;							// 磁分量Z
  short	nConf;							// 可信度
  short	nBad;							// 是否坏点:No|0.否;Yes|1.是
  short	nDrillActiv;							// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
  short	nSliding;							// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
  short	nReLog;							// 是否复测 :Yes|0.否;No|1.是
  short	nLas;							// 是否Las :Yes|0.否;No|1.是
  time_t	lCreateTime;							// 创建时间戳
  time_t	lUpdTime;							// 最后一次修改时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_CA_SURVEYDATA_MEMO_DIGITS + 1];							// 备注
} TS_CA_SURVEYDATA;

typedef	TS_CA_SURVEYDATA FAR*	LPTS_CA_SURVEYDATA;

#pragma	pack(pop)

#endif // _TCA_SURVEYDATA_H

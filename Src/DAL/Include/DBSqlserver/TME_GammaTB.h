//---------------------------------------------------------------------------//
// 文件名: ME_GammaTB.h
// 说明:	仪器内存数据表(自然伽马）
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2022/12/27 15:25:52
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TME_GammaTB.h : DME_GammaTB

#ifndef	_TME_GAMMATB_H
#define	_TME_GAMMATB_H

#define	TID_ME_GAMMATB								_T("ME_GammaTB")
#define	OID_ME_GAMMATB								_T("")

// Sort No
#define	SORT_ME_GAMMATB_PK0				0							// PK:自增字段
//#define	SORT_ME_GAMMATB_A1							%#%							// A1:

// Colum No
#define	COL_ME_GAMMATB_DATAID					(short)0						// 自增字段
#define	COL_ME_GAMMATB_RUNID					(short)1						// 趟钻编号
#define	COL_ME_GAMMATB_TDATETIME					(short)2						// 时间（精确到秒）
#define	COL_ME_GAMMATB_MILLITIME					(short)3						// 毫秒
#define	COL_ME_GAMMATB_TOOLID					(short)4						// 工具编号
#define	COL_ME_GAMMATB_GRMDEPTH					(short)5						// 自然伽马测量深度 
#define	COL_ME_GAMMATB_GRVDEPTH					(short)6						// 自然伽马测量深度 
#define	COL_ME_GAMMATB_MWDGAM					(short)7						// 伽马RAW
#define	COL_ME_GAMMATB_MWDGAM_API					(short)8						// 伽马经过API计算系数校正后数据
#define	COL_ME_GAMMATB_MWDGAM_APIC					(short)9						// 伽马经过API计算环境校正后数据
#define	COL_ME_GAMMATB_GRS0					(short)10						// 16扇0区扇区伽马值
#define	COL_ME_GAMMATB_GRS1					(short)11						// 16扇1区扇区伽马值
#define	COL_ME_GAMMATB_GRS2					(short)12						// 16扇2区扇区伽马值
#define	COL_ME_GAMMATB_GRS3					(short)13						// 16扇3区扇区伽马值
#define	COL_ME_GAMMATB_GRS4					(short)14						// 16扇4区扇区伽马值
#define	COL_ME_GAMMATB_GRS5					(short)15						// 16扇5区扇区伽马值
#define	COL_ME_GAMMATB_GRS6					(short)16						// 16扇6区扇区伽马值
#define	COL_ME_GAMMATB_GRS7					(short)17						// 16扇7区扇区伽马值
#define	COL_ME_GAMMATB_GRS8					(short)18						// 16扇8区扇区伽马值
#define	COL_ME_GAMMATB_GRS9					(short)19						// 16扇9区扇区伽马值
#define	COL_ME_GAMMATB_GRS10					(short)20						// 16扇10区扇区伽马值
#define	COL_ME_GAMMATB_GRS11					(short)21						// 16扇11区扇区伽马值
#define	COL_ME_GAMMATB_GRS12					(short)22						// 16扇12区扇区伽马值
#define	COL_ME_GAMMATB_GRS13					(short)23						// 16扇13区扇区伽马值
#define	COL_ME_GAMMATB_GRS14					(short)24						// 16扇14区扇区伽马值
#define	COL_ME_GAMMATB_GRS15					(short)25						// 16扇15区扇区伽马值
#define	COL_ME_GAMMATB_GRS0A					(short)26						// 16扇区0扇区伽马值（API计算系数校正后数据）
#define	COL_ME_GAMMATB_GRS0C					(short)27						// 16扇区0扇区伽马值（API计算环境校正后数据）
#define	COL_ME_GAMMATB_GRS1A					(short)28						// 16扇区1扇区伽马值（API计算系数校正后数据）
#define	COL_ME_GAMMATB_GRS1C					(short)29						// 16扇区1扇区伽马值（API计算环境校正后数据）
#define	COL_ME_GAMMATB_GRS2A					(short)30						// 16扇区2扇区伽马值（API计算系数校正后数据）
#define	COL_ME_GAMMATB_GRS2C					(short)31						// 16扇区2扇区伽马值（API计算环境校正后数据）
#define	COL_ME_GAMMATB_GRS3A					(short)32						// 16扇区3扇区伽马值（API计算系数校正后数据）
#define	COL_ME_GAMMATB_GRS3C					(short)33						// 16扇区3扇区伽马值（API计算环境校正后数据）
#define	COL_ME_GAMMATB_GRS4A					(short)34						// 16扇区4扇区伽马值（API计算系数校正后数据）
#define	COL_ME_GAMMATB_GRS4C					(short)35						// 16扇区4扇区伽马值（API计算环境校正后数据）
#define	COL_ME_GAMMATB_GRS5A					(short)36						// 16扇区5扇区伽马值（API计算系数校正后数据）
#define	COL_ME_GAMMATB_GRS5C					(short)37						// 16扇区5扇区伽马值（API计算环境校正后数据）
#define	COL_ME_GAMMATB_GRS6A					(short)38						// 16扇区6扇区伽马值（API计算系数校正后数据）
#define	COL_ME_GAMMATB_GRS6C					(short)39						// 16扇区6扇区伽马值（API计算环境校正后数据）
#define	COL_ME_GAMMATB_GRS7A					(short)40						// 16扇区7扇区伽马值（API计算系数校正后数据）
#define	COL_ME_GAMMATB_GRS7C					(short)41						// 16扇区7扇区伽马值（API计算环境校正后数据）
#define	COL_ME_GAMMATB_GRS8A					(short)42						// 16扇区8扇区伽马值（API计算系数校正后数据）
#define	COL_ME_GAMMATB_GRS8C					(short)43						// 16扇区8扇区伽马值（API计算环境校正后数据）
#define	COL_ME_GAMMATB_GRS9A					(short)44						// 16扇区9扇区伽马值（API计算系数校正后数据）
#define	COL_ME_GAMMATB_GRS9C					(short)45						// 16扇区9扇区伽马值（API计算环境校正后数据）
#define	COL_ME_GAMMATB_GRS10A					(short)46						// 16扇区10扇区伽马值（API计算系数校正后数据）
#define	COL_ME_GAMMATB_GRS10C					(short)47						// 16扇区10扇区伽马值（API计算环境校正后数据）
#define	COL_ME_GAMMATB_GRS11A					(short)48						// 16扇区11扇区伽马值（API计算系数校正后数据）
#define	COL_ME_GAMMATB_GRS11C					(short)49						// 16扇区11扇区伽马值（API计算环境校正后数据）
#define	COL_ME_GAMMATB_GRS12A					(short)50						// 16扇区12扇区伽马值（API计算系数校正后数据）
#define	COL_ME_GAMMATB_GRS12C					(short)51						// 16扇区12扇区伽马值（API计算环境校正后数据）
#define	COL_ME_GAMMATB_GRS13A					(short)52						// 16扇区13扇区伽马值（API计算系数校正后数据）
#define	COL_ME_GAMMATB_GRS13C					(short)53						// 16扇区13扇区伽马值（API计算环境校正后数据）
#define	COL_ME_GAMMATB_GRS14A					(short)54						// 16扇区14扇区伽马值（API计算系数校正后数据）
#define	COL_ME_GAMMATB_GRS14C					(short)55						// 16扇区14扇区伽马值（API计算环境校正后数据）
#define	COL_ME_GAMMATB_GRS15A					(short)56						// 16扇区15扇区伽马值（API计算系数校正后数据）
#define	COL_ME_GAMMATB_GRS15C					(short)57						// 16扇区15扇区伽马值（API计算环境校正后数据）
#define	COL_ME_GAMMATB_GRS0D					(short)58						// 16扇区0扇区伽马值（API暗电流校正后数据）
#define	COL_ME_GAMMATB_GRS0CG					(short)59						// 16扇区0扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_GAMMATB_GRS1D					(short)60						// 16扇区1扇区伽马值（API暗电流校正后数据）
#define	COL_ME_GAMMATB_GRS1CG					(short)61						// 16扇区1扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_GAMMATB_GRS2D					(short)62						// 16扇区2扇区伽马值（API暗电流校正后数据）
#define	COL_ME_GAMMATB_GRS2CG					(short)63						// 16扇区2扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_GAMMATB_GRS3D					(short)64						// 16扇区3扇区伽马值（API暗电流校正后数据）
#define	COL_ME_GAMMATB_GRS3CG					(short)65						// 16扇区3扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_GAMMATB_GRS4D					(short)66						// 16扇区4扇区伽马值（API暗电流校正后数据）
#define	COL_ME_GAMMATB_GRS4CG					(short)67						// 16扇区4扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_GAMMATB_GRS5D					(short)68						// 16扇区5扇区伽马值（API暗电流校正后数据）
#define	COL_ME_GAMMATB_GRS5CG					(short)69						// 16扇区5扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_GAMMATB_GRS6D					(short)70						// 16扇区6扇区伽马值（API暗电流校正后数据）
#define	COL_ME_GAMMATB_GRS6CG					(short)71						// 16扇区6扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_GAMMATB_GRS7D					(short)72						// 16扇区7扇区伽马值（API暗电流校正后数据）
#define	COL_ME_GAMMATB_GRS7CG					(short)73						// 16扇区7扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_GAMMATB_GRS8D					(short)74						// 16扇区8扇区伽马值（API暗电流校正后数据）
#define	COL_ME_GAMMATB_GRS8CG					(short)75						// 16扇区8扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_GAMMATB_GRS9D					(short)76						// 16扇区9扇区伽马值（API暗电流校正后数据）
#define	COL_ME_GAMMATB_GRS9CG					(short)77						// 16扇区9扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_GAMMATB_GRS10D					(short)78						// 16扇区10扇区伽马值（API暗电流校正后数据）
#define	COL_ME_GAMMATB_GRS10CG					(short)79						// 16扇区10扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_GAMMATB_GRS11D					(short)80						// 16扇区11扇区伽马值（API暗电流校正后数据）
#define	COL_ME_GAMMATB_GRS11CG					(short)81						// 16扇区11扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_GAMMATB_GRS12D					(short)82						// 16扇区12扇区伽马值（API暗电流校正后数据）
#define	COL_ME_GAMMATB_GRS12CG					(short)83						// 16扇区12扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_GAMMATB_GRS13D					(short)84						// 16扇区13扇区伽马值（API暗电流校正后数据）
#define	COL_ME_GAMMATB_GRS13CG					(short)85						// 16扇区13扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_GAMMATB_GRS14D					(short)86						// 16扇区14扇区伽马值（API暗电流校正后数据）
#define	COL_ME_GAMMATB_GRS14CG					(short)87						// 16扇区14扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_GAMMATB_GRS15D					(short)88						// 16扇区15扇区伽马值（API暗电流校正后数据）
#define	COL_ME_GAMMATB_GRS15CG					(short)89						// 16扇区15扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	COL_ME_GAMMATB_GR					(short)90						// 总伽马RAW
#define	COL_ME_GAMMATB_GRA					(short)91						// 总伽马经过API计算系数校正后数据
#define	COL_ME_GAMMATB_GRC					(short)92						// 总伽马经过API计算环境校正后数据
#define	COL_ME_GAMMATB_GRU					(short)93						// 16扇上伽马值
#define	COL_ME_GAMMATB_GRUA					(short)94						// 16扇上伽马值（API计算系数校正后数据）
#define	COL_ME_GAMMATB_GRUC					(short)95						// 上伽马经过API计算环境校正后数据
#define	COL_ME_GAMMATB_GRD					(short)96						// 16扇下伽马值
#define	COL_ME_GAMMATB_GRDA					(short)97						// 16扇下伽马值（API计算系数校正后数据）
#define	COL_ME_GAMMATB_GRDC					(short)98						// 下伽马经过API计算环境校正后数据
#define	COL_ME_GAMMATB_GRL					(short)99						// 16扇左伽马值
#define	COL_ME_GAMMATB_GRLA					(short)100						// 16扇左伽马值（API计算系数校正后数据）
#define	COL_ME_GAMMATB_GRLC					(short)101						// 左伽马经过API计算环境校正后数据
#define	COL_ME_GAMMATB_GRR					(short)102						// 16扇右伽马值
#define	COL_ME_GAMMATB_GRRA					(short)103						// 16扇右伽马值（API计算系数校正后数据）
#define	COL_ME_GAMMATB_GRRC					(short)104						// 右伽马经过API计算环境校正后数据
#define	COL_ME_GAMMATB_INC					(short)105						// 井斜
#define	COL_ME_GAMMATB_CONF					(short)106						// 旋转状态:No|0.否;Yes|1.是
#define	COL_ME_GAMMATB_BAD					(short)107						// 是否坏点:No|0.否;Yes|1.是
#define	COL_ME_GAMMATB_DRILLACTIV					(short)108						// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	COL_ME_GAMMATB_SLIDING					(short)109						// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	COL_ME_GAMMATB_RELOG					(short)110						// 是否复测 :Yes|0.否;No|1.是
#define	COL_ME_GAMMATB_LAS					(short)111						// 是否Las :Yes|0.否;No|1.是
#define	COL_ME_GAMMATB_CREATETIME					(short)112						// 创建时间戳
#define	COL_ME_GAMMATB_UPDTIME					(short)113						// 最后一次修改时间戳
#define	COL_ME_GAMMATB_STATUS					(short)114						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_ME_GAMMATB_MEMO					(short)115						// 备注

// Colum(Field) Name
#define	FLD_ME_GAMMATB_DATAID					_T("DataID")					// 自增字段
#define	FLD_ME_GAMMATB_RUNID					_T("RunID")					// 趟钻编号
#define	FLD_ME_GAMMATB_TDATETIME					_T("TDateTime")					// 时间（精确到秒）
#define	FLD_ME_GAMMATB_MILLITIME					_T("Millitime")					// 毫秒
#define	FLD_ME_GAMMATB_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_ME_GAMMATB_GRMDEPTH					_T("GRMDepth")					// 自然伽马测量深度 
#define	FLD_ME_GAMMATB_GRVDEPTH					_T("GRVDepth")					// 自然伽马测量深度 
#define	FLD_ME_GAMMATB_MWDGAM					_T("MWDGAM")					// 伽马RAW
#define	FLD_ME_GAMMATB_MWDGAM_API					_T("MWDGAM_API")					// 伽马经过API计算系数校正后数据
#define	FLD_ME_GAMMATB_MWDGAM_APIC					_T("MWDGAM_APIC")					// 伽马经过API计算环境校正后数据
#define	FLD_ME_GAMMATB_GRS0					_T("GRS0")					// 16扇0区扇区伽马值
#define	FLD_ME_GAMMATB_GRS1					_T("GRS1")					// 16扇1区扇区伽马值
#define	FLD_ME_GAMMATB_GRS2					_T("GRS2")					// 16扇2区扇区伽马值
#define	FLD_ME_GAMMATB_GRS3					_T("GRS3")					// 16扇3区扇区伽马值
#define	FLD_ME_GAMMATB_GRS4					_T("GRS4")					// 16扇4区扇区伽马值
#define	FLD_ME_GAMMATB_GRS5					_T("GRS5")					// 16扇5区扇区伽马值
#define	FLD_ME_GAMMATB_GRS6					_T("GRS6")					// 16扇6区扇区伽马值
#define	FLD_ME_GAMMATB_GRS7					_T("GRS7")					// 16扇7区扇区伽马值
#define	FLD_ME_GAMMATB_GRS8					_T("GRS8")					// 16扇8区扇区伽马值
#define	FLD_ME_GAMMATB_GRS9					_T("GRS9")					// 16扇9区扇区伽马值
#define	FLD_ME_GAMMATB_GRS10					_T("GRS10")					// 16扇10区扇区伽马值
#define	FLD_ME_GAMMATB_GRS11					_T("GRS11")					// 16扇11区扇区伽马值
#define	FLD_ME_GAMMATB_GRS12					_T("GRS12")					// 16扇12区扇区伽马值
#define	FLD_ME_GAMMATB_GRS13					_T("GRS13")					// 16扇13区扇区伽马值
#define	FLD_ME_GAMMATB_GRS14					_T("GRS14")					// 16扇14区扇区伽马值
#define	FLD_ME_GAMMATB_GRS15					_T("GRS15")					// 16扇15区扇区伽马值
#define	FLD_ME_GAMMATB_GRS0A					_T("GRS0A")					// 16扇区0扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_GAMMATB_GRS0C					_T("GRS0C")					// 16扇区0扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_GAMMATB_GRS1A					_T("GRS1A")					// 16扇区1扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_GAMMATB_GRS1C					_T("GRS1C")					// 16扇区1扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_GAMMATB_GRS2A					_T("GRS2A")					// 16扇区2扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_GAMMATB_GRS2C					_T("GRS2C")					// 16扇区2扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_GAMMATB_GRS3A					_T("GRS3A")					// 16扇区3扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_GAMMATB_GRS3C					_T("GRS3C")					// 16扇区3扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_GAMMATB_GRS4A					_T("GRS4A")					// 16扇区4扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_GAMMATB_GRS4C					_T("GRS4C")					// 16扇区4扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_GAMMATB_GRS5A					_T("GRS5A")					// 16扇区5扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_GAMMATB_GRS5C					_T("GRS5C")					// 16扇区5扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_GAMMATB_GRS6A					_T("GRS6A")					// 16扇区6扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_GAMMATB_GRS6C					_T("GRS6C")					// 16扇区6扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_GAMMATB_GRS7A					_T("GRS7A")					// 16扇区7扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_GAMMATB_GRS7C					_T("GRS7C")					// 16扇区7扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_GAMMATB_GRS8A					_T("GRS8A")					// 16扇区8扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_GAMMATB_GRS8C					_T("GRS8C")					// 16扇区8扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_GAMMATB_GRS9A					_T("GRS9A")					// 16扇区9扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_GAMMATB_GRS9C					_T("GRS9C")					// 16扇区9扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_GAMMATB_GRS10A					_T("GRS10A")					// 16扇区10扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_GAMMATB_GRS10C					_T("GRS10C")					// 16扇区10扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_GAMMATB_GRS11A					_T("GRS11A")					// 16扇区11扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_GAMMATB_GRS11C					_T("GRS11C")					// 16扇区11扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_GAMMATB_GRS12A					_T("GRS12A")					// 16扇区12扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_GAMMATB_GRS12C					_T("GRS12C")					// 16扇区12扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_GAMMATB_GRS13A					_T("GRS13A")					// 16扇区13扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_GAMMATB_GRS13C					_T("GRS13C")					// 16扇区13扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_GAMMATB_GRS14A					_T("GRS14A")					// 16扇区14扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_GAMMATB_GRS14C					_T("GRS14C")					// 16扇区14扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_GAMMATB_GRS15A					_T("GRS15A")					// 16扇区15扇区伽马值（API计算系数校正后数据）
#define	FLD_ME_GAMMATB_GRS15C					_T("GRS15C")					// 16扇区15扇区伽马值（API计算环境校正后数据）
#define	FLD_ME_GAMMATB_GRS0D					_T("GRS0D")					// 16扇区0扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_GAMMATB_GRS0CG					_T("GRS0CG")					// 16扇区0扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_GAMMATB_GRS1D					_T("GRS1D")					// 16扇区1扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_GAMMATB_GRS1CG					_T("GRS1CG")					// 16扇区1扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_GAMMATB_GRS2D					_T("GRS2D")					// 16扇区2扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_GAMMATB_GRS2CG					_T("GRS2CG")					// 16扇区2扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_GAMMATB_GRS3D					_T("GRS3D")					// 16扇区3扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_GAMMATB_GRS3CG					_T("GRS3CG")					// 16扇区3扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_GAMMATB_GRS4D					_T("GRS4D")					// 16扇区4扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_GAMMATB_GRS4CG					_T("GRS4CG")					// 16扇区4扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_GAMMATB_GRS5D					_T("GRS5D")					// 16扇区5扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_GAMMATB_GRS5CG					_T("GRS5CG")					// 16扇区5扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_GAMMATB_GRS6D					_T("GRS6D")					// 16扇区6扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_GAMMATB_GRS6CG					_T("GRS6CG")					// 16扇区6扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_GAMMATB_GRS7D					_T("GRS7D")					// 16扇区7扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_GAMMATB_GRS7CG					_T("GRS7CG")					// 16扇区7扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_GAMMATB_GRS8D					_T("GRS8D")					// 16扇区8扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_GAMMATB_GRS8CG					_T("GRS8CG")					// 16扇区8扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_GAMMATB_GRS9D					_T("GRS9D")					// 16扇区9扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_GAMMATB_GRS9CG					_T("GRS9CG")					// 16扇区9扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_GAMMATB_GRS10D					_T("GRS10D")					// 16扇区10扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_GAMMATB_GRS10CG					_T("GRS10CG")					// 16扇区10扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_GAMMATB_GRS11D					_T("GRS11D")					// 16扇区11扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_GAMMATB_GRS11CG					_T("GRS11CG")					// 16扇区11扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_GAMMATB_GRS12D					_T("GRS12D")					// 16扇区12扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_GAMMATB_GRS12CG					_T("GRS12CG")					// 16扇区12扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_GAMMATB_GRS13D					_T("GRS13D")					// 16扇区13扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_GAMMATB_GRS13CG					_T("GRS13CG")					// 16扇区13扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_GAMMATB_GRS14D					_T("GRS14D")					// 16扇区14扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_GAMMATB_GRS14CG					_T("GRS14CG")					// 16扇区14扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_GAMMATB_GRS15D					_T("GRS15D")					// 16扇区15扇区伽马值（API暗电流校正后数据）
#define	FLD_ME_GAMMATB_GRS15CG					_T("GRS15CG")					// 16扇区15扇区伽马值（API泥浆贡献伽玛值校正后数据）
#define	FLD_ME_GAMMATB_GR					_T("GR")					// 总伽马RAW
#define	FLD_ME_GAMMATB_GRA					_T("GRA")					// 总伽马经过API计算系数校正后数据
#define	FLD_ME_GAMMATB_GRC					_T("GRC")					// 总伽马经过API计算环境校正后数据
#define	FLD_ME_GAMMATB_GRU					_T("GRU")					// 16扇上伽马值
#define	FLD_ME_GAMMATB_GRUA					_T("GRUA")					// 16扇上伽马值（API计算系数校正后数据）
#define	FLD_ME_GAMMATB_GRUC					_T("GRUC")					// 上伽马经过API计算环境校正后数据
#define	FLD_ME_GAMMATB_GRD					_T("GRD")					// 16扇下伽马值
#define	FLD_ME_GAMMATB_GRDA					_T("GRDA")					// 16扇下伽马值（API计算系数校正后数据）
#define	FLD_ME_GAMMATB_GRDC					_T("GRDC")					// 下伽马经过API计算环境校正后数据
#define	FLD_ME_GAMMATB_GRL					_T("GRL")					// 16扇左伽马值
#define	FLD_ME_GAMMATB_GRLA					_T("GRLA")					// 16扇左伽马值（API计算系数校正后数据）
#define	FLD_ME_GAMMATB_GRLC					_T("GRLC")					// 左伽马经过API计算环境校正后数据
#define	FLD_ME_GAMMATB_GRR					_T("GRR")					// 16扇右伽马值
#define	FLD_ME_GAMMATB_GRRA					_T("GRRA")					// 16扇右伽马值（API计算系数校正后数据）
#define	FLD_ME_GAMMATB_GRRC					_T("GRRC")					// 右伽马经过API计算环境校正后数据
#define	FLD_ME_GAMMATB_INC					_T("Inc")					// 井斜
#define	FLD_ME_GAMMATB_CONF					_T("Conf")					// 旋转状态:No|0.否;Yes|1.是
#define	FLD_ME_GAMMATB_BAD					_T("Bad")					// 是否坏点:No|0.否;Yes|1.是
#define	FLD_ME_GAMMATB_DRILLACTIV					_T("DrillActiv")					// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
#define	FLD_ME_GAMMATB_SLIDING					_T("Sliding")					// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
#define	FLD_ME_GAMMATB_RELOG					_T("ReLog")					// 是否复测 :Yes|0.否;No|1.是
#define	FLD_ME_GAMMATB_LAS					_T("Las")					// 是否Las :Yes|0.否;No|1.是
#define	FLD_ME_GAMMATB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_ME_GAMMATB_UPDTIME					_T("UpdTime")					// 最后一次修改时间戳
#define	FLD_ME_GAMMATB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_ME_GAMMATB_MEMO					_T("Memo")					// 备注

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_ME_GAMMATB_RUNID_DIGITS				50					// 趟钻编号位数
#define	TV_ME_GAMMATB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_ME_GAMMATB
{
 
  int	iDataID;							// 自增字段
  char	szRunID[TV_ME_GAMMATB_RUNID_DIGITS + 1];							// 趟钻编号
  time_t	lTDateTime;							// 时间（精确到秒）
  int	iMillitime;							// 毫秒
  int	iToolID;							// 工具编号
  long	lGRMDepth;							// 自然伽马测量深度 
  long	lGRVDepth;							// 自然伽马测量深度 
  float	fMWDGAM;							// 伽马RAW
  float	fMWDGAM_API;							// 伽马经过API计算系数校正后数据
  float	fMWDGAM_APIC;							// 伽马经过API计算环境校正后数据
  float	fGRS0;							// 16扇0区扇区伽马值
  float	fGRS1;							// 16扇1区扇区伽马值
  float	fGRS2;							// 16扇2区扇区伽马值
  float	fGRS3;							// 16扇3区扇区伽马值
  float	fGRS4;							// 16扇4区扇区伽马值
  float	fGRS5;							// 16扇5区扇区伽马值
  float	fGRS6;							// 16扇6区扇区伽马值
  float	fGRS7;							// 16扇7区扇区伽马值
  float	fGRS8;							// 16扇8区扇区伽马值
  float	fGRS9;							// 16扇9区扇区伽马值
  float	fGRS10;							// 16扇10区扇区伽马值
  float	fGRS11;							// 16扇11区扇区伽马值
  float	fGRS12;							// 16扇12区扇区伽马值
  float	fGRS13;							// 16扇13区扇区伽马值
  float	fGRS14;							// 16扇14区扇区伽马值
  float	fGRS15;							// 16扇15区扇区伽马值
  float	fGRS0A;							// 16扇区0扇区伽马值（API计算系数校正后数据）
  float	fGRS0C;							// 16扇区0扇区伽马值（API计算环境校正后数据）
  float	fGRS1A;							// 16扇区1扇区伽马值（API计算系数校正后数据）
  float	fGRS1C;							// 16扇区1扇区伽马值（API计算环境校正后数据）
  float	fGRS2A;							// 16扇区2扇区伽马值（API计算系数校正后数据）
  float	fGRS2C;							// 16扇区2扇区伽马值（API计算环境校正后数据）
  float	fGRS3A;							// 16扇区3扇区伽马值（API计算系数校正后数据）
  float	fGRS3C;							// 16扇区3扇区伽马值（API计算环境校正后数据）
  float	fGRS4A;							// 16扇区4扇区伽马值（API计算系数校正后数据）
  float	fGRS4C;							// 16扇区4扇区伽马值（API计算环境校正后数据）
  float	fGRS5A;							// 16扇区5扇区伽马值（API计算系数校正后数据）
  float	fGRS5C;							// 16扇区5扇区伽马值（API计算环境校正后数据）
  float	fGRS6A;							// 16扇区6扇区伽马值（API计算系数校正后数据）
  float	fGRS6C;							// 16扇区6扇区伽马值（API计算环境校正后数据）
  float	fGRS7A;							// 16扇区7扇区伽马值（API计算系数校正后数据）
  float	fGRS7C;							// 16扇区7扇区伽马值（API计算环境校正后数据）
  float	fGRS8A;							// 16扇区8扇区伽马值（API计算系数校正后数据）
  float	fGRS8C;							// 16扇区8扇区伽马值（API计算环境校正后数据）
  float	fGRS9A;							// 16扇区9扇区伽马值（API计算系数校正后数据）
  float	fGRS9C;							// 16扇区9扇区伽马值（API计算环境校正后数据）
  float	fGRS10A;							// 16扇区10扇区伽马值（API计算系数校正后数据）
  float	fGRS10C;							// 16扇区10扇区伽马值（API计算环境校正后数据）
  float	fGRS11A;							// 16扇区11扇区伽马值（API计算系数校正后数据）
  float	fGRS11C;							// 16扇区11扇区伽马值（API计算环境校正后数据）
  float	fGRS12A;							// 16扇区12扇区伽马值（API计算系数校正后数据）
  float	fGRS12C;							// 16扇区12扇区伽马值（API计算环境校正后数据）
  float	fGRS13A;							// 16扇区13扇区伽马值（API计算系数校正后数据）
  float	fGRS13C;							// 16扇区13扇区伽马值（API计算环境校正后数据）
  float	fGRS14A;							// 16扇区14扇区伽马值（API计算系数校正后数据）
  float	fGRS14C;							// 16扇区14扇区伽马值（API计算环境校正后数据）
  float	fGRS15A;							// 16扇区15扇区伽马值（API计算系数校正后数据）
  float	fGRS15C;							// 16扇区15扇区伽马值（API计算环境校正后数据）
  float	fGRS0D;							// 16扇区0扇区伽马值（API暗电流校正后数据）
  float	fGRS0CG;							// 16扇区0扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS1D;							// 16扇区1扇区伽马值（API暗电流校正后数据）
  float	fGRS1CG;							// 16扇区1扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS2D;							// 16扇区2扇区伽马值（API暗电流校正后数据）
  float	fGRS2CG;							// 16扇区2扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS3D;							// 16扇区3扇区伽马值（API暗电流校正后数据）
  float	fGRS3CG;							// 16扇区3扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS4D;							// 16扇区4扇区伽马值（API暗电流校正后数据）
  float	fGRS4CG;							// 16扇区4扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS5D;							// 16扇区5扇区伽马值（API暗电流校正后数据）
  float	fGRS5CG;							// 16扇区5扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS6D;							// 16扇区6扇区伽马值（API暗电流校正后数据）
  float	fGRS6CG;							// 16扇区6扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS7D;							// 16扇区7扇区伽马值（API暗电流校正后数据）
  float	fGRS7CG;							// 16扇区7扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS8D;							// 16扇区8扇区伽马值（API暗电流校正后数据）
  float	fGRS8CG;							// 16扇区8扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS9D;							// 16扇区9扇区伽马值（API暗电流校正后数据）
  float	fGRS9CG;							// 16扇区9扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS10D;							// 16扇区10扇区伽马值（API暗电流校正后数据）
  float	fGRS10CG;							// 16扇区10扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS11D;							// 16扇区11扇区伽马值（API暗电流校正后数据）
  float	fGRS11CG;							// 16扇区11扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS12D;							// 16扇区12扇区伽马值（API暗电流校正后数据）
  float	fGRS12CG;							// 16扇区12扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS13D;							// 16扇区13扇区伽马值（API暗电流校正后数据）
  float	fGRS13CG;							// 16扇区13扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS14D;							// 16扇区14扇区伽马值（API暗电流校正后数据）
  float	fGRS14CG;							// 16扇区14扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGRS15D;							// 16扇区15扇区伽马值（API暗电流校正后数据）
  float	fGRS15CG;							// 16扇区15扇区伽马值（API泥浆贡献伽玛值校正后数据）
  float	fGR;							// 总伽马RAW
  float	fGRA;							// 总伽马经过API计算系数校正后数据
  float	fGRC;							// 总伽马经过API计算环境校正后数据
  float	fGRU;							// 16扇上伽马值
  float	fGRUA;							// 16扇上伽马值（API计算系数校正后数据）
  float	fGRUC;							// 上伽马经过API计算环境校正后数据
  float	fGRD;							// 16扇下伽马值
  float	fGRDA;							// 16扇下伽马值（API计算系数校正后数据）
  float	fGRDC;							// 下伽马经过API计算环境校正后数据
  float	fGRL;							// 16扇左伽马值
  float	fGRLA;							// 16扇左伽马值（API计算系数校正后数据）
  float	fGRLC;							// 左伽马经过API计算环境校正后数据
  float	fGRR;							// 16扇右伽马值
  float	fGRRA;							// 16扇右伽马值（API计算系数校正后数据）
  float	fGRRC;							// 右伽马经过API计算环境校正后数据
  float	fInc;							// 井斜
  short	nConf;							// 旋转状态:No|0.否;Yes|1.是
  short	nBad;							// 是否坏点:No|0.否;Yes|1.是
  short	nDrillActiv;							// 钻井状态 是否到底:OffBottom|0.否;OnButtom|1.是
  short	nSliding;							// 滑动状态:SSQ|0.静态测斜;TLSq|1.滑动序列;RLSQ|2.旋转序列
  short	nReLog;							// 是否复测 :Yes|0.否;No|1.是
  short	nLas;							// 是否Las :Yes|0.否;No|1.是
  time_t	lCreateTime;							// 创建时间戳
  time_t	lUpdTime;							// 最后一次修改时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_ME_GAMMATB_MEMO_DIGITS + 1];							// 备注
} TS_ME_GAMMATB;

typedef	TS_ME_GAMMATB FAR*	LPTS_ME_GAMMATB;

#pragma	pack(pop)

#endif // _TME_GAMMATB_H

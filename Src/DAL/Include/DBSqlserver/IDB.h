﻿/**********************************************************************************
* IDB.H修订记录
*
* 作者：		软件组
* 创建日期		2014-5-27
*
*
*2014-10-15改：增加排序方式宏定义、读取曲线段数据函数增加排序方式（SortMode）参数;
*2014-10-17:增加判断数据库是否存在函数IsExistDB();
*2015-1-13: 重构idb接口
*2015-1-23:	更改ReadDepth函数：精确到秒，参数tratio设置默认值0x7fffffff
*			修改SaveToolData函数：增加深度（__int64 depth）参数
*
*2015-10-29:
*			1.SaveToolData函数增加是否匹配深度参数
*			2.重构DataMatch函数
*			3.ReadCurveInfo函数增加指定仪器名称参数
*2015-11-06:
*			1.增加DeleteTool函数
*			2.增加DeleteTableData函数
*
*2015-11-11:
*			1.增加ReadOrders函数：读取当前服务项目中所有的序列
*			2.增加ReadRTCurves函数：读取当前服务项目中所有实时曲线
*
*2015-11-17:
*			1.增加OutPutMatchData函数的重载形式：输出指定仪器指定曲线的数据至文件（可能有多个仪器）
*
*2016-07-13
*			1.增加根据深度点（段）/时间点（段）删除仪器数据/实时数据
*2016-07-15
*			1.增加2个宏定义
*2016-09-14
*			1.优化仪器数据输出函数OutPutMatchData
*2018-12-14
*			1.增加泥浆信息、Run信息、钻井信息相关操作
*2019-01-29
*			1.增加仪器数据导出功能
*
*		
***********************************************************************************/

#ifndef _IDB_H_
#define _IDB_H_

#include "ULInterface.h"
#include "DBDefine.h"
#include "TTS_RXAzimuthalTB.h"
#include "TTS_AziResistivityTB.h"
#include "TTS_RstCompensatedTB.h"
#include "TTS_RstUncompensatedTB.h"

#include "TUnitTB.h"
#include "TCurveInfoTB.h"
#include "TWITSTB.h"
#include "TMeasPointInfoTB.h"
#include "TToolInfoTB.h"
#include "TToolClass.h"
#include "TCurveClass.h"
#include "TME_AziGammaTB.h"
#include "TME_GammaTB.h"
#include "TME_AziResTB.h"
#include "TME_ResistivityTB.h"
#include "TME_AziResGeoSig.h"
#include "TCurveStore.h"
#include "TRunInfoTB.h"
#include "TMudInfoTB.h"
#include "THoleInfoTB.h"
#include "TGammaSub.h"
#include "TTimeDrillingMode.h"
#include "TProjectTB.h"
#include "TToolAizRes.h"
//#include "ULCOMMDEF.H"
//#include "iproject.h"
//#include "iulfile.h"
//2020.2.24 Ver1.6.0 TASK【02】 Start
#include "TTimeDepthTB.h"
#include "TViewItemUnitTB.h"
//2020.2.24 Ver1.6.0 TASK【02】 End

//2020.5.26 dyl start 引入自然伽马结构体
#include "TME_GammaTB.h"
//2020.5.26 dyl end

//2020.7.21 ver1.6.0 bug[115] start
#include "TWellSiteInfoTB.h"
//2020.7.21 ver1.6.0 bug[115] end

//2020.2.25 Ver1.6.0 TASK【02】 Start
#include <TRT_APDataTB.h>
#include <TRT_AZIGRDataTB.h>
#include <TRT_AZIResDataTB.h>
#include <TRT_CollarTempDataTB.h>
#include <TRT_GRDataTB.h>
#include <TRT_MWDDataTB.h>
#include <TRT_ResistivityDataTB.h>
#include <TRT_ROPDataTB.h>
#include <TWellInfoTB.h>
#include <THoleInfoTB.h>
#include <TBHAInfoMTB.h>
#include <TBHAInfoDTB.h>
#include <TBHADMeasPointD.h>
#include <TBHACurveD.h>
#include "TSurveyCheckPara.h"
#include "TCA_SurveyData.h"

#include <TME_APR.h>
//2020.2.25 Ver1.6.0 TASK【02】 End

#include "TResCalcPara.h"

#include <TRT_GENTIME.h>
#include <TRT_CUSTOM.h>
#include <TSU_CUSTOM.h>
#include <TRT_NBSUB.h>

#include <TPressureCalibration.h>
//2023.12.07 Start
#include "TRestoreInfo.h"
//2023.12.07 End
#include <map>
#include <list>
//数据表类型
#define TABLE_DEPTH		1
#define TABLE_RTDATA	2
#define TABLE_TOOLDATA	3

#define CURVEVALUE_MIN	-pow(10,30)
#define CURVEVALUE_MAX	pow(10,30)
typedef void(*CallBackDisFunc)(int) ;
//Interfaces
interface IDB:IUnknown
{
	//连接数据库系统（无参数，读取默认配置文件）
	ULMETHOD	ConnectDBSource() = 0;	
	//连接数据库系统
	ULMETHOD	ConnectDBSource(LPCTSTR strUsername,LPCTSTR strServer,LPCTSTR strPasswd) = 0;
	//列出当前数据库系统中的所有数据库，存到DBArray中
	ULMETHOD	ListDB(CStringArray &DBArray) = 0; 
	//关闭数据库连接
	ULMETHOD	CloseDB() = 0;

	//设置服务项目
	ULMETHOD	SetService(LPCTSTR ServiceName) = 0;

	//创建、打开、附加数据库文件(database为完整路径（带后缀），dbname为数据库名称)
	ULMETHOD	CreateDatabase(LPCTSTR database) = 0;
	ULMETHOD	OpenDatabase(LPCTSTR dbname) = 0;	//打开数据库
	ULMETHOD	OpenProjectDB(LPCTSTR strProFile) = 0;	//打开工程对应数据库
	ULMETHOD	AdditionalDatabase(LPCTSTR database) = 0;//数据库附加

	//创建各种表
	ULMETHOD	CreateTable_Project() = 0;  //工程信息表
	ULMETHOD	CreateTable_Service() = 0;		//服务项目表
	ULMETHOD	CreateTable_CurveInfo() = 0;//曲线信息表
	ULMETHOD	CreateTable_LogInfo() = 0;	//测井信息表
	ULMETHOD	CreateTable_Order() = 0;	//序列配置表
	ULMETHOD	CreateTable_RunInfo() = 0;	//Run信息表
	ULMETHOD	CreateTable_MudInfo() = 0;	//泥浆信息表
	ULMETHOD	CreateTable_DrillInfo() = 0;//钻井信息表
	ULMETHOD	CreateTable_SurveyData() = 0;//井斜数据表

	ULMETHOD	CreateTable_Depth() = 0;	//深度信息表
	ULMETHOD	CreateTable_RTData() = 0;	//实时测井数据表
	ULMETHOD	CreateTable_MTData() = 0;	//实时测井数据表(修改数据)

	ULMETHOD	CreateTable_ToolData(LPCTSTR ToolName) = 0;  //仪器数据表（服务项目名_仪器名,创建前先为仪器添加曲线AddCurveInfo）

	//序列信息存取
	ULMETHOD	SetOrderInfo(DBOrderInfo* pOrder) = 0;
	//ULMETHOD	ReadOrder(DB_Orders* pOrders) = 0;
	ULMETHOD	ReadOrders(CStringArray* pArrOrders) = 0;	//读取服务项目中包含的所有序列
	ULMETHOD	ReadRTCurves(CStringArray* pArrCurves) = 0;	//读取服务项目中包含的所有实时曲线

	//深度信息存取
	ULMETHOD	SaveDepth(DBDepthData* ddata) = 0;
	//ULMETHOD	ReadDepth(SYSTEMTIME time,__int64 &depth) = 0;
	ULMETHOD	ReadDepth(SYSTEMTIME time,__int64 &depth,int tratio=0) = 0;

	//实时数据存取
	ULMETHOD	SaveRTCurve() = 0;
	ULMETHOD	SaveRTData(RTDataInfo* pData) = 0;
	ULMETHOD	SaveMTData(RTDataInfo* pData) = 0;
	ULMETHOD	SetMTData(DWORD dwMask , RTDataInfo* pData) = 0;	//修改指定记录数据

	ULMETHOD	SaveRTData(LPCTSTR strSN , LPCTSTR strCurve , float fValue , long lDepth, int OnBottom, int Bad , LPCTSTR lpszRunNo) = 0;
	ULMETHOD	SaveMTData(LPCTSTR strSN , LPCTSTR strCurve , float fValue , long lDepth, int OnBottom, int Bad , LPCTSTR lpszRunNo) = 0;
	ULMETHOD	ReadRTData(DB_RTDatas* pRTDatas, bool m_bOnBottom) = 0;					 //读取实时数据
	ULMETHOD	ReadMTData(DB_RTDatas* pRTDatas, bool m_bOnBottom) = 0;					 //读取实时数据
	ULMETHOD	ReadRTData(DB_RTDatas* pRTDatas,CStringArray* pArrOrders,CStringArray* pArrCurves, bool m_bOnBottom) = 0;  //根据曲线读取实时数据 

	//工程信息存取
	ULMETHOD	SaveProjectInfo(DB_ProjectInfo* pro_infos) = 0;
	ULMETHOD	ReadProjectInfo(DB_ProjectInfo* pro_infos) = 0;

	//Run信息存取
	ULMETHOD	SaveRunInfo(DB_RunInfo* Run_infos) = 0;
	ULMETHOD	ReadRunInfo(DB_RunInfo* Run_infos) = 0;

	//泥浆信息存取
	ULMETHOD	SaveMudInfo(DB_MudInfo* Mud_infos) = 0;
	ULMETHOD	ReadMudInfo(DB_MudInfo* Mud_infos) = 0;

	//钻井信息存取
	ULMETHOD	SaveDrillInfo(DB_DrillInfo* Drill_infos) = 0;
	ULMETHOD	ReadDrillInfo(DB_DrillInfo* Drill_infos) = 0;

	//井斜数据存取
	ULMETHOD	SaveSurveyData(DB_SurveyData* SurveDatas) = 0;
	ULMETHOD	ReadSurveyData(DB_SurveyData* SurveyDatas , LPCSTR lpszHoleID, LPCTSTR lpszWhere) = 0;

	//服务项目信息存取
	ULMETHOD	ReadService(CStringArray &sArray) = 0;

	//测井信息存取(num为上一次测井的num，若没有则为0)
	ULMETHOD	SaveLogInfo(LPCTSTR ToolName,int &num,LPCTSTR SNName) = 0;
	//ULMETHOD	ReadLogInfo() = 0;

	//仪器信息存取
	ULMETHOD	ReadTools(CStringArray* sArray)	= 0;	//读取当前服务项目中所有仪器
	ULMETHOD	CheckTool(LPCTSTR strTool) = 0;			//判断是否存在某个仪器
	ULMETHOD	AddTool(LPCTSTR strToolName) = 0;		//向当前服务项目中添加一个仪器

	//曲线信息存取
	//SaveCurveInfo() = 0;
	//ReadCurveInfo() = 0;
	ULMETHOD	AddCurveInfo(LPCTSTR strCurveName,LPCTSTR strCurveUnit,float fDepthOffset,WORD nCurveType,int nDimension,int nPointCount,int nMode) = 0;		//添加一条曲线信息
	ULMETHOD	ReadCurveInfo(int nMode,CString strTool,DB_CurveInfos* vecCurveInfos) = 0;
	
	
	//仪器数据存取

	ULMETHOD	AddCurveValue(LPCTSTR strCurveName,float fCurveValue) = 0;	//添加一条曲线值
	ULMETHOD	AddToolData(SYSTEMTIME time,__int64 depth,int OnBottom,CArray<float,float>* pArrValues,int num=-1) = 0;
	ULMETHOD	SaveToolDataBegin(LPCTSTR ToolName,CStringArray* pArrNames) = 0;
	//ULMETHOD	SaveToolData(LPCTSTR ToolName,CStringArray* pArrNames) = 0;
	ULMETHOD	SaveToolDataEnd(int &nTotalCount) = 0;
	ULMETHOD	SaveToolData(LPCTSTR ToolName,int num,SYSTEMTIME time,__int64 depth,BOOL bMatch=FALSE) = 0;  //保存一条记录(先添加曲线值)
	ULMETHOD	SetProgressInfo(UINT nID,CWnd* pWnd) = 0;
	//	ULMETHOD	ReadToolData() = 0; 

// 	//公用操作
// 	ULMETHOD	DeleteTable(LPCTSTR strTableName) = 0;	//删除表数据
	ULMETHOD	GetDepthRange(long& lMinDepth,long& lMaxDepth) = 0;
	ULMETHOD	GetTimeRange(COleDateTime& minTime,COleDateTime& maxTime) = 0;
	ULMETHOD	DeleteTool(LPCTSTR pszTool) = 0;
	ULMETHOD	DeleteTableData(UINT TableType,LPCTSTR strTool = NULL) = 0;

	/*仪器数据匹配*/
	//ULMETHOD		DataMatch(/*LPCTSTR ServiceName=NULL,*/CStringArray* ToolArray=NULL,COleDateTime* starttime=NULL,COleDateTime* endtime=NULL) = 0;   //数据匹配
	//ULMETHOD		DataMatch(int &nRowCount,CStringArray* ToolArray=NULL) = 0;   //数据匹配
	ULMETHOD		DataMatch(int &nRowCount,int nLingC,CStringArray* ToolArray=NULL) = 0;   //数据匹配
	ULMETHOD		OutPutMatchData(LPCTSTR strFile,LPCTSTR strTool,CStringArray* pArrCurves,BOOL bTitle,LPCTSTR strMark,long lDepthSpace,COleDateTime* pStartTime,COleDateTime* pEndTime) = 0;	//输出
	ULMETHOD		OutPutMatchData(LPCTSTR strFile,LPCTSTR strTool,CStringArray* pArrCurves,BOOL bTitle,LPCTSTR strMark,long lDepthSpace,long lStartDepth,long lEndDepth) = 0;	//输出
	ULMETHOD		OutPutMatchData(LPCTSTR strFile,DB_OutMatchDatas* pOutCurves,OutDataParam stOutParam,long lStartDepth,long lEndDepth, bool m_bOnBottom) = 0;		//指定仪器指定曲线输出
	ULMETHOD		OutPutToolData(LPCTSTR strTool,COleDateTime* pStartTime,COleDateTime* pEndTime , DB_ToolDatas* pToolDatas) = 0;	//输出
	ULMETHOD		SetCorParams(CorParams* pPars) = 0;
//	ULMETHOD		OutPutRecordset() = 0;		//输出数据库记录集   ???？？？注释掉

	//数据删除
	/********************************************************************************************************************************
	*DeleteToolDataByDepth	- 根据深度删除仪器数据，起始深度等于终止深度时，则删除某个深度点的仪器数据；否则删除一个深度段的仪器数据
	*DeleteToolDataByTime	- 根据时间删除仪器数据，起始时间等于终止时间时，则删除某个时间点的仪器数据；否则删除一个深度段的仪器数据
	*注：若pCurveArray不为空；则将该仪器下的指定曲线数据置为无效值（-999.25）
	*DeleteRTDataByDepth	- 根据深度删除实时数据
	*DeleteRTDataByTime		- 根据时间删除实时数据
	*********************************************************************************************************************************/
	ULMETHOD		DeleteToolDataByDepth(LPCTSTR strTool, long lStartDepth, long lEndDepth, CStringArray* pCurveArray = NULL) = 0;
	ULMETHOD		DeleteToolDataByTime(LPCTSTR strTool, SYSTEMTIME StartTime, SYSTEMTIME EndTime, CStringArray* pCurveArray = NULL) = 0;

	ULMETHOD		DeleteRTDataByDepth(long lStartDepth, long lEndDepth) = 0;
	ULMETHOD		DeleteRTDataByTime(SYSTEMTIME StartTime, SYSTEMTIME EndTime) = 0;
// 2020.01.10 Ver1.60 TASK【007】Start

//------------------------------------//
//函数名 : 计算Rep
//参数 : fCCP 相差的值
//参数 : nIndex 第几个相差
//说明: 根据CCP 查找计算对应的 Rep
//返回值: 
//备注:
//-------------------------------------//
	ULMETHOD		CalcuteRep(double dCCP,int nIndex,double& dRep) = 0;

//------------------------------------//
//函数名 : 计算Rea
//参数 : fCCA 相差的值
//参数 : nIndex 第几个幅差
//说明: 根据CCA 查找计算对应的 Rea
//返回值: 
//备注:
//-------------------------------------//
	ULMETHOD		CalcuteRea(double dCCA,int nIndex,double& dRea ) = 0;

//------------------------------------//
//函数名 : 把电阻率记录读到内存map里面
//参数 : 
//参数 : 
//说明: 
//返回值: 
//备注:
//-------------------------------------//
//ULMETHOD		ReadRecordRepReaInMemory() = 0;

//------------------------------------//
//函数名 : 插入一条PRD0017.RAW的记录
//参数 : pCPRD0017Field 
//说明: 
//返回值: 
//备注:
//-------------------------------------//
	ULMETHOD		SaveMEPRD0017Record(const LPMEResistivityTB pCPRD0017Field) = 0;

	// 2020.02.03 Ver1.60 TASK【007】Start
	//------------------------------------//
	//函数名 : 向MEResAirHangTB表插入一条记录
	//参数 : pMERAHangTB 表示一条记录 
	//说明: 
	//返回值: 
	//备注:
	//-------------------------------------//
	ULMETHOD		SaveMEResAirHangTBRecord(const LPMEResAirHangTB pMERAHangTB) = 0;

	//------------------------------------//
	//函数名 : 获取ToolInfoTB表中的ToolSN
	//参数 :  
	//说明: 
	//返回值: 
	//备注:
	//-------------------------------------//
	ULMETHOD		 GetToolSN(std::vector<tagToolInfoTB>& vecToolSN) = 0;

	//------------------------------------//
	//函数名 : 查询 MEResAirHangTB
	//参数 :  vecMERAHangTB 记录查询到的结果
	//说明: 
	//返回值: 
	//备注:
	//-------------------------------------//
	ULMETHOD	GetMEResAirHangTBRecord(LPCTSTR strToolSN, std::vector< MEResAirHangTB>& vecMERAHangTB) = 0;
	// 2020.02.03 Ver1.60 TASK【007】End

	//作者 : ning
	//时间 : 20200221
	//作用 : 查询“单位”
	ULMETHOD	QueryUnits(std::vector<TS_UNITTB>& vecUnit) = 0;;


	//作者 : ning
	//时间 : 20200223
	//作用 : 查询“曲线”
	ULMETHOD	QueryCurves(LPCTSTR strConditions, std::vector<TS_CURVEINFOTB>& vecCurve) = 0;
	//作者 : yangshuo
	//时间 : 20200513
	//作用 : 查询“曲线”
	ULMETHOD	GetAllCurveNameFromDB(std::vector<TS_CURVEINFOTB>& vecCurve) = 0;
	//作者 : ning
	//时间 : 20200225
	//作用 : 更新数据库表CurveInfoTB中的UID字段
	ULMETHOD	UpdateCurveInfoUID(CString strSql) = 0;

	//作者 : ning
	//时间 : 20200303
	//作用 : 根据CurvID删除CurveInfoTB中的某些行
	ULMETHOD	DelCurveInfo(CString strSql) = 0;

	//作者 : ning
	//时间 : 20200304
	//作用 : 获取数据库表WITSTB中的部分信息
	ULMETHOD	QueryWITS(/*CString strSql, */std::vector<TS_WITSTB>& vecWits) = 0;

	//作者 : ning
	//时间 : 20200304
	//作用 : 获取数据库表MeasPointInfoTB中的部分信息
	ULMETHOD	QueryMeasPointInfo(std::vector<TS_MEASPOINTINFOTB>& vecMeasPoint) = 0;


	//作者 : ning
	//时间 : 20200304
	//作用 : 获取数据库表ToolInfoTB中的部分信息
	ULMETHOD	QueryToolInfo(std::vector<TS_TOOLINFOTB>& vecToolInfo) = 0;
	
	//作者 : ning
	//时间 : 20200309
	//作用 : 获取数据库表CurveClass中的数据
	ULMETHOD	QueryCurveClass(std::vector<TS_CURVECLASS>& vecCurveClass) = 0;

	//作者 : ning
	//时间 : 20200316
	//作用 : 获取数据库表CurveStore中的数据
	ULMETHOD	QueryCurveStore(std::vector<TS_CURVESTORE>& vecCurveStore) = 0;

	//作者 : ning
	//时间 : 20200316
	//作用 : 执行一条SQL语句，并返回一个CString向量
	//说明 : 第一次使用——根据表名获取其所有字段名
	ULMETHOD	GetTableColumns(CString strSql, std::vector<CString>& strCols) = 0;
	ULMETHOD	GetTableColumns2( CString& strTableName, std::vector<CString>& strCols) = 0;

	//作者 : ning
	//时间 : 20200330
	//作用 : 获取数据库表TimeDrillingMode中的数据
	ULMETHOD	QueryTimeDrillingMode(std::vector<TS_TIMEDRILLINGMODE>& vecTimeDrillingMode) = 0;

	//作者 : ning
	//时间 : 20200421
	//作用 : 获取数据库中的所有表名
	ULMETHOD	QueryAllTableName(std::vector<CString>& vecAllTableName) = 0;

	//作者 : ning
	//时间 : 20200509
	//作用 : 插入、更新数据库表ViewItemUnitTB中的数据
	ULMETHOD	DataChangeViewItemUnit(LPCTSTR pszViewID, LPCTSTR pszItemID, int iDBUID, int iShowUID, LPCTSTR pszDesc) = 0;

	//作者 : ning
	//时间 : 20200511
	//作用 : 查询数据库表ViewItemUnitTBZ中的数据
	ULMETHOD	QueryViewItemUnit(std::vector<TS_VIEWITEMUNITTB>& vecItemUnit) = 0;

//------------------------------------//
//函数名 : 向TS_RXAzimuthalTB表插入一条记录
//参数 : pRxAziMuthaltb 表示一条记录 
//说明: 
//返回值: 
//备注:作者杨硕 时间2020/2/26
//-------------------------------------//
	ULMETHOD		SaveRXAZIMUTHALTBRecord(const LPTS_TS_RXAZIMUTHALTB pRxAziMuthaltb) = 0;

//------------------------------------//
//函数名 : 向TS_AziResistivityTB表插入一条记录
//参数 : pAziRestb 表示一条记录 
//说明: 
//返回值: 
//备注:作者杨硕 时间2020/2/26
//-------------------------------------//
	ULMETHOD		SaveAZIResTBRecord(const LPTS_TS_AZIRESISTIVITYTB pAziRes) = 0;


	ULMETHOD		SaveRSTComTBRecord(const LPTS_TS_RSTCOMPENSATEDTB pRSTCom) = 0;
	ULMETHOD		SaveRSTUnComTBRecord(const LPTS_TS_RSTUNCOMPENSATEDTB pRSTUnCom) = 0;

	ULMETHOD		GetRXAZIMUTHALTBRecord(std::vector<TS_TS_RXAZIMUTHALTB>& vecRxAziMuthaltb) = 0;
	ULMETHOD		GetAZIResTBRecord(std::vector<TS_TS_AZIRESISTIVITYTB>& vecAziRes) = 0;
	ULMETHOD		GetRSTComTBRecord(std::vector <TS_TS_RSTCOMPENSATEDTB>& vecRSTCom) = 0;
	ULMETHOD		GetRSTUnComTBRecord(std::vector<TS_TS_RSTUNCOMPENSATEDTB>& vecRSTUnCom) = 0;
// 2020.01.10 Ver1.60 TASK【007】End

// 2020.03.04 Ver1.60 TASK【008】Start
//------------------------------------//
//函数名 : 查询ToolInfoTB结果
//参数 : vecToolSN 保存查询结果 szTCID 多个szTCID 用逗号隔开
//说明: 
//返回值: 
//备注:作者杨硕 时间2020/3/10
//-------------------------------------//
	ULMETHOD		 GetToolInfoTB(std::vector<TS_TOOLINFOTB>& vecToolSN,LPSTR szTCID = NULL) = 0;
	ULMETHOD		 UpdateToolSN(const TS_TOOLINFOTB* pToolSN) = 0;
	ULMETHOD		 DeleteToolSN(const TS_TOOLINFOTB* pToolSN) = 0;
	ULMETHOD		 AddToolSN(const TS_TOOLINFOTB* pToolSN) = 0;
	ULMETHOD		 GetMeasPointInfo(std::vector<TS_MEASPOINTINFOTB>& vecMPInfo) = 0;
	ULMETHOD		 UpdateMeasPointInfo(const TS_MEASPOINTINFOTB* pMPInfo) = 0;
	ULMETHOD		 DeleteMeasPointInfo(const TS_MEASPOINTINFOTB* pMPInfo) = 0;
	ULMETHOD		 AddMeasPointInfo(const TS_MEASPOINTINFOTB* pMPInfo) = 0;
	ULMETHOD		 QueryToolclass(std::vector < TS_TOOLCLASS>& vecToolClass) = 0;
	ULMETHOD		 AddAziGamma(const TS_ME_AZIGAMMATB* pMeAZIGamma) = 0;
	ULMETHOD		 AddAziRes(const TS_ME_AZIRESTB* pMeAZIRes) = 0;
	ULMETHOD		 AddResistivity(const TS_ME_RESISTIVITYTB* pMeResistivity) = 0;
	ULMETHOD		 QueryRuninfotb(std::vector<MS_RUNINFOTB>& vecRuninfo) = 0;	
	ULMETHOD		 QueryToolIdName(std::vector<TS_TOOLINFOTB>& vecToolinfo, LPCTSTR lpszBHAID) = 0;
	ULMETHOD		 QueryMudInfotb(std::vector<TS_MUDINFOTB>& vecMudinfo, LPCTSTR lpszHoleId) = 0;
	ULMETHOD		 QueryHoleInfotb(std::vector<TS_HOLEINFOTB>& vecHoleinfo, LPCTSTR lpszHoleID) = 0;
	ULMETHOD		 QueryGammaSub(std::vector<TS_GAMMASUB>& vecGammaSub, LPCTSTR lpszRunID) = 0;
	ULMETHOD         DeleteAziInTime(LPCTSTR strTableName, LPCTSTR strStartTime, LPCTSTR strEndTime) = 0;
	ULMETHOD         GetWellDepth(time_t lStartTime, time_t lEndTime, LPCSTR lpszRunID, std::vector <TS_TIMEDEPTHTB>& vecWellDepth) = 0;
	ULMETHOD         UpdateGammaSub(LPCTSTR lpszRunID, const float& fToolDCF, const float& fAPICF) = 0;
	ULMETHOD         GetUIDUNameFromCurveUnit(std::map<int, CString>& mapUidName) = 0;
// 2020.03.04 Ver1.60 TASK【008】End

// 2020.1.19 Ver1.6.0 TASK【002】 Start
	//////////////////////////////////////////////////////////////////////////
	ULMETHOD	GetAllProject(CArray<TS_PROJECTTB, TS_PROJECTTB>* pArray) = 0;
	ULMETHOD	AddProject(TS_PROJECTTB* pInfo) = 0;
	ULMETHOD	DeleteProject(LPCTSTR lpszProjectID) = 0;
	ULMETHOD	GetProject(LPCTSTR lpszProjectID, TS_PROJECTTB* pInfo) = 0;
	ULMETHOD	SetProject(LPCTSTR lpszProjectID, TS_PROJECTTB* pInfo) = 0;
	ULMETHOD	GetProjectByName(LPCTSTR lpszProjectName, TS_PROJECTTB* pInfo) = 0;
	ULMETHOD	GetAllWellSiteID(LPCTSTR lpszProjectID, CArray<CString, CString&>* pArray) = 0;
	ULMETHOD	GetAllWellSite(LPCTSTR lpszProjectID, std::vector<TS_WELLSITEINFOTB>& vecWellSite) = 0;

	ULMETHOD	AddWellSite(LPTS_WELLSITEINFOTB pInfo) = 0;
	ULMETHOD	DeleteWellSite(LPCTSTR lpszWellSiteID) = 0;
	ULMETHOD	GetWellSite(LPCTSTR lpszWellSiteID, LPTS_WELLSITEINFOTB pInfo) = 0;
	ULMETHOD	SetWellSite(LPTS_WELLSITEINFOTB pInfo) = 0;
	ULMETHOD	GetAllWellID(LPCTSTR lpszWellSiteID , CArray<CString , CString&> *pArray) = 0;
	ULMETHOD	GetAllWell(LPCTSTR lpszWellSiteID, std::vector<TS_WELLINFOTB>& vecWell) = 0;

	ULMETHOD	AddWell(LPTS_WELLINFOTB pInfo) = 0;
	ULMETHOD	DeleteWell(LPCTSTR lpszWellID) = 0;
	ULMETHOD	GetWell(LPCTSTR lpszWellID, LPTS_WELLINFOTB pInfo) = 0;
	ULMETHOD	SetWell(LPTS_WELLINFOTB pInfo) = 0;
	ULMETHOD	GetAllHoleID(LPCTSTR lpszWellID, CArray<CString, CString&> *pArray) = 0;
	ULMETHOD	GetAllHole(LPCTSTR lpszWellID, std::vector<TS_HOLEINFOTB>& vecHole) = 0;

	ULMETHOD	AddHole(LPTS_HOLEINFOTB pInfo) = 0;
	ULMETHOD	DeleteHole(LPCTSTR lpszHoleID) = 0;
	ULMETHOD	GetHole(LPCTSTR lpszHoleID, LPTS_HOLEINFOTB pInfo) = 0;
	ULMETHOD	SetHole(LPTS_HOLEINFOTB pInfo) = 0;
	ULMETHOD	GetAllRunID(LPCTSTR lpszHoleID, CArray<CString, CString&>* pArray) = 0;
	ULMETHOD	GetAllRun(LPCTSTR lpszHoleID, std::vector<TS_RUNINFOTB>& vecRun) = 0;

	ULMETHOD	AddRun(LPTS_RUNINFOTB pInfo) = 0;
	ULMETHOD	DeleteRun(LPCTSTR lpszRunID) = 0;
	ULMETHOD	GetRun(LPCTSTR lpszRunID, LPTS_RUNINFOTB pInfo) = 0;
	ULMETHOD	SetRun(LPTS_RUNINFOTB pInfo) = 0;
	ULMETHOD	SetActiveRun(LPCTSTR lpszRunID) = 0;
	ULMETHOD	GetActiveRun(CString &strRunID) = 0;
	
	ULMETHOD	AddBHA(LPTS_BHAINFOMTB pInfo) = 0;
	ULMETHOD	DeleteBHA(LPCTSTR lpszBHAID) = 0;
	ULMETHOD	GetBHA(LPCTSTR lpszBHAID, LPTS_BHAINFOMTB pInfo) = 0;
	ULMETHOD	SetBHA(LPTS_BHAINFOMTB pInfo) = 0;
	ULMETHOD	GetAllBHAID(CArray<CString, CString&>* pArray) = 0;
	ULMETHOD	GetAllToolID(LPCTSTR lpszBHAID, CArray<int , int> *pArray) = 0;

	ULMETHOD	AddBHATool(LPTS_BHAINFODTB pInfo) = 0;
	ULMETHOD	DeleteBHATool(LPCTSTR lpszBHAID ,int nToolNo) = 0;
	ULMETHOD	DeleteBHATool(LPCTSTR lpszBHAID) = 0;
	ULMETHOD	GetBHATool(LPCTSTR lpszBHAID , int nToolID , int nToolNo , LPTS_BHAINFODTB pInfo) = 0;
	ULMETHOD	SetBHATool(LPCTSTR lpszBHAID , int nToolID , int nToolNo , LPTS_BHAINFODTB pInfo) = 0;
	ULMETHOD	GetBHAIDByName(LPCTSTR lpszBHAName, CString& strBHAID) = 0;
	///////////////////////////////////////////////////////////////////////////

	ULMINT		AddDBTool(DB_TOOL_INFO *pInfo) = 0;
	ULMETHOD	DeleteDBTool(int nToolID) = 0;
	ULMETHOD	GetDBTool(int nToolID , DB_TOOL_INFO *pInfo) = 0;
	ULMETHOD	SetDBTool(int nToolID , DB_TOOL_INFO *pInfo) = 0;
	ULMINT		GetDBToolIDByName(CString strToolName) = 0;
	ULMETHOD	GetAllDBToolID(CArray<int , int> *pArray) = 0;

	ULMETHOD	GetDBToolTypeInfo(CString strTCID , DB_TOOL_TYPE_INFO *pInfo) = 0;

	ULMINT		AddMud(MUD_INFO *pInfo) = 0;
	ULMETHOD	DeleteMud(int nMudID) = 0;
	ULMETHOD	GetMud(int nMudID , MUD_INFO *pInfo) = 0;
	ULMETHOD	SetMud(int nMudID , MUD_INFO *pInfo) = 0;

	ULMINT		AddSurvey(SURVEY_INFO *pInfo) = 0;
	ULMETHOD	DeleteSurvey(int nSurveyID) = 0;
	ULMETHOD	GetSurvey(int nSurveyID , SURVEY_INFO *pInfo) = 0;
	ULMETHOD	SetSurvey(int nSurveyID , SURVEY_INFO *pInfo) = 0;

	ULMINT		AddTarget(TARGET_INFO *pInfo) = 0;
	ULMETHOD	DeleteTarget(int nTargetID) = 0;
	ULMETHOD	GetTarget(int nTargetID , TARGET_INFO *pInfo) = 0;
	ULMETHOD	SetTarget(int nTargetID , TARGET_INFO *pInfo) = 0;
// 2020.1.19 Ver1.6.0 TASK【002】 End

    //2020.2.11 Ver1.6.0 BUG【24】 Start
	ULMETHOD    GetCurveNameFromDB(int iWitsID,CString &strCurveName) = 0;
	//2020.2.11 Ver1.6.0 BUG【24】 End

	//2020.2.18 Ver1.6.0 BUG【24】 Start
	ULMETHOD    GetCurveInfoFromDB(CArray<CURVEINFOTB ,CURVEINFOTB &> *pCurveInfo) = 0;
	ULMETHOD    GetCurveInfoFromWITS(CArray<CURVEINFOTB ,CURVEINFOTB &> *pCurveInfo) = 0;
	//2020.1.18 Ver1.6.0 BUG【24】 End

	//2020.2.24 Ver1.6.0 TASK【02】 Start
	ULMETHOD	SaveTimeDepthDB(TS_TIMEDEPTHTB* pTimeDepth) = 0;
	//2020.2.24 Ver1.6.0 TASK【02】 End

	//2020.2.25 Ver1.6.0 TASK【02】 Start
	//20200617 sys 判断数据库是否存在 Start
	ULMETHOD    ExistDatabase(LPCTSTR dbname, CString& strDb) = 0;
	//20200617 sys 判断数据库是否存在 End
	ULMETHOD	GetTableNameFromDB(LPCTSTR lpszCurveName, CString &strTableName, CString &strFieldName) = 0;
	ULMETHOD    SaveRTApDataTB(TS_RT_APDATATB *pRtAppData) = 0;
	ULMETHOD    SaveRTAziGRTB(TS_RT_AZIGRDATATB *pRtAziGrData) = 0;
	ULMETHOD	SaveRTAziResTB(TS_RT_AZIRESDATATB *pRtAziResData) = 0;
	ULMETHOD	SaveRTCollTmpTB(TS_RT_COLLARTEMPDATATB *pRtCollData) = 0;
	ULMETHOD	SaveRTGRTB(TS_RT_GRDATATB *pRtGrData) = 0;
	ULMETHOD	SaveRTMwdTB(TS_RT_MWDDATATB *pRtMwdData) = 0;
	ULMETHOD	SaveRTResTB(TS_RT_RESISTIVITYDATATB *pRtResData) = 0;
	ULMETHOD	SaveRTRopTB(TS_RT_ROPDATATB *pRtRopData) = 0;
	//2020.2.25 Ver1.6.0 TASK【02】 End
	ULMETHOD    QueryViewItemUnitTBEx(std::vector<TS_VIEWITEMUNITTB>& vecViewItemunit, const CString& strViewName) = 0;
	ULMETHOD    SaveViewItemUnitTB(const TS_VIEWITEMUNITTB* pvecViewItemunit) = 0;
	ULMETHOD    SaveViewItemUnitTBAll(const CString& strViewName, const std::vector<TS_VIEWITEMUNITTB>& vecViewItemunit) = 0;
	ULMETHOD    UpdateCurveInfoByViewItemUnit(const TS_VIEWITEMUNITTB* pViewItem) = 0;
	ULMETHOD	GetCurveStore(std::vector<TS_CURVESTORE>& vecCurveStore) = 0;
	ULMETHOD	GetCurveFromTable(LPCTSTR lpszTableName, std::vector<TS_CURVEINFOTB>& vecCurveInfo) = 0;
	ULMETHOD	GetAllRunInfo(std::vector<TS_RUNINFOTB>& vecRunInfo) = 0;
	ULMETHOD	GetCurveValue(LPCTSTR lpszCurveName, CDBFilterCurveInfo* pInfo, std::vector<MS_CURVEVALUE>& vecCurveValue) = 0;
	ULMETHOD	SetCurveValue(DWORD dwMask, RTDataInfo *pData) = 0;
	//2020.5.22 dyl start获取深度
	ULMETHOD GetWellDepth_2(time_t lTDateTime, long& strDepth) = 0;
	//2020.5.22 dyl end

	//2020.5.26 dyl start 写自然伽马数据
	ULMETHOD	AddGamma(const TS_ME_GAMMATB* pMeGamma) = 0;
	//2020.5.26 dyl end
	ULMETHOD	QuerySysobject(std::vector<CString>& vecSysobj) = 0;
	ULMETHOD	QuerySyscolumns(LPCTSTR lpszTableName, std::vector<CString>& vecSyscolumns) = 0;
	// 2020.06.16 ltg Start
	ULMETHOD	CopmuteSensorToBit() = 0;
	// 2020.06.16 ltg End
	ULMETHOD	GetBHAInfo(std::vector<CString>& vecBHAInfo) = 0;
	ULMETHOD	GetAllBHAInfo(std::vector<TS_BHAINFOMTB>& vecBHAInfo, LPCTSTR lpszBHAID) = 0;
	ULMETHOD	GetBHATableInfo(std::vector<CString>& vecBHAInfo) = 0;
	ULMETHOD	QueryMeasPointFromTool(LPCTSTR lpszToolID, std::vector<TS_MEASPOINTINFOTB>& vecMeasPoint) = 0;
	ULMETHOD	SetGammaSub(int iGSID, TS_GAMMASUB* pgGAMMASUB) = 0;
	ULMETHOD	BackUpDataBase(LPCTSTR lpszDataBaseName, LPCTSTR strPathFileName) = 0;
	ULMETHOD	RestoreDataBase(LPCTSTR lpszDataBaseName, LPCTSTR strPathFileName) = 0;
	ULMETHOD	CreateDatabaseEx(LPCTSTR database) = 0 ;
	ULMETHOD	QueryGamaCorrPara(std::vector<MS_GAMACORR_PARA>& vecGamaCorrPara, LPCTSTR lpszRunID = NULL) = 0;

	ULMETHOD    GetToolFromDB(CArray<CURVETOOLINFOTB, CURVETOOLINFOTB&>* pToolInfo) = 0;
	ULMETHOD    GetCapiFromDB(CArray<BHAINFODTB, BHAINFODTB&>* pCapiInfo) = 0;
	ULMETHOD    GetPositionFromDB(CArray<POSITIONS, POSITIONS&>* pPositionInfo) = 0;
	ULMETHOD	AddGuid(char *pID) = 0;
	//2020.7.11 very1.6.0 Bug[76] start
	ULMETHOD    GetDBDepth(CString strDbName, CString strHoleID, std::vector<CAL_VDEPTH>& vecDepth, INT64 nStartDepth = -10000000, INT64 nEndDepth = 310000000) = 0;
	ULMETHOD	SetDBDepth(CString strDbName, CAL_VDEPTH *vecDepth) = 0;
	//2020.7.11 very1.6.0 Bug[76] end
	ULMETHOD	GetGuid(int nIndex , char *pID) = 0;

	//2020.7.14 ver1.6.0 bug[102] start
	ULMETHOD	GetPressureVDepth(std::vector<CAL_ECD>& verECD) = 0;
	ULMETHOD    SetECD(CAL_ECD* ecd) = 0;
	//2020.7.14 ver1.6.0 bug[102] end

	ULMETHOD	ExistsRun(LPTS_RUNINFOTB pgRUNINFOTB, BOOL& bRet) = 0;
	ULMETHOD	GetBHACount(LPCTSTR lpszBHAID, int& nCount) = 0;
	ULMETHOD    GetBHAINFODTBList(LPCTSTR lpszBHAID, std::vector<TS_BHAINFODTB>& vecBhainfodtb) = 0;
	ULMETHOD    QueryBHADMeasPointD(LPCTSTR lpszBHAID,std::vector<TS_BHADMEASPOINTD>& vecBhatb) = 0;
	////2020.7.22 dyl start 获取数据库名字
	//ULMVOID GetDbName(CString& strDBName) = 0;
	//ULMVOID GetDBServer(CString& strDBService) = 0;
	////2020.7.22 dyl end 获取数据库名字
	ULMETHOD    QueryBHACURVED(LPCTSTR lpszBHAID, std::vector<TS_BHACURVED>& vecBhatb) = 0;
	ULMETHOD    QueryTimeDepth(LPCTSTR lpszRUNID, std::list<TS_TIMEDEPTHTB>& vecTB) = 0;
	ULMETHOD    QueryRT_ROPDataTB(LPCTSTR lpszRUNID, std::list<TS_RT_ROPDATATB>& vecTB) = 0;
	ULMETHOD    QueryRT_MWDDataTB(LPCTSTR lpszRUNID, std::list<TS_RT_MWDDATATB>& vecTB) = 0;
	ULMETHOD    QueryRT_GRDataTB(LPCTSTR lpszRUNID, std::list<TS_RT_GRDATATB>& vecTB) = 0;
	ULMETHOD    QueryRT_AZIGRDataTB(LPCTSTR lpszRUNID, std::list<TS_RT_AZIGRDATATB>& vecTB) = 0;
	ULMETHOD    QueryRT_ResistivityDataTB(LPCTSTR lpszRUNID, std::list<TS_RT_RESISTIVITYDATATB>& vecTB) = 0;
	ULMETHOD    QueryRT_AZIResDataTB(LPCTSTR lpszRUNID, std::list<TS_RT_AZIRESDATATB>& vecTB) = 0;
	ULMETHOD    QueryRT_APDataTB(LPCTSTR lpszRUNID, std::list<TS_RT_APDATATB>& vecTB) = 0;
	ULMETHOD    QueryRT_CollarTempDataTB(LPCTSTR lpszRUNID, std::list<TS_RT_COLLARTEMPDATATB>& vecTB) = 0;
	ULMETHOD    QueryME_AziGammaTB(LPCTSTR lpszRUNID, std::list<TS_ME_AZIGAMMATB>& vecTB) = 0;
	ULMETHOD    QueryME_GammaTB(LPCTSTR lpszRUNID, std::list<TS_ME_GAMMATB>& vecTB) = 0;
	ULMETHOD    QueryME_AziResTB(LPCTSTR lpszRUNID, std::list<TS_ME_AZIRESTB>& vecTB) = 0;
	ULMETHOD    QueryME_ResistivityTB(LPCTSTR lpszRUNID, std::list<TS_ME_RESISTIVITYTB>& vecTB) = 0;
	ULMETHOD    QueryME_AziResGeoSig(LPCTSTR lpszRUNID, std::list<TS_ME_AZIRESGEOSIG>& vecTB) = 0;
	ULMETHOD    QueryTableColName(LPCTSTR lpszTableName, std::vector<CString>& vecColName) = 0;
	ULMETHOD    QueryProjectSimple(LPCTSTR lpProjectID, int& nCountProject) = 0;
	ULMETHOD    QueryRunInSimple(LPCTSTR lpszRUNID,int& nCountRuninfo) = 0;
	ULMETHOD    QueryHoleSimple(LPCTSTR lpszHoleID, int& nCount) = 0;

	//2020.8.6 ver1.6.0 bug[151] start
	ULMETHOD	DeleteAllSurvey(LPCTSTR strHoleID) = 0;
	//2020.8.6 ver1.6.0 bug[151] end
	ULMETHOD   QueryRtMeCount(LPCTSTR lpszRUNID, LONG& nCount) = 0;
	ULMETHOD   QueryWellSiteCount(LPCTSTR lpszID, LONG& nCount) = 0;

	ULMETHOD   QueryWellCount(LPCTSTR lpszID, LONG& nCount) = 0;
	ULMETHOD   CallbackDisconnectMsg(CallBackDisFunc pFunc) = 0;
	ULMETHOD   AddToolclass(const TS_TOOLCLASS* pToolClass) = 0;

	//2020.9.17 ver1.6.0 bug[183] start
	ULMETHOD  GetMeasurment(int ntype, std::vector<CALMEASUREMENT>& vecMeasurement, LPCSTR strWhere) = 0;
	ULMETHOD  UpdateMeasurment(int ntype, const std::vector<CALMEASUREMENT> updateMeasure) = 0;
	ULMETHOD  GetAziRes(int ntype, std::vector<CALAZIRES>& vecAziRes, LPCSTR strWhere) = 0;
	ULMETHOD  UpdateAziRes(int ntype, const std::vector<CALAZIRES> vecUpdateAziRes) = 0;
	ULMETHOD  GetECDM(std::vector<CALECDM>& vecAziRes, LPCSTR strWhere) = 0;
	ULMETHOD  UpdateECDM(const std::vector<CALECDM> vecUpdateAziRes) = 0;
	//2020.9.17 ver1.6.0 bug[183] end
	//2020.9.23 sys Start 单位
	ULMETHOD    UpdateViewItemUnitTBAll(const std::vector<TS_VIEWITEMUNITTB>& vecViewItemunit) = 0;
	//2020.9.23 sys End 单位
	ULMETHOD    QueryViewItemUnitTBExs(std::vector<TS_VIEWITEMUNITTB>& vecViewItemunit, const CString& strViewName) = 0;

	//2020.10.22 ver1.6.0 bug[193] start
	ULMETHOD  GetNearbySurveyData(const INT64 nDepth, SURVEY_INFO& startSurvey, SURVEY_INFO& endSurvey) = 0;
	//2020.10.22 ver1.6.0 bug[193] end

	ULMETHOD  GetAPECD(std::vector<CALECDM>& vecECDM) = 0;
	ULMETHOD  UpdateAPECD(const std::vector<CALECDM> vecUpdateECDM) = 0;
	
	ULMETHOD  AddTMAprDataTB(const TS_ME_APR* pTmApr) = 0;
	ULMETHOD  GetECDMs(std::vector<CALECDM>& vecAziRes, LPCSTR strWhere = "") = 0;

	//2020.11.2 bug[229] start
	ULMETHOD  GetUnitInfo(std::vector< CHOOSEUNIT>& vecUnit) = 0;
	ULMETHOD  GetUnitByName(const CString strCurveName, CString& strUName) = 0;
	//2020.11.2 bug[229] end

	//2020.11.4 ver1.6.0 bug[236] start
	ULMETHOD  GetToolAziRes(std::vector<TOOLAIZRES>& vecAizRes) = 0;
	ULMETHOD  SetToolAziRes(const TOOLAIZRES toolAizRes) = 0;
	ULMETHOD  InsertToolAziRes(const TOOLAIZRES toolAizRes) = 0;
	ULMETHOD  DeleteToolAziRes(const TOOLAIZRES toolAizRes) = 0;
	//2020.11.4 ver1.6.0 bug[236] end

	//2020.11.2 ver1.60 bug[262] start
	ULMETHOD  AddAziGammaGroup(std::vector <TS_ME_AZIGAMMATB> vecMeAziGama) = 0;
	ULMETHOD  AddTMApRDataTBGroup(std::vector<TS_ME_APR> vecSaveApr) = 0;
	ULMETHOD  AddResisGroup(std::vector<TS_ME_RESISTIVITYTB> vecMeResis) = 0;
	ULMETHOD  AddAZIResGroup(std::vector<TS_ME_AZIRESTB> vecMeAZIRes) = 0;
	ULMETHOD  AddGammaGroup(std::vector<TS_ME_GAMMATB> vecMeGamma) = 0;
	//2020.11.2 ver1.60 bug[262] end

	ULMETHOD  GetReLog(std::vector<RELOG_GRID>& vecReLog) = 0;
	ULMETHOD  InsertReLog(const TS_RELOG reLog) = 0;
	ULMETHOD  UpdateReLog(const TS_RELOG reLog) = 0;
	ULMETHOD  DeleteReLog(const TS_RELOG reLog) = 0;
	ULMETHOD  GetActiveRunName(CString& strRunName) = 0;

	ULMETHOD  GetNowDepth(long& nBitDepth) = 0;

	ULMETHOD  GetMeasurments(int ntype, std::vector<CALMEASUREMENT>& vecMeasurement, LPCTSTR strRunID, float m_fDepth, float m_fEndDepth, LPCSTR strWhere) = 0;
	ULMETHOD  GetAziRess(int ntype, std::vector<CALAZIRES>& vecAziRes, LPCTSTR strRunID, float m_fDepth, float m_fEndDepth, LPCSTR strWhere) = 0;
	ULMETHOD  GetECDMTwo(std::vector<CALECDM>& vecAziRes, LPCTSTR strRunID, float m_fDepth, float m_fEndDepth, LPCSTR strWhere) = 0;
	ULMETHOD  GetDBDepths(CString strDbName, CString strHoleID, std::vector<CAL_VDEPTH>& vecDepth, INT64 nStartDepth, INT64 nEndDepth, LPCTSTR strRunID) = 0;

	//2020.12.31 ver1.6.0 bug[307] start
	ULMETHOD  GetGRTCalcMode(int& nGRTCalcMode) = 0;
	ULMETHOD  SetGRTCalcMode(int nGRTCalcMode, CString strRun, BOOL bNull) = 0;
	//2020.12.31 ver1.6.0 bug[307] end

	//2020.12.31 ver1.6.0 bug[308] start
	ULMETHOD  SetEffiectMWD(const TS_RT_MWDDATATB MWDData) = 0;
	ULMETHOD  GetSurveyMWD(std::vector<TS_RT_MWDDATATB>& vecMWD, const float fStartDep, const float fEndDep) = 0;
	//2020.12.31 ver1.6.0 bug[308] end
	// 测斜评价参数设置表
	ULMETHOD	SaveSurveyCheckPara(LPTS_SURVEYCHECKPARA pInfo) = 0;
	ULMETHOD	ReadSurveyCheckPara(LPTS_SURVEYCHECKPARA pInfo) = 0;
	ULMETHOD	SaveCA_SurveryData(LPTS_CA_SURVEYDATA pInfo) = 0;
	ULMETHOD	GetCA_SurveryData(CDBFilterCurveInfo* pInfo, std::vector<TS_CA_SURVEYDATA>& vecCA_SURVEYDATA) = 0;
	ULMETHOD	GetDepthSysData(CDBFilterCurveInfo* pInfo, std::vector<MS_DEPTHSYSDATA>& vecMS_DEPTHSYSDATA) = 0;

	ULMETHOD	SaveRT_GenTime(LPTS_RT_GENTIME pInfo) = 0;
	ULMETHOD	SaveRT_Custom(LPTS_RT_CUSTOM pInfo) = 0;
	ULMETHOD	SaveSU_Custom(LPTS_SU_CUSTOM pInfo) = 0;

	ULMETHOD  Get_NGR(int ntype, std::vector<TS_ME_GAMMATB>& vecNGR, LPCTSTR strRunID, float m_fDepth, float m_fEndDepth) = 0;
	ULMETHOD  Update_NGR(int ntype, const std::vector<TS_ME_GAMMATB> vecUpdateNGR) = 0;

	ULMETHOD  Get_NGR_RT(int ntype, std::vector<TS_RT_GRDATATB>& vecNGR, LPCTSTR strRunID, float m_fDepth, float m_fEndDepth) = 0;
	ULMETHOD  Update_NGR_RT(int ntype, const std::vector<TS_ME_GAMMATB> vecUpdateNGR) = 0;

	ULMETHOD  Get_RES_ME(int ntype, std::vector<TS_ME_RESISTIVITYTB>& vecRES, LPCTSTR strRunID, float m_fDepth, float m_fEndDepth) = 0;
	ULMETHOD  Get_RES_RT(int ntype, std::vector<TS_RT_RESISTIVITYDATATB>& vecRES, LPCTSTR strRunID, float m_fDepth, float m_fEndDepth) = 0;

	ULMETHOD  GetRtResCoefficient(LPCTSTR strTableName, float fboreholeSize, float fmudResistivity, float fToolSize, std::vector <CTF>& m_vecCTF) = 0;
	ULMETHOD  Update_RES_ME_DB(int ntype, const std::vector<TS_ME_RESISTIVITYTB> vecUpdataRTNGR) = 0;
	ULMETHOD  Update_RES_RT(LPCTSTR lpszCurveName, int iWitsID, float fValue, long lDepth, long lTime, WORD wMilliseconds = 0) = 0;
	ULMETHOD  Get_ResCalcPara(std::vector<TS_RESCALCPARA>& vecRESCalcPara, LPCTSTR strRunID) = 0;
	ULMETHOD  Update_ResCalcPara(LPCTSTR lpszRunID, TS_RESCALCPARA& gRESCalcPara) = 0;
	ULMETHOD  Delete_T_ME_AziResTB() = 0;
	ULMETHOD  Create_T_ME_AziResTB() = 0;
	ULMETHOD  ExecSQL(LPCTSTR strSQL) = 0;
	ULMETHOD  GetShockInfo(CString& strShockInfo) = 0;	

	// 2022.11.28 Start
	ULMETHOD  GetPressureCalibration(std::vector<TS_PRESSURECALIBRATION>& vecPressureCalibration) = 0;
	ULMETHOD  GetPressureCalibration(std::vector<PRESSURE_CALIBRATION>& vecPressureCalibration) = 0;
	ULMETHOD  SetPressureCalibration(const TS_PRESSURECALIBRATION pressureCalibration) = 0;
	ULMETHOD  InsertPressureCalibration(const TS_PRESSURECALIBRATION pressureCalibration) = 0;
	ULMETHOD  DeletePressureCalibration(const TS_PRESSURECALIBRATION pressureCalibration) = 0;
	// 2022.11.28 End
	// 2022.12.11 Start
	ULMETHOD  GetRT_AZIGRDataTB(CDBFilterCurveInfo* pInfo, std::vector<TS_RT_AZIGRDATATB>& vecRT_AZIGRDATA) = 0;
	ULMETHOD  GetME_AziGammaTB(CDBFilterCurveInfo* pInfo, std::vector<TS_ME_AZIGAMMATB>& vecME_AZIGAMMATB) = 0;
	ULMETHOD  UpdateMAziGamma(const TS_ME_AZIGAMMATB* pMeAZIGamma) = 0;
	ULMETHOD  GetRT_ResistivityDataTB(CDBFilterCurveInfo* pInfo, std::vector<TS_RT_RESISTIVITYDATATB>& vecTS_RT_RESISTIVITYDATATB) = 0;
	ULMETHOD  GetME_ResistivityTB(CDBFilterCurveInfo* pInfo, std::vector<TS_ME_RESISTIVITYTB>& vecTS_ME_RESISTIVITYTB) = 0;
	ULMETHOD  Update_GR(int ntype, const std::vector<TS_ME_AZIGAMMATB> vecUpdateGR) = 0;
	ULMETHOD  Update_Res(int ntype, const std::vector<TS_ME_RESISTIVITYTB> vecUpdateRes) = 0;
	ULMETHOD  UpdateMGamma(const std::vector<TS_ME_GAMMATB> vecUpdateGR) = 0;
	// 2022.12.11 End
	ULMETHOD IsContainAziExpress(LPCSTR lpszRunID) = 0;
	ULMETHOD IsContainCTF(LPCSTR lpszRunID) = 0;
	ULMETHOD IsContainRGS(LPCSTR lpszRunID) = 0;
	ULMETHOD IsContainNBGRSub(LPCSTR lpszRunID) = 0;

	ULMETHOD GetAllBHA(LPCTSTR lpszBHAID, std::vector<TS_BHAINFOMTB>& vecBHAINFOMTB) = 0;
	ULMETHOD SaveRT_NBSub(LPTS_RT_NBSUB pInfo) = 0;
// 2023.12.07 Start
	ULMETHOD  GetRestoreInfo(std::vector<TS_RESTOREINFO>& vecRESTOREINFO) = 0;
// 2023.12.07 End

	ULMETHOD  SaveReLog(const TS_RELOG reLog) = 0;
	ULMETHOD  QueryCurves(LPCTSTR strConditions, std::vector<MS_CURVEINFOTB>& vecCurve) = 0;
	ULMETHOD  DeleteRT_CurveFilter1(LPCTSTR lpszCurveName) = 0;
	ULMETHOD  GetUnitFack(CString strCurveName,CString strUnitType,CString strUnitName,float& fARCk) = 0;
	ULMETHOD  SaveLASData(CString strTableName,std::vector<RTDataInfo> vecRTData) = 0;
	ULMETHOD  GetLASData(LPCTSTR strCurveName,LPCTSTR strTableName,std::vector<MS_CURVEVALUE>& CurveValue) = 0;
	ULMETHOD  UpdateCurveInfo(TS_CURVEINFOTB &gCurveInfo) = 0;
	ULMETHOD  UpdateME_AziGammaTB(LPCTSTR strSQL) = 0;
};
//接口ID   

// {B9A24C5E-E462-4682-8B54-EB37CD01D124}
static const IID IID_IDB= 
{ 0xb9a24c5e, 0xe462, 0x4682, { 0x8b, 0x54, 0xeb, 0x37, 0xcd, 0x1, 0xd1, 0x24 } };


extern "C" DWORD ULGetVersion();
extern "C" IUnknown* CreateDB();
extern "C" void DestoryDB(IUnknown * pI);
#endif
//---------------------------------------------------------------------------//
// 文件名: WellSiteInfoTB.h
// 说明:	井场信息表（井场）
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020/10/19 22:11:35
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TWellSiteInfoTB.h : DWellSiteInfoTB

#ifndef	_TWELLSITEINFOTB_H
#define	_TWELLSITEINFOTB_H

#define	TID_WELLSITEINFOTB								_T("WellSiteInfoTB")
#define	OID_WELLSITEINFOTB								_T("")

// Sort No
#define	SORT_WELLSITEINFOTB_PK0				0							// PK:井场编号
//#define	SORT_WELLSITEINFOTB_A1							%#%							// A1:

// Colum No
#define	COL_WELLSITEINFOTB_WELLSITEID					(short)0						// 井场编号
#define	COL_WELLSITEINFOTB_PROID					(short)1						// 工程ID 使用全球唯一字符串UUID
#define	COL_WELLSITEINFOTB_WELLSITENAME					(short)2						// 井场名称
#define	COL_WELLSITEINFOTB_COUNTRY					(short)3						// 国家
#define	COL_WELLSITEINFOTB_PROVINCE					(short)4						// 省
#define	COL_WELLSITEINFOTB_COUNTY					(short)5						// 县
#define	COL_WELLSITEINFOTB_REGION					(short)6						// 地区
#define	COL_WELLSITEINFOTB_FIELD					(short)7						// 油田
#define	COL_WELLSITEINFOTB_WELLLOCATION					(short)8						// 井位置
#define	COL_WELLSITEINFOTB_DRILLCOMPANY					(short)9						// 钻井公司
#define	COL_WELLSITEINFOTB_RIG					(short)10						// 钻机
#define	COL_WELLSITEINFOTB_CREATETIME					(short)11						// 创建时间戳
#define	COL_WELLSITEINFOTB_STATUS					(short)12						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_WELLSITEINFOTB_MEMO					(short)13						// 备注
#define	COL_WELLSITEINFOTB_UPDCOUNT					(short)14						// 更新计数

// Colum(Field) Name
#define	FLD_WELLSITEINFOTB_WELLSITEID					_T("WellSiteID")					// 井场编号
#define	FLD_WELLSITEINFOTB_PROID					_T("ProID")					// 工程ID 使用全球唯一字符串UUID
#define	FLD_WELLSITEINFOTB_WELLSITENAME					_T("WellSiteName")					// 井场名称
#define	FLD_WELLSITEINFOTB_COUNTRY					_T("Country")					// 国家
#define	FLD_WELLSITEINFOTB_PROVINCE					_T("Province")					// 省
#define	FLD_WELLSITEINFOTB_COUNTY					_T("County")					// 县
#define	FLD_WELLSITEINFOTB_REGION					_T("Region")					// 地区
#define	FLD_WELLSITEINFOTB_FIELD					_T("Field")					// 油田
#define	FLD_WELLSITEINFOTB_WELLLOCATION					_T("WellLocation")					// 井位置
#define	FLD_WELLSITEINFOTB_DRILLCOMPANY					_T("DrillCompany")					// 钻井公司
#define	FLD_WELLSITEINFOTB_RIG					_T("Rig")					// 钻机
#define	FLD_WELLSITEINFOTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_WELLSITEINFOTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_WELLSITEINFOTB_MEMO					_T("Memo")					// 备注
#define	FLD_WELLSITEINFOTB_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_WELLSITEINFOTB_WELLSITEID_DIGITS				50					// 井场编号位数
#define	TV_WELLSITEINFOTB_PROID_DIGITS				50					// 工程ID 使用全球唯一字符串UUID位数
#define	TV_WELLSITEINFOTB_WELLSITENAME_DIGITS				50					// 井场名称位数
#define	TV_WELLSITEINFOTB_COUNTRY_DIGITS				50					// 国家位数
#define	TV_WELLSITEINFOTB_PROVINCE_DIGITS				50					// 省位数
#define	TV_WELLSITEINFOTB_COUNTY_DIGITS				50					// 县位数
#define	TV_WELLSITEINFOTB_REGION_DIGITS				50					// 地区位数
#define	TV_WELLSITEINFOTB_FIELD_DIGITS				50					// 油田位数
#define	TV_WELLSITEINFOTB_WELLLOCATION_DIGITS				50					// 井位置位数
#define	TV_WELLSITEINFOTB_DRILLCOMPANY_DIGITS				50					// 钻井公司位数
#define	TV_WELLSITEINFOTB_RIG_DIGITS				50					// 钻机位数
#define	TV_WELLSITEINFOTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_WELLSITEINFOTB
{
 
  char	szWellSiteID[TV_WELLSITEINFOTB_WELLSITEID_DIGITS + 1];							// 井场编号
  char	szProID[TV_WELLSITEINFOTB_PROID_DIGITS + 1];							// 工程ID 使用全球唯一字符串UUID
  char	szWellSiteName[TV_WELLSITEINFOTB_WELLSITENAME_DIGITS + 1];							// 井场名称
  char	szCountry[TV_WELLSITEINFOTB_COUNTRY_DIGITS + 1];							// 国家
  char	szProvince[TV_WELLSITEINFOTB_PROVINCE_DIGITS + 1];							// 省
  char	szCounty[TV_WELLSITEINFOTB_COUNTY_DIGITS + 1];							// 县
  char	szRegion[TV_WELLSITEINFOTB_REGION_DIGITS + 1];							// 地区
  char	szField[TV_WELLSITEINFOTB_FIELD_DIGITS + 1];							// 油田
  char	szWellLocation[TV_WELLSITEINFOTB_WELLLOCATION_DIGITS + 1];							// 井位置
  char	szDrillCompany[TV_WELLSITEINFOTB_DRILLCOMPANY_DIGITS + 1];							// 钻井公司
  char	szRig[TV_WELLSITEINFOTB_RIG_DIGITS + 1];							// 钻机
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_WELLSITEINFOTB_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_WELLSITEINFOTB;

typedef	TS_WELLSITEINFOTB FAR*	LPTS_WELLSITEINFOTB;

#pragma	pack(pop)

#endif // _TWELLSITEINFOTB_H

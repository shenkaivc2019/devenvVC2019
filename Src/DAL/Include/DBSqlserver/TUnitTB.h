//---------------------------------------------------------------------------//
// 文件名: UnitTB.h
// 说明:	单位信息表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020/10/19 22:11:35
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TUnitTB.h : DUnitTB

#ifndef	_TUNITTB_H
#define	_TUNITTB_H

#define	TID_UNITTB								_T("UnitTB")
#define	OID_UNITTB								_T("")

// Sort No
#define	SORT_UNITTB_PK0				0							// PK:单位ID，单位唯一标识
//#define	SORT_UNITTB_A1							%#%							// A1:

// Colum No
#define	COL_UNITTB_UID					(short)0						// 单位ID，单位唯一标识
#define	COL_UNITTB_UNAME					(short)1						// 单位名称
#define	COL_UNITTB_UNAME_CN					(short)2						// 中文单位名称
#define	COL_UNITTB_UALIAS					(short)3						// 单位别名
#define	COL_UNITTB_UTYPE					(short)4						// 单位类型
#define	COL_UNITTB_UTYPE_CN					(short)5						// 中文单位类型
#define	COL_UNITTB_UFORMAT					(short)6						// 单位系统:System|1.数据库;Mets|2.公制;Eng|4.英制;Custom|0.自定义单位
#define	COL_UNITTB_UK					(short)7						// 转换系数正向计算 乘校正
#define	COL_UNITTB_UB					(short)8						// 转换系数正向计算 加校正
#define	COL_UNITTB_ARCK					(short)9						// 转换系数反射计算 乘校正
#define	COL_UNITTB_ARCB					(short)10						// 转换系数反射计算 加校正
#define	COL_UNITTB_CREATETIME					(short)11						// 创建时间戳
#define	COL_UNITTB_STATUS					(short)12						// 状态:Deleted|0.删除;InUse|1.使用中
#define	COL_UNITTB_MEMO					(short)13						// 备注
#define	COL_UNITTB_UPDCOUNT					(short)14						// 更新计数

// Colum(Field) Name
#define	FLD_UNITTB_UID					_T("UID")					// 单位ID，单位唯一标识
#define	FLD_UNITTB_UNAME					_T("UName")					// 单位名称
#define	FLD_UNITTB_UNAME_CN					_T("UName_CN")					// 中文单位名称
#define	FLD_UNITTB_UALIAS					_T("UAlias")					// 单位别名
#define	FLD_UNITTB_UTYPE					_T("UType")					// 单位类型
#define	FLD_UNITTB_UTYPE_CN					_T("UType_CN")					// 中文单位类型
#define	FLD_UNITTB_UFORMAT					_T("UFormat")					// 单位系统:System|1.数据库;Mets|2.公制;Eng|4.英制;Custom|0.自定义单位
#define	FLD_UNITTB_UK					_T("Uk")					// 转换系数正向计算 乘校正
#define	FLD_UNITTB_UB					_T("Ub")					// 转换系数正向计算 加校正
#define	FLD_UNITTB_ARCK					_T("ARCk")					// 转换系数反射计算 乘校正
#define	FLD_UNITTB_ARCB					_T("ARCb")					// 转换系数反射计算 加校正
#define	FLD_UNITTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_UNITTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中
#define	FLD_UNITTB_MEMO					_T("Memo")					// 备注
#define	FLD_UNITTB_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_UNITTB_UNAME_DIGITS				50					// 单位名称位数
#define	TV_UNITTB_UNAME_CN_DIGITS				50					// 中文单位名称位数
#define	TV_UNITTB_UALIAS_DIGITS				50					// 单位别名位数
#define	TV_UNITTB_UTYPE_DIGITS				50					// 单位类型位数
#define	TV_UNITTB_UTYPE_CN_DIGITS				50					// 中文单位类型位数
#define	TV_UNITTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_UNITTB
{
 
  int	iUID;							// 单位ID，单位唯一标识
  char	szUName[TV_UNITTB_UNAME_DIGITS + 1];							// 单位名称
  char	szUName_CN[TV_UNITTB_UNAME_CN_DIGITS + 1];							// 中文单位名称
  char	szUAlias[TV_UNITTB_UALIAS_DIGITS + 1];							// 单位别名
  char	szUType[TV_UNITTB_UTYPE_DIGITS + 1];							// 单位类型
  char	szUType_CN[TV_UNITTB_UTYPE_CN_DIGITS + 1];							// 中文单位类型
  int	iUFormat;							// 单位系统:System|1.数据库;Mets|2.公制;Eng|4.英制;Custom|0.自定义单位
  float	fUk;							// 转换系数正向计算 乘校正
  float	fUb;							// 转换系数正向计算 加校正
  float	fARCk;							// 转换系数反射计算 乘校正
  float	fARCb;							// 转换系数反射计算 加校正
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中
  char	szMemo[TV_UNITTB_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_UNITTB;

typedef	TS_UNITTB FAR*	LPTS_UNITTB;

#pragma	pack(pop)

#endif // _TUNITTB_H

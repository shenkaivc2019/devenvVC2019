//---------------------------------------------------------------------------//
// 文件名: CurveClass.h
// 说明:	曲线分类表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020/10/19 22:11:35
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TCurveClass.h : DCurveClass

#ifndef	_TCURVECLASS_H
#define	_TCURVECLASS_H

#define	TID_CURVECLASS								_T("CurveClass")
#define	OID_CURVECLASS								_T("")

// Sort No
#define	SORT_CURVECLASS_PK0				0							// PK:曲线分类编号
//#define	SORT_CURVECLASS_A1							%#%							// A1:

// Colum No
#define	COL_CURVECLASS_CCID					(short)0						// 曲线分类编号
#define	COL_CURVECLASS_CCNAMECN					(short)1						// 曲线分类名称
#define	COL_CURVECLASS_CCNAMEEN					(short)2						// 曲线分类名称
#define	COL_CURVECLASS_FCCID					(short)3						// 曲线分类父亲编号
#define	COL_CURVECLASS_CCORDER					(short)4						// 排序
#define	COL_CURVECLASS_GRADE					(short)5						// 级次
#define	COL_CURVECLASS_IMGPATH					(short)6						// 曲线分类图片
#define	COL_CURVECLASS_CREATETIME					(short)7						// 创建时间戳
#define	COL_CURVECLASS_STATUS					(short)8						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_CURVECLASS_MEMO					(short)9						// 备注
#define	COL_CURVECLASS_UPDCOUNT					(short)10						// 更新计数

// Colum(Field) Name
#define	FLD_CURVECLASS_CCID					_T("CCID")					// 曲线分类编号
#define	FLD_CURVECLASS_CCNAMECN					_T("CCNameCN")					// 曲线分类名称
#define	FLD_CURVECLASS_CCNAMEEN					_T("CCNameEN")					// 曲线分类名称
#define	FLD_CURVECLASS_FCCID					_T("FCCID")					// 曲线分类父亲编号
#define	FLD_CURVECLASS_CCORDER					_T("CCOrder")					// 排序
#define	FLD_CURVECLASS_GRADE					_T("Grade")					// 级次
#define	FLD_CURVECLASS_IMGPATH					_T("ImgPath")					// 曲线分类图片
#define	FLD_CURVECLASS_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_CURVECLASS_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_CURVECLASS_MEMO					_T("Memo")					// 备注
#define	FLD_CURVECLASS_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_CURVECLASS_CCID_DIGITS				10					// 曲线分类编号位数
#define	TV_CURVECLASS_CCNAMECN_DIGITS				50					// 曲线分类名称位数
#define	TV_CURVECLASS_CCNAMEEN_DIGITS				50					// 曲线分类名称位数
#define	TV_CURVECLASS_FCCID_DIGITS				10					// 曲线分类父亲编号位数
#define	TV_CURVECLASS_IMGPATH_DIGITS				255					// 曲线分类图片位数
#define	TV_CURVECLASS_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_CURVECLASS
{
 
  char	szCCID[TV_CURVECLASS_CCID_DIGITS + 1];							// 曲线分类编号
  char	szCCNameCN[TV_CURVECLASS_CCNAMECN_DIGITS + 1];							// 曲线分类名称
  char	szCCNameEN[TV_CURVECLASS_CCNAMEEN_DIGITS + 1];							// 曲线分类名称
  char	szFCCID[TV_CURVECLASS_FCCID_DIGITS + 1];							// 曲线分类父亲编号
  int	iCCOrder;							// 排序
  int	iGrade;							// 级次
  char	szImgPath[TV_CURVECLASS_IMGPATH_DIGITS + 1];							// 曲线分类图片
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_CURVECLASS_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_CURVECLASS;

typedef	TS_CURVECLASS FAR*	LPTS_CURVECLASS;

#pragma	pack(pop)

#endif // _TCURVECLASS_H

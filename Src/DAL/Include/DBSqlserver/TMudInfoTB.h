//---------------------------------------------------------------------------//
// 文件名: MudInfoTB.h
// 说明:	泥浆信息表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020/10/19 22:11:35
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TMudInfoTB.h : DMudInfoTB

#ifndef	_TMUDINFOTB_H
#define	_TMUDINFOTB_H

#define	TID_MUDINFOTB								_T("MudInfoTB")
#define	OID_MUDINFOTB								_T("")

// Sort No
#define	SORT_MUDINFOTB_PK0				0							// PK:泥浆编号 自增字段
//#define	SORT_MUDINFOTB_A1							%#%							// A1:

// Colum No
#define	COL_MUDINFOTB_MUDID					(short)0						// 泥浆编号 自增字段
#define	COL_MUDINFOTB_MUDNAME					(short)1						// 泥浆名称
#define	COL_MUDINFOTB_HOLEID					(short)2						// 井眼编号
#define	COL_MUDINFOTB_SDATETIME					(short)3						// 开始使用（采样）时间(精确到ms) 
#define	COL_MUDINFOTB_EDATETIME					(short)4						// 结束使用时间(精确到ms) 第一次填写时因不知道结束时间 此时为空 当填写下一条记录时 开始时间填写上一条记录的结束时间
#define	COL_MUDINFOTB_MUDTYPE					(short)5						// 泥浆类型:light|0.light;hematite|1.hematite;barite|2.barite
#define	COL_MUDINFOTB_DENSITY					(short)6						// 密度
#define	COL_MUDINFOTB_MUDRESISTIVITY					(short)7						// 泥浆电阻率
#define	COL_MUDINFOTB_PITTEMPERATURE					(short)8						// 泥浆池温度
#define	COL_MUDINFOTB_PERCENTAGEOFKM					(short)9						// 氯化钾KCl百分比
#define	COL_MUDINFOTB_PERCENTAGEOFK2CO3					(short)10						// 碳酸钾百分比（这个值以后用查表法计算参与电阻率的计算）
#define	COL_MUDINFOTB_DOWNHOLETEMPERATURE					(short)11						// 井下温度
#define	COL_MUDINFOTB_CREATETIME					(short)12						// 创建时间戳
#define	COL_MUDINFOTB_STATUS					(short)13						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_MUDINFOTB_MEMO					(short)14						// 备注
#define	COL_MUDINFOTB_UPDCOUNT					(short)15						// 更新计数

// Colum(Field) Name
#define	FLD_MUDINFOTB_MUDID					_T("MudID")					// 泥浆编号 自增字段
#define	FLD_MUDINFOTB_MUDNAME					_T("MudName")					// 泥浆名称
#define	FLD_MUDINFOTB_HOLEID					_T("HoleID")					// 井眼编号
#define	FLD_MUDINFOTB_SDATETIME					_T("SDateTime")					// 开始使用（采样）时间(精确到ms) 
#define	FLD_MUDINFOTB_EDATETIME					_T("EDateTime")					// 结束使用时间(精确到ms) 第一次填写时因不知道结束时间 此时为空 当填写下一条记录时 开始时间填写上一条记录的结束时间
#define	FLD_MUDINFOTB_MUDTYPE					_T("MudType")					// 泥浆类型:light|0.light;hematite|1.hematite;barite|2.barite
#define	FLD_MUDINFOTB_DENSITY					_T("Density")					// 密度
#define	FLD_MUDINFOTB_MUDRESISTIVITY					_T("MudResistivity")					// 泥浆电阻率
#define	FLD_MUDINFOTB_PITTEMPERATURE					_T("PitTemperature")					// 泥浆池温度
#define	FLD_MUDINFOTB_PERCENTAGEOFKM					_T("PercentageOfKm")					// 氯化钾KCl百分比
#define	FLD_MUDINFOTB_PERCENTAGEOFK2CO3					_T("PercentageOfK2CO3")					// 碳酸钾百分比（这个值以后用查表法计算参与电阻率的计算）
#define	FLD_MUDINFOTB_DOWNHOLETEMPERATURE					_T("DownHoleTemperature")					// 井下温度
#define	FLD_MUDINFOTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_MUDINFOTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_MUDINFOTB_MEMO					_T("Memo")					// 备注
#define	FLD_MUDINFOTB_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_MUDINFOTB_MUDNAME_DIGITS				50					// 泥浆名称位数
#define	TV_MUDINFOTB_HOLEID_DIGITS				50					// 井眼编号位数
#define	TV_MUDINFOTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_MUDINFOTB
{
 
  int	iMudID;							// 泥浆编号 自增字段
  char	szMudName[TV_MUDINFOTB_MUDNAME_DIGITS + 1];							// 泥浆名称
  char	szHoleID[TV_MUDINFOTB_HOLEID_DIGITS + 1];							// 井眼编号
  time_t	lSDateTime;							// 开始使用（采样）时间(精确到ms) 
  time_t	lEDateTime;							// 结束使用时间(精确到ms) 第一次填写时因不知道结束时间 此时为空 当填写下一条记录时 开始时间填写上一条记录的结束时间
  int	iMudType;							// 泥浆类型:light|0.light;hematite|1.hematite;barite|2.barite
  double	dDensity;							// 密度
  double	dMudResistivity;							// 泥浆电阻率
  double	dPitTemperature;							// 泥浆池温度
  double	dPercentageOfKm;							// 氯化钾KCl百分比
  double	dPercentageOfK2CO3;							// 碳酸钾百分比（这个值以后用查表法计算参与电阻率的计算）
  double	dDownHoleTemperature;							// 井下温度
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_MUDINFOTB_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_MUDINFOTB;

typedef	TS_MUDINFOTB FAR*	LPTS_MUDINFOTB;

#pragma	pack(pop)

#endif // _TMUDINFOTB_H

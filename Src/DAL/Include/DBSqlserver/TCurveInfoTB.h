//---------------------------------------------------------------------------//
// 文件名: CurveInfoTB.h
// 说明:	曲线信息表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2021/9/15 19:16:01
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TCurveInfoTB.h : DCurveInfoTB

#ifndef	_TCURVEINFOTB_H
#define	_TCURVEINFOTB_H

#define	TID_CURVEINFOTB								_T("CurveInfoTB")
#define	OID_CURVEINFOTB								_T("")

// Sort No
#define	SORT_CURVEINFOTB_PK1				1							// PK:曲线名称
//#define	SORT_CURVEINFOTB_A1							%#%							// A1:

// Colum No
#define	COL_CURVEINFOTB_CURVID					(short)0						// 曲线编号(WITS表对应字段)
#define	COL_CURVEINFOTB_CURVENAME					(short)1						// 曲线名称
#define	COL_CURVEINFOTB_CCID					(short)2						// 曲线分类编号
#define	COL_CURVEINFOTB_WITSID					(short)3						// WITS表对应字段
#define	COL_CURVEINFOTB_TCID					(short)4						// 工具分类编号
#define	COL_CURVEINFOTB_TOOLID					(short)5						// 工具编号
#define	COL_CURVEINFOTB_MPID					(short)6						// 测量点编号
#define	COL_CURVEINFOTB_UTYPE					(short)7						// 曲线单位类型 对应UnitTB表的UType字段 创建时选择输入
#define	COL_CURVEINFOTB_UID					(short)8						// 数据库中保存的单位ID，如果界面没有设置显示单位，则作为缺省界面的显示单位
#define	COL_CURVEINFOTB_SENSORTOBIT					(short)9						// 深度偏移(该曲线测点距离钻头的距离) 该值自动计算=MeasPointInfoTB.SensorToolToBot + BHAInfoDTB.ToolBotToBit
#define	COL_CURVEINFOTB_CURVEMIN					(short)10						// 最小值
#define	COL_CURVEINFOTB_CURVEMAX					(short)11						// 最大值
#define	COL_CURVEINFOTB_WARNMIN					(short)12						// 预警最小值 显示共色线
#define	COL_CURVEINFOTB_WARNMAX					(short)13						// 预警最大值 显示共色线
#define	COL_CURVEINFOTB_ERRORMIN					(short)14						// 错误最小值 显示红色线
#define	COL_CURVEINFOTB_ERRORMAX					(short)15						// 错误最大值 显示红色线
#define	COL_CURVEINFOTB_INVPOINTVAL					(short)16						// 无效点值
#define	COL_CURVEINFOTB_FILTER					(short)17						// 滤波
#define	COL_CURVEINFOTB_TABLENAME					(short)18						// 对应存储表名
#define	COL_CURVEINFOTB_FIELDNAME					(short)19						// 对应存储字段名
#define	COL_CURVEINFOTB_DATATYPE					(short)20						// WITS数据类型:A|65.字符型;L|73.LONG;S|79.S;F|128.浮点型
#define	COL_CURVEINFOTB_DIMENSION					(short)21						// 维数(常规曲线1，阵列曲线2)
#define	COL_CURVEINFOTB_POINTCOUNT					(short)22						// 点数(常规曲线1)
#define	COL_CURVEINFOTB_DESCRIPTION_EN					(short)23						// 参数英文描述
#define	COL_CURVEINFOTB_DESCRIPTION_CN					(short)24						// 参数中文描述
#define	COL_CURVEINFOTB_CURVENAMEM					(short)25						// 当前对应内存曲线名称
#define	COL_CURVEINFOTB_SORDER					(short)26						// 排序
#define	COL_CURVEINFOTB_ISCALC					(short)27						// 是否计算字段:NO|0.否;Yes|1.是
#define	COL_CURVEINFOTB_DATAFROM					(short)28						// 曲线数据来源:RunTime|0.实时;Mem|1.内存数据;Las|3.Las数据;
#define	COL_CURVEINFOTB_FIELDUSEDNO					(short)29						// 对应存储字段名No
#define	COL_CURVEINFOTB_FIELDNAME2					(short)30						// 对应存储字段名2
#define	COL_CURVEINFOTB_FIELDNAME3					(short)31						// 对应存储字段名3
#define	COL_CURVEINFOTB_FIELDNAME4					(short)32						// 对应存储字段名4
#define	COL_CURVEINFOTB_FIELDNAME5					(short)33						// 对应存储字段名5
#define	COL_CURVEINFOTB_CREATETIME					(short)34						// 创建时间戳
#define	COL_CURVEINFOTB_STATUS					(short)35						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_CURVEINFOTB_MEMO					(short)36						// 备注
#define	COL_CURVEINFOTB_UPDCOUNT					(short)37						// 更新计数

// Colum(Field) Name
#define	FLD_CURVEINFOTB_CURVID					_T("CurvID")					// 曲线编号(WITS表对应字段)
#define	FLD_CURVEINFOTB_CURVENAME					_T("CurveName")					// 曲线名称
#define	FLD_CURVEINFOTB_CCID					_T("CCID")					// 曲线分类编号
#define	FLD_CURVEINFOTB_WITSID					_T("WITSID")					// WITS表对应字段
#define	FLD_CURVEINFOTB_TCID					_T("TCID")					// 工具分类编号
#define	FLD_CURVEINFOTB_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_CURVEINFOTB_MPID					_T("MPID")					// 测量点编号
#define	FLD_CURVEINFOTB_UTYPE					_T("UType")					// 曲线单位类型 对应UnitTB表的UType字段 创建时选择输入
#define	FLD_CURVEINFOTB_UID					_T("UID")					// 数据库中保存的单位ID，如果界面没有设置显示单位，则作为缺省界面的显示单位
#define	FLD_CURVEINFOTB_SENSORTOBIT					_T("SensorToBit")					// 深度偏移(该曲线测点距离钻头的距离) 该值自动计算=MeasPointInfoTB.SensorToolToBot + BHAInfoDTB.ToolBotToBit
#define	FLD_CURVEINFOTB_CURVEMIN					_T("CurveMin")					// 最小值
#define	FLD_CURVEINFOTB_CURVEMAX					_T("CurveMax")					// 最大值
#define	FLD_CURVEINFOTB_WARNMIN					_T("WarnMin")					// 预警最小值 显示共色线
#define	FLD_CURVEINFOTB_WARNMAX					_T("WarnMax")					// 预警最大值 显示共色线
#define	FLD_CURVEINFOTB_ERRORMIN					_T("ErrorMin")					// 错误最小值 显示红色线
#define	FLD_CURVEINFOTB_ERRORMAX					_T("ErrorMax")					// 错误最大值 显示红色线
#define	FLD_CURVEINFOTB_INVPOINTVAL					_T("InvPointVal")					// 无效点值
#define	FLD_CURVEINFOTB_FILTER					_T("Filter")					// 滤波
#define	FLD_CURVEINFOTB_TABLENAME					_T("TableName")					// 对应存储表名
#define	FLD_CURVEINFOTB_FIELDNAME					_T("FieldName")					// 对应存储字段名
#define	FLD_CURVEINFOTB_DATATYPE					_T("DataType")					// WITS数据类型:A|65.字符型;L|73.LONG;S|79.S;F|128.浮点型
#define	FLD_CURVEINFOTB_DIMENSION					_T("Dimension")					// 维数(常规曲线1，阵列曲线2)
#define	FLD_CURVEINFOTB_POINTCOUNT					_T("PointCount")					// 点数(常规曲线1)
#define	FLD_CURVEINFOTB_DESCRIPTION_EN					_T("Description_en")					// 参数英文描述
#define	FLD_CURVEINFOTB_DESCRIPTION_CN					_T("Description_cn")					// 参数中文描述
#define	FLD_CURVEINFOTB_CURVENAMEM					_T("CurveNameM")					// 当前对应内存曲线名称
#define	FLD_CURVEINFOTB_SORDER					_T("SOrder")					// 排序
#define	FLD_CURVEINFOTB_ISCALC					_T("IsCalc")					// 是否计算字段:NO|0.否;Yes|1.是
#define	FLD_CURVEINFOTB_DATAFROM					_T("DataFrom")					// 曲线数据来源:RunTime|0.实时;Mem|1.内存数据;Las|3.Las数据;
#define	FLD_CURVEINFOTB_FIELDUSEDNO					_T("FieldUsedNo")					// 对应存储字段名No
#define	FLD_CURVEINFOTB_FIELDNAME2					_T("FieldName2")					// 对应存储字段名2
#define	FLD_CURVEINFOTB_FIELDNAME3					_T("FieldName3")					// 对应存储字段名3
#define	FLD_CURVEINFOTB_FIELDNAME4					_T("FieldName4")					// 对应存储字段名4
#define	FLD_CURVEINFOTB_FIELDNAME5					_T("FieldName5")					// 对应存储字段名5
#define	FLD_CURVEINFOTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_CURVEINFOTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_CURVEINFOTB_MEMO					_T("Memo")					// 备注
#define	FLD_CURVEINFOTB_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_CURVEINFOTB_CURVENAME_DIGITS				50					// 曲线名称位数
#define	TV_CURVEINFOTB_CCID_DIGITS				10					// 曲线分类编号位数
#define	TV_CURVEINFOTB_TCID_DIGITS				10					// 工具分类编号位数
#define	TV_CURVEINFOTB_UTYPE_DIGITS				50					// 曲线单位类型 对应UnitTB表的UType字段 创建时选择输入位数
#define	TV_CURVEINFOTB_FILTER_DIGITS				50					// 滤波位数
#define	TV_CURVEINFOTB_TABLENAME_DIGITS				50					// 对应存储表名位数
#define	TV_CURVEINFOTB_FIELDNAME_DIGITS				2000					// 对应存储字段名位数
#define	TV_CURVEINFOTB_DESCRIPTION_EN_DIGITS				200					// 参数英文描述位数
#define	TV_CURVEINFOTB_DESCRIPTION_CN_DIGITS				200					// 参数中文描述位数
#define	TV_CURVEINFOTB_CURVENAMEM_DIGITS				50					// 当前对应内存曲线名称位数
#define	TV_CURVEINFOTB_FIELDNAME2_DIGITS				2000					// 对应存储字段名2位数
#define	TV_CURVEINFOTB_FIELDNAME3_DIGITS				2000					// 对应存储字段名3位数
#define	TV_CURVEINFOTB_FIELDNAME4_DIGITS				2000					// 对应存储字段名4位数
#define	TV_CURVEINFOTB_FIELDNAME5_DIGITS				2000					// 对应存储字段名5位数
#define	TV_CURVEINFOTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_CURVEINFOTB
{
 
  int	iCurvID;							// 曲线编号(WITS表对应字段)
  char	szCurveName[TV_CURVEINFOTB_CURVENAME_DIGITS + 1];							// 曲线名称
  char	szCCID[TV_CURVEINFOTB_CCID_DIGITS + 1];							// 曲线分类编号
  int	iWITSID;							// WITS表对应字段
  char	szTCID[TV_CURVEINFOTB_TCID_DIGITS + 1];							// 工具分类编号
  int	iToolID;							// 工具编号
  int	iMPID;							// 测量点编号
  char	szUType[TV_CURVEINFOTB_UTYPE_DIGITS + 1];							// 曲线单位类型 对应UnitTB表的UType字段 创建时选择输入
  int	iUID;							// 数据库中保存的单位ID，如果界面没有设置显示单位，则作为缺省界面的显示单位
  double	dSensorToBit;							// 深度偏移(该曲线测点距离钻头的距离) 该值自动计算=MeasPointInfoTB.SensorToolToBot + BHAInfoDTB.ToolBotToBit
  double	dCurveMin;							// 最小值
  double	dCurveMax;							// 最大值
  float	fWarnMin;							// 预警最小值 显示共色线
  float	fWarnMax;							// 预警最大值 显示共色线
  float	fErrorMin;							// 错误最小值 显示红色线
  float	fErrorMax;							// 错误最大值 显示红色线
  double	dInvPointVal;							// 无效点值
  char	szFilter[TV_CURVEINFOTB_FILTER_DIGITS + 1];							// 滤波
  char	szTableName[TV_CURVEINFOTB_TABLENAME_DIGITS + 1];							// 对应存储表名
  char	szFieldName[TV_CURVEINFOTB_FIELDNAME_DIGITS + 1];							// 对应存储字段名
  int	iDataType;							// WITS数据类型:A|65.字符型;L|73.LONG;S|79.S;F|128.浮点型
  int	iDimension;							// 维数(常规曲线1，阵列曲线2)
  int	iPointCount;							// 点数(常规曲线1)
  char	szDescription_en[TV_CURVEINFOTB_DESCRIPTION_EN_DIGITS + 1];							// 参数英文描述
  char	szDescription_cn[TV_CURVEINFOTB_DESCRIPTION_CN_DIGITS + 1];							// 参数中文描述
  char	szCurveNameM[TV_CURVEINFOTB_CURVENAMEM_DIGITS + 1];							// 当前对应内存曲线名称
  int	iSOrder;							// 排序
  short	nIsCalc;							// 是否计算字段:NO|0.否;Yes|1.是
  short	nDataFrom;							// 曲线数据来源:RunTime|0.实时;Mem|1.内存数据;Las|3.Las数据;
  short	nFieldUsedNo;							// 对应存储字段名No
  char	szFieldName2[TV_CURVEINFOTB_FIELDNAME2_DIGITS + 1];							// 对应存储字段名2
  char	szFieldName3[TV_CURVEINFOTB_FIELDNAME3_DIGITS + 1];							// 对应存储字段名3
  char	szFieldName4[TV_CURVEINFOTB_FIELDNAME4_DIGITS + 1];							// 对应存储字段名4
  char	szFieldName5[TV_CURVEINFOTB_FIELDNAME5_DIGITS + 1];							// 对应存储字段名5
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_CURVEINFOTB_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_CURVEINFOTB;

typedef	TS_CURVEINFOTB FAR*	LPTS_CURVEINFOTB;

#pragma	pack(pop)

#endif // _TCURVEINFOTB_H

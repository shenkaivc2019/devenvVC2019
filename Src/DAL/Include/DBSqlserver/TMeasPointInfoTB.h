//---------------------------------------------------------------------------//
// 文件名: MeasPointInfoTB.h
// 说明:	工具的测量点信息表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020/10/19 22:11:35
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TMeasPointInfoTB.h : DMeasPointInfoTB

#ifndef	_TMEASPOINTINFOTB_H
#define	_TMEASPOINTINFOTB_H

#define	TID_MEASPOINTINFOTB								_T("MeasPointInfoTB")
#define	OID_MEASPOINTINFOTB								_T("")

// Sort No
#define	SORT_MEASPOINTINFOTB_PK0				0							// PK:测量点编号
//#define	SORT_MEASPOINTINFOTB_A1							%#%							// A1:

// Colum No
#define	COL_MEASPOINTINFOTB_MPID					(short)0						// 测量点编号
#define	COL_MEASPOINTINFOTB_TOOLID					(short)1						// 工具编号
#define	COL_MEASPOINTINFOTB_MPNAME					(short)2						// 测量点名称
#define	COL_MEASPOINTINFOTB_DESCRIPTION					(short)3						// 测量点描述
#define	COL_MEASPOINTINFOTB_SENSORTOOLTOBOT					(short)4						// 深度偏移(该测点距离所在工具底部距离)
#define	COL_MEASPOINTINFOTB_POSITION					(short)5						// 测点位置:Default|0.缺省无意义;Center|1.居中伽马; Sidewall|2.侧壁伽马
#define	COL_MEASPOINTINFOTB_SORDER					(short)6						// 排序
#define	COL_MEASPOINTINFOTB_CREATETIME					(short)7						// 创建时间戳
#define	COL_MEASPOINTINFOTB_STATUS					(short)8						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_MEASPOINTINFOTB_MEMO					(short)9						// 备注
#define	COL_MEASPOINTINFOTB_UPDCOUNT					(short)10						// 更新计数

// Colum(Field) Name
#define	FLD_MEASPOINTINFOTB_MPID					_T("MPID")					// 测量点编号
#define	FLD_MEASPOINTINFOTB_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_MEASPOINTINFOTB_MPNAME					_T("MPName")					// 测量点名称
#define	FLD_MEASPOINTINFOTB_DESCRIPTION					_T("Description")					// 测量点描述
#define	FLD_MEASPOINTINFOTB_SENSORTOOLTOBOT					_T("SensorToolToBot")					// 深度偏移(该测点距离所在工具底部距离)
#define	FLD_MEASPOINTINFOTB_POSITION					_T("Position")					// 测点位置:Default|0.缺省无意义;Center|1.居中伽马; Sidewall|2.侧壁伽马
#define	FLD_MEASPOINTINFOTB_SORDER					_T("SOrder")					// 排序
#define	FLD_MEASPOINTINFOTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_MEASPOINTINFOTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_MEASPOINTINFOTB_MEMO					_T("Memo")					// 备注
#define	FLD_MEASPOINTINFOTB_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_MEASPOINTINFOTB_MPNAME_DIGITS				50					// 测量点名称位数
#define	TV_MEASPOINTINFOTB_DESCRIPTION_DIGITS				50					// 测量点描述位数
#define	TV_MEASPOINTINFOTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_MEASPOINTINFOTB
{
 
  int	iMPID;							// 测量点编号
  int	iToolID;							// 工具编号
  char	szMPName[TV_MEASPOINTINFOTB_MPNAME_DIGITS + 1];							// 测量点名称
  char	szDescription[TV_MEASPOINTINFOTB_DESCRIPTION_DIGITS + 1];							// 测量点描述
  double	dSensorToolToBot;							// 深度偏移(该测点距离所在工具底部距离)
  short	nPosition;							// 测点位置:Default|0.缺省无意义;Center|1.居中伽马; Sidewall|2.侧壁伽马
  int	iSOrder;							// 排序
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_MEASPOINTINFOTB_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_MEASPOINTINFOTB;

typedef	TS_MEASPOINTINFOTB FAR*	LPTS_MEASPOINTINFOTB;

#pragma	pack(pop)

#endif // _TMEASPOINTINFOTB_H

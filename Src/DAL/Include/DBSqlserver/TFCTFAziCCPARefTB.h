//---------------------------------------------------------------------------//
// 文件名: FCTFAziCCPARefTB.h
// 说明:	CTF电阻率CCP1-4CCA1-4c对应电阻率参照表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020-03-07 23:51:31
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TFCTFAziCCPARefTB.h : DFCTFAziCCPARefTB

#ifndef	_TFCTFAZICCPAREFTB_H
#define	_TFCTFAZICCPAREFTB_H

#define	TID_FCTFAZICCPAREFTB								_T("FCTFAziCCPARefTB")
#define	OID_FCTFAZICCPAREFTB								_T("")

// Sort No
#define	SORT_FCTFAZICCPAREFTB_PK0				0							// PK:自增字段
//#define	SORT_FCTFAZICCPAREFTB_A1							%#%							// A1:

// Colum No
#define	COL_FCTFAZICCPAREFTB_CACRID					(short)0						// 自增字段
#define	COL_FCTFAZICCPAREFTB_CCP1					(short)1						// 校正幅度比(长源/低频)
#define	COL_FCTFAZICCPAREFTB_CCP2					(short)2						// 校正幅度比(短源/低频)
#define	COL_FCTFAZICCPAREFTB_CCP3					(short)3						// 校正幅度比(长源/高频)
#define	COL_FCTFAZICCPAREFTB_CCP4					(short)4						// 校正幅度比(短源/高频)
#define	COL_FCTFAZICCPAREFTB_CCA1					(short)5						// 校正相位差(长源/低频)
#define	COL_FCTFAZICCPAREFTB_CCA2					(short)6						// 校正相位差(短源/低频)
#define	COL_FCTFAZICCPAREFTB_CCA3					(short)7						// 校正相位差(长源/高频)
#define	COL_FCTFAZICCPAREFTB_CCA4					(short)8						// 校正相位差(短源/高频)
#define	COL_FCTFAZICCPAREFTB_RE1P					(short)9						// 视电阻率,校正幅度比(长源/低频)
#define	COL_FCTFAZICCPAREFTB_RE2P					(short)10						// 视电阻率,校正幅度比(短源/低频)
#define	COL_FCTFAZICCPAREFTB_RE3P					(short)11						// 视电阻率,校正幅度比(长源/高频)
#define	COL_FCTFAZICCPAREFTB_RE4P					(short)12						// 视电阻率,校正幅度比(短源/高频)
#define	COL_FCTFAZICCPAREFTB_RE1A					(short)13						// 视电阻率,校正相位差(长源/低频)
#define	COL_FCTFAZICCPAREFTB_RE2A					(short)14						// 视电阻率,校正相位差(短源/低频)
#define	COL_FCTFAZICCPAREFTB_RE3A					(short)15						// 视电阻率,校正相位差(长源/高频)
#define	COL_FCTFAZICCPAREFTB_RE4A					(short)16						// 视电阻率,校正相位差(短源/高频)

// Colum(Field) Name
#define	FLD_FCTFAZICCPAREFTB_CACRID					_T("CACRID")					// 自增字段
#define	FLD_FCTFAZICCPAREFTB_CCP1					_T("CCP1")					// 校正幅度比(长源/低频)
#define	FLD_FCTFAZICCPAREFTB_CCP2					_T("CCP2")					// 校正幅度比(短源/低频)
#define	FLD_FCTFAZICCPAREFTB_CCP3					_T("CCP3")					// 校正幅度比(长源/高频)
#define	FLD_FCTFAZICCPAREFTB_CCP4					_T("CCP4")					// 校正幅度比(短源/高频)
#define	FLD_FCTFAZICCPAREFTB_CCA1					_T("CCA1")					// 校正相位差(长源/低频)
#define	FLD_FCTFAZICCPAREFTB_CCA2					_T("CCA2")					// 校正相位差(短源/低频)
#define	FLD_FCTFAZICCPAREFTB_CCA3					_T("CCA3")					// 校正相位差(长源/高频)
#define	FLD_FCTFAZICCPAREFTB_CCA4					_T("CCA4")					// 校正相位差(短源/高频)
#define	FLD_FCTFAZICCPAREFTB_RE1P					_T("Re1p")					// 视电阻率,校正幅度比(长源/低频)
#define	FLD_FCTFAZICCPAREFTB_RE2P					_T("Re2p")					// 视电阻率,校正幅度比(短源/低频)
#define	FLD_FCTFAZICCPAREFTB_RE3P					_T("Re3p")					// 视电阻率,校正幅度比(长源/高频)
#define	FLD_FCTFAZICCPAREFTB_RE4P					_T("Re4p")					// 视电阻率,校正幅度比(短源/高频)
#define	FLD_FCTFAZICCPAREFTB_RE1A					_T("Re1a")					// 视电阻率,校正相位差(长源/低频)
#define	FLD_FCTFAZICCPAREFTB_RE2A					_T("Re2a")					// 视电阻率,校正相位差(短源/低频)
#define	FLD_FCTFAZICCPAREFTB_RE3A					_T("Re3a")					// 视电阻率,校正相位差(长源/高频)
#define	FLD_FCTFAZICCPAREFTB_RE4A					_T("Re4a")					// 视电阻率,校正相位差(短源/高频)

// Colum(Field) Values

// Colum(Field)min,max


// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_FCTFAZICCPAREFTB
{
 
  int	iCACRID;							// 自增字段
  float	fCCP1;							// 校正幅度比(长源/低频)
  float	fCCP2;							// 校正幅度比(短源/低频)
  float	fCCP3;							// 校正幅度比(长源/高频)
  float	fCCP4;							// 校正幅度比(短源/高频)
  float	fCCA1;							// 校正相位差(长源/低频)
  float	fCCA2;							// 校正相位差(短源/低频)
  float	fCCA3;							// 校正相位差(长源/高频)
  float	fCCA4;							// 校正相位差(短源/高频)
  float	fRe1p;							// 视电阻率,校正幅度比(长源/低频)
  float	fRe2p;							// 视电阻率,校正幅度比(短源/低频)
  float	fRe3p;							// 视电阻率,校正幅度比(长源/高频)
  float	fRe4p;							// 视电阻率,校正幅度比(短源/高频)
  float	fRe1a;							// 视电阻率,校正相位差(长源/低频)
  float	fRe2a;							// 视电阻率,校正相位差(短源/低频)
  float	fRe3a;							// 视电阻率,校正相位差(长源/高频)
  float	fRe4a;							// 视电阻率,校正相位差(短源/高频)
} TS_FCTFAZICCPAREFTB;

typedef	TS_FCTFAZICCPAREFTB FAR*	LPTS_FCTFAZICCPAREFTB;

#pragma	pack(pop)

#endif // _TFCTFAZICCPAREFTB_H

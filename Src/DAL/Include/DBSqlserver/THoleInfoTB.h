//---------------------------------------------------------------------------//
// 文件名: HoleInfoTB.h
// 说明:	井眼信息表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2022/11/9 9:30:13
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// THoleInfoTB.h : DHoleInfoTB

#ifndef	_THOLEINFOTB_H
#define	_THOLEINFOTB_H

#define	TID_HOLEINFOTB								_T("HoleInfoTB")
#define	OID_HOLEINFOTB								_T("")

// Sort No
#define	SORT_HOLEINFOTB_PK0				0							// PK:井眼编号
//#define	SORT_HOLEINFOTB_A1							%#%							// A1:

// Colum No
#define	COL_HOLEINFOTB_HOLEID					(short)0						// 井眼编号
#define	COL_HOLEINFOTB_WELLID					(short)1						// 井编号
#define	COL_HOLEINFOTB_HOLENAME					(short)2						// 井眼名称
#define	COL_HOLEINFOTB_PARENTHOLEID					(short)3						// 父井眼编号
#define	COL_HOLEINFOTB_STARTDEPTH					(short)4						// 起始井深精确到0.1mm
#define	COL_HOLEINFOTB_ENDDEPTH					(short)5						// 结束井深精确到0.1mm
#define	COL_HOLEINFOTB_JOGNUMBER					(short)6						// 作业编号
#define	COL_HOLEINFOTB_APISN					(short)7						// API编号
#define	COL_HOLEINFOTB_GEOLOGIST1					(short)8						// 地质学家1
#define	COL_HOLEINFOTB_GEOLOGIST2					(short)9						// 地质学家2
#define	COL_HOLEINFOTB_DIRECTIONALDRILLER1					(short)10						// 定向工程师1
#define	COL_HOLEINFOTB_DIRECTIONALDRILLER2					(short)11						// 定向工程师2
#define	COL_HOLEINFOTB_MWDENGNEER1					(short)12						// MWD工程师1
#define	COL_HOLEINFOTB_MWDENGNEER2					(short)13						// MWD工程师2
#define	COL_HOLEINFOTB_HOLESIZE					(short)14						// 井眼尺寸（裸眼直经）
#define	COL_HOLEINFOTB_CREATETIME					(short)15						// 创建时间戳
#define	COL_HOLEINFOTB_STATUS					(short)16						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_HOLEINFOTB_MEMO					(short)17						// 备注
#define	COL_HOLEINFOTB_UPDCOUNT					(short)18						// 更新计数

// Colum(Field) Name
#define	FLD_HOLEINFOTB_HOLEID					_T("HoleID")					// 井眼编号
#define	FLD_HOLEINFOTB_WELLID					_T("WellID")					// 井编号
#define	FLD_HOLEINFOTB_HOLENAME					_T("HoleName")					// 井眼名称
#define	FLD_HOLEINFOTB_PARENTHOLEID					_T("ParentHoleID")					// 父井眼编号
#define	FLD_HOLEINFOTB_STARTDEPTH					_T("StartDepth")					// 起始井深精确到0.1mm
#define	FLD_HOLEINFOTB_ENDDEPTH					_T("EndDepth")					// 结束井深精确到0.1mm
#define	FLD_HOLEINFOTB_JOGNUMBER					_T("JogNumber")					// 作业编号
#define	FLD_HOLEINFOTB_APISN					_T("APISN")					// API编号
#define	FLD_HOLEINFOTB_GEOLOGIST1					_T("Geologist1")					// 地质学家1
#define	FLD_HOLEINFOTB_GEOLOGIST2					_T("Geologist2")					// 地质学家2
#define	FLD_HOLEINFOTB_DIRECTIONALDRILLER1					_T("DirectionalDriller1")					// 定向工程师1
#define	FLD_HOLEINFOTB_DIRECTIONALDRILLER2					_T("DirectionalDriller2")					// 定向工程师2
#define	FLD_HOLEINFOTB_MWDENGNEER1					_T("MWDEngneer1")					// MWD工程师1
#define	FLD_HOLEINFOTB_MWDENGNEER2					_T("MWDEngneer2")					// MWD工程师2
#define	FLD_HOLEINFOTB_HOLESIZE					_T("HoleSize")					// 井眼尺寸（裸眼直经）
#define	FLD_HOLEINFOTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_HOLEINFOTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_HOLEINFOTB_MEMO					_T("Memo")					// 备注
#define	FLD_HOLEINFOTB_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_HOLEINFOTB_HOLEID_DIGITS				50					// 井眼编号位数
#define	TV_HOLEINFOTB_WELLID_DIGITS				50					// 井编号位数
#define	TV_HOLEINFOTB_HOLENAME_DIGITS				50					// 井眼名称位数
#define	TV_HOLEINFOTB_PARENTHOLEID_DIGITS				50					// 父井眼编号位数
#define	TV_HOLEINFOTB_JOGNUMBER_DIGITS				50					// 作业编号位数
#define	TV_HOLEINFOTB_APISN_DIGITS				50					// API编号位数
#define	TV_HOLEINFOTB_GEOLOGIST1_DIGITS				50					// 地质学家1位数
#define	TV_HOLEINFOTB_GEOLOGIST2_DIGITS				50					// 地质学家2位数
#define	TV_HOLEINFOTB_DIRECTIONALDRILLER1_DIGITS				50					// 定向工程师1位数
#define	TV_HOLEINFOTB_DIRECTIONALDRILLER2_DIGITS				50					// 定向工程师2位数
#define	TV_HOLEINFOTB_MWDENGNEER1_DIGITS				50					// MWD工程师1位数
#define	TV_HOLEINFOTB_MWDENGNEER2_DIGITS				50					// MWD工程师2位数
#define	TV_HOLEINFOTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_HOLEINFOTB
{
 
  char	szHoleID[TV_HOLEINFOTB_HOLEID_DIGITS + 1];							// 井眼编号
  char	szWellID[TV_HOLEINFOTB_WELLID_DIGITS + 1];							// 井编号
  char	szHoleName[TV_HOLEINFOTB_HOLENAME_DIGITS + 1];							// 井眼名称
  char	szParentHoleID[TV_HOLEINFOTB_PARENTHOLEID_DIGITS + 1];							// 父井眼编号
  long	lStartDepth;							// 起始井深精确到0.1mm
  long	lEndDepth;							// 结束井深精确到0.1mm
  char	szJogNumber[TV_HOLEINFOTB_JOGNUMBER_DIGITS + 1];							// 作业编号
  char	szAPISN[TV_HOLEINFOTB_APISN_DIGITS + 1];							// API编号
  char	szGeologist1[TV_HOLEINFOTB_GEOLOGIST1_DIGITS + 1];							// 地质学家1
  char	szGeologist2[TV_HOLEINFOTB_GEOLOGIST2_DIGITS + 1];							// 地质学家2
  char	szDirectionalDriller1[TV_HOLEINFOTB_DIRECTIONALDRILLER1_DIGITS + 1];							// 定向工程师1
  char	szDirectionalDriller2[TV_HOLEINFOTB_DIRECTIONALDRILLER2_DIGITS + 1];							// 定向工程师2
  char	szMWDEngneer1[TV_HOLEINFOTB_MWDENGNEER1_DIGITS + 1];							// MWD工程师1
  char	szMWDEngneer2[TV_HOLEINFOTB_MWDENGNEER2_DIGITS + 1];							// MWD工程师2
  double	dHoleSize;							// 井眼尺寸（裸眼直经）
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_HOLEINFOTB_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_HOLEINFOTB;

typedef	TS_HOLEINFOTB FAR*	LPTS_HOLEINFOTB;

#pragma	pack(pop)

#endif // _THOLEINFOTB_H

//---------------------------------------------------------------------------//
// 文件名: ToolInfoTB.h
// 说明:	工具信息表
// 公司名: 上海神开测控技术有限公司
// 作成者: 李铁刚
// 创建时间：2020/10/19 22:11:35
// 备注:	无
//---------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////
// TToolInfoTB.h : DToolInfoTB

#ifndef	_TTOOLINFOTB_H
#define	_TTOOLINFOTB_H

#define	TID_TOOLINFOTB								_T("ToolInfoTB")
#define	OID_TOOLINFOTB								_T("")

// Sort No
#define	SORT_TOOLINFOTB_PK0				0							// PK:工具编号
//#define	SORT_TOOLINFOTB_A1							%#%							// A1:

// Colum No
#define	COL_TOOLINFOTB_TOOLID					(short)0						// 工具编号
#define	COL_TOOLINFOTB_TOOLNAME					(short)1						// 工具名称（曲线所属工具名称）
#define	COL_TOOLINFOTB_TCID					(short)2						// 工具分类编号
#define	COL_TOOLINFOTB_TOOLALIAS					(short)3						// 工具别名
#define	COL_TOOLINFOTB_TOOLPIC					(short)4						// 工具图片
#define	COL_TOOLINFOTB_TOOLDESCRIPTION					(short)5						// 工具描述
#define	COL_TOOLINFOTB_TOOLPN					(short)6						// 工具PartNumber
#define	COL_TOOLINFOTB_TOOLLENGTH					(short)7						// 长度
#define	COL_TOOLINFOTB_TOOLWEIGHT					(short)8						// 重量
#define	COL_TOOLINFOTB_TOOLINTDIA					(short)9						// 内径
#define	COL_TOOLINFOTB_TOOLEXTDIA					(short)10						// 外径
#define	COL_TOOLINFOTB_TOOLODMAX					(short)11						// 最大外径
#define	COL_TOOLINFOTB_TOOLODMIN					(short)12						// 最小外径
#define	COL_TOOLINFOTB_TOOLPREMAX					(short)13						// 最大压强
#define	COL_TOOLINFOTB_TOOLTEMPMAX					(short)14						// 最高温度
#define	COL_TOOLINFOTB_INDLL					(short)15						// 工具所在DLL名称
#define	COL_TOOLINFOTB_CONNECTIONSD					(short)16						// 扣型下
#define	COL_TOOLINFOTB_CONNECTIONSU					(short)17						// 扣型上
#define	COL_TOOLINFOTB_TORQUE					(short)18						// 扭矩
#define	COL_TOOLINFOTB_SORDER					(short)19						// 排序
#define	COL_TOOLINFOTB_CREATETIME					(short)20						// 创建时间戳
#define	COL_TOOLINFOTB_STATUS					(short)21						// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	COL_TOOLINFOTB_MEMO					(short)22						// 备注
#define	COL_TOOLINFOTB_UPDCOUNT					(short)23						// 更新计数

// Colum(Field) Name
#define	FLD_TOOLINFOTB_TOOLID					_T("ToolID")					// 工具编号
#define	FLD_TOOLINFOTB_TOOLNAME					_T("ToolName")					// 工具名称（曲线所属工具名称）
#define	FLD_TOOLINFOTB_TCID					_T("TCID")					// 工具分类编号
#define	FLD_TOOLINFOTB_TOOLALIAS					_T("ToolAlias")					// 工具别名
#define	FLD_TOOLINFOTB_TOOLPIC					_T("ToolPic")					// 工具图片
#define	FLD_TOOLINFOTB_TOOLDESCRIPTION					_T("ToolDescription")					// 工具描述
#define	FLD_TOOLINFOTB_TOOLPN					_T("ToolPN")					// 工具PartNumber
#define	FLD_TOOLINFOTB_TOOLLENGTH					_T("ToolLength")					// 长度
#define	FLD_TOOLINFOTB_TOOLWEIGHT					_T("ToolWeight")					// 重量
#define	FLD_TOOLINFOTB_TOOLINTDIA					_T("ToolIntDia")					// 内径
#define	FLD_TOOLINFOTB_TOOLEXTDIA					_T("ToolExtDia")					// 外径
#define	FLD_TOOLINFOTB_TOOLODMAX					_T("ToolODMax")					// 最大外径
#define	FLD_TOOLINFOTB_TOOLODMIN					_T("ToolODMin")					// 最小外径
#define	FLD_TOOLINFOTB_TOOLPREMAX					_T("ToolPreMax")					// 最大压强
#define	FLD_TOOLINFOTB_TOOLTEMPMAX					_T("ToolTempMax")					// 最高温度
#define	FLD_TOOLINFOTB_INDLL					_T("InDLL")					// 工具所在DLL名称
#define	FLD_TOOLINFOTB_CONNECTIONSD					_T("ConnectionsD")					// 扣型下
#define	FLD_TOOLINFOTB_CONNECTIONSU					_T("ConnectionsU")					// 扣型上
#define	FLD_TOOLINFOTB_TORQUE					_T("Torque")					// 扭矩
#define	FLD_TOOLINFOTB_SORDER					_T("SOrder")					// 排序
#define	FLD_TOOLINFOTB_CREATETIME					_T("CreateTime")					// 创建时间戳
#define	FLD_TOOLINFOTB_STATUS					_T("Status")					// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
#define	FLD_TOOLINFOTB_MEMO					_T("Memo")					// 备注
#define	FLD_TOOLINFOTB_UPDCOUNT					_T("UpdCount")					// 更新计数

// Colum(Field) Values

// Colum(Field)min,max
#define	TV_TOOLINFOTB_TOOLNAME_DIGITS				50					// 工具名称（曲线所属工具名称）位数
#define	TV_TOOLINFOTB_TCID_DIGITS				10					// 工具分类编号位数
#define	TV_TOOLINFOTB_TOOLALIAS_DIGITS				50					// 工具别名位数
#define	TV_TOOLINFOTB_TOOLPIC_DIGITS				255					// 工具图片位数
#define	TV_TOOLINFOTB_TOOLDESCRIPTION_DIGITS				50					// 工具描述位数
#define	TV_TOOLINFOTB_TOOLPN_DIGITS				50					// 工具PartNumber位数
#define	TV_TOOLINFOTB_INDLL_DIGITS				50					// 工具所在DLL名称位数
#define	TV_TOOLINFOTB_CONNECTIONSD_DIGITS				50					// 扣型下位数
#define	TV_TOOLINFOTB_CONNECTIONSU_DIGITS				50					// 扣型上位数
#define	TV_TOOLINFOTB_TORQUE_DIGITS				50					// 扭矩位数
#define	TV_TOOLINFOTB_MEMO_DIGITS				100					// 备注位数

// TableDefine
#pragma	pack(push, 1)
typedef struct tagTS_TOOLINFOTB
{
 
  int	iToolID;							// 工具编号
  char	szToolName[TV_TOOLINFOTB_TOOLNAME_DIGITS + 1];							// 工具名称（曲线所属工具名称）
  char	szTCID[TV_TOOLINFOTB_TCID_DIGITS + 1];							// 工具分类编号
  char	szToolAlias[TV_TOOLINFOTB_TOOLALIAS_DIGITS + 1];							// 工具别名
  char	szToolPic[TV_TOOLINFOTB_TOOLPIC_DIGITS + 1];							// 工具图片
  char	szToolDescription[TV_TOOLINFOTB_TOOLDESCRIPTION_DIGITS + 1];							// 工具描述
  char	szToolPN[TV_TOOLINFOTB_TOOLPN_DIGITS + 1];							// 工具PartNumber
  double	dToolLength;							// 长度
  double	dToolWeight;							// 重量
  double	dToolIntDia;							// 内径
  double	dToolExtDia;							// 外径
  double	dToolODMax;							// 最大外径
  double	dToolODMin;							// 最小外径
  double	dToolPreMax;							// 最大压强
  double	dToolTempMax;							// 最高温度
  char	szInDLL[TV_TOOLINFOTB_INDLL_DIGITS + 1];							// 工具所在DLL名称
  char	szConnectionsD[TV_TOOLINFOTB_CONNECTIONSD_DIGITS + 1];							// 扣型下
  char	szConnectionsU[TV_TOOLINFOTB_CONNECTIONSU_DIGITS + 1];							// 扣型上
  char	szTorque[TV_TOOLINFOTB_TORQUE_DIGITS + 1];							// 扭矩
  int	iSOrder;							// 排序
  time_t	lCreateTime;							// 创建时间戳
  short	nStatus;							// 状态:Deleted|0.删除;InUse|1.使用中; Frozen|2.冻结
  char	szMemo[TV_TOOLINFOTB_MEMO_DIGITS + 1];							// 备注
  int	iUpdCount;							// 更新计数
} TS_TOOLINFOTB;

typedef	TS_TOOLINFOTB FAR*	LPTS_TOOLINFOTB;

#pragma	pack(pop)

#endif // _TTOOLINFOTB_H
